
IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'ANNYPAYPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
CREATE TABLE VM1DTA.ANNYPAYPF(

UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL,
CHDRNUM nchar(8) NULL,
CHDRCOY nchar(1) NULL,
ANNPMNTOPT nchar(2) NULL,
ANNUPMNTTRM int NULL,
ANNPMNTFREQ nchar(2) NULL,
DFRRDTRM  int NULL,
PMNTNO int NULL,
PMNTDT int NULL,
NEXTPAYDT int NULL,
ANNAMT numeric(17, 2) NULL,
ANNINTAMT numeric(17, 2) NULL,
PAIDAMT numeric(17, 2) NULL, 
INTTAX numeric(17, 2) NULL,
WITHOLDTAX numeric(17, 2) NULL,
PMNTSTAT nchar(2) NULL,
USRPRF nchar(10) NULL,
DATIME datetime2(6) NULL,


  
	CONSTRAINT [PK_ANNYPAYPF] PRIMARY KEY CLUSTERED 
 (
       [UNIQUE_NUMBER] ASC
 )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annuity Payment' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Company' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annuity Payment Option' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'ANNPMNTOPT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annuity Payment Term' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'ANNUPMNTTRM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annuity Payment Frequency' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'ANNPMNTFREQ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deferred Term' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'DFRRDTRM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'PMNTNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'PMNTDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Next Payment Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'NEXTPAYDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annuity Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'ANNAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annuity with Interest' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'ANNINTAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Paid Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'PAIDAMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Interest Tax' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'INTTAX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Witholding Tax' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'WITHOLDTAX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment Status' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'PMNTSTAT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Profile' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'DATIME';


END
GO