SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.FLDDPF i WHERE i.FDID='AGNCYSEL' AND i.NMPG='AGNCYSEL') 
insert INTO VM1DTA.FLDDPF(FDID, NMPG, TYPE_T, EDIT, LENF, DECP, WNTP, WNST, TABW, TCOY, ERRCD, PROG01, PROG02, PROG03, PROG04, CFID, TERMID, 
USER_T, TRDT, TRTM, ADDF01, ADDF02, ADDF03, ADDF04, CURRFDID, ALLOCLEN, DBDATIME, SFDATIME, DBCSCAP, USRPRF, JOBNM, DATIME, TYPE) 
VALUES('AGNCYSEL', 'AGNCYSEL', 'A', ' ',10, 0, 'D ', 'N ', ' ', ' ', ' ', 'CLTWD', 'P2473', 'P2594', ' ', 'AGNCYNAME ','DSP1', 0, 
0, 0, ' ', ' ', ' ', ' ', ' ', 0, getdate(), getdate(),'N', 'UNDERWR1', 'DSP14', getdate(), ' ');
GO
SET QUOTED_IDENTIFIER OFF  
GO