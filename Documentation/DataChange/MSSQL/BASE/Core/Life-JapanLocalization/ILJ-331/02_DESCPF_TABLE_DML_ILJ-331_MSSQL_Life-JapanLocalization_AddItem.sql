
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1690' and d.DESCITEM='PLJ30' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1690','PLJ30','  ','E','','Life','Agency Maintenance Sub Menu','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO 

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1688' and d.DESCITEM='SAS6' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1688','SAS6','  ','E','','SAS6','Agency Maintenance Submenu','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1688' and d.DESCITEM='TAS7' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1688','TAS7','  ','E','','TAS7','Agency Appointment','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1688' and d.DESCITEM='TAS8' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1688','TAS8','  ','E','','TAS8','Agency Modify','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1688' and d.DESCITEM='TAS9' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1688','TAS9','  ','E','','TAS9','Agency Enquiry','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1688' and d.DESCITEM='TASA' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1688','TASA','  ','E','','TASA','Agency Terminate','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='T1688' and d.DESCITEM='TASB' and d.DESCCOY='2' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','T1688','TASB','  ','E','','TASB','Agency Reinstate','UNDERWR1  ','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF  
GO
