SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'DECLDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BR50HDTODATA'))       
    BEGIN
		alter table BR50HDTODATA add DECLDATE numeric(8, 0);
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<DECLDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BR50HDTODATA', @level2type=N'COLUMN',@level2name=N'DECLDATE ';
	END
GO
SET QUOTED_IDENTIFIER OFF

