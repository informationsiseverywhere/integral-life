SET QUOTED_IDENTIFIER ON  

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[erorpf] e
where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL15' AND ERORDESC='有効期限開始日必須' )

INSERT INTO [VM1DTA].[erorpf] (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
VALUES ( 'ER', '', 'J','','JL15','有効期限開始日必須', '', '', '', '', '', '',getDate(),'');
GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[erorpf] e
where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL16' AND ERORDESC='有効期限終了日必須' )

INSERT INTO[VM1DTA].[erorpf] (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
VALUES ( 'ER', '', 'J','','JL16','有効期限終了日必須', '', '', '', '', '', '',getDate(),'');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[erorpf] e
where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL17' AND ERORDESC='有効期限開始日が有効期限終了日より後' )

INSERT INTO [VM1DTA].[erorpf] (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
VALUES ( 'ER', '', 'J','','JL17','有効期限開始日が有効期限終了日より後', '', '', '', '', '', '',getDate(),'');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[erorpf] e
where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL18' AND ERORDESC='資格番号必須' )

INSERT INTO [VM1DTA].[erorpf] (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
VALUES ( 'ER', '', 'J','','JL18','資格番号必須', '', '', '', '', '', '',getDate(),'');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[erorpf] e
where e.ERORPFX='ER' AND e.ERORLANG='J' AND e.EROREROR='JL13' AND ERORDESC='番号は英数字のみ' )

INSERT INTO[VM1DTA].[erorpf] (ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
VALUES ( 'ER', '', 'J','','JL13','番号は英数字のみ', '', '', '', '', '', '',getDate(),'');
Go

SET QUOTED_IDENTIFIER OFF  
GO