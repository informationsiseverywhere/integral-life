
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RUOM')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','E','','RUOM', '> allowable period' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF 
GO

SET QUOTED_IDENTIFIER ON  
GO 

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'RUOM')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','J','','RUOM', N'> 許容期間' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RUON')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','E','','RUON', 'Did you complete the underwriting by medical examination?' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');
 
GO
SET QUOTED_IDENTIFIER OFF
GO  

SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'RUON')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','J','','RUON', N'診査による査定を完了しましたか？' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RUOO')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','E','','RUOO', 'Sum insured is over the total limit. Do you want to proceed?' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO


IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'RUOO')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','J','','RUOO', N'通算限度額を超えています。処理を続けますか？' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF 
GO 

SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'RUOB')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','E','','RUOB', 'Repay Loans first' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF  
GO

SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'RUOB')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE]) 
VALUES ('ER',' ','J','','RUOB', N'貸付返済を先に行う' ,'' ,'' ,'' ,'','UNDERWR1' ,'UNDERWR1',CURRENT_TIMESTAMP,' ');

GO
SET QUOTED_IDENTIFIER OFF  
GO