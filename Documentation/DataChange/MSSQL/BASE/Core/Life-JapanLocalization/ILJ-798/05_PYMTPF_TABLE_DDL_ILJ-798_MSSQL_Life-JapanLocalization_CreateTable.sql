IF NOT EXISTS (SELECT * FROM sys.tables where name = 'PYMTPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN

CREATE TABLE [VM1DTA].[PYMTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[PAYNUM_PYMT] [nchar](9) ,
	[PAYNUM_CHEQ] [nchar](9) ,
	[CLAIMNUM] [nchar](9),
	[PAYEE] [nchar](8) ,
	[CLNTNAME] [nchar](50) ,
	[BANKKEY] [nchar] (10) ,
	[BANKDESC] [nchar] (50) ,
	[ACCTYPE] [nchar] (30) ,
	[BANKACCKEY] [nchar](20),
	[ACCTNAME] [nchar](50) ,
	[PRNT] [numeric](5) ,
	[AMOUNT] [numeric](17) ,
	[PROCIND] [nchar](2) ,
	[REQNTYPE] [nchar](1),
	[PAYDATE] [int],
	[REQNBCDE] [nchar](2),
	[CREATEUSER] [nchar](15),
	[CREATEDATE] [int] NULL,
	[MODUSER] [nchar](15),
	[MODDATE] [int] NULL,
	[APPRUSER] [nchar](15),
	[APPRDTE] [int] NULL,
	[AUTHUSER] [nchar](15),
	[AUTHDATE] [int] NULL,
	[TOTALPYBLAMOUNT] [numeric](17) ,

 CONSTRAINT [PK_PYMTPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Payment Data' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment number of pymtpf' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'PAYNUM_PYMT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment number of cheqpf' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'PAYNUM_CHEQ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Claim Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'CLAIMNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payee' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'PAYEE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'CLNTNAME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank Key' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'BANKKEY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank Description' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'BANKDESC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'ACCTYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank Account Key' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'BANKACCKEY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'ACCTNAME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentage' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'PRNT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'AMOUNT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Process Indicator' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'PROCIND';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Request Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'REQNTYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'PAYDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'REQNBCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created By' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'CREATEUSER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'CREATEDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modified By' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'MODUSER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modified Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'MODDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Approved By' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'APPRUSER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Approved Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'APPRDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authorized By' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'AUTHUSER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Authrized Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'AUTHDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Payable Amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'PYMTPF', @level2type=N'COLUMN',@level2name=N'TOTALPYBLAMOUNT';

	END
GO