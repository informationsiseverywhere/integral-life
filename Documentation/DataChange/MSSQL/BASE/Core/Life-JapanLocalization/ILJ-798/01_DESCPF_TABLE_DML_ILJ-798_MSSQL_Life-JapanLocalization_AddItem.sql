SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T3642' AND DESCITEM='RQ02' AND LANGUAGE='E' AND
SHORTDESC='Clm Pymt' AND LONGDESC='Claim Payment Requisition')
Insert into VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
values ('IT','2','T3642','RQ02','','E','','Clm Pymt','Claim Payment Requisition','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TR386' AND DESCITEM='EPJL54' AND LANGUAGE='E' AND
SHORTDESC='Clm Pymt' AND LONGDESC='Claim Payment')
Insert into VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
values ('IT','2','TR386','EPJL54','','E','','Clm Pymt','Claim Payment ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TR386' AND DESCITEM='EPJL56' AND LANGUAGE='E' AND
SHORTDESC='Clm Confrm' AND LONGDESC='Claim Confirmation')
Insert into VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
values ('IT','2','TR386','EPJL56','','E','','Clm Confrm','Claim Confirmation','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='T3642' AND DESCITEM='RQ02' AND LANGUAGE='J' AND
SHORTDESC='Clm Pymt' AND LONGDESC=N'保険金支払番号')
Insert into VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
values ('IT','2','T3642','RQ02','','J','','Clm Pymt',N'保険金支払番号','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TR386' AND DESCITEM='JPJL54' AND LANGUAGE='J' AND
SHORTDESC='Clm Pymt' AND LONGDESC=N'保険金支払')
Insert into VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
values ('IT','2','TR386','JPJL54','','J','','Clm Pymt',N'保険金支払','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCPFX='IT' AND DESCCOY='2' AND DESCTABL='TR386' AND DESCITEM='JPJL56' AND LANGUAGE='J' AND
SHORTDESC='Clm Confrm' AND LONGDESC=N'クレーム確認')
Insert into VM1DTA.DESCPF ([DESCPFX],[DESCCOY],[DESCTABL],[DESCITEM],[ITEMSEQ],[LANGUAGE],[TRANID],[SHORTDESC],[LONGDESC],[USRPRF],[JOBNM],[DATIME]) 
values ('IT','2','TR386','JPJL56','','J','','Clm Confrm',N'クレーム確認','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO

--TA522(IT)
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TA522' and DESCITEM='DC' and ITEMSEQ='  ' and LANGUAGE='E')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
 values ('IT','2','TA522','DC','  ','E','','DC','Death Claim','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF 
GO



SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TA522' and DESCITEM='RP' and ITEMSEQ='  ' and LANGUAGE='E')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TA522','RP','  ','E','','RP','Regular Payment Claim','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF 
GO

--TA522(HE)
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='HE' and DESCCOY='2' and DESCTABL='TA522' and DESCITEM=' ' and ITEMSEQ='  ' and LANGUAGE='E')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('HE','2','TA522',' ','  ','E','','IT','Incident Type','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF 
GO

--TA522(IT)
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TA522' and DESCITEM='DC' and ITEMSEQ='  ' and LANGUAGE='J')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TA522','DC','  ','J','','DC',N'死亡請求','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF 
GO



SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='TA522' and DESCITEM='RP' and ITEMSEQ='  ' and LANGUAGE='J')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TA522','RP','  ','J','','RP',N'給付金請求','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF 
GO

--TA522(HE)
SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='HE' and DESCCOY='2' and DESCTABL='TA522' and DESCITEM=' ' and ITEMSEQ='  ' and LANGUAGE='J')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('HE','2','TA522',' ','  ','J','','IT',N'事故タイプ','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF 
GO
