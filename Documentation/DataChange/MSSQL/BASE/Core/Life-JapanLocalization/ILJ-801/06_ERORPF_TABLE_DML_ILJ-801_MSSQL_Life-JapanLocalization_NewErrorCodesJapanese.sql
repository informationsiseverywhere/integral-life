SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL71')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL71', N'無効なクレーム番号です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL72')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL72', N'支払番号は不要です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO	 

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL73')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL73', N'クレーム番号は不要です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL74')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL74', N'銀行コードは不要です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL75')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL75', N'支払番号が必要です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL76')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL76', N'保険金支払は既に承認１済です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL77')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL77', N'保険金支払は既に承認２済です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL78')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL78', N'承認権限がありません','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL79')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL79', N'ユーザーに権限がありません','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL83')
INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	 VALUES ('ER','','J','','JL83', N'保険金支払は作成済です','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
GO
SET QUOTED_IDENTIFIER OFF 
GO



