SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (	SELECT * FROM [VM1DTA].[FLDTPF]	WHERE FDID = 'CLAIMNMBER' AND LANG = 'E' )
INSERT INTO [VM1DTA].[FLDTPF] (FDID, LANG, FDTX, COLH01, COLH02, COLH03, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME)
 VALUES ( 'CLAIMNMBER', 'E', 'CLAIM NUMBER', 'Claim Number', '', '', '', 0, 0, 0, 'UNDERWR1  ', 'UNDERWR1', CURRENT_TIMESTAMP);
GO

IF NOT EXISTS (	SELECT * FROM [VM1DTA].[FLDTPF]	WHERE FDID = 'BENEFTYPE' AND LANG = 'E' )
INSERT INTO [VM1DTA].[FLDTPF] (FDID, LANG, FDTX, COLH01, COLH02, COLH03, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME)
 VALUES ( 'PAYMTNUM', 'E', 'PAYMENT NUMBER', 'Payment Number', '', '', '', 0, 0, 0, 'UNDERWR1  ', 'UNDERWR1', CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF  
GO
