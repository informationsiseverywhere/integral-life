SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TR2A5' AND DESCITEM='BTPRO027' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TR2A5','BTPRO027','  ','J',' ','BCJL12', N'保険料請求・収納','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF