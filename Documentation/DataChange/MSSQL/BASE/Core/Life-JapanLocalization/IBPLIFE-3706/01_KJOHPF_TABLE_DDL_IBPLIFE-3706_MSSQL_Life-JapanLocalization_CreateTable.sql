IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'KJOHPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN 
create TABLE [VM1DTA].[KJOHPF](
       [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
       [CHDRCOY] [nchar] (1) NULL,
       [CHDRNUM] [nchar](8) NULL,
	   [PLNSFX] [int] NULL,
       [LIFE] [nchar](2) NULL,
       [JLIFE] [nchar](2) NULL,
       [TRANNO] [int] NULL,
       [TERMID] [nchar](4) NULL,
	   [USER_T] [int] NULL,
	   [EFFDATE] [int] NULL,
	   [CURRCD] [nchar](3) NULL,
	   [CLAMTYP] [nchar](2) NULL,
	   [POLICYLOAN] [numeric] (17, 2) NULL,
	   [OTHERADJST] [numeric](17, 2) NULL,
	   [REASONCD] [nchar](4) NULL,
	   [RESNDESC] [nchar](50) NULL,
	   [CNTTYPE] [nchar](3) NULL,
	   [TRTM] [int] NULL,
	   [PLINT01] [numeric](17, 2) NULL,
	   [PLINT02] [numeric](17, 2) NULL,
	   [APLAMT01] [numeric](17, 2) NULL,
	   [APLAMT02] [numeric](17, 2) NULL,
	   [APLINT01] [numeric](17, 2) NULL,
	   [APLINT02] [numeric](17, 2) NULL,
	   [PREMSUSP] [numeric](17, 2) NULL,
	   [PREMADVBAL] [numeric](17, 2) NULL,
	   [PREMADVINT] [numeric](17, 2) NULL,
	   [SURRTAX] [numeric](17, 2) NULL,
	   [INTEREST] [numeric](17, 2) NULL,
	   [INTDAYS] [int] NULL,
	   [PAYCLT] [nchar](8) NULL,
	   [BANKKEY] [nchar](10) NULL,
	   [BANKACCKEY] [nchar](20) NULL,
	   [BANKACTYPE] [numeric](1, 0) NULL,
	   [PAYMMETH] [nchar](2) NULL,
	   [SURRVAL01] [numeric](17, 2) NULL,
	   [SURRVAL02] [numeric](17, 2) NULL,
	   [SURRVAL03] [numeric](17, 2) NULL,
	   [CLAMSTAT] [nchar](2) NULL,
	   [PLAMT01] [numeric](17, 2) NULL,
	   [PLAMT02] [numeric](17, 2) NULL,
	   [CLAMAMT01] [numeric](17, 2) NULL,
	   [CLAMAMT02] [numeric](17, 2) NULL,
	   [OFCHARGE] [numeric](13, 2) NULL,
	   [CHEQNO] [nchar](9) NULL,
	   [PAYDATE] [int] NULL,
	   [PAYCOY] [nchar] (1) NULL,
	   [VALIDFLAG] [nchar](1) NULL,
	   [CLAIM] [nchar](9) NULL,
	   [USRPRF] [nchar](10) NULL,
	   [JOBNM] [nchar] (10) NULL,
	   [DATIME] [datetime2](6) NULL,
	   [SKPMSUSP] [numeric](17, 2) NULL,
	   [WUEPAMT] [numeric](17, 2) NULL,
	   [PREMPAID] [numeric](17, 2) NULL,
	   [TOTAL] [numeric](17, 2) NULL,
	   [REFUNDOPTION] [nchar](4) NULL,
	   [CREATED_AT] [datetime2](6) DEFAULT getdate(),
	   CONSTRAINT [PK_KJOHPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Company>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
			
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Contract number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Plan Suffix>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PLNSFX';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Life insured number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'LIFE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Joint life Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'JLIFE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Transaction code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'TRANNO';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Terminal ID>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'TERMID';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<User ID>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'USER_T';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Effective date>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'EFFDATE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Currency Code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CURRCD';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Claim Type>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CLAMTYP';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<POLICYLOAN>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'POLICYLOAN';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<OTHERADJST>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'OTHERADJST';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Reason Code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'REASONCD';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Reason Code Japnese>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'RESNDESC';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Contract type>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CNTTYPE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Transaction time>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'TRTM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PL interest 01>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PLINT01';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PL interest 02>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PLINT02';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<APL principal 01>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'APLAMT01';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<APL principal 02>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'APLAMT02';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<APL interest 01>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'APLINT01';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<APL interest 02>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'APLINT02';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Premium suspense receipt>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PREMSUSP';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Advanced suspense receipt>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PREMADVBAL';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Advanced interest>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PREMADVINT';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Surrender fee>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'SURRTAX';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Late payment interest>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'INTEREST';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Late payment days>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'INTDAYS';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PAYCLT>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PAYCLT';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Bank code/Branch code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'BANKKEY';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Bank account number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'BANKACCKEY';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Bank account type>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'BANKACTYPE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Payment method>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PAYMMETH';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Cash value>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'SURRVAL01';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Policy Reserve>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'SURRVAL02';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Fund value>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'SURRVAL03';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Claim status>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CLAMSTAT';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PL principal 01>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PLAMT01';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PL principal 02>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PLAMT02';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Refund amount 01>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CLAMAMT01';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Refund amount 02>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CLAMAMT02';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Fee>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'OFCHARGE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Cheque Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CHEQNO';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Payment Date>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PAYDATE'; 

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Payment Company>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PAYCOY';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Validflag>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Claim Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'CLAIM'; 

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<User profile>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'USRPRF';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Job name>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'JOBNM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Processing date/time>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'DATIME';  

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Premium Suspense SK>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'SKPMSUSP';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Unexpired premium>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'WUEPAMT';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Premium Already Paid>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'PREMPAID';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Total>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'TOTAL';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<RefundOption>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJOHPF', @level2type=N'COLUMN',@level2name=N'REFUNDOPTION';
END
GO
