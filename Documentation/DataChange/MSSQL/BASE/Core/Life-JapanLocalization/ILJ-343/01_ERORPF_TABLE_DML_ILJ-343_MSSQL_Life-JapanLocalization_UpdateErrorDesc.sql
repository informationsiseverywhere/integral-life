SET QUOTED_IDENTIFIER ON 
GO

IF NOT EXISTS (SELECT *
FROM VM1DTA.ERORPF
WHERE ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'RFL8')
INSERT INTO VM1DTA.ERORPF(ERORPFX,ERORCOY,ERORLANG,ERORPROG,EROREROR,ERORDESC,TRDT,TRTM,USERID,TERMINALID,USRPRF,JOBNM,DATIME,ERORFILE)
VALUES ('ER',' ','J','','RFL8', N'TR52Z 受取人タイプなし' ,'' ,'','','' ,'UNDERWR1' ,'UNDERWR1' ,CURRENT_TIMESTAMP,' ');

else
UPDATE VM1DTA.ERORPF SET ERORDESC=N'TR52Z 受取人タイプなし'
WHERE  ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'RFL8';

GO

SET QUOTED_IDENTIFIER OFF 
GO