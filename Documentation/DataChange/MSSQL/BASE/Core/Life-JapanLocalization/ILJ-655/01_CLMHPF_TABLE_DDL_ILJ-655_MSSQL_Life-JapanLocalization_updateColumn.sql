 
GO
IF EXISTS (SELECT * FROM sys.columns WHERE name in('ZCLAIMER') and object_id in (SELECT object_id FROM sys.tables WHERE name ='CLMHPF'))
BEGIN
	ALTER TABLE VM1DTA.CLMHPF ALTER COLUMN ZCLAIMER nchar(8);
END
GO
