
IF NOT EXISTS ( SELECT * FROM SYS.TABLES WHERE NAME = 'AGSDPF' AND TYPE = 'U' AND SCHEMA_ID = (SELECT SCHEMA_ID FROM SYS.SCHEMAS WHERE NAME='VM1DTA'))
BEGIN
CREATE TABLE VM1DTA.AGSDPF(

UNIQUE_NUMBER bigint IDENTITY(1,1) NOT NULL,
LEVELNO nchar(8) NULL,
LEVELCLASS nchar(1) NULL,
AGENTCLASS nchar(2) NULL,
SALESDIV nchar(8) NULL,
CLIENTNO nchar(8) NULL,
USERID  nchar(10) NULL,
VALIDFLAG  nchar(1) NULL,
ACTIVESTATUS  nchar(1) NULL,
REGISCLASS  nchar(1) NULL,
EFFECTIVEDT  int NULL,
REASONCD  nchar(8) NULL,
REASONDTL  nchar(120) NULL,
GENERATIONNO  nchar(2) NULL


	CONSTRAINT [PK_AGSDPF] PRIMARY KEY CLUSTERED 
 (
       [UNIQUE_NUMBER] ASC
 )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Agent  Sales Department file' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Level Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'LEVELNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Level Classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'LEVELCLASS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AGENT Classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'AGENTCLASS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sales Division' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'SALESDIV';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'CLIENTNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Id' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'USERID';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valid Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Active Status' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'ACTIVESTATUS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registration Class' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'REGISCLASS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'EFFECTIVEDT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'REASONCD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason Details' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'REASONDTL';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Generation Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'AGSDPF', @level2type=N'COLUMN',@level2name=N'GENERATIONNO';



END
GO