SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'TNTAPDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='HPADPF'))       
    BEGIN
		ALTER TABLE VM1DTA.HPADPF ADD TNTAPDATE int NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<TNTAPDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'HPADPF', @level2type=N'COLUMN',@level2name=N'TNTAPDATE ';
	END
GO
SET QUOTED_IDENTIFIER OFF
GO

