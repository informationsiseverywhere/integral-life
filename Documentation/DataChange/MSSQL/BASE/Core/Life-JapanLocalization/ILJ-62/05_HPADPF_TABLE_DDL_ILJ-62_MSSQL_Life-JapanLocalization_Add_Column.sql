
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CNFIRMTNINTENTDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='HPADPF'))       
    BEGIN
		ALTER TABLE VM1DTA.HPADPF ADD CNFIRMTNINTENTDATE INT NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<CNFIRMTNINTENTDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'HPADPF', @level2type=N'COLUMN',@level2name=N'CNFIRMTNINTENTDATE ';
	END
GO
