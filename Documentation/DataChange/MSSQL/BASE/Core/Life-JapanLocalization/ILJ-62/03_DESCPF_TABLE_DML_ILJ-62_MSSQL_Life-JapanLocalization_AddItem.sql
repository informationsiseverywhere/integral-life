
GO
IF NOT EXISTS (SELECT * FROM vm1dta.descpf d WHERE d.DESCTABL='TR2A5' and d.DESCITEM='NBPRP115' and d.DESCCOY='2' and Language ='E' )
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
values ('IT','2','TR2A5','NBPRP115','  ','E',' ','NBPRP115  ','2JP NB Proposal Validation','UNDERWR1  ','UNDERWR1',current_timestamp);
GO 
