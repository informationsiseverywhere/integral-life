SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='T3588' and DESCITEM='TR' and LANGUAGE='E')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T3588','TR','  ','E','','TR','Terminated','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (Select * from VM1DTA.DESCPF where DESCPFX='IT' and DESCCOY='2' and DESCTABL='T3588' and DESCITEM='TR' and LANGUAGE='J')
Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
values ('IT','2','T3588','TR','  ','J','','TR',N'消滅','UNDERWR1','UNDERWR1',current_timestamp);
GO
SET QUOTED_IDENTIFIER OFF
GO