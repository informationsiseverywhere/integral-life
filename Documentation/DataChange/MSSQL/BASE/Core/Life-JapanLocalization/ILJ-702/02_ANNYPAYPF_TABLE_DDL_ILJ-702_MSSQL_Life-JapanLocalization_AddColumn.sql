
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'EFFECTIVEDT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ANNYPAYPF'))
       BEGIN
              ALTER TABLE [VM1DTA].ANNYPAYPF ADD EFFECTIVEDT INT NULL;
              EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Effective date>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'EFFECTIVEDT';
       END
GO 

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'VALIDFLAG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ANNYPAYPF'))
       BEGIN
              ALTER TABLE [VM1DTA].ANNYPAYPF ADD VALIDFLAG nchar(1) NULL;
              EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Valid Flag>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
       END
GO 



IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'TRANNO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ANNYPAYPF'))
       BEGIN
              ALTER TABLE [VM1DTA].ANNYPAYPF ADD TRANNO INT NULL;
              EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Transaction Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
       END
GO 

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'BATCTRCDE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='ANNYPAYPF'))
       BEGIN
              ALTER TABLE [VM1DTA].ANNYPAYPF ADD BATCTRCDE char(4) NULL;
              EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Batch code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'ANNYPAYPF', @level2type=N'COLUMN',@level2name=N'BATCTRCDE';
       END
GO 
