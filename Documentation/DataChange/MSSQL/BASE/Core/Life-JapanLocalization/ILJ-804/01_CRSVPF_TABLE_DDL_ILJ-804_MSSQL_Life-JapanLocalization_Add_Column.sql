SET QUOTED_IDENTIFIER ON  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PAYDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='CRSVPF'))       
    BEGIN
		ALTER TABLE VM1DTA.CRSVPF ADD PAYDATE int NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PAYDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CRSVPF', @level2type=N'COLUMN',@level2name=N'PAYDATE ';
	END
GO
SET QUOTED_IDENTIFIER OFF
GO