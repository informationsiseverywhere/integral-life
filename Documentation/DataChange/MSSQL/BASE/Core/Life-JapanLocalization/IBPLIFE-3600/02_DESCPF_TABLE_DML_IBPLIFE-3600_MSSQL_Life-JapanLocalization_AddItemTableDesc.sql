SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCPFX='HE' AND DESCITEM='' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TH5C0','','  ','E',' ','','Cancellation Reason Codes','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCPFX='HE' AND DESCITEM='' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TH5C1','','  ','E',' ','','Refund Option','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M201' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M201','  ','E',' ','M201','Insured criminal activity','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M202' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M202','  ','E',' ','M202','Intentional act of Owner','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M203' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M203','  ','E',' ','M203','Serious negligence of Owner','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M204' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M204','  ','E',' ','M204','Intentional act of Insured','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO



SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M205' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M205','  ','E',' ','M205','Serious negligence of Insured','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M206' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M206','  ','E',' ','M206','Intentional act of Beneficiary','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M207' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M207','  ','E',' ','M207','Serious negligence of Benef','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M208' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M208','  ','E',' ','M208','Insured''s mental illness','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M209' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M209','  ','E',' ','M209','Accident by Insured drunkennes','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M210' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M210','  ','E',' ','M210','Accident by unlicense driving ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M211' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M211','  ','E',' ','M211','Accident Insured drunken zone','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M212' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M212','  ','E',' ','M212','Earthquake,Eruption or Tsunami','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M213' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M213','  ','E',' ','M213','War and other upheavals','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M214' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M214','  ','E',' ','M214','Suicide in exemption period','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M215' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M215','  ','E',' ','M215','Insured drug dependence','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M216' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M216','  ','E',' ','M216','Suicide of the Insured','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO



SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M231' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M231','  ','E',' ','M231','Cancel of notification breach','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M232' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M232','  ','E',' ','M232','Cancel due to serious reasons','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M233' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M233','  ','E',' ','M233','Contract Lapse','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M234' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M234','  ','E',' ','M234','Invalid','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M235' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M235','  ','E',' ','M235','Fraud Rescission','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M236' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M236','  ','E',' ','M236','Prescription','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M237' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M237','  ','E',' ','M237','Invalid before Cancer Liablity','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M251' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M251','  ','E',' ','M251','Payment not applicable','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M252' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M252','  ','E',' ','M252','Unsecured Part','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M253' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M253','  ','E',' ','M253','Contract Extinction','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M254' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M254','  ','E',' ','M254','Contract Expiration','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M255' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M255','  ','E',' ','M255','Overdue Payment','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M299' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M299','  ','E',' ','M299','Other','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='01' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','01','  ','E',' ','Cash Value','Cash Value','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='02' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','02','  ','E',' ','PolReserve','Policy Reserve','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='03' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','03','  ','E',' ','Prem Paid','Premium Already Paid','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='04' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','04','  ','E',' ','No Refund','No Refund','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='05' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','05','  ','E',' ','NoRefndall','No refund at all','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='06' AND LANGUAGE = 'E')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','06','  ','E',' ','Fund Value','Fund Value','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCPFX='HE' AND DESCITEM='' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TH5C0','','  ','J',' ','','解除事由コード','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCPFX='HE' AND DESCITEM='' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('HE','2','TH5C1','','  ','J',' ','','返金選択','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M201' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M201','  ','J',' ','M201', N'被保険者の犯罪行為','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M202' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M202','  ','J',' ','M202', N'保険契約者の故意','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M203' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M203','  ','J',' ','M203', N'保険契約者の重大な過失','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M204' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M204','  ','J',' ','M204', N'被保険者の故意','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M205' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M205','  ','J',' ','M205', N'被保険者の重大な過失','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M206' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M206','  ','J',' ','M206', N'死亡保険金受取人の故意','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M207' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M207','  ','J',' ','M207', N'死亡保険金受取人の重大な過失','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M208' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M208','  ','J',' ','M208', N'被保険者の精神障害が原因','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M209' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M209','  ','J',' ','M209', N'被保険者の泥酔状態が原因の事故','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M210' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M210','  ','J',' ','M210', N'被保険者の無免許運転中の事故','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M211' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M211','  ','J',' ','M211', N'被保険者の酒気帯運転中の事故','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M212' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M212','  ','J',' ','M212', N'地震、噴火または津波','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M213' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M213','  ','J',' ','M213', N'戦争その他の変乱','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M214' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M214','  ','J',' ','M214', N'免責期間中の被保険者の自殺','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M215' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M215','  ','J',' ','M215', N'被保険者の薬物依存','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M216' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M216','  ','J',' ','M216', N'被保険者の自殺行為','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M231' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M231','  ','J',' ','M231', N'告知義務違反解除','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M232' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M232','  ','J',' ','M232', N'重大事由解除','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M233' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M233','  ','J',' ','M233', N'契約失効','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M234' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M234','  ','J',' ','M234', N'無効','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M235' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M235','  ','J',' ','M235', N'詐欺取消','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M236' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M236','  ','J',' ','M236', N'時効','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M237' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M237','  ','J',' ','M237', N'がん保障責任開始日前無効','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M251' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M251','  ','J',' ','M251', N'支払事由非該当','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M252' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M252','  ','J',' ','M252', N'部位不担保','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M253' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M253','  ','J',' ','M253', N'契約消滅','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M254' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M254','  ','J',' ','M254', N'契約満了','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M255' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M255','  ','J',' ','M255', N'支払期限超過','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C0' AND DESCITEM='M299' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C0','M299','  ','J',' ','M299', N'その他','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO



SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='01' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','01','  ','J',' ','Cash Value', N'解約返戻金','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO



SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='02' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','02','  ','J',' ','PolReserve', N'責任準備金','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='03' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','03','  ','J',' ','Prem Paid', N'既払込保険料','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='04' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','04','  ','J',' ','No Refund', N'返金なし','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='05' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','05','  ','J',' ','NoRefndall', N'一切返金なし','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='2' AND DESCTABL='TH5C1' AND DESCITEM='06' AND LANGUAGE = 'J')
	Insert into VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) values ('IT','2','TH5C1','06','  ','J',' ','Fund Value', N'積立金','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO