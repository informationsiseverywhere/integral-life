
	GO
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'JL25  ')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','E','','JL25  ', 'Contract should be reversed and reissue','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO
	
	IF NOT EXISTS (	SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='E' AND h.[EROREROR] = 'JL26  ')

	INSERT INTO [VM1DTA].[ERORPF]([ERORPFX],[ERORCOY],[ERORLANG],[ERORPROG],[EROREROR],[ERORDESC],[TRDT],[TRTM],[USERID],[TERMINALID],[USRPRF],[JOBNM],[DATIME],[ERORFILE])
	    VALUES ('ER','','E','','JL26  ', 'Date of Death < RCD','','','','','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,' ');
	GO