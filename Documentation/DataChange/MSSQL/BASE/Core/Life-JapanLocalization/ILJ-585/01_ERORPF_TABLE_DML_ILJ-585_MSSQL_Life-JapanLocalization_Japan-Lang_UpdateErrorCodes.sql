

GO
IF  EXISTS (SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL41')

UPDATE [VM1DTA].[ERORPF] set ERORDESC = N'反社会勢力該当あり' WHERE ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'JL41';

GO

GO
IF  EXISTS (SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL42')

UPDATE [VM1DTA].[ERORPF] set ERORDESC = N'反社会勢力（法人）該当あり' WHERE ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'JL42';

GO

GO
IF  EXISTS (SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL62')

UPDATE [VM1DTA].[ERORPF] set ERORDESC = N'反社会勢力に複数該当あり' WHERE ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'JL62';

GO

GO
IF  EXISTS (SELECT * FROM [VM1DTA].[ERORPF] h 	
	WHERE h.[ERORPFX] = 'ER' AND h.[ERORLANG] ='J' AND h.[EROREROR] = 'JL63')

UPDATE [VM1DTA].[ERORPF] set ERORDESC = N'反社会勢力（法人）に複数該当あり' WHERE ERORPFX = 'ER' AND ERORLANG ='J' AND EROREROR = 'JL63';

GO
