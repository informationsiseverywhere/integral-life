-- -- -- -- -- -- -- -- CREATE A NEW TABLE -- -- -- -- -- -- -- -- 
IF NOT EXISTS (SELECT * FROM sys.tables where name = 'SLNCPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	BEGIN

CREATE TABLE [VM1DTA].[SLNCPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[ZDATACLS] [nchar](1),
	[ZRECLASS] [nchar](2) ,
	[ZMODTYPE] [nchar](1) ,
	[ZRSLTCDE] [nchar](1) ,
	[ZRSLTRSN] [nchar](2) ,
	[ZERRORNO] [nchar](3),
	[ZNOFRECS] [nchar](3) ,
	[KLINCKEY] [nchar](12) ,
	[HCLTNAM] [nchar](32) ,
	[CLTSEX] [nchar](1),
	[ZCLNTDOB] [int] ,
	[ZEFFDATE] [int] ,
	[FLAG] [nchar](1) ,
	[KGLCMPCD] [nchar](2) ,
	[ADDRSS] [nchar](30) ,
	[ZHOSPBNF] [numeric](5) ,
	[ZSICKBNF] [numeric](5) ,
	[ZCANCBNF] [numeric](5) ,
	[ZOTHSBNF] [numeric](5) ,
	[ZREGDATE] [int] ,
	[ZLINCDTE] [int] ,
	[KJHCLTNAM] [nchar](32) ,
	[ZHOSPOPT] [nchar](40) ,
	[ZREVDATE] [int] ,
	[ZLOWNNAM] [nchar](42) ,
	[ZCSUMINS] [numeric](10) ,
	[ZASUMINS] [numeric](10) ,
	[ZDETHOPT] [nchar](40) ,
	[ZRQFLAG] [nchar](1) ,
	[ZRQSETDTE] [int] ,
	[ZRQCANDTE] [int] ,
	[ZEPFLAG] [nchar](1),
	[ZCVFLAG] [nchar](1),
	[ZCONTSEX] [nchar](1),
	[ZCONTDOB] [int] ,
	[ZCONTADDR] [nchar](30) ,
	[LIFNAMKJ] [nchar](60) ,
	[OWNNAMKJ] [nchar] (60) ,
	[WFLGA] [nchar](1),
	[ZERRFLG] [nchar](1),
	[ZOCCDATE] [int] ,
	[ZTRDATE] [int] ,
	[ZADRDDTE] [int],
	[ZEXDRDDTE] [int],
	[USRPRF] [nchar](10),
	[JOBNM] [nchar](10),
	[DATIME] [datetime2],

 CONSTRAINT [PK_SLNCPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SLNC Data' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZDATACLS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZRECLASS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Change type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZMODTYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Result code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZRSLTCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Result reason' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZRSLTRSN';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Error no.' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZERRORNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name matching record no.' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZNOFRECS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LINC key number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'KLINCKEY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name matching kana name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'HCLTNAM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured gender' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'CLTSEX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured DOB' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCLNTDOB';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZEFFDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Policyholder/insured same person flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'FLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sending company' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'KGLCMPCD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured kana address' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ADDRSS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disaster hospitalization daily  benefit amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZHOSPBNF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Illness hospitalization daily  benefit amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZSICKBNF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lifestyle disease hospitalization daily  benefit amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCANCBNF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hospitalization daily  benefit amount for other reasons' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZOTHSBNF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Registration date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZREGDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LINC processing date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZLINCDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life insurance kana name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'KJHCLTNAM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Policy management no.' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZHOSPOPT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maintenance date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZREVDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Policyholder kana name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZLOWNNAM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ordinary death benefit amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCSUMINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disaster death increased benefit' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZASUMINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Field used by insurance company' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZDETHOPT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Request flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZRQFLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Request flag config. date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZRQSETDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Request flag cancel date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZRQCANDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business insurance flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZEPFLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Conversion flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCVFLAG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured gender' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCONTSEX';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured DOB' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCONTDOB';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured kana address' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZCONTADDR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insured  kanji name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'LIFNAMKJ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Policyholder kanji name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'OWNNAMKJ';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Under 15 years old/over 10 mil. flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'WFLGA';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Out of range kanji flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZERRFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expected date of contract issue' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZOCCDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expected date of contract maintenance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZTRDATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of adding riders at the age <15' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZADRDDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expected date of adding riders at the age <15' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'ZEXDRDDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USER PROFILE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'JOB NAME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TIMESTAMP' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SLNCPF', @level2type=N'COLUMN',@level2name=N'DATIME';

	END
GO

