IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'KJODPF' and type = 'U' 
				and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
create TABLE [VM1DTA].[KJODPF](
       [UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
       [CHDRCOY] [nchar] (1) NULL,
       [CHDRNUM] [nchar](8) NULL,
       [LIFE] [nchar](2) NULL,
       [JLIFE] [nchar](2) NULL,
       [COVERAGE] [nchar](2) NULL,
       [RIDER] [nchar](2) NULL,
	   [PLNSFX] [int] NULL,
	   [TRANNO] [int] NULL,
	   [EFFDATE] [int] NULL,
	   [CURRCD] [nchar](3) NULL,
	   [CRTABLE] [nchar](4) NULL, 
	   [SHORTDS] [nchar](10) NULL,
	   [LIENCD] [nchar](2) NULL,
	   [RIIND] [nchar](1) NULL,
	   [UNITYP] [nchar](1) NULL,   
	   [ACTVALUE] [numeric](17, 2) NULL,
	   [EMV] [numeric](17,2) NULL,
	   [VRTFUND] [nchar](4) NULL,
	   [CLAIM] [nchar](9) NULL,
	   [TERMID] [nchar](4) NULL,
	   [TRDT] [int] NULL,
	   [TRTM] [int] NULL,
	   [USER_T] [int] NULL,
	   [USRPRF] [nchar](10) NULL,
	   [JOBNM] [nchar](10) NULL,
	   [DATIME] [datetime2](6) NULL,
	   [CREATED_AT] [datetime2](6) DEFAULT getdate(),
	   
	   CONSTRAINT [PK_KJODPF] PRIMARY KEY CLUSTERED 
(
       [UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Company>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Contract number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Life insured number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'LIFE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Joint Life Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'JLIFE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Coverage Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Contract Rider number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'RIDER';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Plan suffix>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'PLNSFX';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Transaction number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'TRANNO';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Effective date>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'EFFDATE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Currency code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'CURRCD';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Coverage/Rider code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'CRTABLE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Processing content>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'SHORTDS';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Item number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'LIENCD';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Tolerance code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'RIIND';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Unit type>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'UNITYP';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<ACTVALUE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'ACTVALUE';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<EMV>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'EMV';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Unit link fund code>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'VRTFUND';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Claim Number>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'CLAIM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Terminal ID>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'TERMID';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Processing date>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'TRDT';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Processing time>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'TRTM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<User ID>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'USER_T';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<User profile>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'USRPRF';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Job Name>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'JOBNM';

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<Update date and time>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'KJODPF', @level2type=N'COLUMN',@level2name=N'DATIME';
END
GO