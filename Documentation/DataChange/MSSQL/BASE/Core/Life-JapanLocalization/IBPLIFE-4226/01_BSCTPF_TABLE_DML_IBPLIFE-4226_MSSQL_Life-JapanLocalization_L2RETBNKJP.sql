SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM [VM1DTA].BSCTPF WHERE [bschednam]='L2RETBNKJP' AND [LANGUAGE]='E' )
	Insert into VM1DTA.BSCT (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values ('E','L2RETBNKJP','Return Bank Tape Updt -App & Rej Debit All Fact Hs','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO 


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM [VM1DTA].BSCTPF WHERE [bschednam]='L2RETBNKJP' AND [LANGUAGE]='J' )
	Insert into VM1DTA.BSCT (LANGUAGE,BSCHEDNAM,DESC_T,JOBNM,USRPRF,DATIME) values ('J','L2RETBNKJP',N'口振テープ結果更新（全収納代行用）','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO