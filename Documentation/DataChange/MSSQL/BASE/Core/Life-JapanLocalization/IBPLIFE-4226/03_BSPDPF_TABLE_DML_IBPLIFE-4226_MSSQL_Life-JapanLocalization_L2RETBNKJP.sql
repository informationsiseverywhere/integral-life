SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM [VM1DTA].BSPDPF WHERE [BSCHEDNAM] = 'L2RETBNKJP' and [COMPANY]='2') 
	Insert into VM1DTA.BSPDPF (BSCHEDNAM,COMPANY,BPROCESNAM,JOBNM,USRPRF,DATIME) values ('L2RETBNKJP','2','F-BACRJPM','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO 