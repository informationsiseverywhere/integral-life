﻿SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261000' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261000','  ','J',' ',N'振替済',N'振替済','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
GO	
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261001' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261001','  ','J',' ',N'資金不足',N'資金不足（残高不足）','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261002' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261002','  ','J',' ',N'取引なし',N'取引なし','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261003' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261003','  ','J',' ',N'預金者',N'預金者の都合による振替停止','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261004' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261004','  ','J',' ',N'口座振',N'口座振替依頼書なし','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261008' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261008','  ','J',' ',N'委託者',N'委託者の都合による振替停止','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM VM1DTA.DESCPF WHERE DESCCOY='9' AND DESCTABL='TR22B' AND DESCPFX='IT' AND DESCITEM='J261009' AND LANGUAGE = 'J')
	INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME) 
	VALUES ('IT','9','TR22B','J261009','  ','J',' ',N'その他',N'その他','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);

GO
SET QUOTED_IDENTIFIER OFF
GO
