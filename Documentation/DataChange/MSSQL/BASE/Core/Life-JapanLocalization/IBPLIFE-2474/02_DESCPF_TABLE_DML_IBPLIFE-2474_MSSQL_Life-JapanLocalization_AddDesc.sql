SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (	SELECT * FROM VM1DTA.DESCPF WHERE DESCTABL = 'T1675' AND DESCITEM='TBGDH5A6' AND DESCCOY='2' AND LANGUAGE='E' AND DESCPFX='IT')
INSERT INTO VM1DTA.DESCPF (DESCPFX,DESCCOY,DESCTABL,DESCITEM,ITEMSEQ,LANGUAGE,TRANID,SHORTDESC,LONGDESC,USRPRF,JOBNM,DATIME)
VALUES ('IT','2','T1675','TBGDH5A6','  ','E','','Life','Tentative Approval Update','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);
GO

SET QUOTED_IDENTIFIER OFF  
GO