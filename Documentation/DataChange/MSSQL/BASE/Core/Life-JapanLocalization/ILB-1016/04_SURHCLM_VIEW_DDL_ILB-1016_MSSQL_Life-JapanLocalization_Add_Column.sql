SET XACT_ABORT ON;
GO

begin tran

IF EXISTS ( SELECT * FROM sys.views where name = 'SURHCLM' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].SURHCLM
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[VM1DTA].[SURHCLM]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW VM1DTA.SURHCLM (UNIQUE_NUMBER,
			CHDRCOY,
            CHDRNUM,
            TRANNO,
			PLNSFX,
            LIFE,
            JLIFE,
            EFFDATE,
            CURRCD,
            POLICYLOAN,
            TDBTAMT,
            TAXAMT,
            OTHERADJST,
            REASONCD,
            RESNDESC,
            CNTTYPE,
            ZRCSHAMT,
			USRPRF,
            JOBNM,
            DATIME,
			SUSPENSEAMT,
			UNEXPIREDPRM)
AS
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
			PLNSFX,
            LIFE,
            JLIFE,
            EFFDATE,
            CURRCD,
            POLICYLOAN,
            TDBTAMT,
            TAXAMT,
            OTHERADJST,
            REASONCD,
            RESNDESC,
            CNTTYPE,
            ZRCSHAMT,
			USRPRF,
            JOBNM,
            DATIME,
			SUSPENSEAMT,
			UNEXPIREDPRM
       FROM SURHPF'
	   
GO
commit tran 