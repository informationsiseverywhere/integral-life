SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'UNEXPIREDPRM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='SURHPF'))      
    BEGIN
        ALTER TABLE VM1DTA.SURHPF ADD UNEXPIREDPRM numeric(17, 2) NULL;
        EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<UNEXPIREDPRM>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SURHPF', @level2type=N'COLUMN',@level2name=N'UNEXPIREDPRM ';
    END
GO
SET QUOTED_IDENTIFIER OFF
GO