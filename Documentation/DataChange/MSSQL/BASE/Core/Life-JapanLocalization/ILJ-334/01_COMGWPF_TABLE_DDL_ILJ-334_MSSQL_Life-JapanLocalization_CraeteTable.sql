IF NOT EXISTS ( SELECT * FROM SYS.TABLES WHERE NAME = 'COMGWPF' AND TYPE = 'U' AND SCHEMA_ID = (SELECT SCHEMA_ID FROM SYS.SCHEMAS WHERE NAME='VM1DTA'))
BEGIN
CREATE TABLE [VM1DTA].[COMGWPF](
	[UNIQUE_NUMBER] [BIGINT] IDENTITY(1,1) NOT NULL,
	[DATATYPE] [NCHAR](1) NULL,
	[INSUCATE] [NCHAR](2) NULL,
	[BOKCLS] [NCHAR](1) NULL,
	[SECUNUM] [NCHAR](12) NULL,
	[BRNCHNUM] [NCHAR](5) NULL,
	[TOI] [NCHAR](2) NULL,
	[MOP] [NCHAR](1) NULL,
	[COLCATE] [NCHAR](1) NULL,
	[INSTAL] [NCHAR](2) NULL,
	[KYOCDE] [NCHAR](1) NULL,
	[KYOACS] [NCHAR](4) NULL,
	[PCDE] [NCHAR](1) NULL,
	[PACS] [NCHAR](4) NULL,
	[GRPCDE][NCHAR](7) NULL,
	[AFFILCDE][NCHAR](10) NULL,
	[EMPCDE][NCHAR](15) NULL,
	[CGRPC][NCHAR](1) NULL,
	[TOTPREM][NCHAR](10) NULL,
	[BCKP] [NCHAR](3) NULL,
	[DPCDE] [NCHAR](7) NULL,
	[BRNCHNUM2] [NCHAR](10) NULL,
	[INCPN] [NCHAR](6) NULL,
	[MATRTY] [NCHAR](6) NULL,
	[TNCDTE] [NCHAR](6) NULL,
	[TRNENDTE] [NCHAR](6) NULL,
	[DTEOFAPP] [NCHAR](6) NULL,
	[SLFCRT] [NCHAR](1) NULL,
	[LPCC] [NCHAR](1) NULL,
	[DCDE] [NCHAR](5) NULL,
	[OPRTCDE] [NCHAR](7) NULL,
	[POSTCDE] [NCHAR](5) NULL,
	[CNTADD] [NCHAR](70) NULL,
	[PNUM] [NCHAR](12) NULL,
	[SUBNME] [NCHAR](70) NULL,
	[SPFIP] [NCHAR](10) NULL,
	[SPLTYP] [NCHAR](2) NULL,
	[PYMNTDTE] [NCHAR](2) NULL,
	[FPDFGH] [NCHAR](4) NULL,
	[INSYR] [NCHAR](2) NULL,
	[FRSTME] [NCHAR](2) NULL,
	[PPREM] [NCHAR](10) NULL,
	[PPENDATME] [NCHAR](4) NULL,
	[NWCCLS] [NCHAR](1) NULL,
	[CCPS] [NCHAR](1) NULL,
	[IPACTC] [NCHAR](1) NULL,
	[AMOR] [NCHAR](1) NULL,
	[GRPCARA] [NCHAR](1) NULL,
	[TELPHNO] [NCHAR](3) NULL,
	[NWCMPNCDE] [NCHAR](2) NULL,
	[RFTRNS] [NCHAR](2) NULL,
	[MARGIN] [NCHAR](18) NULL,
	[INACVIWE] [NCHAR](1) NULL,
	[PROSQNC] [NCHAR](10) NULL,
	[YNMRCD] [NCHAR](4) NULL,
	[INSCMPNYCDE] [NCHAR](2) NULL,
	[INSPROCDE] [NCHAR](14) NULL,
	[CMPNYCDE] [NCHAR](2) NULL,
	[PRSNMEKANA] [NCHAR](20) NULL,
	[IPECFLG] [NCHAR](1) NULL,
	[IPSFTCDE01] [NCHAR](1) NULL,
	[IPNMKNJI] [NCHAR](30) NULL,
	[IPSFTCDE02] [NCHAR](1) NULL,
	[IPDOB] [NCHAR](8) NULL,
	[IPAGE] [NCHAR](3) NULL,
	[IPGNDR] [NCHAR](1) NULL,
	[IPRLN] [NCHAR](2) NULL,
	[IPOSTCDE] [NCHAR](8) NULL,
	[IAEFLG] [NCHAR](1) NULL,
	[IASHTCDE01] [NCHAR](1) NULL,
	[IADRSKNJI] [NCHAR](100) NULL,
	[IASHTCDE02] [NCHAR](1) NULL,
	[PYMNTRT] [NCHAR](1) NULL,
	[LSPREM] [NCHAR](10) NULL,
	[MODUNDRPRD] [NCHAR](2) NULL,
	[INDCATE] [NCHAR](1) NULL,
	[INDCPRD] [NCHAR](2) NULL,
	[DVNDCLS] [NCHAR](1) NULL,
	[PDEXNTDVW] [NCHAR](1) NULL,
	[ATTCHNGCLS] [NCHAR](1) NULL,
	[APPCATE] [NCHAR](1) NULL,
	[IPPYEAR] [NCHAR](3) NULL,
	[PPSDTE] [NCHAR](8) NULL,
	[PASBDY] [NCHAR](1) NULL,
	[ADDSCNT] [NCHAR](1) NULL,
	[NORIDS] [NCHAR](3) NULL,
	[GRPCDE02] [NCHAR](8) NULL,
	[LOCDE] [NCHAR](3) NULL,
	[EXMCATE] [NCHAR](1) NULL,
	[TAXELG] [NCHAR](1) NULL,
	[LVNNDS] [NCHAR](1) NULL,
	[BNSPREM] [NCHAR](10) NULL,
	[BCAGYR] [NCHAR](2) NULL,
	[OPCDE] [NCHAR](10) NULL,
	[MCCTYPCDE] [NCHAR](7) NULL,
	[MCCECFLG] [NCHAR](1) NULL,
	[MCCSCDE01] [NCHAR](1) NULL,
	[MCCNMC] [NCHAR](16) NULL,
	[MCCSCDE02] [NCHAR](1) NULL,
	[MCCPPA] [NCHAR](11) NULL,
	[MCCITC] [NCHAR](1) NULL,
	[MCCIP] [NCHAR](2) NULL,
	[MCCPPC] [NCHAR](1) NULL,
	[MCCPP] [NCHAR](2) NULL,
	[SA01TYPCDE] [NCHAR](7) NULL,
	[SA01ECFLG] [NCHAR](1) NULL,
	[SA01SCDE01] [NCHAR](1) NULL,
	[SA01NME] [NCHAR](16) NULL,
	[SA01SCDE02] [NCHAR](1) NULL,
	[SA01SINS] [NCHAR](11) NULL,
	[SA01ITC] [NCHAR](1) NULL,
	[SA01INSPRD] [NCHAR](2) NULL,
	[SA01PPC] [NCHAR](1) NULL,
	[SA01PP] [NCHAR](2) NULL,
	[SA02TYPCDE] [NCHAR](7) NULL,
	[SA02ECFLG] [NCHAR](1) NULL,
	[SA02SCDE01] [NCHAR](1) NULL,
	[SA02SCDE02] [NCHAR](1) NULL,
	[SA02NME] [NCHAR](16) NULL,
	[SA02SINS] [NCHAR](11) NULL,
	[SA02ITC] [NCHAR](1) NULL,
	[SA02INSPRD] [NCHAR](2) NULL,
	[SA02PPC] [NCHAR](1) NULL,
	[SA02PP] [NCHAR](2) NULL,
	[SA03TYPCDE] [NCHAR](7) NULL,
	[SA03ECFLG] [NCHAR](1) NULL,
	[SA03SCDE01] [NCHAR](1) NULL,
	[SA03SCDE02] [NCHAR](1) NULL,
	[SA03NME] [NCHAR](16) NULL,
	[SA03SINS] [NCHAR](11) NULL,
	[SA03ITC] [NCHAR](1) NULL,
	[SA03INSPRD] [NCHAR](2) NULL,
	[SA03PPC] [NCHAR](1) NULL,
	[SA03PP] [NCHAR](2) NULL,
	[SA04TYPCDE] [NCHAR](7) NULL,
	[SA04ECFLG] [NCHAR](1) NULL,
	[SA04SCDE01] [NCHAR](1) NULL,
	[SA04SCDE02] [NCHAR](1) NULL,
	[SA04NME] [NCHAR](16) NULL,
	[SA04SINS] [NCHAR](11) NULL,
	[SA04ITC] [NCHAR](1) NULL,
	[SA04INSPRD] [NCHAR](2) NULL,
	[SA04PPC] [NCHAR](1) NULL,
	[SA04PP] [NCHAR](2) NULL,
	[SA05TYPCDE] [NCHAR](7) NULL,
	[SAECFLG5] [NCHAR](1) NULL,
	[SA05SCDE01] [NCHAR](1) NULL,
	[SA05SCDE02] [NCHAR](1) NULL,
	[SA05NME] [NCHAR](16) NULL,
	[SA05SINS] [NCHAR](11) NULL,
	[SA05ITC] [NCHAR](1) NULL,
	[SA05INSPRD] [NCHAR](2) NULL,
	[SA05PPC] [NCHAR](1) NULL,
	[SA05PP] [NCHAR](2) NULL,
	[SA06TYPCDE] [NCHAR](7) NULL,
	[SA06ECFLG] [NCHAR](1) NULL,
	[SA06SCDE01] [NCHAR](1) NULL,
	[SA06SCDE02] [NCHAR](1) NULL,
	[SA06NME] [NCHAR](16) NULL,
	[SA06SINS] [NCHAR](11) NULL,
	[SA06ITC] [NCHAR](1) NULL,
	[SA06INSPRD] [NCHAR](2) NULL,
	[SA06PPC] [NCHAR](1) NULL,
	[SA06PP] [NCHAR](2) NULL,
	[USTATCATE] [NCHAR](2) NULL,
	[CMEC] [NCHAR](1) NULL,
	[CIFCCC] [NCHAR](1) NULL,
	[ASMCDE] [NCHAR](12) NULL,
	[LIRCDE] [NCHAR](13) NULL,
	[ACCTYP] [NCHAR](1) NULL,
	[ACCNO] [NCHAR](7) NULL,
	[LCODE] [NCHAR](5) NULL,
	[DTHBNFT] [NCHAR](6) NULL,
	[RACATE] [NCHAR](1) NULL,
	[DTYPCDE] [NCHAR](2) NULL,
	[CAIC] [NCHAR](1) NULL,
	[CNTICNRCT] [NCHAR](1) NULL,
	[TNCP] [NCHAR](10) NULL,
	[WRKPLC] [NCHAR](25) NULL,
	[BPHNO] [NCHAR](12) NULL,
	[CMGNTCDE] [NCHAR](12) NULL,
	[LNCDE] [NCHAR](4) NULL,
	[OSNOCC] [NCHAR](2) NULL,
	[OSNMN] [NCHAR](12) NULL,
	[OSNBN] [NCHAR](5) NULL,
	[GRPCRTLNO] [NCHAR](25) NULL,
	[SCNO] [NCHAR](15) NULL,
	[GRPINSNME] [NCHAR](20) NULL,
	[CRSNME] [NCHAR](12) NULL,
	[TYPE] [NCHAR](6) NULL,
	[NOUNITS] [NCHAR](2) NULL,
	[AGREGNO] [NCHAR](11) NULL,
	[AGCDE01] [NCHAR](10) NULL,
	[AGCDE02] [NCHAR](10) NULL,
	[CANKNJIECFLG] [NCHAR](1) NULL,
	[CAFLGSCDE01] [NCHAR](1) NULL,
	[CAFLGADRS] [NCHAR](100) NULL,
	[CAFLGSCDE02] [NCHAR](1) NULL,
	[CNKJICFLGSCD] [NCHAR](1) NULL,
	[CNFLGSCDE] [NCHAR](1) NULL,
	[SPNMKNJI] [NCHAR](60) NULL,
	[SPNMSCDE] [NCHAR](1) NULL,
	[DTECHGPREM] [NCHAR](6) NULL,
	[MODINSTPREM] [NCHAR](10) NULL,
	[PREMLNCRT] [NCHAR](1) NULL,
	[DOBPH] [NCHAR](8) NULL,
	[CGENDER] [NCHAR](1) NULL,
	[BPHNO02] [NCHAR](3) NULL,
	[BCKUP01] [NCHAR](8) NULL,
	[CMPNYTER] [NCHAR](120) NULL,
	[PSTRTFLG] [NCHAR](1) NULL,
	[ANTYPYMNT] [NCHAR](10) NULL,
	[ANPENPYMNT] [NCHAR](2) NULL,
	[PENSNFUND] [NCHAR](10) NULL,
	[PENSNCRTNO] [NCHAR](12) NULL,
	[PAYECATE] [NCHAR](1) NULL,
	[APPNO] [NCHAR](20) NULL,
	[BCKUP02] [NCHAR](24) NULL,
	[CORPTERTY] [NCHAR](100) NULL,
	[AGNCNUM] [NCHAR](8) NULL,
	[EXPRTFLG] [NCHAR](1) NULL,
	[USRPRF] [NCHAR](10) NULL,
	[DATIME] [DATETIME2](6) NULL

 CONSTRAINT [PK_COMGWPF] PRIMARY KEY
 ([UNIQUE_NUMBER]))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMMON GATEWAY TABLE' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Insurance Category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INSUCATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Book Classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BOKCLS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Security Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SECUNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Branch number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BRNCHNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TOI';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Method of Payment' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MOP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'collection category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'COLCATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'installments' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INSTAL';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Kyoho Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'KYOCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Kyoho admin. company share' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'KYOACS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'percentage Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'percentage admin. company share' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PACS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'group code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'GRPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'affiliation code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'AFFILCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'employee code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'EMPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category Group Category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CGRPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total premium (2)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TOTPREM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'backup' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BCKP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'7-digit postal code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'branch number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BRNCHNUM2';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'inception' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INCPN';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'maturity' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MATRTY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'transfer and cancellation date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TNCDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transfer End Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TRNENDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'date of application' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DTEOFAPP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'self-contract' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SLFCRT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line Parent Child Class' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LPCC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Department Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'operator code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'OPRTCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Postal Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'POSTCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contractor Address' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CNTADD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Subscriber Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SUBNME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'single premium for installment plan' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SPFIP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Split type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SPLTYP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'payment date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PYMNTDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'First Payment Date for Group Handling' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'FPDFGH';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insurance year' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INSYR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'first time' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'FRSTME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Partial Prepayment (premium) premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PPREM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Partial Prepayment (premium) End date and time' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PPENDATME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New/Continuation Class' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'NWCCLS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Credit card payment section' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CCPS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initial premium account transfer category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPACTC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'agency mail order rider (sales form)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'AMOR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'group control area' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'GRPCARA';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telephone number (2)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TELPHNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'new company code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'NWCMPNCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'reason for transfer' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'RFTRNS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'margin' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MARGIN';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Inactive Contracts View' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INACVIWE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Process Sequence' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PROSQNC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'year and month of recording' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'YNMRCD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insurance company code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INSCMPNYCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insurance proprietary code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INSPROCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CMPNYCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'name of insured person in kana' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PRSNMEKANA';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person external character flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPSFTCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person Name Kanji' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPNMKNJI';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPSFTCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person Date of Birth' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPDOB';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person Age' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPAGE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person Gender' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPGNDR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person relationship' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPRLN';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured person Postal Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPOSTCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured address external flag ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IAEFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured address shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IASHTCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured address address kanji' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IADRSKNJI';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insured address shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IASHTCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'payment route' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PYMNTRT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'lump sum premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LSPREM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'modified underwriting period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MODUNDRPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'increasing/decreasing category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INDCATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'increasing and decreasing period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'INDCPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dividend classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DVNDCLS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Paid Extended View' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PDEXNTDVW';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Attribute Change Class' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ATTCHNGCLS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'annuity payment period Category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'APPCATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'annuity payment period Year' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'IPPYEAR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pension payment start date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PPSDTE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'presence or absence of special body' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PASBDY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'addition of special contract' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ADDSCNT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'number of riders' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'NORIDS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'group code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'GRPCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Location Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LOCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'examination category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'EXMCATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tax Eligibility' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TAXELG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Living Needs' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LVNNDS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'bonus premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BNSPREM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'basic contract annuity guaranteed year' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BCAGYR';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'operator code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'OPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCTYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents external character flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCSCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents Name of the main contract' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCNMC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCSCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents principal policy amount' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCPPA';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCIP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCPPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Main contract contents payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MCCPP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) type code ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01TYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) external character flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01ECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01SCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) Special Agreement (1) Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01NME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01SCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) amount of special insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01SINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01ITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01INSPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01PPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (1) payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA01PP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02TYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) external character flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02ECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02SCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02SCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) Special Agreement (2) Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02NME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) amount of special insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02SINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02ITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02INSPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02PPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (2) payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA02PP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03TYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) external char. flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03ECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03SCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03SCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) Special Agreement (3) Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03NME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) amount of special insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03SINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03ITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03INSPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03PPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (3) payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA03PP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04TYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) external char. flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04ECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04SCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04SCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) Special Agreement (4) Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04NME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) amount of special insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04SINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04ITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04INSPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04PPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (4) payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA04PP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05TYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05SCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05SCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) Special Agreement (5) Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05NME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) amount of special insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05SINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05ITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05INSPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05PPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (5) payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA05PP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06TYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) external char. flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06ECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06SCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06SCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) Special Agreement (6) Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06NME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) amount of special insurance' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06SINS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) insurance term classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06ITC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) insurance period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06INSPRD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) payment period category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06PPC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special Agreement (6) payment period' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SA06PP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Update Status Category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'USTATCATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Management Entity Classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CMEC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CIF Code Control Class' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CIFCCC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'agent store manager code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ASMCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life Insurance Recruiting Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LIRCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'account type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ACCTYP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'account number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ACCNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Location Code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LCODE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'death benefit' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DTHBNFT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'reference authority category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'RACATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'data type code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DTYPCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corporate and Individual Classification' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CAIC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contingent contract' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CNTICNRCT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'transfer and cancellation premiums' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TNCP';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'work place' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'WRKPLC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Phone Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BPHNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor management code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CMGNTCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line Count' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'LNCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'old security number old company code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'OSNOCC';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'old security number old company code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'OSNMN';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'old security number old company code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'OSNBN';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'group control number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'GRPCRTLNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'subscriber control number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'SCNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'group insured name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'GRPINSNME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Course name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CRSNME';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'TYPE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'number of units' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'NOUNITS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'agent registration number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'AGREGNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Agency Code (1)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'AGCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Agency Code (2)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'AGCDE02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor address and kanji external character flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CANKNJIECFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor address and kanji external character flag shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CAFLGSCDE01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor address and kanji external character flag address kanji' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CAFLGADRS';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor address and kanji external character flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CAFLGSCDE02 shift code';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor name and Kanji character flag ' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CNKJICFLGSCD';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'contractor name and Kanji character flag shift code' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CNFLGSCDE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'date of change of installment premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DTECHGPREM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'modified installment premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'MODINSTPREM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'premium loan contract' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PREMLNCRT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'date of birth of policyholder' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DOBPH';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contractor Gender' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CGENDER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Phone (2)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BPHNO02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'backup' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BCKUP01';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company territory (1)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CMPNYTER';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pension start flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PSTRTFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'annuity payments' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ANTYPYMNT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Annual number of pension payments' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'ANPENPYMNT';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pension fund' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PENSNFUND';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pension certificate number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PENSNCRTNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'payee category' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'PAYECATE';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'application number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'APPNO';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'backup' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'BCKUP02';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Corporate territory (2)' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'CORPTERTY';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Agency Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'AGNCNUM';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Extract Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'EXPRTFLG';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'USER ID' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DATE and TIME' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COMGWPF', @level2type=N'COLUMN',@level2name=N'DATIME';

END
GO 

