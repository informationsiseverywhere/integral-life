
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RSKCOMMDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='HPADPF'))       
    BEGIN
		ALTER TABLE VM1DTA.HPADPF ADD RSKCOMMDATE INT NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<RSKCOMMDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'HPADPF', @level2type=N'COLUMN',@level2name=N'RSKCOMMDATE ';
	END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'DECLDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='HPADPF'))       
    BEGIN
		ALTER TABLE VM1DTA.HPADPF ADD DECLDATE INT NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<DECLDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'HPADPF', @level2type=N'COLUMN',@level2name=N'DECLDATE ';
	END
GO

GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'FIRSTPRMRCPDATE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='HPADPF'))       
    BEGIN
		ALTER TABLE VM1DTA.HPADPF ADD FIRSTPRMRCPDATE INT NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<FIRSTPRMRCPDATE>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'HPADPF', @level2type=N'COLUMN',@level2name=N'FIRSTPRMRCPDATE ';
	END
GO

