
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'CONCOMMFLG' and object_id in (SELECT object_id FROM sys.tables WHERE name ='CHDRPF'))       
    BEGIN
		ALTER TABLE VM1DTA.CHDRPF ADD CONCOMMFLG NCHAR(1) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<CONCOMMFLG>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'CHDRPF', @level2type=N'COLUMN',@level2name=N'CONCOMMFLG ';
	END
GO