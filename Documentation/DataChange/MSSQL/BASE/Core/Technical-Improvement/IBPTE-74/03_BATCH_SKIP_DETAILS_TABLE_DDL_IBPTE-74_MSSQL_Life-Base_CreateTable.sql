IF NOT EXISTS ( SELECT * FROM sys.tables where name = 'BATCH_SKIP_DETAILS' and type = 'U' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))

BEGIN

CREATE TABLE [VM1DTA].[BATCH_SKIP_DETAILS](
[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
[COMPANY] [VARCHAR](1),
[RUNDATE] [DATE],
[BATCHNAME] [VARCHAR](20),
[BATCHNUMBER] [INT],
[EXECUTIONSEQ] [INT] NOT NULL,
[ENTITYPREFIX] [VARCHAR](2),
[ENTITYNUMBER] [VARCHAR](20),
[RETRYCOUNT] [INT] DEFAULT 0,
[PROCESSFLAG] [VARCHAR] (1),
[CREATED_AT] [datetime2](6) DEFAULT getdate() ,
[DATIME] [datetime2](6),
CONSTRAINT [PK_BATCH_SKIP_DETAILS] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


IF EXISTS ( SELECT o.* FROM sys.indexes i inner join sys.objects o on i.object_id = o.object_id WHERE i.name = 'IDX_BSD1' and o.name = 'BATCH_SKIP_DETAILS')
DROP INDEX [IDX_BSD1] ON [VM1DTA].[BATCH_SKIP_DETAILS]

CREATE NONCLUSTERED INDEX [IDX_BSD1] ON [VM1DTA].[BATCH_SKIP_DETAILS]
(
	[ENTITYNUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)


IF EXISTS ( SELECT o.* FROM sys.indexes i inner join sys.objects o on i.object_id = o.object_id WHERE i.name = 'IDX_BSD2' and o.name = 'BATCH_SKIP_DETAILS')
DROP INDEX [IDX_BSD2] ON [VM1DTA].[BATCH_SKIP_DETAILS]

CREATE NONCLUSTERED INDEX [IDX_BSD2] ON [VM1DTA].[BATCH_SKIP_DETAILS]
(
	[EXECUTIONSEQ] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch Skip Details' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'COMPANY'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Run Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'RUNDATE'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch Name' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'BATCHNAME'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'BATCHNUMBER'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Execution Sequence' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'EXECUTIONSEQ'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity Prefix' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'ENTITYPREFIX'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'ENTITYNUMBER'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Retry Count' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'RETRYCOUNT'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Process Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'PROCESSFLAG'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created at' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'CREATED_AT'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Updated at' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'BATCH_SKIP_DETAILS', @level2type=N'COLUMN',@level2name=N'DATIME'

END
GO