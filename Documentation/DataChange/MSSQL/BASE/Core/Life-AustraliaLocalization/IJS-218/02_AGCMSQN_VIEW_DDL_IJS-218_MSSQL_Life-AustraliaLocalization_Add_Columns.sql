IF EXISTS ( SELECT * FROM sys.views where name = 'AGCMSQN' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].AGCMSQN
GO



CREATE VIEW [VM1DTA].[AGCMSQN](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TRANNO, AGNTNUM, EFDATE, ANNPREM, BASCMETH, INITCOM, BASCPY, COMPAY, COMERN, SRVCPY, SCMDUE, SCMEARN, RNWCPY, RNLCDUE, RNLCEARN, AGCLS, TERMID, TRDT, TRTM, USER_T, VALIDFLAG, CURRFROM, CURRTO, PTDATE, SEQNO, CEDAGENT, OVRDCAT, DORMFLAG,INITCOMMGST, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            LIFE,
            COVERAGE,
            RIDER,
            PLNSFX,
            TRANNO,
            AGNTNUM,
            EFDATE,
            ANNPREM,
            BASCMETH,
            INITCOM,
            BASCPY,
            COMPAY,
            COMERN,
            SRVCPY,
            SCMDUE,
            SCMEARN,
            RNWCPY,
            RNLCDUE,
            RNLCEARN,
            AGCLS,
            TERMID,
            TRDT,
            TRTM,
            USER_T,
            VALIDFLAG,
            CURRFROM,
            CURRTO,
            PTDATE,
            SEQNO,
            CEDAGENT,
            OVRDCAT,
            DORMFLAG,
			INITCOMMGST,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.AGCMPF
      WHERE VALIDFLAG = '1' AND DORMFLAG <> 'Y'
GO