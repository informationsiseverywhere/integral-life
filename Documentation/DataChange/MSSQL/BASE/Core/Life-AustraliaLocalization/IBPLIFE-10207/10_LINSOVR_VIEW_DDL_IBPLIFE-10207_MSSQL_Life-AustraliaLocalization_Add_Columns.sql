IF EXISTS ( SELECT * FROM sys.views where name = 'LINSOVR' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
DROP VIEW [VM1DTA].[LINSOVR]
GO

CREATE VIEW [VM1DTA].[LINSOVR](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTCURR, BILLCURR, VALIDFLAG, BRANCH, INSTFROM, INSTTO, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, INSTJCTL, CBILLAMT, BILLCHNL, PAYFLAG, DUEFLG, TRANSCODE, USRPRF, JOBNM, DATIME, PRORATEREC) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            CNTCURR,
            BILLCURR,
            VALIDFLAG,
            BRANCH,
            INSTFROM,
            INSTTO,
            INSTAMT01,
            INSTAMT02,
            INSTAMT03,
            INSTAMT04,
            INSTAMT05,
            INSTAMT06,
            INSTFREQ,
            INSTJCTL,
            CBILLAMT,
            BILLCHNL,
            PAYFLAG,
            DUEFLG,
            TRANSCODE,
            USRPRF,
            JOBNM,
            DATIME,
			PRORATEREC
       FROM vm1dta.LINSPF
      WHERE PAYFLAG = 'P'
GO
