IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PRORCNTFEE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5353DATA'))
	BEGIN
		ALTER TABLE [VM1DTA].[B5353DATA] ADD PRORCNTFEE numeric(17,2) null ;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<test comment>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5353DATA', @level2type=N'COLUMN',@level2name=N'PRORCNTFEE';
	END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PRORAMT' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5353DATA'))
	BEGIN
		ALTER TABLE [VM1DTA].[B5353DATA] ADD PRORAMT numeric(17,2) null ;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<test comment>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5353DATA', @level2type=N'COLUMN',@level2name=N'PRORAMT';
	END
GO