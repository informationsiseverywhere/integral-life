IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'REINSTATED' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVRPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[COVRPF] ADD REINSTATED nchar(1) null ;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<test comment>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVRPF', @level2type=N'COLUMN',@level2name=N'REINSTATED';
	END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PRORATEPREM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVRPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[COVRPF] ADD PRORATEPREM numeric(17,2) null ;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<test comment>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'COVRPF', @level2type=N'COLUMN',@level2name=N'PRORATEPREM';
	END
GO
