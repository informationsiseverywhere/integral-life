IF NOT EXISTS (SELECT * FROM sys.tables where name = 'RERTPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
CREATE TABLE [VM1DTA].[RERTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[LIFE] [nchar](2) NULL,
	[JLIFE] [nchar](2) NULL,
	[COVERAGE] [nchar](2) NULL,
	[RIDER] [nchar](2) NULL,
	[PLNSFX] [int] NULL,
	[EFFDATE] [int] NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[LASTINST] [numeric](17, 2) NULL,
	[NEWINST] [numeric](17, 2) NULL,
	[ZBLASTINST] [numeric](17, 2) NULL,
	[ZBNEWINST] [numeric](17, 2) NULL,
	[ZLLASTINST] [numeric](17, 2) NULL,
	[ZLNEWINST] [numeric](17, 2) NULL,
	[LASTZSTPDUTY] [numeric](17, 2) NULL,
	[NEWZSTPDUTY] [numeric](17, 2) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[CREATED_AT] [datetime2](6) NULL
	

 CONSTRAINT [PK_RERTPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];


		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RERTPF Table file' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Life' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'LIFE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Joint Life' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'JLIFE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Coverage' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'COVERAGE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rider' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'RIDER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Plan suffix' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'PLNSFX';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'EFFDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valid Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
        EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last Inst' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'LASTINST';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New Inst' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'NEWINST';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ZB LAST INST' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'ZBLASTINST';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ZB NEW INST' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'ZBNEWINST';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ZL LAST INST' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'ZLLASTINST';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ZL NEW INST' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'ZLNEWINST';
        EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LAST ZSTAMP DUTY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'LASTZSTPDUTY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NEW ZSTAMP DUTY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'NEWZSTPDUTY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Profile' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jobnm' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datime' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'DATIME';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created At' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'CREATED_AT';
END
GO



	

	
	
	
	
	