SET QUOTED_IDENTIFIER ON 
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'LIFE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='EXCLPF'))
ALTER TABLE [VM1DTA].[EXCLPF] ADD LIFE nchar(2);
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COVERAGE' and object_id in (SELECT object_id FROM sys.tables WHERE name ='EXCLPF'))
ALTER TABLE [VM1DTA].[EXCLPF] ADD COVERAGE nchar(2);
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RIDER' and object_id in (SELECT object_id FROM sys.tables WHERE name ='EXCLPF'))
ALTER TABLE [VM1DTA].[EXCLPF] ADD RIDER nchar(2);
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PLNSFX' and object_id in (SELECT object_id FROM sys.tables WHERE name ='EXCLPF'))
ALTER TABLE [VM1DTA].[EXCLPF] ADD PLNSFX int;
GO

SET QUOTED_IDENTIFIER OFF
GO 