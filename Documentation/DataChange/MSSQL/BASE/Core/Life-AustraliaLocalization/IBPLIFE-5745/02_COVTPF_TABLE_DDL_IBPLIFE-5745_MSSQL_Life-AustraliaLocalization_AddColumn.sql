IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('COMMPREM') and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVTPF'))
BEGIN
	ALTER TABLE VM1DTA.COVTPF ADD COMMPREM NUMERIC(17, 2) DEFAULT '0';
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Commission Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'COVTPF', @level2type=N'COLUMN',@level2name=N'COMMPREM';
END
GO


