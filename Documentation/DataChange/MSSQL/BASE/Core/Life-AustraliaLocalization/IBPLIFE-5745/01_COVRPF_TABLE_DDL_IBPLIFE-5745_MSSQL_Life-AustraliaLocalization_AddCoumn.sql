IF NOT EXISTS (SELECT * FROM sys.columns WHERE name in('COMMPREM') and object_id in (SELECT object_id FROM sys.tables WHERE name ='COVRPF'))
BEGIN
	ALTER TABLE VM1DTA.COVRPF ADD COMMPREM numeric(17, 2);
EXEC sys.sp_addextendedproperty @name=N'MS_Description', 
@value=N'Used for Commission Premium' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
@level1name=N'COVRPF', @level2type=N'COLUMN',@level2name=N'COMMPREM';
END
GO

