SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM sys.views where name = 'BEXTDDB' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
	DROP VIEW [VM1DTA].BEXTDDB
GO


CREATE VIEW [VM1DTA].[BEXTDDB](UNIQUE_NUMBER, INSTBCHNL, FACTHOUS, BANKCODE, PAYRCOY, PAYRNUM, MANDREF, BILLDATE, CHDRPFX, CHDRCOY, CHDRNUM, SERVUNIT, BILLCHNL, CNTTYPE, CNTCURR, OCCDATE, CCDATE, PTDATE, BTDATE, INSTFROM, INSTTO, INSTCCHNL, INSTFREQ, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTAMT07, INSTAMT08, INSTAMT09, INSTAMT10, INSTAMT11, INSTAMT12, INSTAMT13, INSTAMT14, INSTAMT15, INSTJCTL, GRUPKEY, MEMBSEL, BANKKEY, BANKACCKEY, COWNPFX, COWNCOY, COWNNUM, PAYRPFX, CNTBRANCH, AGNTPFX, AGNTCOY, AGNTNUM, PAYFLAG, BILFLAG, OUTFLAG, SUPFLAG, BILLCD, SACSCODE, SACSTYP, GLMAP, MANDSTAT, DDDEREF, DDRSNCDE, CANFLAG, USRPRF, COMPANY, JOBNO, JOBNM, DATIME, EFFDATEX,RDOCNUM) AS
SELECT UNIQUE_NUMBER,
            INSTBCHNL,
            FACTHOUS,
            BANKCODE,
            PAYRCOY,
            PAYRNUM,
            MANDREF,
            BILLDATE,
            CHDRPFX,
            CHDRCOY,
            CHDRNUM,
            SERVUNIT,
            BILLCHNL,
            CNTTYPE,
            CNTCURR,
            OCCDATE,
            CCDATE,
            PTDATE,
            BTDATE,
            INSTFROM,
            INSTTO,
            INSTCCHNL,
            INSTFREQ,
            INSTAMT01,
            INSTAMT02,
            INSTAMT03,
            INSTAMT04,
            INSTAMT05,
            INSTAMT06,
            INSTAMT07,
            INSTAMT08,
            INSTAMT09,
            INSTAMT10,
            INSTAMT11,
            INSTAMT12,
            INSTAMT13,
            INSTAMT14,
            INSTAMT15,
            INSTJCTL,
            GRUPKEY,
            MEMBSEL,
            BANKKEY,
            BANKACCKEY,
            COWNPFX,
            COWNCOY,
            COWNNUM,
            PAYRPFX,
            CNTBRANCH,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            PAYFLAG,
            BILFLAG,
            OUTFLAG,
            SUPFLAG,
            BILLCD,
            SACSCODE,
            SACSTYP,
            GLMAP,
            MANDSTAT,
            DDDEREF,
            DDRSNCDE,
            CANFLAG,
            USRPRF,
            COMPANY,
            JOBNO,
            JOBNM,
            DATIME,
            EFFDATEX,
			RDOCNUM
       FROM VM1DTA.BEXTPF
GO
SET QUOTED_IDENTIFIER OFF  