
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'RDOCNUM' and object_id in (SELECT object_id FROM sys.tables WHERE name ='BEXTPF'))	
ALTER TABLE [VM1DTA].[BEXTPF] ADD RDOCNUM nchar(9);

GO  
