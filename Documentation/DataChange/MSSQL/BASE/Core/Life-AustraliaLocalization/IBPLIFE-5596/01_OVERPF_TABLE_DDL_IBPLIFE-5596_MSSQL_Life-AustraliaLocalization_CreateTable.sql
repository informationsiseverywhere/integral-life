IF NOT EXISTS (SELECT * FROM sys.tables where name = 'OVERPF' and schema_id = (select schema_id from sys.schemas where name='VM1DTA'))
BEGIN
CREATE TABLE [VM1DTA].[OVERPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[OVERDUEFLAG] [nchar](1) NULL,
	[EFFECTIVEDATE] [int] NULL,
	[ENDDATE] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[CREATED_AT] [datetime2](6) NULL
	

 CONSTRAINT [PK_OVERPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];


		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OVERPF Table file' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNIQUE_NUMBER' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'UNIQUE_NUMBER';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'COMPANY' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
		@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'CHDRCOY';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Type' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'CNTTYPE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract Number' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
		@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'CHDRNUM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valid Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'VALIDFLAG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Overdue Flag' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'OVERDUEFLAG';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'EFFECTIVEDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End Date' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',
		@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'ENDDATE';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User Profile' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'USRPRF';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jobnm' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'JOBNM';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Datime' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'DATIME';
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created At' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'OVERPF', @level2type=N'COLUMN',@level2name=N'CREATED_AT';
END
GO



	

	
	
	
	
	