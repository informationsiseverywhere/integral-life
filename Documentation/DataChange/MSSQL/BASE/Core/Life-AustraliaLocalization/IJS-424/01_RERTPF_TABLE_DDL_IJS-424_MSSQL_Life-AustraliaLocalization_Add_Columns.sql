IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'TRANNO' and object_id in (SELECT object_id FROM sys.tables WHERE name ='RERTPF'))
	BEGIN
		ALTER TABLE [VM1DTA].[RERTPF] ADD TRANNO INT null ;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<TRANSACTION NUMBER>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'RERTPF', @level2type=N'COLUMN',@level2name=N'TRANNO';
	END
GO


