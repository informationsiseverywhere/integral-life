
SET QUOTED_IDENTIFIER ON  
GO
IF EXISTS (SELECT  * FROM VM1DTA.itempf where itemitem in ('C***','CACC','CACR','CAE1','CAEN','CAER','CDAN','CDAT','CDAW','CFLX','CFWL','CGCS',
'CLCE','CLST','CMRT','CPHI','CPPE','CPPN','CRSB','CRTA','CRTG','CRTN','CRUL','CRUX','CTEN','CTRM','CTWL','CULM','CULP','D***','DACC','DACR',
'DAEN','DAER','DDAN','DDAT','DDAW','DFLX','DFWL','DGCS','DLCE','DPHI','DPPE','DPPN','DRSB','DRTG','DRTN','DRUL','DRUX','DTEN','DTRM','DTWL','DULM',
'DULP','EGCS','G***','GACC','GACR','GAEN','GAER','GDAN','GDAT','GDAW','GFWL','GGCS','GLCE','GLST','GMRT','GPHI','GPPE','GPPN','GRSB','GRTG','GRTN',
'GRUL','GRUX','GTEN','GTRM','GTWL','GULM','N***','NACS','NAEN','NAES','NGCS','NIAN','NIAT','NMRT','NRTA','NSDT','NSPB','NSPE','NVAP','R***','RAEN',
'RGCS','RTEN','A***','B***','S***','NOIS','NSIS','DLCP','RLCP','ALCP','BLCP','NIAA','NLCP') AND ITEMCOY='2')
       DELETE FROM VM1DTA.itempf where itemitem IN ('C***','CACC','CACR','CAE1','CAEN','CAER','CDAN','CDAT','CDAW','CFLX','CFWL','CGCS',
'CLCE','CLST','CMRT','CPHI','CPPE','CPPN','CRSB','CRTA','CRTG','CRTN','CRUL','CRUX','CTEN','CTRM','CTWL','CULM','CULP','D***','DACC','DACR',
'DAEN','DAER','DDAN','DDAT','DDAW','DFLX','DFWL','DGCS','DLCE','DPHI','DPPE','DPPN','DRSB','DRTG','DRTN','DRUL','DRUX','DTEN','DTRM','DTWL','DULM',
'DULP','EGCS','G***','GACC','GACR','GAEN','GAER','GDAN','GDAT','GDAW','GFWL','GGCS','GLCE','GLST','GMRT','GPHI','GPPE','GPPN','GRSB','GRTG','GRTN',
'GRUL','GRUX','GTEN','GTRM','GTWL','GULM','N***','NACS','NAEN','NAES','NGCS','NIAN','NIAT','NMRT','NRTA','NSDT','NSPB','NSPE','NVAP','R***','RAEN',
'RGCS','RTEN','A***','B***','S***','NOIS','NSIS','DLCP','RLCP','ALCP','BLCP','NIAA','NLCP') AND ITEMCOY='2';
GO
SET QUOTED_IDENTIFIER OFF 
GO

