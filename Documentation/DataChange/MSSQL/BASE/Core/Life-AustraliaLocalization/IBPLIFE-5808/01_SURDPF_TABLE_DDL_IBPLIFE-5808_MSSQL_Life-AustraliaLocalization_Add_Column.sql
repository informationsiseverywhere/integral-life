
SET QUOTED_IDENTIFIER ON  
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'COMMCLAW' and object_id in (SELECT object_id FROM sys.tables WHERE name ='SURDPF'))
	
	BEGIN
		ALTER TABLE VM1DTA.SURDPF ADD COMMCLAW numeric(17, 2) NULL;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<COMMCLAW>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'SURDPF', @level2type=N'COLUMN',@level2name=N'COMMCLAW';
	END
	
GO 
SET QUOTED_IDENTIFIER OFF  
GO