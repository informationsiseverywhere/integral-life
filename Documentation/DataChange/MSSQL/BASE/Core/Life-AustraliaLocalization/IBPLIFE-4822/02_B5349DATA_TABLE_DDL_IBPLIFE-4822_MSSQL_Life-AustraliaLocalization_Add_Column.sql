GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'ORGBILLCD' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5349DATA'))       
    BEGIN
		ALTER TABLE VM1DTA.B5349DATA ADD ORGBILLCD int NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<ORGBILLCD>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5349DATA', @level2type=N'COLUMN',@level2name=N'ORGBILLCD';
	END
GO