GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name = 'PAYORGBILLCD' and object_id in (SELECT object_id FROM sys.tables WHERE name ='B5353DATA'))       
    BEGIN
		ALTER TABLE VM1DTA.B5353DATA ADD PAYORGBILLCD int NULL DEFAULT 0;
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'<PAYORGBILLCD>' , @level0type=N'SCHEMA',@level0name=N'VM1DTA', @level1type=N'TABLE',@level1name=N'B5353DATA', @level2type=N'COLUMN',@level2name=N'PAYORGBILLCD';
	END
GO


