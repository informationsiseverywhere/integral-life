<?xml version="1.0" encoding="GB2312"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
06/04/05    01/01         XMLP01         Lily   Zhu                                                                  
                   Original Version                                                                 *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LAPPL"/>
	</xsl:template>
	<xsl:template match="LAPPL">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="15pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
				
					<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>	
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
					
					</xsl:call-template>
				</fo:static-content>
				<!--============ Writing the body section ===================-->				
				<fo:flow flow-name="xsl-region-body">
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-size="16pt" line-height="18pt" text-align="left" space-after.optimum="15pt" text-decoration="underline">
						机密
					</fo:block>
					<fo:table width="100%">
						<fo:table-column column-width="proportional-column-width(70)"/>
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="CltName1"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Request_Date"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:block>
						<xsl:value-of select="CltAddr1"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="CltAddr2"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="CltAddr3"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="CltAddr4"/>
					</fo:block>
					<fo:block space-after.optimum="15pt">
						<xsl:value-of select="CltAddr5"/>
					</fo:block>
					<fo:block space-after.optimum="15pt">
					尊敬的
					<xsl:value-of select="CltName2"/>
					</fo:block>
					<!--===========  Date and Contract num  ==============================-->
					<fo:table space-after.optimum="20pt" width="80%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>申请从         ：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Contract_Type_Desc"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>保单号         ：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="ContrNo"/>
										-
										<xsl:value-of select="Brn"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>受保人：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
									<xsl:value-of select="CltName"/>
                                                            </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:block space-before.optimum="15pt" space-after.optimum="10pt">
					      感谢您在    <xsl:value-of select="Proposal_Date"/>  的申请。
					</fo:block>
					<fo:block space-after.optimum="10pt">
					     为了我们能更快地处理您的申请，请在十四天内完成以下的内容：		   
					</fo:block>
					<fo:table width="100%" space-after.optimum="10pt">
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-column column-width="proportional-column-width(70)"/>
						<fo:table-body>
						<xsl:for-each select="Follow_Up_Details">
						<xsl:if test="SNo!='' or FollowUp_Desc1!='' or FollowUp_Desc2!='' or FollowUp_Desc3!=''" >
						      <fo:table-row line-height="15pt" font-weight="bold" background-color="rgb(180,180,180)">
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>序列号</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>跟进描述</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
								<fo:table-row line-height="12pt">
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="SNo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="FollowUp_Desc1"/>
										</fo:block>
										<fo:block>
											<xsl:value-of select="FollowUp_Desc2"/>
										</fo:block>
										<fo:block>
											<xsl:value-of select="FollowUp_Desc3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>		
								</xsl:if>						
							</xsl:for-each>				
						</fo:table-body>
					</fo:table>
					<fo:block space-after.optimum="10pt">
					   如果您有任何疑问，请拨打我们的客户服务热线  <xsl:value-of select="Service_Hotline"/>。
					</fo:block>
					<fo:block space-after.optimum="20pt">
					    我们期待能够进一步为您服务。					
					</fo:block>
					<fo:block space-after.optimum="20pt">
					   您忠诚的					    
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Name"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Title"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Department_Name"/>
					</fo:block>
					<fo:block space-before="10pt" space-after="15pt">
					       传：					
						<xsl:value-of select="New_Agent_No"/>
						<xsl:value-of select="New_Agent_Name"/>
					</fo:block>					
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
