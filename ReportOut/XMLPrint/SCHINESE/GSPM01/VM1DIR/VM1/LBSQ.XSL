<?xml version="1.0" encoding="GB2312"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
11/04/05    01/01          XMLP01         Lily   Zhu                                                                  
                   Original Version                               
                                             *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LBSQ"/>
	</xsl:template>
	<xsl:template match="LBSQ">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="15pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
			
				<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>	
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
						
					</xsl:call-template>
				</fo:static-content>
				<!--============ Writing the body section ===================-->				
				<fo:flow flow-name="xsl-region-body">
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" text-decoration="underline">
						红利退保报价
					</fo:block>
					<fo:table space-after.optimum="10pt" width="50%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="OwnerName"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs1"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs2"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs3"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs4"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="30pt">
								<fo:table-cell padding-top="1pt">
									<fo:block>
										<xsl:value-of select="Addrs5"/>
										<xsl:text>            </xsl:text>
										<xsl:value-of select="Postcode"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="30pt">
								<fo:table-cell padding-top="1pt">
									<fo:block>
										<xsl:value-of select="Request_Date"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!--===========  Date and Contract num  ==============================-->
					<fo:table space-after.optimum="20pt" width="60%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body>
							<fo:table-row height="30pt" padding-top="1pt">
								<fo:table-cell padding="1pt">
									<fo:block>给保单持有者：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Life_Name"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="30pt" padding-top="1pt">
								<fo:table-cell padding="1pt" text-decoration="underline">
									<fo:block>保单号：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="ContrNo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:block space-after.optimum="10pt">
	针对您最近的询问，我高兴地通知您保单下归原红利的退保金额是<xsl:value-of select="Surrender_Value_Currency"/>
						<xsl:text>   </xsl:text><xsl:value-of select="Surrender_Estimated_Value"/>
					</fo:block>
					<fo:block space-after.optimum="30pt">
					     如果您对于保单还有什么问题，请与我们联络。							</fo:block>
					<fo:block space-after.optimum="10pt">
					   您忠诚的，					    
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Name"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Title"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Department_Name"/>
					</fo:block>					
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
