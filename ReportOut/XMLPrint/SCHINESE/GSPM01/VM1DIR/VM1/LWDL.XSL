<?xml version="1.0" encoding="GB2312"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
08/09/05    01/01          XMLP01          Lewis Liu                                                                        
                   Original Version                               
 *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LWDL"/>
	</xsl:template>
	<xsl:template match="LWDL">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="25pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
				<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>		
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
						
					</xsl:call-template>
				</fo:static-content>			
				<!--============ Writing the body section ===================-->
				<fo:flow flow-name="xsl-region-body">
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" font-weight="bold" text-decoration="underline">
						提取信函
					</fo:block>
					<fo:table space-after.optimum="20pt" width="100%">
						<fo:table-column column-width="proportional-column-width(80)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="OwnerName"/>
									</fo:block>
									<fo:block>
										<xsl:value-of select="Addrs1"/>
									</fo:block>
									<fo:block>
										<xsl:value-of select="Addrs2"/>
									</fo:block>
									<fo:block>
										<xsl:value-of select="Addrs3"/>
									</fo:block>
									<fo:block>
										<xsl:value-of select="Addrs4"/>
									</fo:block>
									<fo:block>
										<xsl:value-of select="Addrs5"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="Postcode"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>
										<xsl:value-of select="Request_Date"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!--===========  Bank Details  ==============================-->
					<fo:block >尊敬的：
					<xsl:value-of select="Life_Name"/>
					</fo:block>
					<fo:table space-after.optimum="20pt" space-before.optimum="20pt" width="80%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>申请为：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Contract_Type_Desc"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>保单号：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="ContrNo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>受保人：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Life_Name"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!-- =============== Transaction Details =====================-->
					<fo:block space-after="10pt">本信函是关于对上述保单进行提取的申请。</fo:block>
					<fo:block space-after="10pt">我们希望确认上述请求已经被处理，请留意附含的退款支票<xsl:value-of select="Suspense_Curr"/>
						<xsl:value-of select="Suspense_Amount"/>paid towards the captioned application.  </fo:block>
					<fo:block space-after="10pt">如果您想再次申请主险，请拨打我们的客户服务热线<xsl:value-of select="Service_Hotline"/>，服务人员将尽其
					所能帮助您。 </fo:block>
					<fo:block space-after="10pt">您忠诚的</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Name"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Title"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Department_Name"/>
					</fo:block>
					<fo:block space-before="10pt">传： <xsl:value-of select="Agent_Number"/>
						<xsl:value-of select="Agent_Name"/>
					</fo:block>
					
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
