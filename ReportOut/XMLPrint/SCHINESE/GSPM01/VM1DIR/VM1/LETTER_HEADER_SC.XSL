<?xml version="1.0" encoding="UTF-8"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
2005/10/11    01/01          XMLP02         Lily  Zhu                                                                    
                   Original Version                               
                                         *************************************************************************************************************************
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template name="HEADER">
		<fo:table  width="80%">
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(80)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell >						
						<fo:block>
						<fo:external-graphic src="../../../gspm01/casdir/LACT/xsl/logo.jpg"/>
					</fo:block>
					</fo:table-cell>
					<fo:table-cell  padding-left="2pt" font-family="Garamond">						
			              <fo:block font-size="18pt" color="rgb(0,0,0)" font-weight="bold">Computer Sciences Corporation </fo:block>
							<fo:block font-size="18pt" color="rgb(0,0,0)" font-weight="200">Financial Services Group</fo:block>						
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:block>
			<fo:leader leader-pattern="rule" rule-thickness="2pt" leader-length="100%"/>
		</fo:block>
	</xsl:template>
</xsl:stylesheet>
