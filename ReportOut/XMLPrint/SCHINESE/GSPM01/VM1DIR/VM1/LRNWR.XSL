<?xml version="1.0" encoding="GB2312"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
04/06/05    01/01          XMLP01          Lewis Liu                                                                        
                   Original Version                               
 *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LRNWR"/>
	</xsl:template>
	<xsl:template match="LRNWR">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="25pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
				<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>		
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
					
					</xsl:call-template>
				</fo:static-content>			
				<!--============ Writing the body section ===================-->
				<fo:flow flow-name="xsl-region-body">
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:table space-after.optimum="20pt" width="50%">
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(80)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>尊敬的：</fo:block>
									<fo:block>地址：</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:table width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="OwnerName"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs1"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs2"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs3"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs4"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs5"/>
														<xsl:text>            </xsl:text>
														<xsl:value-of select="Postcode"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:block font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" font-weight="bold" text-decoration="underline">
						续期收据
					</fo:block>
					<!--===========  Date and Contract num  ==============================-->
					<fo:block>
						<fo:table space-after.optimum="20pt" width="80%">
							<fo:table-column column-width="proportional-column-width(40)"/>
							<fo:table-column column-width="proportional-column-width(60)"/>
							<fo:table-body >
								<fo:table-row  height="35pt">
									<fo:table-cell padding="1pt">
										<fo:block>收据号：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Document_Number"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>保单号：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="ContrNo"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>续期年份：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Renewal_Year"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>分期数目：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Instalment"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>主险条款：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="MainCovr_Risk_Cess_Term"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>保费条款：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="MainCovr_Prem_Cess_Term"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row   height="35pt">
									<fo:table-cell padding="1pt">
										<fo:block>风险生效日期：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Original_Commencement_Date"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>收费日期：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Bill_To_Date"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row   height="35pt">
									<fo:table-cell padding="1pt">
										<fo:block>支付描述：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Payment_Desc"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="1pt">
										<fo:block>保费总计：</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1pt">
										<fo:block>
											<xsl:value-of select="Total_Premium"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
