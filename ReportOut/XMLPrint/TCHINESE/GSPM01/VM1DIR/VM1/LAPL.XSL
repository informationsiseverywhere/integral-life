<?xml version="1.0" encoding="Big5"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
06/04/05    01/01          XMLP01         Lily  Zhu                                                                   
                   Original Version                               
                                       *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LAPL"/>
	</xsl:template>
	<xsl:template match="LAPL">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="15pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
				
				<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>	
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
				
					</xsl:call-template>
				</fo:static-content>
				<!--============ Writing the body section ===================-->				
				<fo:flow flow-name="xsl-region-body">
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" text-decoration="underline">
						自動保費貸款信函
					</fo:block>
					<fo:table space-after.optimum="20pt" width="50%">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row height="30pt" padding-top="1pt">
								<fo:table-cell padding="1pt">
									<fo:block>
										保密
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="OwnerName"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs1"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs2"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs3"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs4"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Addrs5"/>
										<xsl:text>            </xsl:text>
										<xsl:value-of select="Postcode"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!--===========  Date and Contract num  ==============================-->
					<fo:table space-after.optimum="20pt" width="80%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body >
							<fo:table-row height="30pt" padding-top="1pt">
								<fo:table-cell padding="1pt">
									<fo:block>尊敬的</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="-80pt">
									<fo:block>
										<xsl:value-of select="Life_Name"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="30pt" padding-top="1pt">
								<fo:table-cell padding="1pt">
									<fo:block>方案：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Contract_Type_Desc"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>証書號</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>保單號：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="ContrNo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>							
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>被保人：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Life_Name"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>保費默認日期：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Prem_Default_Date"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>期交保費：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Contract_Currency"/>
										<xsl:text>            </xsl:text>
										<xsl:value-of select="Modal_Premium"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>逾期保費 :</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Contract_Currency"/>
										<xsl:text>            </xsl:text>
										<xsl:value-of select="Overdue_Premium"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:block space-after.optimum="10pt">
					
我們希望通知您，以上保單的到期保費在寬限期結束時還沒有被付清。                                          					
					</fo:block>
					<fo:block space-after.optimum="10pt">
					    根据您保單中的不能作廢預備下的自動保費貸款，公司已經申請了不能作廢金額用來償還
					    逾期保費。因此，保單已經支付至    <xsl:value-of select="New_PTD"/> 。
					    除非我們收到您的意見，我們將在下次保費到期時重新寄出收費單。                               		     
					</fo:block>
					<fo:block space-after.optimum="30pt">
					    如果您想償還保單貸款，一次性償還或分期付款，請撥打我們的客戶服務熱縣
						<xsl:value-of select="Service_Hotline"/>
					</fo:block>
					<fo:block space-after.optimum="10pt">
					    您忠誠的					    
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Name"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Title"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Department_Name"/>
					</fo:block>
					<fo:block space-before="10pt" space-after="15pt">
					       PID					
						<xsl:value-of select="Agent_Number"/>
						<xsl:value-of select="Agent_Name"/>
					</fo:block>						
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
