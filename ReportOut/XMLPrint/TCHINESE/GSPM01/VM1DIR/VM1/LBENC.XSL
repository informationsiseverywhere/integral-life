<?xml version="1.0" encoding="Big5"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
 07/04/05     01/01      XMLP01          Lily   Zhu                                                                   
              Initial  Version     
                         
*************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LBENC"/>
	</xsl:template>
	<xsl:template match="LBENC">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="25pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
			
					<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>	
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
					
					</xsl:call-template>
				</fo:static-content>
				   		
				<!--============ Writing the body section ===================-->
				<fo:flow flow-name="xsl-region-body">
					<fo:table space-after.optimum="20pt" width="50%">
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(80)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block >尊敬的：</fo:block>
									<fo:block >地址：</fo:block>									
								</fo:table-cell>
								<fo:table-cell>
									<fo:table width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="OwnerName"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs1"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs2"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs3"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs4"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs5"/>
														<xsl:text >            </xsl:text >														
														<xsl:value-of select="Postcode"  />																									
													</fo:block>
												</fo:table-cell>												
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-weight="bold" font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" text-decoration="underline">
						受益人更改信函
					</fo:block>
					<!--===========  Date and Contract num  ==============================-->
					<fo:table space-after.optimum="20pt" width="50%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block >日期：</fo:block>									
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
											<xsl:value-of select="Request_Date"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block >保單號：</fo:block>									
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
											<xsl:value-of select="ContrNo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>								
						</fo:table-body>
					</fo:table>
					
					<!-- =============== change Details =====================-->
					<fo:block>前受益人：</fo:block>
					<fo:table width="100%" space-after.optimum="20pt">
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-body>
						<xsl:if test="PreviousBeneficiary">
							<fo:table-row line-height="15pt" font-weight="bold" background-color="rgb(180,180,180)">
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>前受益人編號</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>前受益人姓名</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:for-each select="PreviousBeneficiary">
								<fo:table-row line-height="12pt">
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Last_Bnfy_No"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Last_Bnfy_Name"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
							</xsl:if>							
						</fo:table-body>
					</fo:table>
                   <!-- =============== change Details =====================-->
					<fo:block >新受益人：</fo:block>
					<fo:table width="100%" space-after.optimum="20pt">
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-body>
						<xsl:if test="NewBeneficiary">
							<fo:table-row line-height="15pt" font-weight="bold" background-color="rgb(180,180,180)">
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>新受益人編號</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>新受益人姓名</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:for-each select="NewBeneficiary">
								<fo:table-row line-height="12pt">
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Bnfy_No"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Bnfy_Name"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>	
							</xsl:if>						
						</fo:table-body>
					</fo:table>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
