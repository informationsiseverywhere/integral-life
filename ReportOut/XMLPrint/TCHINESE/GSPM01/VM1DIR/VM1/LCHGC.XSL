<?xml version="1.0" encoding="Big5"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
11/04/05    01/01          XMLP01         Lily    Zhu                                                                 
                   Original Version                                                              *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LCHGC"/>
	</xsl:template>
	<xsl:template match="LCHGC">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Songti" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="25pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="3cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
				<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
					
					</xsl:call-template>
				</fo:static-content>
				<!--============ Writing the body section ===================-->
				<fo:flow flow-name="xsl-region-body">
					<fo:table space-after.optimum="20pt" width="50%">
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(80)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>尊敬的：</fo:block>
									<fo:block>地址：</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:table width="100%">
										<fo:table-column column-width="proportional-column-width(100)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="OwnerName"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs1"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs2"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs3"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs4"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Addrs5"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1pt">
													<fo:block>
														<xsl:value-of select="Postcode"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-weight="bold" font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" text-decoration="underline">
						組成更改信函
					</fo:block>
					<!--===========  Date and Contract num  ==============================-->
					<fo:table space-after.optimum="20pt" width="50%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>請求日期：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Request_Date"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>保單號：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="ContrNo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<!-- =============== Component Change =====================-->
					<fo:block space-after="15pt" font-weight="bold">組成更改  </fo:block>
					<fo:table width="100%">
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-body>
							<fo:table-row line-height="15pt" font-weight="bold" background-color="rgb(180,180,180)">
								<fo:table-cell border-width="1pt" padding="1pt" border-style="solid">
									<fo:block>組成描述</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" padding="1pt" border-style="solid">
									<fo:block>前保額</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" padding="1pt" border-style="solid">
									<fo:block>現保額</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:if test="ComponentChange">
								<xsl:for-each select="ComponentChange">
									<fo:table-row line-height="12pt">
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="Chg_Cvr_Desc"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="Prev_SI"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="Chg_SI"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:for-each>
							</xsl:if>
						</fo:table-body>
					</fo:table>
					<fo:block font-style="italic" space-after.optimum="20pt"> (每份保單最多可打印10個組成更改)</fo:block>
					<!-- =============== Component Details  =====================-->
					<fo:block space-after="15pt" font-weight="bold">組件明細 </fo:block>
					<fo:table width="100%">
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-body>
							<fo:table-row line-height="15pt" font-weight="bold" background-color="rgb(180,180,180)">
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>組成描述</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>保額</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>保費</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>保費終止日期</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:if test="ComponentDetails">
								<xsl:for-each select="ComponentDetails">
									<fo:table-row line-height="12pt">
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="Covr_Desc"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="CovrSI"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="Covr_Prem"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
											<fo:block>
												<xsl:value-of select="Prem_CessDte"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:for-each>
							</xsl:if>
						</fo:table-body>
					</fo:table>
					<fo:block font-style="italic" space-after.optimum="20pt">(每份保單最多可打印30個主險/附加險)</fo:block>
					<!--===========  Total amt.  ==============================-->
					<fo:table space-after.optimum="20pt" width="60%">
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>總保費金額：</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Tot_Prem"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
