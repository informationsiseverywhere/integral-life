<?xml version="1.0" encoding="UTF-8"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
05/04/05    01/01          XMLP01          Lewis Liu                                                                        
                   Original Version                               
05/28/07                        XMLP06         Lewis Liu   
                    Update to avoid  blank rows in PDF tables                                                 *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LPOLCVR"/>
	</xsl:template>
	<xsl:template match="LPOLCVR">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Times Roman" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="25pt" margin-bottom="25pt" margin-left="75pt" margin-right="75pt">
					<fo:region-body margin-bottom="2cm" margin-top="2cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="2cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
			<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>		
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content space-before="10pt" flow-name="xsl-region-after" display-align="after">
					<xsl:call-template name="FOOTER_L">
						
					</xsl:call-template>
				</fo:static-content>	
				<!--============ Writing the body section ===================-->
				<fo:flow flow-name="xsl-region-body">
					<!--			<xsl:call-template name="HEADER"/>          -->
					<fo:block font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" font-weight="bold" text-decoration="underline">
						Policy Coverage Letter
					</fo:block>
					<fo:block font-size="14pt" line-height="16pt" space-after="10pt" text-align="center">CONFIDENTIAL</fo:block>
					<fo:table space-after.optimum="10pt" space-before.optimum="10pt" width="100%">
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-column column-width="proportional-column-width(80)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>
									<fo:block space-after="10pt">
										<xsl:value-of select="Request_Date_Bhs"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="OwnerName"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="Addrs1"/><xsl:value-of select="Addrs2"/><br/><xsl:value-of select="Addrs3"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>
								
									<!--<fo:table-row>
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="Addrs5"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>-->
									<!--<fo:table-row>
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="Postcode"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>-->
									<fo:table-row>
									<fo:table-cell padding="1pt">
									<fo:block space-before="8pt">Consultant</fo:block>
								    </fo:table-cell>
									<fo:table-cell>
									<fo:block space-before="8pt">:
										<xsl:value-of select="OwnerSalut"/>&#160;<xsl:value-of select="Agent_Name"/>/<xsl:value-of select="New_Agent_No"/>/<xsl:value-of select="Agent_Brn_Area"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>
									<!--<fo:table-row>
									<fo:table-cell padding="1pt">
									<fo:block>No  :</fo:block>
								    </fo:table-cell>
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="Agent_Name"/>/<xsl:value-of select="New_Agent_No"/>/<xsl:value-of select="Agent_Brn_Area"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>-->
									<!--<fo:table-row>
									<fo:table-cell padding="1pt">
									<fo:block>New Agent No. :</fo:block>
								    </fo:table-cell>
									<fo:table-cell>
									<fo:block>
									    <xsl:value-of select="New_Agent_No"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>-->
									
									<!--<fo:table-row>
									<fo:table-cell padding="1pt">
									<fo:block>No  :</fo:block>
								    </fo:table-cell>
									<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="New_Agent_No"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
									<fo:table-cell>
									<fo:block>
										<xsl:value-of select="Agent_Brn_Area"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>-->
									<fo:table-row>
									<fo:table-cell>
									<fo:block space-before="8pt">No</fo:block>
									</fo:table-cell>
									<fo:table-cell><fo:block space-before="8pt">:
										<xsl:value-of select="ReferenceNumber"/>
									</fo:block>
									</fo:table-cell>
									</fo:table-row>
									<!--<fo:table-row>
									<fo:table-cell space-before="8pt" >
									<fo:block space-before="8pt">
										<xsl:value-of select="OwnerSalut"/>
									</fo:block>
								</fo:table-cell>
								</fo:table-row>-->
						</fo:table-body>
					</fo:table>
	

<fo:block font-size="10pt" line-height="12pt" space-after="2pt" text-align="left">With Respect,</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="8pt" space-before="9pt" text-align="left">
Welcome to join the big family CSC. We would like to thank <xsl:value-of select="OwnerSalut"/>&#160;
who has chosen CSC to provide protection for <xsl:value-of select="OwnerSalut"/>&#160;and family.
</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="8pt" text-align="left">
We ask <xsl:value-of select="OwnerSalut"/>&#160;to study the provisions of this Policy carefully. If there is no response within 14
(Fourteen) days after <xsl:value-of select="OwnerSalut"/>&#160;Polis received then we assume <xsl:value-of select="OwnerSalut"/>&#160;has approved this Policy.
</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="8pt" text-align="left">
If <xsl:value-of select="OwnerSalut"/>&#160;require further explanation, <xsl:value-of select="OwnerSalut"/>&#160;can contact Customer Service at + 62-21 21345678 and + 62-21 23456789 or email: customercare@csc-life.co.id
</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="8pt" text-align="left">
We, therefore, convey. The attention and trust that <xsl:value-of select="OwnerSalut"/>&#160;provide, we say thank you.
</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="4pt" space-before="16pt" text-align="left">
Best Regards,
</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="2pt" space-before="8pt" text-align="left">
Andrew Banbang 
</fo:block>
<fo:block font-size="10pt" line-height="12pt" space-after="8pt" text-align="left">
Underwriting Dept. Head
</fo:block>

					<!--<fo:block >Dear: 
					<xsl:value-of select="Life_Name"/>
					</fo:block>
					<fo:table space-after.optimum="20pt" space-before.optimum="20pt" width="80%">
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-body >
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>Plan:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Contract_Type_Desc"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>Policy Number:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="ContrNo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>Life Insured: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select="Life_Name"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
					<fo:block space-after="10pt">This refers to your application for Fund Withdrawal of the above-numbered policy. </fo:block>
					<fo:block space-after="10pt">We would like to confirm that your request has been processed and the withdrawal took effect on <xsl:value-of select="Fund_Switch_Date"/>. Details are as follows:</fo:block>
					<fo:table width="100%" space-after.optimum="20pt">
						<fo:table-column column-width="proportional-column-width(35)"/>
						<fo:table-column column-width="proportional-column-width(35)"/>
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-body>
						<xsl:if test="SourceFundDetails/Source_Fund_Name != ''  or SourceFundDetails/Source_Fund_Amount != ''   or SourceFundDetails/Source_Fund_Unit != ''  ">
							<fo:table-row line-height="15pt" font-weight="bold" background-color="rgb(180,180,180)">
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>Withdrawal From</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>Amount(<xsl:value-of select="Source_Fund_Currency"/>)</fo:block>
								</fo:table-cell>
								<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
									<fo:block>Units</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:for-each select="SourceFundDetails">
							<xsl:if test="Source_Fund_Name != ''  or Source_Fund_Amount != ''   or Source_Fund_Unit != ''  ">
								<fo:table-row line-height="12pt">
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Source_Fund_Name"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Source_Fund_Amount"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-width="1pt" border-style="solid" padding="1pt">
										<fo:block>
											<xsl:value-of select="Source_Fund_Unit"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:if>	
							</xsl:for-each>
							</xsl:if>
						</fo:table-body>
					</fo:table>
					<fo:block space-after="10pt">Should you have any queries please call our Customer Services Hotline on <xsl:value-of select="Service_Hotline"/>.
					</fo:block>
					<fo:block space-after="10pt">Yours faithfully, </fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Name"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Manager_Title"/>
					</fo:block>
					<fo:block>
						<xsl:value-of select="Department_Name"/>
					</fo:block>
					<fo:block space-before="10pt">cc:  <xsl:value-of select="Agent_Number"/><xsl:text > </xsl:text>
						<xsl:value-of select="Agent_Name"/>
					</fo:block>-->
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
