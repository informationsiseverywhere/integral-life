package com.dxc.integral.life.beans;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="singleton")
public class RequestParams {
	
	private Integer accountingYear;
	private Integer accountingMonth;
	private String batchTranCode;
	private String batchName;
	private String batchBatch;
	private String batchCompany;
	private String batchBranch;
	private String billUsed;
	private String gstFlag;
	private String gfnFlag;
	private String invFlag;
	private String invProductType;
	private Integer jobNumber;
	private String language;
	private Integer pointer;
	private String policyCompany;
	private String policyNumber;
	private String policyBranch;
	private Integer policyEffdate;
	private String policyStatcode;
	private String subsidaryFlag;
	private Date submitTime;
	private String tbIndic;
	private String tranno;
	private String userName;
	private String ynFlag;
	private Integer user;
	public Integer getAccountingYear() {
		return accountingYear;
	}
	public void setAccountingYear(Integer accountingYear) {
		this.accountingYear = accountingYear;
	}
	public Integer getAccountingMonth() {
		return accountingMonth;
	}
	public void setAccountingMonth(Integer accountingMonth) {
		this.accountingMonth = accountingMonth;
	}
	public String getBatchTranCode() {
		return batchTranCode;
	}
	public void setBatchTranCode(String batchTranCode) {
		this.batchTranCode = batchTranCode;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getBatchBatch() {
		return batchBatch;
	}
	public void setBatchBatch(String batchBatch) {
		this.batchBatch = batchBatch;
	}
	public String getBatchCompany() {
		return batchCompany;
	}
	public void setBatchCompany(String batchCompany) {
		this.batchCompany = batchCompany;
	}
	public String getBatchBranch() {
		return batchBranch;
	}
	public void setBatchBranch(String batchBranch) {
		this.batchBranch = batchBranch;
	}
	public String getBillUsed() {
		return billUsed;
	}
	public void setBillUsed(String billUsed) {
		this.billUsed = billUsed;
	}
	public String getGstFlag() {
		return gstFlag;
	}
	public void setGstFlag(String gstFlag) {
		this.gstFlag = gstFlag;
	}
	public String getGfnFlag() {
		return gfnFlag;
	}
	public void setGfnFlag(String gfnFlag) {
		this.gfnFlag = gfnFlag;
	}
	public String getInvFlag() {
		return invFlag;
	}
	public void setInvFlag(String invFlag) {
		this.invFlag = invFlag;
	}
	public String getInvProductType() {
		return invProductType;
	}
	public void setInvProductType(String invProductType) {
		this.invProductType = invProductType;
	}
	public Integer getJobNumber() {
		return jobNumber;
	}
	public void setJobNumber(Integer jobNumber) {
		this.jobNumber = jobNumber;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Integer getPointer() {
		return pointer;
	}
	public void setPointer(Integer pointer) {
		this.pointer = pointer;
	}
	public String getPolicyCompany() {
		return policyCompany;
	}
	public void setPolicyCompany(String policyCompany) {
		this.policyCompany = policyCompany;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getPolicyBranch() {
		return policyBranch;
	}
	public void setPolicyBranch(String policyBranch) {
		this.policyBranch = policyBranch;
	}
	public Integer getPolicyEffdate() {
		return policyEffdate;
	}
	public void setPolicyEffdate(Integer policyEffdate) {
		this.policyEffdate = policyEffdate;
	}
	public String getPolicyStatcode() {
		return policyStatcode;
	}
	public void setPolicyStatcode(String policyStatcode) {
		this.policyStatcode = policyStatcode;
	}
	public String getSubsidaryFlag() {
		return subsidaryFlag;
	}
	public void setSubsidaryFlag(String subsidaryFlag) {
		this.subsidaryFlag = subsidaryFlag;
	}
	public Date getSubmitTime() {
		return submitTime;
	}
	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}
	public String getTbIndic() {
		return tbIndic;
	}
	public void setTbIndic(String tbIndic) {
		this.tbIndic = tbIndic;
	}
	public String getTranno() {
		return tranno;
	}
	public void setTranno(String tranno) {
		this.tranno = tranno;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Integer getUser() {
		return user;
	}
	public void setUser(Integer user) {
		this.user = user;
	}
	public String getYnFlag() {
		return ynFlag;
	}
	public void setYnFlag(String ynFlag) {
		this.ynFlag = ynFlag;
	}
}
