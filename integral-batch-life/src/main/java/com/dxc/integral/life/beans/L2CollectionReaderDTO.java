package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.dao.model.Clrrpf;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Bprdpf;
import com.dxc.integral.iaf.dao.model.Bsscpf;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.life.dao.model.Acagpf;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hdivpf;
import com.dxc.integral.life.dao.model.LifacmvPojo;
import com.dxc.integral.life.dao.model.LifrtrnPojo;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.dao.model.Trwppf;
import com.dxc.integral.life.dao.model.Zctnpf;
import com.dxc.integral.life.dao.model.Zptnpf;
import com.dxc.integral.life.smarttable.pojo.T5679;

/**
 * 
 * @author xma3
 *
 */
public class L2CollectionReaderDTO {

	private boolean isSkiped;

	public boolean isSkiped() {
		return isSkiped;
	}

	public void setSkiped(boolean isSkiped) {
		this.isSkiped = isSkiped;
	}

	private long chdrUniqueNumber;
	private int linsinstto;
	private RnlAllRec rnlallrec = new RnlAllRec();
	private LifrtrnDTO lifrtrnPojo = new LifrtrnDTO();

	public RnlAllRec getRnlallrec() {
		return rnlallrec;
	}

	public void setRnlallrec(RnlAllRec rnlallrec) {
		this.rnlallrec = rnlallrec;
	}

	public LifrtrnDTO getLifrtrnPojo() {
		return lifrtrnPojo;
	}

	public void setLifrtrnPojo(LifrtrnDTO lifrtrnPojo) {
		this.lifrtrnPojo = lifrtrnPojo;
	}

	public long getChdrUniqueNumber() {
		return chdrUniqueNumber;
	}

	public void setChdrUniqueNumber(long chdrUniqueNumber) {
		this.chdrUniqueNumber = chdrUniqueNumber;
	}

	public int getLinsinstto() {
		return linsinstto;
	}

	public void setLinsinstto(int linsinstto) {
		this.linsinstto = linsinstto;
	}

	private String wsaaLinxRunid;
	private int intBatchExtractSize;
	private String chdrcoy;
	private String chdrnum;
	private int instfrom;
	private String cntcurr;
	private String billcurr;
	private int payrseqno;
	private BigDecimal cbillamt;
	private int billcd;
	private String billchnl;
	private BigDecimal instamt01;
	private BigDecimal instamt02;
	private BigDecimal instamt03;
	private BigDecimal instamt04;
	private BigDecimal instamt05;
	private BigDecimal instamt06;
	private String instfreq;
	private String chdrcnttype;
	private int chdroccdate;
	private String chdrstatcode;
	private String chdrpstatcode;
	private int chdrtranno;
	private BigDecimal chdrinsttot01;
	private BigDecimal chdrinsttot02;
	private BigDecimal chdrinsttot03;
	private BigDecimal chdrinsttot04;
	private BigDecimal chdrinsttot05;
	private BigDecimal chdrinsttot06;
	private BigDecimal chdroutstamt;
	private String chdragntnum;
	private String chdrcntcurr;
	private int chdrptdate;
	private String chdrrnwlsupr;
	private int chdrrnwlspfrom;
	private int chdrrnwlspto;
	private String chdrcowncoy;
	private String chdrcownnum;
	private String chdrcntbranch;
	private String chdragntcoy;
	private String chdrregister;
	private String chdrchdrpfx;
	private String payrtaxrelmth;
	private int payrincomeseqno;
	private int payrtranno;
	private BigDecimal payroutstamt;
	private String payrbillchnl;
	private String payrmandref;
	private String payrcntcurr;
	private String payrbillfreq;
	private int payrbillcd;
	private int payrptdate;
	private int payrbtdate;
	private int payrnextdate;
	private int lsinstto;
	private long cunique_number;
	private long lsunique_number;
	private long psunique_number;
	private int aglfdteapp;
	private int aglfdteexp;
	private int agldtetrm;
	private String hcsdzcshdivmth;
	private String hcsdzdivopt;
	private int hdishcapndt;
    private long linsUniqueNumber;
    private long payrUniqueNumber;
	// insert fields
	private List<Ptrnpf> ptrnpfList;
	private Hdivpf hdivInsertList;
	private List<Zptnpf> zptnInsertList;
	private List<Acagpf> acagInsertList;
	private List<Zctnpf> zctnInsertList;
	private Trwppf trwppfInsertList;
	// update fields
	private Chdrpf chdrpf;
	private Payrpf payrpf;
	private List<Taxdpf> taxdUpdateList;
	private List<Agcmpf> agcmpfUpdateList;
	private List<Linspf> linsUpdateList;

	private Map<String, List<Taxdpf>> taxdMap = new HashMap<String, List<Taxdpf>>();
	private Map<String, List<Acblpf>> acblMap = new HashMap<String, List<Acblpf>>();
	private Map<String, List<Acblpf>> acblGLMap = new HashMap<String, List<Acblpf>>();
	private Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
	private Map<String, List<Agcmpf>> agcmMap = new HashMap<String, List<Agcmpf>>();
	private Map<String, List<Clrrpf>> clrrMap = new HashMap<String, List<Clrrpf>>();

	// process field
	private Map<String, String> wsaaT6654Collsub = new HashMap<>();
	private String wsaaPayrnum;
	private String wsaaPayrcoy;
	private BigDecimal wsaaToleranceAllowed = BigDecimal.ZERO;
	private BigDecimal wsaaWaiveAmtPaid = BigDecimal.ZERO;
	private BigDecimal wsaaNetCbillamt = BigDecimal.ZERO;
	private BigDecimal wsaaAmtPaid = BigDecimal.ZERO;
	private BigDecimal wsaaDivRequired = BigDecimal.ZERO;
	private BigDecimal wsaaCntcurrReceived = BigDecimal.ZERO;
	private BigDecimal wsaaApaRequired = BigDecimal.ZERO;
	private Integer[][] wsaaT5645Cnttot;
	private String[][] wsaaT5645Glmap;
	private String[][] wsaaT5645Sacscode;
	private String[][] wsaaT5645Sacstype;
	private String[][] wsaaT5645Sign;
	private Bsscpf bsscpf;
	private Batcpf batchDetail = null;
	private Bprdpf bprdpf;
	private Descpf descpfT1688;
	private String wsaaT5688Comlvlacc;
	private String wsaaT5688Revacc;
	private String wsaaToleranceChk;
	private boolean agtTerminated = false;
	private String th605recIndic;
	private PrasDTO prasDTO;
	private T5679 t5679;
	private Map<String, List<String>> wsaaT5671Subprogs = new HashMap<>();
	private Map<String, L2CollectionsProcessDTO.WsaaT5687Rec> wsaaT5687Rec = new HashMap<>();
	private Map<String, String> wsaaT5534UnitFreq = new HashMap<>();
	private Map<String, String> wsaaT5644Comsub = new HashMap<>();
	private BigDecimal temptot = BigDecimal.ZERO;
	
	public BigDecimal getTemptot() {
		return temptot;
	}

	public void setTemptot(BigDecimal temptot) {
		this.temptot = temptot;
	}

	public Map<String, String> getWsaaT5644Comsub() {
		return wsaaT5644Comsub;
	}

	public void setWsaaT5644Comsub(Map<String, String> wsaaT5644Comsub) {
		this.wsaaT5644Comsub = wsaaT5644Comsub;
	}

	public Map<String, String> getWsaaT5534UnitFreq() {
		return wsaaT5534UnitFreq;
	}

	public void setWsaaT5534UnitFreq(Map<String, String> wsaaT5534UnitFreq) {
		this.wsaaT5534UnitFreq = wsaaT5534UnitFreq;
	}

	public Map<String, L2CollectionsProcessDTO.WsaaT5687Rec> getWsaaT5687Rec() {
		return wsaaT5687Rec;
	}

	public void setWsaaT5687Rec(Map<String, L2CollectionsProcessDTO.WsaaT5687Rec> wsaaT5687Rec) {
		this.wsaaT5687Rec = wsaaT5687Rec;
	}

	public Map<String, List<String>> getWsaaT5671Subprogs() {
		return wsaaT5671Subprogs;
	}

	public void setWsaaT5671Subprogs(Map<String, List<String>> wsaaT5671Subprogs) {
		this.wsaaT5671Subprogs = wsaaT5671Subprogs;
	}

	public T5679 getT5679() {
		return t5679;
	}

	public void setT5679(T5679 t5679) {
		this.t5679 = t5679;
	}

	public PrasDTO getPrasDTO() {
		return prasDTO;
	}

	public void setPrasDTO(PrasDTO prasDTO) {
		this.prasDTO = prasDTO;
	}

	public String getTh605recIndic() {
		return th605recIndic;
	}

	public void setTh605recIndic(String th605recIndic) {
		this.th605recIndic = th605recIndic;
	}

	public boolean isAgtTerminated() {
		return agtTerminated;
	}

	public void setAgtTerminated(boolean agtTerminated) {
		this.agtTerminated = agtTerminated;
	}

	public String getWsaaToleranceChk() {
		return wsaaToleranceChk;
	}

	public void setWsaaToleranceChk(String wsaaToleranceChk) {
		this.wsaaToleranceChk = wsaaToleranceChk;
	}

	public String getWsaaT5688Comlvlacc() {
		return wsaaT5688Comlvlacc;
	}

	public void setWsaaT5688Comlvlacc(String wsaaT5688Comlvlacc) {
		this.wsaaT5688Comlvlacc = wsaaT5688Comlvlacc;
	}

	public String getWsaaT5688Revacc() {
		return wsaaT5688Revacc;
	}

	public void setWsaaT5688Revacc(String wsaaT5688Revacc) {
		this.wsaaT5688Revacc = wsaaT5688Revacc;
	}

	public Bprdpf getBprdpf() {
		return bprdpf;
	}

	public void setBprdpf(Bprdpf bprdpf) {
		this.bprdpf = bprdpf;
	}

	public Descpf getDescpfT1688() {
		return descpfT1688;
	}

	public void setDescpfT1688(Descpf descpfT1688) {
		this.descpfT1688 = descpfT1688;
	}

	public Bsscpf getBsscpf() {
		return bsscpf;
	}

	public void setBsscpf(Bsscpf bsscpf) {
		this.bsscpf = bsscpf;
	}

	public Batcpf getBatchDetail() {
		return batchDetail;
	}

	public void setBatchDetail(Batcpf batchDetail) {
		this.batchDetail = batchDetail;
	}

	public BigDecimal getWsaaApaRequired() {
		return wsaaApaRequired;
	}

	public void setWsaaApaRequired(BigDecimal wsaaApaRequired) {
		this.wsaaApaRequired = wsaaApaRequired;
	}

	public BigDecimal getWsaaCntcurrReceived() {
		return wsaaCntcurrReceived;
	}

	public void setWsaaCntcurrReceived(BigDecimal wsaaCntcurrReceived) {
		this.wsaaCntcurrReceived = wsaaCntcurrReceived;
	}

	public Integer[][] getWsaaT5645Cnttot() {
		return wsaaT5645Cnttot;
	}

	public void setWsaaT5645Cnttot(Integer[][] wsaaT5645Cnttot) {
		this.wsaaT5645Cnttot = wsaaT5645Cnttot;
	}

	public String[][] getWsaaT5645Glmap() {
		return wsaaT5645Glmap;
	}

	public void setWsaaT5645Glmap(String[][] wsaaT5645Glmap) {
		this.wsaaT5645Glmap = wsaaT5645Glmap;
	}

	public String[][] getWsaaT5645Sacscode() {
		return wsaaT5645Sacscode;
	}

	public void setWsaaT5645Sacscode(String[][] wsaaT5645Sacscode) {
		this.wsaaT5645Sacscode = wsaaT5645Sacscode;
	}

	public String[][] getWsaaT5645Sacstype() {
		return wsaaT5645Sacstype;
	}

	public void setWsaaT5645Sacstype(String[][] wsaaT5645Sacstype) {
		this.wsaaT5645Sacstype = wsaaT5645Sacstype;
	}

	public String[][] getWsaaT5645Sign() {
		return wsaaT5645Sign;
	}

	public void setWsaaT5645Sign(String[][] wsaaT5645Sign) {
		this.wsaaT5645Sign = wsaaT5645Sign;
	}

	public BigDecimal getWsaaDivRequired() {
		return wsaaDivRequired;
	}

	public void setWsaaDivRequired(BigDecimal wsaaDivRequired) {
		this.wsaaDivRequired = wsaaDivRequired;
	}

	public BigDecimal getWsaaWaiveAmtPaid() {
		return wsaaWaiveAmtPaid;
	}

	public void setWsaaWaiveAmtPaid(BigDecimal wsaaWaiveAmtPaid) {
		this.wsaaWaiveAmtPaid = wsaaWaiveAmtPaid;
	}

	public BigDecimal getWsaaNetCbillamt() {
		return wsaaNetCbillamt;
	}

	public void setWsaaNetCbillamt(BigDecimal wsaaNetCbillamt) {
		this.wsaaNetCbillamt = wsaaNetCbillamt;
	}

	public BigDecimal getWsaaAmtPaid() {
		return wsaaAmtPaid;
	}

	public void setWsaaAmtPaid(BigDecimal wsaaAmtPaid) {
		this.wsaaAmtPaid = wsaaAmtPaid;
	}

	public BigDecimal getWsaaToleranceAllowed() {
		return wsaaToleranceAllowed;
	}

	public void setWsaaToleranceAllowed(BigDecimal wsaaToleranceAllowed) {
		this.wsaaToleranceAllowed = wsaaToleranceAllowed;
	}

	public Map<String, String> getWsaaT6654Collsub() {
		return wsaaT6654Collsub;
	}

	public void setWsaaT6654Collsub(Map<String, String> wsaaT6654Collsub) {
		this.wsaaT6654Collsub = wsaaT6654Collsub;
	}

	public String getWsaaPayrnum() {
		return wsaaPayrnum;
	}

	public void setWsaaPayrnum(String wsaaPayrnum) {
		this.wsaaPayrnum = wsaaPayrnum;
	}

	public String getWsaaPayrcoy() {
		return wsaaPayrcoy;
	}

	public void setWsaaPayrcoy(String wsaaPayrcoy) {
		this.wsaaPayrcoy = wsaaPayrcoy;
	}

	public Map<String, List<Taxdpf>> getTaxdMap() {
		return taxdMap;
	}

	public void setTaxdMap(Map<String, List<Taxdpf>> taxdMap) {
		this.taxdMap = taxdMap;
	}

	public Map<String, List<Acblpf>> getAcblMap() {
		return acblMap;
	}

	public void setAcblMap(Map<String, List<Acblpf>> acblMap) {
		this.acblMap = acblMap;
	}

	public Map<String, List<Acblpf>> getAcblGLMap() {
		return acblGLMap;
	}

	public void setAcblGLMap(Map<String, List<Acblpf>> acblGLMap) {
		this.acblGLMap = acblGLMap;
	}

	public Map<String, List<Covrpf>> getCovrMap() {
		return covrMap;
	}

	public void setCovrMap(Map<String, List<Covrpf>> covrMap) {
		this.covrMap = covrMap;
	}

	public Map<String, List<Agcmpf>> getAgcmMap() {
		return agcmMap;
	}

	public void setAgcmMap(Map<String, List<Agcmpf>> agcmMap) {
		this.agcmMap = agcmMap;
	}

	public Map<String, List<Clrrpf>> getClrrMap() {
		return clrrMap;
	}

	public void setClrrMap(Map<String, List<Clrrpf>> clrrMap) {
		this.clrrMap = clrrMap;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public int getInstfrom() {
		return instfrom;
	}

	public void setInstfrom(int instfrom) {
		this.instfrom = instfrom;
	}

	public String getCntcurr() {
		return cntcurr;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	public String getBillcurr() {
		return billcurr;
	}

	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}

	public int getPayrseqno() {
		return payrseqno;
	}

	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}

	public BigDecimal getCbillamt() {
		return cbillamt;
	}

	public void setCbillamt(BigDecimal cbillamt) {
		this.cbillamt = cbillamt;
	}

	public int getBillcd() {
		return billcd;
	}

	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}

	public String getBillchnl() {
		return billchnl;
	}

	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}

	public BigDecimal getInstamt01() {
		return instamt01;
	}

	public void setInstamt01(BigDecimal instamt01) {
		this.instamt01 = instamt01;
	}

	public BigDecimal getInstamt02() {
		return instamt02;
	}

	public void setInstamt02(BigDecimal instamt02) {
		this.instamt02 = instamt02;
	}

	public BigDecimal getInstamt03() {
		return instamt03;
	}

	public void setInstamt03(BigDecimal instamt03) {
		this.instamt03 = instamt03;
	}

	public BigDecimal getInstamt04() {
		return instamt04;
	}

	public void setInstamt04(BigDecimal instamt04) {
		this.instamt04 = instamt04;
	}

	public BigDecimal getInstamt05() {
		return instamt05;
	}

	public void setInstamt05(BigDecimal instamt05) {
		this.instamt05 = instamt05;
	}

	public BigDecimal getInstamt06() {
		return instamt06;
	}

	public void setInstamt06(BigDecimal instamt06) {
		this.instamt06 = instamt06;
	}

	public String getInstfreq() {
		return instfreq;
	}

	public void setInstfreq(String instfreq) {
		this.instfreq = instfreq;
	}

	public String getChdrcnttype() {
		return chdrcnttype;
	}

	public void setChdrcnttype(String chdrcnttype) {
		this.chdrcnttype = chdrcnttype;
	}

	public int getChdroccdate() {
		return chdroccdate;
	}

	public void setChdroccdate(int chdroccdate) {
		this.chdroccdate = chdroccdate;
	}

	public String getChdrstatcode() {
		return chdrstatcode;
	}

	public void setChdrstatcode(String chdrstatcode) {
		this.chdrstatcode = chdrstatcode;
	}

	public String getChdrpstatcode() {
		return chdrpstatcode;
	}

	public void setChdrpstatcode(String chdrpstatcode) {
		this.chdrpstatcode = chdrpstatcode;
	}

	public int getChdrtranno() {
		return chdrtranno;
	}

	public void setChdrtranno(int chdrtranno) {
		this.chdrtranno = chdrtranno;
	}

	public BigDecimal getChdrinsttot01() {
		return chdrinsttot01;
	}

	public void setChdrinsttot01(BigDecimal chdrinsttot01) {
		this.chdrinsttot01 = chdrinsttot01;
	}

	public BigDecimal getChdrinsttot02() {
		return chdrinsttot02;
	}

	public void setChdrinsttot02(BigDecimal chdrinsttot02) {
		this.chdrinsttot02 = chdrinsttot02;
	}

	public BigDecimal getChdrinsttot03() {
		return chdrinsttot03;
	}

	public void setChdrinsttot03(BigDecimal chdrinsttot03) {
		this.chdrinsttot03 = chdrinsttot03;
	}

	public BigDecimal getChdrinsttot04() {
		return chdrinsttot04;
	}

	public void setChdrinsttot04(BigDecimal chdrinsttot04) {
		this.chdrinsttot04 = chdrinsttot04;
	}

	public BigDecimal getChdrinsttot05() {
		return chdrinsttot05;
	}

	public void setChdrinsttot05(BigDecimal chdrinsttot05) {
		this.chdrinsttot05 = chdrinsttot05;
	}

	public BigDecimal getChdrinsttot06() {
		return chdrinsttot06;
	}

	public void setChdrinsttot06(BigDecimal chdrinsttot06) {
		this.chdrinsttot06 = chdrinsttot06;
	}

	public BigDecimal getChdroutstamt() {
		return chdroutstamt;
	}

	public void setChdroutstamt(BigDecimal chdroutstamt) {
		this.chdroutstamt = chdroutstamt;
	}

	public String getChdragntnum() {
		return chdragntnum;
	}

	public void setChdragntnum(String chdragntnum) {
		this.chdragntnum = chdragntnum;
	}

	public String getChdrcntcurr() {
		return chdrcntcurr;
	}

	public void setChdrcntcurr(String chdrcntcurr) {
		this.chdrcntcurr = chdrcntcurr;
	}

	public int getChdrptdate() {
		return chdrptdate;
	}

	public void setChdrptdate(int chdrptdate) {
		this.chdrptdate = chdrptdate;
	}

	public String getChdrrnwlsupr() {
		return chdrrnwlsupr;
	}

	public void setChdrrnwlsupr(String chdrrnwlsupr) {
		this.chdrrnwlsupr = chdrrnwlsupr;
	}

	public int getChdrrnwlspfrom() {
		return chdrrnwlspfrom;
	}

	public void setChdrrnwlspfrom(int chdrrnwlspfrom) {
		this.chdrrnwlspfrom = chdrrnwlspfrom;
	}

	public int getChdrrnwlspto() {
		return chdrrnwlspto;
	}

	public void setChdrrnwlspto(int chdrrnwlspto) {
		this.chdrrnwlspto = chdrrnwlspto;
	}

	public String getChdrcowncoy() {
		return chdrcowncoy;
	}

	public void setChdrcowncoy(String chdrcowncoy) {
		this.chdrcowncoy = chdrcowncoy;
	}

	public String getChdrcownnum() {
		return chdrcownnum;
	}

	public void setChdrcownnum(String chdrcownnum) {
		this.chdrcownnum = chdrcownnum;
	}

	public String getChdrcntbranch() {
		return chdrcntbranch;
	}

	public void setChdrcntbranch(String chdrcntbranch) {
		this.chdrcntbranch = chdrcntbranch;
	}

	public String getChdragntcoy() {
		return chdragntcoy;
	}

	public void setChdragntcoy(String chdragntcoy) {
		this.chdragntcoy = chdragntcoy;
	}

	public String getChdrregister() {
		return chdrregister;
	}

	public void setChdrregister(String chdrregister) {
		this.chdrregister = chdrregister;
	}

	public String getChdrchdrpfx() {
		return chdrchdrpfx;
	}

	public void setChdrchdrpfx(String chdrchdrpfx) {
		this.chdrchdrpfx = chdrchdrpfx;
	}

	public String getPayrtaxrelmth() {
		return payrtaxrelmth;
	}

	public void setPayrtaxrelmth(String payrtaxrelmth) {
		this.payrtaxrelmth = payrtaxrelmth;
	}

	public int getPayrincomeseqno() {
		return payrincomeseqno;
	}

	public void setPayrincomeseqno(int payrincomeseqno) {
		this.payrincomeseqno = payrincomeseqno;
	}

	public int getPayrtranno() {
		return payrtranno;
	}

	public void setPayrtranno(int payrtranno) {
		this.payrtranno = payrtranno;
	}

	public BigDecimal getPayroutstamt() {
		return payroutstamt;
	}

	public void setPayroutstamt(BigDecimal payroutstamt) {
		this.payroutstamt = payroutstamt;
	}

	public String getPayrbillchnl() {
		return payrbillchnl;
	}

	public void setPayrbillchnl(String payrbillchnl) {
		this.payrbillchnl = payrbillchnl;
	}

	public String getPayrmandref() {
		return payrmandref;
	}

	public void setPayrmandref(String payrmandref) {
		this.payrmandref = payrmandref;
	}

	public String getPayrcntcurr() {
		return payrcntcurr;
	}

	public void setPayrcntcurr(String payrcntcurr) {
		this.payrcntcurr = payrcntcurr;
	}

	public String getPayrbillfreq() {
		return payrbillfreq;
	}

	public void setPayrbillfreq(String payrbillfreq) {
		this.payrbillfreq = payrbillfreq;
	}

	public int getPayrbillcd() {
		return payrbillcd;
	}

	public void setPayrbillcd(int payrbillcd) {
		this.payrbillcd = payrbillcd;
	}

	public int getPayrptdate() {
		return payrptdate;
	}

	public void setPayrptdate(int payrptdate) {
		this.payrptdate = payrptdate;
	}

	public int getPayrbtdate() {
		return payrbtdate;
	}

	public void setPayrbtdate(int payrbtdate) {
		this.payrbtdate = payrbtdate;
	}

	public int getPayrnextdate() {
		return payrnextdate;
	}

	public void setPayrnextdate(int payrnextdate) {
		this.payrnextdate = payrnextdate;
	}

	public int getLsinstto() {
		return lsinstto;
	}

	public void setLsinstto(int lsinstto) {
		this.lsinstto = lsinstto;
	}

	public long getCunique_number() {
		return cunique_number;
	}

	public void setCunique_number(long cunique_number) {
		this.cunique_number = cunique_number;
		this.chdrUniqueNumber = cunique_number;
	}

	public long getLsunique_number() {
		return lsunique_number;
	}

	public void setLsunique_number(long lsunique_number) {
		this.lsunique_number = lsunique_number;
	}

	public long getPsunique_number() {
		return psunique_number;
	}

	public void setPsunique_number(long psunique_number) {
		this.psunique_number = psunique_number;
	}

	public int getAglfdteapp() {
		return aglfdteapp;
	}

	public void setAglfdteapp(int aglfdteapp) {
		this.aglfdteapp = aglfdteapp;
	}

	public int getAglfdteexp() {
		return aglfdteexp;
	}

	public void setAglfdteexp(int aglfdteexp) {
		this.aglfdteexp = aglfdteexp;
	}

	public int getAgldtetrm() {
		return agldtetrm;
	}

	public void setAgldtetrm(int agldtetrm) {
		this.agldtetrm = agldtetrm;
	}

	public String getHcsdzcshdivmth() {
		return hcsdzcshdivmth;
	}

	public void setHcsdzcshdivmth(String hcsdzcshdivmth) {
		this.hcsdzcshdivmth = hcsdzcshdivmth;
	}

	public String getHcsdzdivopt() {
		return hcsdzdivopt;
	}

	public void setHcsdzdivopt(String hcsdzdivopt) {
		this.hcsdzdivopt = hcsdzdivopt;
	}

	public int getHdishcapndt() {
		return hdishcapndt;
	}

	public void setHdishcapndt(int hdishcapndt) {
		this.hdishcapndt = hdishcapndt;
	}

	public List<Ptrnpf> getPtrnpfList() {
		return ptrnpfList;
	}

	public void setPtrnpfList(List<Ptrnpf> ptrnpfList) {
		this.ptrnpfList = ptrnpfList;
	}

	public Hdivpf getHdivInsertList() {
		return hdivInsertList;
	}

	public void setHdivInsertList(Hdivpf hdivInsertList) {
		this.hdivInsertList = hdivInsertList;
	}

	public List<Zptnpf> getZptnInsertList() {
		return zptnInsertList;
	}

	public void setZptnInsertList(List<Zptnpf> zptnInsertList) {
		this.zptnInsertList = zptnInsertList;
	}

	public List<Acagpf> getAcagInsertList() {
		return acagInsertList;
	}

	public void setAcagInsertList(List<Acagpf> acagInsertList) {
		this.acagInsertList = acagInsertList;
	}

	public List<Zctnpf> getZctnInsertList() {
		return zctnInsertList;
	}

	public void setZctnInsertList(List<Zctnpf> zctnInsertList) {
		this.zctnInsertList = zctnInsertList;
	}

	public Trwppf getTrwppfInsertList() {
		return trwppfInsertList;
	}

	public void setTrwppfInsertList(Trwppf trwppfInsertList) {
		this.trwppfInsertList = trwppfInsertList;
	}

	public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}

	public List<Taxdpf> getTaxdUpdateList() {
		return taxdUpdateList;
	}

	public void setTaxdUpdateList(List<Taxdpf> taxdUpdateList) {
		this.taxdUpdateList = taxdUpdateList;
	}

	public List<Agcmpf> getAgcmpfUpdateList() {
		return agcmpfUpdateList;
	}

	public void setAgcmpfUpdateList(List<Agcmpf> agcmpfUpdateList) {
		this.agcmpfUpdateList = agcmpfUpdateList;
	}

	public List<Linspf> getLinsUpdateList() {
		return linsUpdateList;
	}

	public void setLinsUpdateList(List<Linspf> linsUpdateList) {
		this.linsUpdateList = linsUpdateList;
	}

	public Payrpf getPayrpf() {
		return payrpf;
	}

	public void setPayrpf(Payrpf payrpf) {
		this.payrpf = payrpf;
	}

	public String getWsaaLinxRunid() {
		return wsaaLinxRunid;
	}

	public void setWsaaLinxRunid(String wsaaLinxRunid) {
		this.wsaaLinxRunid = wsaaLinxRunid;
	}

	public int getIntBatchExtractSize() {
		return intBatchExtractSize;
	}

	public void setIntBatchExtractSize(int intBatchExtractSize) {
		this.intBatchExtractSize = intBatchExtractSize;
	}
	public long getLinsUniqueNumber() {
		return linsUniqueNumber;
	}
	public void setLinsUniqueNumber(long linsUniqueNumber) {
		this.linsUniqueNumber = linsUniqueNumber;
	}
	public long getPayrUniqueNumber() {
		return payrUniqueNumber;
	}
	public void setPayrUniqueNumber(long payrUniqueNumber) {
		this.payrUniqueNumber = payrUniqueNumber;
	}

}
