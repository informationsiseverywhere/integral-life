package com.dxc.integral.life.beans;

import com.dxc.integral.life.dao.model.Covrpf;


public class L2AnnyProcReaderDTO {
	
	private String chdrnum;
	private String chdrcoy;
	private String life;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String crtable;
	private int crrcd;
	private String statcode;
	private Covrpf covrUpdate;
	
	
	
	public Covrpf getCovrUpdate() {
		return covrUpdate;
	}
	public void setCovrUpdate(Covrpf covrUpdate) {
		this.covrUpdate = covrUpdate;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	

}
