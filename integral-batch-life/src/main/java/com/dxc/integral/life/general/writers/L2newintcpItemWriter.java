package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2newintcpReaderDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ItemWriter for L2NEWINTCP batch step.
 * 
 * @author dpuhawan
 *
 */
@Async
public class L2newintcpItemWriter implements ItemWriter<L2newintcpReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2newintcpItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;
	
	@Value("#{jobParameters['transactionDate']}")
	private String transactionDate;	

    private ItemWriter<L2newintcpReaderDTO> chdrpfUpdateWriter;
    private ItemWriter<L2newintcpReaderDTO> ptrnpfWriter;
    private ItemWriter<L2newintcpReaderDTO> bextpfInsertWriter;    
    private ItemWriter<L2newintcpReaderDTO> loanpfUpdateWriter; 
    private ItemWriter<L2newintcpReaderDTO> loanpfCapUpdateWriter;
    
    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
	@Override
	public void write(List<? extends L2newintcpReaderDTO> readerDTOList) throws Exception {
		
		LOGGER.info("L2newintcpItemWriter starts.");
		Integer tranDate = DatimeUtil.covertToyyyyMMddDate(transactionDate);
		
		List<L2newintcpReaderDTO> readerDTOModifiedList;
		readerDTOModifiedList = new ArrayList<L2newintcpReaderDTO>();
		for (int x = 0; x < readerDTOList.size(); x++) {
			L2newintcpReaderDTO readerDTOModified = null;
			if(readerDTOList.get(x).getLstintbdte().compareTo(tranDate) < 0) {
				readerDTOModified = readerDTOList.get(x);
				readerDTOModifiedList.add(readerDTOModified);
			}
		}
		if (!readerDTOModifiedList.isEmpty()) {
			chdrpfUpdateWriter.write(readerDTOModifiedList);
			loanpfUpdateWriter.write(readerDTOModifiedList);
			bextpfInsertWriter.write(readerDTOModifiedList);			
		}
		loanpfCapUpdateWriter.write(readerDTOList);
		ptrnpfWriter.write(readerDTOList); 
		
		if(!readerDTOList.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOList.size(); i++) {
				entityList.add(readerDTOList.get(i).getChdrnum());
			}			
			
			sftlock.unlockByList("CH", readerDTOList.get(0).getChdrcoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2newintcpItemWriter ends.");
	}

	public void setChdrpfUpdateWriter(ItemWriter<L2newintcpReaderDTO> chdrpfUpdateWriter) {
		this.chdrpfUpdateWriter = chdrpfUpdateWriter;
	}

	public void setPtrnpfWriter(ItemWriter<L2newintcpReaderDTO> ptrnpfWriter) {
		this.ptrnpfWriter = ptrnpfWriter;
	}

	public void setBextpfInsertWriter(ItemWriter<L2newintcpReaderDTO> bextpfInsertWriter) {
		this.bextpfInsertWriter = bextpfInsertWriter;
	}

	public void setLoanpfUpdateWriter(ItemWriter<L2newintcpReaderDTO> loanpfUpdateWriter) {
		this.loanpfUpdateWriter = loanpfUpdateWriter;
	}
	
	public void setLoanpfCapUpdateWriter(ItemWriter<L2newintcpReaderDTO> loanpfCapUpdateWriter) {
		this.loanpfCapUpdateWriter = loanpfCapUpdateWriter;
	}	


   
}
