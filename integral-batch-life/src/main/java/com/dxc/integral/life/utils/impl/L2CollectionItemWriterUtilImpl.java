package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.beans.L2CollectionControlTotalDTO;
import com.dxc.integral.life.beans.L2CollectionReaderDTO;
import com.dxc.integral.life.beans.L2CollectionsProcessDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.LifrtrnDTO;
import com.dxc.integral.life.beans.NlgcalcDTO;
import com.dxc.integral.life.beans.RlpdlonDTO;
import com.dxc.integral.life.beans.RnlallDTO;
import com.dxc.integral.life.beans.TxcalcDTO;
import com.dxc.integral.life.beans.ZorlnkDTO;
import com.dxc.integral.life.dao.NlgtpfDAO;
import com.dxc.integral.life.dao.model.Acagpf;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Nlgtpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.dao.model.Zctnpf;
import com.dxc.integral.life.dao.model.Zptnpf;
import com.dxc.integral.life.exception.L2CollectionsException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.utils.Cmpy001;
import com.dxc.integral.life.utils.Cmpy002;
import com.dxc.integral.life.utils.L2CollectionItemWriterUtil;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Lifrtrn;
import com.dxc.integral.life.utils.Nlgcalc;
import com.dxc.integral.life.utils.Rlpdlon;
import com.dxc.integral.life.utils.Servtax;
import com.dxc.integral.life.utils.Zorcompy;
import com.dxc.integral.life.utils.Zrulaloc;

@Service("l2CollectionItemWriterUtil")
public class L2CollectionItemWriterUtilImpl implements L2CollectionItemWriterUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CollectionItemWriterUtilImpl.class);
	
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private NlgtpfDAO nlgtpfDAO;
	@Autowired
	private Rlpdlon rlpdlon;
	@Autowired
	private Lifacmv lifacmvUtils;
	@Autowired
	private Zorcompy zorcompy;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Zrulaloc zrulaloc;
//	@Autowired
//	private Cmpy2rv cmpy2rv;
	@Autowired
	private Cmpy002 cmpy002;
	@Autowired
	private Cmpy001 cmpy001;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Nlgcalc nlgcalc;
	@Autowired
	private Servtax servtax;
	@Autowired
	private Lifrtrn lifrtrn;
    @Autowired
	private L2CollectionControlTotalDTO l2collectioncontroltotaldto;
    
    private static final String STATUS_OK = "****";
    private static final DecimalFormat df = new DecimalFormat("0000");

	private LifacmvDTO lifacmvPojo = new LifacmvDTO();
    private ZorlnkDTO zorlnkDTO = new ZorlnkDTO();
    private String wsaaCnttype2;
    private int wsaaGlSub;
    private int wsaaGlSub1;
//    private String wsaaTxCrtable;
//    private String wsaaTxLife;
//    private String wsaaTxCoverage;
//    private String wsaaTxRider;
//    private int wsaaTxPlansfx;
    private String wsaaLastChdrnum;
    private String wsaaLastLife;
    private String wsaaLastCoverage;
    private String wsaaLastRider;
    private int wsaaLastPlanSuff;
    private List<WsaaAgcmRec> wsaaAgcmRec = new ArrayList<>();
    private BigDecimal wsaaAgcmPremium = new BigDecimal(0);
    private List<WsaaCovrRec> wsaaCovrRecs = new ArrayList<>();
    private WsaaRldgacct wsaaRldgacct = new WsaaRldgacct();
    private WsaaRldgagnt wsaaRldgagnt = new WsaaRldgagnt();
    private String wsaaBillfq = "";
    private String wsaaPremType;
    private BigDecimal wsaaBascpyDue = new BigDecimal(0);
    private BigDecimal wsaaBascpyErn = new BigDecimal(0);
    private BigDecimal wsaaBascpyPay = new BigDecimal(0);
    private BigDecimal wsaaSrvcpyDue = new BigDecimal(0);
    private BigDecimal wsaaSrvcpyErn = new BigDecimal(0);
    private BigDecimal wsaaRnwcpyErn = new BigDecimal(0);
    private BigDecimal wsaaRnwcpyDue = new BigDecimal(0);
    private BigDecimal wsaaOvrdBascpyDue = new BigDecimal(0);
    private BigDecimal wsaaOvrdBascpyErn = new BigDecimal(0);
    private BigDecimal wsaaOvrdBascpyPay = new BigDecimal(0);
    private int tranDate;


	@Override
	public void process(List<L2CollectionReaderDTO> readerDTOList, int tranDate) {
		LOGGER.info("L2CollectionItemWriterUtilImpl Loop start.");
		this.tranDate = tranDate;
		for (L2CollectionReaderDTO item : readerDTOList) {
			// init
			lifacmvPojo.setBatccoy(item.getBatchDetail().getBatccoy());
			lifacmvPojo.setBatctrcde(item.getBatchDetail().getBatctrcde());
	        lifacmvPojo.setBatccoy(item.getBatchDetail().getBatccoy());
	        lifacmvPojo.setRldgcoy(item.getBatchDetail().getBatccoy());
	        lifacmvPojo.setGenlcoy(item.getBatchDetail().getBatccoy());
	        lifacmvPojo.setBatcactyr(item.getBatchDetail().getBatcactyr());
	        lifacmvPojo.setBatcactmn(item.getBatchDetail().getBatcactmn());
	        lifacmvPojo.setBatctrcde(item.getBatchDetail().getBatctrcde());
	        lifacmvPojo.setBatcbatch(item.getBatchDetail().getBatcbatch());
	        lifacmvPojo.setBatcbrn(item.getBatchDetail().getBatcbrn());
	        lifacmvPojo.setJrnseq(0);
	        lifacmvPojo.setUsrprofile(item.getBatchDetail().getUsrprf());
	        lifacmvPojo.setJobname(item.getBatchDetail().getJobnm());
			
			callPremiumCollect3200(item);
			updateSuspense3300(item);
			individualLedger3400(item);
			covrsAcmvsLinsPtrn3500(item);
		}
		LOGGER.info("L2CollectionItemWriterUtilImpl Loop end.");
	}
	private void callPremiumCollect3200(L2CollectionReaderDTO item) {
	    String wsaaBillchnl2 = item.getPayrbillchnl();
	    wsaaCnttype2 = item.getChdrcnttype();
	    /*  Look up the subroutine starting with the contract type then*/
	    /*  if not found, use '***'.  If neither case is found on T6654,*/
	    /*  then no record is written, and processing continues.*/
	    if (!item.getWsaaT6654Collsub().keySet().stream().anyMatch(t6654 -> t6654.equals(wsaaBillchnl2 + wsaaCnttype2))) {
	    	wsaaCnttype2 = "***";
	    	if (!item.getWsaaT6654Collsub().keySet().stream().anyMatch(t6654 -> t6654.equals(wsaaBillchnl2 + wsaaCnttype2))) {
	    		l2collectioncontroltotaldto.setControlTotal10(l2collectioncontroltotaldto.getControlTotal10().add(BigDecimal.ONE));
	    		return;
	    	}
	    }
	    if(StringUtils.isBlank(item.getWsaaT6654Collsub().get(wsaaBillchnl2 + wsaaCnttype2))) {
	    	return;
	    }

	    // overdue will not be implement
//	    ComlinkDTO comlinkrec = new ComlinkDTO();
//	    comlinkrec.setChdrcoy(item.getWsaaPayrcoy());
//	    comlinkrec.setChdrnum(item.getWsaaPayrnum());
//	    comlinkrec.setMethod(item.getPayrmandref());
//	    comlinkrec.setAnnprem(item.getCbillamt());
//	    ddbtallrec.ddbtRec.set(SPACES);
//	    ddbtallrec.statuz.set(varcom.oK);
//	    ddbtallrec.payrcoy.set(wsaaPayrcoy);
//	    ddbtallrec.payrnum.set(wsaaPayrnum);
//	    ddbtallrec.mandref.set(linxpfResult.getPayrMandref());
//	    ddbtallrec.billamt.set(linxpfResult.getCbillamt());
//	    ddbtallrec.lastUsedDate.set(bsscIO.getEffectiveDate());
//	    callProgram(wsaaT6654Collsub[wsaaT6654Ix.toInt()], ddbtallrec.ddbtRec);
//	    if (isNE(ddbtallrec.statuz, varcom.oK)) {
//	        wsysSysparams.set(ddbtallrec.ddbtRec);
//	        syserrrec.params.set(wsysSystemErrorParams);
//	        syserrrec.statuz.set(ddbtallrec.statuz);
//	        fatalError600();
//	    }
	}

	private void updateSuspense3300(L2CollectionReaderDTO item) {
		LifrtrnDTO lifrtrnPojo = null;
		if (item.getLifrtrnPojo() == null) {
			lifrtrnPojo = new LifrtrnDTO();
		} else {
			lifrtrnPojo = item.getLifrtrnPojo();
		}
		
	        if (BigDecimal.ZERO.compareTo(item.getWsaaToleranceAllowed()) == 0) {
	        	if (item.getWsaaWaiveAmtPaid().compareTo(item.getWsaaNetCbillamt()) >= 0) 
	        		lifrtrnPojo.setOrigamt(BigDecimal.ZERO);
	        	else
	        		lifrtrnPojo.setOrigamt(item.getWsaaNetCbillamt().subtract(item.getWsaaWaiveAmtPaid()));
	        }
	        else {
	        	lifrtrnPojo.setOrigamt(item.getWsaaAmtPaid());
	        }
	        if (lifrtrnPojo.getOrigamt().compareTo(BigDecimal.ZERO) == 0) {
	            return;
	        }
	        lifrtrnPojo.setRdocnum(item.getChdrnum());
	        lifrtrnPojo.setTranref(item.getChdrnum());
	        lifrtrnPojo.setBatccoy(Companies.LIFE);
	        lifrtrnPojo.setRldgcoy(Companies.LIFE);
	        lifrtrnPojo.setBatcactmn(item.getBatchDetail().getBatcactmn());
	        lifrtrnPojo.setBatcactyr(item.getBatchDetail().getBatcactyr());
	        lifrtrnPojo.setJrnseq(0);
	        lifrtrnPojo.setContot(item.getWsaaT5645Cnttot()[0][0]);
	        lifrtrnPojo.setSacscode(item.getWsaaT5645Sacscode()[0][0]);
	        lifrtrnPojo.setSacstyp(item.getWsaaT5645Sacstype()[0][0]);
	        lifrtrnPojo.setGlsign(item.getWsaaT5645Sign()[0][0]);
	        lifrtrnPojo.setGlcode(item.getWsaaT5645Glmap()[0][0]);
	        lifrtrnPojo.setTranno(item.getChdrtranno());
	        lifrtrnPojo.setOrigcurr(item.getBillcurr());
	        lifrtrnPojo.setEffdate(item.getInstfrom());
	        lifrtrnPojo.setBatctrcde(item.getBatchDetail().getBatctrcde());
	        lifrtrnPojo.setSubstituteCodes(setSubstituteCodes(
	        		item.getChdrcnttype(), 0, 5, lifrtrnPojo.getSubstituteCodes()));
	        lifrtrnPojo.setUsrprf(item.getBatchDetail().getUsrprf());
	        lifrtrnPojo.setJobnm(item.getBatchDetail().getJobnm());
	        lifrtrnPojo.setDatime(item.getBatchDetail().getDatime());
//	        if (lifrtrnPojo.getSubstituteCodes() != null) {
//	        	lifrtrnPojo.getSubstituteCodes()[0] = item.getChdrcnttype();
//	        } else {
//	        	String[] substituteCodes = new String[5];
//	            substituteCodes[0] = item.getChdrcnttype();
//	            lifrtrnPojo.setSubstituteCodes(substituteCodes);
//	        }
	        
	        lifrtrnPojo.setRldgacct(item.getChdrnum());
//	        lifrtrnUtils.calcLitrtrn(lifrtrnPojo);
	        lifrtrnPojo.setFunction("PSTW");
	        try {
				lifrtrn.calcLitrtrn(lifrtrnPojo);
			} catch (IOException e) {
				e.printStackTrace();
				throw new L2CollectionsException("Writer error for lifrtrn " + lifrtrnPojo);
			}
	        /* Pull Cash Dividend required.                                    */
	        if (BigDecimal.ZERO.compareTo(item.getWsaaDivRequired()) == 0) {
	        	advancePremDep3320(item);
	        }
	        lifrtrnPojo.setOrigamt(item.getWsaaDivRequired());
	        lifrtrnPojo.setRdocnum(item.getChdrnum());
	        lifrtrnPojo.setTranref(item.getChdrnum());
	        lifrtrnPojo.setJrnseq(0);
	        lifrtrnPojo.setBatccoy(Companies.LIFE);
	        lifrtrnPojo.setRldgcoy(Companies.LIFE);
	        lifrtrnPojo.setBatcactmn(item.getBatchDetail().getBatcactmn());
	        lifrtrnPojo.setBatcactyr(item.getBatchDetail().getBatcactyr());
	        lifrtrnPojo.setUsrprf(item.getBatchDetail().getUsrprf());
	        lifrtrnPojo.setJobnm(item.getBatchDetail().getJobnm());
	        lifrtrnPojo.setDatime(item.getBatchDetail().getDatime());
//	        lifrtrnPojo.setContot(wsaaT5645Cnttot[35].toInt());
//	        lifrtrnPojo.setSacscode(wsaaT5645Sacscode[35].toString());
//	        lifrtrnPojo.setSacstyp(wsaaT5645Sacstype[35].toString());
//	        lifrtrnPojo.setGlsign(wsaaT5645Sign[35].toString());
//	        lifrtrnPojo.setGlcode(wsaaT5645Glmap[35].toString());
	        lifrtrnPojo.setContot(item.getWsaaT5645Cnttot()[2][3]);
	        lifrtrnPojo.setSacscode(item.getWsaaT5645Sacscode()[2][3]);
	        lifrtrnPojo.setSacstyp(item.getWsaaT5645Sacstype()[2][3]);
	        lifrtrnPojo.setGlsign(item.getWsaaT5645Sign()[2][3]);
	        lifrtrnPojo.setGlcode(item.getWsaaT5645Glmap()[2][3]);
	        lifrtrnPojo.setTranno(item.getChdrtranno());
	        lifrtrnPojo.setOrigcurr(item.getBillcurr());
	        lifrtrnPojo.setEffdate(item.getInstfrom());
//	        lifrtrnPojo.setSubstituteCodes(substituteCodes);
	        lifrtrnPojo.setRldgacct(item.getChdrnum());
	        lifrtrnPojo.setFunction("PSTW");
	        try {
				lifrtrn.calcLitrtrn(lifrtrnPojo);
			} catch (IOException e) {
				e.printStackTrace();
				throw new L2CollectionsException("Writer error for lifrtrn " + lifrtrnPojo);
			}
	}

	private void advancePremDep3320(L2CollectionReaderDTO item) {
		/* Pull Advance premium deposit required. */
		a300UpdateApa(item);
		if (StringUtils.equals(item.getCntcurr(), item.getBillcurr())) {
			return;
		}
		/* Do the following if the accounting currency and the billing */
		/* currency are different, starting with the accounting currency. */
		if (BigDecimal.ZERO.compareTo(item.getWsaaToleranceAllowed()) == 0) {
			lifacmvPojo.setOrigamt(item.getInstamt06());
		} else {
			lifacmvPojo.setOrigamt(item.getWsaaCntcurrReceived());
		}
		lifacmvPojo.setRldgacct(item.getChdrnum());
		lifacmvPojo.setTranref(item.getChdrnum());
		lifacmvPojo.setEffdate(item.getInstfrom());
		lifacmvPojo.setSubstituteCodes(setSubstituteCodeList("", 5, 6, lifacmvPojo.getSubstituteCodes()));
//		if (lifacmvPojo.getSubstituteCodes() != null) {
//			lifacmvPojo.getSubstituteCodes()[5] = "";
//		} else {
//			String[] substituteCodes = new String[6];
//			substituteCodes[5] = "";
//			lifacmvPojo.setSubstituteCodes(substituteCodes);
//		}

		/* MOVE 0 TO LIFA-JRNSEQ. <005>< */
		if (BigDecimal.ZERO.compareTo(lifacmvPojo.getOrigamt()) != 0) {
//			wsaaGlSub = 2;
			wsaaGlSub = 1;
			wsaaGlSub1 = 0;
			lifacmvPojo.setOrigcurr(item.getCntcurr());
			callLifacmv6000(item);
		}
		/* ... then the billing currency, */
		if (BigDecimal.ZERO.compareTo(item.getWsaaToleranceAllowed()) == 0) {
			lifacmvPojo.setOrigamt(item.getCbillamt());
		} else {
			lifacmvPojo.setOrigamt(item.getWsaaAmtPaid());
		}
		if (BigDecimal.ZERO.compareTo(lifacmvPojo.getOrigamt()) == 0) {
//			wsaaGlSub = 15;
			wsaaGlSub = 14;
			wsaaGlSub1 = 0;
			lifacmvPojo.setOrigcurr(item.getBillcurr());
			callLifacmv6000(item);
		}
	}
	
	private void a300UpdateApa(L2CollectionReaderDTO item) {
		RlpdlonDTO rlpdlonDTO = new RlpdlonDTO();
		if (BigDecimal.ZERO.compareTo(item.getWsaaApaRequired()) == 0) {
			return;
		} else {
			if (BigDecimal.ZERO.compareTo(item.getWsaaApaRequired()) > 0) {
				rlpdlonDTO.setChdrcoy(item.getChdrcoy());
				rlpdlonDTO.setChdrnum(item.getChdrnum());
				rlpdlonDTO.setFunction("DELT");
				rlpdlonDTO.setPrmdepst(item.getWsaaApaRequired());
			} else {
				return;
			}
		}
		a302Params(rlpdlonDTO, item);
	}
	
	private void a302Params(RlpdlonDTO rlpdlonPojo, L2CollectionReaderDTO item) {
		rlpdlonPojo.setPstw("PSTW");
		rlpdlonPojo.setChdrcoy(item.getChdrcoy());
		rlpdlonPojo.setChdrnum(item.getChdrnum());
//		wsaaRdockey.rdocRdocpfx.set("CH");
//		wsaaRdockey.rdocRdoccoy.set(item.getChdrcoy());
//		wsaaRdockey.rdocRdocnum.set(item.getChdrnum());
//		wsaaRdockey.rdocTranseq.set(lifacmvPojo.getJrnseq());
		StringBuilder wsaaRdockey = new StringBuilder("CH");
		wsaaRdockey = wsaaRdockey.append(item.getChdrcoy()).append(item.getChdrnum()).append(lifacmvPojo.getJrnseq());
		rlpdlonPojo.setDoctkey(wsaaRdockey.toString());
		rlpdlonPojo.setEffdate(item.getInstfrom());
		rlpdlonPojo.setCurrency(item.getBillcurr());
		rlpdlonPojo.setTranno(item.getChdrtranno());
		rlpdlonPojo.setTranseq(lifacmvPojo.getJrnseq());
		rlpdlonPojo.setLongdesc(item.getDescpfT1688().getLongdesc());
		rlpdlonPojo.setLanguage(item.getBsscpf().getLanguage());//IJTI-1421
		rlpdlonPojo.setPrefix(item.getBatchDetail().getBatcpfx());
		rlpdlonPojo.setCompany(item.getBatchDetail().getBatccoy());
		rlpdlonPojo.setBranch(item.getBatchDetail().getBatcbrn());
		rlpdlonPojo.setActyear(item.getBatchDetail().getBatcactyr());
		rlpdlonPojo.setActmonth(item.getBatchDetail().getBatcactmn());
		rlpdlonPojo.setTrcde(item.getBatchDetail().getBatctrcde());
		rlpdlonPojo.setAuthCode(item.getBatchDetail().getBatctrcde());
		rlpdlonPojo.setBatch(item.getBatchDetail().getBatcbatch());
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		rlpdlonPojo.setTime(Integer.valueOf(sdf.format(new Date(System.currentTimeMillis()))));
		sdf = new SimpleDateFormat("yyyyMMdd");
		rlpdlonPojo.setDate_var(Integer.valueOf(sdf.format(new Date(System.currentTimeMillis()))));
		rlpdlonPojo.setUser(0);
		rlpdlonPojo.setTermid(item.getBatchDetail().getTermid());

		RlpdlonDTO resultRlpdlonDTO = null;
		try {
			resultRlpdlonDTO = rlpdlon.processRlpdlonInfo(rlpdlonPojo);
		} catch (IOException e) {
			throw new SubroutineIOException("B5353: IOException in rlpdlon: " + e.getMessage());
		}
		if (!STATUS_OK.equals(resultRlpdlonDTO.getStatuz())) {
			fatalError600(resultRlpdlonDTO.getStatuz());
		}
		lifacmvPojo.setJrnseq(resultRlpdlonDTO.getTranseq());
	}
	
	private void callLifacmv6000(L2CollectionReaderDTO item) {
		/* This is the only place LIFACMV parameters are referenced. LIFA */
		/* fields that are changeable are set outside this section within */
		/* a working storagwe variable. */
		lifacmvPojo.setRdocnum(item.getChdrnum());
		lifacmvPojo.setTranno(item.getChdrtranno());
		/* If the ACMV is being created for the COVR-INSTPREM, the */
		/* seventh table entries will be used. Move the appropriate */
		/* value for the COVR posting. */
		lifacmvPojo.setEffdate(item.getInstfrom());
		// lifacmvPojo.setEffdate(linxpfResult.getInstfrom());
		lifacmvPojo.setTermid(item.getBatchDetail().getTermid());
		lifacmvPojo.setRcamt(BigDecimal.ZERO);
		lifacmvPojo.setFrcdate(99999999);
		lifacmvPojo.setSuprflag("");
		lifacmvPojo.setUser(0);
		lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(item.getChdrcnttype(), 0, 6, lifacmvPojo.getSubstituteCodes()));
//		if (lifacmvPojo.getSubstituteCodes() != null) {
////			lifacmvPojo.getSubstituteCodes().[0] = item.getChdrCnttype();
//			lifacmvPojo.getSubstituteCodes().set(0, item.getChdrcnttype());
//		} else {
//			List<String> substituteCodes = new ArrayList<String>(6);
//			substituteCodes.add(item.getChdrcnttype());
////			substituteCodes[0] = item.getChdrcnttype();
//			lifacmvPojo.setSubstituteCodes(substituteCodes);
//		}

		lifacmvPojo.setTrandesc(item.getDescpfT1688().getLongdesc());
		lifacmvPojo.setJrnseq(lifacmvPojo.getJrnseq() + 1);
		lifacmvPojo.setSacscode(item.getWsaaT5645Sacscode()[wsaaGlSub1][wsaaGlSub]);
		lifacmvPojo.setSacstyp(item.getWsaaT5645Sacstype()[wsaaGlSub1][wsaaGlSub]);
		lifacmvPojo.setGlsign(item.getWsaaT5645Sign()[wsaaGlSub1][wsaaGlSub]);
		lifacmvPojo.setGlcode(item.getWsaaT5645Glmap()[wsaaGlSub1][wsaaGlSub]);
		lifacmvPojo.setContot(item.getWsaaT5645Cnttot()[wsaaGlSub1][wsaaGlSub]);
		
		/* Where the system parameter 3 on the process definition */
		/* is not spaces, LIFACMV will be called with the function */
		/* NPSTW instead of PSTW, so that the ACBL file will not */
		/* be updated by this program. */
		/* MOVE 'PSTW' TO LIFA-FUNCTION. */
		try {
			if (StringUtils.isBlank(item.getBprdpf().getBpsyspar03())) {

				lifacmvUtils.executePSTW(lifacmvPojo);

				// lifacmvPojo.setFunction("PSTW");
			} else {
				if (StringUtils.equals(item.getBprdpf().getBpsyspar03(), item.getWsaaT5645Sacscode()[wsaaGlSub1][wsaaGlSub])) {
					lifacmvUtils.executeNPSTW(lifacmvPojo);
					// lifacmvPojo.setFunction("NPSTW");
					deferredPostings6100(item);
				} else {
					lifacmvUtils.executePSTW(lifacmvPojo);
					// lifacmvPojo.setFunction("PSTW");
				}
			}
		} catch (IOException e) {
			fatalError600(e.getMessage());
			// e.printStackTrace();
		} catch (ParseException e) {
			fatalError600(e.getMessage());
		}
//		wsaaRldgacct.set(SPACES);
	}
	
	private void deferredPostings6100(L2CollectionReaderDTO item) {
		Acagpf acgapf = new Acagpf();
		acgapf.setRldgcoy(lifacmvPojo.getRldgcoy());
		acgapf.setSacscode(lifacmvPojo.getSacscode());
		acgapf.setRldgacct(lifacmvPojo.getRldgacct());
		acgapf.setOrigcurr(lifacmvPojo.getOrigcurr());
		acgapf.setSacstyp(lifacmvPojo.getSacstyp());
		acgapf.setSacscurbal(lifacmvPojo.getOrigamt());
		acgapf.setRdocnum(lifacmvPojo.getRdocnum());
		acgapf.setGlsign(lifacmvPojo.getGlsign());
		if (item.getAcagInsertList() == null) {
			item.setAcagInsertList(new ArrayList<Acagpf>());
		}
		item.getAcagInsertList().add(acgapf);
	}
	
	private void individualLedger3400(L2CollectionReaderDTO item) {
		item.setWsaaToleranceAllowed(BigDecimal.ZERO);
		// wsaaToleranceAllowed.set(ZERO);
		/* Post the Premium-due. */
		lifacmvPojo.setEffdate(item.getInstfrom());
		lifacmvPojo.setTranref(item.getChdrnum());
		lifacmvPojo.setRldgacct(item.getChdrnum());
		/* MOVE 0 TO LIFA-JRNSEQ. */
		lifacmvPojo.setOrigcurr(item.getPayrcntcurr());
		/* IF INSTAMT06 >= 0 */
		if (item.getInstamt06().compareTo(BigDecimal.ZERO) > 0 && StringUtils.equals(item.getWsaaT5688Revacc(), "Y")) {
			wsaaGlSub = 1;//32
			wsaaGlSub1 = 2;
			lifacmvPojo.setOrigamt(item.getInstamt06());
			callLifacmv6000(item);
		}
		/* Post the Premium-Income. */
		if (item.getInstamt01().compareTo(BigDecimal.ZERO) > 0
				&& (!StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y") && !StringUtils.equals(item.getWsaaT5688Revacc(), "Y"))) {
			lifacmvPojo.setOrigamt(item.getInstamt01());
//			wsaaGlSub = 3;
			wsaaGlSub = 2;
			wsaaGlSub1 = 0;
			callLifacmv6000(item);
		}
		/* Post the Fees. */
		if (item.getInstamt02().compareTo(BigDecimal.ZERO) > 0 && !StringUtils.equals(item.getWsaaT5688Revacc(), "Y")) {
			lifacmvPojo.setOrigamt(item.getInstamt02());
			wsaaGlSub = 3; //4
			wsaaGlSub1 = 0;
			callLifacmv6000(item);
		}
		/* Post the Tolerance. */
		/* IF INSTAMT03 > 0 */
		/* MOVE INSTAMT03 TO LIFA-ORIGAMT */
		/* MOVE 5 TO WSAA-GL-SUB */
		/* PERFORM 6000-CALL-LIFACMV */
		/* END-IF. */
		if (item.getInstamt03().compareTo(BigDecimal.ZERO) > 0) {
			lifacmvPojo.setOrigamt(item.getInstamt03());
			if ("toleranceLimit1".equals(item.getWsaaToleranceChk())) {
				wsaaGlSub = 4; //5
				wsaaGlSub1 = 0;
			}
			if ("toleranceLimit2".equals(item.getWsaaToleranceChk())) {
				if (item.isAgtTerminated()) {
					wsaaGlSub = 4; //5
					wsaaGlSub1 = 0;
				} else {
					wsaaGlSub = 5; //36
					wsaaGlSub1 = 2;
					lifacmvPojo.setRldgacct(item.getChdragntnum());
				}
			}
			callLifacmv6000(item);
			if (wsaaGlSub == 5 && wsaaGlSub1 == 2) { //36
				/* Override Commission */
				if ("Y".equals(item.getTh605recIndic())) {
					zorlnkDTO.setAnnprem(lifacmvPojo.getOrigamt());
					zorlnkDTO.setEffdate(item.getChdroccdate());
					b500CallZorcompy(item);
				}
				lifacmvPojo.setRldgacct(item.getChdrnum());
			}
		}
		/* Post the Stamp-Duty. */
		if (item.getInstamt04().compareTo(BigDecimal.ZERO) > 0 && !StringUtils.equals(item.getWsaaT5688Revacc(), "Y")) {
			lifacmvPojo.setOrigamt(item.getInstamt04());
			wsaaGlSub = 5; // 6
			wsaaGlSub1 = 0;
			callLifacmv6000(item);
		}
		/* Post Dividend Suspense utilized */
		if (item.getWsaaDivRequired().compareTo(BigDecimal.ONE) > 0) {
			lifacmvPojo.setOrigamt(item.getWsaaDivRequired());
			wsaaGlSub = 3; //34
			wsaaGlSub1 = 2;
			callLifacmv6000(item);
		}
		/* Post the Waiver-holding. */
		if (item.getInstamt05().compareTo(BigDecimal.ZERO) != 0 && !StringUtils.equals(item.getWsaaT5688Revacc(), "Y")) {
			lifacmvPojo.setOrigamt(item.getWsaaWaiveAmtPaid());
			wsaaGlSub = 6; // 7
			wsaaGlSub1 = 0;
			callLifacmv6000(item);
		}
		if (item.getPrasDTO() != null && BigDecimal.ZERO.compareTo(item.getPrasDTO().getTaxrelamt()) != 0) {
			lifacmvPojo.setOrigcurr(item.getBillcurr());
			lifacmvPojo.setOrigamt(item.getPrasDTO().getTaxrelamt());
			/* MOVE PRAS-INREVNUM TO LIFA-RLDGACCT */
			lifacmvPojo.setRldgacct(item.getChdrnum());
			wsaaGlSub = 3; //19
			wsaaGlSub1 = 1;
			callLifacmv6000(item);
		}
		/* Post the calculated taxes */
//		wsaaTxLife.set(SPACES);
//		wsaaTxCoverage.set(SPACES);
//		wsaaTxRider.set(SPACES);
//		/* MOVE SPACES TO WSAA-TX-PLANSFX <S19FIX> */
//		wsaaTxPlansfx.set(ZERO);
//		wsaaTxCrtable.set(SPACES);
//		wsaaTxLife = "";
//		wsaaTxCoverage = "";
//		wsaaTxRider = "";
//		wsaaTxPlansfx = 0;
//		wsaaTxCrtable = "";
		b700PostTax(item, "", "", "", 0, "");

	}
	
	private void b500CallZorcompy(L2CollectionReaderDTO item) {
		zorlnkDTO.setFunction("");
        zorlnkDTO.setClawback("");
        zorlnkDTO.setAgent(lifacmvPojo.getRldgacct());
        zorlnkDTO.setChdrcoy(lifacmvPojo.getRldgcoy());
        zorlnkDTO.setChdrnum(lifacmvPojo.getRdocnum());
        zorlnkDTO.setCrtable(lifacmvPojo.getSubstituteCodes().get(5));
        zorlnkDTO.setPtdate(item.getChdrptdate());
        zorlnkDTO.setOrigcurr(lifacmvPojo.getOrigcurr());
        zorlnkDTO.setCrate(lifacmvPojo.getCrate());
        zorlnkDTO.setOrigamt(lifacmvPojo.getOrigamt());
        zorlnkDTO.setTranno(item.getChdrtranno());
        zorlnkDTO.setTrandesc(lifacmvPojo.getTrandesc());
        zorlnkDTO.setTranref(lifacmvPojo.getTranref());
        zorlnkDTO.setGenlcur(lifacmvPojo.getGenlcur());
        zorlnkDTO.setSacstyp(lifacmvPojo.getSacstyp());
        zorlnkDTO.setTermid(lifacmvPojo.getTermid());
        zorlnkDTO.setCnttype(lifacmvPojo.getSubstituteCodes().get(0));
        zorlnkDTO.setBatchKey(lifacmvPojo.getBatckey());
        try {
			zorlnkDTO = zorcompy.processZorcompy(zorlnkDTO);
		} catch (Exception e) {
			e.printStackTrace();
			fatalError600(e.getMessage());
		}
	}
	
	private void b700PostTax(L2CollectionReaderDTO item, String wsaaTxLife, String wsaaTxCoverage, String wsaaTxRider,
			int wsaaTxPlansfx, String wsaaTxCrtable) {
		if (item.getTaxdMap().containsKey(item.getChdrnum())) {
			List<Taxdpf> taxdpfList = item.getTaxdMap().get(item.getChdrnum());
			for (Taxdpf taxdpf : taxdpfList) {
				if (taxdpf.getInstfrom() == item.getInstfrom() && StringUtils.equals(taxdpf.getLife(), wsaaTxLife)
						&& StringUtils.equals(taxdpf.getCoverage(), wsaaTxCoverage)
						&& StringUtils.equals(taxdpf.getRider(), wsaaTxRider)
						&& taxdpf.getPlansfx() == wsaaTxPlansfx) {
					if (taxdpf.getPostflg() != null && !taxdpf.getPostflg().trim().equals("")) {
						continue;
					} else {
//						List<Itempf> tr52eItems = tr52eMap.get(subString(taxdpf.getTranref(), 1, 8));
//						for (Itempf item : tr52eItems) {
//							if (item.getItmfrm().compareTo(new BigDecimal(taxdpf.getEffdate())) == 0) {
//								tr52erec.tr52eRec.set(StringUtil.rawToString(item.getGenarea()));
//								break;
//							}
//						}
						b710Start(taxdpf, item, wsaaTxLife, wsaaTxCoverage, wsaaTxRider, wsaaTxPlansfx, wsaaTxCrtable);
					}
				}
			}
		}
	}

	private void b710Start(Taxdpf taxdpf, L2CollectionReaderDTO item, String wsaaTxLife, String wsaaTxCoverage, String wsaaTxRider,
			int wsaaTxPlansfx, String wsaaTxCrtable) {

		TxcalcDTO txcalcDTO = new TxcalcDTO();
		/* Call tax subroutine */
		txcalcDTO.setFunction("POST");
		txcalcDTO.setStatuz("OK");
		txcalcDTO.setChdrcoy(taxdpf.getChdrcoy());
		txcalcDTO.setChdrnum(taxdpf.getChdrnum());
		txcalcDTO.setLife(taxdpf.getLife());
		txcalcDTO.setCoverage(taxdpf.getCoverage());
		txcalcDTO.setRider(taxdpf.getRider());
		txcalcDTO.setPlanSuffix(taxdpf.getPlansfx());
		txcalcDTO.setCrtable(wsaaTxCrtable);
		if (StringUtils.isBlank(wsaaTxCrtable)) {
			txcalcDTO.setCntTaxInd("Y");
		}
		txcalcDTO.setCnttype(item.getChdrcnttype());
		txcalcDTO.setRegister(item.getChdrregister());
		txcalcDTO.setTaxrule(StringUtils.substring(taxdpf.getTranref(), 0, 8));
		txcalcDTO.setRateItem(StringUtils.substring(taxdpf.getTranref(), 8, 16));
		txcalcDTO.setAmountIn(taxdpf.getBaseamt());
		txcalcDTO.setEffdate(taxdpf.getEffdate());
		txcalcDTO.setTransType(taxdpf.getTrantype());
		txcalcDTO.setTaxType01(taxdpf.getTxtype01());
		txcalcDTO.setTaxType02(taxdpf.getTxtype02());
		txcalcDTO.setTaxAmt01(taxdpf.getTaxamt01());
		txcalcDTO.setTaxAmt02(taxdpf.getTaxamt02());
		txcalcDTO.setTaxAbsorb01(taxdpf.getTxabsind01());
		txcalcDTO.setTaxAbsorb02(taxdpf.getTxabsind02());
//		txcalcDTO.setBatckey(item.getBatchDetail().getBatbatcdorrec.batchkey);
		txcalcDTO.setCcy(item.getChdrcntcurr());
		txcalcDTO.setLanguage(item.getBsscpf().getLanguage());
		txcalcDTO.setJrnseq(lifacmvPojo.getJrnseq());
		txcalcDTO.setTranno(item.getChdrtranno());
		try {
			txcalcDTO = servtax.processServtax(txcalcDTO);
			lifacmvPojo.setJrnseq(txcalcDTO.getJrnseq());
		} catch (IOException e) {
			e.printStackTrace();
			this.fatalError600("Servtax error " + e.getMessage());
		}
//		callProgram(tr52drec.txsubr, txcalcDTO.linkRec);

		
		Taxdpf t = new Taxdpf();
		t.setUnique_number(taxdpf.getUnique_number());
		t.setPostflg("P");
		if (item.getTaxdUpdateList() == null) {
			item.setTaxdUpdateList(new ArrayList<Taxdpf>());
		}
		item.getTaxdUpdateList().add(t);
	}
	
	private void covrsAcmvsLinsPtrn3500(L2CollectionReaderDTO item) {
		if ("Y".equals(item.getTh605recIndic())) {
			/* Initialize the AGCM arrays */
			wsaaAgcmRec = new ArrayList<>();
		}

		/* Create a posting for every COVR-INSTPREMs for the contract. */
//		wsaaAgcmIx.set(0);
//		wsaaCovrIx.set(1);
		wsaaLastChdrnum = "";
		wsaaLastLife = "";
		wsaaLastCoverage = "";
		wsaaLastRider = "";
		wsaaLastPlanSuff = 0;

		/* VARYING WSAA-COVR-IX */
		/* FROM 1 */
		/* BY 1 */
		if (!processCovrs3510(item)) {
			/* Log the no. of payrseqno errors */
			l2collectioncontroltotaldto.setControlTotal10(l2collectioncontroltotaldto.getControlTotal10().add(BigDecimal.ONE));
			return;
		}
		/* MOVE 0 TO LIFA-JRNSEQ. */
		wsaaRldgacct = new WsaaRldgacct();

		List<Agcmpf> agcmList = item.getAgcmMap().get(item.getChdrnum());
//		wsaaCompayKept.set(ZERO);
//		wsaaAgentKept.set(ZERO);
		for (Agcmpf a : agcmList) {
			if (a.getPtdate() == 0) {
				continue;
			}
			readAgcm3530(a, item);
		}

		writePtrn3600(item);

		updateLinsPayr3700(item);
	}

	protected void updateLinsPayr3700(L2CollectionReaderDTO item) {
		Linspf l = new Linspf();
		l.setPayflag("P");
		l.setUnique_number(item.getLinsUniqueNumber());
		if (item.getLinsUpdateList() == null) {
			item.setLinsUpdateList(new ArrayList<Linspf>());
		}
		item.getLinsUpdateList().add(l);
		l2collectioncontroltotaldto.setControlTotal02(l2collectioncontroltotaldto.getControlTotal02().add(BigDecimal.ONE));
		item.setTemptot(item.getInstamt06());
		l2collectioncontroltotaldto.setControlTotal02(l2collectioncontroltotaldto.getControlTotal02().add(item.getTemptot()));

		Payrpf payrpf = new Payrpf();
		payrpf.setTranno(item.getChdrtranno());// ILIFE-4688
		payrpf.setPtdate(item.getLsinstto());
		payrpf.setBtdate(item.getPayrbtdate());
		payrpf.setOutstamt(item.getPayroutstamt().subtract(item.getInstamt06()));
		payrpf.setPayrseqno(item.getPayrseqno());
		payrpf.setUniqueNumber(item.getPayrUniqueNumber());
		payrpf.setChdrcoy(item.getChdrcoy());
		payrpf.setChdrnum(item.getChdrnum());
		payrpf.setBillcd(item.getPayrbillcd());
		payrpf.setNextdate(item.getPayrnextdate());
		payrpf.setSinstamt06(item.getInstamt01().add(item.getInstamt02())
				.add(item.getInstamt03()).add(item.getInstamt04()).add(item.getInstamt05()));
		payrpf.setJobName(item.getBatchDetail().getJobnm());
		payrpf.setUserProfile(item.getBatchDetail().getUsrprf());
		item.setPayrpf(payrpf);
	}
	
	private void writePtrn3600(L2CollectionReaderDTO item) {
		Ptrnpf p = getPtrnpf(item);
		p.setTranno(item.getChdrtranno());
		p.setValidflag("1");
		p.setChdrpfx("CH");
		p.setRecode("");
		p.setTermid(item.getBatchDetail().getTermid());
		p.setTrdt(DatimeUtil.DateToInt(item.getBsscpf().getBprceffdat()));
		p.setUserT(0);
		p.setDatesub(this.tranDate);
		p.setCrtuser("");
		p.setUsrprf(item.getBatchDetail().getUsrprf());
		p.setJobnm(item.getBatchDetail().getJobnm());
		p.setChdrcoy(item.getChdrcoy());
		p.setChdrnum(item.getChdrnum());
		p.setBatcpfx(item.getBatchDetail().getBatcpfx());
		p.setPtrneff(item.getInstfrom());
//		p.setDatesub(DateToIntegerInt(item.getBsscpf().getBprceffdat()));
		if (item.getPtrnpfList() == null) {
			item.setPtrnpfList(new ArrayList<Ptrnpf>());
		}
		item.getPtrnpfList().add(p);
		// ILIFE-3997-STARTS
		Chdrpf chdrpf = null;
		chdrpf = chdrpfDAO.readRecordOfChdrpf(p.getChdrcoy(), p.getChdrnum());//IJTI-1421
		if (chdrpf != null && chdrpf.getNlgflg() != null && "Y".equals(chdrpf.getNlgflg()))
			writeNLGRec(p, chdrpf, item);
		// ILIFE-3997-ENDS
	}

	private Ptrnpf getPtrnpf(L2CollectionReaderDTO item) {
		Ptrnpf p = new Ptrnpf();
		p.setUserT(0);
		p.setPtrneff(DatimeUtil.DateToInt(item.getBsscpf().getBprceffdat()));
		p.setTrdt(DatimeUtil.DateToInt(new Date(System.currentTimeMillis())));
		p.setBatccoy(item.getBatchDetail().getBatccoy());
		p.setBatcactyr(item.getBatchDetail().getBatcactyr());
		p.setBatcactmn(item.getBatchDetail().getBatcactmn());
		p.setBatctrcde(item.getBatchDetail().getBatctrcde());
		p.setBatcbatch(item.getBatchDetail().getBatcbatch());
		p.setBatcbrn(item.getBatchDetail().getBatcbrn());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		String t = formatter.format((new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalTime()));
		p.setTrtm(Integer.valueOf(t));
		p.setValidflag("");
		return p;
	}
	
	private void writeNLGRec(Ptrnpf ptrnIO, Chdrpf chdr, L2CollectionReaderDTO item) {

		Iterator<Nlgtpf> iteratorListNlg;
		List<Nlgtpf> nlgtpfList = nlgtpfDAO.readNlgtpf(ptrnIO.getChdrcoy(), ptrnIO.getChdrnum());//IJTI-1421
		if (!(nlgtpfList.isEmpty())) {
			iteratorListNlg = nlgtpfList.iterator();
			while (iteratorListNlg.hasNext()) {
				Nlgtpf nlgtpf = new Nlgtpf();
				nlgtpf = iteratorListNlg.next();
				if ((nlgtpf.getBatctrcde().equals("B521") && nlgtpf.getEffdate() == ptrnIO.getPtrneff()
						&& nlgtpf.getAmnt04().doubleValue() != 0)
						|| (nlgtpf.getBatctrcde().equals("B673") && nlgtpf.getEffdate() == ptrnIO.getPtrneff()
								&& nlgtpf.getAmnt03().doubleValue() != 0)) {

					NlgcalcDTO nlgcalcPojo = new NlgcalcDTO();
					nlgcalcPojo.setChdrcoy(ptrnIO.getChdrcoy());
					nlgcalcPojo.setChdrnum(ptrnIO.getChdrnum());
					nlgcalcPojo.setEffdate(ptrnIO.getPtrneff());
					nlgcalcPojo.setTranno(ptrnIO.getTranno());
					nlgcalcPojo.setBatcactyr(ptrnIO.getBatcactyr());
					nlgcalcPojo.setBatcactmn(ptrnIO.getBatcactmn());
					nlgcalcPojo.setBatctrcde(ptrnIO.getBatctrcde());
					nlgcalcPojo.setBatctrcde(ptrnIO.getBatctrcde());
					nlgcalcPojo.setBillfreq(chdr.getBillfreq());
					nlgcalcPojo.setCnttype(chdr.getCnttype());
					nlgcalcPojo.setLanguage(item.getBsscpf().getLanguage());
					nlgcalcPojo.setFrmdate(chdr.getOccdate());
					nlgcalcPojo.setTodate(99999999);
					nlgcalcPojo.setOccdate(chdr.getOccdate());
					nlgcalcPojo.setPtdate(chdr.getPtdate());
					nlgcalcPojo.setInputAmt(BigDecimal.ZERO);
					nlgcalcPojo.setUnpaidPrem(BigDecimal.ZERO);
					nlgcalcPojo.setOvduePrem(BigDecimal.ZERO);
					nlgcalcPojo.setFunction("COLCT");
					try {
						nlgcalcPojo = nlgcalc.processNlgcalOvdue(nlgcalcPojo);
					} catch (IOException e) {
						e.printStackTrace();
						fatalError600("Nlgcalc error " + e.getMessage());
					} catch (ParseException e) {
						e.printStackTrace();
						fatalError600("Nlgcalc error " + e.getMessage());
					}

					break;
				}
			}
		}
	}
	
	private void readAgcm3530(Agcmpf a, L2CollectionReaderDTO item) {
		/* Do not process single premium AGCMs (PTDATE = zeroes). */
		if (readcovrlnb(a, item)) {
			return;
		}
		/* Look up the COVRs for this AGCM from the array and the CRTABLE */
		/* for the commission processing */
		int wsaaCovrIx = 0;
		boolean found = false;
		for (WsaaCovrRec wsaaCovrRec : wsaaCovrRecs) {
			if (StringUtils.equals(a.getChdrnum(), wsaaCovrRec.wsaaCovrChdrnum)
					&& StringUtils.equals(a.getLife(), wsaaCovrRec.wsaaCovrLife)
					&& StringUtils.equals(a.getCoverage(), wsaaCovrRec.wsaaCovrCovrg)
					&& StringUtils.equals(a.getRider(), wsaaCovrRec.wsaaCovrRider)
					&& a.getPlanSuffix() == wsaaCovrRec.wsaaCovrPlanSuff) {
				found = true;
				break;
			}
		}
		if (!found) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(a.getChdrnum());
			stringVariable1.append(a.getLife());
			stringVariable1.append(a.getCoverage());
			stringVariable1.append(a.getRider());
			fatalError600("e351: " + stringVariable1.toString());
		}
		/* Do not process AGCMs attached to components of invalid */
		/* status. */
		if (!StringUtils.equals(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrValid, "Y")) {
			return;
		}
		/* PERFORM 3540-UPDATE-AGCM */
		updateAgcm3540(a, wsaaCovrIx, item);
	}

	// ILIFE-896, if status is 'LA' skip that record
	private boolean readcovrlnb(Agcmpf a, L2CollectionReaderDTO item) {
		List<Covrpf> covrList = item.getCovrMap().get(a.getChdrnum());
		boolean foundFlag = false;
		for (Covrpf c : covrList) {
			if (c.getLife().equals(a.getLife()) && c.getCoverage().equals(a.getCoverage())
					&& c.getRider().equals(a.getRider()) && c.getPlanSuffix().intValue() == a.getPlanSuffix().intValue()) {
				foundFlag = true;
				if ("LA".equals(c.getStatcode())) {
					return true;
				}
			}
		}
		if (!foundFlag) {
			fatalError600("Covrpf:" + a.getChdrnum());
		}
		return false;
	}

	private void updateAgcm3540(Agcmpf a, int wsaaCovrIx, L2CollectionReaderDTO item) {
		ComlinkDTO comlinkrec = new ComlinkDTO();
		
		/* Call the three types of commission routine from AGCM */
		for (int wsaaCommethNo = 1; !(wsaaCommethNo > 3); wsaaCommethNo++) {
			comlinkrec.setBillfreq(item.getPayrbillfreq());
			comlinkrec.setInstprem(a.getAnnprem().divide(new BigDecimal(item.getPayrbillfreq())).setScale(3));
			comlinkrec.setChdrnum(item.getChdrnum());
			comlinkrec.setChdrcoy(item.getChdrcoy());
			comlinkrec.setLife(a.getLife());
			comlinkrec.setAgent(a.getAgntnum());
			comlinkrec.setCoverage(a.getCoverage());
			comlinkrec.setAnnprem(a.getAnnprem());
			comlinkrec.setRider(a.getRider());
			comlinkrec.setPlanSuffix(a.getPlanSuffix());
			comlinkrec.setCrtable(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable);
			comlinkrec.setAgentClass(a.getAgcls());
			comlinkrec.setIcommtot(a.getInitcom());
			comlinkrec.setIcommpd(a.getCompay());
			comlinkrec.setIcommernd(a.getComern());
			comlinkrec.setEffdate(a.getEfdate());
			comlinkrec.setPtdate(item.getLinsinstto());
			comlinkrec.setLanguage(item.getBsscpf().getLanguage());
			comlinkrec.setCurrto(0);
			comlinkrec.setTargetPrem(BigDecimal.ZERO);
			setUpCommMethod3550(a, item, wsaaCommethNo, comlinkrec);
		}
		/* Write an ACMV for each CHDRNUM/AGNTNUM/COVR/COMMISSION AMOUNT */
		postCommission3570(a, item, wsaaCovrIx, comlinkrec);
		a.setPtdate(item.getLinsinstto());
		/* MOVE REWRT TO AGCMRNL-FUNCTION. */
		if (item.getAgcmpfUpdateList() == null) {
			item.setAgcmpfUpdateList(new ArrayList<Agcmpf>());
		}
		item.getAgcmpfUpdateList() .add(a);
	}
	
	private void postCommission3570(Agcmpf a, L2CollectionReaderDTO item, int wsaaCovrIx, ComlinkDTO comlinkrec) {
		/* Initialise the ACMV area and move the contract type to its */
		/* relevant field (same for all calls to LIFACMV). */
		lifacmvPojo.setRldgacct("");
		lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(item.getChdrcnttype(), 0, 6, lifacmvPojo.getSubstituteCodes()));
//		if (lifacmvPojo.getSubstituteCodes() != null) {
//			lifacmvPojo.getSubstituteCodes().set(0, item.getChdrcnttype());
//		} else {
//			String[] substituteCodes = new String[6];
//			substituteCodes[0] = item.getChdrcnttype();
//			lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//		}

		wsaaRldgagnt.wsaaAgntChdrnum = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrChdrnum;
		wsaaRldgagnt.wsaaAgntLife = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrLife;
		wsaaRldgagnt.wsaaAgntCoverage = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCovrg;
		wsaaRldgagnt.wsaaAgntRider = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrRider;
		
		wsaaRldgagnt.wsaaAgntPlanSuffix = (df.format(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrPlanSuff).substring(2, 4));
		postInitialCommission7000(a, item, wsaaCovrIx, comlinkrec);
		postServiceCommission8000(a, item, wsaaCovrIx, comlinkrec);
		postRenewalCommission9000(a, item, wsaaCovrIx, comlinkrec);
		postOverridCommission10000(a, item, wsaaCovrIx, comlinkrec);
		wsaaBascpyDue = BigDecimal.ZERO;
		wsaaBascpyErn = BigDecimal.ZERO;
		wsaaBascpyPay = BigDecimal.ZERO;
		wsaaSrvcpyDue = BigDecimal.ZERO;
		wsaaSrvcpyErn = BigDecimal.ZERO;
		wsaaRnwcpyDue = BigDecimal.ZERO;
		wsaaRnwcpyErn = BigDecimal.ZERO;
		wsaaOvrdBascpyPay = BigDecimal.ZERO;
		wsaaOvrdBascpyDue = BigDecimal.ZERO;
		wsaaOvrdBascpyErn = BigDecimal.ZERO;
	}
	
	private void postInitialCommission7000(Agcmpf a, L2CollectionReaderDTO item, int wsaaCovrIx, ComlinkDTO comlinkrec) {
		/* Initial commission due */
		if (BigDecimal.ZERO.compareTo(wsaaBascpyDue) != 0) {
			/* ADD +1 TO LIFA-JRNSEQ */
			wsaaGlSub = 7; // 8
			wsaaGlSub1 = 0;
			wsaaRldgacct.wsaaRldgChdrnum=(a.getChdrnum());
			wsaaRldgacct.wsaaRldgLife=(a.getLife());
			wsaaRldgacct.wsaaRldgCoverage=(a.getCoverage());
			wsaaRldgacct.wsaaRldgRider=(a.getRider());
			wsaaRldgacct.wsaaRldgPlanSuff=(df.format(a.getPlanSuffix()).substring(2, 4));
			
			lifacmvPojo.setTranref(wsaaRldgacct.toString());
			lifacmvPojo.setOrigamt(wsaaBascpyDue);
			lifacmvPojo.setRldgacct(a.getAgntnum());
			lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(), 5, 6, lifacmvPojo.getSubstituteCodes()));
//			if (lifacmvPojo.getSubstituteCodes() != null) {
//				lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//			} else {
//				String[] substituteCodes = new String[6];
//				substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//				lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//			}

			callLifacmv6000(item);
			/* Override Commission */
			if ("Y".equals(item.getTh605recIndic())) {
				/* MOVE AGCMRNL-ANNPREM TO ZORL-ANNPREM <LA4973> */
				/* MOVE AGCMRNL-EFDATE TO ZORL-EFFDATE <LA4973> */
				zorlnkDTO.setAnnprem(comlinkrec.getInstprem());
				zorlnkDTO.setEffdate(item.getInstfrom());
				b500CallZorcompy(item);
			}
		}
		/* Initial commission earned */
		if (BigDecimal.ZERO.compareTo(wsaaBascpyErn) != 0) {
			/* ADD +1 TO LIFA-JRNSEQ */
			lifacmvPojo.setOrigamt(wsaaBascpyErn);
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				wsaaGlSub = 7; // 23
				wsaaGlSub1 = 1;
				lifacmvPojo.setRldgacct(wsaaRldgagnt.wsaaAgntChdrnum);
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(), 5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			} else {
				wsaaGlSub = 8; // 9
				wsaaGlSub1 = 0;
				lifacmvPojo.setRldgacct(item.getChdrnum());
			}
			lifacmvPojo.setTranref(a.getAgntnum());
			callLifacmv6000(item);
		}
		/* Initial commission paid */
		if (BigDecimal.ZERO.compareTo(wsaaBascpyPay) != 0) {
			lifacmvPojo.setOrigamt(wsaaBascpyPay);
			/* ADD +1 TO LIFA-JRNSEQ */
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				wsaaGlSub = 8; // 24
				wsaaGlSub1 = 1;
				lifacmvPojo.setRldgacct(wsaaRldgagnt.wsaaAgntChdrnum);
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(), 5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			} else {
				wsaaGlSub = 9; //10
				wsaaGlSub1 = 0;
				lifacmvPojo.setRldgacct(item.getChdrnum());
			}
			lifacmvPojo.setTranref(a.getAgntnum());
			callLifacmv6000(item);
		}

	}
	
	private void postServiceCommission8000(Agcmpf a, L2CollectionReaderDTO item, int wsaaCovrIx, ComlinkDTO comlinkrec) {
		/* Service commission due */
		if (BigDecimal.ZERO.compareTo(wsaaSrvcpyDue) != 0) {
			/* ADD +1 TO LIFA-JRNSEQ */
			lifacmvPojo.setOrigamt(wsaaSrvcpyDue);
			/* MOVE 9 TO WSAA-GL-SUB */
			wsaaGlSub = 10; // 11
			wsaaGlSub1 = 0;
			lifacmvPojo.setSubstituteCodes(setSubstituteCodeList("", 5, 6, lifacmvPojo.getSubstituteCodes()));
//			if (lifacmvPojo.getSubstituteCodes() != null) {
//				lifacmvPojo.getSubstituteCodes().set(5, "");
//			} else {
//				String[] substituteCodes = new String[6];
//				substituteCodes[5] = "";
//				lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//			}

			lifacmvPojo.setRldgacct(a.getAgntnum());
			lifacmvPojo.setTranref(item.getChdrnum());
			callLifacmv6000(item);
			/* Override Commission */
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				/* MOVE AGCMRNL-ANNPREM TO ZORL-ANNPREM <LA4973> */
				/* MOVE AGCMRNL-EFDATE TO ZORL-EFFDATE <LA4973> */
				zorlnkDTO.setAnnprem(comlinkrec.getInstprem());
				zorlnkDTO.setEffdate(item.getInstfrom());
				b500CallZorcompy(item);
			}
		}
		/* Service commission earned */
		if (BigDecimal.ZERO.compareTo(wsaaSrvcpyErn) != 0) {
			/* ADD +1 TO LIFA-JRNSEQ */
			lifacmvPojo.setOrigamt(wsaaSrvcpyErn);
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				wsaaGlSub = 9; // 25
				wsaaGlSub1 = 1;
				lifacmvPojo.setRldgacct(wsaaRldgagnt.wsaaAgntChdrnum);
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(), 5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			} else {
				wsaaGlSub = 11; //12
				wsaaGlSub1 = 0;
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList("", 5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, "");
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = "";
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

				lifacmvPojo.setRldgacct(item.getChdrnum());
			}
			lifacmvPojo.setTranref(a.getAgntnum());
			callLifacmv6000(item);
		}
	}
	
	private void postRenewalCommission9000(Agcmpf a, L2CollectionReaderDTO item, int wsaaCovrIx, ComlinkDTO comlinkrec) {
		/* Renewal commission due */
		if (BigDecimal.ZERO.compareTo(wsaaRnwcpyDue) != 0) {
			/* ADD +1 TO LIFA-JRNSEQ */
			wsaaGlSub = 12; //13
			wsaaGlSub1 = 0;
			lifacmvPojo.setOrigamt(wsaaRnwcpyDue);
			lifacmvPojo.setRldgacct(a.getAgntnum());
			/* MOVE CHDRNUM TO LIFA-TRANREF */
			
			wsaaRldgacct.wsaaRldgChdrnum=(a.getChdrnum());
			wsaaRldgacct.wsaaRldgLife=(a.getLife());
			wsaaRldgacct.wsaaRldgCoverage=(a.getCoverage());
			wsaaRldgacct.wsaaRldgRider=(a.getRider());
			wsaaRldgacct.wsaaRldgPlanSuff=(df.format(a.getPlanSuffix()).substring(2, 4));
			lifacmvPojo.setTranref(wsaaRldgacct.toString());
			lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(), 5, 6, lifacmvPojo.getSubstituteCodes()));
//			if (lifacmvPojo.getSubstituteCodes() != null) {
//				lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//			} else {
//				String[] substituteCodes = new String[6];
//				substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//				lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//			}

			callLifacmv6000(item);
			/* Override Commission */
			if ("Y".equals(item.getTh605recIndic())) {
				/* MOVE AGCMRNL-ANNPREM TO ZORL-ANNPREM <LA4973> */
				/* MOVE AGCMRNL-EFDATE TO ZORL-EFFDATE <LA4973> */
				zorlnkDTO.setAnnprem(comlinkrec.getInstprem());
				zorlnkDTO.setEffdate(item.getInstfrom());
				b500CallZorcompy(item);
			}
		}
		/* Renewal commission earned */
		if (BigDecimal.ZERO.compareTo(wsaaRnwcpyErn) != 0) {
			lifacmvPojo.setOrigamt(wsaaRnwcpyErn);
			/* ADD +1 TO LIFA-JRNSEQ */
			
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				wsaaGlSub = 10; //26
				wsaaGlSub1 = 1;
				lifacmvPojo.setRldgacct(wsaaRldgagnt.wsaaAgntChdrnum);
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(),
						5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			} else {
				wsaaGlSub = 13; //14
				wsaaGlSub1 = 0;
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList("",
						5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, "");
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = "";
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}
				lifacmvPojo.setRldgacct(item.getChdrnum());
			}
			lifacmvPojo.setTranref(a.getAgntnum());
			callLifacmv6000(item);
		}

	}

	private void postOverridCommission10000(Agcmpf a, L2CollectionReaderDTO item, int wsaaCovrIx, ComlinkDTO comlinkrec) {
		/* Initial over-riding commission due */
		if (BigDecimal.ZERO.compareTo(wsaaOvrdBascpyDue) != 0) {
			/* ADD 1 TO LIFA-JRNSEQ */
			wsaaGlSub = 0; //16
			wsaaGlSub1 = 1;
			lifacmvPojo.setOrigamt(new BigDecimal(wsaaOvrdBascpyDue.toString()));
			lifacmvPojo.setRldgacct(a.getAgntnum());
			lifacmvPojo.setTranref(a.getCedagent());
			lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(),
					5, 6, lifacmvPojo.getSubstituteCodes()));
//			if (lifacmvPojo.getSubstituteCodes() != null) {
//				lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//			} else {
//				String[] substituteCodes = new String[6];
//				substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//				lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//			}

			callLifacmv6000(item);
		}
		/* Initial over-riding commission earned */
		if (BigDecimal.ZERO.compareTo(wsaaOvrdBascpyErn) != 0) {
			lifacmvPojo.setOrigamt(new BigDecimal(wsaaOvrdBascpyErn.toString()));
			/* ADD +1 TO LIFA-JRNSEQ */
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				wsaaGlSub = 11; //27
				wsaaGlSub1 = 1;
				lifacmvPojo.setTranref(a.getAgntnum());
				lifacmvPojo.setRldgacct(wsaaRldgagnt.toString());
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(),
						5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			} else {
				wsaaGlSub = 1; //17
				wsaaGlSub1 = 1;
				lifacmvPojo.setTranref(a.getAgntnum());
				lifacmvPojo.setRldgacct(item.getChdrnum());
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList("",
						5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, "");
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = "";
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			}
			callLifacmv6000(item);
		}
		/* Initial over-riding commission paid */
		if (BigDecimal.ZERO.compareTo(wsaaOvrdBascpyPay) != 0) {
			/* ADD +1 TO LIFA-JRNSEQ */
			lifacmvPojo.setOrigamt(new BigDecimal(wsaaOvrdBascpyPay.toString()));
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
				wsaaGlSub = 12; //28
				wsaaGlSub1 = 1;
				lifacmvPojo.setTranref(a.getAgntnum());
				/* MOVE WSAA-RLDGACCT TO LIFA-RLDGACCT */
				lifacmvPojo.setRldgacct(wsaaRldgagnt.toString());
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim(),
						5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = wsaaCovrRecs.get(wsaaCovrIx).wsaaCovrCrtable.trim();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

			} else {
				wsaaGlSub = 2; //18
				wsaaGlSub1 = 1;
				lifacmvPojo.setTranref(a.getAgntnum());
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList("",
						5, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(5, "");
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = "";
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}
				lifacmvPojo.setRldgacct(item.getChdrnum());
			}
			callLifacmv6000(item);
		}
	}
	
	private void setUpCommMethod3550(Agcmpf a, L2CollectionReaderDTO item, int wsaaCommethNo, ComlinkDTO comlinkrec) {
		if (wsaaCommethNo == 1) {
			comlinkrec.setMethod(a.getBascpy());
		} else if (wsaaCommethNo == 2) {
			comlinkrec.setMethod(a.getSrvcpy());
		} else if (wsaaCommethNo == 3) {
			comlinkrec.setMethod(a.getRnwcpy());
		}
		if (!item.getWsaaT5644Comsub().containsKey(comlinkrec.getMethod())) {
			return;
		}
		/* Check initial commission for BASCPY method only. */
		if (wsaaCommethNo == 1) {
			if (a.getCompay().compareTo(a.getInitcom()) != 0 || a.getComern().compareTo(a.getInitcom()) != 0) {
				callCommRoutine3560(a, item, comlinkrec, wsaaCommethNo);
			}
		} else {
			if (wsaaCommethNo == 3 && StringUtils.equals(item.getChdrrnwlsupr(), "Y")
					&& (item.getPayrptdate() >= item.getChdrrnwlspfrom())
					&& (item.getPayrptdate()<= item.getChdrrnwlspto())) {
				/* NEXT_SENTENCE */
			} else {
				callCommRoutine3560(a, item, comlinkrec, wsaaCommethNo);
			}
		}

	}

	private void callCommRoutine3560(Agcmpf a, L2CollectionReaderDTO item, ComlinkDTO comlinkrec, int wsaaCommethNo) {
		
		comlinkrec.setPayamnt(BigDecimal.ZERO);
		comlinkrec.setErndamt(BigDecimal.ZERO);
		/* MOVE 0 TO CLNK-ERNDAMT. */
		/* Read ZRAP to get ZRORCODE, this one is valid only for Overide */
		/* Agent */
		/* PERFORM A100-READ-ZRAP */
		/* Call the subroutine relating to the CLNK-METHOD */
		try {
			if (wsaaCommethNo == 1) {
				comlinkrec = cmpy001.processCmpy001(comlinkrec);
			} else {
				comlinkrec = cmpy002.processCmpy002(comlinkrec);
			}
			if (StringUtils.equals("BOMB", comlinkrec.getStatuz())) {
				String errorP = "cmpy002";
				if (wsaaCommethNo == 1) {
					errorP = "cmpy001";
				}
				fatalError600(errorP + " error: BOMB");
			}
		} catch (Exception e) {
			e.printStackTrace();
			String errorP = "cmpy002";
			if (wsaaCommethNo == 1) {
				errorP = "cmpy001";
			}
			fatalError600(errorP + " error: " + e.getMessage());
		}
		
//		callProgram(wsaaT5644Comsub[wsaaT5644Ix.toInt()], comlinkrec.clnkallRec);
		/* MOVE CLNK-PAYAMNT TO ZRDP-AMOUNT-IN. */
		/* MOVE CNTCURR TO ZRDP-CURRENCY. */
		/* PERFORM C000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO CLNK-PAYAMNT. */
		if (BigDecimal.ZERO.compareTo(comlinkrec.getPayamnt()) != 0) {
			// ILB-441 starts
			// zrdecplrec.amountIn.set(comlinkrec.payamnt);
			ZrdecplcDTO zrdecplcPojo = new ZrdecplcDTO();
			zrdecplcPojo.setAmountIn(comlinkrec.getPayamnt());
			zrdecplcPojo.setCurrency(item.getCntcurr());
			/*
			 * zrdecplrec.amountIn.set(comlinkrec.payamnt);
			 * zrdecplrec.currency.set(linxpfResult.getCntcurr());
			 */
			Optional<BigDecimal> amoutOut = c000CallRounding(zrdecplcPojo, item);
			if (amoutOut.isPresent()) {
				comlinkrec.setPayamnt(amoutOut.get());
			}
		}
		/* MOVE CLNK-ERNDAMT TO ZRDP-AMOUNT-IN. */
		/* MOVE CNTCURR TO ZRDP-CURRENCY. */
		/* PERFORM C000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO CLNK-ERNDAMT. */
		if (BigDecimal.ZERO.compareTo(comlinkrec.getErndamt()) != 0) {
			ZrdecplcDTO zrdecplcPojo = new ZrdecplcDTO();
			zrdecplcPojo.setAmountIn(comlinkrec.getErndamt());
			zrdecplcPojo.setCurrency(item.getCntcurr());
			/*
			 * zrdecplrec.amountIn.set(comlinkrec.erndamt);
			 * zrdecplrec.currency.set(linxpfResult.getCntcurr());
			 */
			Optional<BigDecimal> amoutOut = c000CallRounding(zrdecplcPojo, item);
			if (amoutOut.isPresent()) {
				comlinkrec.setErndamt(amoutOut.get());
			}
		}
		/* Basic Commission */
		if ((wsaaCommethNo == 1)) {
			if (StringUtils.equals(a.getOvrdcat(), "O")) {
				wsaaOvrdBascpyDue = new BigDecimal(comlinkrec.getPayamnt().toString());
				wsaaOvrdBascpyErn= new BigDecimal(comlinkrec.getErndamt().toString());
				wsaaOvrdBascpyPay = wsaaOvrdBascpyPay.add(comlinkrec.getPayamnt().subtract(comlinkrec.getErndamt())).setScale(2);
//				compute(wsaaOvrdBascpyPay, 2)
//						.set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			} else {
				wsaaBascpyDue = new BigDecimal(comlinkrec.getPayamnt().toString());
				wsaaBascpyErn= new BigDecimal(comlinkrec.getErndamt().toString());
				wsaaBascpyPay = wsaaBascpyPay.add(comlinkrec.getPayamnt().subtract(comlinkrec.getErndamt())).setScale(2);
//				compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
				/***** IF WSAA-COMPAY-KEPT = ZEROES */
				/***** MOVE AGCMRNL-AGNTNUM TO WSAA-AGENT-KEPT */
				/***** MOVE CLNK-PAYAMNT TO WSAA-COMPAY-KEPT */
				/***** END-IF */
			}
			a.setCompay(a.getCompay().add(comlinkrec.getPayamnt()));
			a.setComern(a.getComern().add(comlinkrec.getErndamt()));
		}
		/* Service Commission */
		if ((wsaaCommethNo == 2)) {
			wsaaSrvcpyDue = new BigDecimal(comlinkrec.getPayamnt().toString());
			wsaaSrvcpyErn= new BigDecimal(comlinkrec.getErndamt().toString());
			a.setScmearn(a.getScmearn().add(comlinkrec.getErndamt()));
			a.setScmdue(a.getScmdue().add(comlinkrec.getPayamnt()));
		}
		/* Renewal Commission */
		if ((wsaaCommethNo == 3)) {
			wsaaRnwcpyDue = new BigDecimal(comlinkrec.getPayamnt().toString());
			wsaaRnwcpyErn= new BigDecimal(comlinkrec.getErndamt().toString());
			a.setRnlcdue(a.getRnlcdue().add(comlinkrec.getPayamnt()));
			a.setRnlcearn(a.getRnlcearn().add(comlinkrec.getErndamt()));
		}
		/* Bonus Workbench * */
		if (!"Y".equals(item.getTh605recIndic())) {
			return;
		}
		/* Skip overriding commission */
		if (!StringUtils.equals(a.getOvrdcat(), "B")) {
			return;
		}
		if ((wsaaCommethNo == 1)) {
			wsaaPremType = "I";
		} else if ((wsaaCommethNo == 3)) {
			wsaaPremType = "R";
		} else {
			return;
		}
		if (BigDecimal.ZERO.compareTo(comlinkrec.getPayamnt()) != 0) {

			Zctnpf zctnpf = new Zctnpf();
			zctnpf.setAgntcoy(a.getChdrcoy());
			zctnpf.setAgntnum(a.getAgntnum());
			zctnpf.setCommAmt(comlinkrec.getPayamnt());
			wsaaAgcmPremium = b400LocatePremium(a);
			zctnpf.setPremium(wsaaAgcmPremium.divide(new BigDecimal(wsaaBillfq).setScale(3)));
			if (BigDecimal.ZERO.compareTo(zctnpf.getPremium()) != 0) {
				
				ZrdecplcDTO zrdecplcPojo = new ZrdecplcDTO();
				zrdecplcPojo.setAmountIn(zctnpf.getPremium());
				zrdecplcPojo.setCurrency(item.getCntcurr());
				/*
				 * zrdecplrec.amountIn.set(comlinkrec.erndamt);
				 * zrdecplrec.currency.set(linxpfResult.getCntcurr());
				 */
				Optional<BigDecimal> amoutOut = c000CallRounding(zrdecplcPojo, item);
				if (amoutOut.isPresent()) {
					zctnpf.setPremium(amoutOut.get());
				}
			}
			zctnpf.setSplitBcomm(a.getAnnprem().multiply(BigDecimal.valueOf(100)).divide(wsaaAgcmPremium).setScale(3));
			if (BigDecimal.ZERO.compareTo(zctnpf.getSplitBcomm()) != 0) {
				
				ZrdecplcDTO zrdecplcPojo = new ZrdecplcDTO();
				
				zrdecplcPojo.setAmountIn(zctnpf.getSplitBcomm());
				zrdecplcPojo.setCurrency(item.getCntcurr());
				/*
				 * zrdecplrec.amountIn.set(zctnpf.getSplitBcomm());
				 * zrdecplrec.currency.set(linxpfResult.getCntcurr());
				 */
				Optional<BigDecimal> amoutOut = c000CallRounding(zrdecplcPojo, item);
				if (amoutOut.isPresent()) {
					zctnpf.setSplitBcomm(amoutOut.get());
				}
			}
			zctnpf.setZprflg(wsaaPremType);
			zctnpf.setChdrcoy(comlinkrec.getChdrcoy());
			zctnpf.setChdrnum(comlinkrec.getChdrnum());
			zctnpf.setLife(comlinkrec.getLife());
			zctnpf.setCoverage(comlinkrec.getCoverage());
			zctnpf.setRider(comlinkrec.getRider());
			zctnpf.setTranno(item.getChdrtranno());
			zctnpf.setTransCode(item.getBprdpf().getBauthcode());
			zctnpf.setEffdate(item.getInstfrom());
			zctnpf.setTrandate(DatimeUtil.DateToInt(item.getBsscpf().getBprceffdat()));

			if (item.getZctnInsertList() == null) {
				item.setZctnInsertList(new ArrayList<Zctnpf>());
			}
			item.getZctnInsertList().add(zctnpf);
		}
	}
	
	private BigDecimal b400LocatePremium(Agcmpf a) {
		for (WsaaAgcmRec rec : wsaaAgcmRec) {
			if (StringUtils.equals(a.getChdrcoy(), rec.wsaaAgcmChdrcoy)
					&& StringUtils.equals(a.getChdrnum(), rec.wsaaAgcmChdrnum)
					&& StringUtils.equals(a.getLife(), rec.wsaaAgcmLife)
					&& StringUtils.equals(a.getCoverage(), rec.wsaaAgcmCoverage)
					&& StringUtils.equals(a.getRider(), rec.wsaaAgcmRider)) {
				for (int wsaaAgcmIb = 0; wsaaAgcmIb < rec.wsaaAgcmSeqno.size(); wsaaAgcmIb++) {
					if (a.getSeqno() == rec.wsaaAgcmSeqno.get(wsaaAgcmIb)) {
						return rec.wsaaAgcmAnnprem.get(wsaaAgcmIb);
					}
				}
			}
		}
		return BigDecimal.ZERO;
	}
	
	private Optional<BigDecimal> c000CallRounding(ZrdecplcDTO zrdecplcPojo, L2CollectionReaderDTO item) {
		/* C100-CALL */
		zrdecplcPojo.setCompany(item.getBprdpf().getCompany());
		zrdecplcPojo.setBatctrcde(item.getBatchDetail().getBatctrcde());
		try {
			return Optional.ofNullable(zrdecplc.convertAmount(zrdecplcPojo));
		} catch (IOException e) {
			fatalError600("Zrdecplc error " + e.getMessage());
			e.printStackTrace();
		}
		return Optional.empty();
	}

	private boolean processCovrs3510(L2CollectionReaderDTO item) {
		List<Covrpf> covrList = item.getCovrMap().get(item.getChdrnum());
		for (Covrpf covrpf : covrList) {
			
			/* If the Payrseqno is not equal to that of the LINS then no */
			/* further processing must be carried out on this LINS. */
			if (covrpf.getPayrseqno() != item.getPayrseqno()) {
				return false;
			}
			
			/* If the working storage array has been exceeded, error. */
//			if (isGT(wsaaCovrIx, wsaaCovrSize)) {
//				fatalError600("h791: WSAA-COVR-REC");
//			}
			if (!"1".equals(covrpf.getValidflag())) {
				continue;
			}
			/* no use for base
			 * if (reinstflag && isFoundPro) {
				isShortTerm = false;
				checkTd5j2Term(covrpf.getCrtable().toString());
				if (isShortTerm) {
					if (item.getLinsinstto() < ptrnpfReinstate.getDatesub()) {
						isShortTerm = false;
						continue;
					} else {
						calprpmrec.prem.set(ZERO);
						calprpmrec.reinstDate.set(ptrnpfReinstate.getDatesub());
						calprpmrec.lastPTDate.set(linxpfResult.getInstfrom());
						calprpmrec.nextPTDate.set(linxpfResult.getLinsInstto());
						calprpmrec.transcode.set(batcdorrec.trcde);
						calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
						calprpmrec.chdrcoy.set(bsprIO.getCompany());
						calprpmrec.chdrnum.set(linxpfResult.getChdrnum());
						callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec, null);
						if (isNE(calprpmrec.statuz, varcom.oK)) {
							syserrrec.params.set(calprpmrec.statuz);
							fatalError600();
						}
					}
				}
			}*/
			Chdrpf readChdrpf = chdrpfDAO.readRecordOfChdrpf(item.getChdrcoy(), item.getChdrnum()); // ILIFE-7075
			/* We will only store the coverage info when it passed the */
			/* validation check. */
			/* Store the CRTABLES of the coverages ready for posting in */
			/* commission so we dont have to read the COVR again. Store them */
			/* in ascending sequence. */
			if ((!(item.getInstfrom() < covrpf.getCurrfrom() && readChdrpf.getBillfreq().equals("12")))
					&& (item.getInstfrom() >= covrpf.getCurrto())) {
				continue;
			}
			WsaaCovrRec wsaaCovrRec = new WsaaCovrRec();
			long validNum = item.getT5679().getCovRiskStats().stream().filter(riskstat -> {
				if (riskstat.equals(covrpf.getStatcode())) {
					if (item.getT5679().getCovPremStats().stream().anyMatch(
							covPremStat -> covPremStat.equals(covrpf.getPstatcode()))) {
						wsaaCovrRec.wsaaCovrValid = "Y";
						return true;
					}
				}
				return false;
			}).count();

			if (validNum == 0) {
				l2collectioncontroltotaldto.setControlTotal12(l2collectioncontroltotaldto.getControlTotal12().add(BigDecimal.ONE));
				return true;
			}
			if (StringUtils.equals(covrpf.getChdrnum(), wsaaLastChdrnum) && StringUtils.equals(covrpf.getLife(), wsaaLastLife)
					&& StringUtils.equals(covrpf.getCoverage(), wsaaLastCoverage) && StringUtils.equals(covrpf.getRider(), wsaaLastRider)
					&& (covrpf.getPlanSuffix() == wsaaLastPlanSuff)) {
				return true;
			}
			/* Store the CRTABLES of the coverages ready for posting in */
			/* commission so we dont have to read the COVR again. Store them */
			/* in ascending sequence. */
			
			wsaaCovrRec.wsaaCovrChdrnum = covrpf.getChdrnum();
			wsaaLastChdrnum = covrpf.getChdrnum();
			wsaaCovrRec.wsaaCovrLife = covrpf.getLife();
			wsaaLastLife=(covrpf.getLife());
			wsaaCovrRec.wsaaCovrCovrg=(covrpf.getCoverage());
			wsaaLastCoverage=(covrpf.getCoverage());
			wsaaCovrRec.wsaaCovrRider=(covrpf.getRider());
			wsaaLastRider=(covrpf.getRider());
			wsaaCovrRec.wsaaCovrPlanSuff=(covrpf.getPlanSuffix());
			wsaaLastPlanSuff=(covrpf.getPlanSuffix());
			wsaaCovrRec.wsaaCovrCrtable=(covrpf.getCrtable());
			wsaaCovrRecs.add(wsaaCovrRec);
			/* For VALID coverages perform component level accounting if */
			/* necessary and if the COVRLNB-INSTPREM not = 0 */
			if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y") && !StringUtils.equals(item.getWsaaT5688Revacc(), "Y")
					&& BigDecimal.ZERO.compareTo(covrpf.getInstprem()) != 0) {
				wsaaGlSub = 6; // 22
				wsaaGlSub1 = 1;
				wsaaRldgacct.wsaaRldgChdrnum=(covrpf.getChdrnum());
				wsaaRldgacct.wsaaRldgLife=(covrpf.getLife());
				wsaaRldgacct.wsaaRldgCoverage=(covrpf.getCoverage());
				wsaaRldgacct.wsaaRldgRider=(covrpf.getRider());
				wsaaRldgacct.wsaaRldgPlanSuff=(df.format(covrpf.getPlanSuffix()).substring(2, 4));
				lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
				
				/* MOVE 0 TO LIFA-JRNSEQ */
				/* no use for base
				 * if (reinstflag && isShortTerm) {
					lifacmvPojo.setOrigamt((calprpmrec.prem).getbigdata());
				} else {
					lifacmvPojo.setOrigamt(covrpf.getInstprem());
				}*/
				lifacmvPojo.setOrigamt(covrpf.getInstprem());
				lifacmvPojo.setOrigcurr(item.getChdrcntcurr());
				// lifacmvPojo.setOrigamt(covrpf.getInstprem());
				lifacmvPojo.setSubstituteCodes(setSubstituteCodeList(covrpf.getCrtable(), 4, 6, lifacmvPojo.getSubstituteCodes()));
//				if (lifacmvPojo.getSubstituteCodes() != null) {
//					lifacmvPojo.getSubstituteCodes().set(4, covrpf.getCrtable());
//				} else {
//					String[] substituteCodes = new String[6];
//					substituteCodes[5] = covrpf.getCrtable();
//					lifacmvPojo.setSubstituteCodes(Arrays.asList(substituteCodes));
//				}

				lifacmvPojo.setEffdate(item.getInstfrom());
				callLifacmv6000(item);
			}
			/* Bonus Workbench * */
			if (!"Y".equals(item.getTh605recIndic())) {
				next3515(covrpf, item);
				return true;
			}
			L2CollectionsProcessDTO.WsaaT5687Rec wsaaT5687Rect = item.getWsaaT5687Rec().get(covrpf.getCrtable().trim());
			if (wsaaT5687Rect == null) {
				fatalError600("t5687");
			}
			if (covrpf.getCrrcd() < wsaaT5687Rect.getWsaaT5687Itmfrm()) {
				fatalError600("h255");
			}
			if (covrpf.getInstprem().compareTo(BigDecimal.ZERO) != 0) {
				wsaaBillfq=(item.getPayrbillfreq());
			} else {
				if (!StringUtils.isBlank(wsaaT5687Rect.getWsaaT5687Bbmeth())) {
					String wsaaT5534UnitFreq = item.getWsaaT5534UnitFreq().get(wsaaT5687Rect.getWsaaT5687Bbmeth());
					if (wsaaT5534UnitFreq == null) {
						fatalError600("t5534");
					}
					wsaaBillfq = wsaaT5534UnitFreq;
					if (!StringUtils.isNumeric(wsaaBillfq)) {
						fatalError600("t5534");
					} else {
						if (BigDecimal.ZERO.compareTo(new BigDecimal(wsaaBillfq)) == 0) {
							wsaaBillfq=("01");
						}
					}
				} else {
					wsaaBillfq=(item.getPayrbillfreq());
				}
			}
			WsaaAgcmRec rec = new WsaaAgcmRec();
			rec.wsaaAgcmChdrcoy = (covrpf.getChdrcoy());
			rec.wsaaAgcmChdrnum = (covrpf.getChdrnum());
			rec.wsaaAgcmLife = (covrpf.getLife());
			rec.wsaaAgcmCoverage = (covrpf.getCoverage());
			rec.wsaaAgcmRider=(covrpf.getRider());
			wsaaAgcmRec.add(rec);
			b200PremiumHistory(covrpf, item, rec);
			if (covrpf.getInstprem().compareTo(BigDecimal.ZERO) != 0) {
				b300WriteArrays(covrpf, item, rec);
			}
			next3515(covrpf, item);
		}
		return true;
	}

	private void b200PremiumHistory(Covrpf covrpf, L2CollectionReaderDTO item, WsaaAgcmRec rec) {
		List<Agcmpf> agcmList = item.getAgcmMap().get(covrpf.getChdrnum());
		for (Agcmpf agcmpf : agcmList) {
			if (agcmpf.getLife().equals(covrpf.getLife()) && agcmpf.getCoverage().equals(covrpf.getCoverage())
					&& agcmpf.getRider().equals(covrpf.getRider())) {
				/* Skip those irrelevant records */
				if (!StringUtils.equals(agcmpf.getOvrdcat(), "B") || agcmpf.getPtdate()==0 || (agcmpf.getEfdate() == 0)
						|| (agcmpf.getEfdate() == 99999999)) {
					continue;
				}
				b230Summary(agcmpf, rec);
			}
		}
	}
	
	private void b230Summary(Agcmpf agcmpf, WsaaAgcmRec rec) {
		boolean wsaaAgcmFound = false;
		int i = 0;
			for (i = 0; i < rec.wsaaAgcmEfdate.size(); i++) {
				if ( rec.wsaaAgcmEfdate.get(i) == 0) {
					break;
				}
				if (agcmpf.getSeqno() == rec.wsaaAgcmSeqno.get(i)) {
					rec.wsaaAgcmAnnprem.set(i, rec.wsaaAgcmAnnprem.get(i).add(agcmpf.getAnnprem()));
					wsaaAgcmFound = true;
					break;
				}
			}
		if (!wsaaAgcmFound) {
			if (i > rec.wsaaAgcmEfdate.size()) {
				fatalError600("e103");
			}
			rec.wsaaAgcmSeqno.set(i, agcmpf.getSeqno());
			rec.wsaaAgcmEfdate.set(i, agcmpf.getEfdate());
			rec.wsaaAgcmAnnprem.set(i, agcmpf.getAnnprem());
		}
	}
	
	private void b300WriteArrays(Covrpf covrpf, L2CollectionReaderDTO item, WsaaAgcmRec rec) {
		int wsaaAgcmIy = 0;
		while (rec.wsaaAgcmEfdate.get(wsaaAgcmIy) != 0) {
			if (rec.wsaaAgcmEfdate.get(wsaaAgcmIy) > item.getInstfrom()) {
				wsaaAgcmIy++;
				continue;
			}
			b330Writ(wsaaAgcmIy, covrpf, item, rec);
			wsaaAgcmIy++;
		}
	}
	
	private void b330Writ(int wsaaAgcmIy, Covrpf covrpf, L2CollectionReaderDTO item, WsaaAgcmRec rec) {
		
		try {
			int freq = datcon3.getYearsDifference(rec.wsaaAgcmEfdate.get(wsaaAgcmIy), item.getInstfrom()).intValue();
			if (freq >= 1) {
				wsaaPremType = "R";
			} else {
				wsaaPremType = "I";
			}
			if (BigDecimal.ZERO.compareTo(rec.wsaaAgcmAnnprem.get(wsaaAgcmIy)) != 0) {

				Zptnpf zptnpf = new Zptnpf();
				zptnpf.setChdrcoy(covrpf.getChdrcoy());
				zptnpf.setChdrnum(covrpf.getChdrnum());
				zptnpf.setLife(covrpf.getLife());
				zptnpf.setCoverage(covrpf.getCoverage());
				zptnpf.setRider(covrpf.getRider());
				zptnpf.setTranno(item.getChdrtranno());
				zptnpf.setOrigamt(rec.wsaaAgcmAnnprem.get(wsaaAgcmIy).divide(new BigDecimal(wsaaBillfq)));
				zptnpf.setTransCode(item.getBprdpf().getBauthcode());
				zptnpf.setEffdate(item.getInstfrom());
				zptnpf.setInstfrom(item.getInstfrom());
				zptnpf.setBillcd(item.getBillcd());
				zptnpf.setInstto(item.getLinsinstto());
				zptnpf.setTrandate(DatimeUtil.DateToInt(item.getBsscpf().getBprceffdat()));
				zptnpf.setZprflg(wsaaPremType);

				if (item.getZptnInsertList() == null) {
					item.setZptnInsertList(new ArrayList<Zptnpf>());
				}
				item.getZptnInsertList().add(zptnpf);
			}
		} catch (ParseException e) {
			fatalError600(e.getMessage());
		}
		
	}
	
	
	private void next3515(Covrpf covrpf, L2CollectionReaderDTO item) {
		/* Post the calculated taxes */
//		wsaaTxLife=(covrpf.getLife());
//		wsaaTxCoverage=(covrpf.getCoverage());
//		wsaaTxRider=(covrpf.getRider());
//		wsaaTxPlansfx=(covrpf.getPlanSuffix());
//		wsaaTxCrtable=(covrpf.getCrtable());
		b700PostTax(item, covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix(), covrpf.getCrtable());
		genericProcessing5000(covrpf, item);
	}
	
	private void genericProcessing5000(Covrpf covrpf, L2CollectionReaderDTO item) {
//		wsaaAuthCode.set(bprdIO.getAuthCode());
//		wsaaCovrlnbCrtable.set(covrpf.getCrtable());
		List<String> t5671Subs = item.getWsaaT5671Subprogs().get(item.getBprdpf().getBauthcode() + covrpf.getCrtable());
		if (t5671Subs == null) {
			return;
		}
		t5671Subs.stream().forEach(t5671Sub -> {
			if (!StringUtils.isBlank(t5671Sub)) {
				callGeneric5100(covrpf, item);
			}
		});
	}
	
	private void callGeneric5100(Covrpf covrpf, L2CollectionReaderDTO item) {
		RnlallDTO rnlallrec = new RnlallDTO();
		rnlallrec.setCompany(item.getChdrcoy());
		rnlallrec.setChdrnum(item.getChdrnum());
		rnlallrec.setLife(covrpf.getLife());
		rnlallrec.setCoverage(covrpf.getCoverage());
		rnlallrec.setRider(covrpf.getRider());
		rnlallrec.setPlanSuffix(covrpf.getPlanSuffix());
		rnlallrec.setCrdate(covrpf.getCrrcd());
		rnlallrec.setCrtable(covrpf.getCrtable());
		rnlallrec.setBillcd(item.getPayrbillcd());
		rnlallrec.setTranno(item.getChdrtranno());
		if (StringUtils.equals(item.getWsaaT5688Comlvlacc(), "Y")) {
			rnlallrec.setSacscode(item.getWsaaT5645Sacscode()[1][13]);
			rnlallrec.setSacstyp(item.getWsaaT5645Sacstype()[1][13]);
			rnlallrec.setGenlcde(item.getWsaaT5645Glmap()[1][13]);
			rnlallrec.setSacscode02(item.getWsaaT5645Sacscode()[1][14]);
			rnlallrec.setSacstyp02(item.getWsaaT5645Sacstype()[1][14]);
			rnlallrec.setGenlcde02(item.getWsaaT5645Glmap()[1][14]);
		} else {
			rnlallrec.setSacscode(item.getWsaaT5645Sacscode()[1][3]);
			rnlallrec.setSacstyp(item.getWsaaT5645Sacstype()[1][3]);
			rnlallrec.setGenlcde(item.getWsaaT5645Glmap()[1][3]);
			rnlallrec.setSacscode02(item.getWsaaT5645Sacscode()[1][4]);
			rnlallrec.setSacstyp02(item.getWsaaT5645Sacstype()[1][4]);
			rnlallrec.setGenlcde02(item.getWsaaT5645Glmap()[1][4]);
		}
		rnlallrec.setCntcurr(item.getPayrcntcurr());
		rnlallrec.setCnttype(item.getChdrcnttype());
		rnlallrec.setBillfreq(item.getPayrbillfreq());
		rnlallrec.setDuedate(item.getInstfrom());
		rnlallrec.setAnbAtCcd(covrpf.getAnbAtCcd());
		/* Calculate the term left to run. It is needed in the generic */
		/* processing routine to work out the initial unit discount factor */
			ZoneId zone = ZoneId.systemDefault();
		    LocalDateTime localDateTime = LocalDateTime.ofInstant(item.getBsscpf().getBprceffdat().toInstant(), zone);
		    LocalDate dt1 = localDateTime.toLocalDate();
		    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			LocalDate dt2 = LocalDate.parse(Integer.toString(covrpf.getPcesdte()), formatter);
			Period p = dt1.until(dt2);
			int freq = p.getYears();
			if (p.getMonths() != 0 || p.getDays() != 0) {
				freq++;
			}
			rnlallrec.setTermLeftToRun(freq);
			/* Get the total premium from the temporary table for the coverage */
			rnlallrec.setCovrInstprem(covrpf.getInstprem());
			rnlallrec.setTotrecd(BigDecimal.ZERO);
			rnlallrec.setEffdate(DatimeUtil.DateToInt(item.getBsscpf().getBprceffdat()));
			rnlallrec.setBatctrcde(item.getBatchDetail().getBatctrcde());
			rnlallrec.setMoniesDate(DatimeUtil.DateToInt(item.getBsscpf().getBprceffdat()));
			try {
				rnlallrec = zrulaloc.processZrulaloc(rnlallrec);
				if ("BOMB".equals(rnlallrec.getStatuz())) {
					fatalError600("Error in zrulaloc");
				}
			} catch (Exception e) {
				e.printStackTrace();
				fatalError600("Error in zrulaloc " + e.getMessage());
			}

	}

	private void fatalError600(String status) {
		throw new StopRunException(status);
	}
	
	private String[] setSubstituteCodes(String code, int pos, int length, String[] substituteCodes) {
		if (substituteCodes != null) {
			substituteCodes[pos] = code;
			return substituteCodes;
        } else {
        	String[] substituteCode = new String[length];
        	for (int i = 0; i < substituteCode.length; i++) {
        		substituteCode[i] = "";
        	}
        	substituteCode[pos] = code;
            return substituteCode;
        }
	}
	
	private List<String> setSubstituteCodeList(String code, int pos, int length, List<String> substituteCodes) {
		if (substituteCodes == null) {
			return Arrays.asList(setSubstituteCodes(code, pos, length, null));
		}
		return Arrays.asList(setSubstituteCodes(code, pos, length, substituteCodes.toArray(new String[] {})));
	}
	
	private static final class WsaaAgcmRec {
		String wsaaAgcmChdrcoy;
		String wsaaAgcmChdrnum;
		String wsaaAgcmLife;
		String wsaaAgcmCoverage;
		String wsaaAgcmRider;
		List<Integer> wsaaAgcmSeqno = new ArrayList<>(Collections.nCopies(500, 0));
		List<Integer> wsaaAgcmEfdate = new ArrayList<>(Collections.nCopies(500, 0));
		List<BigDecimal> wsaaAgcmAnnprem = new ArrayList<>(Collections.nCopies(500, BigDecimal.ZERO));
	}
	
	private static final class WsaaCovrRec {
		private String wsaaCovrChdrnum;
		private String wsaaCovrLife;
		private String wsaaCovrCovrg;
		private String wsaaCovrRider;
		private int wsaaCovrPlanSuff;
		private String wsaaCovrCrtable;
		private String wsaaCovrValid = "N";
		
//		public String getWsaaCovrKey() {
//			return wsaaCovrChdrnum + wsaaCovrLife + wsaaCovrCovrg + wsaaCovrRider + wsaaCovrPlanSuff;
//		}
		
	}
	
	private static final class WsaaRldgacct {
		private String wsaaRldgChdrnum;
		private String wsaaRldgLife;
		private String wsaaRldgCoverage;
		private String wsaaRldgRider;
		private String wsaaRldgPlanSuff;
		
		public String toString() {
			return wsaaRldgChdrnum + wsaaRldgLife + wsaaRldgCoverage + wsaaRldgRider + wsaaRldgPlanSuff;
		}
	}
	
	private static final class WsaaRldgagnt {
		private String wsaaAgntChdrnum;
		private String wsaaAgntLife;
		private String wsaaAgntCoverage;
		private String wsaaAgntRider;
		private String wsaaAgntPlanSuffix;

	}
}
