package com.dxc.integral.life.general.readers;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Clrrpf;
import com.dxc.integral.life.beans.L2CollectionReaderDTO;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Taxdpf;

public class L2CollectionsReader extends JdbcPagingItemReader<L2CollectionReaderDTO> {

	private DataSource dataSourceForL2Reader;
	@Override
	protected void doReadPage() {
		super.doReadPage();
		if (this.results != null) {
			for (L2CollectionReaderDTO l2CollectionReaderDTO : this.results) {
				String chrdNum = l2CollectionReaderDTO.getChdrnum();
				String chdrCoy = l2CollectionReaderDTO.getChdrcoy();
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceForL2Reader);
				// tax
				StringBuilder sqltax = new StringBuilder();
				sqltax.append("SELECT * FROM TAXDPF WHERE CHDRCOY=? AND CHDRNUM = ? ")
						.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, INSTFROM DESC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLANSFX ASC, TRANTYPE ASC, UNIQUE_NUMBER DESC ");
				Object[] paraTax = new Object[] { chdrCoy, chrdNum };
				List<Taxdpf> taxdpfs = jdbcTemplate.query(sqltax.toString(), paraTax, new BeanPropertyRowMapper<Taxdpf>(Taxdpf.class));
				l2CollectionReaderDTO.getTaxdMap().put(chrdNum, taxdpfs);
				
				// clrr
				StringBuilder sqlclrr = new StringBuilder();
				sqlclrr.append(
		                "SELECT * FROM CLRRPF CL WHERE CL.FORECOY=? AND CL.CLRRROLE='PY' AND RTRIM(FORENUM) = ? ")
		                .append(" ORDER BY CL.FOREPFX ASC, CL.FORECOY ASC, CL.FORENUM ASC, CL.CLRRROLE ASC, CL.UNIQUE_NUMBER ASC ");
				Object[] paraclrr = new Object[] { chdrCoy, chrdNum + l2CollectionReaderDTO.getPayrseqno() };
				List<Clrrpf> clrrpfs = jdbcTemplate.query(sqlclrr.toString(), paraclrr, new BeanPropertyRowMapper<Clrrpf>(Clrrpf.class));
				l2CollectionReaderDTO.getClrrMap().put(chrdNum + l2CollectionReaderDTO.getPayrseqno(), clrrpfs);
				
				// acbl
				StringBuilder sqlacbl = new StringBuilder("SELECT * FROM ACBLPF WHERE RLDGCOY=? AND RLDGACCT = ? ");
				sqlacbl.append(" ORDER BY RLDGCOY ASC, SACSCODE ASC, RLDGACCT ASC, ORIGCURR ASC, SACSTYP ASC, UNIQUE_NUMBER DESC");
				Object[] paraAcbl = new Object[] { chdrCoy, chrdNum };
				List<Acblpf> acblpfs = jdbcTemplate.query(sqlacbl.toString(), paraAcbl, new BeanPropertyRowMapper<Acblpf>(Acblpf.class));
				l2CollectionReaderDTO.getAcblMap().put(chrdNum, acblpfs);
				
				// covr
				StringBuilder sqlCovr = new StringBuilder("SELECT * FROM COVRPF WHERE CHDRCOY=? AND CHDRNUM = ? ");
				sqlCovr.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		        Object[] paraCover = new Object[] { chdrCoy, chrdNum };
				List<Covrpf> covrpfs = jdbcTemplate.query(sqlCovr.toString(), paraCover, new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
				l2CollectionReaderDTO.getCovrMap().put(chrdNum, covrpfs);
				
				// agcm
				StringBuilder sqlagcm = new StringBuilder();
		        sqlagcm.append("SELECT * FROM AGCMPF WHERE CHDRCOY=? AND VALIDFLAG = '1' AND DORMFLAG <> 'Y' AND CHDRNUM = ? ")
		            .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,SEQNO ASC, AGNTNUM ASC,UNIQUE_NUMBER ASC ");
		        Object[] paraAgcm = new Object[] { chdrCoy, chrdNum };
				List<Agcmpf> agcmpfs = jdbcTemplate.query(sqlagcm.toString(), paraAgcm, new BeanPropertyRowMapper<Agcmpf>(Agcmpf.class));
				l2CollectionReaderDTO.getAgcmMap().put(chrdNum, agcmpfs);
				
				// acblGL
				Object[] paraAcblGl = new Object[] { chdrCoy, chrdNum + "00" };
				List<Acblpf> acblpfGls = jdbcTemplate.query(sqlacbl.toString(), paraAcblGl, new BeanPropertyRowMapper<Acblpf>(Acblpf.class));
				l2CollectionReaderDTO.getAcblMap().put(chrdNum + "00", acblpfGls);
			}
		}
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
		dataSourceForL2Reader = dataSource;
	}

}
