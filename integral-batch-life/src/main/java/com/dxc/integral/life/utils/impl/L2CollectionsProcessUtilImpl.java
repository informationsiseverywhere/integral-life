package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Clrrpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.beans.Csdopt;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.BatchInformBean;
import com.dxc.integral.life.beans.L2CollectionControlTotalDTO;
import com.dxc.integral.life.beans.L2CollectionReaderDTO;
import com.dxc.integral.life.beans.L2CollectionsProcessDTO;
import com.dxc.integral.life.beans.LifrtrnDTO;
import com.dxc.integral.life.beans.PrasDTO;
import com.dxc.integral.life.beans.RnlAllRec;
import com.dxc.integral.life.dao.model.LifacmvPojo;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.exception.L2CollectionsException;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.smarttable.pojo.T3695;
import com.dxc.integral.life.smarttable.pojo.T5534;
import com.dxc.integral.life.smarttable.pojo.T5644;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5667;
import com.dxc.integral.life.smarttable.pojo.T5671;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.T6654;
import com.dxc.integral.life.smarttable.pojo.T6687;
import com.dxc.integral.life.smarttable.pojo.Th605;
import com.dxc.integral.life.utils.L2CollectionsProcessUtil;
import com.dxc.integral.life.utils.Prascalc;

@Service("l2CollectionsProcessUtil")
public class L2CollectionsProcessUtilImpl implements L2CollectionsProcessUtil {
	
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Prascalc prascalc;
	@Autowired
	private AcblpfDAO acblpfDAO;
	
	private static final int wsaaTotalSize = 1000;

	@Override
	public L2CollectionsProcessDTO init(L2CollectionReaderDTO item, BatchInformBean informBean) {
		L2CollectionsProcessDTO processDto = new L2CollectionsProcessDTO();
		if (StringUtils.isNumeric(informBean.getBprdpf().getBpsyspar01())) {
        	Integer size = Integer.valueOf(informBean.getBprdpf().getBpsyspar01());
        	if (size > 0) {
        		item.setIntBatchExtractSize(size);
        	} else {
        		item.setIntBatchExtractSize(Integer.valueOf(informBean.getBprdpf().getBcycpercmt()));
        	}
        } else {
        	item.setIntBatchExtractSize(Integer.valueOf(informBean.getBprdpf().getBcycpercmt()));
        }
        Descpf descpfT1688;
        Map<String, Descpf> t1688Map = descpfDAO.getItems("IT", Companies.LIFE, "T1688", informBean.getBsscpf().getLanguage());
        if (t1688Map.containsKey(informBean.getBprdpf().getBauthcode())) {
            descpfT1688 = t1688Map.get(informBean.getBprdpf().getBauthcode());
        } else {
            descpfT1688 = new Descpf();
            descpfT1688.setLongdesc("");
        }
        processDto.setDescpfT1688(descpfT1688);
        
        RnlAllRec rnlallrec = new RnlAllRec();
        LifacmvPojo lifacmvPojo = new LifacmvPojo();
        LifrtrnDTO lifrtrnPojo = new LifrtrnDTO();
        lifacmvPojo.setJrnseq(0);
        rnlallrec.setEffdate(DatimeUtil.DateToInt(informBean.getBsscpf().getBprceffdat()));
        rnlallrec.setMoniesDate(DatimeUtil.DateToInt(informBean.getBsscpf().getBprceffdat()));
        lifrtrnPojo.setBatccoy(informBean.getBatchDetail().getBatccoy());
        lifrtrnPojo.setRldgcoy(informBean.getBatchDetail().getBatccoy());
        lifrtrnPojo.setGenlcoy(informBean.getBatchDetail().getBatccoy());
        lifacmvPojo.setBatccoy(informBean.getBatchDetail().getBatccoy());
        lifacmvPojo.setRldgcoy(informBean.getBatchDetail().getBatccoy());
        lifacmvPojo.setGenlcoy(informBean.getBatchDetail().getBatccoy());
        rnlallrec.setBatccoy(informBean.getBatchDetail().getBatccoy());
        rnlallrec.setLanguage(informBean.getBsscpf().getLanguage());
        lifrtrnPojo.setBatcactyr(informBean.getBatchDetail().getBatcactyr());
        lifacmvPojo.setBatcactyr(informBean.getBatchDetail().getBatcactyr());
        rnlallrec.setBatcactyr(informBean.getBatchDetail().getBatcactyr());
        lifrtrnPojo.setBatcactmn(informBean.getBatchDetail().getBatcactmn());
        lifacmvPojo.setBatcactmn(informBean.getBatchDetail().getBatcactmn());
        rnlallrec.setBatcactmn(informBean.getBatchDetail().getBatcactmn());
        lifrtrnPojo.setBatctrcde(informBean.getBatchDetail().getBatctrcde());
        lifacmvPojo.setBatctrcde(informBean.getBatchDetail().getBatctrcde());
        rnlallrec.setBatctrcde(informBean.getBatchDetail().getBatctrcde());
        lifrtrnPojo.setBatcbatch(informBean.getBatchDetail().getBatcbatch());
        lifacmvPojo.setBatcbatch(informBean.getBatchDetail().getBatcbatch());
        rnlallrec.setBatcbatch(informBean.getBatchDetail().getBatcbatch());
        lifrtrnPojo.setBatcbrn(informBean.getBatchDetail().getBatcbrn());
        lifacmvPojo.setBatcbrn(informBean.getBatchDetail().getBatcbrn());
        rnlallrec.setBatcbrn(informBean.getBatchDetail().getBatcbrn());
        lifrtrnPojo.setRcamt(0);
        lifacmvPojo.setRcamt(0);
        lifrtrnPojo.setCrate(BigDecimal.ZERO);
        lifacmvPojo.setCrate(BigDecimal.ZERO);
        lifrtrnPojo.setAcctamt(BigDecimal.ZERO);
        lifacmvPojo.setAcctamt(BigDecimal.ZERO);
        lifrtrnPojo.setUser(0);
        lifacmvPojo.setUser(0);
        rnlallrec.setUser(0);;
        lifrtrnPojo.setFrcdate(CommonConstants.MAXDATE);
        lifacmvPojo.setFrcdate(CommonConstants.MAXDATE);
        lifrtrnPojo.setTransactionTime(informBean.getBatchDetail().getTimopen().intValue());

        lifrtrnPojo.setTrandesc(descpfT1688.getLongdesc());
        lifacmvPojo.setTrandesc(descpfT1688.getLongdesc());
        /* MOVE BSSC-EFFECTIVE-DATE TO LIFR-TRANSACTION-DATE. */
        lifrtrnPojo.setTransactionDate(DatimeUtil.DateToInt(informBean.getBsscpf().getBprceffdat()));
        processDto.setLifacmvPojo(lifacmvPojo);
        processDto.setLifrtrnPojo(lifrtrnPojo);
        processDto.setRnlallrec(rnlallrec);
        
        // init items
        // Get gl information
        List<TableItem<T5645>> t5645Item = itempfService.readSmartTableByTrimItemList(Companies.LIFE, "T5645", informBean.getProgramName(), "IT", T5645.class);
        if (t5645Item.size() > wsaaTotalSize || t5645Item.size() == 0) throw new L2CollectionsException("H791: t5645 " + informBean.getProgramName());
        loadT56451100(t5645Item, processDto);
        
        /* Retrieve the suspense account sign */
        TableItem<T3695> t3695 = itempfService.readSmartTableByTrimItem(Companies.LIFE, "T3695", processDto.getWsaaT5645Sacstype()[0][0], "IT", T3695.class);
        if (t3695 == null) {
        	throw new L2CollectionsException("t3695: " + processDto.getWsaaT5645Sacstype()[0][0]);
        }
        processDto.setT3695(t3695.getItemDetail());
        
        /* Load tax relief methods. */
        List<TableItem<T6687>> t6687List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T6687", T6687.class);
        if (t6687List.size() > wsaaTotalSize) throw new L2CollectionsException("H791: t6687 " + informBean.getProgramName());
        loadT66871300(t6687List, processDto);
        
        /* Load contract type details. */
        List<TableItem<T5688>> t5688List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T5688", T5688.class);
        if (t5688List.size() > wsaaTotalSize || t5688List.size() == 0) throw new L2CollectionsException("H791: t5688 " + informBean.getProgramName());
        loadT56881400(t5688List, processDto);
        
        /* Load Contract statii. */
        TableItem<T5679> t5679 = itempfService.readSmartTableByTrimItem(Companies.LIFE, "T5679", informBean.getBprdpf().getBauthcode().trim(), "IT", T5679.class);
        if (t5679 == null) {
        	throw new L2CollectionsException("t3695: " + informBean.getBprdpf().getBauthcode());
        }
        processDto.setT5679(t5679.getItemDetail());
        
        /* Load the commission methods. */
        List<TableItem<T5644>> t5644List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T5644", T5644.class);
        if (t5644List.size() > wsaaTotalSize) throw new L2CollectionsException("H791: t5644 " + informBean.getProgramName());
        loadT56441600(t5644List, processDto);
        
        /* Load the Collection routines */
        List<TableItem<T6654>> t6654List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T6654", T6654.class);
        if (t6654List.size() > wsaaTotalSize) throw new L2CollectionsException("H791: T6654 " + informBean.getProgramName());
        loadT66541700(t6654List, processDto);
        
        /* Load the tolerance limits. */
        List<TableItem<T5667>> t5667List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T5667", T5667.class);
        loadT56671800(t5667List, processDto, informBean.getBprdpf().getBauthcode().trim());
        /* Load the tolerance limits. */
        List<TableItem<T5671>> t5671List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T5671", T5671.class);
        loadT56711900(t5671List, processDto, informBean.getBprdpf().getBauthcode().trim());
        
        /* Load Company Defaults */
        TableItem<Th605> th605 = itempfService.readSmartTableByTrimItem(Companies.LIFE, "TH605", Companies.LIFE, "IT", Th605.class);
        if (th605 == null) {
        	throw new L2CollectionsException("th605: " + Companies.LIFE);
        }
        if (!"Y".equals(th605.getItemDetail().getBonusInd())) {
    		item.setSkiped(true);
    		processDto.setSkiped(true);
    		return processDto;
    	}
    	item.setTh605recIndic("Y");
    	
    	/* Load coverage details. */
    	List<TableItem<T5687>> t5687List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T5687", T5687.class);
    	if (t5687List.size() > wsaaTotalSize) throw new L2CollectionsException("H791: T5687 " + informBean.getProgramName());
    	loadT56871a00(t5687List, processDto);
    	
    	/* Load the benefit billing details. */
    	List<TableItem<T5534>> t5534List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T5534", T5534.class);
    	if (t5534List.size() > wsaaTotalSize) throw new L2CollectionsException("H791: T5534 " + informBean.getProgramName());
    	loadT55341b00(t5534List, processDto);
    	
        return processDto;
	}

	@Override
	public void edit(L2CollectionReaderDTO item, BatchInformBean informBean, L2CollectionsProcessDTO processDto, L2CollectionControlTotalDTO l2collectioncontroltotaldto) {
		/* See if the instalment being processed has the same contract */
		/* as the previous instalment, we do not need to validate the */
		/* CHDR details again. */
		if (!item.getChdrnum().equals(processDto.getWsaaPrevChdrnum())) {
			processDto.setWsaaPrevLins("Y");
		}
		if (!validateContract2300(item, processDto, informBean)) {
			/* Increment LINS not collected */	
			l2collectioncontroltotaldto.addControlTotal09((BigDecimal.ONE));
			item.setSkiped(true);
			return;
		}
		if ("N".equals(processDto.getWsaaPrevLins())) {
			/* Log no. LINS still short of the cbillamt. */
			l2collectioncontroltotaldto.addControlTotal06(BigDecimal.ONE);
			/* Increment the total of outstanding premiums. */
			processDto.setTemptot(item.getInstamt06());
			l2collectioncontroltotaldto.addControlTotal05(processDto.getTemptot());
			/* Log no. LINS not collected. */
			l2collectioncontroltotaldto.addControlTotal04((BigDecimal.ONE));
			item.setSkiped(true);
			return;
		}
		String forenum = item.getChdrnum() + item.getPayrseqno();
		Clrrpf clrrpf = null;
		for (Clrrpf c : item.getClrrMap().get(forenum)) {
			if (c.getForepfx().equals(item.getChdrchdrpfx())) {
				clrrpf = c;
		        break;
			}
		}
		if (clrrpf == null) {
			throw new L2CollectionsException("Not Clrrpf for " + forenum + informBean.getProgramName());
		}
		item.setWsaaPayrnum(clrrpf.getClntnum());
        item.setWsaaPayrcoy(clrrpf.getClntcoy());

		/* Calculate tax */
        BigDecimal wsaaTotTax = b600GetTax(item);
		/* Check for tax relief */
//        PrasDTO prasrec = new PrasDTO();
//		prasrec.setTaxrelamt(BigDecimal.ZERO);
		processDto.setWsaaNetCbillamt(item.getCbillamt());

		processDto.addWsaaNetCbillamt(wsaaTotTax);

		if (!StringUtils.isBlank(item.getPayrtaxrelmth())) {
			calcTaxRelief2600(item, clrrpf, wsaaTotTax, informBean, processDto);
		}
		/* If waiver-holding is required, validate the amount required */
		/* is already in the holding account. */
		BigDecimal wsaaWaiverAvail = BigDecimal.ZERO;
		if (BigDecimal.ZERO.compareTo(item.getInstamt05()) != 0) {
			wsaaWaiverAvail = waiverHolding2900b(item, processDto);
			if ("-".equals(processDto.getWsaaT5645Sign()[0][7])) {
				wsaaWaiverAvail = BigDecimal.ZERO.subtract(wsaaWaiverAvail).setScale(2);
//				compute(wsaaWaiverAvail, 2).set((sub(0, wsaaWaiverAvail)));
			}
			if (wsaaWaiverAvail.compareTo(item.getInstamt05()) > 0) {
				/* Insufficient waiver holding */
				l2collectioncontroltotaldto.setControlTotal13(l2collectioncontroltotaldto.getControlTotal13().add(BigDecimal.ONE));
				l2collectioncontroltotaldto.setControlTotal04(l2collectioncontroltotaldto.getControlTotal04().add(BigDecimal.ONE));
				/* Log no. LINS not collected. */
				item.setSkiped(true);
				return;
			}
		}
		if (wsaaWaiverAvail.compareTo(BigDecimal.ZERO) < 0) {
			wsaaWaiverAvail = wsaaWaiverAvail.multiply(new BigDecimal(-1)).setScale(2);
		}
		if (wsaaWaiverAvail.compareTo(processDto.getWsaaNetCbillamt()) >= 0) {
			processDto.setWsaaSuspAvail(new BigDecimal(wsaaWaiverAvail.toString()));
			processDto.setWsaaWaiveAmtPaid(new BigDecimal(wsaaWaiverAvail.toString()));
		} else {
			suspenseAvail2700(item, processDto);
			if (wsaaWaiverAvail.compareTo(BigDecimal.ZERO) > 0) {
				processDto.setWsaaSuspAvail(new BigDecimal(wsaaWaiverAvail.toString()));
				processDto.setWsaaWaiveAmtPaid(new BigDecimal(wsaaWaiverAvail.toString()));
			}
		}

		/* Check money is available */
		// suspenseAvail2700();
		// ILIFE-1828 END

		/* If insufficient suspense, pull Advance Premium Deposit (APA) */
		/* available into suspense before checking for tolerance. */
		/* Check tolerance if there is insufficient money. */
		if (processDto.getWsaaNetCbillamt().compareTo(processDto.getWsaaSuspAvail()) <= 0) {
//			isLTE(wsaaNetCbillamt, wsaaSuspAvail)) {
			item.setInstamt03(BigDecimal.ZERO);
		} else {
			item.setWsaaDivRequired(cashDivAvail2900a(item, processDto));
//			if (isGT(wsaaNetCbillamt, wsaaSuspAvail)) {
			if (processDto.getWsaaNetCbillamt().compareTo(processDto.getWsaaSuspAvail()) > 0) {
				item.setWsaaApaRequired(apaAvail2900(item, processDto));
			}
			/* PERFORM 2900-APA-AVAIL <V42009> */
			processDto.setWsaaWaiveAmtPaid(new BigDecimal(wsaaWaiverAvail.toString()));
			processDto.setWsaaAmtPaid(processDto.getWsaaSuspAvail().subtract(processDto.getWsaaWaiveAmtPaid()));
			processDto.setAgtTerminated(a400CheckAgent(item, informBean)); 
			calcTolerance2800(item, processDto,  informBean, l2collectioncontroltotaldto);
		}

	}

	private void loadT56451100(List<TableItem<T5645>> t5645Item, L2CollectionsProcessDTO processDto) {
		Integer[][] wsaaT5645Cnttot = new Integer[t5645Item.size()][];
		String[][] wsaaT5645Glmap = new String[t5645Item.size()][];
		String[][] wsaaT5645Sacscode = new String[t5645Item.size()][];
		String[][] wsaaT5645Sacstype = new String[t5645Item.size()][];
		String[][] wsaaT5645Sign = new String[t5645Item.size()][];
		int i = 0;
		for (TableItem<T5645> t5645 : t5645Item) {
			wsaaT5645Cnttot[i] = t5645.getItemDetail().getCnttots().toArray(new Integer[0]);
			wsaaT5645Glmap[i] = t5645.getItemDetail().getGlmaps().toArray(new String[0]);
			wsaaT5645Sacscode[i] = t5645.getItemDetail().getSacscodes().toArray(new String[0]);
			wsaaT5645Sacstype[i] = t5645.getItemDetail().getSacstypes().toArray(new String[0]);
			wsaaT5645Sign[i] = t5645.getItemDetail().getSigns().toArray(new String[0]);
			i++;
		}
		processDto.setWsaaT5645Cnttot(wsaaT5645Cnttot);
		processDto.setWsaaT5645Glmap(wsaaT5645Glmap);
		processDto.setWsaaT5645Sacscode(wsaaT5645Sacscode);
		processDto.setWsaaT5645Sacstype(wsaaT5645Sacstype);
		processDto.setWsaaT5645Sign(wsaaT5645Sign);
	}
	
	private void loadT66871300(List<TableItem<T6687>> t6687List, L2CollectionsProcessDTO processDto) {
		for (TableItem<T6687> itempf : t6687List) {
			processDto.getWsaaT6687Taxrelsubr().put(itempf.getItemitem().trim(), itempf.getItemDetail().getTaxrelsub());
		}
	}
	
	private void loadT56881400(List<TableItem<T5688>> itemList, L2CollectionsProcessDTO processDto) {
		List<L2CollectionsProcessDTO.WsaaT5688Rec> wsaaT5688Recs = new ArrayList<>();
		for (TableItem<T5688> itempf : itemList) {
			L2CollectionsProcessDTO.WsaaT5688Rec wsaaT5688Rec = new L2CollectionsProcessDTO.WsaaT5688Rec();
			wsaaT5688Rec.setWsaaT5688Itmfrm(itempf.getItmfrm());
			wsaaT5688Rec.setWsaaT5688Cnttype(itempf.getItemitem());
			wsaaT5688Rec.setWsaaT5688Comlvlacc(itempf.getItemDetail().getComlvlacc());
			wsaaT5688Rec.setWsaaT5688Revacc(itempf.getItemDetail().getRevacc());
			wsaaT5688Recs.add(wsaaT5688Rec);
		}
		processDto.setWsaaT5688Recs(wsaaT5688Recs);
	}

	private void loadT56441600(List<TableItem<T5644>> itemList, L2CollectionsProcessDTO processDto) {
		Map<String, String> wsaaT5644Comsub = new HashMap<>();
		for (TableItem<T5644> itempf : itemList) {
			wsaaT5644Comsub.put(itempf.getItemitem().trim(), itempf.getItemDetail().getCompysubr());
		}
		processDto.setWsaaT5644Comsub(wsaaT5644Comsub);
	}
	
	private void loadT66541700(List<TableItem<T6654>> itemList, L2CollectionsProcessDTO processDto) {
		Map<String, String> wsaaT6654Collsub = new HashMap<>();
		for (TableItem<T6654> itempf : itemList) {
			wsaaT6654Collsub.put(itempf.getItemitem().trim(), itempf.getItemDetail().getCollectsub());
		}
		processDto.setWsaaT6654Collsub(wsaaT6654Collsub);
	}
	
	private void loadT56671800(List<TableItem<T5667>> itemList, L2CollectionsProcessDTO processDto, String bauthcode) {
		Map<String, L2CollectionsProcessDTO.WsaaT5667ArrayInner> wsaaT5667ArrayInner = new HashMap<>();
		for(TableItem<T5667> itemT5667 : itemList){
	        if(itemT5667.getItemitem().trim().substring(0,4).equals(bauthcode)){
	        	L2CollectionsProcessDTO.WsaaT5667ArrayInner wsaaT5667ArrayInnert = new L2CollectionsProcessDTO.WsaaT5667ArrayInner();
				wsaaT5667ArrayInnert.setWsaaT5667Key(itemT5667.getItemitem());
				wsaaT5667ArrayInnert.setWsaaT5667Freqs(itemT5667.getItemDetail().getFreqs());
				wsaaT5667ArrayInnert.setWsaaT5667MaxAmounts(itemT5667.getItemDetail().getMaxAmounts());
				wsaaT5667ArrayInnert.setWsaaT5667Maxamts(itemT5667.getItemDetail().getMaxamts());
				wsaaT5667ArrayInnert.setWsaaT5667Prmtolns(itemT5667.getItemDetail().getPrmtolns());
				wsaaT5667ArrayInnert.setWsaaT5667Prmtols(itemT5667.getItemDetail().getPrmtols());
				wsaaT5667ArrayInnert.setWsaaT5667Sfind(itemT5667.getItemDetail().getSfind());
				wsaaT5667ArrayInner.put(wsaaT5667ArrayInnert.getWsaaT5667Key(), wsaaT5667ArrayInnert);
	        }
	    }
	    if (wsaaT5667ArrayInner.isEmpty()) {
	        throw new L2CollectionsException("t5667: " + bauthcode);
	    }
	    processDto.setWsaaT5667ArrayInner(wsaaT5667ArrayInner);
	}
	
	private void loadT56711900(List<TableItem<T5671>> itemList, L2CollectionsProcessDTO processDto, String bauthcode) {
		Map<String, List<String>> wsaaT5671Subprogs = new HashMap<>();
		for(TableItem<T5671> item:itemList){
            if(item.getItemitem().trim().substring(0,4).equals(bauthcode)){
            	wsaaT5671Subprogs.put(item.getItemitem().trim(), item.getItemDetail().getSubprogs());
            }
        }
        if (wsaaT5671Subprogs.isEmpty()) {
        	throw new L2CollectionsException("t5671: " + bauthcode);
        }
        processDto.setWsaaT5671Subprogs(wsaaT5671Subprogs);
	}
	
	private void loadT56871a00(List<TableItem<T5687>> itemList, L2CollectionsProcessDTO processDto) {
		Map<String, L2CollectionsProcessDTO.WsaaT5687Rec> wsaaT5687Rec = new HashMap<>();
		for (TableItem<T5687> itempf : itemList) {
			L2CollectionsProcessDTO.WsaaT5687Rec wsaaT5687Rect = new L2CollectionsProcessDTO.WsaaT5687Rec();
			wsaaT5687Rect.setWsaaT5687Bbmeth(itempf.getItemDetail().getBbmeth());
			wsaaT5687Rect.setWsaaT5687Crtable(itempf.getItemitem());
			wsaaT5687Rect.setWsaaT5687Itmfrm(itempf.getItmfrm());
			wsaaT5687Rec.put(itempf.getItemitem().trim(), wsaaT5687Rect);
		}
		processDto.setWsaaT5687Rec(wsaaT5687Rec);
	}
	
	private void loadT55341b00(List<TableItem<T5534>> itemList, L2CollectionsProcessDTO processDto) {
		Map<String, String> wsaaT5534UnitFreq = new HashMap<>();
		for (TableItem<T5534> itempf : itemList) {
			wsaaT5534UnitFreq.put(itempf.getItemitem(), itempf.getItemDetail().getUnitFreq());
		}
		processDto.setWsaaT5534UnitFreq(wsaaT5534UnitFreq);
	}
	
	private void calcTolerance2800(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto, BatchInformBean informBean, L2CollectionControlTotalDTO l2collectioncontroltotaldto) {
		if (!processDto.getWsaaT5667ArrayInner().keySet().stream().anyMatch(key -> key.trim().endsWith(item.getBillcurr()))) {
			item.setSkiped(true);
			return;
		}
		BigDecimal wsaaPrmtol = BigDecimal.ZERO;
		BigDecimal wsaaMaxAmount = BigDecimal.ZERO;
		BigDecimal wsaaPrmtoln = BigDecimal.ZERO;
		BigDecimal wsaaMaxamt = BigDecimal.ZERO;
		String wsaaSfind = "";
		boolean isFound = false;
		for (String key : processDto.getWsaaT5667ArrayInner().keySet()) {
			if (key.trim().endsWith(item.getBillcurr())) {
				processDto.setWsaaToleranceChk(L2CollectionsProcessDTO.WsaaToleranceChk.toleranceLimit1);
				L2CollectionsProcessDTO.WsaaT5667ArrayInner wchk = processDto.getWsaaT5667ArrayInner().get(key);
				int i = 0;
				for (String freqs : wchk.getWsaaT5667Freqs()) {
					if (freqs.equals(item.getInstfreq())) {
						wsaaSfind = wchk.getWsaaT5667Sfind();
						wsaaPrmtol = wchk.getWsaaT5667Prmtols().get(i);
						wsaaPrmtoln = wchk.getWsaaT5667Prmtolns().get(i);
						wsaaMaxAmount = wchk.getWsaaT5667MaxAmounts().get(i);
						wsaaMaxamt = wchk.getWsaaT5667Maxamts().get(i);
						isFound = true;
						break;
					}
					i++;
				}
			}
		}
		if (!isFound) {
			/* Log no. of LINS where FREQ not on T5667 */
	        l2collectioncontroltotaldto.addControlTotal07((BigDecimal.ONE));
	        item.setSkiped(true);
			return;
		}
		boolean isSucess = computeTolerance2820(item, processDto, informBean, l2collectioncontroltotaldto, wsaaPrmtol, wsaaPrmtoln, wsaaMaxAmount, wsaaMaxamt, wsaaSfind);
        if (!isSucess) {
        	failTolerance2855(item, processDto, l2collectioncontroltotaldto);
        }
	}
	
	private boolean computeTolerance2820(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto, BatchInformBean informBean, L2CollectionControlTotalDTO l2collectioncontroltotaldto,
			BigDecimal wsaaPrmtol, BigDecimal wsaaPrmtoln,
			BigDecimal wsaaMaxAmount, BigDecimal wsaaMaxamt, String wsaaSfind) {
		/* Using the tolerance entry found calculate the overall amount */
		/* to be collected. */
		if (processDto.getWsaaToleranceChk().equals(L2CollectionsProcessDTO.WsaaToleranceChk.toleranceLimit1)) {
//			compute(wsaaToleranceAllowed, 3).setRounded(div((mult(wsaaPrmtol, wsaaNetCbillamt)), 100));
			processDto.setWsaaToleranceAllowed(wsaaPrmtol.multiply(processDto.getWsaaNetCbillamt()).divide(new BigDecimal(100)));
//			if (isGT(wsaaToleranceAllowed, wsaaMaxAmount)) {
			if (processDto.getWsaaToleranceAllowed().compareTo(wsaaMaxAmount) > 0) {
				processDto.setWsaaToleranceAllowed(wsaaMaxAmount);
			}
		} else {
//			compute(wsaaToleranceAllowed, 3).setRounded(div((mult(wsaaPrmtoln, wsaaNetCbillamt)), 100));
			processDto.setWsaaToleranceAllowed(wsaaPrmtoln.multiply(processDto.getWsaaNetCbillamt()).divide(new BigDecimal(100)));
//			if (isGT(wsaaToleranceAllowed, wsaaMaxamt)) {
//				wsaaToleranceAllowed.set(wsaaMaxamt);
//			}
			if (processDto.getWsaaToleranceAllowed().compareTo(wsaaMaxamt) > 0) {
				processDto.setWsaaToleranceAllowed(wsaaMaxamt);
			}
		}
		/* MOVE WSAA-TOLERANCE-ALLOWED TO ZRDP-AMOUNT-IN. */
		/* MOVE BILLCURR TO ZRDP-CURRENCY. */
		/* PERFORM C000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO WSAA-TOLERANCE-ALLOWED. */
		// ILB-441 starts
//		if (isNE(wsaaToleranceAllowed, 0)) {
		if (processDto.getWsaaToleranceAllowed().compareTo(BigDecimal.ZERO) != 0) {
			/*
			 * zrdecplrec.amountIn.set(wsaaToleranceAllowed);
			 * zrdecplrec.currency.set(linxpfResult.getBillcurr());
			 */
			processDto.setWsaaToleranceAllowed(c000CallRounding(processDto.getWsaaToleranceAllowed(), item.getBillcurr(), informBean));
			// wsaaToleranceAllowed.set(zrdecplrec.amountOut);
			// ILB-441 ends
		}
		processDto.setWsaaSuspCbillDiff(processDto.getWsaaNetCbillamt().subtract(processDto.getWsaaSuspAvail()));
//		compute(wsaaSuspCbillDiff, 3).setRounded((sub(wsaaNetCbillamt, wsaaSuspAvail)));
		/* Update the suspense as being enough to cover the premium if */
		/* it is within the allowable tolerance. Convert the CBILLAMT */
		/* (the billing currency's amount) for multi-currency contracts. */
		/* By-pass the outstanding instalment totals. */
//		if (isLTE(wsaaSuspCbillDiff, wsaaToleranceAllowed)) {
		if (processDto.getWsaaSuspCbillDiff().compareTo(processDto.getWsaaToleranceAllowed()) <= 0) {
			processDto.setWsaaToleranceAllowed(processDto.getWsaaSuspCbillDiff());
			processDto.addWsaaSuspAvail(processDto.getWsaaSuspCbillDiff());
			if (StringUtils.equalsIgnoreCase(item.getCntcurr(), item.getBillcurr())) {
				item.setInstamt03(processDto.getWsaaToleranceAllowed());
			} else {
				processDto.setWsaaImRatio(processDto.getWsaaAmtPaid().divide(processDto.getWsaaNetCbillamt()).setScale(10));
//				compute(wsaaImRatio, 10).setRounded(div(wsaaAmtPaid, wsaaNetCbillamt));
				processDto.setWsaaCntcurrReceived(item.getInstamt06().multiply(processDto.getWsaaImRatio()).setScale(10));
//				compute(wsaaCntcurrReceived, 10).setRounded(mult(item.getInstamt06(), wsaaImRatio));
//				if (isNE(wsaaCntcurrReceived, 0)) {
				if (processDto.getWsaaCntcurrReceived().compareTo(BigDecimal.ZERO) != 0) {
					
					/*
					 * zrdecplrec.amountIn.set(wsaaCntcurrReceived);
					 * zrdecplrec.currency.set(linxpfResult.getCntcurr());
					 */
					processDto.setWsaaCntcurrReceived(c000CallRounding(processDto.getWsaaCntcurrReceived(), item.getCntcurr(), informBean));
				}
				item.setInstamt03(item.getInstamt06().subtract(processDto.getWsaaCntcurrReceived()));
			}
			return true;
		}
//		if (toleranceLimit1.isTrue() && isNE(wsaaMaxamt, 0)) {
		if (processDto.getWsaaToleranceChk().equals(L2CollectionsProcessDTO.WsaaToleranceChk.toleranceLimit1)
			&& wsaaMaxamt.compareTo(BigDecimal.ZERO) != 0) {
//			if (agtNotTerminated.isTrue() || (agtTerminated.isTrue() && isEQ(wsaaSfind, "2"))) {
				if (!processDto.isAgtTerminated() || (processDto.isAgtTerminated() && "2".equals(wsaaSfind))) {
					processDto.setWsaaToleranceChk(L2CollectionsProcessDTO.WsaaToleranceChk.toleranceLimit2);
				 return computeTolerance2820(item, processDto, informBean, l2collectioncontroltotaldto, wsaaPrmtol, wsaaPrmtoln, wsaaMaxAmount, wsaaMaxamt, wsaaSfind);
			}
		}
		return false;
	}
	
	private void failTolerance2855(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto, L2CollectionControlTotalDTO l2collectioncontroltotaldto) {
		/* PERFORM 3800-UNLK-CHDR. */
		/* Log no. LINS still short of the cbillamt. */
//		contot06++;
		l2collectioncontroltotaldto.addControlTotal06(BigDecimal.ONE);
		/* Log no. LINS not collected. */
//		contot04++;
		l2collectioncontroltotaldto.addControlTotal04((BigDecimal.ONE));
		processDto.setWsaaPrevLins("N");
		/* Increment the total of outstanding premiums. */
		processDto.setTemptot(item.getInstamt06());
//		contot05.add(temptot);
		l2collectioncontroltotaldto.addControlTotal05((processDto.getTemptot()));
		processDto.setWsaaToleranceAllowed(BigDecimal.ZERO);
		item.setSkiped(true);
	}
	
	private boolean validateContract2300(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto, BatchInformBean informBean) {
		/* For each contract retrieved we must look up the contract type */
		/* details on T5688 */
		long count = processDto.getWsaaT5688Recs().stream().filter( t5688Rec -> {
				if (t5688Rec.getWsaaT5688Cnttype().trim().equals(item.getChdrcnttype())
						&& t5688Rec.getWsaaT5688Itmfrm() <= item.getChdroccdate()) {
					item.setWsaaT5688Comlvlacc(t5688Rec.getWsaaT5688Comlvlacc());
					item.setWsaaT5688Revacc(t5688Rec.getWsaaT5688Revacc());
					return true;
				}
				return false;
			}).count();
		if (count <= 0) {
			throw new L2CollectionsException("E308 or H979 t5645:" + informBean.getProgramName() + item.getChdrnum());
		}
		

		/* Validate the statii of the contract */
		boolean wsaaValidContract = false;
		wsaaValidContract = processDto.getT5679().getCnRiskStats().stream().anyMatch(t5679rec -> {
			if (t5679rec.equals(item.getChdrstatcode())) {
				return processDto.getT5679().getCnPremStats().stream().anyMatch(prem -> prem.trim().equals(item.getChdrpstatcode()));
			}return false;
		});
		processDto.setWsaaPrevChdrnum(item.getChdrnum());
		return wsaaValidContract;
	}

	private BigDecimal b600GetTax(L2CollectionReaderDTO item) {
//		/* Read table TR52D */
//		List<TableItem<Tr52d>> tr52dList = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "Tr52d", Tr52d.class);
//		Tr52d tr52d = null;
//		for (TableItem<Tr52d> tableItem : tr52dList) {
//			if (tableItem.getItemitem().trim().equals(item.getChdrregister())) {
//				tr52d = tableItem.getItemDetail();
//				break;
//			}
//		}
//		if (tr52d == null) {
//			for (TableItem<Tr52d> tableItem : tr52dList) {
//				if (tableItem.getItemitem().trim().equals("***")) {
//					tr52d = tableItem.getItemDetail();
//					break;
//				}
//			}
//		}
		
		BigDecimal wsaaTotTax = BigDecimal.ZERO;
		if (item.getTaxdMap().containsKey(item.getChdrnum())) {
			List<Taxdpf> taxdpfList = item.getTaxdMap().get(item.getChdrnum());
			for (Taxdpf taxdpf : taxdpfList) {
				wsaaTotTax = b610ProcessTax(taxdpf, item.getInstfrom());
			}
		}
		return wsaaTotTax;
	}

	private BigDecimal b610ProcessTax(Taxdpf taxdpf, int instfrom) {
		BigDecimal wsaaTotTax = BigDecimal.ZERO;
		if (taxdpf.getInstfrom() == instfrom) {
			if (taxdpf.getPostflg() == null || taxdpf.getPostflg().trim().equals("")
					|| StringUtils.isBlank(taxdpf.getPostflg())) {
				if (!"Y".equals(taxdpf.getTxabsind01())) {
					wsaaTotTax = wsaaTotTax.add(taxdpf.getTaxamt01());
				}
				if (!"Y".equals(taxdpf.getTxabsind02())) {
					wsaaTotTax = wsaaTotTax.add(taxdpf.getTaxamt02());
				}
				if (!"Y".equals(taxdpf.getTxabsind03())) {
					wsaaTotTax = wsaaTotTax.add(taxdpf.getTaxamt03());
				}
			}
		}
		return wsaaTotTax;
	}

	private BigDecimal cashDivAvail2900a(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto) {
		BigDecimal wsaaDivRequired = BigDecimal.ZERO;
//		if (isGT(wsaaNetCbillamt, wsaaSuspAvail)) {
		if (processDto.getWsaaNetCbillamt().compareTo(processDto.getWsaaSuspAvail()) > 0) {
			if (checkDivAvailable2950a(item)) {
				return checkDivAmount2960a(item, processDto);
			}
		}
		return wsaaDivRequired;
	}

	private boolean checkDivAvailable2950a(L2CollectionReaderDTO item) {
		if (!StringUtils.isBlank(item.getHcsdzdivopt())) {
//			csdopt.divOption.set(item.getHcsdzdivopt());
//			if (csdopt.csdPremSettlement.isTrue()) {
			if (Csdopt.csdPremSettlement.toString().equals(item.getHcsdzdivopt().trim())) {
				return true;
			}
		}
		return false;
	}

	private BigDecimal checkDivAmount2960a(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto) {
		BigDecimal wsaaAcblSacscurbal = BigDecimal.ZERO;
		BigDecimal wsaaDivRequired = BigDecimal.ZERO;
		Acblpf acblpfResult = null;
		List<Acblpf> acblpfList = item.getAcblMap().get(item.getChdrnum());

			for (Acblpf acblpf : acblpfList) {
				if (acblpf.getOrigcurr().equals(item.getBillcurr())
//						&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[34])
//						&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[34])) {
						&& processDto.getWsaaT5645Sacscode()[2][2].equals(acblpf.getSacscode())
						&& processDto.getWsaaT5645Sacstype()[2][2].equals(acblpf.getSacstyp())) {
					acblpfResult = acblpf;
					break;
				}
			}
		// ILIFE-7911 ends
		if (acblpfResult != null) {
//			if (isEQ(wsaaT5645Sign[34], "-")) {
			if ("-".equals(processDto.getWsaaT5645Sign()[2][2])) {
				wsaaAcblSacscurbal = acblpfResult.getSacscurbal();
			} else {
				wsaaAcblSacscurbal = acblpfResult.getSacscurbal().negate();
//				compute(wsaaAcblSacscurbal, 2).set(sub(0, acblpfResult.getSacscurbal()));
			}
		} else {
			wsaaAcblSacscurbal = BigDecimal.ZERO;
		}
//		compute(wsaaDivRequired, 2).set(sub(wsaaNetCbillamt, wsaaSuspAvail));
		wsaaDivRequired = processDto.getWsaaNetCbillamt().subtract(processDto.getWsaaSuspAvail());
//		if (isLT(wsaaAcblSacscurbal, wsaaDivRequired)) {
//			wsaaDivRequired.set(wsaaAcblSacscurbal);
//		}
		if (wsaaAcblSacscurbal.compareTo(wsaaDivRequired) < 0) {
			wsaaDivRequired = (wsaaAcblSacscurbal);
		}
		processDto.addWsaaSuspAvail(wsaaDivRequired);
		return wsaaDivRequired;
	}
	
	private void calcTaxRelief2600(L2CollectionReaderDTO item, Clrrpf clrrpf, BigDecimal wsaaTotTax, BatchInformBean informBean, L2CollectionsProcessDTO processDto) {
		
		if (!processDto.getWsaaT6687Taxrelsubr().containsKey(item.getPayrtaxrelmth())) {
			throw new L2CollectionsException("B5353: failed t6687: " + item.getPayrtaxrelmth());
		}
		String payrTaxelsubr = processDto.getWsaaT6687Taxrelsubr().get(item.getPayrtaxrelmth());
		if (StringUtils.isBlank(payrTaxelsubr)) return;
		
		PrasDTO prasDTO = new PrasDTO();
		prasDTO.setTaxrelamt(BigDecimal.ZERO);
		prasDTO.setCnttype(item.getChdrcnttype());
		prasDTO.setClntnum(clrrpf.getClntnum());
		prasDTO.setClntcoy(clrrpf.getClntcoy());
		prasDTO.setIncomeSeqNo(item.getPayrincomeseqno());
		prasDTO.setTaxrelmth(item.getPayrtaxrelmth());
		prasDTO.setEffdate(item.getBillcd());
		prasDTO.setCompany(item.getChdrcoy());
		prasDTO.setGrossprem(item.getInstamt06().subtract(wsaaTotTax));
		
		try {
			prasDTO = prascalc.processPrascalc(prasDTO);
		} catch (IOException e) {
			throw new L2CollectionsException("B5353 fail in Prascalc " + prasDTO.toString());
		} catch (ParseException e) {
			throw new L2CollectionsException("B5353 fail in Prascalc " + prasDTO.toString());
		}
		if(prasDTO == null) {
			throw new L2CollectionsException("B5353 fail in Prascalc " + prasDTO.toString());
		}

		

		if (BigDecimal.ZERO.compareTo(prasDTO.getTaxrelamt()) == 0) {

			prasDTO.setTaxrelamt(c000CallRounding(prasDTO.getTaxrelamt(), item.getCntcurr(), informBean));
		}
		processDto.setWsaaNetCbillamt(item.getCbillamt().subtract(prasDTO.getTaxrelamt()));
		item.setPrasDTO(prasDTO);
//		compute(wsaaNetCbillamt, 2).set(sub(item.getCbillamt(), prasrec.taxrelamt));
	}

	private BigDecimal c000CallRounding(BigDecimal amountIn, String currency, BatchInformBean informBean) {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(currency);
		zrdecplDTO.setCompany(informBean.getBatchDetail().getBatccoy());
		zrdecplDTO.setBatctrcde(informBean.getBatchDetail().getBatctrcde());
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = zrdecplc.convertAmount(zrdecplDTO);
		} catch (IOException e) {
			throw new ItemParseException("B5353: Parsed failed in zrdecplc:" + e.getMessage());
		}
		return result;
	}
	
	
	private BigDecimal waiverHolding2900b(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto) {
		BigDecimal wsaaWaiverAvail = BigDecimal.ZERO;
		Acblpf acblpfResult = null;
		List<Acblpf> acblpfList = item.getAcblMap().get(item.getChdrnum());
			for (Acblpf acblpf : acblpfList) {
				if (acblpf.getOrigcurr().equals(item.getBillcurr())
//						&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[7])
//						&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[7])) {
						&& processDto.getWsaaT5645Sacscode()[0][6].equals(acblpf.getSacscode())
						&& processDto.getWsaaT5645Sacstype()[0][6].equals(acblpf.getSacstyp())) {
					acblpfResult = acblpf;
					break;
				}
			}
		// ILIFE-7911 ends
		if (acblpfResult != null) {
			wsaaWaiverAvail = acblpfResult.getSacscurbal();
		} else {
			wsaaWaiverAvail = BigDecimal.ZERO;
		}
		return wsaaWaiverAvail;
	}
	
	private void suspenseAvail2700(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto) {
		Acblpf acblpfResult = null;
		// Follow by MTI Ticket ILIFE-7869
		// List<Acblpf> acblpfList = acblMap.get(linxpfResult.getChdrnum());
		List<Acblpf> acblpfList = acblpfDAO.searchAcblenqRecord(item.getChdrcoy(), item.getChdrnum());
		// ILIFE-7911 starts
			for (Acblpf acblpf : acblpfList) {
				if (acblpf.getOrigcurr().equals(item.getBillcurr())
//						&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[1])
//						&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[1])) {
						&& processDto.getWsaaT5645Sacscode()[0][0].equals(acblpf.getSacscode())
						&& processDto.getWsaaT5645Sacstype()[0][0].equals(acblpf.getSacstyp())) {
					acblpfResult = acblpf;
					break;
				}
			}
		// ILIFE-7911 ends
		if (acblpfResult != null) {
			if ("-".equals(processDto.getT3695().getSign())) {
				processDto.setWsaaSuspAvail(acblpfResult.getSacscurbal().negate());
			} else {
				processDto.setWsaaSuspAvail(acblpfResult.getSacscurbal());
			}
		} else {
			processDto.setWsaaSuspAvail(BigDecimal.ZERO);
		}
	}
	
	private BigDecimal apaAvail2900(L2CollectionReaderDTO item, L2CollectionsProcessDTO processDto) {
		BigDecimal wsaaXPrmdepst = BigDecimal.ZERO;
		BigDecimal wsaaApaRequired = BigDecimal.ZERO;
		if (processDto.getWsaaNetCbillamt().compareTo(processDto.getWsaaSuspAvail()) > 0) {
			wsaaXPrmdepst = BigDecimal.ZERO;
			if (StringUtils.isBlank(processDto.getWsaaT5645Sacscode()[2][7]) || StringUtils.isBlank(processDto.getWsaaT5645Sacstype()[2][7]) 
					|| StringUtils.isBlank(processDto.getWsaaT5645Sacscode()[2][6])  || StringUtils.isBlank(processDto.getWsaaT5645Sacstype()[2][6])) {
				throw new L2CollectionsException("B5353: G450 T5645:");
			}
//			readPrincipalApa2960(item);
//			readInterestAccruel2965(item);
			List<Acblpf> acblpfList;
			String glChdrnum = item.getChdrnum();
			acblpfList = item.getAcblGLMap().get(glChdrnum + "00");
			if (acblpfList == null)
				acblpfList = item.getAcblMap().get(glChdrnum);
			for (Acblpf a : acblpfList) {
				if (a.getSacscode().equals(processDto.getWsaaT5645Sacscode()[2][6]) && a.getSacscode().equals(processDto.getWsaaT5645Sacstype()[2][6])) {
					if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) != 0) {
						/* Negate the Account balances if it is negative (for the */
						/* case of Cash accounts). */
						if (a.getSacscurbal().compareTo(BigDecimal.ZERO) < 0) {
							a.setSacscurbal(a.getSacscurbal().negate());
						}
						wsaaXPrmdepst.add((a.getSacscurbal()));
					}
					
				}
				if (a.getSacscode().equals(processDto.getWsaaT5645Sacscode()[2][7]) && a.getSacscode().equals(processDto.getWsaaT5645Sacstype()[2][7])) {
					if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) != 0) {
						/* Negate the Account balances if it is negative (for the */
						/* case of Cash accounts). */
						if (a.getSacscurbal().compareTo(BigDecimal.ZERO) < 0) {
							a.setSacscurbal(a.getSacscurbal().negate());
						}
						wsaaXPrmdepst.add(a.getSacscurbal());
					}
				}
			}
			wsaaApaRequired = processDto.getWsaaNetCbillamt().subtract(processDto.getWsaaSuspAvail());
			if (wsaaXPrmdepst.compareTo(wsaaApaRequired) < 0) {
				wsaaApaRequired = wsaaXPrmdepst;
			}
			/* Sum the total suspense avaible to one variable. Makesure the */
			/* APA amount is book into suspense UPDATE-SUSPENSE in 3000-UPDATE */
			processDto.addWsaaSuspAvail(wsaaApaRequired);
		}
		return wsaaApaRequired;
	}
	
	private boolean a400CheckAgent(L2CollectionReaderDTO item, BatchInformBean informBean) {
		if (item.getAgldtetrm() < DatimeUtil.DateToInt(informBean.getBsscpf().getBprceffdat())
				|| item.getAglfdteexp() < DatimeUtil.DateToInt(informBean.getBsscpf().getBprceffdat())
				|| item.getAglfdteapp() < DatimeUtil.DateToInt(informBean.getBsscpf().getBprceffdat())) {
			return true;
		}
		return false;
	}
}
