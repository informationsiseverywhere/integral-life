package com.dxc.integral.life.general.processors;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.L2BillingControlTotalDTO;
import com.dxc.integral.life.beans.L2BillingProcessorDTO;
import com.dxc.integral.life.beans.L2BillingReaderDTO;
import com.dxc.integral.life.utils.L2billingProcessorUtil;

/**
 * ItemProcessor for L2POLRNWL Billing batch step.
 * 
 * @author yyang21
 *
 */
@Scope(value = "step")
@Async
@Lazy
public class L2BillingItemProcessor extends BaseProcessor
		implements ItemProcessor<L2BillingReaderDTO, L2BillingReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2BillingItemProcessor.class);

	/** The batch transaction code for L2POLRNWL */
	private static final String BATCH_TRAN_CODE = "B521";

	@Autowired
	private Datcon2 datcon2;

	@Autowired
	private L2billingProcessorUtil l2billingProcessorUtil;
	@Autowired
	private L2BillingControlTotalDTO l2BillingControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public L2BillingReaderDTO process(L2BillingReaderDTO readerDTO) throws Exception {
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		Usrdpf usrdpf = getUserDetails(userName);
		String tempDate = transactionDate;
		if (transactionDate.indexOf("-") < 0) {
			tempDate = DatimeUtil.covertToDDmmyyyyDate(transactionDate);
		}
		Batcpf batchDetail = getBatcpfInfo(tempDate, businessDate, Companies.LIFE, BATCH_TRAN_CODE, branch, usrdpf,
				batchName);
		// ///////////initialise1000//////////
		L2BillingProcessorDTO l2BillingProcessorDTO = new L2BillingProcessorDTO();
		l2BillingProcessorDTO.setBprdAuthCode(BATCH_TRAN_CODE);
		l2BillingProcessorDTO.setActmonth(getActMonth(transactionDate));
		l2BillingProcessorDTO.setActyear(getActYear(transactionDate));
		l2BillingProcessorDTO.setEffDate(Integer.parseInt(transactionDate));
		l2BillingProcessorDTO.setBillcd(readerDTO.getBillcd());
		l2BillingProcessorDTO.setUserName(userName);
		l2BillingProcessorDTO.setBatchName(batchName);
		l2BillingProcessorDTO.setLang(usrdpf.getLanguage());
		l2BillingProcessorDTO.setPrefix(batchDetail.getBatcpfx());
		l2BillingProcessorDTO.setBatch(batchDetail.getBatcbatch());
		l2BillingProcessorDTO.setBatchTrcde(batchDetail.getBatctrcde());
		l2BillingProcessorDTO.setL2BillingControlTotalDTO(l2BillingControlTotalDTO);
		l2BillingProcessorDTO.setOldOutstamt(readerDTO.getOutstamt());
		l2BillingProcessorDTO.setOldBtdate(readerDTO.getBtdate());
		l2BillingProcessorDTO.setOldBillcd(readerDTO.getPayrbillcd());
		l2BillingProcessorDTO.setOldNextdate(readerDTO.getNextdate());
		l2BillingProcessorDTO.setBranch(branch);

		// ///////////edit//////////
		if (!l2billingProcessorUtil.validContract(l2BillingProcessorDTO, readerDTO)) {
			LOGGER.info("Invalid contract : {}", readerDTO.getChdrnum());//IJTI-1498
			l2BillingControlTotalDTO.setControlTotal07(BigDecimal.ONE);
			return null;
		}
		boolean flexFlag = l2billingProcessorUtil.validateFlexibleContract(readerDTO);
		l2BillingProcessorDTO.setFlexiblePremiumContract(flexFlag);

		int leadDays = l2billingProcessorUtil.readLeadDays(readerDTO, l2BillingProcessorDTO);
		l2BillingProcessorDTO.setLeadDays(leadDays);

		int effdatePlusCntlead = datcon2.plusDays(l2BillingProcessorDTO.getEffDate(), leadDays);
		l2BillingProcessorDTO.setEffdatePlusCntlead(effdatePlusCntlead);
		/* Once we have calculated the lead days for this contract */
		/* type we can compare it to the BILLCD date to see if the */
		/* contract requires billing. */
		if (readerDTO.getBillcd() > effdatePlusCntlead) {
			return null;
		}

		/* If suppressed billing, check if the present bill date is within */
		/* the suppressed period. */
		if ("Y".equals(readerDTO.getBillsupr())) {
			/* IF BILLSPFROM NOT > WSAA-EFFDATE-PLUS-CNTLEAD */
			/* AND BILLSPTO NOT < WSAA-EFFDATE-PLUS-CNTLEAD */
			if (readerDTO.getBillcd() >= readerDTO.getBillspfrom() && readerDTO.getBillcd() < readerDTO.getBillspto()
					&& l2BillingProcessorDTO.getEffDate() < readerDTO.getBillspto()) {
				l2BillingControlTotalDTO.setControlTotal02(BigDecimal.ONE);
				return null;
			}
		}

		boolean slkFlag = softlock(readerDTO.getChdrnum(), BATCH_TRAN_CODE, batchName);
		if (!slkFlag) {
			LOGGER.info("Contract is already locked, chdrnum: {}", readerDTO.getChdrnum());//IJTI-1498
			l2BillingControlTotalDTO.setControlTotal08(BigDecimal.ONE);
			return null;
		}

		// ///////////update//////////
		l2billingProcessorUtil.loadSmartTables(l2BillingProcessorDTO, readerDTO);

		l2billingProcessorUtil.updateRecord(l2BillingProcessorDTO, readerDTO);

		releaseSoftlock(readerDTO.getChdrcoy(), readerDTO.getChdrnum(), BATCH_TRAN_CODE, userName, batchName,
				batchDetail.getBatctrcde());
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		return readerDTO;
	}
}