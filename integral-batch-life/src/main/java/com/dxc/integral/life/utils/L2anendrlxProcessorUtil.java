package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.L2anendrlxReaderDTO;
import com.dxc.integral.life.beans.L2anendrlxProcessorDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;

/**
 * Processor Utility for L2ANENDRLX batch.
 * 
 * @author mmalik25
 *
 */
public interface L2anendrlxProcessorUtil {
	
	/**
	 * @param readerDTO
	 * @param tranDate
	 * @return
	 * @throws IOException
	 */
	public L2anendrlxProcessorDTO getPaymnetDueInfo(L2anendrlxReaderDTO readerDTO, Integer tranDate) throws IOException ;/*ILIFE-5977*/

	/**
	 * @param readerDTO
	 * @param t5645IO
	 * @param processorDTO
	 * @param longdesc
	 * @return
	 * @throws IOException
	 */
	public ZrcshopDTO populateZrcshopDTO(L2anendrlxReaderDTO readerDTO, T5645 t5645IO,
			String longdesc) throws IOException;/*ILIFE-5977*/
	
	/**
	 * @param readerDTO
	 * @return
	 * @throws IOException
	 * @throws ItemNotfoundException 
	 */
	public boolean checkValidCoverage(L2anendrlxReaderDTO readerDTO) throws IOException;//ILIFE-5763 /*ILIFE-5977*/
	
	/**
	 * @param processorDTO
	 * @param readerDTO
	 * @param usrprf
	 * @return
	 */
	public L2anendrlxReaderDTO updateReaderDTOInfo(L2anendrlxProcessorDTO processorDTO, 
			L2anendrlxReaderDTO readerDTO);

	/**
	 * @param readerDTO
	 * @param t5645IO
	 * @param longdesc
	 * @return
	 */
	public CrtloanDTO populateCrtloanDTO(L2anendrlxReaderDTO readerDTO, T5645 t5645IO,
			String longdesc) ;
}
