package com.dxc.integral.life.general.writers;

import java.io.IOException;
import java.io.Writer;
import org.springframework.batch.item.file.FlatFileHeaderCallback;

/**
 * Writes header to Flat File.
 *  
 * @author gsaluja2
 *
 */
public class L2lincsndHeaderWriter implements FlatFileHeaderCallback{

	private final String header;
	 
	public L2lincsndHeaderWriter(String exportFileHeader) {
        this.header = exportFileHeader;
    }
    
	@Override
	public void writeHeader(Writer writer) throws IOException {
		writer.write(header);
	}
}
