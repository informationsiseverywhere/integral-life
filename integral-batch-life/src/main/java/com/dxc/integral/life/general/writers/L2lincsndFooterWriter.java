package com.dxc.integral.life.general.writers;

import java.io.IOException;
import java.io.Writer;
import org.springframework.batch.item.file.FlatFileFooterCallback;

/**
 * Writes footer to Flat File.
 * 
 * @author gsaluja2
 *
 */
public class L2lincsndFooterWriter implements FlatFileFooterCallback{

	private final String footer;
	
	public L2lincsndFooterWriter(String exportFileFooter) {
		this.footer = exportFileFooter;
	}
	
	@Override
	public void writeFooter(Writer writer) throws IOException {
		writer.write(footer);
	}

}
