package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5010 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S5010screensfl s5010screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S5010screensfl getS5010screensfl() {
		return s5010screensfl;
	}
	public void setS5010screensfl(S5010screensfl s5010screensfl) {
		this.s5010screensfl = s5010screensfl;
	}


	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class S5010screensfl implements Serializable {
		private static final long serialVersionUID = 1L;

		private List<KeyValueItem> actions;

		
		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
		
		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String cltreln;
			private String bnycd;
			private String bnypc;	
			private String bnysel;		
			private String sequence;	
			private String clntsname;
			private String effdateDisp;
			private String bnytype;
			private String relto;
			private String revcflg;
			private String enddateDisp;

			public String getCltreln() {
				return cltreln;
			}
			public void setCltreln(String cltreln) {
				this.cltreln = cltreln;
			}
			public String getBnycd() {
				return bnycd;
			}
			public void setBnycd(String bnycd) {
				this.bnycd = bnycd;
			}
			public String getBnypc() {
				return bnypc;
			}
			public void setBnypc(String bnypc) {
				this.bnypc = bnypc;
			}
			public String getBnysel() {
				return bnysel;
			}
			public void setBnysel(String bnysel) {
				this.bnysel = bnysel;
			}
			public String getSequence() {
				return sequence;
			}
			public void setSequence(String sequence) {
				this.sequence = sequence;
			}
			public String getClntsname() {
				return clntsname;
			}
			public void setClntsname(String clntsname) {
				this.clntsname = clntsname;
			}
			public String getEffdateDisp() {
				return effdateDisp;
			}
			public void setEffdateDisp(String effdateDisp) {
				this.effdateDisp = effdateDisp;
			}
			public String getBnytype() {
				return bnytype;
			}
			public void setBnytype(String bnytype) {
				this.bnytype = bnytype;
			}
			public String getRelto() {
				return relto;
			}
			public void setRelto(String relto) {
				this.relto = relto;
			}
			public String getRevcflg() {
				return revcflg;
			}
			public void setRevcflg(String revcflg) {
				this.revcflg = revcflg;
			}
			public String getEnddateDisp() {
				return enddateDisp;
			}
			public void setEnddateDisp(String enddateDisp) {
				this.enddateDisp = enddateDisp;
			}
		} 
	}


	@Override
	public String toString() {
		return "S5010 []";
	}
	
}
