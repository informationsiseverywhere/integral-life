package com.dxc.integral.life.general.processors;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

import com.dxc.integral.fsu.utils.FeaConfg;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.L2AutoIncrControlTotalDTO;
import com.dxc.integral.life.beans.L2AutoIncrProcessorDTO;
import com.dxc.integral.life.beans.L2AutoIncrReaderDTO;
import com.dxc.integral.life.utils.L2AutoIncrProcessorUtil;

/**
 * ItemProcessor for PENDING AUTOMATIC INCREASES batch step.
 * 
 * @author yyang21
 *
 */
@Scope(value = "step")
@Lazy
public class L2AutoIncrItemProcessor extends BaseItemProcessor<L2AutoIncrReaderDTO, L2AutoIncrReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AutoIncrItemProcessor.class);

	/** The batch transaction code for PENDING AUTOMATIC INCREASES */

	@Autowired
	private L2AutoIncrProcessorUtil l2AutoIncrProcessorUtil;
	@Autowired
	private L2AutoIncrControlTotalDTO l2AutoIncrControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;

	@Autowired
	private Environment env;

	private String wsysChdrnum;
	private String wsysCoverage;
	private BigDecimal wsaaOriginst01 = BigDecimal.ZERO;
	private BigDecimal wsaaOriginst02 = BigDecimal.ZERO;
	private BigDecimal wsaaOriginst03 = BigDecimal.ZERO;
	private BigDecimal wsaaOrigsum = BigDecimal.ZERO;
	private String wsaaChdrChdrcoy;
	private String wsaaChdrChdrnum;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public L2AutoIncrReaderDTO process(L2AutoIncrReaderDTO readerDTO) throws Exception {
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrNum());//IJTI-1498
 
		Usrdpf usrdpf = getUsrdpf();
		Batcpf batchDetail = getBatchDetail();

		// ///////////initialise1000//////////
		boolean vpmsFlag = env.getProperty("vpms.flag").equals("1");
		L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO = new L2AutoIncrProcessorDTO();
		l2AutoIncrProcessorDTO.setVpmsFlag(vpmsFlag);
		l2AutoIncrProcessorDTO.setPermission(FeaConfg.isFeatureExist(readerDTO.getChdrCoy(), "NBPROP08", "IT"));
		l2AutoIncrProcessorDTO.setPrmhldtrad(FeaConfg.isFeatureExist(readerDTO.getChdrCoy(), "CSOTH010", "IT"));
		l2AutoIncrProcessorDTO.setTransTime(getVrcmTime());
		l2AutoIncrProcessorDTO.setBprdAuthCode(batchDetail.getBatctrcde());
		l2AutoIncrProcessorDTO.setEffDate(Integer.parseInt(transactionDate));
		l2AutoIncrProcessorDTO.setWsaaSysparam03(this.getBprdpf().getBpsyspar03());
		l2AutoIncrProcessorDTO.setStampDutyflag(FeaConfg.isFeatureExist(readerDTO.getChdrCoy(), "NBPROP01", "IT"));
		l2AutoIncrProcessorDTO.setPrmhldtrad(FeaConfg.isFeatureExist(readerDTO.getChdrCoy(), "CSOTH010", "IT"));
		l2AutoIncrProcessorDTO.setLang(usrdpf.getLanguage());
		l2AutoIncrProcessorDTO.setDatimeInit(getDatimeInit());
		l2AutoIncrProcessorDTO.setL2AutoIncrControlTotalDTO(l2AutoIncrControlTotalDTO);
		l2AutoIncrProcessorDTO.setUserName(userName);
		l2AutoIncrProcessorDTO.setBatchName(batchName);

		l2AutoIncrProcessorDTO.setPrefix(batchDetail.getBatcpfx());
		l2AutoIncrProcessorDTO.setBatcbrn(batchDetail.getBatcbrn());
		l2AutoIncrProcessorDTO.setActmonth(getActMonth(transactionDate));
		l2AutoIncrProcessorDTO.setActyear(getActYear(transactionDate));
		l2AutoIncrProcessorDTO.setBatchTrcde(batchDetail.getBatctrcde());
		l2AutoIncrProcessorDTO.setBatch(batchDetail.getBatcbatch());

		l2AutoIncrControlTotalDTO.setControlTotal01(BigDecimal.ONE);
		l2AutoIncrProcessorUtil.loadSmartTables(l2AutoIncrProcessorDTO, readerDTO);
		//////////////////// read2000//////////////
		l2AutoIncrProcessorDTO.setNewContract(true);
		if (l2AutoIncrProcessorDTO.isPrmhldtrad()) {
			if (readerDTO.getChdrNum().equals(wsysChdrnum)) {
				l2AutoIncrProcessorDTO.setNewContract(false);
			}
		} else {
			if (readerDTO.getChdrNum().equals(wsysChdrnum) && readerDTO.getCoverage().equals(wsysCoverage)) {
				l2AutoIncrProcessorDTO.setNewContract(false);
			}
		}

		// l2AutoIncrProcessorUtil.readLeadDays(l2AutoIncrProcessorDTO, readerDTO);
		////////////////////////// edit2500
		l2AutoIncrProcessorDTO.setWsaaOriginst01(wsaaOriginst01);
		l2AutoIncrProcessorDTO.setWsaaOriginst02(wsaaOriginst02);
		l2AutoIncrProcessorDTO.setWsaaOriginst03(wsaaOriginst03);
		l2AutoIncrProcessorDTO.setWsaaOrigsum(wsaaOrigsum);
		l2AutoIncrProcessorDTO.setWsysChdrnum(wsysChdrnum);
		l2AutoIncrProcessorDTO.setWsysCoverage(wsysCoverage);
		l2AutoIncrProcessorDTO.setWsaaChdrChdrcoy(wsaaChdrChdrcoy);
		l2AutoIncrProcessorDTO.setWsaaChdrChdrnum(wsaaChdrChdrnum);
		boolean validFlag = l2AutoIncrProcessorUtil.validContract(l2AutoIncrProcessorDTO, readerDTO);
		if (!validFlag) {
			LOGGER.error("invalid contract : {}", readerDTO.getChdrNum());//IJTI-1498
			l2AutoIncrControlTotalDTO.setControlTotal09(BigDecimal.ONE);
			return readerDTO;
		}
		////////////////////////// update3000
		boolean slkFlag = l2AutoIncrProcessorUtil.softlock(readerDTO.getChdrNum(),
				l2AutoIncrProcessorDTO.getBprdAuthCode(), batchName);
		if (!slkFlag && (wsysChdrnum == null || !wsysChdrnum.equals(readerDTO.getChdrNum()))) {
			LOGGER.error("Contract is already locked, chdrnum: {}", readerDTO.getChdrNum());//IJTI-1498
			l2AutoIncrControlTotalDTO.setControlTotal11(BigDecimal.ONE);
			return readerDTO;
		}
		wsysChdrnum = readerDTO.getChdrNum();
		wsysCoverage = readerDTO.getCoverage();
		l2AutoIncrProcessorUtil.updateRecord(l2AutoIncrProcessorDTO, readerDTO);
		wsaaOriginst01 = l2AutoIncrProcessorDTO.getWsaaOriginst01();
		wsaaOriginst02 = l2AutoIncrProcessorDTO.getWsaaOriginst02();
		wsaaOriginst03 = l2AutoIncrProcessorDTO.getWsaaOriginst03();
		wsaaOrigsum = l2AutoIncrProcessorDTO.getWsaaOrigsum();
		wsaaChdrChdrcoy = l2AutoIncrProcessorDTO.getWsaaChdrChdrcoy();
		wsaaChdrChdrnum = l2AutoIncrProcessorDTO.getWsaaChdrChdrnum();
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrNum());//IJTI-1498
		return readerDTO;
	}

	private int getActMonth(String transDate) throws ParseException {
		if (transDate.indexOf('-') == -1) {
			transDate = DatimeUtil.covertToDDmmyyyyDate(transDate);
		}
		String[] transactionDateParts = transDate.split("-");
		return Integer.parseInt(transactionDateParts[1]);
	}

	private int getActYear(String transDate) throws ParseException {
		if (transDate.indexOf('-') == -1) {
			transDate = DatimeUtil.covertToDDmmyyyyDate(transDate);
		}
		String[] transactionDateParts = transDate.split("-");
		return Integer.parseInt(transactionDateParts[2]);
	}

	private int getVrcmTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		return Integer.parseInt(formatter.format(LocalTime.now()));
	}

	private String getDatimeInit() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("SSSSSS");
		return formatter.format(LocalTime.now());
	}
}