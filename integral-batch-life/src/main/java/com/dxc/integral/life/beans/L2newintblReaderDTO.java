package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class L2newintblReaderDTO {
	private Long lo_unique_number;
	private String lo_chdrcoy;
	private String lo_chdrnum;
  	private Integer lo_loannumber;
  	private String lo_loantype;
  	private String lo_loancurr;
  	private String lo_validflag;
  	private Integer lo_ftranno;
  	private Integer lo_ltranno;
  	private BigDecimal lo_loanorigam;
  	private Integer lo_loanstdate;
  	private BigDecimal lo_lstcaplamt;
    private Integer lo_lstcapdate; 
  	private Integer lo_nxtcapdate;
  	private Integer lo_lstintbdte;
  	private Integer lo_nxtintbdte;
  	private BigDecimal lo_tplstmdty;
  	private String lo_termid;
  	private Integer lo_user_t;
  	private Integer lo_trdt;
	private Integer lo_trtm;
	private String lo_usrprf;
  	private String lo_jobnm;
  	private Long ch_unique_number;
  	private Integer ch_currfrom;
  	private Integer ch_tranno;
  	private String ch_cnttype;
  	private String ch_chdrpfx;
  	private String ch_servunit;
  	private Integer ch_occdate;
  	private Integer ch_ccdate;
  	private String ch_cownpfx;
  	private String ch_cowncoy;
	private String ch_cownnum;
	private String ch_collchnl;
	private String ch_cntbranch;
	private String ch_agntpfx;
	private String ch_agntcoy;
	private String ch_agntnum;
	private String cl_clntpfx;
	private String cl_clntcoy;
	private String cl_clntnum;
	private String py_mandref;
	private int py_ptdate;
	private String py_billchnl;
	private String py_billfreq;
	private String py_billcurr;
	private int py_nextdate;
	private String ma_bankkey;
	private String ma_bankacckey;	
	private String ma_mandstat;
	private String  cb_facthous;
	private String bankcode;
	private BigDecimal interestAmount;
	private String Sacscode;
	private String SacsType;
	private String glMap;
	private String batcpfx;
	private String batccoy;
	private String batcbrn;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batctrcde;
	private String batcbatch;
	private Integer effectiveDate;
	
	public Integer getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Integer effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getSacscode() {
		return Sacscode;
	}
	public void setSacscode(String sacscode) {
		Sacscode = sacscode;
	}
	public String getSacsType() {
		return SacsType;
	}
	public void setSacsType(String sacsType) {
		SacsType = sacsType;
	}
	public String getGlMap() {
		return glMap;
	}
	public void setGlMap(String glMap) {
		this.glMap = glMap;
	}

   public BigDecimal getInterestAmount() {
		return interestAmount;
	}
	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public Long getLo_unique_number() {
		return lo_unique_number;
	}
	public void setLo_unique_number(Long lo_unique_number) {
		this.lo_unique_number = lo_unique_number;
	}
	public String getLo_chdrcoy() {
		return lo_chdrcoy;
	}
	public void setLo_chdrcoy(String lo_chdrcoy) {
		this.lo_chdrcoy = lo_chdrcoy;
	}
	public String getLo_chdrnum() {
		return lo_chdrnum;
	}
	public void setLo_chdrnum(String lo_chdrnum) {
		this.lo_chdrnum = lo_chdrnum;
	}
	public Integer getLo_loannumber() {
		return lo_loannumber;
	}
	public void setLo_loannumber(Integer lo_loannumber) {
		this.lo_loannumber = lo_loannumber;
	}
	public String getLo_loantype() {
		return lo_loantype;
	}
	public void setLo_loantype(String lo_loantype) {
		this.lo_loantype = lo_loantype;
	}
	public String getLo_loancurr() {
		return lo_loancurr;
	}
	public void setLo_loancurr(String lo_loancurr) {
		this.lo_loancurr = lo_loancurr;
	}
	public String getLo_validflag() {
		return lo_validflag;
	}
	public void setLo_validflag(String lo_validflag) {
		this.lo_validflag = lo_validflag;
	}
	public Integer getLo_ftranno() {
		return lo_ftranno;
	}
	public void setLo_ftranno(Integer lo_ftranno) {
		this.lo_ftranno = lo_ftranno;
	}
	public Integer getLo_ltranno() {
		return lo_ltranno;
	}
	public void setLo_ltranno(Integer lo_ltranno) {
		this.lo_ltranno = lo_ltranno;
	}
	public BigDecimal getLo_loanorigam() {
		return lo_loanorigam;
	}
	public void setLo_loanorigam(BigDecimal lo_loanorigam) {
		this.lo_loanorigam = lo_loanorigam;
	}
	public Integer getLo_loanstdate() {
		return lo_loanstdate;
	}
	public void setLo_loanstdate(Integer lo_loanstdate) {
		this.lo_loanstdate = lo_loanstdate;
	}
	public BigDecimal getLo_lstcaplamt() {
		return lo_lstcaplamt;
	}
	public void setLo_lstcaplamt(BigDecimal lo_lstcaplamt) {
		this.lo_lstcaplamt = lo_lstcaplamt;
	}
	public Integer getLo_lstcapdate() {
		return lo_lstcapdate;
	}
	public void setLo_lstcapdate(Integer lo_lstcapdate) {
		this.lo_lstcapdate = lo_lstcapdate;
	}
	public Integer getLo_nxtcapdate() {
		return lo_nxtcapdate;
	}
	public void setLo_nxtcapdate(Integer lo_nxtcapdate) {
		this.lo_nxtcapdate = lo_nxtcapdate;
	}
	public Integer getLo_lstintbdte() {
		return lo_lstintbdte;
	}
	public void setLo_lstintbdte(Integer lo_lstintbdte) {
		this.lo_lstintbdte = lo_lstintbdte;
	}
	public Integer getLo_nxtintbdte() {
		return lo_nxtintbdte;
	}
	public void setLo_nxtintbdte(Integer lo_nxtintbdte) {
		this.lo_nxtintbdte = lo_nxtintbdte;
	}
	public BigDecimal getLo_tplstmdty() {
		return lo_tplstmdty;
	}
	public void setLo_tplstmdty(BigDecimal lo_tplstmdty) {
		this.lo_tplstmdty = lo_tplstmdty;
	}
	public String getLo_termid() {
		return lo_termid;
	}
	public void setLo_termid(String lo_termid) {
		this.lo_termid = lo_termid;
	}
	public Integer getLo_user_t() {
		return lo_user_t;
	}
	public void setLo_user_t(Integer lo_user_t) {
		this.lo_user_t = lo_user_t;
	}
	public Integer getLo_trdt() {
		return lo_trdt;
	}
	public void setLo_trdt(Integer lo_trdt) {
		this.lo_trdt = lo_trdt;
	}
	public Integer getLo_trtm() {
		return lo_trtm;
	}
	public void setLo_trtm(Integer lo_trtm) {
		this.lo_trtm = lo_trtm;
	}
	public String getLo_usrprf() {
		return lo_usrprf;
	}
	public void setLo_usrprf(String lo_usrprf) {
		this.lo_usrprf = lo_usrprf;
	}
	public String getLo_jobnm() {
		return lo_jobnm;
	}
	public void setLo_jobnm(String lo_jobnm) {
		this.lo_jobnm = lo_jobnm;
	}
	public Long getCh_unique_number() {
		return ch_unique_number;
	}
	public void setCh_unique_number(Long ch_unique_number) {
		this.ch_unique_number = ch_unique_number;
	}
	public Integer getCh_currfrom() {
		return ch_currfrom;
	}
	public void setCh_currfrom(Integer ch_currfrom) {
		this.ch_currfrom = ch_currfrom;
	}
	public Integer getCh_tranno() {
		return ch_tranno;
	}
	public void setCh_tranno(Integer ch_tranno) {
		this.ch_tranno = ch_tranno;
	}
	public String getCh_cnttype() {
		return ch_cnttype;
	}
	public void setCh_cnttype(String ch_cnttype) {
		this.ch_cnttype = ch_cnttype;
	}
	public String getCh_chdrpfx() {
		return ch_chdrpfx;
	}
	public void setCh_chdrpfx(String ch_chdrpfx) {
		this.ch_chdrpfx = ch_chdrpfx;
	}
	public String getCh_servunit() {
		return ch_servunit;
	}
	public void setCh_servunit(String ch_servunit) {
		this.ch_servunit = ch_servunit;
	}
	public Integer getCh_occdate() {
		return ch_occdate;
	}
	public void setCh_occdate(Integer ch_occdate) {
		this.ch_occdate = ch_occdate;
	}
	public Integer getCh_ccdate() {
		return ch_ccdate;
	}
	public void setCh_ccdate(Integer ch_ccdate) {
		this.ch_ccdate = ch_ccdate;
	}
	public String getCh_cownpfx() {
		return ch_cownpfx;
	}
	public void setCh_cownpfx(String ch_cownpfx) {
		this.ch_cownpfx = ch_cownpfx;
	}
	public String getCh_cowncoy() {
		return ch_cowncoy;
	}
	public void setCh_cowncoy(String ch_cowncoy) {
		this.ch_cowncoy = ch_cowncoy;
	}
	public String getCh_cownnum() {
		return ch_cownnum;
	}
	public void setCh_cownnum(String ch_cownnum) {
		this.ch_cownnum = ch_cownnum;
	}
	public String getCh_collchnl() {
		return ch_collchnl;
	}
	public void setCh_collchnl(String ch_collchnl) {
		this.ch_collchnl = ch_collchnl;
	}
	public String getCh_cntbranch() {
		return ch_cntbranch;
	}
	public void setCh_cntbranch(String ch_cntbranch) {
		this.ch_cntbranch = ch_cntbranch;
	}
	public String getCh_agntpfx() {
		return ch_agntpfx;
	}
	public void setCh_agntpfx(String ch_agntpfx) {
		this.ch_agntpfx = ch_agntpfx;
	}
	public String getCh_agntcoy() {
		return ch_agntcoy;
	}
	public void setCh_agntcoy(String ch_agntcoy) {
		this.ch_agntcoy = ch_agntcoy;
	}
	public String getCh_agntnum() {
		return ch_agntnum;
	}
	public void setCh_agntnum(String ch_agntnum) {
		this.ch_agntnum = ch_agntnum;
	}
	public String getCl_clntpfx() {
		return cl_clntpfx;
	}
	public void setCl_clntpfx(String cl_clntpfx) {
		this.cl_clntpfx = cl_clntpfx;
	}
	public String getCl_clntcoy() {
		return cl_clntcoy;
	}
	public void setCl_clntcoy(String cl_clntcoy) {
		this.cl_clntcoy = cl_clntcoy;
	}
	public String getCl_clntnum() {
		return cl_clntnum;
	}
	public void setCl_clntnum(String cl_clntnum) {
		this.cl_clntnum = cl_clntnum;
	}
	public String getPy_mandref() {
		return py_mandref;
	}
	public void setPy_mandref(String py_mandref) {
		this.py_mandref = py_mandref;
	}
	public int getPy_ptdate() {
		return py_ptdate;
	}
	public void setPy_ptdate(int py_ptdate) {
		this.py_ptdate = py_ptdate;
	}
	public String getPy_billchnl() {
		return py_billchnl;
	}
	public void setPy_billchnl(String py_billchnl) {
		this.py_billchnl = py_billchnl;
	}
	public String getPy_billfreq() {
		return py_billfreq;
	}
	public void setPy_billfreq(String py_billfreq) {
		this.py_billfreq = py_billfreq;
	}
	public String getPy_billcurr() {
		return py_billcurr;
	}
	public void setPy_billcurr(String py_billcurr) {
		this.py_billcurr = py_billcurr;
	}
	public int getPy_nextdate() {
		return py_nextdate;
	}
	public void setPy_nextdate(int py_nextdate) {
		this.py_nextdate = py_nextdate;
	}
	public String getMa_bankkey() {
		return ma_bankkey;
	}
	public void setMa_bankkey(String ma_bankkey) {
		this.ma_bankkey = ma_bankkey;
	}
	public String getMa_bankacckey() {
		return ma_bankacckey;
	}
	public void setMa_bankacckey(String ma_bankacckey) {
		this.ma_bankacckey = ma_bankacckey;
	}
	public String getMa_mandstat() {
		return ma_mandstat;
	}
	public void setMa_mandstat(String ma_mandstat) {
		this.ma_mandstat = ma_mandstat;
	}
	public String getCb_facthous() {
		return cb_facthous;
	}
	public void setCb_facthous(String cb_facthous) {
		this.cb_facthous = cb_facthous;
	}
	
	
	}
