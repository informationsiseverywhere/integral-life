package com.dxc.integral.life.general.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import com.dxc.integral.life.beans.L2lincReaderDTO;
import com.dxc.integral.life.general.mappers.L2lincFieldSetMapper;
import com.dxc.integral.life.utils.L2lincReaderUtil;

public class L2rlncReader implements ItemReader<L2lincReaderDTO> {
	
	@Value("#{stepExecutionContext['fileResource']}")
	private Resource resource;
	
	private FlatFileItemReader<L2lincReaderDTO> reader = null;
	private List<L2lincReaderDTO> list = new ArrayList<>();
	private int recordCount = 0;
	private L2lincReaderDTO test = null; 
	private String recieveFolder;                     
	@Autowired
	private Environment env;
	
	public L2rlncReader() {
		//Added for removing Sonar Qube Errors
	};
	
	public synchronized void reader() throws Exception {
		if(reader==null)
			 reader = new FlatFileItemReader<>();
		
		recieveFolder = env.getProperty("report.output.path") + env.getProperty("folder.linc.receive") 
		+ env.getProperty("folder.archive");
		
		File f1=FileSystems.getDefault().getPath(recieveFolder + resource.getFilename()).toFile(); 
	    int linecount=0;            
	    FileReader fr=new FileReader(f1); 
	    BufferedReader br = new BufferedReader(fr);    
	    try {
	    	String s;
	 	    while((s=br.readLine())!=null)   
	 	    {
	 	        linecount++;              
	 	    }
	    }
	   finally {
		    br.close();
		    fr.close();
	   }
		reader.setEncoding("UTF-8");
		reader.setResource(new FileSystemResource(recieveFolder + resource.getFilename()));  
		L2lincReaderUtil<L2lincReaderDTO> lineMapper = new L2lincReaderUtil<>();
		lineMapper.setTotalItemsToRead(linecount-2);
	    lineMapper.setLineTokenizer(fixedLengthTokenizer());
	    L2lincFieldSetMapper fsm = new L2lincFieldSetMapper();   
	    lineMapper.setFieldSetMapper(fsm);
	    reader.setLinesToSkip(1);
	    reader.setLineMapper((LineMapper) lineMapper);
	    reader.open(new ExecutionContext());
	    test = reader.read();
	    if(test != null)
	    {
	    	list.add(test);
	    }
	 } 
	     
	public  FixedLengthTokenizer fixedLengthTokenizer() {
		FixedLengthTokenizer tokenizer = new FixedLengthTokenizer();

		tokenizer.setNames(new String[] { "zdatacls","zreclass","zmodtype","zrsltcde","zrsltrsn","zerrorno",
				"znofrecs","klinckey","hcltnam","cltsex","zclntdob","zeffdate","flag","kglcmpcd","addrss",
				"zhospbnf","zsickbnf","zcancbnf","zothsbnf","zregdate","zlincdte","kjhcltnam","zhospopt",
				"zrevdate","zlownnam","zcsumins","zasumins","zdethopt","zrqflag","zrqsetdte","zrqcandte",
				"zepflag","zcvflag","zcontsex","zcontdob","zcontaddr","lifnamkj","ownnamkj","wflga",
				"zerrflg","zoccdate","ztrdate","zadrddte", "zexdrddte", "dummy"});
		tokenizer.setColumns(new Range[] { new Range(1, 1),new Range(2, 3),new Range(4,4),new Range(5,5),
				new Range(6,7),new Range(8,10),new Range(11,13),new Range(14,25),new Range(26,57),
				new Range(58,58),new Range(59,66),new Range(67,74),new Range(75,75),new Range(76,77),
				new Range(78,107),new Range(108,112),new Range(113,117),new Range(118,122),
				new Range(123,127),new Range(128,135),new Range(136,143),new Range(144,175),
				new Range(176,215),new Range(216,223),new Range(224,265),new Range(266,275),
				new Range(276,285),new Range(286,325),new Range(326,326),new Range(327,334)
				,new Range(335,342),new Range(343,343),new Range(344,344),new Range(345,345),
				new Range(346,353),new Range(354,383),new Range(384,443),new Range(444,503),
				new Range(504,504),new Range(505,505),new Range(506,513),new Range(514,521),
				new Range(522,529),new Range(530,537),new Range(538,600)});
		

		return tokenizer;
	}
	
	@Override
	public synchronized L2lincReaderDTO read() throws Exception {
		
		reader();
		if (recordCount < list.size()) {
			return list.get(recordCount++);
		} else {
			list.clear();
			recordCount = 0;
			return null;
		}
	} 
}


