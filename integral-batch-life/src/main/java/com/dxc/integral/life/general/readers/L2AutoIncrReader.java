package com.dxc.integral.life.general.readers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.L2AutoIncrReaderDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.smarttable.pojo.T5655;
import com.fasterxml.jackson.databind.ObjectMapper;

@StepScope
public class L2AutoIncrReader implements ItemReader<L2AutoIncrReaderDTO> {

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;

	@Value("#{jobParameters['chdrcoy']}")
	private String company;

	@Value("#{jobParameters['trandate']}")
	private String effDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['transactionDate']}")
	private String transactionDate;

	@Value("#{jobParameters['chdrnumFrom']}")
	private String chdrnumFrom;

	@Value("#{jobParameters['chdrnumTo']}")
	private String chdrnumTo;

	@Autowired
	CovrpfDAO covrpfDAO;

	@Autowired
	private Datcon2 datcon2;

	@Autowired
	private ItempfDAO itempfDAO;

	private int recordCount = 0;

	private List<L2AutoIncrReaderDTO> list = new ArrayList<>();

	//@Autowired(required=false)
	//private L2AutoIncrItemProcessor l2AutoIncrItemProcessor;

	private static final Logger LOGGER = LoggerFactory.getLogger(L2AutoIncrReader.class);

	public void init() throws Exception {

		String indxin = "P";
		Covrpf covrpf = new Covrpf();
		L2AutoIncrReaderDTO l2autoIncrFirstRecord = new L2AutoIncrReaderDTO();
		String trandate = readMaxLeadDays() + "";
		List<Covrpf> covrList = covrpfDAO.fetchRecords(company, trandate, indxin, chdrnumTo, chdrnumFrom, "1");
		if (covrList.isEmpty()) {
			LOGGER.info(" No records available ");
		}
		if (!covrList.isEmpty()) {
			for (int i = 0; i < covrList.size(); i++) {
				covrpf = covrList.get(i);
				setAllFields(l2autoIncrFirstRecord, covrpf);
				//L2AutoIncrReaderDTO readerDTO = l2AutoIncrItemProcessor.process(l2autoIncrFirstRecord);
				list.add(l2autoIncrFirstRecord);
			}
		}
	}

	public void setAllFields(L2AutoIncrReaderDTO l2autoIncrReader, Covrpf covrpf) {
		l2autoIncrReader.setChdrCoy(covrpf.getChdrcoy());
		l2autoIncrReader.setChdrNum(covrpf.getChdrnum());
		l2autoIncrReader.setjLife(covrpf.getJlife());
		l2autoIncrReader.setLife(covrpf.getLife());
		l2autoIncrReader.setCoverage(covrpf.getCoverage());
		l2autoIncrReader.setRider(covrpf.getRider());
		l2autoIncrReader.setIndxin(covrpf.getIndxin());
		l2autoIncrReader.setPlnSfx(covrpf.getPlnsfx());
		l2autoIncrReader.setStatCode(covrpf.getStatcode());
		l2autoIncrReader.setPstatCode(covrpf.getPstatcode());
		l2autoIncrReader.setCrrcd(covrpf.getCrrcd());
		l2autoIncrReader.setCrtable(covrpf.getCrtable());
		l2autoIncrReader.setMortCls(covrpf.getMortcls());
		l2autoIncrReader.setPrmcur(covrpf.getPrmcur());
		l2autoIncrReader.setRcesdte(covrpf.getRcesdte());
		l2autoIncrReader.setSumIns(covrpf.getSumins());
		l2autoIncrReader.setZstpDuty01(covrpf.getZstpduty01());
		l2autoIncrReader.setInstPrem(covrpf.getInstprem());
		l2autoIncrReader.setZlinstPrem(covrpf.getZlinstprem());
		l2autoIncrReader.setZbinstPrem(covrpf.getZbinstprem());
		l2autoIncrReader.setCpiDte(covrpf.getCpidte());
		l2autoIncrReader.setValidFlag(covrpf.getValidflag());
		l2autoIncrReader.setUniqueNumber(covrpf.getUniqueNumber());

	}

	@Override
	public L2AutoIncrReaderDTO read() throws Exception {

		if (recordCount < list.size()) {
			return list.get(recordCount++);
		} else {
			list.clear();
			recordCount = 0;
			return null;
		}
	}

	private int readMaxLeadDays() {
		int maxLeadDays = 0;
		List<Itempf> t5655List = itempfDAO.readSmartTableByTableNameAndItem2(company, "T5655", "B523****",
				CommonConstants.IT_ITEMPFX);

		Iterator<Itempf> iterator = t5655List.iterator();
		try {
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf.getGenareaj() != null) {
					T5655 t5655IO = new ObjectMapper().readValue(itempf.getGenareaj(), T5655.class);
					if (t5655IO.getLeadDays() > maxLeadDays) {
						maxLeadDays = t5655IO.getLeadDays();
					}
				}
			}
		} catch (IOException e) {
		}
		return datcon2.plusDays(Integer.parseInt(effDate), maxLeadDays);
	}

}
