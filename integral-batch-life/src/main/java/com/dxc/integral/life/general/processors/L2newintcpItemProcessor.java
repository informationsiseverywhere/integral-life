package com.dxc.integral.life.general.processors;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2newintcpControlTotalDTO;
import com.dxc.integral.life.beans.L2newintcpProcessorDTO;
import com.dxc.integral.life.beans.L2newintcpReaderDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Intcalc;
import com.dxc.integral.life.utils.L2newintcpProcessorUtil;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * ItemProcessor for L2NEWINTCP batch step.
 * 
 * @author dpuhawan
 *
 */
@Scope(value = "step")
@Async
public class L2newintcpItemProcessor implements ItemProcessor<L2newintcpReaderDTO, L2newintcpReaderDTO> {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2newintcpItemProcessor.class);
	
	/** The batch transaction code for L2NEWINTCP */
	public static final String BATCH_TRAN_CODE = "BA68";
	/** The batch job name for L2NEWINTCP */
	public static final String L2NEWINTCP_JOB_NAME = "L2NEWINTCP";

	@Autowired
	private UsrdpfDAO usrdpfDAO;

	@Autowired
	private L2newintcpProcessorUtil l2newintcpProcessorUtil;

	@Autowired
	private ApplicationContext appContext;
	
	@Autowired
	private Intcalc intcalc;	

	@Autowired
	private Batcup batcup;

	@Autowired
	private Sftlock sftlock;

	@Autowired
	private L2newintcpControlTotalDTO l2newintcpControlTotalDTO;

	private String company = Companies.LIFE;
	private String programName = "BR535";

	@Autowired
	private ItempfDAO itempfDAO;
	
	@Autowired
	private DescpfDAO descpfDAO;	

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['transactionDate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	private Map<String, List<Itempf>> t5645ListMap = new HashMap<String, List<Itempf>>();
	private List<T5645> t5645IOList = new ArrayList<T5645>();
	
	private Map<String, List<Itempf>> t6633ListMap = new HashMap<String, List<Itempf>>();
	private List<T6633> t6633IOList = new ArrayList<T6633>();	
	
	private Map<String, List<Itempf>> t3629ListMap = new HashMap<String, List<Itempf>>();
	private List<T3629> t3629IOList = new ArrayList<T3629>();	
	
	private Map<String, List<Itempf>> t3695ListMap = new HashMap<String, List<Itempf>>();
	private List<T3695> t3695IOList = new ArrayList<T3695>();	
	
	private T5645 t5645IO = null;
	private T6633 t6633IO = null;
	private T3629 t3629IO = null;
	private T3695 t3695IO = null;
	private Descpf t5645Descpf = null;

	private Usrdpf usrdpf = null;
	private Batcpf batchDetail = null;
	

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public L2newintcpReaderDTO process(L2newintcpReaderDTO readerDTO) throws IOException,ParseException{
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		Integer tranDate = DatimeUtil.covertToyyyyMMddDate(transactionDate);
		
		readerDTO.setBatctrcde(BATCH_TRAN_CODE);
		if (readerDTO.getOldChdrnum() == null) readerDTO.setOldChdrnum("");

		l2newintcpControlTotalDTO.setControlTotal01(l2newintcpControlTotalDTO.getControlTotal01().add(BigDecimal.ONE));
		
		switch(readerDTO.getLoantype()){
		case "P":
			readerDTO.setLoantypidx(0);
			break;
		case "A":
			readerDTO.setLoantypidx(4);
			break;
		 case "E":
			 readerDTO.setLoantypidx(8);
			break;
		 case "D":
			 readerDTO.setLoantypidx(12);
			break;
	      }
	   
		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(readerDTO.getChdrnum());
		slckpf.setEnttyp("CH");
		slckpf.setCompany(readerDTO.getChdrcoy());
		slckpf.setProctrancd(BATCH_TRAN_CODE);
		slckpf.setUsrprf(userName);
		slckpf.setJobnm(batchName);
		boolean isLocked = sftlock.lockEntity(slckpf);
		
		if (!isLocked) {
			LOGGER.info("Contract [{}] is already locked.", readerDTO.getChdrnum());//IJTI-1498
			l2newintcpControlTotalDTO.setControlTotal02(l2newintcpControlTotalDTO.getControlTotal02().add(BigDecimal.ONE));
			readerDTO.setOldChdrnum("");
			return null;
		}
		LOGGER.info("Locked contract - {}", readerDTO.getChdrnum());//IJTI-1498

		getUserDetails(userName);
		getBatcpfInfo(usrdpf);

		readReqSmartTableInfo(readerDTO, tranDate);
		
		readerDTO.setUsrprf(usrdpf.getUserid());
		readerDTO.setJobnm(batchName);
		readerDTO.setUserNum(usrdpf.getUsernum().intValue());
		if (readerDTO.getOldChdrnum().equals(readerDTO.getChdrnum())) return null;
		
		readerDTO.setTranno(readerDTO.getTranno()+1);
		readerDTO.setUserT(usrdpf.getUsernum().intValue());
		readerDTO.setLanguage(usrdpf.getLanguage());
		
		readerDTO.setOldChdrnum(readerDTO.getChdrnum());
		getT6633Info(readerDTO, tranDate);
		
		L2newintcpProcessorDTO processorDTO = l2newintcpProcessorUtil.process(readerDTO, tranDate, t6633IOList, t5645IOList, batchDetail);
		L2newintcpReaderDTO readerIO = l2newintcpProcessorUtil.fillDTOInfo(processorDTO, readerDTO,batchDetail, tranDate);

		readerIO.setTranCount(batchDetail.getTrancnt().intValue());
		if (batchDetail != null) {
			batchDetail.setTrancnt(batchDetail.getTrancnt() + 2);
			batcup.updateBatchTrancnt(batchDetail);
		}
		
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		return readerIO;
	}
	
	

	private void getUserDetails(String userName) {
		if (usrdpf == null) {
			usrdpf = usrdpfDAO.getUserInfo(userName);
		}
	}

	private void getBatcpfInfo(Usrdpf usrdpf) throws ParseException {
		if (batchDetail == null) {
			batchDetail = batcup.getBatchDetails(transactionDate, businessDate, company, BATCH_TRAN_CODE, branch, usrdpf, batchName);
		}
	}

	private void readReqSmartTableInfo(L2newintcpReaderDTO readerDTO, Integer tranDate) throws IOException {
		getT5645Info(readerDTO);
		getT3629Info(readerDTO, tranDate);
		//getT3695Info(readerDTO, tranDate);
		getT5645Descpf();
	}

	private void getT5645Info(L2newintcpReaderDTO readerDTO) throws IOException {
		//Fix keyitem
		//Use this as reference only if your item key will not vary for each contract being processed
		if (t5645IOList.isEmpty()) {
			t5645ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T5645", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/			
		}else return;
		if(!t5645ListMap.isEmpty() && t5645ListMap.containsKey(programName)) {
			List<Itempf> t5645ItemList = t5645ListMap.get(programName);
			for (int i = 0; i < t5645ItemList.size(); i++) {
				t5645IO = new ObjectMapper().readValue(t5645ItemList.get(i).getGenareaj(), T5645.class);
				t5645IOList.add(t5645IO);
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+programName+" not found in Table T5645 while processing contract "+readerDTO.getChdrnum());
		}
	}
	
	private void getT6633Info(L2newintcpReaderDTO readerDTO, Integer tranDate) throws IOException {
		//Variable keyitem
		if (t6633IOList.isEmpty()) t6633ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T6633", CommonConstants.IT_ITEMPFX);
		
		String t6633Itemkey = readerDTO.getCnttype().concat(readerDTO.getLoantype());
		
		if(!t6633ListMap.isEmpty() && t6633ListMap.containsKey(t6633Itemkey)) {
			List<Itempf> t6633ItemList = t6633ListMap.get(t6633Itemkey);
			for (int i = 0; i < t6633ItemList.size(); i++) {
				if (t6633ItemList.get(i).getItmfrm() > 0) {
					if (tranDate >= t6633ItemList.get(i).getItmfrm() && tranDate <= t6633ItemList.get(i).getItmto()) {
						t6633IO = new ObjectMapper().readValue(t6633ItemList.get(i).getGenareaj(), T6633.class);
						t6633IOList.add(t6633IO);						
					}					
				}else {
					t6633IO = new ObjectMapper().readValue(t6633ItemList.get(i).getGenareaj(), T6633.class);
					t6633IOList.add(t6633IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+programName+" not found in Table T6633 while processing contract "+readerDTO.getChdrnum());
		}		
	}	
	
	
	private void getT3629Info(L2newintcpReaderDTO readerDTO, Integer tranDate) throws IOException {
		//Variable keyitem
		if (t3629IOList.isEmpty()) t3629ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T3629", CommonConstants.IT_ITEMPFX);
		
		String t3629Itemkey = readerDTO.getBillcurr();
		
		if(!t3629ListMap.isEmpty() && t3629ListMap.containsKey(t3629Itemkey)) {
			List<Itempf> t3629ItemList = t3629ListMap.get(t3629Itemkey);
			for (int i = 0; i < t3629ItemList.size(); i++) {
				if (t3629ItemList.get(i).getItmfrm() > 0) {
					if (tranDate >= t3629ItemList.get(i).getItmfrm() && tranDate <= t3629ItemList.get(i).getItmto()) {
						t3629IO = new ObjectMapper().readValue(t3629ItemList.get(i).getGenareaj(), T3629.class);
						t3629IOList.add(t3629IO);						
					}					
				}else {
					t3629IO = new ObjectMapper().readValue(t3629ItemList.get(i).getGenareaj(), T3629.class);
					t3629IOList.add(t3629IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+programName+" not found in Table T3629 while processing contract "+readerDTO.getChdrnum());
		}
	}
	
	
	
	private void getT5645Descpf() {

		if (t5645Descpf == null) {
			t5645Descpf = descpfDAO.getDescInfo(Companies.LIFE, "T5645", programName);
		}
	}	
}