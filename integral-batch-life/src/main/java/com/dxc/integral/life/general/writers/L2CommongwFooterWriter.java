package com.dxc.integral.life.general.writers;

import java.io.IOException;
import java.io.Writer;

import org.springframework.batch.item.file.FlatFileFooterCallback;

/**
 * 
 * FooterWriter for L2commongw batch step.
 *
 */
public class L2CommongwFooterWriter implements FlatFileFooterCallback {

	private final String footer;

	public L2CommongwFooterWriter(String exportFileFooter) {
		this.footer = exportFileFooter;
	}

	@Override
	public void writeFooter(Writer writer) throws IOException {
		writer.write(footer);
	}
}
