package com.dxc.integral.life.beans.in;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class CheckInfoClientInput implements Serializable{
	private static final long serialVersionUID = 1L;
	private CheckExistingClientInput checkExistingOwnerInput;
	private CheckExistingClientInput checkExistingLifeAssuredInput;
	private List<CheckExistingClientInput> checkExistingBeneficiariesInput;
	private List<CheckExistingClientInput> checkExistingTrusteesInput;
	
	public CheckInfoClientInput() {
		//does nothing
	}
	
	/**
	 * @return the checkExistingOwnerInput
	 */
	public CheckExistingClientInput getCheckExistingOwnerInput() {
		return checkExistingOwnerInput;
	}
	/**
	 * @param checkExistingOwnerInput the checkExistingOwnerInput to set
	 */
	public void setCheckExistingOwnerInput(CheckExistingClientInput checkExistingOwnerInput) {
		this.checkExistingOwnerInput = checkExistingOwnerInput;
	}
	/**
	 * @return the checkExistingLifeAssuredInput
	 */
	public CheckExistingClientInput getCheckExistingLifeAssuredInput() {
		return checkExistingLifeAssuredInput;
	}
	/**
	 * @param checkExistingLifeAssuredInput the checkExistingLifeAssuredInput to set
	 */
	public void setCheckExistingLifeAssuredInput(CheckExistingClientInput checkExistingLifeAssuredInput) {
		this.checkExistingLifeAssuredInput = checkExistingLifeAssuredInput;
	}
	/**
	 * @return the checkExistingBeneficiariesInput
	 */
	public List<CheckExistingClientInput> getCheckExistingBeneficiariesInput() {
		return checkExistingBeneficiariesInput;
	}
	/**
	 * @param checkExistingBeneficiariesInput the checkExistingBeneficiariesInput to set
	 */
	public void setCheckExistingBeneficiariesInput(List<CheckExistingClientInput> checkExistingBeneficiariesInput) {
		this.checkExistingBeneficiariesInput = checkExistingBeneficiariesInput;
	}
	/**
	 * @return the checkExistingTrusteesInput
	 */
	public List<CheckExistingClientInput> getCheckExistingTrusteesInput() {
		return checkExistingTrusteesInput;
	}
	/**
	 * @param checkExistingTrusteesInput the checkExistingTrusteesInput to set
	 */
	public void setCheckExistingTrusteesInput(List<CheckExistingClientInput> checkExistingTrusteesInput) {
		this.checkExistingTrusteesInput = checkExistingTrusteesInput;
	}
}
