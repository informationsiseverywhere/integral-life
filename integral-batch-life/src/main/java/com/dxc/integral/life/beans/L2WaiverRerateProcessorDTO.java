package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Annypf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Regppf;
import com.dxc.integral.life.smarttable.pojo.T5399;
import com.dxc.integral.life.smarttable.pojo.T5655;
import com.dxc.integral.life.smarttable.pojo.T5671;
import com.dxc.integral.life.smarttable.pojo.T5674;
import com.dxc.integral.life.smarttable.pojo.T5675;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.T6658;
import com.dxc.integral.life.smarttable.pojo.Tr517;
import com.dxc.integral.life.smarttable.pojo.Tr52d;
import com.dxc.integral.life.smarttable.pojo.Tr52e;


public class L2WaiverRerateProcessorDTO {

	private T5655 t5655=new T5655();
	private T5679 t5679=new T5679();
	private Tr517 tr517=new Tr517();
	private T5674 t5674=new T5674();
	private T5688 t5688=new T5688();
	private T5687 t5687=new T5687();
	private T6658 t6658=new T6658();
	private T5671 t5671= new T5671();
	private T5675 t5675= new T5675();
	private T5399 t5399= new T5399();
	private Tr52d tr52d= new Tr52d();
	private Tr52e tr52e= new Tr52e();
	private int crrcd;
	private boolean premReqd;
	private Covrpf covrpf = new Covrpf();
	private Ptrnpf ptrnpf= new Ptrnpf();
	private Covrpf covrUpdate;
	private PremiumDTO premiumDTO= new PremiumDTO();
	private Covrpf covrInsert;
	private Incrpf incrInsert;
	private Ptrnpf ptrnInsert;
	private Cfee001 cfee001=new Cfee001();
	private Chdrpf chdrpfUpdate;
	private Chdrpf chdrpfInsert;
	private Agcmpf agcmpfUpdate;
	private Agcmpf agcmpfInsert;
	private Batcpf batcpf= new Batcpf();
	private Usrdpf usrdpf= new Usrdpf();
	private String mainCrtable;
	private String mainCoverage;
	private int mainCessDate;
	private int mainPcessDate;
	private String mainMortClass;
	private String mainLife;
	private String t5675PremSubrVal;
	private List<String> covrStatcode;
	private List<String> covrPstatCode;
	
	private Payrpf payrpfUpdate;
	private Payrpf payrpfInsert;
	private Payrpf payrpf=new Payrpf();
	private Chdrpf chdrpf=new Chdrpf();
	private Lifepf lifepf=new Lifepf();
	private Incrpf incrpf=new Incrpf();
	private Annypf annypf=new Annypf();
	private AgecalcDTO agecalcDTO=new AgecalcDTO();
	private BigDecimal instPrem;
	private BigDecimal zbInstPrem;
	private BigDecimal zlInstPrem;
	private BigDecimal tax=BigDecimal.ZERO;
	private BigDecimal premdiff;
	private BigDecimal covrSumins;
	private int lastRerateDate;
	private int billDate=0;
	private int t6658billFreq;
	private int billFreq;
	private int anb=0;
	private int accYear;
	private int accMonth;
	private Integer planSfx;
	private int rateFrom;
	private int rerateStore;
	private int effDate;
	private BigDecimal durationInt;
	private int durationRem;
	public int leadDays;
	private int maxDate=99999999;
	private int earliestDate;
	private int firstIncrDate=99999999;
	private int tranNo;
	private boolean stampdutyFlag;
	private boolean isValidchdr;
	private boolean isValidCovr;
	private boolean fullyPaid;
	private boolean isCovrUpdated;
	private boolean isAgntFound;
	private boolean dateFound;
	private boolean crtableFound;
	private boolean endFlag=false;
	private String tranDate;
	private boolean vpmsFlag;
	private String tr52eKey;
	private String t5688FeeMeth;
	private String cedAgent;
	private String agntNum;
	private String redTerm;
	private String rerateType;
	private String waiverCount;
	private int actMonth;
	private int actYear;
	private String lang;
	private String prefix;
	private String batch;
	private String batchtrancde;
	private String batchbranch;
	private String chdrNum;
	private String chdrCoy;
	private String userName;
	private String batchName;
	private int transactionDate;
	private int tranTime;
	
	private List<Covrpf> covrList= new ArrayList<>();	
	private List<String> t6658BillFreqList = new ArrayList<>();
	private List<String> t6658Annvry = new ArrayList<>();
	private List<Integer> t6658Itmfrm = new ArrayList<>();
	private List<String> t5688Cnttype = new ArrayList<>();
	private List<Integer> t5688Itmfrm = new ArrayList<>();
	private List<String> t5675Key = new ArrayList<>();
	private List<String> t5675PremSubr = new ArrayList<>();
	private List<Integer> t5687Itmfrm = new ArrayList<>();
	private List<String> t5687Crtable = new ArrayList<>();
	private List<String> t5687Annvry = new ArrayList<>();
	private List<String> t5687Premmeth= new ArrayList<>();
	private List<String> t5687JlPremmeth = new ArrayList<>();
	private List<Integer> t5687Pguarp = new ArrayList<>();
	private List<Integer> t5687Rtrnwfreq = new ArrayList<>();
	private List<String> t5687ZrrCombas = new ArrayList<>();
	private List<String> t5655ItemKey = new ArrayList<>();
	private List<Integer> t5655Itmfrm = new ArrayList<>();
	private List<Integer> t5655ItmTo = new ArrayList<>();
	private List<String> t5674Key = new ArrayList<>();
	private List<String> t5674CommSubr = new ArrayList<>();
	private List<String> t5671Key = new ArrayList<>();
	private List<String> t5671Subprogs = new ArrayList<>();
	private List<String> tr517Zrwvflgs = new ArrayList<>();
	private List<String> tr517Ctables = new ArrayList<>();
	private String tr517Contitem ;
	private List<Integer> tr517Itmfrm = new ArrayList<>();
	private List<String> tr517Crtable = new ArrayList<>(); 
	private List<Chdrpf> chdrList;
	private List<Lifepf> lifeList;
	private List<Payrpf> payrList;
	private List<Annypf> annyList;
	private List<Lextpf> lextList;
	public int rerateDate;
	private int cpidte;
	private int rcesdte;
	private int pcesdte;
	private ZrdecplcDTO zrdecplDTO;
	private String annvMethod;
	private String premMeth;
	private String jlPremMeth;
	private String zrrCombas;
	private BigDecimal rtrnwfreq;
	private Integer premGuarPeriod;
	private List<Incrpf> insertIncrpfList;
	private L2WaiverRerateControlTotalDTO l2waiverRerateControlDTO;
	private Regppf regppfUpdate;
	
	public int getPcesdte() {
		return pcesdte;
	}
	public void setPcesdte(int pcesdte) {
		this.pcesdte = pcesdte;
	}
	
	public int getRcesdte() {
		return rcesdte;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	
	public BigDecimal getPremdiff() {
		return premdiff;
	}
	public void setPremdiff(BigDecimal premdiff) {
		this.premdiff = premdiff;
	}
	
	public boolean isStampdutyFlag() {
		return stampdutyFlag;
	}
	public void setStampdutyFlag(boolean stampdutyFlag) {
		this.stampdutyFlag = stampdutyFlag;
	}
	public boolean isCovrUpdated() {
		return isCovrUpdated;
	}
	public void setCovrUpdated(boolean isCovrUpdated) {
		this.isCovrUpdated = isCovrUpdated;
	}
	public T5655 getT5655() {
		return t5655;
	}
	public void setT5655(T5655 t5655) {
		this.t5655 = t5655;
	}
	public T5679 getT5679() {
		return t5679;
	}
	public void setT5679(T5679 t5679) {
		this.t5679 = t5679;
	}
	public T5674 getT5674() {
		return t5674;
	}
	public void setT5674(T5674 t5674) {
		this.t5674 = t5674;
	}
	public T5688 getT5688() {
		return t5688;
	}
	public void setT5688(T5688 t5688) {
		this.t5688 = t5688;
	}
	public Tr517 getTr517() {
		return tr517;
	}
	public void setTr517(Tr517 tr517) {
		this.tr517 = tr517;
	}
	public int getBillDate() {
		return billDate;
	}
	public void setBillDate(int billDate) {
		this.billDate = billDate;
	}
	public List<Covrpf> getCovrList() {
		return covrList;
	}
	public void setCovrList(List<Covrpf> covrList) {
		this.covrList = covrList;
	}
	public List<String> getTr517Zrwvflgs() {
		return tr517Zrwvflgs;
	}
	public void setTr517Zrwvflgs(List<String> tr517Zrwvflgs) {
		this.tr517Zrwvflgs = tr517Zrwvflgs;
	}
	public List<String> getTr517Ctables() {
		return tr517Ctables;
	}
	public void setTr517Ctables(List<String> tr517Ctables) {
		this.tr517Ctables = tr517Ctables;
	}
	public String getTr517Contitem() {
		return tr517Contitem;
	}
	public void setTr517Contitem(String tr517Contitem) {
		this.tr517Contitem = tr517Contitem;
	}
	public List<Integer> getTr517Itmfrm() {
		return tr517Itmfrm;
	}
	public void setTr517Itmfrm(List<Integer> tr517Itmfrm) {
		this.tr517Itmfrm = tr517Itmfrm;
	}
	public List<String> getTr517Crtable() {
		return tr517Crtable;
	}
	public void setTr517Crtable(List<String> tr517Crtable) {
		this.tr517Crtable = tr517Crtable;
	}
	public Chdrpf getChdrpf() {
		return chdrpf;
	}
	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}
	public T5671 getT5671() {
		return t5671;
	}
	public void setT5671(T5671 t5671) {
		this.t5671 = t5671;
	}
	public T5675 getT5675() {
		return t5675;
	}
	public void setT5675(T5675 t5675) {
		this.t5675 = t5675;
	}
	public List<Chdrpf> getChdrList() {
		return chdrList;
	}
	public void setChdrList(List<Chdrpf> list) {
		this.chdrList = list;
	}
	public List<Lextpf> getLextList() {
		return lextList;
	}
	public void setLextList(List<Lextpf> lextList) {
		this.lextList = lextList;
	}
	public List<Lifepf> getLifeList() {
		return lifeList;
	}
	public void setLifeList( List<Lifepf> lifeList) {
		this.lifeList = lifeList;
	}
	public List<Payrpf> getPayrList() {
		return payrList;
	}
	public void setPayrList( List<Payrpf> payrList) {
		this.payrList = payrList;
	}
	public List<Annypf> getAnnyList() {
		return annyList;
	}
	public void setAnnyList(List<Annypf> annyList) {
		this.annyList = annyList;
	}
	public boolean isValidchdr() {
		return isValidchdr;
	}
	public void setValidchdr(boolean isValidchdr) {
		this.isValidchdr = isValidchdr;
	}
	public boolean isValidCovr() {
		return isValidCovr;
	}
	public void setValidCovr(boolean isValidCovr) {
		this.isValidCovr = isValidCovr;
	}
	public boolean isDateFound() {
		return dateFound;
	}
	public void setDateFound(boolean dateFound) {
		this.dateFound = dateFound;
	}
	public Tr52d getTr52d() {
		return tr52d;
	}
	public void setTr52d(Tr52d tr52d) {
		this.tr52d = tr52d;
	}
	public int getEarliestDate() {
		return earliestDate;
	}
	public void setEarliestDate(int earliestDate) {
		this.earliestDate = earliestDate;
	}
	public int getFirstIncrDate() {
		return firstIncrDate;
	}
	public void setFirstIncrDate(int firstIncrDate) {
		this.firstIncrDate = firstIncrDate;
	}
	public BigDecimal getCovrSumins() {
		return covrSumins;
	}
	public void setCovrSumins(BigDecimal covrSumins) {
		this.covrSumins = covrSumins;
	}
	public boolean isCrtableFound() {
		return crtableFound;
	}
	public void setCrtableFound(boolean crtableFound) {
		this.crtableFound = crtableFound;
	}
	public boolean isEndFlag() {
		return endFlag;
	}
	public void setEndFlag(boolean endFlag) {
		this.endFlag = endFlag;
	}
	public Incrpf getIncrpf() {
		return incrpf;
	}
	public void setIncrpf(Incrpf incrpf) {
		this.incrpf = incrpf;
	}
	public T5687 getT5687() {
		return t5687;
	}
	public void setT5687(T5687 t5687) {
		this.t5687 = t5687;
	}
	public T6658 getT6658() {
		return t6658;
	}
	public void setT5687(T6658 t6658) {
		this.t6658 = t6658;
	}
	public List<String> getT5674Key() {
		return t5674Key;
	}
	public void setT5674Key(List<String> t5674Key) {
		this.t5674Key = t5674Key;
	}
	public List<String> getT5674CommSubr() {
		return t5674CommSubr;
	}
	public void setT5674CommSubr(List<String> t5674CommSubr) {
		this.t5674CommSubr = t5674CommSubr;
	}
	public List<String> getT5655ItemKey() {
		return t5655ItemKey;
	}
	public void setT5655ItemKey(List<String> t5655ItemKey) {
		this.t5655ItemKey = t5655ItemKey;
	}
	public List<Integer> getT5655Itmfrm() {
		return t5655Itmfrm;
	}
	public void setT5655Itmfrm(List<Integer> t5655Itmfrm) {
		this.t5655Itmfrm = t5655Itmfrm;
	}
	public List<Integer> getT5655ItmTo() {
		return t5655ItmTo;
	}
	public void setT5655ItmTo(List<Integer> t5655ItmTo) {
		this.t5655ItmTo = t5655ItmTo;
	}
	public List<String> getT5671Key() {
		return t5671Key;
	}
	public void setT5671Key(List<String> t5671Key) {
		this.t5671Key = t5671Key;
	}
	public List<String> getT5671Subprogs() {
		return t5671Subprogs;
	}
	public void setT5671Subprogs(List<String> t5671Subprogs) {
		this.t5671Subprogs = t5671Subprogs;
	}
	public String getWaiverCount() {
		return waiverCount;
	}
	public void setWaiverCount(String waiverCount) {
		this.waiverCount = waiverCount;
	}
	public void setT6658(T6658 t6658) {
		this.t6658 = t6658;
	}
	public List<String> getT5688Cnttype() {
		return t5688Cnttype;
	}
	public void setT5688Cnttype(List<String> t5688Cnttype) {
		this.t5688Cnttype = t5688Cnttype;
	}
	public List<Integer> getT5688Itmfrm() {
		return t5688Itmfrm;
	}
	public void setT5688Itmfrm(List<Integer> t5688Itmfrm) {
		this.t5688Itmfrm = t5688Itmfrm;
	}
	public String getT5688FeeMeth() {
		return t5688FeeMeth;
	}
	public void setT5688FeeMeth(String t5688FeeMeth) {
		this.t5688FeeMeth = t5688FeeMeth;
	}
	public List<String> getT5675Key() {
		return t5675Key;
	}
	public void setT5675Key(List<String> t5675Key) {
		this.t5675Key = t5675Key;
	}
	public List<String> getT5675PremSubr() {
		return t5675PremSubr;
	}
	public void setT5675PremSubr(List<String> t5675PremSubr) {
		this.t5675PremSubr = t5675PremSubr;
	}
	public T5399 getT5399() {
		return t5399;
	}
	public void setT5399(T5399 t5399) {
		this.t5399 = t5399;
	}
	public List<String> getT6658BillFreqList() {
		return t6658BillFreqList;
	}
	public void setT6658BillFreqList(List<String> t6658BillFreqList) {
		this.t6658BillFreqList = t6658BillFreqList;
	}
	public List<String> getT6658Annvry() {
		return t6658Annvry;
	}
	public void setT6658Annvry(List<String> t6658Annvry) {
		this.t6658Annvry = t6658Annvry;
	}
	public List<Integer> getT6658Itmfrm() {
		return t6658Itmfrm;
	}
	public void setT6658Itmfrm(List<Integer> t6658Itmfrm) {
		this.t6658Itmfrm = t6658Itmfrm;
	}
	public List<Integer> getT5687Itmfrm() {
		return t5687Itmfrm;
	}
	public void setT5687Itmfrm(List<Integer> t5687Itmfrm) {
		this.t5687Itmfrm = t5687Itmfrm;
	}
	public List<String> getT5687Crtable() {
		return t5687Crtable;
	}
	public void setT5687Crtable(List<String> t5687Crtable) {
		this.t5687Crtable = t5687Crtable;
	}
	public List<String> getT5687Annvry() {
		return t5687Annvry;
	}
	public void setT5687Annvry(List<String> t5687Annvry) {
		this.t5687Annvry = t5687Annvry;
	}
	public List<String> getT5687Premmeth() {
		return t5687Premmeth;
	}
	public void setT5687Premmeth(List<String> t5687Premmeth) {
		this.t5687Premmeth = t5687Premmeth;
	}
	public List<String> getT5687JlPremmeth() {
		return t5687JlPremmeth;
	}
	public void setT5687JlPremmeth(List<String> t5687JlPremmeth) {
		this.t5687JlPremmeth = t5687JlPremmeth;
	}
	public List<Integer> getT5687Pguarp() {
		return t5687Pguarp;
	}
	public void setT5687Pguarp(List<Integer> t5687Pguarp) {
		this.t5687Pguarp = t5687Pguarp;
	}
	public List<Integer> getT5687Rtrnwfreq() {
		return t5687Rtrnwfreq;
	}
	public void setT5687Rtrnwfreq(List<Integer> t5687Rtrnwfreq) {
		this.t5687Rtrnwfreq = t5687Rtrnwfreq;
	}
	public List<String> getT5687ZrrCombas() {
		return t5687ZrrCombas;
	}
	public void setT5687ZrrCombas(List<String> t5687ZrrCombas) {
		this.t5687ZrrCombas = t5687ZrrCombas;
	}
	public Integer getPlanSfx() {
		return planSfx;
	}
	public void setPlanSfx(Integer planSfx) {
		this.planSfx = planSfx;
	}
	public Tr52e getTr52e() {
		return tr52e;
	}
	public void setTr52e(Tr52e tr52e) {
		this.tr52e = tr52e;
	}
	public Covrpf getCovrUpdate() {
		return covrUpdate;
	}
	public void setCovrUpdate(Covrpf covrUpdate) {
		this.covrUpdate = covrUpdate;
	}
	public Covrpf getCovrInsert() {
		return covrInsert;
	}
	public void setCovrInsert(Covrpf covrInsert) {
		this.covrInsert = covrInsert;
	}
	public int getRerateStore() {
		return rerateStore;
	}
	public void setRerateStore(int rerateStore) {
		this.rerateStore = rerateStore;
	}
	public boolean isFullyPaid() {
		return fullyPaid;
	}
	public void setFullyPaid(boolean fullyPaid) {
		this.fullyPaid = fullyPaid;
	}
	public int getRateFrom() {
		return rateFrom;
	}
	public void setRateFrom(int rateFrom) {
		this.rateFrom = rateFrom;
	}
	public BigDecimal getInstPrem() {
		return instPrem;
	}
	public void setInstPrem(BigDecimal instPrem) {
		this.instPrem = instPrem;
	}
	public BigDecimal getZbInstPrem() {
		return zbInstPrem;
	}
	public void setZbInstPrem(BigDecimal zbInstPrem) {
		this.zbInstPrem = zbInstPrem;
	}
	public BigDecimal getZlInstPrem() {
		return zlInstPrem;
	}
	public void setZlInstPrem(BigDecimal zlInstPrem) {
		this.zlInstPrem = zlInstPrem;
	}
	public Lifepf getLifepf() {
		return lifepf;
	}
	public void setLifepf(Lifepf lifepf) {
		this.lifepf = lifepf;
	}
	public Agcmpf getAgcmpfUpdate() {
		return agcmpfUpdate;
	}
	public void setAgcmpfUpdate(Agcmpf agcmpfUpdate) {
		this.agcmpfUpdate = agcmpfUpdate;
	}
	public Agcmpf getAgcmpfInsert() {
		return agcmpfInsert;
	}
	public void setAgcmpfInsert(Agcmpf agcmpfInsert) {
		this.agcmpfInsert = agcmpfInsert;
	}
	public int getTranNo() {
		return tranNo;
	}
	public void setTranNo(int tranNo) {
		this.tranNo = tranNo;
	}
	public String getAgntnum() {
		return agntNum;
	}
	public void setAgntnum(String agntnum) {
		this.agntNum = agntnum;
	}
	public boolean isAgntFound() {
		return isAgntFound;
	}
	public void setAgntFound(boolean isAgntFound) {
		this.isAgntFound = isAgntFound;
	}
	public String getCedAgent() {
		return cedAgent;
	}
	public void setCedAgent(String cedAgent) {
		this.cedAgent = cedAgent;
	}
	public String getRedTerm() {
		return redTerm;
	}
	public void setRedTerm(String redTerm) {
		this.redTerm = redTerm;
	}
	public int getAccYear() {
		return accYear;
	}
	public void setAccYear(int accYear) {
		this.accYear = accYear;
	}
	public int getAccMonth() {
		return accMonth;
	}
	public void setAccMonth(int accMonth) {
		this.accMonth = accMonth;
	}
	public int getEffDate() {
		return effDate;
	}
	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}
	public Batcpf getBatcpf() {
		return batcpf;
	}
	public void setBatcpf(Batcpf batcpf) {
		this.batcpf = batcpf;
	}
	public Usrdpf getUsrdpf() {
		return usrdpf;
	}
	public void setUsrdpf(Usrdpf usrdpf) {
		this.usrdpf = usrdpf;
	}
	public Incrpf getIncrInsert() {
		return incrInsert;
	}
	public void setIncrInsert(Incrpf incrpfInsert) {
		incrInsert = incrpfInsert;
	}
	public String getTranDate() {
		return tranDate;
	}
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	public String getTr52eKey() {
		return tr52eKey;
	}
	public void setTr52eKey(String tr52eKey) {
		this.tr52eKey = tr52eKey;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public Ptrnpf getPtrnpf() {
		return ptrnpf;
	}
	public void setPtrnpf(Ptrnpf ptrnpf) {
		this.ptrnpf = ptrnpf;
	}
	public Ptrnpf getPtrnInsert() {
		return ptrnInsert;
	}
	public void setPtrnInsert(Ptrnpf ptrnpfInsert) {
		ptrnInsert = ptrnpfInsert;
	}
	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}
	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}
	public Chdrpf getChdrpfInsert() {
		return chdrpfInsert;
	}
	public void setChdrpfInsert(Chdrpf chdrpfInsert) {
		this.chdrpfInsert = chdrpfInsert;
	}
	public Payrpf getPayrpf() {
		return payrpf;
	}
	public void setPayrpf(Payrpf payrpf) {
		this.payrpf = payrpf;
	}
	public Payrpf getPayrpfUpdate() {
		return payrpfUpdate;
	}
	public void setPayrpfUpdate(Payrpf payrpfUpdate) {
		this.payrpfUpdate = payrpfUpdate;
	}
	public Payrpf getPayrpfInsert() {
		return payrpfInsert;
	}
	public void setPayrpfInsert(Payrpf payrpfInsert) {
		this.payrpfInsert = payrpfInsert;
	}
	public int getLeadDays() {
		return leadDays;
	}
	public void setLeadDays(int leadDays) {
		this.leadDays = leadDays;
	}
	
	public String getRerateType() {
		return rerateType;
	}
	public void setRerateType(String rerateType) {
		this.rerateType = rerateType;
	}
	public int getMaxDate() {
		return maxDate;
	}
	public Cfee001 getCfee001() {
		return cfee001;
	}
	public void setCfee001(Cfee001 cfee001) {
		this.cfee001=cfee001;
	}
	public void setMaxDate(int maxDate) {
		this.maxDate = maxDate;
	}
	public PremiumDTO getPremiumDTO() {
		return premiumDTO;
	}
	public void setPremiumDTO(PremiumDTO premiumDTO) {
		this.premiumDTO = premiumDTO;
	}
	public int getT6658Billfreq() {
		return t6658billFreq;
	}
	public void setT6658Billfreq(int t6658billFreq) {
		this.t6658billFreq = t6658billFreq;
	}
	public int getAnb() {
		return anb;
	}
	public void setAnb(int anb) {
		this.anb = anb;
	}
	public boolean getVpmsFlag() {
		return vpmsFlag;
	}
	public void setVpmsFlag(boolean vpmsFlag2) {
		this.vpmsFlag = vpmsFlag2;
	}
	public int getLastRerateDate() {
		return lastRerateDate;
	}
	public void setLastRerateDate(int lastRerateDate) {
		this.lastRerateDate = lastRerateDate;
	}
	public BigDecimal getDurationInt() {
		return durationInt;
	}
	public void setDurationInt(BigDecimal result) {
		this.durationInt = result;
	}
	public int getDurationRem() {
		return durationRem;
	}
	public void setDurationRem(int durationRem) {
		this.durationRem = durationRem;
	}
	public AgecalcDTO getAgecalcDTO() {
		return agecalcDTO;
	}
	public void setAgecalcDTO(AgecalcDTO agecalcDTO) {
		this.agecalcDTO = agecalcDTO;
	}
	public Annypf getAnnypf() {
		return annypf;
	}
	public void setAnnypf(Annypf annypf) {
		this.annypf = annypf;
	}
	public int getActMonth() {
		return actMonth;
	}
	public void setActMonth(int actMonth) {
		this.actMonth = actMonth;
	}
	public int getActYear() {
		return actYear;
	}
	public void setActYear(int actYear) {
		this.actYear = actYear;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBatchtrancde() {
		return batchtrancde;
	}
	public void setBatchtrancde(String batchtrancde) {
		this.batchtrancde = batchtrancde;
	}
	public String getBatchbranch() {
		return batchbranch;
	}
	public void setBatchbranch(String batchbranch) {
		this.batchbranch = batchbranch;
	}
	public String getChdrnum() {
		return chdrNum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrNum = chdrnum;
	}
	public String getChdrcoy() {
		return chdrCoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrCoy = chdrcoy;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTranTime() {
		return tranTime;
	}
	public void setTranTime(int tranTime) {
		this.tranTime = tranTime;
	}
	public L2WaiverRerateControlTotalDTO getL2waiverRerateControlDTO() {
		return l2waiverRerateControlDTO;
	}
	public void setL2waiverRerateControlDTO(L2WaiverRerateControlTotalDTO l2waiverRerateControlDTO) {
		this.l2waiverRerateControlDTO = l2waiverRerateControlDTO;
	}
	public int getRerateDate() {
		return rerateDate;
	}
	public void setRerateDate(int rerateDate) {
		this.rerateDate = rerateDate;
	}
	public int getCpidte() {
		return cpidte;
	}
	public void setCpidte(int cpidte) {
		this.cpidte = cpidte;
	}
	public ZrdecplcDTO getZrdecplDTO() {
		return zrdecplDTO;
	}
	public void setZrdecplDTO(ZrdecplcDTO zrdecplDTO) {
		this.zrdecplDTO = zrdecplDTO;
	}
	public String getAnnvMethod() {
		return annvMethod;
	}
	public void setAnnvMethod(String annvMethod) {
		this.annvMethod = annvMethod;
	}
	public String getPremMeth() {
		return premMeth;
	}
	public void setPremMeth(String premMeth) {
		this.premMeth = premMeth;
	}
	public String getJlPremMeth() {
		return jlPremMeth;
	}
	public void setJlPremMeth(String jlPremMeth) {
		this.jlPremMeth = jlPremMeth;
	}
	public String getZrrCombas() {
		return zrrCombas;
	}
	public void setZrrCombas(String zrrCombas) {
		this.zrrCombas = zrrCombas;
	}
	public BigDecimal getRtrnwfreq() {
		return rtrnwfreq;
	}
	public void setRtrnwfreq(BigDecimal rtrnwfreq) {
		this.rtrnwfreq = rtrnwfreq;
	}
	public Integer getPremGuarPeriod() {
		return premGuarPeriod;
	}
	public void setPremGuarPeriod(Integer premGuarPeriod) {
		this.premGuarPeriod = premGuarPeriod;
	}
	public String getMainCrtable() {
		return mainCrtable;
	}
	public void setMainCrtable(String mainCrtable) {
		this.mainCrtable = mainCrtable;
	}
	public Covrpf getCovrpf() {
		return covrpf;
	}
	public void setCovrpf(Covrpf readerDTO) {
		this.covrpf = readerDTO;
	}
	public String getMainCoverage() {
		return mainCoverage;
	}
	public void setMainCoverage(String mainCoverage) {
		this.mainCoverage = mainCoverage;
	}
	public int getMainCessDate() {
		return mainCessDate;
	}
	public void setMainCessDate(int mainCessDate) {
		this.mainCessDate = mainCessDate;
	}
	public int getMainPcessDate() {
		return mainPcessDate;
	}
	public void setMainPcessDate(int mainPcessDate) {
		this.mainPcessDate = mainPcessDate;
	}
	public String getMainMortClass() {
		return mainMortClass;
	}
	public void setMainMortClass(String mainMortClass) {
		this.mainMortClass = mainMortClass;
	}
	public String getMainLife() {
		return mainLife;
	}
	public void setMainLife(String mainLife) {
		this.mainLife = mainLife;
	}
	public int getBillfreq() {
		return billFreq;
	}
	public void setBillfreq(int billfreq) {
		this.billFreq = billfreq;
	}
	public String getT5675PremSubrVal() {
		return t5675PremSubrVal;
	}
	public void setT5675PremSubrVal(String t5675PremSubrVal) {
		this.t5675PremSubrVal = t5675PremSubrVal;
	}
	public boolean isPremReqd() {
		return premReqd;
	}
	public void setPremReqd(boolean premReqd) {
		this.premReqd = premReqd;
	}
	public List<Incrpf> getInsertIncrpfList() {
		return insertIncrpfList;
	}
	public void setInsertIncrpfList(List<Incrpf> insertIncrpfList) {
		this.insertIncrpfList = insertIncrpfList;
	}
	public List<String> getCovrStatcode() {
		return covrStatcode;
	}
	public void setCovrStatcode(List<String> covrStatcode) {
		this.covrStatcode = covrStatcode;
	}
	public List<String> getCovrPstatCode() {
		return covrPstatCode;
	}
	public void setCovrPstatCode(List<String> covrPstatCode) {
		this.covrPstatCode = covrPstatCode;
	}
	public void setUpdateRegppf(Regppf regppf) {
		this.regppfUpdate = regppf;
		
	}
	public Regppf getUpdateRegppf() {
		return regppfUpdate;
	}
	
	
	
}
