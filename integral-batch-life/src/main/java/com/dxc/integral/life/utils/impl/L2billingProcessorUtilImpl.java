package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.fsu.utils.FeaConfg;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.BillreqDTO;
import com.dxc.integral.life.beans.HdisArrayInner;
import com.dxc.integral.life.beans.L2BillingControlTotalDTO;
import com.dxc.integral.life.beans.L2BillingProcessorDTO;
import com.dxc.integral.life.beans.L2BillingReaderDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.NlgcalcDTO;
import com.dxc.integral.life.beans.PrasDTO;
import com.dxc.integral.life.beans.RlpdlonDTO;
import com.dxc.integral.life.beans.TxcalcDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.DdsupfDAO;
import com.dxc.integral.life.dao.FpcopfDAO;
import com.dxc.integral.life.dao.FprmpfDAO;
import com.dxc.integral.life.dao.HcsdpfDAO;
import com.dxc.integral.life.dao.HdispfDAO;
import com.dxc.integral.life.dao.HdivpfDAO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Ddsupf;
import com.dxc.integral.life.dao.model.Fpcopf;
import com.dxc.integral.life.dao.model.Fprmpf;
import com.dxc.integral.life.dao.model.Hcsdpf;
import com.dxc.integral.life.dao.model.Hdispf;
import com.dxc.integral.life.dao.model.Hdivpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T3620;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T6654;
import com.dxc.integral.life.smarttable.pojo.T6687;
import com.dxc.integral.life.smarttable.pojo.Tr384;
import com.dxc.integral.life.smarttable.pojo.Tr52d;
import com.dxc.integral.life.smarttable.pojo.Tr52e;
import com.dxc.integral.life.utils.Billreq1;
import com.dxc.integral.life.utils.Datcon4;
import com.dxc.integral.life.utils.L2billingProcessorUtil;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Nlgcalc;
import com.dxc.integral.life.utils.Rlpdlon;
import com.dxc.integral.life.utils.Servtax;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * l2billing processor utility implementation.
 * 
 * @author yyang21
 *
 */
@Service("l2billingProcessorUtil")
@Lazy
public class L2billingProcessorUtilImpl implements L2billingProcessorUtil {

	private static final String STATUS_OK = "****";
	private static final String XCVRT_MSG = "L2billing: IOException in xcvrt: ";

	@Autowired
	private ItempfService itempfService;
	@Autowired
	private Rlpdlon rlpdlon;
	@Autowired
	private Xcvrt xcvrt;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Servtax servtax;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Datcon4 datcon4;
	@Autowired
	private Lifacmv lifacmv;
	@Autowired
	private Billreq1 billreq1;
	@Autowired
	private Nlgcalc nlgcalc;

	@Autowired
	private AcblpfDAO acblpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private FpcopfDAO fpcopfDAO;
	@Autowired
	private FprmpfDAO fprmpfDAO;

	@Autowired
	private IncrpfDAO incrpfDAO;

	@Autowired
	private HdispfDAO hdispfDAO;

	@Autowired
	private HcsdpfDAO hcsdpfDAO;

	@Autowired
	private HdivpfDAO hdivpfDAO;

	@Autowired
	private DdsupfDAO ddsupfDAO;

	public boolean validContract(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO) {
		boolean validCoverage = false;
		String batchTrcde = l2BillingProcessorDTO.getBatchTrcde();
		T5679 t5679 = null;
		t5679 = itempfService.readSmartTableByTrim(Companies.LIFE, "T5679", batchTrcde, T5679.class);
		if (t5679 == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table T5679: " + batchTrcde);
		}
		for (int i = 0; i < t5679.getCnRiskStats().size(); i++) {
			if (t5679.getCnRiskStats().get(i).equals(readerDTO.getStatcode())) {
				for (int j = 0; j < t5679.getCnPremStats().size(); j++) {
					if (t5679.getCnPremStats().get(j).equals(readerDTO.getPstcde())) {
						validCoverage = true;
						l2BillingProcessorDTO.setT5679IO(t5679);
						break;
					}
				}
			}
			if (validCoverage) {
				break;
			}
		}
		return validCoverage;
	}

	public boolean validateFlexibleContract(L2BillingReaderDTO readerDTO) {
		boolean flexFlag = false;
		List<Itempf> t5729List = itempfService.readSmartTableList(Companies.LIFE, "T5729", readerDTO.getCnttype());
		if (null != t5729List && !t5729List.isEmpty()) {
			Iterator<Itempf> iterator = t5729List.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf.getItmfrm() <= readerDTO.getOccdate() && itempf.getItmto() >= readerDTO.getOccdate()) {
					flexFlag = true;
					break;
				}
			}
		}
		return flexFlag;
	}

	public int readLeadDays(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		String keyItemitem = readerDTO.getBillchnl().trim().concat(readerDTO.getCnttype().trim());
		T6654 t6654 = readT6654(keyItemitem);
		if (t6654 == null) {
			keyItemitem = readerDTO.getBillchnl().trim().concat("***");
			t6654 = readT6654(keyItemitem);
		}
		if (t6654 == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table T6654: " + keyItemitem);
		}
		l2BillingProcessorDTO.setT6654IO(t6654);
		return t6654.getLeadDays();
	}

	private T6654 readT6654(String keyItemitem) {
		T6654 t6654 = null;
		t6654 = itempfService.readSmartTableByTrim(Companies.LIFE, "T6654", keyItemitem, T6654.class);
		return t6654;
	}

	public void loadSmartTables(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO) {
		T5645 t5645IO = readT5645();
		l2BillingProcessorDTO.setT5645IO(t5645IO);
		l2BillingProcessorDTO.setT3695IO(readT3695(t5645IO.getSacstypes().get(0)));
		l2BillingProcessorDTO.setTr52dIO(readTr52d(readerDTO.getReg()));
	}

	private T5645 readT5645() {
		T5645 t5645 = null;
		t5645 = itempfService.readSmartTableByTrim(Companies.LIFE, "T5645", "B5349", T5645.class);
		if (t5645 == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table T5645: B5349");
		}
		return t5645;
	}

	private T3695 readT3695(String sacsType) {
		String keyItemitem = sacsType;
		T3695 t3695 = null;
		t3695 = itempfService.readSmartTableByTrim(Companies.LIFE, "T3695", keyItemitem, T3695.class);
		if (t3695 == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table T3695: " + keyItemitem);
		}
		return t3695;
	}

	private Tr52d readTr52d(String reg) {
		Tr52d tr52d = readTr52dSub(reg);

		if (tr52d == null) {
			tr52d = readTr52dSub("***");
		}
		if (tr52d == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table TR52D: " + reg);
		}
		return tr52d;
	}

	private Tr52d readTr52dSub(String keyItemitem) {
		return itempfService.readSmartTableByTrim(Companies.LIFE, "TR52D", keyItemitem, Tr52d.class);
	}

	public void updateRecord(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO) {
		/* Save original amount as this is not to be updated for <D9604> */
		/* flexible premium contracts <D9604> */

		Payrpf payrpf = readPayr(readerDTO);
		readAcbl(readerDTO, l2BillingProcessorDTO);

		if (readerDTO.getSinstamt05().compareTo(BigDecimal.ZERO) != 0
				&& (l2BillingProcessorDTO.getTr52dIO().getTxcode() != null
						&& !l2BillingProcessorDTO.getTr52dIO().getTxcode().isEmpty())) {
			List<Covrpf> covrpfList = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(Companies.LIFE,
					readerDTO.getChdrnum());
			for (Covrpf covr : covrpfList) {
				if ("1".equals(covr.getValidflag()) && checkWop(covr)) {
					break;
				}
			}
		}
		int effdatePlusCntlead = l2BillingProcessorDTO.getEffdatePlusCntlead();
		boolean isbillday = FeaConfg.isFeatureExist(readerDTO.getChdrcoy(), "NBPRP011", "IT");
		if (isbillday && payrpf.getBillfreq() != null && "12".equals(payrpf.getBillfreq())
				&& ("D".equals(payrpf.getBillchnl()) || "R".equals(payrpf.getBillchnl()))) {
			effdatePlusCntlead = l2BillingProcessorDTO.getEffDate();
			l2BillingProcessorDTO.setEffdatePlusCntlead(effdatePlusCntlead);
		}
		int curTranno = 0;
		Chdrpf chdrpf = new Chdrpf();
		chdrpf.setCnttype(readerDTO.getCnttype());
		l2BillingProcessorDTO.setChdrpf(chdrpf);
		l2BillingProcessorDTO.setPayrpf(payrpf);
		while (!((payrpf.getBillcd() > effdatePlusCntlead)
				|| ((payrpf.getBillcd() >= readerDTO.getBillspfrom()) && (payrpf.getBillcd() < readerDTO.getBillspto())
						&& (l2BillingProcessorDTO.getEffDate() < readerDTO.getBillspto())))) {
			if (curTranno == 0) {
				curTranno = readerDTO.getChdrtranno();
			}
			curTranno++;
			chdrpf.setTranno(curTranno);
			payrpf.setTranno(chdrpf.getTranno());
			a1000GetCurrConvDate(l2BillingProcessorDTO, readerDTO, payrpf.getBillcd());
			calcPayrPrem(readerDTO, l2BillingProcessorDTO);
			advanceBtdate(readerDTO, l2BillingProcessorDTO);
			calcTax(readerDTO, l2BillingProcessorDTO);
			automaticIncrease(readerDTO, l2BillingProcessorDTO);
			Linspf linspf = writeLins(readerDTO, l2BillingProcessorDTO);
			produceBextRecord(linspf, readerDTO, l2BillingProcessorDTO);
			writePtrnRecord(linspf, readerDTO, l2BillingProcessorDTO);

			/* Advance the old billed to date for subsequent billing */
			l2BillingProcessorDTO.setOldBtdate(payrpf.getBtdate());
			/* Add increase due and tax to outstanding amount */

			chdrpf.setOutstamt(chdrpf.getOutstamt()
					.add(l2BillingProcessorDTO.getIncreaseDue().add(l2BillingProcessorDTO.getTax())));
			payrpf.setOutstamt(payrpf.getOutstamt()
					.add(l2BillingProcessorDTO.getIncreaseDue().add(l2BillingProcessorDTO.getTax())));
		}

		readerDTO.setTaxdpfs(l2BillingProcessorDTO.getTaxdpfs());
		readerDTO.setLinspfList(l2BillingProcessorDTO.getLinspfList());
		readerDTO.setHdivpfList(l2BillingProcessorDTO.getHdivpfList());
		readerDTO.setPtrnpfList(l2BillingProcessorDTO.getPtrnpfList());
		readerDTO.setFpcopfList(l2BillingProcessorDTO.getFpcopfList());
		readerDTO.setFprmpfList(l2BillingProcessorDTO.getFprmpfList());

		if (payrpf.getBillcd() >= readerDTO.getBillspfrom() && payrpf.getBillcd() < readerDTO.getBillspto()
				&& l2BillingProcessorDTO.getEffDate() < readerDTO.getBillspto()) {
			l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal02(BigDecimal.ONE);
		}
		rewriteChdr(readerDTO, l2BillingProcessorDTO);
		rewritePayr(readerDTO, l2BillingProcessorDTO);
		writeLetter(readerDTO, l2BillingProcessorDTO);
	}

	private Payrpf readPayr(L2BillingReaderDTO readerDTO) {
		Payrpf payrpf = new Payrpf();
		payrpf.setBtdate(readerDTO.getBtdate());
		payrpf.setNextdate(readerDTO.getNextdate());
		payrpf.setBillcd(readerDTO.getPayrbillcd());
		payrpf.setBillchnl(readerDTO.getChdrbillchnl());
		payrpf.setBillfreq(readerDTO.getBillfreq());
		payrpf.setEffdate(readerDTO.getEffdate());
		payrpf.setSinstamt01(readerDTO.getSinstamt01());
		payrpf.setPtdate(readerDTO.getPtdate());
		return payrpf;
	}

	private void readAcbl(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {

		Acblpf acblpf = new Acblpf();
		acblpf.setRldgacct(readerDTO.getChdrnum());
		acblpf.setRldgcoy(Companies.LIFE);
		acblpf.setOrigcurr(readerDTO.getPayrbillcurr());
		acblpf.setSacscode(l2BillingProcessorDTO.getT5645IO().getSacscodes().get(0)); // sacscode01
		acblpf.setSacstyp(l2BillingProcessorDTO.getT5645IO().getSacstypes().get(0)); // sacstype01
		Acblpf currAcbl = acblpfDAO.readAcblpf(acblpf);

		/* Multiply the balance by the sign from T3695. */
		BigDecimal suspAvail = BigDecimal.ZERO;
		if (currAcbl != null) {
			if ("-".equals(l2BillingProcessorDTO.getT3695IO().getSign())) {
				suspAvail = BigDecimal.ZERO.subtract(currAcbl.getSacscurbal());
			} else {
				suspAvail = currAcbl.getSacscurbal();
			}
		}
		l2BillingProcessorDTO.setSuspAvail(suspAvail);
		includeApa(readerDTO, l2BillingProcessorDTO);
	}

	private void includeApa(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		RlpdlonDTO rlpdlonDTO = new RlpdlonDTO();
		rlpdlonDTO.setChdrcoy(readerDTO.getChdrcoy());
		rlpdlonDTO.setChdrnum(readerDTO.getChdrnum());
		rlpdlonDTO.setPrmdepst(BigDecimal.ZERO);
		rlpdlonDTO.setLanguage(l2BillingProcessorDTO.getLang());
		RlpdlonDTO resultRlpdlonDTO = null;
		try {
			resultRlpdlonDTO = rlpdlon.processRlpdlonInfo(rlpdlonDTO);
		} catch (IOException e) {
			throw new SubroutineIOException("L2billing: IOException in rlpdlon: " + e.getMessage());
		}
		if (!STATUS_OK.equals(resultRlpdlonDTO.getStatuz())) {
			fatalError(resultRlpdlonDTO.getStatuz());
		}
		BigDecimal suspAvail = l2BillingProcessorDTO.getSuspAvail();
		suspAvail = suspAvail.add(resultRlpdlonDTO.getPrmdepst());
		/* <LA2106> */
		/* Store LP S amount just read to WSAA-PREM-SUSP <LA2106> */
		BigDecimal premSusp = suspAvail;
		/* MOVE WSAA-PREM-SUSP TO ZRDP-AMOUNT-IN. */
		/* MOVE PAYR-BILLCURR TO ZRDP-CURRENCY. */
		/* PERFORM 8000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO WSAA-PREM-SUSP. */
		if (BigDecimal.ZERO.compareTo(premSusp) != 0) {
			premSusp = callRounding8000(premSusp, readerDTO.getPayrbillcurr(), l2BillingProcessorDTO.getBatchTrcde());
		}
		l2BillingProcessorDTO.setPremSusp(premSusp);
		/* <LA2106> */
		/* Read ACBL for LC DS <LA2106> */
		Acblpf acblpf = new Acblpf();
		acblpf.setRldgacct(readerDTO.getChdrnum());
		acblpf.setRldgcoy(Companies.LIFE);
		acblpf.setOrigcurr(readerDTO.getChdrcntcurr());
		acblpf.setSacscode(l2BillingProcessorDTO.getT5645IO().getSacscodes().get(2)); // sacscode03
		acblpf.setSacstyp(l2BillingProcessorDTO.getT5645IO().getSacstypes().get(2)); // sacstype03
		Acblpf currAcbl = acblpfDAO.readAcblpf(acblpf);

		if (currAcbl == null) {
			l2BillingProcessorDTO.setDvdSusp(BigDecimal.ZERO);
			return;
		}

		if (BigDecimal.ZERO.compareTo(currAcbl.getSacscurbal()) == 0) {
			l2BillingProcessorDTO.setDvdSusp(BigDecimal.ZERO);
			return;
		}

		T3695 t3695IO = readT3695(l2BillingProcessorDTO.getT5645IO().getSacstypes().get(2));// sacstype03
		l2BillingProcessorDTO.setT3695IO(t3695IO);
		if ("-".equals(t3695IO.getSign())) {
			currAcbl.setSacscurbal(currAcbl.getSacscurbal().multiply(new BigDecimal(-1)));
		}
		/* <LA2106> */
		/* Convert if billing ccy and contract ccy differs. <LA2106> */
		BigDecimal dvdSusp = BigDecimal.ZERO;
		if (!readerDTO.getBillcurr().equals(readerDTO.getChdrcntcurr())) {

			ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
			conlinkInput.setFromCurrency(readerDTO.getChdrcntcurr());
			conlinkInput.setToCurrency(readerDTO.getBillcurr());
			conlinkInput.setCompany(readerDTO.getChdrcoy());
			conlinkInput.setAmount(currAcbl.getSacscurbal());
			conlinkInput.setCashdate(99999999);
			try {
				l2BillingProcessorDTO.setConlinkOutputDTO(xcvrt.executeRealFunction(conlinkInput));
			} catch (IOException e) {
				throw new SubroutineIOException(XCVRT_MSG + e.getMessage());
			}
			dvdSusp = l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount();

			if (BigDecimal.ZERO.compareTo(dvdSusp) != 0) {
				dvdSusp = callRounding8000(dvdSusp, readerDTO.getBillcurr(), l2BillingProcessorDTO.getBatchTrcde());
			}
		} else {
			dvdSusp = currAcbl.getSacscurbal();
		}
		l2BillingProcessorDTO.setDvdSusp(dvdSusp);
		/* <LA2106> */
		/* Consolidate total suspense <LA2106> */
		l2BillingProcessorDTO.setSuspAvail(l2BillingProcessorDTO.getSuspAvail().add(dvdSusp));
	}

	private BigDecimal callRounding8000(BigDecimal amountIn, String currency, String batchTrcde) {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(currency);
		zrdecplDTO.setCompany(Companies.LIFE);
		zrdecplDTO.setBatctrcde(batchTrcde);
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = zrdecplc.convertAmount(zrdecplDTO);
		} catch (IOException e) {
			throw new ItemParseException("L2billing: Parsed failed in zrdecplc:" + e.getMessage());
		}
		return result;
	}

	private boolean checkWop(Covrpf covrpf) {
		/* Check to see if coverage is found in TR517 */
		return readTr517(covrpf.getCrtable(), covrpf.getCrrcd());
	}

	private boolean readTr517(String itemKey, Integer crrCd) {
		List<Itempf> tr517List = itempfService.readSmartTableList(Companies.LIFE, "TR517", itemKey);
		if (tr517List != null && !tr517List.isEmpty()) {
			Iterator<Itempf> iterator = tr517List.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf.getItmfrm() > 0) {
					if (crrCd >= itempf.getItmfrm() && crrCd <= itempf.getItmto()) {
						return true;
					}
				} else {
					return true;
				}
			}
		}
		return false;
	}

	private void a1000GetCurrConvDate(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO,
			int billcd) {
		/* If Contract and Billing currency is not the same : */
		/* Subtract the lead days from the current BILLCD to get a date */
		/* for currency conversion. This date will be used when calling */
		/* XCVRT subroutine. For normal case this date = BSSC-EFF-DATE. */
		int cashdate = 99999999;
		if (readerDTO.getPayrcntcurr().equals(readerDTO.getPayrbillcurr())) {
			l2BillingProcessorDTO.setCashdate(cashdate);
			return;
		}
		cashdate = datcon2.plusDays(billcd, 0 - l2BillingProcessorDTO.getT6654IO().getLeadDays());
		if (cashdate < readerDTO.getOccdate()) {
			cashdate = readerDTO.getOccdate();
		}
		l2BillingProcessorDTO.setCashdate(cashdate);
	}

	private void calcPayrPrem(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Check if the PAYR record has the correct premium for the */
		/* instalment date, if not obtain it from history records */
		Payrpf currPayrLif = new Payrpf();
		l2BillingProcessorDTO.setOldBillcd(readerDTO.getBillcd());
		l2BillingProcessorDTO.setOldNextdate(readerDTO.getNextdate());
		l2BillingProcessorDTO.setGotPayrAtBtdate("N");
		if (readerDTO.getBtdate() < readerDTO.getEffdate()) {
			String chdrnum = readerDTO.getChdrnum();
			List<Payrpf> payrList = payrpfDAO.searchPayrRecord(Companies.LIFE, chdrnum);
			if (payrList != null && !payrList.isEmpty()) {
				for (Payrpf payr : payrList) {
					if (readerDTO.getBtdate() >= payr.getEffdate()) {
						l2BillingProcessorDTO.setGotPayrAtBtdate("Y");
						currPayrLif = payr;
						l2BillingProcessorDTO.setCurrPayrLif(currPayrLif);
						break;
					}
				}
			}
		}

		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			if ("Y".equals(l2BillingProcessorDTO.getFirstBill())) {
				l2BillingProcessorDTO.getPayrpf()
						.setOutstamt(currPayrLif.getOutstamt().add(currPayrLif.getSinstamt06()));
				l2BillingProcessorDTO.setFirstBill("N");
			} else {
				l2BillingProcessorDTO.getPayrpf().setOutstamt(readerDTO.getOutstamt().add(currPayrLif.getSinstamt06()));
			}
			l2BillingProcessorDTO.getChdrpf().setOutstamt(l2BillingProcessorDTO.getPayrpf().getOutstamt());
		} else {
			/* COMPUTE CONT-TOTVAL = PAYR-SINSTAMT06 + */
			/* WSAA-INCREASE-DUE */
			l2BillingProcessorDTO.getPayrpf().setOutstamt(readerDTO.getOutstamt().add(readerDTO.getSinstamt06()));
			/* PAYR-SINSTAMT06 + */
			/* WSAA-INCREASE-DUE */
			l2BillingProcessorDTO.getChdrpf().setOutstamt(l2BillingProcessorDTO.getPayrpf().getOutstamt());
		}
	}

	private void advanceBtdate(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Advance the 'Billed to date' by one Frequency. */
		/* The current saved BTDATE is used, so that the INSTFROM and */
		/* BILLdates can be updated. */

		int intDate2 = datcon4.getIntDate2(readerDTO.getBillfreq(),
				l2BillingProcessorDTO.getPayrpf().getBtdate().toString(), 1);

		l2BillingProcessorDTO.setOldBtdate(l2BillingProcessorDTO.getPayrpf().getBtdate());
		l2BillingProcessorDTO.getPayrpf().setBtdate(intDate2);
		l2BillingProcessorDTO.getChdrpf().setBtdate(intDate2);
		l2BillingProcessorDTO.setOldBillcd(l2BillingProcessorDTO.getPayrpf().getBillcd());
		l2BillingProcessorDTO.setOldNextdate(l2BillingProcessorDTO.getPayrpf().getNextdate());
		/* Advance the BILLING COMMENCEMENT DATE by one frequency */
		intDate2 = datcon4.getIntDate2(readerDTO.getBillfreq(),
				l2BillingProcessorDTO.getPayrpf().getBillcd().toString(), 1);
		l2BillingProcessorDTO.getPayrpf().setBillcd(intDate2);
		l2BillingProcessorDTO.getChdrpf().setBillcd(intDate2);
		/* Subtract the lead days from the billed to date to calculate */
		/* the next billing extract date for the PAYR record. */
		int freqFactor = 0 - l2BillingProcessorDTO.getT6654IO().getLeadDays();
		l2BillingProcessorDTO.getPayrpf()
				.setNextdate(datcon2.plusDays(l2BillingProcessorDTO.getPayrpf().getBillcd(), freqFactor));
	}

	private void calcTax(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		l2BillingProcessorDTO.setTax(BigDecimal.ZERO);
		if (l2BillingProcessorDTO.isFlexiblePremiumContract() || (l2BillingProcessorDTO.getTr52dIO().getTxcode() == null
				|| l2BillingProcessorDTO.getTr52dIO().getTxcode().trim().isEmpty())) {
			return;
		}
		Covrpf currCovr = null;
		String chdrnum = readerDTO.getChdrnum();
		List<Covrpf> covrpfList = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(Companies.LIFE, chdrnum);
		if (covrpfList != null && !covrpfList.isEmpty()) {
			for (Covrpf covr : covrpfList) {
				processCovrTax(covr, readerDTO, l2BillingProcessorDTO);
				currCovr = covr;
			}
		}
		if (readerDTO.getSinstamt02().compareTo(BigDecimal.ZERO) > 0) {
			processCtfeeTax(currCovr, readerDTO, l2BillingProcessorDTO);
		}
	}

	private void processCovrTax(Covrpf covr, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Calculate tax on premiums. */
		String wsaaValidCovr = "N";
		/* Check to see CURRFROM <= PAYR-BTDATE & CURRTO > PAYR-BTDATE */
		if ((covr.getCurrfrom() < readerDTO.getBtdate() || covr.getCurrfrom() == readerDTO.getBtdate())
				&& covr.getCurrto() > readerDTO.getBtdate()) {
			/* NEXT_SENTENCE */
		} else {
			return;
		}

		/* If installment premium = zero, exit */
		if (covr.getInstprem().compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		/* Check to see if coverage is of a valid status */
		if (!"1".equals(covr.getValidflag())) {
			return;
		}
		for (int wsaaT5679Sub = 0; wsaaT5679Sub < 12; wsaaT5679Sub++) {
			if (l2BillingProcessorDTO.getT5679IO().getCovRiskStats().get(wsaaT5679Sub).equals(covr.getStatcode())) {
				for (int wsaaT5679Sub2 = 0; wsaaT5679Sub2 < 12; wsaaT5679Sub2++) {
					if (l2BillingProcessorDTO.getT5679IO().getCovPremStats().get(wsaaT5679Sub2)
							.equals(covr.getPstatcode())) {
						wsaaValidCovr = "Y";
						break;
					}
				}
				if ("Y".equals(wsaaValidCovr)) {
					break;
				}
			}
		}
		/* If the coverage is not of a valid status read the next */
		/* record for the contract: */
		if (!"Y".equals(wsaaValidCovr)) {
			return;
		}
		/* Read table TR52E. */
		StringBuilder sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(l2BillingProcessorDTO.getTr52dIO().getTxcode().trim());
		sbTr52eKey.append(readerDTO.getCnttype().trim());
		sbTr52eKey.append(covr.getCrtable().trim());
		Tr52e tr52e = readTr52e(sbTr52eKey.toString());

		if (tr52e == null) {
			sbTr52eKey = new StringBuilder("");
			sbTr52eKey.append(l2BillingProcessorDTO.getTr52dIO().getTxcode().trim());
			sbTr52eKey.append(readerDTO.getCnttype().trim());
			sbTr52eKey.append("****");
			tr52e = readTr52e(sbTr52eKey.toString());
			if (tr52e == null) {
				sbTr52eKey = new StringBuilder("");
				sbTr52eKey.append(l2BillingProcessorDTO.getTr52dIO().getTxcode().trim());
				sbTr52eKey.append("***");
				sbTr52eKey.append("****");
				tr52e = readTr52e(sbTr52eKey.toString());
				if (tr52e == null) {
					throw new ItemNotfoundException(
							"L2billing: Item not found in Table Tr52e: " + sbTr52eKey.toString());
				}

			}
		}
		l2BillingProcessorDTO.setTr52eIO(tr52e);
		/* If TR52E tax indicator not = 'Y', do not calculate tax */
		if (!"Y".equals(l2BillingProcessorDTO.getTr52eIO().getTaxinds().get(0))) {
			return;
		}

		Incrpf currIncr = null;
		String chdrnum = readerDTO.getChdrnum();
		boolean incrFound = false;

		List<Incrpf> incrpfList = incrpfDAO.readValidIncrpf(Companies.LIFE, chdrnum);
		if (incrpfList != null && !incrpfList.isEmpty()) {
			for (Incrpf incr : incrpfList) {
				if (incr.getLife().equals(covr.getLife()) && incr.getCoverage().equals(covr.getCoverage())
						&& incr.getRider().equals(covr.getRider()) && incr.getPlnsfx().equals(covr.getPlnsfx())) {
					currIncr = incr;
					incrFound = true;
					break;

				}
			}
		}
		BigDecimal wsaaIncrBinstprem = BigDecimal.ZERO;
		BigDecimal wsaaIncrInstprem = BigDecimal.ZERO;
		if (incrFound && currIncr.getValidflag().equals("1") && currIncr.getCrrcd() < readerDTO.getBtdate()) {
			wsaaIncrBinstprem = currIncr.getZbnewinst().subtract(currIncr.getZblastinst());
			wsaaIncrInstprem = currIncr.getNewinst().subtract(currIncr.getLastInst());
		}

		/* Set values to tax linkage and call tax subroutine */
		TxcalcDTO txcalcDTO = new TxcalcDTO();
		txcalcDTO.setFunction("CALC");
		txcalcDTO.setStatuz(STATUS_OK);
		txcalcDTO.setTransType("PREM");
		txcalcDTO.setChdrcoy(covr.getChdrcoy());
		txcalcDTO.setChdrnum(covr.getChdrnum());
		txcalcDTO.setTaxrule(sbTr52eKey.toString());
		txcalcDTO.setCcy(readerDTO.getChdrcntcurr());
		txcalcDTO.setRateItem(readerDTO.getChdrcntcurr() + l2BillingProcessorDTO.getTr52eIO().getTxitem());
		txcalcDTO.setTxcode(l2BillingProcessorDTO.getTr52dIO().getTxcode());
		txcalcDTO.setAmountIn(BigDecimal.ZERO);
		if ("Y".equals(l2BillingProcessorDTO.getTr52eIO().getZbastyp())) {
			txcalcDTO.setAmountIn(covr.getZbinstprem().add(wsaaIncrBinstprem));
		} else {
			txcalcDTO.setAmountIn(covr.getInstprem().add(wsaaIncrInstprem));
		}
		txcalcDTO.setEffdate(l2BillingProcessorDTO.getOldBtdate());
		List<BigDecimal> taxAmts = new ArrayList<>();
		taxAmts.add(0, BigDecimal.ZERO);
		taxAmts.add(1, BigDecimal.ZERO);
		txcalcDTO.setTaxAmts(taxAmts);
		List<String> taxTyps = new ArrayList<>();
		taxTyps.add(0, "");
		taxTyps.add(1, "");
		txcalcDTO.setTaxTypes(taxTyps);
		List<String> taxAbsorbs = new ArrayList<>();
		taxAbsorbs.add(0, "");
		taxAbsorbs.add(1, "");
		txcalcDTO.setTaxAbsorbs(taxAbsorbs);
		txcalcDTO.setBatctrcde("B521");
		if (l2BillingProcessorDTO.getTr52dIO().getTxsubr().equalsIgnoreCase("Servtax")) {
			try {
				txcalcDTO = servtax.processServtax(txcalcDTO);
			} catch (IOException e) {
				throw new SubroutineIOException("L2billing: IOException in servtax: " + e.getMessage());
			}
		}
		if (txcalcDTO.getTaxAmts().get(0).compareTo(BigDecimal.ZERO) > 0
				|| txcalcDTO.getTaxAmts().get(1).compareTo(BigDecimal.ZERO) > 0) {
			if (!"Y".equals(txcalcDTO.getTaxAbsorb01())) {
				l2BillingProcessorDTO.setTax(l2BillingProcessorDTO.getTax().add(txcalcDTO.getTaxAmts().get(0)));
			}
			if (!"Y".equals(txcalcDTO.getTaxAbsorb02())) {
				l2BillingProcessorDTO.setTax(l2BillingProcessorDTO.getTax().add(txcalcDTO.getTaxAmts().get(1)));
			}
		}

		/* Create TAXD record */
		Taxdpf taxdpf = new Taxdpf();

		taxdpf.setChdrcoy(covr.getChdrcoy());
		taxdpf.setChdrnum(covr.getChdrnum());
		taxdpf.setTrantype("PREM");
		taxdpf.setLife(covr.getLife());
		taxdpf.setCoverage(covr.getCoverage());
		taxdpf.setRider(covr.getRider());
		taxdpf.setPlansfx(0);
		taxdpf.setEffdate(l2BillingProcessorDTO.getOldBtdate());
		taxdpf.setInstfrom(l2BillingProcessorDTO.getOldBtdate());
		taxdpf.setInstto(readerDTO.getBtdate());
		taxdpf.setBillcd(l2BillingProcessorDTO.getBillcd());
		taxdpf.setTranref(txcalcDTO.getTaxrule() + txcalcDTO.getRateItem());
		taxdpf.setTranno(l2BillingProcessorDTO.getChdrpf().getTranno());
		taxdpf.setBaseamt(txcalcDTO.getAmountIn());
		taxdpf.setTaxamt01(txcalcDTO.getTaxAmts().get(0));
		taxdpf.setTaxamt02(txcalcDTO.getTaxAmts().get(1));
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setTxabsind01(txcalcDTO.getTaxAbsorbs().get(0));
		taxdpf.setTxabsind02(txcalcDTO.getTaxAbsorbs().get(1));
		taxdpf.setTxabsind03("");
		taxdpf.setTxtype01(txcalcDTO.getTaxTypes().get(0));
		taxdpf.setTxtype02(txcalcDTO.getTaxTypes().get(1));
		taxdpf.setTxtype03("");
		taxdpf.setPostflg("");
		taxdpf.setUsrprf(l2BillingProcessorDTO.getUserName());
		taxdpf.setJobnm(l2BillingProcessorDTO.getBatchName());
		l2BillingProcessorDTO.getTaxdpfs().add(taxdpf);
	}

	private void processCtfeeTax(Covrpf covr, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {

		/* Read table TR52E. */
		StringBuilder sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(l2BillingProcessorDTO.getTr52dIO().getTxcode().trim());
		sbTr52eKey.append(readerDTO.getCnttype().trim());
		sbTr52eKey.append(covr.getCrtable().trim());
		Tr52e tr52e = readTr52e(sbTr52eKey.toString());

		if (tr52e == null) {
			sbTr52eKey = new StringBuilder("");
			sbTr52eKey.append(l2BillingProcessorDTO.getTr52dIO().getTxcode().trim());
			sbTr52eKey.append(readerDTO.getCnttype().trim());
			sbTr52eKey.append("****");
			tr52e = readTr52e(sbTr52eKey.toString());
			if (tr52e == null) {
				sbTr52eKey = new StringBuilder("");
				sbTr52eKey.append(l2BillingProcessorDTO.getTr52dIO().getTxcode().trim());
				sbTr52eKey.append("***");
				sbTr52eKey.append("****");
				tr52e = readTr52e(sbTr52eKey.toString());
				if (tr52e == null) {
					throw new ItemNotfoundException(
							"L2billing: Item not found in Table Tr52e: " + sbTr52eKey.toString());
				}
			}
		}
		l2BillingProcessorDTO.setTr52eIO(tr52e);
		/* If TR52E tax indicator2 not 'Y', do not calculate tax */
		if (!"Y".equals(tr52e.getTaxinds().get(1))) {// taxind02
			return;
		}
		/* Set values to tax linkage and call tax subroutine */
		TxcalcDTO txcalcDTO = new TxcalcDTO();
		txcalcDTO.setFunction("CALC");
		txcalcDTO.setStatuz(STATUS_OK);
		txcalcDTO.setTransType("CNTF");
		txcalcDTO.setChdrcoy(readerDTO.getChdrcoy());
		txcalcDTO.setChdrnum(readerDTO.getChdrnum());
		txcalcDTO.setTaxrule(sbTr52eKey.toString());
		txcalcDTO.setCcy(readerDTO.getChdrcntcurr());
		txcalcDTO.setRateItem(readerDTO.getChdrcntcurr() + l2BillingProcessorDTO.getTr52eIO().getTxitem());
		txcalcDTO.setEffdate(l2BillingProcessorDTO.getOldBtdate());
		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			txcalcDTO.setAmountIn(l2BillingProcessorDTO.getCurrPayrLif().getSinstamt02());
		} else {
			txcalcDTO.setAmountIn(readerDTO.getSinstamt02());
		}
		List<BigDecimal> taxAmts = new ArrayList<>();
		taxAmts.add(0, BigDecimal.ZERO);
		taxAmts.add(1, BigDecimal.ZERO);
		txcalcDTO.setTaxAmts(taxAmts);
		List<String> taxTyps = new ArrayList<>();
		taxTyps.add(0, "");
		taxTyps.add(1, "");
		txcalcDTO.setTaxTypes(taxTyps);
		List<String> taxAbsorbs = new ArrayList<>();
		taxAbsorbs.add(0, "");
		taxAbsorbs.add(1, "");
		txcalcDTO.setTaxAbsorbs(taxAbsorbs);
		txcalcDTO.setBatctrcde("B521");
		if (l2BillingProcessorDTO.getTr52dIO().getTxsubr().equalsIgnoreCase("Servtax")) {
			try {
				txcalcDTO = servtax.processServtax(txcalcDTO);
			} catch (IOException e) {
				throw new SubroutineIOException("L2billing: IOException in servtax: " + e.getMessage());
			}
		}
		if (!"****".equals(txcalcDTO.getStatuz())) {
			throw new SubroutineIOException("L2billing: IOException in servtax");
		}
		if (txcalcDTO.getTaxAmts().get(0).compareTo(BigDecimal.ZERO) > 0
				|| txcalcDTO.getTaxAmts().get(1).compareTo(BigDecimal.ZERO) > 0) {
			if (!"Y".equals(txcalcDTO.getTaxAbsorbs().get(0))) {
				l2BillingProcessorDTO.setTax(l2BillingProcessorDTO.getTax().add(txcalcDTO.getTaxAmts().get(0)));
			}
			if (!"Y".equals(txcalcDTO.getTaxAbsorbs().get(1))) {
				l2BillingProcessorDTO.setTax(l2BillingProcessorDTO.getTax().add(txcalcDTO.getTaxAmts().get(1)));
			}
		}
		/* Create TAXD record */
		Taxdpf taxdpf = new Taxdpf();
		taxdpf.setChdrcoy(readerDTO.getChdrcoy());
		taxdpf.setChdrnum(readerDTO.getChdrnum());
		taxdpf.setTrantype("CNTF");
		taxdpf.setLife("");
		taxdpf.setCoverage("");
		taxdpf.setRider("");
		taxdpf.setPlansfx(0);
		taxdpf.setEffdate(l2BillingProcessorDTO.getOldBtdate());
		taxdpf.setInstfrom(l2BillingProcessorDTO.getOldBtdate());
		taxdpf.setInstto(readerDTO.getBtdate());
		taxdpf.setBillcd(l2BillingProcessorDTO.getOldBillcd());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(txcalcDTO.getTaxrule());
		stringVariable1.append(txcalcDTO.getRateItem());
		taxdpf.setTranref(stringVariable1.toString());
		taxdpf.setTranno(l2BillingProcessorDTO.getChdrpf().getTranno());
		taxdpf.setBaseamt(txcalcDTO.getAmountIn());
		taxdpf.setTaxamt01(txcalcDTO.getTaxAmts().get(0));
		taxdpf.setTaxamt02(txcalcDTO.getTaxAmts().get(1));
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setTxabsind01(txcalcDTO.getTaxAbsorbs().get(0));
		taxdpf.setTxabsind02(txcalcDTO.getTaxAbsorbs().get(1));
		taxdpf.setTxabsind03("");
		taxdpf.setTxtype01(txcalcDTO.getTaxTypes().get(0));
		taxdpf.setTxtype02(txcalcDTO.getTaxTypes().get(1));
		taxdpf.setTxtype03("");
		taxdpf.setPostflg("");
		taxdpf.setUsrprf(l2BillingProcessorDTO.getUserName());
		taxdpf.setJobnm(l2BillingProcessorDTO.getBatchName());
		l2BillingProcessorDTO.getTaxdpfs().add(taxdpf);
	}

	private Tr52e readTr52e(String itemKey) {
		String keyItemitem = itemKey.trim();
		return itempfService.readSmartTableByTrim(Companies.LIFE, "TR52E", keyItemitem, Tr52e.class);
	}

	private void automaticIncrease(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Look up any automatic increases. */
		l2BillingProcessorDTO.setIncreaseDue(BigDecimal.ZERO);

		String chdrnum = readerDTO.getChdrnum();
		List<Incrpf> incrpfList = incrpfDAO.readValidIncrpf(Companies.LIFE, chdrnum);
		if (incrpfList != null && !incrpfList.isEmpty()) {
			for (Incrpf incr : incrpfList) {
				if (incr.getCrrcd() < readerDTO.getBtdate()) {
					l2BillingProcessorDTO.setIncreaseDue(
							l2BillingProcessorDTO.getIncreaseDue().add(incr.getNewinst()).subtract(incr.getLastInst()));
				}
			}
		}

		/* Update Increase amount into PAYR OUTSTAMT */
		l2BillingProcessorDTO.getPayrpf().setOutstamt(
				l2BillingProcessorDTO.getPayrpf().getOutstamt().add(l2BillingProcessorDTO.getIncreaseDue()));
		l2BillingProcessorDTO.getChdrpf().setOutstamt(l2BillingProcessorDTO.getPayrpf().getOutstamt());
		BigDecimal tempTotval = null;
		/* Log total amount billed. */
		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			tempTotval = l2BillingProcessorDTO.getCurrPayrLif().getSinstamt06()
					.add(l2BillingProcessorDTO.getIncreaseDue());
		} else {
			tempTotval = readerDTO.getSinstamt06().add(l2BillingProcessorDTO.getIncreaseDue());
		}
		tempTotval = tempTotval.add(l2BillingProcessorDTO.getTax());
		l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal04(
				tempTotval.add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal04()));
	}

	private Linspf writeLins(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		Linspf linspf = new Linspf();
		linspf.setInstjctl("");
		linspf.setDueflg("");
		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			linspf.setInstamt01(readerDTO.getSinstamt01());
			linspf.setInstamt01(linspf.getInstamt01().add(l2BillingProcessorDTO.getIncreaseDue()));
			Payrpf currPayrLif = l2BillingProcessorDTO.getCurrPayrLif();
			linspf.setInstamt02(currPayrLif.getSinstamt02());
			linspf.setInstamt03(currPayrLif.getSinstamt03());
			linspf.setInstamt04(currPayrLif.getSinstamt04());
			linspf.setInstamt05(currPayrLif.getSinstamt05());
			linspf.setInstamt06(currPayrLif.getSinstamt06());
			linspf.setInstamt06(linspf.getInstamt06().add(l2BillingProcessorDTO.getIncreaseDue()));

		} else {
			linspf.setInstamt01(readerDTO.getSinstamt01());
			linspf.setInstamt01(linspf.getInstamt01().add(l2BillingProcessorDTO.getIncreaseDue()));
			linspf.setInstamt02(readerDTO.getSinstamt02());
			linspf.setInstamt03(readerDTO.getSinstamt03());
			linspf.setInstamt04(readerDTO.getSinstamt04());
			linspf.setInstamt05(readerDTO.getSinstamt05());
			linspf.setInstamt06(readerDTO.getSinstamt06());
			linspf.setInstamt06(linspf.getInstamt06().add(l2BillingProcessorDTO.getIncreaseDue()));
		}
		BigDecimal wsaaBillAmount = linspf.getInstamt06();
		linspf.setInstamt06(linspf.getInstamt06().add(l2BillingProcessorDTO.getTax()));
		/* Convert contract amount(total) to billing amount if contract */
		/* currency is not the same as billing currency. */
		if (readerDTO.getPayrcntcurr().equals(readerDTO.getPayrbillcurr())
				|| linspf.getInstamt06().compareTo(BigDecimal.ZERO) == 0) {
			linspf.setCbillamt(linspf.getInstamt06());
		} else {
			l2BillingProcessorDTO.getConlinkDTO().setAmount(linspf.getInstamt06());
			callXcvrt5000(readerDTO, l2BillingProcessorDTO);
			linspf.setCbillamt(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
		}
		/* If this is a flexible premium contract then we do not <D9604> */
		/* want to write a LINS record. We have come this far <D9604> */
		/* however as we do need to know linsrnl-cbillamt value <D9604> */
		/* to update the FPRM file. Also we maintain CT11 which <D9604> */
		/* logs the number of flexible premium billings: <D9604> */
		if (l2BillingProcessorDTO.isFlexiblePremiumContract()) {
			writeFpco(readerDTO, l2BillingProcessorDTO);
			l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal11(
					BigDecimal.ONE.add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal11()));
			return linspf;
		}
		linspf.setChdrnum(readerDTO.getChdrnum());
		linspf.setChdrcoy(readerDTO.getChdrcoy());
		linspf.setBillchnl(readerDTO.getBillchnl());
		linspf.setPayrseqno(readerDTO.getPayrseqno());
		linspf.setBranch(l2BillingProcessorDTO.getBranch());
		linspf.setTranscode(l2BillingProcessorDTO.getBprdAuthCode());
		linspf.setInstfreq(readerDTO.getBillfreq());
		linspf.setCntcurr(readerDTO.getPayrcntcurr());
		linspf.setBillcurr(readerDTO.getBillcurr());
		linspf.setInstto(l2BillingProcessorDTO.getPayrpf().getBtdate());
		linspf.setMandref(readerDTO.getMandref());
		linspf.setInstfrom(l2BillingProcessorDTO.getOldBtdate());
		linspf.setBillcd(l2BillingProcessorDTO.getOldBillcd());
		linspf.setAcctmeth(readerDTO.getAcctmeth());
		linspf.setPayflag("O");
		linspf.setValidflag("1");
		/* Update the tax relief method on the LINS record */
		/* if tax relief is applied. */
		/* MOVE LINSRNL-INSTAMT06 TO PRAS-GROSSPREM. */
		/* Exclude WSAA-TAX in calculating Tax Relief */
		if (readerDTO.getPayrcntcurr().equals(readerDTO.getPayrbillcurr())) {
			l2BillingProcessorDTO.getPrasDTO().setGrossprem(wsaaBillAmount);
		} else {
			l2BillingProcessorDTO.getConlinkDTO().setAmount(wsaaBillAmount);
			callXcvrt5000(readerDTO, l2BillingProcessorDTO);
			l2BillingProcessorDTO.getPrasDTO()
					.setGrossprem(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
		}
		calculateTaxRelief(linspf, readerDTO, l2BillingProcessorDTO);

		/* Log number of LINS written */
		l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal05(
				BigDecimal.ONE.add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal05()));
		l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal06(
				linspf.getInstamt06().add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal06()));
		linspf.setUserProfile(l2BillingProcessorDTO.getUserName());
		linspf.setJobName(
				l2BillingProcessorDTO.getBatchName().length() > 8 ? l2BillingProcessorDTO.getBatchName().substring(0, 7)
						: l2BillingProcessorDTO.getBatchName());
		l2BillingProcessorDTO.getLinspfList().add(linspf);
		return linspf;
	}

	private void callXcvrt5000(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {

		l2BillingProcessorDTO.getConlinkDTO().setFromCurrency(readerDTO.getPayrcntcurr());
		l2BillingProcessorDTO.getConlinkDTO().setToCurrency(readerDTO.getPayrbillcurr());
		l2BillingProcessorDTO.getConlinkDTO().setCashdate(l2BillingProcessorDTO.getCashdate());
		l2BillingProcessorDTO.getConlinkDTO().setCompany(Companies.LIFE);
		try {
			l2BillingProcessorDTO.setConlinkOutputDTO(xcvrt.executeSurrFunction(l2BillingProcessorDTO.getConlinkDTO()));
		} catch (IOException e) {
			throw new SubroutineIOException(XCVRT_MSG + e.getMessage());
		}
		if (l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount().compareTo(BigDecimal.ZERO) != 0) {
			l2BillingProcessorDTO.getConlinkOutputDTO().setCalculatedAmount(
					callRounding8000(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount(),
							readerDTO.getPayrbillcurr(), l2BillingProcessorDTO.getBprdAuthCode()));
		}
	}

	private void writeFpco(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		Fprmpf fprmpf = new Fprmpf();
		l2BillingProcessorDTO.setTotalAmount(BigDecimal.ZERO);
		BigDecimal wsaaOverduePer = BigDecimal.ZERO;

		String chdrnum = readerDTO.getChdrnum();
		List<Covrpf> covrpfList = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(Companies.LIFE, chdrnum);
		if (covrpfList != null && !covrpfList.isEmpty()) {
			for (Covrpf covr : covrpfList) {
				readCovrlnb(covr, readerDTO, l2BillingProcessorDTO);
			}
		}

		int payrSeqNo = 0;
		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			payrSeqNo = l2BillingProcessorDTO.getCurrPayrLif().getPayrseqno();
		} else {
			payrSeqNo = readerDTO.getPayrseqno();
		}

		/* Read the FPRM record: */
		Fprmpf currFprm = null;
		chdrnum = readerDTO.getChdrnum();
		List<Fprmpf> fprmpfListTemp = fprmpfDAO.readFprmpfRecords(Companies.LIFE, chdrnum);
		if (fprmpfListTemp != null && !fprmpfListTemp.isEmpty()) {
			for (Fprmpf fprm : fprmpfListTemp) {
				if (fprm.getPayrseqno() == payrSeqNo) {
					currFprm = fprm;
					break;
				}
			}
		}

		/* bug #ILIFE-1055 START */
		// payrIO.sinstamt02 CONTRACT MANAGEMENT FEE FROM P5074AT
		l2BillingProcessorDTO.setTotalAmount(l2BillingProcessorDTO.getTotalAmount().add(readerDTO.getSinstamt02()));

		/* bug #ILIFE-1055 end */
		/* Update total billed: <D9604> */
		// IJTI-320 START
		if (currFprm != null) {
			fprmpf.setUnique_number(currFprm.getUnique_number());
			fprmpf.setTotbill(currFprm.getTotbill().add(l2BillingProcessorDTO.getTotalAmount()));
		}
		// IJTI-320 END
		/* Log total billed: <D9604> */

		if ("12".equals(readerDTO.getBillfreq()) && BigDecimal.ZERO
				.compareTo(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal12()) != 0) {
			l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal12(readerDTO.getSinstamt01()
					.add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal12()));
		}
		l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal12(l2BillingProcessorDTO.getTotalAmount()
				.add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal12()));
		// IJTI-320 START
		if (currFprm != null) {
			fprmpf.setMinreqd(wsaaOverduePer.divide(BigDecimal.valueOf(100))
					.multiply(l2BillingProcessorDTO.getTotalAmount()).add(currFprm.getMinreqd()));

			/* MOVE FPRM-MIN-PRM-REQD TO ZRDP-AMOUNT-IN. */
			/* MOVE PAYR-BILLCURR TO ZRDP-CURRENCY. */
			/* PERFORM 8000-CALL-ROUNDING. */
			/* MOVE ZRDP-AMOUNT-OUT TO FPRM-MIN-PRM-REQD. */
			if (BigDecimal.ZERO.compareTo(currFprm.getMinreqd()) != 0) {
				fprmpf.setMinreqd(callRounding8000(currFprm.getMinreqd(), readerDTO.getPayrbillcurr(),
						l2BillingProcessorDTO.getBprdAuthCode()));
			}
		}
		/* Write the record back to FPRM file. <D9604> */
		fprmpf.setUsrprf(l2BillingProcessorDTO.getUserName());
		fprmpf.setJobnm(l2BillingProcessorDTO.getBatchName());
		if (l2BillingProcessorDTO.getFprmpfList() == null) {
			l2BillingProcessorDTO.setFprmpfList(new ArrayList<>());
		}
		l2BillingProcessorDTO.getFprmpfList().add(fprmpf);
	}

	private void readCovrlnb(Covrpf covr, L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Check to see if coverage is of a valid status <D9604> */
		boolean wsaaValidCoverage = false;
		if (!"1".equals(covr.getValidflag())) {
			return;
		}
		for (int wsaaT5679Sub = 0; wsaaT5679Sub <= 12; wsaaT5679Sub++) {
			if (l2BillingProcessorDTO.getT5679IO().getCovRiskStats().get(wsaaT5679Sub).equals(covr.getStatcode())) {
				for (int wsaaT5679Sub2 = 0; wsaaT5679Sub2 <= 12; wsaaT5679Sub2++) {
					if (l2BillingProcessorDTO.getT5679IO().getCovPremStats().get(wsaaT5679Sub2)
							.equals(covr.getPstatcode())) {
						wsaaValidCoverage = true;
					}
				}
			}
		}
		/* If the coverage is not of a valid status read the next <D9604> */
		/* record for the contract: <D9604> */
		if (!wsaaValidCoverage)
			return;

		if (covr.getInstprem().compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		Incrpf currIncr = null;
		String chdrnum = readerDTO.getChdrnum();

		boolean incrFound = false;
		List<Incrpf> incrpfList = incrpfDAO.readValidIncrpf(Companies.LIFE, chdrnum);
		if (incrpfList != null && !incrpfList.isEmpty()) {
			for (Incrpf incr : incrpfList) {
				if (incr.getCoverage().equals(covr.getCoverage()) && incr.getRider().equals(covr.getRider())
						&& incr.getPlnsfx().equals(covr.getPlnsfx())) {
					currIncr = incr;
					incrFound = true;
					break;

				}
			}
		}
		BigDecimal wsaaCovrInc = BigDecimal.ZERO;

		if (incrFound && currIncr.getCrrcd() < readerDTO.getBtdate()) {
			wsaaCovrInc = currIncr.getNewinst().subtract(currIncr.getLastInst());
		}

		/* Select FPCO record which is active and has not reached <D9604> */
		/* Target Premium <D9604> */
		List<Fpcopf> fpcopfList = fpcopfDAO.readFpcopfRecords(Companies.LIFE, chdrnum);
		if (fpcopfList != null && !fpcopfList.isEmpty()) {
			for (Fpcopf fpco : fpcopfList) {
				if (fpco.getLife().equals(covr.getLife()) && fpco.getCoverage().equals(covr.getCoverage())
						&& fpco.getRider().equals(covr.getRider()) && fpco.getPlnsfx().equals(covr.getPlnsfx())) {
					readFpco(fpco, covr, readerDTO, wsaaCovrInc, l2BillingProcessorDTO);
					break;
				}
			}
		}
		l2BillingProcessorDTO
				.setTotalAmount(l2BillingProcessorDTO.getTotalAmount().add(covr.getInstprem()).add(wsaaCovrInc));
	}

	private void readFpco(Fpcopf fpco, Covrpf covr, L2BillingReaderDTO readerDTO, BigDecimal wsaaCovrInc,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		Fpcopf fpcopf = new Fpcopf();
		if (fpco.getBilledp().compareTo(fpco.getPrmper()) >= 0) {
			return;
		}

		/* Update FPCO record with total of installment due and <D9604> */
		/* overdue minimum percentage. This should include any <D9604> */
		/* increases due <D9604> */
		fpcopf.setBilledp(fpco.getBilledp().add(covr.getInstprem()).add(wsaaCovrInc));

		BigDecimal wsaaOverduePer = new BigDecimal(fpco.getMinovrpro());
		fpcopf.setOvrminreq(wsaaOverduePer.divide(BigDecimal.valueOf(100)).multiply(covr.getInstprem().add(wsaaCovrInc))
				.add(fpco.getOvrminreq()));

		if (fpco.getOvrminreq().compareTo(BigDecimal.ZERO) != 0) {
			fpcopf.setOvrminreq(callRounding8000(fpco.getOvrminreq(), readerDTO.getBillcurr(),
					l2BillingProcessorDTO.getBatchTrcde()));
		}
		/* Write the record back to FPCO file. <D9604> */
		/* MOVE REWRT TO FPCO-FUNCTION. <V65L19> */
		fpcopf.setUniqueNumber(fpco.getUniqueNumber());
		fpcopf.setUsrprf(l2BillingProcessorDTO.getUserName());
		fpcopf.setJobnm(l2BillingProcessorDTO.getBatchName());
		l2BillingProcessorDTO.getFpcopfList().add(fpcopf);
	}

	private void calculateTaxRelief(Linspf linspf, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		linspf.setTaxrelmth("");
		l2BillingProcessorDTO.getPrasDTO().setTaxrelamt(BigDecimal.ZERO);
		if ("".equals(readerDTO.getTaxrelmth().trim())) {
			return;
		}

		/* Look up the subroutine on T6687 Array in working storage. */
		String strT6687TaxRelSub = readT6687(readerDTO);

		if (strT6687TaxRelSub == null || strT6687TaxRelSub.isEmpty()) {
			return;
		}
		/* If the tax relief method is not spaces calculate the tax */
		/* relief amount and deduct it from the premium...... */

		// prasrec.clntnum.set(readerDTO.getClntnum());
		// prasrec.clntcoy.set(readerDTO.getClntcoy());
		// prasrec.incomeSeqNo.set(readerDTO.getIncseqno());
		// prasrec.cnttype.set(readerDTO.getCnttype());
		// prasrec.taxrelmth.set(readerDTO.getTaxrelmth());
		// prasrec.effdate.set(payrpf.getBillcd());
		// prasrec.company.set(readerDTO.getChdrcoy());
		// prasrec.statuz.set(varcom.oK);
		// callProgram(strT6687TaxRelSub, prasrec.prascalcRec);
		// if (isNE(prasrec.statuz, varcom.oK)) {
		// syserrrec.statuz.set(prasrec.statuz);
		// syserrrec.subrname.set(strT6687TaxRelSub);
		// wsysSysparams.set(prasrec.prascalcRec);
		// syserrrec.params.set(wsysSystemErrorParams);
		// fatalError600();
		// }
		//
		// if (isNE(prasrec.taxrelamt, 0)) {
		// prasrec.taxrelamt.set(callRounding8000(prasrec.taxrelamt,
		// readerDTO.getBillcurr()));
		// }
		// if (isNE(prasrec.taxrelamt, 0)) {
		// linspf.setTaxrelmth(readerDTO.getTaxrelmth());
		// }
	}

	private String readT6687(L2BillingReaderDTO readerDTO) {

		String keyItemitem = readerDTO.getTaxrelmth().trim();
		T6687 t6687 = itempfService.readSmartTableByTrim(Companies.LIFE, "T6687", keyItemitem, T6687.class);
		if (t6687 != null) {
			return t6687.getTaxrelsub();
		}
		return null;
	}

	private void produceBextRecord(Linspf linspf, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		BigDecimal wsaaBillOutst = BigDecimal.ZERO;
		readT3620(readerDTO);
		/* Convert contract amount to billing amount if contract currency */
		/* is not the same as billing currency. */
		if (readerDTO.getPayrcntcurr().equals(readerDTO.getPayrbillcurr())) {
			l2BillingProcessorDTO.getPrasDTO().setGrossprem(
					l2BillingProcessorDTO.getIncreaseDue().add(l2BillingProcessorDTO.getPayrpf().getOutstamt()));
			calculateTaxRelief(linspf, readerDTO, l2BillingProcessorDTO);
			l2BillingProcessorDTO.getConlinkOutputDTO()
					.setCalculatedAmount(l2BillingProcessorDTO.getPayrpf().getOutstamt()
							.subtract(l2BillingProcessorDTO.getPrasDTO().getTaxrelamt())
							.add(l2BillingProcessorDTO.getIncreaseDue()));
			wsaaBillOutst = l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount()
					.add(l2BillingProcessorDTO.getTax());
		} else {
			if (l2BillingProcessorDTO.getPayrpf().getOutstamt().compareTo(BigDecimal.ZERO) != 0) {
				l2BillingProcessorDTO.getConlinkDTO().setAmount(
						l2BillingProcessorDTO.getPayrpf().getOutstamt().add(l2BillingProcessorDTO.getIncreaseDue()));
				callXcvrt5000(readerDTO, l2BillingProcessorDTO);
				l2BillingProcessorDTO.getPrasDTO()
						.setGrossprem(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
				calculateTaxRelief(linspf, readerDTO, l2BillingProcessorDTO);
				l2BillingProcessorDTO.getConlinkOutputDTO()
						.setCalculatedAmount(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount()
								.subtract(l2BillingProcessorDTO.getPrasDTO().getTaxrelamt()));
				wsaaBillOutst = l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount();
				l2BillingProcessorDTO.getConlinkDTO().setAmount(l2BillingProcessorDTO.getTax());
				callXcvrt5000(readerDTO, l2BillingProcessorDTO);
				wsaaBillOutst = wsaaBillOutst.add(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
			}
		}
		/* Do not produce bext if there is enough money in the suspense */
		/* account to cover the instalment amount. */
		/* Additional checking for dividend suspense <LA2106> */
		/* IF WSAA-SUSP-AVAIL >= CLNK-AMOUNT-OUT */
		if (l2BillingProcessorDTO.getSuspAvail().compareTo(wsaaBillOutst) >= 0) {
			if (BigDecimal.ZERO.compareTo(l2BillingProcessorDTO.getDvdSusp()) == 0) {
				return;
			}
			/* <LA2106> */
			/* Transfer LP DS to LP S <LA2106> */
			h100TfrDvdSusp(readerDTO, l2BillingProcessorDTO);
			return;
		}
		readDishonours(readerDTO, l2BillingProcessorDTO);
		callBillreq(linspf, readerDTO, l2BillingProcessorDTO);
	}

	private void readT3620(L2BillingReaderDTO readerDTO) {
		String keyItemitem = readerDTO.getBillchnl().trim();
		T3620 t3620 = itempfService.readSmartTableByTrim(Companies.LIFE, "T3620", keyItemitem, T3620.class);
		if (t3620 == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table T3620: " + keyItemitem);
		}
	}

	private void h100TfrDvdSusp(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		if (h100Para(readerDTO, l2BillingProcessorDTO)) {
			h120WriteHdiv(readerDTO, l2BillingProcessorDTO);
		}
	}

	private boolean h100Para(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* <LA2106> */
		/* Determine the shortfall from the premium suspense for billing */
		BigDecimal wsaaShortfall = l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount()
				.subtract(l2BillingProcessorDTO.getPremSusp());
		/* <LA2106> */
		/* Shortfall < or = zero, no billing generated <LA2106> */
		if (wsaaShortfall.compareTo(BigDecimal.ZERO) <= 0) {
			return false;
		} else {
			/* <LA2106> */
			/* Shortfall > dividend suspense, transfer from dividend suspense */
			/* Note that with tolerance limit, even the shortfall > dvd-susp, */
			/* there is a chance no billing will be necessary. <LA2106> */
			if (wsaaShortfall.compareTo(l2BillingProcessorDTO.getDvdSusp()) > 0) {
				l2BillingProcessorDTO.setTfrAmt(l2BillingProcessorDTO.getDvdSusp());
			} else {
				/* <LA2106> */
				/*
				 * Dividend suspense > shortfall, transfer enough for dvd-susp
				 */
				/* no billing required. <LA2106> */
				l2BillingProcessorDTO.setTfrAmt(wsaaShortfall);
			}
		}
		/* <LA2106> */
		/* Set up common fields in LIFA once only <LA2106> */
		l2BillingProcessorDTO.setJrnseq(0);
		LifacmvDTO lifeacmvdto = new LifacmvDTO();
		lifeacmvdto.setBatccoy(Companies.LIFE);
		lifeacmvdto.setBatcbrn(l2BillingProcessorDTO.getBranch());
		lifeacmvdto.setBatcactyr(l2BillingProcessorDTO.getActyear());
		lifeacmvdto.setBatcactmn(l2BillingProcessorDTO.getActmonth());
		lifeacmvdto.setBatctrcde(l2BillingProcessorDTO.getBprdAuthCode());
		lifeacmvdto.setBatcbatch(l2BillingProcessorDTO.getBatch());
		lifeacmvdto.setRldgcoy(Companies.LIFE);
		lifeacmvdto.setGenlcoy(Companies.LIFE);
		lifeacmvdto.setPostyear("");
		lifeacmvdto.setPostmonth("");
		lifeacmvdto.setRdocnum(readerDTO.getChdrnum());
		lifeacmvdto.setRldgacct(readerDTO.getChdrnum());
		lifeacmvdto.setTranref(readerDTO.getChdrnum());
		lifeacmvdto.setTranno(l2BillingProcessorDTO.getChdrpf().getTranno());
		lifeacmvdto.setEffdate(l2BillingProcessorDTO.getPayrpf().getBillcd());
		lifeacmvdto.setFrcdate(99999999);
		lifeacmvdto.setCrate(BigDecimal.ZERO);
		lifeacmvdto.setAcctamt(BigDecimal.ZERO);
		lifeacmvdto.setContot(0);
		lifeacmvdto.setRcamt(BigDecimal.ZERO);
		lifeacmvdto.setTermid("");
		lifeacmvdto.setTransactionDate(l2BillingProcessorDTO.getEffDate());
		lifeacmvdto.setTransactionTime(getVrcmTime());
		lifeacmvdto.setUser(0);
		lifeacmvdto.setTrandesc("Dividend Transfer");
		List<String> subCodes = new ArrayList<>();
		subCodes.add(readerDTO.getCnttype());
		lifeacmvdto.setSubstituteCodes(subCodes);
		T5645 t5645IO = l2BillingProcessorDTO.getT5645IO();
		/* Compare contract currency to billing and post accordingly. */
		if (readerDTO.getChdrcntcurr().equals(readerDTO.getBillcurr())) {
			l2BillingProcessorDTO.getConlinkDTO().setFromCurrency(readerDTO.getBillcurr());
			l2BillingProcessorDTO.getConlinkDTO().setToCurrency(readerDTO.getChdrcntcurr());
			l2BillingProcessorDTO.getConlinkDTO().setAmount(l2BillingProcessorDTO.getTfrAmt());
			l2BillingProcessorDTO.getConlinkDTO().setCashdate(99999999);
			l2BillingProcessorDTO.getConlinkDTO().setCompany(readerDTO.getChdrcoy());
			try {
				l2BillingProcessorDTO
						.setConlinkOutputDTO(xcvrt.executeRealFunction(l2BillingProcessorDTO.getConlinkDTO()));
			} catch (IOException e) {
				throw new SubroutineIOException(XCVRT_MSG + e.getMessage());
			}

			if (l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount().compareTo(BigDecimal.ZERO) != 0) {
				l2BillingProcessorDTO.getConlinkOutputDTO().setCalculatedAmount(
						callRounding8000(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount(),
								readerDTO.getChdrcntcurr(), l2BillingProcessorDTO.getBprdAuthCode()));
			}
			/* <LA2106> */
			/* Transfer out Dividend Suspense <LA2106> */
			lifeacmvdto.setOrigamt(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
			lifeacmvdto.setOrigcurr(readerDTO.getChdrcntcurr());

			lifeacmvdto.setSacscode(t5645IO.getSacscodes().get(2));// sacscode03
			lifeacmvdto.setSacstyp(t5645IO.getSacstypes().get(2));
			lifeacmvdto.setGlcode(t5645IO.getGlmaps().get(2));
			lifeacmvdto.setGlsign(t5645IO.getSigns().get(2));
			lifeacmvdto.setContot(t5645IO.getCnttots().get(2));
			h200PostAcmvRecord(lifeacmvdto, l2BillingProcessorDTO);
			/* <LA2106> */
			/* Transfer Dividend into Currency Exchange Account <LA2106> */
			lifeacmvdto.setSacscode(t5645IO.getSacscodes().get(3));
			lifeacmvdto.setSacstyp(t5645IO.getSacstypes().get(3));
			lifeacmvdto.setGlcode(t5645IO.getGlmaps().get(3));
			lifeacmvdto.setGlsign(t5645IO.getSigns().get(3));
			lifeacmvdto.setContot(t5645IO.getCnttots().get(3));
			h200PostAcmvRecord(lifeacmvdto, l2BillingProcessorDTO);
			/* <LA2106> */
			/* Transfer out Dividend Suspense from Currency Exchange Account */
			lifeacmvdto.setOrigamt(l2BillingProcessorDTO.getTfrAmt());
			lifeacmvdto.setOrigcurr(readerDTO.getBillcurr());
			lifeacmvdto.setSacscode(t5645IO.getSacscodes().get(4));// sacscode05
			lifeacmvdto.setSacstyp(t5645IO.getSacstypes().get(4));
			lifeacmvdto.setGlcode(t5645IO.getGlmaps().get(4));
			lifeacmvdto.setGlsign(t5645IO.getSigns().get(4));
			lifeacmvdto.setContot(t5645IO.getCnttots().get(4));
			h200PostAcmvRecord(lifeacmvdto, l2BillingProcessorDTO);
			/* <LA2106> */
			/* Transfer Dividend Suspense into Premium Suspense <LA2106> */
			lifeacmvdto.setSacscode(t5645IO.getSacscodes().get(0));// sacscode01
			lifeacmvdto.setSacstyp(t5645IO.getSacstypes().get(0));
			lifeacmvdto.setGlcode(t5645IO.getGlmaps().get(0));
			lifeacmvdto.setGlsign(t5645IO.getSigns().get(0));
			lifeacmvdto.setContot(t5645IO.getCnttots().get(0));
			h200PostAcmvRecord(lifeacmvdto, l2BillingProcessorDTO);
			/* <LA2106> */
			/* Set WSAA-TFR-AMT to converted amount in contract currency */
			l2BillingProcessorDTO.setTfrAmt(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
		} else {
			/* <LA2106> */
			/* Transfer out Dividend Suspense <LA2106> */
			lifeacmvdto.setOrigamt(l2BillingProcessorDTO.getTfrAmt());
			lifeacmvdto.setOrigcurr(readerDTO.getChdrcntcurr());
			lifeacmvdto.setSacscode(t5645IO.getSacscodes().get(2));// sacscode03
			lifeacmvdto.setSacstyp(t5645IO.getSacstypes().get(2));
			lifeacmvdto.setGlcode(t5645IO.getGlmaps().get(2));
			lifeacmvdto.setGlsign(t5645IO.getSigns().get(2));
			lifeacmvdto.setContot(t5645IO.getCnttots().get(2));
			h200PostAcmvRecord(lifeacmvdto, l2BillingProcessorDTO);
			/* <LA2106> */
			/* Transfer Dividend Suspense into Premium Suspense <LA2106> */
			lifeacmvdto.setSacscode(t5645IO.getSacscodes().get(0));// sacscode01
			lifeacmvdto.setSacstyp(t5645IO.getSacstypes().get(0));
			lifeacmvdto.setGlcode(t5645IO.getGlmaps().get(0));
			lifeacmvdto.setGlsign(t5645IO.getSigns().get(0));
			lifeacmvdto.setContot(t5645IO.getCnttots().get(0));
			h200PostAcmvRecord(lifeacmvdto, l2BillingProcessorDTO);
		}

		return true;
	}

	private void h120WriteHdiv(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* <LA2106> */
		/* Now write HDIV to denote the withdrawal of dividend at coverage */
		/* level. So spread the amount across all the coverages according */
		/* to their share. <LA2106> */
		String chdrnum = readerDTO.getChdrnum();
		int intCtr = 0;
		List<Hdispf> hdispfList = hdispfDAO.readHdispfRecords(Companies.LIFE, chdrnum);
		if (hdispfList != null && !hdispfList.isEmpty()) {
			for (Hdispf hdis : hdispfList) {
				if (hdis.getPlnsfx() == 0) {
					processHdis(hdis, intCtr++, readerDTO, l2BillingProcessorDTO);
					break;
				}
			}
		}
		l2BillingProcessorDTO.setAllDvdTot(BigDecimal.ZERO);
		/* <LA2106> */
		/* Set total no. of HDIS read <LA2106> */
		int wsaaNoOfHdis = intCtr;
		/* <LA2106> */
		/* Then calculate share of each coverage on the dividend <LA2106> */
		int wsaaIdx = 0;
		HdisArrayInner wsaaHdisArrayInner = l2BillingProcessorDTO.getHdisArrayInner();
		while (wsaaIdx < wsaaNoOfHdis) {
			wsaaIdx++;
			if (wsaaIdx == wsaaNoOfHdis) {
				wsaaHdisArrayInner.getDvdShare().set(wsaaIdx,
						l2BillingProcessorDTO.getTfrAmt().subtract(l2BillingProcessorDTO.getRunDvdTot()));
			} else {
				wsaaHdisArrayInner.getDvdShare().set(wsaaIdx, l2BillingProcessorDTO.getTfrAmt().multiply(
						wsaaHdisArrayInner.getDvdTot().get(wsaaIdx).divide(l2BillingProcessorDTO.getAllDvdTot())));
				l2BillingProcessorDTO.setRunDvdTot(
						l2BillingProcessorDTO.getRunDvdTot().add(wsaaHdisArrayInner.getDvdShare().get(wsaaIdx)));
			}
			if (wsaaHdisArrayInner.getDvdShare().get(wsaaIdx).compareTo(BigDecimal.ZERO) != 0) {
				wsaaHdisArrayInner.getDvdShare().set(wsaaIdx,
						callRounding8000(wsaaHdisArrayInner.getDvdShare().get(wsaaIdx), readerDTO.getChdrcntcurr(),
								l2BillingProcessorDTO.getBatchTrcde()));
			}
		}

		/* <LA2106> */
		/* Write withdrawn amount for each coverage <LA2106> */
		wsaaIdx = 0;
		while (wsaaIdx < wsaaNoOfHdis) {
			wsaaIdx++;
			Hdivpf hdivpf = new Hdivpf();
			hdivpf.setChdrcoy(readerDTO.getChdrcoy());
			hdivpf.setChdrnum(readerDTO.getChdrnum());
			hdivpf.setLife(wsaaHdisArrayInner.getHdisLife().get(wsaaIdx));
			hdivpf.setJlife(wsaaHdisArrayInner.getHdisJlife().get(wsaaIdx));
			hdivpf.setCoverage(wsaaHdisArrayInner.getHdisCoverage().get(wsaaIdx));
			hdivpf.setRider(wsaaHdisArrayInner.getHdisRider().get(wsaaIdx));
			hdivpf.setPlnsfx(wsaaHdisArrayInner.getHdisPlnsfx().get(wsaaIdx));
			hdivpf.setTranno(l2BillingProcessorDTO.getChdrpf().getTranno());
			/* MOVE PAYR-BILLCD TO HDIV-EFFDATE <LFA1034> */
			/* HDIV-DIVD-ALLOC-DATE <LFA1034> */
			hdivpf.setEffdate(readerDTO.getPtdate());
			hdivpf.setHdvaldt(readerDTO.getPtdate());
			hdivpf.setHincapdt(wsaaHdisArrayInner.getNextCapDate().get(wsaaIdx));
			hdivpf.setCntcurr(readerDTO.getChdrcntcurr());
			hdivpf.setHdvamt(wsaaHdisArrayInner.getDvdShare().get(wsaaIdx).multiply(BigDecimal.valueOf(-1)));
			hdivpf.setHdvrate(BigDecimal.ZERO);
			hdivpf.setHdveffdt(99999999);
			hdivpf.setBatccoy(Companies.LIFE);
			hdivpf.setBatcbrn(l2BillingProcessorDTO.getBranch());
			hdivpf.setBatcactyr(l2BillingProcessorDTO.getActyear());
			hdivpf.setBatcactmn(l2BillingProcessorDTO.getActmonth());
			hdivpf.setBatctrcde(l2BillingProcessorDTO.getBprdAuthCode());
			hdivpf.setBatcbatch(l2BillingProcessorDTO.getBatch());
			hdivpf.setHdvtyp("C");
			hdivpf.setZdivopt(wsaaHdisArrayInner.getZdivopt().get(wsaaIdx));
			hdivpf.setZcshdivmth(wsaaHdisArrayInner.getZcshdivmth().get(wsaaIdx));
			hdivpf.setHdvopttx(0);
			hdivpf.setHdvcaptx(0);
			hdivpf.setHdvsmtno(0);
			hdivpf.setHpuanbr(0);
			hdivpf.setUsrprf(l2BillingProcessorDTO.getUserName());
			hdivpf.setJobnm(l2BillingProcessorDTO.getBatchName());
			l2BillingProcessorDTO.getHdivpfList().add(hdivpf);
		}
	}

	private void h200PostAcmvRecord(LifacmvDTO lifeacmvdto, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* H210-POST */
		if (lifeacmvdto.getOrigamt().compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		l2BillingProcessorDTO.setJrnseq(l2BillingProcessorDTO.getJrnseq() + 1);
		lifeacmvdto.setJrnseq(l2BillingProcessorDTO.getJrnseq());
		try {
			lifacmv.executePSTW(lifeacmvdto);
		} catch (IOException | ParseException e) {
			throw new ItemParseException("L2billing: Parsed failed in lifacmv:" + e.getMessage());
		}
	}

	private void processHdis(Hdispf hdis, int ctr, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		HdisArrayInner wsaaHdisArrayInner = l2BillingProcessorDTO.getHdisArrayInner();
		wsaaHdisArrayInner.getHdisLife().set(ctr, hdis.getLife());
		wsaaHdisArrayInner.getHdisCoverage().set(ctr, hdis.getCoverage());
		wsaaHdisArrayInner.getHdisRider().set(ctr, hdis.getRider());
		wsaaHdisArrayInner.getHdisPlnsfx().set(ctr, hdis.getPlnsfx());
		wsaaHdisArrayInner.getHdisJlife().set(ctr, hdis.getJlife());
		wsaaHdisArrayInner.getNextCapDate().set(ctr, hdis.getHcapndt());

		Hcsdpf currHcsd = null;
		String chdrnum = readerDTO.getChdrnum();
		List<Hcsdpf> hcsdpfList = hcsdpfDAO.readHcsdpfRecords(Companies.LIFE, chdrnum);

		if (hcsdpfList != null && !hcsdpfList.isEmpty()) {

			for (Hcsdpf hcsd : hcsdpfList) {
				if (hcsd.getLife().equals(hdis.getLife()) && hcsd.getCoverage().equals(hdis.getCoverage())
						&& hcsd.getRider().equals(hdis.getRider()) && hcsd.getPlnsfx().equals(hdis.getPlnsfx())) {
					currHcsd = hcsd;
					break;

				}
			}
		}
		wsaaHdisArrayInner.getZdivopt().set(ctr, currHcsd.getZdivopt());
		wsaaHdisArrayInner.getZcshdivmth().set(ctr, currHcsd.getZcshdivmth());
		wsaaHdisArrayInner.getDvdTot().set(ctr, hdis.getHdvbalst().add(wsaaHdisArrayInner.getDvdTot().get(ctr)));
		l2BillingProcessorDTO.setAllDvdTot(l2BillingProcessorDTO.getAllDvdTot().add(hdis.getHdvbalst()));

		List<Hdivpf> hdivpfList = hdivpfDAO.readHdivpfRecords(Companies.LIFE, chdrnum);
		if (hdivpfList != null && !hdivpfList.isEmpty()) {
			for (Hdivpf hdiv : hdivpfList) {
				if (hdiv.getLife().equals(hdis.getLife()) && hdiv.getCoverage().equals(hdis.getCoverage())
						&& hdiv.getRider().equals(hdis.getRider()) && hdiv.getPlnsfx().equals(hdis.getPlnsfx())
						&& hdiv.getHincapdt() == (hdis.getHcapldt() + 1)
						|| hdiv.getHincapdt() < (hdis.getHcapldt() + 1)) {
					wsaaHdisArrayInner.getDvdTot().set(ctr, hdiv.getHdvamt().add(wsaaHdisArrayInner.getDvdTot().get(ctr)));
				}
			}
		}
	}

	private void readDishonours(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Check if a previously dishonoured payment exists by reading */
		/* DDSURNL with a BEGN. If there is a key break move the */
		/* original MANDSTAT to BEXT-MANDSTAT, move the later of the */
		/* BSSC-EFFECTIVE-DATE or BILLCD to BILLDATE, else move DDSURNL- */
		/* MANDSTAT to BEXT-MANDSTAT and increment the BSSC-EFFECTIVE- */
		/* DATE by DDSURNL-LAPDAY to give BEXT-BILLDATE. */
		Ddsupf currDdsu = null;
		String clntnum = readerDTO.getClntnum();
		boolean isFound = false;
		List<Ddsupf> ddsupfList = ddsupfDAO.readDdsupfRecords(Companies.LIFE, clntnum);
		if (ddsupfList != null && !ddsupfList.isEmpty()) {
			for (Ddsupf ddsu : ddsupfList) {
				if (ddsu.getMandref().equals(readerDTO.getMandref())
						&& ddsu.getBillcd().equals(l2BillingProcessorDTO.getOldBillcd())
						&& ddsu.getMandstat().equals("99")) {
					currDdsu = ddsu;
					isFound = true;
					break;
				}
			}
		}
		BillreqDTO billreqDTO = l2BillingProcessorDTO.getBillreqDTO();
		if (!isFound) {
			billreqDTO.setMandstat(readerDTO.getMandstat());
			if (l2BillingProcessorDTO.getEffDate() > l2BillingProcessorDTO.getOldBillcd()) {
				billreqDTO.setBilldate(l2BillingProcessorDTO.getEffDate());
			} else {
				billreqDTO.setBilldate(l2BillingProcessorDTO.getOldBillcd());
			}
		} else {
			billreqDTO.setMandstat(currDdsu.getMandstat());
			billreqDTO.setBilldate(datcon2.plusDays(l2BillingProcessorDTO.getEffDate(), currDdsu.getLapday()));
		}
	}

	private void callBillreq(Linspf linspf, L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Search the T3629 array to obtain the required bank code. */
		String strT3629BankCode = readT3629(readerDTO);
		BillreqDTO billreqDTO = l2BillingProcessorDTO.getBillreqDTO();
		billreqDTO.setBankcode(strT3629BankCode);
		billreqDTO.setUser(0);
		billreqDTO.setContot01(0);
		billreqDTO.setContot02(0);
		billreqDTO.setInstjctl("");
		billreqDTO.setPayflag("");
		billreqDTO.setBilflag("");
		billreqDTO.setOutflag("");
		billreqDTO.setSupflag("N");
		billreqDTO.setCompany(Companies.LIFE);
		billreqDTO.setBranch(l2BillingProcessorDTO.getBranch());
		billreqDTO.setLanguage(l2BillingProcessorDTO.getLang());
		billreqDTO.setEffdate(l2BillingProcessorDTO.getEffDate());
		billreqDTO.setAcctyear(l2BillingProcessorDTO.getActyear());
		billreqDTO.setAcctmonth(l2BillingProcessorDTO.getActmonth());
		billreqDTO.setTrancode(l2BillingProcessorDTO.getBprdAuthCode());
		billreqDTO.setBatch(l2BillingProcessorDTO.getBatch());
		billreqDTO.setFsuco(Companies.FSU);
		billreqDTO.setModeInd("BATCH");
		billreqDTO.setTermid("");
		billreqDTO.setTime(0);
		billreqDTO.setDate_var(0);
		billreqDTO.setTranno(l2BillingProcessorDTO.getChdrpf().getTranno());
		billreqDTO.setChdrpfx(readerDTO.getChdrpfx());
		billreqDTO.setChdrcoy(readerDTO.getChdrcoy());
		billreqDTO.setChdrnum(readerDTO.getChdrnum());
		billreqDTO.setServunit(readerDTO.getServunit());
		billreqDTO.setCnttype(readerDTO.getCnttype());
		billreqDTO.setOccdate(readerDTO.getOccdate());
		billreqDTO.setCcdate(readerDTO.getCcdate());
		billreqDTO.setInstcchnl(readerDTO.getCollchnl());
		billreqDTO.setCownpfx(readerDTO.getCownpfx());
		billreqDTO.setCowncoy(readerDTO.getCowncoy());
		billreqDTO.setCownnum(readerDTO.getCownnum());
		billreqDTO.setCntbranch(readerDTO.getCntbranch());
		billreqDTO.setAgntpfx(readerDTO.getAgntpfx());
		billreqDTO.setAgntcoy(readerDTO.getAgntcoy());
		billreqDTO.setAgntnum(readerDTO.getAgntnum());
		billreqDTO.setCntcurr(readerDTO.getPayrcntcurr());
		billreqDTO.setBillcurr(readerDTO.getPayrbillcurr());
		billreqDTO.setPtdate(readerDTO.getPtdate());
		billreqDTO.setInstto(readerDTO.getBtdate());
		billreqDTO.setInstbchnl(readerDTO.getChdrbillchnl());
		billreqDTO.setBillchnl(readerDTO.getChdrbillchnl());
		billreqDTO.setInstfreq(readerDTO.getBillfreq());
		billreqDTO.setGrpscoy(readerDTO.getGrupcoy());
		billreqDTO.setGrpsnum(readerDTO.getGrupnum());
		billreqDTO.setMembsel(readerDTO.getMembsel());
		billreqDTO.setMandref(readerDTO.getMandref());
		billreqDTO.setNextdate(readerDTO.getNextdate());
		billreqDTO.setPayrpfx(readerDTO.getClntpfx());
		billreqDTO.setPayrcoy(readerDTO.getClntcoy());
		billreqDTO.setPayrnum(readerDTO.getClntnum());
		billreqDTO.setFacthous(readerDTO.getFacthous());
		billreqDTO.setBankkey(readerDTO.getBankkey());
		billreqDTO.setBankacckey(readerDTO.getBankacckey());
		billreqDTO.setInstfrom(l2BillingProcessorDTO.getOldBtdate());
		billreqDTO.setBtdate(l2BillingProcessorDTO.getOldBtdate());
		billreqDTO.setDuedate(l2BillingProcessorDTO.getOldBtdate());
		billreqDTO.setBillcd(l2BillingProcessorDTO.getOldBillcd());
		billreqDTO.setNextdate(l2BillingProcessorDTO.getOldNextdate());
		T5645 t5645IO = l2BillingProcessorDTO.getT5645IO();
		billreqDTO.setSacscode01(t5645IO.getSacscodes().get(0));
		billreqDTO.setSacstype01(t5645IO.getSacstypes().get(0));
		billreqDTO.setGlmap01(t5645IO.getGlmaps().get(0));
		billreqDTO.setGlsign01(t5645IO.getSigns().get(0));
		billreqDTO.setSacscode02(t5645IO.getSacscodes().get(1));
		billreqDTO.setSacstype02(t5645IO.getSacstypes().get(1));
		billreqDTO.setGlmap02(t5645IO.getGlmaps().get(1));
		billreqDTO.setGlsign02(t5645IO.getSigns().get(1));
		/* If BILLREQ1 needs to know the PAYER NAME then it will be looked */
		/* up using the PAYRPFX */
		billreqDTO.setPayername("");
		if (!readerDTO.getPayrcntcurr().equals(readerDTO.getPayrbillcurr())) {
			currNotEqualBillcurr(linspf, readerDTO, l2BillingProcessorDTO);
		} else {
			if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
				gotPayrAtBtdate(linspf, readerDTO, l2BillingProcessorDTO);
			} else {
				payr(linspf, readerDTO, l2BillingProcessorDTO);
			}
		}
		try {
			billreqDTO = billreq1.processBillreq1(billreqDTO);
		} catch (Exception e) {
			throw new SubroutineIOException("L2billing: Exception in billreq1: " + e.getMessage());
		}
		/* Log number of BEXT recs created. */
		BigDecimal billreq = BigDecimal.valueOf(billreqDTO.getContot01());
		L2BillingControlTotalDTO l2BillingControlTotalDTO = l2BillingProcessorDTO.getL2BillingControlTotalDTO();
		l2BillingControlTotalDTO.setControlTotal09(billreq.add(l2BillingControlTotalDTO.getControlTotal09()));
		/* Log total amount for this BEXT rec. */
		l2BillingControlTotalDTO.setControlTotal10(
				BigDecimal.valueOf(billreqDTO.getContot02()).add(l2BillingControlTotalDTO.getControlTotal10()));
	}

	private String readT3629(L2BillingReaderDTO readerDTO) {
		String keyItemitem = readerDTO.getPayrbillcurr().trim();

		T3629 t3629 = itempfService.readSmartTableByTrim(Companies.LIFE, "T3629", keyItemitem, T3629.class);
		if (t3629 == null) {
			throw new ItemNotfoundException("L2billing: Item not found in Table T3629: " + keyItemitem);
		}
		return t3629.getBankcode();
	}

	private void currNotEqualBillcurr(Linspf linspf, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			for (int wsaaInstSub = 1; wsaaInstSub <= 6; wsaaInstSub++) {
				convertOldInstamts3736(readerDTO, wsaaInstSub, l2BillingProcessorDTO);
			}
		} else {
			for (int wsaaInstSub = 1; wsaaInstSub <= 6; wsaaInstSub++) {
				convertInstamts(readerDTO, wsaaInstSub, l2BillingProcessorDTO);
			}
		}
		BillreqDTO billreqDTO = l2BillingProcessorDTO.getBillreqDTO();
		PrasDTO prasDTO = l2BillingProcessorDTO.getPrasDTO();
		prasDTO.setGrossprem(billreqDTO.getInstamts().get(5));// instamt06);
		/* ADD WSAA-INCREASE-DUE TO PRAS-GROSSPREM. */
		l2BillingProcessorDTO.getConlinkDTO().setAmount(l2BillingProcessorDTO.getTax());
		callXcvrt(readerDTO, l2BillingProcessorDTO);
		prasDTO.setGrossprem(
				prasDTO.getGrossprem().subtract(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount()));
		calculateTaxRelief(linspf, readerDTO, l2BillingProcessorDTO);
		billreqDTO.getInstamts().set(5, billreqDTO.getInstamts().get(5).subtract(prasDTO.getTaxrelamt()));
		/* PRAS-TAXRELAMT + */
		/* WSAA-INCREASE-DUE. */
		billreqDTO.getInstamts().set(6, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(7, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(8, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(9, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(10, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(11, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(12, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(13, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(14, BigDecimal.ZERO);
	}

	private void convertOldInstamts3736(L2BillingReaderDTO readerDTO, int wsaaInstSub,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* START */
		Class aClass = Payrpf.class;
		Field field;
		Object value = null;
		Payrpf objectInstance = new Payrpf();
		try {
			field = aClass.getField("sinsamt".concat("" + wsaaInstSub));
			value = field.get(objectInstance);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
			throw new StopRunException("L2billing: Exception in convertOldInstamts3736: " + e.getMessage());
		}

		if (value != null && value.equals(0)) {// IJTI-320
			l2BillingProcessorDTO.getBillreqDTO().getInstamts().set(wsaaInstSub, BigDecimal.ZERO);
		}

		/* The increase is only added the first time. */
		ConlinkInputDTO conlinkDTO = l2BillingProcessorDTO.getConlinkDTO();
		if (wsaaInstSub == 1) {
			conlinkDTO.setAmount(l2BillingProcessorDTO.getIncreaseDue().add(conlinkDTO.getAmount()));
		}
		if (wsaaInstSub == 6) {
			conlinkDTO.setAmount(l2BillingProcessorDTO.getTax().add(l2BillingProcessorDTO.getIncreaseDue())
					.add(conlinkDTO.getAmount()));
		}
		callXcvrt5000(readerDTO, l2BillingProcessorDTO);
		l2BillingProcessorDTO.getBillreqDTO().getInstamts().set(wsaaInstSub,
				l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());
		/* EXIT */
	}

	private void callXcvrt(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		ConlinkInputDTO conlinkDTO = l2BillingProcessorDTO.getConlinkDTO();
		conlinkDTO.setFromCurrency(readerDTO.getPayrcntcurr());
		conlinkDTO.setToCurrency(readerDTO.getPayrbillcurr());
		conlinkDTO.setCashdate(l2BillingProcessorDTO.getCashdate());
		conlinkDTO.setCompany(Companies.LIFE);
		try {
			l2BillingProcessorDTO.setConlinkOutputDTO(xcvrt.executeSurrFunction(conlinkDTO));
		} catch (IOException e) {
			throw new SubroutineIOException(XCVRT_MSG + e.getMessage());
		}
		/* MOVE BSSC-EFFECTIVE-DATE TO CLNK-CASHDATE. */
		if (l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount().compareTo(BigDecimal.ZERO) != 0) {
			l2BillingProcessorDTO.getConlinkOutputDTO().setCalculatedAmount(
					callRounding8000(l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount(),
							readerDTO.getPayrbillcurr(), l2BillingProcessorDTO.getBprdAuthCode()));
		}
	}

	private void convertInstamts(L2BillingReaderDTO readerDTO, int ctr, L2BillingProcessorDTO l2BillingProcessorDTO) {
		if (readerDTO.getSinstamt(ctr).compareTo(BigDecimal.ZERO) == 0) {
			l2BillingProcessorDTO.getBillreqDTO().getInstamts().set(ctr, BigDecimal.ZERO);
		}
		l2BillingProcessorDTO.getConlinkDTO().setAmount(readerDTO.getSinstamt(ctr));
		/* The increase is only added the first time. */
		if (ctr == 1) {
			l2BillingProcessorDTO.getConlinkDTO().setAmount(
					l2BillingProcessorDTO.getIncreaseDue().add(l2BillingProcessorDTO.getConlinkDTO().getAmount()));
		}

		if (ctr == 6) {
			l2BillingProcessorDTO.getConlinkDTO()
					.setAmount(l2BillingProcessorDTO.getTax().add(l2BillingProcessorDTO.getIncreaseDue())
							.add(l2BillingProcessorDTO.getConlinkDTO().getAmount()));
		}
		callXcvrt(readerDTO, l2BillingProcessorDTO);
		l2BillingProcessorDTO.getBillreqDTO().getInstamts().set(ctr,
				l2BillingProcessorDTO.getConlinkOutputDTO().getCalculatedAmount());

		/* EXIT */
	}

	private void gotPayrAtBtdate(Linspf linspf, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {
		BillreqDTO billreqDTO = l2BillingProcessorDTO.getBillreqDTO();
		Payrpf currPayrLif = l2BillingProcessorDTO.getCurrPayrLif();
		billreqDTO.getInstamts().set(0, currPayrLif.getSinstamt01());
		billreqDTO.getInstamts().set(0, billreqDTO.getInstamts().get(0).add(l2BillingProcessorDTO.getIncreaseDue()));
		billreqDTO.getInstamts().set(1, currPayrLif.getSinstamt02());
		billreqDTO.getInstamts().set(2, currPayrLif.getSinstamt03());
		billreqDTO.getInstamts().set(3, currPayrLif.getSinstamt04());
		billreqDTO.getInstamts().set(4, currPayrLif.getSinstamt05());
		l2BillingProcessorDTO.getPrasDTO()
				.setGrossprem(l2BillingProcessorDTO.getIncreaseDue().add(currPayrLif.getSinstamt06()));
		calculateTaxRelief(linspf, readerDTO, l2BillingProcessorDTO);
		billreqDTO.getInstamts().set(5,
				currPayrLif.getSinstamt06().subtract(l2BillingProcessorDTO.getPrasDTO().getTaxrelamt())
						.add(l2BillingProcessorDTO.getIncreaseDue()));
		billreqDTO.getInstamts().set(5, l2BillingProcessorDTO.getTax().add(billreqDTO.getInstamts().get(5)));
		billreqDTO.getInstamts().set(6, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(7, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(8, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(9, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(10, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(11, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(12, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(13, BigDecimal.ZERO);
		billreqDTO.getInstamts().set(14, BigDecimal.ZERO);

	}

	private void payr(Linspf linspf, L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		BillreqDTO billreqDTO = l2BillingProcessorDTO.getBillreqDTO();
		billreqDTO.getInstamts().add(0, readerDTO.getSinstamt01());
		billreqDTO.getInstamts().add(0, billreqDTO.getInstamts().get(0).add(l2BillingProcessorDTO.getIncreaseDue()));
		billreqDTO.getInstamts().add(1, readerDTO.getSinstamt02());
		billreqDTO.getInstamts().add(2, readerDTO.getSinstamt03());
		billreqDTO.getInstamts().add(3, readerDTO.getSinstamt04());
		billreqDTO.getInstamts().add(4, readerDTO.getSinstamt05());
		l2BillingProcessorDTO.getPrasDTO()
				.setGrossprem(l2BillingProcessorDTO.getIncreaseDue().add(readerDTO.getSinstamt06()));
		calculateTaxRelief(linspf, readerDTO, l2BillingProcessorDTO);
		billreqDTO.getInstamts().add(5,
				readerDTO.getSinstamt06().subtract(l2BillingProcessorDTO.getPrasDTO().getTaxrelamt())
						.add(l2BillingProcessorDTO.getIncreaseDue()));
		billreqDTO.getInstamts().add(5, l2BillingProcessorDTO.getTax().add(billreqDTO.getInstamts().get(5)));
		billreqDTO.getInstamts().add(6, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(7, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(8, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(9, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(10, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(11, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(12, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(13, BigDecimal.ZERO);
		billreqDTO.getInstamts().add(14, BigDecimal.ZERO);
	}

	private void writePtrnRecord(Linspf linspf, L2BillingReaderDTO readerDTO,
			L2BillingProcessorDTO l2BillingProcessorDTO) {

		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setChdrcoy(readerDTO.getChdrcoy());
		ptrnpf.setChdrpfx(readerDTO.getChdrpfx());
		ptrnpf.setChdrnum(readerDTO.getChdrnum());
		ptrnpf.setTranno(l2BillingProcessorDTO.getChdrpf().getTranno());
		ptrnpf.setTrdt(Integer.parseInt(getCobolDate()));
		ptrnpf.setTrtm(getVrcmTime());
		ptrnpf.setValidflag("1");
		if (l2BillingProcessorDTO.isFlexiblePremiumContract()) {
			ptrnpf.setPtrneff(l2BillingProcessorDTO.getOldBtdate());
		} else {
			ptrnpf.setPtrneff(linspf.getInstfrom());
		}
		ptrnpf.setUserT(0);
		ptrnpf.setBatccoy(Companies.LIFE);
		ptrnpf.setBatcpfx(l2BillingProcessorDTO.getPrefix());
		ptrnpf.setBatcbrn(l2BillingProcessorDTO.getBranch());
		ptrnpf.setBatcactyr(l2BillingProcessorDTO.getActyear());
		ptrnpf.setBatctrcde(l2BillingProcessorDTO.getBprdAuthCode());
		ptrnpf.setBatcactmn(l2BillingProcessorDTO.getActmonth());
		ptrnpf.setBatcbatch(l2BillingProcessorDTO.getBatch());
		ptrnpf.setDatesub(l2BillingProcessorDTO.getEffDate());
		ptrnpf.setUsrprf(l2BillingProcessorDTO.getUserName());
		ptrnpf.setJobnm(l2BillingProcessorDTO.getBatchName());
		l2BillingProcessorDTO.getPtrnpfList().add(ptrnpf);
		// ILIF-3997-STARTS
		String wsaaItem = l2BillingProcessorDTO.getPayrpf().getBillchnl().trim();
		String wsaaItem1 = wsaaItem.concat(l2BillingProcessorDTO.getChdrpf().getCnttype().trim());
		if ((isUpdateNlgt(wsaaItem1, l2BillingProcessorDTO))
				&& (l2BillingProcessorDTO.getChdrpf() != null && l2BillingProcessorDTO.getChdrpf().getNlgflg() != null
						&& "Y".equals(l2BillingProcessorDTO.getChdrpf().getNlgflg()))) {
			writeNLGRec(ptrnpf, l2BillingProcessorDTO);
		}
		l2BillingProcessorDTO.getL2BillingControlTotalDTO().setControlTotal03(
				BigDecimal.ONE.add(l2BillingProcessorDTO.getL2BillingControlTotalDTO().getControlTotal03()));
	}

	private boolean isUpdateNlgt(String item, L2BillingProcessorDTO l2BillingProcessorDTO) {
		List<Itempf> t6654List = itempfService.readSmartTableList(Companies.LIFE, "T6654", item);
		if (t6654List != null && !t6654List.isEmpty()) {
			Iterator<Itempf> iterator = t6654List.iterator();
			try {
				while (iterator.hasNext()) {
					Itempf itempf = iterator.next();
					if (itempf.getItmfrm() > 0) {
						if (l2BillingProcessorDTO.getEffDate() >= itempf.getItmfrm()
								&& l2BillingProcessorDTO.getEffDate() <= itempf.getItmto()) {
							T6654 t6654IO = new ObjectMapper().readValue(itempf.getGenareaj(), T6654.class);
							if (t6654IO.getDaexpys().get(0) == 0 && t6654IO.getDaexpys().get(1) == 0
									&& t6654IO.getDaexpys().get(2) == 0) {
								l2BillingProcessorDTO.setT6654IO(t6654IO);
								return true;
							}
						}
					} else {
						T6654 t6654IO = new ObjectMapper().readValue(itempf.getGenareaj(), T6654.class);
						if (t6654IO.getDaexpys().get(0) == 0 && t6654IO.getDaexpys().get(1) == 0
								&& t6654IO.getDaexpys().get(2) == 0) {
							l2BillingProcessorDTO.setT6654IO(t6654IO);
							return true;
						}
					}
				}
			} catch (IOException e) {
				throw new ItemParseException("L2billing: Item parsed failed in Table T6654: " + item);
			}
		}
		return false;
	}

	private void writeNLGRec(Ptrnpf ptrnIO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		Chdrpf chdrpf = l2BillingProcessorDTO.getChdrpf();
		Payrpf payrpf = l2BillingProcessorDTO.getPayrpf();
		NlgcalcDTO nlgcalcrec = new NlgcalcDTO();
		nlgcalcrec.setChdrcoy(ptrnIO.getChdrcoy());
		nlgcalcrec.setChdrnum(ptrnIO.getChdrnum());
		nlgcalcrec.setTranno(ptrnIO.getTranno());
		nlgcalcrec.setEffdate(ptrnIO.getPtrneff());
		nlgcalcrec.setBatcactyr(ptrnIO.getBatcactyr());
		nlgcalcrec.setBatcactmn(ptrnIO.getBatcactmn());
		nlgcalcrec.setBatctrcde(ptrnIO.getBatctrcde());
		nlgcalcrec.setCnttype(chdrpf.getCnttype());
		nlgcalcrec.setLanguage(l2BillingProcessorDTO.getLang());
		nlgcalcrec.setFrmdate(chdrpf.getOccdate());
		nlgcalcrec.setOccdate(chdrpf.getOccdate());
		nlgcalcrec.setTodate(99999999);
		nlgcalcrec.setPtdate(chdrpf.getPtdate());
		if (payrpf.getBtdate() < payrpf.getEffdate()) {
			nlgcalcrec.setInputAmt(l2BillingProcessorDTO.getCurrPayrLif().getSinstamt01());
		} else {
			nlgcalcrec.setInputAmt(payrpf.getSinstamt01());
		}
		nlgcalcrec.setInputAmt(
				l2BillingProcessorDTO.getIncreaseDue().add(nlgcalcrec.getInputAmt()).multiply(new BigDecimal(-1)));
		NlgcalcDTO resultNlgcalcDTO = null;
		try {
			resultNlgcalcDTO = nlgcalc.processNlgcalOvdue(nlgcalcrec);
		} catch (IOException | ParseException e) {
			throw new SubroutineIOException("L2billing: IOException in rlpdlon: " + e.getMessage());
		}
		if (!STATUS_OK.equals(resultNlgcalcDTO.getStatus())) {
			fatalError(resultNlgcalcDTO.getStatus());
		}
	}

	private void rewriteChdr(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Restore outstanding amount if this is a flexible premium<D9604> */
		Chdrpf chdrpf = l2BillingProcessorDTO.getChdrpf();
		if (l2BillingProcessorDTO.isFlexiblePremiumContract()) {
			chdrpf.setOutstamt(l2BillingProcessorDTO.getOldOutstamt());
		}
		/* Rewrite contract header */
		chdrpf.setUniqueNumber(readerDTO.getChdruniqueno());
		chdrpf.setChdrcoy(readerDTO.getChdrcoy());
		chdrpf.setChdrnum(readerDTO.getChdrnum());
		chdrpf.setUsrprf(l2BillingProcessorDTO.getUserName());
		chdrpf.setJobnm(l2BillingProcessorDTO.getBatchName());
		readerDTO.setChdrpf(chdrpf);
	}

	private void rewritePayr(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		/* Restore original outstanding amount <D9604> */
		Payrpf payrpf = l2BillingProcessorDTO.getPayrpf();
		if (l2BillingProcessorDTO.isFlexiblePremiumContract()) {
			payrpf.setOutstamt(l2BillingProcessorDTO.getOldOutstamt());
		}
		/* Rewrite the PAYR record */
		payrpf.setChdrcoy(readerDTO.getChdrcoy());
		payrpf.setChdrnum(readerDTO.getChdrnum());
		payrpf.setPayrseqno(readerDTO.getPayrseqno());
		payrpf.setUsrprf(l2BillingProcessorDTO.getUserName());
		payrpf.setJobnm(l2BillingProcessorDTO.getBatchName());
		if ("Y".equals(l2BillingProcessorDTO.getGotPayrAtBtdate())) {
			payrpf.setSinstamt06(l2BillingProcessorDTO.getCurrPayrLif().getSinstamt06());
		} else {
			payrpf.setSinstamt06(readerDTO.getSinstamt06());
		}
		readerDTO.setPayrpf(payrpf);
	}

	private void writeLetter(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		String letterType = readTr384(readerDTO, l2BillingProcessorDTO);
		if (letterType == null || letterType.isEmpty()) {
			return;
		}
		// /* Get set-up parameter for call 'HLETRQS' */
		// letrqstrec.statuz.set("");
		// letrqstrec.requestCompany.set(readerDTO.getChdrcoy());
		// /* MOVE T6634-LETTER-TYPE TO LETRQST-LETTER-TYPE. <PCPPRT> */
		// letrqstrec.letterType.set(tr384rec.letterType);
		// letrqstrec.letterRequestDate.set(effDate);
		// letrqstrec.rdocpfx.set(readerDTO.getChdrpfx());
		// letrqstrec.rdoccoy.set(readerDTO.getChdrcoy());
		// letrqstrec.rdocnum.set(readerDTO.getChdrnum());
		// letrqstrec.otherKeys.set(lang);
		// letrqstrec.clntcoy.set(readerDTO.getCowncoy());
		// letrqstrec.clntnum.set(readerDTO.getCownnum());
		// letrqstrec.chdrcoy.set(readerDTO.getChdrcoy());
		// letrqstrec.chdrnum.set(readerDTO.getChdrnum());
		// letrqstrec.tranno.set(readerDTO.getChdrtranno() + 1);
		// letrqstrec.branch.set(readerDTO.getCntbranch());
		// letrqstrec.function.set("ADD");
		// /* CALL 'HLETRQS' USING LETRQST-PARAMS. <PCPPRT> */
		// callProgram(Letrqst.class, letrqstrec.params);
		// if (isNE(letrqstrec.statuz, varcom.oK)) {
		// syserrrec.params.set(letrqstrec.params);
		// syserrrec.statuz.set(letrqstrec.statuz);
		// fatalError600();
		// }
	}

	private String readTr384(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO) {
		String keyItemitem = readerDTO.getCnttype().trim().concat(l2BillingProcessorDTO.getBatchTrcde());
		String letterType = readTr384Record(keyItemitem);
		if (letterType == null || letterType.isEmpty()) {
			keyItemitem = "***".concat(l2BillingProcessorDTO.getBatchTrcde());
			letterType = readTr384Record(keyItemitem);

			if (letterType == null || letterType.isEmpty()) {
				throw new ItemNotfoundException("L2billing: Item not found in Table Tr384: " + keyItemitem);
			}
		}
		return letterType;
	}

	private String readTr384Record(String keyItemitem) {
		Tr384 tr384IO = itempfService.readSmartTableByTrim(Companies.LIFE, "TR384", keyItemitem, Tr384.class);
		if (tr384IO != null) {
			return tr384IO.getLetterType();
		}
		return null;
	}

	public int getVrcmTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		return Integer.parseInt(formatter.format(LocalTime.now()));
	}

	public static String getCobolDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		return sdf.format(new java.util.Date());
	}

	public void fatalError(String status) {
		throw new StopRunException(status);
	}
}
