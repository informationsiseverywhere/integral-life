package com.dxc.integral.life.beans;

import java.math.BigDecimal;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * DTO for ControlTota lDTO
 * 
 * @author wli31
 *
 */
@Component("l2RerateControlTotalDTO")
@Lazy
public class L2RerateControlTotalDTO {

	private BigDecimal controlTotal01 = BigDecimal.ZERO;
	private BigDecimal controlTotal02 = BigDecimal.ZERO;
	private BigDecimal controlTotal03 = BigDecimal.ZERO;
	private BigDecimal controlTotal04 = BigDecimal.ZERO;
	private BigDecimal controlTotal05 = BigDecimal.ZERO;
	
	public BigDecimal getControlTotal01() {
		return controlTotal01;
	}
	public void setControlTotal01(BigDecimal controlTotal01) {
		this.controlTotal01 = controlTotal01;
	}
	public BigDecimal getControlTotal02() {
		return controlTotal02;
	}
	public void setControlTotal02(BigDecimal controlTotal02) {
		this.controlTotal02 = controlTotal02;
	}
	public BigDecimal getControlTotal03() {
		return controlTotal03;
	}
	public void setControlTotal03(BigDecimal controlTotal03) {
		this.controlTotal03 = controlTotal03;
	}
	public BigDecimal getControlTotal04() {
		return controlTotal04;
	}
	public void setControlTotal04(BigDecimal controlTotal04) {
		this.controlTotal04 = controlTotal04;
	}
	public BigDecimal getControlTotal05() {
		return controlTotal05;
	}
	public void setControlTotal05(BigDecimal controlTotal05) {
		this.controlTotal05 = controlTotal05;
	}
	
}
