package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2464 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String surname;
	private String exact;
	private String secuityno;
	private String zkanasnm;
	private String zkanagnm;
	private String activeField;
	private S2464screensfl s2464screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getExact() {
		return exact;
	}
	public void setExact(String exact) {
		this.exact = exact;
	}
	public String getSecuityno() {
		return secuityno;
	}
	public void setSecuityno(String secuityno) {
		this.secuityno = secuityno;
	}
	public String getZkanasnm() {
		return zkanasnm;
	}
	public void setZkanasnm(String zkanasnm) {
		this.zkanasnm = zkanasnm;
	}
	public String getZkanagnm() {
		return zkanagnm;
	}
	public void setZkanagnm(String zkanagnm) {
		this.zkanagnm = zkanagnm;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S2464screensfl getS2464screensfl() {
		return s2464screensfl;
	}
	public void setS2464screensfl(S2464screensfl s2464screensfl) {
		this.s2464screensfl = s2464screensfl;
	}

	@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
	public static class S2464screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;
		
		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
	
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String slt = "";
			private String alflag = "";	
			private String clntnum = "";	
			private String aaflag = "";	
			private String ownnam = "";	
			private String zdesc = "";	
			private String nameadr = "";	
			private String cltstat = "";

			public String getSlt() {
				return slt;
			}
			public void setSlt(String slt) {
				this.slt = slt;
			}
			public String getAlflag() {
				return alflag;
			}
			public void setAlflag(String alflag) {
				this.alflag = alflag;
			}
			public String getClntnum() {
				return clntnum;
			}
			public void setClntnum(String clntnum) {
				this.clntnum = clntnum;
			}
			public String getAaflag() {
				return aaflag;
			}
			public void setAaflag(String aaflag) {
				this.aaflag = aaflag;
			}
			public String getOwnnam() {
				return ownnam;
			}
			public void setOwnnam(String ownnam) {
				this.ownnam = ownnam;
			}
			public String getZdesc() {
				return zdesc;
			}
			public void setZdesc(String zdesc) {
				this.zdesc = zdesc;
			}
			public String getNameadr() {
				return nameadr;
			}
			public void setNameadr(String nameadr) {
				this.nameadr = nameadr;
			}
			public String getCltstat() {
				return cltstat;
			}
			public void setCltstat(String cltstat) {
				this.cltstat = cltstat;
			}
		} 
	}

	@Override
	public String toString() {
		return "S2464 []";
	}
	
}
