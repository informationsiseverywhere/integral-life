package com.dxc.integral.life.exception;

public class ItemProcessorInitException extends RuntimeException {

	public ItemProcessorInitException(String msg) {
		super(msg);
	}
	
	public ItemProcessorInitException(String msg, Throwable cause) {
		super(msg);
	}
	
	public ItemProcessorInitException(Throwable cause) {
		super(cause);
	}
}
