package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2466 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String clntnum;
	private String servbrh;
	private String startDateDisp;
	private String lsurname;
	private String forAttOf;
	private String cltaddr01;
	private String cltaddr02;
	private String cltaddr03;
	private String cltaddr04;
	private String cltaddr05;
	private String cltpcode;
	private String ctrycode;
	private String cltphone01;
	private String cltphone02;
	private String tlxno;
	private String faxno;
	private String tgram;
	private String secuityno;
	private String statcode;
	private String staffno;
	private String ecact;
	private String capital;
	private String ctryorig;
	private String language;
	private String vip;
	private String cltdobxDisp;
	private String mailing;
	private String dirmail;
	private String lgivname;
	private String optind01;
	private String optind02;
	private String optind03;
	private String optind04;
	private String optind05;
	private String optind06;
	private String taxflag;
	private String cltstat;
	private String optind07;
	private String optind08;
	private String amlstatus;
	private String optind09;
	private String optind10;
	private String optind11;
	private String excep;
	private String zkanasurname;
	private String zkanagivname;
	private String zkanacltaddr01;
	private String zkanacltaddr02;
	private String zkanacltaddr03;
	private String zkanacltaddr04;
	private String zkanacltaddr05;
	private String salutl;
	private String sib001Flag;
	private String abusnumFlag;
	private String abusnum;
	private String branchid;
	private String cltphone01idd;
	private String cltphone02idd;
	private String cltphoneidd;
	private String prefconmtd;
	private String dirmktmtd;
	private String rmblphone;
	private String rinternet;
	private String zdlind;
	private String zstates;
	private String ztown;
	private String fundadminflag;
	private String cntryState;
	private String activeField;
	
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getServbrh() {
		return servbrh;
	}
	public void setServbrh(String servbrh) {
		this.servbrh = servbrh;
	}
	public String getStartDateDisp() {
		return startDateDisp;
	}
	public void setStartDateDisp(String startDateDisp) {
		this.startDateDisp = startDateDisp;
	}
	public String getLsurname() {
		return lsurname;
	}
	public void setLsurname(String lsurname) {
		this.lsurname = lsurname;
	}
	public String getForAttOf() {
		return forAttOf;
	}
	public void setForAttOf(String forAttOf) {
		this.forAttOf = forAttOf;
	}
	public String getCltaddr01() {
		return cltaddr01;
	}
	public void setCltaddr01(String cltaddr01) {
		this.cltaddr01 = cltaddr01;
	}
	public String getCltaddr02() {
		return cltaddr02;
	}
	public void setCltaddr02(String cltaddr02) {
		this.cltaddr02 = cltaddr02;
	}
	public String getCltaddr03() {
		return cltaddr03;
	}
	public void setCltaddr03(String cltaddr03) {
		this.cltaddr03 = cltaddr03;
	}
	public String getCltaddr04() {
		return cltaddr04;
	}
	public void setCltaddr04(String cltaddr04) {
		this.cltaddr04 = cltaddr04;
	}
	public String getCltaddr05() {
		return cltaddr05;
	}
	public void setCltaddr05(String cltaddr05) {
		this.cltaddr05 = cltaddr05;
	}
	public String getCltpcode() {
		return cltpcode;
	}
	public void setCltpcode(String cltpcode) {
		this.cltpcode = cltpcode;
	}
	public String getCtrycode() {
		return ctrycode;
	}
	public void setCtrycode(String ctrycode) {
		this.ctrycode = ctrycode;
	}
	public String getCltphone01() {
		return cltphone01;
	}
	public void setCltphone01(String cltphone01) {
		this.cltphone01 = cltphone01;
	}
	public String getCltphone02() {
		return cltphone02;
	}
	public void setCltphone02(String cltphone02) {
		this.cltphone02 = cltphone02;
	}
	public String getTlxno() {
		return tlxno;
	}
	public void setTlxno(String tlxno) {
		this.tlxno = tlxno;
	}
	public String getFaxno() {
		return faxno;
	}
	public void setFaxno(String faxno) {
		this.faxno = faxno;
	}
	public String getTgram() {
		return tgram;
	}
	public void setTgram(String tgram) {
		this.tgram = tgram;
	}
	public String getSecuityno() {
		return secuityno;
	}
	public void setSecuityno(String secuityno) {
		this.secuityno = secuityno;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getStaffno() {
		return staffno;
	}
	public void setStaffno(String staffno) {
		this.staffno = staffno;
	}
	public String getEcact() {
		return ecact;
	}
	public void setEcact(String ecact) {
		this.ecact = ecact;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public String getCtryorig() {
		return ctryorig;
	}
	public void setCtryorig(String ctryorig) {
		this.ctryorig = ctryorig;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getVip() {
		return vip;
	}
	public void setVip(String vip) {
		this.vip = vip;
	}
	public String getCltdobxDisp() {
		return cltdobxDisp;
	}
	public void setCltdobxDisp(String cltdobxDisp) {
		this.cltdobxDisp = cltdobxDisp;
	}
	public String getMailing() {
		return mailing;
	}
	public void setMailing(String mailing) {
		this.mailing = mailing;
	}
	public String getDirmail() {
		return dirmail;
	}
	public void setDirmail(String dirmail) {
		this.dirmail = dirmail;
	}
	public String getLgivname() {
		return lgivname;
	}
	public void setLgivname(String lgivname) {
		this.lgivname = lgivname;
	}
	public String getOptind01() {
		return optind01;
	}
	public void setOptind01(String optind01) {
		this.optind01 = optind01;
	}
	public String getOptind02() {
		return optind02;
	}
	public void setOptind02(String optind02) {
		this.optind02 = optind02;
	}
	public String getOptind03() {
		return optind03;
	}
	public void setOptind03(String optind03) {
		this.optind03 = optind03;
	}
	public String getOptind04() {
		return optind04;
	}
	public void setOptind04(String optind04) {
		this.optind04 = optind04;
	}
	public String getOptind05() {
		return optind05;
	}
	public void setOptind05(String optind05) {
		this.optind05 = optind05;
	}
	public String getOptind06() {
		return optind06;
	}
	public void setOptind06(String optind06) {
		this.optind06 = optind06;
	}
	public String getTaxflag() {
		return taxflag;
	}
	public void setTaxflag(String taxflag) {
		this.taxflag = taxflag;
	}
	public String getCltstat() {
		return cltstat;
	}
	public void setCltstat(String cltstat) {
		this.cltstat = cltstat;
	}
	public String getOptind07() {
		return optind07;
	}
	public void setOptind07(String optind07) {
		this.optind07 = optind07;
	}
	public String getOptind08() {
		return optind08;
	}
	public void setOptind08(String optind08) {
		this.optind08 = optind08;
	}
	public String getAmlstatus() {
		return amlstatus;
	}
	public void setAmlstatus(String amlstatus) {
		this.amlstatus = amlstatus;
	}
	public String getOptind09() {
		return optind09;
	}
	public void setOptind09(String optind09) {
		this.optind09 = optind09;
	}
	public String getOptind10() {
		return optind10;
	}
	public void setOptind10(String optind10) {
		this.optind10 = optind10;
	}
	public String getOptind11() {
		return optind11;
	}
	public void setOptind11(String optind11) {
		this.optind11 = optind11;
	}
	public String getExcep() {
		return excep;
	}
	public void setExcep(String excep) {
		this.excep = excep;
	}
	public String getZkanasurname() {
		return zkanasurname;
	}
	public void setZkanasurname(String zkanasurname) {
		this.zkanasurname = zkanasurname;
	}
	public String getZkanagivname() {
		return zkanagivname;
	}
	public void setZkanagivname(String zkanagivname) {
		this.zkanagivname = zkanagivname;
	}
	public String getZkanacltaddr01() {
		return zkanacltaddr01;
	}
	public void setZkanacltaddr01(String zkanacltaddr01) {
		this.zkanacltaddr01 = zkanacltaddr01;
	}
	public String getZkanacltaddr02() {
		return zkanacltaddr02;
	}
	public void setZkanacltaddr02(String zkanacltaddr02) {
		this.zkanacltaddr02 = zkanacltaddr02;
	}
	public String getZkanacltaddr03() {
		return zkanacltaddr03;
	}
	public void setZkanacltaddr03(String zkanacltaddr03) {
		this.zkanacltaddr03 = zkanacltaddr03;
	}
	public String getZkanacltaddr04() {
		return zkanacltaddr04;
	}
	public void setZkanacltaddr04(String zkanacltaddr04) {
		this.zkanacltaddr04 = zkanacltaddr04;
	}
	public String getZkanacltaddr05() {
		return zkanacltaddr05;
	}
	public void setZkanacltaddr05(String zkanacltaddr05) {
		this.zkanacltaddr05 = zkanacltaddr05;
	}
	public String getSalutl() {
		return salutl;
	}
	public void setSalutl(String salutl) {
		this.salutl = salutl;
	}
	public String getSib001Flag() {
		return sib001Flag;
	}
	public void setSib001Flag(String sib001Flag) {
		this.sib001Flag = sib001Flag;
	}
	public String getAbusnumFlag() {
		return abusnumFlag;
	}
	public void setAbusnumFlag(String abusnumFlag) {
		this.abusnumFlag = abusnumFlag;
	}
	public String getAbusnum() {
		return abusnum;
	}
	public void setAbusnum(String abusnum) {
		this.abusnum = abusnum;
	}
	public String getBranchid() {
		return branchid;
	}
	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
	public String getCltphone01idd() {
		return cltphone01idd;
	}
	public void setCltphone01idd(String cltphone01idd) {
		this.cltphone01idd = cltphone01idd;
	}
	public String getCltphone02idd() {
		return cltphone02idd;
	}
	public void setCltphone02idd(String cltphone02idd) {
		this.cltphone02idd = cltphone02idd;
	}
	public String getCltphoneidd() {
		return cltphoneidd;
	}
	public void setCltphoneidd(String cltphoneidd) {
		this.cltphoneidd = cltphoneidd;
	}
	public String getPrefconmtd() {
		return prefconmtd;
	}
	public void setPrefconmtd(String prefconmtd) {
		this.prefconmtd = prefconmtd;
	}
	public String getDirmktmtd() {
		return dirmktmtd;
	}
	public void setDirmktmtd(String dirmktmtd) {
		this.dirmktmtd = dirmktmtd;
	}
	public String getRmblphone() {
		return rmblphone;
	}
	public void setRmblphone(String rmblphone) {
		this.rmblphone = rmblphone;
	}
	public String getRinternet() {
		return rinternet;
	}
	public void setRinternet(String rinternet) {
		this.rinternet = rinternet;
	}
	public String getZdlind() {
		return zdlind;
	}
	public void setZdlind(String zdlind) {
		this.zdlind = zdlind;
	}
	public String getZstates() {
		return zstates;
	}
	public void setZstates(String zstates) {
		this.zstates = zstates;
	}
	public String getZtown() {
		return ztown;
	}
	public void setZtown(String ztown) {
		this.ztown = ztown;
	}
	public String getFundadminflag() {
		return fundadminflag;
	}
	public void setFundadminflag(String fundadminflag) {
		this.fundadminflag = fundadminflag;
	}
	public String getCntryState() {
		return cntryState;
	}
	public void setCntryState(String cntryState) {
		this.cntryState = cntryState;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S2466 []";
	}
	
}
