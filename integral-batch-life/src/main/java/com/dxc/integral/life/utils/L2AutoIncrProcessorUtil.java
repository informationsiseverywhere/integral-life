package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.L2AutoIncrProcessorDTO;
import com.dxc.integral.life.beans.L2AutoIncrReaderDTO;

public interface L2AutoIncrProcessorUtil {
	public void loadSmartTables(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO);

	public int readLeadDays(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO);

	public boolean validContract(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO);

	public boolean softlock(String chdrnum, String tranCode, String bactchName);

	public void updateRecord(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO);
}
