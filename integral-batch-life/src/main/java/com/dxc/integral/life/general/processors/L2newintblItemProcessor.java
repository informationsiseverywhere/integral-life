package com.dxc.integral.life.general.processors;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.beans.L2newintblControlTotalDTO;
import com.dxc.integral.life.beans.L2newintblReaderDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.utils.Intcalc;
import com.dxc.integral.life.utils.L2newintblProcessorUtil;
import com.dxc.integral.life.utils.Lifacmv;
import com.fasterxml.jackson.databind.ObjectMapper;

@Scope(value = "step")
@Async
public class L2newintblItemProcessor extends BaseProcessor implements ItemProcessor<L2newintblReaderDTO, L2newintblReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2newintblItemProcessor.class);
	
	/** The batch job name for L2NEWINTBL */
	public static final String L2NEWINTBL_JOB_NAME = "L2NEWINTBL";
	
	private String batchCompany = Companies.LIFE;
	private String programName = "BR539";
	
	@Autowired
	private ItempfDAO itempfDAO;
	
	@Autowired
	private L2newintblControlTotalDTO l2newintblControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Autowired
	private L2newintblProcessorUtil l2newintblProcessorUtil;
	
	@Autowired
	private Intcalc intcalc;
	
	@Autowired
	private Lifacmv lifacmv;
	
	@Autowired
	private Zrdecplc zrdecplc;
	
	private Usrdpf usrdpf = null;
	private Batcpf batchDetail = null;
	public static final String BATCH_TRAN_CODE = "BA69";
	
	// smart table
	
	private T3629 t3629IO = null;
	private T5645 t5645IO = null;

	
	private int sequenceNo;
	private int intIndex;
	private String strSacscode;
	private String strSacsType;
	private String strglMap;
	private String strglSign;
	private Integer oldlstintbdte;
	@Override
	public L2newintblReaderDTO process(L2newintblReaderDTO readerDTO) throws IOException, ParseException, Exception{
		LOGGER.info("Processing starts for contract : {}", readerDTO.getLo_chdrnum());//IJTI-1498
		/*Init section*/
		Integer tranDate = getEffdate(transactionDate);
		usrdpf = getUserDetails(userName);
		
		readSmartTables(readerDTO);
		batchDetail = getBatcpfInfo(transactionDate, businessDate,batchCompany, BATCH_TRAN_CODE,branch, usrdpf, batchName);		
		/* read section*/
//		Slckpf slckpf = new Slckpf();
//		slckpf.setEntity(readerDTO.getLo_chdrnum());
//		slckpf.setEnttyp("CH");
//		slckpf.setCompany(readerDTO.getLo_chdrcoy());
//		slckpf.setProctrancd(BATCH_TRAN_CODE);
//		slckpf.setUsrprf(userName);
//		slckpf.setJobnm(batchName);
		boolean softLock = softlock(readerDTO.getLo_chdrnum(),BATCH_TRAN_CODE,batchName);
		if (!softLock) {
			LOGGER.info("Contract [{}] is already locked.", readerDTO.getLo_chdrnum());//IJTI-1498
			l2newintblControlTotalDTO
				.setControlTotal02(l2newintblControlTotalDTO.getControlTotal02().add(BigDecimal.ONE));
			return null;
		} 
		
		/* update section */
		// chdr
		readerDTO.setCh_tranno(readerDTO.getCh_tranno()+1);
		readerDTO.setLo_jobnm(batchName);
		readerDTO.setLo_usrprf(userName);
		readerDTO.setEffectiveDate(tranDate);
		IntcalcDTO intercalDTO = l2newintblProcessorUtil.populateIntcalculation(readerDTO);
		BigDecimal totalIntrest=intcalc.interestCalculation(intercalDTO);
		totalIntrest = callRounding(totalIntrest, readerDTO);
		readerDTO.setInterestAmount(totalIntrest);
		// updateLoanRecord3040
	    readerDTO.setLo_validflag("1");
	    oldlstintbdte = readerDTO.getLo_lstintbdte();
		readerDTO.setLo_lstintbdte(readerDTO.getLo_nxtintbdte());
		readerDTO.setLo_nxtintbdte(l2newintblProcessorUtil.calcNextBillDate(readerDTO));
		readerDTO.setLo_trdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
		readerDTO.setLo_trtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
		
		// postInterestAcmvs3050
		LifacmvDTO lifeacmvDTO = l2newintblProcessorUtil.populateLifAcmv(readerDTO, batchDetail, oldlstintbdte);
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		/* Post the interest to the Loan debit account (depending on*/
		/* whether it is a Policy Loan, an APL or a Cash Deposit).*/
		getGLAcctCodes(readerDTO.getLo_loantype(), readerDTO.getPy_mandref(),0);
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifeacmvDTO.setSacscode(strSacscode);
		lifeacmvDTO.setSacstyp(strSacsType);
		lifeacmvDTO.setGlcode(strglMap);
		lifeacmvDTO.setGlsign(strglSign);
		lifacmv.executePSTW(lifeacmvDTO);
		readerDTO.setSacscode(strSacscode);
		readerDTO.setSacsType(strSacsType);
		readerDTO.setGlMap(strglMap);
		sequenceNo++;
		getGLAcctCodes(readerDTO.getLo_loantype(), readerDTO.getPy_mandref(),1);
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifeacmvDTO.setSacscode(strSacscode);
		lifeacmvDTO.setSacstyp(strSacsType);
		lifeacmvDTO.setGlcode(strglMap);
		lifeacmvDTO.setGlsign(strglSign);
		lifacmv.executePSTW(lifeacmvDTO);
		
		// writeBextRecord3060
		readerDTO.setBankcode(t3629IO.getBankcode());
		
		// writePtrnRecord3070
		readerDTO.setBatcpfx(batchDetail.getBatcpfx());
		readerDTO.setBatccoy(batchDetail.getBatccoy());
		readerDTO.setBatcbrn(batchDetail.getBatcbrn());
		readerDTO.setBatcactyr(batchDetail.getBatcactyr());
		readerDTO.setBatcactmn(batchDetail.getBatcactmn());
		readerDTO.setBatctrcde(batchDetail.getBatctrcde());
		readerDTO.setBatcbatch(batchDetail.getBatcbatch());
		return readerDTO;
	}
	
	private BigDecimal callRounding(BigDecimal amountIn, L2newintblReaderDTO readerDTO) throws IOException {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCompany(readerDTO.getLo_chdrcoy());
		zrdecplDTO.setCurrency(readerDTO.getLo_loancurr());
		zrdecplDTO.setBatctrcde(BATCH_TRAN_CODE);
		return zrdecplc.convertAmount(zrdecplDTO);
	}

	private void readSmartTables(L2newintblReaderDTO readerDTO) throws IOException {
		Map<String, List<Itempf>> t5645Map=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T5645", CommonConstants.IT_ITEMPFX);
		if (t5645IO == null && t5645Map.containsKey(programName)) {
			List<Itempf> t5645ItemList = t5645Map.get(programName);
			t5645IO = new ObjectMapper().readValue(t5645ItemList.get(0).getGenareaj(), T5645.class);
		}else if(t5645IO==null){
			throw new ItemNotfoundException("Br539: Item "+programName+" not found in Table T5645 while processing contract "+readerDTO.getLo_chdrnum());
		}
		Map<String, List<Itempf>> t3629Map=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T3629", CommonConstants.IT_ITEMPFX);
		if (t3629IO == null && t3629Map.containsKey(readerDTO.getPy_billcurr())) {
			List<Itempf> t3629ItemList = t3629Map.get(readerDTO.getPy_billcurr());
			t3629IO = new ObjectMapper().readValue(t3629ItemList.get(0).getGenareaj(), T3629.class);
		}else if(t5645IO==null){
			throw new ItemNotfoundException("Br539: Item "+readerDTO.getPy_billcurr()+" not found in Table T3629 while processing contract "+readerDTO.getLo_chdrnum());
		}
		
	}
	
	
	private void getGLAcctCodes(String loanType, String mandRef, int intIncrement){
		
		if (loanType.toUpperCase().equals("P")){
			intIndex = 0;
		}
		else if (loanType.toUpperCase().equals("A")){
			intIndex = 2;
		}
		else if (loanType.toUpperCase().equals("E")){		
			intIndex = 4;
		}
		else if (loanType.toUpperCase().equals("D")){
			intIndex = 6;
		}	
		if (!mandRef.equals(" ")) {
			strSacscode = t5645IO.getSacscodes().get(intIndex + intIncrement);
			strSacsType = t5645IO.getSacstypes().get(intIndex + intIncrement);
			strglMap = t5645IO.getGlmaps().get(intIndex + intIncrement);
			strglSign = t5645IO.getSigns().get(intIndex + intIncrement);
		}
		else {
			strSacscode = "";
			strSacsType = "";
			strglMap = "";
			strglSign = "";
		}
	}
	
}
