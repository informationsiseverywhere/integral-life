package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.beans.L2newintblReaderDTO;
import com.dxc.integral.life.beans.LifacmvDTO;

public interface L2newintblProcessorUtil {
     public IntcalcDTO populateIntcalculation(L2newintblReaderDTO readerDTO) ;
     public Integer calcNextBillDate(L2newintblReaderDTO readerDTO)throws IOException, ParseException;
     public LifacmvDTO populateLifAcmv(L2newintblReaderDTO readerDTO,Batcpf batcpf, Integer lstintbdte) throws Exception;
}
