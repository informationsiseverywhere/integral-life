package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.List;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Ainrpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

public class L2AutoIncrReaderDTO {
	
	private long uniqueNumber;
	private String chdrCoy;
	private String life;
	private String chdrNum;
	private String jLife;
	private String coverage;
	private String rider;
	private Integer plnSfx;
	private String validFlag;
	private Integer tranNo;
	private String batcBrn;
	private String termId;
	private Integer batcActyr;
	private Integer batcActmn;
	private String batcTrcde;
	private String batcBatch;
	private String batcPfx;
	private String batcCoy;
	private String effectiveDate;
	private Integer trdt;
	private Integer trtm;
	private String cntType;
	private int billcd;
	private String statCode;
	private String pstatCode;
	private int crrcd;
	private int anbccd;
	private int rcesdte;
	private int pcesdte;
	private int bcesdte;
	private String crtable;
	private int nxtdte;
	private int rcesage;
	private int bcesage;
	private int pcesage;
	private int bcestrm;
	private int rcestrm;
	private int pcestrm;
	private int rrtfrm;
	private String prmcur;
	private BigDecimal sumIns;
	private String mortCls;
	private String liencd;
	private int cpiDte;
	private BigDecimal zstpDuty01;
	private String zclState;
	private String lnkgNo;
	private String lnkgsubrefno;
	private String tpdType;
	private String lnkgInd;
	private String singPremType;
	private BigDecimal instPrem;
	private BigDecimal zlinstPrem;
	private BigDecimal zbinstPrem;
	
	private boolean permission;
	private boolean stampDutyflag;
	private boolean prmhldtrad;
	private int transDate;
	private int transTime;
	private String sysChdrnum;
	private String sysCoverage;
	private String indxin;
	private List<Ptrnpf> ptrnpfInsertList;
	private Covrpf covrpfInsert;
	private Covrpf covrpfUpdate;
	private Chdrpf chdrpfUpdate;
	private Payrpf payrpfUpdate;
	private Ainrpf ainrpfInsert;
	private Incrpf incrpfInsert;
	
	public String getSysChdrnum() {
		return sysChdrnum;
	}
	public void setSysChdrnum(String sysChdrnum) {
		this.sysChdrnum = sysChdrnum;
	}
	public String getSysCoverage() {
		return sysCoverage;
	}
	public void setSysCoverage(String sysCoverage) {
		this.sysCoverage = sysCoverage;
	}
	public boolean isStampDutyflag() {
		return stampDutyflag;
	}
	public void setStampDutyflag(boolean stampDutyflag) {
		this.stampDutyflag = stampDutyflag;
	}
	public boolean isPrmhldtrad() {
		return prmhldtrad;
	}
	public void setPrmhldtrad(boolean prmhldtrad) {
		this.prmhldtrad = prmhldtrad;
	}
	public int getTransDate() {
		return transDate;
	}
	public void setTransDate(int transDate) {
		this.transDate = transDate;
	}
	public int getTransTime() {
		return transTime;
	}
	public void setTransTime(int transTime) {
		this.transTime = transTime;
	}
	public boolean isPermission() {
		return permission;
	}
	public void setPermission(boolean permission) {
		this.permission = permission;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrCoy() {
		return chdrCoy;
	}
	public void setChdrCoy(String chdrCoy) {
		this.chdrCoy = chdrCoy;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getjLife() {
		return jLife;
	}
	public void setjLife(String jLife) {
		this.jLife = jLife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnSfx() {
		return plnSfx;
	}
	public void setPlnSfx(Integer plnSfx) {
		this.plnSfx = plnSfx;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public Integer getTranNo() {
		return tranNo;
	}
	public void setTranNo(Integer tranNo) {
		this.tranNo = tranNo;
	}
	public String getBatcBrn() {
		return batcBrn;
	}
	public void setBatcBrn(String batcBrn) {
		this.batcBrn = batcBrn;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public Integer getBatcActyr() {
		return batcActyr;
	}
	public void setBatcActyr(Integer batcActyr) {
		this.batcActyr = batcActyr;
	}
	public Integer getBatcActmn() {
		return batcActmn;
	}
	public void setBatcActmn(Integer batcActmn) {
		this.batcActmn = batcActmn;
	}
	public String getBatcTrcde() {
		return batcTrcde;
	}
	public void setBatcTrcde(String batcTrcde) {
		this.batcTrcde = batcTrcde;
	}
	public String getBatcBatch() {
		return batcBatch;
	}
	public void setBatcBatch(String batcBatch) {
		this.batcBatch = batcBatch;
	}
	public String getBatcPfx() {
		return batcPfx;
	}
	public void setBatcPfx(String batcPfx) {
		this.batcPfx = batcPfx;
	}
	public String getBatcCoy() {
		return batcCoy;
	}
	public void setBatcCoy(String batcCoy) {
		this.batcCoy = batcCoy;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public String getCntType() {
		return cntType;
	}
	public void setCntType(String cntType) {
		this.cntType = cntType;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getStatCode() {
		return statCode;
	}
	public void setStatCode(String statCode) {
		this.statCode = statCode;
	}
	public String getPstatCode() {
		return pstatCode;
	}
	public void setPstatCode(String pstatCode) {
		this.pstatCode = pstatCode;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public int getAnbccd() {
		return anbccd;
	}
	public void setAnbccd(int anbccd) {
		this.anbccd = anbccd;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public int getPcesdte() {
		return pcesdte;
	}
	public void setPcesdte(int pcesdte) {
		this.pcesdte = pcesdte;
	}
	public int getBcesdte() {
		return bcesdte;
	}
	public void setBcesdte(int bcesdte) {
		this.bcesdte = bcesdte;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getNxtdte() {
		return nxtdte;
	}
	public void setNxtdte(int nxtdte) {
		this.nxtdte = nxtdte;
	}
	public int getRcesage() {
		return rcesage;
	}
	public void setRcesage(int rcesage) {
		this.rcesage = rcesage;
	}
	public int getBcesage() {
		return bcesage;
	}
	public void setBcesage(int bcesage) {
		this.bcesage = bcesage;
	}
	public int getPcesage() {
		return pcesage;
	}
	public void setPcesage(int pcesage) {
		this.pcesage = pcesage;
	}
	public int getBcestrm() {
		return bcestrm;
	}
	public void setBcestrm(int bcestrm) {
		this.bcestrm = bcestrm;
	}
	public int getRcestrm() {
		return rcestrm;
	}
	public void setRcestrm(int rcestrm) {
		this.rcestrm = rcestrm;
	}
	public int getPcestrm() {
		return pcestrm;
	}
	public void setPcestrm(int pcestrm) {
		this.pcestrm = pcestrm;
	}
	public int getRrtfrm() {
		return rrtfrm;
	}
	public void setRrtfrm(int rrtfrm) {
		this.rrtfrm = rrtfrm;
	}
	public String getPrmcur() {
		return prmcur;
	}
	public void setPrmcur(String prmcur) {
		this.prmcur = prmcur;
	}
	public BigDecimal getSumIns() {
		return sumIns;
	}
	public void setSumIns(BigDecimal sumIns) {
		this.sumIns = sumIns;
	}
	public String getMortCls() {
		return mortCls;
	}
	public void setMortCls(String mortCls) {
		this.mortCls = mortCls;
	}
	public String getLiencd() {
		return liencd;
	}
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	public int getCpiDte() {
		return cpiDte;
	}
	public void setCpiDte(int cpiDte) {
		this.cpiDte = cpiDte;
	}
	public BigDecimal getZstpDuty01() {
		return zstpDuty01;
	}
	public void setZstpDuty01(BigDecimal zstpDuty01) {
		this.zstpDuty01 = zstpDuty01;
	}
	public String getZclState() {
		return zclState;
	}
	public void setZclState(String zclState) {
		this.zclState = zclState;
	}
	public String getLnkgNo() {
		return lnkgNo;
	}
	public void setLnkgNo(String lnkgNo) {
		this.lnkgNo = lnkgNo;
	}
	public String getLnkgsubrefno() {
		return lnkgsubrefno;
	}
	public void setLnkgsubrefno(String lnkgsubrefno) {
		this.lnkgsubrefno = lnkgsubrefno;
	}
	public String getTpdType() {
		return tpdType;
	}
	public void setTpdType(String tpdType) {
		this.tpdType = tpdType;
	}
	public String getLnkgInd() {
		return lnkgInd;
	}
	public void setLnkgInd(String lnkgInd) {
		this.lnkgInd = lnkgInd;
	}
	public String getSingPremType() {
		return singPremType;
	}
	public void setSingPremType(String singPremType) {
		this.singPremType = singPremType;
	}
	public BigDecimal getZlinstPrem() {
		return zlinstPrem;
	}
	public void setZlinstPrem(BigDecimal zlinstPrem) {
		this.zlinstPrem = zlinstPrem;
	}
	public BigDecimal getInstPrem() {
		return instPrem;
	}
	public void setInstPrem(BigDecimal instPrem) {
		this.instPrem = instPrem;
	}
	public BigDecimal getZbinstPrem() {
		return zbinstPrem;
	}
	public void setZbinstPrem(BigDecimal zbinstPrem) {
		this.zbinstPrem = zbinstPrem;
	}
	public String getIndxin() {
		return indxin;
	}
	public void setIndxin(String indxin) {
		this.indxin = indxin;
	}
	public List<Ptrnpf> getPtrnpfInsertList() {
		return ptrnpfInsertList;
	}
	public void setPtrnpfInsertList(List<Ptrnpf> ptrnpfInsertList) {
		this.ptrnpfInsertList = ptrnpfInsertList;
	}
	public Covrpf getCovrpfInsert() {
		return covrpfInsert;
	}
	public void setCovrpfInsert(Covrpf covrpfInsert) {
		this.covrpfInsert = covrpfInsert;
	}
	public Covrpf getCovrpfUpdate() {
		return covrpfUpdate;
	}
	public void setCovrpfUpdate(Covrpf covrpfUpdate) {
		this.covrpfUpdate = covrpfUpdate;
	}
	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}
	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}
	public Payrpf getPayrpfUpdate() {
		return payrpfUpdate;
	}
	public void setPayrpfUpdate(Payrpf payrpfUpdate) {
		this.payrpfUpdate = payrpfUpdate;
	}
	public Ainrpf getAinrpfInsert() {
		return ainrpfInsert;
	}
	public void setAinrpfInsert(Ainrpf ainrpfInsert) {
		this.ainrpfInsert = ainrpfInsert;
	}
	public Incrpf getIncrpfInsert() {
		return incrpfInsert;
	}
	public void setIncrpfInsert(Incrpf incrpfInsert) {
		this.incrpfInsert = incrpfInsert;
	}
}
