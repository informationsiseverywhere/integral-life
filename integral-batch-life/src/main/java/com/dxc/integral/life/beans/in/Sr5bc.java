package com.dxc.integral.life.beans.in;
import java.io.Serializable;
public class Sr5bc implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String mbrAccNum;
	private String taxFreeCompAmt;
	private String kiwiCompAmt;
	private String nonTaxAmt;
	private String presAmt;
	private String kiwiPresAmt;
	private String unRestrictAmt;
	private String restrictAmt;
	private String totTranAmt;
	private String moneyInd;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}
	
	public String getMbrAccNum() {
		return mbrAccNum;
	}
	public void setMbrAccNum(String mbrAccNum) {
		this.mbrAccNum = mbrAccNum;
	}
	public String getTaxFreeCompAmt() {
		return taxFreeCompAmt;
	}
	public void setTaxFreeCompAmt(String taxFreeCompAmt) {
		this.taxFreeCompAmt = taxFreeCompAmt;
	}
	public String getKiwiCompAmt() {
		return kiwiCompAmt;
	}
	public void setKiwiCompAmt(String kiwiCompAmt) {
		this.kiwiCompAmt = kiwiCompAmt;
	}
	public String getNonTaxAmt() {
		return nonTaxAmt;
	}
	public void setNonTaxAmt(String nonTaxAmt) {
		this.nonTaxAmt = nonTaxAmt;
	}
	public String getPresAmt() {
		return presAmt;
	}
	public void setPresAmt(String presAmt) {
		this.presAmt = presAmt;
	}
	public String getKiwiPresAmt() {
		return kiwiPresAmt;
	}
	public void setKiwiPresAmt(String kiwiPresAmt) {
		this.kiwiPresAmt = kiwiPresAmt;
	}
	public String getUnRestrictAmt() {
		return unRestrictAmt;
	}
	public void setUnRestrictAmt(String unRestrictAmt) {
		this.unRestrictAmt = unRestrictAmt;
	}
	public String getRestrictAmt() {
		return restrictAmt;
	}
	public void setRestrictAmt(String restrictAmt) {
		this.restrictAmt = restrictAmt;
	}
	public String getTotTranAmt() {
		return totTranAmt;
	}
	public void setTotTranAmt(String totTranAmt) {
		this.totTranAmt = totTranAmt;
	}
	public String getMoneyInd() {
		return moneyInd;
	}
	public void setMoneyInd(String moneyInd) {
		this.moneyInd = moneyInd;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "Sr5bc []";
	}
	
}
