package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5125 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String sumin;
	private String riskCessAge;
	private String riskCessTerm;
	private String riskCessDateDisp;
	private String premCessAge;
	private String premCessTerm;
	private String premCessDateDisp;
	private String benCessAge;
	private String benCessTerm;
	private String benCessDateDisp;
	private String mortcls;
	private String liencd;
	private String bappmeth;
	private String optextind;
	private String instPrem;
	private String ratypind;
	private String anntind;
	private String select;
	private String numapp;
	private String pbind;
	private String taxind;
	private String waitperiod;
	private String bentrm;
	private String poltyp;
	private String prmbasis;
	private String statcode;
	private String dialdownoption;
	private String exclind;
	private String lnkgno;
	private String lnkgsubrefno;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getSumin() {
		return sumin;
	}
	public void setSumin(String sumin) {
		this.sumin = sumin;
	}
	public String getRiskCessAge() {
		return riskCessAge;
	}
	public void setRiskCessAge(String riskCessAge) {
		this.riskCessAge = riskCessAge;
	}
	public String getRiskCessTerm() {
		return riskCessTerm;
	}
	public void setRiskCessTerm(String riskCessTerm) {
		this.riskCessTerm = riskCessTerm;
	}
	public String getRiskCessDateDisp() {
		return riskCessDateDisp;
	}
	public void setRiskCessDateDisp(String riskCessDateDisp) {
		this.riskCessDateDisp = riskCessDateDisp;
	}
	public String getPremCessAge() {
		return premCessAge;
	}
	public void setPremCessAge(String premCessAge) {
		this.premCessAge = premCessAge;
	}
	public String getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(String premCessTerm) {
		this.premCessTerm = premCessTerm;
	}
	public String getPremCessDateDisp() {
		return premCessDateDisp;
	}
	public void setPremCessDateDisp(String premCessDateDisp) {
		this.premCessDateDisp = premCessDateDisp;
	}
	public String getBenCessAge() {
		return benCessAge;
	}
	public void setBenCessAge(String benCessAge) {
		this.benCessAge = benCessAge;
	}
	public String getBenCessTerm() {
		return benCessTerm;
	}
	public void setBenCessTerm(String benCessTerm) {
		this.benCessTerm = benCessTerm;
	}
	public String getBenCessDateDisp() {
		return benCessDateDisp;
	}
	public void setBenCessDateDisp(String benCessDateDisp) {
		this.benCessDateDisp = benCessDateDisp;
	}
	public String getMortcls() {
		return mortcls;
	}
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	public String getLiencd() {
		return liencd;
	}
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	public String getBappmeth() {
		return bappmeth;
	}
	public void setBappmeth(String bappmeth) {
		this.bappmeth = bappmeth;
	}
	public String getOptextind() {
		return optextind;
	}
	public void setOptextind(String optextind) {
		this.optextind = optextind;
	}
	public String getInstPrem() {
		return instPrem;
	}
	public void setInstPrem(String instPrem) {
		this.instPrem = instPrem;
	}
	public String getRatypind() {
		return ratypind;
	}
	public void setRatypind(String ratypind) {
		this.ratypind = ratypind;
	}
	public String getAnntind() {
		return anntind;
	}
	public void setAnntind(String anntind) {
		this.anntind = anntind;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getNumapp() {
		return numapp;
	}
	public void setNumapp(String numapp) {
		this.numapp = numapp;
	}
	public String getPbind() {
		return pbind;
	}
	public void setPbind(String pbind) {
		this.pbind = pbind;
	}
	public String getTaxind() {
		return taxind;
	}
	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}
	public String getWaitperiod() {
		return waitperiod;
	}
	public void setWaitperiod(String waitperiod) {
		this.waitperiod = waitperiod;
	}
	public String getBentrm() {
		return bentrm;
	}
	public void setBentrm(String bentrm) {
		this.bentrm = bentrm;
	}
	public String getPoltyp() {
		return poltyp;
	}
	public void setPoltyp(String poltyp) {
		this.poltyp = poltyp;
	}
	public String getPrmbasis() {
		return prmbasis;
	}
	public void setPrmbasis(String prmbasis) {
		this.prmbasis = prmbasis;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getDialdownoption() {
		return dialdownoption;
	}
	public void setDialdownoption(String dialdownoption) {
		this.dialdownoption = dialdownoption;
	}
	public String getExclind() {
		return exclind;
	}
	public void setExclind(String exclind) {
		this.exclind = exclind;
	}
	public String getLnkgno() {
		return lnkgno;
	}
	public void setLnkgno(String lnkgno) {
		this.lnkgno = lnkgno;
	}
	public String getLnkgsubrefno() {
		return lnkgsubrefno;
	}
	public void setLnkgsubrefno(String lnkgsubrefno) {
		this.lnkgsubrefno = lnkgsubrefno;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5125 []";
	}
	
}
