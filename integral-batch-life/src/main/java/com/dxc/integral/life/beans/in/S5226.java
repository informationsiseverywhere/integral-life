package com.dxc.integral.life.beans.in;
import java.io.Serializable;
public class S5226 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String regpayfreq;
	private String destkey;
	private String ddind;
	private String cltype;
	private String claimevd;
	private String rgpymop;
	private String payclt;
	private String claimcur;
	private String anvdateDisp;
	private String finalPaydateDisp;
	private String pymt;
	private String prcnt;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getRegpayfreq() {
		return regpayfreq;
	}
	public void setRegpayfreq(String regpayfreq) {
		this.regpayfreq = regpayfreq;
	}
	public String getDestkey() {
		return destkey;
	}
	public void setDestkey(String destkey) {
		this.destkey = destkey;
	}
	public String getDdind() {
		return ddind;
	}
	public void setDdind(String ddind) {
		this.ddind = ddind;
	}
	public String getCltype() {
		return cltype;
	}
	public void setCltype(String cltype) {
		this.cltype = cltype;
	}
	public String getClaimevd() {
		return claimevd;
	}
	public void setClaimevd(String claimevd) {
		this.claimevd = claimevd;
	}
	public String getRgpymop() {
		return rgpymop;
	}
	public void setRgpymop(String rgpymop) {
		this.rgpymop = rgpymop;
	}
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	public String getClaimcur() {
		return claimcur;
	}
	public void setClaimcur(String claimcur) {
		this.claimcur = claimcur;
	}
	public String getAnvdateDisp() {
		return anvdateDisp;
	}
	public void setAnvdateDisp(String anvdateDisp) {
		this.anvdateDisp = anvdateDisp;
	}
	public String getFinalPaydateDisp() {
		return finalPaydateDisp;
	}
	public void setFinalPaydateDisp(String finalPaydateDisp) {
		this.finalPaydateDisp = finalPaydateDisp;
	}
	public String getPymt() {
		return pymt;
	}
	public void setPymt(String pymt) {
		this.pymt = pymt;
	}
	public String getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(String prcnt) {
		this.prcnt = prcnt;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5226 []";
	}
	
}
