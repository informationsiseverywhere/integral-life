package com.dxc.integral.life.beans;


/**
 * L2ANENDRLX batch reader DTO.
 * 
 * @author mmalik25
 *
 */
public class L2ownchgReportDTO {
	private int zseqno;
	private String chdrpfx;
	private String chdrnum;
	private String cownum;
	private String cnttype;
	private String occdate;
	private String effdates; 
	private String rname; 
	private String lname;
	private String cownnum; 
	private String lifcnum;
	private String clntlname;
	private String cltdob;
	public int getZseqno() {
		return zseqno;
	}
	public void setZseqno(int zseqno) {
		this.zseqno = zseqno;
	}
	
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getCownum() {
		return cownum;
	}
	public void setCownum(String cownum) {
		this.cownum = cownum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getOccdate() {
		return occdate;
	}
	public void setOccdate(String occdate) {
		this.occdate = occdate;
	}
	public String getEffdates() {
		return effdates;
	}
	public void setEffdates(String effdates) {
		this.effdates = effdates;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}
	public String getClntlname() {
		return clntlname;
	}
	public void setClntlname(String clntlname) {
		this.clntlname = clntlname;
	}
	public String getCltdob() {
		return cltdob;
	}
	public void setCltdob(String cltdob) {
		this.cltdob = cltdob;
	}
	

}