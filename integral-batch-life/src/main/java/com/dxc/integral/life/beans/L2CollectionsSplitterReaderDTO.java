package com.dxc.integral.life.beans;

public class L2CollectionsSplitterReaderDTO {

	private String wsaaLinxRunid;
	private int noOfSubThreads;
	private String wsaaEffdate;
	private String wsaaCompany;
	private String wsaaChdrnumFrom;
	private String wsaaChdrnumTo;
	private boolean aCAGClearFlag;
	public String getWsaaLinxRunid() {
		return wsaaLinxRunid;
	}
	public void setWsaaLinxRunid(String wsaaLinxRunid) {
		this.wsaaLinxRunid = wsaaLinxRunid;
	}
	public int getNoOfSubThreads() {
		return noOfSubThreads;
	}
	public void setNoOfSubThreads(int noOfSubThreads) {
		this.noOfSubThreads = noOfSubThreads;
	}
	public String getWsaaEffdate() {
		return wsaaEffdate;
	}
	public void setWsaaEffdate(String wsaaEffdate) {
		this.wsaaEffdate = wsaaEffdate;
	}
	public String getWsaaCompany() {
		return wsaaCompany;
	}
	public void setWsaaCompany(String wsaaCompany) {
		this.wsaaCompany = wsaaCompany;
	}
	public String getWsaaChdrnumFrom() {
		return wsaaChdrnumFrom;
	}
	public void setWsaaChdrnumFrom(String wsaaChdrnumFrom) {
		this.wsaaChdrnumFrom = wsaaChdrnumFrom;
	}
	public String getWsaaChdrnumTo() {
		return wsaaChdrnumTo;
	}
	public void setWsaaChdrnumTo(String wsaaChdrnumTo) {
		this.wsaaChdrnumTo = wsaaChdrnumTo;
	}
	public int getScheduleNumber() {
		return scheduleNumber;
	}
	public void setScheduleNumber(int scheduleNumber) {
		this.scheduleNumber = scheduleNumber;
	}
	private int scheduleNumber;
	public boolean isaCAGClearFlag() {
		return aCAGClearFlag;
	}
	public void setaCAGClearFlag(boolean aCAGClearFlag) {
		this.aCAGClearFlag = aCAGClearFlag;
	}
}
