package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2AutoIncrReaderDTO;
import com.dxc.integral.life.dao.AinrpfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Ainrpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

public class L2AutoIncrWriter implements ItemWriter<L2AutoIncrReaderDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(L2AutoIncrWriter.class);

	@Autowired
	private PayrpfDAO payrpfDAO;

	@Autowired
	private PtrnpfDAO ptrnpfDAO;

	@Autowired
	private CovrpfDAO covrpfDAO;

	@Autowired
	private IncrpfDAO incrpfDAO;

	@Autowired
	private AinrpfDAO ainrpfDAO;

	@Autowired
	private ChdrpfDAO chdrpfDAO;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	private ItemWriter<L2AutoIncrReaderDTO> covrpfInsertWriter;
	
	@Autowired
	public Sftlock sftlock;

	@Override
	public void write(List<? extends L2AutoIncrReaderDTO> items) throws Exception {
		LOGGER.info("L2AutoIncrWriter Starts");
		List<L2AutoIncrReaderDTO> readerDTOModifiedList = new ArrayList<>();

		for (L2AutoIncrReaderDTO reader : items) {
			if (null != reader.getCovrpfInsert()) {
				readerDTOModifiedList.add(reader); 
			}
		}
		if (!readerDTOModifiedList.isEmpty()) {
			covrpfInsertWriter.write(readerDTOModifiedList);
		}
		processOtherData(items);
		
		if(!items.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < items.size(); i++) {
				entityList.add(items.get(i).getChdrNum());
			}			
			
			sftlock.unlockByList("CH", items.get(0).getChdrCoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		
		LOGGER.info("L2AUTOINCR batch ends.");

	}

	private void processOtherData(List<? extends L2AutoIncrReaderDTO> readerDTOList) throws ParseException {
		List<Ptrnpf> ptrnpfInsertList = new ArrayList<>();
		List<Covrpf> covrpfUpdateList = new ArrayList<>();
		List<Payrpf> payrpfUpdateList = new ArrayList<>();
		List<Incrpf> incrpfInsertList = new ArrayList<>();
		List<Ainrpf> ainrpfInsertList = new ArrayList<>();
		List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
		String chdrnum = "";

		if (!readerDTOList.isEmpty()) {
			for (L2AutoIncrReaderDTO dto : readerDTOList) {
				if (null != dto.getPtrnpfInsertList() && !dto.getPtrnpfInsertList().isEmpty()) {
					ptrnpfInsertList.addAll(dto.getPtrnpfInsertList());
					dto.getPtrnpfInsertList().clear();
				}
				if (!chdrnum.equals(dto.getChdrNum()) && null != dto.getCovrpfUpdate()) {
					covrpfUpdateList.add(dto.getCovrpfUpdate());
					dto.setCovrpfUpdate(null);
				}
				if (!chdrnum.equals(dto.getChdrNum()) && null != dto.getPayrpfUpdate()) {
					payrpfUpdateList.add(dto.getPayrpfUpdate());
					dto.setPayrpfUpdate(null);
				}
				if (null != dto.getIncrpfInsert()) {
					incrpfInsertList.add(dto.getIncrpfInsert());
					dto.setIncrpfInsert(null);
				}
				if (null != dto.getAinrpfInsert()) {
					ainrpfInsertList.add(dto.getAinrpfInsert());
					dto.setAinrpfInsert(null);
				}
				if (!chdrnum.equals(dto.getChdrNum()) && null != dto.getChdrpfUpdate()) {
					chdrpfUpdateList.add(dto.getChdrpfUpdate());
					dto.setChdrpfUpdate(null);
				}
				chdrnum = dto.getChdrNum();
			}

			if (!ptrnpfInsertList.isEmpty()) {
				ptrnpfDAO.insertPtrnpfRecords(ptrnpfInsertList);
			}

			if (!covrpfUpdateList.isEmpty()) {
				covrpfDAO.updateCovrpfRecords(covrpfUpdateList);
			}

			if (!payrpfUpdateList.isEmpty()) {
				payrpfDAO.updatePayrpfRecords(payrpfUpdateList);
			}
			if (!incrpfInsertList.isEmpty()) {
				incrpfDAO.insertIncrpfRecords(incrpfInsertList);
			}

			if (!ainrpfInsertList.isEmpty()) {
				ainrpfDAO.insertAinrpfRecords(ainrpfInsertList);
			}

			if (!incrpfInsertList.isEmpty()) {
				incrpfDAO.insertIncrpfRecords(incrpfInsertList);
			}

			if (!chdrpfUpdateList.isEmpty()) {
				chdrpfDAO.updateChdrpfRecords(chdrpfUpdateList);
			}
		}
	}

	public void setCovrpfInsertWriter(ItemWriter<L2AutoIncrReaderDTO> covrpfInsertWriter) {
		this.covrpfInsertWriter = covrpfInsertWriter;
	}

}
