package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5004 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String chdrnum;
	private String ownersel;
	private String occdateDisp;
	private String hpropdteDisp;
	private String billfreq;
	private String indxflg;
	private String nlgflg;
	private String hprrcvdtDisp;
	private String mop;
	private String incomeSeqNo;
	private String huwdcdteDisp;
	private String billcdDisp;
	private String plansuff;
	private String cntcurr;
	private String billcurr;
	private String znfopt;
	private String register;
	private String srcebus;
	private String dlvrmode;
	private String reptype;
	private String lrepnum;
	private String agntsel;
	private String bnkout;
	private String bnktel;
	private String campaign;
	private String stcal;
	private String stcbl;
	private String stccl;
	private String stcdl;
	private String stcel;
	private String jownind;
	private String asgnind;
	private String apcind;
	private String addtype;
	private String payind;
	private String comind;
	private String crcind;
	private String ddind;
	private String fupflg;
	private String bnfying;
	private String grpind;
	private String aiind;
	private String zsredtrm;
	private String ttmprcno;
	private String ttmprcdteDisp;
	private String transhist;
	private String ctrsind;
	private String ind;
	private String reqntype;
	private String zdpind;
	private String zdcind;
	private String fatcastatusdesc;
	private String zctaxind;
	private String schmno;
	private String billday;
	private String zroloverind;
	private String refundOverpay;
	private String zmultOwner;
	private String custrefnum;
	private String concommflg;
	private String rskcommdateDisp;
	private String decldateDisp;
	private String premRcptDateDisp;
	private String cnfirmtnIntentDateDisp;
	private String ownerOccupation;
	private String activeField;
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getOwnersel() {
		return ownersel;
	}
	public void setOwnersel(String ownersel) {
		this.ownersel = ownersel;
	}
	public String getOccdateDisp() {
		return occdateDisp;
	}
	public void setOccdateDisp(String occdateDisp) {
		this.occdateDisp = occdateDisp;
	}
	public String getHpropdteDisp() {
		return hpropdteDisp;
	}
	public void setHpropdteDisp(String hpropdteDisp) {
		this.hpropdteDisp = hpropdteDisp;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getIndxflg() {
		return indxflg;
	}
	public void setIndxflg(String indxflg) {
		this.indxflg = indxflg;
	}
	public String getNlgflg() {
		return nlgflg;
	}
	public void setNlgflg(String nlgflg) {
		this.nlgflg = nlgflg;
	}
	public String getHprrcvdtDisp() {
		return hprrcvdtDisp;
	}
	public void setHprrcvdtDisp(String hprrcvdtDisp) {
		this.hprrcvdtDisp = hprrcvdtDisp;
	}
	public String getMop() {
		return mop;
	}
	public void setMop(String mop) {
		this.mop = mop;
	}
	public String getIncomeSeqNo() {
		return incomeSeqNo;
	}
	public void setIncomeSeqNo(String incomeSeqNo) {
		this.incomeSeqNo = incomeSeqNo;
	}
	public String getHuwdcdteDisp() {
		return huwdcdteDisp;
	}
	public void setHuwdcdteDisp(String huwdcdteDisp) {
		this.huwdcdteDisp = huwdcdteDisp;
	}
	public String getBillcdDisp() {
		return billcdDisp;
	}
	public void setBillcdDisp(String billcdDisp) {
		this.billcdDisp = billcdDisp;
	}
	public String getPlansuff() {
		return plansuff;
	}
	public void setPlansuff(String plansuff) {
		this.plansuff = plansuff;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}
	public String getZnfopt() {
		return znfopt;
	}
	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public String getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(String srcebus) {
		this.srcebus = srcebus;
	}
	public String getDlvrmode() {
		return dlvrmode;
	}
	public void setDlvrmode(String dlvrmode) {
		this.dlvrmode = dlvrmode;
	}
	public String getReptype() {
		return reptype;
	}
	public void setReptype(String reptype) {
		this.reptype = reptype;
	}
	public String getLrepnum() {
		return lrepnum;
	}
	public void setLrepnum(String lrepnum) {
		this.lrepnum = lrepnum;
	}
	public String getAgntsel() {
		return agntsel;
	}
	public void setAgntsel(String agntsel) {
		this.agntsel = agntsel;
	}
	public String getBnkout() {
		return bnkout;
	}
	public void setBnkout(String bnkout) {
		this.bnkout = bnkout;
	}
	public String getBnktel() {
		return bnktel;
	}
	public void setBnktel(String bnktel) {
		this.bnktel = bnktel;
	}
	public String getCampaign() {
		return campaign;
	}
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}
	public String getStcal() {
		return stcal;
	}
	public void setStcal(String stcal) {
		this.stcal = stcal;
	}
	public String getStcbl() {
		return stcbl;
	}
	public void setStcbl(String stcbl) {
		this.stcbl = stcbl;
	}
	public String getStccl() {
		return stccl;
	}
	public void setStccl(String stccl) {
		this.stccl = stccl;
	}
	public String getStcdl() {
		return stcdl;
	}
	public void setStcdl(String stcdl) {
		this.stcdl = stcdl;
	}
	public String getStcel() {
		return stcel;
	}
	public void setStcel(String stcel) {
		this.stcel = stcel;
	}
	public String getJownind() {
		return jownind;
	}
	public void setJownind(String jownind) {
		this.jownind = jownind;
	}
	public String getAsgnind() {
		return asgnind;
	}
	public void setAsgnind(String asgnind) {
		this.asgnind = asgnind;
	}
	public String getApcind() {
		return apcind;
	}
	public void setApcind(String apcind) {
		this.apcind = apcind;
	}
	public String getAddtype() {
		return addtype;
	}
	public void setAddtype(String addtype) {
		this.addtype = addtype;
	}
	public String getPayind() {
		return payind;
	}
	public void setPayind(String payind) {
		this.payind = payind;
	}
	public String getComind() {
		return comind;
	}
	public void setComind(String comind) {
		this.comind = comind;
	}
	public String getCrcind() {
		return crcind;
	}
	public void setCrcind(String crcind) {
		this.crcind = crcind;
	}
	public String getDdind() {
		return ddind;
	}
	public void setDdind(String ddind) {
		this.ddind = ddind;
	}
	public String getFupflg() {
		return fupflg;
	}
	public void setFupflg(String fupflg) {
		this.fupflg = fupflg;
	}
	public String getBnfying() {
		return bnfying;
	}
	public void setBnfying(String bnfying) {
		this.bnfying = bnfying;
	}
	public String getGrpind() {
		return grpind;
	}
	public void setGrpind(String grpind) {
		this.grpind = grpind;
	}
	public String getAiind() {
		return aiind;
	}
	public void setAiind(String aiind) {
		this.aiind = aiind;
	}
	public String getZsredtrm() {
		return zsredtrm;
	}
	public void setZsredtrm(String zsredtrm) {
		this.zsredtrm = zsredtrm;
	}
	public String getTtmprcno() {
		return ttmprcno;
	}
	public void setTtmprcno(String ttmprcno) {
		this.ttmprcno = ttmprcno;
	}
	public String getTtmprcdteDisp() {
		return ttmprcdteDisp;
	}
	public void setTtmprcdteDisp(String ttmprcdteDisp) {
		this.ttmprcdteDisp = ttmprcdteDisp;
	}
	public String getTranshist() {
		return transhist;
	}
	public void setTranshist(String transhist) {
		this.transhist = transhist;
	}
	public String getCtrsind() {
		return ctrsind;
	}
	public void setCtrsind(String ctrsind) {
		this.ctrsind = ctrsind;
	}
	public String getInd() {
		return ind;
	}
	public void setInd(String ind) {
		this.ind = ind;
	}
	public String getReqntype() {
		return reqntype;
	}
	public void setReqntype(String reqntype) {
		this.reqntype = reqntype;
	}
	public String getZdpind() {
		return zdpind;
	}
	public void setZdpind(String zdpind) {
		this.zdpind = zdpind;
	}
	public String getZdcind() {
		return zdcind;
	}
	public void setZdcind(String zdcind) {
		this.zdcind = zdcind;
	}
	public String getFatcastatusdesc() {
		return fatcastatusdesc;
	}
	public void setFatcastatusdesc(String fatcastatusdesc) {
		this.fatcastatusdesc = fatcastatusdesc;
	}
	public String getZctaxind() {
		return zctaxind;
	}
	public void setZctaxind(String zctaxind) {
		this.zctaxind = zctaxind;
	}
	public String getSchmno() {
		return schmno;
	}
	public void setSchmno(String schmno) {
		this.schmno = schmno;
	}
	public String getBillday() {
		return billday;
	}
	public void setBillday(String billday) {
		this.billday = billday;
	}
	public String getZroloverind() {
		return zroloverind;
	}
	public void setZroloverind(String zroloverind) {
		this.zroloverind = zroloverind;
	}
	public String getRefundOverpay() {
		return refundOverpay;
	}
	public void setRefundOverpay(String refundOverpay) {
		this.refundOverpay = refundOverpay;
	}
	public String getZmultOwner() {
		return zmultOwner;
	}
	public void setZmultOwner(String zmultOwner) {
		this.zmultOwner = zmultOwner;
	}
	public String getCustrefnum() {
		return custrefnum;
	}
	public void setCustrefnum(String custrefnum) {
		this.custrefnum = custrefnum;
	}
	public String getConcommflg() {
		return concommflg;
	}
	public void setConcommflg(String concommflg) {
		this.concommflg = concommflg;
	}
	public String getRskcommdateDisp() {
		return rskcommdateDisp;
	}
	public void setRskcommdateDisp(String rskcommdateDisp) {
		this.rskcommdateDisp = rskcommdateDisp;
	}
	public String getDecldateDisp() {
		return decldateDisp;
	}
	public void setDecldateDisp(String decldateDisp) {
		this.decldateDisp = decldateDisp;
	}
	public String getPremRcptDateDisp() {
		return premRcptDateDisp;
	}
	public void setPremRcptDateDisp(String premRcptDateDisp) {
		this.premRcptDateDisp = premRcptDateDisp;
	}
	public String getCnfirmtnIntentDateDisp() {
		return cnfirmtnIntentDateDisp;
	}
	public void setCnfirmtnIntentDateDisp(String cnfirmtnIntentDateDisp) {
		this.cnfirmtnIntentDateDisp = cnfirmtnIntentDateDisp;
	}
	public String getOwnerOccupation() {
		return ownerOccupation;
	}
	public void setOwnerOccupation(String ownerOccupation) {
		this.ownerOccupation = ownerOccupation;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5004 []";
	}
	
}
