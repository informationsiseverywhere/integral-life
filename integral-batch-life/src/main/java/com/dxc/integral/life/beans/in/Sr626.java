package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class Sr626 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private Sr626screensfl sr626screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public Sr626screensfl getSr626screensfl() {
		return sr626screensfl;
	}
	public void setSr626screensfl(Sr626screensfl sr626screensfl) {
		this.sr626screensfl = sr626screensfl;
	}
	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Sr626screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;

		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
	
		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String clntnum;
			private String textone;
			private String reasoncd ;
			private String datefrmDisp;
			private String datetoDisp;

			public String getClntnum() {
				return clntnum;
			}
			public void setClntnum(String clntnum) {
				this.clntnum = clntnum;
			}
			public String getTextone() {
				return textone;
			}
			public void setTextone(String textone) {
				this.textone = textone;
			}
			public String getReasoncd() {
				return reasoncd;
			}
			public void setReasoncd(String reasoncd) {
				this.reasoncd = reasoncd;
			}
			public String getDatefrmDisp() {
				return datefrmDisp;
			}
			public void setDatefrmDisp(String datefrmDisp) {
				this.datefrmDisp = datefrmDisp;
			}
			public String getDatetoDisp() {
				return datetoDisp;
			}
			public void setDatetoDisp(String datetoDisp) {
				this.datetoDisp = datetoDisp;
			}
		} 
	}
	@Override
	public String toString() {
		return "Sr626 []";
	}
	
}
