package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2anendrlxReaderDTO;

/**
 * RowMapper for L2anendrlxReaderDTO.
 * 
 * @author mmalik25
 */
public class L2anendrlxReaderRowMapper implements RowMapper<L2anendrlxReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2anendrlxReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2anendrlxReaderDTO l2anendrlxReader = new L2anendrlxReaderDTO();

		l2anendrlxReader.setPolicyTranno(rs.getInt("policyTranno"));
		l2anendrlxReader.setPolicyCurr(rs.getString("policyCurr"));
		l2anendrlxReader.setPolicyType(rs.getString("policyType"));
		l2anendrlxReader.setPolicyBillCurr(rs.getString("policyBillCurr"));
		l2anendrlxReader.setPolicyOccdate(rs.getInt("policyOccdate"));
		l2anendrlxReader.setStatcode(rs.getString("statcode"));
		l2anendrlxReader.setPstatcode(rs.getString("pstatcode"));
		l2anendrlxReader.setSumins(rs.getBigDecimal("sumins"));
		l2anendrlxReader.setUniqueNumber(rs.getInt("unique_number"));
		l2anendrlxReader.setChdrcoy(rs.getString("chdrcoy"));
		l2anendrlxReader.setLife(rs.getString("life"));
		l2anendrlxReader.setChdrnum(rs.getString("chdrnum"));
		l2anendrlxReader.setCoverage(rs.getString("coverage"));
		l2anendrlxReader.setNpaydate(rs.getInt("npaydate"));
		l2anendrlxReader.setPlnsfx(rs.getInt("plnsfx"));
		l2anendrlxReader.setRider(rs.getString("rider"));
		l2anendrlxReader.setValidflag(rs.getString("validflag"));
		l2anendrlxReader.setTranno(rs.getInt("tranno"));
		l2anendrlxReader.setCurrfrom(rs.getInt("currfrom"));
		l2anendrlxReader.setCurrto(rs.getInt("currto"));
		l2anendrlxReader.setZrduedte01(rs.getInt("zrduedte01"));
		l2anendrlxReader.setZrduedte02(rs.getInt("zrduedte02"));
		l2anendrlxReader.setZrduedte03(rs.getInt("zrduedte03"));
		l2anendrlxReader.setZrduedte04(rs.getInt("zrduedte04"));
		l2anendrlxReader.setZrduedte05(rs.getInt("zrduedte05"));
		l2anendrlxReader.setZrduedte06(rs.getInt("zrduedte06"));
		l2anendrlxReader.setZrduedte07(rs.getInt("zrduedte07"));
		l2anendrlxReader.setZrduedte08(rs.getInt("zrduedte08"));
		l2anendrlxReader.setPrcnt01(rs.getBigDecimal("prcnt01"));
		l2anendrlxReader.setPrcnt02(rs.getBigDecimal("prcnt02"));
		l2anendrlxReader.setPrcnt03(rs.getBigDecimal("prcnt03"));
		l2anendrlxReader.setPrcnt04(rs.getBigDecimal("prcnt04"));
		l2anendrlxReader.setPrcnt05(rs.getBigDecimal("prcnt05"));
		l2anendrlxReader.setPrcnt06(rs.getBigDecimal("prcnt06"));
		l2anendrlxReader.setPrcnt07(rs.getBigDecimal("prcnt07"));
		l2anendrlxReader.setPrcnt08(rs.getBigDecimal("prcnt08"));
		l2anendrlxReader.setPaydte01(rs.getInt("paydte01"));
		l2anendrlxReader.setPaydte02(rs.getInt("paydte02"));
		l2anendrlxReader.setPaydte03(rs.getInt("paydte03"));
		l2anendrlxReader.setPaydte04(rs.getInt("paydte04"));
		l2anendrlxReader.setPaydte05(rs.getInt("paydte05"));
		l2anendrlxReader.setPaydte06(rs.getInt("paydte06"));
		l2anendrlxReader.setPaydte07(rs.getInt("paydte07"));
		l2anendrlxReader.setPaydte08(rs.getInt("paydte08"));
		l2anendrlxReader.setPaid01(rs.getBigDecimal("paid01"));
		l2anendrlxReader.setPaid02(rs.getBigDecimal("paid02"));
		l2anendrlxReader.setPaid03(rs.getBigDecimal("paid03"));
		l2anendrlxReader.setPaid04(rs.getBigDecimal("paid04"));
		l2anendrlxReader.setPaid05(rs.getBigDecimal("paid05"));
		l2anendrlxReader.setPaid06(rs.getBigDecimal("paid06"));
		l2anendrlxReader.setPaid07(rs.getBigDecimal("paid07"));
		l2anendrlxReader.setPaid08(rs.getBigDecimal("paid08"));
		l2anendrlxReader.setPaymmeth01(rs.getString("paymmeth01"));
		l2anendrlxReader.setPaymmeth02(rs.getString("paymmeth02"));
		l2anendrlxReader.setPaymmeth03(rs.getString("paymmeth03"));
		l2anendrlxReader.setPaymmeth04(rs.getString("paymmeth04"));
		l2anendrlxReader.setPaymmeth05(rs.getString("paymmeth05"));
		l2anendrlxReader.setPaymmeth06(rs.getString("paymmeth06"));
		l2anendrlxReader.setPaymmeth07(rs.getString("paymmeth07"));
		l2anendrlxReader.setPaymmeth08(rs.getString("paymmeth08"));
		l2anendrlxReader.setZrpayopt01(rs.getString("zrpayopt01"));
		l2anendrlxReader.setZrpayopt02(rs.getString("zrpayopt02"));
		l2anendrlxReader.setZrpayopt03(rs.getString("zrpayopt03"));
		l2anendrlxReader.setZrpayopt04(rs.getString("zrpayopt04"));
		l2anendrlxReader.setZrpayopt05(rs.getString("zrpayopt05"));
		l2anendrlxReader.setZrpayopt06(rs.getString("zrpayopt06"));
		l2anendrlxReader.setZrpayopt07(rs.getString("zrpayopt07"));
		l2anendrlxReader.setZrpayopt08(rs.getString("zrpayopt08"));
		l2anendrlxReader.setTotamnt(rs.getBigDecimal("totamnt"));
		l2anendrlxReader.setNpaydate(rs.getInt("npaydate"));
		l2anendrlxReader.setPayclt(rs.getString("payclt"));
		l2anendrlxReader.setBankkey(rs.getString("bankkey"));
		l2anendrlxReader.setBankacckey(rs.getString("bankacckey"));
		l2anendrlxReader.setPaycurr(rs.getString("paycurr"));
		l2anendrlxReader.setUsrprf(rs.getString("usrprf"));
		l2anendrlxReader.setJobnm(rs.getString("jobnm"));
		l2anendrlxReader.setDatime(rs.getTimestamp("datime"));
		l2anendrlxReader.setPlansuffix(rs.getInt("plansuffix"));
		l2anendrlxReader.setCownCoy(rs.getString("cownCoy"));  /*IJTI-379*/
		return l2anendrlxReader;
	}

}