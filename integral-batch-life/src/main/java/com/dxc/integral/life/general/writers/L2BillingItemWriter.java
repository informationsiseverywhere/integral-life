package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2BillingReaderDTO;
import com.dxc.integral.life.dao.FpcopfDAO;
import com.dxc.integral.life.dao.FprmpfDAO;
import com.dxc.integral.life.dao.LinspfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.TaxdpfDAO;

/**
 * ItemWriter for L2NEWINTCP batch step.
 * 
 * @author wli31
 *
 */
@Async
public class L2BillingItemWriter implements ItemWriter<L2BillingReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2BillingItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;
	@Autowired
	private TaxdpfDAO taxdpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	@Autowired
	private LinspfDAO linspfDAO;
	@Autowired
	private FpcopfDAO fpcopfDAO;
	@Autowired
	private FprmpfDAO fprmpfDAO;
	
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;	
	
	private ItemWriter<L2BillingReaderDTO> chdrpfUpdateWriter;
    private ItemWriter<L2BillingReaderDTO> payrpfUpdateWriter;    
    
    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
	@Override
	public void write(List<? extends L2BillingReaderDTO> readerDTOList) throws Exception {
		
		LOGGER.info("L2polrnwlBillingWrite starts.");
		Integer tranDate = Integer.parseInt(transactionDate);
		
		List<L2BillingReaderDTO> readerDTOModifiedList;
		readerDTOModifiedList = new ArrayList<L2BillingReaderDTO>();
		for (int x = 0; x < readerDTOList.size(); x++) {
			L2BillingReaderDTO readerDTOModified = null;
			if (!("Y".equals(readerDTOList.get(x).getBillsupr()) || readerDTOList.get(x).getBillcd() >= readerDTOList.get(x).getBillspfrom()
				&& readerDTOList.get(x).getBillcd() <= readerDTOList.get(x).getBillspto() && tranDate<= readerDTOList.get(x).getBillspto())) {
				readerDTOModified = readerDTOList.get(x);
				readerDTOModifiedList.add(readerDTOModified);
			}	
		}
		if (!readerDTOModifiedList.isEmpty()) {
			processOtherData(readerDTOModifiedList);

			chdrpfUpdateWriter.write(readerDTOModifiedList);
			payrpfUpdateWriter.write(readerDTOModifiedList);
		

		}
		
		if(!readerDTOList.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOList.size(); i++) {
				entityList.add(readerDTOList.get(i).getChdrnum());
			}			
			
			sftlock.unlockByList("CH", readerDTOList.get(0).getChdrcoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2newintcpItemWriter ends.");
	}

	private void processOtherData(List<? extends L2BillingReaderDTO> readerDTOList) throws ParseException {

		if (!readerDTOList.isEmpty()) {
			for(L2BillingReaderDTO dto : readerDTOList ){
				if(null != dto.getTaxdpfs() && dto.getTaxdpfs().size() > 0){
					taxdpfDAO.insertTaxdpfRecords(dto.getTaxdpfs());
				}
				if(null != dto.getPtrnpfList() && dto.getPtrnpfList().size() > 0){
					ptrnpfDAO.insertPtrnpfRecords(dto.getPtrnpfList());
				}
				if(null != dto.getLinspfList() && dto.getLinspfList().size() > 0){
					linspfDAO.insertLinspfRecords(dto.getLinspfList());
				}
				if(null != dto.getFpcopfList() && dto.getFpcopfList().size() > 0){
					fpcopfDAO.updateFpcopfRecords(dto.getFpcopfList());
				}
				if(null != dto.getFpcopfList() && dto.getFprmpfList().size() > 0){
					fprmpfDAO.updateFprmpfRecords(dto.getFprmpfList());
				}
				if(null != dto.getHdivpfList() && dto.getHdivpfList().size() > 0 ){
					fprmpfDAO.updateFprmpfRecords(dto.getFprmpfList());
				}
			}
		}
	}
//	private void processPtrnpf(List<? extends L2BillingReaderDTO> readerDTOList) throws ParseException {
//		if (!readerDTOList.isEmpty()) {
//			for(L2BillingReaderDTO dto : readerDTOList ){
//				if(null != dto.getPtrnpfList()){
//					ptrnpfDAO.insertPtrnpfRecords(dto.getPtrnpfList());
//				}
//			}
//		}
//	}
//	private void processLinspf(List<? extends L2BillingReaderDTO> readerDTOList) throws ParseException {
//		if (!readerDTOList.isEmpty()) {
//			for(L2BillingReaderDTO dto : readerDTOList ){
//				if(null != dto.getLinspfList()){
//					linspfDAO.insertLinspfRecords(dto.getLinspfList());
//				}
//			}
//		}
//	}
//	private void processFpcopf(List<? extends L2BillingReaderDTO> readerDTOList) throws ParseException {
//		if (!readerDTOList.isEmpty()) {
//			for(L2BillingReaderDTO dto : readerDTOList ){
//				if(null != dto.getFpcopfList()){
//					fpcopfDAO.updateFpcopfRecords(dto.getFpcopfList());
//				}
//			}
//		}
//	}
//	private void processFprmpf(List<? extends L2BillingReaderDTO> readerDTOList) throws ParseException {
//		if (!readerDTOList.isEmpty()) {
//			for(L2BillingReaderDTO dto : readerDTOList ){
//				if(null != dto.getFpcopfList()){
//					fprmpfDAO.updateFprmpfRecords(dto.getFprmpfList());
//				}
//			}
//		}
//	}
	
	public void setChdrpfUpdateWriter(ItemWriter<L2BillingReaderDTO> chdrpfUpdateWriter) {
		this.chdrpfUpdateWriter = chdrpfUpdateWriter;
	}

	public void setPayrpfUpdateWriter(ItemWriter<L2BillingReaderDTO> payrpfUpdateWriter) {
		this.payrpfUpdateWriter = payrpfUpdateWriter;
	}
   
}
