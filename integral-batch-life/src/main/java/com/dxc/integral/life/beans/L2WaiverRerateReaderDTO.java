package com.dxc.integral.life.beans;

import java.math.BigDecimal;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Regppf;

public class L2WaiverRerateReaderDTO {
	
	private long uniqueNumber;
	private String chdrCoy;
	private String life;
	private String chdrNum;
	private String jLife;
	private String coverage;
	private String rider;
	private Integer plnSfx;
	private String validFlag;
	private Integer tranNo;
	private String batcBrn;
	private String termId;
	private Integer batcActyr;
	private Integer batcActmn;
	private String batcTrcde;
	private String batcBatch;
	private String batcPfx;
	private String batcCoy;
	private String effectiveDate;
	private Integer trdt;
	private Integer trtm;
	private String cntType;
	private int billcd;
	private String statCode;
	private String pstatCode;
	private String reg;
	private int rrtDat;
	private int currTo;
	private int currFrom;
	private int crrcd;
	private int anbccd;
	private int rcesdte;
	private int pcesdte;
	private int bcesdte;
	private String crtable;
	private int nxtdte;
	private int rcesage;
	private int bcesage;
	private int pcesage;
	private int bcestrm;
	private int rcestrm;
	private int pcestrm;
	private int rrtfrm;
	private String prmcur;
	private BigDecimal sumIns;
	private String mortCls;
	private String liencd;
	private int cpiDte;
	private BigDecimal zstpDuty01;
	private String zclState;
	private String lnkgNo;
	private String lnkgsubrefno;
	private String tpdType;
	private String lnkgInd;
	private String singPremType;
	private BigDecimal zlinstPrem;
	private BigDecimal zinstPrem;
	private BigDecimal zbinstPrem;
	private Agcmpf agcmpfUpdate;
	private Agcmpf agcmpfInsert;
	private Ptrnpf ptrnpfInsert;
	private Chdrpf chdrpfUpdate;
	private Payrpf payrpfUpdate;
	private Chdrpf chdrpfInsert;
	private Incrpf incrpfInsert;
	private Covrpf covrpfInsert;
	private Covrpf covrpfUpdate;
	private Payrpf payrpfInsert;
	private Regppf updateRegppf;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrCoy() {
		return chdrCoy;
	}
	public void setChdrCoy(String chdrCoy) {
		this.chdrCoy = chdrCoy;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getjLife() {
		return jLife;
	}
	public void setjLife(String jLife) {
		this.jLife = jLife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnSfx() {
		return plnSfx;
	}
	public void setPlnSfx(Integer plnSfx) {
		this.plnSfx = plnSfx;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public Integer getTranNo() {
		return tranNo;
	}
	public void setTranNo(Integer tranNo) {
		this.tranNo = tranNo;
	}
	public String getBatcBrn() {
		return batcBrn;
	}
	public void setBatcBrn(String batcBrn) {
		this.batcBrn = batcBrn;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public Integer getBatcActyr() {
		return batcActyr;
	}
	public void setBatcActyr(Integer batcActyr) {
		this.batcActyr = batcActyr;
	}
	public Integer getBatcActmn() {
		return batcActmn;
	}
	public void setBatcActmn(Integer batcActmn) {
		this.batcActmn = batcActmn;
	}
	public String getBatcTrcde() {
		return batcTrcde;
	}
	public void setBatcTrcde(String batcTrcde) {
		this.batcTrcde = batcTrcde;
	}
	public String getBatcBatch() {
		return batcBatch;
	}
	public void setBatcBatch(String batcBatch) {
		this.batcBatch = batcBatch;
	}
	public String getBatcPfx() {
		return batcPfx;
	}
	public void setBatcPfx(String batcPfx) {
		this.batcPfx = batcPfx;
	}
	public String getBatcCoy() {
		return batcCoy;
	}
	public void setBatcCoy(String batcCoy) {
		this.batcCoy = batcCoy;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public String getCntType() {
		return cntType;
	}
	public void setCntType(String cntType) {
		this.cntType = cntType;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getStatCode() {
		return statCode;
	}
	public void setStatCode(String statCode) {
		this.statCode = statCode;
	}
	public String getPstatCode() {
		return pstatCode;
	}
	public void setPstatCode(String pstatCode) {
		this.pstatCode = pstatCode;
	}
	public String getReg() {
		return reg;
	}
	public void setReg(String reg) {
		this.reg = reg;
	}
	public int getRrtDat() {
		return rrtDat;
	}
	public void setRrtDat(int rrtDat) {
		this.rrtDat = rrtDat;
	}
	public int getCurrTo() {
		return currTo;
	}
	public void setCurrTo(int currTo) {
		this.currTo = currTo;
	}
	public int getCurrFrom() {
		return currFrom;
	}
	public void setCurrFrom(int currFrom) {
		this.currFrom = currFrom;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public int getAnbccd() {
		return anbccd;
	}
	public void setAnbccd(int anbccd) {
		this.anbccd = anbccd;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public int getPcesdte() {
		return pcesdte;
	}
	public void setPcesdte(int pcesdte) {
		this.pcesdte = pcesdte;
	}
	public int getBcesdte() {
		return bcesdte;
	}
	public void setBcesdte(int bcesdte) {
		this.bcesdte = bcesdte;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getNxtdte() {
		return nxtdte;
	}
	public void setNxtdte(int nxtdte) {
		this.nxtdte = nxtdte;
	}
	public int getRcesage() {
		return rcesage;
	}
	public void setRcesage(int rcesage) {
		this.rcesage = rcesage;
	}
	public int getBcesage() {
		return bcesage;
	}
	public void setBcesage(int bcesage) {
		this.bcesage = bcesage;
	}
	public int getPcesage() {
		return pcesage;
	}
	public void setPcesage(int pcesage) {
		this.pcesage = pcesage;
	}
	public int getBcestrm() {
		return bcestrm;
	}
	public void setBcestrm(int bcestrm) {
		this.bcestrm = bcestrm;
	}
	public int getRcestrm() {
		return rcestrm;
	}
	public void setRcestrm(int rcestrm) {
		this.rcestrm = rcestrm;
	}
	public int getPcestrm() {
		return pcestrm;
	}
	public void setPcestrm(int pcestrm) {
		this.pcestrm = pcestrm;
	}
	public String getPrmcur() {
		return prmcur;
	}
	public void setPrmcur(String prmcur) {
		this.prmcur = prmcur;
	}
	public BigDecimal getSumIns() {
		return sumIns;
	}
	public void setSumIns(BigDecimal sumIns) {
		this.sumIns = sumIns;
	}
	public String getMortCls() {
		return mortCls;
	}
	public void setMortCls(String mortCls) {
		this.mortCls = mortCls;
	}
	public String getLiencd() {
		return liencd;
	}
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	public int getCpiDte() {
		return cpiDte;
	}
	public void setCpiDte(int cpiDte) {
		this.cpiDte = cpiDte;
	}
	public BigDecimal getZstpDuty01() {
		return zstpDuty01;
	}
	public void setZstpDuty01(BigDecimal zstpDuty01) {
		this.zstpDuty01 = zstpDuty01;
	}
	public String getZclState() {
		return zclState;
	}
	public void setZclState(String zclState) {
		this.zclState = zclState;
	}
	public String getLnkgNo() {
		return lnkgNo;
	}
	public void setLnkgNo(String lnkgNo) {
		this.lnkgNo = lnkgNo;
	}
	public String getLnkgsubrefno() {
		return lnkgsubrefno;
	}
	public void setLnkgsubrefno(String lnkgsubrefno) {
		this.lnkgsubrefno = lnkgsubrefno;
	}
	public String getTpdType() {
		return tpdType;
	}
	public void setTpdType(String tpdType) {
		this.tpdType = tpdType;
	}
	public String getLnkgInd() {
		return lnkgInd;
	}
	public void setLnkgInd(String lnkgInd) {
		this.lnkgInd = lnkgInd;
	}
	public String getSingPremType() {
		return singPremType;
	}
	public void setSingPremType(String singPremType) {
		this.singPremType = singPremType;
	}
	public BigDecimal getZlinstPrem() {
		return zlinstPrem;
	}
	public void setZlinstPrem(BigDecimal zlinstPrem) {
		this.zlinstPrem = zlinstPrem;
	}
	public BigDecimal getZinstPrem() {
		return zinstPrem;
	}
	public void setZinstPrem(BigDecimal zinstPrem) {
		this.zinstPrem = zinstPrem;
	}
	public BigDecimal getZbinstPrem() {
		return zbinstPrem;
	}
	public void setZbinstPrem(BigDecimal zbinstPrem) {
		this.zbinstPrem = zbinstPrem;
	}
	public Agcmpf getAgcmpfUpdate() {
		return agcmpfUpdate;
	}
	public void setAgcmpfUpdate(Agcmpf agcmpfUpdate) {
		this.agcmpfUpdate = agcmpfUpdate;
	}
	public Agcmpf getAgcmpfInsert() {
		return agcmpfInsert;
	}
	public void setAgcmpfInsert(Agcmpf agcmpfInsert) {
		this.agcmpfInsert = agcmpfInsert;
	}
	public Ptrnpf getPtrnpfInsert() {
		return ptrnpfInsert;
	}
	public void setPtrnpfInsert(Ptrnpf ptrnpfInsert) {
		this.ptrnpfInsert = ptrnpfInsert;
	}
	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}
	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}
	public Payrpf getPayrpfUpdate() {
		return payrpfUpdate;
	}
	public void setPayrpfUpdate(Payrpf payrpfUpdate) {
		this.payrpfUpdate = payrpfUpdate;
	}
	public Chdrpf getChdrpfInsert() {
		return chdrpfInsert;
	}
	public void setChdrpfInsert(Chdrpf chdrpfInsert) {
		this.chdrpfInsert = chdrpfInsert;
	}
	public Incrpf getIncrpfInsert() {
		return incrpfInsert;
	}
	public void setIncrpfInsert(Incrpf incrpfInsert) {
		this.incrpfInsert = incrpfInsert;
	}
	public Covrpf getCovrpfInsert() {
		return covrpfInsert;
	}
	public void setCovrpfInsert(Covrpf covrpfInsert) {
		this.covrpfInsert = covrpfInsert;
	}
	public Covrpf getCovrpfUpdate() {
		return covrpfUpdate;
	}
	public void setCovrpfUpdate(Covrpf covrpfUpdate) {
		this.covrpfUpdate = covrpfUpdate;
	}
	public Payrpf getPayrpfInsert() {
		return payrpfInsert;
	}
	public void setPayrpfInsert(Payrpf payrpfInsert) {
		this.payrpfInsert = payrpfInsert;
	}
	public Regppf getUpdateRegppf() {
		return updateRegppf;
	}
	public void setUpdateRegppf(Regppf updateRegppf) {
		this.updateRegppf = updateRegppf;
	}
	public int getRrtfrm() {
		return rrtfrm;
	}
	public void setRrtfrm(int rrtfrm) {
		this.rrtfrm = rrtfrm;
	}
	
}