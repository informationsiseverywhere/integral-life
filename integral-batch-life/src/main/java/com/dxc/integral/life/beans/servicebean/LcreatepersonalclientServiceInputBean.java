package com.dxc.integral.life.beans.servicebean;

import java.io.Serializable;
import java.util.List;
import com.dxc.integral.life.beans.in.AuthenticationInfo;
import com.dxc.integral.life.beans.in.S2465;
import com.dxc.integral.life.beans.in.S2480;
import com.dxc.integral.life.beans.in.Sr2d2;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LcreatepersonalclientServiceInputBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private AuthenticationInfo authenticationInfo;
	private S2480 s2480;
	private List<S2465> s2465;
	private List<Sr2d2> sr2d2;
	
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}
	public S2480 getS2480() {
		return s2480;
	}
	public void setS2480(S2480 s2480) {
		this.s2480 = s2480;
	}
	public List<S2465>  getS2465() {
		return s2465;
	}
	public void setS2465(List<S2465> s2465) {
		this.s2465 = s2465;
	}
	public List<Sr2d2>  getSr2d2() {
		return sr2d2;
	}
	public void setSr2d2(List<Sr2d2> sr2d2) {
		this.sr2d2 = sr2d2;
	}
	@Override
	public String toString() {
		return "lcreatepersonalclientServiceInputBean []";
	}
	
}