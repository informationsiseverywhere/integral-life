package com.dxc.integral.life.general.processors;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import com.dxc.integral.fsu.general.utilities.CommonUtility;
import com.dxc.integral.life.beans.L2asstmgmtReaderDTO;
import com.dxc.integral.life.general.mappers.L2asstmgmtReaderRowMapper;

public class L2asstmgmtReadStoredProc extends StoredProcedure {
	private static final Logger LOGGER = LoggerFactory.getLogger(L2asstmgmtReadStoredProc.class);
	
	private final String RESULTLIST = "RESULTLIST";
	private String businessDate = null;

	public L2asstmgmtReadStoredProc(JdbcTemplate jdbcTemplate,String businessDate) {
		super(jdbcTemplate, "RetrieveFundDetails");
		this.businessDate = businessDate;
		RowMapper rowMapper = new L2asstmgmtReaderRowMapper();
		declareParameter(new SqlReturnResultSet(RESULTLIST, rowMapper));		
		declareParameter(new SqlParameter("EFFDATE",Types.INTEGER));
		compile();
	}

	@SuppressWarnings("unchecked")
	public List<L2asstmgmtReaderDTO> getAllDetails() {
		// now execute
		// Call on parent class
		List<L2asstmgmtReaderDTO> list = null;
		Map<String, Object> result = null;
		Map inParameters = new HashMap();
		inParameters.put("EFFDATE", new Integer(CommonUtility.changetoIntegralFormat(businessDate)));		
		try {
			result = execute(inParameters);
			if (result != null && result.get(RESULTLIST) != null) {
				list = (List<L2asstmgmtReaderDTO>) result.get(RESULTLIST);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception thrown while executing Stored Procedure",ex);//IJTI-1498
		}
		return list;

	}
	
}
