package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import com.dxc.integral.life.beans.L2lincReaderDTO;
import com.dxc.integral.life.dao.RlncpfDAO;
import com.dxc.integral.life.dao.model.Rlncpf;

public class L2rlncWriter implements ItemWriter<L2lincReaderDTO> {
	
	private ItemWriter<L2lincReaderDTO> rlncpfInsertWriter;
	private ItemWriter<L2lincReaderDTO> rlncbkpfInsertWriter;
	private ItemWriter<L2lincReaderDTO> lincpfUpdateWriter;
	private ItemWriter<L2lincReaderDTO> rlncpfUpdateWriter;
	
	@Autowired
	private RlncpfDAO rlncpfDAO;
	
	public L2rlncWriter() {
		//Added for removing Sonar Qube Errors
	};
	
	
	public synchronized void write(List<? extends L2lincReaderDTO> readerDTOList) throws Exception {
		
		    List<Rlncpf> rlncpfList = rlncpfDAO.readRecordOfRlncpf();
		    List<L2lincReaderDTO> tempUpdateList = new ArrayList<L2lincReaderDTO>();
		    List<L2lincReaderDTO> tempInsertList = new ArrayList<L2lincReaderDTO>();

		    Map<String, Rlncpf> rlncpfListMap = rlncpfList.stream()
		    		.collect(Collectors.toMap(Rlncpf::getZhospopt, Function.identity()));
		    Map<String, L2lincReaderDTO> readerDTOListMap = readerDTOList.stream()
		        .collect(Collectors.toMap(L2lincReaderDTO::getZhospopt, Function.identity()));
		    
		   for (String key : readerDTOListMap.keySet()) {
			   if (rlncpfListMap.containsKey(StringUtils.rightPad(key,40," "))) {
					   tempUpdateList.add(readerDTOListMap.get(key.trim()));
		           } else {
		        	   tempInsertList.add(readerDTOListMap.get(key.trim()));
		           }
		       }
		       
		   if(!tempUpdateList.isEmpty())
			   rlncpfUpdateWriter.write(tempUpdateList);
		       
		   if(!tempInsertList.isEmpty())
			   rlncpfInsertWriter.write(tempInsertList);
		       
			rlncbkpfInsertWriter.write(readerDTOList);
			lincpfUpdateWriter.write(readerDTOList);
		}

	public void setRlncpfInsertWriter(ItemWriter<L2lincReaderDTO> rlncpfInsertWriter) {
		this.rlncpfInsertWriter = rlncpfInsertWriter;
	}
	
	public void setRlncbkpfInsertWriter(ItemWriter<L2lincReaderDTO> rlncbkpfInsertWriter) {
		this.rlncbkpfInsertWriter = rlncbkpfInsertWriter;
	}
	
	public void setLincpfUpdateWriter(ItemWriter<L2lincReaderDTO> lincpfUpdateWriter) {
		this.lincpfUpdateWriter = lincpfUpdateWriter;
	}
	
	public void setRlncpfUpdateWriter(ItemWriter<L2lincReaderDTO> rlncpfUpdateWriter) {
		this.rlncpfUpdateWriter = rlncpfUpdateWriter;
	}
	
}