package com.dxc.integral.life.beans;

import java.math.BigDecimal;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * DTO for control totals.
 * 
 * @author dpuhawan
 *
 */
@Component("l2newintcpControlTotalDTO")
@Lazy
public class L2newintcpControlTotalDTO {

	/** The controlTotal01 */
	private BigDecimal controlTotal01 = BigDecimal.ZERO;
	/** The controlTotal02 */
	private BigDecimal controlTotal02 = BigDecimal.ZERO;
	/** The controlTotal03 */
	private BigDecimal controlTotal03 = BigDecimal.ZERO;
	/** The controlTotal04 */
	private BigDecimal controlTotal04 = BigDecimal.ZERO;
	/** The controlTotal05 */
	private BigDecimal controlTotal05 = BigDecimal.ZERO;
	/** The controlTotal06 */
	private BigDecimal controlTotal06 = BigDecimal.ZERO;

	/**
	 * @return the controlTotal01
	 */
	public BigDecimal getControlTotal01() {
		return controlTotal01;
	}
	
	/**
	 * Setter for controlTotal01
	 * 
	 * @param controlTotal01
	 */
	public synchronized  void setControlTotal01(BigDecimal controlTotal01) {
		this.controlTotal01 = controlTotal01;
	}

	/**
	 * @return the controlTotal02
	 */
	public BigDecimal getControlTotal02() {
		return controlTotal02;
	}
	
	/**
	 *  Setter for controlTotal02
	 *  
	 * @param controlTotal02
	 */
	public synchronized  void setControlTotal02(BigDecimal controlTotal02) {
		this.controlTotal02 = controlTotal02;
	}

	/**
	 * @return the controlTotal03
	 */
	public BigDecimal getControlTotal03() {
		return controlTotal03;
	}
	
	/**
	 * Setter for controlTotal03
	 * 
	 * @param controlTotal03
	 */
	public synchronized  void setControlTotal03(BigDecimal controlTotal03) {
		this.controlTotal03 = controlTotal03;
	}

	/**
	 * @return the controlTotal04
	 */
	public BigDecimal getControlTotal04() {
		return controlTotal04;
	}
	
	/**
	 * Setter for controlTotal04
	 * 
	 * @param controlTotal04
	 */
	public synchronized  void setControlTotal04(BigDecimal controlTotal04) {
		this.controlTotal04 = controlTotal04;
	}

	/**
	 * @return the controlTotal05
	 */
	public BigDecimal getControlTotal05() {
		return controlTotal05;
	}
	
	/**
	 * Setter for controlTotal05
	 * 
	 * @param controlTotal05
	 */
	public synchronized  void setControlTotal05(BigDecimal controlTotal05) {
		this.controlTotal05 = controlTotal05;
	}

	/**
	 * @return the controlTotal06
	 */
	public BigDecimal getControlTotal06() {
		return controlTotal06;
	}
	
	/**
	 * Setter for controlTotal06
	 * 
	 * @param controlTotal06
	 */
	public synchronized  void setControlTotal06(BigDecimal controlTotal06) {
		this.controlTotal06 = controlTotal06;
	}
	
	
}
