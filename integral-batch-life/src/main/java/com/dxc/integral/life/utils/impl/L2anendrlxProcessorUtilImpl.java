package com.dxc.integral.life.utils.impl;

import java.io.IOException;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.L2anendrlxReaderDTO;
import com.dxc.integral.life.beans.L2anendrlxProcessorDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.utils.L2anendrlxProcessorUtil;

/**
 * L2ANENDRLX processor utility implementation.
 * 
 * @author mmalik25
 *
 */

@Service("l2anendrlxProcessorUtil")
@Lazy
public class L2anendrlxProcessorUtilImpl implements L2anendrlxProcessorUtil {

 	@Autowired
	private Zrdecplc zrdecplc;
	
	@Autowired
	private Datcon2 datcon2;
	
	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/
	
	/* 
	 * This method calculates the payment due , next billing date, payment method
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.ProcessorUtil
	 * #getPaymnetDueInfo(com.dxc.integral.life.dao.model.Zraepf, 
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public L2anendrlxProcessorDTO getPaymnetDueInfo(L2anendrlxReaderDTO readerDTO, Integer tranDate) throws IOException{/*ILIFE-5977*/
		
		int payIdx = 1;
		int payMax = 8;
		int maxDate = 99999999;
		int idx = 0;
		BigDecimal paymentDue;
		int nextPaydate;
		L2anendrlxProcessorDTO processorDTO = new L2anendrlxProcessorDTO();
		
		processorDTO.setEffectiveDate(tranDate);
		
		Integer zrduedte = readerDTO.getZrduedte(payIdx);
		while ( zrduedte.intValue() != readerDTO.getNpaydate().intValue() && payIdx < payMax) {
			payIdx ++;
			zrduedte = readerDTO.getZrduedte(payIdx);
		}
		if (payIdx == payMax) {
			nextPaydate = maxDate;
		}
		else {
			idx = payIdx;
			nextPaydate = readerDTO.getZrduedte(idx+1);
		}
		processorDTO.setNextPaydate(nextPaydate);
		processorDTO.setPaymmeth(readerDTO.getPaymmeth(idx));
		processorDTO.setPayOpt(readerDTO.getZrpayopt(idx));

		BigDecimal percent = readerDTO.getPrcnt(payIdx);
		paymentDue = percent.multiply(readerDTO.getSumins()).divide(BigDecimal.valueOf(100));
		if (paymentDue.compareTo(BigDecimal.valueOf(0))!=0 ) {
			paymentDue = callRounding(readerDTO.getPolicyCurr(), paymentDue , 
					readerDTO.getBatctrcde(), readerDTO.getChdrcoy());/*ILIFE-5977*/ /*IJTI-379*/
		}
		processorDTO.setIndex(idx);
		processorDTO.setPaymentDue(paymentDue);
		return processorDTO;
	}

	/* 
	 * This method populates the ZrcshopDTO
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.ProcessorUtil#populateZrcshopDTO
	 * (com.dxc.integral.life.dao.model.Zraepf, com.dxc.integral.iaf.dao.model.Batcpf, 
	 * com.dxc.integral.life.beans.ProcessorDTO)
	 */
	@Override
	public ZrcshopDTO populateZrcshopDTO(L2anendrlxReaderDTO readerDTO, T5645 t5645IO,
			String longdesc) throws IOException{/*ILIFE-5977*/
		Map<String, Itempf> t3629Map=itempfDAO.readSmartTableByTableName2(readerDTO.getChdrcoy(), "T3629", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/
		T3629 t3629IO = smartTableDataUtils.convertGenareaToPojo(t3629Map.get(readerDTO.getPaycurr()).getGenareaj(), T3629.class); /*IJTI-398*/
		ZrcshopDTO zrcshopDTO = new ZrcshopDTO();
		zrcshopDTO.setBankcode(t3629IO.getBankcode());
		zrcshopDTO.setBankkey(readerDTO.getBankkey());
		zrcshopDTO.setBankacckey(readerDTO.getBankacckey());
		zrcshopDTO.setClntcoy(readerDTO.getCownCoy());/*IJTI-379*/
		zrcshopDTO.setClntnum(readerDTO.getPayclt());
		zrcshopDTO.setPaycurr(readerDTO.getPolicyCurr());
		zrcshopDTO.setCntcurr(readerDTO.getPolicyCurr());
		zrcshopDTO.setReqntype(readerDTO.getPaymmeth());
		zrcshopDTO.setPymt(readerDTO.getPaymentDue());
		zrcshopDTO.setTranref(readerDTO.getChdrnum());
		zrcshopDTO.setChdrnum(readerDTO.getChdrnum());
		zrcshopDTO.setChdrcoy(readerDTO.getChdrcoy());
		zrcshopDTO.setTranno(readerDTO.getPolicyTranno());
		zrcshopDTO.setEffdate(readerDTO.getEffectiveDate());
		zrcshopDTO.setLanguage("E");
		zrcshopDTO.setTrandesc(longdesc);
		zrcshopDTO.setUser(readerDTO.getUserNum());
		String glcode = t5645IO.getGlmaps().get(2);
		glcode = glcode.replace("@", "");
		zrcshopDTO.setGlcode(glcode);
		zrcshopDTO.setSacscode(t5645IO.getSacscodes().get(2));
		zrcshopDTO.setSacstyp(t5645IO.getSacstypes().get(2));
		zrcshopDTO.setSign(t5645IO.getSigns().get(2));
		zrcshopDTO.setCnttot(t5645IO.getCnttots().get(2));
		zrcshopDTO.setBatcpfx(readerDTO.getBatcpfx());
		zrcshopDTO.setBatccoy(readerDTO.getBatccoy());
		zrcshopDTO.setBatcbrn(readerDTO.getBatcbrn());
		zrcshopDTO.setBatcactyr(readerDTO.getBatcactyr());
		zrcshopDTO.setBatcactmn(readerDTO.getBatcactmn());
		zrcshopDTO.setBatctrcde(readerDTO.getBatctrcde());
		zrcshopDTO.setBatcbatch(readerDTO.getBatcbatch());
		return zrcshopDTO;
	}
	
	/* (non-Javadoc)
	 * @see com.dxc.integral.life.utils.L2anendrlxProcessorUtil#updateReaderDTOInfo(com.dxc.integral.life.beans.L2anendrlxProcessorDTO, com.dxc.integral.life.beans.L2anendrlxReaderDTO)
	 */
	@Override
	public L2anendrlxReaderDTO updateReaderDTOInfo(L2anendrlxProcessorDTO processorDTO, 
			L2anendrlxReaderDTO readerDTO){

		Integer newDate = datcon2.plusDays(processorDTO.getEffectiveDate(), -1);
		//Old Zraepf record
		readerDTO.setOldCurrto(newDate);
		readerDTO.setDatime(new Timestamp(System.currentTimeMillis()));
		
		//New Zraepf record
		readerDTO.setNewNpaydate(processorDTO.getNextPaydate());
		readerDTO.setPaydte(processorDTO.getIndex(), processorDTO.getEffectiveDate());
		readerDTO.setPaid(processorDTO.getIndex(), processorDTO.getPaymentDue());
		readerDTO.setNewTotalamnt(readerDTO.getTotamnt().add(processorDTO.getPaymentDue()));
		readerDTO.setNewCurrfrom(processorDTO.getEffectiveDate());
		readerDTO.setNewCurrto(99999999);
		readerDTO.setNewPaymentOpt(processorDTO.getPayOpt());

		//ptrnpf required
		readerDTO.setTrdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
		readerDTO.setTrtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
		readerDTO.setEffectiveDate(processorDTO.getEffectiveDate());
		readerDTO.setPaymentDue(processorDTO.getPaymentDue());
		readerDTO.setPaymmeth(processorDTO.getPaymmeth());
		
		return readerDTO;

	}

	private BigDecimal callRounding(String cntcurr, BigDecimal amountIn, String batctrCode, String company) throws IOException{/*ILIFE-5977*/

		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(cntcurr);
		zrdecplDTO.setBatctrcde(batctrCode);
		zrdecplDTO.setCompany(company);/*IJTI-379*/
		return zrdecplc.convertAmount(zrdecplDTO);/*ILIFE-5977*/
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.L2anendrlxProcessorUtil#checkValidCoverage(com.dxc.integral.life.beans.L2anendrlxReaderDTO)
	 */
	@Override
	public boolean checkValidCoverage(L2anendrlxReaderDTO readerDTO) throws IOException {/*ILIFE-5977*/
		boolean validCoverage = false;
		Map<String, Itempf> t5679Map = itempfDAO.readSmartTableByTableName2(readerDTO.getChdrcoy(), "T5679", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/
		//ILIFE-5763 Starts
		if(null==t5679Map.get(readerDTO.getBatctrcde())){
			throw new ItemNotfoundException("Br622: Item "+readerDTO.getBatctrcde()+" not found in Table T5679 while processing contract "+readerDTO.getChdrnum());
		}
		//ILIFE-5763 Ends
		T5679 t5679 = smartTableDataUtils.convertGenareaToPojo(t5679Map.get(readerDTO.getBatctrcde()).getGenareaj(), T5679.class); /*IJTI-398*/
		for (int i = 0; i < t5679.getCovRiskStats().size(); i++) {
			if (t5679.getCovRiskStats().get(i).equals(readerDTO.getStatcode())) {
				for (int j = 0; j < t5679.getCovPremStats().size(); j++) {
					if (t5679.getCovPremStats().get(j).equals(readerDTO.getPstatcode())) {
						validCoverage = true;
						break;
					}
				}
			}
		}
		return validCoverage;
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.L2anendrlxProcessorUtil#populateCrtloanDTO(com.dxc.integral.life.beans.L2anendrlxReaderDTO, com.dxc.integral.life.smarttable.pojo.T5645, java.lang.String)
	 */
	@Override
	public CrtloanDTO populateCrtloanDTO(L2anendrlxReaderDTO readerDTO, T5645 t5645IO,
			String longdesc){
	
		CrtloanDTO crtloanDTO = new CrtloanDTO();
		crtloanDTO.setLongdesc(longdesc);
		crtloanDTO.setChdrcoy("2");
		crtloanDTO.setChdrnum(readerDTO.getChdrnum());
		crtloanDTO.setCnttype(readerDTO.getPolicyType());
		crtloanDTO.setLoantype("E");
		crtloanDTO.setCntcurr(readerDTO.getPolicyCurr());
		crtloanDTO.setBillcurr(readerDTO.getPolicyBillCurr());
		crtloanDTO.setTranno(readerDTO.getPolicyTranno());
		crtloanDTO.setOccdate(readerDTO.getPolicyOccdate());
		crtloanDTO.setEffdate(readerDTO.getEffectiveDate());
		crtloanDTO.setAuthCode(readerDTO.getBatctrcde());
		crtloanDTO.setLanguage("E");
		crtloanDTO.setOutstamt(readerDTO.getPaymentDue());
		crtloanDTO.setCbillamt(readerDTO.getPaymentDue());
		crtloanDTO.setActmonth(readerDTO.getBatcactmn());
		crtloanDTO.setActyear(readerDTO.getBatcactyr());
		crtloanDTO.setBatch(readerDTO.getBatcbatch());
		crtloanDTO.setBranch(readerDTO.getBatcbrn());
		crtloanDTO.setBranch(readerDTO.getBatcbrn());
		crtloanDTO.setTrcde(readerDTO.getBatctrcde());
		crtloanDTO.setSacscode01(t5645IO.getSacscodes().get(0));
		crtloanDTO.setSacscode02(t5645IO.getSacscodes().get(1));
		crtloanDTO.setSacstyp01(t5645IO.getSacstypes().get(0));
		crtloanDTO.setSacstyp02(t5645IO.getSacstypes().get(1));
		crtloanDTO.setGlcode01(t5645IO.getGlmaps().get(0));
		crtloanDTO.setGlcode02(t5645IO.getGlmaps().get(1));
		crtloanDTO.setGlsign01(t5645IO.getSigns().get(0));
		crtloanDTO.setGlsign02(t5645IO.getSigns().get(1));
		crtloanDTO.setJobname(readerDTO.getJobnm());
		crtloanDTO.setUsrprofile(readerDTO.getUsrprf());

		return crtloanDTO;
	}
}
