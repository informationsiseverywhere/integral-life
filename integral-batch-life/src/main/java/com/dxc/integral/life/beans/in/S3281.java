package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S3281 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String mandstat;
	private String timesUse;
	private String effdateDisp;
	private String bankkey;
	private String bankacckey;
	private String facthous;
	private String mandAmt;
	private String detlsumm;
	private String atm;
	private String ddop;
	private String mrbnk;
	private String bnkbrn;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getTimesUse() {
		return timesUse;
	}
	public void setTimesUse(String timesUse) {
		this.timesUse = timesUse;
	}
	public String getEffdateDisp() {
		return effdateDisp;
	}
	public void setEffdateDisp(String effdateDisp) {
		this.effdateDisp = effdateDisp;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getMandAmt() {
		return mandAmt;
	}
	public void setMandAmt(String mandAmt) {
		this.mandAmt = mandAmt;
	}
	public String getDetlsumm() {
		return detlsumm;
	}
	public void setDetlsumm(String detlsumm) {
		this.detlsumm = detlsumm;
	}
	public String getAtm() {
		return atm;
	}
	public void setAtm(String atm) {
		this.atm = atm;
	}
	public String getDdop() {
		return ddop;
	}
	public void setDdop(String ddop) {
		this.ddop = ddop;
	}
	public String getMrbnk() {
		return mrbnk;
	}
	public void setMrbnk(String mrbnk) {
		this.mrbnk = mrbnk;
	}
	public String getBnkbrn() {
		return bnkbrn;
	}
	public void setBnkbrn(String bnkbrn) {
		this.bnkbrn = bnkbrn;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S3281 []";
	}
	
}
