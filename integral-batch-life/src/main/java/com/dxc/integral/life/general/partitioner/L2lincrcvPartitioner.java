package com.dxc.integral.life.general.partitioner;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class L2lincrcvPartitioner implements Partitioner{
	
	@Autowired
	private Environment env;
	private List<String> temperoryList;
	private static final Logger LOGGER = LoggerFactory.getLogger(L2lincrcvPartitioner.class);
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		
		Map<String, ExecutionContext> partitionMap = null;
		String fileName = "RECV";
		String hash = "\\";
		String filePath = env.getProperty("report.output.path") +  env.getProperty("folder.linc.receive");
		String archiveFolder = env.getProperty("folder.archive");   
		File dir = FileSystems.getDefault().getPath(filePath).toFile();
		FilenameFilter filter = (tempFile, name)-> name.contains(fileName) ;
		filter.accept(dir,fileName);	
	    partitionMap  = new HashMap<>();  
	    ExecutionContext context = new ExecutionContext();
	    String[] fileList = dir.list(filter);
    	File[]  inputResources = new File[fileList.length];
	    temperoryList = new ArrayList<>(fileList.length);
	    for (int i = 0; i< fileList.length; i++) {
	    	inputResources[i] = FileSystems.getDefault().getPath(dir.toString() + hash + fileList[i]).toFile();
	        		if(dir.isDirectory() && inputResources[i].renameTo(FileSystems.getDefault().getPath
	        				(filePath + archiveFolder + inputResources[i].getName()).toFile())) 
			        	{  
	        				try {
	        					Files.deleteIfExists(Paths.get(inputResources[i].toURI()));
	        				} catch (IOException e) {
							LOGGER.info("{The error is- }", e);
						} 
        				temperoryList.add(fileList[i]);
			        }
	        	    String username;
	        		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        		if (principal instanceof UserDetails)
	        			username  = ((UserDetails)principal).getUsername();
	        		else
	        			username = principal.toString();
	        		context.put("username", username);
	        		String tempPath = env.getProperty("report.output.path") + 
	        				env.getProperty("folder.linc.receive") + env.getProperty("folder.archive");
	        		context.put("fileResource", tempPath + temperoryList.toArray()[i].toString());
	        		ExecutionContext tempContext = new ExecutionContext(context);
	        		partitionMap.put(inputResources[i].getName(), tempContext);
	        	}
	   
	    return partitionMap;
		}
	
		public List<String> getTemperoryList() {
			
			return temperoryList;
	}
}	
