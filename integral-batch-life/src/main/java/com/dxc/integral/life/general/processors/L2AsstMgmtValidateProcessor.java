package com.dxc.integral.life.general.processors;

import org.springframework.batch.item.ItemProcessor;

import com.dxc.integral.life.beans.L2asstmgmtReaderDTO;

public class L2AsstMgmtValidateProcessor implements ItemProcessor<L2asstmgmtReaderDTO, L2asstmgmtReaderDTO> {

	@Override
	public L2asstmgmtReaderDTO process(L2asstmgmtReaderDTO item) throws Exception {		
		return item;
	}

}
