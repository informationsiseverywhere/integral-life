package com.dxc.integral.life.utils;

import java.util.List;
import com.dxc.integral.life.beans.L2CreateContractReaderDTO;

public interface L2CreateContractProcessorUtil {
	public List<String> loadSmartTables(L2CreateContractReaderDTO l2CreateContractReaderDTO);
	public String loadSmartTablesT600(String crtable);
	public String setWaiverOfPrem(List<String> baseContractAndRiderCode);
}
