package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.dxc.integral.life.beans.L2lincsndReaderDTO;
import com.dxc.integral.life.dao.LincpfDAO;
import com.dxc.integral.life.dao.SlncpfDAO;
import com.dxc.integral.life.dao.model.Lincpf;
import org.slf4j.Logger;

/**
 * Delete records from database once records are written
 * to flat file. 
 * 
 * @author gsaluja2
 *
 */

public class L2lincsndItemWriter implements ItemWriter<L2lincsndReaderDTO> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2lincsndItemWriter.class);

	@Value("#{stepExecutionContext['zreclass']}")
	private String zreclass;
	
	@Value("#{stepExecutionContext['transactionDate']}")
	private int transactionDate;
	
	@Value("#{stepExecutionContext['username']}")
	private String username;
	
	@Value("#{stepExecution.jobExecution.jobInstance.jobName}")
    private String jobnm;
	
	@Value("#{stepExecutionContext['resetTranscds']}")
	private List<String> resetTranscdList;
	
	@Autowired
	private SlncpfDAO slncpfDAO;
	
	@Autowired
	private LincpfDAO lincpfDAO;
	
	public L2lincsndItemWriter() {
		//does nothing
	}

	@Override
	public synchronized void write(List<? extends L2lincsndReaderDTO> readerDTOList) throws Exception {
		List<Lincpf> lincpfList = new ArrayList<>();
		Lincpf lincpf;
		if(slncpfDAO.deleteSlncpfData(zreclass) > 0)
			LOGGER.info("Data Processed for record type {}",zreclass);
		for(L2lincsndReaderDTO readerDTO: readerDTOList) {
			if(readerDTO.getLincpf()!=null) {
				lincpf = readerDTO.getLincpf();
				lincpf.setSenddate(transactionDate);
				if(null != resetTranscdList && !resetTranscdList.isEmpty() && 
						resetTranscdList.contains(lincpf.getBatctrcde().trim())) {
					lincpf.setRecvdate(0);
					lincpf.setZlincdte(0);
				}
				lincpf.setUsrprf(username.trim().toUpperCase());
				lincpf.setJobnm(jobnm.trim().toUpperCase());
				lincpfList.add(lincpf);
			}
		}
		if(!lincpfList.isEmpty()) {
			lincpfDAO.updateLincData(lincpfList);
		}
	}
}
