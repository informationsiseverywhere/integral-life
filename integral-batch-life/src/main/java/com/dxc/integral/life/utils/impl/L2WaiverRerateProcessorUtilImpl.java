package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.ClntpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.dao.model.Clntpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.CrtundwrtDTO;
import com.dxc.integral.life.beans.IsuallDTO;
import com.dxc.integral.life.beans.L2WaiverRerateControlTotalDTO;
import com.dxc.integral.life.beans.L2WaiverRerateProcessorDTO;
import com.dxc.integral.life.beans.L2WaiverRerateReaderDTO;
import com.dxc.integral.life.beans.LifsttrDTO;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.TxcalcDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.beans.VpxacblDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.beans.VpxlextDTO;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.AnnypfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PcddpfDAO;
import com.dxc.integral.life.dao.RegppfDAO;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Annypf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Pcddpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Regppf;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T5399;
import com.dxc.integral.life.smarttable.pojo.T5655;
import com.dxc.integral.life.smarttable.pojo.T5671;
import com.dxc.integral.life.smarttable.pojo.T5674;
import com.dxc.integral.life.smarttable.pojo.T5675;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.T6658;
import com.dxc.integral.life.smarttable.pojo.Tr517;
import com.dxc.integral.life.smarttable.pojo.Tr52d;
import com.dxc.integral.life.smarttable.pojo.Tr52e;
import com.dxc.integral.life.utils.Agecalc;
import com.dxc.integral.life.utils.Cfee001Utils;
import com.dxc.integral.life.utils.Crtundwrt;
import com.dxc.integral.life.utils.L2WaiverRerateItemProcessorUtil;
import com.dxc.integral.life.utils.Lifsttr;
import com.dxc.integral.life.utils.Premium;
import com.dxc.integral.life.utils.Servtax;
import com.dxc.integral.life.utils.VPMScaller;
import com.dxc.integral.life.utils.Vpxacbl;
import com.dxc.integral.life.utils.Vpxchdr;
import com.dxc.integral.life.utils.Vpxlext;
import com.fasterxml.jackson.databind.ObjectMapper;
@Service
@Lazy
public class L2WaiverRerateProcessorUtilImpl implements L2WaiverRerateItemProcessorUtil{
			
	@Autowired
	ItempfService itemService;
	
	@Autowired
	ClntpfDAO clntpfDAO;
	
	@Autowired
	Vpxlext vpxLext;
	
	@Autowired
	Vpxacbl vpxAcbl;
	
	@Autowired
	Vpxchdr vpxChdr;
	
	@Autowired
	private L2WaiverRerateControlTotalDTO l2WaiverRerateControlTotalDTO;
	
	@Autowired
	Agecalc ageCalc;
	
	@Autowired
	ChdrpfDAO chdrpfDAO;
	
	@Autowired
	Crtundwrt crtUndwrt;
	
	@Autowired
	LextpfDAO lextpfDAO;
	
	@Autowired
	LifepfDAO lifepfDAO;
	
	@Autowired
	AnnypfDAO annypfDAO;
	
	@Autowired
	PayrpfDAO payrpfDAO;
	
	@Autowired
	private Zrdecplc zrdecPlc;
	
	@Autowired
	private Lifsttr lifSttr;
	
	@Autowired
	private Datcon2 datCon2;
	
	@Autowired
	private Datcon3 datCon3;
	
	@Autowired
	CovrpfDAO covrpfDAO;
	
	@Autowired
	ItempfDAO itempfDAO;
	
	@Autowired
	AgcmpfDAO agcmpfDAO;
	
	@Autowired
	PcddpfDAO pcddpfDAO;
	
	@Autowired
	IncrpfDAO incrpfDAO;
	
	@Autowired
	RegppfDAO regpfDAO;
		
	@Autowired
	Servtax servTax;
	
	@Autowired
	Cfee001Utils cfeeUtils;
	
	@Autowired
	VPMScaller vpmsCaller;
	
	@Autowired
	Premium premium;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2WaiverRerateProcessorUtilImpl.class);
	

	@Override
	public L2WaiverRerateProcessorDTO loadTableDetails(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO) {				
		readT5679(l2processorDTO);
		readT5674(l2processorDTO);
		readT5688(l2processorDTO);
		readT5655(l2processorDTO);
		readT5671(l2processorDTO);
		readT5675(l2processorDTO);
		readT5399(l2processorDTO);
		readTr517(l2processorDTO);
		readT6658(l2processorDTO);
		readT5687(l2processorDTO);	
	return l2processorDTO;
	}
	public void readT5679(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		T5679 t5679 = itemService.readSmartTableByTrim(Companies.LIFE,"T5679",l2processorDTO.getBatchtrancde(),T5679.class);
		l2processorDTO.setT5679(t5679);
		if(null==l2processorDTO.getT5679()) {
				throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5679");
		}
	}
	public void readT5674(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Map<String, List<Itempf>> t5674Map = itemService.readItemTabl(Companies.LIFE,"T5674");
		List<String> t5674KeyList= new ArrayList<>();
		List<String> t5674CommsubrList= new ArrayList<>();
		if (null != t5674Map) {
			for (List<Itempf> itemList : t5674Map.values()) {
				for (Itempf i : itemList) {
					try {
						l2processorDTO.setT5674(new ObjectMapper().readValue(i.getGenareaj(), T5674.class));
					}  catch (IOException e) {
						throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T5674: " );
					}
					t5674KeyList.add(i.getItemitem());
					t5674CommsubrList.add(l2processorDTO.getT5674().getCommsubr());
				}				
    		 }
			l2processorDTO.setT5674Key(t5674KeyList);
			l2processorDTO.setT5674CommSubr(t5674CommsubrList);
		}	
		else {
			throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5674");
		}
	}
	public void readT5688(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Map<String, List<Itempf>> t5688Map=itemService.readItemTabl(Companies.LIFE,"T5688");
		List<Integer> t5688Itmfrm=new ArrayList<>();
		List<String> t5688Cnttype= new ArrayList<>();
		String t5688feeMeth;
		if (null != t5688Map) {
			for (List<Itempf> itemList : t5688Map.values()) {
				for (Itempf i : itemList) {try {
					l2processorDTO.setT5688(new ObjectMapper().readValue(i.getGenareaj(), T5688.class));
				} catch (IOException e) {
					throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T5688: ");
				}
				t5688Itmfrm.add(i.getItmfrm());
				t5688Cnttype.add(i.getItemitem());
				
				}
			}
			t5688feeMeth=l2processorDTO.getT5688().getFeemeth();
			l2processorDTO.setT5688Cnttype(t5688Cnttype);
			l2processorDTO.setT5688Itmfrm(t5688Itmfrm);
			l2processorDTO.setT5688FeeMeth(t5688feeMeth);
		}
		else {
			throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5688");
		}
	}
	public void readT5655(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Map<String, List<Itempf>> t5655Map= itemService.readItemTabl(Companies.LIFE, "T5655");
		List<String> t5655ItemKey = new ArrayList<>();
		List<Integer> t5655Itmfrm = new ArrayList<>();
		List<Integer> t5655ItmTo = new ArrayList<>();
		List<Integer> t5655LeadDays = new ArrayList<>();
		int index=0,maxIndex=0;
		if (t5655Map != null) {
			String authCode = l2processorDTO.getBatchtrancde();
			for (List<Itempf> itemList : t5655Map.values()) {
				for (Itempf i : itemList) {
					if (i.getItemitem().startsWith(authCode)) {
						try {							
							l2processorDTO.setT5655(new ObjectMapper().readValue(i.getGenareaj(), T5655.class));
						} catch (IOException e) {
							throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T5655: ");
						}
						t5655ItemKey.add(i.getItemitem());
						t5655Itmfrm.add(i.getItmfrm());
						t5655ItmTo.add(i.getItmto());
						t5655LeadDays.add(l2processorDTO.getT5655().getLeadDays());
						maxIndex=index;
						index++;				
					}
				}
			}
			l2processorDTO.setT5655ItemKey(t5655ItemKey);
			l2processorDTO.setT5655Itmfrm(t5655Itmfrm);
			l2processorDTO.setT5655ItmTo(t5655ItmTo);
		}
		else {
			throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5655");
		}
	}
	public void readT5671(L2WaiverRerateProcessorDTO l2processorDTO) {
		
		List<String> t5671Key = new ArrayList<>();
		List<String> t5671Subprogs = new ArrayList<>();
		int index=0;
		Map<String, List<Itempf>> t5671Map= itemService.readItemTabl(Companies.LIFE, "T5671");
		if (t5671Map != null) {
			String code = l2processorDTO.getBatchtrancde();
			for (List<Itempf> itemList : t5671Map.values()) {
				for (Itempf i : itemList) {
					if (i.getItemitem().startsWith(code)) {
						try {
							l2processorDTO.setT5671(new ObjectMapper().readValue(i.getGenareaj(), T5671.class));
						}
						catch (IOException e) {
							throw new ItemParseException("L2WaiverRerate : Parsing Item  failed in Table T5671: " );
						}	
						t5671Key.add(i.getItemitem());
						t5671Subprogs.add(l2processorDTO.getT5671().getSubprogs().get(index));
						index++;
					}
				}
			}
			l2processorDTO.setT5671Key(t5671Key);
			l2processorDTO.setT5671Subprogs(t5671Subprogs);
		}
		else {
			throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5671");
		}
	}
	public void readT5675(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Map<String,List<Itempf>> t5675Map=itemService.readItemTabl(Companies.LIFE,"T5675");
		List<String> t5675Key = new ArrayList<>();
		List<String> t5675PremSubr = new ArrayList<>();
		if (t5675Map != null) {
			for (List<Itempf> itemList : t5675Map.values()) {
				for (Itempf i : itemList) {
					try{
						l2processorDTO.setT5675(new ObjectMapper().readValue(i.getGenareaj(), T5675.class));
					}
					catch (IOException e) {
						throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T5675: " );
					}		
					t5675Key.add(i.getItemitem());
					t5675PremSubr.add(l2processorDTO.getT5675().getPremsubr());
				}
			}
			l2processorDTO.setT5675Key(t5675Key);
			l2processorDTO.setT5675PremSubr(t5675PremSubr);
			
		}
		else {
			throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5675");
		}
	}
	public void readT5399(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Map<String,List<Itempf>> t5399Map=itemService.readItemTabl(Companies.LIFE,"T5399");
		for (List<Itempf> itemList : t5399Map.values()) {
			for (Itempf i : itemList) {
				try {
					l2processorDTO.setT5399(new ObjectMapper().readValue(i.getGenareaj(), T5399.class));
				} catch (IOException e) {
					throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T5399: ");
				}
			}
		}
	}
	public void readTr517(L2WaiverRerateProcessorDTO l2processorDTO) 
	{
		Map<String,List<Itempf>> tr517Map=itemService.readItemTabl(Companies.LIFE,"TR517");
		List<Integer> tr517Itmfrm = new ArrayList<>();
		List<String> tr517Crtable= new ArrayList<>();
		if (tr517Map != null) {
			for (List<Itempf> itemList : tr517Map.values()) {
				for (Itempf i : itemList) {
					try {
						l2processorDTO.setTr517(new ObjectMapper().readValue(i.getGenareaj(), Tr517.class));
					}  catch (IOException e) {
						throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table Tr517: " );
					}
					tr517Itmfrm.add(i.getItmfrm());
					tr517Crtable.add(i.getItemitem());
				}
			}
			l2processorDTO.setTr517Zrwvflgs(l2processorDTO.getTr517().getZrwvflgs());
			l2processorDTO.setTr517Ctables(l2processorDTO.getTr517().getCtables());
			l2processorDTO.setTr517Itmfrm(tr517Itmfrm);
			l2processorDTO.setTr517Crtable(tr517Crtable);
			l2processorDTO.setTr517Contitem(l2processorDTO.getTr517().getContitem());
		}
		else {
			throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in TR517");
		}
	}
	public void readT6658(L2WaiverRerateProcessorDTO l2processorDTO) 
	{
		Map<String,List<Itempf>> t6658Map=itemService.readItemTabl(Companies.LIFE,"T6658");
		List<Integer> t6658Itmfrm = new ArrayList<>();
		List<String> t6658Annvry= new ArrayList<>();
		List<String> t6658Billfreq= new ArrayList<>();
		if (t6658Map != null) {
			for (List<Itempf> itemList : t6658Map.values()) {
				for (Itempf i : itemList) {
					try {
						l2processorDTO.setT6658(new ObjectMapper().readValue(i.getGenareaj(), T6658.class));
					}  catch (IOException e) {
						throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T6658: " );
					}
					t6658Itmfrm.add(i.getItmfrm());
					t6658Annvry.add(i.getItemitem());
					t6658Billfreq.add(l2processorDTO.getT6658().getBillfreq());
				}
			}
			l2processorDTO.setT6658Annvry(t6658Annvry);
			l2processorDTO.setT6658BillFreqList(t6658Billfreq);
			l2processorDTO.setT6658Itmfrm(t6658Itmfrm);
		}
	}
	public void readT5687(L2WaiverRerateProcessorDTO l2processorDTO) {
		
			Map<String,List<Itempf>> t5687Map=itemService.readItemTabl(Companies.LIFE,"T5687");
			List<Integer> t5687Itmfrm = new ArrayList<>();
			List<String> t5687Crtable = new ArrayList<>();
			List<String> t5687Annvry = new ArrayList<>();
			List<String> t5687Premmeth= new ArrayList<>();
			List<String> t5687JlPremmeth = new ArrayList<>();
			List<Integer> t5687Pguarp = new ArrayList<>();
			List<Integer> t5687Rtrnwfreq = new ArrayList<>();
			List<String> t5687ZrrCombas = new ArrayList<>();
			if (t5687Map != null) {
				for (List<Itempf> itemList : t5687Map.values()) {
					for (Itempf i : itemList) {
						try{
							l2processorDTO.setT5687(new ObjectMapper().readValue(i.getGenareaj(), T5687.class));
						}
						catch (IOException e) {
							throw new ItemParseException("L2WaiverRerate : Parsing Item failed in Table T5687: " );
						}
						t5687Itmfrm.add(i.getItmfrm());
						t5687Crtable.add(i.getItemitem());
						t5687Annvry.add(l2processorDTO.getT5687().getAnniversaryMethod());
						t5687Premmeth.add(l2processorDTO.getT5687().getPremmeth());
						t5687JlPremmeth.add(l2processorDTO.getT5687().getJlPremMeth());
						t5687Pguarp.add(l2processorDTO.getT5687().getPremGuarPeriod());
						t5687Rtrnwfreq.add(l2processorDTO.getT5687().getRtrnwfreq());
						t5687ZrrCombas.add(l2processorDTO.getT5687().getZrrcombas());
					}
				}
				l2processorDTO.setT5687Annvry(t5687Annvry);
				l2processorDTO.setT5687Crtable(t5687Crtable);
				l2processorDTO.setT5687Itmfrm(t5687Itmfrm);
				l2processorDTO.setT5687JlPremmeth(t5687JlPremmeth);
				l2processorDTO.setT5687Pguarp(t5687Pguarp);
				l2processorDTO.setT5687Premmeth(t5687Premmeth);
				l2processorDTO.setT5687Rtrnwfreq(t5687Rtrnwfreq);
				l2processorDTO.setT5687ZrrCombas(t5687ZrrCombas);
			}
			else
			{
				throw new ItemNotfoundException("L2WaiverRerate: Item Not Found in T5687");
			}
		}
	@Override
	public L2WaiverRerateProcessorDTO checkValidContract(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO) {
		l2processorDTO.setChdrpf(chdrpfDAO.getLPContractInfo(l2processorDTO.getChdrcoy(), l2processorDTO.getChdrnum()));
		if(l2processorDTO.getT5655() != null)
		{
			l2processorDTO.setLeadDays(l2processorDTO.getT5655().getLeadDays());
			l2processorDTO.setBillDate(datCon2.plusDays(l2processorDTO.getEffDate(), l2processorDTO.getLeadDays()));
		}
		//check for status on T5679
		for (int i = 0; i < l2processorDTO.getT5679().getCnRiskStats().size(); i++) {
			if (l2processorDTO.getT5679().getCnRiskStats().get(i).equals(l2processorDTO.getChdrpf().getStatcode())) {
				for (int j=0; j< l2processorDTO.getT5679().getCnPremStats().size(); j++) {
					if (l2processorDTO.getT5679().getCnPremStats().get(j).equals(l2processorDTO.getChdrpf().getPstcde())) {
						l2processorDTO.setValidchdr(true);
					}
				}
			}
		}
		if (!l2processorDTO.isValidchdr()) {
			l2WaiverRerateControlTotalDTO.setControlTotal06(l2WaiverRerateControlTotalDTO.getControlTotal06().add(BigDecimal.ONE));
		} 
		return l2processorDTO;
	}
	@Override
	public L2WaiverRerateProcessorDTO processValidCoverage(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO) {
					
		l2processorDTO.setCovrUpdated(false);
		l2processorDTO.setPremdiff(BigDecimal.ZERO);
	
			boolean isValidCovr=validateCoverage(l2processorDTO,l2processorDTO.getCovrpf());
			if(isValidCovr)
			{
				readTr52d(l2processorDTO);
				try {
					processWop(l2processorDTO.getCovrpf(),l2processorDTO);
				} catch (ParseException e1) {
					throw new ItemParseException("Exception Parsing the data" , e1.getMessage());
				}					
				try {
					Covrpf covr = updateCovr(l2processorDTO.getCovrpf(),l2processorDTO);
				} catch (IOException | ParseException e) {
					throw new ItemParseException("Exception Parsing the data" , e.getMessage());
				}						
				updateAgcm(l2processorDTO.getCovrpf(),l2processorDTO);
				genericProcessing(l2processorDTO.getCovrpf(),l2processorDTO);
				updateUndr(l2processorDTO.getCovrpf(),l2processorDTO);
			}
			else {
				l2processorDTO.getL2waiverRerateControlDTO().setControlTotal07(l2WaiverRerateControlTotalDTO.getControlTotal07().add(BigDecimal.ONE));
			}
	//	}	
		if(l2processorDTO.getCovrpf()==null)
		{
			LOGGER.error("NO records to process--Process Ends");
		}
		
		if (l2processorDTO.isCovrUpdated()) {
			updateChdrPtrn(l2processorDTO);
			updatePayr(l2processorDTO);
		}
		return l2processorDTO;
	}
	public void updateChdrPtrn(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		/* Write PTRN record.*/
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(l2processorDTO.getChdrcoy());
		ptrnIO.setChdrnum(l2processorDTO.getChdrpf().getChdrnum());
		ptrnIO.setTranno(l2processorDTO.getTranNo());
		ptrnIO.setPtrneff(l2processorDTO.getEffDate());
		ptrnIO.setTrdt(l2processorDTO.getTransactionDate());
		ptrnIO.setTrtm(l2processorDTO.getTranTime());
		ptrnIO.setUserT(0);
		ptrnIO.setTermid("");
		ptrnIO.setBatcpfx(l2processorDTO.getPrefix());
		ptrnIO.setBatccoy(Companies.LIFE);
		ptrnIO.setBatcbrn(l2processorDTO.getBatchbranch());
		ptrnIO.setBatcactyr(l2processorDTO.getActYear());
		ptrnIO.setBatctrcde(l2processorDTO.getBatchtrancde());
		ptrnIO.setBatcactmn(l2processorDTO.getActMonth());
		ptrnIO.setBatcbatch(l2processorDTO.getBatch());
		ptrnIO.setDatesub(l2processorDTO.getEffDate());
		ptrnIO.setJobnm(l2processorDTO.getBatchName().toUpperCase());
		ptrnIO.setUsrprf(l2processorDTO.getUserName());
		ptrnIO.setValidflag("1"); 
		
		l2processorDTO.setPtrnInsert(ptrnIO);
		
		loadNonWopCovrStatus(l2processorDTO);
				
		Chdrpf chdrUpdate = createCopyChdrpf(l2processorDTO.getChdrpf());
		chdrUpdate.setValidflag("2");
		chdrUpdate.setCurrto(l2processorDTO.getRerateStore());
		chdrUpdate.setUniqueNumber(l2processorDTO.getChdrpf().getUniqueNumber());
		
		l2processorDTO.setChdrpfUpdate(chdrUpdate);
	
		l2processorDTO.getChdrpf().setValidflag("1");
		l2processorDTO.getChdrpf().setCurrto(l2processorDTO.getMaxDate());
		l2processorDTO.getChdrpf().setCurrfrom(l2processorDTO.getRerateStore());
		l2processorDTO.getChdrpf().setTranno(l2processorDTO.getTranNo());
		l2processorDTO.getChdrpf().setSinstamt01(l2processorDTO.getChdrpf().getSinstamt01().add(l2processorDTO.getPremdiff()));
	
		if(l2processorDTO.getChdrpf().getSinstamt01().equals(BigDecimal.ZERO)) {
			l2processorDTO.getChdrpf().setSinstamt02(BigDecimal.ZERO);
		}
		
		if(l2processorDTO.getChdrpf().getSinstamt01()==null)		
			l2processorDTO.getChdrpf().setSinstamt01(BigDecimal.ZERO);
		
		if(l2processorDTO.getChdrpf().getSinstamt02()==null)
			l2processorDTO.getChdrpf().setSinstamt02(BigDecimal.ZERO);
		
		if(l2processorDTO.getChdrpf().getSinstamt03()==null)
			l2processorDTO.getChdrpf().setSinstamt03(BigDecimal.ZERO);
		
		
		if(l2processorDTO.getChdrpf().getSinstamt04()==null)
			l2processorDTO.getChdrpf().setSinstamt04(BigDecimal.ZERO);
		
		
		if(l2processorDTO.getChdrpf().getSinstamt05()==null)
			l2processorDTO.getChdrpf().setSinstamt05(BigDecimal.ZERO);
		
		
		if(l2processorDTO.getChdrpf().getSinstamt06()==null)
			l2processorDTO.getChdrpf().setSinstamt06(BigDecimal.ZERO);	
		
		
		l2processorDTO.getChdrpf().setSinstamt06(l2processorDTO.getChdrpf().getSinstamt01().add(l2processorDTO.getChdrpf().getSinstamt02()).add(l2processorDTO.getChdrpf().getSinstamt03()).add(l2processorDTO.getChdrpf().getSinstamt04()));

		l2processorDTO.setChdrpfInsert(l2processorDTO.getChdrpf());
		l2WaiverRerateControlTotalDTO.setControlTotal03(l2WaiverRerateControlTotalDTO.getControlTotal03().add(l2processorDTO.getPremdiff()));
		l2WaiverRerateControlTotalDTO.setControlTotal08(l2WaiverRerateControlTotalDTO.getControlTotal08().add(BigDecimal.ONE));
	}
	public void loadNonWopCovrStatus(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		List<String> covrStatcode = new ArrayList<>();
		List<String> covrpStatcode = new ArrayList<>();
			if(l2processorDTO.getCovrpf().getChdrcoy().equals(l2processorDTO.getChdrpf().getChdrcoy()))
			{
				covrStatcode.add(l2processorDTO.getCovrpf().getStatcode());
				covrpStatcode.add(l2processorDTO.getCovrpf().getPstatcode());
			}
		l2processorDTO.setCovrStatcode(covrStatcode);
		l2processorDTO.setCovrPstatCode(covrpStatcode);
	}	
	public Chdrpf createCopyChdrpf(Chdrpf chdrpf) {
		Chdrpf chdrpfCopy = new Chdrpf();
		chdrpfCopy.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpfCopy.setChdrpfx(chdrpf.getChdrpfx());
		chdrpfCopy.setChdrcoy(chdrpf.getChdrcoy());
		chdrpfCopy.setChdrnum(chdrpf.getChdrnum());
		chdrpfCopy.setRecode(chdrpf.getRecode());
		chdrpfCopy.setServunit(chdrpf.getServunit());
		chdrpfCopy.setCnttype(chdrpf.getCnttype());
		chdrpfCopy.setTranno(chdrpf.getTranno());
		chdrpfCopy.setTranid(chdrpf.getTranid());
		chdrpfCopy.setValidflag(chdrpf.getValidflag());
		chdrpfCopy.setCurrfrom(chdrpf.getCurrfrom());
		chdrpfCopy.setCurrto(chdrpf.getCurrto());
		chdrpfCopy.setProctrancd(chdrpf.getProctrancd());
		chdrpfCopy.setProcflag(chdrpf.getProcflag());
		chdrpfCopy.setProcid(chdrpf.getProcid());
		chdrpfCopy.setStatcode(chdrpf.getStatcode());
		chdrpfCopy.setStatreasn(chdrpf.getStatreasn());
		chdrpfCopy.setStatdate(chdrpf.getStatdate());
		chdrpfCopy.setStattran(chdrpf.getStattran());
		chdrpfCopy.setTranlused(chdrpf.getTranlused());
		chdrpfCopy.setOccdate(chdrpf.getOccdate());
		chdrpfCopy.setCcdate(chdrpf.getCcdate());
		chdrpfCopy.setCrdate(chdrpf.getCrdate());
		chdrpfCopy.setAnnamt01(chdrpf.getAnnamt01());
		chdrpfCopy.setAnnamt02(chdrpf.getAnnamt02());
		chdrpfCopy.setAnnamt03(chdrpf.getAnnamt03());
		chdrpfCopy.setAnnamt04(chdrpf.getAnnamt04());
		chdrpfCopy.setAnnamt05(chdrpf.getAnnamt05());
		chdrpfCopy.setAnnamt06(chdrpf.getAnnamt06());
		chdrpfCopy.setRnltype(chdrpf.getRnltype());
		chdrpfCopy.setRnlnots(chdrpf.getRnlnots());
		chdrpfCopy.setRnlnotto(chdrpf.getRnlnotto());
		chdrpfCopy.setRnlattn(chdrpf.getRnlattn());
		chdrpfCopy.setRnldurn(chdrpf.getRnldurn());
		chdrpfCopy.setReptype(chdrpf.getReptype());
		chdrpfCopy.setRepnum(chdrpf.getRepnum());
		chdrpfCopy.setCownpfx(chdrpf.getCownpfx());
		chdrpfCopy.setCowncoy(chdrpf.getCowncoy());
		chdrpfCopy.setCownnum(chdrpf.getCownnum());
		chdrpfCopy.setJownnum(chdrpf.getJownnum());
		chdrpfCopy.setPayrpfx(chdrpf.getPayrpfx());
		chdrpfCopy.setPayrcoy(chdrpf.getPayrcoy());
		chdrpfCopy.setPayrnum(chdrpf.getPayrnum());
		chdrpfCopy.setDesppfx(chdrpf.getDesppfx());
		chdrpfCopy.setDespcoy(chdrpf.getDespcoy());
		chdrpfCopy.setDespnum(chdrpf.getDespnum());
		chdrpfCopy.setAsgnpfx(chdrpf.getAsgnpfx());
		chdrpfCopy.setAsgncoy(chdrpf.getAsgncoy());
		chdrpfCopy.setAsgnnum(chdrpf.getAsgnnum());
		chdrpfCopy.setCntbranch(chdrpf.getCntbranch());
		chdrpfCopy.setAgntpfx(chdrpf.getAgntpfx());
		chdrpfCopy.setAgntcoy(chdrpf.getAgntcoy());
		chdrpfCopy.setAgntnum(chdrpf.getAgntnum());
		chdrpfCopy.setCntcurr(chdrpf.getCntcurr());
		chdrpfCopy.setAcctccy(chdrpf.getAcctccy());
		chdrpfCopy.setCrate(chdrpf.getCrate());
		chdrpfCopy.setPayplan(chdrpf.getPayplan());
		chdrpfCopy.setAcctmeth(chdrpf.getAcctmeth());
		chdrpfCopy.setBillfreq(chdrpf.getBillfreq());
		chdrpfCopy.setBillchnl(chdrpf.getBillchnl());
		chdrpfCopy.setCollchnl(chdrpf.getCollchnl());
		chdrpfCopy.setBillday(chdrpf.getBillday());
		chdrpfCopy.setBillmonth(chdrpf.getBillmonth());
		chdrpfCopy.setBillcd(chdrpf.getBillcd());
		chdrpfCopy.setBtdate(chdrpf.getBtdate());
		chdrpfCopy.setPtdate(chdrpf.getPtdate());
		chdrpfCopy.setPayflag(chdrpf.getPayflag());
		chdrpfCopy.setSinstfrom(chdrpf.getSinstfrom());
		chdrpfCopy.setSinstto(chdrpf.getSinstto());
		chdrpfCopy.setSinstamt01(chdrpf.getSinstamt01());
		chdrpfCopy.setSinstamt02(chdrpf.getSinstamt02());
		chdrpfCopy.setSinstamt03(chdrpf.getSinstamt03());
		chdrpfCopy.setSinstamt04(chdrpf.getSinstamt04());
		chdrpfCopy.setSinstamt05(chdrpf.getSinstamt05());
		chdrpfCopy.setSinstamt06(chdrpf.getSinstamt06());
		chdrpfCopy.setInstfrom(chdrpf.getInstfrom());
		chdrpfCopy.setInstto(chdrpf.getInstto());
		chdrpfCopy.setInstbchnl(chdrpf.getInstbchnl());
		chdrpfCopy.setInstcchnl(chdrpf.getInstcchnl());
		chdrpfCopy.setInstfreq(chdrpf.getInstfreq());
		chdrpfCopy.setInsttot01(chdrpf.getInsttot01());
		chdrpfCopy.setInsttot02(chdrpf.getInsttot02());
		chdrpfCopy.setInsttot03(chdrpf.getInsttot03());
		chdrpfCopy.setInsttot04(chdrpf.getInsttot04());
		chdrpfCopy.setInsttot05(chdrpf.getInsttot05());
		chdrpfCopy.setInsttot06(chdrpf.getInsttot06());
		chdrpfCopy.setInstpast01(chdrpf.getInstpast01());
		chdrpfCopy.setInstpast02(chdrpf.getInstpast02());
		chdrpfCopy.setInstpast03(chdrpf.getInstpast03());
		chdrpfCopy.setInstpast04(chdrpf.getInstpast04());
		chdrpfCopy.setInstpast05(chdrpf.getInstpast05());
		chdrpfCopy.setInstpast06(chdrpf.getInstpast06());
		chdrpfCopy.setInstjctl(chdrpf.getInstjctl());
		chdrpfCopy.setNofoutinst(chdrpf.getNofoutinst());
		chdrpfCopy.setOutstamt(chdrpf.getOutstamt());
		chdrpfCopy.setBilldate01(chdrpf.getBilldate01());
		chdrpfCopy.setBilldate02(chdrpf.getBilldate02());
		chdrpfCopy.setBilldate03(chdrpf.getBilldate03());
		chdrpfCopy.setBilldate04(chdrpf.getBilldate04());
		chdrpfCopy.setBillamt01(chdrpf.getBillamt01());
		chdrpfCopy.setBillamt02(chdrpf.getBillamt02());
		chdrpfCopy.setBillamt03(chdrpf.getBillamt03());
		chdrpfCopy.setBillamt04(chdrpf.getBillamt04());
		chdrpfCopy.setFacthous(chdrpf.getFacthous());
		chdrpfCopy.setBankkey(chdrpf.getBankkey());
		chdrpfCopy.setBankacckey(chdrpf.getBankacckey());
		chdrpfCopy.setDiscode01(chdrpf.getDiscode01());
		chdrpfCopy.setDiscode02(chdrpf.getDiscode02());
		chdrpfCopy.setDiscode03(chdrpf.getDiscode03());
		chdrpfCopy.setDiscode04(chdrpf.getDiscode04());
		chdrpfCopy.setGrupkey(chdrpf.getGrupkey());
		chdrpfCopy.setMembsel(chdrpf.getMembsel());
		chdrpfCopy.setAplsupr(chdrpf.getAplsupr());
		chdrpfCopy.setAplspfrom(chdrpf.getAplspfrom());
		chdrpfCopy.setAplspto(chdrpf.getAplspto());
		chdrpfCopy.setBillsupr(chdrpf.getBillsupr());
		chdrpfCopy.setBillspfrom(chdrpf.getBillspfrom());
		chdrpfCopy.setBillspto(chdrpf.getBillspto());
		chdrpfCopy.setCommsupr(chdrpf.getCommsupr());
		chdrpfCopy.setCommspfrom(chdrpf.getCommspfrom());
		chdrpfCopy.setCommspto(chdrpf.getCommspto());
		chdrpfCopy.setLapssupr(chdrpf.getLapssupr());
		chdrpfCopy.setLapsspfrom(chdrpf.getLapsspfrom());
		chdrpfCopy.setLapsspto(chdrpf.getLapsspto());
		chdrpfCopy.setMailsupr(chdrpf.getMailsupr());
		chdrpfCopy.setMailspfrom(chdrpf.getMailspfrom());
		chdrpfCopy.setMailspto(chdrpf.getMailspto());
		chdrpfCopy.setNotssupr(chdrpf.getNotssupr());
		chdrpfCopy.setNotsspfrom(chdrpf.getNotsspfrom());
		chdrpfCopy.setNotsspto(chdrpf.getNotsspto());
		chdrpfCopy.setRnwlsupr(chdrpf.getRnwlsupr());
		chdrpfCopy.setRnwlspfrom(chdrpf.getRnwlspfrom());
		chdrpfCopy.setRnwlspto(chdrpf.getRnwlspto());
		chdrpfCopy.setCampaign(chdrpf.getCampaign());
		chdrpfCopy.setSrcebus(chdrpf.getSrcebus());
		chdrpfCopy.setNofrisks(chdrpf.getNofrisks());
		chdrpfCopy.setJacket(chdrpf.getJacket());
		chdrpfCopy.setIsam01(chdrpf.getIsam01());
		chdrpfCopy.setIsam02(chdrpf.getIsam02());
		chdrpfCopy.setIsam03(chdrpf.getIsam03());
		chdrpfCopy.setIsam04(chdrpf.getIsam04());
		chdrpfCopy.setIsam05(chdrpf.getIsam05());
		chdrpfCopy.setIsam06(chdrpf.getIsam06());
		chdrpfCopy.setPstcde(chdrpf.getPstcde());
		chdrpfCopy.setPstrsn(chdrpf.getPstrsn());
		chdrpfCopy.setPsttrn(chdrpf.getPsttrn());
		chdrpfCopy.setPstdat(chdrpf.getPstdat());
		chdrpfCopy.setPdind(chdrpf.getPdind());
		chdrpfCopy.setReg(chdrpf.getReg());
		chdrpfCopy.setStca(chdrpf.getStca());
		chdrpfCopy.setStcb(chdrpf.getStcb());
		chdrpfCopy.setStcc(chdrpf.getStcc());
		chdrpfCopy.setStcd(chdrpf.getStcd());
		chdrpfCopy.setStce(chdrpf.getStce());
		chdrpfCopy.setMplpfx(chdrpf.getMplpfx());
		chdrpfCopy.setMplcoy(chdrpf.getMplcoy());
		chdrpfCopy.setMplnum(chdrpf.getMplnum());
		chdrpfCopy.setPoapfx(chdrpf.getPoapfx());
		chdrpfCopy.setPoacoy(chdrpf.getPoacoy());
		chdrpfCopy.setPoanum(chdrpf.getPoanum());
		chdrpfCopy.setFinpfx(chdrpf.getFinpfx());
		chdrpfCopy.setFincoy(chdrpf.getFincoy());
		chdrpfCopy.setFinnum(chdrpf.getFinnum());
		chdrpfCopy.setWvfdat(chdrpf.getWvfdat());
		chdrpfCopy.setWvtdat(chdrpf.getWvtdat());
		chdrpfCopy.setWvfind(chdrpf.getWvfind());
		chdrpfCopy.setClupfx(chdrpf.getClupfx());
		chdrpfCopy.setClucoy(chdrpf.getClucoy());
		chdrpfCopy.setClunum(chdrpf.getClunum());
		chdrpfCopy.setPolpln(chdrpf.getPolpln());
		chdrpfCopy.setChgflag(chdrpf.getChgflag());
		chdrpfCopy.setLaprind(chdrpf.getLaprind());
		chdrpfCopy.setSpecind(chdrpf.getSpecind());
		chdrpfCopy.setDueflg(chdrpf.getDueflg());
		chdrpfCopy.setBfcharge(chdrpf.getBfcharge());
		chdrpfCopy.setDishnrcnt(chdrpf.getDishnrcnt());
		chdrpfCopy.setPdtype(chdrpf.getPdtype());
		chdrpfCopy.setDishnrdte(chdrpf.getDishnrdte());
		chdrpfCopy.setStmpdtyamt(chdrpf.getStmpdtyamt());
		chdrpfCopy.setStmpdtydte(chdrpf.getStmpdtydte());
		chdrpfCopy.setPolinc(chdrpf.getPolinc());
		chdrpfCopy.setPolsum(chdrpf.getPolsum());
		chdrpfCopy.setNxtsfx(chdrpf.getNxtsfx());
		chdrpfCopy.setAvlisu(chdrpf.getAvlisu());
		chdrpfCopy.setStmdte(chdrpf.getStmdte());
		chdrpfCopy.setBillcurr(chdrpf.getBillcurr());
		chdrpfCopy.setTfrswused(chdrpf.getTfrswused());
		chdrpfCopy.setTfrswleft(chdrpf.getTfrswleft());
		chdrpfCopy.setLastswdate(chdrpf.getLastswdate());
		chdrpfCopy.setMandref(chdrpf.getMandref());
		chdrpfCopy.setCntiss(chdrpf.getCntiss());
		chdrpfCopy.setCntrcv(chdrpf.getCntrcv());
		chdrpfCopy.setCoppn(chdrpf.getCoppn());
		chdrpfCopy.setCotype(chdrpf.getCotype());
		chdrpfCopy.setCovernt(chdrpf.getCovernt());
		chdrpfCopy.setDocnum(chdrpf.getDocnum());
		chdrpfCopy.setDtecan(chdrpf.getDtecan());
		chdrpfCopy.setQuoteno(chdrpf.getQuoteno());
		chdrpfCopy.setRnlsts(chdrpf.getRnlsts());
		chdrpfCopy.setSustrcde(chdrpf.getSustrcde());
		chdrpfCopy.setBankcode(chdrpf.getBankcode());
		chdrpfCopy.setPndate(chdrpf.getPndate());
		chdrpfCopy.setSubsflg(chdrpf.getSubsflg());
		chdrpfCopy.setHrskind(chdrpf.getHrskind());
		chdrpfCopy.setSlrypflg(chdrpf.getSlrypflg());
		chdrpfCopy.setTakovrflg(chdrpf.getTakovrflg());
		chdrpfCopy.setGprnltyp(chdrpf.getGprnltyp());
		chdrpfCopy.setGprmnths(chdrpf.getGprmnths());
		chdrpfCopy.setCoysrvac(chdrpf.getCoysrvac());
		chdrpfCopy.setMrksrvac(chdrpf.getMrksrvac());
		chdrpfCopy.setPolschpflg(chdrpf.getPolschpflg());
		chdrpfCopy.setAdjdate(chdrpf.getAdjdate());
		chdrpfCopy.setPtdateab(chdrpf.getPtdateab());
		chdrpfCopy.setLmbrno(chdrpf.getLmbrno());
		chdrpfCopy.setLheadno(chdrpf.getLheadno());
		chdrpfCopy.setEffdcldt(chdrpf.getEffdcldt());
		chdrpfCopy.setPntrcde(chdrpf.getPntrcde());
		chdrpfCopy.setTaxflag(chdrpf.getTaxflag());
		chdrpfCopy.setAgedef(chdrpf.getAgedef());
		chdrpfCopy.setTermage(chdrpf.getTermage());
		chdrpfCopy.setPersoncov(chdrpf.getPersoncov());
		chdrpfCopy.setEnrolltyp(chdrpf.getEnrolltyp());
		chdrpfCopy.setSplitsubs(chdrpf.getSplitsubs());
		chdrpfCopy.setDtlsind(chdrpf.getDtlsind());
		chdrpfCopy.setZrenno(chdrpf.getZrenno());
		chdrpfCopy.setZendno(chdrpf.getZendno());
		chdrpfCopy.setZresnpd(chdrpf.getZresnpd());
		chdrpfCopy.setZrepolno(chdrpf.getZrepolno());
		chdrpfCopy.setZcomtyp(chdrpf.getZcomtyp());
		chdrpfCopy.setZrinum(chdrpf.getZrinum());
		chdrpfCopy.setZschprt(chdrpf.getZschprt());
		chdrpfCopy.setZpaymode(chdrpf.getZpaymode());
		chdrpfCopy.setUsrprf(chdrpf.getUsrprf());
		chdrpfCopy.setJobnme(chdrpf.getJobnme());
		chdrpfCopy.setDatime(chdrpf.getDatime());
		chdrpfCopy.setJobnm(chdrpf.getJobnm());
		chdrpfCopy.setQuotetype(chdrpf.getQuotetype());
		chdrpfCopy.setJobnum(chdrpf.getJobnum());
		chdrpfCopy.setNlgflg(chdrpf.getNlgflg());
		return chdrpfCopy;
	}
	public void getPayrpf(L2WaiverRerateProcessorDTO l2processorDTO) {
		l2processorDTO.setPayrpf(payrpfDAO.getPayrRecord(l2processorDTO.getChdrpf().getChdrcoy(), l2processorDTO.getChdrpf().getChdrnum()));
		if(l2processorDTO.getPayrpf() == null) {
			throw new StopRunException("No Payrpf Records Exist");
		}
	}
	public void updatePayr(L2WaiverRerateProcessorDTO l2processorDTO) {
		getPayrpf(l2processorDTO);		
		Payrpf payrpf = new Payrpf(l2processorDTO.getPayrpf());
		payrpf.setValidflag("2");
		l2processorDTO.setPayrpfUpdate(payrpf);
		l2processorDTO.getPayrpf().setValidflag("1");
		l2processorDTO.getPayrpf().setEffdate(l2processorDTO.getRerateStore());
		l2processorDTO.getPayrpf().setTranno(l2processorDTO.getTranNo());
		l2processorDTO.getPayrpf().setSinstamt01(l2processorDTO.getChdrpf().getSinstamt01());
		l2processorDTO.getPayrpf().setSinstamt02(l2processorDTO.getChdrpf().getSinstamt02());
		l2processorDTO.getPayrpf().setSinstamt03(l2processorDTO.getChdrpf().getSinstamt03());
		l2processorDTO.getPayrpf().setSinstamt04(l2processorDTO.getChdrpf().getSinstamt04());
		l2processorDTO.getPayrpf().setSinstamt05(l2processorDTO.getChdrpf().getSinstamt05());
		l2processorDTO.getPayrpf().setSinstamt06(l2processorDTO.getChdrpf().getSinstamt06());
		l2processorDTO.getPayrpf().setPstcde(l2processorDTO.getChdrpf().getPstcde());
		
		int leaddays= l2processorDTO.getLeadDays()*(-1);
		int datconResult = datCon2.plusDays(l2processorDTO.getChdrpf().getBillcd(), leaddays);
		l2processorDTO.getPayrpf().setNextdate(datconResult);
		l2processorDTO.setPayrpfInsert(l2processorDTO.getPayrpf());	
		
	}
	@SuppressWarnings("null")
	public void genericProcessing(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		String wsaaT5671Item = l2processorDTO.getBatchtrancde() + covrpf.getCrtable();
		List<Itempf> t5671List = itempfDAO.readSmartTableByTrimItemList(Companies.LIFE, "T5671", wsaaT5671Item,"IT");
		if (!t5671List.isEmpty()) {
			try {
				l2processorDTO.setT5671(new ObjectMapper().readValue(t5671List.get(0).getGenareaj(), T5671.class));
			} catch (IOException e) {
				throw new ItemParseException("Item parsed failed in Table 5671: " + wsaaT5671Item);
			}
		}
		
		IsuallDTO isuallrec = new IsuallDTO();
		isuallrec.setFunction("ACTIN");
		isuallrec.setCompany(covrpf.getChdrcoy());
		isuallrec.setChdrnum(covrpf.getChdrnum());
		isuallrec.setLife(covrpf.getLife());
		isuallrec.setCoverage(covrpf.getCoverage());
		isuallrec.setRider(covrpf.getRider());
		isuallrec.setPlanSuffix(covrpf.getPlanSuffix());
		isuallrec.setOldcovr(covrpf.getCoverage());
		isuallrec.setOldrider(covrpf.getRider());
		if (l2processorDTO.isFullyPaid()) {
			isuallrec.setFreqFactor(BigDecimal.ZERO);
		} else {
			isuallrec.setFreqFactor(BigDecimal.ONE);
		}
		isuallrec.setBatcpfx(l2processorDTO.getPrefix());
		isuallrec.setBatccoy(Companies.LIFE);
		isuallrec.setBatcbrn(l2processorDTO.getBatchbranch());
		isuallrec.setBatcactyr(l2processorDTO.getActYear());
		isuallrec.setBatcactmn(l2processorDTO.getActMonth());
		isuallrec.setBatctrcde(l2processorDTO.getBatchtrancde());
		isuallrec.setBatcbatch(l2processorDTO.getBatch());
		isuallrec.setTransactionDate(l2processorDTO.getTransactionDate());
		isuallrec.setTransactionTime(l2processorDTO.getTranTime());
		isuallrec.setUser(0);
		isuallrec.setTermid("");
		isuallrec.setEffdate(covrpf.getCrrcd());
		isuallrec.setNewTranno(covrpf.getTranno());
		isuallrec.setCovrSingp(BigDecimal.ZERO);
		isuallrec.setCovrInstprem(covrpf.getInstprem());
		isuallrec.setLanguage(l2processorDTO.getLang());
		if (l2processorDTO.getChdrpf().getPolinc() > 1) {
			isuallrec.setConvertUnlt("Y");
		}
				
		// call UNLCHG,GRVULCHG, but it's blank for TEN&RUL
		/*if (l2processorDTO.getT5671() != null) {
			List<String> subprogs = l2processorDTO.getT5671().getSubprogs();
			if (subprogs != null && !subprogs.isEmpty()) {
				for (String subprog : subprogs) {
					// call UNLCHG,GRVULCHG, but it's blank for TEN&RUL
					// if (isNE(t5671rec.subprog, SPACES)) {
					// callProgram(t5671rec.subprog[wsaaSub.toInt()], isuallrec.isuallRec);
					// }
				}
			}
		}
		 */
	}
	public void updateAgcm(Covrpf covrpf, L2WaiverRerateProcessorDTO l2processorDTO)
	 {
		List<Agcmpf> agcmList = agcmpfDAO.getAgcmpfRecord(covrpf.getChdrcoy(), covrpf.getChdrnum());
		for (Agcmpf agcmpf : agcmList) {
			if (agcmpf.getChdrnum().equals(covrpf.getChdrnum()) && agcmpf.getLife().equals(covrpf.getLife())
					&& agcmpf.getRider().equals(covrpf.getRider()) && agcmpf.getCoverage().equals(covrpf.getCoverage())
					&& agcmpf.getPlnsfx().equals(covrpf.getPlanSuffix())) {
				agcmUpdateLoop(covrpf, agcmpf, l2processorDTO);
				return;
			}
		}
	}
	public void updateUndr(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO) {

		CrtundwrtDTO crtundwrec = new CrtundwrtDTO();
		crtundwrec.setClntnum(l2processorDTO.getLifepf().getLifcnum());
		crtundwrec.setCoy(covrpf.getChdrcoy());
		crtundwrec.setCurrcode(l2processorDTO.getChdrpf().getCntcurr());
		crtundwrec.setChdrnum(covrpf.getChdrnum());
		crtundwrec.setCrtable(covrpf.getCrtable());
		crtundwrec.setCnttyp(l2processorDTO.getChdrpf().getCnttype());
		crtundwrec.setFunction("DEL");
		try {
			crtUndwrt.processCrtundwrt(crtundwrec);
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Exception in crtundwrt: " + e.getMessage());
		}

		crtundwrec.setClntnum(l2processorDTO.getLifepf().getLifcnum());
		crtundwrec.setCoy(covrpf.getChdrcoy());
		crtundwrec.setChdrnum(covrpf.getChdrnum());
		crtundwrec.setLife(covrpf.getLife());
		crtundwrec.setCrtable(covrpf.getCrtable());
		crtundwrec.setBatctrcde(l2processorDTO.getBatchtrancde());
		crtundwrec.setSumins(l2processorDTO.getCovrSumins());
		crtundwrec.setCnttyp(l2processorDTO.getChdrpf().getCnttype());
		crtundwrec.setCurrcode(l2processorDTO.getChdrpf().getCntcurr());
		crtundwrec.setFunction("ADD");
		try {
			crtUndwrt.processCrtundwrt(crtundwrec);
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Exception in crtundwrt: " + e.getMessage());
		}
	}
	public void writeNewAgcm(Covrpf covrpf, Agcmpf agcmpf, L2WaiverRerateProcessorDTO l2processorDTO) {
		Agcmpf agcmpfInsert = new Agcmpf(agcmpf);
		agcmpfInsert.setTranno(l2processorDTO.getTranNo());
		agcmpfInsert.setCurrfrom(l2processorDTO.getRerateStore());
		agcmpfInsert.setCurrto(l2processorDTO.getMaxDate());
		agcmpfInsert.setValidflag("1");
		if (covrpf.getPcesdte() != covrpf.getRrtdat()) {
			agcmpfInsert.setAnnprem(agentAnnprem(covrpf, agcmpfInsert, l2processorDTO));
		}
		l2processorDTO.setAgcmpfInsert(agcmpfInsert);
	}
	public void agcmUpdateLoop(Covrpf covrpf, Agcmpf agcmpf, L2WaiverRerateProcessorDTO l2processorDTO) {
		if (!"Y".equals(agcmpf.getDormflag())) {
			Agcmpf agcmpfUpdate = new Agcmpf(agcmpf);
			agcmpfUpdate.setCurrto(l2processorDTO.getRerateStore());
			agcmpfUpdate.setValidflag("2");
			l2processorDTO.setAgcmpfUpdate(agcmpfUpdate);
			writeNewAgcm(covrpf, agcmpf, l2processorDTO);
		}
	}
	public void findBasicAgent(Agcmpf agcmpf,L2WaiverRerateProcessorDTO l2processorDTO) {
		agcmpf.setChdrcoy(agcmpf.getChdrcoy());
		agcmpf.setChdrnum(agcmpf.getChdrnum());
		agcmpf.setLife(agcmpf.getLife());
		agcmpf.setCoverage(agcmpf.getCoverage());
		agcmpf.setRider(agcmpf.getRider());
		agcmpf.setPlnsfx(agcmpf.getPlnsfx());
		agcmpf.setAgntnum(l2processorDTO.getCedAgent());
		List<Agcmpf> agcmList = agcmpfDAO.getAgcmpfRecordByAgntnum(agcmpf);
		if (!agcmList.isEmpty()) {
			agcmpf = agcmList.get(0);
			if ("B".equals(agcmpf.getOvrdcat())) {
				l2processorDTO.setAgntFound( true);
				l2processorDTO.setAgntnum(agcmpf.getAgntnum());
			} else {
				l2processorDTO.setCedAgent(agcmpf.getCedagent());
			}
		}
	}

	public BigDecimal agentAnnprem(Covrpf covrpf, Agcmpf agcmpf, L2WaiverRerateProcessorDTO l2processorDTO) {
		l2processorDTO.setAgntnum(agcmpf.getAgntnum());
		if ("O".equals(agcmpf.getOvrdcat())) {
			l2processorDTO.setAgntFound(false);
			l2processorDTO.setCedAgent(agcmpf.getCedagent());
			while (!l2processorDTO.isAgntFound()) {
				findBasicAgent(agcmpf,l2processorDTO);
			}
		}
		List<Pcddpf> pcddpfList = pcddpfDAO.getPcddpfRecord(l2processorDTO.getChdrcoy(), l2processorDTO.getChdrnum());
		BigDecimal wsaaAnnprem;
		for (Pcddpf pcddpf : pcddpfList) {
			if (pcddpf.getAgntNum().equals(l2processorDTO.getAgntnum())) {
				wsaaAnnprem = BigDecimal.ZERO;
				wsaaAnnprem = agcmpf.getAnnprem();
				if (l2processorDTO.getT5687().getZrrcombas() != null && !l2processorDTO.getT5687().getZrrcombas().trim().isEmpty()) {
					wsaaAnnprem = covrpf.getZbinstprem().multiply(new BigDecimal(l2processorDTO.getChdrpf().getBillfreq())
							.multiply(pcddpf.getSplitC().divide(new BigDecimal(100))));
				} else {
					wsaaAnnprem = covrpf.getInstprem().multiply(new BigDecimal(l2processorDTO.getChdrpf().getBillfreq()))
							.multiply(pcddpf.getSplitC().divide(new BigDecimal(100)));
				}
				if (wsaaAnnprem.compareTo(BigDecimal.ZERO) != 0) {
					wsaaAnnprem = agcmpf.getAnnprem();
				}
				return wsaaAnnprem;
			}
		}
		return null;
	}

	public Covrpf updateCovr(Covrpf covr, L2WaiverRerateProcessorDTO l2processorDTO) throws IOException, ParseException
	{
		l2processorDTO.setCovrUpdated(true);
		l2processorDTO.setRerateStore(covr.getRrtdat());
		l2processorDTO.setRerateType("1");
		l2processorDTO.setInstPrem(BigDecimal.ZERO);
		l2processorDTO.setZbInstPrem(BigDecimal.ZERO);
		l2processorDTO.setZlInstPrem(BigDecimal.ZERO);
		
		List<Covrpf> updateCovrrnlList =null;
		
		if (covr.getRrtdat().equals(covr.getPremCessDate())) {
			l2processorDTO.setFullyPaid(true);
			pastCeaseDate(covr,l2processorDTO);
			return covr;
		}	
		l2processorDTO.setFullyPaid(false);
		calculatePremium(covr,l2processorDTO);
		covr.setCurrto(l2processorDTO.getRerateStore());
		covr.setVarsi(BigDecimal.ZERO);
		covr.setValidflag("2");
		
		updateCovrrnlList = new ArrayList<>();
		
		updateCovrrnlList.add(covr);
		l2processorDTO.setCovrUpdate(covr);

		/* Read LEXTBRR for unexpired records.*/
		checkLexts(covr,l2processorDTO);
		statistics(l2processorDTO);
		return covr;
	}
	public void checkLexts(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Covrpf covrInsert = new Covrpf(covrpf);
		l2processorDTO.setRerateStore(l2processorDTO.getEarliestDate());
		List<Lextpf> lextpfList = lextpfDAO.getLextpfListRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),
				covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider());
		for (Lextpf l : lextpfList) {
			if (l.getExtcd() < l2processorDTO.getRateFrom() && l.getExtcd() > covrpf.getRrtdat()) {
				l2processorDTO.setRateFrom(l.getExtcd());
			}
		}
		covrInsert.setRrtdat(l2processorDTO.getRateFrom());
		covrInsert.setRrtfrm(covrpf.getRrtdat());
		
		if (l2processorDTO.getPremGuarPeriod()==0) {
			covrInsert.setRrtfrm(covrpf.getRrtdat());
		}
		else {			
		Integer datconResult = datCon2.plusYears(covrpf.getCrrcd(), l2processorDTO.getPremGuarPeriod());
			
			if (datconResult<covrpf.getRrtdat()) {
				covrInsert.setRrtfrm(covrpf.getRrtdat());
			}
			else {
				covrInsert.setRrtfrm(covrpf.getCrrcd());
			}
		}		
		
		//checkMgp(covrpf,l2processorDTO);
		if (l2processorDTO.getPremMeth().trim() != null || !l2processorDTO.getPremMeth().trim().equals("")) 
		{
			if(null != l2processorDTO.getPremiumDTO().getCalcPrem()) {
				l2processorDTO.setInstPrem(l2processorDTO.getPremiumDTO().getCalcPrem().divide(new BigDecimal(l2processorDTO.getChdrpf().getPolinc())));
			}
			if(null != l2processorDTO.getPremiumDTO().getCalcBasPrem()) {
				l2processorDTO.setZbInstPrem(l2processorDTO.getPremiumDTO().getCalcBasPrem().divide(new BigDecimal(l2processorDTO.getChdrpf().getPolinc())));
			}
			if(null != l2processorDTO.getPremiumDTO().getCalcLoaPrem()) {
				l2processorDTO.setZlInstPrem(l2processorDTO.getPremiumDTO().getCalcLoaPrem().divide(new BigDecimal(l2processorDTO.getChdrpf().getPolinc())));
			}
			
			if (covrpf.getPlanSuffix()== 0 && l2processorDTO.getChdrpf().getPolinc() != 1) {
				l2processorDTO.setInstPrem(l2processorDTO.getInstPrem().multiply(new BigDecimal(l2processorDTO.getChdrpf().getPolsum())));
				l2processorDTO.setZbInstPrem(l2processorDTO.getZbInstPrem().multiply(new BigDecimal(l2processorDTO.getChdrpf().getPolsum())));
				l2processorDTO.setZlInstPrem(l2processorDTO.getZlInstPrem().multiply(new BigDecimal(l2processorDTO.getChdrpf().getPolsum())));
			}
			if (l2processorDTO.getInstPrem().compareTo(BigDecimal.ZERO) != 0) {
				l2processorDTO.setInstPrem(callRounding(l2processorDTO.getInstPrem(),l2processorDTO));
			}
			if (l2processorDTO.getZbInstPrem().compareTo(BigDecimal.ZERO) != 0) {
				l2processorDTO.setZbInstPrem(callRounding(l2processorDTO.getZbInstPrem(),l2processorDTO));
			}
			if (l2processorDTO.getZlInstPrem().compareTo(BigDecimal.ZERO) != 0) {
				l2processorDTO.setZlInstPrem(callRounding(l2processorDTO.getZlInstPrem(),l2processorDTO));
			}
			l2processorDTO.setPremdiff((l2processorDTO.getPremdiff() == null ? BigDecimal.ZERO : l2processorDTO.getPremdiff())
					.add(l2processorDTO.getInstPrem().subtract(covrpf.getInstprem())));
			
			covrInsert.setInstprem(l2processorDTO.getInstPrem());
			covrInsert.setZbinstprem(l2processorDTO.getZbInstPrem());
			covrInsert.setZlinstprem(l2processorDTO.getZlInstPrem());
		}
			checkForIncrease(covrpf, l2processorDTO);
			
			if (covrpf.getRrtdat() > covrpf.getPcesdte()) {
				covrInsert.setRrtdat(covrpf.getPcesdte());
			}
			if (covrpf.getRrtfrm() > covrpf.getPcesdte()) {
				covrInsert.setRrtfrm(covrpf.getPcesdte());
			}
			if (covrpf.getCpiDate()!=l2processorDTO.getMaxDate()) {
				covrInsert.setCpiDate(covrpf.getRrtdat());
			}
			covrInsert.setValidflag("1");
			l2processorDTO.setTranNo(l2processorDTO.getChdrpf().getTranno() + 1);
			covrInsert.setTranno(l2processorDTO.getTranNo());
			covrInsert.setCurrfrom(l2processorDTO.getRerateStore());
			covrInsert.setCurrto(l2processorDTO.getMaxDate());
			
			if(null != l2processorDTO.getPremiumDTO().getRiskPrem()) {
				covrInsert.setRiskprem(l2processorDTO.getPremiumDTO().getRiskPrem());
			}
			else
				covrInsert.setRiskprem(BigDecimal.ZERO);
			boolean stampDutyflag =  itemService.isFeatureExist(Companies.LIFE, "NBPROP01", "IT");
			if(stampDutyflag) {
				covrInsert.setZstpduty01(l2processorDTO.getPremiumDTO().getZstpduty01());
				covrInsert.setZclstate(l2processorDTO.getPremiumDTO().getRstate01());
			}
			l2processorDTO.setCovrInsert(covrInsert);
			boolean isFeatureConfig = itemService.isFeatureExist(Companies.LIFE, "BTPRO012", "IT");
			if(isFeatureConfig){
				checkRerateOnExistingWaiverClaim(covrInsert,l2processorDTO);
			}
			l2WaiverRerateControlTotalDTO.setControlTotal02(l2WaiverRerateControlTotalDTO.getControlTotal02().add(BigDecimal.ONE));
		
	}
	public void checkRerateOnExistingWaiverClaim(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO){
		if(l2processorDTO.getTr517().getZrwvflgs().get(4).equals("Y")){
			List<Regppf> regppfList = new ArrayList<>();
			List<Regppf>  rgpytypelist = regpfDAO.readRegpByRgpytype(covrpf.getChdrnum(), covrpf.getCrtable());
			
			if(rgpytypelist !=null && !rgpytypelist.isEmpty()){
				for(Regppf regppf : rgpytypelist)
				{
					regppf.setPymt(covrpf.getSumins());
					if (l2processorDTO.getTr517().getZrwvflgs().get(0).equals("Y")) 
					{
						BigDecimal computPaymt=regppf.getPymt();
						checkCalcCompTax(covrpf, l2processorDTO.getIncrpf(),l2processorDTO);
						
						BigDecimal amountIn=computPaymt.add(covrpf.getInstprem().add(l2processorDTO.getTax())).multiply(new BigDecimal(l2processorDTO.getPayrpf().getBillfreq())).divide(new BigDecimal(regppf.getRegpayfreq()));
						callRounding(amountIn,l2processorDTO);
						regppf.setPymt(callRounding(amountIn,l2processorDTO));
						if(regppf.getAdjamt()!=BigDecimal.ZERO)
						{
							BigDecimal temp = regppf.getPymt().add(regppf.getAdjamt());
							regppf.setNetamt(temp);
						}else{
							regppf.setNetamt(regppf.getPymt());	
						}
						BigDecimal interMed=regppf.getPymt().divide(covrpf.getSumins());
						BigDecimal percent= interMed.multiply(new BigDecimal(100));
						regppf.setPrcnt(percent);						
					}

					regppf.setUser(0);
					regppf.setTransactionDate(l2processorDTO.getTransactionDate());
					regppf.setTransactionTime(l2processorDTO.getTranTime());
					regppfList.add(regppf);	
					l2processorDTO.setUpdateRegppf(regppf);
				}				
			}
			else
			{
				l2processorDTO.setUpdateRegppf(null);
			}
		}
		
	}
	public void checkForIncrease(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Incrpf incrpf = null;
		List<Incrpf> incrpfList = incrpfDAO.readInValidIncrpf(covrpf.getChdrcoy(), covrpf.getChdrnum(),
				covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix());
		if (incrpfList != null && !incrpfList.isEmpty()) {
			incrpf = incrpfList.get(0);
		}

		if (incrpf == null) {
			return;
		}
		incrpf.setLastInst(incrpf.getNewinst());
		incrpf.setZblastinst(incrpf.getZbnewinst());
		incrpf.setZllastinst(incrpf.getZlnewinst());
		incrpf.setNewinst(incrpf.getNewinst().add(covrpf.getInstprem().subtract(covrpf.getInstprem())));
		incrpf.setZbnewinst(incrpf.getZbnewinst().add(covrpf.getZbinstprem().subtract(covrpf.getZbinstprem())));
		incrpf.setZlnewinst(incrpf.getZlnewinst().add(covrpf.getZlinstprem().subtract(covrpf.getZlinstprem())));
		incrpf.setTranno(l2processorDTO.getChdrpf().getTranno() + 1);
		incrpf.setStatcode(covrpf.getStatcode());
		incrpf.setPstatcode(covrpf.getPstatcode());
		incrpf.setCrrcd(l2processorDTO.getEffDate());
		incrpf.setTransactionDate(l2processorDTO.getTransactionDate());
		incrpf.setTransactionTime(l2processorDTO.getTranTime());
		incrpf.setTermid(covrpf.getTermid());
		incrpf.setUser(0);
		incrpf.setZstpduty01(incrpf.getNewinst().add(covrpf.getZstpduty01().subtract(covrpf.getZstpduty01())));

		l2processorDTO.setIncrInsert(incrpf);
	}
	public void checkMgp(Covrpf c,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		if (l2processorDTO.getPremGuarPeriod()==0) {
			c.setRrtfrm(c.getRrtdat());
		}
		else {			
		Integer datconResult = datCon2.plusYears(c.getCrrcd(), l2processorDTO.getPremGuarPeriod());
			
			if (datconResult<c.getRrtdat()) {
				c.setRrtfrm(c.getRrtdat());
			}
			else {
				c.setRrtfrm(c.getCrrcd());
			}
		}
	}
	public void statistics(L2WaiverRerateProcessorDTO l2processorDTO){
		LifsttrDTO lifsttrrec = new LifsttrDTO();
		lifsttrrec.setBatccoy(Companies.LIFE);
		lifsttrrec.setBatcbrn(l2processorDTO.getBatchbranch());
		lifsttrrec.setBatcacty(l2processorDTO.getAccYear());
		lifsttrrec.setBatcactmn(l2processorDTO.getAccMonth());
		lifsttrrec.setBatctrcde(l2processorDTO.getBatchtrancde());
	    lifsttrrec.setBatcbatch(l2processorDTO.getBatch());
		lifsttrrec.setChdrcoy(l2processorDTO.getChdrpf().getChdrcoy());
		lifsttrrec.setChdrnum(l2processorDTO.getChdrpf().getChdrnum());
		lifsttrrec.setTranno(l2processorDTO.getTranNo());
		lifsttrrec.setTrannor(99999);
		lifsttrrec.setAgntnum("");
		lifsttrrec.setOldAgntnum("");
		try {
			lifSttr.processLifsttr(lifsttrrec);
		} catch (IOException e) {
			throw new SubroutineIOException(" IOException in lifsttr: " + e.getMessage());
		}
	}
	public void moveT5687(int index,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		l2processorDTO.setAnnvMethod(l2processorDTO.getT5687Annvry().get(index));
		l2processorDTO.setPremMeth(l2processorDTO.getT5687Premmeth().get(index));
		l2processorDTO.setJlPremMeth(l2processorDTO.getT5687JlPremmeth().get(index));
		l2processorDTO.setPremGuarPeriod(l2processorDTO.getT5687Pguarp().get(index));
		l2processorDTO.setRtrnwfreq(new BigDecimal(l2processorDTO.getT5687Rtrnwfreq().get(index)));
		l2processorDTO.setZrrCombas(l2processorDTO.getT5687ZrrCombas().get(index));
	}
	public void calculatePremium(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO) throws IOException, ParseException
	{
		l2processorDTO.setDateFound(false);
		int index=0;
		while (index<(l2processorDTO.getT5687Crtable().size()) && !l2processorDTO.isDateFound())
		{
			if(covrpf.getCrtable().equals(l2processorDTO.getT5687Crtable().get(index).trim())) {
				if (covrpf.getCrrcd()>=l2processorDTO.getT5687Itmfrm().get(index)) {
					l2processorDTO.setDateFound(true);
					moveT5687(index,l2processorDTO);
				}				
			}
			index++;			
		}	
		if (!l2processorDTO.isDateFound()) {
			throw new ItemNotfoundException("L2WaiverRerate: Item not found" );
		}
		if (l2processorDTO.getPremMeth().trim().equals("")) {
			return ;
		}
		/*  Determine whether this is TRUE or LEXT triggered re-rate*/
		/*  date*/
		setRerateType(covrpf,l2processorDTO);
		List<Lifepf> lifepfList=lifepfDAO.getLifeRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
		if(!lifepfList.isEmpty())
		{
			l2processorDTO.setLifepf(lifepfList.get(0));
		}
		l2processorDTO.getPremiumDTO().setLsex(l2processorDTO.getLifepf().getCltsex());
		
		calculateAnb(l2processorDTO.getLifepf().getCltdob(),l2processorDTO);
		
		l2processorDTO.getPremiumDTO().setLage(l2processorDTO.getAnb());
		checkJointLife(covrpf,l2processorDTO);
		
		//read Payrpf details
		List<Payrpf> payrList = payrpfDAO.searchPayrRecord(covrpf.getChdrcoy(), covrpf.getChdrnum());
		if (!payrList.isEmpty()) {
			Payrpf payrpf = payrList.get(0);
			l2processorDTO.setPayrpf(payrpf);
			l2processorDTO.setBillfreq(Integer.parseInt(payrpf.getBillfreq()));
			l2processorDTO.getPremiumDTO().setMop(payrpf.getBillchnl());
			l2processorDTO.getPremiumDTO().setBillfreq(payrpf.getBillfreq());
		}
		l2processorDTO.getPremiumDTO().setCrtable(covrpf.getCrtable());
		l2processorDTO.getPremiumDTO().setChdrChdrcoy(covrpf.getChdrcoy());
		l2processorDTO.getPremiumDTO().setChdrChdrnum(covrpf.getChdrnum());
		l2processorDTO.getPremiumDTO().setLifeLife(covrpf.getLife());
		l2processorDTO.getPremiumDTO().setLifeJlife(covrpf.getJlife());
		l2processorDTO.getPremiumDTO().setCovrRider(covrpf.getRider());
		l2processorDTO.getPremiumDTO().setCovrCoverage(covrpf.getCoverage());
		l2processorDTO.getPremiumDTO().setEffectdt(l2processorDTO.getLastRerateDate());
		l2processorDTO.getPremiumDTO().setMortcls(covrpf.getMortcls());
		l2processorDTO.getPremiumDTO().setCurrcode(covrpf.getPrmcur());
		l2processorDTO.getPremiumDTO().setTermdate(covrpf.getPremCessDate());
		
		double freqFactor=datCon3.getYearsDifference(l2processorDTO.getPremiumDTO().getEffectdt(), l2processorDTO.getPremiumDTO().getTermdate());
		l2processorDTO.getPremiumDTO().setDuration((int) (freqFactor + 0.99999));
			
		l2processorDTO.getPremiumDTO().setRatingdate(covrpf.getRrtfrm());
		l2processorDTO.getPremiumDTO().setReRateDate(covrpf.getRrtdat());
		l2processorDTO.getPremiumDTO().setCalcPrem(covrpf.getInstprem());
		l2processorDTO.getPremiumDTO().setCalcBasPrem(covrpf.getZbinstprem());
		l2processorDTO.getPremiumDTO().setCalcLoaPrem(covrpf.getZlinstprem());
		covrpf.setSumins(l2processorDTO.getCovrSumins());
		
		if (covrpf.getPlanSuffix()==0 && l2processorDTO.getChdrpf().getPolinc()!=1) {
			l2processorDTO.getPremiumDTO().setSumin(l2processorDTO.getPremiumDTO().getSumin().divide(new BigDecimal(l2processorDTO.getChdrpf().getPolsum())));
		}
		else {
			l2processorDTO.getPremiumDTO().setSumin(covrpf.getSumins());
		}
		l2processorDTO.getPremiumDTO().setSumin(l2processorDTO.getPremiumDTO().getSumin().multiply(new BigDecimal(l2processorDTO.getChdrpf().getPolinc())));
		l2processorDTO.getPremiumDTO().setFunction("CALC");
		getAnny(covrpf,l2processorDTO);
		
		VpxacblDTO vpxacblDTO = new VpxacblDTO();
		VpxlextDTO vpxlextDTO = new VpxlextDTO();
		VPMSinfoDTO vpmsInfoDTO = new VPMSinfoDTO();
		vpmsInfoDTO.setCoy(Companies.LIFE);
		//get premium subroutine name based on the premium method fetched from T5687 for coverage/rider
		getSubroutineName(l2processorDTO);
		vpmsInfoDTO.setModelName(l2processorDTO.getT5675().getPremsubr());
		vpmsInfoDTO.setTransDate(l2processorDTO.getEffDate()+"");
		l2processorDTO.getPremiumDTO().setCnttype(l2processorDTO.getChdrpf().getCnttype());
		l2processorDTO.getPremiumDTO().setLanguage(l2processorDTO.getLang());
		getOccpClass(l2processorDTO);
		if (!(l2processorDTO.getVpmsFlag() && (vpmsCaller.checkVPMSModel(l2processorDTO.getT5675().getPremsubr().trim())
				|| l2processorDTO.getT5675().getPremsubr().trim().equals("PMEX")))) {
			l2processorDTO=callSubroutine(l2processorDTO,vpmsInfoDTO,null,null);
		}
		else
		{
			vpxlextDTO.setFunction("INIT");
			vpxlextDTO=vpxLext.processVpxlext(vpxlextDTO.getFunction(), l2processorDTO.getPremiumDTO());
						
			VpxchdrDTO vpxchdrDTO = new VpxchdrDTO();
			vpxchdrDTO.setFunction("INIT");
			vpxchdrDTO=vpxChdr.callInit(l2processorDTO.getPremiumDTO());
			l2processorDTO.getPremiumDTO().setRstaflag(vpxchdrDTO.getRstaflag());
			vpxacblDTO=vpxAcbl.processVpxacbl(l2processorDTO.getPremiumDTO());
			l2processorDTO.getPremiumDTO().setPremMethod(l2processorDTO.getT5675().getPremsubr());
			l2processorDTO.getPremiumDTO().setCnttype(l2processorDTO.getChdrpf().getCnttype());
			
			if(l2processorDTO.getT5675().getPremsubr().trim().equals("PMEX")){
				l2processorDTO.getPremiumDTO().setSetPmexCall("Y");
				l2processorDTO.getPremiumDTO().setUpdateRequired("N");
				l2processorDTO.getPremiumDTO().setValidflag("Y");
			}
			l2processorDTO=callSubroutine(l2processorDTO,vpmsInfoDTO,vpxacblDTO,vpxlextDTO);
		}
		l2processorDTO.getPremiumDTO().setRiskPrem(BigDecimal.ZERO);
		
		boolean riskPremflag = itemService.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP094", "IT");
		if(riskPremflag)
		{
			l2processorDTO.getPremiumDTO().setRiskPrem(BigDecimal.ZERO);
			l2processorDTO.getPremiumDTO().setCnttype(l2processorDTO.getChdrpf().getCnttype());
			l2processorDTO.getPremiumDTO().setCrtable(covrpf.getCrtable());
			l2processorDTO.getPremiumDTO().setCalcTotPrem(l2processorDTO.getPremiumDTO().getCalcPrem().add(l2processorDTO.getPremiumDTO().getZstpduty01()));
			VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
			vpmsinfoDTO.setCoy(l2processorDTO.getChdrpf().getChdrcoy());
			vpmsinfoDTO.setTransDate(l2processorDTO.getEffDate() + "");
			vpmsinfoDTO.setModelName("RISKPREMIUM");
			l2processorDTO.getPremiumDTO().setPremMethod(l2processorDTO.getT5675().getPremsubr().substring(3));
			vpmsCaller.callPRMPMREST(vpmsinfoDTO, l2processorDTO.getPremiumDTO(), null, null);
		}
		if (null != l2processorDTO.getPremiumDTO().getStatuz() && !"****".equals(l2processorDTO.getPremiumDTO().getStatuz())) {
			throw new SubroutineIOException("Exception in Premium Subroutine Call " );
		}
		l2processorDTO.setPremiumDTO(l2processorDTO.getPremiumDTO());
		
	}
	public void getSubroutineName(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		T5675 t5675 = itemService.readSmartTableByTrim(Companies.LIFE,"T5675",l2processorDTO.getPremMeth(),T5675.class);
		if(t5675!=null)
			l2processorDTO.getT5675().setPremsubr(t5675.getPremsubr());
	}
	public void getOccpClass(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		//Get Occupation Class from Clntpf table
		Clntpf clntpf = clntpfDAO.getClntpf(CommonConstants.CLNTPFX_CN, Companies.FSU, l2processorDTO.getChdrpf().getCownnum());
		if(clntpf !=null) {
			l2processorDTO.getPremiumDTO().setOccpclass(clntpf.getOccpcode());
		}
		else {
			l2processorDTO.getPremiumDTO().setOccpclass("");
		}					
	}
	public void getAnny(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO) {
		l2processorDTO.getAnnypf().setChdrcoy(covrpf.getChdrcoy());
		l2processorDTO.getAnnypf().setLife(covrpf.getLife());
		l2processorDTO.getAnnypf().setCoverage(covrpf.getCoverage());
		l2processorDTO.getAnnypf().setRider(covrpf.getRider());
		l2processorDTO.getAnnypf().setPlnsfx(covrpf.getPlanSuffix());
		List<Annypf> annyList = annypfDAO.getAnnypfRecord(l2processorDTO.getAnnypf());
		Annypf annyIO = null;
		if (annyList != null && !annyList.isEmpty()) {
			annyIO = annyList.get(0);
		}
		if (annyIO != null) {
			l2processorDTO.getPremiumDTO().setFreqann(annyIO.getFreqann());
			l2processorDTO.getPremiumDTO().setAdvance(annyIO.getAdvance());
			l2processorDTO.getPremiumDTO().setArrears(annyIO.getArrears());
			l2processorDTO.getPremiumDTO().setGuarperd(annyIO.getGuarperd());
			l2processorDTO.getPremiumDTO().setIntanny(annyIO.getIntanny());
			l2processorDTO.getPremiumDTO().setCapcont(annyIO.getCapcont());
			l2processorDTO.getPremiumDTO().setWithprop(annyIO.getWithprop());
			l2processorDTO.getPremiumDTO().setWithoprop(annyIO.getWithoprop());
			l2processorDTO.getPremiumDTO().setPpind(annyIO.getPpind());
			l2processorDTO.getPremiumDTO().setNomlife(annyIO.getNomlife());
			l2processorDTO.getPremiumDTO().setDthpercn(annyIO.getDthpercn());
			l2processorDTO.getPremiumDTO().setDthperco(annyIO.getDthperco());
		} else {
			l2processorDTO.getPremiumDTO().setAdvance("");
			l2processorDTO.getPremiumDTO().setArrears("");
			l2processorDTO.getPremiumDTO().setFreqann("");
			l2processorDTO.getPremiumDTO().setWithprop("");
			l2processorDTO.getPremiumDTO().setWithoprop("");
			l2processorDTO.getPremiumDTO().setPpind("");
			l2processorDTO.getPremiumDTO().setNomlife("");
			l2processorDTO.getPremiumDTO().setGuarperd(0);
			l2processorDTO.getPremiumDTO().setIntanny(BigDecimal.ZERO);
			l2processorDTO.getPremiumDTO().setCapcont(BigDecimal.ZERO);
			l2processorDTO.getPremiumDTO().setDthpercn(BigDecimal.ZERO);
			l2processorDTO.getPremiumDTO().setDthperco(BigDecimal.ZERO);
		}
	}
	public void checkJointLife(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO) {
		String t5675Item="";
		if (l2processorDTO.getLifepf() != null ) {
			 t5675Item = l2processorDTO.getJlPremMeth().trim();
			l2processorDTO.getPremiumDTO().setJlsex(l2processorDTO.getLifepf().getCltsex());
			calculateAnb(l2processorDTO.getLifepf().getCltdob(),l2processorDTO);
			l2processorDTO.getPremiumDTO().setJlage(l2processorDTO.getAnb());
		} else {
			t5675Item = l2processorDTO.getPremMeth().trim();
			l2processorDTO.getPremiumDTO().setJlsex("");
			l2processorDTO.getPremiumDTO().setJlage(0);
		}
	}
	public void calculateAnb(int cltdob,L2WaiverRerateProcessorDTO l2processorDTO) {
		l2processorDTO.setAnb(0);
		l2processorDTO.getAgecalcDTO().setFunction("CALCP");
		l2processorDTO.getAgecalcDTO().setLanguage(l2processorDTO.getLang());
		l2processorDTO.getAgecalcDTO().setCnttype(l2processorDTO.getChdrpf().getCnttype());
		l2processorDTO.getAgecalcDTO().setIntDate1(l2processorDTO.getLifepf().getCltdob() + "");
		l2processorDTO.getAgecalcDTO().setIntDate2(l2processorDTO.getLastRerateDate() + "");
		l2processorDTO.getAgecalcDTO().setCompany(Companies.FSU);
		try {
			l2processorDTO.setAgecalcDTO(ageCalc.processAgecalc(l2processorDTO.getAgecalcDTO()));
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Exception in agecalc: " + e.getMessage());
		}
		l2processorDTO.setAnb(l2processorDTO.getAgecalcDTO().getAgerating());
	}
	public void setRerateType(Covrpf c,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException
	{
		long freqFactor=0;
		if (l2processorDTO.getRtrnwfreq().equals(BigDecimal.ZERO)) {
			l2processorDTO.setRerateType("2");
		}
		else {				
			try {
				freqFactor = datCon3.getYearsDifference(c.getCrrcd(), l2processorDTO.getEarliestDate());
			} catch (ParseException e1) {
				throw new SubroutineIOException(" IOException in datcon3: " + e1.getMessage());
			}
			
			BigDecimal result = new BigDecimal(freqFactor).divide(l2processorDTO.getRtrnwfreq());
			l2processorDTO.setDurationInt(result);
			l2processorDTO.setDurationRem(l2processorDTO.getDurationInt().intValue());
			if (l2processorDTO.getDurationRem()==0) {
				checkLextRerate(c,l2processorDTO);
			}
		}
		
		if (l2processorDTO.getRerateType().equals("1")) {//check
			BigDecimal result=l2processorDTO.getRtrnwfreq().multiply(l2processorDTO.getDurationInt());
			try {
				freqFactor = datCon2.plusYears(l2processorDTO.getEarliestDate(),(int)result.doubleValue());
			} catch (Exception e1) {
				throw new SubroutineIOException(" IOException in datcon3: " + e1.getMessage());
			}
			
			l2processorDTO.setLastRerateDate((int)freqFactor);
		}
		else {
			l2processorDTO.setLastRerateDate(l2processorDTO.getEarliestDate());
		}
		
	}
	public L2WaiverRerateProcessorDTO callSubroutine(L2WaiverRerateProcessorDTO l2processorDTO,VPMSinfoDTO vpmsInfoDTO,VpxacblDTO vpxacblDTO,VpxlextDTO vpxlextDTO) throws ParseException {
		PremiumDTO premiumrec = l2processorDTO.getPremiumDTO();
		if (!l2processorDTO.getVpmsFlag()) {
			if ("PRMPM04".equals(l2processorDTO.getT5675().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm04(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm04: " + e.getMessage());
				}
			} else if ("PRMPM03".equals(l2processorDTO.getT5675().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm03(premiumrec);
				} catch (IOException | ParseException e) {
					throw new SubroutineIOException("IOException in Prmpm03: " + e.getMessage());
				}
			} else if ("PRMPM17".equals(l2processorDTO.getT5675().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm17(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm18: " + e.getMessage());
				}
			}
			else if ("PRMPM01".equals(l2processorDTO.getT5675().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm01(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm01: " + e.getMessage());
				}
			}
			else if ("PRMPM18".equals(l2processorDTO.getT5675().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm18(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm18: " + e.getMessage());
				}
			}
		} else {
			l2processorDTO.getPremiumDTO().setPremMethod(l2processorDTO.getT5675().getPremsubr().substring(3));
			vpmsCaller.callPRMPMREST(vpmsInfoDTO, premiumrec, vpxacblDTO,vpxlextDTO);
		}
		return l2processorDTO;
	}
	
	public void checkLextRerate(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Lextpf lextbrr = new Lextpf();
		lextbrr.setChdrcoy(covrpf.getChdrcoy());
		lextbrr.setChdrnum(covrpf.getChdrnum());
		lextbrr.setLife(covrpf.getLife());
		lextbrr.setCoverage(covrpf.getCoverage());
		lextbrr.setRider(covrpf.getRider());
		lextbrr.setExtcd(covrpf.getRrtdat());
		List<Lextpf> lextpfList = lextpfDAO.checkLextpfRecord(lextbrr);
		if (lextpfList != null && !lextpfList.isEmpty()) {
			l2processorDTO.setRerateType("2");
		}
	}
	public boolean calc(Covrpf covrpf, L2WaiverRerateProcessorDTO l2processorDTO) {
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(l2processorDTO.getRerateStore());
		covrpfUpdate.setValidflag("2");
		covrpfUpdate.setUniqueNumber(covrpf.getUniqueNumber());
		l2processorDTO.setCovrUpdate(covrpfUpdate);
		l2processorDTO.setPremdiff(l2processorDTO.getPremdiff().add(BigDecimal.ZERO.subtract(covrpf.getInstprem())));
		
		if ("00".equals(l2processorDTO.getChdrpf().getBillfreq())) {
			return true;
		}
		if ("01".equals(covrpf.getCoverage()) && "01".equals(covrpf.getLife()) && "00".equals(covrpf.getRider())) {
			if (!l2processorDTO.getT5679().getSetCovPremStat().trim().isEmpty()) {
				covrpf.setPstatcode(l2processorDTO.getT5679().getSetCovPremStat());
			}
		} else {
			if (!l2processorDTO.getT5679().getSetRidPremStat().trim().isEmpty()) {
				covrpf.setPstatcode(l2processorDTO.getT5679().getSetRidPremStat());
			}
		}
		return false;
	}

	public void pastCeaseDate(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(l2processorDTO.getRerateStore());
		covrpfUpdate.setVarsi(BigDecimal.ZERO);
		covrpfUpdate.setValidflag("2");
		covrpfUpdate.setUniqueNumber(covrpf.getUniqueNumber());
		l2processorDTO.setCovrUpdate(covrpfUpdate); 
		if ("00".equals(covrpf.getRider()) || covrpf.getRider().trim().equals("")) {
			if(!l2processorDTO.getT5679().getSetCovPremStat().trim().isEmpty()) {
				covrpf.setPstatcode(l2processorDTO.getT5679().getSetCovPremStat());
			}
		}
		else {
			if (!l2processorDTO.getT5679().getRidPremStats().isEmpty()) {
				covrpf.setPstatcode(l2processorDTO.getT5679().getSetRidPremStat());
			}
		}
		Covrpf covrpfInsert= new Covrpf(covrpf);
		l2processorDTO.setTranNo(l2processorDTO.getChdrpf().getTranno()+1);
		covrpfInsert.setTranno(l2processorDTO.getTranNo());
		covrpfInsert.setCurrfrom(l2processorDTO.getRerateStore());
		covrpfInsert.setCurrto(l2processorDTO.getMaxDate());
		covrpfInsert.setRrtdat(l2processorDTO.getEarliestDate());
		if (covrpf.getCpiDate() != l2processorDTO.getMaxDate()) {
			covrpfInsert.setCpiDate(l2processorDTO.getEarliestDate());
		}
		l2processorDTO.setPremdiff(l2processorDTO.getPremdiff().add(covrpf.getInstprem().multiply(BigDecimal.valueOf(-1))));
		covrpfInsert.setRiskprem(l2processorDTO.getPremiumDTO().getRiskPrem());
		
		l2processorDTO.setCovrInsert(covrpfInsert);
		l2WaiverRerateControlTotalDTO.setControlTotal04(l2WaiverRerateControlTotalDTO.getControlTotal04().add(BigDecimal.ONE));
	}
	public void readTr52d(L2WaiverRerateProcessorDTO l2processorDTO)
	{
		boolean itemFound = checkTr52dForItem(l2processorDTO.getChdrpf().getReg(),l2processorDTO);
		if (!itemFound) {
			itemFound = checkTr52dForItem("***",l2processorDTO);
			if(!itemFound) {
				throw new ItemNotfoundException("L2WaiverRerate: Item not found in Table TR52D: " + l2processorDTO.getChdrpf().getReg());
			}
		}	
	}
	public boolean checkTr52dForItem(String keyItemitem,L2WaiverRerateProcessorDTO l2processorDTO) {
		Tr52d tr52d = itemService.readSmartTableByTrim(Companies.LIFE,"TR52D",keyItemitem,Tr52d.class);
		l2processorDTO.setTr52d(tr52d);
		if(l2processorDTO.getTr52d()!=null)
			return true;
		return false;
	}
	public void processWop(Covrpf covrpf, L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException 
	{
		if(covrpf.getCpiDate()> covrpf.getRrtdat())
		{
			l2processorDTO.setEarliestDate(covrpf.getRrtdat());
		}
		else {
			l2processorDTO.setEarliestDate(covrpf.getCpiDate());
		}		
		l2processorDTO.setCovrSumins(covrpf.getSumins());
		if (!l2processorDTO.getTr517().getZrwvflgs().get(1).equals("Y")) 
		{
				if (l2processorDTO.getCovrpf().getChdrcoy().equals(covrpf.getChdrcoy()) && l2processorDTO.getCovrpf().getLife().equals(covrpf.getLife())) {
					l2processorDTO.setCrtableFound(false);
					check3b22(l2processorDTO.getCovrpf(),l2processorDTO);
				}			
			l2processorDTO.setEndFlag(false);
				if (l2processorDTO.getCovrpf().getChdrcoy().equals(covrpf.getChdrcoy()) && l2processorDTO.getCovrpf().getLife().equals(covrpf.getLife())) {
					l2processorDTO.setPlanSfx(l2processorDTO.getCovrpf().getPlanSuffix());
											checkCovrStatus(l2processorDTO.getCovrpf(),l2processorDTO);
						l2processorDTO.setCrtableFound(false);
						check3a22(l2processorDTO.getCovrpf(), l2processorDTO.getCovrpf().getLife(),l2processorDTO);
				}
		}
		else {
					if (l2processorDTO.getCovrpf().getChdrcoy().equals(covrpf.getChdrcoy())) {
						l2processorDTO.setCrtableFound(false);
						check3b22(l2processorDTO.getCovrpf(),l2processorDTO);
					}		
				l2processorDTO.setEndFlag(false);
				
				if (l2processorDTO.getCovrpf().getChdrcoy().equals(covrpf.getChdrcoy())) {
					l2processorDTO.setPlanSfx(l2processorDTO.getCovrpf().getPlanSuffix());
					
					checkCovrStatus(l2processorDTO.getCovrpf(),l2processorDTO);
					
					l2processorDTO.setCrtableFound(false);
					check3a22(l2processorDTO.getCovrpf(), l2processorDTO.getCovrpf().getLife(),l2processorDTO);
				}
		}			
			if(l2processorDTO.getTr517().getZrwvflgs().get(2).equals("Y"))
			{
				calcFee(covrpf.getCoverage(),l2processorDTO);
			}
	}
	public void calcFee(String coverage,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException {
		
		l2processorDTO.setDateFound(false);
		int index=0;
		while (index< l2processorDTO.getT5688Cnttype().size() && !l2processorDTO.isDateFound())
		{
			if(l2processorDTO.getChdrpf().getCnttype().equals(l2processorDTO.getT5688Cnttype().get(index).trim()))	
			{
				if (l2processorDTO.getChdrpf().getOccdate()>=l2processorDTO.getT5688Itmfrm().get(index)) {
					l2processorDTO.setDateFound(true);
					l2processorDTO.setT5688FeeMeth(l2processorDTO.getT5688FeeMeth());
				}
			}
			index++;
		}
		if(!l2processorDTO.isDateFound()) {
			throw new ItemNotfoundException("Item not found");
		}		
		if (l2processorDTO.getT5688FeeMeth().trim().equals("")) {
			return ;
		}		
		if (l2processorDTO.getT5674().getCommsubr().trim().equals("")){
			return ;
		}
		
		l2processorDTO.getCfee001().setEffdate(0);
		l2processorDTO.getCfee001().setMgfee(0);
		l2processorDTO.getCfee001().setCnttype(l2processorDTO.getChdrpf().getCnttype());
		l2processorDTO.getCfee001().setBillfreq(Integer.parseInt(l2processorDTO.getChdrpf().getBillfreq()));		
		l2processorDTO.getCfee001().setEffdate(l2processorDTO.getChdrpf().getOccdate());
		l2processorDTO.getCfee001().setCntcurr(l2processorDTO.getChdrpf().getCntcurr());
		l2processorDTO.getCfee001().setCompany(l2processorDTO.getChdrpf().getChdrcoy());
		if (l2processorDTO.getT5674().getCommsubr().equalsIgnoreCase("CFEE001")) 
		{
			try {
				//Calling CFEE001 subroutine
				cfeeUtils.calCfee001(l2processorDTO.getCfee001());
			} catch (IOException e) {
				throw new SubroutineIOException(" Exception in CFEE001: " + e.getMessage());
			}
		}
		if(!("****").equals(l2processorDTO.getCfee001().getStatuz()))
		{
			throw new SubroutineIOException(" Exception in CFEE001: "+l2processorDTO.getCfee001().getStatuz());
		}
		if(coverage.equals("01") && (l2processorDTO.getTr517().getZrwvflgs().get(2).equals("Y")))	 {
			l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().add(new BigDecimal(l2processorDTO.getCfee001().getMgfee())));
			if (l2processorDTO.getCfee001().getMgfee()!= 0) {
				checkCalcContTax(l2processorDTO);
			}
		}
	}
	public void checkCalcContTax(L2WaiverRerateProcessorDTO l2processorDTO)
	{		l2processorDTO.setTax(BigDecimal.ZERO);
		if (l2processorDTO.getTr52d().getTxcode().trim().equals("")) {
			return ;
		}			
		
		l2processorDTO.setTr52eKey(l2processorDTO.getTr52d().getTxcode().trim().concat(l2processorDTO.getChdrpf().getCnttype().trim()).concat("****"));
		Tr52e tr52e=readTr52e(l2processorDTO.getTr52eKey(),l2processorDTO);
		
		if(tr52e==null) {
			l2processorDTO.setTr52eKey(l2processorDTO.getTr52d().getTxcode().trim().concat("***").concat("****"));
			tr52e=readTr52e(l2processorDTO.getTr52eKey(),l2processorDTO);
		}
		if(null != l2processorDTO.getTr52e().getTaxinds() && !l2processorDTO.getTr52e().getTaxinds().get(1).equals("Y")) {
				return;
		}
		
		TxcalcDTO txcalcrec = new TxcalcDTO();
		txcalcrec.setFunction("CALC");
		txcalcrec.setStatuz("****");
		txcalcrec.setChdrcoy(l2processorDTO.getChdrpf().getChdrcoy());
		txcalcrec.setChdrnum(l2processorDTO.getChdrpf().getChdrnum());
		txcalcrec.setLife(" ");
		txcalcrec.setCoverage(" ");
		txcalcrec.setRider(" ");
		txcalcrec.setCrtable(" ");
		txcalcrec.setPlanSuffix(0);
		txcalcrec.setCnttype(l2processorDTO.getChdrpf().getCnttype());
		txcalcrec.setRegister(l2processorDTO.getChdrpf().getReg());
		txcalcrec.setTaxrule(l2processorDTO.getTr52eKey());
		txcalcrec.setCcy((l2processorDTO.getChdrpf().getCntcurr()));
		txcalcrec.setRateItem(l2processorDTO.getChdrpf().getCntcurr().concat("****"));
		txcalcrec.setTaxType01(" ");
		txcalcrec.setTaxType02(" ");
		txcalcrec.setTaxAmt01(BigDecimal.ZERO);
		txcalcrec.setTaxAmt02(BigDecimal.ZERO);
		List<BigDecimal> taxAmts=new ArrayList<>();
		taxAmts.add(txcalcrec.getTaxAmt01());
		taxAmts.add(txcalcrec.getTaxAmt02());		
		txcalcrec.setTaxAmts(taxAmts);
		txcalcrec.setTaxAbsorb01(" ");
		txcalcrec.setTaxAbsorb02(" ");
		txcalcrec.setAmountIn(new BigDecimal(l2processorDTO.getCfee001().getMgfee()));
		txcalcrec.setEffdate(l2processorDTO.getChdrpf().getOccdate());
		txcalcrec.setTransType("CNTF");
		if (l2processorDTO.getTr52d().getTxsubr().equalsIgnoreCase("Servtax")) {
			try {
				txcalcrec = servTax.processServtax(txcalcrec);
			} catch (IOException e) {
				throw new SubroutineIOException(" IOException in servtax: " + e.getMessage());
			}
		}
		if (!"****".equals(txcalcrec.getStatuz())) {
			throw new SubroutineIOException("IOException in servtax");
		}
		if (txcalcrec.getTaxAmt01().compareTo(BigDecimal.ZERO) > 0
				|| txcalcrec.getTaxAmt02().compareTo(BigDecimal.ZERO) > 0) {
			if (!"Y".equals(txcalcrec.getTaxAbsorb01())) {
				l2processorDTO.setTax(l2processorDTO.getTax().add(txcalcrec.getTaxAmt01()));
			}
			if (!"Y".equals(txcalcrec.getTaxAbsorb02())) {
				l2processorDTO.setTax(l2processorDTO.getTax().add(txcalcrec.getTaxAmt02()));
			}
		}
		l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().add(l2processorDTO.getTax()));		
	}
	public void check3a22(Covrpf covrpf,String life,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException {
		int index=0;
		for (index=0; index<l2processorDTO.getTr517Ctables().size();index++) 
		{
			if (covrpf.getCrtable().trim().equals(l2processorDTO.getTr517().getCtables().get(index).trim()))
			{
				l2processorDTO.setCrtableFound(true);
			}
		}
		if ( !l2processorDTO.isCrtableFound() &&  !l2processorDTO.getTr517().getContitem().trim().equals(""))
		{
			readTr517Covr(covrpf,l2processorDTO);
			if (l2processorDTO.getWaiverCount().trim().equals("Y")) {
				check3a22(covrpf,life,l2processorDTO);
				return ;
			}
		}
		if ( l2processorDTO.isCrtableFound() && l2processorDTO.getTr517().getZrwvflgs().get(1).trim().equals("Y")
					|| covrpf.getLife().trim().equals(life)) {
			if (covrpf.getRrtdat()< l2processorDTO.getEarliestDate() 
					&& covrpf.getRrtdat()>0) {
				l2processorDTO.setEarliestDate(covrpf.getRrtdat());
			}
			if (covrpf.getCpiDate()<l2processorDTO.getEarliestDate()
					&& covrpf.getCpiDate()>0) {
				l2processorDTO.setEarliestDate(covrpf.getCpiDate());
			}
			readIncr(covrpf,l2processorDTO);
		}
	}
	public void readIncr(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException
	{   
		Incrpf incrpf= getIncrDetail(covr,l2processorDTO);
		l2processorDTO.setIncrpf(incrpf);
		if(incrpf!=null) {
			accSumins(covr, l2processorDTO.getIncrpf(),l2processorDTO);
		}
	}
	public boolean validateCoverage(L2WaiverRerateProcessorDTO l2processorDTO,Covrpf covrpf) {
		l2processorDTO.setValidCovr(false);
		if (covrpf.getRrtdat()== 0 || l2processorDTO.getMaxDate() ==(covrpf.getRrtdat())) {
			return false;
		}
		//Validate Coverage Status	
		if(covrpf.getRrtdat()!=0 && covrpf.getRrtdat() <= l2processorDTO.getBillDate())
		{
			//iterate through all the Coverage Risk status
			for (int i = 0; i < l2processorDTO.getT5679().getCovRiskStats().size(); i++) {
				if (covrpf.getStatcode().equals(l2processorDTO.getT5679().getCovRiskStats().get(i))) {
					//iterate through all the Coverage Premium status
					for (int j=0; j< l2processorDTO.getT5679().getCovPremStats().size(); j++) {						
						if (!covrpf.getRider().equals("00")) {
							if (l2processorDTO.getT5679().getRidPremStats().get(j).equals(covrpf.getPstatcode())) {
								l2processorDTO.setValidCovr(true);
							}
							break;
						}
						if (covrpf.getPstatcode().equals(l2processorDTO.getT5679().getCovPremStats().get(j))) {
							l2processorDTO.setValidCovr(true);
						}
					}
				}
			}
			if (l2processorDTO.isValidCovr()) {
				l2WaiverRerateControlTotalDTO.setControlTotal05(l2WaiverRerateControlTotalDTO.getControlTotal05().add(BigDecimal.ONE));
			}
		}	
		return l2processorDTO.isValidCovr();
		
	}
	public void a210ByPass(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException
	{
		int index=0;
		 searchlabel1:
			{
				for (index=0; index<l2processorDTO.getT5675Key().size(); index++){
					if (l2processorDTO.getT5675Key().get(index).equals(l2processorDTO.getPremMeth())) {
						l2processorDTO.getT5675().setPremsubr(l2processorDTO.getT5675PremSubr().get(index));
						l2processorDTO.setT5675PremSubrVal(l2processorDTO.getT5675Key().get(index));
						break searchlabel1;
					}
				}
				l2processorDTO.setPremReqd(true);
				return;
			}
		String premStatus="N";
		boolean isPremiumRequired=false;
		if (!l2processorDTO.getT5675().getPremsubr().trim().equals("")) {
			premStatus="Y";
		}
		else {
			premStatus="N";
		}
		callPremiumCalc(covr,l2processorDTO);
		
		int i =0 ;
		 searchlabel2:
		{
			for (i=0; i<l2processorDTO.getT5687Crtable().size(); i++){
				if (l2processorDTO.getT5687Crtable().get(i).equals(covr.getCrtable())) {
					break searchlabel2;
				}
			}
		}
	}
	public void checkCovrStatus(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		for (int i = 0; i < l2processorDTO.getT5679().getCovRiskStats().size(); i++) {
			if (covrpf.getStatcode().equals(l2processorDTO.getT5679().getCovRiskStats().get(i))) {
				for (int j=0; j< l2processorDTO.getT5679().getCovPremStats().size(); j++) {
						
					if (covrpf.getRider().equals("00")) {
						if (l2processorDTO.getT5679().getCovPremStats().get(j).equals(covrpf.getPstatcode())) {
							l2processorDTO.setValidCovr(true);
						}
					}
					else {
						if (l2processorDTO.getT5679().getRidPremStats().get(j).equals(covrpf.getPstatcode())) {
							l2processorDTO.setValidCovr(true);
						}
					}
				}
			}
		}
	}
	public void callPremiumCalc(Covrpf covrpf,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException
	{
		
		List<Lifepf> lifepfList=lifepfDAO.getLifeRecord(Companies.LIFE, covrpf.getChdrnum(), covrpf.getLife(), covrpf.getJlife());
		if(!lifepfList.isEmpty())
		{
			l2processorDTO.setLifepf(lifepfList.get(0));
		}
		if (l2processorDTO.getLifepf() != null) {
				l2processorDTO.getLifepf().getAnbccd();
		 		l2processorDTO.getLifepf().getCltsex();
		}
	
		l2processorDTO.getPremiumDTO().setFunction("CALC");
		l2processorDTO.getPremiumDTO().setCrtable(l2processorDTO.getMainCrtable());
		l2processorDTO.getPremiumDTO().setChdrChdrcoy(covrpf.getChdrcoy());
		l2processorDTO.getPremiumDTO().setChdrChdrnum(covrpf.getChdrnum());
		l2processorDTO.getPremiumDTO().setLifeLife(l2processorDTO.getLifepf().getLife());
		l2processorDTO.getPremiumDTO().setLifeJlife("00");
		l2processorDTO.getPremiumDTO().setCovrCoverage(l2processorDTO.getMainCoverage());
		l2processorDTO.getPremiumDTO().setCovrRider("00");
		l2processorDTO.getPremiumDTO().setEffectdt(l2processorDTO.getChdrpf().getOccdate());
		l2processorDTO.getPremiumDTO().setTermdate(l2processorDTO.getMainPcessDate());
		l2processorDTO.getPremiumDTO().setCurrcode(l2processorDTO.getChdrpf().getCntcurr());
		l2processorDTO.getPremiumDTO().setLsex(l2processorDTO.getLifepf().getCltsex());
		l2processorDTO.getPremiumDTO().setLage(l2processorDTO.getLifepf().getAnbccd());
		l2processorDTO.getPremiumDTO().setJlsex(l2processorDTO.getLifepf().getCltsex());
		l2processorDTO.getPremiumDTO().setJlage(l2processorDTO.getLifepf().getAnbccd());
		long freqFactor=0L;
		try {
			 freqFactor = datCon3.getYearsDifference(l2processorDTO.getPremiumDTO().getEffectdt(), l2processorDTO.getPremiumDTO().getTermdate());
		} catch (ParseException e) {
			throw new SubroutineIOException("ParseException in datcon3: " + e.getMessage());
		}
		l2processorDTO.getPremiumDTO().setCnttype(l2processorDTO.getChdrpf().getCnttype());
		l2processorDTO.getPremiumDTO().setDuration((int) (freqFactor+(0.99999)));

		l2processorDTO.getPremiumDTO().setRiskCessTerm((int) freqFactor);
		l2processorDTO.getPremiumDTO().setSumin(l2processorDTO.getCovrSumins());
		l2processorDTO.getPremiumDTO().setMortcls(l2processorDTO.getMainMortClass());
		l2processorDTO.getPremiumDTO().setBillfreq(l2processorDTO.getChdrpf().getBillfreq());
		l2processorDTO.getPremiumDTO().setMop(l2processorDTO.getChdrpf().getBillchnl());
		l2processorDTO.getPremiumDTO().setRatingdate(l2processorDTO.getChdrpf().getOccdate());
		l2processorDTO.getPremiumDTO().setReRateDate(l2processorDTO.getChdrpf().getOccdate());
		l2processorDTO.getPremiumDTO().setLanguage(l2processorDTO.getLang());
		
		VpxlextDTO vpxlextDTO = new VpxlextDTO();
		VPMSinfoDTO vpmsInfoDTO = new VPMSinfoDTO();
		vpmsInfoDTO.setCoy(Companies.LIFE);
		getSubroutineName(l2processorDTO);
		vpmsInfoDTO.setModelName(l2processorDTO.getT5675().getPremsubr());
		vpmsInfoDTO.setTransDate(String.valueOf(l2processorDTO.getTransactionDate()));
		getOccpClass(l2processorDTO);
		
		if(!l2processorDTO.getVpmsFlag()) {
			l2processorDTO.getPremiumDTO().setPremMethod(l2processorDTO.getT5675().getPremsubr().substring(3));
			vpmsCaller.callPRMPMREST(vpmsInfoDTO, l2processorDTO.getPremiumDTO(),null,null);
		}
		else{
			
			vpxlextDTO.setFunction("INIT");
			vpxlextDTO=vpxLext.processVpxlext(vpxlextDTO.getFunction(), l2processorDTO.getPremiumDTO());
						
			VpxchdrDTO vpxchdrDTO = new VpxchdrDTO();
			vpxchdrDTO.setFunction("INIT");
			vpxchdrDTO=vpxChdr.callInit(l2processorDTO.getPremiumDTO());
			l2processorDTO.getPremiumDTO().setRstaflag(vpxchdrDTO.getRstaflag());
			l2processorDTO.getPremiumDTO().setPremMethod(l2processorDTO.getT5675().getPremsubr().substring(3));
			vpmsCaller.callPRMPMRESTNew(vpmsInfoDTO, l2processorDTO.getPremiumDTO(), vpxlextDTO, vpxchdrDTO);
		}
	
		if (!l2processorDTO.getPremiumDTO().getStatuz().equals("****")) {
			throw new SubroutineIOException("Exception in Premium Subroutine Call " );
		}
		l2processorDTO.setCovrSumins(l2processorDTO.getPremiumDTO().getCalcPrem());
	}
	public void calcBenefitAmount(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException
	{
		a200Ctrl(covr,l2processorDTO);
	}
	public void a200Ctrl(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException {
		int index=0;
		 searchlabel1:
		{
			for (index=0; index< l2processorDTO.getT5687Premmeth().size();index++){
				if (l2processorDTO.getT5687Premmeth().get(index).trim().equals(l2processorDTO.getMainCrtable())) {
					break searchlabel1;
				}
			}
			a210ByPass(covr,l2processorDTO);
		}
		l2processorDTO.setDateFound(false);
		while ( index<l2processorDTO.getT5687Premmeth().size() && !l2processorDTO.isDateFound())
		{
			if(l2processorDTO.getT5687Premmeth().get(index).trim().equals(l2processorDTO.getMainCrtable()))
			{
				if (l2processorDTO.getChdrpf().getOccdate()>=l2processorDTO.getT5687Itmfrm().get(index)) {
					l2processorDTO.setDateFound(true);
					moveT5687(index,l2processorDTO);
				}		
			}
			index++;
		}
	}
	public void accSumins(Covrpf covrpf, Incrpf incr,L2WaiverRerateProcessorDTO l2processorDTO) throws ParseException
	{
		if (l2processorDTO.getTr517().getZrwvflgs().get(3).trim().equals("Y")) {
			if (covrpf.getRider().trim().equals("00")) {
				if (null != incr) {
					l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().add(incr.getNewinst()));
				}
				else
				{
					l2processorDTO.setCovrSumins(covrpf.getSumins());
					l2processorDTO.setMainCrtable(covrpf.getCrtable());
					l2processorDTO.setMainCoverage(covrpf.getCoverage());
					l2processorDTO.setMainCessDate(covrpf.getRcesdte());
					l2processorDTO.setMainPcessDate(covrpf.getPremCessDate());
					l2processorDTO.setMainMortClass(covrpf.getMortcls());
					l2processorDTO.setMainLife(covrpf.getLife());
				}
			}
			else {
				if (incr == null) {
					l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().subtract(covrpf.getSumins()));
					calcBenefitAmount(covrpf,l2processorDTO);
				}
				else {
					l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().subtract(incr.getNewinst()));
				}
			}
		}
		else {
			if (incr == null) {
				l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().add(covrpf.getInstprem()));
				if (covrpf.getInstprem().compareTo(BigDecimal.ZERO)>0) {	}				
					checkCalcCompTax(covrpf,incr,l2processorDTO);
			}
			else {
				
				l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().subtract(incr.getNewinst()));
				if (incr.getNewinst().compareTo(BigDecimal.ZERO)>0)	{}					
				   checkCalcCompTax(covrpf,incr,l2processorDTO);
			}
		}
	}
	
	public void checkCalcCompTax(Covrpf covr,Incrpf incr,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		l2processorDTO.setTax(BigDecimal.ZERO);
		if (l2processorDTO.getTr52d().getTxcode().trim().equals(" ")) {
			return ;
		}
		l2processorDTO.setTr52eKey(l2processorDTO.getTr52d().getTxcode().trim().concat(l2processorDTO.getChdrpf().getCnttype().trim()).concat(covr.getCrtable().trim()));
		Tr52e tr52e = readTr52e(l2processorDTO.getTr52eKey(),l2processorDTO);
		
		if(tr52e==null) {
			l2processorDTO.setTr52eKey(l2processorDTO.getTr52d().getTxcode().concat(l2processorDTO.getChdrpf().getCnttype()).concat("****"));
			tr52e=readTr52e(l2processorDTO.getTr52eKey(),l2processorDTO);
		}
		if(tr52e==null) {
			l2processorDTO.setTr52eKey(l2processorDTO.getTr52d().getTxcode().concat("***").concat("****"));
			tr52e=readTr52e(l2processorDTO.getTr52eKey(),l2processorDTO);
		}	
		if(null != l2processorDTO.getTr52e().getTaxinds() && !l2processorDTO.getTr52e().getTaxinds().get(0).equals("Y")) {
			return;
		}
		
		//Call TR52D tax subroutine                                       
		TxcalcDTO txcalcrec = new TxcalcDTO();
		txcalcrec.setFunction("CALC");
		txcalcrec.setStatuz("****");
		txcalcrec.setChdrcoy(l2processorDTO.getChdrpf().getChdrcoy());
		txcalcrec.setChdrnum(l2processorDTO.getChdrpf().getChdrnum());
		txcalcrec.setLife(covr.getLife());
		txcalcrec.setCoverage(covr.getCoverage());
		txcalcrec.setRider(covr.getRider());
		txcalcrec.setCrtable(covr.getCrtable());
		txcalcrec.setPlanSuffix(0);
		txcalcrec.setCnttype(l2processorDTO.getChdrpf().getCnttype());
		txcalcrec.setRegister(l2processorDTO.getChdrpf().getReg());
		txcalcrec.setTaxrule(l2processorDTO.getTr52eKey());
		txcalcrec.setCcy(l2processorDTO.getChdrpf().getCntcurr());
		txcalcrec.setRateItem(l2processorDTO.getChdrpf().getCntcurr().concat("****"));
		txcalcrec.setEffdate(l2processorDTO.getChdrpf().getOccdate());
		txcalcrec.setTransType("PREM");
		txcalcrec.setTaxType01(" ");
		txcalcrec.setTaxType02(" ");
		txcalcrec.setTaxAmt01(BigDecimal.ZERO);
		txcalcrec.setTaxAmt02(BigDecimal.ZERO);
		List<BigDecimal> taxAmts=new ArrayList<>();
		taxAmts.add(txcalcrec.getTaxAmt01());
		taxAmts.add(txcalcrec.getTaxAmt02());		
		txcalcrec.setTaxAmts(taxAmts);
		txcalcrec.setTaxAbsorb01(" ");
		txcalcrec.setTaxAbsorb02(" ");
		
		if ((l2processorDTO.getIncrpf() == null && (covr.getInstprem().compareTo(BigDecimal.ZERO)>0))
				|| (l2processorDTO.getIncrpf().getNewinst().compareTo(BigDecimal.ZERO)>0)) 
		{ 
			if (l2processorDTO.getTr52e().getZbastyp().equals("Y")) {
				if(l2processorDTO.getIncrpf() == null)
					txcalcrec.setAmountIn(covr.getZbinstprem());
				else 
					txcalcrec.setAmountIn(l2processorDTO.getIncrpf().getZbnewinst());
			}
			else {
				if(l2processorDTO.getIncrpf() == null )
					txcalcrec.setAmountIn(covr.getInstprem());
				else
					txcalcrec.setAmountIn(l2processorDTO.getIncrpf().getNewinst());
			}
		
			if (l2processorDTO.getTr52d().getTxsubr().equalsIgnoreCase("Servtax")) {
				try {
					txcalcrec = servTax.processServtax(txcalcrec);
				} catch (IOException e) {
					throw new SubroutineIOException(" IOException in servtax: " + e.getMessage());
				}
			}	
			if (!"****".equals(txcalcrec.getStatuz())) {
				throw new SubroutineIOException("IOException in servtax");
			}
			if (txcalcrec.getTaxAmt01().compareTo(BigDecimal.ZERO) > 0
					|| txcalcrec.getTaxAmt02().compareTo(BigDecimal.ZERO) > 0) {
				if (txcalcrec.getTaxAmt01().compareTo(BigDecimal.ZERO) != 1) {
					l2processorDTO.setTax(l2processorDTO.getTax().add(txcalcrec.getTaxAmt01()));
				}
				if (txcalcrec.getTaxAmt02().compareTo(BigDecimal.ZERO) != 1) {
					l2processorDTO.setTax(l2processorDTO.getTax().add(txcalcrec.getTaxAmt02()));
				}
			}
		}
		l2processorDTO.setCovrSumins(l2processorDTO.getCovrSumins().add(l2processorDTO.getTax()));
	}
	public Tr52e readTr52e(String itemitem,L2WaiverRerateProcessorDTO l2processorDTO)
	{		
		Tr52e tr52e = itemService.readSmartTableByDate("TR52E",itemitem,l2processorDTO.getChdrpf().getOccdate(),l2processorDTO.getChdrpf().getOccdate(),Tr52e.class);
		if(tr52e!=null) {
			l2processorDTO.setTr52e(tr52e);
		}
		return tr52e;
	}
	public Incrpf getIncrDetail(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO)
	{
		l2processorDTO.setDateFound(false);
		int index=0;
		
		while (index<l2processorDTO.getT5687Crtable().size() && !l2processorDTO.isDateFound())
		{
			if(covr.getCrtable().equals(l2processorDTO.getT5687Crtable().get(index).trim()))
			{
				if (covr.getCrrcd() >= l2processorDTO.getT5687Itmfrm().get(index)) {
					l2processorDTO.setDateFound(true);
					moveT5687(index,l2processorDTO);
				}
			}
			index++;
		}
		if(!l2processorDTO.isDateFound())
			throw new ItemNotfoundException("Crtable Match not found in T5687");
		
		List<Incrpf> incrList = incrpfDAO.readValidIncrpf(Companies.LIFE, covr.getChdrnum());
		Incrpf incrpf = null;
		for(Incrpf incr : incrList) {
			if(incr.getChdrcoy().equals(covr.getChdrcoy())&& incr.getLife().equals(covr.getLife())&& incr.getCoverage().equals(covr.getCoverage())&&incr.getRider().equals(covr.getRider())&&incr.getPlnsfx().equals(covr.getPlanSuffix()))
			{
				incrpf=incr;
				break;
			}	
		} if(incrpf !=null) {
			if (l2processorDTO.getAnnvMethod().equals("")) {
				return incrpf;
			}
			index=0;
			l2processorDTO.setDateFound(false);
			while (index<l2processorDTO.getT6658Annvry().size() && !l2processorDTO.isDateFound())
			{
				if(l2processorDTO.getT5687Annvry().get(index).equals(l2processorDTO.getT6658Annvry().get(index).trim())) 
				{
					if (covr.getCrrcd() >= l2processorDTO.getT6658Itmfrm().get(index)) {
						l2processorDTO.setDateFound(true);
						l2processorDTO.setT6658Billfreq(Integer.parseInt(l2processorDTO.getT6658BillFreqList().get(index)));
					}
				}
				index++;
			}
			if(!l2processorDTO.isDateFound())
				throw new ItemNotfoundException("AnniversaryMethod not found in T6658");
			
			if (l2processorDTO.getT6658Billfreq()==00 || covr.getCpiDate()==l2processorDTO.getMaxDate())
			{
				return incrpf;
			}
			int freqFactor= l2processorDTO.getT6658Billfreq();
			int datconResult=0;
			if(incrpf !=null) {
				 datconResult=datCon2.plusYears(incrpf.getCrrcd(), freqFactor);
			}
			if(l2processorDTO.getFirstIncrDate()==l2processorDTO.getMaxDate()) {
				l2processorDTO.setEarliestDate(datconResult);
				l2processorDTO.setFirstIncrDate(datconResult);
			}
			else {
				if (datconResult< l2processorDTO.getFirstIncrDate()) {
					l2processorDTO.setEarliestDate(datconResult);
					l2processorDTO.setFirstIncrDate(datconResult);
				}
				else {
					l2processorDTO.setEarliestDate(l2processorDTO.getFirstIncrDate());
				}
			}
		}
			return incrpf;
	}
	public void check3b22(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO)
	{		
		for(int index=0;index<l2processorDTO.getTr517Ctables().size();index++) 
			{
				if (covr.getCrtable().equals(l2processorDTO.getTr517Ctables().get(index))){
					l2processorDTO.setCrtableFound(true);
				
			}
		}
		if (!l2processorDTO.isCrtableFound() && !l2processorDTO.getTr517().getContitem().equals(" ")) 
		{ 
			readTr517Covr(covr,l2processorDTO);	
			if (l2processorDTO.getWaiverCount().equals("Y")) {
				check3b22(covr,l2processorDTO);
			}
		}
		if (l2processorDTO.isCrtableFound()) {			
			l2processorDTO.setEarliestDate(l2processorDTO.getMaxDate());
			l2processorDTO.setEndFlag(true);
			return;
		}
	}
	public void readTr517Covr(Covrpf covr,L2WaiverRerateProcessorDTO l2processorDTO) {
		l2processorDTO.setWaiverCount("N");
		l2processorDTO.setDateFound(false);
		int index=0;
		 searchlabel1:
			{
				for (index=0; index<l2processorDTO.getTr517Crtable().size(); index++){
					if (l2processorDTO.getTr517Crtable().get(index).equals(l2processorDTO.getTr517().getContitem())) {
						break searchlabel1;
					}
				}
				return ;
			}
		List<String> zrwflgs = new ArrayList<>();
		List<String> ctables = new ArrayList<>();
		while (index <l2processorDTO.getTr517Crtable().size() && !l2processorDTO.isDateFound())
		{
			if(l2processorDTO.getTr517().getContitem().equals(l2processorDTO.getTr517Crtable().get(index).trim()))
			{
				if (covr.getCrrcd()>= l2processorDTO.getTr517Itmfrm().get(index)) {
					l2processorDTO.setDateFound(true);
					zrwflgs.add(l2processorDTO.getTr517Zrwvflgs().get(index));
					ctables.add(l2processorDTO.getTr517Ctables().get(index));
				}
			}
			index++;
		}	
		if(!zrwflgs.isEmpty())
			l2processorDTO.setTr517Zrwvflgs(zrwflgs);
		if(!ctables.isEmpty())
			l2processorDTO.setTr517Ctables(ctables);
		
		if (!l2processorDTO.isDateFound()) {
			return ;
		}
		l2processorDTO.setWaiverCount("Y");
	}
	public BigDecimal callRounding(BigDecimal amountIn,L2WaiverRerateProcessorDTO l2processorDTO) {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(l2processorDTO.getChdrpf().getCntcurr());
		zrdecplDTO.setCompany(Companies.LIFE);
		zrdecplDTO.setBatctrcde(l2processorDTO.getBatchtrancde());
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = zrdecPlc.convertAmount(zrdecplDTO);
		} catch (IOException e) {
			throw new ItemParseException("Parsed failed in zrdecplc:" + e.getMessage());
		}
		l2processorDTO.setZrdecplDTO(zrdecplDTO);
		return result;
	}
}
