package com.dxc.integral.life.exception;

public class L2CollectionsException extends RuntimeException {

	public L2CollectionsException(String msg) {
		super(msg);
	}
}
