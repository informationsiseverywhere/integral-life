package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2newintblReaderDTO;
import com.dxc.integral.life.dao.BextpfDAO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Bextpf;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

public class L2newintblItemWriter implements ItemWriter<L2newintblReaderDTO>{
	

	/** The LOGGER Object */
	 private static final Logger LOGGER = LoggerFactory.getLogger(L2newintblItemWriter.class);
	      
     @Autowired
     private Sftlock sftlock;
     @Autowired
     private ChdrpfDAO chdrpfDAO;
     @Autowired
     private LoanpfDAO loanpfDAO;
     @Autowired
     private BextpfDAO bextpfDAO;
     @Autowired
     private PtrnpfDAO ptrnpfDAO;
	    
	    @Override
	    public void write(List<? extends L2newintblReaderDTO> readerDTOList) throws Exception {
			
			LOGGER.info("L2newintblItemWriter starts.");
			processData(readerDTOList);
			if(!readerDTOList.isEmpty()){
				LOGGER.info("Unlock contracts");
				sftlock.unlockByRange("CH", readerDTOList.get(0).getLo_chdrcoy(), readerDTOList.get(0).getLo_chdrnum(), readerDTOList.get(readerDTOList.size()-1).getLo_chdrnum());
			}
			LOGGER.info("L2newintblItemWriter ends.");
		}
	    private void processData(List<? extends L2newintblReaderDTO> readerDTOList) throws ParseException {
	    	if (!readerDTOList.isEmpty()) {
	    		Chdrpf chdrpf = null;
	    		Loanpf loanpf = null;
	    		Bextpf bextpf = null;
	    		Ptrnpf ptrnpf = null;
	    		List<Chdrpf> chdrpfUpdateList = new ArrayList<Chdrpf>();
	    		List<Loanpf> loanpfUpdateList = new ArrayList<Loanpf>();
	    		List<Bextpf> bextInsertList = new ArrayList<Bextpf>();
	    		List<Ptrnpf> ptrnInsertList = new ArrayList<Ptrnpf>();
				for(L2newintblReaderDTO dto : readerDTOList ){
					chdrpf = new Chdrpf();
					chdrpf.setTranno(dto.getCh_tranno());
					chdrpf.setUsrprf(dto.getLo_usrprf());
					chdrpf.setJobnm(dto.getLo_jobnm());
					chdrpf.setUniqueNumber(dto.getCh_unique_number());
					chdrpfUpdateList.add(chdrpf);
					
					loanpf = new Loanpf();
					loanpf.setValidflag(dto.getLo_validflag());
					loanpf.setLstintbdte(dto.getLo_lstintbdte());
					loanpf.setNxtintbdte(dto.getLo_nxtintbdte());
					loanpf.setTrdt(dto.getLo_trdt());
					loanpf.setTrtm(dto.getLo_trtm());
					loanpf.setUsrprf(dto.getLo_usrprf());
					loanpf.setJobnm(dto.getLo_jobnm());
					loanpf.setUnique_number(dto.getLo_unique_number());
					loanpfUpdateList.add(loanpf);
					
					bextpf = new Bextpf();
					bextpf.setChdrpfx(dto.getCh_chdrpfx());
					bextpf.setChdrcoy(dto.getLo_chdrcoy());
					bextpf.setChdrnum(dto.getLo_chdrnum());
					bextpf.setServunit(dto.getCh_servunit());
					bextpf.setCnttype(dto.getCh_cnttype());
					bextpf.setCntcurr(dto.getPy_billcurr());
					bextpf.setOccdate(dto.getCh_occdate());
					bextpf.setCcdate(dto.getCh_ccdate());
					bextpf.setPtdate(dto.getPy_ptdate());
					bextpf.setBtdate(dto.getLo_nxtintbdte());
					bextpf.setBilldate(dto.getLo_nxtintbdte());
					bextpf.setBillchnl(dto.getPy_billchnl());
					bextpf.setBankcode(dto.getBankcode());
					bextpf.setInstfrom(dto.getLo_lstintbdte());
					bextpf.setInstto(dto.getLo_nxtintbdte());
					bextpf.setInstbchnl(dto.getPy_billchnl());
					bextpf.setInstcchnl(dto.getCh_collchnl());
					bextpf.setInstfreq(dto.getPy_billfreq());
					bextpf.setInstamt01(dto.getInterestAmount().doubleValue());
					bextpf.setInstamt06(dto.getInterestAmount().doubleValue());
					bextpf.setInstjctl("L2NEWINTBL");
					bextpf.setFacthous(dto.getCb_facthous());
					bextpf.setBankkey(dto.getMa_bankkey());
					bextpf.setBankacckey(dto.getMa_bankacckey());
					bextpf.setCownpfx(dto.getCh_cownpfx());
					bextpf.setCowncoy(dto.getCh_cowncoy());
					bextpf.setCownnum(dto.getCh_cownnum());
					bextpf.setPayrpfx(dto.getCl_clntpfx());
					bextpf.setPayrcoy(dto.getCl_clntcoy());
					bextpf.setPayrnum(dto.getCl_clntnum());
					bextpf.setCntbranch(dto.getCh_cntbranch());
					bextpf.setAgntpfx(dto.getCh_agntpfx());
					bextpf.setAgntcoy(dto.getCh_agntcoy());
					bextpf.setAgntnum(dto.getCh_agntnum());
					bextpf.setSupflag("N");
					bextpf.setMandref(dto.getPy_mandref());
					bextpf.setBillcd(dto.getLo_nxtintbdte());
					bextpf.setSacscode(dto.getSacscode());
					bextpf.setSacstyp(dto.getSacsType());
					bextpf.setGlmap(dto.getGlMap());
					bextpf.setMandstat(dto.getMa_mandstat());
					bextpf.setDdderef(0);
					bextpf.setEffdatex(0);
					bextpf.setUserProfile(dto.getLo_usrprf());
					bextpf.setJobName(dto.getLo_jobnm());
					bextpf.setNextdate(dto.getPy_nextdate());
					bextInsertList.add(bextpf);
					
					ptrnpf = new Ptrnpf();
					ptrnpf.setChdrpfx(dto.getCh_chdrpfx());
					ptrnpf.setChdrcoy(dto.getLo_chdrcoy());
					ptrnpf.setChdrnum(dto.getLo_chdrnum());
					ptrnpf.setTranno(dto.getCh_tranno());
					ptrnpf.setTrdt(dto.getLo_trdt());
					ptrnpf.setTrtm(dto.getLo_trtm());
					ptrnpf.setValidflag("1");
					ptrnpf.setPtrneff(dto.getLo_nxtintbdte());
					ptrnpf.setUserT(0);
					ptrnpf.setBatccoy(dto.getBatccoy());
					ptrnpf.setBatcpfx(dto.getBatcpfx());
					ptrnpf.setBatcbrn(dto.getBatcbrn());
					ptrnpf.setBatcactyr(dto.getBatcactyr());
					ptrnpf.setBatctrcde(dto.getBatctrcde());
					ptrnpf.setBatcactmn(dto.getBatcactmn());
					ptrnpf.setBatcbatch(dto.getBatcbatch());
					ptrnpf.setDatesub(dto.getEffectiveDate());
					ptrnpf.setUsrprf(dto.getLo_usrprf());
					ptrnpf.setJobnm(dto.getLo_jobnm());
					ptrnInsertList.add(ptrnpf);
				}
				if(!chdrpfUpdateList.isEmpty()){
					chdrpfDAO.updateChdrTrannoByUniqueNo(chdrpfUpdateList);
				}
				if(!loanpfUpdateList.isEmpty()){
					loanpfDAO.updateLoanpfByUniqueNumberList(loanpfUpdateList);
				}
				if(!bextInsertList.isEmpty()){
					bextpfDAO.insertBextPFList(bextInsertList);
				}
				if(!ptrnInsertList.isEmpty()){
					ptrnpfDAO.insertPtrnpfRecords(ptrnInsertList);
				}
			}
	    	
	    }
	   
}
