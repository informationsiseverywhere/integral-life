package com.dxc.integral.life.general.readers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.dxc.integral.life.beans.L2lincsndReaderDTO;
import com.dxc.integral.life.dao.LincpfDAO;
import com.dxc.integral.life.dao.SlncpfDAO;
import com.dxc.integral.life.dao.model.Slncpf;
import com.dxc.integral.life.dao.model.Lincpf;

/**
 * Reads records from Database
 * 
 * @author gsaluja2
 *
 */
public class L2lincsndItemReader implements ItemReader<L2lincsndReaderDTO>{

	private int recordCount;
	
	private List<L2lincsndReaderDTO> list;
	
	@Autowired
	private SlncpfDAO slncpfDAO;
	
	@Autowired
	private LincpfDAO lincpfDAO;
	
	private Map<String, Lincpf> lincpfMap;
	
	@Value("#{stepExecutionContext['zreclass']}")
	private String zreclass;
	
	public L2lincsndItemReader() {
		//does nothing
	}
	
	public void init() {
		list = new ArrayList<>();
		List<Slncpf> slncpfDataRecords = slncpfDAO.readSlncpfData(zreclass);
		lincpfMap = lincpfDAO.readLincpfData();
		if(slncpfDataRecords != null && !slncpfDataRecords.isEmpty()) {
			for(Slncpf slncpfRecord: slncpfDataRecords)
				list.add(setReaderDTO(slncpfRecord));
		}
	}
	
	public L2lincsndReaderDTO setReaderDTO(Slncpf slncpfRecord) {
		L2lincsndReaderDTO l2lincsndReader = new L2lincsndReaderDTO();
		l2lincsndReader.setUniqueNumber(slncpfRecord.getUniqueNumber());
		l2lincsndReader.setZdatacls(slncpfRecord.getZdatacls());
		l2lincsndReader.setZreclass(slncpfRecord.getZreclass());
		l2lincsndReader.setZmodtype(slncpfRecord.getZmodtype());
		l2lincsndReader.setZrsltcde(slncpfRecord.getZrsltcde());
		l2lincsndReader.setZrsltrsn(slncpfRecord.getZrsltrsn());
		l2lincsndReader.setZerrorno(slncpfRecord.getZerrorno());
		l2lincsndReader.setZnofrecs(slncpfRecord.getZnofrecs());
		l2lincsndReader.setKlinckey(slncpfRecord.getKlinckey());
		l2lincsndReader.setHcltnam(slncpfRecord.getHcltnam());
		l2lincsndReader.setCltsex(slncpfRecord.getCltsex());
		l2lincsndReader.setZclntdob(slncpfRecord.getZclntdob());
		l2lincsndReader.setZeffdate(slncpfRecord.getZeffdate());
		l2lincsndReader.setFlag(slncpfRecord.getFlag());
		l2lincsndReader.setKglcmpcd(slncpfRecord.getKglcmpcd());
		l2lincsndReader.setAddrss(slncpfRecord.getAddrss());
		l2lincsndReader.setZhospbnf(slncpfRecord.getZhospbnf());
		l2lincsndReader.setZsickbnf(slncpfRecord.getZsickbnf());
		l2lincsndReader.setZcancbnf(slncpfRecord.getZcancbnf());
		l2lincsndReader.setZothsbnf(slncpfRecord.getZothsbnf());
		l2lincsndReader.setZregdate(slncpfRecord.getZregdate());
		l2lincsndReader.setZlincdte(slncpfRecord.getZlincdte());
		l2lincsndReader.setKjhcltnam(slncpfRecord.getKjhcltnam());
		l2lincsndReader.setZhospopt(slncpfRecord.getZhospopt());
		l2lincsndReader.setZrevdate(slncpfRecord.getZrevdate());
		l2lincsndReader.setZlownnam(slncpfRecord.getZlownnam());
		l2lincsndReader.setZcsumins(slncpfRecord.getZcsumins());
		l2lincsndReader.setZasumins(slncpfRecord.getZasumins());
		l2lincsndReader.setZdethopt(slncpfRecord.getZdethopt());
		l2lincsndReader.setZrqflag(slncpfRecord.getZrqflag());
		l2lincsndReader.setZrqsetdte(slncpfRecord.getZrqsetdte());
		l2lincsndReader.setZrqcandte(slncpfRecord.getZrqcandte());
		l2lincsndReader.setZepflag(slncpfRecord.getZepflag());
		l2lincsndReader.setZcvflag(slncpfRecord.getZcvflag());
		l2lincsndReader.setZcontsex(slncpfRecord.getZcontsex());
		l2lincsndReader.setZcontdob(slncpfRecord.getZcontdob());
		l2lincsndReader.setZcontaddr(slncpfRecord.getZcontaddr());
		l2lincsndReader.setLifnamkj(slncpfRecord.getLifnamkj());
		l2lincsndReader.setOwnnamkj(slncpfRecord.getOwnnamkj());
		l2lincsndReader.setWflga(slncpfRecord.getWflga());
		l2lincsndReader.setZerrflg(slncpfRecord.getZerrflg());
		l2lincsndReader.setZoccdate(slncpfRecord.getZoccdate());
		l2lincsndReader.setZtrdate(slncpfRecord.getZtrdate());
		l2lincsndReader.setZadrddte(slncpfRecord.getZadrddte());
		l2lincsndReader.setZexdrddte(slncpfRecord.getZexdrddte());
		if(lincpfMap!=null && lincpfMap.containsKey(slncpfRecord.getZhospopt().trim()))
			l2lincsndReader.setLincpf(lincpfMap.get(slncpfRecord.getZhospopt().trim()));
		return l2lincsndReader;
	}
	
	@Override
	public L2lincsndReaderDTO read() 
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if (recordCount < list.size()) {
			return list.get(recordCount++);
		} else {
			recordCount = 0;
			return null;
		}
	}

}
