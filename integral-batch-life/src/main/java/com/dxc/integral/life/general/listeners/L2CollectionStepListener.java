package com.dxc.integral.life.general.listeners;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.L2CollectionControlTotalDTO;

/**
 * 
 * Listener for L2polrnwlColllectionStepListener batch step
 * 
 * @author xma3
 * 
 * 
 */

public class L2CollectionStepListener extends IntegralStepListener{
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CollectionStepListener.class);

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private L2CollectionControlTotalDTO l2CollectionControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;
	
	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['chdrcoy']}")
	private String company;
	
	@Value("#{jobParameters['businessDate']}")
	private String effDate;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Value("#{jobParameters['trandate']}") 
	private String transactionDate;

	@Autowired
	private ElogpfDAO elogpfDAO;

	private static String programName = "B5352";
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("L2polrnwlCollectionStepListener::afterStep::Entry");
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.info("Control total update for L2POLRNWLCOLLECTION batch starts");
			Sbcontotpf sbcontotpf = new Sbcontotpf();
			sbcontotpf.setStepname(stepExecution.getStepName());
			sbcontotpf.setUsrprf(userName);
			sbcontotpf.setBranch(branch);
			sbcontotpf.setCompany(company);
			try {
				sbcontotpf.setEffdate(DatimeUtil.covertToyyyyMMddDate(effDate));
			} catch (ParseException e) {
				e.printStackTrace();
				sbcontotpf.setEffdate(Integer.parseInt(transactionDate));
			}
			sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
			sbcontotpf.setJobnm(batchName.substring(0, 10));
			sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
			sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
			sbcontotpf.setContot2(l2CollectionControlTotalDTO.getControlTotal02());
			sbcontotpf.setContot3(l2CollectionControlTotalDTO.getControlTotal03());
			sbcontotpf.setContot4(l2CollectionControlTotalDTO.getControlTotal04());
			sbcontotpf.setContot5(l2CollectionControlTotalDTO.getControlTotal05());
			sbcontotpf.setContot6(l2CollectionControlTotalDTO.getControlTotal06());
			sbcontotpf.setContot7(l2CollectionControlTotalDTO.getControlTotal07());
			sbcontotpf.setContot8(l2CollectionControlTotalDTO.getControlTotal08());
			sbcontotpf.setContot9(l2CollectionControlTotalDTO.getControlTotal09());
			sbcontotpf.setContot10(l2CollectionControlTotalDTO.getControlTotal10());
			sbcontotpf.setContot11(l2CollectionControlTotalDTO.getControlTotal11());
			sbcontotpf.setContot12(l2CollectionControlTotalDTO.getControlTotal12());
			sbcontotpf.setContot13(l2CollectionControlTotalDTO.getControlTotal13());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);
			LOGGER.info("Control total update for L2polrnwlCollectionStepListener batch ends");
			LOGGER.info("L2polrnwlCollectionStepListener batch step completed.");
		} else {
			LOGGER.error("Some exception occurred,logging it in Elogpf.{}", 
					exceptionList.get(0).getStackTrace().toString());//IJTI-1498
			if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", programName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("L2polrnwlCollectionStepListener::afterStep::Exit");
		return stepExecution.getExitStatus();
	}


}
