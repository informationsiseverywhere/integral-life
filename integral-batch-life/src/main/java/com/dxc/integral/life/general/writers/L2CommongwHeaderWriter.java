package com.dxc.integral.life.general.writers;

import java.io.IOException;
import java.io.Writer;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

/**
 * 
 * HeaderWriter for L2commongw batch step.
 *
 */
public class L2CommongwHeaderWriter implements FlatFileHeaderCallback {

	private final String header;

	public L2CommongwHeaderWriter(String exportFileHeader) {
		this.header = exportFileHeader;
	}

	@Override
	public void writeHeader(Writer writer) throws IOException {
		writer.write(header);
	}
}
