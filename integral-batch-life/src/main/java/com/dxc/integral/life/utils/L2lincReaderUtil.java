package com.dxc.integral.life.utils;

import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import com.dxc.integral.life.beans.L2lincReaderDTO;
import com.dxc.integral.life.general.mappers.L2lincFieldSetMapper;

public class L2lincReaderUtil<T> extends DefaultLineMapper<L2lincReaderDTO> {
	
	private int totalItemsToRead;
	public L2lincReaderUtil() {
		//Added for removing Sonar Qube Errors
	};

	@Override
	public L2lincReaderDTO mapLine(String line, int lineNumber) throws Exception {
		 if(line != null && (line.startsWith("8") || line.startsWith("9"))){
             return null;
         }
		 return super.mapLine(line, lineNumber);             
	}
	
	public int getTotalItemsToRead() {     
			return totalItemsToRead;
	}

	public void setTotalItemsToRead(int totalItemsToRead) {
			this.totalItemsToRead = totalItemsToRead;
	}

	public void setFieldSetMapper(L2lincFieldSetMapper fs) {
			super.setFieldSetMapper((FieldSetMapper)fs);
	}
}
