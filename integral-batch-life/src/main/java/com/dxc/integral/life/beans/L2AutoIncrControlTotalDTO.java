package com.dxc.integral.life.beans;

import java.math.BigDecimal;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component("l2polrnwlAutoIncrControlTotalDTO")
@Lazy
public class L2AutoIncrControlTotalDTO {

	/** The controlTotal01 */
	private BigDecimal controlTotal01 = BigDecimal.ZERO;
	/** The controlTotal02 */
	private BigDecimal controlTotal02 = BigDecimal.ZERO;
	/** The controlTotal03 */
	private BigDecimal controlTotal03 = BigDecimal.ZERO;
	/** The controlTotal04 */
	private BigDecimal controlTotal04 = BigDecimal.ZERO;
	/** The controlTotal05 */
	private BigDecimal controlTotal05 = BigDecimal.ZERO;
	/** The controlTotal06 */
	private BigDecimal controlTotal06 = BigDecimal.ZERO;
	/** The controlTotal01 */
	private BigDecimal controlTotal07 = BigDecimal.ZERO;
	/** The controlTotal02 */
	private BigDecimal controlTotal08 = BigDecimal.ZERO;
	/** The controlTotal03 */
	private BigDecimal controlTotal09 = BigDecimal.ZERO;
	/** The controlTotal04 */
	private BigDecimal controlTotal10 = BigDecimal.ZERO;
	/** The controlTotal05 */
	private BigDecimal controlTotal11 = BigDecimal.ZERO;
	/** The controlTotal06 */
	private BigDecimal controlTotal12 = BigDecimal.ZERO;
	
	/**
	 * @return the controlTotal01
	 */
	public BigDecimal getControlTotal01() {
		return controlTotal01;
	}
	
	/**
	 * Setter for controlTotal01
	 * 
	 * @param controlTotal01
	 */
	public  void setControlTotal01(BigDecimal controlTotal01) {
		this.controlTotal01 = controlTotal01;
	}

	/**
	 * @return the controlTotal02
	 */
	public BigDecimal getControlTotal02() {
		return controlTotal02;
	}
	
	/**
	 *  Setter for controlTotal02
	 *  
	 * @param controlTotal02
	 */
	public  void setControlTotal02(BigDecimal controlTotal02) {
		this.controlTotal02 = controlTotal02;
	}
	/**
	 * @return the controlTotal03
	 */
	public BigDecimal getControlTotal03() {
		return controlTotal03;
	}
	
	/**
	 * Setter for controlTotal03
	 * 
	 * @param controlTotal03
	 */
	public  void setControlTotal03(BigDecimal controlTotal03) {
		this.controlTotal03 = controlTotal03;
	}

	/**
	 * @return the controlTotal04
	 */
	public BigDecimal getControlTotal04() {
		return controlTotal04;
	}
	
	/**
	 * Setter for controlTotal04
	 * 
	 * @param controlTotal04
	 */
	public  void setControlTotal04(BigDecimal controlTotal04) {
		this.controlTotal04 = controlTotal04;
	}

	/**
	 * @return the controlTotal05
	 */
	public BigDecimal getControlTotal05() {
		return controlTotal05;
	}
	
	/**
	 * Setter for controlTotal05
	 * 
	 * @param controlTotal05
	 */
	public void setControlTotal05(BigDecimal controlTotal05) {
		this.controlTotal05 = controlTotal05;
	}

	/**
	 * @return the controlTotal06
	 */
	public BigDecimal getControlTotal06() {
		return controlTotal06;
	}
	
	/**
	 * Setter for controlTotal06
	 * 
	 * @param controlTotal06
	 */
	public  void setControlTotal06(BigDecimal controlTotal06) {
		this.controlTotal06 = controlTotal06;
	}

	public BigDecimal getControlTotal07() {
		return controlTotal07;
	}

	public void setControlTotal07(BigDecimal controlTotal07) {
		this.controlTotal07 = controlTotal07;
	}

	public BigDecimal getControlTotal08() {
		return controlTotal08;
	}

	public void setControlTotal08(BigDecimal controlTotal08) {
		this.controlTotal08 = controlTotal08;
	}

	public BigDecimal getControlTotal09() {
		return controlTotal09;
	}

	public void setControlTotal09(BigDecimal controlTotal09) {
		this.controlTotal09 = controlTotal09;
	}

	public BigDecimal getControlTotal10() {
		return controlTotal10;
	}

	public void setControlTotal10(BigDecimal controlTotal10) {
		this.controlTotal10 = controlTotal10;
	}

	public BigDecimal getControlTotal11() {
		return controlTotal11;
	}

	public void setControlTotal11(BigDecimal controlTotal11) {
		this.controlTotal11 = controlTotal11;
	}

	public BigDecimal getControlTotal12() {
		return controlTotal12;
	}

	public void setControlTotal12(BigDecimal controlTotal12) {
		this.controlTotal12 = controlTotal12;
	}
	
	
}
