package com.dxc.integral.life.general.processors;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.fsu.beans.CltrelnDTO;
import com.dxc.integral.fsu.utils.Bldenrl;
import com.dxc.integral.fsu.utils.Cltreln;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.life.beans.L2ownchgControlTotalDTO;
import com.dxc.integral.life.beans.L2ownchgReaderDTO;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.utils.L2ownchgProcessorUtil;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * ItemProcessor for L2ANENDRLX batch step.
 * 
 * @author vhukumagrawa
 *
 */
@Scope(value = "step")
@Async
@Lazy
public class L2ownchgItemProcessor extends BaseProcessor implements ItemProcessor<L2ownchgReaderDTO, L2ownchgReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2ownchgItemProcessor.class);

	/** The batch transaction code for L2ANENDRLX */
	public static final String BATCH_TRAN_CODE = "BR6E";
	/** The batch job name for L2ANENDRLX */
	public static final String L2ANENDRLX_JOB_NAME = "L2OWNCHG";

	@Autowired
	private L2ownchgProcessorUtil l2ownchgProcessorUtil;

	@Autowired
	private DescpfDAO descpfDAO;

	@Autowired
	private Cltreln cltreln;

	@Autowired
	private Bldenrl bldenrl;

	@Autowired
	private CltrelnDTO cltrelnDTO;


	@Autowired
	private L2ownchgControlTotalDTO l2ownchgControlTotalDTO;

	private String programName = "BR629";

	private String bprogtam_authCode = "BR6E";

	//private String company = Companies.LIFE;

	@Autowired
	private ItempfDAO itempfDAO;

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;

	@Value("#{jobParameters['trandate']}")
	private String trandate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['chdrcoy']}")
	private String company;

	private T5679 t5679IO = null;
	private int wsaaNumber = 0;
	private Descpf t1692Descpf = null;
	private Descpf t1693Descpf = null;
	private String defaultBranch = "10";
	private Usrdpf usrdpf = null;
	private String batchBacth = "";

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public L2ownchgReaderDTO process(L2ownchgReaderDTO readerDTO) throws Exception {
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		/*ILIFE-5977*/


		readerDTO.setBatctrcde(BATCH_TRAN_CODE);
		boolean validCoverage = l2ownchgProcessorUtil.checkValidContractStatus(readerDTO);/*ILIFE-5977*/

		if (!validCoverage) {
			LOGGER.info("Invalid contract : {}", readerDTO.getChdrnum());//IJTI-1498
			l2ownchgControlTotalDTO.setControlTotal02(l2ownchgControlTotalDTO.getControlTotal02().add(BigDecimal.ONE));
			return null;
		}


		boolean softLock = softlock(readerDTO.getChdrnum(),"BR6E",batchName);
		if (!softLock) {
			LOGGER.info("Contract [{}] is already locked.", readerDTO.getChdrnum());//IJTI-1498
			l2ownchgControlTotalDTO
			.setControlTotal03(l2ownchgControlTotalDTO.getControlTotal03().add(BigDecimal.ONE));
			return null;
		}

//		int tranDate = DatimeUtil.covertToyyyyMMddDate(trandate);
//
//		String[] transactionDateParts = trandate.split("-");
		String acctYear = String.valueOf(getActYear(trandate));
		String acctMonth = String.valueOf(getActMonth(trandate));

		usrdpf = getUserDetails(userName);

		readReqSmartTableInfo(readerDTO);
		readerDTO.setUserNum(usrdpf.getUsernum().intValue());

		/*Report setting part
		L2ownchgReportDTO reportDTO = new L2ownchgReportDTO();
		reportDTO.setOccdate(datcon1.dateConvert(readerDTO.getOccdate()));
     	reportDTO.setEffdates(datcon1.dateConvert(readerDTO.getZsufcdte()));
		reportDTO.setRname(readerDTO.getCl1lsurname()+"  "+readerDTO.getCl1lgivname());
		reportDTO.setLname(readerDTO.getCl2lsurname()+"  "+readerDTO.getCl2lgivname());
		reportDTO.setClntlname(datcon1.dateConvert(readerDTO.getCl2clntdob()));
		reportDTO.setCltdob(datcon1.dateConvert(readerDTO.getCl2clntdob()));
		 */
		updateOwner(readerDTO);
		bldenrl.checkProcessMode1000(readerDTO.getChdrpfx(),readerDTO.getChdrcoy(),readerDTO.getChdrnum());
		if("".equals(batchBacth)){
			batchBacth = batcup.getBatchBatch(acctYear, acctMonth, readerDTO.getChdrcoy(), 
					BATCH_TRAN_CODE, branch, String.valueOf(usrdpf.getUsernum()));
		}
		LOGGER.info("batchBacth:{}:{}",batchBacth,readerDTO.getChdrnum());//IJTI-1498
		/*
		 * update chdrpf set validflag to 2
			insert to chdrpf a new record with validflag=1
			insert to ptrnpf a new record
			update hpadpf set procflg to 1
		 */
		Batcpf batcpf = getBatcpfInfo(trandate, businessDate,company, BATCH_TRAN_CODE,branch, usrdpf, batchName);
		L2ownchgReaderDTO reader = populateReaderDTO(readerDTO, acctMonth, acctYear,usrdpf,Integer.parseInt(trandate));
		//L2ownchgReportDTO report = l2ownchgProcessorUtil.initReportMap(reader,reportDTO, wsaaNumber++);
		L2ownchgReaderDTO processDTO = l2ownchgProcessorUtil.updateReaderDTOInfo(reader, batcpf);




		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		return processDTO;
	}


	private void readReqSmartTableInfo(L2ownchgReaderDTO readerDTO) throws IOException {
		getT5679Info(readerDTO);
		getT1692Descpf(readerDTO);
		getT1693Descpf(readerDTO);
	}

	private void getT5679Info(L2ownchgReaderDTO readerDTO) throws IOException {

		Map<String, List<Itempf>> t5679Map=itempfDAO.readSmartTableByTableName(company, "T5679", CommonConstants.IT_ITEMPFX);
		if (null == t5679IO && t5679Map.containsKey(bprogtam_authCode)) {
			List<Itempf> t5679ItemList = t5679Map.get(bprogtam_authCode);
			t5679IO = new ObjectMapper().readValue(t5679ItemList.get(0).getGenareaj(), T5679.class);
		}else if (!t5679Map.containsKey(bprogtam_authCode)){
			throw new ItemNotfoundException("Br629: Item "+programName+" not found in Table T5679 while processing contract "+readerDTO.getChdrnum());
		}
	}

	private void getT1692Descpf(L2ownchgReaderDTO readerDTO) {
		if (t1692Descpf == null) {
			t1692Descpf = descpfDAO.getDescInfo(company, "T1692", defaultBranch);
		}
		if(null==t1692Descpf){
			throw new ItemNotfoundException("Br629: Description for item "+defaultBranch+" not found in Table T1692 while processing contract "+readerDTO.getChdrnum());
		}
	}

	private void getT1693Descpf(L2ownchgReaderDTO readerDTO) {

		if (t1693Descpf == null) {
			t1693Descpf = descpfDAO.getDescInfo("0", "T1693", company);
		}
		if(null==t1693Descpf){
			throw new ItemNotfoundException("Br629: Description for item "+company+" not found in Table T1693 while processing contract "+readerDTO.getChdrnum());
		}
	}

	private void updateOwner(L2ownchgReaderDTO readerDTO) throws IOException {

		/* Remove owner role */
		cltrelnDTO.setClntpfx(readerDTO.getCownpfx());
		cltrelnDTO.setClntcoy(readerDTO.getCowncoy());
		cltrelnDTO.setClntnum(readerDTO.getCownnum());
		cltrelnDTO.setClrrrole("OW");
		cltrelnDTO.setFunction("REM");
		cltrelnDTO.setForepfx(readerDTO.getChdrpfx());
		cltrelnDTO.setForecoy(readerDTO.getChdrcoy());
		cltrelnDTO.setForenum(readerDTO.getChdrnum());
		cltreln.removeRole(cltrelnDTO);


		/*  Add new role for owner*/
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(readerDTO.getCownpfx());
		cltrelnDTO.setClntcoy(readerDTO.getCowncoy());
		cltrelnDTO.setClntnum(readerDTO.getLifcnum());
		cltrelnDTO.setClrrrole("OW");
		cltrelnDTO.setFunction("ADD");
		cltrelnDTO.setForepfx(readerDTO.getChdrpfx());
		cltrelnDTO.setForecoy(readerDTO.getChdrcoy());
		cltrelnDTO.setForenum(readerDTO.getChdrnum());
		cltreln.addRole(cltrelnDTO);

		/*    Remove despatch role*/
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(readerDTO.getCownpfx());
		cltrelnDTO.setClntcoy(readerDTO.getCowncoy());
		cltrelnDTO.setClntnum(readerDTO.getCownnum());
		cltrelnDTO.setClrrrole("DA");
		cltrelnDTO.setFunction("REM");
		cltrelnDTO.setForepfx(readerDTO.getChdrpfx());
		cltrelnDTO.setForecoy(readerDTO.getChdrcoy());
		cltrelnDTO.setForenum(readerDTO.getChdrnum());
		cltreln.removeRole(cltrelnDTO);


		/*    Add new role for despatch*/
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(readerDTO.getCownpfx());
		cltrelnDTO.setClntcoy(readerDTO.getCowncoy());
		cltrelnDTO.setClntnum(readerDTO.getLifcnum());
		cltrelnDTO.setClrrrole("DA");
		cltrelnDTO.setFunction("ADD");
		cltrelnDTO.setForepfx(readerDTO.getChdrpfx());
		cltrelnDTO.setForecoy(readerDTO.getChdrcoy());
		cltrelnDTO.setForenum(readerDTO.getChdrnum());
		cltreln.removeRole(cltrelnDTO);

	}



	/**
	 * Populates L2anendrlxReaderDTO with Batcpf table related information
	 * @param readerDTO
	 * @param acctMonth
	 * @param acctYear
	 * @return
	 */
	private L2ownchgReaderDTO populateReaderDTO(L2ownchgReaderDTO readerDTO, String acctMonth, String acctYear, Usrdpf usrdpf, int effectiveDate) {

		Integer accountingMonth = Integer.parseInt(acctMonth);
		Integer accountingYear = Integer.parseInt(acctYear);

		readerDTO.setUsrprf(usrdpf.getUserid());
		readerDTO.setJobnm(batchName);
		readerDTO.setPolicyTranno(readerDTO.getPolicyTranno() + 1);
		readerDTO.setUserNum(usrdpf.getUsernum().intValue());
		readerDTO.setBatcactmn(accountingMonth);
		readerDTO.setBatcactyr(accountingYear);
		readerDTO.setBatcbatch(batchBacth);
		readerDTO.setBatcbrn(branch);
		readerDTO.setBatccoy(company);
		readerDTO.setBatcpfx(CommonConstants.BATCH_PREFIX);
		readerDTO.setBatctrcde(BATCH_TRAN_CODE);
		readerDTO.setTermid(" ");
		readerDTO.setUsrprf(usrdpf.getUsrprf());
		readerDTO.setEffectiveDate(effectiveDate);
		return readerDTO;
	}
}