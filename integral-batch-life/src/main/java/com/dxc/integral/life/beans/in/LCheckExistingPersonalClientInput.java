package com.dxc.integral.life.beans.in;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeName(value = "LCheckExistingPersonalClientInputBean") 
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
public class LCheckExistingPersonalClientInput implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private AuthenticationInfo authenticationInfo;

	private String zkanasnmnor;
	
	private String zkanagnmnor;
	
	private String cltsex;

	private String cltDob;

	public LCheckExistingPersonalClientInput() {
		//does nothing
	}
	
	/**
	 * @return the authenticationInfo
	 */
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}

	/**
	 * @param authenticationInfo the authenticationInfo to set
	 */
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}

	/**
	 * @return the zkanasnmnor
	 */
	public String getZkanasnmnor() {
		return zkanasnmnor;
	}

	/**
	 * @param zkanasnmnor the zkanasnmnor to set
	 */
	public void setZkanasnmnor(String zkanasnmnor) {
		this.zkanasnmnor = zkanasnmnor;
	}

	/**
	 * @return the zkanagnmnor
	 */
	public String getZkanagnmnor() {
		return zkanagnmnor;
	}

	/**
	 * @param zkanagnmnor the zkanagnmnor to set
	 */
	public void setZkanagnmnor(String zkanagnmnor) {
		this.zkanagnmnor = zkanagnmnor;
	}

	/**
	 * @return the cltsex
	 */
	public String getCltsex() {
		return cltsex;
	}

	/**
	 * @param cltsex the cltsex to set
	 */
	public void setCltsex(String cltsex) {
		this.cltsex = cltsex;
	}

	/**
	 * @return the postcode
	 */
	public String getCltdob() {
		return cltDob;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setCltdob(String cltDob) {
		this.cltDob = cltDob;
	}
}
