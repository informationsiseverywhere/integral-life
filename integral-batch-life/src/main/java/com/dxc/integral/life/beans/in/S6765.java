package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;
public class S6765 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey = "";
	private String activeField;
	private S6765screensfl s6765screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S6765screensfl getS6765screensfl() {
		return s6765screensfl;
	}
	public void setS6765screensfl(S6765screensfl s6765screensfl) {
		this.s6765screensfl = s6765screensfl;
	}

	public static class S6765screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		private List<KeyValueItem> actions;
		public int size() {
			return rows.length;
		}

		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
		
		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
		
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String question = "";
			private String answer = "";


			public String getQuestion() {
				return question;
			}
			public void setQuestion(String question) {
				this.question = question;
			}
			public String getAnswer() {
				return answer;
			}
			public void setAnswer(String answer) {
				this.answer = answer;
			}
		} 	
	}

	@Override
	public String toString() {
		return "S6765 []";
	}
	
}
