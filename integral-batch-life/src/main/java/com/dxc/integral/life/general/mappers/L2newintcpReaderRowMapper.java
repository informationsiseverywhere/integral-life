package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2newintcpReaderDTO;

/**
 * RowMapper for L2newintcpReaderDTO.
 * 
 * @author dpuhawan
 */
public class L2newintcpReaderRowMapper implements RowMapper<L2newintcpReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2newintcpReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2newintcpReaderDTO l2newintcpReader = new L2newintcpReaderDTO();

		l2newintcpReader.setChdrcoy(rs.getString("chdrcoy"));
		l2newintcpReader.setChdrnum(rs.getString("chdrnum"));
		l2newintcpReader.setLoannumber(rs.getInt("loannumber"));
		l2newintcpReader.setLoantype(rs.getString("loantype"));
		l2newintcpReader.setLoancurr(rs.getString("loancurr"));
		l2newintcpReader.setValidflag(rs.getString("validflag"));
		l2newintcpReader.setLoanorigam(rs.getBigDecimal("loanorigam"));
		l2newintcpReader.setLoanstdate(rs.getInt("loanstdate"));
		l2newintcpReader.setLstcaplamt(rs.getBigDecimal("lstcaplamt"));
		l2newintcpReader.setLstcapdate(rs.getInt("lstcapdate"));
		l2newintcpReader.setNxtcapdate(rs.getInt("nxtcapdate"));
		l2newintcpReader.setLstintbdte(rs.getInt("lstintbdte"));
		l2newintcpReader.setNxtintbdte(rs.getInt("nxtintbdte"));
		l2newintcpReader.setTranno(rs.getInt("tranno"));
		l2newintcpReader.setCnttype(rs.getString("cnttype"));
		l2newintcpReader.setChdrpfx(rs.getString("chdrpfx"));
		l2newintcpReader.setServunit(rs.getString("servunit"));
		l2newintcpReader.setOccdate(rs.getInt("occdate"));
		l2newintcpReader.setCcdate(rs.getInt("ccdate"));
		l2newintcpReader.setCownpfx(rs.getString("cownpfx"));
		l2newintcpReader.setCowncoy(rs.getString("cowncoy"));
		l2newintcpReader.setCownnum(rs.getString("cownnum"));
		l2newintcpReader.setCollchnl(rs.getString("collchnl"));
		l2newintcpReader.setCntbranch(rs.getString("cntbranch"));
		l2newintcpReader.setCntcurr(rs.getString("cntcurr"));
		l2newintcpReader.setPolsum(rs.getInt("polsum"));
		l2newintcpReader.setPolinc(rs.getInt("polinc"));
		l2newintcpReader.setAgntpfx(rs.getString("agntpfx"));
		l2newintcpReader.setAgntcoy(rs.getString("agntcoy"));
		l2newintcpReader.setAgntnum(rs.getString("agntnum"));
		l2newintcpReader.setClntpfx(rs.getString("clntpfx"));
		l2newintcpReader.setClntcoy(rs.getString("clntcoy"));
		l2newintcpReader.setClntnum(rs.getString("clntnum"));
		l2newintcpReader.setMandref(rs.getString("mandref"));
		l2newintcpReader.setPtdate(rs.getInt("ptdate"));
		l2newintcpReader.setBillchnl(rs.getString("billchnl"));
		l2newintcpReader.setBillfreq(rs.getString("billfreq"));
		l2newintcpReader.setBillcurr(rs.getString("billcurr"));
		l2newintcpReader.setNextdate(rs.getInt("nextdate"));
		l2newintcpReader.setBankkey(rs.getString("bankkey"));
		l2newintcpReader.setBankacckey(rs.getString("bankacckey"));
		l2newintcpReader.setMandstat(rs.getString("mandstat"));
		l2newintcpReader.setFacthous(rs.getString("facthous"));
		return l2newintcpReader;
	}

}