package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5417 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String virtFundSplitMethod;
	private String percOrAmntInd;
	private String unitVirtualFund01;
	private String unitAllocPercAmt01;
	private String unitBidPrice01;
	private String unitVirtualFund02;
	private String unitAllocPercAmt02;
	private String unitBidPrice02;
	private String unitVirtualFund03;
	private String unitAllocPercAmt03;
	private String unitBidPrice03;
	private String unitVirtualFund04;
	private String unitAllocPercAmt04;
	private String unitBidPrice04;
	private String unitVirtualFund05;
	private String unitAllocPercAmt05;
	private String unitBidPrice05;
	private String unitVirtualFund06;
	private String unitAllocPercAmt06;
	private String unitBidPrice06;
	private String unitVirtualFund07;
	private String unitAllocPercAmt07;
	private String unitBidPrice07;
	private String unitVirtualFund08;
	private String unitAllocPercAmt08;
	private String unitBidPrice08;
	private String unitVirtualFund09;
	private String unitAllocPercAmt09;
	private String unitBidPrice09;
	private String unitVirtualFund10;
	private String unitAllocPercAmt10;
	private String unitBidPrice10;
	private String zafropt1;
	private String zafrfreq;
	private String zafritem;
	private String newFundList01;
	private String newFundList02;
	private String newFundList03;
	private String newFundList04;
	private String newFundList05;
	private String newFundList06;
	private String newFundList07;
	private String newFundList08;
	private String newFundList09;
	private String newFundList10;
	private String newFundList11;
	private String newFundList12;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getVirtFundSplitMethod() {
		return virtFundSplitMethod;
	}
	public void setVirtFundSplitMethod(String virtFundSplitMethod) {
		this.virtFundSplitMethod = virtFundSplitMethod;
	}
	public String getPercOrAmntInd() {
		return percOrAmntInd;
	}
	public void setPercOrAmntInd(String percOrAmntInd) {
		this.percOrAmntInd = percOrAmntInd;
	}
	public String getUnitVirtualFund01() {
		return unitVirtualFund01;
	}
	public void setUnitVirtualFund01(String unitVirtualFund01) {
		this.unitVirtualFund01 = unitVirtualFund01;
	}
	public String getUnitAllocPercAmt01() {
		return unitAllocPercAmt01;
	}
	public void setUnitAllocPercAmt01(String unitAllocPercAmt01) {
		this.unitAllocPercAmt01 = unitAllocPercAmt01;
	}
	public String getUnitBidPrice01() {
		return unitBidPrice01;
	}
	public void setUnitBidPrice01(String unitBidPrice01) {
		this.unitBidPrice01 = unitBidPrice01;
	}
	public String getUnitVirtualFund02() {
		return unitVirtualFund02;
	}
	public void setUnitVirtualFund02(String unitVirtualFund02) {
		this.unitVirtualFund02 = unitVirtualFund02;
	}
	public String getUnitAllocPercAmt02() {
		return unitAllocPercAmt02;
	}
	public void setUnitAllocPercAmt02(String unitAllocPercAmt02) {
		this.unitAllocPercAmt02 = unitAllocPercAmt02;
	}
	public String getUnitBidPrice02() {
		return unitBidPrice02;
	}
	public void setUnitBidPrice02(String unitBidPrice02) {
		this.unitBidPrice02 = unitBidPrice02;
	}
	public String getUnitVirtualFund03() {
		return unitVirtualFund03;
	}
	public void setUnitVirtualFund03(String unitVirtualFund03) {
		this.unitVirtualFund03 = unitVirtualFund03;
	}
	public String getUnitAllocPercAmt03() {
		return unitAllocPercAmt03;
	}
	public void setUnitAllocPercAmt03(String unitAllocPercAmt03) {
		this.unitAllocPercAmt03 = unitAllocPercAmt03;
	}
	public String getUnitBidPrice03() {
		return unitBidPrice03;
	}
	public void setUnitBidPrice03(String unitBidPrice03) {
		this.unitBidPrice03 = unitBidPrice03;
	}
	public String getUnitVirtualFund04() {
		return unitVirtualFund04;
	}
	public void setUnitVirtualFund04(String unitVirtualFund04) {
		this.unitVirtualFund04 = unitVirtualFund04;
	}
	public String getUnitAllocPercAmt04() {
		return unitAllocPercAmt04;
	}
	public void setUnitAllocPercAmt04(String unitAllocPercAmt04) {
		this.unitAllocPercAmt04 = unitAllocPercAmt04;
	}
	public String getUnitBidPrice04() {
		return unitBidPrice04;
	}
	public void setUnitBidPrice04(String unitBidPrice04) {
		this.unitBidPrice04 = unitBidPrice04;
	}
	public String getUnitVirtualFund05() {
		return unitVirtualFund05;
	}
	public void setUnitVirtualFund05(String unitVirtualFund05) {
		this.unitVirtualFund05 = unitVirtualFund05;
	}
	public String getUnitAllocPercAmt05() {
		return unitAllocPercAmt05;
	}
	public void setUnitAllocPercAmt05(String unitAllocPercAmt05) {
		this.unitAllocPercAmt05 = unitAllocPercAmt05;
	}
	public String getUnitBidPrice05() {
		return unitBidPrice05;
	}
	public void setUnitBidPrice05(String unitBidPrice05) {
		this.unitBidPrice05 = unitBidPrice05;
	}
	public String getUnitVirtualFund06() {
		return unitVirtualFund06;
	}
	public void setUnitVirtualFund06(String unitVirtualFund06) {
		this.unitVirtualFund06 = unitVirtualFund06;
	}
	public String getUnitAllocPercAmt06() {
		return unitAllocPercAmt06;
	}
	public void setUnitAllocPercAmt06(String unitAllocPercAmt06) {
		this.unitAllocPercAmt06 = unitAllocPercAmt06;
	}
	public String getUnitBidPrice06() {
		return unitBidPrice06;
	}
	public void setUnitBidPrice06(String unitBidPrice06) {
		this.unitBidPrice06 = unitBidPrice06;
	}
	public String getUnitVirtualFund07() {
		return unitVirtualFund07;
	}
	public void setUnitVirtualFund07(String unitVirtualFund07) {
		this.unitVirtualFund07 = unitVirtualFund07;
	}
	public String getUnitAllocPercAmt07() {
		return unitAllocPercAmt07;
	}
	public void setUnitAllocPercAmt07(String unitAllocPercAmt07) {
		this.unitAllocPercAmt07 = unitAllocPercAmt07;
	}
	public String getUnitBidPrice07() {
		return unitBidPrice07;
	}
	public void setUnitBidPrice07(String unitBidPrice07) {
		this.unitBidPrice07 = unitBidPrice07;
	}
	public String getUnitVirtualFund08() {
		return unitVirtualFund08;
	}
	public void setUnitVirtualFund08(String unitVirtualFund08) {
		this.unitVirtualFund08 = unitVirtualFund08;
	}
	public String getUnitAllocPercAmt08() {
		return unitAllocPercAmt08;
	}
	public void setUnitAllocPercAmt08(String unitAllocPercAmt08) {
		this.unitAllocPercAmt08 = unitAllocPercAmt08;
	}
	public String getUnitBidPrice08() {
		return unitBidPrice08;
	}
	public void setUnitBidPrice08(String unitBidPrice08) {
		this.unitBidPrice08 = unitBidPrice08;
	}
	public String getUnitVirtualFund09() {
		return unitVirtualFund09;
	}
	public void setUnitVirtualFund09(String unitVirtualFund09) {
		this.unitVirtualFund09 = unitVirtualFund09;
	}
	public String getUnitAllocPercAmt09() {
		return unitAllocPercAmt09;
	}
	public void setUnitAllocPercAmt09(String unitAllocPercAmt09) {
		this.unitAllocPercAmt09 = unitAllocPercAmt09;
	}
	public String getUnitBidPrice09() {
		return unitBidPrice09;
	}
	public void setUnitBidPrice09(String unitBidPrice09) {
		this.unitBidPrice09 = unitBidPrice09;
	}
	public String getUnitVirtualFund10() {
		return unitVirtualFund10;
	}
	public void setUnitVirtualFund10(String unitVirtualFund10) {
		this.unitVirtualFund10 = unitVirtualFund10;
	}
	public String getUnitAllocPercAmt10() {
		return unitAllocPercAmt10;
	}
	public void setUnitAllocPercAmt10(String unitAllocPercAmt10) {
		this.unitAllocPercAmt10 = unitAllocPercAmt10;
	}
	public String getUnitBidPrice10() {
		return unitBidPrice10;
	}
	public void setUnitBidPrice10(String unitBidPrice10) {
		this.unitBidPrice10 = unitBidPrice10;
	}
	public String getZafropt1() {
		return zafropt1;
	}
	public void setZafropt1(String zafropt1) {
		this.zafropt1 = zafropt1;
	}
	public String getZafrfreq() {
		return zafrfreq;
	}
	public void setZafrfreq(String zafrfreq) {
		this.zafrfreq = zafrfreq;
	}
	public String getZafritem() {
		return zafritem;
	}
	public void setZafritem(String zafritem) {
		this.zafritem = zafritem;
	}
	public String getNewFundList01() {
		return newFundList01;
	}
	public void setNewFundList01(String newFundList01) {
		this.newFundList01 = newFundList01;
	}
	public String getNewFundList02() {
		return newFundList02;
	}
	public void setNewFundList02(String newFundList02) {
		this.newFundList02 = newFundList02;
	}
	public String getNewFundList03() {
		return newFundList03;
	}
	public void setNewFundList03(String newFundList03) {
		this.newFundList03 = newFundList03;
	}
	public String getNewFundList04() {
		return newFundList04;
	}
	public void setNewFundList04(String newFundList04) {
		this.newFundList04 = newFundList04;
	}
	public String getNewFundList05() {
		return newFundList05;
	}
	public void setNewFundList05(String newFundList05) {
		this.newFundList05 = newFundList05;
	}
	public String getNewFundList06() {
		return newFundList06;
	}
	public void setNewFundList06(String newFundList06) {
		this.newFundList06 = newFundList06;
	}
	public String getNewFundList07() {
		return newFundList07;
	}
	public void setNewFundList07(String newFundList07) {
		this.newFundList07 = newFundList07;
	}
	public String getNewFundList08() {
		return newFundList08;
	}
	public void setNewFundList08(String newFundList08) {
		this.newFundList08 = newFundList08;
	}
	public String getNewFundList09() {
		return newFundList09;
	}
	public void setNewFundList09(String newFundList09) {
		this.newFundList09 = newFundList09;
	}
	public String getNewFundList10() {
		return newFundList10;
	}
	public void setNewFundList10(String newFundList10) {
		this.newFundList10 = newFundList10;
	}
	public String getNewFundList11() {
		return newFundList11;
	}
	public void setNewFundList11(String newFundList11) {
		this.newFundList11 = newFundList11;
	}
	public String getNewFundList12() {
		return newFundList12;
	}
	public void setNewFundList12(String newFundList12) {
		this.newFundList12 = newFundList12;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5417 []";
	}
	
}
