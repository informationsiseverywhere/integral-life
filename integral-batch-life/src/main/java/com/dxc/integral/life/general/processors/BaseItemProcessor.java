package com.dxc.integral.life.general.processors;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.BprdpfDAO;
import com.dxc.integral.iaf.dao.BsscpfDAO;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Bprdpf;
import com.dxc.integral.iaf.dao.model.Bsscpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.life.exception.ItemProcessorInitException;

public abstract class BaseItemProcessor<I, O> implements ItemProcessor<I, O>, InitializingBean {
	@Autowired
	private BprdpfDAO bprdpfDAO;
	
	@Autowired
	private BsscpfDAO bsscpfDAO;
	
	@Autowired
	private UsrdpfDAO usrdpfDAO;
	
	@Autowired
	private Batcup batcup;

	private Bprdpf bprdpf;
	private Bsscpf bsscpf;
	private Batcpf batchDetail;
	private Usrdpf usrdpf;
	
	// B program name which is set by xml
	private String programName;

	@Value("#{jobParameters['userName']}")
	protected String userName;

	@Value("#{jobParameters['cntBranch']}")
	protected String branch;
	
	@Value("#{jobParameters['trandate']}")
	protected String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	protected String businessDate;

	@Value("#{jobParameters['batchName']}")
	protected String batchName;
	
	@Value("#{jobParameters['bschednum']}")
	protected Integer bschednum;
	
	protected static final String DEFLANG = CommonConstants.LANGUAGE;
	protected final static String batchCompany = Companies.LIFE;
	
	
	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}
	
	public Bprdpf getBprdpf() {
		return bprdpf;
	}

	public Bsscpf getBsscpf() {
		return bsscpf;
	}

	public Batcpf getBatchDetail() {
		return batchDetail;
	}

	public Usrdpf getUsrdpf() {
		return usrdpf;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		init();
		Assert.notNull(bprdpf, "The 'bprdpf' may not be null");
		Assert.notNull(usrdpf, "The 'usrdpf' may not be null");
		Assert.notNull(bsscpf, "The 'bsscpf' may not be null");
		Assert.notNull(batchDetail, "The 'batchDetail' may not be null");
	}

	private void init() {
		if (bprdpf == null)
			bprdpf = bprdpfDAO.getProcessDefine(getProgramName(), batchCompany);
		if (usrdpf == null) {
			usrdpf = usrdpfDAO.getUserInfo(userName);
		}
		if (batchDetail == null) {
			try {
				batchDetail = batcup.getBatchDetails(transactionDate, businessDate, Companies.LIFE, bprdpf.getBauthcode(), branch,
						usrdpf, batchName.substring(0, 10));
			} catch (ParseException e) {
				throw new ItemProcessorInitException(e);
			}
		}
		if (bsscpf == null) {
			bsscpf = bsscpfDAO.getScheduleInfo(batchName, bschednum);
			if (bsscpf == null) {
				bsscpf = new Bsscpf();
				SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
				try {
					bsscpf.setBprceffdat(sd.parse(transactionDate));
				} catch (ParseException e) {
					throw new ItemProcessorInitException(e);
				}
				bsscpf.setBschednum(bschednum);
				bsscpf.setLanguage(DEFLANG);
			}
		}
	}
}
