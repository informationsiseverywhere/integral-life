package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2BillingReaderDTO;

/**
 * RowMapper for L2polrnwlBillingReaderDTO.
 * 
 * @author wli31
 */
public class L2BillingReaderRowMapper implements RowMapper<L2BillingReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2BillingReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2BillingReaderDTO l2billingReader = new L2BillingReaderDTO();
		l2billingReader.setChdrcoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
		l2billingReader.setChdrnum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");
		l2billingReader.setPayrseqno(rs.getInt("PAYRSEQNO"));
		l2billingReader.setBillsupr(rs.getString("BILLSUPR")!= null ? (rs.getString("BILLSUPR").trim()) : "");
		l2billingReader.setBillspfrom(rs.getInt("BILLSPFROM"));
		l2billingReader.setBillspto(rs.getInt("BILLSPTO"));
		l2billingReader.setBillchnl(rs.getString("BILLCHNL")!= null ? (rs.getString("BILLCHNL").trim()) : "");
		l2billingReader.setBillcd(rs.getInt("BILLCD"));
		l2billingReader.setPayrtranno(rs.getInt("PAYRTRANNO"));
		l2billingReader.setPayrbillcd(rs.getInt("PAYRBILLCD"));
		l2billingReader.setBtdate(rs.getInt("BTDATE"));
		l2billingReader.setPtdate(rs.getInt("PTDATE"));
		l2billingReader.setOutstamt(rs.getBigDecimal("OUTSTAMT"));
		l2billingReader.setNextdate(rs.getInt("NEXTDATE"));
		l2billingReader.setPayrbillcurr(rs.getString("PAYRBILLCURR")!= null ? (rs.getString("PAYRBILLCURR").trim()) : "");
		l2billingReader.setBillfreq(rs.getString("BILLFREQ")!= null ? (rs.getString("BILLFREQ").trim()) : "");
		l2billingReader.setDuedd(rs.getString("DUEDD") != null ? (rs.getString("DUEDD").trim()) : "");
		l2billingReader.setDuemm(rs.getString("DUEMM")!= null ? (rs.getString("DUEMM").trim()) : "");
		l2billingReader.setBillday(rs.getString("BILLDAY")!= null ? (rs.getString("BILLDAY").trim()) : "");
		l2billingReader.setBillmonth(rs.getString("BILLMONTH"));
		l2billingReader.setSinstamt01(rs.getBigDecimal("SINSTAMT01"));
		l2billingReader.setSinstamt02(rs.getBigDecimal("SINSTAMT02"));	
		l2billingReader.setSinstamt03(rs.getBigDecimal("SINSTAMT03"));
		l2billingReader.setSinstamt04(rs.getBigDecimal("SINSTAMT04"));				
		l2billingReader.setSinstamt05(rs.getBigDecimal("SINSTAMT05"));
		l2billingReader.setSinstamt06(rs.getBigDecimal("SINSTAMT06"));				
		l2billingReader.setPayrcntcurr(rs.getString("PAYRCNTCURR")!= null ? (rs.getString("PAYRCNTCURR").trim()) : "");
		l2billingReader.setMandref(rs.getString("MANDREF") != null ? (rs.getString("MANDREF")) : "");				
		l2billingReader.setGrupcoy(rs.getString("GRUPCOY")!= null ? (rs.getString("GRUPCOY").trim()) : "");
		l2billingReader.setGrupnum(rs.getString("GRUPNUM")!= null ? (rs.getString("GRUPNUM").trim()) : "");				
		l2billingReader.setMembsel(rs.getString("MEMBSEL")!= null ? (rs.getString("MEMBSEL").trim()) : "");
		l2billingReader.setTaxrelmth(rs.getString("TAXRELMTH")!= null ? (rs.getString("TAXRELMTH").trim()) : "");				
		l2billingReader.setIncseqno(rs.getInt("INCSEQNO"));
		l2billingReader.setEffdate(rs.getInt("EFFDATE"));
		l2billingReader.setChdruniqueno(rs.getLong("CHDRUNIQUENO"));				
		l2billingReader.setChdrtranno(rs.getInt("CHDRTRANNO"));
		l2billingReader.setCnttype(rs.getString("CNTTYPE")!= null ? (rs.getString("CNTTYPE").trim()) : "");				
		l2billingReader.setOccdate(rs.getInt("OCCDATE"));
		l2billingReader.setStatcode(rs.getString("STATCODE")!= null ? (rs.getString("STATCODE").trim()) : "");				
		l2billingReader.setPstcde(rs.getString("PSTCDE")!= null ? (rs.getString("PSTCDE").trim()) : "");
		l2billingReader.setReg(rs.getString("REG")!= null ? (rs.getString("REG").trim()) : "");				
		l2billingReader.setChdrcntcurr(rs.getString("CHDRCNTCURR")!= null ? (rs.getString("CHDRCNTCURR").trim()) : "");
		l2billingReader.setBillcurr(rs.getString("BILLCURR")!= null ? (rs.getString("BILLCURR").trim()) : "");	
		l2billingReader.setChdrpfx(rs.getString("CHDRPFX")!= null ? (rs.getString("CHDRPFX").trim()) : "");
		l2billingReader.setCowncoy(rs.getString("COWNCOY")!= null ? (rs.getString("COWNCOY").trim()) : "");
		l2billingReader.setCownnum(rs.getString("COWNNUM")!= null ? (rs.getString("COWNNUM").trim()) : "");
		l2billingReader.setCntbranch(rs.getString("CNTBRANCH")!= null ? (rs.getString("CNTBRANCH").trim()) : "");
		l2billingReader.setServunit(rs.getString("SERVUNIT")!= null ? (rs.getString("SERVUNIT").trim()) : "");
		l2billingReader.setCcdate(rs.getInt("CCDATE"));
		l2billingReader.setCollchnl(rs.getString("COLLCHNL")!= null ? (rs.getString("COLLCHNL").trim()) : "");
		l2billingReader.setCownpfx(rs.getString("COWNPFX")!= null ? (rs.getString("COWNPFX").trim()) : "");
		l2billingReader.setAgntpfx(rs.getString("AGNTPFX")!= null ? (rs.getString("AGNTPFX").trim()) : "");
		l2billingReader.setAgntcoy(rs.getString("AGNTCOY")!= null ? (rs.getString("AGNTCOY").trim()) : "");
		l2billingReader.setAgntnum(rs.getString("AGNTNUM")!= null ? (rs.getString("AGNTNUM").trim()) : "");
		l2billingReader.setChdrbillchnl(rs.getString("CHDRBILLCHNL")!= null ? (rs.getString("CHDRBILLCHNL").trim()) : "");				
		l2billingReader.setAcctmeth(rs.getString("ACCTMETH")!= null ? (rs.getString("ACCTMETH").trim()) : "");
		l2billingReader.setClntpfx(rs.getString("CLNTPFX")!= null ? (rs.getString("CLNTPFX").trim()) : "");
		l2billingReader.setClntcoy(rs.getString("CLNTCOY")!= null ? (rs.getString("CLNTCOY").trim()) : "");
		l2billingReader.setClntnum(rs.getString("CLNTNUM")!= null ? (rs.getString("CLNTNUM").trim()) : "");
		l2billingReader.setBankkey(rs.getString("BANKKEY") != null ? (rs.getString("BANKKEY")) : "");
		l2billingReader.setBankacckey(rs.getString("BANKACCKEY") != null ? (rs.getString("BANKACCKEY")) : "");
		l2billingReader.setMandstat(rs.getString("MANDSTAT") != null ? (rs.getString("MANDSTAT")) : "");	
		l2billingReader.setFacthous(rs.getString("FACTHOUS") != null ? (rs.getString("FACTHOUS")) : "");
		
		
		return l2billingReader;
	}

}