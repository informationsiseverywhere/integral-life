package com.dxc.integral.life.beans.in;

import java.io.Serializable;
import com.dxc.integral.life.beans.servicebean.LcreatepersonalclientServiceInputBean;
import com.dxc.integral.life.beans.servicebean.LcreatecorporateclientServiceInputBean;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class CheckExistingClientInput implements Serializable{
	private static final long serialVersionUID = 1L;
	private String zkanasnmnor;
	private String zkanagnmnor;
	private String sex;
	private String dob;
	private String postcode;
	@JsonProperty("LCreatePersonalClientServiceInputBean")
	private LcreatepersonalclientServiceInputBean lCreatePersonalClientServiceInputBean;
	@JsonProperty("LCreateCorporateClientServiceInputBean")
	private LcreatecorporateclientServiceInputBean lCreateCorporateClientServiceInputBean;
	
	public CheckExistingClientInput() {
		//does nothing
	}

	/**
	 * @return the zkanasnmnor
	 */
	public String getZkanasnmnor() {
		return zkanasnmnor;
	}

	/**
	 * @param zkanasnmnor the zkanasnmnor to set
	 */
	public void setZkanasnmnor(String zkanasnmnor) {
		this.zkanasnmnor = zkanasnmnor;
	}

	/**
	 * @return the zkanagnmnor
	 */
	public String getZkanagnmnor() {
		return zkanagnmnor;
	}

	/**
	 * @param zkanagnmnor the zkanagnmnor to set
	 */
	public void setZkanagnmnor(String zkanagnmnor) {
		this.zkanagnmnor = zkanagnmnor;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex the cltSex to set
	 */
	public void setSex(String cltsex) {
		this.sex = cltsex;
	}

	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob the cltDob to set
	 */
	public void setDob(String cltDob) {
		this.dob = cltDob;
	}

	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * @return the lCreatePersonalClientServiceInputBean
	 */
	public LcreatepersonalclientServiceInputBean getlCreatePersonalClientServiceInputBean() {
		return lCreatePersonalClientServiceInputBean;
	}

	/**
	 * @param lCreatePersonalClientServiceInputBean the lCreatePersonalClientServiceInputBean to set
	 */
	public void setlCreatePersonalClientServiceInputBean(
			LcreatepersonalclientServiceInputBean lCreatePersonalClientServiceInputBean) {
		this.lCreatePersonalClientServiceInputBean = lCreatePersonalClientServiceInputBean;
	}

	/**
	 * @return the lCreateCorporateClientServiceInputBean
	 */
	public LcreatecorporateclientServiceInputBean getlCreateCorporateClientServiceInputBean() {
		return lCreateCorporateClientServiceInputBean;
	}

	/**
	 * @param lCreateCorporateClientServiceInputBean the lCreateCorporateClientServiceInputBean to set
	 */
	public void setlCreateCorporateClientServiceInputBean(
			LcreatecorporateclientServiceInputBean lCreateCorporateClientServiceInputBean) {
		this.lCreateCorporateClientServiceInputBean = lCreateCorporateClientServiceInputBean;
	}
}
