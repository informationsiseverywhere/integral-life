package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr5ah implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private Sr5ahscreensfl sr5ahscreensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public Sr5ahscreensfl getSr5ahscreensfl() {
		return sr5ahscreensfl;
	}
	public void setSr5ahscreensfl(Sr5ahscreensfl sr5ahscreensfl) {
		this.sr5ahscreensfl = sr5ahscreensfl;
	}


	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Sr5ahscreensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private  List<KeyValueItem> actions;
	
		public  List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList( List<KeyValueItem> actions) {
			this.actions = actions;
		}
		
		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String select;
			private String excda;
			private String longdesc;
			private String prntstat;

			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
			public String getExcda() {
				return excda;
			}
			public void setExcda(String excda) {
				this.excda = excda;
			}
			public String getLongdesc() {
				return longdesc;
			}
			public void setLongdesc(String longdesc) {
				this.longdesc = longdesc;
			}
			public String getPrntstat() {
				return prntstat;
			}
			public void setPrntstat(String prntstat) {
				this.prntstat = prntstat;
			}
		} 
	}


	@Override
	public String toString() {
		return "Sr5ah []";
	}
	
}
