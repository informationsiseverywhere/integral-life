package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2anendrlxReaderDTO;

/**
 * ItemWriter for L2ANENDRLX batch step.
 * 
 * @author vhukumagrawa
 *
 */
@Async
public class L2anendrlxItemWriter implements ItemWriter<L2anendrlxReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2anendrlxItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;
	
	@Autowired
	private Batcup batcup = null;
	
	/** The batch transaction code for L2ANENDRLX */
	public static final String BATCH_TRAN_CODE = "BA67";
	
	@Value("#{jobParameters['businessDate']}")
	private String businessDate;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;

	private int tranCount = 0;
    private ItemWriter<L2anendrlxReaderDTO> l2anendrlxChdrpfWriter;
    private ItemWriter<L2anendrlxReaderDTO> l2anendrlxPtrnpfWriter;
    private ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfInsertWriter;    
    private ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfUpdateWriter; 
    
    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
	@Override
	public void write(List<? extends L2anendrlxReaderDTO> readerDTOList) throws Exception {
		
		LOGGER.info("L2anendrlxItemWriter starts.");
		l2anendrlxZraepfUpdateWriter.write(readerDTOList);
		l2anendrlxChdrpfWriter.write(readerDTOList);
		l2anendrlxZraepfInsertWriter.write(readerDTOList);
		l2anendrlxPtrnpfWriter.write(readerDTOList); 
        synchronized (L2anendrlxItemWriter.class) {
        	processBatcpf(readerDTOList);
        }
		if (!readerDTOList.isEmpty()) {
			if (0 >= readerDTOList.get(0).getChdrnum()
					.compareToIgnoreCase(readerDTOList.get(readerDTOList.size() - 1).getChdrnum())) {
				sftlock.unlockByRange("CH", readerDTOList.get(0).getChdrcoy(), readerDTOList.get(0).getChdrnum(),
						readerDTOList.get(readerDTOList.size() - 1).getChdrnum());
			} else {
				sftlock.unlockByRange("CH", readerDTOList.get(0).getChdrcoy(),
						readerDTOList.get(readerDTOList.size() - 1).getChdrnum(), readerDTOList.get(0).getChdrnum());

			}
		}	
		
		LOGGER.info("L2anendrlxItemWriter ends.");
	}

	public void setL2anendrlxChdrpfWriter(ItemWriter<L2anendrlxReaderDTO> l2anendrlxChdrpfWriter) {
		this.l2anendrlxChdrpfWriter = l2anendrlxChdrpfWriter;
	}
	
	public void setL2anendrlxPtrnpfWriter(ItemWriter<L2anendrlxReaderDTO> l2anendrlxPtrnpfWriter) {
		this.l2anendrlxPtrnpfWriter = l2anendrlxPtrnpfWriter;
	}
	
	public void setL2anendrlxZraepfInsertWriter(ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfInsertWriter) {
		this.l2anendrlxZraepfInsertWriter = l2anendrlxZraepfInsertWriter;
	}

	public void setL2anendrlxZraepfUpdateWriter(ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfUpdateWriter) {
		this.l2anendrlxZraepfUpdateWriter = l2anendrlxZraepfUpdateWriter;
	}

	/**
	 * Process batcpf information
	 * Checks if a record is present in Batcpf table based on the input parameters , 
	 * if the record is not present, then inserts the record 
	 * finally updates the tranCnt of batcpf record based on the input parameter tranCount
	 * @param readerDTOList
	 */
	private void processBatcpf(List<? extends L2anendrlxReaderDTO> readerDTOList) throws ParseException {

		if (!readerDTOList.isEmpty()) {
			L2anendrlxReaderDTO readerDTO = readerDTOList.get(0);
			tranCount = batcup.processBatcpf(String.valueOf(readerDTO.getBatcactyr()),
					String.valueOf(readerDTO.getBatcactmn()), businessDate, readerDTO.getChdrcoy(),
					readerDTO.getBatctrcde(), readerDTO.getBatcbrn(), batchName, readerDTO.getBatcbatch(),
					readerDTO.getUsrprf(), String.valueOf(readerDTO.getUserNum()), readerDTOList.size(), tranCount);
			LOGGER.info("tranCount:{}", tranCount);//IJTI-1498
		}

	}
	
}
