package com.dxc.integral.life.general.listeners;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.L2AnnyProcControlTotalDTO;

public class L2AnnyProcStepListener extends IntegralStepListener{
	

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AnnyProcStepListener.class);

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private L2AnnyProcControlTotalDTO l2AnnyProcControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;
	
	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['chdrcoy']}")
	private String company;
	
	@Value("#{jobParameters['trandate']}")
	private String effDate;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Value("#{jobParameters['transactionDate']}") 
	private String transactionDate;
	
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private ElogpfDAO elogpfDAO;

	private static String programName = "BR618";

	
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("L2AnnyProcStepListener::afterStep::Entry");
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.info("Control total update for L2AnnyProcStepListener batch starts");
			Sbcontotpf sbcontotpf = new Sbcontotpf();
			sbcontotpf.setStepname(stepExecution.getStepName());
			sbcontotpf.setUsrprf(userName);
			sbcontotpf.setBranch(branch);
			sbcontotpf.setCompany(company);
			sbcontotpf.setEffdate(Integer.parseInt(effDate));
			sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
			sbcontotpf.setJobnm(batchName);
			sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
			sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
			sbcontotpf.setContot2(l2AnnyProcControlTotalDTO.getControlTotal02());
			sbcontotpf.setContot4(l2AnnyProcControlTotalDTO.getControlTotal04());
			sbcontotpf.setContot5(l2AnnyProcControlTotalDTO.getControlTotal05());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);
			LOGGER.info("Control total update for L2AnnyProcStepListener batch ends");
			LOGGER.info("L2AnnyProcStepListener batch step completed.");
		} else {
			LOGGER.error("Some exception occurred,logging it in Elogpf.{}", 
					exceptionList.get(0).getStackTrace().toString());//IJTI-1498
			if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", programName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("L2AnnyProcStepListener::afterStep::Exit");
		return stepExecution.getExitStatus();
	}
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("L2AnnyProcStepListener::beforeStep::Entry");
		super.beforeStep(stepExecution);;
		LOGGER.debug("L2AnnyProcStepListener::beforeStep::Exit");
	}
}
