package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.stereotype.Service;
import com.dxc.integral.life.beans.L2lincsndReaderDTO;
import com.dxc.integral.life.config.L2lincsndJsonMapping;
import com.dxc.integral.life.dao.model.Slncpf;
import com.dxc.integral.life.utils.L2lincsndCommonsUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author gsaluja2
 *
 */

@Service("l2lincsndCommonsUtil")
public class L2lincsndCommonsUtilImpl implements L2lincsndCommonsUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(L2lincsndCommonsUtilImpl.class);
		
	public String setDataFormat(Map<String, Integer> formattedStringMap) {
		StringBuilder formatString = new StringBuilder();
		String zero ="0";
		Field[] fields = null;
		try {
			fields = Slncpf.class.getDeclaredFields();
		}
		catch(Exception e) {
			LOGGER.info("Exception in Util :: setDataFormat", e);
		}
		if(fields!=null && fields.length > 0) {
			for(int i=0;i<=fields.length-4;i++) {
				if(String.valueOf(fields[i].getType()).indexOf("java.lang.String") >=0) {
					formatString.append("%-"+formattedStringMap.get(fields[i].getName())+"s");
				}
				else if(String.valueOf(fields[i].getType()).indexOf("java.math.BigDecimal") >=0) {
					formatString.append("%"+zero+formattedStringMap.get(fields[i].getName())+"."+zero+"f");
				}
				else if(String.valueOf(fields[i].getType()).indexOf(Integer.TYPE.toString()) >=0) {
					formatString.append("%"+zero+formattedStringMap.get(fields[i].getName())+"d");
				}
			}
		}
		return formatString.toString();
	}
	
	public L2lincsndJsonMapping fetchAllJsonData(){
		ObjectMapper mapper = new ObjectMapper();
		L2lincsndJsonMapping jsonNodeMapping = null;
		try {
			jsonNodeMapping = mapper.readValue(new InputStreamReader(this.getClass().getClassLoader()
					.getResourceAsStream("L2lincsndMapping.json")), L2lincsndJsonMapping.class);
		}
		catch(IOException e) {
			LOGGER.info("Exception in reading JSON :: fetchAllJsonData", e);
		}
		return jsonNodeMapping;
	}
	
	public Map<String, Integer> getJsonDataFormatNode(Map<String, String> dataFormatListMap) {
		Map<String, Integer> formattedStringMap = new LinkedHashMap<>();
		for(Map.Entry<String, String> jsonFields : dataFormatListMap.entrySet())
			formattedStringMap.put(jsonFields.getKey(), Integer.parseInt(jsonFields.getValue()));
		return formattedStringMap;
	}
	
	public Map<String, FlatFileItemWriter<L2lincsndReaderDTO>> getJsonFileType(Map<String, ?> fileTypeListMap) {
		Map<String, FlatFileItemWriter<L2lincsndReaderDTO>> jsonMap = new HashMap<>();
		for(Map.Entry<String, ?> entry: fileTypeListMap.entrySet())
			jsonMap.put(entry.getKey(), new FlatFileItemWriter<>());
		return jsonMap;
	}
	
	public String getJsonHeader(Map<String, ?> headerListMap, int transactionDate, String coy, String key) {
		StringBuilder header = new StringBuilder();
		Map<?,?> dataTypeMap;
		String zero="0";
		for(Map.Entry<String, ?> entry: headerListMap.entrySet()) {
			if(entry.getValue() instanceof Map<?,?>) {
				dataTypeMap = (Map<?, ?>) entry.getValue();
				if(dataTypeMap!=null)
					header.append(dataTypeMap.get(key));
			}
			else
				header.append(entry.getValue());
			if(entry.getKey().equals("companyCode"))
				header.append(StringUtils.leftPad(StringUtils.EMPTY, 3, zero)).append(coy)
				.append(StringUtils.leftPad(StringUtils.EMPTY, 5, zero));
			if(entry.getKey().equals("sentDate"))
				header.append(transactionDate);
		}
		return header.toString();
	}
	
	public String getJsonFooter(Map<String, String> trailerEndListMap, boolean appendLine, StringBuilder footer) {
		if(footer == null)
			footer = new StringBuilder();
		for(Map.Entry<String, String> entry: trailerEndListMap.entrySet()) {
			footer.append(entry.getValue());
		}
		if(appendLine)
			footer.append(System.lineSeparator());
		return footer.toString();
	}
	
	public Map<String, List<L2lincsndReaderDTO>> convertListToMap(List<? extends L2lincsndReaderDTO> readerDTOList,
			Map<String, ?> jsonDataMap, List<String> resetTranscdList) {
		Map<String, List<L2lincsndReaderDTO>> readerDTOListMap = new HashMap<>();
		List<?> dataList1 = new ArrayList<>();
		Map<String, String> dataMap = new HashMap<>();
		Map<String, String> newDataMap = new HashMap<>();
		for(Map.Entry<String, ?> entry: jsonDataMap.entrySet()) {
			if(entry.getValue() instanceof List<?>) {
				dataList1 = (List<?>) entry.getValue();
			}
			else {
				newDataMap.put((String) entry.getValue(), entry.getKey());
			}
			if(!dataList1.isEmpty())
				for(Object data: dataList1)
					dataMap.put((String) data, entry.getKey());
		}
		if(!dataMap.isEmpty())
			newDataMap.putAll((Map<? extends String, ? extends String>) dataMap);
		iterateMap(readerDTOListMap, readerDTOList, newDataMap, resetTranscdList);
		return readerDTOListMap;
	}
	
	public Map<String, List<L2lincsndReaderDTO>> iterateMap(Map<String, List<L2lincsndReaderDTO>> readerDTOListMap,
			List<? extends L2lincsndReaderDTO> readerDTOList, Map<String, String> newDataMap,
			List<String> resetTranscdList) {
		for(L2lincsndReaderDTO readerObject: readerDTOList) {
			for(Map.Entry<String, String> mapEntry : newDataMap.entrySet()) {
				if(mapEntry.getKey().equals(readerObject.getZreclass())) {
					readerDTOListMap = generateReaderDTOListMap(readerDTOListMap, mapEntry, readerObject, resetTranscdList);
					break;
				}
			}
		}
		return readerDTOListMap;
	}
	
	public Map<String, List<L2lincsndReaderDTO>> generateReaderDTOListMap(
			Map<String, List<L2lincsndReaderDTO>> readerDTOListMap, Map.Entry<String, String> jsonDataMapEntry,
			L2lincsndReaderDTO readerObject, List<String> resetTranscdList){
		if(resetTranscdList.contains(readerObject.getLincpf().getBatctrcde().trim()))
			readerObject.setZlincdte(0);
		if(readerDTOListMap.containsKey(jsonDataMapEntry.getValue())) {
			readerDTOListMap.get(jsonDataMapEntry.getValue()).add(readerObject);
		}
		else {
			List<L2lincsndReaderDTO> l2lincsndReaderReturnList = new ArrayList<>();
			l2lincsndReaderReturnList.add(readerObject);
			readerDTOListMap.put(jsonDataMapEntry.getValue(), l2lincsndReaderReturnList);
		}
		return readerDTOListMap;
	}
}
