package com.dxc.integral.life.utils;

import java.util.List;

import com.dxc.integral.life.beans.L2CollectionReaderDTO;

public interface L2CollectionItemWriterUtil {

	public void process(List<L2CollectionReaderDTO> readerDTOList, int tranDate);
}
