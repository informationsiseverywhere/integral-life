package com.dxc.integral.life.general.readers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.AgncypfDAO;
import com.dxc.integral.fsu.dao.AgntcypfDAO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.ClntpfDAO;
import com.dxc.integral.fsu.dao.MandpfDAO;
import com.dxc.integral.fsu.dao.TtrcpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Agncypf;
import com.dxc.integral.fsu.dao.model.Agntcypf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.dao.model.Clntpf;
import com.dxc.integral.fsu.smarttable.pojo.T3584;
import com.dxc.integral.fsu.smarttable.pojo.Tr386;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.L2CommongwDTO;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.ExclpfDAO;
import com.dxc.integral.life.dao.HpadpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PcddpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.ResnpfDAO;
import com.dxc.integral.life.dao.model.Clexpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hpadpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Pcddpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.smarttable.pojo.T1673;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.Tjl21;
import com.dxc.integral.life.smarttable.pojo.Tjl27;
import com.dxc.integral.life.smarttable.pojo.Tjl29;
import com.dxc.integral.life.smarttable.pojo.Tr663;

/**
 * 
 * @author vrathi4
 *
 */

public class L2CommongwItemReaderS1 implements ItemReader<L2CommongwDTO> {
	
	@Value("#{jobParameters['userName']}")
	private String userName;
	@Value("#{jobParameters['effectiveDate']}")
	private String effectiveDate;
	@Value("#{stepExecution}")
	private StepExecution stepExecution;

	@Autowired
	private AgncypfDAO agncypfDAO;
	@Autowired
	private AgntcypfDAO agntcypfDAO;
	@Autowired
	private MandpfDAO mandpfDAO;
	@Autowired
	private TtrcpfDAO ttrcpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private PcddpfDAO pcddpfDAO;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	@Autowired
	private ClntpfDAO clntpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private HpadpfDAO hpadpfDAO;
	@Autowired
	private ExclpfDAO exclpfDAO;
	@Autowired
	private ClexpfDAO clexpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private AcblpfDAO acblpfDAO;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private ResnpfDAO resnpfDAO;

	private Chdrpf chdrpf;
	private Ptrnpf ptrnpf;
	private Lifepf lifepf;
	private Clntpf clntpf;
	private Clntpf insrdClnt;
	// Collection
	private List<L2CommongwDTO> list = new ArrayList<>();
	private Map<String, Descpf> t5687ItemsMap;
	private Map<String, List<Covrpf>> covrpfListMap;
	// Smart tables
	private Tr663 tr663Item;
	private T1673 t1673Item;
	private Tjl27 tjl27Item;
	private Tjl21 tjl21Item;
	private Tjl29 tjl29ItemInsrd;
	private Tjl29 tjl29ItemOwnr;
	private T3584 t3584Item;
	// primitive variable
	private int recordCount = 0;
	private static final String SPACE = " ";
	private static final String ZERO = "0";
	private String agncynum;
	private String agntnum;
	private boolean dataTypeFlag =false;
	private boolean isOwnerCorporate = false;	//ILJ-708

	@SuppressWarnings("unused")
	private void init(){

		List<Agncypf> agncyList = agncypfDAO.getAgncypfByDate(effectiveDate);
		for (Agncypf agncy : agncyList) {
			agncynum=agncy.getAgncynum();
			List<Agntcypf> agntcyList = agntcypfDAO.getAgntcy(agncynum, effectiveDate);
			for(Agntcypf agntcy:agntcyList) {
				agntnum = agntcy.getAgntnum();
				List<Pcddpf> pcddpfList = pcddpfDAO.getPcddByAgntnum(Companies.LIFE, agntnum);
				for (Pcddpf pcddpf : pcddpfList) {
					ptrnpf = ptrnpfDAO.getPtrnByTrn(Companies.LIFE, pcddpf.getChdrnum());
					chdrpf = chdrpfDAO.readRecordOfChdr(Companies.LIFE, pcddpf.getChdrnum());
					if(chdrpf == null)
						continue;
					if(Integer.parseInt(effectiveDate)==agncy.getIpdate()) {
						if(ptrnpf.getDatesub()>Integer.parseInt(effectiveDate)) {
							continue;
						} else {
							T5679 t5679Item = itempfService.readSmartTableByTrim(Companies.LIFE, "T5679", "BASM", T5679.class);
							T5679 t5679Itemcont = itempfService.readSmartTableByTrim(Companies.LIFE, "T5679", "BASMA", T5679.class);
							if (!t5679Item.getCnRiskStats().contains(chdrpf.getStatcode()) && !t5679Itemcont.getCnRiskStats().contains(chdrpf.getStatcode())) {	//ILJ-699
								continue;
					     	}
						}
						dataTypeFlag=false; //ILJ-706
					 }else if(ptrnpf.getDatesub()>agncy.getPpdate() && ptrnpf.getDatesub()<=Integer.parseInt(effectiveDate)){
						 Tjl21 tjl21 = itempfService.readSmartTableByTrim(Companies.LIFE, "Tjl21", ptrnpf.getBatctrcde(), Tjl21.class);
						 if(tjl21 !=null)
							 dataTypeFlag=true;
						 else
							 continue;
					 }else {
						 continue;
					 }
					clntpf = clntpfDAO.searchCltsRecord(CommonConstants.CLNTPFX_CN, Companies.FSU, chdrpf.getCownnum());
					lifepf = lifepfDAO.getValidLifeRecord(Companies.LIFE, chdrpf.getChdrnum(), "01","00");
					insrdClnt = clntpfDAO.searchCltsRecord(CommonConstants.CLNTPFX_CN, Companies.FSU, lifepf.getLifcnum());
					List<String> chdrnumList = new ArrayList<>();
					chdrnumList.add(chdrpf.getChdrnum());
					covrpfListMap = covrpfDAO.searchCovrpf(Companies.LIFE, chdrnumList);
					readTables();
					list.add(setReaderDTO());
				}
			}
		}
	}

	private void readTables() {

		t1673Item = itempfService.readSmartTableByTrim(Companies.LIFE, "T1673", chdrpf.getBillfreq(), T1673.class);
		tjl27Item = itempfService.readSmartTableByTrim(Companies.LIFE, "Tjl27", chdrpf.getBillchnl(), Tjl27.class);
		tjl21Item = itempfService.readSmartTableByTrim(Companies.LIFE, "Tjl21", ptrnpf.getBatctrcde(), Tjl21.class);
		tjl29ItemInsrd = itempfService.readSmartTableByTrim(Companies.LIFE, "Tjl29", lifepf.getCltsex(), Tjl29.class);
		if(clntpf.getClttype().equals("P")) {	//ILJ-708
			tjl29ItemOwnr = itempfService.readSmartTableByTrim(Companies.LIFE, "Tjl29", clntpf.getCltsex(), Tjl29.class);
			isOwnerCorporate = false;
		} else {
			isOwnerCorporate = true;
		}
		tr663Item = itempfService.readSmartTableByTrim(Companies.LIFE, "Tr663", Companies.LIFE, Tr663.class);
		t3584Item = itempfService.readSmartTableByTrim(Companies.FSU, "T3584", lifepf.getRelation(), T3584.class);
		t5687ItemsMap = descpfDAO.getItems("IT", Companies.LIFE, "T5687", "E");
	}

	private L2CommongwDTO setReaderDTO() {
		L2CommongwDTO reader = new L2CommongwDTO();
		reader.setDatatype(SPACE);
		if(dataTypeFlag && tjl21Item!=null) {
			String trncd = ptrnpf.getBatctrcde();
			if(trncd.equals("T642")||trncd.equals("T510")||trncd.equals("TR7K")
			 ||trncd.equals("T607")||trncd.equals("B673")||trncd.equals("T514")
			 ||trncd.equals("T669")||trncd.equals("TA85")||trncd.equals("T513")) {
				reader.setDatatype(tjl21Item.getCmgwdattyp());
			} else {
				reader.setDatatype("1");
			}
		}
		reader.setInsucate("AA");
		reader.setBokcls("A");
		reader.setSecunum(chdrpf.getChdrnum());
		reader.setBrnchnum(SPACE);
		reader.setMop(t1673Item.getCmgwmop());
		reader.setColcate(tjl27Item.getCmgwcolcat());
		reader.setInstal(chdrpf.getBillfreq());
		reader.setKyocde(ZERO);
		reader.setKyoacs(SPACE);
		List<Pcddpf> temppcddpfList = pcddpfDAO.getPcddpfRecord(Companies.LIFE, chdrpf.getChdrnum());
		String pcde = ZERO;
		String pacs = ZERO;
		if (temppcddpfList != null && temppcddpfList.size() > 1) {
			pcde = "1";
			for(Pcddpf pcdd:temppcddpfList) {
				if(agntnum.trim().equals(pcdd.getAgntNum().trim())) {
					pacs = pcdd.getSplitC().toString();
					break;
				}
			}
			pacs=pacs.substring(0,2);
			pacs=pacs+"00";
		} else {
			pacs="0000";
		}
		reader.setPcde(pcde);
		reader.setPacs(pacs);
		reader.setGrpcde(SPACE);
		reader.setAffilcde(SPACE);
		reader.setEmpcde(SPACE);
		reader.setCgrpc("L");
		BigDecimal totprem;
		if ("00".equals(chdrpf.getBillfreq()))
			totprem = chdrpf.getInsttot06();
		else
			totprem = chdrpf.getSinstamt06().multiply(new BigDecimal(chdrpf.getBillfreq()));
		reader.setTotprem(appendData(getRounded(totprem),10));
		reader.setBckp(SPACE);
		reader.setDpcde(SPACE);
		reader.setBrnchnum2(SPACE);
		reader.setIncpn(chdrpf.getOccdate().toString().substring(2, 8));
		reader.setTncdte(ptrnpf.getPtrneff().toString().substring(2, 8));
		reader.setTrnendte(SPACE);
		Hpadpf hpadpf = hpadpfDAO.readHpadpfByCompanyAndContractNumber(Companies.LIFE, chdrpf.getChdrnum());
		reader.setDteofapp(Integer.toString(hpadpf.getHpropdte()).substring(2, 8));
		String ctrowner = chdrpf.getCownnum();
		String agntclnt = agntcypfDAO.getAgntClnt(Companies.LIFE, agntnum);
		if(ctrowner.equals(agntclnt))
			reader.setSlfcrt("1");
		else
			reader.setSlfcrt("0");
		reader.setLpcc(SPACE);
		reader.setDcde(SPACE);
		reader.setOprtcde(SPACE);
		reader.setPostcde(SPACE);
		String ctrAddr;
		StringBuilder addr = new StringBuilder();
		if(clntpf.getZkanaddr01() != null){
			addr.append(clntpf.getZkanaddr01().trim());
		}
		if(clntpf.getZkanaddr02() != null){
			addr.append(SPACE);
			addr.append(clntpf.getZkanaddr02().trim());
		}
		if(clntpf.getZkanaddr03() != null){
			addr.append(SPACE);
			addr.append(clntpf.getZkanaddr03().trim());
		}
		if(clntpf.getZkanaddr04() != null){
			addr.append(SPACE);
			addr.append(clntpf.getZkanaddr04().trim());
		}
		if(clntpf.getZkanaddr05() != null){
			addr.append(SPACE);
			addr.append(clntpf.getZkanaddr05().trim());
		}
		if(addr.length()>70) {
			ctrAddr = addr.substring(0, 70);
		} else {
			ctrAddr = addr.toString();
		}
		reader.setCntadd(ctrAddr);
		String phone1 = null;
		String phone2 = null;
		//ILJ-748 Starts
		if("P".equals(clntpf.getClttype())) {
			Clexpf clexpf = clexpfDAO.getClexpfRecord("CN", Companies.FSU, clntpf.getClntnum());
			//ILJ-719 Starts
			if (clexpf !=null && null !=clexpf.getRmblphone()) {
				String rmblphone = clexpf.getRmblphone().trim();
				if(rmblphone.length()>0) {
					if (rmblphone.length() > 12) {
						phone1 = rmblphone.substring(0, 12);
						if (rmblphone.length() >= 15)
							phone2 = rmblphone.substring(12, 15);
						else
							phone2 = rmblphone.substring(12);
					} else {
						phone1 = rmblphone;
					}
				}
			}
			if (phone1==null && clntpf.getCltphone02() != null) {
				String cltphone02 = clntpf.getCltphone02().trim();
				if (cltphone02.length()>0) {
					if (cltphone02.length() > 12) {
						phone1 = cltphone02.substring(0, 12);
						if (cltphone02.length() >= 15)
							phone2 = cltphone02.substring(12, 15);
						else
							phone2 = cltphone02.substring(12);
					} else {
						phone1 = cltphone02;
					}
				}
			}
			if (phone1==null && clntpf.getCltphone01() != null) {
				String cltphone01 = clntpf.getCltphone01().trim();
				if (cltphone01.length()>0) {
					if (cltphone01.length() > 12) {
						phone1 = cltphone01.substring(0, 12);
						if (cltphone01.length() >= 15)
							phone2 = cltphone01.substring(12, 15);
						else
							phone2 = cltphone01.substring(12);
					} else {
						phone1 = cltphone01;
					}
				}
			}
			//ILJ-719 End
		} else {
			if (phone1==null && clntpf.getCltphone01() != null) {
				String cltphone01 = clntpf.getCltphone01().trim();
				if (cltphone01.length()>0) {
					if (cltphone01.length() > 12) {
						phone1 = cltphone01.substring(0, 12);
						if (cltphone01.length() >= 15)
							phone2 = cltphone01.substring(12, 15);
						else
							phone2 = cltphone01.substring(12);
					} else {
						phone1 = cltphone01;
					}
				}
			}
			if (phone1==null && clntpf.getCltphone02() != null) {
				String cltphone02 = clntpf.getCltphone02().trim();
				if (cltphone02.length()>0) {
					if (cltphone02.length() > 12) {
						phone1 = cltphone02.substring(0, 12);
						if (cltphone02.length() >= 15)
							phone2 = cltphone02.substring(12, 15);
						else
							phone2 = cltphone02.substring(12);
					} else {
						phone1 = cltphone02;
					}
				}
			}
		}
		//ILJ-748 End
		reader.setPnum(phone1 != null ? phone1 : SPACE);
		String subscrName;
		StringBuilder sbsnm = new StringBuilder();
		if(clntpf.getZkanasnm()!=null) {
			sbsnm.append(clntpf.getZkanasnm().trim());
		}
		if(clntpf.getZkanagnm()!=null) {
			sbsnm.append(SPACE);
			sbsnm.append(clntpf.getZkanagnm().trim());
		}
		if (sbsnm != null && sbsnm.length() > 70) {
			subscrName = sbsnm.substring(0, 70);
		} else {
			subscrName = sbsnm.toString();
		}
		reader.setSubnme(subscrName);
		Payrpf payrpf = payrpfDAO.getPayrRecord(Companies.LIFE, chdrpf.getChdrnum());
		if ("1".equals(t1673Item.getCmgwmop())) {
			reader.setSpfip(ZERO);
		} else {
			reader.setSpfip(appendData(getRounded(payrpf.getSinstamt06()),10));
		}
		reader.setSpltyp(t1673Item.getCmgwspltyp());
		reader.setPymntdte(tjl27Item.getCmgwcpdte());
		reader.setFpdfgh(SPACE);
		reader.setInsyr(SPACE);
		reader.setFrstme(SPACE);
		Acblpf acblpf = acblpfDAO.getAcblpf(chdrpf.getChdrnum(), "VP", "LN");
		String pprem;
		if(acblpf !=null)
			pprem= acblpf.getSacscurbal().toString();
		else
			pprem = ZERO;

		reader.setPprem(appendData(pprem,10));	//ILJ-695
		reader.setPpendatme(SPACE);
		reader.setNwccls(ZERO);
		reader.setCcps(SPACE);
		reader.setIpactc(SPACE);
		reader.setAmor(SPACE);
		reader.setGrpcara(SPACE);
		reader.setTelphno(phone2 != null ? phone2 : SPACE);
		reader.setNwcmpncde(SPACE);
		if(tjl21Item!=null && tjl21Item.getCmgwreason()!=null) {
			reader.setRftrns(tjl21Item.getCmgwreason());
		} else {
			reader.setRftrns(SPACE);
		}
		reader.setMargin(SPACE);
		reader.setInacviwe(SPACE);
		reader.setProsqnc(SPACE);
		reader.setYnmrcd(SPACE);
		reader.setInscmpnycde(tr663Item.getZprofile());
		reader.setInsprocde(SPACE);
		reader.setCmpnycde(SPACE);
		String prsnmekana;
		StringBuilder prsnm = new StringBuilder();
		if(clntpf.getZkanasnm()!=null) {
			prsnm.append(clntpf.getZkanasnm().trim());
		}
		if(clntpf.getZkanagnm()!=null) {
			prsnm.append(SPACE);
			prsnm.append(clntpf.getZkanagnm().trim());
		}
		if (prsnm.length() > 20) {
			prsnmekana = prsnm.substring(0, 20);
		} else {
			prsnmekana = prsnm.toString();
		}
		reader.setPrsnmekana(prsnmekana);
		reader.setIpecflg(SPACE);
		reader.setIpsftcde01(SPACE);
		String ipnmknji;
		StringBuilder ipnm = new StringBuilder();
		if(insrdClnt.getSurname()!=null) {
			ipnm.append(insrdClnt.getSurname().trim());
		}
		if(insrdClnt.getGivname()!=null) {
			ipnm.append(SPACE);
			ipnm.append(insrdClnt.getGivname().trim());
		}
		if (ipnm.length() > 30) {
			ipnmknji = ipnm.substring(0, 30);
		} else {
			ipnmknji = ipnm.toString();
		}
		reader.setIpnmknji(ipnmknji);
		reader.setIpsftcde02(SPACE);
		reader.setIpdob(Integer.toString(insrdClnt.getCltdob()));
		reader.setIpage(appendData(lifepf.getAnbccd().toString(),3));
		reader.setIpgndr(tjl29ItemInsrd.getCmgwgndr());
		if(t3584Item!=null && t3584Item.getCmgwrlmpng()!=null)
			reader.setIprln(t3584Item.getCmgwrlmpng());
		else
			reader.setIprln(SPACE);
		if(insrdClnt.getCltpcode()!=null) {
			if(insrdClnt.getCltpcode().length()>8) {
				reader.setIpostcde(insrdClnt.getCltpcode().substring(0,8));
			} else {
				reader.setIpostcde(insrdClnt.getCltpcode());
			}
		} else {
			reader.setIpostcde(SPACE);
		}
		reader.setIaeflg(SPACE);
		reader.setIashtcde01(SPACE);
		String iadrsknji;
		StringBuilder addrs = new StringBuilder();
		if(insrdClnt.getCltaddr01() != null) {
			addrs.append(insrdClnt.getCltaddr01().trim());
		}
		if(insrdClnt.getCltaddr02() != null) {
			addrs.append(SPACE);
			addrs.append(insrdClnt.getCltaddr02().trim());
		}
		if(insrdClnt.getCltaddr03() != null) {
			addrs.append(SPACE);
			addrs.append(insrdClnt.getCltaddr03().trim());
		}
		if(insrdClnt.getCltaddr04() != null) {
			addrs.append(SPACE);
			addrs.append(insrdClnt.getCltaddr04().trim());
		}
		if(insrdClnt.getCltaddr05() != null) {
			addrs.append(SPACE);
			addrs.append(insrdClnt.getCltaddr05().trim());
		}
		if (addrs.length() > 100) {
			iadrsknji = addrs.substring(0, 100);
		} else {
			iadrsknji = addrs.toString();
		}
		reader.setIadrsknji(iadrsknji);
		reader.setIashtcde02(SPACE);
		reader.setPymntrt(tjl27Item.getCmgwcprot());
		if(chdrpf.getBillfreq().equals("00"))
			reader.setLsprem(appendData(getRounded(chdrpf.getInsttot06()),10));
		else
			reader.setLsprem(appendData(ZERO,10));
		reader.setModundrprd("00");
		reader.setIndcate(SPACE);
		reader.setIndcprd("00");
		reader.setDvndcls(SPACE);
		reader.setPdexntdvw(SPACE);
		reader.setAttchngcls(SPACE);
		String tr386item;
		if(dataTypeFlag) {
			tr386item="EBJL33";
		} else {
			tr386item="EBJL32";
		}
		if(ptrnpf.getBatctrcde().equals("T622")) {
			Tr386 tr386Item = itempfService.readSmartTableByTrim(Companies.LIFE, "TR386", tr386item, Tr386.class);
			if(tr386Item!=null) {
				String[] tr386Data = tr386Item.getProgdescs();
				if(tr386Data.length>0) {
					String msg1 = tr386Data[0];
					if(msg1.length()>10 && "T622".equals(msg1.substring(0, 4))) {
						String resnCode = resnpfDAO.getReasoncd(Companies.LIFE, chdrpf.getChdrnum(), "T622");
						if(resnCode!=null && resnCode.equals(msg1.substring(5, 9))) {
							reader.setAttchngcls(msg1.substring(10, 11));
						}
					}
				}
			}
		} else if(ptrnpf.getBatctrcde().equals("TR6Y")) {
			Lifepf rcntChngRec = lifepfDAO.getRcntChngRecord(Companies.LIFE, chdrpf.getChdrnum(), "01","00");
			if(rcntChngRec!=null && (!rcntChngRec.getCltdob().equals(lifepf.getCltdob()) || !rcntChngRec.getCltsex().equals(lifepf.getCltsex()))) {
				reader.setAttchngcls("4");
			}
		} else if(tjl21Item!=null && tjl21Item.getCmgwcngcls()!=null) {
			reader.setAttchngcls(tjl21Item.getCmgwcngcls());
		}
		reader.setAppcate(SPACE);
		reader.setIppyear("000");
		reader.setPpsdte(ZERO);
		boolean isExclusive = exclpfDAO.isExclExist(Companies.LIFE, chdrpf.getChdrnum(), effectiveDate);
		if (isExclusive)
			reader.setPasbdy("1");
		else
			reader.setPasbdy(SPACE);
		reader.setGrpcde02(SPACE);
		reader.setLocde(SPACE);
		reader.setExmcate(SPACE);
		reader.setTaxelg(SPACE);
		reader.setLvnnds(SPACE);
		reader.setBnsprem(appendData(ZERO,10));
		reader.setBcagyr(SPACE);
		reader.setOpcde(agntnum);
		reader.setMccecflg(SPACE);
		reader.setMccscde01(SPACE);
		reader.setMccscde02(SPACE);
		setSplAgrmnts(reader);
		List<Covrpf> covrList = covrpfListMap.get(chdrpf.getChdrnum());
		Integer riderCount = covrList.size()-1;
		reader.setNorids(appendData(riderCount.toString(),3));
		for(Covrpf covr:covrList) {
			boolean t5687SecFlg = isT5687SecVal01(covr.getCrtable());
			if(covr.getRider().equals("00")) {
				T5687 t5687Item = itempfService.readSmartTableByTrim(Companies.LIFE, "T5687", covr.getCrtable(),T5687.class);
				reader.setToi(t5687Item.getStatSect());
				reader.setMcctypcde(covr.getCrtable());
				reader.setMccnmc(getRiderDesc(covr.getCrtable()));
				reader.setMccppa(appendData(getRounded(covr.getSumins()),10));
				reader.setMccitc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setMccip(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setMccppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setMccpp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setMatrty(covr.getRcesdte().toString().substring(2, 8));
				if (chdrpf.getOccdate() < covr.getCrrcd())
					reader.setAddscnt("1");
				else
					reader.setAddscnt(SPACE);
				String sumins="";
				if(null !=covr.getSumins() && covr.getSumins().signum()>0) {
					sumins= covr.getSumins().divide(new BigDecimal(10000)).toBigInteger().toString();
				}
				reader.setDthbnft(appendData(sumins,6));	//ILJ-695
			} else if(covr.getRider().equals("01")) {
				reader.setSa01typcde(covr.getCrtable());
				reader.setSa01nme(getRiderDesc(covr.getCrtable()));
				reader.setSa01sins(appendData(covr.getSumins().toBigInteger().toString(),10));
				reader.setSa01itc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa01insprd(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa01ppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setSa01pp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
			} else if(covr.getRider().equals("02")) {
				reader.setSa02typcde(covr.getCrtable());
				reader.setSa02nme(getRiderDesc(covr.getCrtable()));
				reader.setSa02sins(appendData(covr.getSumins().toBigInteger().toString(),10));
				reader.setSa02itc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa02insprd(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa02ppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setSa02pp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
			} else if(covr.getRider().equals("03")) {
				reader.setSa03typcde(covr.getCrtable());
				reader.setSa03nme(getRiderDesc(covr.getCrtable()));
				reader.setSa03sins(appendData(covr.getSumins().toBigInteger().toString(),10));
				reader.setSa03itc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa03insprd(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa03ppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setSa03pp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
			} else if(covr.getRider().equals("04")) {
				reader.setSa04typcde(covr.getCrtable());
				reader.setSa04nme(getRiderDesc(covr.getCrtable()));
				reader.setSa04sins(appendData(covr.getSumins().toBigInteger().toString(),10));
				reader.setSa04itc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa04insprd(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa04ppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setSa04pp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
			} else if(covr.getRider().equals("05")) {
				reader.setSa05typcde(covr.getCrtable());
				reader.setSa05nme(getRiderDesc(covr.getCrtable()));
				reader.setSa05sins(appendData(covr.getSumins().toBigInteger().toString(),10));
				reader.setSa05itc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa05insprd(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa05ppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setSa05pp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
			} else if(covr.getRider().equals("06")) {
				reader.setSa06typcde(covr.getCrtable());
				reader.setSa06nme(getRiderDesc(covr.getCrtable()));
				reader.setSa06sins(appendData(covr.getSumins().toBigInteger().toString(),10));
				reader.setSa06itc(getInsTermClsfctn(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa06insprd(getInsPrd(covr.getRcesage(), covr.getRcestrm(), t5687SecFlg));
				reader.setSa06ppc(getInsTermClsfctn(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
				reader.setSa06pp(getInsPrd(covr.getPcesage(), covr.getPcestrm(), t5687SecFlg));
			}
		}
		
		reader.setUstatcate(SPACE);
		reader.setCmec(SPACE);
		reader.setCifccc(SPACE);
		reader.setAsmcde(SPACE);
		reader.setLircde(SPACE);
		reader.setAcctyp(SPACE);
		String accNo=null;
		if("D".equals(payrpf.getBillchnl().trim())){
			accNo = mandpfDAO.getBankacckey(chdrpf.getMandref(), chdrpf.getCownnum());
		}
		if(accNo!=null) {
			if(accNo.length()>7) {
				reader.setAccno(accNo.substring(0, 7));
			} else {
				reader.setAccno(accNo);
			}
		} else {
			reader.setAccno(SPACE);
		}
		reader.setLcode(SPACE);
		reader.setRacate(SPACE);
		if(dataTypeFlag)
			reader.setDtypcde("06");
		else
			reader.setDtypcde("96");
		if(clntpf.getClttype().equals("P"))
			reader.setCaic("2");
		else if(clntpf.getClttype().equals("C"))
			reader.setCaic("1");
		reader.setCnticnrct(SPACE);
		reader.setTncp(appendData(ZERO, 10));
		reader.setWrkplc(SPACE);
		if(clntpf.getCltphone01()!=null && clntpf.getCltphone01().length()>12)
			reader.setBphno(clntpf.getCltphone01().substring(0,12));
		else
			reader.setBphno(clntpf.getCltphone01());
		reader.setCmgntcde(SPACE);
		reader.setLncde(appendData(ZERO,4));
		reader.setOsnocc(SPACE);
		reader.setOsnmn(SPACE);
		reader.setOsnbn(SPACE);
		reader.setGrpcrtlno(SPACE);
		reader.setScno(SPACE);
		reader.setGrpinsnme(SPACE);
		reader.setCrsnme(SPACE);
		reader.setType(SPACE);
		reader.setNounits("00");
		reader.setAgregno(SPACE);
		reader.setAgcde01(SPACE);
		reader.setAgcde02(SPACE);
		reader.setCaflgscde01(SPACE);
		reader.setCanknjiecflg(SPACE);
		String caflgadrs;
		StringBuilder caflg = new StringBuilder();
		if(clntpf.getCltaddr01()!=null) {
			caflg.append(clntpf.getCltaddr01().trim());
		}
		if(clntpf.getCltaddr02()!=null) {
			caflg.append(SPACE);
			caflg.append(clntpf.getCltaddr02().trim());
		}
		if(clntpf.getCltaddr03()!=null) {
			caflg.append(SPACE);
			caflg.append(clntpf.getCltaddr03().trim());
		}
		if(clntpf.getCltaddr04()!=null) {
			caflg.append(SPACE);
			caflg.append(clntpf.getCltaddr04().trim());
		}
		if(clntpf.getCltaddr05()!=null) {
			caflg.append(SPACE);
			caflg.append(clntpf.getCltaddr05().trim());
		}
		if(caflg.length()>100) {
			caflgadrs=caflg.substring(0,100);
		} else {
			caflgadrs=caflg.toString();
		}
		reader.setCaflgadrs(caflgadrs);
		reader.setCnkjicflgscd(SPACE);
		reader.setCaflgscde02(SPACE);
		reader.setCnflgscde(SPACE);
		StringBuilder spnm = new StringBuilder();
		if(clntpf.getSurname()!=null) {
			spnm.append(clntpf.getSurname().trim());
		}
		if(clntpf.getGivname()!=null) {
			spnm.append(SPACE);
			spnm.append(clntpf.getGivname().trim());
		}
		reader.setSpnmknji(spnm.toString());
		reader.setSpnmscde(SPACE);
		reader.setDtechgprem(SPACE);
		reader.setModinstprem(appendData(ZERO, 10));
		reader.setPremlncrt(SPACE);
		reader.setDobph(Integer.toString(clntpf.getCltdob()));
		if(isOwnerCorporate) {	//ILJ-708
			reader.setCgender("9");
		} else {
			reader.setCgender(tjl29ItemOwnr.getCmgwgndr());
		}
		reader.setBphno02(SPACE);
		reader.setBckup01(SPACE);
		reader.setCmpnyter(SPACE);
		reader.setPstrtflg(SPACE);
		reader.setAntypymnt(appendData(ZERO, 10));
		reader.setAnpenpymnt("00");
		reader.setPensnfund(appendData(ZERO, 10));
		reader.setPensncrtno(SPACE);
		reader.setPayecate(SPACE);
		String appno = ttrcpfDAO.getTtmprcno(Companies.LIFE, chdrpf.getChdrnum());
		if(appno!=null)
			reader.setAppno(appno);
		else
			reader.setAppno(SPACE);
		reader.setBckup02(SPACE);
		reader.setCorpterty(SPACE);
		reader.setUsrprf(userName);
		reader.setAgncnum(agncynum);
		reader.setExprtflg("");
		return reader;
	}
	
	private void setSplAgrmnts(L2CommongwDTO reader) {
		reader.setSa01typcde(SPACE);
		reader.setSa01ecflg(SPACE);
		reader.setSa01scde01(SPACE);
		reader.setSa01scde02(SPACE);
		reader.setSa01nme(SPACE);
		reader.setSa01sins(appendData(ZERO,11));
		reader.setSa01itc(SPACE);
		reader.setSa01insprd(SPACE);
		reader.setSa01ppc(SPACE);
		reader.setSa01pp(SPACE);
		
		reader.setSa02typcde(SPACE);
		reader.setSa02ecflg(SPACE);
		reader.setSa02scde01(SPACE);
		reader.setSa02scde02(SPACE);
		reader.setSa02nme(SPACE);
		reader.setSa02sins(appendData(ZERO,11));
		reader.setSa02itc(SPACE);
		reader.setSa02insprd(SPACE);
		reader.setSa02ppc(SPACE);
		reader.setSa02pp(SPACE);
		
		reader.setSa03typcde(SPACE);
		reader.setSa03ecflg(SPACE);
		reader.setSa03scde01(SPACE);
		reader.setSa03scde02(SPACE);
		reader.setSa03nme(SPACE);
		reader.setSa03sins(appendData(ZERO,11));
		reader.setSa03itc(SPACE);
		reader.setSa03insprd(SPACE);
		reader.setSa03ppc(SPACE);
		reader.setSa03pp(SPACE);
		
		reader.setSa04typcde(SPACE);
		reader.setSa04ecflg(SPACE);
		reader.setSa04scde01(SPACE);
		reader.setSa04scde02(SPACE);
		reader.setSa04nme(SPACE);
		reader.setSa04sins(appendData(ZERO,11));
		reader.setSa04itc(SPACE);
		reader.setSa04insprd(SPACE);
		reader.setSa04ppc(SPACE);
		reader.setSa04pp(SPACE);
		
		reader.setSa05typcde(SPACE);
		reader.setSaecflg5(SPACE);
		reader.setSa05scde01(SPACE);
		reader.setSa05scde02(SPACE);
		reader.setSa05nme(SPACE);
		reader.setSa05sins(appendData(ZERO,11));
		reader.setSa05itc(SPACE);
		reader.setSa05insprd(SPACE);
		reader.setSa05ppc(SPACE);
		reader.setSa05pp(SPACE);
		
		reader.setSa06typcde(SPACE);
		reader.setSa06ecflg(SPACE);
		reader.setSa06scde01(SPACE);
		reader.setSa06scde02(SPACE);
		reader.setSa06nme(SPACE);
		reader.setSa06sins(appendData(ZERO,11));
		reader.setSa06itc(SPACE);
		reader.setSa06insprd(SPACE);
		reader.setSa06ppc(SPACE);
		reader.setSa06pp(SPACE);
	}
	
	private String getInsTermClsfctn(Integer rcesage, Integer rcesterm, boolean t5687Secflag) {
		String insTermClsfctn = SPACE;
		if(t5687Secflag) {
			insTermClsfctn = "3";
		} else if (rcesage!= null  && rcesage>0) {
			insTermClsfctn = "2";
		} else if (rcesterm!=null && rcesterm>0) {
			insTermClsfctn = "1";
		}
		return insTermClsfctn;
	}
	
	private String getInsPrd(Integer rcesage, Integer rcesterm, boolean t5687Secflag) {
		String insPrd = SPACE;
		if(t5687Secflag) {
			insPrd = "00";
		} else if (rcesage!= null  && rcesage>0) {
			if(rcesage>99)
				insPrd="99";
			else 
				insPrd=rcesage.toString();
		} else if (rcesterm!=null && rcesterm>0) {
			if(rcesterm>99)
				insPrd="99";
			else 
				insPrd=rcesterm.toString();
		}
		return insPrd;
	}
	
	private String getRiderDesc(String t5687Item) {
		Descpf t5687ItemDesc = t5687ItemsMap.get(t5687Item);
		String longDesc = t5687ItemDesc.getLongdesc();
		if(longDesc.length()>16) {
			longDesc = longDesc.substring(0, 16);
		}
		return longDesc;
	}
	
	private boolean isT5687SecVal01(String rider) {
		boolean flag = false;
		T5687 t5687 = itempfService.readSmartTableByTrim(Companies.LIFE, "T5687", rider,T5687.class);
		if(t5687!=null && t5687.getStatSect()!=null && t5687.getStatSect().equals("01"))
			flag = true;
		return flag;
	}
	
	private String getRounded(BigDecimal val) {
		return String.valueOf(val.setScale(0, BigDecimal.ROUND_HALF_EVEN));
	}
	
	private String appendData(String data, int size)	{
		StringBuilder appendedData = new StringBuilder();
		if(data.length()==size) {
			appendedData.append(data);
		} else if(data.length()>size) {
			appendedData.append(data.substring(0, size));
		} else {
			StringBuilder appender = new StringBuilder();
			for(int i=1;i<=size-data.length();i++) {
				appender.append("0");
			}
			appender.append(data);
			appendedData.append(appender);
		}
		return appendedData.toString();
	}

	@Override
	public L2CommongwDTO read() throws Exception {
		
		if (recordCount < list.size()) {
			return list.get(recordCount++);
		} else {
			recordCount = 0;
			return null;
		}
	}

}
