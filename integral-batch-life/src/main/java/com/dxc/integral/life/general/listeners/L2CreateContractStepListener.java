package com.dxc.integral.life.general.listeners;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.dxc.integral.iaf.listener.IntegralStepListener;


public class L2CreateContractStepListener extends IntegralStepListener {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CreateContractStepListener.class);
	
	@Autowired
	private Environment env;
	
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("L2CreateContractStepListener::afterStep::Entry");
		
		deleteFile();
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.debug("L2CreateContractStepListener batch step completed.");
		}
		else{
			LOGGER.error("Some exception occurred in batch step L2CreateContractStepListener : {0}",exceptionList.get(0));
			stepExecution.setExitStatus(ExitStatus.FAILED);
		}

		return stepExecution.getExitStatus();
	}
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("L2CreateContractStepListener::beforeStep::Entry");
		super.beforeStep(stepExecution);
		
		LOGGER.debug("L2CreateContractStepListener::beforeStep::Exit");
	}
	
	private void deleteFile(){
		String inputFilePath = env.getProperty("report.csv.input") + env.getProperty("folder.createProposal").trim();
		String archiveFolder = env.getProperty("folder.archive");
		File dir = FileSystems.getDefault().getPath(inputFilePath).toFile();
		
	     File[] fileList = dir.listFiles(); 
		 if(fileList != null) {
				for(File file : fileList) {
					if(!file.isDirectory() && file.renameTo(FileSystems.getDefault().getPath(inputFilePath + archiveFolder + file.getName()).toFile())) 
				       {  
						Path path = file.toPath();
						
						try {
							Files.delete(path);
							
						} catch (IOException e) {
							 LOGGER.debug("Error moving file to {}",archiveFolder.replaceAll("[\r\n]",""));
						} 
				     } 
				}
			}
	}
}
