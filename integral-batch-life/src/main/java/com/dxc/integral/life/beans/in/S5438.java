package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5438 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String rasnum;
	private String rngmnt;
	private String raAmount;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getRasnum() {
		return rasnum;
	}
	public void setRasnum(String rasnum) {
		this.rasnum = rasnum;
	}
	public String getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(String rngmnt) {
		this.rngmnt = rngmnt;
	}
	public String getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(String raAmount) {
		this.raAmount = raAmount;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5438 []";
	}
	
}
