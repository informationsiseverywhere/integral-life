package com.dxc.integral.life.general.listeners;

import java.math.BigDecimal;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.L2newintcpControlTotalDTO;

/**
 * Listener for L2NEWINTCP batch step.
 * 
 * @author dpuhawan
 *
 */
public class L2newintcpStepListener extends IntegralStepListener {//ILIFE-5763
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2newintcpStepListener.class);

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private L2newintcpControlTotalDTO l2newintcpControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;
	
	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['chdrcoy']}")
	private String company;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Value("#{jobParameters['transactionDate']}")
	private String transactionDate;
	
	//ILIFE-5763 Starts
	@Autowired
	private ElogpfDAO elogpfDAO;

	private static String programName = "Br535";
	//ILIFE-5763 Ends
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("L2newintcpStepListener::beforeStep::Entry");
		super.beforeStep(stepExecution);
		Integer tranDate = 0;
		try {
			tranDate = DatimeUtil.covertToyyyyMMddDate(transactionDate);
		} catch (ParseException e) {
			LOGGER.error("L2newintcpStepListener::beforeStep::Error:");
			LOGGER.error(e.toString());
		}
		stepExecution.getExecutionContext().putInt("trandate", tranDate);
		LOGGER.debug("L2newintcpStepListener::beforeStep::Exit");
	}


	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		//ILIFE-5763 Starts
		LOGGER.debug("L2newintcpStepListener::afterStep::Entry");
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {//ILIFE-5763 Ends
			LOGGER.info("Control total update for L2NEWINTCP batch starts");
			Sbcontotpf sbcontotpf = new Sbcontotpf();
			sbcontotpf.setStepname(stepExecution.getStepName());
			sbcontotpf.setUsrprf(userName);
			sbcontotpf.setBranch(branch);
			sbcontotpf.setCompany(company);
			Integer effDate = 0;
			try {
				effDate = DatimeUtil.covertToyyyyMMddDate(transactionDate);
			} catch (ParseException e) {
				LOGGER.error("L2newintcpStepListener::afterStep::effDate:");
				LOGGER.error(e.toString());
			}
			sbcontotpf.setEffdate(effDate);
			sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
			sbcontotpf.setJobnm(batchName);
			sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
			sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
			sbcontotpf.setContot2(l2newintcpControlTotalDTO.getControlTotal02());
			sbcontotpf.setContot3(l2newintcpControlTotalDTO.getControlTotal03());
			sbcontotpf.setContot4(new BigDecimal(stepExecution.getWriteCount()));
			sbcontotpf.setContot5(l2newintcpControlTotalDTO.getControlTotal05());
			sbcontotpf.setContot6(l2newintcpControlTotalDTO.getControlTotal06());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);
			LOGGER.info("Control total update for L2NEWINTCP batch ends");
			LOGGER.info("L2NEWINTCP batch step completed.");
		} else {//ILIFE-5763 Starts
			LOGGER.error("Some exception occurred,logging it in Elogpf.{}", 
					exceptionList.get(0).getStackTrace().toString());//IJTI-1498
			if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", programName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("L2newintcpStepListener::afterStep::Exit");
		//ILIFE-5763 Ends
		return stepExecution.getExitStatus();
	}

}
