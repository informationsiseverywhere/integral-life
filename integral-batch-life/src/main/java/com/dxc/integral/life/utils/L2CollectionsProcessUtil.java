package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.BatchInformBean;
import com.dxc.integral.life.beans.L2CollectionControlTotalDTO;
import com.dxc.integral.life.beans.L2CollectionReaderDTO;
import com.dxc.integral.life.beans.L2CollectionsProcessDTO;

public interface L2CollectionsProcessUtil {

	public L2CollectionsProcessDTO init(L2CollectionReaderDTO item, BatchInformBean informBean);
	public void edit(L2CollectionReaderDTO item, BatchInformBean informBean, L2CollectionsProcessDTO processDto, L2CollectionControlTotalDTO l2collectioncontroltotaldto);
	//public void update(L2CollectionReaderDTO item, BatchInformBean informBean, L2CollectionsProcessDTO processDto, L2CollectionControlTotalDTO l2collectioncontroltotaldto);
}
