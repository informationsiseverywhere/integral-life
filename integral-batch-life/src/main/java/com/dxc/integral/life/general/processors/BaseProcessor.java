package com.dxc.integral.life.general.processors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;

public class BaseProcessor {
	@Autowired
	public Sftlock sftlock;
	@Autowired
	public UsrdpfDAO usrdpfDAO;
	@Autowired
	public Batcup batcup;

	public boolean softlock(String chdrnum, String tranCode,String bactchName) {
		/* Soft lock the contract, if it is to be processed. */
		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(chdrnum);
		slckpf.setEnttyp("CH");
		slckpf.setCompany("2");
		slckpf.setProctrancd(tranCode);
		slckpf.setUsrprf("999999");
		slckpf.setJobnm(bactchName);
		return sftlock.lockEntity(slckpf);
	}
	public void fatalError(String status) {
		throw new StopRunException(status);
	}

	public static String getCobolDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		return sdf.format(new java.util.Date());
	}

	public Usrdpf getUserDetails(String userName) {
		Usrdpf usrpf = usrdpfDAO.getUserInfo(userName);
		return usrpf;
	}
	public Batcpf getBatcpfInfo(String transactionDate, String businessDate, String batchCompany,
							String batchTransactionCode, String batchBranch, Usrdpf usrdpf, String batchName) throws ParseException {
		if (transactionDate.indexOf('-') == -1) {
			transactionDate = DatimeUtil.covertToDDmmyyyyDate(transactionDate);
		}
		Batcpf batchDetail = batcup.getBatchDetails(transactionDate, businessDate, batchCompany, batchTransactionCode, batchBranch,
				usrdpf, batchName);
		return batchDetail;
	}
	public int getVrcmTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		return Integer.parseInt(formatter.format(LocalTime.now()));
	}
	public int getActMonth(String transDate) throws ParseException{
		if (transDate.indexOf('-') == -1) {
			transDate = DatimeUtil.covertToDDmmyyyyDate(transDate);
		}
		String[] transactionDateParts = transDate.split("-");
		return Integer.parseInt(transactionDateParts[1]);
	}
	public int getActYear(String transDate) throws ParseException{
		if (transDate.indexOf('-') == -1) {
			transDate = DatimeUtil.covertToDDmmyyyyDate(transDate);
		}
		String[] transactionDateParts = transDate.split("-");
		return Integer.parseInt(transactionDateParts[2]);
	}
	public int getEffdate(String transDate) throws ParseException{
		if (transDate.indexOf('-') > -1) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date date = sdf.parse(transDate);
			sdf = new SimpleDateFormat("yyyyMMdd");
			return Integer.valueOf(sdf.format(date));
		}
		return Integer.valueOf(transDate);
	}
	public void releaseSoftlock(String chdrcoy, String chdrnum, String trancode, String username, String batchname,String batchTrcde) {
		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(chdrnum);
		slckpf.setEnttyp("CH");
		slckpf.setCompany(chdrcoy);
		slckpf.setProctrancd(trancode);
		slckpf.setUsrprf(username);
		slckpf.setJobnm(batchname);
		slckpf.setUser_t(0);
		slckpf.setProctrancd(batchTrcde);
		try {
			sftlock.unlockEntity(slckpf);
		} catch (Exception e) {
			throw new SubroutineIOException("Br613: IOException in Slckpf: " + e.getMessage());
		}
	}
}

