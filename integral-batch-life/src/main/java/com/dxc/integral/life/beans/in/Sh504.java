package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sh504 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String sumin;
	private String riskCessAge;
	private String riskCessTerm;
	private String riskCessDateDisp;
	private String premCessAge;
	private String premCessTerm;
	private String premCessDateDisp;
	private String mortcls;
	private String liencd;
	private String bappmeth;
	private String optextind;
	private String instPrem;
	private String ratypind;
	private String pbind;
	private String select;
	private String numapp;
	private String zdivopt;
	private String payeesel;
	private String paymth;
	private String paycurr;
	private String bankaccreq;
	private String taxind;
	private String prmbasis;
	private String dialdownoption;
	private String exclind;
	private String aepaydet;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getSumin() {
		return sumin;
	}
	public void setSumin(String sumin) {
		this.sumin = sumin;
	}
	public String getRiskCessAge() {
		return riskCessAge;
	}
	public void setRiskCessAge(String riskCessAge) {
		this.riskCessAge = riskCessAge;
	}
	public String getRiskCessTerm() {
		return riskCessTerm;
	}
	public void setRiskCessTerm(String riskCessTerm) {
		this.riskCessTerm = riskCessTerm;
	}
	public String getRiskCessDateDisp() {
		return riskCessDateDisp;
	}
	public void setRiskCessDateDisp(String riskCessDateDisp) {
		this.riskCessDateDisp = riskCessDateDisp;
	}
	public String getPremCessAge() {
		return premCessAge;
	}
	public void setPremCessAge(String premCessAge) {
		this.premCessAge = premCessAge;
	}
	public String getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(String premCessTerm) {
		this.premCessTerm = premCessTerm;
	}
	public String getPremCessDateDisp() {
		return premCessDateDisp;
	}
	public void setPremCessDateDisp(String premCessDateDisp) {
		this.premCessDateDisp = premCessDateDisp;
	}
	public String getMortcls() {
		return mortcls;
	}
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	public String getLiencd() {
		return liencd;
	}
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	public String getBappmeth() {
		return bappmeth;
	}
	public void setBappmeth(String bappmeth) {
		this.bappmeth = bappmeth;
	}
	public String getOptextind() {
		return optextind;
	}
	public void setOptextind(String optextind) {
		this.optextind = optextind;
	}
	public String getInstPrem() {
		return instPrem;
	}
	public void setInstPrem(String instPrem) {
		this.instPrem = instPrem;
	}
	public String getRatypind() {
		return ratypind;
	}
	public void setRatypind(String ratypind) {
		this.ratypind = ratypind;
	}
	public String getPbind() {
		return pbind;
	}
	public void setPbind(String pbind) {
		this.pbind = pbind;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getNumapp() {
		return numapp;
	}
	public void setNumapp(String numapp) {
		this.numapp = numapp;
	}
	public String getZdivopt() {
		return zdivopt;
	}
	public void setZdivopt(String zdivopt) {
		this.zdivopt = zdivopt;
	}
	public String getPayeesel() {
		return payeesel;
	}
	public void setPayeesel(String payeesel) {
		this.payeesel = payeesel;
	}
	public String getPaymth() {
		return paymth;
	}
	public void setPaymth(String paymth) {
		this.paymth = paymth;
	}
	public String getPaycurr() {
		return paycurr;
	}
	public void setPaycurr(String paycurr) {
		this.paycurr = paycurr;
	}
	public String getBankaccreq() {
		return bankaccreq;
	}
	public void setBankaccreq(String bankaccreq) {
		this.bankaccreq = bankaccreq;
	}
	public String getTaxind() {
		return taxind;
	}
	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}
	public String getPrmbasis() {
		return prmbasis;
	}
	public void setPrmbasis(String prmbasis) {
		this.prmbasis = prmbasis;
	}
	public String getDialdownoption() {
		return dialdownoption;
	}
	public void setDialdownoption(String dialdownoption) {
		this.dialdownoption = dialdownoption;
	}
	public String getExclind() {
		return exclind;
	}
	public void setExclind(String exclind) {
		this.exclind = exclind;
	}
	public String getAepaydet() {
		return aepaydet;
	}
	public void setAepaydet(String aepaydet) {
		this.aepaydet = aepaydet;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "Sh504 []";
	}
	
}
