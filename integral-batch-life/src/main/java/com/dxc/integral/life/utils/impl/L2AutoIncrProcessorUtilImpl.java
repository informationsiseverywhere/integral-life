package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.FeaConfg;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.T1693;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.AgecalcDTO;
import com.dxc.integral.life.beans.IncrsumDTO;
import com.dxc.integral.life.beans.L2AutoIncrProcessorDTO;
import com.dxc.integral.life.beans.L2AutoIncrReaderDTO;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.T5687ArrayInner;
import com.dxc.integral.life.beans.T6658ArrayInner;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.dao.AnnypfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.RcvdpfDAO;
import com.dxc.integral.life.dao.model.Ainrpf;
import com.dxc.integral.life.dao.model.Annypf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Rcvdpf;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T5654;
import com.dxc.integral.life.smarttable.pojo.T5655;
import com.dxc.integral.life.smarttable.pojo.T5675;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6658;
import com.dxc.integral.life.smarttable.pojo.Tr384;
import com.dxc.integral.life.smarttable.pojo.Tr695;
import com.dxc.integral.life.utils.Agecalc;
import com.dxc.integral.life.utils.Incrsum;
import com.dxc.integral.life.utils.L2AutoIncrProcessorUtil;
import com.dxc.integral.life.utils.VPMScaller;
import com.dxc.integral.life.utils.Vpxacbl;
import com.dxc.integral.life.utils.Vpxchdr;
import com.dxc.integral.life.utils.Vpxlext;

/**
 * l2AutoIncr processor utility implementation.
 * 
 * @author yyang21
 *
 */
@Service("l2AutoIncrProcessorUtil")
@Lazy
public class L2AutoIncrProcessorUtilImpl implements L2AutoIncrProcessorUtil {

	private static final int MAX_SIZE = 1000;
	private static final int MAX_TR384_SIZE = 2500;
	private static final String TA85 = "TA85";
	private static final String B523 = "B523";
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private ItempfService itempfService;

	@Autowired
	private Datcon2 datcon2;

	@Autowired
	private Datcon3 datcon3;

	@Autowired
	public Sftlock sftlock;

	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;

	@Autowired
	private IncrpfDAO incrpfDAO;

	@Autowired
	private PtrnpfDAO ptrnpfDAO;

	@Autowired
	private ChdrpfDAO chdrpfDAO;

	@Autowired
	private RcvdpfDAO rcvdpfDAO;

	@Autowired
	private LifepfDAO lifepfDAO;

	@Autowired
	private AnnypfDAO annypfDAO;

	@Autowired
	private VPMScaller vpmscaller;

	@Autowired
	private Agecalc agecalc;

	@Autowired
	private Incrsum incrsum;

	@Autowired
	private Vpxlext vpxlext;

	@Autowired
	private Vpxchdr vpxchdr;

	@Autowired
	private Vpxacbl vpxacbl;

	@Autowired
	private Zrdecplc zrdecplc;

	public void loadSmartTables(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {

		String intDate = datcon1.todaysDate();
		l2AutoIncrProcessorDTO.setTransDate(Integer.parseInt(intDate));

		T5679 t5679 = itempfService.readSmartTableByTrim(Companies.LIFE, "T5679",
				l2AutoIncrProcessorDTO.getBprdAuthCode(), T5679.class);
		l2AutoIncrProcessorDTO.setT5679IO(t5679);

		List<TableItem<T5687>> t5687List = itempfService.getAllitemsFromTable(CommonConstants.IT_ITEMPFX,
				readerDTO.getChdrCoy(), "T5687", T5687.class);
		l2AutoIncrProcessorDTO.setT5687ArrayInner(loadT56871100(l2AutoIncrProcessorDTO, t5687List));
		List<TableItem<T6658>> t6658List = itempfService.getAllitemsFromTable(CommonConstants.IT_ITEMPFX,
				readerDTO.getChdrCoy(), "T6658", T6658.class);
		l2AutoIncrProcessorDTO.setT6658ArrayInner(loadT66581200(t6658List));
		List<TableItem<Tr384>> tr384List = itempfService.getAllitemsFromTable(CommonConstants.IT_ITEMPFX,
				readerDTO.getChdrCoy(), "TR384", Tr384.class);
		loadTr3841300(tr384List, l2AutoIncrProcessorDTO);
	}

	private T5687ArrayInner loadT56871100(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO,
			List<TableItem<T5687>> t5687List) {
		/* If no item is retrieved or the item retrieved is not from */
		/* the correct table, if this is the first read, this is a */
		/* fatal error. If on a subsequent read, simply move ENDP to */
		/* the status to end the loop. */
		if (t5687List == null || t5687List.isEmpty()) {
			return null;
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t5687List.size() > MAX_SIZE) {
			return null;
		}
		T5687ArrayInner t5687ArrayInner = new T5687ArrayInner();
		for (TableItem<T5687> t5687Item : t5687List) {
			/* Move the necessary data from the table to the working */
			/* storage array. */
			T5687 t5687IO = t5687Item.getItemDetail();
			t5687ArrayInner.getWsaaT5687Currfrom().add(t5687Item.getItmfrm());
			t5687ArrayInner.getWsaaT5687Crtable().add(t5687Item.getItemitem());
			if (t5687IO != null) {
				t5687ArrayInner.getWsaaT5687Annmthd().add(t5687IO.getAnniversaryMethod());
				t5687ArrayInner.getWsaaT5687Bcmthd().add(t5687IO.getBasicCommMeth());
				t5687ArrayInner.getWsaaT5687Bascpy().add(t5687IO.getBascpy());
				t5687ArrayInner.getWsaaT5687Rnwcpy().add(t5687IO.getRnwcpy());
				t5687ArrayInner.getWsaaT5687Srvcpy().add(t5687IO.getSrvcpy());
				l2AutoIncrProcessorDTO.setT5687IO(t5687IO);
			} else {
				t5687ArrayInner.getWsaaT5687Annmthd().add("");
				t5687ArrayInner.getWsaaT5687Bcmthd().add("");
				t5687ArrayInner.getWsaaT5687Bascpy().add("");
				t5687ArrayInner.getWsaaT5687Rnwcpy().add("");
				t5687ArrayInner.getWsaaT5687Srvcpy().add("");
				l2AutoIncrProcessorDTO.setT5687IO(new T5687());
			}
		}
		return t5687ArrayInner;
	}

	private T6658ArrayInner loadT66581200(List<TableItem<T6658>> t6658List) {
		/* If no item is retrieved or the item retrieved is not from */
		/* the correct table, if this is the first read, this is a */
		/* fatal error. If on a subsequent read, simply move ENDP to */
		/* the status to end the loop. */
		if (t6658List == null || t6658List.isEmpty()) {
			return null;
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t6658List.size() > MAX_SIZE) {
			return null;
		}
		T6658ArrayInner t6658ArrayInner = new T6658ArrayInner();
		for (TableItem<T6658> t6658Item : t6658List) {
			/* Move the necessary data from the table to the working */
			/* storage array. */
			T6658 t6658rec = t6658Item.getItemDetail();
			t6658ArrayInner.getWsaaT6658Annmthd().add(t6658Item.getItemitem());
			t6658ArrayInner.getWsaaT6658Currfrom().add(t6658Item.getItmfrm());
			t6658ArrayInner.getWsaaT6658Billfreq().add(t6658rec.getBillfreq());
			t6658ArrayInner.getWsaaT6658Fixdtrm().add(t6658rec.getFixdtrm());
			t6658ArrayInner.getWsaaT6658Manopt().add(t6658rec.getManopt());
			t6658ArrayInner.getWsaaT6658Maxpcnt().add(t6658rec.getMaxpcnt());
			t6658ArrayInner.getWsaaT6658Minpcnt().add(t6658rec.getMinpcnt());
			t6658ArrayInner.getWsaaT6658Optind().add(t6658rec.getOptind());
			t6658ArrayInner.getWsaaT6658Subprog().add(t6658rec.getSubprog());
			t6658ArrayInner.getWsaaT6658Premsubr().add(t6658rec.getPremsubr());
			t6658ArrayInner.getWsaaT6658SimpInd().add(t6658rec.getSimpleInd());
		}
		return t6658ArrayInner;
	}

	private void loadTr3841300(List<TableItem<Tr384>> tr384List, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		/* If no item is retrieved or the item retrieved is not from */
		/* the correct table, if this is the first read, this is a */
		/* fatal error. If on a subsequent read, simply move ENDP to */
		/* the status to end the loop. */
		if (tr384List == null || tr384List.isEmpty()) {
			return;
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		/* IF WSAA-T6634-IX > WSAA-T6634-SIZE */
		if (tr384List.size() > MAX_TR384_SIZE) {
			return;
		}
		for (TableItem<Tr384> tr384Item : tr384List) {
			/* Move the necessary data from the table to the working */
			/* storage array. */
			/* MOVE ITEM-GENAREA TO T6634-T6634-REC. */
			/* MOVE ITEM-ITEMITEM TO WSAA-T6634-KEY */
			/* (WSAA-T6634-IX). */
			/* MOVE T6634-LETTER-TYPE TO WSAA-T6634-LETTER-TYPE */
			/* (WSAA-T6634-IX). */
			/* SET WSAA-T6634-IX-MAX TO WSAA-T6634-IX. */
			/* SET WSAA-T6634-IX UP BY 1. */
			l2AutoIncrProcessorDTO.getWsaaTr384Key().add(tr384Item.getItemitem());
			l2AutoIncrProcessorDTO.getWsaaTr384LetterType().add(tr384Item.getItemDetail().getLetterType());
		}
	}

	public int readLeadDays(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		T5655 t5655IO = itempfService.readSmartTableByTrim(Companies.LIFE, "T5655",
				l2AutoIncrProcessorDTO.getBprdAuthCode() + "****", T5655.class);
		if (t5655IO == null) {
			throw new ItemNotfoundException(
					"L2AutoIncr: Item not found in Table T5655: " + l2AutoIncrProcessorDTO.getBprdAuthCode());
		}
		return datcon2.plusDays(l2AutoIncrProcessorDTO.getEffDate(), t5655IO.getLeadDays());
	}

	public boolean validContract(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		Chdrpf chdrrgpIO = readChdrrgp(readerDTO);
		l2AutoIncrProcessorDTO.setChdrlifIO(chdrrgpIO);
		boolean validStatus = checkContractStatus2800(l2AutoIncrProcessorDTO, chdrrgpIO);
		if (!validStatus) {
			l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal09(BigDecimal.ONE);
			return false;
		}
		validStatus = checkComponentStatus2600(l2AutoIncrProcessorDTO, readerDTO);
		if (!validStatus) {
			l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal10(BigDecimal.ONE);
			return false;
		}
		return true;
	}

	private Chdrpf readChdrrgp(L2AutoIncrReaderDTO readerDTO) {

		List<Chdrpf> chdrList = chdrpfDAO.readRecordOfChdrlnb(readerDTO.getChdrCoy(), readerDTO.getChdrNum());
		if (chdrList == null || chdrList.isEmpty()) {
			throw new StopRunException("L2AutoIncr: can't find the contract: " + readerDTO.getChdrNum());
		}
		return chdrList.get(0);
	}

	private boolean checkContractStatus2800(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, Chdrpf chdrrgpIO) {
		boolean validFlag = false;
		T5679 t5679rec = l2AutoIncrProcessorDTO.getT5679IO();
		for (String cnrisk : t5679rec.getCnRiskStats()) {
			if (chdrrgpIO.getStatcode().equals(cnrisk)) {
				validFlag = true;
				break;
			}
		}
		if (validFlag) {
			for (String cnPremStat : t5679rec.getCnPremStats()) {
				if (chdrrgpIO.getPstcde().equals(cnPremStat)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkComponentStatus2600(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO,
			L2AutoIncrReaderDTO readerDTO) {
		boolean validFlag = false;
		T5679 t5679rec = l2AutoIncrProcessorDTO.getT5679IO();
		for (String covrisk : t5679rec.getCovRiskStats()) {
			if (readerDTO.getStatCode().equals(covrisk)) {
				validFlag = true;
				break;
			}
		}
		if (validFlag) {
			for (String covPremStat : t5679rec.getCovPremStats()) {
				if (readerDTO.getPstatCode().equals(covPremStat)) {
					return true;
				}
			}
		}
		return false;
	}

	public void updateRecord(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		l2AutoIncrProcessorDTO.setReinstflag(FeaConfg.isFeatureExist(readerDTO.getChdrCoy(), "CSLRI003", "IT"));
		if (l2AutoIncrProcessorDTO.isReinstflag()) {
			l2AutoIncrProcessorDTO.setWsaaSub(0);
			markprorated(l2AutoIncrProcessorDTO, readerDTO);
		}
		Payrpf payrobj = null;
		if (l2AutoIncrProcessorDTO.isFoundPro()) {
			while (readerDTO.getCpiDte() < l2AutoIncrProcessorDTO.getWsaaProCpiDate()) {
				searchT56875100(l2AutoIncrProcessorDTO, readerDTO);
				if (!"00".equals(readerDTO.getRider())) {
					a100ReadTableTr695(l2AutoIncrProcessorDTO, readerDTO);
				}
				searchT66585200(l2AutoIncrProcessorDTO, readerDTO);
				payrobj = callIncPremsubr3100(l2AutoIncrProcessorDTO, readerDTO);
				IncrsumDTO incrsrec = l2AutoIncrProcessorDTO.getIncrsumDTO();
				if ((incrsrec.getCurrsum().compareTo(incrsrec.getNewsum()) == 0)
						&& (incrsrec.getCurrinst01().compareTo(incrsrec.getNewinst01()) == 0)
						|| (BigDecimal.ZERO.compareTo(incrsrec.getCurrsum()) == 0)
								&& (BigDecimal.ZERO.compareTo(incrsrec.getCurrinst01()) == 0)) {
					setupNextCall(readerDTO, l2AutoIncrProcessorDTO);
					noIncreaseOnCovr3050(readerDTO, l2AutoIncrProcessorDTO);
				} else {
					l2AutoIncrProcessorDTO.setChangeExist(true);
					l2AutoIncrProcessorDTO.setWsaaSub(l2AutoIncrProcessorDTO.getWsaaSub() + 1);

					if (!existingIncrease3400(l2AutoIncrProcessorDTO.getIncrsumDTO())) {
						writeIncr3300(readerDTO, l2AutoIncrProcessorDTO);
					}
					writeAinr3700(readerDTO, l2AutoIncrProcessorDTO);
					setupNextCall(readerDTO, l2AutoIncrProcessorDTO);
				}
			}
			if (l2AutoIncrProcessorDTO.isChangeExist()) {
				updateCovr3800(readerDTO, l2AutoIncrProcessorDTO);
				l2AutoIncrProcessorDTO.setChangeExist(false);
			}
			rewriteChdrlifPayr3950(payrobj, readerDTO, l2AutoIncrProcessorDTO);
		} else {
			searchT56875100(l2AutoIncrProcessorDTO, readerDTO);
			if (!"00".equals(readerDTO.getRider())) {
				a100ReadTableTr695(l2AutoIncrProcessorDTO, readerDTO);
			}
			searchT66585200(l2AutoIncrProcessorDTO, readerDTO);
			Payrpf p = callIncPremsubr3100(l2AutoIncrProcessorDTO, readerDTO);
			/* If there has been no increase on the component even though */
			/* one is allowed, the CPI-DATE on the COVR must still be */
			/* updated. This will prevent selection until the next time an */
			/* increase is due. */
			IncrsumDTO incrsrec = l2AutoIncrProcessorDTO.getIncrsumDTO();
			if (incrsrec.getCurrsum().compareTo(incrsrec.getNewsum()) == 0
					&& incrsrec.getCurrinst01().compareTo(incrsrec.getNewinst01()) == 0) {
				noIncreaseOnCovr3050(readerDTO, l2AutoIncrProcessorDTO);
				rewriteChdrlifPayr3950(p, readerDTO, l2AutoIncrProcessorDTO);
				return;
			}
			if (incrsrec.getCurrsum().compareTo(BigDecimal.ZERO) == 0
					&& incrsrec.getCurrinst01().compareTo(BigDecimal.ZERO) == 0) {
				noIncreaseOnCovr3050(readerDTO, l2AutoIncrProcessorDTO);
				rewriteChdrlifPayr3950(p, readerDTO, l2AutoIncrProcessorDTO);
				return;
			}
			if (!existingIncrease3400(l2AutoIncrProcessorDTO.getIncrsumDTO())) {
				writeIncr3300(readerDTO, l2AutoIncrProcessorDTO);
			}
			writeAinr3700(readerDTO, l2AutoIncrProcessorDTO);
			updateCovr3800(readerDTO, l2AutoIncrProcessorDTO);
			rewriteChdrlifPayr3950(p, readerDTO, l2AutoIncrProcessorDTO);
		}

		if (l2AutoIncrProcessorDTO.isIncrWritten()
				&& !readerDTO.getChdrNum().equals(l2AutoIncrProcessorDTO.getWsaaChdrChdrnum())
				&& !readerDTO.getChdrCoy().equals(l2AutoIncrProcessorDTO.getWsaaChdrChdrcoy())) {
			contractChange3900(l2AutoIncrProcessorDTO, readerDTO);
		}
	}

	protected void contractChange3900(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		/* CHANGE */
		/* FOR THE OLD CONTRACT... */
		/* Check to see if an INCR was written. */
		/* If an INCR does exist for the old contract */
		/* write a PTRN and check the "Optional" indicator */
		/* if the optional indicator on T6658 not = spaces */
		/* generate a letter request for later */
		if (l2AutoIncrProcessorDTO.isFoundPro()) {
			while (l2AutoIncrProcessorDTO.getWsaaLastCpiDate() < l2AutoIncrProcessorDTO.getWsaaProCpiDate()) {
				writePtrn3980(l2AutoIncrProcessorDTO, readerDTO);
				l2AutoIncrProcessorDTO.setWsaaLastCpiDate(datcon2.plusYears(l2AutoIncrProcessorDTO.getWsaaLastCpiDate(),
						Integer.parseInt(l2AutoIncrProcessorDTO.getWsaaFreq())));
			}
		} else {
			writePtrn3980(l2AutoIncrProcessorDTO, readerDTO);
		}
		T6658ArrayInner t6658ArrayInner = l2AutoIncrProcessorDTO.getT6658ArrayInner();
		int wsaaT6658Ix = l2AutoIncrProcessorDTO.getWsaaT6658Ix();
		if (!isEmpty(t6658ArrayInner.getWsaaT6658Optind().get(wsaaT6658Ix))
				|| !isEmpty(t6658ArrayInner.getWsaaT6658Manopt().get(wsaaT6658Ix))) {
			letterRequest3930();
		}
		/* FOR THE NEW CONTRACT... */
		/* Set various flags and save contract details for later. */
		l2AutoIncrProcessorDTO.setIncrWritten(false);
		l2AutoIncrProcessorDTO.setWsaaChdrChdrcoy(readerDTO.getChdrCoy());
		l2AutoIncrProcessorDTO.setWsaaChdrChdrnum(readerDTO.getChdrNum());
		/* EXIT */
	}

	protected void letterRequest3930() {
//		if(searchT6634Array3931()){
//		    ct02Value++;
//		}
	}
	/*
	 * protected boolean searchT6634Array3931() { // Search T6634 the Auto Letters
	 * table to get the letter type. // Build key to T6634 from contract type &
	 * transaction code. // MOVE CHDRLIF-CNTTYPE TO WSAA-T6634-CNTTYPE2. // MOVE
	 * BPRD-AUTH-CODE TO WSAA-T6634-TRCODE2.
	 * wsaaTr384Cnttype2.set(chdrlifIO.getCnttype());
	 * wsaaTr384Trcode2.set(bprdIO.getAuthCode()); // SEARCH ALL WSAA-T6634-REC
	 * ArraySearch as1 = ArraySearch.getInstance(wsaaTr384Rec);
	 * as1.setIndices(wsaaTr384Ix); as1.addSearchKey(wsaaTr384Key, wsaaTr384Key2,
	 * true); if (as1.binarySearch()) { // CONTINUE_STMT } else { // MOVE '***' TO
	 * WSAA-T6634-CNTTYPE2 // SEARCH ALL WSAA-T6634-REC
	 * wsaaTr384Cnttype2.set("***"); ArraySearch as3 =
	 * ArraySearch.getInstance(wsaaTr384Rec); as3.setIndices(wsaaTr384Ix);
	 * as3.addSearchKey(wsaaTr384Key, wsaaTr384Key2, true); if (as3.binarySearch())
	 * { // CONTINUE_STMT } else { return false; } } initialize(letrqstrec.params);
	 * letrqstrec.requestCompany.set(bsprIO.getCompany());
	 * letrqstrec.letterType.set(wsaaTr384LetterType[wsaaTr384Ix.toInt()]);
	 * letrqstrec.letterRequestDate.set(wsaaTransDate);
	 * letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
	 * letrqstrec.clntnum.set(chdrlifIO.getCownnum());
	 * letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
	 * letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
	 * letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
	 * letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
	 * letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
	 * letrqstrec.tranno.set(chdrlifIO.getTranno());
	 * letrqstrec.branch.set(chdrlifIO.getCntbranch());
	 * letrqstrec.function.set("ADD"); callProgram(Letrqst.class,
	 * letrqstrec.params); if (isNE(letrqstrec.statuz,varcom.oK)) {
	 * syserrrec.params.set(letrqstrec.params);
	 * syserrrec.statuz.set(letrqstrec.statuz); fatalError600(); } return true; }
	 */

	private boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}

	protected void writePtrn3980(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		Chdrpf chdrlifIO = l2AutoIncrProcessorDTO.getChdrlifIO();
		/* Write PTRN record. */
		Ptrnpf ptrnIO = new Ptrnpf();
		/* INITIALIZE PTRNREC-KEY-DATA */
		/* PTRNREC-NON-KEY-DATA. contractChange3900 */
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		if (l2AutoIncrProcessorDTO.isFoundPro()) {
			ptrnIO.setTranno(l2AutoIncrProcessorDTO.getTranno());
			l2AutoIncrProcessorDTO.setTranno(l2AutoIncrProcessorDTO.getTranno() + 1);
		} else {
			ptrnIO.setTranno(chdrlifIO.getTranno());
		}
		ptrnIO.setTrdt(l2AutoIncrProcessorDTO.getEffDate());
		ptrnIO.setTrtm(l2AutoIncrProcessorDTO.getTransTime());
		if (l2AutoIncrProcessorDTO.isFoundPro()) {
			ptrnIO.setPtrneff(l2AutoIncrProcessorDTO.getWsaaLastCpiDate());
		} else {
			ptrnIO.setPtrneff(l2AutoIncrProcessorDTO.getOldCpiDate());
		}
		ptrnIO.setDatesub(l2AutoIncrProcessorDTO.getEffDate());
		ptrnIO.setUserT(getUser(l2AutoIncrProcessorDTO));
		ptrnIO.setTermid("");

		ptrnIO.setBatcpfx(l2AutoIncrProcessorDTO.getPrefix());
		ptrnIO.setBatccoy(Companies.LIFE);
		ptrnIO.setBatcbrn(l2AutoIncrProcessorDTO.getBatcbrn());
		ptrnIO.setBatcactyr(l2AutoIncrProcessorDTO.getActyear());
		ptrnIO.setBatcactmn(l2AutoIncrProcessorDTO.getActmonth());
		ptrnIO.setBatctrcde(l2AutoIncrProcessorDTO.getBatchTrcde());
		ptrnIO.setBatcbatch(l2AutoIncrProcessorDTO.getBatch());

		ptrnIO.setJobnm(l2AutoIncrProcessorDTO.getBatchName().toUpperCase());
		ptrnIO.setUsrprf(l2AutoIncrProcessorDTO.getUserName());

		List<Ptrnpf> ptrnList = readerDTO.getPtrnpfInsertList();
		if (ptrnList == null) {
			ptrnList = new ArrayList<>();
		}
		ptrnList.add(ptrnIO);
		readerDTO.setPtrnpfInsertList(ptrnList);
		l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal05(
				l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().getControlTotal05().add(BigDecimal.ONE));
	}

	private boolean existingIncrease3400(IncrsumDTO incrsrec) {
		List<Incrpf> incrrgpIOList = incrpfDAO.readIncrpfList(incrsrec.getChdrcoy(), incrsrec.getChdrnum(),
				incrsrec.getLife(), incrsrec.getCoverage(), incrsrec.getRider(),
				Integer.parseInt(incrsrec.getPlnsfx()));
		if (incrrgpIOList != null && !incrrgpIOList.isEmpty()) {
			Incrpf incrrgpIO = incrrgpIOList.get(0);
			if (incrrgpIO.getCrrcd().equals(incrsrec.getEffdate())) {
				return true;
			}
		}
		return false;
	}

	protected void searchT56875100(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		/* Search table T5687 to get the anniversary method, using */
		/* CRTABLE as the key. */
		T5687ArrayInner t5687ArrayInner = l2AutoIncrProcessorDTO.getT5687ArrayInner();
		List<String> keys = t5687ArrayInner.getWsaaT5687Crtable();// getWsaaT5687Key();
		List<Integer> currfroms = t5687ArrayInner.getWsaaT5687Currfrom();
		boolean foundFlag = false;
		int i = 0;
		for (; i < keys.size(); i++) {
			if (keys.get(i) != null && keys.get(i).equals(readerDTO.getCrtable())
					&& currfroms.get(i) <= readerDTO.getCpiDte()) {
				foundFlag = true;
				break;
			}
		}
		if (!foundFlag) {
			throw new ItemNotfoundException("Item not found in Table T5687: " + readerDTO.getChdrNum());
		}
		if (t5687ArrayInner.getWsaaT5687Annmthd().get(i) == null
				|| t5687ArrayInner.getWsaaT5687Annmthd().get(i).isEmpty()) {
			fatalError("h053");
		}
		l2AutoIncrProcessorDTO.setWsaaT5687Ix(i);
		l2AutoIncrProcessorDTO.setWsaaBasicCommMeth(t5687ArrayInner.getWsaaT5687Bcmthd().get(i));
		l2AutoIncrProcessorDTO.setWsaaBascpy(t5687ArrayInner.getWsaaT5687Bascpy().get(i));
		l2AutoIncrProcessorDTO.setWsaaRnwcpy(t5687ArrayInner.getWsaaT5687Rnwcpy().get(i));
		l2AutoIncrProcessorDTO.setWsaaSrvcpy(t5687ArrayInner.getWsaaT5687Srvcpy().get(i));
	}

	protected void markprorated(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		List<Ptrnpf> ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(readerDTO.getChdrCoy(), readerDTO.getChdrNum());
		if (ptrnrecords != null) {
			for (Ptrnpf ptrn : ptrnrecords) {
				if (!TA85.equals(ptrn.getBatctrcde()) && !B523.equals(ptrn.getBatctrcde())) {
					continue;
				}
				if (B523.equals(ptrn.getBatctrcde())) {
					break;
				}
				if (TA85.equals(ptrn.getBatctrcde())) {
					l2AutoIncrProcessorDTO.setFoundPro(true);
					// Ptrnpf ptrnpfReinstate = ptrnpfDAO.getPtrnData(readerDTO.getChdrCoy(),
					// readerDTO.getChdrNum(), TA85);
					calcProCpi(l2AutoIncrProcessorDTO, readerDTO);
				}
			}
		}
	}

	protected void calcProCpi(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		int wsaaProCpiDate = readLeadDays(l2AutoIncrProcessorDTO, readerDTO);
		if (wsaaProCpiDate == 0) {
			Chdrpf chdrlifIO = l2AutoIncrProcessorDTO.getChdrlifIO();
			l2AutoIncrProcessorDTO.setWsaaLastCpiDate(chdrlifIO.getBtdate());
			l2AutoIncrProcessorDTO.setTranno(chdrlifIO.getTranno());
			l2AutoIncrProcessorDTO.setWsaaFreq(chdrlifIO.getBillfreq());
		}
		l2AutoIncrProcessorDTO.setWsaaProCpiDate(wsaaProCpiDate);
	}

	protected void a100ReadTableTr695(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		Covrpf covrincIO = null;
		if (l2AutoIncrProcessorDTO.getCovrincIO() != null) {
			covrincIO = l2AutoIncrProcessorDTO.getCovrincIO();
		} else {
			List<Covrpf> covrincList = covrpfDAO.searchValidCovrpfRecords(readerDTO.getChdrCoy(),
					readerDTO.getChdrNum(), readerDTO.getLife(), readerDTO.getCoverage(), readerDTO.getRider());
			if (covrincList != null && !covrincList.isEmpty()) {
				for (Covrpf covrpf : covrincList) {
					if (readerDTO.getjLife().equals(covrpf.getJlife())
							&& readerDTO.getPlnSfx().equals(covrpf.getPlanSuffix())) {
						covrincIO = covrpf;
						break;
					}
				}
			}
		}
		String wsaaTr695Coverage = null;
		String wsaaTr695Rider = null;
		if (covrincIO != null) {
			wsaaTr695Coverage = covrincIO.getCrtable();
			wsaaTr695Rider = covrincIO.getCrtable();
		}
		String wsaaTr695Key = wsaaTr695Coverage + wsaaTr695Rider;
		Tr695 tr695Item = itempfService.readSmartTableByTrim(Companies.LIFE, "TR695", wsaaTr695Key, Tr695.class);
		if (tr695Item == null) {
			wsaaTr695Rider = "****";
			wsaaTr695Key = wsaaTr695Coverage + wsaaTr695Rider;
			tr695Item = itempfService.readSmartTableByTrim(Companies.LIFE, "TR695", wsaaTr695Key, Tr695.class);
		}
		if (tr695Item == null) {
			return;
		}
		l2AutoIncrProcessorDTO.setWsaaBasicCommMeth(tr695Item.getBasicCommMeth());
		l2AutoIncrProcessorDTO.setWsaaBascpy(tr695Item.getBascpy());
		l2AutoIncrProcessorDTO.setWsaaSrvcpy(tr695Item.getSrvcpy());
		l2AutoIncrProcessorDTO.setWsaaRnwcpy(tr695Item.getRnwcpy());
	}

	protected void searchT66585200(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {

		T6658ArrayInner t6658ArrayInner = l2AutoIncrProcessorDTO.getT6658ArrayInner();
		List<String> keys = t6658ArrayInner.getWsaaT6658Annmthd();
		List<Integer> currfroms = t6658ArrayInner.getWsaaT6658Currfrom();
		boolean foundFlag = false;
		int i = 0;
		int t5687Ix = l2AutoIncrProcessorDTO.getWsaaT5687Ix();
		String t5687Annmthd = l2AutoIncrProcessorDTO.getT5687ArrayInner().getWsaaT5687Annmthd().get(t5687Ix);

		for (; i < keys.size(); i++) {
			if (keys.get(i).equals(t5687Annmthd) && currfroms.get(i) <= readerDTO.getCpiDte()) {
				foundFlag = true;
				break;
			}
		}
		if (!foundFlag) {
			throw new ItemNotfoundException("Item not found in Table T6658: " + readerDTO.getChdrNum());
		}
		if (t6658ArrayInner.getWsaaT6658Annmthd().get(i) == null
				|| t6658ArrayInner.getWsaaT6658Annmthd().get(i).isEmpty()) {
			fatalError("h036");
		}
		if (t6658ArrayInner.getWsaaT6658Premsubr().get(i) == null
				|| t6658ArrayInner.getWsaaT6658Premsubr().get(i).isEmpty()) {
			fatalError("e512");
		}
		l2AutoIncrProcessorDTO.setWsaaT6658Ix(i);
	}

	protected Payrpf callIncPremsubr3100(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO, L2AutoIncrReaderDTO readerDTO) {
		Payrpf payrIO = payrpfDAO.getPayrRecord(readerDTO.getChdrCoy(), readerDTO.getChdrNum());
		if (payrIO == null) {
			fatalError("MNRF:payrIO");
		}
		Chdrpf chdrlifIO = l2AutoIncrProcessorDTO.getChdrlifIO();
		IncrsumDTO incrsrec = new IncrsumDTO();
		l2AutoIncrProcessorDTO.setIncrsumDTO(incrsrec);
		incrsrec.setOrigsum(BigDecimal.ZERO);
		incrsrec.setLastsum(BigDecimal.ZERO);
		incrsrec.setNewsum(BigDecimal.ZERO);
		incrsrec.setOriginst01(BigDecimal.ZERO);
		incrsrec.setLastinst01(BigDecimal.ZERO);
		incrsrec.setNewinst01(BigDecimal.ZERO);
		incrsrec.setCurrinst01(BigDecimal.ZERO);
		incrsrec.setOriginst02(BigDecimal.ZERO);
		incrsrec.setLastinst02(BigDecimal.ZERO);
		incrsrec.setNewinst02(BigDecimal.ZERO);
		incrsrec.setCurrinst02(BigDecimal.ZERO);
		incrsrec.setOriginst03(BigDecimal.ZERO);
		incrsrec.setLastinst03(BigDecimal.ZERO);
		incrsrec.setNewinst03(BigDecimal.ZERO);
		incrsrec.setCurrinst03(BigDecimal.ZERO);
		incrsrec.setPctinc(BigDecimal.ZERO);
		incrsrec.setCurrinst01(readerDTO.getInstPrem());
		incrsrec.setCurrinst02(readerDTO.getZbinstPrem());
		incrsrec.setCurrinst03(readerDTO.getZlinstPrem());
		incrsrec.setCurrsum(readerDTO.getSumIns());
		getOrigLastDetails3200(incrsrec, l2AutoIncrProcessorDTO, readerDTO);
		incrsrec.setFunction("");
		incrsrec.setStatuz("");
		incrsrec.setChdrcoy(readerDTO.getChdrCoy());
		incrsrec.setChdrnum(readerDTO.getChdrNum());
		incrsrec.setCoverage(readerDTO.getCoverage());
		incrsrec.setLife(readerDTO.getLife());
		incrsrec.setRider(readerDTO.getRider());
		incrsrec.setPlnsfx(readerDTO.getPlnSfx().toString());
		incrsrec.setEffdate(readerDTO.getCpiDte());
		incrsrec.setCrtable(readerDTO.getCrtable());
		incrsrec.setRcesdte(readerDTO.getRcesdte());
		incrsrec.setMortcls(readerDTO.getMortCls());
		if (payrIO != null) {
			incrsrec.setBillfreq(payrIO.getBillfreq());
			incrsrec.setMop(payrIO.getBillchnl());
		}
		incrsrec.setOccdate(chdrlifIO.getOccdate());
		incrsrec.setCntcurr(readerDTO.getPrmcur());
		incrsrec.setLanguage(l2AutoIncrProcessorDTO.getLang());
		int wsaaT6658Ix = l2AutoIncrProcessorDTO.getWsaaT6658Ix();
		int wsaaT5687Ix = l2AutoIncrProcessorDTO.getWsaaT5687Ix();
		T6658ArrayInner t6658ArrayInner = l2AutoIncrProcessorDTO.getT6658ArrayInner();
		if (BigDecimal.ZERO.compareTo(t6658ArrayInner.getWsaaT6658Maxpcnt().get(wsaaT6658Ix)) == 0) {
			incrsrec.setMaxpcnt(BigDecimal.valueOf(99.999));
		} else {
			incrsrec.setMaxpcnt(t6658ArrayInner.getWsaaT6658Maxpcnt().get(wsaaT6658Ix));
		}
		incrsrec.setMinpcnt(t6658ArrayInner.getWsaaT6658Minpcnt().get(wsaaT6658Ix));
		incrsrec.setFixdtrm(t6658ArrayInner.getWsaaT6658Fixdtrm().get(wsaaT6658Ix));
		incrsrec.setAnnvmeth(l2AutoIncrProcessorDTO.getT5687ArrayInner().getWsaaT5687Annmthd().get(wsaaT5687Ix));

		/*
		 * IVE-866 Life - Auto Increase Calculation Rework- Integration with latest PA
		 * compatible models started
		 */
		// callProgram(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()],
		// incrsrec.setIncreaseRec);
		/*
		 * ILIFE-3915 ALS-358 - premium is not increased when benefit amount is
		 * increased Start
		 */
		if (l2AutoIncrProcessorDTO.isPermission()) {
			incrsrec.setAutoincreaseindicator("");
			readT5654(l2AutoIncrProcessorDTO);
			T5654 t5654rec = l2AutoIncrProcessorDTO.getT5654IO();
			if (!"N".equals(t5654rec.getIndxflg())) {
				if ((t5654rec.getCpidef() != null && !t5654rec.getCpidef().isEmpty())
						|| (t5654rec.getCpiallwd() != null && !t5654rec.getCpiallwd().isEmpty()))
					incrsrec.setAutoincreaseindicator("C");
				if ((t5654rec.getPredef() != null && !t5654rec.getPredef().isEmpty())
						|| (t5654rec.getPredefallwd() != null && !t5654rec.getPredefallwd().isEmpty()))
					incrsrec.setAutoincreaseindicator("P");
			}

		} else {
			incrsrec.setAutoincreaseindicator("P");
		}
		String caller = l2AutoIncrProcessorDTO.getT6658ArrayInner().getWsaaT6658Premsubr().get(wsaaT6658Ix);
		PremiumDTO premiumrec = new PremiumDTO();
		l2AutoIncrProcessorDTO.setPremiumrec(premiumrec);
		if (!(l2AutoIncrProcessorDTO.isVpmsFlag() && vpmscaller.checkVPMSModel(caller))) {
			try {
				incrsum.calcIncrPrem(incrsrec);
			} catch (IOException | ParseException e) {
				throw new SubroutineIOException("Exception in incrsum: " + e.getMessage());
			}
		} else {
			premiumrec.setWaitperiod("");
			premiumrec.setBentrm("");
			premiumrec.setPrmbasis("");
			premiumrec.setPoltyp("");
			premiumrec.setOccpcode("");
			if (l2AutoIncrProcessorDTO.isPermission()) {
				Rcvdpf rcvdPFObject = callReadRCVDPF(readerDTO);
				if (rcvdPFObject != null) {
					premiumrec.setWaitperiod(rcvdPFObject.getWaitperiod());
					premiumrec.setBentrm(rcvdPFObject.getBentrm());
					if (rcvdPFObject.getPrmbasis() != null && "S".equals(rcvdPFObject.getPrmbasis()))
						premiumrec.setPrmbasis("Y");
					else
						premiumrec.setPrmbasis("");
					premiumrec.setPoltyp(rcvdPFObject.getPoltyp());
					boolean dialdownflag = FeaConfg.isFeatureExist(chdrlifIO.getChdrcoy(), "NBPRP012", "IT");
					if (dialdownflag) {
						if (rcvdPFObject.getDialdownoption() != null) {
							if (rcvdPFObject.getDialdownoption().startsWith("0")) {
								premiumrec.setDialdownoption(rcvdPFObject.getDialdownoption().substring(1));
							} else {
								premiumrec.setDialdownoption(rcvdPFObject.getDialdownoption());
							}
						}
					}
				}
			}
			calc6005(incrsrec, premiumrec, l2AutoIncrProcessorDTO);
			premiumrec.setCnttype(chdrlifIO.getCnttype());

			vpxlext.processVpxlext("INIT", premiumrec);
			VpxchdrDTO vpxchdrDTO = vpxchdr.callInit(premiumrec);

			premiumrec.setRstaflag(vpxchdrDTO.getRstaflag());

			try {
				vpxacbl.processVpxacbl(premiumrec);
			} catch (IOException e) {
				throw new SubroutineIOException("Exception in vpxacbl: " + e.getMessage());
			}

			premiumrec.setFunction("INCR");

			T5687 t5687IO = itempfService.readSmartTableByTrim(incrsrec.getChdrcoy(), "T5687", incrsrec.getCrtable(),
					T5687.class);
			if (l2AutoIncrProcessorDTO.getLifergpIO() == null) {
				premiumrec.setPremMethod(t5687IO.getPremmeth());
			} else {
				premiumrec.setPremMethod(t5687IO.getJlPremMeth());
			}
			Covrpf covrincIO = null;
			if (l2AutoIncrProcessorDTO.getCovrincIO() != null) {
				covrincIO = l2AutoIncrProcessorDTO.getCovrincIO();
			} else {
				List<Covrpf> covrincList = covrpfDAO.searchValidCovrpfRecords(readerDTO.getChdrCoy(),
						readerDTO.getChdrNum(), readerDTO.getLife(), readerDTO.getCoverage(), readerDTO.getRider());
				if (covrincList != null && !covrincList.isEmpty()) {
					for (Covrpf covrpf : covrincList) {
						if (readerDTO.getjLife().equals(covrpf.getJlife())
								&& readerDTO.getPlnSfx().equals(covrpf.getPlanSuffix())) {
							covrincIO = covrpf;
							break;
						}
					}
				}
			}
			premiumrec.setRstate01(covrincIO.getZclstate() == null ? "" : covrincIO.getZclstate());
			if (l2AutoIncrProcessorDTO.isStampDutyflag()) {
				LinkageInfoService linkgService = new LinkageInfoService();
				premiumrec.setLinkcov(linkgService.getLinkageInfo(covrincIO.getLnkgno()));
				incrsrec.setZstpduty01(BigDecimal.ZERO);
				incrsrec.setZstpduty02(BigDecimal.ZERO);
				incrsrec.setZstpduty03(BigDecimal.ZERO);
			}

			if ("AN14".equals(incrsrec.getAnnvmeth())) {
				incrsrec.setAnnvmeth("AN03");
			}
			if (incrsrec.getAutoincreaseindicator() == null || incrsrec.getAutoincreaseindicator().isEmpty()) {
				incrsrec.setAutoincreaseindicator("P");
			}
			String simpleInd = l2AutoIncrProcessorDTO.getT6658ArrayInner().getWsaaT6658SimpInd().get(wsaaT6658Ix);
			incrsrec.setSimpleInd(simpleInd);
			try {
				incrsum.calcIncrPrem(incrsrec);
				if (BigDecimal.ZERO.compareTo(incrsrec.getNewsum()) != 0) {
					incrsum.calcIncrPrem(incrsrec);
				}
			} catch (IOException | ParseException e) {
				throw new SubroutineIOException("Exception in incrsum: " + e.getMessage());
			}
		}
		premiumrec.setRiskPrem(BigDecimal.ZERO);
		boolean riskPremflag = FeaConfg.isFeatureExist(readerDTO.getChdrCoy(), "NBPRP094", "IT");
		if (riskPremflag) {
			premiumrec.setRiskPrem(BigDecimal.ZERO);
			premiumrec.setCnttype(chdrlifIO.getCnttype());
			premiumrec.setCrtable(readerDTO.getCrtable());
			premiumrec.setCalcTotPrem(readerDTO.getInstPrem());
//			callProgram("RISKPREMIUM", premiumrec.premiumRec);

			VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
			vpmsinfoDTO.setCoy(readerDTO.getChdrCoy());
			vpmsinfoDTO.setTransDate(l2AutoIncrProcessorDTO.getEffDate() + "");
			vpmsinfoDTO.setModelName("RISKPREMIUM");
			vpmscaller.callPRMPMREST(vpmsinfoDTO, premiumrec, null, null);

		}
		if (BigDecimal.ZERO.compareTo(incrsrec.getNewsum()) == 0) {
			incrsrec.setNewsum(incrsrec.getCurrsum());
		}
		if (BigDecimal.ZERO.compareTo(incrsrec.getNewinst01()) == 0) {
			incrsrec.setNewinst01(incrsrec.getCurrinst01());
		}
		if (BigDecimal.ZERO.compareTo(incrsrec.getNewinst02()) == 0) {
			incrsrec.setNewinst02(incrsrec.getCurrinst02());
		}
		if (BigDecimal.ZERO.compareTo(incrsrec.getNewinst03()) == 0) {
			incrsrec.setNewinst03(incrsrec.getCurrinst03());
		}
		if (l2AutoIncrProcessorDTO.isStampDutyflag()) {
			if (BigDecimal.ZERO.compareTo(incrsrec.getNewinst01()) != 0
					&& BigDecimal.ZERO.compareTo(incrsrec.getZstpduty01()) != 0) {
				incrsrec.setNewinst01(incrsrec.getNewinst01().add(incrsrec.getZstpduty01()));
			}
			if (BigDecimal.ZERO.compareTo(incrsrec.getNewinst02()) != 0
					&& BigDecimal.ZERO.compareTo(incrsrec.getZstpduty02()) != 0) {
				incrsrec.setNewinst02(incrsrec.getNewinst02().add(incrsrec.getZstpduty02()));
			}
			if (BigDecimal.ZERO.compareTo(incrsrec.getNewinst03()) != 0
					&& BigDecimal.ZERO.compareTo(incrsrec.getZstpduty03()) != 0) {
				incrsrec.setNewinst03(incrsrec.getNewinst03().add(incrsrec.getZstpduty03()));
			}
		}
		return payrIO;
	}

	protected Rcvdpf callReadRCVDPF(L2AutoIncrReaderDTO readerDTO) {
		Rcvdpf rcvdPFObject = new Rcvdpf();
		rcvdPFObject.setChdrcoy(readerDTO.getChdrCoy());
		rcvdPFObject.setChdrnum(readerDTO.getChdrNum());
		rcvdPFObject.setLife(readerDTO.getLife());
		rcvdPFObject.setCoverage(readerDTO.getCoverage());
		rcvdPFObject.setRider(readerDTO.getRider());
		rcvdPFObject.setCrtable(readerDTO.getCrtable());
		rcvdPFObject = rcvdpfDAO.readRcvdpf(rcvdPFObject);
		return rcvdPFObject;
	}

	protected void getOrigLastDetails3200(IncrsumDTO incrsrec, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO,
			L2AutoIncrReaderDTO readerDTO) {
		if (l2AutoIncrProcessorDTO.isFoundPro() && l2AutoIncrProcessorDTO.isChangeExist()) {
			incrsrec.setOriginst01(l2AutoIncrProcessorDTO.getWsaaOriginst01());
			incrsrec.setOriginst02(l2AutoIncrProcessorDTO.getWsaaOriginst02());
			incrsrec.setOriginst03(l2AutoIncrProcessorDTO.getWsaaOriginst03());
			incrsrec.setOrigsum(l2AutoIncrProcessorDTO.getWsaaOrigsum());
		} else {
			incrsrec.setOriginst01(readerDTO.getInstPrem());
			incrsrec.setOriginst02(readerDTO.getZbinstPrem());
			incrsrec.setOriginst03(readerDTO.getZlinstPrem());
			incrsrec.setOrigsum(readerDTO.getSumIns());
		}
		incrsrec.setLastinst01(readerDTO.getInstPrem());
		incrsrec.setLastinst02(readerDTO.getZbinstPrem());
		incrsrec.setLastinst03(readerDTO.getZlinstPrem());
		incrsrec.setLastsum(readerDTO.getSumIns());
		read3201(incrsrec, l2AutoIncrProcessorDTO, readerDTO);
	}

	/**
	 * <pre>
	**** Read the original INCR records to obtain the last and
	**** original premium / sum assured details.
	 * </pre>
	 */
	protected void read3201(IncrsumDTO incrsrec, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO,
			L2AutoIncrReaderDTO readerDTO) {
		if (l2AutoIncrProcessorDTO.isFoundPro() && l2AutoIncrProcessorDTO.isChangeExist()) {
			return;
		}
		List<Incrpf> incrpfList = incrpfDAO.readValidIncrpf(readerDTO.getChdrCoy(), readerDTO.getChdrNum());
		if (incrpfList != null && !incrpfList.isEmpty()) {
			for (Incrpf c : incrpfList) {
				if (readerDTO.getChdrCoy().equals(c.getChdrcoy()) && readerDTO.getLife().equals(c.getLife())
						&& readerDTO.getCoverage().equals(c.getCoverage()) && readerDTO.getRider().equals(c.getRider())
						&& readerDTO.getPlnSfx().equals(c.getPlnsfx())) {
					/*
					 * For the first valid INCR record found, move the ORIG details to the INCC
					 * record.
					 */
					if (c.getRefusalFlag() == null || c.getRefusalFlag().isEmpty()) {
						incrsrec.setLastinst01(c.getNewinst());
						incrsrec.setCurrinst01(c.getNewinst());
						incrsrec.setLastinst02(c.getZbnewinst());
						incrsrec.setCurrinst02(c.getZbnewinst());
						incrsrec.setLastinst03(c.getZlnewinst());
						incrsrec.setCurrinst03(c.getZlnewinst());
						incrsrec.setLastsum(c.getNewsum());
						incrsrec.setCurrsum(c.getNewsum());
						/* If the contract retrieved is not the required one, (after */
						/* using BEGN), move ENDP to INCR-STATUZ to end the loop, */
						/* then exit the section. */
						if ((incrsrec.getOriginst01().compareTo(readerDTO.getInstPrem()) == 0)
								&& (incrsrec.getOrigsum().compareTo(readerDTO.getSumIns()) == 0)) {
							incrsrec.setOriginst01(c.getOrigInst());
							incrsrec.setOriginst02(c.getZboriginst());
							incrsrec.setOriginst03(c.getZboriginst());
							incrsrec.setOrigsum(c.getOrigSum());
							break;
						}
					}
				}
			}
		}
	}

	private void readT5654(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		String cnttype = l2AutoIncrProcessorDTO.getChdrlifIO().getCnttype();
		T5654 item5654 = itempfService.readSmartTableByTrim(Companies.LIFE, "T5654", cnttype, T5654.class);
		if (item5654 == null) {
			fatalError("t5654:" + cnttype);
		} else {
			l2AutoIncrProcessorDTO.setT5654IO(item5654);
		}
	}

	protected void calc6005(IncrsumDTO incrsrec, PremiumDTO premiumrec, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		premiumrec.setFunction("INCR");
		premiumrec.setChdrChdrcoy(incrsrec.getChdrcoy());
		premiumrec.setChdrChdrnum(incrsrec.getChdrnum());
		premiumrec.setLifeLife(incrsrec.getLife());
		premiumrec.setLifeJlife("00");
		premiumrec.setCovrCoverage(incrsrec.getCoverage());
		premiumrec.setCovrRider(incrsrec.getRider());
		premiumrec.setCrtable(incrsrec.getCrtable());
		premiumrec.setEffectdt(incrsrec.getEffdate());
		premiumrec.setTermdate(incrsrec.getRcesdte());
		premiumrec.setLanguage(incrsrec.getLanguage());
		getLifeDetails7000(incrsrec, premiumrec, l2AutoIncrProcessorDTO);
		Long freqFactor;
		try {
			freqFactor = datcon3.getYearsDifference(incrsrec.getEffdate(), premiumrec.getTermdate());
		} catch (ParseException e) {
			throw new SubroutineIOException("Exception in datcon3: " + e.getMessage());
		}
//		datcon3rec.freqFactor.add(0.99999());
		premiumrec.setDuration(freqFactor.intValue());
		premiumrec.setCurrcode(incrsrec.getCntcurr());
		premiumrec.setSumin(incrsrec.getOrigsum());
		premiumrec.setMortcls(incrsrec.getMortcls());
		premiumrec.setBillfreq(incrsrec.getBillfreq());
		premiumrec.setMop(incrsrec.getMop());
		premiumrec.setRatingdate(incrsrec.getOccdate());
		premiumrec.setReRateDate(incrsrec.getOccdate());
		premiumrec.setCalcPrem(incrsrec.getCurrinst01());// ILIFE-7336
		premiumrec.setCalcBasPrem(BigDecimal.ZERO);
		premiumrec.setCalcLoaPrem(BigDecimal.ZERO);
		if (l2AutoIncrProcessorDTO.getT5675Premsubr() == null) {
			incrsrec.setNewinst01(incrsrec.getCurrinst01());
		}
		getAnny8000(incrsrec, premiumrec);
	}

	protected void getAnny8000(IncrsumDTO incrsrec, PremiumDTO premiumrec) {
		Annypf annyIO = new Annypf();
		annyIO.setChdrcoy(incrsrec.getChdrcoy());
		annyIO.setChdrnum(incrsrec.getChdrnum());
		annyIO.setLife(incrsrec.getLife());
		annyIO.setCoverage(incrsrec.getCoverage());
		annyIO.setRider(incrsrec.getRider());
		annyIO.setPlnsfx(Integer.parseInt(incrsrec.getPlnsfx()));
		List<Annypf> annyIOList = annypfDAO.getAnnypfRecord(annyIO);

		if (annyIOList != null && !annyIOList.isEmpty()) {
			annyIO = annyIOList.get(0);
			premiumrec.setFreqann(annyIO.getFreqann());
			premiumrec.setArrears(annyIO.getArrears());
			premiumrec.setAdvance(annyIO.getAdvance());
			premiumrec.setGuarperd(annyIO.getGuarperd());
			premiumrec.setIntanny(annyIO.getIntanny());
			premiumrec.setCapcont(annyIO.getCapcont());
			premiumrec.setWithprop(annyIO.getWithprop());
			premiumrec.setWithoprop(annyIO.getWithoprop());
			premiumrec.setPpind(annyIO.getPpind());
			premiumrec.setNomlife(annyIO.getNomlife());
			premiumrec.setDthpercn(annyIO.getDthpercn());
			premiumrec.setDthperco(annyIO.getDthperco());
		} else {
			premiumrec.setAdvance("");
			premiumrec.setArrears("");
			premiumrec.setFreqann("");
			premiumrec.setWithprop("");
			premiumrec.setWithoprop("");
			premiumrec.setPpind("");
			premiumrec.setNomlife("");
			premiumrec.setGuarperd(0);
			premiumrec.setIntanny(BigDecimal.ZERO);
			premiumrec.setCapcont(BigDecimal.ZERO);
			premiumrec.setDthpercn(BigDecimal.ZERO);
			premiumrec.setDthperco(BigDecimal.ZERO);
		}
	}

	protected void getLifeDetails7000(IncrsumDTO incrsrec, PremiumDTO premiumrec,
			L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		// lifergpIO.setJlife("00");
		List<Lifepf> lifeList = lifepfDAO.getLifeRecord(incrsrec.getChdrcoy(), incrsrec.getChdrnum(),
				incrsrec.getLife(), "00");
		Lifepf lifergpIO = null;
		if (lifeList != null && !lifeList.isEmpty()) {
			for (Lifepf life : lifeList) {
				if ("1".equals(life.getValidflag())) {
					lifergpIO = life;
					break;
				}
			}
		}
		int wsaaAnb = calculateAnb7500(incrsrec, lifergpIO);
		premiumrec.setLage(wsaaAnb);
		if (lifergpIO != null) {
			premiumrec.setLsex(lifergpIO.getCltsex());
		}
		// lifergpIO.setJlife("01");
		lifeList = lifepfDAO.getLifeRecord(incrsrec.getChdrcoy(), incrsrec.getChdrnum(), incrsrec.getLife(), "01");
		lifergpIO = null;
		if (lifeList != null && !lifeList.isEmpty()) {
			for (Lifepf life : lifeList) {
				if ("1".equals(life.getValidflag())) {
					lifergpIO = life;
					break;
				}
			}
		}

		T5687 t5687rec = l2AutoIncrProcessorDTO.getT5687IO();

		String itemitem = null;
		if (lifergpIO == null) {
			premiumrec.setJlsex("");
			premiumrec.setJlage(0);
			itemitem = t5687rec.getPremmeth();
		} else {
			wsaaAnb = calculateAnb7500(incrsrec, lifergpIO);
			premiumrec.setJlsex(lifergpIO.getCltsex());
			premiumrec.setJlage(wsaaAnb);
			itemitem = t5687rec.getJlPremMeth();
			l2AutoIncrProcessorDTO.setLifergpIO(lifergpIO);
		}
		T5675 itemT5675 = itempfService.readSmartTableByTrim(incrsrec.getChdrcoy(), "T5675", itemitem, T5675.class);
		if (itemT5675 == null) {
			l2AutoIncrProcessorDTO.setT5675Premsubr(null);
			return;
		}
		l2AutoIncrProcessorDTO.setT5675Premsubr(itemT5675.getPremsubr());
	}

	protected int calculateAnb7500(IncrsumDTO incrsrec, Lifepf lifergpIO) {
		List<Chdrpf> chdrlnbIOList = chdrpfDAO.readRecordOfChdrlnb(incrsrec.getChdrcoy(), incrsrec.getChdrnum());
		if (chdrlnbIOList == null || chdrlnbIOList.isEmpty()) {
			fatalError("Chdrlnb:MNRF");
		}
		T1693 itemIO1693 = itempfService.readSmartTableByTrim(Companies.SYSTEM_COMPANY, "T1693", incrsrec.getChdrcoy(),
				T1693.class);

		if (itemIO1693 == null) {
			fatalError("T1693:MNRF");
		}
		AgecalcDTO agecalcrec = new AgecalcDTO();
		agecalcrec.setFunction("CALCP");
		if (chdrlnbIOList != null) {
			agecalcrec.setCnttype(chdrlnbIOList.get(0).getCnttype());
		}
		agecalcrec.setIntDate1(lifergpIO.getCltdob() + "");
		agecalcrec.setIntDate2(incrsrec.getEffdate() + "");
		if (itemIO1693 != null) {
			agecalcrec.setCompany(itemIO1693.getFsuco());
		}
		agecalcrec.setLanguage(incrsrec.getLanguage());
		try {
			agecalcrec = agecalc.processAgecalc(agecalcrec);
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Exception in agecalc: " + e.getMessage());
		}
		return agecalcrec.getAgerating();
	}

	protected void setupNextCall(L2AutoIncrReaderDTO readerDTO, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		String billfreq = l2AutoIncrProcessorDTO.getT6658ArrayInner().getWsaaT6658Billfreq()
				.get(l2AutoIncrProcessorDTO.getWsaaT6658Ix());
		int cpiDate = datcon2.plusYears(readerDTO.getCpiDte(), Integer.parseInt(billfreq));
		readerDTO.setCpiDte(cpiDate);
		IncrsumDTO incrsrec = l2AutoIncrProcessorDTO.getIncrsumDTO();
		readerDTO.setInstPrem(incrsrec.getNewinst01());
		readerDTO.setZbinstPrem(incrsrec.getNewinst02());
		readerDTO.setZlinstPrem(incrsrec.getNewinst03());
		readerDTO.setSumIns(incrsrec.getNewsum());
		l2AutoIncrProcessorDTO.setWsaaOriginst01(incrsrec.getOriginst01());
		l2AutoIncrProcessorDTO.setWsaaOriginst02(incrsrec.getOriginst02());
		l2AutoIncrProcessorDTO.setWsaaOriginst03(incrsrec.getOriginst03());
		l2AutoIncrProcessorDTO.setWsaaOrigsum(incrsrec.getOrigsum());
	}

	protected void noIncreaseOnCovr3050(L2AutoIncrReaderDTO readerDTO, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		Covrpf covrincIO = null;
		if (l2AutoIncrProcessorDTO.getCovrincIO() != null) {
			covrincIO = l2AutoIncrProcessorDTO.getCovrincIO();
		} else {
			List<Covrpf> covrincList = covrpfDAO.searchValidCovrpfRecords(readerDTO.getChdrCoy(),
					readerDTO.getChdrNum(), readerDTO.getLife(), readerDTO.getCoverage(), readerDTO.getRider());
			if (covrincList != null && !covrincList.isEmpty()) {
				for (Covrpf covrpf : covrincList) {
					if (readerDTO.getjLife().equals(covrpf.getJlife())
							&& readerDTO.getPlnSfx().equals(covrpf.getPlanSuffix())) {
						covrincIO = covrpf;
						break;
					}
				}
			}
		}
		/* Update the COVR record with a new CPI-DATE so that it does */
		/* not get selected until the next increase 'Anniversary'. */
		int years = Integer.parseInt(l2AutoIncrProcessorDTO.getT6658ArrayInner().getWsaaT6658Billfreq()
				.get(l2AutoIncrProcessorDTO.getWsaaT6658Ix()));
		if (covrincIO != null) {
			covrincIO.setCpidte(datcon2.plusYears(covrincIO.getCpidte(), years));
		}
		readerDTO.setCovrpfUpdate(covrincIO);
		l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal08(BigDecimal.ONE);
	}

	protected void writeIncr3300(L2AutoIncrReaderDTO readerDTO, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		/* Set up INCR. */
		Chdrpf chdrlifIO = l2AutoIncrProcessorDTO.getChdrlifIO();
		IncrsumDTO incrsrec = l2AutoIncrProcessorDTO.getIncrsumDTO();
		Incrpf incrIO = new Incrpf();
		incrIO.setTranno(chdrlifIO.getTranno());
		incrIO.setValidflag("1");
		incrIO.setStatcode(readerDTO.getStatCode());
		incrIO.setPstatcode(readerDTO.getPstatCode());
		incrIO.setCrtable(readerDTO.getCrtable());
		incrIO.setChdrcoy(incrsrec.getChdrcoy());
		incrIO.setChdrnum(incrsrec.getChdrnum());
		incrIO.setLife(incrsrec.getLife());
		incrIO.setCoverage(incrsrec.getCoverage());
		incrIO.setRider(incrsrec.getRider());
		incrIO.setPlnsfx(Integer.parseInt(incrsrec.getPlnsfx()));
		incrIO.setCrrcd(incrsrec.getEffdate());
		incrIO.setCrtable(incrsrec.getCrtable());
		incrIO.setAnniversaryMethod(incrsrec.getAnnvmeth());
		/* MOVE WSAA-T5687-BCMTHD(WSAA-T5687-IX) */
		/* TO INCR-BASIC-COMM-METH. */
		/* MOVE WSAA-T5687-BASCPY(WSAA-T5687-IX) */
		/* TO INCR-BASCPY. */
		/* MOVE WSAA-T5687-RNWCPY(WSAA-T5687-IX) */
		/* TO INCR-RNWCPY */
		/* MOVE WSAA-T5687-SRVCPY(WSAA-T5687-IX) */
		/* TO INCR-SRVCPY. */
		incrIO.setBasicCommMeth(l2AutoIncrProcessorDTO.getWsaaBasicCommMeth());
		incrIO.setBascpy(l2AutoIncrProcessorDTO.getWsaaBascpy());
		incrIO.setRnwcpy(l2AutoIncrProcessorDTO.getWsaaRnwcpy());
		incrIO.setSrvcpy(l2AutoIncrProcessorDTO.getWsaaSrvcpy());
		incrIO.setOrigSum(incrsrec.getOrigsum());
		incrIO.setLastSum(incrsrec.getLastsum());
		incrIO.setNewsum(incrsrec.getNewsum());
		incrIO.setOrigInst(incrsrec.getOriginst01());
		incrIO.setLastInst(incrsrec.getLastinst01());
		incrIO.setNewinst(incrsrec.getNewinst01());
		incrIO.setZboriginst(incrsrec.getOriginst02());
		incrIO.setZblastinst(incrsrec.getLastinst02());
		incrIO.setZbnewinst(incrsrec.getNewinst02());
		incrIO.setZloriginst(incrsrec.getOriginst03());
		incrIO.setZllastinst(incrsrec.getLastinst03());
		incrIO.setZlnewinst(incrsrec.getNewinst03());
		incrIO.setPctinc(incrsrec.getPctinc().intValue());
		incrIO.setRefusalFlag("");
		if (l2AutoIncrProcessorDTO.isStampDutyflag()) {
			String vpmModule = "INCRSUM";
			int wsaaT6658Ix = l2AutoIncrProcessorDTO.getWsaaT6658Ix();
			String premsubr = l2AutoIncrProcessorDTO.getT6658ArrayInner().getWsaaT6658Premsubr().get(wsaaT6658Ix);

			if ((vpmModule).equals(premsubr)) {
				incrIO.setZstpduty01(incrsrec.getZstpduty01());
			} else {
				if (incrsrec.getZstpduty03().compareTo(BigDecimal.ZERO) != 0)
					incrIO.setZstpduty01(incrsrec.getZstpduty03());
				else
					incrIO.setZstpduty01(incrsrec.getZstpduty02());
			}
		}
		rounding6000(incrIO, readerDTO, l2AutoIncrProcessorDTO);
		incrIO.setTransactionDate(l2AutoIncrProcessorDTO.getTransDate());
		incrIO.setTransactionTime(l2AutoIncrProcessorDTO.getTransTime());
		incrIO.setUser(getUser(l2AutoIncrProcessorDTO));
		incrIO.setTermid("");
		readerDTO.setIncrpfInsert(incrIO);
		l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal04(BigDecimal.ONE);
		/* Update flag to indicate an INCR has been written for this */
		/* component so that we write a PTRN for the contract later on */
		l2AutoIncrProcessorDTO.setIncrWritten(true);
	}

	private int getUser(L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		return Integer.parseInt(l2AutoIncrProcessorDTO.getDatimeInit());
	}

	protected void rounding6000(Incrpf incrIO, L2AutoIncrReaderDTO readerDTO,
			L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {

		/* MOVE INCR-ORIG-SUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ORIG-SUM. */
		if (BigDecimal.ZERO.compareTo(incrIO.getOrigSum()) != 0) {
			incrIO.setOrigSum(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getOrigSum()));
		}
		/* MOVE INCR-LAST-SUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-LAST-SUM. */
		if (BigDecimal.ZERO.compareTo(incrIO.getLastSum()) != 0) {
			incrIO.setLastSum(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getLastSum()));
		}
		/* MOVE INCR-NEWSUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-NEWSUM. */
		if (BigDecimal.ZERO.compareTo(incrIO.getNewsum()) != 0) {
			incrIO.setNewsum(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getNewsum()));
		}
		/* MOVE INCR-ORIG-INST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ORIG-INST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getOrigInst()) != 0) {
			incrIO.setOrigInst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getOrigInst()));
		}
		/* MOVE INCR-LAST-INST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-LAST-INST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getLastInst()) != 0) {
			incrIO.setLastInst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getLastInst()));
		}
		/* MOVE INCR-NEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-NEWINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getNewinst()) != 0) {
			incrIO.setNewinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getNewinst()));
		}
		/* MOVE INCR-ZBORIGINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZBORIGINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getZboriginst()) != 0) {
			incrIO.setZboriginst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZboriginst()));
		}
		/* MOVE INCR-ZBLASTINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZBLASTINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getZblastinst()) != 0) {
			incrIO.setZblastinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZblastinst()));
		}
		/* MOVE INCR-ZBNEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZBNEWINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getZbnewinst()) != 0) {
			incrIO.setZbnewinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZbnewinst()));
		}
		/* MOVE INCR-ZLORIGINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZLORIGINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getZloriginst()) != 0) {
			incrIO.setZloriginst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZloriginst()));
		}
		/* MOVE INCR-ZLLASTINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZLLASTINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getZllastinst()) != 0) {
			incrIO.setZllastinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZllastinst()));
		}
		/* MOVE INCR-ZLNEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZLNEWINST. */
		if (BigDecimal.ZERO.compareTo(incrIO.getZlnewinst()) != 0) {
			incrIO.setZlnewinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZlnewinst()));
		}

		if (l2AutoIncrProcessorDTO.isStampDutyflag()) {
			if (BigDecimal.ZERO.compareTo(incrIO.getZstpduty01()) != 0) {
				incrIO.setZstpduty01(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, incrIO.getZstpduty01()));
			}
		}
	}

	protected BigDecimal callRounding7000(L2AutoIncrReaderDTO readerDTO, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO,
			BigDecimal amountIn) {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(readerDTO.getPrmcur());
		zrdecplDTO.setCompany(Companies.LIFE);
		zrdecplDTO.setBatctrcde(l2AutoIncrProcessorDTO.getBprdAuthCode());
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = zrdecplc.convertAmount(zrdecplDTO);
		} catch (IOException e) {
			throw new ItemParseException("Parsed failed in zrdecplc:" + e.getMessage());
		}
		return result;
	}

	protected void writeAinr3700(L2AutoIncrReaderDTO readerDTO, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		IncrsumDTO incrsrec = l2AutoIncrProcessorDTO.getIncrsumDTO();
		Ainrpf ainrIO = new Ainrpf();
		ainrIO.setChdrcoy(readerDTO.getChdrCoy());
		ainrIO.setPlnsfx(readerDTO.getPlnSfx());
		ainrIO.setChdrnum(readerDTO.getChdrNum());
		ainrIO.setLife(readerDTO.getLife());
		ainrIO.setCoverage(readerDTO.getCoverage());
		ainrIO.setRider(readerDTO.getRider());
		ainrIO.setCrtable(readerDTO.getCrtable());
		/* MOVE INCC-CURRSUM TO AINR-OLDSUM. */
		ainrIO.setOldsum(incrsrec.getCurrsum());
		ainrIO.setNewsumi(incrsrec.getNewsum());
		ainrIO.setOldinst(incrsrec.getCurrinst01());
		ainrIO.setNewinst(incrsrec.getNewinst01());
		ainrIO.setRcesdte(readerDTO.getCpiDte());
		ainrIO.setCurrency(readerDTO.getPrmcur());
		/* MOVE AINR-RD01-OLDSUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO AINR-RD01-OLDSUM. */
		if (BigDecimal.ZERO.compareTo(ainrIO.getOldsum()) != 0) {
			ainrIO.setOldsum(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, ainrIO.getOldsum()));
		}
		/* MOVE AINR-NEWSUMI TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO AINR-NEWSUMI. */
		if (BigDecimal.ZERO.compareTo(ainrIO.getNewsumi()) != 0) {
			ainrIO.setNewsumi(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, ainrIO.getNewsumi()));
		}
		/* MOVE AINR-OLDINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO AINR-OLDINST. */
		if (BigDecimal.ZERO.compareTo(ainrIO.getOldinst()) != 0) {
			ainrIO.setOldinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, ainrIO.getOldinst()));
		}
		/* MOVE AINR-NEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO AINR-NEWINST. */
		if (BigDecimal.ZERO.compareTo(ainrIO.getNewinst()) != 0) {
			ainrIO.setNewinst(callRounding7000(readerDTO, l2AutoIncrProcessorDTO, ainrIO.getNewinst()));
		}
		/* Write AINR record, with a type of pending. This is so */
		/* that the single thread report program which follows B5134 */
		/* can distinguish between the AINR records written by B5134 */
		/* and those written by B5137 (Auto Increases Actual). */
		ainrIO.setAintype(l2AutoIncrProcessorDTO.getWsaaSysparam03().substring(0, 4));
		readerDTO.setAinrpfInsert(ainrIO);
		l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal07(BigDecimal.ONE);
	}

	protected void updateCovr3800(L2AutoIncrReaderDTO readerDTO, L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {

		Covrpf covrincIO = null;
		Chdrpf chdrlifIO = l2AutoIncrProcessorDTO.getChdrlifIO();
		if (l2AutoIncrProcessorDTO.getCovrincIO() != null) {
			covrincIO = l2AutoIncrProcessorDTO.getCovrincIO();
		} else {
			List<Covrpf> covrincList = covrpfDAO.searchValidCovrpfRecords(readerDTO.getChdrCoy(),
					readerDTO.getChdrNum(), readerDTO.getLife(), readerDTO.getCoverage(), readerDTO.getRider());
			if (covrincList != null && !covrincList.isEmpty()) {
				for (Covrpf covrpf : covrincList) {
					if (readerDTO.getPlnSfx().equals(covrpf.getPlanSuffix())) {
						covrincIO = covrpf;
						break;
					}
				}
			}
		}

		if (covrincIO == null) {
			fatalError("covrincIO:MRNF");
		} // IJTI-462 START
		else {
			/* Update the COVR record with a valid flag of 2 and set */
			/* 'COVR-CURRTO' date to the effective date of the Increase. */
			Covrpf updateCovrincIO = new Covrpf(covrincIO);
			updateCovrincIO.setValidflag("2");
			updateCovrincIO.setCurrto(covrincIO.getCpidte());
			readerDTO.setCovrpfUpdate(updateCovrincIO);
			if (l2AutoIncrProcessorDTO.isNewContract() && l2AutoIncrProcessorDTO.getChdrlifIO() != null) {
				if (l2AutoIncrProcessorDTO.isFoundPro()) {
					int newTranno = chdrlifIO.getTranno() + l2AutoIncrProcessorDTO.getWsaaSub();
					chdrlifIO.setTranno(newTranno);
				} else {
					chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
				}
			}

			covrincIO.setValidflag("1");
			if (chdrlifIO != null) {
				covrincIO.setTranno(chdrlifIO.getTranno());
			}
			covrincIO.setCurrfrom(covrincIO.getCpidte());
			l2AutoIncrProcessorDTO.setOldCpiDate(covrincIO.getCpidte());
			covrincIO.setCurrto(99999999);
			covrincIO.setTransactionDate(l2AutoIncrProcessorDTO.getTransDate());
			covrincIO.setTransactionTime(l2AutoIncrProcessorDTO.getTransTime());
			covrincIO.setUser(getUser(l2AutoIncrProcessorDTO));
			covrincIO.setTermid("");
			covrincIO.setIndexationInd("P");
			covrincIO.setRiskprem(l2AutoIncrProcessorDTO.getPremiumrec().getRiskPrem());
			readerDTO.setCovrpfInsert(covrincIO);
			l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal06(BigDecimal.ONE);
		}
	}

	protected void rewriteChdrlifPayr3950(Payrpf payrIO, L2AutoIncrReaderDTO readerDTO,
			L2AutoIncrProcessorDTO l2AutoIncrProcessorDTO) {
		/* Update the transaction number on the PAYR file */
		/* using the transaction number from CHDRLIF. */
		Chdrpf chdrlifIO = l2AutoIncrProcessorDTO.getChdrlifIO();
		if (chdrlifIO != null) {
			payrIO.setTranno(chdrlifIO.getTranno());
		}
		readerDTO.setPayrpfUpdate(payrIO);
		l2AutoIncrProcessorDTO.getL2AutoIncrControlTotalDTO().setControlTotal03(BigDecimal.ONE);
	}

	public boolean softlock(String chdrnum, String tranCode, String bactchName) {
		/* Soft lock the contract, if it is to be processed. */
		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(chdrnum);
		slckpf.setEnttyp("CH");
		slckpf.setCompany("2");
		slckpf.setProctrancd(tranCode);
		slckpf.setUsrprf("999999");
		slckpf.setJobnm(bactchName);
		return sftlock.lockEntity(slckpf);
	}

	public int getVrcmTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		return Integer.parseInt(formatter.format(LocalTime.now()));
	}

	public static String getCobolDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		return sdf.format(new java.util.Date());
	}

	public void fatalError(String status) {
		throw new StopRunException(status);
	}
}
