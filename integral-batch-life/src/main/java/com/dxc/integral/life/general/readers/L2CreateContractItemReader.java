/**
 * 
 */
package com.dxc.integral.life.general.readers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.apache.commons.lang3.StringUtils;
import com.dxc.integral.life.beans.L2CreateContractReaderDTO;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

@StepScope
public class L2CreateContractItemReader implements ItemReader<L2CreateContractReaderDTO>{

	private static final Logger LOGGER = LoggerFactory.getLogger(L2CreateContractItemReader.class);
	
	@Autowired
	private Environment env;
	private int count = 0;
	private List<L2CreateContractReaderDTO> list ;
	private L2CreateContractReaderDTO readerDTO;
	private String regularDate;
	
	public void init() {
		 
		LOGGER.info(" L2CreateContractReaderDTO Starts ");
	}

	@Override
	public L2CreateContractReaderDTO read() throws Exception {
		
		if(list == null)
			itemReader();
		if (count < list.size()) {
			return list.get(count++);
		} else {
			list.clear();
			count = 0;
			return null;
		}
	}
		
	public List<L2CreateContractReaderDTO> itemReader(){

				File filedir = FileSystems.getDefault().getPath(env.getProperty("report.csv.input").trim()+ env.getProperty("folder.createProposal").trim()).toFile();
				File[] fileList = filedir.listFiles((dir, name) -> name.endsWith(".csv"));
				if(null !=fileList && null !=fileList[0]){
				try( InputStream inputStream =Files.newInputStream(fileList[0].toPath()); ){
		      	CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).build();
				List<String[]> allData = csvReader.readAll();
				list = new ArrayList<>(allData.size());
				for (String[] row : allData) {
					int index = 0;
					readerDTO = new L2CreateContractReaderDTO();
					index = setfieldsPart1(row, index);
					index = setfieldsPart2(row, index);
					index = setExclusion(row, index);
					index = setfieldsPart3(row, index);
					index = setfieldsPart4(row, index);
					index = setfieldsPart5(row, index);
					index = setBaseContractAndRiderCode(row, index);
					index = setContractCessAge(row,index);
					index = setPremiumCessAge(row,index);
					index = setContractCessTerm(row,index);
					index = setPremiumCessTerm(row,index);
					index = setSumInsured(row,index);
					index = setPremium(row,index);
					index = setMortalityClass(row,index);
					readerDTO.sethBNFNumbersofInsuredPerson(row[index++]);
					readerDTO.sethBNFNumbersofUnit(row[index++]);
					readerDTO.sethBNFPlanCode(row[index++]);
					index = setBeneficiaryType(row,index);
					index = setRelation(row,index);
					index = setShareType(row,index);
					index = setShare(row,index);
					index = setBeneficiaryLastNameKana(row,index);
					index = setBeneficiaryFirstNameKana(row,index);
					index = setBeneficiaryLastNameKanji(row,index);
					index = setBeneficiaryFirstNameKanji(row,index);
					index = setBeneficiaryDOB(row,index);
					index = setBeneficiaryGender(row,index);
					index = setBeneficiaryNationality(row,index);
					index = setBeneficiaryPostalCode(row,index);
					index = setBeneficiaryAddressKana(row,index);
					setbeneficiaryAddressKanji(row,index);
					
					list.add(readerDTO);
			}
		}
		catch (IOException | CsvException e) {
					LOGGER.error("Exception thrown while parsing CSV file : {}",e.getMessage().replaceAll("[\r\n]",""));
			}
			LOGGER.debug("Inside L2CreateContractReaderDTO class : Number of records to read : {}",list.size());
		}
		return list;
	}
	
	public int setfieldsPart1(String[] row,int index){
		if(index < row.length)readerDTO.setProposalNumber(row[index++]);
		if(index < row.length)readerDTO.setProposalDate(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setProposalReceivedDate(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setInsuranceTypeCode(row[index++]);
		if(index < row.length)readerDTO.setContractCountry(row[index++]);
		if(index < row.length)readerDTO.setContractCurrency(row[index++]);
		if(index < row.length)readerDTO.setBillingCurrency(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerLastNameKana(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerFirstNameKana(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerLastNameKanji(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerFirstNameKanji(row[index++]);
		return index;
	}
	
	public int setfieldsPart2(String[] row,int index){
		if(index < row.length)readerDTO.setContractOwnerDOB(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setContractOwnerGender(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerNationalityOnlyPerson(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerPostalCode(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress1Katakana(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress2Katakana(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress3Katakana(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress4Katakana(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress1Kanji(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress2Kanji(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress3Kanji(row[index++]);
		if(index < row.length)readerDTO.setContactOwnerAddress4Kanji(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerHomePhoneNumber1(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerDayPhoneNumber2(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerDayPhoneNumberCorporate(row[index++]);
		return setfieldsPart21(row, index);
	}
	public int setfieldsPart21(String[] row,int index){
		if(index < row.length)readerDTO.setContractOwnerEMailAddress(row[index++]);
		if(index < row.length)readerDTO.setContractOwnerOccupationCode(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonLastNameKana(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonFirstNameKana(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonLastNameKanji(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonFirstNameKanji(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonGender(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonDOB(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setGuardianInsuredPersonNationality(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonPostalCode(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonAddress1Kana(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonAddress1Kanji(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonPhoneNumber1(row[index++]);
		if(index < row.length)readerDTO.setGuardianInsuredPersonReasonCode(row[index++]);
		return setfieldsPart22(row, index);
	}
	public int setfieldsPart22(String[] row,int index){
		if(index < row.length)readerDTO.setGuardianContractOwnerLastNameKana(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerFirstNameKana(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerLastNameKanji(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerFirstNameKanji(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerGender(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerDOB(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setGuardianContractOwnerNationality(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerPostalCode(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerAddress1Kana(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerAddress1Kanji(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerPhoneNumber1(row[index++]);
		if(index < row.length)readerDTO.setGuardianContractOwnerReasonCode(row[index++]);
		return index;
	}
	
	public int setfieldsPart3(String[] row,int index){
		if(index < row.length)readerDTO.setBillingFrequency(row[index++]);
		if(index < row.length)readerDTO.setMethodOfPayment(row[index++]);
		if(index < row.length)readerDTO.setAccWithdrawlFactCompany(row[index++]);
		if(index < row.length)readerDTO.setFinancialInstiCDD(row[index++]);
		if(index < row.length)readerDTO.setBranchCode(row[index++]);
		if(index < row.length)readerDTO.setBankAccountType(row[index++]);
		if(index < row.length)readerDTO.setBankAccountNumber(row[index++]);
		if(index < row.length)readerDTO.setPostBankAccountType(row[index++]);
		if(index < row.length)readerDTO.setPostBankAccountNumber(row[index++]);
		if(index < row.length)readerDTO.setBankAccountHolderLastNameKana(row[index++]);
		if(index < row.length)readerDTO.setBankAccountHolderFirstNameKana(row[index++]);
		return setfieldsPart31(row,index);
	}
	public int setfieldsPart31(String[] row,int index){
		if(index < row.length)readerDTO.setContractCommencementFlg(row[index++]);
		if(index < row.length)readerDTO.setAgentNumber1(row[index++]);
		if(index < row.length)readerDTO.setRatioHFTimeofOffering1(row[index++]);
		if(index < row.length)readerDTO.setAgentNumber2(row[index++]);
		if(index < row.length)readerDTO.setRatioHFTimeofOffering2(row[index++]);
		if(index < row.length)readerDTO.setSalesChannel(row[index++]);
		return index;
	}
	public int setfieldsPart4(String[] row,int index){
		if(index < row.length)readerDTO.setDateofConfirmingIntent(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setAgencyNumber(row[index++]);
		if(index < row.length)readerDTO.setInsuredLastNameKana(row[index++]);
		if(index < row.length)readerDTO.setInsuredNameKana(row[index++]);
		if(index < row.length)readerDTO.setInsuredLastNameKanji(row[index++]);
		if(index < row.length)readerDTO.setInsuredNameKanji(row[index++]);
		if(index < row.length)readerDTO.setInsuredDateofBirth(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setInsuredGender(row[index++]);
		if(index < row.length)readerDTO.setRelationshipWithPolicyholders(row[index++]);
		if(index < row.length)readerDTO.setInsuredNationality(row[index++]);
		if(index < row.length)readerDTO.setInsuredPostalCode(row[index++]);
		return setfieldsPart41(row, index);
	}
	public int setfieldsPart41(String[] row,int index){
		if(index < row.length)readerDTO.setInsuredAddress1Kana(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress2Kana(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress3Kana(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress4Kana(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress1Kanji(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress2Kanji(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress3Kanji(row[index++]);
		if(index < row.length)readerDTO.setInsuredAddress4Kanji(row[index++]);
		if(index < row.length)readerDTO.setInsuredTelephoneNumber1(row[index++]);
		if(index < row.length)readerDTO.setInsuredTelephoneNumber2(row[index++]);
		if(index < row.length)readerDTO.setInsuredTelephoneNumberWorkPlace(row[index++]);
		if(index < row.length)readerDTO.setInsuredEmailAddress(row[index++]);
		return index;
	}
	public int setfieldsPart5(String[] row,int index){
		if(index < row.length)readerDTO.setOccupationCode(row[index++]);
		if(index < row.length)readerDTO.setPursuitCode1(row[index++]);
		if(index < row.length)readerDTO.setPursuitCode2(row[index++]);
		if(index < row.length)readerDTO.setExaminationCategory(row[index++]);
		if(index < row.length)readerDTO.setDateofDeclaration(setDateFormat(row[index++]));
		if(index < row.length)readerDTO.setSmokingInformation(row[index++]);
		if(index < row.length)readerDTO.setHeight(row[index++]);
		if(index < row.length)readerDTO.setWeight(row[index++]);
		return index;
	}
	
	
	
	
	public int setExclusion(String[] row,int index){
		List<String> exclusionReasonCode = new ArrayList<>();
		List<String> exclusionDetails = new ArrayList<>();
		for (int idx=0; idx <3; idx++){
			if(index < row.length)exclusionReasonCode.add(row[index++]);	
			if(index < row.length)exclusionDetails.add(row[index++]);
		}
		readerDTO.setExclusionReasonCode(exclusionReasonCode);
		readerDTO.setExclusionDetails(exclusionDetails);
		return index;
	}
	
	public int setBaseContractAndRiderCode(String[] row,int index){
		List<String> baseContractAndRiderCode = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)baseContractAndRiderCode.add(row[index++]);
			else baseContractAndRiderCode.add(StringUtils.SPACE);
		}
		readerDTO.setBaseContractAndRiderCode(baseContractAndRiderCode);
		return index;
	}
	
	public int setContractCessAge(String[] row,int index){
		List<String> contractCessAge = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)contractCessAge.add(row[index++]);
			else contractCessAge.add(StringUtils.SPACE);
		}
		readerDTO.setContractCessAge(contractCessAge);
		return index;
	}
	
	public int setPremiumCessAge(String[] row,int index){
		List<String> premiumCessAge = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)premiumCessAge.add(row[index++]);
			else premiumCessAge.add(StringUtils.SPACE);
		}
		readerDTO.setPremiumCessAge(premiumCessAge);
		return index;
	}
	
	public int setContractCessTerm(String[] row,int index){
		List<String> contractCessTerm = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)contractCessTerm.add(row[index++]);
			else contractCessTerm.add(StringUtils.SPACE);
		}
		readerDTO.setContractCessTerm(contractCessTerm);
		return index;
	}
	
	public int setPremiumCessTerm(String[] row,int index){
		List<String> premiumCessTerm = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)premiumCessTerm.add(row[index++]);
			else premiumCessTerm.add(StringUtils.SPACE);
		}
		readerDTO.setPremiumCessTerm(premiumCessTerm);
		return index;
	}
	
	public int setSumInsured(String[] row,int index){
		List<String> sumInsured = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)sumInsured.add(row[index++]);
			else sumInsured.add(StringUtils.SPACE);
		}
		readerDTO.setSumInsured(sumInsured);
		return index;
	}
	
	public int setPremium(String[] row,int index){
		List<String> premium = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)premium.add(row[index++]);
			else premium.add(StringUtils.SPACE);
		}
		readerDTO.setPremium(premium);
		return index;
	}
	
	public int setMortalityClass(String[] row,int index){
		List<String> mortalityClass = new ArrayList<>();
		for (int idx=0; idx < 14; idx++){
			if(index < row.length)mortalityClass.add(row[index++]);
			else mortalityClass.add(StringUtils.SPACE);
		}
		readerDTO.setMortalityClass(mortalityClass);
		return index;
	}
	public int setBeneficiaryType(String[] row,int index){
		List<String> beneficiaryType = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryType.add(row[index++]);
			else beneficiaryType.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryType(beneficiaryType);
		return index;
	}
	
	public int setRelation(String[] row,int index){
		List<String> relation = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)relation.add(row[index++]);
			else relation.add(StringUtils.SPACE);
		}
		readerDTO.setRelation(relation);
		return index;
	}
	
	public int setShareType(String[] row,int index){
		List<String> shareType = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)shareType.add(row[index++]);
			else shareType.add(StringUtils.SPACE);
		}
		readerDTO.setShareType(shareType);
		return index;
	}
	
	public int setShare(String[] row,int index){
		List<String> share = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)share.add(row[index++]);
			else share.add(StringUtils.SPACE);
		}
		readerDTO.setShare(share);
		return index;
	}
	
	public int setBeneficiaryLastNameKana(String[] row,int index){
		List<String> beneficiaryLastNameKana = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryLastNameKana.add(row[index++]);
			else beneficiaryLastNameKana.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryLastNameKana(beneficiaryLastNameKana);
		return index;
	}
	
	public int setBeneficiaryFirstNameKana(String[] row,int index){
		List<String> beneficiaryFirstNameKana = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryFirstNameKana.add(row[index++]);
			else beneficiaryFirstNameKana.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryFirstNameKana(beneficiaryFirstNameKana);
		return index;
	}
	
	public int setBeneficiaryLastNameKanji(String[] row,int index){
		List<String> beneficiaryLastNameKanji = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryLastNameKanji.add(row[index++]);
			else beneficiaryLastNameKanji.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryLastNameKanji(beneficiaryLastNameKanji);
		return index;
	}
	
	public int setBeneficiaryFirstNameKanji(String[] row,int index){
		List<String> beneficiaryFirstNameKanji = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryFirstNameKanji.add(row[index++]);
			else beneficiaryFirstNameKanji.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryFirstNameKanji(beneficiaryFirstNameKanji);
		return index;
	}
	
	public int setBeneficiaryDOB(String[] row,int index){
		List<String> beneficiaryDOB = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryDOB.add(setDateFormat(row[index++]));
			else beneficiaryDOB.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryDOB(beneficiaryDOB);
		return index;
	}
	
	public int setBeneficiaryGender(String[] row,int index){
		List<String> beneficiaryGender = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryGender.add(row[index++]);
			else beneficiaryGender.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryGender(beneficiaryGender);
		return index;
	}
	
	public int setBeneficiaryNationality(String[] row,int index){
		List<String> beneficiaryNationality = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryNationality.add(row[index++]);
			else beneficiaryNationality.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryNationality(beneficiaryNationality);
		return index;
	}
	public int setBeneficiaryPostalCode(String[] row,int index){
		List<String> beneficiaryPostalCode = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryPostalCode.add(row[index++]);
			else beneficiaryPostalCode.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryPostalCode(beneficiaryPostalCode);
		return index;
	}
	
	public int setBeneficiaryAddressKana(String[] row,int index){
		
		List<String> beneficiaryAddress1Kana = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress1Kana.add(row[index++]);
			else beneficiaryAddress1Kana.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress1Kana(beneficiaryAddress1Kana);
		List<String> beneficiaryAddress2Kana = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress2Kana.add(row[index++]);
			else beneficiaryAddress2Kana.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress2Kana(beneficiaryAddress2Kana);
		List<String> beneficiaryAddress3Kana = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress3Kana.add(row[index++]);
			else beneficiaryAddress3Kana.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress3Kana(beneficiaryAddress3Kana);
		return setBeneficiaryAddress4Kana(row,index);
	}
	
	public int setBeneficiaryAddress4Kana(String[] row,int index){
		List<String> beneficiaryAddress4Kana = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress4Kana.add(row[index++]);
			else beneficiaryAddress4Kana.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress4Kana(beneficiaryAddress4Kana);
		return index;
	}
	
	public int setbeneficiaryAddressKanji(String[] row,int index){
		
		List<String> beneficiaryAddress1Kanji = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress1Kanji.add(row[index++]);
			else beneficiaryAddress1Kanji.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress1Kanji(beneficiaryAddress1Kanji);
	
		List<String> beneficiaryAddress2Kanji = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress2Kanji.add(row[index++]);
			else beneficiaryAddress2Kanji.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress2Kanji(beneficiaryAddress2Kanji);
	
		List<String> beneficiaryAddress3Kanji = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress3Kanji.add(row[index++]);
			else beneficiaryAddress3Kanji.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress3Kanji(beneficiaryAddress3Kanji);
		return setBeneficiaryAddress4Kanji(row,index);
	}
	
	public int setBeneficiaryAddress4Kanji(String[] row,int index){
		List<String> beneficiaryAddress4Kanji = new ArrayList<>();
		for (int idx=0; idx < 10; idx++){
			if(index < row.length)beneficiaryAddress4Kanji.add(row[index++]);
			else beneficiaryAddress4Kanji.add(StringUtils.SPACE);
		}
		readerDTO.setBeneficiaryAddress4Kanji(beneficiaryAddress4Kanji);
		return index;
	}
	
	public String setDateFormat(String fileDate){
		if(null != fileDate){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date date;
			try {
				date = sdf.parse(fileDate);
				sdf.applyPattern("yyyy/MM/dd");
				regularDate = sdf.format(date);
			} catch (ParseException e) {
				regularDate = StringUtils.SPACE;
				LOGGER.error("Exception thrown while parsing Date  : {}",e.getMessage().replaceAll("[\r\n]",""));
			}
		}	
		return regularDate;
	}

	@Override
	public String toString() {
		return "L2CreateContractItemReader []";
	}
	
}	
