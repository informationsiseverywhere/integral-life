package com.dxc.integral.life.config;

import java.util.Map;

/**
 * 
 * JsonMapping for L2commongw batch step.
 *
 */
public class L2commongwJsonMapping {

	L2commongwJsonMapping() {
		super();
	}

	private Map<String, String> header;
	private Map<String, String> trailer;
	private Map<String, String> end;
	private Map<String, String> dataFormat;

	/**
	 * @return the header
	 */
	public Map<String, String> getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            the header to set
	 */
	public void setHeader(Map<String, String> header) {
		this.header = header;
	}

	/**
	 * @return the trailer
	 */
	public Map<String, String> getTrailer() {
		return trailer;
	}

	/**
	 * @param trailer
	 *            the trailer to set
	 */
	public void setTrailer(Map<String, String> trailer) {
		this.trailer = trailer;
	}

	/**
	 * @return the end
	 */
	public Map<String, String> getEnd() {
		return end;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(Map<String, String> end) {
		this.end = end;
	}

	/**
	 * @return the dataFormat
	 */
	public Map<String, String> getDataFormat() {
		return dataFormat;
	}

	/**
	 * @param dataFormat
	 *            the dataFormat to set
	 */
	public void setDataFormat(Map<String, String> dataFormat) {
		this.dataFormat = dataFormat;
	}
}
