package com.dxc.integral.life.general.processors;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.life.beans.L2RerateControlTotalDTO;
import com.dxc.integral.life.beans.L2RerateProcessorDTO;
import com.dxc.integral.life.beans.L2RerateReaderDTO;
import com.dxc.integral.life.utils.L2RerateItemProcessorUtil;

/**
 * ItemProcessor for L2POLRNWL Re-rating batch step.
 * 
 * @author yyang21
 *
 */
@Scope(value = "step")
@Async
@Lazy
public class L2RerateItemProcessor extends BaseProcessor implements ItemProcessor<L2RerateReaderDTO, L2RerateReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2RerateItemProcessor.class);
	
	@Autowired
	private L2RerateItemProcessorUtil l2RerateItemProcessorUtil;
	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	@Autowired
	private Environment env;
	@Autowired
	private L2RerateControlTotalDTO l2RerateControlTotalDTO;
	@Override
	public L2RerateReaderDTO process(L2RerateReaderDTO readerDTO) throws Exception {
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		Usrdpf usrdpf = getUserDetails(userName);
		Batcpf batchDetail = getBatcpfInfo(transactionDate,businessDate,Companies.LIFE,"B672",branch,usrdpf,batchName);
		boolean vpmsFlag = env.getProperty("vpms.flag").equals("1");
		L2RerateProcessorDTO l2RerateProcessorDTO = new L2RerateProcessorDTO();
		l2RerateProcessorDTO.setVpmsFlag(vpmsFlag);
		l2RerateProcessorDTO.setActmonth(getActMonth(transactionDate));
		l2RerateProcessorDTO.setActyear(getActYear(transactionDate));
		l2RerateProcessorDTO.setLang(usrdpf.getLanguage());
		l2RerateProcessorDTO.setPrefix(batchDetail.getBatcpfx());
		l2RerateProcessorDTO.setBatch(batchDetail.getBatcbatch());
		l2RerateProcessorDTO.setBatchTrcde(batchDetail.getBatctrcde());
		l2RerateProcessorDTO.setBatcbrn(batchDetail.getBatcbrn());
		l2RerateProcessorDTO.setChdrcoy(readerDTO.getChdrcoy());
		l2RerateProcessorDTO.setChdrnum(readerDTO.getChdrnum());
		l2RerateProcessorDTO.setUserName(userName);
		l2RerateProcessorDTO.setBatchName(batchName);
		l2RerateProcessorDTO.setTransactionDate(Integer.parseInt(getCobolDate()));
		l2RerateProcessorDTO.setTransactionTime(getVrcmTime());
		l2RerateProcessorDTO.setEffDate(Integer.parseInt(transactionDate));
		l2RerateProcessorDTO = l2RerateItemProcessorUtil.loadSmartTables(l2RerateProcessorDTO);
		l2RerateProcessorDTO.setPstatcode(readerDTO.getPstatcode());
		l2RerateProcessorDTO.setStatcode(readerDTO.getStatcode());
		l2RerateProcessorDTO.setRider(readerDTO.getRider());
		l2RerateProcessorDTO.setLife(readerDTO.getLife());
		l2RerateProcessorDTO.setCoverage(readerDTO.getCoverage());
		l2RerateProcessorDTO.setPlnsfx(readerDTO.getPlnsfx());
		l2RerateControlTotalDTO.setControlTotal01(BigDecimal.ONE);
		l2RerateProcessorDTO.setL2RerateControlTotalDTO(l2RerateControlTotalDTO);
		
		boolean chdrValid = l2RerateItemProcessorUtil.validIncrpf(l2RerateProcessorDTO);
		if (!chdrValid) {
			l2RerateControlTotalDTO.setControlTotal05(BigDecimal.ONE);
			return null;
		} else {
			l2RerateProcessorDTO = l2RerateItemProcessorUtil.validateChdrpf(l2RerateProcessorDTO);
		}
		
		boolean slkFlag = softlock(readerDTO.getChdrnum(),"B672",batchName);
		if (!slkFlag) {
			LOGGER.info("Contract is already locked, chdrnum: {}", readerDTO.getChdrnum());//IJTI-1498
			return null;
		}
		
		l2RerateProcessorDTO = l2RerateItemProcessorUtil.updateCovrAgcm(l2RerateProcessorDTO);
		l2RerateProcessorDTO = l2RerateItemProcessorUtil.updateChdrPayr(l2RerateProcessorDTO);
		l2RerateControlTotalDTO.setControlTotal04(l2RerateProcessorDTO.getL2RerateControlTotalDTO().getControlTotal04());
		l2RerateControlTotalDTO.setControlTotal02(l2RerateProcessorDTO.getL2RerateControlTotalDTO().getControlTotal02());
		l2RerateControlTotalDTO.setControlTotal03(l2RerateProcessorDTO.getL2RerateControlTotalDTO().getControlTotal03());
		if(l2RerateProcessorDTO.getAgcmInsert() != null){
			readerDTO.setAgcmInsert(l2RerateProcessorDTO.getAgcmInsert());
		}
		if(l2RerateProcessorDTO.getAgcmUpdate() != null){
			readerDTO.setAgcmUpdate(l2RerateProcessorDTO.getAgcmUpdate());
		}
		if(l2RerateProcessorDTO.getChdrpfInsert() != null){
			readerDTO.setChdrpfInsert(l2RerateProcessorDTO.getChdrpfInsert());
		}
		if(l2RerateProcessorDTO.getChdrpfUpdate() != null){
			readerDTO.setChdrpfUpdate(l2RerateProcessorDTO.getChdrpfUpdate());
		}
		if(l2RerateProcessorDTO.getPayrInsert() != null){
			readerDTO.setPayrInsert(l2RerateProcessorDTO.getPayrInsert());
		}
		if(l2RerateProcessorDTO.getPayrUpdate() != null){
			readerDTO.setPayrUpdate(l2RerateProcessorDTO.getPayrUpdate());
		}
		if(l2RerateProcessorDTO.getCovrInsert() != null){
			readerDTO.setCovrInsert(l2RerateProcessorDTO.getCovrInsert());
		}
		
		if(l2RerateProcessorDTO.getCovrUpdate() != null){
			readerDTO.setCovrUpdate(l2RerateProcessorDTO.getCovrUpdate());
		}
		
		if(l2RerateProcessorDTO.getPtrnInsert() != null){
			readerDTO.setPtrnInsert(l2RerateProcessorDTO.getPtrnInsert());
		}
		
		if(l2RerateProcessorDTO.getIncrInsert() != null){
			readerDTO.setIncrInsert(l2RerateProcessorDTO.getIncrInsert());
		}
		
		releaseSoftlock(readerDTO.getChdrcoy(),readerDTO.getChdrnum(),"B672",userName,batchName,batchDetail.getBatctrcde());
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		return readerDTO;
	}
}