package com.dxc.integral.life.beans;

import java.math.BigDecimal;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component("l2newintblControlTotalDTO")
@Lazy
public class L2newintblControlTotalDTO {

	/** The controlTotal01 */
	private BigDecimal controlTotal01 = BigDecimal.ZERO;
	/** The controlTotal02 */
	private BigDecimal controlTotal02 = BigDecimal.ZERO;
	public BigDecimal getControlTotal01() {
		return controlTotal01;
	}
	public void setControlTotal01(BigDecimal controlTotal01) {
		this.controlTotal01 = controlTotal01;
	}
	public BigDecimal getControlTotal02() {
		return controlTotal02;
	}
	public void setControlTotal02(BigDecimal controlTotal02) {
		this.controlTotal02 = controlTotal02;
	}
}
