package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.L2RerateProcessorDTO;

public interface L2RerateItemProcessorUtil {
	public boolean validIncrpf(L2RerateProcessorDTO l2RerateProcessorDTO);
	public L2RerateProcessorDTO validateChdrpf(L2RerateProcessorDTO l2RerateProcessorDTO);
	public L2RerateProcessorDTO loadSmartTables(L2RerateProcessorDTO l2RerateProcessorDTO) throws IOException;
	public L2RerateProcessorDTO updateCovrAgcm(L2RerateProcessorDTO l2RerateProcessorDTO);
	public L2RerateProcessorDTO updateChdrPayr(L2RerateProcessorDTO l2RerateProcessorDTO);
}
