package com.dxc.integral.life.general.partitioner;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.dxc.integral.life.dao.SlncpfDAO;

/**
 * Forms partition over records fetched from database.
 * Dynamically adjusts grid size based on record type in SLNCPF. 
 * 
 * @author gsaluja2
 *
 */
public class L2lincsndPartitioner implements Partitioner{

	private static final Logger LOGGER = LoggerFactory.getLogger(L2lincsndPartitioner.class);
	
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	
	@Autowired
	private SlncpfDAO slncpfDAO;
	
	private String zreclass;
	
	private List<String> slncpfZreclassRecordsList;
	
	public L2lincsndPartitioner() {
		//does nothing
	}
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		LOGGER.info("Begin partitioning at L2lincsndDataPartitioner");
		Map<String, ExecutionContext> partitionMap = new LinkedHashMap<>();
		int count;
		slncpfZreclassRecordsList = slncpfDAO.readUniqueSlncpfRecords();
		if (slncpfZreclassRecordsList.isEmpty())
			LOGGER.info(" No records in SLNCPF table for processing !! ");
		count = slncpfZreclassRecordsList.size();
		if(count == 0){
			LOGGER.info("No records found to partition");
			return partitionMap;
		}
		if(gridSize > count)
			gridSize = count;
		Iterator<String> itr = slncpfZreclassRecordsList.iterator();
		for(int i=0;i <gridSize;i++) {
			if(itr.hasNext())
				zreclass = itr.next();
			ExecutionContext exe = new ExecutionContext(stepExecution.getExecutionContext());
			exe.put("zreclass", zreclass); 
			partitionMap.put("partition"+i,exe);
			LOGGER.info("Partition- {} created for record type: {}", i, zreclass);
		}	
		LOGGER.info("End partitioning at L2lincsndDataPartitioner");
		return partitionMap;
	}
}
