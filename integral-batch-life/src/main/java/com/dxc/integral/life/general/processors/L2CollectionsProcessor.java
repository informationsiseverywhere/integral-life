package com.dxc.integral.life.general.processors;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.BatchInformBean;
import com.dxc.integral.life.beans.L2CollectionControlTotalDTO;
import com.dxc.integral.life.beans.L2CollectionReaderDTO;
import com.dxc.integral.life.beans.L2CollectionsProcessDTO;
import com.dxc.integral.life.dao.model.Hdivpf;
import com.dxc.integral.life.dao.model.Trwppf;
import com.dxc.integral.life.utils.L2CollectionsProcessUtil;
@Scope(value = "step")
@Async
public class L2CollectionsProcessor extends BaseItemProcessor<L2CollectionReaderDTO, L2CollectionReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CollectionsProcessor.class);
	
	@Autowired
	private L2CollectionsProcessUtil l2CollectionsProcessUtil;
	
	@Autowired
	private L2CollectionControlTotalDTO l2collectioncontroltotaldto;
	
	private Map<String, Integer> trannoMap;
    
	@Override
	public L2CollectionReaderDTO process(L2CollectionReaderDTO item) throws Exception {
		// init1100
		LOGGER.info("L2CollectionReaderDTO.process() start."); 
		BatchInformBean informBean = new BatchInformBean();
		informBean.setBatchDetail(this.getBatchDetail());
		informBean.setBprdpf(this.getBprdpf());
		informBean.setBsscpf(this.getBsscpf());
		informBean.setUsrdpf(this.getUsrdpf());
		informBean.setProgramName(this.getProgramName());
		LOGGER.info("L2CollectionReaderDTO init start."); 
		L2CollectionsProcessDTO processDto = l2CollectionsProcessUtil.init(item, informBean);
		LOGGER.info("L2CollectionReaderDTO init end."); 
		
		// edit
		LOGGER.info("L2CollectionReaderDTO edit start."); 
		l2CollectionsProcessUtil.edit(item, informBean, processDto, l2collectioncontroltotaldto);
		LOGGER.info("L2CollectionReaderDTO edit end.");

        // pass value to writer
        item.setWsaaToleranceAllowed(processDto.getWsaaToleranceAllowed());
        item.setWsaaAmtPaid(processDto.getWsaaAmtPaid());
        item.setWsaaNetCbillamt(processDto.getWsaaNetCbillamt());
        item.setWsaaWaiveAmtPaid(processDto.getWsaaWaiveAmtPaid());
        
        item.setWsaaCntcurrReceived(processDto.getWsaaCntcurrReceived());
        item.setWsaaT5645Cnttot(processDto.getWsaaT5645Cnttot());
        item.setWsaaT5645Sacscode(processDto.getWsaaT5645Sacscode());
        item.setWsaaT5645Sacstype(processDto.getWsaaT5645Sacstype());
        item.setWsaaT5645Sign(processDto.getWsaaT5645Sign());
        item.setWsaaT5645Glmap(processDto.getWsaaT5645Glmap());
        item.setBatchDetail(getBatchDetail());
        item.setBsscpf(getBsscpf());
        item.setBprdpf(getBprdpf());
        item.setDescpfT1688(processDto.getDescpfT1688());
//        item.setFoundPro(isFoundPro);
        if (processDto.getWsaaToleranceChk() == L2CollectionsProcessDTO.WsaaToleranceChk.toleranceLimit1) {
        	item.setWsaaToleranceChk("toleranceLimit1");
        } else {
        	item.setWsaaToleranceChk("toleranceLimit2");
        }
        item.setAgtTerminated(processDto.isAgtTerminated());
        
        item.setT5679(processDto.getT5679());
        item.setWsaaT5671Subprogs(processDto.getWsaaT5671Subprogs());
        item.setWsaaT5687Rec(processDto.getWsaaT5687Rec());
        item.setWsaaT5534UnitFreq(processDto.getWsaaT5534UnitFreq());
        item.setWsaaT5644Comsub(processDto.getWsaaT5644Comsub());
        item.setTemptot(processDto.getTemptot());
        item.setRnlallrec(processDto.getRnlallrec());
        item.setLifrtrnPojo(processDto.getLifrtrnPojo());
        
        updateChdr3100(item);
        writeTrwppf(item);
        updateHdiv3900(item);
        LOGGER.info("L2CollectionReaderDTO.process() end."); 
		return item;
	}
	
	private void updateChdr3100(L2CollectionReaderDTO item) {
		Chdrpf chdrpf = new Chdrpf();
		chdrpf.setUniqueNumber(item.getCunique_number());
		chdrpf.setInstjctl("");
		if (trannoMap == null) {
			trannoMap = new HashMap<>();
		}
		int curTranno = 0;
		if (trannoMap.containsKey(item.getChdrnum())) {
			curTranno = trannoMap.get(item.getChdrnum());
		} else {
			curTranno = item.getChdrtranno();
		}
		curTranno++;
		trannoMap.put(item.getChdrnum(), curTranno);
		item.setChdrtranno(curTranno);
		chdrpf.setTranno(item.getChdrtranno());
		chdrpf.setPtdate(item.getLinsinstto());
		chdrpf.setInstto(item.getLinsinstto());
		chdrpf.setInstfrom(item.getInstfrom());
		chdrpf.setInsttot01(item.getChdrinsttot01().add(item.getInstamt01()));
		chdrpf.setInsttot02(item.getChdrinsttot02().add(item.getInstamt02()));
		chdrpf.setInsttot03(item.getChdrinsttot03().add(item.getInstamt03()));
		chdrpf.setInsttot04(item.getChdrinsttot04().add(item.getInstamt04()));
		chdrpf.setInsttot05(item.getChdrinsttot05().add(item.getInstamt05()));
		chdrpf.setInsttot06(item.getChdrinsttot06().add(item.getInstamt06()));
		chdrpf.setInsttot01(chdrpf.getInsttot01().subtract(item.getInstamt03()));
		chdrpf.setOutstamt(item.getChdroutstamt().subtract(item.getInstamt06()));
		chdrpf.setSinstamt06(item.getInstamt01().add(item.getInstamt02())
				.add(item.getInstamt03()).add(item.getInstamt04()).add(item.getInstamt05()));
		// chdrpf.setSinstamt05(linxpfResult.getInstamt05().add(wsaaWaiverAvail.getbigdata()));
		item.setChdrpf(chdrpf);
	}
	
	private void writeTrwppf(L2CollectionReaderDTO item) {
		/* WRITE INTO TRWP FILE */
        Trwppf trwppf = new Trwppf();
        trwppf.setChdrcoy(item.getChdrcoy());
        trwppf.setChdrnum(item.getChdrnum());
        trwppf.setEffdate(DatimeUtil.DateToInt(getBsscpf().getBprceffdat()));
        trwppf.setTranno(item.getChdrtranno());
        trwppf.setValidflag("1");
        trwppf.setRdocpfx("RW");
        trwppf.setRdoccoy(batchCompany);
        trwppf.setOrigcurr(item.getBillcurr());
        trwppf.setOrigamt(item.getCbillamt());
        item.setTrwppfInsertList(trwppf);
	}
	
	private void updateHdiv3900(L2CollectionReaderDTO item) {
		/* Write a record in HDIV with negative amount to denote */
		/* cash dividend withdrawal if WSAA-DIV-REQUIRED > 0. */
		if (BigDecimal.ZERO.compareTo(item.getWsaaDivRequired()) == 0) {
			return;
		}

		Hdivpf hdivpf = new Hdivpf();
		hdivpf.setChdrcoy(item.getChdrcoy());
		hdivpf.setChdrnum(item.getChdrnum());
		hdivpf.setLife("01");
		hdivpf.setJlife("  ");
		hdivpf.setCoverage("01");
		hdivpf.setRider("00");
		hdivpf.setPlnsfx(0);
		hdivpf.setTranno(item.getChdrtranno());
		hdivpf.setEffdate(DatimeUtil.DateToInt(getBsscpf().getBprceffdat()));
		hdivpf.setHdvaldt(DatimeUtil.DateToInt(getBsscpf().getBprceffdat()));
		hdivpf.setHincapdt(item.getHdishcapndt());
		hdivpf.setCntcurr(item.getCntcurr());
		hdivpf.setHdvamt(item.getWsaaDivRequired().negate().setScale(2));
		hdivpf.setHdvrate(BigDecimal.ZERO);
		hdivpf.setHdveffdt(99999999);
		hdivpf.setBatccoy(batchCompany);
		hdivpf.setBatcbrn(getBatchDetail().getBatcbrn());
		hdivpf.setBatcactyr(getBatchDetail().getBatcactyr());
		hdivpf.setBatcactmn(getBatchDetail().getBatcactmn());
		hdivpf.setBatctrcde(getBatchDetail().getBatctrcde());
		hdivpf.setBatcbatch(getBatchDetail().getBatcbatch());
		hdivpf.setHdvtyp("C");
		hdivpf.setZdivopt(item.getHcsdzdivopt());
		hdivpf.setZcshdivmth(item.getHcsdzcshdivmth());
		hdivpf.setHdvopttx(0);
		hdivpf.setHdvcaptx(0);
		hdivpf.setHdvsmtno(0);
		hdivpf.setHpuanbr(0);
		item.setHdivInsertList(hdivpf);
	}
}
