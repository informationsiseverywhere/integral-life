package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5437 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S5437screensfl s5437screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S5437screensfl getS5437screensfl() {
		return s5437screensfl;
	}
	public void setS5437screensfl(S5437screensfl s5437screensfl) {
		this.s5437screensfl = s5437screensfl;
	}

	public static class S5437screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		public int size() {
			return rows.length;
		}

		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
		
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String cmdateDisp = "";
			private String rasnum = "";
			private String rngmnt = "";
			private String raAmount = "";
			private String ovrdind = "";
			private String select = "";

			public String getCmdateDisp() {
				return cmdateDisp;
			}
			public void setCmdateDisp(String cmdateDisp) {
				this.cmdateDisp = cmdateDisp;
			}
			public String getRasnum() {
				return rasnum;
			}
			public void setRasnum(String rasnum) {
				this.rasnum = rasnum;
			}
			public String getRngmnt() {
				return rngmnt;
			}
			public void setRngmnt(String rngmnt) {
				this.rngmnt = rngmnt;
			}
			public String getRaAmount() {
				return raAmount;
			}
			public void setRaAmount(String raAmount) {
				this.raAmount = raAmount;
			}
			public String getOvrdind() {
				return ovrdind;
			}
			public void setOvrdind(String ovrdind) {
				this.ovrdind = ovrdind;
			}
			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
		} 
	}

	@Override
	public String toString() {
		return "S5437 []";
	}
	
}
