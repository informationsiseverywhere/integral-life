package com.dxc.integral.life.beans;

import java.util.List;

public class L2CreateContractReaderDTO {
	
	String proposalNumber;
	String proposalDate;
	String proposalReceivedDate;
	String insuranceTypeCode;
	String contractCountry;
	String contractCurrency;
	String billingCurrency;
	String contractOwnerLastNameKana;
	String contractOwnerFirstNameKana;
	String contractOwnerLastNameKanji;
	String contractOwnerFirstNameKanji;
	String contractOwnerDOB;
	String contractOwnerGender;
	String contactOwnerNationalityOnlyPerson;
	String contactOwnerPostalCode;
	String contactOwnerAddress1Katakana;
	String contactOwnerAddress2Katakana;
	String contactOwnerAddress3Katakana;
	String contactOwnerAddress4Katakana;
	String contactOwnerAddress1Kanji;
	String contactOwnerAddress2Kanji;
	String contactOwnerAddress3Kanji;
	String contactOwnerAddress4Kanji;
	String contractOwnerHomePhoneNumber1;
	String contractOwnerDayPhoneNumber2;
	String contractOwnerDayPhoneNumberCorporate;
	String contractOwnerEMailAddress;
	String contractOwnerOccupationCode;
	String guardianInsuredPersonLastNameKana;
	String guardianInsuredPersonFirstNameKana;
	String guardianInsuredPersonLastNameKanji;
	String guardianInsuredPersonFirstNameKanji;
	String guardianInsuredPersonGender;
	String guardianInsuredPersonDOB;
	String guardianInsuredPersonNationality;
	String guardianInsuredPersonPostalCode;
	String guardianInsuredPersonAddress1Kana;
	String guardianInsuredPersonAddress1Kanji;
	String guardianInsuredPersonPhoneNumber1;
	String guardianInsuredPersonReasonCode;
	String guardianContractOwnerLastNameKana;
	String guardianContractOwnerFirstNameKana;
	String guardianContractOwnerLastNameKanji;
	String guardianContractOwnerFirstNameKanji;
	String guardianContractOwnerGender;
	String guardianContractOwnerDOB;
	String guardianContractOwnerNationality;
	String guardianContractOwnerPostalCode;
	String guardianContractOwnerAddress1Kana;
	String guardianContractOwnerAddress1Kanji;
	String guardianContractOwnerPhoneNumber1;
	String guardianContractOwnerReasonCode;
	List<String> exclusionReasonCode;
	List<String> exclusionDetails;
	String billingFrequency;
	String methodOfPayment;
	String accWithdrawlFactCompany;
	String financialInstiCDD;
	String branchCode;
	String bankAccountType;
	String bankAccountNumber;
	String postBankAccountType;
	String postBankAccountNumber;
	String bankAccountHolderLastNameKana;
	String bankAccountHolderFirstNameKana;
	String contractCommencementFlg;
	String agentNumber1;
	String ratioHFTimeofOffering1;
	String agentNumber2;
	String ratioHFTimeofOffering2;
	String salesChannel;
	String dateofConfirmingIntent;
	String agencyNumber;
	String insuredLastNameKana;
	String insuredNameKana;
	String insuredLastNameKanji;
	String insuredNameKanji;
	String insuredDateofBirth;
	String insuredGender;
	String relationshipWithPolicyholders;
	String insuredNationality;
	String insuredPostalCode;
	String insuredAddress1Kana;
	String insuredAddress2Kana;
	String insuredAddress3Kana;
	String insuredAddress4Kana;
	String insuredAddress1Kanji;
	String insuredAddress2Kanji;
	String insuredAddress3Kanji;
	String insuredAddress4Kanji;
	String insuredTelephoneNumber1;
	String insuredTelephoneNumber2;
	String insuredTelephoneNumberWorkPlace;
	String insuredEmailAddress;
	String occupationCode;
	String pursuitCode1;
	String pursuitCode2;
	String examinationCategory;
	String dateofDeclaration;
	String smokingInformation;
	String height;
	String weight;
	List<String> baseContractAndRiderCode;
	List<String> contractCessAge;
	List<String> premiumCessAge;
	List<String> contractCessTerm;
	List<String> premiumCessTerm;
	List<String> sumInsured;
	List<String> premium;
	List<String> mortalityClass;
	String hBNFNumbersofInsuredPerson;
	String hBNFNumbersofUnit;
	String hBNFPlanCode;
	List<String> beneficiaryType;
	List<String> relation;
	List<String> shareType;
	List<String> share;
	List<String> beneficiaryLastNameKana;
	List<String> beneficiaryFirstNameKana;
	List<String> beneficiaryLastNameKanji;
	List<String> beneficiaryFirstNameKanji;
	List<String> beneficiaryDOB;
	List<String> beneficiaryGender;
	List<String> beneficiaryNationality;
	List<String> beneficiaryPostalCode;
	List<String> beneficiaryAddress1Kana;
	List<String> beneficiaryAddress2Kana;
	List<String> beneficiaryAddress3Kana;
	List<String> beneficiaryAddress4Kana;
	List<String> beneficiaryAddress1Kanji;
	List<String> beneficiaryAddress2Kanji;
	List<String> beneficiaryAddress3Kanji;
	List<String> beneficiaryAddress4Kanji;
	
	public String getProposalNumber() {
		return proposalNumber;
	}
	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}
	public String getProposalDate() {
		return proposalDate;
	}
	public void setProposalDate(String proposalDate) {
		this.proposalDate = proposalDate;
	}
	public String getProposalReceivedDate() {
		return proposalReceivedDate;
	}
	public void setProposalReceivedDate(String proposalReceivedDate) {
		this.proposalReceivedDate = proposalReceivedDate;
	}
	public String getInsuranceTypeCode() {
		return insuranceTypeCode;
	}
	public void setInsuranceTypeCode(String insuranceTypeCode) {
		this.insuranceTypeCode = insuranceTypeCode;
	}
	public String getContractCountry() {
		return contractCountry;
	}
	public void setContractCountry(String contractCountry) {
		this.contractCountry = contractCountry;
	}
	public String getContractCurrency() {
		return contractCurrency;
	}
	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	public String getBillingCurrency() {
		return billingCurrency;
	}
	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}
	public String getContractOwnerLastNameKana() {
		return contractOwnerLastNameKana;
	}
	public void setContractOwnerLastNameKana(String contractOwnerLastNameKana) {
		this.contractOwnerLastNameKana = contractOwnerLastNameKana;
	}
	public String getContractOwnerFirstNameKana() {
		return contractOwnerFirstNameKana;
	}
	public void setContractOwnerFirstNameKana(String contractOwnerFirstNameKana) {
		this.contractOwnerFirstNameKana = contractOwnerFirstNameKana;
	}
	public String getContractOwnerLastNameKanji() {
		return contractOwnerLastNameKanji;
	}
	public void setContractOwnerLastNameKanji(String contractOwnerLastNameKanji) {
		this.contractOwnerLastNameKanji = contractOwnerLastNameKanji;
	}
	public String getContractOwnerFirstNameKanji() {
		return contractOwnerFirstNameKanji;
	}
	public void setContractOwnerFirstNameKanji(String contractOwnerFirstNameKanji) {
		this.contractOwnerFirstNameKanji = contractOwnerFirstNameKanji;
	}
	public String getContractOwnerDOB() {
		return contractOwnerDOB;
	}
	public void setContractOwnerDOB(String contractOwnerDOB) {
		this.contractOwnerDOB = contractOwnerDOB;
	}
	public String getContractOwnerGender() {
		return contractOwnerGender;
	}
	public void setContractOwnerGender(String contractOwnerGender) {
		this.contractOwnerGender = contractOwnerGender;
	}
	public String getContactOwnerNationalityOnlyPerson() {
		return contactOwnerNationalityOnlyPerson;
	}
	public void setContactOwnerNationalityOnlyPerson(String contactOwnerNationalityOnlyPerson) {
		this.contactOwnerNationalityOnlyPerson = contactOwnerNationalityOnlyPerson;
	}
	public String getContactOwnerPostalCode() {
		return contactOwnerPostalCode;
	}
	public void setContactOwnerPostalCode(String contactOwnerPostalCode) {
		this.contactOwnerPostalCode = contactOwnerPostalCode;
	}
	public String getContactOwnerAddress1Katakana() {
		return contactOwnerAddress1Katakana;
	}
	public void setContactOwnerAddress1Katakana(String contactOwnerAddress1Katakana) {
		this.contactOwnerAddress1Katakana = contactOwnerAddress1Katakana;
	}
	public String getContactOwnerAddress2Katakana() {
		return contactOwnerAddress2Katakana;
	}
	public void setContactOwnerAddress2Katakana(String contactOwnerAddress2Katakana) {
		this.contactOwnerAddress2Katakana = contactOwnerAddress2Katakana;
	}
	public String getContactOwnerAddress3Katakana() {
		return contactOwnerAddress3Katakana;
	}
	public void setContactOwnerAddress3Katakana(String contactOwnerAddress3Katakana) {
		this.contactOwnerAddress3Katakana = contactOwnerAddress3Katakana;
	}
	public String getContactOwnerAddress4Katakana() {
		return contactOwnerAddress4Katakana;
	}
	public void setContactOwnerAddress4Katakana(String contactOwnerAddress4Katakana) {
		this.contactOwnerAddress4Katakana = contactOwnerAddress4Katakana;
	}
	public String getContactOwnerAddress1Kanji() {
		return contactOwnerAddress1Kanji;
	}
	public void setContactOwnerAddress1Kanji(String contactOwnerAddress1Kanji) {
		this.contactOwnerAddress1Kanji = contactOwnerAddress1Kanji;
	}
	public String getContactOwnerAddress2Kanji() {
		return contactOwnerAddress2Kanji;
	}
	public void setContactOwnerAddress2Kanji(String contactOwnerAddress2Kanji) {
		this.contactOwnerAddress2Kanji = contactOwnerAddress2Kanji;
	}
	public String getContactOwnerAddress3Kanji() {
		return contactOwnerAddress3Kanji;
	}
	public void setContactOwnerAddress3Kanji(String contactOwnerAddress3Kanji) {
		this.contactOwnerAddress3Kanji = contactOwnerAddress3Kanji;
	}
	public String getContactOwnerAddress4Kanji() {
		return contactOwnerAddress4Kanji;
	}
	public void setContactOwnerAddress4Kanji(String contactOwnerAddress4Kanji) {
		this.contactOwnerAddress4Kanji = contactOwnerAddress4Kanji;
	}
	public String getContractOwnerHomePhoneNumber1() {
		return contractOwnerHomePhoneNumber1;
	}
	public void setContractOwnerHomePhoneNumber1(String contractOwnerHomePhoneNumber1) {
		this.contractOwnerHomePhoneNumber1 = contractOwnerHomePhoneNumber1;
	}
	public String getContractOwnerDayPhoneNumber2() {
		return contractOwnerDayPhoneNumber2;
	}
	public void setContractOwnerDayPhoneNumber2(String contractOwnerDayPhoneNumber2) {
		this.contractOwnerDayPhoneNumber2 = contractOwnerDayPhoneNumber2;
	}
	public String getContractOwnerDayPhoneNumberCorporate() {
		return contractOwnerDayPhoneNumberCorporate;
	}
	public void setContractOwnerDayPhoneNumberCorporate(String contractOwnerDayPhoneNumberCorporate) {
		this.contractOwnerDayPhoneNumberCorporate = contractOwnerDayPhoneNumberCorporate;
	}
	public String getContractOwnerEMailAddress() {
		return contractOwnerEMailAddress;
	}
	public void setContractOwnerEMailAddress(String contractOwnerEMailAddress) {
		this.contractOwnerEMailAddress = contractOwnerEMailAddress;
	}
	public String getContractOwnerOccupationCode() {
		return contractOwnerOccupationCode;
	}
	public void setContractOwnerOccupationCode(String contractOwnerOccupationCode) {
		this.contractOwnerOccupationCode = contractOwnerOccupationCode;
	}
	public String getGuardianInsuredPersonLastNameKana() {
		return guardianInsuredPersonLastNameKana;
	}
	public void setGuardianInsuredPersonLastNameKana(String guardianInsuredPersonLastNameKana) {
		this.guardianInsuredPersonLastNameKana = guardianInsuredPersonLastNameKana;
	}
	public String getGuardianInsuredPersonFirstNameKana() {
		return guardianInsuredPersonFirstNameKana;
	}
	public void setGuardianInsuredPersonFirstNameKana(String guardianInsuredPersonFirstNameKana) {
		this.guardianInsuredPersonFirstNameKana = guardianInsuredPersonFirstNameKana;
	}
	public String getGuardianInsuredPersonLastNameKanji() {
		return guardianInsuredPersonLastNameKanji;
	}
	public void setGuardianInsuredPersonLastNameKanji(String guardianInsuredPersonLastNameKanji) {
		this.guardianInsuredPersonLastNameKanji = guardianInsuredPersonLastNameKanji;
	}
	public String getGuardianInsuredPersonFirstNameKanji() {
		return guardianInsuredPersonFirstNameKanji;
	}
	public void setGuardianInsuredPersonFirstNameKanji(String guardianInsuredPersonFirstNameKanji) {
		this.guardianInsuredPersonFirstNameKanji = guardianInsuredPersonFirstNameKanji;
	}
	public String getGuardianInsuredPersonGender() {
		return guardianInsuredPersonGender;
	}
	public void setGuardianInsuredPersonGender(String guardianInsuredPersonGender) {
		this.guardianInsuredPersonGender = guardianInsuredPersonGender;
	}
	public String getGuardianInsuredPersonDOB() {
		return guardianInsuredPersonDOB;
	}
	public void setGuardianInsuredPersonDOB(String guardianInsuredPersonDOB) {
		this.guardianInsuredPersonDOB = guardianInsuredPersonDOB;
	}
	public String getGuardianInsuredPersonNationality() {
		return guardianInsuredPersonNationality;
	}
	public void setGuardianInsuredPersonNationality(String guardianInsuredPersonNationality) {
		this.guardianInsuredPersonNationality = guardianInsuredPersonNationality;
	}
	public String getGuardianInsuredPersonPostalCode() {
		return guardianInsuredPersonPostalCode;
	}
	public void setGuardianInsuredPersonPostalCode(String guardianInsuredPersonPostalCode) {
		this.guardianInsuredPersonPostalCode = guardianInsuredPersonPostalCode;
	}
	public String getGuardianInsuredPersonAddress1Kana() {
		return guardianInsuredPersonAddress1Kana;
	}
	public void setGuardianInsuredPersonAddress1Kana(String guardianInsuredPersonAddress1Kana) {
		this.guardianInsuredPersonAddress1Kana = guardianInsuredPersonAddress1Kana;
	}
	public String getGuardianInsuredPersonAddress1Kanji() {
		return guardianInsuredPersonAddress1Kanji;
	}
	public void setGuardianInsuredPersonAddress1Kanji(String guardianInsuredPersonAddress1Kanji) {
		this.guardianInsuredPersonAddress1Kanji = guardianInsuredPersonAddress1Kanji;
	}
	public String getGuardianInsuredPersonPhoneNumber1() {
		return guardianInsuredPersonPhoneNumber1;
	}
	public void setGuardianInsuredPersonPhoneNumber1(String guardianInsuredPersonPhoneNumber1) {
		this.guardianInsuredPersonPhoneNumber1 = guardianInsuredPersonPhoneNumber1;
	}
	public String getGuardianInsuredPersonReasonCode() {
		return guardianInsuredPersonReasonCode;
	}
	public void setGuardianInsuredPersonReasonCode(String guardianInsuredPersonReasonCode) {
		this.guardianInsuredPersonReasonCode = guardianInsuredPersonReasonCode;
	}
	public String getGuardianContractOwnerLastNameKana() {
		return guardianContractOwnerLastNameKana;
	}
	public void setGuardianContractOwnerLastNameKana(String guardianContractOwnerLastNameKana) {
		this.guardianContractOwnerLastNameKana = guardianContractOwnerLastNameKana;
	}
	public String getGuardianContractOwnerFirstNameKana() {
		return guardianContractOwnerFirstNameKana;
	}
	public void setGuardianContractOwnerFirstNameKana(String guardianContractOwnerFirstNameKana) {
		this.guardianContractOwnerFirstNameKana = guardianContractOwnerFirstNameKana;
	}
	public String getGuardianContractOwnerLastNameKanji() {
		return guardianContractOwnerLastNameKanji;
	}
	public void setGuardianContractOwnerLastNameKanji(String guardianContractOwnerLastNameKanji) {
		this.guardianContractOwnerLastNameKanji = guardianContractOwnerLastNameKanji;
	}
	public String getGuardianContractOwnerFirstNameKanji() {
		return guardianContractOwnerFirstNameKanji;
	}
	public void setGuardianContractOwnerFirstNameKanji(String guardianContractOwnerFirstNameKanji) {
		this.guardianContractOwnerFirstNameKanji = guardianContractOwnerFirstNameKanji;
	}
	public String getGuardianContractOwnerGender() {
		return guardianContractOwnerGender;
	}
	public void setGuardianContractOwnerGender(String guardianContractOwnerGender) {
		this.guardianContractOwnerGender = guardianContractOwnerGender;
	}
	public String getGuardianContractOwnerDOB() {
		return guardianContractOwnerDOB;
	}
	public void setGuardianContractOwnerDOB(String guardianContractOwnerDOB) {
		this.guardianContractOwnerDOB = guardianContractOwnerDOB;
	}
	public String getGuardianContractOwnerNationality() {
		return guardianContractOwnerNationality;
	}
	public void setGuardianContractOwnerNationality(String guardianContractOwnerNationality) {
		this.guardianContractOwnerNationality = guardianContractOwnerNationality;
	}
	public String getGuardianContractOwnerPostalCode() {
		return guardianContractOwnerPostalCode;
	}
	public void setGuardianContractOwnerPostalCode(String guardianContractOwnerPostalCode) {
		this.guardianContractOwnerPostalCode = guardianContractOwnerPostalCode;
	}
	public String getGuardianContractOwnerAddress1Kana() {
		return guardianContractOwnerAddress1Kana;
	}
	public void setGuardianContractOwnerAddress1Kana(String guardianContractOwnerAddress1Kana) {
		this.guardianContractOwnerAddress1Kana = guardianContractOwnerAddress1Kana;
	}
	public String getGuardianContractOwnerAddress1Kanji() {
		return guardianContractOwnerAddress1Kanji;
	}
	public void setGuardianContractOwnerAddress1Kanji(String guardianContractOwnerAddress1Kanji) {
		this.guardianContractOwnerAddress1Kanji = guardianContractOwnerAddress1Kanji;
	}
	public String getGuardianContractOwnerPhoneNumber1() {
		return guardianContractOwnerPhoneNumber1;
	}
	public void setGuardianContractOwnerPhoneNumber1(String guardianContractOwnerPhoneNumber1) {
		this.guardianContractOwnerPhoneNumber1 = guardianContractOwnerPhoneNumber1;
	}
	public String getGuardianContractOwnerReasonCode() {
		return guardianContractOwnerReasonCode;
	}
	public void setGuardianContractOwnerReasonCode(String guardianContractOwnerReasonCode) {
		this.guardianContractOwnerReasonCode = guardianContractOwnerReasonCode;
	}
	public List<String> getExclusionReasonCode() {
		return exclusionReasonCode;
	}
	public void setExclusionReasonCode(List<String> exclusionReasonCode) {
		this.exclusionReasonCode = exclusionReasonCode;
	}
	public List<String> getExclusionDetails() {
		return exclusionDetails;
	}
	public void setExclusionDetails(List<String> exclusionDetails) {
		this.exclusionDetails = exclusionDetails;
	}
	public String getBillingFrequency() {
		return billingFrequency;
	}
	public void setBillingFrequency(String billingFrequency) {
		this.billingFrequency = billingFrequency;
	}
	public String getMethodOfPayment() {
		return methodOfPayment;
	}
	public void setMethodOfPayment(String methodOfPayment) {
		this.methodOfPayment = methodOfPayment;
	}
	public String getAccWithdrawlFactCompany() {
		return accWithdrawlFactCompany;
	}
	public void setAccWithdrawlFactCompany(String accWithdrawlFactCompany) {
		this.accWithdrawlFactCompany = accWithdrawlFactCompany;
	}
	public String getFinancialInstiCDD() {
		return financialInstiCDD;
	}
	public void setFinancialInstiCDD(String financialInstiCDD) {
		this.financialInstiCDD = financialInstiCDD;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBankAccountType() {
		return bankAccountType;
	}
	public void setBankAccountType(String bankAccountType) {
		this.bankAccountType = bankAccountType;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getPostBankAccountType() {
		return postBankAccountType;
	}
	public void setPostBankAccountType(String postBankAccountType) {
		this.postBankAccountType = postBankAccountType;
	}
	public String getPostBankAccountNumber() {
		return postBankAccountNumber;
	}
	public void setPostBankAccountNumber(String postBankAccountNumber) {
		this.postBankAccountNumber = postBankAccountNumber;
	}
	public String getBankAccountHolderLastNameKana() {
		return bankAccountHolderLastNameKana;
	}
	public void setBankAccountHolderLastNameKana(String bankAccountHolderLastNameKana) {
		this.bankAccountHolderLastNameKana = bankAccountHolderLastNameKana;
	}
	public String getBankAccountHolderFirstNameKana() {
		return bankAccountHolderFirstNameKana;
	}
	public void setBankAccountHolderFirstNameKana(String bankAccountHolderFirstNameKana) {
		this.bankAccountHolderFirstNameKana = bankAccountHolderFirstNameKana;
	}
	public String getContractCommencementFlg() {
		return contractCommencementFlg;
	}
	public void setContractCommencementFlg(String contractCommencementFlg) {
		this.contractCommencementFlg = contractCommencementFlg;
	}
	public String getAgentNumber1() {
		return agentNumber1;
	}
	public void setAgentNumber1(String agentNumber1) {
		this.agentNumber1 = agentNumber1;
	}
	public String getRatioHFTimeofOffering1() {
		return ratioHFTimeofOffering1;
	}
	public void setRatioHFTimeofOffering1(String ratioHFTimeofOffering1) {
		this.ratioHFTimeofOffering1 = ratioHFTimeofOffering1;
	}
	public String getAgentNumber2() {
		return agentNumber2;
	}
	public void setAgentNumber2(String agentNumber2) {
		this.agentNumber2 = agentNumber2;
	}
	public String getRatioHFTimeofOffering2() {
		return ratioHFTimeofOffering2;
	}
	public void setRatioHFTimeofOffering2(String ratioHFTimeofOffering2) {
		this.ratioHFTimeofOffering2 = ratioHFTimeofOffering2;
	}
	public String getSalesChannel() {
		return salesChannel;
	}
	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}
	public String getDateofConfirmingIntent() {
		return dateofConfirmingIntent;
	}
	public void setDateofConfirmingIntent(String dateofConfirmingIntent) {
		this.dateofConfirmingIntent = dateofConfirmingIntent;
	}
	public String getAgencyNumber() {
		return agencyNumber;
	}
	public void setAgencyNumber(String agencyNumber) {
		this.agencyNumber = agencyNumber;
	}
	public String getInsuredLastNameKana() {
		return insuredLastNameKana;
	}
	public void setInsuredLastNameKana(String insuredLastNameKana) {
		this.insuredLastNameKana = insuredLastNameKana;
	}
	public String getInsuredNameKana() {
		return insuredNameKana;
	}
	public void setInsuredNameKana(String insuredNameKana) {
		this.insuredNameKana = insuredNameKana;
	}
	public String getInsuredLastNameKanji() {
		return insuredLastNameKanji;
	}
	public void setInsuredLastNameKanji(String insuredLastNameKanji) {
		this.insuredLastNameKanji = insuredLastNameKanji;
	}
	public String getInsuredNameKanji() {
		return insuredNameKanji;
	}
	public void setInsuredNameKanji(String insuredNameKanji) {
		this.insuredNameKanji = insuredNameKanji;
	}
	public String getInsuredDateofBirth() {
		return insuredDateofBirth;
	}
	public void setInsuredDateofBirth(String insuredDateofBirth) {
		this.insuredDateofBirth = insuredDateofBirth;
	}
	public String getInsuredGender() {
		return insuredGender;
	}
	public void setInsuredGender(String insuredGender) {
		this.insuredGender = insuredGender;
	}
	public String getRelationshipWithPolicyholders() {
		return relationshipWithPolicyholders;
	}
	public void setRelationshipWithPolicyholders(String relationshipWithPolicyholders) {
		this.relationshipWithPolicyholders = relationshipWithPolicyholders;
	}
	public String getInsuredNationality() {
		return insuredNationality;
	}
	public void setInsuredNationality(String insuredNationality) {
		this.insuredNationality = insuredNationality;
	}
	public String getInsuredPostalCode() {
		return insuredPostalCode;
	}
	public void setInsuredPostalCode(String insuredPostalCode) {
		this.insuredPostalCode = insuredPostalCode;
	}
	public String getInsuredAddress1Kana() {
		return insuredAddress1Kana;
	}
	public void setInsuredAddress1Kana(String insuredAddress1Kana) {
		this.insuredAddress1Kana = insuredAddress1Kana;
	}
	public String getInsuredAddress2Kana() {
		return insuredAddress2Kana;
	}
	public void setInsuredAddress2Kana(String insuredAddress2Kana) {
		this.insuredAddress2Kana = insuredAddress2Kana;
	}
	public String getInsuredAddress3Kana() {
		return insuredAddress3Kana;
	}
	public void setInsuredAddress3Kana(String insuredAddress3Kana) {
		this.insuredAddress3Kana = insuredAddress3Kana;
	}
	public String getInsuredAddress4Kana() {
		return insuredAddress4Kana;
	}
	public void setInsuredAddress4Kana(String insuredAddress4Kana) {
		this.insuredAddress4Kana = insuredAddress4Kana;
	}
	public String getInsuredAddress1Kanji() {
		return insuredAddress1Kanji;
	}
	public void setInsuredAddress1Kanji(String insuredAddress1Kanji) {
		this.insuredAddress1Kanji = insuredAddress1Kanji;
	}
	public String getInsuredAddress2Kanji() {
		return insuredAddress2Kanji;
	}
	public void setInsuredAddress2Kanji(String insuredAddress2Kanji) {
		this.insuredAddress2Kanji = insuredAddress2Kanji;
	}
	public String getInsuredAddress3Kanji() {
		return insuredAddress3Kanji;
	}
	public void setInsuredAddress3Kanji(String insuredAddress3Kanji) {
		this.insuredAddress3Kanji = insuredAddress3Kanji;
	}
	public String getInsuredAddress4Kanji() {
		return insuredAddress4Kanji;
	}
	public void setInsuredAddress4Kanji(String insuredAddress4Kanji) {
		this.insuredAddress4Kanji = insuredAddress4Kanji;
	}
	public String getInsuredTelephoneNumber1() {
		return insuredTelephoneNumber1;
	}
	public void setInsuredTelephoneNumber1(String insuredTelephoneNumber1) {
		this.insuredTelephoneNumber1 = insuredTelephoneNumber1;
	}
	public String getInsuredTelephoneNumber2() {
		return insuredTelephoneNumber2;
	}
	public void setInsuredTelephoneNumber2(String insuredTelephoneNumber2) {
		this.insuredTelephoneNumber2 = insuredTelephoneNumber2;
	}
	public String getInsuredTelephoneNumberWorkPlace() {
		return insuredTelephoneNumberWorkPlace;
	}
	public void setInsuredTelephoneNumberWorkPlace(String insuredTelephoneNumberWorkPlace) {
		this.insuredTelephoneNumberWorkPlace = insuredTelephoneNumberWorkPlace;
	}
	public String getInsuredEmailAddress() {
		return insuredEmailAddress;
	}
	public void setInsuredEmailAddress(String insuredEmailAddress) {
		this.insuredEmailAddress = insuredEmailAddress;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getPursuitCode1() {
		return pursuitCode1;
	}
	public void setPursuitCode1(String pursuitCode1) {
		this.pursuitCode1 = pursuitCode1;
	}
	public String getPursuitCode2() {
		return pursuitCode2;
	}
	public void setPursuitCode2(String pursuitCode2) {
		this.pursuitCode2 = pursuitCode2;
	}
	public String getExaminationCategory() {
		return examinationCategory;
	}
	public void setExaminationCategory(String examinationCategory) {
		this.examinationCategory = examinationCategory;
	}
	public String getDateofDeclaration() {
		return dateofDeclaration;
	}
	public void setDateofDeclaration(String dateofDeclaration) {
		this.dateofDeclaration = dateofDeclaration;
	}
	public String getSmokingInformation() {
		return smokingInformation;
	}
	public void setSmokingInformation(String smokingInformation) {
		this.smokingInformation = smokingInformation;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String gethBNFNumbersofInsuredPerson() {
		return hBNFNumbersofInsuredPerson;
	}
	public void sethBNFNumbersofInsuredPerson(String hBNFNumbersofInsuredPerson) {
		this.hBNFNumbersofInsuredPerson = hBNFNumbersofInsuredPerson;
	}
	public String gethBNFNumbersofUnit() {
		return hBNFNumbersofUnit;
	}
	public void sethBNFNumbersofUnit(String hBNFNumbersofUnit) {
		this.hBNFNumbersofUnit = hBNFNumbersofUnit;
	}
	public String gethBNFPlanCode() {
		return hBNFPlanCode;
	}
	public void sethBNFPlanCode(String hBNFPlanCode) {
		this.hBNFPlanCode = hBNFPlanCode;
	}
	public List<String> getBaseContractAndRiderCode() {
		return baseContractAndRiderCode;
	}
	public void setBaseContractAndRiderCode(List<String> baseContractAndRiderCode) {
		this.baseContractAndRiderCode = baseContractAndRiderCode;
	}
	public List<String> getContractCessAge() {
		return contractCessAge;
	}
	public void setContractCessAge(List<String> contractCessAge) {
		this.contractCessAge = contractCessAge;
	}
	public List<String> getPremiumCessAge() {
		return premiumCessAge;
	}
	public void setPremiumCessAge(List<String> premiumCessAge) {
		this.premiumCessAge = premiumCessAge;
	}
	public List<String> getContractCessTerm() {
		return contractCessTerm;
	}
	public void setContractCessTerm(List<String> contractCessTerm) {
		this.contractCessTerm = contractCessTerm;
	}
	public List<String> getPremiumCessTerm() {
		return premiumCessTerm;
	}
	public void setPremiumCessTerm(List<String> premiumCessTerm) {
		this.premiumCessTerm = premiumCessTerm;
	}
	public List<String> getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(List<String> sumInsured) {
		this.sumInsured = sumInsured;
	}
	public List<String> getPremium() {
		return premium;
	}
	public void setPremium(List<String> premium) {
		this.premium = premium;
	}
	public List<String> getMortalityClass() {
		return mortalityClass;
	}
	public void setMortalityClass(List<String> mortalityClass) {
		this.mortalityClass = mortalityClass;
	}
	public List<String> getBeneficiaryType() {
		return beneficiaryType;
	}
	public void setBeneficiaryType(List<String> beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}
	public List<String> getRelation() {
		return relation;
	}
	public void setRelation(List<String> relation) {
		this.relation = relation;
	}
	public List<String> getShareType() {
		return shareType;
	}
	public void setShareType(List<String> shareType) {
		this.shareType = shareType;
	}
	public List<String> getShare() {
		return share;
	}
	public void setShare(List<String> share) {
		this.share = share;
	}
	public List<String> getBeneficiaryLastNameKana() {
		return beneficiaryLastNameKana;
	}
	public void setBeneficiaryLastNameKana(List<String> beneficiaryLastNameKana) {
		this.beneficiaryLastNameKana = beneficiaryLastNameKana;
	}
	public List<String> getBeneficiaryFirstNameKana() {
		return beneficiaryFirstNameKana;
	}
	public void setBeneficiaryFirstNameKana(List<String> beneficiaryFirstNameKana) {
		this.beneficiaryFirstNameKana = beneficiaryFirstNameKana;
	}
	public List<String> getBeneficiaryLastNameKanji() {
		return beneficiaryLastNameKanji;
	}
	public void setBeneficiaryLastNameKanji(List<String> beneficiaryLastNameKanji) {
		this.beneficiaryLastNameKanji = beneficiaryLastNameKanji;
	}
	public List<String> getBeneficiaryFirstNameKanji() {
		return beneficiaryFirstNameKanji;
	}
	public void setBeneficiaryFirstNameKanji(List<String> beneficiaryFirstNameKanji) {
		this.beneficiaryFirstNameKanji = beneficiaryFirstNameKanji;
	}
	public List<String> getBeneficiaryDOB() {
		return beneficiaryDOB;
	}
	public void setBeneficiaryDOB(List<String> beneficiaryDOB) {
		this.beneficiaryDOB = beneficiaryDOB;
	}
	public List<String> getBeneficiaryGender() {
		return beneficiaryGender;
	}
	public void setBeneficiaryGender(List<String> beneficiaryGender) {
		this.beneficiaryGender = beneficiaryGender;
	}
	public List<String> getBeneficiaryNationality() {
		return beneficiaryNationality;
	}
	public void setBeneficiaryNationality(List<String> beneficiaryNationality) {
		this.beneficiaryNationality = beneficiaryNationality;
	}
	public List<String> getBeneficiaryPostalCode() {
		return beneficiaryPostalCode;
	}
	public void setBeneficiaryPostalCode(List<String> beneficiaryPostalCode) {
		this.beneficiaryPostalCode = beneficiaryPostalCode;
	}
	public List<String> getBeneficiaryAddress1Kana() {
		return beneficiaryAddress1Kana;
	}
	public void setBeneficiaryAddress1Kana(List<String> beneficiaryAddress1Kana) {
		this.beneficiaryAddress1Kana = beneficiaryAddress1Kana;
	}
	public List<String> getBeneficiaryAddress2Kana() {
		return beneficiaryAddress2Kana;
	}
	public void setBeneficiaryAddress2Kana(List<String> beneficiaryAddress2Kana) {
		this.beneficiaryAddress2Kana = beneficiaryAddress2Kana;
	}
	public List<String> getBeneficiaryAddress3Kana() {
		return beneficiaryAddress3Kana;
	}
	public void setBeneficiaryAddress3Kana(List<String> beneficiaryAddress3Kana) {
		this.beneficiaryAddress3Kana = beneficiaryAddress3Kana;
	}
	public List<String> getBeneficiaryAddress4Kana() {
		return beneficiaryAddress4Kana;
	}
	public void setBeneficiaryAddress4Kana(List<String> beneficiaryAddress4Kana) {
		this.beneficiaryAddress4Kana = beneficiaryAddress4Kana;
	}
	public List<String> getBeneficiaryAddress1Kanji() {
		return beneficiaryAddress1Kanji;
	}
	public void setBeneficiaryAddress1Kanji(List<String> beneficiaryAddress1Kanji) {
		this.beneficiaryAddress1Kanji = beneficiaryAddress1Kanji;
	}
	public List<String> getBeneficiaryAddress2Kanji() {
		return beneficiaryAddress2Kanji;
	}
	public void setBeneficiaryAddress2Kanji(List<String> beneficiaryAddress2Kanji) {
		this.beneficiaryAddress2Kanji = beneficiaryAddress2Kanji;
	}
	public List<String> getBeneficiaryAddress3Kanji() {
		return beneficiaryAddress3Kanji;
	}
	public void setBeneficiaryAddress3Kanji(List<String> beneficiaryAddress3Kanji) {
		this.beneficiaryAddress3Kanji = beneficiaryAddress3Kanji;
	}
	public List<String> getBeneficiaryAddress4Kanji() {
		return beneficiaryAddress4Kanji;
	}
	public void setBeneficiaryAddress4Kanji(List<String> beneficiaryAddress4Kanji) {
		this.beneficiaryAddress4Kanji = beneficiaryAddress4Kanji;
	}
	@Override
	public String toString() {
		return "L2CreateContractReaderDTO []";
	}

}