package com.dxc.integral.life.utils.impl;



import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.csc.dip.jvpms.json.cmd.HttpRequest;
import com.dxc.integral.fsu.general.utilities.CommonUtility;
import com.dxc.integral.life.beans.AssetMgmtOutput;
import com.dxc.integral.life.beans.L2asstmgmtReaderDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author sagrawal35
 * Integrate Integral-VPMS for Asset Management Data Processing
 */
@Component
public class AssetMgmtVPMSProcessHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AssetMgmtVPMSProcessHelper.class);	
	
	private static final String SETVAR2 = "[\"!\", \"Input TransEffDate\", ";
	private static final String SETVAR3 = "[\"!\", \"Input Calling System\", ";
	private static final String SETVAR4 = "[\"!\", \"Input Contract Type\", ";
	private static final String SETVAR5 = "[\"!\", \"Input Region\", ";
	private static final String SETVAR6 = "[\"!\", \"Input Locale\", ";
	private static final String SETVAR7 = "[\"!\", \"Input Language\", ";	
	private static final String SETVAR8 = "[\"!\", \"Input Currency\", ";	
	private static final String OUTPUT_ASSET_BASED_COMMISSION = "[\"?=\" , \"Output Asset Based Commission JSON\"]";
	private static final String CLOSE = "[\"]\"]";		
	
	private final String ERROR_MSG1 = "Exception thrown while parsing VPMS response : ";
	
	private String inputCallingSystem;	
	
	private String inputRegion;	
	
	private String inputLocale;	
	
	private String inputLanguage;	
	
	private String inputContractType;	
	
	private String inputCurrency;	
	
	private String vpmsIpAddress;
	
	private String vpmsIpPort;
	
	private String vpmsModelPath;
	
	public AssetMgmtVPMSProcessHelper(){
		
	}
	public AssetMgmtVPMSProcessHelper(String inputCallingSystem, String inputRegion, String inputLocale,
			String inputLanguage, String inputContractType, String inputCurrency, String vpmsIpAddress,
			String vpmsIpPort, String vpmsModelPath) {
		super();
		this.inputCallingSystem = inputCallingSystem;
		this.inputRegion = inputRegion;
		this.inputLocale = inputLocale;
		this.inputLanguage = inputLanguage;
		this.inputContractType = inputContractType;
		this.inputCurrency = inputCurrency;
		this.vpmsIpAddress = vpmsIpAddress;
		this.vpmsIpPort = vpmsIpPort;	
		this.vpmsModelPath = vpmsModelPath;		
	}	;
	
	
	@Autowired
	CommonUtility utility;
	
	
	
	public List<AssetMgmtOutput> sendAsstMgmtDataToVpms(List<L2asstmgmtReaderDTO> asstmgmtReaderDTOList) {
		List<AssetMgmtOutput> vpmsResponse = null;		
		if(asstmgmtReaderDTOList != null && !asstmgmtReaderDTOList.isEmpty()){					
			String vpmsInput = createInputData(getJSONData(getAsstMgmtJsonData(asstmgmtReaderDTOList)));
			vpmsResponse = callREST(vpmsInput);			
		}		
		return vpmsResponse;
	}
	
	private String getJSONData(String jsonStr){		
		String SETVAR1 = jsonStr.replace("\"", "\\\"");
		SETVAR1 = "[\"!\", \"Input Asset Based Commission JSON\",\"" + SETVAR1 + "\"]";
		return SETVAR1;
	}

	private List<AssetMgmtOutput> callREST(String cmdArray) {		
		int IP_PORT  = Integer.parseInt(vpmsIpPort);		
		String EXEC_URL = "http://" + vpmsIpAddress + "/vpms/v1/rest/exec/" + IP_PORT;
		String vpmsResult = HttpRequest.post(EXEC_URL).send(cmdArray).body();		
		LOGGER.info("Inside AssetMgmtVPMSProcessHelper class : Response received from VPMS : {}", vpmsResult);//IJTI-1498		
		return parseResponse(vpmsResult);
	}	
	
	/**
	 * 
	 * The method creates an array of AssetMgmtOutput objects in String format from the input data
	 * @param asstmgmtReaderDTOList
	 * @param itemArray
	 */
	private String getAsstMgmtJsonData(List<L2asstmgmtReaderDTO> asstmgmtReaderDTOList){
		JSONArray itemArray = new JSONArray();		
		for(L2asstmgmtReaderDTO clienttDTO : asstmgmtReaderDTOList){
			JSONObject item = new JSONObject();
			item.put("Input_Fund_Code", clienttDTO.getVrtfnd());
			item.put("Input_Contract_No", clienttDTO.getChdrnum());
			item.put("Input_Contract_Type", clienttDTO.getCrtable());
			item.put("Input_Fund_Value", clienttDTO.getNoFundUnit());
			item.put("Input_Advisor_Name", clienttDTO.getFirstName());	
			item.put("Input_Advisor_Code", clienttDTO.getAgentNum());
			item.put("Input_Agent_Fund_Split", clienttDTO.getSplitc());		
			itemArray.put(item);
		}		
		LOGGER.debug("Inside AssetMgmtVPMSProcessHelper class : JSON formatted Input String : {}", itemArray);//IJTI-1498
		return itemArray.toString();
	}		
	
	/**
	 * 
	 * This method accepts VPMS response in String format, parses it
	 * and stores in a list of AssetMgmtOutput object
	 * 
	 * @param vpmsResponse
	 * @return assetMgmtOutputList
	 */		
	
	public List<AssetMgmtOutput> parseResponse(String vpmsResponse) {		
		ObjectMapper objectMapper = new ObjectMapper();
		List<Object> objList1 = null;
		List<Object> objList2 = null;
		Object arrayObject = null;
		List<AssetMgmtOutput> assetMgmtOutputList= null;
		try {		
			objList1 = objectMapper.readValue(vpmsResponse, new TypeReference<List<Object>>(){});			
			if(objList1 != null && !objList1.isEmpty()){
				objList2 = (List<Object>)objList1.get(9);				
			}
			if(objList2 != null && !objList2.isEmpty()){
				arrayObject = (Object)objList2.get(0);				
			}	
			if(arrayObject != null){
				TypeReference<List<AssetMgmtOutput>> mapType = new TypeReference<List<AssetMgmtOutput>>() {};			
				assetMgmtOutputList = objectMapper.readValue(arrayObject.toString(), mapType);				
			}			
		} catch (JsonParseException e) {			
			 LOGGER.error(ERROR_MSG1,e);//IJTI-1498
		} catch (JsonMappingException e) {
			 LOGGER.error(ERROR_MSG1,e);//IJTI-1498
		} catch (IOException e) {
			 LOGGER.error(ERROR_MSG1,e);//IJTI-1498
		}
		return assetMgmtOutputList;
	}
	
	

	/**
	 * This method return the current date in "yyyyMMdd" format
	 * 
	 * @return
	 */
	private String getCurrentDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");		
		return CommonUtility.formatDate(dateFormat.format(new Date()));
	}	
	
	/**
	 * The method returns a string consisting of all input parameters required by vp/ms
	 * 
	 * @return CMD_ARRAY String
	 */
	private String createInputData(String SETVAR1){					
		String CMD_ARRAY = 
				"[" + 
						"[\"[\", \""+vpmsModelPath+"\""+"]" + ", " +
				SETVAR1 + ", " +
				SETVAR2 + "\"" +getCurrentDate() + "\"" + "]" + ", " +   
				SETVAR3 + "\"" + inputCallingSystem + "\"" + "]" + ", " +   
				SETVAR4 + "\"" + inputContractType + "\"" + "]" + ", " +   
				SETVAR5 + "\"" + inputRegion + "\"" + "]" + ", " +   
				SETVAR6 + "\"" + inputLocale + "\"" + "]" + ", " +   
				SETVAR7 + "\"" +inputLanguage + "\"" + "]" + ", " +  
				SETVAR8 + "\"" + inputCurrency + "\"" + "]" + ", " +				 					
				OUTPUT_ASSET_BASED_COMMISSION + ", " +				
				CLOSE +
				"]";
		LOGGER.info("Inside AssetMgmtVPMSProcessHelper class : VPMS Input Parameters Array : {}",CMD_ARRAY);//IJTI-1498
		return CMD_ARRAY;
	}		
	
}
