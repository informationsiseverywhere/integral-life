package com.dxc.integral.life.general.listeners;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import com.dxc.integral.fsu.dao.ZbsdpfDAO;
import com.dxc.integral.fsu.dao.model.Zbsdpf;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.dao.RlncbkpfDAO;
import com.dxc.integral.life.dao.RlncpfDAO;
import com.dxc.integral.life.general.partitioner.L2lincrcvPartitioner;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class L2rlncStepListner implements StepExecutionListener{
	
	private int transactionDate;
	
	@Autowired
	private ZbsdpfDAO zbsdpfDAO;
	
	@Autowired
	private UsrdpfDAO usrdpfDAO;
	
	@Autowired
	private RlncpfDAO rlncpfDAO;

	@Autowired
	private Datcon2 datcon2;
	
	@Autowired
	private RlncbkpfDAO rlncbkpfDAO;
	
	@Autowired
	private L2lincrcvPartitioner l2lincrcvPartitionerObj;
	
	private List<String> tempList;
	private String recieveFolder; 
		
	@Autowired
	private Environment env;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2rlncStepListner.class);
	
	@Override
	public synchronized void beforeStep(StepExecution stepExecution) {
		
		String username;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails)
			username  = ((UserDetails)principal).getUsername();
		else
			username = principal.toString();
		Zbsdpf zbsd = zbsdpfDAO.readRecordZbsdpf(Companies.LIFE, usrdpfDAO.getUserId(username.trim()));
		transactionDate = zbsd.getBusdate();
	    stepExecution.getExecutionContext().put("transactionDate", transactionDate);
	    stepExecution.getExecutionContext().put("username", username);
	    
	    int monthDate = datcon2.plusMonths(transactionDate, -4);
    	rlncbkpfDAO.deleteRlncbkpfRecord(monthDate);
        rlncpfDAO.deleteRlncpfData();
	}

	@Override
	public synchronized ExitStatus afterStep(StepExecution stepExecution) {
		
		recieveFolder = env.getProperty("report.output.path") + env.getProperty("folder.linc.receive") 
		+ env.getProperty("folder.archive");
		
		tempList = l2lincrcvPartitionerObj.getTemperoryList();
		if(tempList != null && ("FAILED").equals(stepExecution.getStatus().toString())) {
			String archiveFolder = env.getProperty("folder.archive");        
			recieveFolder = env.getProperty("report.output.path") + env.getProperty("folder.linc.receive");
				for(String f1 : tempList) {
					File archFile = FileSystems.getDefault().getPath(recieveFolder + archiveFolder + f1).toFile();
					if(archFile.renameTo(FileSystems.getDefault().getPath(recieveFolder + archFile.getName()).toFile())) 
				    {  
						deleteFile(archiveFolder, f1);
				    }
				}
			}
		return null;
	}

	private void deleteFile(String archiveFolder, String f1) {
		try {
			Files.deleteIfExists(FileSystems.getDefault().getPath(recieveFolder + archiveFolder + f1).getFileName());
		} catch (IOException e) {
			LOGGER.info("{The error is- }", e);
		}
	}
}
