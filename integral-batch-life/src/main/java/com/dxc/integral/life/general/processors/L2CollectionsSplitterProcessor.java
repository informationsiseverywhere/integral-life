package com.dxc.integral.life.general.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.BprdpfDAO;
import com.dxc.integral.iaf.dao.BsscpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Bprdpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2CollectionsSplitterReaderDTO;

@Scope(value = "step")
@Async
public class L2CollectionsSplitterProcessor implements ItemProcessor<L2CollectionsSplitterReaderDTO, L2CollectionsSplitterReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CollectionsSplitterProcessor.class);
	/** The batch job name for L2POLRNWL */
	private static final String JOB_NAME = "L2POLRNWL";
	
	private String batchCompany = Companies.LIFE;
	private String programName = "B5352";
	
	@Autowired
	private BprdpfDAO bprdpfDAO;
	
	@Autowired
	private BsscpfDAO bsscpfDAO;
	
	@Autowired
	private UsrdpfDAO usrdpfDAO;
	
	@Autowired
	private Batcup batcup;

	@Autowired
	private Sftlock sftlock;
	
	@Autowired
	private ItempfDAO itempfDAO;
	
	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['transactionDate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	private Bprdpf bprdpf;
	private Batcpf batchDetail = null;
	private Usrdpf usrdpf = null;
	
	@Override
	public L2CollectionsSplitterReaderDTO process(L2CollectionsSplitterReaderDTO item) throws Exception {
		if (bprdpf != null)
			bprdpf = bprdpfDAO.getProcessDefine(programName, batchCompany);
		String wsaaLinxRunid = bprdpf.getBpsyspar04();
		if (usrdpf == null) {
			usrdpf = usrdpfDAO.getUserInfo(userName);
		}
		if (batchDetail == null) {
			batchDetail = batcup.getBatchDetails(transactionDate, businessDate, Companies.LIFE, bprdpf.getBauthcode(), branch,
					usrdpf, batchName);
		}
		if (bprdpf.getBpsyspar03() != null && !bprdpf.getBpsyspar03().isEmpty()) {
			item.setaCAGClearFlag(true);
		}
		item.setWsaaLinxRunid(wsaaLinxRunid);
		item.setNoOfSubThreads(bprdpf.getBthrdssprc());
		item.setWsaaCompany(Companies.LIFE);
		item.setWsaaEffdate(transactionDate);
		return item;
	}

}
