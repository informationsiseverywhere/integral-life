package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr60d implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String datefrmDisp;
	private String datetoDisp;
	private String zsgtpct;
	private String zoerpct;
	private String zdedpct;
	private String zundpct;
	private String zspspct;
	private String totprcnt;
	private String zsgtamt;
	private String zoeramt;
	private String zdedamt;
	private String zundamt;
	private String zspsamt;
	private String totamnt;
	private String zslrysamt;
	private String zslryspct;
	private String coContriPmt;
	private String receivedFromAto;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getDatefrmDisp() {
		return datefrmDisp;
	}
	public void setDatefrmDisp(String datefrmDisp) {
		this.datefrmDisp = datefrmDisp;
	}
	public String getDatetoDisp() {
		return datetoDisp;
	}
	public void setDatetoDisp(String datetoDisp) {
		this.datetoDisp = datetoDisp;
	}
	public String getZsgtpct() {
		return zsgtpct;
	}
	public void setZsgtpct(String zsgtpct) {
		this.zsgtpct = zsgtpct;
	}
	public String getZoerpct() {
		return zoerpct;
	}
	public void setZoerpct(String zoerpct) {
		this.zoerpct = zoerpct;
	}
	public String getZdedpct() {
		return zdedpct;
	}
	public void setZdedpct(String zdedpct) {
		this.zdedpct = zdedpct;
	}
	public String getZundpct() {
		return zundpct;
	}
	public void setZundpct(String zundpct) {
		this.zundpct = zundpct;
	}
	public String getZspspct() {
		return zspspct;
	}
	public void setZspspct(String zspspct) {
		this.zspspct = zspspct;
	}
	public String getTotprcnt() {
		return totprcnt;
	}
	public void setTotprcnt(String totprcnt) {
		this.totprcnt = totprcnt;
	}
	public String getZsgtamt() {
		return zsgtamt;
	}
	public void setZsgtamt(String zsgtamt) {
		this.zsgtamt = zsgtamt;
	}
	public String getZoeramt() {
		return zoeramt;
	}
	public void setZoeramt(String zoeramt) {
		this.zoeramt = zoeramt;
	}
	public String getZdedamt() {
		return zdedamt;
	}
	public void setZdedamt(String zdedamt) {
		this.zdedamt = zdedamt;
	}
	public String getZundamt() {
		return zundamt;
	}
	public void setZundamt(String zundamt) {
		this.zundamt = zundamt;
	}
	public String getZspsamt() {
		return zspsamt;
	}
	public void setZspsamt(String zspsamt) {
		this.zspsamt = zspsamt;
	}
	public String getTotamnt() {
		return totamnt;
	}
	public void setTotamnt(String totamnt) {
		this.totamnt = totamnt;
	}
	public String getZslrysamt() {
		return zslrysamt;
	}
	public void setZslrysamt(String zslrysamt) {
		this.zslrysamt = zslrysamt;
	}
	public String getZslryspct() {
		return zslryspct;
	}
	public void setZslryspct(String zslryspct) {
		this.zslryspct = zslryspct;
	}
	public String getCoContriPmt() {
		return coContriPmt;
	}
	public void setCoContriPmt(String coContriPmt) {
		this.coContriPmt = coContriPmt;
	}
	public String getReceivedFromAto() {
		return receivedFromAto;
	}
	public void setReceivedFromAto(String receivedFromAto) {
		this.receivedFromAto = receivedFromAto;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "Sr60d []";
	}
	
}
