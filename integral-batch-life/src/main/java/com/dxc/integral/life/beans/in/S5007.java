package com.dxc.integral.life.beans.in;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5007 implements Serializable{
		private static final long serialVersionUID = 1L;
		private String actionKey;
		private String activeField;
		private S5007screensfl s5007screensfl;
		public String getActionKey() {
			return actionKey;
		}
		public void setActionKey(String actionKey) {
			this.actionKey = actionKey;
		}
		public String getActiveField() {
			return activeField;
		}
		public void setActiveField(String activeField) {
			this.activeField = activeField;
		}
		public S5007screensfl getS5007screensfl() {
			return s5007screensfl;
		}
		public void setS5007screensfl(S5007screensfl s5007screensfl) {
			this.s5007screensfl = s5007screensfl;
		}
		
		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class S5007screensfl implements Serializable {
			private static final long serialVersionUID = 1L;
			private List<KeyValueItem> actions;

			public List<KeyValueItem> getAttributeList() {
				return actions;
			}
			public void setAttributeList(List<KeyValueItem> actions) {
				this.actions = actions;
			}
		
			@JsonInclude(value = JsonInclude.Include.NON_NULL)
			@JsonIgnoreProperties(ignoreUnknown = true)
			public static class SFLRow implements Serializable {
				private static final long serialVersionUID = 1L;
				private String agntsel;
				private String splitBcomm;
				private String splitBpts;

				public String getAgntsel() {
					return agntsel;
				}
				public void setAgntsel(String agntsel) {
					this.agntsel = agntsel;
				}
				public String getSplitBcomm() {
					return splitBcomm;
				}
				public void setSplitBcomm(String splitBcomm) {
					this.splitBcomm = splitBcomm;
				}
				public String getSplitBpts() {
					return splitBpts;
				}
				public void setSplitBpts(String splitBpts) {
					this.splitBpts = splitBpts;
				}
			} 
		}

		@Override
		public String toString() {
			return "S5007 []";
		}
}
