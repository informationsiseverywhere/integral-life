package com.dxc.integral.life.beans;

import java.util.List;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hpuapf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;


/**
 * DTO for Itemreader
 * 
 * @author wli31
 *
 */
public class L2OverdueReaderDTO {
	private String chdrcoy;
	private String chdrnum;
	private int payrseqno;
	private int btdate;
	private int ptdate;
	private String billchnl;
	private Chdrpf  chdrpfUpdate;
	private Chdrpf  chdrpfUpdate2;
	private Chdrpf  chdrpfInsert;
	private Payrpf payrInsert;
	private Payrpf payrUpdate;
	private Payrpf payrpfPstcde;
	private List<Covrpf> covrInsert;
	private List<Covrpf> covrUpdate;
	private List<Hpuapf> hpuapfUpdate;
	private List<Hpuapf> hpuapfInsert;
	private Ptrnpf ptrnInsert;
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}
	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}
	public Chdrpf getChdrpfInsert() {
		return chdrpfInsert;
	}
	public void setChdrpfInsert(Chdrpf chdrpfInsert) {
		this.chdrpfInsert = chdrpfInsert;
	}
	public Payrpf getPayrInsert() {
		return payrInsert;
	}
	public void setPayrInsert(Payrpf payrInsert) {
		this.payrInsert = payrInsert;
	}
	public Payrpf getPayrUpdate() {
		return payrUpdate;
	}
	public void setPayrUpdate(Payrpf payrUpdate) {
		this.payrUpdate = payrUpdate;
	}
	public List<Covrpf> getCovrInsert() {
		return covrInsert;
	}
	public void setCovrInsert(List<Covrpf> covrInsert) {
		this.covrInsert = covrInsert;
	}
	public List<Covrpf> getCovrUpdate() {
		return covrUpdate;
	}
	public void setCovrUpdate(List<Covrpf> covrUpdate) {
		this.covrUpdate = covrUpdate;
	}
	public List<Hpuapf> getHpuapfUpdate() {
		return hpuapfUpdate;
	}
	public void setHpuapfUpdate(List<Hpuapf> hpuapfUpdate) {
		this.hpuapfUpdate = hpuapfUpdate;
	}
	public List<Hpuapf> getHpuapfInsert() {
		return hpuapfInsert;
	}
	public void setHpuapfInsert(List<Hpuapf> hpuapfInsert) {
		this.hpuapfInsert = hpuapfInsert;
	}
	public Ptrnpf getPtrnInsert() {
		return ptrnInsert;
	}
	public void setPtrnInsert(Ptrnpf ptrnInsert) {
		this.ptrnInsert = ptrnInsert;
	}
	public Chdrpf getChdrpfUpdate2() {
		return chdrpfUpdate2;
	}
	public void setChdrpfUpdate2(Chdrpf chdrpfUpdate2) {
		this.chdrpfUpdate2 = chdrpfUpdate2;
	}
	public Payrpf getPayrpfPstcde() {
		return payrpfPstcde;
	}
	public void setPayrpfPstcde(Payrpf payrpfPstcde) {
		this.payrpfPstcde = payrpfPstcde;
	}
	
	
	
	
}