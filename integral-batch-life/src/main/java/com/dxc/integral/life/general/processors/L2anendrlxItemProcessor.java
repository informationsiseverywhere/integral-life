package com.dxc.integral.life.general.processors;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.beans.ControlTotalDTO;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.L2anendrlxProcessorDTO;
import com.dxc.integral.life.beans.L2anendrlxReaderDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.utils.Crtloan;
import com.dxc.integral.life.utils.L2anendrlxProcessorUtil;
import com.dxc.integral.life.utils.Paydebts;

/**
 * ItemProcessor for L2ANENDRLX batch step.
 * 
 * @author vhukumagrawa
 *
 */
@Scope(value = "step")
@Async
public class L2anendrlxItemProcessor implements ItemProcessor<L2anendrlxReaderDTO, L2anendrlxReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2anendrlxItemProcessor.class);

	/** The batch job name for L2ANENDRLX */
	public static final String L2ANENDRLX_JOB_NAME = "L2ANENDRLX";

	/** The batch transaction code for L2ANENDRLX */
	private String batchTranCode;

	@Autowired
	private UsrdpfDAO usrdpfDAO;

	@Autowired
	private L2anendrlxProcessorUtil l2anendrlxProcessorUtil;

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private Crtloan crtloan;

	@Autowired
	private Batcup batcup;

	@Autowired
	private DescpfDAO descpfDAO;

	@Autowired
	private Sftlock sftlock;

	@Autowired
	private ControlTotalDTO controlTotalDTO;

	private String programName = "BR622";

	@Autowired
	private ItempfDAO itempfDAO;

	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/* IJTI-398 */

	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;

	@Value("#{jobParameters['transactionDate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['chdrcoy']}")
	private String company;

	private T5645 t5645IO = null;
	private Descpf t5645Descpf = null;
	private Descpf t3695Descpf = null;
	private Usrdpf usrdpf = null;
	private String batchBacth = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public L2anendrlxReaderDTO process(L2anendrlxReaderDTO readerDTO) throws IOException, ParseException {
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrnum());
		/* ILIFE-5977 */
		readerDTO.setBatctrcde(batchTranCode);
		boolean validCoverage = l2anendrlxProcessorUtil
				.checkValidCoverage(readerDTO);/* ILIFE-5977 */

		if (!validCoverage) {
			LOGGER.info("Invalid contract : {}", readerDTO.getChdrnum());
			controlTotalDTO.setControlTotal02(controlTotalDTO.getControlTotal02().add(BigDecimal.ONE));
			return null;
		}

		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(readerDTO.getChdrnum());
		slckpf.setEnttyp("CH");
		slckpf.setCompany(readerDTO.getChdrcoy());
		slckpf.setProctrancd(batchTranCode);
		slckpf.setUsrprf(userName);
		slckpf.setJobnm(batchName);
		if (!sftlock.lockEntity(slckpf)) {
			LOGGER.info("Contract [{}] is already locked.", readerDTO.getChdrnum());
			controlTotalDTO.setControlTotal03(controlTotalDTO.getControlTotal03().add(BigDecimal.ONE));
			return null;
		}
		Integer tranDate = DatimeUtil.covertToyyyyMMddDate(transactionDate);

		String[] transactionDateParts = transactionDate.split("-");
		String acctYear = transactionDateParts[2];
		String acctMonth = transactionDateParts[1];

		getUserDetails(userName);

		readReqSmartTableInfo(readerDTO);// ILIFE-5763

		if ("".equals(batchBacth)) {
			batchBacth = batcup.getBatchBatch(acctYear, acctMonth, readerDTO.getChdrcoy(), batchTranCode, branch,
					String.valueOf(usrdpf.getUsernum()));
		}

		L2anendrlxReaderDTO reader = populateReaderDTO(readerDTO, acctMonth, acctYear);

		L2anendrlxProcessorDTO processorDTO = l2anendrlxProcessorUtil.getPaymnetDueInfo(reader,
				tranDate);/* ILIFE-5977 */
		L2anendrlxReaderDTO readerIO = l2anendrlxProcessorUtil.updateReaderDTOInfo(processorDTO, reader);
		CrtloanDTO crtloanDTO = l2anendrlxProcessorUtil.populateCrtloanDTO(readerIO, t5645IO,
				t5645Descpf.getLongdesc());
		crtloan.createLoan(crtloanDTO);/* ILIFE-5977 */

		if (null != readerIO.getNewPaymentOpt() && !readerIO.getNewPaymentOpt().trim().isEmpty()) {
			if ("paydebts".equalsIgnoreCase(readerIO.getNewPaymentOpt())) {
				ZrcshopDTO zrcshopDTO = l2anendrlxProcessorUtil.populateZrcshopDTO(readerIO, t5645IO,
						t3695Descpf.getLongdesc());/* ILIFE-5977 */
				((Paydebts) appContext.getBean(readerIO.getNewPaymentOpt().toLowerCase())).payOffDebt(zrcshopDTO);
				controlTotalDTO.setControlTotal06(controlTotalDTO.getControlTotal06().add(BigDecimal.ONE));
			}
		} else {
			controlTotalDTO.setControlTotal05(controlTotalDTO.getControlTotal05().add(BigDecimal.ONE));
		}
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrnum());
		return readerIO;
	}

	protected void getUserDetails(String userName) {
		if (usrdpf == null) {
			usrdpf = usrdpfDAO.getUserInfo(userName);
		}
	}

	private void readReqSmartTableInfo(L2anendrlxReaderDTO readerDTO) throws IOException {// ILIFE-5763
		getT5645Info(readerDTO);// ILIFE-5763 /*ILIFE-5977*/
		getT5645Descpf();
		getT3695Descpf(readerDTO);// ILIFE-5763
	}

	private void getT5645Info(L2anendrlxReaderDTO readerDTO) throws IOException {// ILIFE-5763
																					// /*ILIFE-5977*/

		Map<String, List<Itempf>> t5645Map = itempfDAO.readSmartTableByTableName(company, "T5645",
				CommonConstants.IT_ITEMPFX);/* ILIFE-5977 */
		if (null == t5645IO && t5645Map.containsKey(programName)) {
			List<Itempf> t5645ItemList = t5645Map.get(programName);
			t5645IO = smartTableDataUtils.convertGenareaToPojo(t5645ItemList.get(0).getGenareaj(),
					T5645.class); /* IJTI-398 */
		} else if (!t5645Map.containsKey(programName)) {// ILIFE-5763 Starts
			throw new ItemNotfoundException("Br622: Item " + programName
					+ " not found in Table T5645 while processing contract " + readerDTO.getChdrnum());
		} // ILIFE-5763 Ends
	}

	private void getT3695Descpf(L2anendrlxReaderDTO readerDTO) {// ILIFE-5763

		if (t3695Descpf == null) {
			t3695Descpf = descpfDAO.getDescInfo(company, "T3695", t5645IO.getSacstypes().get(0));
		}
		if (null == t3695Descpf) {// ILIFE-5763 Starts
			throw new ItemNotfoundException("Br622: Description for item " + t5645IO.getSacstypes().get(0)
					+ " not found in Table T3695 while processing contract " + readerDTO.getChdrnum());
		} // ILIFE-5763 Ends
	}

	private void getT5645Descpf() {

		if (t5645Descpf == null) {
			t5645Descpf = descpfDAO.getDescInfo(company, "T5645", programName);
		}
	}/* ILIFE-5977 */

	/**
	 * Populates L2anendrlxReaderDTO with Batcpf table related information
	 * 
	 * @param readerDTO
	 * @param acctMonth
	 * @param acctYear
	 * @return
	 */
	private L2anendrlxReaderDTO populateReaderDTO(L2anendrlxReaderDTO readerDTO, String acctMonth, String acctYear) {

		Integer accountingMonth = Integer.parseInt(acctMonth);
		Integer accountingYear = Integer.parseInt(acctYear);

		readerDTO.setUsrprf(usrdpf.getUserid());
		readerDTO.setJobnm(batchName);
		readerDTO.setPolicyTranno(readerDTO.getPolicyTranno() + 1);
		readerDTO.setUserNum(usrdpf.getUsernum().intValue());
		readerDTO.setBatcactmn(accountingMonth);
		readerDTO.setBatcactyr(accountingYear);
		readerDTO.setBatcbatch(batchBacth);
		readerDTO.setBatcbrn(branch);
		readerDTO.setBatccoy(company);
		readerDTO.setBatcpfx(CommonConstants.BATCH_PREFIX);
		readerDTO.setBatctrcde(batchTranCode);
		readerDTO.setTermid(" ");

		return readerDTO;
	}

	/**
	 * Getter for batchTranCode
	 * 
	 * @return - the batchTranCode
	 */
	public String getBatchTranCode() {
		return batchTranCode;
	}

	/**
	 * Setter for batchTranCode
	 * 
	 * @param batchTranCode
	 *            -the batchTranCode
	 */
	public void setBatchTranCode(String batchTranCode) {
		this.batchTranCode = batchTranCode;
	}

	/**
	 * Getter for UsrdpfDAO
	 * 
	 * @return - usrdpfDAO
	 */
	public UsrdpfDAO getUsrdpfDAO() {
		return usrdpfDAO;
	}

	/**
	 * Getter for Usrdpf
	 * 
	 * @return - usrdpf
	 */
	public Usrdpf getUsrdpf() {
		return usrdpf;
	}

	/**
	 * Setter for Usrdpf
	 * 
	 * @param usrdpf
	 *            - Usrdpf POJO
	 */
	public void setUsrdpf(Usrdpf usrdpf) {
		this.usrdpf = usrdpf;
	}
}