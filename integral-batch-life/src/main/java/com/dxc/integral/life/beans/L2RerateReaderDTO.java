package com.dxc.integral.life.beans;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

/**
 * DTO for Itemreader
 * 
 * @author wli31
 *
 */
public class L2RerateReaderDTO {
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String pstatcode;
	private String statcode;
	private long uniqueNumber;
	private Chdrpf  chdrpfUpdate;
	private Chdrpf  chdrpfInsert;
	private Payrpf payrInsert;
	private Payrpf payrUpdate;
	private Covrpf covrInsert;
	private Covrpf covrUpdate;
	private Incrpf incrInsert;
	private Ptrnpf ptrnInsert;
	private Agcmpf agcmUpdate;
	private Agcmpf agcmInsert;
	
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}
	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}
	public Chdrpf getChdrpfInsert() {
		return chdrpfInsert;
	}
	public void setChdrpfInsert(Chdrpf chdrpfInsert) {
		this.chdrpfInsert = chdrpfInsert;
	}
	public Payrpf getPayrInsert() {
		return payrInsert;
	}
	public void setPayrInsert(Payrpf payrInsert) {
		this.payrInsert = payrInsert;
	}
	public Payrpf getPayrUpdate() {
		return payrUpdate;
	}
	public void setPayrUpdate(Payrpf payrUpdate) {
		this.payrUpdate = payrUpdate;
	}
	public Covrpf getCovrInsert() {
		return covrInsert;
	}
	public void setCovrInsert(Covrpf covrInsert) {
		this.covrInsert = covrInsert;
	}
	public Covrpf getCovrUpdate() {
		return covrUpdate;
	}
	public void setCovrUpdate(Covrpf covrUpdate) {
		this.covrUpdate = covrUpdate;
	}
	public Incrpf getIncrInsert() {
		return incrInsert;
	}
	public void setIncrInsert(Incrpf incrInsert) {
		this.incrInsert = incrInsert;
	}
	public Ptrnpf getPtrnInsert() {
		return ptrnInsert;
	}
	public void setPtrnInsert(Ptrnpf ptrnInsert) {
		this.ptrnInsert = ptrnInsert;
	}
	public Agcmpf getAgcmUpdate() {
		return agcmUpdate;
	}
	public void setAgcmUpdate(Agcmpf agcmUpdate) {
		this.agcmUpdate = agcmUpdate;
	}
	public Agcmpf getAgcmInsert() {
		return agcmInsert;
	}
	public void setAgcmInsert(Agcmpf agcmInsert) {
		this.agcmInsert = agcmInsert;
	}
	
}