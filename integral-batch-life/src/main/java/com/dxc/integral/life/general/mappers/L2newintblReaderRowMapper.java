package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2newintblReaderDTO;

public class L2newintblReaderRowMapper  implements RowMapper<L2newintblReaderDTO>{
	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2newintblReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2newintblReaderDTO l2newintblReader = new L2newintblReaderDTO();

		
		l2newintblReader.setLo_unique_number(rs.getLong("lo_unique_number"));
		l2newintblReader.setLo_chdrcoy(rs.getString("lo_chdrcoy"));
		l2newintblReader.setLo_chdrnum(rs.getString("lo_chdrnum"));
		l2newintblReader.setLo_loannumber(rs.getInt("loannumber"));
		l2newintblReader.setLo_loantype(rs.getString("lo_loantype"));
		l2newintblReader.setLo_loancurr(rs.getString("lo_loancurr"));
		l2newintblReader.setLo_validflag(rs.getString("lo_validflag"));
		l2newintblReader.setLo_ftranno(rs.getInt("lo_ftranno"));
		l2newintblReader.setLo_ltranno(rs.getInt("lo_ltranno"));
		l2newintblReader.setLo_loanorigam(rs.getBigDecimal("lo_loanorigam"));
		l2newintblReader.setLo_loanstdate(rs.getInt("lo_loanstdate"));
		l2newintblReader.setLo_lstcaplamt(rs.getBigDecimal("lo_lstcaplamt"));
		l2newintblReader.setLo_lstcapdate(rs.getInt("lo_lstcapdate"));
		l2newintblReader.setLo_nxtcapdate(rs.getInt("lo_nxtcapdate"));
		l2newintblReader.setLo_lstintbdte(rs.getInt("lo_lstintbdte"));
		l2newintblReader.setLo_nxtintbdte(rs.getInt("lo_nxtintbdte"));
		l2newintblReader.setLo_tplstmdty(rs.getBigDecimal("lo_tplstmdty"));
		l2newintblReader.setLo_termid(rs.getString("lo_termid"));
		l2newintblReader.setLo_user_t(rs.getInt("lo_user_t"));
		l2newintblReader.setLo_trdt(rs.getInt("lo_trdt"));
		l2newintblReader.setLo_trtm(rs.getInt("lo_trtm"));
		l2newintblReader.setLo_usrprf(rs.getString("lo_usrprf"));
		l2newintblReader.setLo_jobnm(rs.getString("lo_jobnm"));
		l2newintblReader.setCh_unique_number(rs.getLong("ch_unique_number"));
		l2newintblReader.setCh_currfrom(rs.getInt("ch_currfrom"));
		l2newintblReader.setCh_tranno(rs.getInt("ch_tranno"));
		l2newintblReader.setCh_cnttype(rs.getString("ch_cnttype"));
		l2newintblReader.setCh_chdrpfx(rs.getString("ch_chdrpfx"));
		l2newintblReader.setCh_servunit(rs.getString("ch_servunit"));
		l2newintblReader.setCh_occdate(rs.getInt("ch_occdate"));
		l2newintblReader.setCh_ccdate(rs.getInt("ch_ccdate"));
		l2newintblReader.setCh_cownpfx(rs.getString("ch_cownpfx"));
		l2newintblReader.setCh_cowncoy(rs.getString("ch_cowncoy"));
		l2newintblReader.setCh_cownnum(rs.getString("ch_cownnum"));
		l2newintblReader.setCh_collchnl(rs.getString("ch_collchnl"));
		l2newintblReader.setCh_cntbranch(rs.getString("ch_cntbranch"));
		l2newintblReader.setCh_agntpfx(rs.getString("ch_agntpfx"));
		l2newintblReader.setCh_agntcoy(rs.getString("ch_agntcoy"));
		l2newintblReader.setCh_agntnum(rs.getString("ch_agntnum"));
		l2newintblReader.setCl_clntpfx(rs.getString("cl_clntpfx"));
		l2newintblReader.setCl_clntcoy(rs.getString("cl_clntcoy"));
		l2newintblReader.setCl_clntnum(rs.getString("cl_clntnum"));
		l2newintblReader.setPy_mandref(rs.getString("py_mandref"));
		l2newintblReader.setPy_ptdate(rs.getInt("py_ptdate"));
		l2newintblReader.setPy_billchnl(rs.getString("py_billchnl"));
		l2newintblReader.setPy_billfreq(rs.getString("py_billfreq"));
		l2newintblReader.setPy_billcurr(rs.getString("py_billcurr"));
		l2newintblReader.setPy_nextdate(rs.getInt("py_nextdate"));
		l2newintblReader.setMa_bankkey(rs.getString("ma_bankkey"));
		l2newintblReader.setMa_bankacckey(rs.getString("ma_bankacckey"));
		l2newintblReader.setMa_mandstat(rs.getString("ma_mandstat"));
		l2newintblReader.setCb_facthous(rs.getString("cb_facthous"));
		return l2newintblReader;
	}

}