package com.dxc.integral.life.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author sagrawal35
 * Create JSON to receive output data from VP/MS model
 * 
 *
 */
@JsonPropertyOrder({
	"Output_Advisor_Code",
    "Output_Fund_Code",
    "Output_Fund_Value",
    "Output_Commission"
})
public class AssetMgmtOutput {

	@JsonProperty("Output_Advisor_Code")
	private String outputAdvisorCode;
	
	@JsonProperty("Output_Fund_Code")
	private String outputFundCode;
	
	@JsonProperty("Output_Fund_Value")
	private String outputFundValue;	
	
	@JsonProperty("Output_Commission")
	private String outputCommission;

	public String getOutputAdvisorCode() {
		return outputAdvisorCode;
	}

	public void setOutputAdvisorCode(String outputAdvisorCode) {
		this.outputAdvisorCode = outputAdvisorCode;
	}

	public String getOutputFundCode() {
		return outputFundCode;
	}

	public void setOutputFundCode(String outputFundCode) {
		this.outputFundCode = outputFundCode;
	}

	public String getOutputFundValue() {
		return outputFundValue;
	}

	public void setOutputFundValue(String outputFundValue) {
		this.outputFundValue = outputFundValue;
	}

	public String getOutputCommission() {
		return outputCommission;
	}

	public void setOutputCommission(String outputCommission) {
		this.outputCommission = outputCommission;
	}	
	
}
