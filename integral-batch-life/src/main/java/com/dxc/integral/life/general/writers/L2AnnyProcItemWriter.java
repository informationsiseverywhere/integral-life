package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2AnnyProcReaderDTO;

/**
 * ItemWriter for L2Annyproc batch step.
 * 
 * @author wli31
 *
 */
@Async
public class L2AnnyProcItemWriter implements ItemWriter<L2AnnyProcReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AnnyProcItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;
	private ItemWriter<L2AnnyProcReaderDTO> covrpfUpdateWriter;
	
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;	

	@Override
	public void write(List<? extends L2AnnyProcReaderDTO> readerDTOList) throws Exception {
		LOGGER.info("L2anniversaryItemWriter starts.");
		if (!readerDTOList.isEmpty()) {
			covrpfUpdateWriter.write(readerDTOList);
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOList.size(); i++) {
				entityList.add(readerDTOList.get(i).getChdrnum());
			}			
			
			sftlock.unlockByList("CH", readerDTOList.get(0).getChdrcoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2anniversaryItemWriter ends.");
	}

	public void setCovrpfUpdateWriter(ItemWriter<L2AnnyProcReaderDTO> covrpfUpdateWriter) {
		this.covrpfUpdateWriter = covrpfUpdateWriter;
	}
}
