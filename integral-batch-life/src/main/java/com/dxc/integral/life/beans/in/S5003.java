package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5003 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S5003screensfl s5003screensfl;
	
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S5003screensfl getS5003screensfl() {
		return s5003screensfl;
	}
	public void setS5003screensfl(S5003screensfl s5003screensfl) {
		this.s5003screensfl = s5003screensfl;
	}

	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class S5003screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;
		
		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String action;
			private String life;
			private String coverage;
			private String rider;
			private String elemkey;
			private String elemdesc;
			private String sumin;
			private String premCessTerm;
			private String riskCessTerm;
			private String instPrem;

			public String getAction() {
				return action;
			}
			public void setAction(String action) {
				this.action = action;
			}
			public String getLife() {
				return life;
			}
			public void setLife(String life) {
				this.life = life;
			}
			public String getCoverage() {
				return coverage;
			}
			public void setCoverage(String coverage) {
				this.coverage = coverage;
			}
			public String getRider() {
				return rider;
			}
			public void setRider(String rider) {
				this.rider = rider;
			}
			public String getElemkey() {
				return elemkey;
			}
			public void setElemkey(String elemkey) {
				this.elemkey = elemkey;
			}
			public String getElemdesc() {
				return elemdesc;
			}
			public void setElemdesc(String elemdesc) {
				this.elemdesc = elemdesc;
			}
			public String getSumin() {
				return sumin;
			}
			public void setSumin(String sumin) {
				this.sumin = sumin;
			}
			public String getPremCessTerm() {
				return premCessTerm;
			}
			public void setPremCessTerm(String premCessTerm) {
				this.premCessTerm = premCessTerm;
			}
			public String getRiskCessTerm() {
				return riskCessTerm;
			}
			public void setRiskCessTerm(String riskCessTerm) {
				this.riskCessTerm = riskCessTerm;
			}
			public String getInstPrem() {
				return instPrem;
			}
			public void setInstPrem(String instPrem) {
				this.instPrem = instPrem;
			}
		} 
	}

	@Override
	public String toString() {
		return "S5003 []";
	}
	
}
