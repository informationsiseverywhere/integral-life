package com.dxc.integral.life.utils;

import java.util.List;
import java.util.Map;
import org.springframework.batch.item.file.FlatFileItemWriter;
import com.dxc.integral.life.beans.L2lincsndReaderDTO;
import com.dxc.integral.life.config.L2lincsndJsonMapping;

/**
 * Common Utility processing for L2LINCSND.
 * 
 * @author gsaluja2
 *
 */
public interface L2lincsndCommonsUtil {
	/**
	 * Generates string format for data to be written to Flat File.
	 * 
	 * @param formattedStringMap
	 * @return formattedString
	 */
	public String setDataFormat(Map<String, Integer> formattedStringMap);
	
	/**
	 * Reads all JSON data at once.
	 * 
	 * @return L2lincsndJsonMapping object
	 */
	public L2lincsndJsonMapping fetchAllJsonData();
	
	/**
	 * Gets String Format size from JSON.
	 * 
	 * @param dataFormatListMap
	 * @return dataFormat
	 */
	public Map<String, Integer> getJsonDataFormatNode(Map<String, String> dataFormatListMap);
	
	/**
	 * Gets File Name for flat file from JSON.
	 * 
	 * @param fileTypeListMap
	 * @return fileType
	 */
	public Map<String, FlatFileItemWriter<L2lincsndReaderDTO>> getJsonFileType(Map<String, ?> fileTypeListMap);
	
	/**
	 * Gets header details from JSON.
	 * 
	 * @param headerListMap
	 * @param transactionDate
	 * @param coy
	 * @param key
	 * @return header
	 */
	public String getJsonHeader(Map<String, ?> headerListMap, int transactionDate, String coy, String key);
	
	/**
	 * Gets Footer details from JSON
	 * 
	 * @param trailerEndListMap
	 * @param appendLine
	 * @param footer
	 * @return footer
	 */
	public String getJsonFooter(Map<String, String> trailerEndListMap, boolean appendLine, StringBuilder footer);
	
	/**
	 * Binds reader DTO object with particular record type and returns its map.
	 * 
	 * @param readerDTOList
	 * @param jsonDataMap
	 * @param resetTranscdList
	 * @return readerDTOListMap
	 */
	public Map<String, List<L2lincsndReaderDTO>> convertListToMap(List<? extends L2lincsndReaderDTO> readerDTOList,
			Map<String, ?> jsonDataMap, List<String> resetTranscdList);
}
