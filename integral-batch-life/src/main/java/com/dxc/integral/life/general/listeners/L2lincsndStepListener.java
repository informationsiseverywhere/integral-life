package com.dxc.integral.life.general.listeners;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import com.dxc.integral.fsu.dao.ZbsdpfDAO;
import com.dxc.integral.fsu.dao.model.Zbsdpf;
import com.dxc.integral.iaf.config.IntegralSbReportConfig;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.L2lincsndReaderDTO;
import com.dxc.integral.life.config.L2lincsndJsonMapping;
import com.dxc.integral.life.general.writers.L2lincsndFooterWriter;
import com.dxc.integral.life.general.writers.L2lincsndHeaderWriter;
import com.dxc.integral.life.smarttable.pojo.Th605;
import com.dxc.integral.life.utils.L2lincsndCommonsUtil;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author gsaluja2
 *
 */
public class L2lincsndStepListener implements StepExecutionListener{
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2lincsndStepListener.class);
		
	@Value("#{stepExecution.jobExecution.jobId}")
    private long jobExecutionId;
	
	private int transactionDate;
	
	@Autowired
	private ZbsdpfDAO zbsdpfDAO;
	
	@Autowired
	private UsrdpfDAO usrdpfDAO;
	
	@Autowired
	private L2lincsndCommonsUtil l2lincsndCommonsUtil;
	
	@Autowired
	private ItempfService itempfService;
	
	private Map<String, FlatFileItemWriter<L2lincsndReaderDTO>> flatFileWriterMap;
	   
	private String exportFileName;
	
	@Autowired
	private Environment env;
	
	private Map<String, Integer> formattedStringMap;
	
	private L2lincsndJsonMapping jsonData;
	
	private static final String SENDPATH = "folder.linc.send";
	
	public L2lincsndStepListener() {
		//does nothing
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		String username;
		final String th605 = "TH605";
		String coy = StringUtils.EMPTY;
		archiveFiles();
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails)
			username  = ((UserDetails)principal).getUsername();
		else
			username = principal.toString();
		Zbsdpf zbsd = zbsdpfDAO.readRecordZbsdpf(Companies.LIFE, usrdpfDAO.getUserId(username.trim()));
		transactionDate = zbsd.getBusdate();
		Th605 th605Object = itempfService.readSmartTableByTrim(Companies.LIFE, th605, Companies.LIFE, Th605.class);
		if(th605Object!=null)
			coy = th605Object.getLiacoy();
        jsonData = l2lincsndCommonsUtil.fetchAllJsonData();
		LOGGER.info("Export Data Before step begins.");
	    formattedStringMap = l2lincsndCommonsUtil.getJsonDataFormatNode(jsonData.getDataFormat());
	    flatFileWriterMap = new HashMap<>();
		LineAggregator<L2lincsndReaderDTO> lineAggregator = createFlatFileLineAggregator();
	    flatFileWriterMap = l2lincsndCommonsUtil.getJsonFileType(jsonData.getFile());
	    if(!flatFileWriterMap.isEmpty()) {
			for(Map.Entry<String, FlatFileItemWriter<L2lincsndReaderDTO>> entry: flatFileWriterMap.entrySet()) {
				stepExecution = setFileResources(transactionDate, entry, 
						coy, lineAggregator, stepExecution);
			}
	    }
	    stepExecution.getExecutionContext().put("resetTranscds", readTjlxx());
	    stepExecution.getExecutionContext().put("transactionDate", transactionDate);
	    stepExecution.getExecutionContext().put("username", username);
    }
	
	public void archiveFiles() {
		String integralReportsPath = IntegralSbReportConfig.getReportOutputPath();
		String archiveFolder = integralReportsPath+env.getProperty(SENDPATH)+env.getProperty("folder.archive");
		String sendFolder = integralReportsPath+env.getProperty(SENDPATH);
		File dir = FileSystems.getDefault().getPath(sendFolder).toFile();
		File[] fileNames;
		fileNames = dir.listFiles();
		if(fileNames != null) {
			for(File file : fileNames) {
				if(file.renameTo(FileSystems.getDefault().getPath(archiveFolder + file.getName()).toFile())) 
					file.delete(); 
			}
		}
	}
	
	/**
	 * Extract fields from reader DTO using FlatFileFieldExtractor.
	 * Get data format from JSON file.
	 * Set the data format to lineAggreator.
	 * @return lineAggregator
	 */
	public FormatterLineAggregator<L2lincsndReaderDTO> createFlatFileLineAggregator() {
    	FormatterLineAggregator<L2lincsndReaderDTO> lineAggregator = new FormatterLineAggregator<>();
        FieldExtractor<L2lincsndReaderDTO> fieldExtractor = createFlatFileFieldExtractor();
        lineAggregator.setFieldExtractor(fieldExtractor);
        lineAggregator.setFormat(l2lincsndCommonsUtil.setDataFormat(formattedStringMap));
        return lineAggregator;
    }
 
    public FieldExtractor<L2lincsndReaderDTO> createFlatFileFieldExtractor() {
        BeanWrapperFieldExtractor<L2lincsndReaderDTO> extractor = new BeanWrapperFieldExtractor<>();
        extractor.setNames(new ArrayList<>(formattedStringMap.keySet()).toArray(new String[0]));
        return extractor;
    }
    
    /**
     * Generates file extract name from JSON.
     * Sets file header and footer.
     * Create a maps of fileName and FlatFileItemWriter object
     * Sets these maps to stepExecutionContext for further access.
     * 
     * @param jobExecution
     * @param transactionDate
     * @param entry
     * @param coy
     * @param lineAggregator
     * @param stepExecution
     * 
     * @return stepExecution
     */
    public StepExecution setFileResources(int transactionDate,
    		Map.Entry<String, FlatFileItemWriter<L2lincsndReaderDTO>> entry, String coy,
    		LineAggregator<L2lincsndReaderDTO> lineAggregator, StepExecution stepExecution) {
    	exportFileName = stepExecution.getJobExecution().getJobInstance().getJobName().toUpperCase()+"_"
				+entry.getKey()+"_"+jobExecutionId+".txt";
    	setHeaderFooterCallbacks(entry.getKey(), transactionDate, coy);
    	flatFileWriterMap.get(entry.getKey()).setLineAggregator(lineAggregator);
		if(new FileSystemResource(IntegralSbReportConfig.getReportOutputPath()
				+env.getProperty(SENDPATH)+exportFileName).exists()) {
			flatFileWriterMap.get(entry.getKey()).setShouldDeleteIfEmpty(true);
			flatFileWriterMap.get(entry.getKey()).setAppendAllowed(true);
		}
		flatFileWriterMap.get(entry.getKey()).setResource(
				new FileSystemResource(IntegralSbReportConfig.getReportOutputPath()
						+env.getProperty(SENDPATH)+exportFileName));
		flatFileWriterMap.get(entry.getKey()).open(stepExecution.getExecutionContext());
		stepExecution.getExecutionContext().put(entry.getKey(), entry.getValue());
		stepExecution.getExecutionContext().put("jsonData", jsonData);
		return stepExecution;
    }
    
    /**
     * Generates headers and footers from JSON 
     * 
     * @param key
     * @param transactionDate
     * @param coy
     */
    public void setHeaderFooterCallbacks(String key, int transactionDate, String coy) {
    	String exportFileHeader;
		String exportFileFooter;
		exportFileHeader = l2lincsndCommonsUtil.getJsonHeader(jsonData.getHeader(), transactionDate, coy, key);
		L2lincsndHeaderWriter headerWriter = new L2lincsndHeaderWriter(exportFileHeader);
		flatFileWriterMap.get(key).setHeaderCallback(headerWriter);
		exportFileFooter = l2lincsndCommonsUtil.getJsonFooter(jsonData.getTrailer(), true, null);
		exportFileFooter = l2lincsndCommonsUtil.getJsonFooter(jsonData.getEnd(), false, new StringBuilder(exportFileFooter));
		L2lincsndFooterWriter footerWriter = new L2lincsndFooterWriter(exportFileFooter);
		flatFileWriterMap.get(key).setFooterCallback(footerWriter);
    }
    
    public List<String> readTjlxx() {
    	Map<String, List<Itempf>> itemMap = itempfService.readItemTabl(Companies.LIFE, "TJL78");
    	List<String> resetTranscd = new ArrayList<>();
    	Iterator<List<Itempf>> itemItr = Collections.emptyIterator();
    	if(null != itemMap && !itemMap.isEmpty())
    		itemItr = itemMap.values().iterator();
    	while(itemItr.hasNext()) {
    		List<Itempf> itemList = itemItr.next();
    		for(Itempf item: itemList) {
    			if("1".equals(item.getValidflag())) {
    				resetTranscd.add(item.getItemitem().trim());
        		}
    		}
    	}
    	return resetTranscd;
    }
    
    @Override
	public ExitStatus afterStep(StepExecution stepExecution) {
    	LOGGER.info("Export Data After step begins.");
    	if(stepExecution.getReadCount() ==  stepExecution.getWriteCount()
    			&& null != flatFileWriterMap && !flatFileWriterMap.isEmpty()) {
    		for(FlatFileItemWriter<L2lincsndReaderDTO> flatFileObject : flatFileWriterMap.values())
    			flatFileObject.close();
	    }
		return stepExecution.getExitStatus();
    }
}
