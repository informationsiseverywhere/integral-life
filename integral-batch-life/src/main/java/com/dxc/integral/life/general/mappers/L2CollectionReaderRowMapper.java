package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2CollectionReaderDTO;

public class L2CollectionReaderRowMapper  implements RowMapper<L2CollectionReaderDTO>{
	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2CollectionReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		L2CollectionReaderDTO L2CollectionReader = new L2CollectionReaderDTO();
		
		L2CollectionReader.setChdrcoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
		L2CollectionReader.setChdrnum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");
		L2CollectionReader.setInstfrom(rs.getInt("INSTFROM"));
		L2CollectionReader.setCntcurr(rs.getString("CNTCURR")!= null ? (rs.getString("CNTCURR").trim()) : "");
		L2CollectionReader.setBillcurr(rs.getString("BILLCURR")!= null ? (rs.getString("BILLCURR").trim()) : "");
		L2CollectionReader.setPayrseqno(rs.getInt("PAYRSEQNO"));
		L2CollectionReader.setCbillamt(rs.getBigDecimal("CBILLAMT"));
		L2CollectionReader.setBillcd(rs.getInt("BILLCD"));
		L2CollectionReader.setBillchnl(rs.getString("BILLCHNL"));
		L2CollectionReader.setInstamt01(rs.getBigDecimal("INSTAMT01"));
		L2CollectionReader.setInstamt02(rs.getBigDecimal("INSTAMT02"));
		L2CollectionReader.setInstamt03(rs.getBigDecimal("INSTAMT03"));
		L2CollectionReader.setInstamt04(rs.getBigDecimal("INSTAMT04"));
		L2CollectionReader.setInstamt05(rs.getBigDecimal("INSTAMT05"));
		L2CollectionReader.setInstamt06(rs.getBigDecimal("INSTAMT06"));
		L2CollectionReader.setInstfreq(rs.getString("INSTFREQ")!= null ? (rs.getString("INSTFREQ").trim()) : "");
		L2CollectionReader.setChdrcnttype(rs.getString("CHDRCNTTYPE")!= null ? (rs.getString("CHDRCNTTYPE").trim()) : "");
		L2CollectionReader.setChdroccdate(rs.getInt("CHDROCCDATE"));
		L2CollectionReader.setChdrstatcode(rs.getString("CHDRSTATCODE")!= null ? (rs.getString("CHDRSTATCODE").trim()) : "");
		L2CollectionReader.setChdrpstatcode(rs.getString("CHDRPSTATCODE")!= null ? (rs.getString("CHDRPSTATCODE").trim()) : "");
		L2CollectionReader.setChdrtranno(rs.getInt("CHDRTRANNO"));
		L2CollectionReader.setChdrinsttot01(rs.getBigDecimal("CHDRINSTTOT01"));
		L2CollectionReader.setChdrinsttot02(rs.getBigDecimal("CHDRINSTTOT02"));
		L2CollectionReader.setChdrinsttot03(rs.getBigDecimal("CHDRINSTTOT03"));
		L2CollectionReader.setChdrinsttot04(rs.getBigDecimal("CHDRINSTTOT04"));
		L2CollectionReader.setChdrinsttot05(rs.getBigDecimal("CHDRINSTTOT05"));
		L2CollectionReader.setChdrinsttot06(rs.getBigDecimal("CHDRINSTTOT06"));
		L2CollectionReader.setChdroutstamt(rs.getBigDecimal("CHDROUTSTAMT"));
		L2CollectionReader.setChdragntnum(rs.getString("CHDRAGNTNUM")!= null ? (rs.getString("CHDRAGNTNUM").trim()) : "");
		L2CollectionReader.setChdrcntcurr(rs.getString("CHDRCNTCURR"));
		L2CollectionReader.setChdrptdate(rs.getInt("CHDRPTDATE"));
		L2CollectionReader.setChdrrnwlsupr(rs.getString("CHDRRNWLSUPR")!= null ? (rs.getString("CHDRRNWLSUPR").trim()) : "");
		L2CollectionReader.setChdrrnwlspfrom(rs.getInt("CHDRRNWLSPFROM"));
		L2CollectionReader.setChdrrnwlspto(rs.getInt("CHDRRNWLSPTO"));
		L2CollectionReader.setChdrcowncoy(rs.getString("CHDRCOWNCOY")!= null ? (rs.getString("CHDRCOWNCOY").trim()) : "");
		L2CollectionReader.setChdrcownnum(rs.getString("CHDRCOWNNUM")!= null ? (rs.getString("CHDRCOWNNUM").trim()) : "");
		L2CollectionReader.setChdrcntbranch(rs.getString("CHDRCNTBRANCH")!= null ? (rs.getString("CHDRCNTBRANCH").trim()) : "");
		L2CollectionReader.setChdragntcoy(rs.getString("CHDRAGNTCOY")!= null ? (rs.getString("CHDRAGNTCOY").trim()) : "");
		L2CollectionReader.setChdrregister(rs.getString("CHDRREGISTER")!= null ? (rs.getString("CHDRREGISTER").trim()) : "");
		L2CollectionReader.setChdrchdrpfx(rs.getString("CHDRCHDRPFX")!= null ? (rs.getString("CHDRCHDRPFX").trim()) : "");
		L2CollectionReader.setPayrtaxrelmth(rs.getString("PAYRTAXRELMTH")!= null ? (rs.getString("PAYRTAXRELMTH").trim()) : "");
		L2CollectionReader.setPayrincomeseqno(rs.getInt("PAYRINCOMESEQNO"));
		L2CollectionReader.setPayrincomeseqno(rs.getInt("PAYRTRANNO"));
		L2CollectionReader.setPayroutstamt(rs.getBigDecimal("PAYROUTSTAMT"));
		L2CollectionReader.setPayrbillchnl(rs.getString("PAYRBILLCHNL")!= null ? (rs.getString("PAYRBILLCHNL").trim()) : "");
		L2CollectionReader.setPayrmandref(rs.getString("PAYRMANDREF")!= null ? (rs.getString("PAYRMANDREF").trim()) : "");
		L2CollectionReader.setPayrcntcurr(rs.getString("PAYRCNTCURR")!= null ? (rs.getString("PAYRCNTCURR").trim()) : "");
		L2CollectionReader.setPayrbillfreq(rs.getString("PAYRBILLFREQ")!= null ? (rs.getString("PAYRBILLFREQ").trim()) : "");
		L2CollectionReader.setPayrbillcd(rs.getInt("PAYRBILLCD"));
		L2CollectionReader.setPayrptdate(rs.getInt("PAYRPTDATE"));
		L2CollectionReader.setPayrbtdate(rs.getInt("PAYRBTDATE"));
		L2CollectionReader.setPayrnextdate(rs.getInt("PAYRNEXTDATE"));
		L2CollectionReader.setLsinstto(rs.getInt("LSINSTTO"));
		L2CollectionReader.setCunique_number(rs.getLong("CUNIQUE_NUMBER"));
		L2CollectionReader.setLsunique_number(rs.getLong("LSUNIQUE_NUMBER"));
		L2CollectionReader.setPsunique_number(rs.getLong("PSUNIQUE_NUMBER"));
		L2CollectionReader.setAglfdteapp(rs.getInt("AGLFDTEAPP"));
		L2CollectionReader.setAglfdteexp(rs.getInt("AGLFDTEEXP"));
		L2CollectionReader.setAgldtetrm(rs.getInt("AGLDTETRM"));
		L2CollectionReader.setHcsdzcshdivmth(rs.getString("HCSDZCSHDIVMTH"));
		L2CollectionReader.setHcsdzdivopt(rs.getString("HCSDZDIVOPT"));
		L2CollectionReader.setHdishcapndt(rs.getInt("HDISHCAPNDT"));
		L2CollectionReader.setLinsUniqueNumber(rs.getLong("LINSUNIQUENUMBER"));
		L2CollectionReader.setPayrUniqueNumber(rs.getLong("PAYRUNIQUENUMBER"));
	
		
		return L2CollectionReader;
	}
}
