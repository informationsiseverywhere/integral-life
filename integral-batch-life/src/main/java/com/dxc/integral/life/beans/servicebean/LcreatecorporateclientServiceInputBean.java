package com.dxc.integral.life.beans.servicebean;
import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.dxc.integral.life.beans.in.*;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LcreatecorporateclientServiceInputBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private AuthenticationInfo authenticationInfo;
	private String company;
	private String startScreen;
	private S2480 s2480;
	private List<S2466> s2466;
	private List<S2472> s2472;
	private List<S2462> s2462;
	private List<Sr2d2> sr2d2;
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}
	public S2480 getS2480() {
		return s2480;
	}
	public void setS2480(S2480 s2480) {
		this.s2480 = s2480;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getStartScreen() {
		return startScreen;
	}
	public void setStartScreen(String startScreen) {
		this.startScreen = startScreen;
	}
	public List<S2466> getS2466() {
		return s2466;
	}
	public void setS2466(List<S2466> s2466) {
		this.s2466 = s2466;
	}
	public List<S2472> getS2472() {
		return s2472;
	}
	public void setS2472(List<S2472> s2472) {
		this.s2472 = s2472;
	}
	
	public List<S2462> getS2462() {
		return s2462;
	}
	public void setS2462(List<S2462> s2462) {
		this.s2462 = s2462;
	}
	public List<Sr2d2>  getSr2d2() {
		return sr2d2;
	}
	public void setSr2d2(List<Sr2d2> sr2d2) {
		this.sr2d2 = sr2d2;
	}
	@Override
	public String toString() {
		return "lcreatecorporateclientServiceInputBean []";
	}

	
}