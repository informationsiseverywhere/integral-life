package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.L2ownchgProcessorDTO;
import com.dxc.integral.life.beans.L2ownchgReaderDTO;
import com.dxc.integral.life.beans.L2ownchgReportDTO;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.utils.L2ownchgProcessorUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * L2OWNCHG processor utility implementation.
 * 
 * @author wli3
 *
 */

@Service("l2ownchgProcessorUtil")
@Lazy
public class L2ownchgProcessorUtilImpl implements L2ownchgProcessorUtil {

 	@Autowired
	private Zrdecplc zrdecplc;
	
	@Autowired
	private Datcon2 datcon2;
	
	@Autowired
	private ItempfDAO itempfDAO;
	
	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.L2ownchgProcessorUtil#checkValidCoverage(com.dxc.integral.life.beans.L2ownchgReaderDTO)
	 */
	@Override
	public boolean checkValidContractStatus(L2ownchgReaderDTO readerDTO) throws IOException {
		boolean validCoverage = false;
		Map<String, Itempf> t5679Map = itempfDAO.readSmartTableByTableName2(readerDTO.getChdrcoy(), "T5679", CommonConstants.IT_ITEMPFX);
		if(null==t5679Map.get(readerDTO.getBatctrcde())){
			throw new ItemNotfoundException("Br629: Item "+readerDTO.getBatctrcde()+" not found in Table T5679 while processing contract "+readerDTO.getChdrnum());
		}
		T5679 t5679 = new ObjectMapper().readValue(t5679Map.get(readerDTO.getBatctrcde()).getGenareaj(),
				T5679.class);
		for (int i = 0; i < t5679.getCnRiskStats().size(); i++) {
			if (t5679.getCnRiskStats().get(i).equals(readerDTO.getStatcode())) {
				for (int j = 0; j < t5679.getCnPremStats().size(); j++) {
					if (t5679.getCnPremStats().get(j).equals(readerDTO.getPstcde())) {
						validCoverage = true;
						break;
					}
				}
			}
		}
		return validCoverage;
	}
	
	@Override
	public L2ownchgReportDTO initReportMap(L2ownchgReaderDTO readerDTO,L2ownchgReportDTO reportDTO, int wsaaNumber) throws IOException{
		L2ownchgReportDTO l2ownchgReportDTO = new L2ownchgReportDTO();
		l2ownchgReportDTO.setZseqno(wsaaNumber);
		l2ownchgReportDTO.setChdrnum(readerDTO.getChdrnum());
		l2ownchgReportDTO.setCownnum(readerDTO.getCownnum());
		l2ownchgReportDTO.setCnttype(readerDTO.getCnttype());
		l2ownchgReportDTO.setOccdate(reportDTO.getOccdate());
		l2ownchgReportDTO.setEffdates(reportDTO.getEffdates());
		l2ownchgReportDTO.setRname(reportDTO.getRname());
		l2ownchgReportDTO.setLname(reportDTO.getLname());
		l2ownchgReportDTO.setClntlname(readerDTO.getCl1lgivname());
		l2ownchgReportDTO.setCownnum(readerDTO.getLifcnum());
		l2ownchgReportDTO.setLifcnum(readerDTO.getLifcnum());
		l2ownchgReportDTO.setCltdob(null);
		return l2ownchgReportDTO;
	}
	@Override
	public L2ownchgReaderDTO updateReaderDTOInfo(L2ownchgReaderDTO readerIO,Batcpf batcpf){
		L2ownchgReaderDTO readerDTO = readerIO;
		int tranno = readerIO.getTranno() + 1;
		//update chdrpf
		readerDTO.setChdrcoy(readerIO.getChdrcoy());
		readerDTO.setChdrnum(readerIO.getChdrnum());
		readerDTO.setChdrpfx(readerIO.getChdrpfx());
		readerDTO.setValidflagforUpdate("2");
		//readerDTO.setCurrto(readerIO.getEffectiveDate().toString());
		//insert chdrpf
		readerDTO.setTranno(tranno);
		readerDTO.setTranid(batcpf.getTermid());
		readerDTO.setValidflagforInsert("1");
		readerDTO.setCownnum(readerIO.getLifcnum());
		readerDTO.setUniqueNumber(readerIO.getUniqueNumber());
		readerDTO.setJobnm(readerIO.getJobnm());
		readerDTO.setUsrprf(readerIO.getUsrprf());
		//insert ptrnpf
		readerDTO.setBatcactmn(batcpf.getBatcactmn());
		readerDTO.setBatcpfx(batcpf.getBatcpfx());
		readerDTO.setBatccoy(batcpf.getBatccoy());
		readerDTO.setBatcbrn(batcpf.getBatcbrn());
		readerDTO.setBatcactyr(batcpf.getBatcactyr());
		readerDTO.setBatcactmn(batcpf.getBatcactmn());
		readerDTO.setBatcbatch(batcpf.getBatcbatch());
		readerDTO.setBatctrcde(batcpf.getBatctrcde());
		readerDTO.setTranno(tranno);
		readerDTO.setTrdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
		readerDTO.setTrtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
		readerDTO.setPtrneff(readerIO.getEffectiveDate());
		readerDTO.setDatesub(readerIO.getEffectiveDate());
		readerDTO.setTermid("");
		readerDTO.setUserT(999999);
		//update hpadpf
		readerDTO.setProcflg("Y");
		return readerDTO;
	}
}
