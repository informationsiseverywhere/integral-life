package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.L2AnnyProcProcessorDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface L2AnnyProcProcessorUtil {
	L2AnnyProcProcessorDTO loadTableDetails(L2AnnyProcProcessorDTO processorDTO) throws JsonParseException, JsonMappingException, IOException;
	L2AnnyProcProcessorDTO validComponent(L2AnnyProcProcessorDTO processorDTO) ;
	L2AnnyProcProcessorDTO processUpdate(L2AnnyProcProcessorDTO processorDTO);
}
