package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2AutoIncrReaderDTO;

public class L2AutoIncrReaderRowMapper implements RowMapper<L2AutoIncrReaderDTO> { 
	@Override
	public L2AutoIncrReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException 
	{
		L2AutoIncrReaderDTO l2autoIncrReader = new L2AutoIncrReaderDTO();
		l2autoIncrReader.setChdrCoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
		l2autoIncrReader.setChdrNum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");
		l2autoIncrReader.setjLife(rs.getString("JLIFE"));
		l2autoIncrReader.setLife(rs.getString("LIFE"));
		l2autoIncrReader.setCoverage(rs.getString("COVERAGE"));
		l2autoIncrReader.setRider(rs.getString("RIDER"));
		l2autoIncrReader.setIndxin(rs.getString("INDXIN"));
		l2autoIncrReader.setPlnSfx(rs.getInt("PLNSFX"));
		l2autoIncrReader.setStatCode(rs.getString("STATCODE")!= null ? (rs.getString("STATCODE").trim()) : "");				
		l2autoIncrReader.setPstatCode(rs.getString("PSTATCODE")!= null ? (rs.getString("PSTATCODE").trim()) : "");
		l2autoIncrReader.setCrrcd(rs.getInt("CRRCD"));
		l2autoIncrReader.setCrtable(rs.getString("CRTABLE"));
		l2autoIncrReader.setMortCls(rs.getString("MORTCLS"));
		l2autoIncrReader.setPrmcur(rs.getString("PRMCUR"));
		l2autoIncrReader.setRcesdte(rs.getInt("RCESDTE"));
		l2autoIncrReader.setSumIns(rs.getBigDecimal("SUMINS"));
		l2autoIncrReader.setTpdType(rs.getString("TPDTYPE"));
		l2autoIncrReader.setZstpDuty01(rs.getBigDecimal("ZSTPDUTY01"));		
		l2autoIncrReader.setInstPrem(rs.getBigDecimal("INSTPREM"));
		l2autoIncrReader.setZlinstPrem(rs.getBigDecimal("ZLINSTPREM"));
		l2autoIncrReader.setZbinstPrem(rs.getBigDecimal("ZBINSTPREM"));
		l2autoIncrReader.setCpiDte(rs.getInt("CPIDTE"));
		l2autoIncrReader.setValidFlag(rs.getString("VALIDFLAG"));
		l2autoIncrReader.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
		
		return l2autoIncrReader;
	}
}
