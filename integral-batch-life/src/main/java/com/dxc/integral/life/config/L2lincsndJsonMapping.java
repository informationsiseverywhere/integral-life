package com.dxc.integral.life.config;

import java.util.Map;

/**
 * Maps fields to L2lincsndMapping.json file.
 * 
 * @author gsaluja2
 *
 */
public class L2lincsndJsonMapping {
	
	L2lincsndJsonMapping(){
		super();
	}

	private Map<String, ?> file;
	private Map<String, ?> header;
	private Map<String, String> trailer;
	private Map<String, String> end;
	private Map<String, String> dataFormat;

	/**
	 * @return the file
	 */
	public Map<String, ?> getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(Map<String, ?> file) {
		this.file = file;
	}

	/**
	 * @return the header
	 */
	public Map<String, ?> getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(Map<String, ?> header) {
		this.header = header;
	}

	/**
	 * @return the trailer
	 */
	public Map<String, String> getTrailer() {
		return trailer;
	}

	/**
	 * @param trailer the trailer to set
	 */
	public void setTrailer(Map<String, String> trailer) {
		this.trailer = trailer;
	}

	/**
	 * @return the end
	 */
	public Map<String, String> getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(Map<String, String> end) {
		this.end = end;
	}

	/**
	 * @return the dataFormat
	 */
	public Map<String, String> getDataFormat() {
		return dataFormat;
	}

	/**
	 * @param dataFormat the dataFormat to set
	 */
	public void setDataFormat(Map<String, String> dataFormat) {
		this.dataFormat = dataFormat;
	}
}
