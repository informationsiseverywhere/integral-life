package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.L2WaiverRerateProcessorDTO;
import com.dxc.integral.life.beans.L2WaiverRerateReaderDTO;

public interface L2WaiverRerateItemProcessorUtil {
	
	public L2WaiverRerateProcessorDTO loadTableDetails(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO);
	public L2WaiverRerateProcessorDTO checkValidContract(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO);
	public L2WaiverRerateProcessorDTO processValidCoverage(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO);

}
