/**
 * 
 */
package com.dxc.integral.life.general.processors;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.dxc.integral.fsu.dao.AgntcypfDAO;
import com.dxc.integral.fsu.dao.model.Agntcypf;
import com.dxc.integral.fsu.utils.DoubleByteToSingleByte;
import com.dxc.integral.life.beans.L2CreateContractReaderDTO;
import com.dxc.integral.life.beans.in.*;
import com.dxc.integral.life.beans.in.S5003.S5003screensfl;
import com.dxc.integral.life.beans.in.S5006.S5006screensfl;
import com.dxc.integral.life.beans.in.S5007.S5007screensfl;
import com.dxc.integral.life.beans.in.S5010.S5010screensfl;
import com.dxc.integral.life.beans.in.Sr2d2.Sr2d2screensfl;
import com.dxc.integral.life.beans.in.Sr5ah.Sr5ahscreensfl;
import com.dxc.integral.life.beans.in.Sr626.Sr626screensfl;
import com.dxc.integral.life.beans.out.L2CreateContractExceptionsBean;
import com.dxc.integral.life.beans.out.L2ProposalsCreatedBean;
import com.dxc.integral.life.beans.servicebean.L2ContractCreateGroupedServiceInputBean;
import com.dxc.integral.life.beans.servicebean.LcreatecorporateclientServiceInputBean;
import com.dxc.integral.life.beans.servicebean.LcreatenewcontractidvServiceInputBean;
import com.dxc.integral.life.beans.servicebean.LcreatepersonalclientServiceInputBean;
import com.dxc.integral.life.utils.L2CreateContractProcessorUtil;
import com.dxc.integral.iaf.constants.Companies;
import org.apache.commons.lang3.StringUtils;

@StepScope
public class L2CreateContractProcessor implements ItemProcessor<L2CreateContractReaderDTO , L2ProposalsCreatedBean>{

	private static final Logger LOGGER = LoggerFactory.getLogger(L2CreateContractProcessor.class);
	private static final String SR626= "sr626";
	private static final String S5010= "s5010";
	private static final String S5004= "s5004";
	private static final String SFLROWS= "sflRows";
	private static final String FIELD_NAME= "fieldName";
	private static final String MESSAGE= "message";
	private static final String DEBIT= "D";
	private static final String S5123_SCREEN= "P5123";
	private static final String S6326_SCREEN= "P6326";
	private static final String S5125_SCREEN= "P5125";
	private static final String SR676_SCREEN= "PR676";
	private static final String SR516_SCREEN= "PR516";
	private static final String SH504_SCREEN= "PH504";
	private static final char END_MESSAGE= ';';
	private static final char END_FIELDNAME= '-';
	
	@Value("#{jobParameters['userName']}") 
	private String username;
	
	@Value("#{jobParameters['trandate']}")
	private String effDate;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private AgntcypfDAO agntcypfDAO;
	
	@Autowired
	private L2CreateContractProcessorUtil l2CreateContractProcessorUtil;
	private L2ProposalsCreatedBean responseDTO;
	private L2CreateContractExceptionsBean l2CreateContractExceptionsBean;
	private AuthenticationInfo authenticationInfo;
	private HttpHeaders headers = new HttpHeaders();
	private LcreatenewcontractidvServiceInputBean lCreateNewContractIdvServiceDTO;
	private StringBuilder errorMessage;
	private String fundCrtable="SGX1";
	private String contractOwnerStatus;
	private String insuredStatus;
	private Map<String, String> guardianStatus;
	private Map<String, String> beneficiaryStatus;
	private List<KeyValueItem> item2; 
	private List<KeyValueItem> item;
	private KeyValueItem keyitemData;
	private List<S5123> s5123DTO; 
	private List<S6326> s6326DTO;
	private List<S5417> s5417DTO;
	private List<Sr676> sr676DTO;
	private List<Sr516> sr516DTO;
	private List<Sh504> sh504DTO;
	private List<S5125> s5125DTO;
	private List<Sr5ah> sr5ahDTO;
	private List<Sr5ai> sr5aiDTO;
	private int position; 
	private int index2;
	private int arraycount;
	private CheckInfoClientInput checkInfoClientInputBean;
	private L2ContractCreateGroupedServiceInputBean l2ContractCreateGroupedServiceInputBean;
	private CheckExistingClientInput checkExistingOwnerInput;
	private CheckExistingClientInput checkExistingLifeAssuredInput;
	private List<CheckExistingClientInput> checkExistingBeneficiariesInput;
	private List<CheckExistingClientInput> checkExistingTrusteesInput;
	private DoubleByteToSingleByte doubleByteToSingleByte =  new DoubleByteToSingleByte();
	private final int beneficiaryCount = 10;
	private final int guardianCount = 2;
	private Map<String, List<String>> resultantMap;
	private final String beneficiary = "Beneficiary";
	private final String lifeAssured = "LifeAssured";
	private final String owner = "Owner";
	private final String guardian = "Guardian";
	private final String output = "output";
	private final String errorMessages = "errorMessages";
	private final String result = "result";
	private Map<String, String> jsonNodeParentMap;
	
	@Override
	public L2ProposalsCreatedBean process(L2CreateContractReaderDTO readerDTO) throws Exception {

		LOGGER.info("Processing starts for contract : ");
		lCreateNewContractIdvServiceDTO = new LcreatenewcontractidvServiceInputBean();
		checkInfoClientInputBean = new CheckInfoClientInput();
		l2ContractCreateGroupedServiceInputBean = new L2ContractCreateGroupedServiceInputBean();
		contractOwnerStatus = StringUtils.SPACE;
		insuredStatus = StringUtils.SPACE;
		guardianStatus = new HashMap<>();
		beneficiaryStatus = new HashMap<>();
		l2CreateContractExceptionsBean = new L2CreateContractExceptionsBean();
		responseDTO = new L2ProposalsCreatedBean();
		errorMessage = new StringBuilder();
		processCreateContract(readerDTO);
		responseDTO.setL2CreateContractExceptionsBean(l2CreateContractExceptionsBean);
		
		return responseDTO;
	}

	public void processCreateContract(L2CreateContractReaderDTO readerDTO){
		
		authenticationInfo = new AuthenticationInfo();
		authenticationInfo.setUsername(username);
		lCreateNewContractIdvServiceDTO.setAuthenticationInfo(authenticationInfo);
		S5002 s5002DTO = new S5002();
		s5002DTO.setChdrtype(readerDTO.getInsuranceTypeCode());
		s5002DTO.setAction("A");
		lCreateNewContractIdvServiceDTO.setS5002(s5002DTO);
		getS5004Details(readerDTO);	

		if(DEBIT.equals(readerDTO.getMethodOfPayment())){
			getDirectDebitDetails(readerDTO);
		}
		if((!StringUtils.SPACE.equals(readerDTO.getBeneficiaryType().get(0)) && !StringUtils.EMPTY.equals(readerDTO.getBeneficiaryType().get(0)))
		||(!StringUtils.SPACE.equals(readerDTO.getBeneficiaryLastNameKana().get(0)) && !StringUtils.SPACE.equals(readerDTO.getBeneficiaryFirstNameKana().get(0))
			&& !StringUtils.EMPTY.equals(readerDTO.getBeneficiaryLastNameKana().get(0)) && !StringUtils.EMPTY.equals(readerDTO.getBeneficiaryFirstNameKana().get(0)))){
			
			getS5010Details(readerDTO);
		}
		if(!readerDTO.getGuardianInsuredPersonReasonCode().isEmpty() || !readerDTO.getGuardianContractOwnerReasonCode().isEmpty()
			|| 	!readerDTO.getGuardianContractOwnerLastNameKana().isEmpty() || !readerDTO.getGuardianInsuredPersonLastNameKana().isEmpty()){
		
			getSr626Details(readerDTO);
		}
		
		if(!readerDTO.getAgentNumber1().isEmpty()){
			getCommissionSplitDetails(readerDTO);
		}
		getS5005Details(readerDTO);
		
		getAllCovrsData(readerDTO);
	
		getS5003Details();

		S6378 s6378Data = new S6378();
		lCreateNewContractIdvServiceDTO.setS6378(s6378Data);
		checkInfoClientInputBean.setCheckExistingBeneficiariesInput(checkExistingBeneficiariesInput);
		checkInfoClientInputBean.setCheckExistingLifeAssuredInput(checkExistingLifeAssuredInput);
		checkInfoClientInputBean.setCheckExistingOwnerInput(checkExistingOwnerInput);
		checkInfoClientInputBean.setCheckExistingTrusteesInput(checkExistingTrusteesInput);
		l2ContractCreateGroupedServiceInputBean.setAuthenticationInfo(authenticationInfo);
		l2ContractCreateGroupedServiceInputBean.setCheckInfoClientInput(checkInfoClientInputBean);
		l2ContractCreateGroupedServiceInputBean.setLcreateNewContractIdvServiceInputBean(lCreateNewContractIdvServiceDTO);
		final String url=env.getProperty("rest.api.url")+"L2ContractCreateGroupedService";
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		headers.setContentType(MediaType.APPLICATION_JSON);
	try {	
		ObjectMapper mapper = new ObjectMapper();
		String bodyData = mapper.writeValueAsString(l2ContractCreateGroupedServiceInputBean);
		HttpEntity<String> entity = new HttpEntity<>(bodyData, headers);
		String response = restTemplate
							.exchange
							(url, HttpMethod.POST, entity, String.class,l2ContractCreateGroupedServiceInputBean)
							.getBody();
		String contractCreateGroupedService = "L2ContractCreateGroupedServiceResult";
		String contractCreateService = "LCreateNewContractIdvServiceResult";
		String checkInfoClientOutput = "checkInfoClientOutput";
		JsonNode valuesNode = mapper.readTree(response)
								.get(contractCreateGroupedService)
								.get(output);
		JsonNode valuesNodeError = valuesNode.get(contractCreateService).get(errorMessages);
		JsonNode valuesNodeOutput = valuesNode.get(contractCreateService).get(output);
		JsonNode checkInfoClientOutputNode = valuesNode.get(checkInfoClientOutput);
		iterateResponse(checkInfoClientOutputNode);
		if((null!= valuesNodeError && valuesNodeError.size()!=0) || 
				(null != (valuesNodeError = checkClientResponseError(checkInfoClientOutputNode)))) {
			generateErrorRecord(valuesNodeError);
			setErrorRecordDetails(readerDTO);
		}
		if(null != valuesNodeOutput && valuesNodeOutput.size()!=0){
			generateOutputRecord(readerDTO,valuesNodeOutput);
		 }
	}catch (IOException e) {
		 LOGGER.error("Exception in processCreateContract: {}", e.getMessage().replaceAll("[\r\n]",""));
	}
}
	
	public JsonNode checkClientResponseError(JsonNode checkInfoClientOutputNode) {
		List<String> jsonNodeList = new ArrayList<>();
		jsonNodeList.add("LCreatePersonalClientServiceResult");
		jsonNodeList.add("LCreateCorporateClientServiceResult");
		Map<String, JsonNode> errorNode = new HashMap<>();
		for(Map.Entry<String, String> jsonNodeParent: jsonNodeParentMap.entrySet()) {
			for(String jsonNode: jsonNodeList) {
				if(null != checkInfoClientOutputNode.get(jsonNodeParent.getValue()) 
						&& null != checkInfoClientOutputNode.get(jsonNodeParent.getValue()).get(jsonNode) 
						&& null != checkInfoClientOutputNode.get(jsonNodeParent.getValue()).get(jsonNode).get(errorMessages))
					return checkInfoClientOutputNode.get(jsonNodeParent.getValue()).get(jsonNode).get(errorMessages);
				else if(null != checkInfoClientOutputNode.get(jsonNodeParent.getValue()) 
						&& checkInfoClientOutputNode.get(jsonNodeParent.getValue()).isArray()) {
					checkInfoClientOutputNode.get(jsonNodeParent.getValue()).forEach(node -> {
						if(null != node.get(jsonNode) && null != node.get(jsonNode).get(errorMessages)) {
							errorNode.put(errorMessages, node.get(jsonNode).get(errorMessages));
							return;
						}
					});
				}
			}
		}
		return errorNode.get(errorMessages);
	}
	
	public void iterateResponse(JsonNode checkInfoClientOutputNode) {
		jsonNodeParentMap = new LinkedHashMap<>();
		jsonNodeParentMap.put(beneficiary, "checkExistingBeneficiariesOutput");
		jsonNodeParentMap.put(lifeAssured, "checkExistingLifeAssuredOutput");
		jsonNodeParentMap.put(owner, "checkExistingOwnerOutput");
		jsonNodeParentMap.put(guardian, "checkExistingTrusteesOutput");
		List<String> jsonNodeList = new ArrayList<>();
		jsonNodeList.add("LCheckExistingPersonalClientOutput");
		jsonNodeList.add("LCheckExistingCorporateClientOutput");
		resultantMap = new LinkedHashMap<>();
		for(Map.Entry<String, String> jsonNodeParent: jsonNodeParentMap.entrySet()) {
			for(String jsonNode: jsonNodeList) {
				if(null != checkInfoClientOutputNode.get(jsonNodeParent.getValue()))
					processResponse(jsonNode, checkInfoClientOutputNode.get(jsonNodeParent.getValue()), jsonNodeParent.getKey());
			}
		}
	}
	
	public void processResponse(String jsonNode, JsonNode checkInfoClientChildNode, 
			String key) {
		List<String> clntnumList = new LinkedList<>();
		for(JsonNode checkClientInfo: checkInfoClientChildNode) {
			if(null != checkClientInfo.get(jsonNode) && null != checkClientInfo.get(jsonNode).get(result)) {
				clntnumList.add(checkClientInfo.get(jsonNode).get(result).toString());
			}
			else if(null != checkClientInfo.get(result))
				clntnumList.add(checkClientInfo.get(result).toString());
		}
		if(!clntnumList.isEmpty() && !resultantMap.containsKey(key))
			resultantMap.put(key, clntnumList);
		else if(!clntnumList.isEmpty() && resultantMap.containsKey(key) 
				&& Collections.disjoint(resultantMap.get(key), clntnumList))
			resultantMap.get(key).addAll(clntnumList);
	}
	
	public void setErrorRecordDetails(L2CreateContractReaderDTO readerDTO){
		
		l2CreateContractExceptionsBean.setErrorMessage(errorMessage.toString());
		l2CreateContractExceptionsBean.setAgencyNumber(readerDTO.getAgencyNumber()!=null?readerDTO.getAgencyNumber():StringUtils.SPACE);
		l2CreateContractExceptionsBean.setAgentNumber(readerDTO.getAgentNumber1()!=null?readerDTO.getAgentNumber1():StringUtils.SPACE);
		l2CreateContractExceptionsBean.setContractCurrency(readerDTO.getContractCurrency()!=null?readerDTO.getContractCurrency():StringUtils.SPACE);
		l2CreateContractExceptionsBean.setContractOwnerNameKana((readerDTO.getContractOwnerLastNameKana()!=null?readerDTO.getContractOwnerLastNameKana():StringUtils.SPACE)+" "+(readerDTO.getContractOwnerFirstNameKana()!=null?readerDTO.getContractOwnerFirstNameKana():StringUtils.SPACE));
		l2CreateContractExceptionsBean.setContractOwnerNameKanji((readerDTO.getContractOwnerLastNameKanji()!=null?readerDTO.getContractOwnerLastNameKanji():StringUtils.SPACE)+" "+(readerDTO.getContractOwnerFirstNameKanji()!=null?readerDTO.getContractOwnerFirstNameKanji():StringUtils.SPACE));
		l2CreateContractExceptionsBean.setContractType(readerDTO.getInsuranceTypeCode()!=null?readerDTO.getInsuranceTypeCode():StringUtils.SPACE);
		l2CreateContractExceptionsBean.setEffDateOfBatchRun(effDate);
		l2CreateContractExceptionsBean.setProposalNumber(readerDTO.getProposalNumber()!=null?readerDTO.getProposalNumber():StringUtils.SPACE);   
	}
	
	public void generateErrorRecord(JsonNode valuesNodeError){
		
		if(valuesNodeError.isArray()){
			for (final JsonNode dataField : valuesNodeError) {
				if(null!=dataField.get(FIELD_NAME)){
					errorMessage.append(dataField.get(FIELD_NAME).asText().trim());
					errorMessage.append(END_FIELDNAME);
				}
				if(null!=dataField.get(MESSAGE)){
					errorMessage.append(dataField.get(MESSAGE).asText().trim());
					errorMessage.append(END_MESSAGE);
				}
			 }
		  }else if(valuesNodeError.isObject()){
			  if(null!=valuesNodeError.get(FIELD_NAME)){
				  errorMessage.append(valuesNodeError.get(FIELD_NAME).asText().trim());
				  errorMessage.append(END_FIELDNAME);
			  }
			  if(null!=valuesNodeError.get(MESSAGE)){
				  errorMessage.append(valuesNodeError.get(MESSAGE).asText().trim());
			  	  errorMessage.append(END_MESSAGE);
			  }
		  }
	}
	
	public void generateOutputRecord(L2CreateContractReaderDTO readerDTO,JsonNode valuesNodeOutput){
			
		setFields();
		responseDTO.setContractType(valuesNodeOutput.get("s5002").get("chdrtype").asText().trim());
		if(valuesNodeOutput.get(S5004).isArray()){
			ArrayNode arrayNode = (ArrayNode) valuesNodeOutput.get(S5004);
			Iterator<JsonNode> node = arrayNode.elements();	
			while (node.hasNext()) {
				JsonNode dataField = node.next();
				setupS5004Output(readerDTO,dataField);
				break;
			}
		}
		else if(valuesNodeOutput.get(S5004).isObject()){
			JsonNode dataField = valuesNodeOutput.get(S5004);
			setupS5004Output(readerDTO,dataField);
			
		}
		List<String> bensel = new LinkedList<>(Collections.nCopies(beneficiaryCount, StringUtils.SPACE));
		List<String> newOrExistingBeneficiary = new LinkedList<>(Collections.nCopies(beneficiaryCount, StringUtils.SPACE));
		List<String> beneficiaryNameKana = new LinkedList<>(Collections.nCopies(beneficiaryCount, StringUtils.SPACE));
		List<String> beneficiaryNameKanji = new LinkedList<>(Collections.nCopies(beneficiaryCount, StringUtils.SPACE));
		if(null != valuesNodeOutput.get(S5010) && valuesNodeOutput.get(S5010).size()!=0 && null!=valuesNodeOutput.get(S5010).get(SFLROWS) && valuesNodeOutput.get(S5010).get(SFLROWS).size()!=0){
			for(int i=0; i<beneficiaryCount; i++) {
				bensel.set(i, valuesNodeOutput.get(S5010).get(SFLROWS).get(i).get("bnysel").asText().trim());
				newOrExistingBeneficiary.set(i, beneficiaryStatus.get(bensel.get(i)));
				beneficiaryNameKana.set(i, readerDTO.getBeneficiaryLastNameKana().get(i)+ StringUtils.SPACE + readerDTO.getBeneficiaryFirstNameKana().get(i));
				beneficiaryNameKanji.set(i, 
						readerDTO.getBeneficiaryLastNameKanji().get(i)+StringUtils.SPACE + readerDTO.getBeneficiaryFirstNameKanji().get(i));
			}
		}
		responseDTO.setBeneficiary(bensel);
		responseDTO.setBeneficiaryNameKana(beneficiaryNameKana);
		responseDTO.setBeneficiaryNameKanji(beneficiaryNameKanji);
		List<String> guardsel = new LinkedList<>(Collections.nCopies(2, StringUtils.SPACE));
		List<String> newOrExistingGuardian = new LinkedList<>(Collections.nCopies(2, StringUtils.SPACE));
		List<String> guardianNameKana = new LinkedList<>(Collections.nCopies(2, StringUtils.SPACE));
		List<String> guardianNameKanji = new LinkedList<>(Collections.nCopies(2, StringUtils.SPACE));
		int i=0;
		if(null != valuesNodeOutput.get(SR626) && valuesNodeOutput.get(SR626).size()!=0 && null!=valuesNodeOutput.get(SR626).get(SFLROWS) && valuesNodeOutput.get(SR626).get(SFLROWS).size()!=0){	
			if(!readerDTO.getGuardianContractOwnerFirstNameKana().isEmpty() ||
					!readerDTO.getGuardianContractOwnerLastNameKana().isEmpty()){
				guardsel.set(i, valuesNodeOutput.get(SR626).get(SFLROWS).get(i).get("clntnum").asText().trim());
				newOrExistingGuardian.set(i, guardianStatus.get(guardsel.get(i)));
				guardianNameKana.set(i, 
						readerDTO.getGuardianContractOwnerLastNameKana() + StringUtils.SPACE +readerDTO.getGuardianContractOwnerFirstNameKana());
				guardianNameKanji.set(i, 
						readerDTO.getGuardianContractOwnerLastNameKanji() + StringUtils.SPACE + readerDTO.getGuardianContractOwnerFirstNameKanji());
				i++;
			}
			if(!readerDTO.getGuardianInsuredPersonFirstNameKana().isEmpty() ||
					!readerDTO.getGuardianInsuredPersonLastNameKana().isEmpty()) {
				guardsel.set(i, valuesNodeOutput.get(SR626).get(SFLROWS).get(i).get("clntnum").asText().trim());
				newOrExistingGuardian.set(i, guardianStatus.get(guardsel.get(i)));
				guardianNameKana.set(i, 
						readerDTO.getGuardianInsuredPersonLastNameKana() + StringUtils.SPACE +readerDTO.getGuardianInsuredPersonFirstNameKana());
				guardianNameKanji.set(i, 
						readerDTO.getGuardianInsuredPersonLastNameKanji() + StringUtils.SPACE + readerDTO.getGuardianInsuredPersonFirstNameKanji());
			}
		}
		responseDTO.setGuardian(guardsel);
		responseDTO.setGuardianNameKana(guardianNameKana);
		responseDTO.setGuardianNameKanji(guardianNameKanji);
		if(null != valuesNodeOutput.get("s5005")){
			JsonNode lifeNode = valuesNodeOutput.get("s5005").get(0);
			responseDTO.setLifeAssured(lifeNode.get("lifesel").asText().trim());
			responseDTO.setLifeAssuredNameKana(readerDTO.getInsuredLastNameKana() + StringUtils.SPACE + readerDTO.getInsuredNameKana());
			responseDTO.setLifeAssuredNameKanji(readerDTO.getInsuredLastNameKanji() + StringUtils.SPACE + readerDTO.getInsuredNameKanji());
		}
		if(!resultantMap.isEmpty()) {
			setClientStatuses();
		}
	}
	
	public void setupS5004Output(L2CreateContractReaderDTO readerDTO, JsonNode dataField){
		
		responseDTO.setAgentNumber(dataField.get("agntsel").asText().trim());
		if(!StringUtils.SPACE.equals(responseDTO.getAgentNumber()) && !StringUtils.EMPTY.equals(responseDTO.getAgentNumber())){
			List<Agntcypf> agntcypfList = agntcypfDAO.getAgntCynum(Companies.LIFE, responseDTO.getAgentNumber());
			if(null!=agntcypfList && !agntcypfList.isEmpty())
				responseDTO.setAgencyNumber(agntcypfList.get(0).getAgncynum());
		}
		responseDTO.setBillingFrequency(dataField.get("billfreq").asText().trim());
		responseDTO.setContractCurrency(dataField.get("cntcurr").asText().trim());
		responseDTO.setContractDate(dataField.get("occdateDisp").asText().trim());
		responseDTO.setContractNumber(dataField.get("chdrnum").asText().trim());
		if(null != readerDTO.getProposalNumber())
			responseDTO.setProposalNumber(dataField.get("ttmprcno").asText().trim());
		responseDTO.setMethodOfPayment(dataField.get("mop").asText().trim());
		responseDTO.setContractOwner(dataField.get("ownersel").asText().trim());
		responseDTO.setContractOwnerNameKana(readerDTO.getContractOwnerLastNameKana()+ StringUtils.SPACE + readerDTO.getContractOwnerFirstNameKana());
		responseDTO.setContractOwnerNameKanji(readerDTO.getContractOwnerLastNameKanji()+ StringUtils.SPACE + readerDTO.getContractOwnerFirstNameKanji());
		responseDTO.setEffectiveDateofBatchRun(effDate);
	}
	
	public void setClientStatuses() {
		String newClient = "New";
		String existingClient = "Existing";
		contractOwnerStatus = newClient;
		insuredStatus = newClient;
		List<String> newOrExistingBenef = new LinkedList<>(Collections.nCopies(10, StringUtils.SPACE));
		List<String> newOrExistingGuard = new LinkedList<>(Collections.nCopies(2, StringUtils.SPACE));
		if(resultantMap.containsKey(owner)) {
			for(String clntnum: resultantMap.get(owner)) {
				if(responseDTO.getContractOwner().equals(clntnum))
					contractOwnerStatus = existingClient;
			}
		}
		if(resultantMap.containsKey(lifeAssured)) {
			for(String clntnum: resultantMap.get(lifeAssured)) {
				if(responseDTO.getLifeAssured().equals(clntnum))
					insuredStatus = existingClient;
			}
		}
		beneficiaryStatus = new HashMap<>();
		guardianStatus = new HashMap<>();
		getBeneficiaryStatus(newClient, existingClient);
		getGuardianStatus(newClient, existingClient);
		int i = 0;
		for(Map.Entry<String, String> benef: beneficiaryStatus.entrySet()) {
			newOrExistingBenef.set(i, benef.getValue());
			i++;
		}
		i = 0;
		for(Map.Entry<String, String> guard: guardianStatus.entrySet()) {
			newOrExistingGuard.set(i, guard.getValue());
			i++;
		}
		responseDTO.setNewOrExistingBenef(newOrExistingBenef);
		responseDTO.setNewOrExistingGuard(newOrExistingGuard);
		responseDTO.setNeworExistingLA(insuredStatus);
		responseDTO.setNewOrExistingOwner(contractOwnerStatus);
	}
	
	public void getBeneficiaryStatus(String newClient, String existingClient) {
		for(int i=0; i<beneficiaryCount; i++) {
			if(!responseDTO.getBeneficiary().isEmpty() && !responseDTO.getBeneficiary().get(i).trim().isEmpty())
				beneficiaryStatus.put(responseDTO.getBeneficiary().get(i).trim(), newClient);
			else
				break;
			if(!resultantMap.isEmpty() && resultantMap.containsKey(beneficiary)) {
				for(String clntnum: resultantMap.get(beneficiary)) {
					if(responseDTO.getBeneficiary().get(i).trim().equals(clntnum))
						beneficiaryStatus.put(clntnum, existingClient);
				}
			}
		}
	}
	
	public void getGuardianStatus(String newClient, String existingClient) {
		for(int i=0; i<guardianCount; i++) {
			if(!responseDTO.getGuardian().isEmpty() && !responseDTO.getGuardian().get(i).trim().isEmpty())
				guardianStatus.put(responseDTO.getGuardian().get(i).trim(), newClient);
			if(resultantMap.containsKey(guardian)) {
				for(String clntnum: resultantMap.get(guardian)) {
					if(!responseDTO.getGuardian().isEmpty() && responseDTO.getGuardian().contains(clntnum))
						guardianStatus.put(clntnum, existingClient);
				}
			}
		}
	}
	
	public void getS5004Details(L2CreateContractReaderDTO readerDTO){
		
		List<S5004> s5004DTO = new ArrayList<>();
		S5004 s5004Data = new S5004();
		s5004Data.setAgntsel(readerDTO.getAgentNumber1());
		String frequency = String.format("%02d", Integer.valueOf(readerDTO.getBillingFrequency()));
		s5004Data.setBillfreq(frequency);
		s5004Data.setOwnerOccupation(readerDTO.getContractOwnerOccupationCode());
		s5004Data.setHpropdteDisp(readerDTO.getProposalDate());
		s5004Data.setDecldateDisp(readerDTO.getDateofDeclaration());
		s5004Data.setMop(readerDTO.getMethodOfPayment());
		s5004Data.setConcommflg(readerDTO.getContractCommencementFlg());
		s5004Data.setCntcurr(readerDTO.getContractCurrency());
		s5004Data.setOwnersel(getContractOwner(readerDTO));	
		s5004Data.setRegister(readerDTO.getContractCountry());
		s5004Data.setSrcebus(readerDTO.getSalesChannel());
		s5004Data.setBillcurr(readerDTO.getBillingCurrency());
		s5004Data.setHprrcvdtDisp(readerDTO.getProposalReceivedDate());
		s5004Data.setCnfirmtnIntentDateDisp(readerDTO.getDateofConfirmingIntent());
		s5004Data.setTtmprcno(readerDTO.getProposalNumber());				
		s5004DTO.add(s5004Data);
		if(!readerDTO.getMethodOfPayment().isEmpty() && DEBIT.equals(readerDTO.getMethodOfPayment())){
			s5004Data = new S5004();
			s5004Data.setDdind("X");
			s5004DTO.add(s5004Data);
			s5004Data = new S5004();
			s5004DTO.add(s5004Data);
		}
		
		if(!StringUtils.SPACE.equals(readerDTO.getBeneficiaryType().get(0)) && !StringUtils.EMPTY.equals(readerDTO.getBeneficiaryType().get(0))){
			s5004Data = new S5004();
			s5004Data.setBnfying("X");	
			s5004DTO.add(s5004Data);
		}
		if(!readerDTO.getGuardianInsuredPersonReasonCode().isEmpty() || !readerDTO.getGuardianContractOwnerReasonCode().isEmpty()){
			s5004Data = new S5004();
			s5004Data.setCtrsind("X");
			s5004DTO.add(s5004Data);
		}
		if(!readerDTO.getAgentNumber1().isEmpty()){	
			s5004Data = new S5004();
			s5004Data.setComind("X");
			s5004DTO.add(s5004Data);
		}
		if(!readerDTO.getGuardianInsuredPersonReasonCode().isEmpty() || !readerDTO.getGuardianContractOwnerReasonCode().isEmpty() 
				|| (!StringUtils.SPACE.equals(readerDTO.getBeneficiaryType().get(0)) && !StringUtils.EMPTY.equals(readerDTO.getBeneficiaryType().get(0))) || !readerDTO.getAgentNumber1().isEmpty()){	
			
			s5004Data = new S5004();
			s5004DTO.add(s5004Data);
		}
		lCreateNewContractIdvServiceDTO.setS5004(s5004DTO);
	}
	
	public void getCommissionSplitDetails(L2CreateContractReaderDTO readerDTO){
		List<KeyValueItem> keyDTO = new ArrayList<>();
		index2= 1;
		if(!readerDTO.getAgentNumber1().isEmpty()){
			
			keyitemData = new KeyValueItem();
			keyitemData.setKey("s5007screensfl.agntsel_R"+index2);
			keyitemData.setValue(readerDTO.getAgentNumber1());
			keyDTO.add(keyitemData);
			
			keyitemData = new KeyValueItem();
			keyitemData.setKey("s5007screensfl.splitBcomm_R"+index2);
			keyitemData.setValue(readerDTO.getRatioHFTimeofOffering1());
			keyDTO.add(keyitemData);

			index2++;
		}
		if(!readerDTO.getAgentNumber2().isEmpty()){
			keyitemData = new KeyValueItem();
			keyitemData.setKey("s5007screensfl.agntsel_R"+index2);
			keyitemData.setValue(readerDTO.getAgentNumber2());
			keyDTO.add(keyitemData);
			keyitemData = new KeyValueItem();
			keyitemData.setKey("s5007screensfl.splitBcomm_R"+index2);
			keyitemData.setValue(readerDTO.getRatioHFTimeofOffering2());
			keyDTO.add(keyitemData);
			
			index2++;
		}
		if(!keyDTO.isEmpty()){
			List<S5007> s5007DTO = new ArrayList<>();
			S5007 s5007Data;
			S5007screensfl s5007screensflDTO = new S5007screensfl();
			s5007screensflDTO.setAttributeList(keyDTO);
			s5007Data = new S5007();
			s5007Data.setS5007screensfl(s5007screensflDTO);
			s5007DTO.add(s5007Data);
			lCreateNewContractIdvServiceDTO.setS5007(s5007DTO);
		}
	}
	
	public void getDirectDebitDetails(L2CreateContractReaderDTO readerDTO){
		
		List<S5070> s5070DTO = new ArrayList<>();
		List<S3280> s3280DTO = new ArrayList<>();
		List<S3281> s3281DTO = new ArrayList<>();
		List<S2571> s2571DTO = new ArrayList<>();
		List<S2081> s2081DTO = new ArrayList<>();
		
		S5070 s5070Data= new S5070();
		s5070Data.setActionKey("PFKEY04");
		s5070Data.setActiveField("mandref");
		s5070DTO.add(s5070Data);
		s5070Data= new S5070();
		s5070DTO.add(s5070Data);
	lCreateNewContractIdvServiceDTO.setS5070(s5070DTO);
	
	S3280 s3280Data = new S3280();
	s3280Data.setActionKey("PFKEY07");
	s3280DTO.add(s3280Data);
	lCreateNewContractIdvServiceDTO.setS3280(s3280DTO);
	
	S3281 s3281Data = new S3281();
	s3281Data.setActionKey("PFKEY04");
	s3281Data.setActiveField("bankacckey");
	s3281DTO.add(s3281Data);
	s3281Data = new S3281();
	s3281Data.setFacthous(readerDTO.getAccWithdrawlFactCompany());
	s3281Data.setBankacckey(readerDTO.getBankAccountNumber());
	s3281DTO.add(s3281Data);
	lCreateNewContractIdvServiceDTO.setS3281(s3281DTO);
	
	S2571 s2571Data =  new S2571();
	s2571Data.setActionKey("PFKEY07");
	s2571DTO.add(s2571Data);
	lCreateNewContractIdvServiceDTO.setS2571(s2571DTO);
	
	S2081 s2081Data = new S2081(); 
		s2081Data.setBankCd(String.format("%04d", Integer.valueOf(readerDTO.getFinancialInstiCDD())));
		s2081Data.setBranchCd(String.format("%03d", Integer.valueOf(readerDTO.getBranchCode())));
		s2081Data.setFacthous(readerDTO.getAccWithdrawlFactCompany());
		s2081Data.setBankacount(readerDTO.getBankAccountNumber());
		s2081Data.setBankaccdsc(readerDTO.getBankAccountHolderLastNameKana() +" "+ readerDTO.getBankAccountHolderFirstNameKana());
		s2081Data.setDatefromDisp(readerDTO.getProposalDate());
		s2081Data.setBnkactyp(readerDTO.getBankAccountType());
		s2081Data.setZpbacno(readerDTO.getPostBankAccountNumber());
		s2081Data.setZpbcode(readerDTO.getPostBankAccountType());
	s2081DTO.add(s2081Data);
	lCreateNewContractIdvServiceDTO.setS2081(s2081DTO);
  }
	public void getS5010Details(L2CreateContractReaderDTO readerDTO){ 
	List<S5010> s5010DTO = new ArrayList<>();
	S5010screensfl s5010screensflDTO = new S5010screensfl();
	S5010 s5010Data;
	item2  = new ArrayList<>();
	KeyValueItem item3;
	index2 = 1;
	checkExistingBeneficiariesInput = new ArrayList<>();
	for (arraycount = 0; arraycount<beneficiaryCount; arraycount++){	
		if(!readerDTO.getBeneficiaryType().get(arraycount).isEmpty()){
			item3 = new KeyValueItem();
			item3.setKey("s5010screensfl.bnytype_R"+index2);
			item3.setValue(readerDTO.getBeneficiaryType().get(arraycount));
			item2.add(item3);
		}
		item2 = checkForBenefNumber(readerDTO,item2,arraycount,index2);
		
		if(!readerDTO.getRelation().get(arraycount).isEmpty()){
			item3 = new KeyValueItem();
			item3.setKey("s5010screensfl.cltreln_R"+index2);
			item3.setValue(readerDTO.getRelation().get(arraycount));
			item2.add(item3);
		}
		if(!readerDTO.getShareType().get(arraycount).isEmpty()){
			item3= new KeyValueItem();
			item3.setKey("s5010screensfl.bnycd_R"+index2);
			item3.setValue(readerDTO.getShareType().get(arraycount));
			item2.add(item3);
		}
		if(!readerDTO.getShare().get(arraycount).isEmpty()){
			item3= new KeyValueItem();
			item3.setKey("s5010screensfl.bnypc_R"+index2);
			item3.setValue(readerDTO.getShare().get(arraycount));
			item2.add(item3);
		}
		index2++;
	}
	if(!item2.isEmpty()){
		s5010screensflDTO.setAttributeList(item2);
		s5010Data = new S5010();
		s5010Data.setS5010screensfl(s5010screensflDTO);
		s5010DTO.add(s5010Data);	
		lCreateNewContractIdvServiceDTO.setS5010(s5010DTO);
	 }
  }
	
	public List<KeyValueItem> checkForBenefNumber(L2CreateContractReaderDTO readerDTO,List<KeyValueItem> item2, int arraycount, int index2){
		KeyValueItem item3;
		boolean benefCondition1 = !readerDTO.getBeneficiaryDOB().get(arraycount).trim().isEmpty() && 
				!readerDTO.getBeneficiaryLastNameKana().get(arraycount).isEmpty() &&
				!readerDTO.getBeneficiaryFirstNameKana().get(arraycount).isEmpty(); //personal client
		boolean benefCondition2 = readerDTO.getBeneficiaryDOB().get(arraycount).trim().isEmpty() && 
				!readerDTO.getBeneficiaryLastNameKana().get(arraycount).isEmpty(); //corporate client
		if(benefCondition1 || benefCondition2){
				item3 = new KeyValueItem();
				item3.setKey("s5010screensfl.bnysel_R"+index2);
				item3.setValue(getContractBeneficiary(readerDTO, arraycount));
				item2.add(item3);
				item3 = new KeyValueItem();
				item3.setKey("s5010screensfl.effdateDisp_R"+index2);
				item3.setValue(readerDTO.getProposalDate());
				item2.add(item3);
			}
		return item2;
	}
	
	public void getS5005Details(L2CreateContractReaderDTO readerDTO){
		List<S5005> s5005DTO = new ArrayList<>();
		S5005 s5005Data = new S5005();
		s5005Data.setHeight(readerDTO.getHeight());
		s5005Data.setWeight(readerDTO.getWeight());
		s5005Data.setLifesel(getContractInsured(readerDTO));
		s5005Data.setSmoking(readerDTO.getSmokingInformation());
		s5005Data.setPursuit01(readerDTO.getPursuitCode1());
		s5005Data.setPursuit02(readerDTO.getPursuitCode2());
		s5005Data.setOccup(readerDTO.getOccupationCode());
		s5005Data.setSelection(readerDTO.getExaminationCategory());
		s5005Data.setRelationwithowner(readerDTO.getRelationshipWithPolicyholders());
 		s5005DTO.add(s5005Data);
 		s5005Data = new S5005();
 		s5005DTO.add(s5005Data);
		lCreateNewContractIdvServiceDTO.setS5005(s5005DTO);
	}
	
	public void getS5003Details(){
		
		List<S5003> s5003DTO = new ArrayList<>(); 
		S5003screensfl s5003screensflDTO = new S5003screensfl();
		item = new ArrayList<>();
		keyitemData = new KeyValueItem(); 
		keyitemData.setKey("s5003screensfl.action_R1");
		keyitemData.setValue("1");
		item.add(keyitemData);
		s5003screensflDTO.setAttributeList(item);
		
		S5003 s5003Data= new S5003();
		s5003Data.setS5003screensfl(s5003screensflDTO);
		s5003DTO.add(s5003Data);
		s5003Data = new S5003();
		s5003Data.setActionKey("PFKEY05");
		s5003DTO.add(s5003Data);
		s5003Data = new S5003();
		s5003DTO.add(s5003Data);
		lCreateNewContractIdvServiceDTO.setS5003(s5003DTO);
	}
	
	public void getSr626Details(L2CreateContractReaderDTO readerDTO){

		List<KeyValueItem> keyitemDTO = new ArrayList<>();
		index2= 1;
		checkExistingTrusteesInput = new ArrayList<>();
		if(!StringUtils.EMPTY.equals(readerDTO.getGuardianContractOwnerReasonCode())){
			
			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr626screensfl.clntnum_R"+index2);
			keyitemData.setValue(getContractOwnerGuardian(readerDTO));
			keyitemDTO.add(keyitemData);
			
			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr626screensfl.reasoncd_R"+index2);
			keyitemData.setValue(readerDTO.getGuardianContractOwnerReasonCode());
			keyitemDTO.add(keyitemData);


			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr626screensfl.datefrmDisp_R"+index2);
			keyitemData.setValue(readerDTO.getProposalDate());
			keyitemDTO.add(keyitemData);
			index2++;
		}
		if(!readerDTO.getGuardianInsuredPersonReasonCode().isEmpty()){
			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr626screensfl.clntnum_R"+index2);
			keyitemData.setValue(getContractInsurerGuardian(readerDTO));
			keyitemDTO.add(keyitemData);
			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr626screensfl.reasoncd_R"+index2);
			keyitemData.setValue(readerDTO.getGuardianInsuredPersonReasonCode());
			keyitemDTO.add(keyitemData);
			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr626screensfl.datefrmDisp_R"+index2);
			keyitemData.setValue(readerDTO.getProposalDate());
			keyitemDTO.add(keyitemData);
			
		}
		if(!keyitemDTO.isEmpty()){
			List<Sr626> sr626DTO = new ArrayList<>();
			Sr626 sr626Data;
			Sr626screensfl sr626screensflDTO = new Sr626screensfl();
			sr626screensflDTO.setAttributeList(keyitemDTO);
			sr626Data = new Sr626();
			sr626Data.setSr626screensfl(sr626screensflDTO);
			sr626DTO.add(sr626Data);
			lCreateNewContractIdvServiceDTO.setSr626(sr626DTO);
		}
	}
	
	public void getAllCovrsData(L2CreateContractReaderDTO readerDTO){
		
		List<String> dataList = l2CreateContractProcessorUtil.loadSmartTables(readerDTO);
		s5123DTO = new ArrayList<>(); 
		s6326DTO = new ArrayList<>();
		s5417DTO = new ArrayList<>();
		sr676DTO = new ArrayList<>();
		sr516DTO = new ArrayList<>();
		sh504DTO = new ArrayList<>();
		s5125DTO = new ArrayList<>();
		sr5ahDTO = new ArrayList<>();
		sr5aiDTO = new ArrayList<>();
		setS5006Details(readerDTO,dataList);
		String screenName;
		for(String crtable : dataList){
			position = readerDTO.getBaseContractAndRiderCode().indexOf(crtable);
			screenName = setT600Data(crtable);
			if(S5123_SCREEN.equals(screenName)){
				setS5123Details(readerDTO, position);
			}
			if(S6326_SCREEN.equals(screenName)){
				setS6326Details(readerDTO,position);
			}
	
			if(S5125_SCREEN.equals(screenName)){
				setS5125Details(readerDTO, position);
			}

			if(SR676_SCREEN.equals(screenName)){
				setSr676Details(readerDTO, position);
			}
				
			if(SR516_SCREEN.equals(screenName)){
				setSr516Details(readerDTO, position);	
			}

			if(SH504_SCREEN.equals(screenName)){
				setSh504Details(readerDTO, position);	
			}	
		}
	}
	
	public String setT600Data(String crtable){
		
		if(!StringUtils.SPACE.equals(crtable)){
			return l2CreateContractProcessorUtil.loadSmartTablesT600(crtable);
		}
		else{
			return StringUtils.SPACE;
		}
	}
	
	public void setS5006Details(L2CreateContractReaderDTO readerDTO,List<String> dataList){
		index2 = 1;
		item = new ArrayList<>(dataList.size());
		S5006 s5006DTO = new S5006();
		S5006screensfl s5006screensflDTO = new S5006screensfl();		
		for(String crtable : dataList){
			position = readerDTO.getBaseContractAndRiderCode().indexOf(crtable);
			keyitemData = new KeyValueItem();
			keyitemData.setKey("s5006screensfl.select_R"+index2);
			if(!StringUtils.SPACE.equals(crtable))
				keyitemData.setValue("X");
			else
				keyitemData.setValue("");
			item.add(keyitemData);
			index2++;
		}
		s5006screensflDTO.setAttributeList(item);
		s5006DTO.setS5006screensfl(s5006screensflDTO);
		lCreateNewContractIdvServiceDTO.setS5006(s5006DTO);
	}
	
	public void setS5123Details(L2CreateContractReaderDTO readerDTO, int position){
		
		S5123 s5123Data = new S5123();
		s5123Data.setMortcls(readerDTO.getMortalityClass().get(position));
		s5123Data.setPremCessTerm(readerDTO.getPremiumCessTerm().get(position));
		s5123Data.setPremCessAge(readerDTO.getPremiumCessAge().get(position));
		s5123Data.setRiskCessTerm(readerDTO.getContractCessTerm().get(position));
		s5123Data.setRiskCessAge(readerDTO.getContractCessAge().get(position));
		s5123Data.setSumin(readerDTO.getSumInsured().get(position));
		s5123Data.setInstPrem(readerDTO.getPremium().get(position));
		s5123DTO.add(s5123Data);
		lCreateNewContractIdvServiceDTO.setS5123(s5123DTO);
	}
	
	public void setS6326Details(L2CreateContractReaderDTO readerDTO, int position){
		
		S6326 s6326Data = new S6326();
		s6326Data.setMortcls(readerDTO.getMortalityClass().get(position));
		s6326Data.setPremCessTerm(readerDTO.getPremiumCessTerm().get(position));
		s6326Data.setMortcls(readerDTO.getPremiumCessAge().get(position));
		s6326Data.setSumin(readerDTO.getSumInsured().get(position));
		s6326Data.setSinglePremium(readerDTO.getPremium().get(position));
		s6326Data.setReserveUnitsInd("Y");
		s6326Data.setReserveUnitsDateDisp(readerDTO.getProposalDate());
		s6326DTO.add(s6326Data);
		lCreateNewContractIdvServiceDTO.setS6326(s6326DTO);
	S5417 s5417Data = new S5417();
	s5417Data.setVirtFundSplitMethod(StringUtils.SPACE);
	s5417Data.setUnitVirtualFund01(fundCrtable);
	s5417Data.setUnitAllocPercAmt01("100");
	s5417DTO.add(s5417Data);
	lCreateNewContractIdvServiceDTO.setS5417(s5417DTO);
	}
	
	public void setS5125Details(L2CreateContractReaderDTO readerDTO, int position){
		
		S5125 s5125Data = new S5125();
		s5125Data.setMortcls(readerDTO.getMortalityClass().get(position));
		s5125Data.setPremCessTerm(readerDTO.getPremiumCessTerm().get(position));
		s5125Data.setPremCessAge(readerDTO.getPremiumCessAge().get(position));
		s5125Data.setRiskCessTerm(readerDTO.getContractCessTerm().get(position));
		s5125Data.setRiskCessAge(readerDTO.getContractCessAge().get(position));
		s5125Data.setSumin(readerDTO.getSumInsured().get(position));
		s5125Data.setInstPrem(readerDTO.getPremium().get(position));
		s5125DTO.add(s5125Data);
		lCreateNewContractIdvServiceDTO.setS5125(s5125DTO);
	}
	
	public void setSr676Details(L2CreateContractReaderDTO readerDTO, int position){
		
		Sr676 sr676Data = new Sr676();
		sr676Data.setZunit(readerDTO.gethBNFNumbersofUnit());
		sr676Data.setBenpln(readerDTO.gethBNFPlanCode());
		sr676Data.setPremCessTerm(readerDTO.getPremiumCessTerm().get(position));
		sr676Data.setRiskCessTerm(readerDTO.getContractCessTerm().get(position));
		sr676Data.setPremCessAge(readerDTO.getPremiumCessAge().get(position));
		sr676Data.setRiskCessAge(readerDTO.getContractCessAge().get(position));
		sr676Data.setLivesno(readerDTO.gethBNFNumbersofInsuredPerson());
		sr676Data.setWaiverprem(l2CreateContractProcessorUtil.setWaiverOfPrem(readerDTO.getBaseContractAndRiderCode()));
		sr676Data.setCoverdtl("X");
		sr676DTO.add(sr676Data);
		if(!readerDTO.getExclusionReasonCode().get(0).trim().isEmpty()){
			sr676Data = new Sr676();
			sr676Data.setExclind("X");
			sr676DTO.add(sr676Data);	

		Sr5ai sr5aiData;
		for (arraycount = 0; arraycount<3; arraycount++){
			if(!readerDTO.getExclusionReasonCode().get(arraycount).isEmpty()){
			Sr5ahscreensfl sr5ahscreensflDTO = new Sr5ahscreensfl();
			Sr5ah sr5ahData = new Sr5ah();
			item2 = new ArrayList<>();
			keyitemData = new KeyValueItem();
			keyitemData.setKey("sr5ahscreensfl.select_R1");
			keyitemData.setValue("2");
			item2.add(keyitemData);
			sr5ahscreensflDTO.setAttributeList(item2);
			sr5ahData.setSr5ahscreensfl(sr5ahscreensflDTO);
			sr5ahDTO.add(sr5ahData);
			sr5aiData = new Sr5ai();
			sr5aiData.setExcda(readerDTO.getExclusionReasonCode().get(arraycount));
			sr5aiData.setExadtxt(readerDTO.getExclusionDetails().get(arraycount));
			sr5aiDTO.add(sr5aiData);
		  }		
		}	
		Sr5ah sr5ahData = new Sr5ah();
		sr5ahDTO.add(sr5ahData);
		lCreateNewContractIdvServiceDTO.setSr5ah(sr5ahDTO);
		lCreateNewContractIdvServiceDTO.setSr5ai(sr5aiDTO);
		}
		sr676Data = new Sr676();
		sr676DTO.add(sr676Data);
		lCreateNewContractIdvServiceDTO.setSr676(sr676DTO);
		Sr677 sr677Data = new Sr677();
		lCreateNewContractIdvServiceDTO.setSr677(sr677Data);
	}
	
	public void setSr516Details(L2CreateContractReaderDTO readerDTO, int position){
		
		Sr516 sr516Data = new Sr516();
		sr516Data.setMortcls(readerDTO.getMortalityClass().get(position));
		sr516Data.setPremCessTerm(readerDTO.getPremiumCessTerm().get(position));
		sr516Data.setPremCessAge(readerDTO.getPremiumCessAge().get(position));
		sr516Data.setRiskCessTerm(readerDTO.getContractCessTerm().get(position));
		sr516Data.setRiskCessAge(readerDTO.getContractCessAge().get(position));
		sr516Data.setZrsumin(readerDTO.getSumInsured().get(position));
		sr516Data.setInstPrem(readerDTO.getPremium().get(position));
		sr516DTO.add(sr516Data);
		lCreateNewContractIdvServiceDTO.setSr516(sr516DTO);
	}
	
	public void setSh504Details(L2CreateContractReaderDTO readerDTO, int position){
		
		Sh504 sh504Data = new Sh504();
		sh504Data.setMortcls(readerDTO.getMortalityClass().get(position));
		sh504Data.setPremCessTerm(readerDTO.getPremiumCessTerm().get(position));
		sh504Data.setPremCessAge(readerDTO.getPremiumCessAge().get(position));
		sh504Data.setRiskCessTerm(readerDTO.getContractCessTerm().get(position));
		sh504Data.setRiskCessAge(readerDTO.getContractCessAge().get(position));
		sh504Data.setSumin(readerDTO.getSumInsured().get(position));
		sh504Data.setInstPrem(readerDTO.getPremium().get(position));
		sh504DTO.add(sh504Data);
		lCreateNewContractIdvServiceDTO.setSh504(sh504DTO);	
	}
	
	public String getContractOwner(L2CreateContractReaderDTO readerDTO){
		checkExistingOwnerInput = new CheckExistingClientInput();
		checkExistingOwnerInput.setZkanasnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getContractOwnerLastNameKana()));
		checkExistingOwnerInput.setZkanagnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getContractOwnerFirstNameKana()));
		checkExistingOwnerInput.setDob(readerDTO.getContractOwnerDOB().trim());
		checkExistingOwnerInput.setSex(readerDTO.getContractOwnerGender().trim());
		checkExistingOwnerInput.setPostcode(readerDTO.getContactOwnerPostalCode());
		if(readerDTO.getContractOwnerDOB().isEmpty() || readerDTO.getContractOwnerGender().isEmpty())
			checkExistingOwnerInput.setZkanasnmnor(
					doubleByteToSingleByte.removeTermOfExpressionLegalPersonality(readerDTO.getContractOwnerLastNameKana()));
		LcreatepersonalclientServiceInputBean createPersonalClientInputBean = 
				new LcreatepersonalclientServiceInputBean();
		LcreatecorporateclientServiceInputBean createCorporateClientInputBean = 
				new LcreatecorporateclientServiceInputBean();
		setPersonalClientOwnerDetails(createPersonalClientInputBean, readerDTO);
		setCorporateClientOwnerDetails(createCorporateClientInputBean, readerDTO);
		S2480 s2480Data = new S2480();
		createPersonalClientInputBean.setS2480(s2480Data);
		createCorporateClientInputBean.setS2480(s2480Data);
		checkExistingOwnerInput.setlCreatePersonalClientServiceInputBean(createPersonalClientInputBean);
		checkExistingOwnerInput.setlCreateCorporateClientServiceInputBean(createCorporateClientInputBean);
		return StringUtils.SPACE;
}
	
	public void setPersonalClientOwnerDetails(LcreatepersonalclientServiceInputBean createPersonalClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2465> s2465DTO = new ArrayList<>();
		S2465 s2465Data = new S2465();
		s2465Data.setLgivname(readerDTO.getContractOwnerFirstNameKanji());
		s2465Data.setLsurname(readerDTO.getContractOwnerLastNameKanji());
		s2465Data.setZkanagivname(readerDTO.getContractOwnerFirstNameKana());
		s2465Data.setZkanasurname(readerDTO.getContractOwnerLastNameKana());
		s2465Data.setCltaddr01(readerDTO.getContactOwnerAddress1Kanji());
		s2465Data.setCltaddr02(readerDTO.getContactOwnerAddress2Kanji());
		s2465Data.setCltaddr03(readerDTO.getContactOwnerAddress3Kanji());
		s2465Data.setCltaddr04(readerDTO.getContactOwnerAddress4Kanji());
		s2465Data.setZkanacltaddr01(readerDTO.getContactOwnerAddress1Katakana());
		s2465Data.setZkanacltaddr02(readerDTO.getContactOwnerAddress2Katakana());
		s2465Data.setZkanacltaddr03(readerDTO.getContactOwnerAddress3Katakana());
		s2465Data.setZkanacltaddr04(readerDTO.getContactOwnerAddress4Katakana());
		s2465Data.setCltdobxDisp(readerDTO.getContractOwnerDOB());
		s2465Data.setCltphone01(readerDTO.getContractOwnerHomePhoneNumber1());
		s2465Data.setCltphone02(readerDTO.getContractOwnerDayPhoneNumber2());
		s2465Data.setRmblphone(readerDTO.getContractOwnerDayPhoneNumberCorporate());
		s2465Data.setCltsex(readerDTO.getContractOwnerGender());
		s2465Data.setCtrycode(readerDTO.getContactOwnerNationalityOnlyPerson());
		s2465Data.setRinternet(readerDTO.getContractOwnerEMailAddress());
		s2465Data.setCltpcode(readerDTO.getContactOwnerPostalCode());
		s2465Data.setNatlty(readerDTO.getContactOwnerNationalityOnlyPerson());
		s2465DTO.add(s2465Data);
		createPersonalClientInputBean.setS2465(s2465DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createPersonalClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}
	
	public void setCorporateClientOwnerDetails(LcreatecorporateclientServiceInputBean createCorporateClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2466> s2466DTO = new ArrayList<>();
		S2466 s2466Data = new S2466();
		s2466Data.setLgivname(readerDTO.getContractOwnerFirstNameKanji());
		s2466Data.setLsurname(readerDTO.getContractOwnerLastNameKanji());
		s2466Data.setZkanagivname(readerDTO.getContractOwnerFirstNameKana());
		s2466Data.setZkanasurname(readerDTO.getContractOwnerLastNameKana());
		s2466Data.setCltaddr01(readerDTO.getContactOwnerAddress1Kanji());
		s2466Data.setCltaddr02(readerDTO.getContactOwnerAddress2Kanji());
		s2466Data.setCltaddr03(readerDTO.getContactOwnerAddress3Kanji());
		s2466Data.setCltaddr04(readerDTO.getContactOwnerAddress4Kanji());
		s2466Data.setZkanacltaddr01(readerDTO.getContactOwnerAddress1Katakana());
		s2466Data.setZkanacltaddr02(readerDTO.getContactOwnerAddress2Katakana());
		s2466Data.setZkanacltaddr03(readerDTO.getContactOwnerAddress3Katakana());
		s2466Data.setZkanacltaddr04(readerDTO.getContactOwnerAddress4Katakana());
		s2466Data.setCltdobxDisp(readerDTO.getContractOwnerDOB());
		s2466Data.setCltphone01(readerDTO.getContractOwnerHomePhoneNumber1());
		s2466Data.setCltphone02(readerDTO.getContractOwnerDayPhoneNumber2());
		s2466Data.setFaxno(readerDTO.getContractOwnerDayPhoneNumberCorporate());
		s2466Data.setCtrycode(readerDTO.getContactOwnerNationalityOnlyPerson());
		s2466Data.setRinternet(readerDTO.getContractOwnerEMailAddress());
		s2466Data.setCltpcode(readerDTO.getContactOwnerPostalCode());
		s2466DTO.add(s2466Data);
		s2466DTO.add(new S2466());
		createCorporateClientInputBean.setS2466(s2466DTO);
		List<S2472> s2472DTO = new ArrayList<>();
		S2472 s2472 = new S2472();
		s2472.setAddrtype("B");
		s2472.setCltaddr01(readerDTO.getContactOwnerAddress1Kanji());
		s2472.setCltaddr02(readerDTO.getContactOwnerAddress2Kanji());
		s2472.setCltaddr03(readerDTO.getContactOwnerAddress3Kanji());
		s2472.setCltaddr04(readerDTO.getContactOwnerAddress4Kanji());
		s2472.setZkanacltaddr01(readerDTO.getContactOwnerAddress1Katakana());
		s2472.setZkanacltaddr02(readerDTO.getContactOwnerAddress2Katakana());
		s2472.setZkanacltaddr03(readerDTO.getContactOwnerAddress3Katakana());
		s2472.setZkanacltaddr04(readerDTO.getContactOwnerAddress4Katakana());
		s2472DTO.add(s2472);
		createCorporateClientInputBean.setS2472(s2472DTO);
		List<S2462> s2462DTO = new ArrayList<>();
		S2462 s2462 = new S2462();
		s2462DTO.add(s2462);
		createCorporateClientInputBean.setS2462(s2462DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createCorporateClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}
	
	public List<Sr2d2> setSr2d2Data(List<Sr2d2> sr2d2List){
		Sr2d2 sr2d2DTO =  new Sr2d2();
		Sr2d2screensfl sr2d2screensflDTO = new Sr2d2screensfl();
		List<KeyValueItem> keyItem = new ArrayList<>();
		KeyValueItem actions = new KeyValueItem();
			actions.setKey("sr2d2screensfl.slt_R2");
			actions.setValue("1");
			keyItem.add(actions);
		sr2d2screensflDTO.setAttributeList(keyItem);
		sr2d2DTO.setSr2d2screensfl(sr2d2screensflDTO);
		sr2d2List.add(sr2d2DTO);
		return sr2d2List;
	}
		
	public String getContractInsured(L2CreateContractReaderDTO readerDTO){
		checkExistingLifeAssuredInput = new CheckExistingClientInput();
		checkExistingLifeAssuredInput.setZkanasnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getInsuredLastNameKana()));
		checkExistingLifeAssuredInput.setZkanagnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getInsuredNameKana()));
		checkExistingLifeAssuredInput.setDob(readerDTO.getInsuredDateofBirth().trim());
		checkExistingLifeAssuredInput.setSex(readerDTO.getInsuredGender().trim());
		checkExistingLifeAssuredInput.setPostcode(readerDTO.getInsuredPostalCode());
		LcreatepersonalclientServiceInputBean createPersonalClientInputBean = 
				new LcreatepersonalclientServiceInputBean();
		LcreatecorporateclientServiceInputBean createCorporateClientInputBean = 
				new LcreatecorporateclientServiceInputBean();
		setPersonalClientLifeAssuredDetails(createPersonalClientInputBean, readerDTO);
		setCorporateClientLifeAssuredDetails(createCorporateClientInputBean, readerDTO);
		S2480 s2480Data = new S2480();
		createPersonalClientInputBean.setS2480(s2480Data);
		createCorporateClientInputBean.setS2480(s2480Data);
		checkExistingLifeAssuredInput.setlCreatePersonalClientServiceInputBean(createPersonalClientInputBean);
		checkExistingLifeAssuredInput.setlCreateCorporateClientServiceInputBean(createCorporateClientInputBean);
		return StringUtils.SPACE;
	}
	
	public void setPersonalClientLifeAssuredDetails(LcreatepersonalclientServiceInputBean createPersonalClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2465> s2465DTO = new ArrayList<>();
		S2465 s2465Data = new S2465();
		s2465Data.setLgivname(readerDTO.getInsuredNameKanji());
		s2465Data.setLsurname(readerDTO.getInsuredLastNameKanji());
		s2465Data.setZkanagivname(readerDTO.getInsuredNameKana());
		s2465Data.setZkanasurname(readerDTO.getInsuredLastNameKana());
		s2465Data.setCltpcode(readerDTO.getInsuredPostalCode());
		s2465Data.setCltaddr01(readerDTO.getInsuredAddress1Kanji());
		s2465Data.setCltaddr02(readerDTO.getInsuredAddress2Kanji());
		s2465Data.setCltaddr03(readerDTO.getInsuredAddress3Kanji());
		s2465Data.setCltaddr04(readerDTO.getInsuredAddress4Kanji());
		s2465Data.setZkanacltaddr01(readerDTO.getInsuredAddress1Kana());
		s2465Data.setZkanacltaddr02(readerDTO.getInsuredAddress2Kana());
		s2465Data.setZkanacltaddr03(readerDTO.getInsuredAddress3Kana());
		s2465Data.setZkanacltaddr04(readerDTO.getInsuredAddress4Kana());
		s2465Data.setCltdobxDisp(readerDTO.getInsuredDateofBirth());
		s2465Data.setCltphone01(readerDTO.getInsuredTelephoneNumber1());
		s2465Data.setCltphone02(readerDTO.getInsuredTelephoneNumber2());
		s2465Data.setRmblphone(readerDTO.getInsuredTelephoneNumberWorkPlace());
		s2465Data.setCltsex(readerDTO.getInsuredGender());
		s2465Data.setCtrycode(readerDTO.getInsuredNationality());
		s2465Data.setMailing(readerDTO.getInsuredEmailAddress());
		s2465Data.setNatlty(readerDTO.getInsuredNationality());
		s2465DTO.add(s2465Data);
		createPersonalClientInputBean.setS2465(s2465DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createPersonalClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}
	
	public void setCorporateClientLifeAssuredDetails(LcreatecorporateclientServiceInputBean createCorporateClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2466> s2466DTO = new ArrayList<>();
		S2466 s2466Data = new S2466();
		s2466Data.setLgivname(readerDTO.getInsuredNameKanji());
		s2466Data.setLsurname(readerDTO.getInsuredLastNameKanji());
		s2466Data.setZkanagivname(readerDTO.getInsuredNameKana());
		s2466Data.setZkanasurname(readerDTO.getInsuredLastNameKana());
		s2466Data.setCltpcode(readerDTO.getInsuredPostalCode());
		s2466Data.setCltaddr01(readerDTO.getInsuredAddress1Kanji());
		s2466Data.setCltaddr02(readerDTO.getInsuredAddress2Kanji());
		s2466Data.setCltaddr03(readerDTO.getInsuredAddress3Kanji());
		s2466Data.setCltaddr04(readerDTO.getInsuredAddress4Kanji());
		s2466Data.setZkanacltaddr01(readerDTO.getInsuredAddress1Kana());
		s2466Data.setZkanacltaddr02(readerDTO.getInsuredAddress2Kana());
		s2466Data.setZkanacltaddr03(readerDTO.getInsuredAddress3Kana());
		s2466Data.setZkanacltaddr04(readerDTO.getInsuredAddress4Kana());
		s2466Data.setCltdobxDisp(readerDTO.getInsuredDateofBirth());
		s2466Data.setCltphone01(readerDTO.getInsuredTelephoneNumber1());
		s2466Data.setCltphone02(readerDTO.getInsuredTelephoneNumber2());
		s2466Data.setFaxno(readerDTO.getInsuredTelephoneNumberWorkPlace());
		s2466Data.setCtrycode(readerDTO.getInsuredNationality());
		s2466Data.setRinternet(readerDTO.getInsuredEmailAddress());
		s2466DTO.add(s2466Data);
		s2466DTO.add(new S2466());
		createCorporateClientInputBean.setS2466(s2466DTO);
		List<S2472> s2472DTO = new ArrayList<>();
		S2472 s2472 = new S2472();
		s2472.setAddrtype("B");
		s2472.setCltaddr01(readerDTO.getInsuredAddress1Kanji());
		s2472.setCltaddr02(readerDTO.getInsuredAddress2Kanji());
		s2472.setCltaddr03(readerDTO.getInsuredAddress3Kanji());
		s2472.setCltaddr04(readerDTO.getInsuredAddress4Kanji());
		s2472.setZkanacltaddr01(readerDTO.getInsuredAddress1Kana());
		s2472.setZkanacltaddr02(readerDTO.getInsuredAddress2Kana());
		s2472.setZkanacltaddr03(readerDTO.getInsuredAddress3Kana());
		s2472.setZkanacltaddr04(readerDTO.getInsuredAddress4Kana());
		s2472DTO.add(s2472);
		createCorporateClientInputBean.setS2472(s2472DTO);
		List<S2462> s2462DTO = new ArrayList<>();
		S2462 s2462 = new S2462();
		s2462DTO.add(s2462);
		createCorporateClientInputBean.setS2462(s2462DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createCorporateClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}

	public String getContractOwnerGuardian(L2CreateContractReaderDTO readerDTO){
		CheckExistingClientInput checkExistingTrustee = new CheckExistingClientInput();
		checkExistingTrustee.setZkanasnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getGuardianContractOwnerLastNameKana()));
		checkExistingTrustee.setZkanagnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getGuardianContractOwnerFirstNameKana()));
		checkExistingTrustee.setDob(readerDTO.getGuardianContractOwnerDOB().trim());
		checkExistingTrustee.setSex(readerDTO.getGuardianContractOwnerGender().trim());
		checkExistingTrustee.setPostcode(readerDTO.getGuardianContractOwnerPostalCode());
		if(readerDTO.getGuardianContractOwnerDOB().isEmpty() || readerDTO.getGuardianContractOwnerGender().isEmpty())
			checkExistingTrustee.setZkanasnmnor(
					doubleByteToSingleByte.removeTermOfExpressionLegalPersonality(readerDTO.getGuardianContractOwnerLastNameKana()));
		LcreatepersonalclientServiceInputBean createPersonalClientInputBean = 
				new LcreatepersonalclientServiceInputBean();
		LcreatecorporateclientServiceInputBean createCorporateClientInputBean = 
				new LcreatecorporateclientServiceInputBean();
		setPersonalClientOwnerTrusteeDetails(createPersonalClientInputBean, readerDTO);
		setCorporateClientOwnerTrusteeDetails(createCorporateClientInputBean, readerDTO);
		S2480 s2480Data = new S2480();
		createPersonalClientInputBean.setS2480(s2480Data);
		createCorporateClientInputBean.setS2480(s2480Data);
		checkExistingTrustee.setlCreatePersonalClientServiceInputBean(createPersonalClientInputBean);
		checkExistingTrustee.setlCreateCorporateClientServiceInputBean(createCorporateClientInputBean);
		checkExistingTrusteesInput.add(checkExistingTrustee);
		return StringUtils.SPACE;
	}
	
	public void setPersonalClientOwnerTrusteeDetails(LcreatepersonalclientServiceInputBean createPersonalClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2465> s2465DTO = new ArrayList<>();
		S2465 s2465Data = new S2465();
		s2465Data.setLgivname(readerDTO.getGuardianContractOwnerFirstNameKanji());
		s2465Data.setLsurname(readerDTO.getGuardianContractOwnerLastNameKanji());
		s2465Data.setZkanagivname(readerDTO.getGuardianContractOwnerFirstNameKana());
		s2465Data.setZkanasurname(readerDTO.getGuardianContractOwnerLastNameKana());
		s2465Data.setCltaddr01(readerDTO.getGuardianContractOwnerAddress1Kanji());
		s2465Data.setZkanacltaddr01(readerDTO.getGuardianContractOwnerAddress1Kana());
		s2465Data.setCltdobxDisp(readerDTO.getGuardianContractOwnerDOB());
		s2465Data.setCltphone01(readerDTO.getGuardianContractOwnerPhoneNumber1());
		s2465Data.setCltsex(readerDTO.getGuardianContractOwnerGender());
		s2465Data.setCtrycode(readerDTO.getGuardianContractOwnerNationality());
		s2465Data.setCltpcode(readerDTO.getGuardianContractOwnerPostalCode());
		s2465Data.setNatlty(readerDTO.getGuardianContractOwnerNationality());
		s2465DTO.add(s2465Data);
		createPersonalClientInputBean.setS2465(s2465DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createPersonalClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}
	
	public void setCorporateClientOwnerTrusteeDetails(
			LcreatecorporateclientServiceInputBean createCorporateClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2466> s2466DTO = new ArrayList<>();
		S2466 s2466Data = new S2466();
		s2466Data.setLgivname(readerDTO.getGuardianContractOwnerFirstNameKanji());
		s2466Data.setLsurname(readerDTO.getGuardianContractOwnerLastNameKanji());
		s2466Data.setZkanagivname(readerDTO.getGuardianContractOwnerFirstNameKana());
		s2466Data.setZkanasurname(readerDTO.getGuardianContractOwnerLastNameKana());
		s2466Data.setCltaddr01(readerDTO.getGuardianContractOwnerAddress1Kanji());
		s2466Data.setZkanacltaddr01(readerDTO.getGuardianContractOwnerAddress1Kana());
		s2466Data.setCltdobxDisp(readerDTO.getGuardianContractOwnerDOB());
		s2466Data.setCltphone01(readerDTO.getGuardianContractOwnerPhoneNumber1());
		s2466Data.setCtrycode(readerDTO.getGuardianContractOwnerNationality());
		s2466Data.setCltpcode(readerDTO.getGuardianContractOwnerPostalCode());
		s2466DTO.add(s2466Data);
		s2466DTO.add(new S2466());
		createCorporateClientInputBean.setS2466(s2466DTO);
		List<S2472> s2472DTO = new ArrayList<>();
		S2472 s2472 = new S2472();
		s2472.setAddrtype("B");
		s2472.setCltaddr01(readerDTO.getGuardianContractOwnerAddress1Kanji());
		s2472.setZkanacltaddr01(readerDTO.getGuardianContractOwnerAddress1Kana());
		s2472DTO.add(s2472);
		createCorporateClientInputBean.setS2472(s2472DTO);
		List<S2462> s2462DTO = new ArrayList<>();
		S2462 s2462 = new S2462();
		s2462DTO.add(s2462);
		createCorporateClientInputBean.setS2462(s2462DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createCorporateClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}

	public String getContractInsurerGuardian(L2CreateContractReaderDTO readerDTO){
		CheckExistingClientInput checkExistingTrustee = new CheckExistingClientInput();
		checkExistingTrustee.setZkanasnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getGuardianInsuredPersonLastNameKana()));
		checkExistingTrustee.setZkanagnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getGuardianInsuredPersonFirstNameKana()));
		checkExistingTrustee.setDob(readerDTO.getGuardianInsuredPersonDOB().trim());
		checkExistingTrustee.setSex(readerDTO.getGuardianInsuredPersonGender().trim());
		checkExistingTrustee.setPostcode(readerDTO.getGuardianInsuredPersonPostalCode());
		if(readerDTO.getGuardianInsuredPersonDOB().isEmpty() || readerDTO.getGuardianInsuredPersonGender().isEmpty())
			checkExistingTrustee.setZkanasnmnor(
					doubleByteToSingleByte.removeTermOfExpressionLegalPersonality(readerDTO.getGuardianInsuredPersonLastNameKana()));
		LcreatepersonalclientServiceInputBean createPersonalClientInputBean = 
				new LcreatepersonalclientServiceInputBean();
		LcreatecorporateclientServiceInputBean createCorporateClientInputBean = 
				new LcreatecorporateclientServiceInputBean();
		setPersonalClientInsuredTrusteeDetails(createPersonalClientInputBean, readerDTO);
		setCorporateClientInsuredTrusteeDetails(createCorporateClientInputBean, readerDTO);
		S2480 s2480Data = new S2480();
		createPersonalClientInputBean.setS2480(s2480Data);
		createCorporateClientInputBean.setS2480(s2480Data);
		checkExistingTrustee.setlCreatePersonalClientServiceInputBean(createPersonalClientInputBean);
		checkExistingTrustee.setlCreateCorporateClientServiceInputBean(createCorporateClientInputBean);
		checkExistingTrusteesInput.add(checkExistingTrustee);
		return StringUtils.SPACE;
	}
	
	public void setPersonalClientInsuredTrusteeDetails(LcreatepersonalclientServiceInputBean createPersonalClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2465> s2465DTO = new ArrayList<>();
		S2465 s2465Data = new S2465();
		s2465Data.setLgivname(readerDTO.getGuardianInsuredPersonFirstNameKanji());
		s2465Data.setLsurname(readerDTO.getGuardianInsuredPersonLastNameKanji());
		s2465Data.setZkanagivname(readerDTO.getGuardianInsuredPersonFirstNameKana());
		s2465Data.setZkanasurname(readerDTO.getGuardianInsuredPersonLastNameKana());
		s2465Data.setCltaddr01(readerDTO.getGuardianInsuredPersonAddress1Kanji());
		s2465Data.setZkanacltaddr01(readerDTO.getGuardianInsuredPersonAddress1Kana());
		s2465Data.setCltdobxDisp(readerDTO.getGuardianInsuredPersonDOB());
		s2465Data.setCltphone01(readerDTO.getGuardianInsuredPersonPhoneNumber1());
		s2465Data.setCltsex(readerDTO.getGuardianInsuredPersonGender());
		s2465Data.setCtrycode(readerDTO.getGuardianInsuredPersonNationality());
		s2465Data.setCltpcode(readerDTO.getGuardianInsuredPersonPostalCode());
		s2465Data.setNatlty(readerDTO.getGuardianInsuredPersonNationality());
		s2465DTO.add(s2465Data);
		createPersonalClientInputBean.setS2465(s2465DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createPersonalClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}
	
	public void setCorporateClientInsuredTrusteeDetails(
			LcreatecorporateclientServiceInputBean createCorporateClientInputBean,
			L2CreateContractReaderDTO readerDTO) {
		List<S2466> s2466DTO = new ArrayList<>();
		S2466 s2466Data = new S2466();
		s2466Data.setLgivname(readerDTO.getGuardianInsuredPersonFirstNameKanji());
		s2466Data.setLsurname(readerDTO.getGuardianInsuredPersonLastNameKanji());
		s2466Data.setZkanagivname(readerDTO.getGuardianInsuredPersonFirstNameKana());
		s2466Data.setZkanasurname(readerDTO.getGuardianInsuredPersonLastNameKana());
		s2466Data.setCltaddr01(readerDTO.getGuardianInsuredPersonAddress1Kanji());
		s2466Data.setZkanacltaddr01(readerDTO.getGuardianInsuredPersonAddress1Kana());
		s2466Data.setCltdobxDisp(readerDTO.getGuardianInsuredPersonDOB());
		s2466Data.setCltphone01(readerDTO.getGuardianInsuredPersonPhoneNumber1());
		s2466Data.setCtrycode(readerDTO.getGuardianInsuredPersonNationality());
		s2466Data.setCltpcode(readerDTO.getGuardianInsuredPersonPostalCode());
		s2466DTO.add(s2466Data);
		s2466DTO.add(new S2466());
		createCorporateClientInputBean.setS2466(s2466DTO);
		List<S2472> s2472DTO = new ArrayList<>();
		S2472 s2472 = new S2472();
		s2472.setAddrtype("B");
		s2472.setCltaddr01(readerDTO.getGuardianInsuredPersonAddress1Kanji());
		s2472.setZkanacltaddr01(readerDTO.getGuardianInsuredPersonAddress1Kana());
		s2472DTO.add(s2472);
		createCorporateClientInputBean.setS2472(s2472DTO);
		List<S2462> s2462DTO = new ArrayList<>();
		S2462 s2462 = new S2462();
		s2462DTO.add(s2462);
		createCorporateClientInputBean.setS2462(s2462DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createCorporateClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}

	public String getContractBeneficiary(L2CreateContractReaderDTO readerDTO, int idx){
		CheckExistingClientInput checkExistingBeneficiary = new CheckExistingClientInput();
		checkExistingBeneficiary.setZkanasnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getBeneficiaryLastNameKana().get(idx)));
		checkExistingBeneficiary.setZkanagnmnor(
				doubleByteToSingleByte.halfByteKatakanaNormalized(readerDTO.getBeneficiaryFirstNameKana().get(idx)));
		checkExistingBeneficiary.setDob(readerDTO.getBeneficiaryDOB().get(idx).trim());
		checkExistingBeneficiary.setSex(readerDTO.getBeneficiaryGender().get(idx).trim());
		checkExistingBeneficiary.setPostcode(readerDTO.getBeneficiaryPostalCode().get(idx));
		if(readerDTO.getBeneficiaryDOB().get(idx).isEmpty() || readerDTO.getBeneficiaryGender().get(idx).isEmpty())
			checkExistingBeneficiary.setZkanasnmnor(
					doubleByteToSingleByte.removeTermOfExpressionLegalPersonality(readerDTO.getBeneficiaryLastNameKana().get(idx)));
		LcreatepersonalclientServiceInputBean createPersonalClientInputBean = 
				new LcreatepersonalclientServiceInputBean();
		LcreatecorporateclientServiceInputBean createCorporateClientInputBean = 
				new LcreatecorporateclientServiceInputBean();
		setPersonalClientBeneficiaryDetails(createPersonalClientInputBean, readerDTO, idx);
		setCorporateClientBeneficiaryDetails(createCorporateClientInputBean, readerDTO, idx);
		S2480 s2480Data = new S2480();
		createPersonalClientInputBean.setS2480(s2480Data);
		createCorporateClientInputBean.setS2480(s2480Data);
		checkExistingBeneficiary.setlCreatePersonalClientServiceInputBean(createPersonalClientInputBean);
		checkExistingBeneficiary.setlCreateCorporateClientServiceInputBean(createCorporateClientInputBean);
		checkExistingBeneficiariesInput.add(checkExistingBeneficiary);
		return StringUtils.SPACE;
	}
	
	public void setPersonalClientBeneficiaryDetails(LcreatepersonalclientServiceInputBean createPersonalClientInputBean,
			L2CreateContractReaderDTO readerDTO, int idx) {
		List<S2465> s2465DTO = new ArrayList<>();
		S2465 s2465Data = new S2465();
		s2465Data.setLgivname(readerDTO.getBeneficiaryFirstNameKanji().get(idx));
		s2465Data.setLsurname(readerDTO.getBeneficiaryLastNameKanji().get(idx));
		s2465Data.setZkanagivname(readerDTO.getBeneficiaryFirstNameKana().get(idx));
		s2465Data.setZkanasurname(readerDTO.getBeneficiaryLastNameKana().get(idx));
		s2465Data.setCltaddr01(readerDTO.getBeneficiaryAddress1Kanji().get(idx));
		s2465Data.setCltaddr02(readerDTO.getBeneficiaryAddress2Kanji().get(idx));
		s2465Data.setCltaddr03(readerDTO.getBeneficiaryAddress3Kanji().get(idx));
		s2465Data.setCltaddr04(readerDTO.getBeneficiaryAddress4Kanji().get(idx));
		s2465Data.setZkanacltaddr01(readerDTO.getBeneficiaryAddress1Kana().get(idx));
		s2465Data.setZkanacltaddr02(readerDTO.getBeneficiaryAddress2Kana().get(idx));
		s2465Data.setZkanacltaddr03(readerDTO.getBeneficiaryAddress3Kana().get(idx));
		s2465Data.setZkanacltaddr04(readerDTO.getBeneficiaryAddress4Kana().get(idx));
		s2465Data.setCltdobxDisp(readerDTO.getBeneficiaryDOB().get(idx));
		s2465Data.setCltsex(readerDTO.getBeneficiaryGender().get(idx));
		s2465Data.setCtrycode(readerDTO.getBeneficiaryNationality().get(idx));
		s2465Data.setCltpcode(readerDTO.getBeneficiaryPostalCode().get(idx));
		s2465Data.setNatlty(readerDTO.getBeneficiaryNationality().get(idx));
		s2465DTO.add(s2465Data);
		createPersonalClientInputBean.setS2465(s2465DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createPersonalClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}
	
	public void setCorporateClientBeneficiaryDetails(LcreatecorporateclientServiceInputBean createCorporateClientInputBean,
			L2CreateContractReaderDTO readerDTO, int idx) {
		List<S2466> s2466DTO = new ArrayList<>();
		S2466 s2466Data = new S2466();
		s2466Data.setLgivname(readerDTO.getBeneficiaryFirstNameKanji().get(idx));
		s2466Data.setLsurname(readerDTO.getBeneficiaryLastNameKanji().get(idx));
		s2466Data.setZkanagivname(readerDTO.getBeneficiaryFirstNameKana().get(idx));
		s2466Data.setZkanasurname(readerDTO.getBeneficiaryLastNameKana().get(idx));
		s2466Data.setCltaddr01(readerDTO.getBeneficiaryAddress1Kanji().get(idx));
		s2466Data.setCltaddr02(readerDTO.getBeneficiaryAddress2Kanji().get(idx));
		s2466Data.setCltaddr03(readerDTO.getBeneficiaryAddress3Kanji().get(idx));
		s2466Data.setCltaddr04(readerDTO.getBeneficiaryAddress4Kanji().get(idx));
		s2466Data.setZkanacltaddr01(readerDTO.getBeneficiaryAddress1Kana().get(idx));
		s2466Data.setZkanacltaddr02(readerDTO.getBeneficiaryAddress2Kana().get(idx));
		s2466Data.setZkanacltaddr03(readerDTO.getBeneficiaryAddress3Kana().get(idx));
		s2466Data.setZkanacltaddr04(readerDTO.getBeneficiaryAddress4Kana().get(idx));
		s2466Data.setCltdobxDisp(readerDTO.getBeneficiaryDOB().get(idx));
		s2466Data.setCtrycode(readerDTO.getBeneficiaryNationality().get(idx));
		s2466Data.setCltpcode(readerDTO.getBeneficiaryPostalCode().get(idx));
		s2466DTO.add(s2466Data);
		s2466DTO.add(new S2466());
		createCorporateClientInputBean.setS2466(s2466DTO);
		List<S2472> s2472DTO = new ArrayList<>();
		S2472 s2472 = new S2472();
		s2472.setAddrtype("B");
		s2472.setCltaddr01(readerDTO.getBeneficiaryAddress1Kanji().get(idx));
		s2472.setCltaddr02(readerDTO.getBeneficiaryAddress2Kanji().get(idx));
		s2472.setCltaddr03(readerDTO.getBeneficiaryAddress3Kanji().get(idx));
		s2472.setCltaddr04(readerDTO.getBeneficiaryAddress4Kanji().get(idx));
		s2472.setZkanacltaddr01(readerDTO.getBeneficiaryAddress1Kana().get(idx));
		s2472.setZkanacltaddr02(readerDTO.getBeneficiaryAddress2Kana().get(idx));
		s2472.setZkanacltaddr03(readerDTO.getBeneficiaryAddress3Kana().get(idx));
		s2472.setZkanacltaddr04(readerDTO.getBeneficiaryAddress4Kana().get(idx));
		s2472DTO.add(s2472);
		createCorporateClientInputBean.setS2472(s2472DTO);
		List<S2462> s2462DTO = new ArrayList<>();
		S2462 s2462 = new S2462();
		s2462DTO.add(s2462);
		createCorporateClientInputBean.setS2462(s2462DTO);
		List<Sr2d2> sr2d2List = new ArrayList<>();
		createCorporateClientInputBean.setSr2d2(setSr2d2Data(sr2d2List));
	}

	public void setFields(){
		responseDTO.setAgencyNumber(StringUtils.SPACE);
		responseDTO.setAgentNumber(StringUtils.SPACE);
		responseDTO.setBeneficiary(new ArrayList<>());
		responseDTO.setBeneficiaryNameKana(new ArrayList<>());
		responseDTO.setBeneficiaryNameKanji(new ArrayList<>());
		responseDTO.setBillingFrequency(StringUtils.SPACE);
		responseDTO.setContractCurrency(StringUtils.SPACE);
		responseDTO.setContractDate(StringUtils.SPACE);
		responseDTO.setContractNumber(StringUtils.SPACE);
		responseDTO.setContractOwner(StringUtils.SPACE);
		responseDTO.setContractOwnerNameKana(StringUtils.SPACE);
		responseDTO.setContractOwnerNameKanji(StringUtils.SPACE);
		responseDTO.setContractType(StringUtils.SPACE);
		responseDTO.setEffectiveDateofBatchRun(StringUtils.SPACE);
		responseDTO.setGuardian(new ArrayList<>());
		responseDTO.setGuardianNameKana(new ArrayList<>());
		responseDTO.setGuardianNameKanji(new ArrayList<>());
		responseDTO.setNewOrExistingGuard(new ArrayList<>());
		responseDTO.setLifeAssured(StringUtils.SPACE);
		responseDTO.setLifeAssuredNameKana(StringUtils.SPACE);
		responseDTO.setLifeAssuredNameKanji(StringUtils.SPACE);
		responseDTO.setNeworExistingLA(StringUtils.SPACE);
		responseDTO.setMethodOfPayment(StringUtils.SPACE);
		responseDTO.setNewOrExistingBenef(new ArrayList<>());
		responseDTO.setNewOrExistingOwner(StringUtils.SPACE);
		responseDTO.setProposalNumber(StringUtils.SPACE);
	}

	@Override
	public String toString() {
		return "L2CreateContractProcessor []";
	}


}
