package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.smarttable.pojo.T5654;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;

/**
 * DTO for L2AutoIncrProcessor
 * 
 * @author yyang21
 *
 */
public class L2AutoIncrProcessorDTO {

	private L2AutoIncrControlTotalDTO l2AutoIncrControlTotalDTO;
	private boolean permission;
	private boolean stampDutyflag;
	private boolean prmhldtrad;
	private boolean vpmsFlag;
	private int transDate;
	private int transTime;
	private int effDate;
	private int OldCpiDate;
	private String bprdAuthCode;
	private String lang;
	private String wsaaSysparam03;
	private T5679 t5679IO;
	private T5654 t5654IO;
	private T5687 t5687IO;
	private String t5675Premsubr;
	private T5687ArrayInner t5687ArrayInner;
	private T6658ArrayInner t6658ArrayInner;
	private List<String> wsaaTr384LetterType = new ArrayList<>();
	private List<String> wsaaTr384Key = new ArrayList<>();
	private boolean newContract = false;
	private Chdrpf chdrlifIO;
	private Lifepf lifergpIO;
	private Covrpf covrincIO;
	private IncrsumDTO incrsumDTO;
	private PremiumDTO premiumrec;
	private boolean changeExist = false;
	private boolean reinstflag = false;
	private boolean incrWritten = false;
	private boolean foundPro;
	private int wsaaProCpiDate;
	private int wsaaLastCpiDate;
	private int tranno;
	private String wsaaFreq;
	private String wsaaBasicCommMeth;
	private String wsaaBascpy;
	private String wsaaRnwcpy;
	private String wsaaSrvcpy;
	private int wsaaT6658Ix;
	private int wsaaT5687Ix;
	private String userName;
	private String batchName;
	private String datimeInit;
	private int wsaaSub;
	private String prefix;
	private String batch;
	private String batcbrn;
	private int actyear;
	private int actmonth;
	private String batchTrcde;
	
	private String wsysChdrnum;
	private String wsysCoverage;
	private BigDecimal wsaaOriginst01;
	private BigDecimal wsaaOriginst02;
	private BigDecimal wsaaOriginst03;
	private BigDecimal wsaaOrigsum;
	private String wsaaChdrChdrcoy;
	private String wsaaChdrChdrnum;
	
	public String getWsaaChdrChdrcoy() {
		return wsaaChdrChdrcoy;
	}

	public void setWsaaChdrChdrcoy(String wsaaChdrChdrcoy) {
		this.wsaaChdrChdrcoy = wsaaChdrChdrcoy;
	}

	public String getWsaaChdrChdrnum() {
		return wsaaChdrChdrnum;
	}

	public void setWsaaChdrChdrnum(String wsaaChdrChdrnum) {
		this.wsaaChdrChdrnum = wsaaChdrChdrnum;
	}

	public String getWsysChdrnum() {
		return wsysChdrnum;
	}

	public void setWsysChdrnum(String wsysChdrnum) {
		this.wsysChdrnum = wsysChdrnum;
	}

	public String getWsysCoverage() {
		return wsysCoverage;
	}

	public void setWsysCoverage(String wsysCoverage) {
		this.wsysCoverage = wsysCoverage;
	}

	public BigDecimal getWsaaOriginst01() {
		return wsaaOriginst01;
	}

	public void setWsaaOriginst01(BigDecimal wsaaOriginst01) {
		this.wsaaOriginst01 = wsaaOriginst01;
	}

	public BigDecimal getWsaaOriginst02() {
		return wsaaOriginst02;
	}

	public void setWsaaOriginst02(BigDecimal wsaaOriginst02) {
		this.wsaaOriginst02 = wsaaOriginst02;
	}

	public BigDecimal getWsaaOriginst03() {
		return wsaaOriginst03;
	}

	public void setWsaaOriginst03(BigDecimal wsaaOriginst03) {
		this.wsaaOriginst03 = wsaaOriginst03;
	}

	public BigDecimal getWsaaOrigsum() {
		return wsaaOrigsum;
	}

	public void setWsaaOrigsum(BigDecimal wsaaOrigsum) {
		this.wsaaOrigsum = wsaaOrigsum;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBatcbrn() {
		return batcbrn;
	}

	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	public int getActyear() {
		return actyear;
	}

	public void setActyear(int actyear) {
		this.actyear = actyear;
	}

	public int getActmonth() {
		return actmonth;
	}

	public void setActmonth(int actmonth) {
		this.actmonth = actmonth;
	}

	public String getBatchTrcde() {
		return batchTrcde;
	}

	public void setBatchTrcde(String batchTrcde) {
		this.batchTrcde = batchTrcde;
	}

	public PremiumDTO getPremiumrec() {
		return premiumrec;
	}

	public void setPremiumrec(PremiumDTO premiumrec) {
		this.premiumrec = premiumrec;
	}

	public int getOldCpiDate() {
		return OldCpiDate;
	}

	public void setOldCpiDate(int oldCpiDate) {
		OldCpiDate = oldCpiDate;
	}

	public boolean isFoundPro() {
		return foundPro;
	}

	public void setFoundPro(boolean foundPro) {
		this.foundPro = foundPro;
	}

	public boolean isIncrWritten() {
		return incrWritten;
	}

	public void setIncrWritten(boolean incrWritten) {
		this.incrWritten = incrWritten;
	}

	public String getDatimeInit() {
		return datimeInit;
	}

	public void setDatimeInit(String datimeInit) {
		this.datimeInit = datimeInit;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Covrpf getCovrincIO() {
		return covrincIO;
	}

	public void setCovrincIO(Covrpf covrincIO) {
		this.covrincIO = covrincIO;
	}

	public IncrsumDTO getIncrsumDTO() {
		return incrsumDTO;
	}

	public void setIncrsumDTO(IncrsumDTO incrsumDTO) {
		this.incrsumDTO = incrsumDTO;
	}

	public Lifepf getLifergpIO() {
		return lifergpIO;
	}

	public void setLifergpIO(Lifepf lifergpIO) {
		this.lifergpIO = lifergpIO;
	}

	public String getT5675Premsubr() {
		return t5675Premsubr;
	}

	public void setT5675Premsubr(String t5675Premsubr) {
		this.t5675Premsubr = t5675Premsubr;
	}

	public T5687 getT5687IO() {
		return t5687IO;
	}

	public void setT5687IO(T5687 t5687io) {
		t5687IO = t5687io;
	}

	public boolean isVpmsFlag() {
		return vpmsFlag;
	}

	public void setVpmsFlag(boolean vpmsFlag) {
		this.vpmsFlag = vpmsFlag;
	}

	public T5654 getT5654IO() {
		return t5654IO;
	}

	public void setT5654IO(T5654 t5654io) {
		t5654IO = t5654io;
	}

	public int getWsaaT5687Ix() {
		return wsaaT5687Ix;
	}

	public void setWsaaT5687Ix(int wsaaT5687Ix) {
		this.wsaaT5687Ix = wsaaT5687Ix;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isChangeExist() {
		return changeExist;
	}

	public void setChangeExist(boolean changeExist) {
		this.changeExist = changeExist;
	}

	public int getWsaaT6658Ix() {
		return wsaaT6658Ix;
	}

	public void setWsaaT6658Ix(int wsaaT6658Ix) {
		this.wsaaT6658Ix = wsaaT6658Ix;
	}

	public String getWsaaBasicCommMeth() {
		return wsaaBasicCommMeth;
	}

	public void setWsaaBasicCommMeth(String wsaaBasicCommMeth) {
		this.wsaaBasicCommMeth = wsaaBasicCommMeth;
	}

	public String getWsaaBascpy() {
		return wsaaBascpy;
	}

	public void setWsaaBascpy(String wsaaBascpy) {
		this.wsaaBascpy = wsaaBascpy;
	}

	public String getWsaaRnwcpy() {
		return wsaaRnwcpy;
	}

	public void setWsaaRnwcpy(String wsaaRnwcpy) {
		this.wsaaRnwcpy = wsaaRnwcpy;
	}

	public String getWsaaSrvcpy() {
		return wsaaSrvcpy;
	}

	public void setWsaaSrvcpy(String wsaaSrvcpy) {
		this.wsaaSrvcpy = wsaaSrvcpy;
	}

	public int getWsaaProCpiDate() {
		return wsaaProCpiDate;
	}

	public void setWsaaProCpiDate(int wsaaProCpiDate) {
		this.wsaaProCpiDate = wsaaProCpiDate;
	}

	public int getWsaaLastCpiDate() {
		return wsaaLastCpiDate;
	}

	public void setWsaaLastCpiDate(int wsaaLastCpiDate) {
		this.wsaaLastCpiDate = wsaaLastCpiDate;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getWsaaFreq() {
		return wsaaFreq;
	}

	public void setWsaaFreq(String wsaaFreq) {
		this.wsaaFreq = wsaaFreq;
	}

	public boolean isReinstflag() {
		return reinstflag;
	}

	public void setReinstflag(boolean reinstflag) {
		this.reinstflag = reinstflag;
	}

	public Chdrpf getChdrlifIO() {
		return chdrlifIO;
	}

	public void setChdrlifIO(Chdrpf chdrlifIO) {
		this.chdrlifIO = chdrlifIO;
	}

	public boolean isNewContract() {
		return newContract;
	}

	public void setNewContract(boolean newContract) {
		this.newContract = newContract;
	}

	public String getWsaaSysparam03() {
		return wsaaSysparam03;
	}

	public void setWsaaSysparam03(String wsaaSysparam03) {
		this.wsaaSysparam03 = wsaaSysparam03;
	}

	public int getEffDate() {
		return effDate;
	}

	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}

	public List<String> getWsaaTr384LetterType() {
		return wsaaTr384LetterType;
	}

	public void setWsaaTr384LetterType(List<String> wsaaTr384LetterType) {
		this.wsaaTr384LetterType = wsaaTr384LetterType;
	}

	public List<String> getWsaaTr384Key() {
		return wsaaTr384Key;
	}

	public void setWsaaTr384Key(List<String> wsaaTr384Key) {
		this.wsaaTr384Key = wsaaTr384Key;
	}

	public T6658ArrayInner getT6658ArrayInner() {
		return t6658ArrayInner;
	}

	public void setT6658ArrayInner(T6658ArrayInner t6658ArrayInner) {
		this.t6658ArrayInner = t6658ArrayInner;
	}

	public T5687ArrayInner getT5687ArrayInner() {
		return t5687ArrayInner;
	}

	public void setT5687ArrayInner(T5687ArrayInner t5687ArrayInner) {
		this.t5687ArrayInner = t5687ArrayInner;
	}

	public T5679 getT5679IO() {
		return t5679IO;
	}

	public void setT5679IO(T5679 t5679io) {
		t5679IO = t5679io;
	}

	public String getBprdAuthCode() {
		return bprdAuthCode;
	}

	public void setBprdAuthCode(String bprdAuthCode) {
		this.bprdAuthCode = bprdAuthCode;
	}

	public L2AutoIncrControlTotalDTO getL2AutoIncrControlTotalDTO() {
		return l2AutoIncrControlTotalDTO;
	}

	public void setL2AutoIncrControlTotalDTO(L2AutoIncrControlTotalDTO l2AutoIncrControlTotalDTO) {
		this.l2AutoIncrControlTotalDTO = l2AutoIncrControlTotalDTO;
	}

	public boolean isPermission() {
		return permission;
	}

	public void setPermission(boolean permission) {
		this.permission = permission;
	}

	public boolean isStampDutyflag() {
		return stampDutyflag;
	}

	public void setStampDutyflag(boolean stampDutyflag) {
		this.stampDutyflag = stampDutyflag;
	}

	public boolean isPrmhldtrad() {
		return prmhldtrad;
	}

	public void setPrmhldtrad(boolean prmhldtrad) {
		this.prmhldtrad = prmhldtrad;
	}

	public int getTransDate() {
		return transDate;
	}

	public void setTransDate(int transDate) {
		this.transDate = transDate;
	}

	public int getTransTime() {
		return transTime;
	}

	public void setTransTime(int transTime) {
		this.transTime = transTime;
	}

	public int getWsaaSub() {
		return wsaaSub;
	}

	public void setWsaaSub(int wsaaSub) {
		this.wsaaSub = wsaaSub;
	}
}
