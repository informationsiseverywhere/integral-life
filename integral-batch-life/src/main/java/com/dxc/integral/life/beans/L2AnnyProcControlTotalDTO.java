package com.dxc.integral.life.beans;

import java.math.BigDecimal;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component("l2AnnyProcControlTotalDTO")
@Lazy
public class L2AnnyProcControlTotalDTO {
	
	private BigDecimal controlTotal01 = BigDecimal.ZERO;
	private BigDecimal controlTotal02 = BigDecimal.ZERO;
	
	private BigDecimal controlTotal04 = BigDecimal.ZERO;
	private BigDecimal controlTotal05 = BigDecimal.ZERO;
	
	/**
	 * @return the controlTotal01
	 */
	public BigDecimal getControlTotal01() {
		return controlTotal01;
	}
	
	/**
	 * Setter for controlTotal01
	 * 
	 * @param controlTotal01
	 */
	public  void setControlTotal01(BigDecimal controlTotal01) {
		this.controlTotal01 = controlTotal01;
	}

	/**
	 * @return the controlTotal02
	 */
	public BigDecimal getControlTotal02() {
		return controlTotal02;
	}
	
	/**
	 *  Setter for controlTotal02
	 *  
	 * @param controlTotal02
	 */
	public  void setControlTotal02(BigDecimal controlTotal02) {
		this.controlTotal02 = controlTotal02;
	}
	
	/**
	 * @return the controlTotal04
	 */
	public BigDecimal getControlTotal04() {
		return controlTotal04;
	}
	
	/**
	 * Setter for controlTotal04
	 * 
	 * @param controlTotal04
	 */
	public  void setControlTotal04(BigDecimal controlTotal04) {
		this.controlTotal04 = controlTotal04;
	}

	/**
	 * @return the controlTotal05
	 */
	public BigDecimal getControlTotal05() {
		return controlTotal05;
	}
	
	/**
	 * Setter for controlTotal05
	 * 
	 * @param controlTotal05
	 */
	public void setControlTotal05(BigDecimal controlTotal05) {
		this.controlTotal05 = controlTotal05;
	}

}
