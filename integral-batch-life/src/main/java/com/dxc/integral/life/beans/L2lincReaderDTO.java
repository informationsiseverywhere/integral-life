package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.sql.Timestamp;


public class L2lincReaderDTO {

	private String zdatacls;
	private String zreclass;
	private String zmodtype;
	private String zrsltcde;
	private String zrsltrsn;
	private String zerrorno;
	private String znofrecs;
	private String klinckey;
	private String hcltnam;
	private String cltsex;
	private int zclntdob;
	private int zeffdate;
	private String flag;
	private String kglcmpcd;
	private String addrss;
	private BigDecimal zhospbnf;
	private BigDecimal zsickbnf;
	private BigDecimal zcancbnf;
	private BigDecimal zothsbnf;
	private int zregdate;
	private int zlincdte;
	private String kjhcltnam;
	private String zhospopt;
	private int zrevdate; 
	private String zlownnam;
	private BigDecimal zcsumins;
	private BigDecimal zasumins;
	private String zdethopt;
	private String zrqflag;
	private int zrqsetdte;
	private int zrqcandte;
	private String zepflag;
	private String zcvflag;
	private String zcontsex;
	private int zcontdob;
	private String zcontaddr;
	private String lifnamkj;
	private String ownnamkj;
	private String wflga;
	private String zerrflg;
	private int zoccdate;
	private int ztrdate;
	private int zadrddte;
	private int	zexdrddte;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private String chdrnum;
	private String cownnnum;
	private String lifecnum;
	private String batctrcde;
	private int senddate;
	private int recvdate;
	private String recveror;
	private String dummy;
	
	
	public String getZdatacls() {
		return zdatacls;
	}
	public void setZdatacls(String zdatacls) {
		this.zdatacls = zdatacls;
	}
	public String getZreclass() {
		return zreclass;
	}
	public String getZmodtype() {
		return zmodtype;
	}
	public String getZrsltcde() {
		return zrsltcde;
	}
	public String getZrsltrsn() {
		return zrsltrsn;
	}
	public String getZerrorno() {
		return zerrorno;
	}
	public String getZnofrecs() {
		return znofrecs;
	}
	public String getKlinckey() {
		return klinckey;
	}
	public String getHcltnam() {
		return hcltnam;
	}
	public String getCltsex() {
		return cltsex;
	}
	public int getZclntdob() {
		return zclntdob;
	}
	public int getZeffdate() {
		return zeffdate;
	}
	public String getFlag() {
		return flag;
	}
	public String getKglcmpcd() {
		return kglcmpcd;
	}
	public String getAddrss() {
		return addrss;
	}
	public BigDecimal getZhospbnf() {
		return zhospbnf;
	}
	public BigDecimal getZsickbnf() {
		return zsickbnf;
	}
	public BigDecimal getZcancbnf() {
		return zcancbnf;
	}
	public BigDecimal getZothsbnf() {
		return zothsbnf;
	}
	public int getZregdate() {
		return zregdate;
	}
	public int getZlincdte() {
		return zlincdte;
	}
	public String getKjhcltnam() {
		return kjhcltnam;
	}
	public String getZhospopt() {
		return zhospopt;
	}
	public int getZrevdate() {
		return zrevdate;
	}
	public String getZlownnam() {
		return zlownnam;
	}
	public BigDecimal getZcsumins() {
		return zcsumins;
	}
	public BigDecimal getZasumins() {
		return zasumins;
	}
	public String getZdethopt() {
		return zdethopt;
	}
	public String getZrqflag() {
		return zrqflag;
	}
	public int getZrqsetdte() {
		return zrqsetdte;
	}
	public int getZrqcandte() {
		return zrqcandte;
	}
	public String getZepflag() {
		return zepflag;
	}
	public String getZcvflag() {
		return zcvflag;
	}
	public String getZcontsex() {
		return zcontsex;
	}
	public int getZcontdob() {
		return zcontdob;
	}
	public String getZcontaddr() {
		return zcontaddr;
	}
	public String getLifnamkj() {
		return lifnamkj;
	}
	public String getOwnnamkj() {
		return ownnamkj;
	}
	public String getWflga() {
		return wflga;
	}
	public String getZerrflg() {
		return zerrflg;
	}
	public int getZoccdate() {
		return zoccdate;
	}
	public int getZtrdate() {
		return ztrdate;
	}
	public int getZadrddte() {
		return zadrddte;
	}
	public int getZexdrddte() {
		return zexdrddte;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public String getCownnnum() {
		return cownnnum;
	}
	public String getLifecnum() {
		return lifecnum;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public int getSenddate() {
		return senddate;
	}
	public int getRecvdate() {
		return recvdate;
	}
	public String getRecveror() {
		return recveror;
	}
	public String getDummy() {
		return dummy;
	}
	public void setZreclass(String zreclass) {
		this.zreclass = zreclass;
	}
	public void setZmodtype(String zmodtype) {
		this.zmodtype = zmodtype;
	}
	public void setZrsltcde(String zrsltcde) {
		this.zrsltcde = zrsltcde;
	}
	public void setZrsltrsn(String zrsltrsn) {
		this.zrsltrsn = zrsltrsn;
	}
	public void setZerrorno(String zerrorno) {
		this.zerrorno = zerrorno;
	}
	public void setZnofrecs(String znofrecs) {
		this.znofrecs = znofrecs;
	}
	public void setKlinckey(String klinckey) {
		this.klinckey = klinckey;
	}
	public void setHcltnam(String hcltnam) {
		this.hcltnam = hcltnam;
	}
	public void setCltsex(String cltsex) {
		this.cltsex = cltsex;
	}
	public void setZclntdob(int zclntdob) {
		this.zclntdob = zclntdob;
	}
	public void setZeffdate(int zeffdate) {
		this.zeffdate = zeffdate;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public void setKglcmpcd(String kglcmpcd) {
		this.kglcmpcd = kglcmpcd;
	}
	public void setAddrss(String addrss) {
		this.addrss = addrss;
	}
	public void setZhospbnf(BigDecimal zhospbnf) {
		this.zhospbnf = zhospbnf;
	}
	public void setZsickbnf(BigDecimal zsickbnf) {
		this.zsickbnf = zsickbnf;
	}
	public void setZcancbnf(BigDecimal zcancbnf) {
		this.zcancbnf = zcancbnf;
	}
	public void setZothsbnf(BigDecimal zothsbnf) {
		this.zothsbnf = zothsbnf;
	}
	public void setZregdate(int zregdate) {
		this.zregdate = zregdate;
	}
	public void setZlincdte(int zlincdte) {
		this.zlincdte = zlincdte;
	}
	public void setKjhcltnam(String kjhcltnam) {
		this.kjhcltnam = kjhcltnam;
	}
	public void setZhospopt(String zhospopt) {
		this.zhospopt = zhospopt;
	}
	public void setZrevdate(int zrevdate) {
		this.zrevdate = zrevdate;
	}
	public void setZlownnam(String zlownnam) {
		this.zlownnam = zlownnam;
	}
	public void setZcsumins(BigDecimal zcsumins) {
		this.zcsumins = zcsumins;
	}
	public void setZasumins(BigDecimal zasumins) {
		this.zasumins = zasumins;
	}
	public void setZdethopt(String zdethopt) {
		this.zdethopt = zdethopt;
	}
	public void setZrqflag(String zrqflag) {
		this.zrqflag = zrqflag;
	}
	public void setZrqsetdte(int zrqsetdte) {
		this.zrqsetdte = zrqsetdte;
	}
	public void setZrqcandte(int zrqcandte) {
		this.zrqcandte = zrqcandte;
	}
	public void setZepflag(String zepflag) {
		this.zepflag = zepflag;
	}
	public void setZcvflag(String zcvflag) {
		this.zcvflag = zcvflag;
	}
	public void setZcontsex(String zcontsex) {
		this.zcontsex = zcontsex;
	}
	public void setZcontdob(int zcontdob) {
		this.zcontdob = zcontdob;
	}
	public void setZcontaddr(String zcontaddr) {
		this.zcontaddr = zcontaddr;
	}
	public void setLifnamkj(String lifnamkj) {
		this.lifnamkj = lifnamkj;
	}
	public void setOwnnamkj(String ownnamkj) {
		this.ownnamkj = ownnamkj;
	}
	public void setWflga(String wflga) {
		this.wflga = wflga;
	}
	public void setZerrflg(String zerrflg) {
		this.zerrflg = zerrflg;
	}
	public void setZoccdate(int zoccdate) {
		this.zoccdate = zoccdate;
	}
	public void setZtrdate(int ztrdate) {
		this.ztrdate = ztrdate;
	}
	public void setZadrddte(int zadrddte) {
		this.zadrddte = zadrddte;
	}
	public void setZexdrddte(int zexdrddte) {
		this.zexdrddte = zexdrddte;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public void setCownnnum(String cownnnum) {
		this.cownnnum = cownnnum;
	}
	public void setLifecnum(String lifecnum) {
		this.lifecnum = lifecnum;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public void setSenddate(int senddate) {
		this.senddate = senddate;
	}
	public void setRecvdate(int recvdate) {
		this.recvdate = recvdate;
	}
	public void setRecveror(String recveror) {
		this.recveror = recveror;
	}
	public void setDummy(String dummy) {
		this.dummy = dummy;
	}
	
}
