package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.List;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Fpcopf;
import com.dxc.integral.life.dao.model.Fprmpf;
import com.dxc.integral.life.dao.model.Hdivpf;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Taxdpf;


/**
 * L2Billing batch reader DTO.
 * 
 * @author wli31
 *
 */
public class L2BillingReaderDTO {
	private String chdrcoy; 
	private String chdrnum;	
	private int payrseqno;
	private String billsupr;
	private int billspfrom;
	private int billspto;
	private String billchnl;
	private int billcd;
	private String membername;
	private int payrtranno;
	private int payrbillcd;
	private int btdate;
	private int ptdate;
	private BigDecimal outstamt;
	private int nextdate;
	private String payrbillcurr;
	private String billfreq;
	private String duedd;
	private String duemm;
	private String billday;
	private String billmonth;
	private BigDecimal sinstamt01;
	private BigDecimal sinstamt02;
	private BigDecimal sinstamt03;
	private BigDecimal sinstamt04;
	private BigDecimal sinstamt05;
	private BigDecimal sinstamt06;
	private String payrcntcurr;
	private String mandref;
	private String grupcoy;
	private String grupnum;
	private String membsel;
	private String taxrelmth;
	private int incseqno;
	private int effdate;
	private long chdruniqueno;
	private int chdrtranno;
	private String cnttype;
	private int occdate;
	private String statcode;
	private String pstcde;
	private String reg;
	private String chdrcntcurr;
	private String billcurr;
	private String chdrpfx;
	private String cowncoy;
	private String cownnum;
	private String cntbranch;
	private String servunit;
	private int ccdate;
	private String collchnl;
	private String cownpfx;
	private String agntpfx;
	private String agntcoy;
	private String agntnum;
	private String chdrbillchnl;
	private String acctmeth;
	private String clntpfx;
	private String clntcoy;
	private String clntnum;
	private String bankkey;
	private String bankacckey;
	private String mandstat;
	private String facthous; 	
	// insert fields
	private List<Ptrnpf> ptrnpfList;
	private List<Linspf> linspfList;
	private List<Hdivpf> hdivpfList;
	private List<Taxdpf> taxdpfs;
	// update fields
	private Chdrpf chdrpf;
	private Payrpf payrpf;
	private List<Fpcopf> fpcopfList;
	private List<Fprmpf> fprmpfList;
	
	public L2BillingReaderDTO() {
	}
	public List<Ptrnpf> getPtrnpfList() {
		return ptrnpfList;
	}
	public void setPtrnpfList(List<Ptrnpf> ptrnpfList) {
		this.ptrnpfList = ptrnpfList;
	}
	public List<Hdivpf> getHdivpfList() {
		return hdivpfList;
	}
	public void setHdivpfList(List<Hdivpf> hdivpfList) {
		this.hdivpfList = hdivpfList;
	}
	public List<Fpcopf> getFpcopfList() {
		return fpcopfList;
	}
	public void setFpcopfList(List<Fpcopf> fpcopfList) {
		this.fpcopfList = fpcopfList;
	}
	public List<Fprmpf> getFprmpfList() {
		return fprmpfList;
	}
	public void setFprmpfList(List<Fprmpf> fprmpfList) {
		this.fprmpfList = fprmpfList;
	}
	public List<Linspf> getLinspfList() {
		return linspfList;
	}
	public void setLinspfList(List<Linspf> linspfList) {
		this.linspfList = linspfList;
	}
	public List<Taxdpf> getTaxdpfs() {
		return taxdpfs;
	}
	public void setTaxdpfs(List<Taxdpf> taxdpfs) {
		this.taxdpfs = taxdpfs;
	}
	public Chdrpf getChdrpf() {
		return chdrpf;
	}
	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}
	public Payrpf getPayrpf() {
		return payrpf;
	}
	public void setPayrpf(Payrpf payrpf) {
		this.payrpf = payrpf;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public String getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(String billsupr) {
		this.billsupr = billsupr;
	}
	public int getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(int billspfrom) {
		this.billspfrom = billspfrom;
	}
	public int getBillspto() {
		return billspto;
	}
	public void setBillspto(int billspto) {
		this.billspto = billspto;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getMembername() {
		return membername;
	}
	public void setMembername(String membername) {
		this.membername = membername;
	}
	public int getPayrtranno() {
		return payrtranno;
	}
	public void setPayrtranno(int payrtranno) {
		this.payrtranno = payrtranno;
	}
	public int getPayrbillcd() {
		return payrbillcd;
	}
	public void setPayrbillcd(int payrbillcd) {
		this.payrbillcd = payrbillcd;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public BigDecimal getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(BigDecimal outstamt) {
		this.outstamt = outstamt;
	}
	public int getNextdate() {
		return nextdate;
	}
	public void setNextdate(int nextdate) {
		this.nextdate = nextdate;
	}
	public String getPayrbillcurr() {
		return payrbillcurr;
	}
	public void setPayrbillcurr(String payrbillcurr) {
		this.payrbillcurr = payrbillcurr;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getDuedd() {
		return duedd;
	}
	public void setDuedd(String duedd) {
		this.duedd = duedd;
	}
	public String getDuemm() {
		return duemm;
	}
	public void setDuemm(String duemm) {
		this.duemm = duemm;
	}
	public String getBillday() {
		return billday;
	}
	public void setBillday(String billday) {
		this.billday = billday;
	}
	public String getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(String billmonth) {
		this.billmonth = billmonth;
	}

	public BigDecimal getSinstamt(int ctr) {
		switch (ctr) {
		case 1:
			return sinstamt01;
		case 2:
			return sinstamt02;
		case 3:
			return sinstamt03;
		case 4:
			return sinstamt04;
		case 5:
			return sinstamt05;
		case 6:
			return sinstamt06;
		default:
			return BigDecimal.ZERO;
		}
	}
	public BigDecimal getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(BigDecimal sinstamt01) {
		this.sinstamt01 = sinstamt01;
	}
	public BigDecimal getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(BigDecimal sinstamt02) {
		this.sinstamt02 = sinstamt02;
	}
	public BigDecimal getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(BigDecimal sinstamt03) {
		this.sinstamt03 = sinstamt03;
	}
	public BigDecimal getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(BigDecimal sinstamt04) {
		this.sinstamt04 = sinstamt04;
	}
	public BigDecimal getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(BigDecimal sinstamt05) {
		this.sinstamt05 = sinstamt05;
	}
	public BigDecimal getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(BigDecimal sinstamt06) {
		this.sinstamt06 = sinstamt06;
	}
	public String getPayrcntcurr() {
		return payrcntcurr;
	}
	public void setPayrcntcurr(String payrcntcurr) {
		this.payrcntcurr = payrcntcurr;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public String getGrupcoy() {
		return grupcoy;
	}
	public void setGrupcoy(String grupcoy) {
		this.grupcoy = grupcoy;
	}
	public String getGrupnum() {
		return grupnum;
	}
	public void setGrupnum(String grupnum) {
		this.grupnum = grupnum;
	}
	public String getMembsel() {
		return membsel;
	}
	public void setMembsel(String membsel) {
		this.membsel = membsel;
	}
	public String getTaxrelmth() {
		return taxrelmth;
	}
	public void setTaxrelmth(String taxrelmth) {
		this.taxrelmth = taxrelmth;
	}
	public int getIncseqno() {
		return incseqno;
	}
	public void setIncseqno(int incseqno) {
		this.incseqno = incseqno;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public long getChdruniqueno() {
		return chdruniqueno;
	}
	public void setChdruniqueno(long chdruniqueno) {
		this.chdruniqueno = chdruniqueno;
	}
	public int getChdrtranno() {
		return chdrtranno;
	}
	public void setChdrtranno(int chdrtranno) {
		this.chdrtranno = chdrtranno;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getPstcde() {
		return pstcde;
	}
	public void setPstcde(String pstcde) {
		this.pstcde = pstcde;
	}
	public String getReg() {
		return reg;
	}
	public void setReg(String reg) {
		this.reg = reg;
	}
	public String getChdrcntcurr() {
		return chdrcntcurr;
	}
	public void setChdrcntcurr(String chdrcntcurr) {
		this.chdrcntcurr = chdrcntcurr;
	}
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}
	public String getServunit() {
		return servunit;
	}
	public void setServunit(String servunit) {
		this.servunit = servunit;
	}
	public int getCcdate() {
		return ccdate;
	}
	public void setCcdate(int ccdate) {
		this.ccdate = ccdate;
	}
	public String getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(String collchnl) {
		this.collchnl = collchnl;
	}
	public String getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}
	public String getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(String agntpfx) {
		this.agntpfx = agntpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getChdrbillchnl() {
		return chdrbillchnl;
	}
	public void setChdrbillchnl(String chdrbillchnl) {
		this.chdrbillchnl = chdrbillchnl;
	}
	public String getAcctmeth() {
		return acctmeth;
	}
	public void setAcctmeth(String acctmeth) {
		this.acctmeth = acctmeth;
	}
	public String getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
}