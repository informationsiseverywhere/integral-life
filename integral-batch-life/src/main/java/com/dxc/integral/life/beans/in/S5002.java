package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5002 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String chdrsel;
	private String chdrtype;
	private String action;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getChdrsel() {
		return chdrsel;
	}
	public void setChdrsel(String chdrsel) {
		this.chdrsel = chdrsel;
	}
	public String getChdrtype() {
		return chdrtype;
	}
	public void setChdrtype(String chdrtype) {
		this.chdrtype = chdrtype;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5002 []";
	}
	
}
