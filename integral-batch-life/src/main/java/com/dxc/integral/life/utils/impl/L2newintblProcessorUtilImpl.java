package com.dxc.integral.life.utils.impl;


import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.beans.L2newintblReaderDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Instintcalc;
import com.dxc.integral.life.utils.L2newintblProcessorUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("l2newintblProcessorUtil")
@Lazy
public class L2newintblProcessorUtilImpl implements L2newintblProcessorUtil {
	 private static final Logger LOGGER = LoggerFactory.getLogger(L2newintblProcessorUtilImpl.class);
	@Autowired
	private ItempfDAO itempfDAO;
	
	@Autowired
	private DescpfDAO descpfDAO;
	
	@Autowired
	private Datcon2 datcon2;
	
	@Autowired
	private Instintcalc instinctcalc;
	
	@Autowired
	private Zrdecplc zrdecplc;
	
	private IntcalcDTO intcalcDTO;
	private ZrdecplcDTO zrdecplcDTO;
	private Descpf t5645Descpf = null;
	
	@Override
	 public IntcalcDTO populateIntcalculation(L2newintblReaderDTO readerDTO) {
		 IntcalcDTO intercalDTO = new IntcalcDTO();
		    intercalDTO.setLoanNumber(readerDTO.getLo_loannumber());
		    intercalDTO.setChdrcoy(readerDTO.getLo_chdrcoy());
		    intercalDTO.setChdrnum(readerDTO.getLo_chdrnum());
		    intercalDTO.setCnttype(readerDTO.getCh_cnttype());
		    intercalDTO.setInterestTo(readerDTO.getLo_nxtintbdte());
		    intercalDTO.setInterestFrom(readerDTO.getLo_lstintbdte());
		    intercalDTO.setLoanorigam(readerDTO.getLo_lstcaplamt());
		    intercalDTO.setLastCaplsnDate(readerDTO.getLo_lstcapdate());
		    intercalDTO.setLoanStartDate(readerDTO.getLo_loanstdate());
		    intercalDTO.setLoanCurrency(readerDTO.getLo_loancurr());
		    intercalDTO.setLoanType(readerDTO.getLo_loantype());	
			return intercalDTO;
	 }
	
	@Override
	 public Integer calcNextBillDate(L2newintblReaderDTO readerDTO) throws IOException, ParseException{
		 Integer intNxtBilDate = new Integer(0);
		 Map<String, Itempf> t6633Map=itempfDAO.readSmartTableByTableName2(Companies.LIFE,"T6633", CommonConstants.IT_ITEMPFX);
			T6633 t6633IO = new ObjectMapper().readValue(t6633Map.get(readerDTO.getCh_cnttype().concat(readerDTO.getLo_loantype())).getGenareaj(), T6633.class);
			if (t6633IO.getLoanAnnivInterest().equals("Y")) {
				intNxtBilDate= readerDTO.getLo_loanstdate();
				while(intNxtBilDate <= readerDTO.getLo_nxtintbdte())
			 intNxtBilDate=datcon2.plusYears(intNxtBilDate, 1);
			}
            if (t6633IO.getPolicyAnnivInterest().equals("Y")) {
            	intNxtBilDate= readerDTO.getCh_occdate();
            	while(intNxtBilDate <= readerDTO.getLo_nxtintbdte())
		       intNxtBilDate =datcon2.plusYears(intNxtBilDate, 1);
		       return intNxtBilDate;
			}
			if (t6633IO.getInterestFrequency().equals(" ")
					|| t6633IO.getInterestFrequency().equals("01")) {
				 intNxtBilDate= readerDTO.getLo_loanstdate();
				while(intNxtBilDate <= readerDTO.getLo_nxtintbdte())
			 intNxtBilDate=datcon2.plusYears(intNxtBilDate, 1);
				return intNxtBilDate;
			}
			else if(t6633IO.getInterestFrequency().equals("02")) { // half yearly
				intNxtBilDate= readerDTO.getLo_loanstdate();
				while(intNxtBilDate <= readerDTO.getLo_nxtintbdte())
			 intNxtBilDate=datcon2.plusMonths(intNxtBilDate, 6);
				return intNxtBilDate;
			} 
			else if(t6633IO.getInterestFrequency().equals("04")) { // quarterly
				intNxtBilDate= readerDTO.getLo_loanstdate();
				while(intNxtBilDate <= readerDTO.getLo_nxtintbdte())
					intNxtBilDate=datcon2.plusMonths(intNxtBilDate, 3);
				return intNxtBilDate;
			} 
			else if(t6633IO.getInterestFrequency().equals("12")) { // monthly
				intNxtBilDate= readerDTO.getLo_loanstdate();
				while(intNxtBilDate <= readerDTO.getLo_nxtintbdte())
					intNxtBilDate=datcon2.plusMonths(intNxtBilDate, 1);
				return intNxtBilDate;
			} 
			else if(t6633IO.getInterestFrequency().equals("24")) { // semi-monthly
				Integer tmpLoanDate= readerDTO.getLo_loanstdate();
				while(tmpLoanDate <= readerDTO.getLo_nxtintbdte())
			 intNxtBilDate=datcon2.plusDays(tmpLoanDate, 15);
				return intNxtBilDate;
			} 
			else if(t6633IO.getInterestFrequency().equals("26")) { // fornightly
				LocalDate tmpLoanDate = DatimeUtil.covertyyyyMMddToLocalDate(readerDTO.getLo_loanstdate());
				tmpLoanDate = tmpLoanDate.plusWeeks(2);
				LocalDate tmpNxtintbdte = DatimeUtil.covertyyyyMMddToLocalDate(readerDTO.getLo_nxtintbdte());
				while (!tmpLoanDate.isAfter(tmpNxtintbdte)) {
					tmpLoanDate = tmpLoanDate.plusWeeks(2);
				}
				try {
					intNxtBilDate = DatimeUtil.covertLocalDateToyyyyMMdd(tmpLoanDate);
				} catch (ParseException e) {
				
					LOGGER.error("Error in calcNextBillDate",e);
				}
				return intNxtBilDate;
			} else if(t6633IO.getInterestFrequency().equals("52")) { // weekly
				LocalDate tmpLoanDate = DatimeUtil.covertyyyyMMddToLocalDate(readerDTO.getLo_loanstdate());
				tmpLoanDate = tmpLoanDate.plusWeeks(1);
				LocalDate tmpNxtintbdte = DatimeUtil.covertyyyyMMddToLocalDate(readerDTO.getLo_nxtintbdte());
				while (!tmpLoanDate.isAfter(tmpNxtintbdte)) {
					tmpLoanDate = tmpLoanDate.plusWeeks(1);
				}
				try {
					intNxtBilDate = DatimeUtil.covertLocalDateToyyyyMMdd(tmpLoanDate);
				} catch (ParseException e) {
					LOGGER.error("Error in calcNextBillDate",e);
				}
				return intNxtBilDate;
			} else if(t6633IO.getInterestFrequency().equals("DY")) { // daily
				
			 intNxtBilDate=datcon2.plusDays(readerDTO.getLo_nxtintbdte(), 1);
				return intNxtBilDate;
			}
			
			return intNxtBilDate;

	 }
	
	@Override
	 public LifacmvDTO populateLifAcmv(L2newintblReaderDTO readerDTO,Batcpf batcpf, Integer lstintbdte) throws Exception{
		  
		    if (t5645Descpf == null) {
				t5645Descpf = descpfDAO.getDescInfo(Companies.LIFE, "T5645", "BR539");
			}
			StringBuilder loanNumber = new StringBuilder("");
			loanNumber.append(String.format("%02d", Integer.parseInt(readerDTO.getLo_loannumber().toString().trim())));
			
			calculateInterest(readerDTO,batcpf, lstintbdte);
			
		    LifacmvDTO lifeacmvDTO = new LifacmvDTO();
		    lifeacmvDTO.setBatccoy(batcpf.getBatccoy());
		    lifeacmvDTO.setBatcbrn(batcpf.getBatcbrn());
		    lifeacmvDTO.setBatcactyr(batcpf.getBatcactyr());
		    lifeacmvDTO.setBatcactmn(batcpf.getBatcactmn());
		    lifeacmvDTO.setBatctrcde(batcpf.getBatctrcde());
		    lifeacmvDTO.setBatcbatch(batcpf.getBatcbatch());
		    lifeacmvDTO.setRldgcoy(readerDTO.getLo_chdrcoy());
		    lifeacmvDTO.setGenlcoy(readerDTO.getLo_chdrcoy());
		    lifeacmvDTO.setRdocnum(readerDTO.getLo_chdrnum());
		    lifeacmvDTO.setGenlcur("");
		    lifeacmvDTO.setOrigcurr(readerDTO.getLo_loancurr());
		    lifeacmvDTO.setOrigamt(intcalcDTO.getInterestAmount());
		    lifeacmvDTO.setRcamt(BigDecimal.ZERO);
		    lifeacmvDTO.setCrate(BigDecimal.ZERO);
		    lifeacmvDTO.setAcctamt(BigDecimal.ZERO);
		    lifeacmvDTO.setContot(0);
		    lifeacmvDTO.setTranno(readerDTO.getCh_tranno());
		    lifeacmvDTO.setFrcdate(99999999);
		    lifeacmvDTO.setEffdate(readerDTO.getLo_nxtintbdte());
		    lifeacmvDTO.setTranref(readerDTO.getLo_chdrnum());
		    lifeacmvDTO.setTrandesc(t5645Descpf.getLongdesc());
		    lifeacmvDTO.setRldgacct(readerDTO.getLo_chdrnum().concat(loanNumber.toString()));
		    lifeacmvDTO.setTermid("");
		    lifeacmvDTO.setTransactionDate(readerDTO.getLo_trdt());
		    lifeacmvDTO.setTransactionTime(readerDTO.getLo_trtm());
		    lifeacmvDTO.setUser(0);
			List<String> codeList = new ArrayList<>();
			codeList.add(readerDTO.getCh_cnttype());
			lifeacmvDTO.setSubstituteCodes(codeList);
		    return lifeacmvDTO;
	 }
	
	protected void calculateInterest(L2newintblReaderDTO readerDTO,Batcpf batcpf, Integer lstintbdte) throws Exception{
		intcalcDTO = new IntcalcDTO();
		
		intcalcDTO.setLoanNumber(readerDTO.getLo_loannumber());
		intcalcDTO.setChdrcoy(readerDTO.getLo_chdrcoy());
		intcalcDTO.setChdrnum(readerDTO.getLo_chdrnum());
		intcalcDTO.setCnttype(readerDTO.getCh_cnttype());
		intcalcDTO.setInterestTo(readerDTO.getEffectiveDate());
		intcalcDTO.setInterestFrom(lstintbdte);
		intcalcDTO.setLoanorigam(readerDTO.getLo_lstcaplamt());
		intcalcDTO.setLastCaplsnDate(readerDTO.getLo_lstcapdate());
		intcalcDTO.setLoanStartDate(readerDTO.getLo_loanstdate());
		intcalcDTO.setInterestAmount(BigDecimal.ZERO);
		intcalcDTO.setLoanCurrency(readerDTO.getLo_loancurr());
		intcalcDTO.setLoanType(readerDTO.getLo_loantype());
		
		intcalcDTO = instinctcalc.processInstintcalc(intcalcDTO);
		
		if (intcalcDTO==null) {
			fatalError("calculateInterest in l2newintblProcessorUtil error");
		}	
		if (intcalcDTO.getInterestAmount().compareTo(BigDecimal.ZERO)==0) {
			zrdecplcDTO = new ZrdecplcDTO();
			zrdecplcDTO.setAmountIn(intcalcDTO.getInterestAmount());
			zrdecplcDTO.setCompany(batcpf.getBatccoy());
			zrdecplcDTO.setCurrency(readerDTO.getLo_loancurr());
			zrdecplcDTO.setBatctrcde(batcpf.getBatctrcde());
			intcalcDTO.setInterestAmount(zrdecplc.convertAmount(zrdecplcDTO));	
		}		
			
	}
			
	public void fatalError(String status) {
		throw new StopRunException(status);
	}
}
