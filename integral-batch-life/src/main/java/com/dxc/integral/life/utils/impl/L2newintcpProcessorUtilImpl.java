package com.dxc.integral.life.utils.impl;

import java.io.IOException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.beans.L2newintcpReaderDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.NfloanDTO;
import com.dxc.integral.life.beans.L2newintcpProcessorDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Intcalc;
import com.dxc.integral.life.utils.L2newintcpProcessorUtil;
import com.dxc.integral.life.utils.Nfloan;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * L2NEWINTCP processor utility implementation.
 * 
 * @author dpuhawan
 *
 */

@Service("l2newintcpProcessorUtil")
@Lazy
public class L2newintcpProcessorUtilImpl implements L2newintcpProcessorUtil {
	
	@Autowired
	private Zrdecplc zrdecplc;		
	
	@Autowired
	private Intcalc intcalc;
	
	@Autowired
	private Datcon2 datcon2;	
	
	@Autowired
	private LifacmvImpl lifacmv;	
	
	@Autowired
	private DescpfDAO descpfDAO;	
	
	@Autowired
	private AcmvpfDAO acmvpfDAO;
	
	@Autowired
	private AcblpfDAO acblpfDAO;
	
	@Autowired
	private ItempfDAO itempfDAO;	
	
	@Autowired
	private Xcvrt xcvrt;	
	
	@Autowired
	private Nfloan nfloan;	
	
	
	private int sequenceNo;
	private Acblpf acblpf = null;
	private Acmvpf acmvpf = null;
	private List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
	
	private Map<String, List<Itempf>> t3695ListMap = new HashMap<String, List<Itempf>>();
	private List<T3695> t3695IOList = new ArrayList<T3695>();	
	private T3695 t3695IO = null;	
	
	public static final String DAY28 = "28";
	public static final String DAY29 = "29";
	public static final String DAY30 = "30";
	public static final String DAY31 = "31";
	private static final Collection<Month> MONTH_WITH_30DAYS = Arrays.asList(Month.APRIL, Month.JUNE, Month.SEPTEMBER,
			Month.NOVEMBER);	
	
	public L2newintcpProcessorDTO process(L2newintcpReaderDTO readerDTO, Integer tranDate, List<T6633> T6633List, List<T5645> t5645IOList, Batcpf batcpf) throws IOException, ParseException {
		L2newintcpProcessorDTO processorDTO = new L2newintcpProcessorDTO();
		
		if (readerDTO.getLstintbdte() < tranDate) {
			processorDTO = calculateInterest(readerDTO, tranDate);
			postInterestAcmv(processorDTO, readerDTO, batcpf, tranDate, t5645IOList);
			processorDTO = calculateNextInterestBillDate(processorDTO,readerDTO, tranDate, T6633List);
		}
		processorDTO = calculateTotalLoanDebt(processorDTO,readerDTO, tranDate, t5645IOList);
		capitalise(processorDTO, readerDTO, batcpf, tranDate, t5645IOList);
		processorDTO = calculateNextCapitalisationDate(processorDTO,readerDTO, tranDate, T6633List);
		processorDTO = checkSurrenderValue(processorDTO,readerDTO, tranDate);

		return processorDTO;
	}
	
	public L2newintcpReaderDTO fillDTOInfo(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Batcpf batcpf, Integer tranDate) {
		
		//Update Loanpf
		readerDTO.setLstintbdte(tranDate);
		readerDTO.setNxtintbdte(processorDTO.getNxtIntbdate());
		
		//ptrnpf required
		readerDTO.setBatcactmn(batcpf.getBatcactmn());
		readerDTO.setBatcactyr(batcpf.getBatcactyr());
		readerDTO.setBatcbatch(batcpf.getBatcbatch());
		readerDTO.setBatcbrn(batcpf.getBatcbrn());
		readerDTO.setBatccoy(batcpf.getBatccoy());
		readerDTO.setBatcpfx(batcpf.getBatcpfx());
		readerDTO.setBatctrcde(batcpf.getBatctrcde());
		readerDTO.setTrdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
		readerDTO.setTrtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
		readerDTO.setTermid(batcpf.getTermid());
		readerDTO.setDatesub(tranDate);
		readerDTO.setPtrneff(tranDate);
		
		readerDTO.setLstcapdate(tranDate);
		readerDTO.setLstcaplamt(processorDTO.getTotalLoanDebtAmount());
		readerDTO.setNxtcapdate(processorDTO.getNxtCapdate());
		
		return readerDTO;
	}	
	
	private L2newintcpProcessorDTO calculateInterest(L2newintcpReaderDTO readerDTO, Integer tranDate) throws IOException, ParseException{
		L2newintcpProcessorDTO processorDTO = new L2newintcpProcessorDTO();
		
		IntcalcDTO intercalDTO = new IntcalcDTO();
		intercalDTO.setLoanNumber(readerDTO.getLoannumber());
		intercalDTO.setChdrcoy(readerDTO.getChdrcoy());
		intercalDTO.setChdrnum(readerDTO.getChdrnum());
		intercalDTO.setCnttype(readerDTO.getCnttype());
		intercalDTO.setInterestTo(tranDate);
		intercalDTO.setInterestFrom(readerDTO.getLstintbdte());
		intercalDTO.setLoanorigam(readerDTO.getLstcaplamt());
		intercalDTO.setLastCaplsnDate(readerDTO.getLstcapdate());
		intercalDTO.setLoanStartDate(readerDTO.getLoanstdate());
		intercalDTO.setInterestAmount(BigDecimal.ZERO);
		intercalDTO.setLoanCurrency(readerDTO.getLoancurr());
		intercalDTO.setLoanType(readerDTO.getLoantype());

		BigDecimal totalIntrest = intcalc.interestCalculation(intercalDTO);
		
		if (!BigDecimal.ZERO.equals(totalIntrest)) totalIntrest = callRounding(totalIntrest, readerDTO);
		
		processorDTO.setInterestAmount(totalIntrest);
		
		return processorDTO;				
	}	
	
	private L2newintcpProcessorDTO calculateNextInterestBillDate(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Integer tranDate, List<T6633> T6633List) throws IOException, ParseException{
		
		if ("Y".equalsIgnoreCase(T6633List.get(0).getLoanAnnivInterest())){
			int nextIntBillDate = readerDTO.getLoanstdate();
			int calculatedDate = 0;
			while (calculatedDate <= tranDate) {
				calculatedDate = datcon2.plusYears(nextIntBillDate, 1);
				nextIntBillDate = calculatedDate;
			}			
			processorDTO.setNxtIntbdate(nextIntBillDate);
		}else {
			if ("Y".equalsIgnoreCase(T6633List.get(0).getPolicyAnnivInterest())){
				int nextIntBillDate = readerDTO.getOccdate();
				int calculatedDate = 0;
				while (calculatedDate <= tranDate) {
					calculatedDate = datcon2.plusYears(nextIntBillDate, 1);
					nextIntBillDate = calculatedDate;
				}				
				processorDTO.setNxtIntbdate(nextIntBillDate);
			}else {
				LocalDate loanDate = LocalDate.parse(readerDTO.getLoanstdate().toString(), DateTimeFormatter.ofPattern("yyyyMMdd"));
				String newDay;
				String interestDay="";
				if (T6633List.get(0).getInterestDay() != null && T6633List.get(0).getInterestDay() != 0) {
					if (T6633List.get(0).getInterestDay() < 10) {
						interestDay = "0" + T6633List.get(0).getInterestDay().toString();
					} else {
						interestDay = T6633List.get(0).getInterestDay().toString();
					}
					if (interestDay.compareTo(DAY28) > 0) {
						if (interestDay == DAY31 && MONTH_WITH_30DAYS.contains(loanDate.getMonth())) {
							newDay = DAY30;
						} else if (loanDate.getMonth().equals(Month.FEBRUARY)) {
							if (loanDate.isLeapYear()) {
								newDay = DAY29;
							} else {
								newDay = DAY28;
							}
						} else {
							newDay = interestDay;
						}
					} else {
						newDay = interestDay;
					}
				} else {
					newDay = readerDTO.getLoanstdate().toString().substring(6, 8);
				}
				
				int nextIntBillDate = Integer.parseInt(Integer.toString(readerDTO.getLoanstdate()).substring(0, 6) + newDay);
				int calculatedDate = 0;
				while (calculatedDate <= tranDate) {
					if (T6633List.get(0).getInterestFrequency() == null || "".equals(T6633List.get(0).getInterestFrequency())) {
						calculatedDate = datcon2.plusYears(nextIntBillDate, 1);
					} else {
						calculatedDate = getDatebyfrequency(nextIntBillDate, T6633List.get(0).getInterestFrequency());
						nextIntBillDate = calculatedDate;
					}
				}
				
				processorDTO.setNxtIntbdate(calculatedDate);
				if (interestDay.compareTo(DAY28) > 0 && (loanDate.getMonth().equals(Month.FEBRUARY)
						|| (interestDay == DAY31 && MONTH_WITH_30DAYS.contains(loanDate.getMonth())))){
					processorDTO.setNxtIntbdate(Integer.parseInt(Integer.toString(calculatedDate).substring(0, 6) + interestDay));
				}				
			}	
		}
		
		return processorDTO;
	}
	
	private L2newintcpProcessorDTO calculateTotalLoanDebt(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Integer tranDate, List<T5645> t5645List) throws IOException, ParseException{
		BigDecimal currBal;
		BigDecimal postedAmount = BigDecimal.ZERO;
		BigDecimal totalLoanDebt = BigDecimal.ZERO;	
		String loanNumber = String.format("%02d", Integer.parseInt(readerDTO.getLoannumber().toString().trim()));
		acblpf = new Acblpf();
		acblpf.setRldgcoy(readerDTO.getChdrcoy());
		acblpf.setSacstyp(t5645List.get(0).getSacstypes().get(readerDTO.getLoantypidx()));
		acblpf.setSacscode(t5645List.get(0).getSacscodes().get(readerDTO.getLoantypidx()));
		acblpf.setRldgacct(readerDTO.getChdrnum().concat(loanNumber));
		acblpf.setOrigcurr(readerDTO.getLoancurr());
		acblpf = acblpfDAO.readAcblpf(acblpf);
		
		if (acblpf == null) currBal = BigDecimal.ZERO;
		else {
			if ("D".equals(readerDTO.getLoantype())) currBal = acblpf.getSacscurbal().negate();
			else currBal = acblpf.getSacscurbal();	
		}
		
		acmvpfList = acmvpfDAO.getAcmvpfList(readerDTO.getChdrnum().concat(loanNumber), t5645List.get(0).getSacscodes().get(readerDTO.getLoantypidx()), t5645List.get(0).getSacstypes().get(readerDTO.getLoantypidx()), readerDTO.getLstcapdate(), readerDTO.getChdrcoy());
		
		Iterator<Acmvpf> acmvpfIter;
		if(acmvpfList != null && acmvpfList.size()>0){
			acmvpfIter = acmvpfList.iterator();    	      	 
			while (acmvpfIter.hasNext()) {
				postedAmount = postedAmount.add(processAcmvpfList(acmvpfIter.next(), readerDTO, tranDate));
			}
		}
		
		postedAmount = postedAmount.add(readerDTO.getLstcaplamt());
		totalLoanDebt = postedAmount.add(currBal);
		
		processorDTO.setCurrBal(currBal);
		processorDTO.setTotalLoanDebtAmount(totalLoanDebt);
		
		return processorDTO;
	}
	
	private BigDecimal processAcmvpfList(Acmvpf acmvpf, L2newintcpReaderDTO readerDTO, Integer tranDate) throws IOException{
		BigDecimal roundedAmount = BigDecimal.ZERO;
		BigDecimal postedAmount = BigDecimal.ZERO;
		ConlinkOutputDTO conlinkOutput = null;
		this.getT3695Info(readerDTO, tranDate, acmvpf.getSacstyp());
		if ("-".equals(t3695IOList.get(0).getSign())) {
			if ("-".equals(acmvpf.getGlsign())) acmvpf.setGlsign("+");
			else acmvpf.setGlsign("-");
		}
		
		if (readerDTO.getLstcapdate().equals(acmvpf.getEffdate())) {
			if(readerDTO.getLoanstdate().equals(readerDTO.getLstcapdate()) && acmvpf.getGlsign().equals("-")) {
				
			}else return BigDecimal.ZERO;	
		}
		
		if (acmvpf.getOrigcurr().equals(readerDTO.getLoancurr())) {
			
			ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
			conlinkInput.setFromCurrency(acmvpf.getOrigcurr());
			conlinkInput.setToCurrency(readerDTO.getLoancurr());
			conlinkInput.setCompany(readerDTO.getChdrcoy());
			conlinkInput.setAmount(acmvpf.getOrigamt());
			conlinkInput.setCashdate(99999999);
			
			conlinkOutput = xcvrt.executeCvrtFunction(conlinkInput);
			
			if (!conlinkOutput.getCalculatedAmount().equals(0)) {
				roundedAmount = callRounding(conlinkOutput.getCalculatedAmount(), readerDTO);
				if ("-".equals(acmvpf.getGlsign())){
					roundedAmount = roundedAmount.negate();
				}				
			}
			conlinkOutput.setCalculatedAmount(roundedAmount);	
			postedAmount = conlinkOutput.getCalculatedAmount();
		}
		else {
			if ("-".equals(acmvpf.getGlsign())) {
				acmvpf.setOrigamt(acmvpf.getOrigamt().negate());
			}
			postedAmount.add(acmvpf.getOrigamt());
		}		
		return postedAmount;
	}
	
	private BigDecimal callRounding(BigDecimal amountIn, L2newintcpReaderDTO readerDTO ) throws IOException {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(readerDTO.getLoancurr());
		zrdecplDTO.setCompany(readerDTO.getChdrcoy());
		zrdecplDTO.setBatctrcde(readerDTO.getBatctrcde());
		return zrdecplc.convertAmount(zrdecplDTO);	
	}
	
	private Integer getDatebyfrequency(Integer nxtcapdate, String frequency) {
		Integer nextdate = null;
		if ("12".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 1);
		}
		if ("02".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 6);
		}
		if ("04".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 3);
		}
		return nextdate;
	}	
	
	private void postInterestAcmv(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Batcpf batcpf, Integer tranDate, List<T5645> t5645IOList)
			throws IOException,ParseException {

		LifacmvDTO lifeacmvDTO = new LifacmvDTO();
		
		String loanNumber = String.format("%02d", Integer.parseInt(readerDTO.getLoannumber().toString().trim()));
		
		lifeacmvDTO.setBatccoy(batcpf.getBatccoy());
		lifeacmvDTO.setBatcbrn(batcpf.getBatcbrn());
		lifeacmvDTO.setBatcactyr(batcpf.getBatcactyr());
		lifeacmvDTO.setBatcactmn(batcpf.getBatcactmn());
		lifeacmvDTO.setBatctrcde(batcpf.getBatctrcde());
		lifeacmvDTO.setBatcbatch(batcpf.getBatcbatch());
		lifeacmvDTO.setRldgcoy(readerDTO.getChdrcoy());
		lifeacmvDTO.setGenlcoy(readerDTO.getChdrcoy());
		lifeacmvDTO.setRdocnum(readerDTO.getChdrnum());
		lifeacmvDTO.setGenlcur("");
		lifeacmvDTO.setOrigcurr(readerDTO.getLoancurr());
		lifeacmvDTO.setOrigamt(processorDTO.getInterestAmount());
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifeacmvDTO.setRcamt(BigDecimal.ZERO);
		lifeacmvDTO.setCrate(BigDecimal.ZERO);
		lifeacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifeacmvDTO.setContot(0);
		lifeacmvDTO.setTranno(readerDTO.getTranno());
		lifeacmvDTO.setFrcdate(99999999);
		lifeacmvDTO.setEffdate(tranDate);
		lifeacmvDTO.setTranref(readerDTO.getChdrnum());
		String longDesc = descpfDAO.getDescInfo(Companies.LIFE, "T5645", "BR535").getLongdesc();
		lifeacmvDTO.setTrandesc(longDesc);
		lifeacmvDTO.setRldgacct(readerDTO.getChdrnum() + loanNumber);
		lifeacmvDTO.setTermid(batcpf.getTermid());
		lifeacmvDTO.setTransactionDate(Integer.valueOf(DatimeUtil.getCurrentDate()));
		lifeacmvDTO.setTransactionTime(999999);
		lifeacmvDTO.setUser(readerDTO.getUserNum());
		List<String> codeList = new ArrayList<>();
		codeList.add(readerDTO.getCnttype());		
		lifeacmvDTO.setSubstituteCodes(codeList);
		lifeacmvDTO.setSacscode(t5645IOList.get(0).getSacscodes().get(readerDTO.getLoantypidx()));
		lifeacmvDTO.setSacstyp(t5645IOList.get(0).getSacstypes().get(readerDTO.getLoantypidx()));
		lifeacmvDTO.setGlcode(t5645IOList.get(0).getGlmaps().get(readerDTO.getLoantypidx()));
		lifeacmvDTO.setGlsign(t5645IOList.get(0).getSigns().get(readerDTO.getLoantypidx()));
		lifacmv.executePSTW(lifeacmvDTO);
		lifeacmvDTO.setSacscode(t5645IOList.get(0).getSacscodes().get(readerDTO.getLoantypidx() + 1));
		lifeacmvDTO.setSacstyp(t5645IOList.get(0).getSacstypes().get(readerDTO.getLoantypidx() + 1));
		lifeacmvDTO.setGlcode(t5645IOList.get(0).getGlmaps().get(readerDTO.getLoantypidx() + 1));
		lifeacmvDTO.setGlsign(t5645IOList.get(0).getSigns().get(readerDTO.getLoantypidx() + 1));
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifacmv.executePSTW(lifeacmvDTO);		
	}
	
	private void capitalise(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Batcpf batcpf, Integer tranDate, List<T5645> t5645IOList) throws IOException,ParseException {
		
		if (processorDTO.getCurrBal().equals(BigDecimal.ZERO)) return; 	

		LifacmvDTO lifeacmvDTO = new LifacmvDTO();
		String loanNumber = String.format("%02d", Integer.parseInt(readerDTO.getLoannumber().toString().trim()));
		lifeacmvDTO.setBatccoy(batcpf.getBatccoy());
		lifeacmvDTO.setBatcbrn(batcpf.getBatcbrn());
		lifeacmvDTO.setBatcactyr(batcpf.getBatcactyr());
		lifeacmvDTO.setBatcactmn(batcpf.getBatcactmn());
		lifeacmvDTO.setBatctrcde(batcpf.getBatctrcde());
		lifeacmvDTO.setBatcbatch(batcpf.getBatcbatch());
		lifeacmvDTO.setRldgcoy(readerDTO.getChdrcoy());
		lifeacmvDTO.setGenlcoy(readerDTO.getChdrcoy());
		lifeacmvDTO.setRdocnum(readerDTO.getChdrnum());
		lifeacmvDTO.setGenlcur("");
		lifeacmvDTO.setOrigcurr(readerDTO.getLoancurr());
		lifeacmvDTO.setOrigamt(processorDTO.getCurrBal());
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifeacmvDTO.setRcamt(BigDecimal.ZERO);
		lifeacmvDTO.setCrate(BigDecimal.ZERO);
		lifeacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifeacmvDTO.setContot(0);
		lifeacmvDTO.setTranno(readerDTO.getTranno());
		lifeacmvDTO.setFrcdate(99999999);
		lifeacmvDTO.setEffdate(tranDate);
		lifeacmvDTO.setTranref(readerDTO.getChdrnum());
		String longDesc = descpfDAO.getDescInfo(Companies.LIFE, "T5645", "BR535").getLongdesc();
		lifeacmvDTO.setTrandesc(longDesc);
		lifeacmvDTO.setRldgacct(readerDTO.getChdrnum() + loanNumber);
		lifeacmvDTO.setTermid(batcpf.getTermid());
		lifeacmvDTO.setTransactionDate(Integer.valueOf(DatimeUtil.getCurrentDate()));
		lifeacmvDTO.setTransactionTime(999999);
		lifeacmvDTO.setUser(readerDTO.getUserNum());
		List<String> codeList = new ArrayList<>();
		codeList.add(readerDTO.getCnttype());		
		lifeacmvDTO.setSubstituteCodes(codeList);
		lifeacmvDTO.setSacscode(t5645IOList.get(0).getSacscodes().get(readerDTO.getLoantypidx() + 2));
		lifeacmvDTO.setSacstyp(t5645IOList.get(0).getSacstypes().get(readerDTO.getLoantypidx() + 2));
		lifeacmvDTO.setGlcode(t5645IOList.get(0).getGlmaps().get(readerDTO.getLoantypidx() + 2));
		lifeacmvDTO.setGlsign(t5645IOList.get(0).getSigns().get(readerDTO.getLoantypidx() + 2));
		lifacmv.executePSTW(lifeacmvDTO);
		
		lifeacmvDTO.setSacscode(t5645IOList.get(0).getSacscodes().get(readerDTO.getLoantypidx() + 3));
		lifeacmvDTO.setSacstyp(t5645IOList.get(0).getSacstypes().get(readerDTO.getLoantypidx() + 3));
		lifeacmvDTO.setGlcode(t5645IOList.get(0).getGlmaps().get(readerDTO.getLoantypidx() + 3));
		lifeacmvDTO.setGlsign(t5645IOList.get(0).getSigns().get(readerDTO.getLoantypidx() + 3));
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifacmv.executePSTW(lifeacmvDTO);		
	}	
	
	private L2newintcpProcessorDTO calculateNextCapitalisationDate(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Integer tranDate, List<T6633> T6633List) throws IOException, ParseException{
		
		if ("Y".equalsIgnoreCase(T6633List.get(0).getAnnloan())){
			int nextCapDate = readerDTO.getLoanstdate();
			int calculatedDate = 0;
			while (calculatedDate <= tranDate) {
				calculatedDate = datcon2.plusYears(nextCapDate, 1);
				nextCapDate = calculatedDate;
			}			
			processorDTO.setNxtCapdate(nextCapDate);
		}else {
			if ("Y".equalsIgnoreCase(T6633List.get(0).getAnnpoly())){
				int nextCapDate = readerDTO.getOccdate();
				int calculatedDate = 0;
				while (calculatedDate <= tranDate) {
					calculatedDate = datcon2.plusYears(nextCapDate, 1);
					nextCapDate = calculatedDate;
				}
				
				processorDTO.setNxtCapdate(nextCapDate);
			}else {
				LocalDate loanDate = LocalDate.parse(readerDTO.getLoanstdate().toString(), DateTimeFormatter.ofPattern("yyyyMMdd"));
				String newDay;
				String interestDay="";
				if (T6633List.get(0).getInterestDay() != null && T6633List.get(0).getInterestDay() != 0) {
					if (T6633List.get(0).getInterestDay() < 10) {
						interestDay = "0" + T6633List.get(0).getInterestDay().toString();
					} else {
						interestDay = T6633List.get(0).getInterestDay().toString();
					}
					if (interestDay.compareTo(DAY28) > 0) {
						if (interestDay == DAY31 && MONTH_WITH_30DAYS.contains(loanDate.getMonth())) {
							newDay = DAY30;
						} else if (loanDate.getMonth().equals(Month.FEBRUARY)) {
							if (loanDate.isLeapYear()) {
								newDay = DAY29;
							} else {
								newDay = DAY28;
							}
						} else {
							newDay = interestDay;
						}
					} else {
						newDay = interestDay;
					}
				} else {
					newDay = readerDTO.getLoanstdate().toString().substring(6, 8);
				}
				
				int nextCapDate = Integer.parseInt(Integer.toString(readerDTO.getLoanstdate()).substring(0, 6) + newDay);
				int calculatedDate = 0;
				while (calculatedDate <= tranDate) {
					if (T6633List.get(0).getInterestFrequency() == null || "".equals(T6633List.get(0).getInterestFrequency())) {
						calculatedDate = datcon2.plusYears(nextCapDate, 1);
					} else {
						calculatedDate = getDatebyfrequency(nextCapDate, T6633List.get(0).getInterestFrequency());
						nextCapDate = calculatedDate;
					}
				}
				
				processorDTO.setNxtCapdate(calculatedDate);
				if (interestDay.compareTo(DAY28) > 0 && (loanDate.getMonth().equals(Month.FEBRUARY)
						|| (interestDay == DAY31 && MONTH_WITH_30DAYS.contains(loanDate.getMonth())))){
					processorDTO.setNxtCapdate(Integer.parseInt(Integer.toString(calculatedDate).substring(0, 6) + interestDay));
				}				
			}	
		}
		
		return processorDTO;
	}	
	
	
	private void getT3695Info(L2newintcpReaderDTO readerDTO, Integer tranDate, String itemKey) throws IOException {
		//Variable keyitem
		if (t3695IOList.isEmpty()) t3695ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T3695", CommonConstants.IT_ITEMPFX);
		
		String t3695Itemkey = itemKey;
		
		if(!t3695ListMap.isEmpty() && t3695ListMap.containsKey(t3695Itemkey)) {
			List<Itempf> t3695ItemList = t3695ListMap.get(t3695Itemkey);
			for (int i = 0; i < t3695ItemList.size(); i++) {
				if (t3695ItemList.get(i).getItmfrm() > 0) {
					if (tranDate >= t3695ItemList.get(i).getItmfrm() && tranDate <= t3695ItemList.get(i).getItmto()) {
						t3695IO = new ObjectMapper().readValue(t3695ItemList.get(i).getGenareaj(), T3695.class);
						t3695IOList.add(t3695IO);						
					}					
				}else {
					t3695IO = new ObjectMapper().readValue(t3695ItemList.get(i).getGenareaj(), T3695.class);
					t3695IOList.add(t3695IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+t3695Itemkey+" not found in Table T3695 while processing contract "+readerDTO.getChdrnum());
		}
	}	
	
	private L2newintcpProcessorDTO checkSurrenderValue(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Integer tranDate) throws IOException, ParseException{
		

		if (!"A".equals(readerDTO.getLoantype()) && !"P".equals(readerDTO.getLoantype())) return null;
				
		NfloanDTO nfloanDTO = new NfloanDTO();
		
		nfloanDTO.setLanguage(readerDTO.getLanguage());
		nfloanDTO.setChdrcoy(readerDTO.getChdrcoy());
		nfloanDTO.setChdrnum(readerDTO.getChdrnum());
		nfloanDTO.setCntcurr(readerDTO.getCntcurr());
		nfloanDTO.setLoantype(readerDTO.getLoantype());
		nfloanDTO.setEffdate(tranDate);
		nfloanDTO.setPtdate(readerDTO.getPtdate());
		nfloanDTO.setBatchbrn(readerDTO.getBatcbrn());
		nfloanDTO.setCompany(readerDTO.getChdrcoy());
		nfloanDTO.setBillfreq(readerDTO.getBillfreq());
		nfloanDTO.setCnttype(readerDTO.getCnttype());
		nfloanDTO.setPolsum(readerDTO.getPolsum());
		nfloanDTO.setPolinc(readerDTO.getPolinc());
		nfloanDTO.setBatctrcde(readerDTO.getBatctrcde());
		nfloanDTO.setOutstamt(BigDecimal.ZERO);
		
		nfloan.processNFLoan(nfloanDTO);
		
		return processorDTO;
	}	
	
}
