package com.dxc.integral.life.beans;

import java.util.Date;


/**
 * L2ANENDRLX batch reader DTO.
 * 
 * @author mmalik25
 *
 */
public class L2ownchgReaderDTO {
	private long uniqueNumber;
	private String chdrpfx;// CHAR(2 CHAR),
	private String chdrcoy;// CHAR(1 CHAR),
	private String chdrnum;// CHAR(8 CHAR),
	private String cownpfx;// CHAR(2 CHAR),
	private String cowncoy; // CHAR(1 CHAR),
	private String cownnum; // CHAR(8 CHAR),
	private String cnttype; // CHAR(3 CHAR),
	private int tranno; // NUMBER(5),
	private String occdate; // NUMBER(8),
	private String statcode;// CHAR(2 CHAR),
	private String pstcde;// CHAR(2 CHAR),
	private String zsufcdte;// NUMBER(8),
	private String cl1lsurname; // CHAR(60 CHAR),
	private String cl1lgivname; // CHAR(60 CHAR),
	private String cl2lsurname; // CHAR(60 CHAR),
	private String cl2lgivname; // CHAR(60 CHAR),
	private String cl2clntdob;
	private String lifcnum; // CHAR(8 CHAR)
	private String usrprf;
	private String jobnm;
	private String policyTranno;
	private Integer userNum;
	private String batcbrn;
	private String termid;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batctrcde;
	private String batcbatch;
	private String batcpfx;
	private String batccoy;
	private Integer effectiveDate;
	private Integer trdt;
	private Integer trtm;
	private int tranCount;
	private String currto;
	private String tranid;
	private String recode;
	private Integer ptrneff;
	private Integer userT;
	private String prtflg;
	private String validflagforInsert;
	private String validflagforUpdate;
	private Date datime;
	private Integer datesub;
	private String crtuser;
	private boolean acmvRtrnFlag;
	private String Procflg;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrpfx() {
		return chdrpfx;
	}

	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getCownpfx() {
		return cownpfx;
	}

	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}

	public String getCowncoy() {
		return cowncoy;
	}

	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}

	public String getCownnum() {
		return cownnum;
	}

	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getOccdate() {
		return occdate;
	}

	public void setOccdate(String occdate) {
		this.occdate = occdate;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstcde() {
		return pstcde;
	}

	public void setPstcde(String pstcde) {
		this.pstcde = pstcde;
	}

	public String getZsufcdte() {
		return zsufcdte;
	}

	public void setZsufcdte(String zsufcdte) {
		this.zsufcdte = zsufcdte;
	}

	public String getCl1lsurname() {
		return cl1lsurname;
	}

	public void setCl1lsurname(String cl1lsurname) {
		this.cl1lsurname = cl1lsurname;
	}

	public String getCl1lgivname() {
		return cl1lgivname;
	}

	public void setCl1lgivname(String cl1lgivname) {
		this.cl1lgivname = cl1lgivname;
	}

	public String getCl2lsurname() {
		return cl2lsurname;
	}

	public void setCl2lsurname(String cl2lsurname) {
		this.cl2lsurname = cl2lsurname;
	}

	public String getCl2lgivname() {
		return cl2lgivname;
	}

	public void setCl2lgivname(String cl2lgivname) {
		this.cl2lgivname = cl2lgivname;
	}

	public String getCl2clntdob() {
		return cl2clntdob;
	}

	public void setCl2clntdob(String cl2clntdob) {
		this.cl2clntdob = cl2clntdob;
	}

	public String getLifcnum() {
		return lifcnum;
	}

	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public String getPolicyTranno() {
		return policyTranno;
	}

	public void setPolicyTranno(String policyTranno) {
		this.policyTranno = policyTranno;
	}
	public Integer getUserNum() {
		return userNum;
	}

	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}

	public String getBatcbrn() {
		return batcbrn;
	}

	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public Integer getBatcactyr() {
		return batcactyr;
	}

	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}

	public Integer getBatcactmn() {
		return batcactmn;
	}

	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}

	public String getBatcbatch() {
		return batcbatch;
	}

	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	public String getBatcpfx() {
		return batcpfx;
	}

	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}

	public String getBatccoy() {
		return batccoy;
	}

	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	public Integer getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Integer effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Integer getTrdt() {
		return trdt;
	}

	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}

	public Integer getTrtm() {
		return trtm;
	}

	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	public int getTranCount() {
		return tranCount;
	}

	public void setTranCount(int tranCount) {
		this.tranCount = tranCount;
	}

	public String getCurrto() {
		return currto;
	}

	public void setCurrto(String currto) {
		this.currto = currto;
	}

	public String getTranid() {
		return tranid;
	}

	public void setTranid(String tranid) {
		this.tranid = tranid;
	}

	public String getRecode() {
		return recode;
	}

	public void setRecode(String recode) {
		this.recode = recode;
	}

	public Integer getPtrneff() {
		return ptrneff;
	}

	public void setPtrneff(Integer ptrneff) {
		this.ptrneff = ptrneff;
	}

	public Integer getUserT() {
		return userT;
	}

	public void setUserT(Integer userT) {
		this.userT = userT;
	}

	public String getPrtflg() {
		return prtflg;
	}

	public void setPrtflg(String prtflg) {
		this.prtflg = prtflg;
	}

	public String getValidflagforInsert() {
		return validflagforInsert;
	}

	public void setValidflagforInsert(String validflagforInsert) {
		this.validflagforInsert = validflagforInsert;
	}

	public String getValidflagforUpdate() {
		return validflagforUpdate;
	}

	public void setValidflagforUpdate(String validflagforUpdate) {
		this.validflagforUpdate = validflagforUpdate;
	}

	public Date getDatime() {
		return datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}

	public Integer getDatesub() {
		return datesub;
	}

	public void setDatesub(Integer datesub) {
		this.datesub = datesub;
	}

	public String getCrtuser() {
		return crtuser;
	}

	public void setCrtuser(String crtuser) {
		this.crtuser = crtuser;
	}

	public boolean isAcmvRtrnFlag() {
		return acmvRtrnFlag;
	}

	public void setAcmvRtrnFlag(boolean acmvRtrnFlag) {
		this.acmvRtrnFlag = acmvRtrnFlag;
	}

	public String getProcflg() {
		return Procflg;
	}

	public void setProcflg(String procflg) {
		Procflg = procflg;
	}


}