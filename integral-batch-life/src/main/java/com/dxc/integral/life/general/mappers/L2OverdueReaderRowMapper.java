package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2OverdueReaderDTO;

/**
 * RowMapper for l2ownchgReaderDTO.
 * 
 * @author mmalik25
 */
public class L2OverdueReaderRowMapper implements RowMapper<L2OverdueReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2OverdueReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2OverdueReaderDTO l2OverdueReaderDTO = new L2OverdueReaderDTO();
		l2OverdueReaderDTO.setChdrcoy(rs.getString("chdrcoy"));
		l2OverdueReaderDTO.setChdrnum(rs.getString("chdrnum"));
		l2OverdueReaderDTO.setBtdate(rs.getInt("btdate"));
		l2OverdueReaderDTO.setPayrseqno(rs.getInt("payrseqno"));
		l2OverdueReaderDTO.setPtdate(rs.getInt("ptdate"));
		l2OverdueReaderDTO.setBillchnl(rs.getString("billchnl"));
		return l2OverdueReaderDTO;
	}

}