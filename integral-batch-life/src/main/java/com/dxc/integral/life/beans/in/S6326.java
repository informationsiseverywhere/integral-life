package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S6326 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String sumin;
	private String matage;
	private String mattrm;
	private String mattcessDisp;
	private String premCessAge;
	private String premCessTerm;
	private String premcessDisp;
	private String reserveUnitsInd;
	private String reserveUnitsDateDisp;
	private String liencd;
	private String singlePremium;
	private String mortcls;
	private String lumpContrib;
	private String optextind;
	private String numapp;
	private String ratypind;
	private String select;
	private String taxind;
	private String optind;
	private String optsmode;
	private String singpremtype;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getSumin() {
		return sumin;
	}
	public void setSumin(String sumin) {
		this.sumin = sumin;
	}
	public String getMatage() {
		return matage;
	}
	public void setMatage(String matage) {
		this.matage = matage;
	}
	public String getMattrm() {
		return mattrm;
	}
	public void setMattrm(String mattrm) {
		this.mattrm = mattrm;
	}
	public String getMattcessDisp() {
		return mattcessDisp;
	}
	public void setMattcessDisp(String mattcessDisp) {
		this.mattcessDisp = mattcessDisp;
	}
	public String getPremCessAge() {
		return premCessAge;
	}
	public void setPremCessAge(String premCessAge) {
		this.premCessAge = premCessAge;
	}
	public String getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(String premCessTerm) {
		this.premCessTerm = premCessTerm;
	}
	public String getPremcessDisp() {
		return premcessDisp;
	}
	public void setPremcessDisp(String premcessDisp) {
		this.premcessDisp = premcessDisp;
	}
	public String getReserveUnitsInd() {
		return reserveUnitsInd;
	}
	public void setReserveUnitsInd(String reserveUnitsInd) {
		this.reserveUnitsInd = reserveUnitsInd;
	}
	public String getReserveUnitsDateDisp() {
		return reserveUnitsDateDisp;
	}
	public void setReserveUnitsDateDisp(String reserveUnitsDateDisp) {
		this.reserveUnitsDateDisp = reserveUnitsDateDisp;
	}
	public String getLiencd() {
		return liencd;
	}
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	public String getSinglePremium() {
		return singlePremium;
	}
	public void setSinglePremium(String singlePremium) {
		this.singlePremium = singlePremium;
	}
	public String getMortcls() {
		return mortcls;
	}
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	public String getLumpContrib() {
		return lumpContrib;
	}
	public void setLumpContrib(String lumpContrib) {
		this.lumpContrib = lumpContrib;
	}
	public String getOptextind() {
		return optextind;
	}
	public void setOptextind(String optextind) {
		this.optextind = optextind;
	}
	public String getNumapp() {
		return numapp;
	}
	public void setNumapp(String numapp) {
		this.numapp = numapp;
	}
	public String getRatypind() {
		return ratypind;
	}
	public void setRatypind(String ratypind) {
		this.ratypind = ratypind;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getTaxind() {
		return taxind;
	}
	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}
	public String getOptind() {
		return optind;
	}
	public void setOptind(String optind) {
		this.optind = optind;
	}
	public String getOptsmode() {
		return optsmode;
	}
	public void setOptsmode(String optsmode) {
		this.optsmode = optsmode;
	}
	public String getSingpremtype() {
		return singpremtype;
	}
	public void setSingpremtype(String singpremtype) {
		this.singpremtype = singpremtype;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S6326 []";
	}
	
}
