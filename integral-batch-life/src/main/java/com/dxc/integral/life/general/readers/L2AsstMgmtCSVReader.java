package com.dxc.integral.life.general.readers;

import java.io.FileReader;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.life.beans.AssetMgmtOutput;
import com.dxc.integral.life.beans.L2asstmgmtReaderDTO;
import com.dxc.integral.life.utils.impl.AssetMgmtVPMSProcessHelper;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

public class L2AsstMgmtCSVReader implements ItemReader<AssetMgmtOutput> {
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AsstMgmtCSVReader.class);	

	@Value("#{jobParameters['csvPath']}")
	private String csvPath;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['trandate']}")
	private String trandate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['inputCallingSystem']}")
	private String inputCallingSystem;
	
	@Value("#{jobParameters['inputRegion']}")
	private String inputRegion;
	
	@Value("#{jobParameters['inputLocale']}")
	private String inputLocale;
	
	@Value("#{jobParameters['inptLanguage']}")
	private String inptLanguage;
	
	@Value("#{jobParameters['inputContractType']}")
	private String inputContractType;
	
	@Value("#{jobParameters['inputCurrency']}")
	private String inputCurrency;
	
	@Value("#{jobParameters['vpmsIpAddress']}")
	private String vpmsIpAddress;
	
	@Value("#{jobParameters['vpmsIpPort']}")
	private String vpmsIpPort;
	
	@Value("#{jobParameters['vpmsModelPath']}")
	private String vpmsModelPath;
	
	private int recordCount = 0;

	private List<AssetMgmtOutput> asstMgmtOutputList = null;

	private List<L2asstmgmtReaderDTO> asstmgmtReaderlist = null;	
	
	
	@SuppressWarnings("unchecked")
	public List<L2asstmgmtReaderDTO> readAllDataAtOnce() {
		String filepath = csvPath + batchName + "_" + trandate + ".csv";

		List<L2asstmgmtReaderDTO> objList = new ArrayList<>();
		L2asstmgmtReaderDTO objDTO = new L2asstmgmtReaderDTO();
		try {
			FileReader filereader = new FileReader(FileSystems.getDefault().getPath(filepath).toFile()); //IJTI-1324
			// create csvReader object and skip first Line
			CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build();
			List<String[]> allData = csvReader.readAll();
			int index = 0;
			for (String[] row : allData) {
				objDTO = new L2asstmgmtReaderDTO();
				objDTO.setVrtfnd(row[0]);
				objDTO.setChdrnum (row[1]);
	        	objDTO.setCrtable(row[2]);
				objDTO.setNoFundUnit(row[3]);
	        	objDTO.setFirstName(row[4]);
				objDTO.setAgentNum(row[5]);
	        	objDTO.setSplitc(row[6]);
	        	objList.add(index, objDTO);
				index++;
			} 
		} catch (Exception e) {
			LOGGER.error("Exception thrown while parsing CSV file : ",e);//IJTI-1498
		}
		LOGGER.info("Inside L2AsstMgmtCSVReader class : Number of records to read : {}",objList.size());//IJTI-1498
		return objList;

	}

	@Override
	public AssetMgmtOutput read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {		
		if (asstMgmtOutputList == null) {
			asstmgmtReaderlist = readAllDataAtOnce();
			AssetMgmtVPMSProcessHelper vpmsHelper = new AssetMgmtVPMSProcessHelper(inputCallingSystem, inputRegion, inputLocale, inptLanguage, inputContractType, inputCurrency, vpmsIpAddress, vpmsIpPort, vpmsModelPath);			
			asstMgmtOutputList = vpmsHelper.sendAsstMgmtDataToVpms(asstmgmtReaderlist);			
		}
		if (asstMgmtOutputList != null && !asstMgmtOutputList.isEmpty()) {
			LOGGER.info("Inside L2AsstMgmtCSVReader class : Number of records returned from VPMS : {}",
					asstMgmtOutputList.size());//IJTI-1498
			if (recordCount == asstMgmtOutputList.size()) {
				return null;
			} else {
				return asstMgmtOutputList.get(recordCount++);
			}
		} else {
			return null;
		}
	}

}
