package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2ownchgReaderDTO;

/**
 * RowMapper for l2ownchgReaderDTO.
 * 
 * @author mmalik25
 */
public class L2ownchgxReaderRowMapper implements RowMapper<L2ownchgReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2ownchgReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2ownchgReaderDTO l2ownchgReader = new L2ownchgReaderDTO();
		l2ownchgReader.setUniqueNumber(rs.getLong("uniqueNumber"));
		l2ownchgReader.setChdrcoy(rs.getString("chdrcoy"));
        l2ownchgReader.setChdrnum(rs.getString("chdrnum"));
		l2ownchgReader.setChdrpfx(rs.getString("chdrpfx"));
		l2ownchgReader.setCownpfx(rs.getString("cownpfx"));
		l2ownchgReader.setCowncoy(rs.getString("cowncoy"));
		l2ownchgReader.setCownnum(rs.getString("cownnum"));
		l2ownchgReader.setStatcode(rs.getString("statcode"));
		l2ownchgReader.setCnttype(rs.getString("cnttype"));
		l2ownchgReader.setTranno(rs.getInt("tranno"));
		l2ownchgReader.setOccdate(rs.getString("occdate"));
		l2ownchgReader.setPstcde(rs.getString("pstcde"));
		l2ownchgReader.setChdrnum(rs.getString("chdrnum"));
		l2ownchgReader.setZsufcdte(rs.getString("zsufcdte"));
		l2ownchgReader.setCl1lsurname(rs.getString("lsurname"));
		l2ownchgReader.setCl1lgivname(rs.getString("lgivname"));
		l2ownchgReader.setLifcnum(rs.getString("lifcnum"));
		l2ownchgReader.setCl2lgivname(rs.getString("cl2Lgivname"));
		l2ownchgReader.setCl2lsurname(rs.getString("cl2Lsurname"));
		l2ownchgReader.setCl2clntdob(rs.getString("cl2cltdob"));
		
		
		return l2ownchgReader;
	}

}