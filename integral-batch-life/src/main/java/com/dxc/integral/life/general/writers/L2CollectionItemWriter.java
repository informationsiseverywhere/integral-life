package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2CollectionReaderDTO;
import com.dxc.integral.life.dao.AcagpfDAO;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.HdivpfDAO;
import com.dxc.integral.life.dao.LinspfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.TaxdpfDAO;
import com.dxc.integral.life.dao.TrwppfDAO;
import com.dxc.integral.life.dao.ZctnpfDAO;
import com.dxc.integral.life.dao.ZptnpfDAO;
import com.dxc.integral.life.utils.L2CollectionItemWriterUtil;
/**
 * 
 * @author xma3
 *
 */
@Async
public class L2CollectionItemWriter implements ItemWriter<L2CollectionReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CollectionItemWriter.class);
	@Autowired
	private Sftlock sftlock;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	@Autowired
	private HdivpfDAO hdivpfDAO;
	@Autowired
	private ZptnpfDAO zptnpfDAO;
	@Autowired
	private AcagpfDAO acagpfDAO;
	@Autowired
	private ZctnpfDAO zctnpfDAO;
	@Autowired
	private TrwppfDAO trwppfDAO;
	@Autowired
	private TaxdpfDAO taxdpfDAO;
	@Autowired
	private AgcmpfDAO agcmpfDAO;
	@Autowired
	private LinspfDAO linspfDAO;
	@Autowired
	private L2CollectionItemWriterUtil l2CollectionItemWriterUtil;
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	protected String businessDate;
	private ItemWriter<L2CollectionReaderDTO> chdrpfUpdateWriter;
    private ItemWriter<L2CollectionReaderDTO> payrpfUpdateWriter;
    
    
    
    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends L2CollectionReaderDTO> readerDTOList) throws Exception {
		
		LOGGER.info("L2polrnwlCollectionWrite starts.");
		l2CollectionItemWriterUtil.process((List<L2CollectionReaderDTO>) readerDTOList, Integer.parseInt(transactionDate));
        
		List<L2CollectionReaderDTO> readerDTOModifiedList;
		readerDTOModifiedList = new ArrayList<L2CollectionReaderDTO>();
		for (int x = 0; x < readerDTOList.size(); x++) {
			L2CollectionReaderDTO readerDTOModified  = readerDTOList.get(x);
			readerDTOModifiedList.add(readerDTOModified);
				
		}
		if (!readerDTOModifiedList.isEmpty()) {
			processOtherData(readerDTOModifiedList);

			chdrpfUpdateWriter.write(readerDTOModifiedList);
			payrpfUpdateWriter.write(readerDTOModifiedList);
		

		}
		
		if(!readerDTOList.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOList.size(); i++) {
				entityList.add(readerDTOList.get(i).getChdrnum());
			}			
			
			sftlock.unlockByList("CH", readerDTOList.get(0).getChdrcoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2CollectionItemWriter ends.");
	}


	private void processOtherData(List<? extends L2CollectionReaderDTO> readerDTOList) throws ParseException {

		if (!readerDTOList.isEmpty()) {
			for(L2CollectionReaderDTO dto : readerDTOList ){
				if(null != dto.getTaxdUpdateList() && dto.getTaxdUpdateList().size() > 0){
					taxdpfDAO.updateTaxdpfRecords(dto.getTaxdUpdateList());
				}
				if(null != dto.getPtrnpfList() && dto.getPtrnpfList().size() > 0){
					ptrnpfDAO.insertPtrnpfRecords(dto.getPtrnpfList());
				}
				if(null != dto.getLinsUpdateList() && dto.getLinsUpdateList().size() > 0){
					linspfDAO.updateLinspfRecords(dto.getLinsUpdateList());
				}
				if(null != dto.getHdivInsertList()){
					hdivpfDAO.insertHdivpfRecords(dto.getHdivInsertList());
				}
				if(null != dto.getZptnInsertList() && dto.getZptnInsertList().size() > 0){
					zptnpfDAO.insertZptnpfRecords(dto.getZptnInsertList());
				}
				if(null != dto.getZctnInsertList() && dto.getZctnInsertList().size() > 0 ){
					zctnpfDAO.insertZctnpfRecords(dto.getZctnInsertList());
				}
				if(null != dto.getAcagInsertList() && dto.getAcagInsertList().size() > 0 ){
					acagpfDAO.insertAcagpfRecords(dto.getAcagInsertList());
				}
				if(null != dto.getTrwppfInsertList() ){
					trwppfDAO.insertTrwppfRecords(dto.getTrwppfInsertList());
				}
				if(null != dto.getAgcmpfUpdateList() && dto.getAgcmpfUpdateList().size() > 0 ){
					agcmpfDAO.updateAcagpfRecords(dto.getAgcmpfUpdateList());
				}
				
			}
		}
	}

	public void setChdrpfUpdateWriter(ItemWriter<L2CollectionReaderDTO> chdrpfUpdateWriter) {
		this.chdrpfUpdateWriter = chdrpfUpdateWriter;
	}

	public void setPayrpfUpdateWriter(ItemWriter<L2CollectionReaderDTO> payrpfUpdateWriter) {
		this.payrpfUpdateWriter = payrpfUpdateWriter;
	}


}
