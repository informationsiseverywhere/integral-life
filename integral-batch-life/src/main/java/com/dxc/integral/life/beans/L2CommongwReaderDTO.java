package com.dxc.integral.life.beans;

public class L2CommongwReaderDTO {
	private String datatype;
	private String insucate;
	private String bokcls;
	private String secunum;
	private String brnchnum;
	private String toi;
	private String mop;
	private String colcate;
	private String instal;
	private String kyocde;
	private String kyoacs;
	private String pcde;
	private String pacs;
	private String grpcde;
	private String affilcde;
	private String empcde;
	private String cgrpc;
	private String totprem;
	private String bckp;
	private String dpcde;
	private String brnchnum2;
	private String incpn;
	private String matrty;
	private String tncdte;
	private String trnendte;
	private String dteofapp;
	private String slfcrt;
	private String lpcc;
	private String dcde;
	private String oprtcde;
	private String postcde;
	private String cntadd;
	private String pnum;
	private String subnme;
	private String spfip;
	private String spltyp;
	private String pymntdte;
	private String fpdfgh;
	private String insyr;
	private String frstme;
	private String pprem;
	private String ppendatme;
	private String nwccls;
	private String ccps;
	private String ipactc;
	private String amor;
	private String grpcara;
	private String telphno;
	private String nwcmpncde;
	private String rftrns;
	private String margin;
	private String inacviwe;
	private String prosqnc;
	private String ynmrcd;
	private String inscmpnycde;
	private String insprocde;
	private String cmpnycde;
	private String prsnmekana;
	private String ipecflg;
	private String ipsftcde01;
	private String ipnmknji;
	private String ipsftcde02;
	private String ipdob;
	private String ipage;
	private String ipgndr;
	private String iprln;
	private String ipostcde;
	private String iaeflg;
	private String iashtcde01;
	private String iadrsknji;
	private String iashtcde02;
	private String pymntrt;
	private String lsprem;
	private String modundrprd;
	private String indcate;
	private String indcprd;
	private String dvndcls;
	private String pdexntdvw;
	private String attchngcls;
	private String appcate;
	private String ippyear;
	private String ppsdte;
	private String pasbdy;
	private String addscnt;
	private String norids;
	private String grpcde02;
	private String locde;
	private String exmcate;
	private String taxelg;
	private String lvnnds;
	private String bnsprem;
	private String bcagyr;
	private String opcde;
	private String mcctypcde;
	private String mccecflg;
	private String mccscde01;
	private String mccnmc;
	private String mccscde02;
	private String mccppa;
	private String mccitc;
	private String mccip;
	private String mccppc;
	private String mccpp;
	private String sa01typcde;
	private String sa01ecflg;
	private String sa01scde01;
	private String sa01nme;
	private String sa01scde02;
	private String sa01sins;
	private String sa01itc;
	private String sa01insprd;
	private String sa01ppc;
	private String sa01pp;
	private String sa02typcde;
	private String sa02ecflg;
	private String sa02scde01;
	private String sa02nme;
	private String sa02scde02;
	private String sa02sins;
	private String sa02itc;
	private String sa02insprd;
	private String sa02ppc;
	private String sa02pp;
	private String sa03typcde;
	private String sa03ecflg;
	private String sa03scde01;
	private String sa03nme;
	private String sa03scde02;
	private String sa03sins;
	private String sa03itc;
	private String sa03insprd;
	private String sa03ppc;
	private String sa03pp;
	private String sa04typcde;
	private String sa04ecflg;
	private String sa04scde01;
	private String sa04nme;
	private String sa04scde02;
	private String sa04sins;
	private String sa04itc;
	private String sa04insprd;
	private String sa04ppc;
	private String sa04pp;
	private String sa05typcde;
	private String saecflg5;
	private String sa05scde01;
	private String sa05nme;
	private String sa05scde02;
	private String sa05sins;
	private String sa05itc;
	private String sa05insprd;
	private String sa05ppc;
	private String sa05pp;
	private String sa06typcde;
	private String sa06ecflg;
	private String sa06scde01;
	private String sa06nme;
	private String sa06scde02;
	private String sa06sins;
	private String sa06itc;
	private String sa06insprd;
	private String sa06ppc;
	private String sa06pp;
	private String ustatcate;
	private String cmec;
	private String cifccc;
	private String asmcde;
	private String lircde;
	private String acctyp;
	private String accno;
	private String lcode;
	private String dthbnft;
	private String racate;
	private String dtypcde;
	private String caic;
	private String cnticnrct;
	private String tncp;
	private String wrkplc;
	private String bphno;
	private String cmgntcde;
	private String lncde;
	private String osnocc;
	private String osnmn;
	private String osnbn;
	private String grpcrtlno;
	private String scno;
	private String grpinsnme;
	private String crsnme;
	private String type;
	private String nounits;
	private String agregno;
	private String agcde01;
	private String agcde02;
	private String caflgscde01;
	private String canknjiecflg;
	private String caflgadrs;
	private String cnkjicflgscd;
	private String caflgscde02;
	private String cnflgscde;
	private String spnmknji;
	private String spnmscde;
	private String dtechgprem;
	private String modinstprem;
	private String premlncrt;
	private String dobph;
	private String cgender;
	private String bphno02;
	private String bckup01;
	private String cmpnyter;
	private String pstrtflg;
	private String antypymnt;
	private String anpenpymnt;
	private String pensnfund;
	private String pensncrtno;
	private String payecate;
	private String appno;
	private String bckup02;
	private String corpterty;
	private String agncnum;
	private String exprtflg;
	
	// Get Methods
	public String getDatatype(){
		return this.datatype;
	}
	public String getInsucate(){
		return this.insucate;
	}
	public String getBokcls(){
		return this.bokcls;
	}
	public String getSecunum(){
		return this.secunum;
	}
	public String getBrnchnum(){
		return this.brnchnum;
	}
	public String getToi(){
		return this.toi;
	}
	public String getMop(){
		return this.mop;
	}
	public String getColcate(){
		return this.colcate;
	}
	public String getInstal(){
		return this.instal;
	}
	public String getKyocde(){
		return this.kyocde;
	}
	public String getKyoacs(){
		return this.kyoacs;
	}
	public String getPcde(){
		return this.pcde;
	}
	public String getPacs(){
		return this.pacs;
	}
	public String getGrpcde(){
		return this.grpcde;
	}
	public String getAffilcde(){
		return this.affilcde;
	}
	public String getEmpcde(){
		return this.empcde;
	}
	public String getCgrpc(){
		return this.cgrpc;
	}
	public String getTotprem(){
		return this.totprem;
	}
	public String getBckp(){
		return this.bckp;
	}
	public String getDpcde(){
		return this.dpcde;
	}
	public String getBrnchnum2(){
		return this.brnchnum2;
	}
	public String getIncpn(){
		return this.incpn;
	}
	public String getMatrty(){
		return this.matrty;
	}
	public String getTncdte(){
		return this.tncdte;
	}
	public String getTrnendte(){
		return this.trnendte;
	}
	public String getDteofapp(){
		return this.dteofapp;
	}
	public String getSlfcrt(){
		return this.slfcrt;
	}
	public String getLpcc(){
		return this.lpcc;
	}
	public String getDcde(){
		return this.dcde;
	}
	public String getOprtcde(){
		return this.oprtcde;
	}
	public String getPostcde(){
		return this.postcde;
	}
	public String getCntadd(){
		return this.cntadd;
	}
	public String getPnum(){
		return this.pnum;
	}
	public String getSubnme(){
		return this.subnme;
	}
	public String getSpfip(){
		return this.spfip;
	}
	public String getSpltyp(){
		return this.spltyp;
	}
	public String getPymntdte(){
		return this.pymntdte;
	}
	public String getFpdfgh(){
		return this.fpdfgh;
	}
	public String getInsyr(){
		return this.insyr;
	}
	public String getFrstme(){
		return this.frstme;
	}
	public String getPprem(){
		return this.pprem;
	}
	public String getPpendatme(){
		return this.ppendatme;
	}
	public String getNwccls(){
		return this.nwccls;
	}
	public String getCcps(){
		return this.ccps;
	}
	public String getIpactc(){
		return this.ipactc;
	}
	public String getAmor(){
		return this.amor;
	}
	public String getGrpcara(){
		return this.grpcara;
	}
	public String getTelphno(){
		return this.telphno;
	}
	public String getNwcmpncde(){
		return this.nwcmpncde;
	}
	public String getRftrns(){
		return this.rftrns;
	}
	public String getMargin(){
		return this.margin;
	}
	public String getInacviwe(){
		return this.inacviwe;
	}
	public String getProsqnc(){
		return this.prosqnc;
	}
	public String getYnmrcd(){
		return this.ynmrcd;
	}
	public String getInscmpnycde(){
		return this.inscmpnycde;
	}
	public String getInsprocde(){
		return this.insprocde;
	}
	public String getCmpnycde(){
		return this.cmpnycde;
	}
	public String getPrsnmekana(){
		return this.prsnmekana;
	}
	public String getIpecflg(){
		return this.ipecflg;
	}
	public String getIpsftcde01(){
		return this.ipsftcde01;
	}
	public String getIpnmknji(){
		return this.ipnmknji;
	}
	public String getIpsftcde02(){
		return this.ipsftcde02;
	}
	public String getIpdob(){
		return this.ipdob;
	}
	public String getIpage(){
		return this.ipage;
	}
	public String getIpgndr(){
		return this.ipgndr;
	}
	public String getIprln(){
		return this.iprln;
	}
	public String getIpostcde(){
		return this.ipostcde;
	}
	public String getIaeflg(){
		return this.iaeflg;
	}
	public String getIashtcde01(){
		return this.iashtcde01;
	}
	public String getIadrsknji(){
		return this.iadrsknji;
	}
	public String getIashtcde02(){
		return this.iashtcde02;
	}
	public String getPymntrt(){
		return this.pymntrt;
	}
	public String getLsprem(){
		return this.lsprem;
	}
	public String getModundrprd(){
		return this.modundrprd;
	}
	public String getIndcate(){
		return this.indcate;
	}
	public String getIndcprd(){
		return this.indcprd;
	}
	public String getDvndcls(){
		return this.dvndcls;
	}
	public String getPdexntdvw(){
		return this.pdexntdvw;
	}
	public String getAttchngcls(){
		return this.attchngcls;
	}
	public String getAppcate(){
		return this.appcate;
	}
	public String getIppyear(){
		return this.ippyear;
	}
	public String getPpsdte(){
		return this.ppsdte;
	}
	public String getPasbdy(){
		return this.pasbdy;
	}
	public String getAddscnt(){
		return this.addscnt;
	}
	public String getNorids(){
		return this.norids;
	}
	public String getGrpcde02(){
		return this.grpcde02;
	}
	public String getLocde(){
		return this.locde;
	}
	public String getExmcate(){
		return this.exmcate;
	}
	public String getTaxelg(){
		return this.taxelg;
	}
	public String getLvnnds(){
		return this.lvnnds;
	}
	public String getBnsprem(){
		return this.bnsprem;
	}
	public String getBcagyr(){
		return this.bcagyr;
	}
	public String getOpcde(){
		return this.opcde;
	}
	public String getMcctypcde(){
		return this.mcctypcde;
	}
	public String getMccecflg(){
		return this.mccecflg;
	}
	public String getMccscde01(){
		return this.mccscde01;
	}
	public String getMccnmc(){
		return this.mccnmc;
	}
	public String getMccscde02(){
		return this.mccscde02;
	}
	public String getMccppa(){
		return this.mccppa;
	}
	public String getMccitc(){
		return this.mccitc;
	}
	public String getMccip(){
		return this.mccip;
	}
	public String getMccppc(){
		return this.mccppc;
	}
	public String getMccpp(){
		return this.mccpp;
	}
	public String getSa01typcde(){
		return this.sa01typcde;
	}
	public String getSa01ecflg(){
		return this.sa01ecflg;
	}
	public String getSa01scde01(){
		return this.sa01scde01;
	}
	public String getSa01nme(){
		return this.sa01nme;
	}
	public String getSa01scde02(){
		return this.sa01scde02;
	}
	public String getSa01sins(){
		return this.sa01sins;
	}
	public String getSa01itc(){
		return this.sa01itc;
	}
	public String getSa01insprd(){
		return this.sa01insprd;
	}
	public String getSa01ppc(){
		return this.sa01ppc;
	}
	public String getSa01pp(){
		return this.sa01pp;
	}
	public String getSa02typcde(){
		return this.sa02typcde;
	}
	public String getSa02ecflg(){
		return this.sa02ecflg;
	}
	public String getSa02scde01(){
		return this.sa02scde01;
	}
	public String getSa02nme(){
		return this.sa02nme;
	}
	public String getSa02scde02(){
		return this.sa02scde02;
	}
	public String getSa02sins(){
		return this.sa02sins;
	}
	public String getSa02itc(){
		return this.sa02itc;
	}
	public String getSa02insprd(){
		return this.sa02insprd;
	}
	public String getSa02ppc(){
		return this.sa02ppc;
	}
	public String getSa02pp(){
		return this.sa02pp;
	}
	public String getSa03typcde(){
		return this.sa03typcde;
	}
	public String getSa03ecflg(){
		return this.sa03ecflg;
	}
	public String getSa03scde01(){
		return this.sa03scde01;
	}
	public String getSa03nme(){
		return this.sa03nme;
	}
	public String getSa03scde02(){
		return this.sa03scde02;
	}
	public String getSa03sins(){
		return this.sa03sins;
	}
	public String getSa03itc(){
		return this.sa03itc;
	}
	public String getSa03insprd(){
		return this.sa03insprd;
	}
	public String getSa03ppc(){
		return this.sa03ppc;
	}
	public String getSa03pp(){
		return this.sa03pp;
	}
	public String getSa04typcde(){
		return this.sa04typcde;
	}
	public String getSa04ecflg(){
		return this.sa04ecflg;
	}
	public String getSa04scde01(){
		return this.sa04scde01;
	}
	public String getSa04nme(){
		return this.sa04nme;
	}
	public String getSa04scde02(){
		return this.sa04scde02;
	}
	public String getSa04sins(){
		return this.sa04sins;
	}
	public String getSa04itc(){
		return this.sa04itc;
	}
	public String getSa04insprd(){
		return this.sa04insprd;
	}
	public String getSa04ppc(){
		return this.sa04ppc;
	}
	public String getSa04pp(){
		return this.sa04pp;
	}
	public String getSa05typcde(){
		return this.sa05typcde;
	}
	public String getSaecflg5(){
		return this.saecflg5;
	}
	public String getSa05scde01(){
		return this.sa05scde01;
	}
	public String getSa05nme(){
		return this.sa05nme;
	}
	public String getSa05scde02(){
		return this.sa05scde02;
	}
	public String getSa05sins(){
		return this.sa05sins;
	}
	public String getSa05itc(){
		return this.sa05itc;
	}
	public String getSa05insprd(){
		return this.sa05insprd;
	}
	public String getSa05ppc(){
		return this.sa05ppc;
	}
	public String getSa05pp(){
		return this.sa05pp;
	}
	public String getSa06typcde(){
		return this.sa06typcde;
	}
	public String getSa06ecflg(){
		return this.sa06ecflg;
	}
	public String getSa06scde01(){
		return this.sa06scde01;
	}
	public String getSa06nme(){
		return this.sa06nme;
	}
	public String getSa06scde02(){
		return this.sa06scde02;
	}
	public String getSa06sins(){
		return this.sa06sins;
	}
	public String getSa06itc(){
		return this.sa06itc;
	}
	public String getSa06insprd(){
		return this.sa06insprd;
	}
	public String getSa06ppc(){
		return this.sa06ppc;
	}
	public String getSa06pp(){
		return this.sa06pp;
	}
	public String getUstatcate(){
		return this.ustatcate;
	}
	public String getCmec(){
		return this.cmec;
	}
	public String getCifccc(){
		return this.cifccc;
	}
	public String getAsmcde(){
		return this.asmcde;
	}
	public String getLircde(){
		return this.lircde;
	}
	public String getAcctyp(){
		return this.acctyp;
	}
	public String getAccno(){
		return this.accno;
	}
	public String getLcode(){
		return this.lcode;
	}
	public String getDthbnft(){
		return this.dthbnft;
	}
	public String getRacate(){
		return this.racate;
	}
	public String getDtypcde(){
		return this.dtypcde;
	}
	public String getCaic(){
		return this.caic;
	}
	public String getCnticnrct(){
		return this.cnticnrct;
	}
	public String getTncp(){
		return this.tncp;
	}
	public String getWrkplc(){
		return this.wrkplc;
	}
	public String getBphno(){
		return this.bphno;
	}
	public String getCmgntcde(){
		return this.cmgntcde;
	}
	public String getLncde(){
		return this.lncde;
	}
	public String getOsnocc(){
		return this.osnocc;
	}
	public String getOsnmn(){
		return this.osnmn;
	}
	public String getOsnbn(){
		return this.osnbn;
	}
	public String getGrpcrtlno(){
		return this.grpcrtlno;
	}
	public String getScno(){
		return this.scno;
	}
	public String getGrpinsnme(){
		return this.grpinsnme;
	}
	public String getCrsnme(){
		return this.crsnme;
	}
	public String getType(){
		return this.type;
	}
	public String getNounits(){
		return this.nounits;
	}
	public String getAgregno(){
		return this.agregno;
	}
	public String getAgcde01(){
		return this.agcde01;
	}
	public String getAgcde02(){
		return this.agcde02;
	}
	public String getCaflgscde01(){
		return this.caflgscde01;
	}
	public String getCanknjiecflg(){
		return this.canknjiecflg;
	}
	public String getCaflgadrs(){
		return this.caflgadrs;
	}
	public String getCnkjicflgscd(){
		return this.cnkjicflgscd;
	}
	public String getCaflgscde02(){
		return this.caflgscde02;
	}
	public String getCnflgscde(){
		return this.cnflgscde;
	}
	public String getSpnmknji(){
		return this.spnmknji;
	}
	public String getSpnmscde(){
		return this.spnmscde;
	}
	public String getDtechgprem(){
		return this.dtechgprem;
	}
	public String getModinstprem(){
		return this.modinstprem;
	}
	public String getPremlncrt(){
		return this.premlncrt;
	}
	public String getDobph(){
		return this.dobph;
	}
	public String getCgender(){
		return this.cgender;
	}
	public String getBphno02(){
		return this.bphno02;
	}
	public String getBckup01(){
		return this.bckup01;
	}
	public String getCmpnyter(){
		return this.cmpnyter;
	}
	public String getPstrtflg(){
		return this.pstrtflg;
	}
	public String getAntypymnt(){
		return this.antypymnt;
	}
	public String getAnpenpymnt(){
		return this.anpenpymnt;
	}
	public String getPensnfund(){
		return this.pensnfund;
	}
	public String getPensncrtno(){
		return this.pensncrtno;
	}
	public String getPayecate(){
		return this.payecate;
	}
	public String getAppno(){
		return this.appno;
	}
	public String getBckup02(){
		return this.bckup02;
	}
	public String getCorpterty(){
		return this.corpterty;
	}
	public String getAgncnum(){
		return this.agncnum;
	}
	public String getExprtflg(){
		return this.exprtflg;
	}

	// Set Methods
	public void setDatatype( String datatype ){
		 this.datatype = datatype;
	}
	public void setInsucate( String insucate ){
		 this.insucate = insucate;
	}
	public void setBokcls( String bokcls ){
		 this.bokcls = bokcls;
	}
	public void setSecunum( String secunum ){
		 this.secunum = secunum;
	}
	public void setBrnchnum( String brnchnum ){
		 this.brnchnum = brnchnum;
	}
	public void setToi( String toi ){
		 this.toi = toi;
	}
	public void setMop( String mop ){
		 this.mop = mop;
	}
	public void setColcate( String colcate ){
		 this.colcate = colcate;
	}
	public void setInstal( String instal ){
		 this.instal = instal;
	}
	public void setKyocde( String kyocde ){
		 this.kyocde = kyocde;
	}
	public void setKyoacs( String kyoacs ){
		 this.kyoacs = kyoacs;
	}
	public void setPcde( String pcde ){
		 this.pcde = pcde;
	}
	public void setPacs( String pacs ){
		 this.pacs = pacs;
	}
	public void setGrpcde( String grpcde ){
		 this.grpcde = grpcde;
	}
	public void setAffilcde( String affilcde ){
		 this.affilcde = affilcde;
	}
	public void setEmpcde( String empcde ){
		 this.empcde = empcde;
	}
	public void setCgrpc( String cgrpc ){
		 this.cgrpc = cgrpc;
	}
	public void setTotprem( String totprem ){
		 this.totprem = totprem;
	}
	public void setBckp( String bckp ){
		 this.bckp = bckp;
	}
	public void setDpcde( String dpcde ){
		 this.dpcde = dpcde;
	}
	public void setBrnchnum2( String brnchnum2 ){
		 this.brnchnum2 = brnchnum2;
	}
	public void setIncpn( String incpn ){
		 this.incpn = incpn;
	}
	public void setMatrty( String matrty ){
		 this.matrty = matrty;
	}
	public void setTncdte( String tncdte ){
		 this.tncdte = tncdte;
	}
	public void setTrnendte( String trnendte ){
		 this.trnendte = trnendte;
	}
	public void setDteofapp( String dteofapp ){
		 this.dteofapp = dteofapp;
	}
	public void setSlfcrt( String slfcrt ){
		 this.slfcrt = slfcrt;
	}
	public void setLpcc( String lpcc ){
		 this.lpcc = lpcc;
	}
	public void setDcde( String dcde ){
		 this.dcde = dcde;
	}
	public void setOprtcde( String oprtcde ){
		 this.oprtcde = oprtcde;
	}
	public void setPostcde( String postcde ){
		 this.postcde = postcde;
	}
	public void setCntadd( String cntadd ){
		 this.cntadd = cntadd;
	}
	public void setPnum( String pnum ){
		 this.pnum = pnum;
	}
	public void setSubnme( String subnme ){
		 this.subnme = subnme;
	}
	public void setSpfip( String spfip ){
		 this.spfip = spfip;
	}
	public void setSpltyp( String spltyp ){
		 this.spltyp = spltyp;
	}
	public void setPymntdte( String pymntdte ){
		 this.pymntdte = pymntdte;
	}
	public void setFpdfgh( String fpdfgh ){
		 this.fpdfgh = fpdfgh;
	}
	public void setInsyr( String insyr ){
		 this.insyr = insyr;
	}
	public void setFrstme( String frstme ){
		 this.frstme = frstme;
	}
	public void setPprem( String pprem ){
		 this.pprem = pprem;
	}
	public void setPpendatme( String ppendatme ){
		 this.ppendatme = ppendatme;
	}
	public void setNwccls( String nwccls ){
		 this.nwccls = nwccls;
	}
	public void setCcps( String ccps ){
		 this.ccps = ccps;
	}
	public void setIpactc( String ipactc ){
		 this.ipactc = ipactc;
	}
	public void setAmor( String amor ){
		 this.amor = amor;
	}
	public void setGrpcara( String grpcara ){
		 this.grpcara = grpcara;
	}
	public void setTelphno( String telphno ){
		 this.telphno = telphno;
	}
	public void setNwcmpncde( String nwcmpncde ){
		 this.nwcmpncde = nwcmpncde;
	}
	public void setRftrns( String rftrns ){
		 this.rftrns = rftrns;
	}
	public void setMargin( String margin ){
		 this.margin = margin;
	}
	public void setInacviwe( String inacviwe ){
		 this.inacviwe = inacviwe;
	}
	public void setProsqnc( String prosqnc ){
		 this.prosqnc = prosqnc;
	}
	public void setYnmrcd( String ynmrcd ){
		 this.ynmrcd = ynmrcd;
	}
	public void setInscmpnycde( String inscmpnycde ){
		 this.inscmpnycde = inscmpnycde;
	}
	public void setInsprocde( String insprocde ){
		 this.insprocde = insprocde;
	}
	public void setCmpnycde( String cmpnycde ){
		 this.cmpnycde = cmpnycde;
	}
	public void setPrsnmekana( String prsnmekana ){
		 this.prsnmekana = prsnmekana;
	}
	public void setIpecflg( String ipecflg ){
		 this.ipecflg = ipecflg;
	}
	public void setIpsftcde01( String ipsftcde01 ){
		 this.ipsftcde01 = ipsftcde01;
	}
	public void setIpnmknji( String ipnmknji ){
		 this.ipnmknji = ipnmknji;
	}
	public void setIpsftcde02( String ipsftcde02 ){
		 this.ipsftcde02 = ipsftcde02;
	}
	public void setIpdob( String ipdob ){
		 this.ipdob = ipdob;
	}
	public void setIpage( String ipage ){
		 this.ipage = ipage;
	}
	public void setIpgndr( String ipgndr ){
		 this.ipgndr = ipgndr;
	}
	public void setIprln( String iprln ){
		 this.iprln = iprln;
	}
	public void setIpostcde( String ipostcde ){
		 this.ipostcde = ipostcde;
	}
	public void setIaeflg( String iaeflg ){
		 this.iaeflg = iaeflg;
	}
	public void setIashtcde01( String iashtcde01 ){
		 this.iashtcde01 = iashtcde01;
	}
	public void setIadrsknji( String iadrsknji ){
		 this.iadrsknji = iadrsknji;
	}
	public void setIashtcde02( String iashtcde02 ){
		 this.iashtcde02 = iashtcde02;
	}
	public void setPymntrt( String pymntrt ){
		 this.pymntrt = pymntrt;
	}
	public void setLsprem( String lsprem ){
		 this.lsprem = lsprem;
	}
	public void setModundrprd( String modundrprd ){
		 this.modundrprd = modundrprd;
	}
	public void setIndcate( String indcate ){
		 this.indcate = indcate;
	}
	public void setIndcprd( String indcprd ){
		 this.indcprd = indcprd;
	}
	public void setDvndcls( String dvndcls ){
		 this.dvndcls = dvndcls;
	}
	public void setPdexntdvw( String pdexntdvw ){
		 this.pdexntdvw = pdexntdvw;
	}
	public void setAttchngcls( String attchngcls ){
		 this.attchngcls = attchngcls;
	}
	public void setAppcate( String appcate ){
		 this.appcate = appcate;
	}
	public void setIppyear( String ippyear ){
		 this.ippyear = ippyear;
	}
	public void setPpsdte( String ppsdte ){
		 this.ppsdte = ppsdte;
	}
	public void setPasbdy( String pasbdy ){
		 this.pasbdy = pasbdy;
	}
	public void setAddscnt( String addscnt ){
		 this.addscnt = addscnt;
	}
	public void setNorids( String norids ){
		 this.norids = norids;
	}
	public void setGrpcde02( String grpcde02 ){
		 this.grpcde02 = grpcde02;
	}
	public void setLocde( String locde ){
		 this.locde = locde;
	}
	public void setExmcate( String exmcate ){
		 this.exmcate = exmcate;
	}
	public void setTaxelg( String taxelg ){
		 this.taxelg = taxelg;
	}
	public void setLvnnds( String lvnnds ){
		 this.lvnnds = lvnnds;
	}
	public void setBnsprem( String bnsprem ){
		 this.bnsprem = bnsprem;
	}
	public void setBcagyr( String bcagyr ){
		 this.bcagyr = bcagyr;
	}
	public void setOpcde( String opcde ){
		 this.opcde = opcde;
	}
	public void setMcctypcde( String mcctypcde ){
		 this.mcctypcde = mcctypcde;
	}
	public void setMccecflg( String mccecflg ){
		 this.mccecflg = mccecflg;
	}
	public void setMccscde01( String mccscde01 ){
		 this.mccscde01 = mccscde01;
	}
	public void setMccnmc( String mccnmc ){
		 this.mccnmc = mccnmc;
	}
	public void setMccscde02( String mccscde02 ){
		 this.mccscde02 = mccscde02;
	}
	public void setMccppa( String mccppa ){
		 this.mccppa = mccppa;
	}
	public void setMccitc( String mccitc ){
		 this.mccitc = mccitc;
	}
	public void setMccip( String mccip ){
		 this.mccip = mccip;
	}
	public void setMccppc( String mccppc ){
		 this.mccppc = mccppc;
	}
	public void setMccpp( String mccpp ){
		 this.mccpp = mccpp;
	}
	public void setSa01typcde( String sa01typcde ){
		 this.sa01typcde = sa01typcde;
	}
	public void setSa01ecflg( String sa01ecflg ){
		 this.sa01ecflg = sa01ecflg;
	}
	public void setSa01scde01( String sa01scde01 ){
		 this.sa01scde01 = sa01scde01;
	}
	public void setSa01nme( String sa01nme ){
		 this.sa01nme = sa01nme;
	}
	public void setSa01scde02( String sa01scde02 ){
		 this.sa01scde02 = sa01scde02;
	}
	public void setSa01sins( String sa01sins ){
		 this.sa01sins = sa01sins;
	}
	public void setSa01itc( String sa01itc ){
		 this.sa01itc = sa01itc;
	}
	public void setSa01insprd( String sa01insprd ){
		 this.sa01insprd = sa01insprd;
	}
	public void setSa01ppc( String sa01ppc ){
		 this.sa01ppc = sa01ppc;
	}
	public void setSa01pp( String sa01pp ){
		 this.sa01pp = sa01pp;
	}
	public void setSa02typcde( String sa02typcde ){
		 this.sa02typcde = sa02typcde;
	}
	public void setSa02ecflg( String sa02ecflg ){
		 this.sa02ecflg = sa02ecflg;
	}
	public void setSa02scde01( String sa02scde01 ){
		 this.sa02scde01 = sa02scde01;
	}
	public void setSa02nme( String sa02nme ){
		 this.sa02nme = sa02nme;
	}
	public void setSa02scde02( String sa02scde02 ){
		 this.sa02scde02 = sa02scde02;
	}
	public void setSa02sins( String sa02sins ){
		 this.sa02sins = sa02sins;
	}
	public void setSa02itc( String sa02itc ){
		 this.sa02itc = sa02itc;
	}
	public void setSa02insprd( String sa02insprd ){
		 this.sa02insprd = sa02insprd;
	}
	public void setSa02ppc( String sa02ppc ){
		 this.sa02ppc = sa02ppc;
	}
	public void setSa02pp( String sa02pp ){
		 this.sa02pp = sa02pp;
	}
	public void setSa03typcde( String sa03typcde ){
		 this.sa03typcde = sa03typcde;
	}
	public void setSa03ecflg( String sa03ecflg ){
		 this.sa03ecflg = sa03ecflg;
	}
	public void setSa03scde01( String sa03scde01 ){
		 this.sa03scde01 = sa03scde01;
	}
	public void setSa03nme( String sa03nme ){
		 this.sa03nme = sa03nme;
	}
	public void setSa03scde02( String sa03scde02 ){
		 this.sa03scde02 = sa03scde02;
	}
	public void setSa03sins( String sa03sins ){
		 this.sa03sins = sa03sins;
	}
	public void setSa03itc( String sa03itc ){
		 this.sa03itc = sa03itc;
	}
	public void setSa03insprd( String sa03insprd ){
		 this.sa03insprd = sa03insprd;
	}
	public void setSa03ppc( String sa03ppc ){
		 this.sa03ppc = sa03ppc;
	}
	public void setSa03pp( String sa03pp ){
		 this.sa03pp = sa03pp;
	}
	public void setSa04typcde( String sa04typcde ){
		 this.sa04typcde = sa04typcde;
	}
	public void setSa04ecflg( String sa04ecflg ){
		 this.sa04ecflg = sa04ecflg;
	}
	public void setSa04scde01( String sa04scde01 ){
		 this.sa04scde01 = sa04scde01;
	}
	public void setSa04nme( String sa04nme ){
		 this.sa04nme = sa04nme;
	}
	public void setSa04scde02( String sa04scde02 ){
		 this.sa04scde02 = sa04scde02;
	}
	public void setSa04sins( String sa04sins ){
		 this.sa04sins = sa04sins;
	}
	public void setSa04itc( String sa04itc ){
		 this.sa04itc = sa04itc;
	}
	public void setSa04insprd( String sa04insprd ){
		 this.sa04insprd = sa04insprd;
	}
	public void setSa04ppc( String sa04ppc ){
		 this.sa04ppc = sa04ppc;
	}
	public void setSa04pp( String sa04pp ){
		 this.sa04pp = sa04pp;
	}
	public void setSa05typcde( String sa05typcde ){
		 this.sa05typcde = sa05typcde;
	}
	public void setSaecflg5( String saecflg5 ){
		 this.saecflg5 = saecflg5;
	}
	public void setSa05scde01( String sa05scde01 ){
		 this.sa05scde01 = sa05scde01;
	}
	public void setSa05nme( String sa05nme ){
		 this.sa05nme = sa05nme;
	}
	public void setSa05scde02( String sa05scde02 ){
		 this.sa05scde02 = sa05scde02;
	}
	public void setSa05sins( String sa05sins ){
		 this.sa05sins = sa05sins;
	}
	public void setSa05itc( String sa05itc ){
		 this.sa05itc = sa05itc;
	}
	public void setSa05insprd( String sa05insprd ){
		 this.sa05insprd = sa05insprd;
	}
	public void setSa05ppc( String sa05ppc ){
		 this.sa05ppc = sa05ppc;
	}
	public void setSa05pp( String sa05pp ){
		 this.sa05pp = sa05pp;
	}
	public void setSa06typcde( String sa06typcde ){
		 this.sa06typcde = sa06typcde;
	}
	public void setSa06ecflg( String sa06ecflg ){
		 this.sa06ecflg = sa06ecflg;
	}
	public void setSa06scde01( String sa06scde01 ){
		 this.sa06scde01 = sa06scde01;
	}
	public void setSa06nme( String sa06nme ){
		 this.sa06nme = sa06nme;
	}
	public void setSa06scde02( String sa06scde02 ){
		 this.sa06scde02 = sa06scde02;
	}
	public void setSa06sins( String sa06sins ){
		 this.sa06sins = sa06sins;
	}
	public void setSa06itc( String sa06itc ){
		 this.sa06itc = sa06itc;
	}
	public void setSa06insprd( String sa06insprd ){
		 this.sa06insprd = sa06insprd;
	}
	public void setSa06ppc( String sa06ppc ){
		 this.sa06ppc = sa06ppc;
	}
	public void setSa06pp( String sa06pp ){
		 this.sa06pp = sa06pp;
	}
	public void setUstatcate( String ustatcate ){
		 this.ustatcate = ustatcate;
	}
	public void setCmec( String cmec ){
		 this.cmec = cmec;
	}
	public void setCifccc( String cifccc ){
		 this.cifccc = cifccc;
	}
	public void setAsmcde( String asmcde ){
		 this.asmcde = asmcde;
	}
	public void setLircde( String lircde ){
		 this.lircde = lircde;
	}
	public void setAcctyp( String acctyp ){
		 this.acctyp = acctyp;
	}
	public void setAccno( String accno ){
		 this.accno = accno;
	}
	public void setLcode( String lcode ){
		 this.lcode = lcode;
	}
	public void setDthbnft( String dthbnft ){
		 this.dthbnft = dthbnft;
	}
	public void setRacate( String racate ){
		 this.racate = racate;
	}
	public void setDtypcde( String dtypcde ){
		 this.dtypcde = dtypcde;
	}
	public void setCaic( String caic ){
		 this.caic = caic;
	}
	public void setCnticnrct( String cnticnrct ){
		 this.cnticnrct = cnticnrct;
	}
	public void setTncp( String tncp ){
		 this.tncp = tncp;
	}
	public void setWrkplc( String wrkplc ){
		 this.wrkplc = wrkplc;
	}
	public void setBphno( String bphno ){
		 this.bphno = bphno;
	}
	public void setCmgntcde( String cmgntcde ){
		 this.cmgntcde = cmgntcde;
	}
	public void setLncde( String lncde ){
		 this.lncde = lncde;
	}
	public void setOsnocc( String osnocc ){
		 this.osnocc = osnocc;
	}
	public void setOsnmn( String osnmn ){
		 this.osnmn = osnmn;
	}
	public void setOsnbn( String osnbn ){
		 this.osnbn = osnbn;
	}
	public void setGrpcrtlno( String grpcrtlno ){
		 this.grpcrtlno = grpcrtlno;
	}
	public void setScno( String scno ){
		 this.scno = scno;
	}
	public void setGrpinsnme( String grpinsnme ){
		 this.grpinsnme = grpinsnme;
	}
	public void setCrsnme( String crsnme ){
		 this.crsnme = crsnme;
	}
	public void setType( String type ){
		 this.type = type;
	}
	public void setNounits( String nounits ){
		 this.nounits = nounits;
	}
	public void setAgregno( String agregno ){
		 this.agregno = agregno;
	}
	public void setAgcde01( String agcde01 ){
		 this.agcde01 = agcde01;
	}
	public void setAgcde02( String agcde02 ){
		 this.agcde02 = agcde02;
	}
	public void setCaflgscde01( String caflgscde01 ){
		 this.caflgscde01 = caflgscde01;
	}
	public void setCanknjiecflg( String canknjiecflg ){
		 this.canknjiecflg = canknjiecflg;
	}
	public void setCaflgadrs( String caflgadrs ){
		 this.caflgadrs = caflgadrs;
	}
	public void setCnkjicflgscd( String cnkjicflgscd ){
		 this.cnkjicflgscd = cnkjicflgscd;
	}
	public void setCaflgscde02( String caflgscde02 ){
		 this.caflgscde02 = caflgscde02;
	}
	public void setCnflgscde( String cnflgscde ){
		 this.cnflgscde = cnflgscde;
	}
	public void setSpnmknji( String spnmknji ){
		 this.spnmknji = spnmknji;
	}
	public void setSpnmscde( String spnmscde ){
		 this.spnmscde = spnmscde;
	}
	public void setDtechgprem( String dtechgprem ){
		 this.dtechgprem = dtechgprem;
	}
	public void setModinstprem( String modinstprem ){
		 this.modinstprem = modinstprem;
	}
	public void setPremlncrt( String premlncrt ){
		 this.premlncrt = premlncrt;
	}
	public void setDobph( String dobph ){
		 this.dobph = dobph;
	}
	public void setCgender( String cgender ){
		 this.cgender = cgender;
	}
	public void setBphno02( String bphno02 ){
		 this.bphno02 = bphno02;
	}
	public void setBckup01( String bckup01 ){
		 this.bckup01 = bckup01;
	}
	public void setCmpnyter( String cmpnyter ){
		 this.cmpnyter = cmpnyter;
	}
	public void setPstrtflg( String pstrtflg ){
		 this.pstrtflg = pstrtflg;
	}
	public void setAntypymnt( String antypymnt ){
		 this.antypymnt = antypymnt;
	}
	public void setAnpenpymnt( String anpenpymnt ){
		 this.anpenpymnt = anpenpymnt;
	}
	public void setPensnfund( String pensnfund ){
		 this.pensnfund = pensnfund;
	}
	public void setPensncrtno( String pensncrtno ){
		 this.pensncrtno = pensncrtno;
	}
	public void setPayecate( String payecate ){
		 this.payecate = payecate;
	}
	public void setAppno( String appno ){
		 this.appno = appno;
	}
	public void setBckup02( String bckup02 ){
		 this.bckup02 = bckup02;
	}
	public void setCorpterty( String corpterty ){
		 this.corpterty = corpterty;
	}
	public void setAgncnum( String agncnum ){
		 this.agncnum = agncnum;
	}
	public void setExprtflg( String exprtflg ){
		 this.exprtflg = exprtflg;
	}
}
