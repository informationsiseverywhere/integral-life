package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;

import com.dxc.integral.fsu.dao.AgncypfDAO;
import com.dxc.integral.fsu.dao.model.Agncypf;
import com.dxc.integral.iaf.config.IntegralSbReportConfig;
import com.dxc.integral.life.beans.L2CommongwDTO;
import com.dxc.integral.life.config.L2commongwJsonMapping;
import com.dxc.integral.life.dao.ComgwpfDAO;
import com.dxc.integral.life.dao.model.Comgwpf;

/**
 * 
 * ItemWriter for L2commongw batch step.
 *
 */
public class L2CommongwItemWriterS2 implements ItemWriter<Agncypf> {
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CommongwItemWriterS2.class);

	@Value("#{stepExecution}")
	private StepExecution stepExecution;

	@Value("#{jobParameters['effectiveDate']}")
	private int effectiveDate;

	@Autowired
	private Environment env;

	@Autowired
	private ComgwpfDAO comgwpfDAO;
	
	@Autowired
	private AgncypfDAO agncypfDAO;

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends Agncypf> readerDTOList) throws Exception {

		List<String> agncList = new ArrayList<>();
		Map<String, String> agncyBrchMap = new ConcurrentHashMap<>();
		Map<String, String> agncyRegnumMap = new ConcurrentHashMap<>();
		List<L2CommongwDTO> readerList = new ArrayList<>();
		List<L2CommongwDTO> tempList;
		Map<String, List<L2CommongwDTO>> agncyMap = new ConcurrentHashMap<>();

		List<String> comgwagncList = new ArrayList<>();
		for (Agncypf dto : readerDTOList) {
			agncList.add(dto.getAgncynum());
			agncyBrchMap.put(dto.getAgncynum(), dto.getBrnchnum());
			agncyRegnumMap.put(dto.getAgncynum(), dto.getRegnum());
		}
		List<Comgwpf> comgwpfList = comgwpfDAO.searchComgwpfRecord(agncList);
		if (comgwpfList != null && !comgwpfList.isEmpty()) {
			for (Comgwpf comgwpf : comgwpfList) {
				readerList.add(setReaderDTO(comgwpf));
			}
		}
		for (String agncnum : agncList) {
			tempList = new ArrayList<>();
			for (L2CommongwDTO dto : readerList) {
				if (agncnum.equals(dto.getAgncnum())) {
					tempList.add(dto);
				}
			}
			if (!tempList.isEmpty()) {
				comgwagncList.add(agncnum);
				agncyMap.put(agncnum, tempList);
			}
		}
		LOGGER.info("L2CommongwItemWriter starts.");
		FlatFileItemWriter<L2CommongwDTO> flatFileObject = (FlatFileItemWriter<L2CommongwDTO>) stepExecution
				.getExecutionContext().get("flatFile");
		L2commongwJsonMapping jsonData = (L2commongwJsonMapping) stepExecution.getExecutionContext().get("jsonData");
		String path = IntegralSbReportConfig.getReportOutputPath() + env.getProperty("folder.cgw");
		String filename = "L2COMMONGW_" + effectiveDate + ".txt";
		String filepath = path + filename;
		int count = 1;
		for (String agncnum : comgwagncList) {
			if (new FileSystemResource(filepath).exists()) {
				flatFileObject.setShouldDeleteIfEmpty(true);
				flatFileObject.setAppendAllowed(true);
			}
			flatFileObject.setResource(new FileSystemResource(filepath));
			String header = getJsonHeader(jsonData.getHeader(), agncyBrchMap.get(agncnum), agncyRegnumMap.get(agncnum));
			L2CommongwHeaderWriter headerWriter = new L2CommongwHeaderWriter(header);
			flatFileObject.setHeaderCallback(headerWriter);
			flatFileObject.open(stepExecution.getExecutionContext());
			writeFile(agncyMap.get(agncnum), flatFileObject);
			String footer = getJsonFooter(jsonData.getTrailer(), agncyMap.get(agncnum).size(), true);
			String end = null;
			if (count == comgwagncList.size())
				end = getJsonFooter(jsonData.getEnd(), 0, false);
			L2CommongwFooterWriter footerWriter;
			if (end != null)
				footerWriter = new L2CommongwFooterWriter(footer + end);
			else
				footerWriter = new L2CommongwFooterWriter(footer);
			flatFileObject.setFooterCallback(footerWriter);
			flatFileObject.close();
			count++;
		}
		if(!comgwagncList.isEmpty()) {
			comgwpfDAO.updateExprtflg(comgwagncList);
			agncypfDAO.updatePpdate(comgwagncList, effectiveDate);
		}
		LOGGER.info("L2CommongwItemWriter ends.");
	}

	private String getJsonHeader(Map<String, ?> headerListMap, String brCode, String agncRegnum) {
		StringBuilder header = new StringBuilder();
		for (Map.Entry<String, ?> entry : headerListMap.entrySet()) {
			if (entry.getKey().equals("prcsdTime")) {
				String prcsdTime = stepExecution.getExecutionContext().get("prcsdTime").toString();
				header.append(appendData(prcsdTime, 6));
			} else if (entry.getKey().equals("trnmstnDate")) {
				header.append(effectiveDate);
			} else if (entry.getKey().equals("coyCode")) {
				header.append(stepExecution.getExecutionContext().get("companyCode"));
			} else if (entry.getKey().equals("brCode")) {
				header.append(brCode);
			} else if (entry.getKey().equals("agncRegnum")) {
				header.append(appendData(agncRegnum.trim(),13));
			} else {
				header.append(entry.getValue());
			}
		}
		return header.toString();
	}

	private String getJsonFooter(Map<String, String> trailerEndListMap, int size, boolean hasMoreRecodrd) {
		StringBuilder footer = new StringBuilder();
		for (Map.Entry<String, String> entry : trailerEndListMap.entrySet()) {
			if (entry.getKey().equals("noOfRcrds")) {
				if (size > 0) {
					String listsize = "" + size;
					StringBuilder recordCount = new StringBuilder();
					for (int i = 1; i <= 10 - listsize.length(); i++) {
						recordCount.append("0");
					}
					recordCount.append(listsize);
					footer.append(recordCount);
				} else {
					footer.append("0000000000");
				}
			} else {
				footer.append(entry.getValue());
			}
		}
		if (hasMoreRecodrd)
			footer.append(System.lineSeparator());
		return footer.toString();
	}
	
	private String appendData(String data, int size)	{
		StringBuilder appendedData = new StringBuilder();
		if(data.length()==size) {
			appendedData.append(data);
		} else if(data.length()>size) {
			appendedData.append(data.substring(0, size));
		} else {
			StringBuilder appender = new StringBuilder();
			for(int i=1;i<=size-data.length();i++) {
				appender.append("0");
			}
			appender.append(data);
			appendedData.append(appender);
		}
		return appendedData.toString();
	}

	public void writeFile(List<? extends L2CommongwDTO> readerDTOList,
			FlatFileItemWriter<L2CommongwDTO> flatFileObject) {
		try {
			LOGGER.info("Export Data Writing to file by thread: {}", Thread.currentThread().getName());
			if (flatFileObject != null) {
				flatFileObject.write(readerDTOList);
			}
		} catch (Exception e) {
			LOGGER.info("Caught Exception while writing to file ", e);
		}
	}

	private L2CommongwDTO setReaderDTO(Comgwpf comgwpfRecord) {
		L2CommongwDTO l2commongwReader = new L2CommongwDTO();
		l2commongwReader.setDatatype(comgwpfRecord.getDatatype());
		l2commongwReader.setInsucate(comgwpfRecord.getInsucate());
		l2commongwReader.setBokcls(comgwpfRecord.getBokcls());
		l2commongwReader.setSecunum(comgwpfRecord.getSecunum());
		l2commongwReader.setBrnchnum(comgwpfRecord.getBrnchnum());
		l2commongwReader.setToi(comgwpfRecord.getToi());
		l2commongwReader.setMop(comgwpfRecord.getMop());
		l2commongwReader.setColcate(comgwpfRecord.getColcate());
		l2commongwReader.setInstal(comgwpfRecord.getInstal());
		l2commongwReader.setKyocde(comgwpfRecord.getKyocde());
		l2commongwReader.setKyoacs(comgwpfRecord.getKyoacs());
		l2commongwReader.setPcde(comgwpfRecord.getPcde());
		l2commongwReader.setPacs(comgwpfRecord.getPacs());
		l2commongwReader.setGrpcde(comgwpfRecord.getGrpcde());
		l2commongwReader.setAffilcde(comgwpfRecord.getAffilcde());
		l2commongwReader.setEmpcde(comgwpfRecord.getEmpcde());
		l2commongwReader.setCgrpc(comgwpfRecord.getCgrpc());
		l2commongwReader.setTotprem(appendData(comgwpfRecord.getTotprem().trim(),10));
		l2commongwReader.setBckp(comgwpfRecord.getBckp());
		l2commongwReader.setDpcde(comgwpfRecord.getDpcde());
		l2commongwReader.setBrnchnum2(comgwpfRecord.getBrnchnum2());
		l2commongwReader.setIncpn(comgwpfRecord.getIncpn());
		l2commongwReader.setMatrty(comgwpfRecord.getMatrty());
		l2commongwReader.setTncdte(comgwpfRecord.getTncdte());
		l2commongwReader.setTrnendte(comgwpfRecord.getTrnendte());
		l2commongwReader.setDteofapp(comgwpfRecord.getDteofapp());
		l2commongwReader.setSlfcrt(comgwpfRecord.getSlfcrt());
		l2commongwReader.setLpcc(comgwpfRecord.getLpcc());
		l2commongwReader.setDcde(comgwpfRecord.getDcde());
		l2commongwReader.setOprtcde(comgwpfRecord.getOprtcde());
		l2commongwReader.setPostcde(comgwpfRecord.getPostcde());
		l2commongwReader.setCntadd(comgwpfRecord.getCntadd());
		l2commongwReader.setPnum(comgwpfRecord.getPnum());
		l2commongwReader.setSubnme(comgwpfRecord.getSubnme());
		l2commongwReader.setSpfip(appendData(comgwpfRecord.getSpfip().trim(),10));
		l2commongwReader.setSpltyp(comgwpfRecord.getSpltyp());
		l2commongwReader.setPymntdte(comgwpfRecord.getPymntdte());
		l2commongwReader.setFpdfgh(comgwpfRecord.getFpdfgh());
		l2commongwReader.setInsyr(comgwpfRecord.getInsyr());
		l2commongwReader.setFrstme(comgwpfRecord.getFrstme());
		l2commongwReader.setPprem(appendData(comgwpfRecord.getPprem().trim(),10));
		l2commongwReader.setPpendatme(comgwpfRecord.getPpendatme());
		l2commongwReader.setNwccls(comgwpfRecord.getNwccls());
		l2commongwReader.setCcps(comgwpfRecord.getCcps());
		l2commongwReader.setIpactc(comgwpfRecord.getIpactc());
		l2commongwReader.setAmor(comgwpfRecord.getAmor());
		l2commongwReader.setGrpcara(comgwpfRecord.getGrpcara());
		l2commongwReader.setTelphno(comgwpfRecord.getTelphno());
		l2commongwReader.setNwcmpncde(comgwpfRecord.getNwcmpncde());
		l2commongwReader.setRftrns(comgwpfRecord.getRftrns());
		l2commongwReader.setMargin(comgwpfRecord.getMargin());
		l2commongwReader.setInacviwe(comgwpfRecord.getInacviwe());
		l2commongwReader.setProsqnc(comgwpfRecord.getProsqnc());
		l2commongwReader.setYnmrcd(comgwpfRecord.getYnmrcd());
		l2commongwReader.setInscmpnycde(comgwpfRecord.getInscmpnycde());
		l2commongwReader.setInsprocde(comgwpfRecord.getInsprocde());
		l2commongwReader.setCmpnycde(comgwpfRecord.getCmpnycde());
		l2commongwReader.setPrsnmekana(comgwpfRecord.getPrsnmekana());
		l2commongwReader.setIpecflg(comgwpfRecord.getIpecflg());
		l2commongwReader.setIpsftcde01(comgwpfRecord.getIpsftcde01());
		l2commongwReader.setIpnmknji(comgwpfRecord.getIpnmknji());
		l2commongwReader.setIpsftcde02(comgwpfRecord.getIpsftcde02());
		l2commongwReader.setIpdob(appendData(comgwpfRecord.getIpdob().trim(),8));
		l2commongwReader.setIpage(appendData(comgwpfRecord.getIpage().trim(),3));
		l2commongwReader.setIpgndr(comgwpfRecord.getIpgndr());
		l2commongwReader.setIprln(comgwpfRecord.getIprln());
		l2commongwReader.setIpostcde(comgwpfRecord.getIpostcde());
		l2commongwReader.setIaeflg(comgwpfRecord.getIaeflg());
		l2commongwReader.setIashtcde01(comgwpfRecord.getIashtcde01());
		l2commongwReader.setIadrsknji(comgwpfRecord.getIadrsknji());
		l2commongwReader.setIashtcde02(comgwpfRecord.getIashtcde02());
		l2commongwReader.setPymntrt(comgwpfRecord.getPymntrt());
		l2commongwReader.setLsprem(appendData(comgwpfRecord.getLsprem().trim(),10));
		l2commongwReader.setModundrprd(appendData(comgwpfRecord.getModundrprd().trim(),2));
		l2commongwReader.setIndcate(comgwpfRecord.getIndcate());
		l2commongwReader.setIndcprd(appendData(comgwpfRecord.getIndcprd().trim(),2));
		l2commongwReader.setDvndcls(comgwpfRecord.getDvndcls());
		l2commongwReader.setPdexntdvw(comgwpfRecord.getPdexntdvw());
		l2commongwReader.setAttchngcls(comgwpfRecord.getAttchngcls());
		l2commongwReader.setAppcate(comgwpfRecord.getAppcate());
		l2commongwReader.setIppyear(appendData(comgwpfRecord.getIppyear().trim(),3));
		l2commongwReader.setPpsdte(comgwpfRecord.getPpsdte());
		l2commongwReader.setPasbdy(comgwpfRecord.getPasbdy());
		l2commongwReader.setAddscnt(comgwpfRecord.getAddscnt());
		l2commongwReader.setNorids(appendData(comgwpfRecord.getNorids().trim(),3));
		l2commongwReader.setGrpcde02(comgwpfRecord.getGrpcde02());
		l2commongwReader.setLocde(comgwpfRecord.getLocde());
		l2commongwReader.setExmcate(comgwpfRecord.getExmcate());
		l2commongwReader.setTaxelg(comgwpfRecord.getTaxelg());
		l2commongwReader.setLvnnds(comgwpfRecord.getLvnnds());
		l2commongwReader.setBnsprem(appendData(comgwpfRecord.getBnsprem().trim(),10));
		l2commongwReader.setBcagyr(comgwpfRecord.getBcagyr());
		l2commongwReader.setOpcde(comgwpfRecord.getOpcde());
		l2commongwReader.setMcctypcde(comgwpfRecord.getMcctypcde());
		l2commongwReader.setMccecflg(comgwpfRecord.getMccecflg());
		l2commongwReader.setMccscde01(comgwpfRecord.getMccscde01());
		l2commongwReader.setMccnmc(comgwpfRecord.getMccnmc());
		l2commongwReader.setMccscde02(comgwpfRecord.getMccscde02());
		l2commongwReader.setMccppa(appendData(comgwpfRecord.getMccppa().trim(),11));
		l2commongwReader.setMccitc(comgwpfRecord.getMccitc());
		l2commongwReader.setMccip(comgwpfRecord.getMccip());
		l2commongwReader.setMccppc(comgwpfRecord.getMccppc());
		l2commongwReader.setMccpp(comgwpfRecord.getMccpp());
		l2commongwReader.setSa01typcde(comgwpfRecord.getSa01typcde());
		l2commongwReader.setSa01ecflg(comgwpfRecord.getSa01ecflg());
		l2commongwReader.setSa01scde01(comgwpfRecord.getSa01scde01());
		l2commongwReader.setSa01scde02(comgwpfRecord.getSa01scde02());
		l2commongwReader.setSa01nme(comgwpfRecord.getSa01nme());
		l2commongwReader.setSa01sins(appendData(comgwpfRecord.getSa01sins().trim(),11));
		l2commongwReader.setSa01itc(comgwpfRecord.getSa01itc());
		l2commongwReader.setSa01insprd(comgwpfRecord.getSa01insprd());
		l2commongwReader.setSa01ppc(comgwpfRecord.getSa01ppc());
		l2commongwReader.setSa01pp(comgwpfRecord.getSa01pp());
		l2commongwReader.setSa02typcde(comgwpfRecord.getSa02typcde());
		l2commongwReader.setSa02ecflg(comgwpfRecord.getSa02ecflg());
		l2commongwReader.setSa02scde01(comgwpfRecord.getSa02scde01());
		l2commongwReader.setSa02scde02(comgwpfRecord.getSa02scde02());
		l2commongwReader.setSa02nme(comgwpfRecord.getSa02nme());
		l2commongwReader.setSa02sins(appendData(comgwpfRecord.getSa02sins().trim(),11));
		l2commongwReader.setSa02itc(comgwpfRecord.getSa02itc());
		l2commongwReader.setSa02insprd(comgwpfRecord.getSa02insprd());
		l2commongwReader.setSa02ppc(comgwpfRecord.getSa02ppc());
		l2commongwReader.setSa02pp(comgwpfRecord.getSa02pp());
		l2commongwReader.setSa03typcde(comgwpfRecord.getSa03typcde());
		l2commongwReader.setSa03ecflg(comgwpfRecord.getSa03ecflg());
		l2commongwReader.setSa03scde01(comgwpfRecord.getSa03scde01());
		l2commongwReader.setSa03scde02(comgwpfRecord.getSa03scde02());
		l2commongwReader.setSa03nme(comgwpfRecord.getSa03nme());
		l2commongwReader.setSa03sins(appendData(comgwpfRecord.getSa03sins().trim(),11));
		l2commongwReader.setSa03itc(comgwpfRecord.getSa03itc());
		l2commongwReader.setSa03insprd(comgwpfRecord.getSa03insprd());
		l2commongwReader.setSa03ppc(comgwpfRecord.getSa03ppc());
		l2commongwReader.setSa03pp(comgwpfRecord.getSa03pp());
		l2commongwReader.setSa04typcde(comgwpfRecord.getSa04typcde());
		l2commongwReader.setSa04ecflg(comgwpfRecord.getSa04ecflg());
		l2commongwReader.setSa04scde01(comgwpfRecord.getSa04scde01());
		l2commongwReader.setSa04scde02(comgwpfRecord.getSa04scde02());
		l2commongwReader.setSa04nme(comgwpfRecord.getSa04nme());
		l2commongwReader.setSa04sins(appendData(comgwpfRecord.getSa04sins().trim(),11));
		l2commongwReader.setSa04itc(comgwpfRecord.getSa04itc());
		l2commongwReader.setSa04insprd(comgwpfRecord.getSa04insprd());
		l2commongwReader.setSa04ppc(comgwpfRecord.getSa04ppc());
		l2commongwReader.setSa04pp(comgwpfRecord.getSa04pp());
		l2commongwReader.setSa05typcde(comgwpfRecord.getSa05typcde());
		l2commongwReader.setSaecflg5(comgwpfRecord.getSaecflg5());
		l2commongwReader.setSa05scde01(comgwpfRecord.getSa05scde01());
		l2commongwReader.setSa05scde02(comgwpfRecord.getSa05scde02());
		l2commongwReader.setSa05nme(comgwpfRecord.getSa05nme());
		l2commongwReader.setSa05sins(appendData(comgwpfRecord.getSa05sins().trim(),11));
		l2commongwReader.setSa05itc(comgwpfRecord.getSa05itc());
		l2commongwReader.setSa05insprd(comgwpfRecord.getSa05insprd());
		l2commongwReader.setSa05ppc(comgwpfRecord.getSa05ppc());
		l2commongwReader.setSa05pp(comgwpfRecord.getSa05pp());
		l2commongwReader.setSa06typcde(comgwpfRecord.getSa06typcde());
		l2commongwReader.setSa06ecflg(comgwpfRecord.getSa06ecflg());
		l2commongwReader.setSa06scde01(comgwpfRecord.getSa06scde01());
		l2commongwReader.setSa06scde02(comgwpfRecord.getSa06scde02());
		l2commongwReader.setSa06nme(comgwpfRecord.getSa06nme());
		l2commongwReader.setSa06sins(appendData(comgwpfRecord.getSa06sins().trim(),11));
		l2commongwReader.setSa06itc(comgwpfRecord.getSa06itc());
		l2commongwReader.setSa06insprd(comgwpfRecord.getSa06insprd());
		l2commongwReader.setSa06ppc(comgwpfRecord.getSa06ppc());
		l2commongwReader.setSa06pp(comgwpfRecord.getSa06pp());
		l2commongwReader.setUstatcate(comgwpfRecord.getUstatcate());
		l2commongwReader.setCmec(comgwpfRecord.getCmec());
		l2commongwReader.setCifccc(comgwpfRecord.getCifccc());
		l2commongwReader.setAsmcde(comgwpfRecord.getAsmcde());
		l2commongwReader.setLircde(comgwpfRecord.getLircde());
		l2commongwReader.setAcctyp(comgwpfRecord.getAcctyp());
		l2commongwReader.setAccno(comgwpfRecord.getAccno());
		l2commongwReader.setLcode(comgwpfRecord.getLcode());
		l2commongwReader.setDthbnft(appendData(comgwpfRecord.getDthbnft().trim(),6));
		l2commongwReader.setRacate(comgwpfRecord.getRacate());
		l2commongwReader.setDtypcde(comgwpfRecord.getDtypcde());
		l2commongwReader.setCaic(comgwpfRecord.getCaic());
		l2commongwReader.setCnticnrct(comgwpfRecord.getCnticnrct());
		l2commongwReader.setTncp(appendData(comgwpfRecord.getTncp().trim(),10));
		l2commongwReader.setWrkplc(comgwpfRecord.getWrkplc());
		l2commongwReader.setBphno(comgwpfRecord.getBphno());
		l2commongwReader.setCmgntcde(comgwpfRecord.getCmgntcde());
		l2commongwReader.setLncde(appendData(comgwpfRecord.getLncde().trim(),4));
		l2commongwReader.setOsnocc(comgwpfRecord.getOsnocc());
		l2commongwReader.setOsnmn(comgwpfRecord.getOsnmn());
		l2commongwReader.setOsnbn(comgwpfRecord.getOsnbn());
		l2commongwReader.setGrpcrtlno(comgwpfRecord.getGrpcrtlno());
		l2commongwReader.setScno(comgwpfRecord.getScno());
		l2commongwReader.setGrpinsnme(comgwpfRecord.getGrpinsnme());
		l2commongwReader.setCrsnme(comgwpfRecord.getCrsnme());
		l2commongwReader.setType(comgwpfRecord.getType());
		l2commongwReader.setNounits(appendData(comgwpfRecord.getNounits().trim(),2));
		l2commongwReader.setAgregno(comgwpfRecord.getAgregno());
		l2commongwReader.setAgcde01(comgwpfRecord.getAgcde01());
		l2commongwReader.setAgcde02(comgwpfRecord.getAgcde02());
		l2commongwReader.setCaflgscde01(comgwpfRecord.getCaflgscde01());
		l2commongwReader.setCanknjiecflg(comgwpfRecord.getCanknjiecflg());
		l2commongwReader.setCaflgadrs(comgwpfRecord.getCaflgadrs());
		l2commongwReader.setCnkjicflgscd(comgwpfRecord.getCnkjicflgscd());
		l2commongwReader.setCaflgscde02(comgwpfRecord.getCaflgscde02());
		l2commongwReader.setCnflgscde(comgwpfRecord.getCnflgscde());
		l2commongwReader.setSpnmknji(comgwpfRecord.getSpnmknji());
		l2commongwReader.setSpnmscde(comgwpfRecord.getSpnmscde());
		l2commongwReader.setDtechgprem(comgwpfRecord.getDtechgprem());
		l2commongwReader.setModinstprem(appendData(comgwpfRecord.getModinstprem().trim(),10));
		l2commongwReader.setPremlncrt(comgwpfRecord.getPremlncrt());
		l2commongwReader.setDobph(comgwpfRecord.getDobph());
		l2commongwReader.setCgender(comgwpfRecord.getCgender());
		l2commongwReader.setBphno02(comgwpfRecord.getBphno02());
		l2commongwReader.setBckup01(comgwpfRecord.getBckup01());
		l2commongwReader.setCmpnyter(comgwpfRecord.getCmpnyter());
		l2commongwReader.setPstrtflg(comgwpfRecord.getPstrtflg());
		l2commongwReader.setAntypymnt(appendData(comgwpfRecord.getAntypymnt().trim(),10));
		l2commongwReader.setAnpenpymnt(appendData(comgwpfRecord.getAnpenpymnt().trim(),2));
		l2commongwReader.setPensnfund(appendData(comgwpfRecord.getPensnfund().trim(),10));
		l2commongwReader.setPensncrtno(comgwpfRecord.getPensncrtno());
		l2commongwReader.setPayecate(comgwpfRecord.getPayecate());
		l2commongwReader.setAppno(comgwpfRecord.getAppno());
		l2commongwReader.setBckup02(comgwpfRecord.getBckup02());
		l2commongwReader.setCorpterty(comgwpfRecord.getCorpterty());
		l2commongwReader.setAgncnum(comgwpfRecord.getAgncnum());
		l2commongwReader.setExprtflg(comgwpfRecord.getExprtflg());

		return l2commongwReader;
	}
}
