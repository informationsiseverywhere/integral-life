package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2571 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S2571screensfl s2571screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S2571screensfl getS2571screensfl() {
		return s2571screensfl;
	}
	public void setS2571screensfl(S2571screensfl s2571screensfl) {
		this.s2571screensfl = s2571screensfl;
	}
	
	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class S2571screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;
	
		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
		
		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String select;
			private String bankacckey;	
			private String bankaccdsc;
			private String currcode;
			private String currfromxDisp;
			private String currtoxDisp;
			private String facthous;
			private String inqline;
			private String bankacckeyhidden;

			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
			public String getBankacckey() {
				return bankacckey;
			}
			public void setBankacckey(String bankacckey) {
				this.bankacckey = bankacckey;
			}
			public String getBankaccdsc() {
				return bankaccdsc;
			}
			public void setBankaccdsc(String bankaccdsc) {
				this.bankaccdsc = bankaccdsc;
			}
			public String getCurrcode() {
				return currcode;
			}
			public void setCurrcode(String currcode) {
				this.currcode = currcode;
			}
			public String getCurrfromxDisp() {
				return currfromxDisp;
			}
			public void setCurrfromxDisp(String currfromxDisp) {
				this.currfromxDisp = currfromxDisp;
			}
			public String getCurrtoxDisp() {
				return currtoxDisp;
			}
			public void setCurrtoxDisp(String currtoxDisp) {
				this.currtoxDisp = currtoxDisp;
			}
			public String getFacthous() {
				return facthous;
			}
			public void setFacthous(String facthous) {
				this.facthous = facthous;
			}
			public String getInqline() {
				return inqline;
			}
			public void setInqline(String inqline) {
				this.inqline = inqline;
			}
			public String getBankacckeyhidden() {
				return bankacckeyhidden;
			}
			public void setBankacckeyhidden(String bankacckeyhidden) {
				this.bankacckeyhidden = bankacckeyhidden;
			}
		} 
	}

	@Override
	public String toString() {
		return "S2571 []";
	}
	
}
