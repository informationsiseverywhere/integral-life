package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * L2NEWINTCP batch reader DTO.
 * 
 * @author dpuhawan
 *
 */
public class L2newintcpReaderDTO {

	//member variables for columns
	private String chdrcoy;
	private String chdrnum;
	private Integer loannumber;
	private String loantype;
	private String loancurr;
	private String validflag;
	private BigDecimal loanorigam;
	private Integer loanstdate;
	private BigDecimal lstcaplamt;
	private Integer lstcapdate;
	private Integer nxtcapdate;
	private Integer lstintbdte;
	private Integer nxtintbdte;
	private Integer tranno;
	private String cnttype;
	private String chdrpfx;
	private String servunit;
	private Integer occdate;
	private Integer ccdate;
	private String cownpfx;
	private String cowncoy;
	private String cownnum;
	private String collchnl;
	private String cntbranch;
	private String cntcurr;
	private Integer polsum;
	private Integer polinc;
	private String agntpfx;
	private String agntcoy;
	private String agntnum;
	private String clntpfx;
	private String clntcoy;
	private String clntnum;
	private String mandref;
	private Integer ptdate;
	private String billchnl;
	private String billfreq;
	private String billcurr;
	private Integer nextdate;
	private String bankkey;
	private String bankacckey;
	private String mandstat;
	private String facthous;
	
	private String usrprf;
	private String jobnm;
	private Integer userNum;
	private Integer tranCount;
	private String batctrcde;
	private Integer loantypidx;
	private String oldChdrnum;
	private String language;
	
	//PTRNPF
	private String recode;
	private Integer ptrneff;
	private Integer trdt;
	private Integer trtm;
	private String termid;
	private Integer userT;
	private String batcpfx;
	private String batccoy;
	private String batcbrn;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batcbatch;
	private String prtflg;
	private Date datime;
	private Integer datesub;
	private String crtuser;
	
	//BEXTPF
	private Integer btdate;
	private Integer billdate;
	private String bankcode;
	private Integer instfrom;
	private Integer instto;
	private String instbchnl;
	private String instcchnl;
	private String instfreq;
	private BigDecimal instamt01;
	private BigDecimal instamt02;
	private BigDecimal instamt03;
	private BigDecimal instamt04;
	private BigDecimal instamt05;
	private BigDecimal instamt06;
	private BigDecimal instamt07;
	private BigDecimal instamt08;
	private BigDecimal instamt09;
	private BigDecimal instamt10;
	private BigDecimal instamt11;
	private BigDecimal instamt12;
	private BigDecimal instamt13;
	private BigDecimal instamt14;
	private BigDecimal instamt15;
	private String instjctl;
	private String grupkey;
	private String membsel;
	private String payrpfx;
	private String payrcoy;
	private String payrnum;
	private String payflag;
	private String bilflag;
	private String outflag;
	private String supflag;
	private Integer billcd;
	private String sacscode;
	private String sacstyp;
	private String glmap;
	private Integer ddderef;
	private Integer effdatex;
	private String ddrsncde;
	private String canflag;
	private String company;
	private Integer jobno;	
	
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public String getLoancurr() {
		return loancurr;
	}
	public void setLoancurr(String loancurr) {
		this.loancurr = loancurr;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public BigDecimal getLoanorigam() {
		return loanorigam;
	}
	public void setLoanorigam(BigDecimal loanorigam) {
		this.loanorigam = loanorigam;
	}
	public Integer getLoanstdate() {
		return loanstdate;
	}
	public void setLoanstdate(Integer loanstdate) {
		this.loanstdate = loanstdate;
	}
	public BigDecimal getLstcaplamt() {
		return lstcaplamt;
	}
	public void setLstcaplamt(BigDecimal lstcaplamt) {
		this.lstcaplamt = lstcaplamt;
	}
	public Integer getLstcapdate() {
		return lstcapdate;
	}
	public void setLstcapdate(Integer lstcapdate) {
		this.lstcapdate = lstcapdate;
	}
	public Integer getNxtcapdate() {
		return nxtcapdate;
	}
	public void setNxtcapdate(Integer nxtcapdate) {
		this.nxtcapdate = nxtcapdate;
	}
	public Integer getLstintbdte() {
		return lstintbdte;
	}
	public void setLstintbdte(Integer lstintbdte) {
		this.lstintbdte = lstintbdte;
	}
	public Integer getNxtintbdte() {
		return nxtintbdte;
	}
	public void setNxtintbdte(Integer nxtintbdte) {
		this.nxtintbdte = nxtintbdte;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getServunit() {
		return servunit;
	}
	public void setServunit(String servunit) {
		this.servunit = servunit;
	}
	public Integer getOccdate() {
		return occdate;
	}
	public void setOccdate(Integer occdate) {
		this.occdate = occdate;
	}
	public Integer getCcdate() {
		return ccdate;
	}
	public void setCcdate(Integer ccdate) {
		this.ccdate = ccdate;
	}
	public String getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}
	public String getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(String collchnl) {
		this.collchnl = collchnl;
	}
	public String getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public Integer getPolsum() {
		return polsum;
	}
	public void setPolsum(Integer polsum) {
		this.polsum = polsum;
	}
	public Integer getPolinc() {
		return polinc;
	}
	public void setPolinc(Integer polinc) {
		this.polinc = polinc;
	}
	public String getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(String agntpfx) {
		this.agntpfx = agntpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public Integer getPtdate() {
		return ptdate;
	}
	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}
	public Integer getNextdate() {
		return nextdate;
	}
	public void setNextdate(Integer nextdate) {
		this.nextdate = nextdate;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Integer getUserNum() {
		return userNum;
	}
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}
	public Integer getTranCount() {
		return tranCount;
	}
	public void setTranCount(Integer tranCount) {
		this.tranCount = tranCount;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public Integer getLoantypidx() {
		return loantypidx;
	}
	public void setLoantypidx(Integer loantypidx) {
		this.loantypidx = loantypidx;
	}
	public String getOldChdrnum() {
		return oldChdrnum;
	}
	public void setOldChdrnum(String oldChdrnum) {
		this.oldChdrnum = oldChdrnum;
	}
	public String getRecode() {
		return recode;
	}
	public void setRecode(String recode) {
		this.recode = recode;
	}
	public Integer getPtrneff() {
		return ptrneff;
	}
	public void setPtrneff(Integer ptrneff) {
		this.ptrneff = ptrneff;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getPrtflg() {
		return prtflg;
	}
	public void setPrtflg(String prtflg) {
		this.prtflg = prtflg;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}
	public Integer getDatesub() {
		return datesub;
	}
	public void setDatesub(Integer datesub) {
		this.datesub = datesub;
	}
	public String getCrtuser() {
		return crtuser;
	}
	public void setCrtuser(String crtuser) {
		this.crtuser = crtuser;
	}
	public Integer getBtdate() {
		return btdate;
	}
	public void setBtdate(Integer btdate) {
		this.btdate = btdate;
	}
	public Integer getBilldate() {
		return billdate;
	}
	public void setBilldate(Integer billdate) {
		this.billdate = billdate;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public Integer getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Integer instfrom) {
		this.instfrom = instfrom;
	}
	public Integer getInstto() {
		return instto;
	}
	public void setInstto(Integer instto) {
		this.instto = instto;
	}
	public String getInstbchnl() {
		return instbchnl;
	}
	public void setInstbchnl(String instbchnl) {
		this.instbchnl = instbchnl;
	}
	public String getInstcchnl() {
		return instcchnl;
	}
	public void setInstcchnl(String instcchnl) {
		this.instcchnl = instcchnl;
	}
	public String getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(String instfreq) {
		this.instfreq = instfreq;
	}
	public BigDecimal getInstamt01() {
		return instamt01;
	}
	public void setInstamt01(BigDecimal instamt01) {
		this.instamt01 = instamt01;
	}
	public BigDecimal getInstamt02() {
		return instamt02;
	}
	public void setInstamt02(BigDecimal instamt02) {
		this.instamt02 = instamt02;
	}
	public BigDecimal getInstamt03() {
		return instamt03;
	}
	public void setInstamt03(BigDecimal instamt03) {
		this.instamt03 = instamt03;
	}
	public BigDecimal getInstamt04() {
		return instamt04;
	}
	public void setInstamt04(BigDecimal instamt04) {
		this.instamt04 = instamt04;
	}
	public BigDecimal getInstamt05() {
		return instamt05;
	}
	public void setInstamt05(BigDecimal instamt05) {
		this.instamt05 = instamt05;
	}
	public BigDecimal getInstamt06() {
		return instamt06;
	}
	public void setInstamt06(BigDecimal instamt06) {
		this.instamt06 = instamt06;
	}
	public BigDecimal getInstamt07() {
		return instamt07;
	}
	public void setInstamt07(BigDecimal instamt07) {
		this.instamt07 = instamt07;
	}
	public BigDecimal getInstamt08() {
		return instamt08;
	}
	public void setInstamt08(BigDecimal instamt08) {
		this.instamt08 = instamt08;
	}
	public BigDecimal getInstamt09() {
		return instamt09;
	}
	public void setInstamt09(BigDecimal instamt09) {
		this.instamt09 = instamt09;
	}
	public BigDecimal getInstamt10() {
		return instamt10;
	}
	public void setInstamt10(BigDecimal instamt10) {
		this.instamt10 = instamt10;
	}
	public BigDecimal getInstamt11() {
		return instamt11;
	}
	public void setInstamt11(BigDecimal instamt11) {
		this.instamt11 = instamt11;
	}
	public BigDecimal getInstamt12() {
		return instamt12;
	}
	public void setInstamt12(BigDecimal instamt12) {
		this.instamt12 = instamt12;
	}
	public BigDecimal getInstamt13() {
		return instamt13;
	}
	public void setInstamt13(BigDecimal instamt13) {
		this.instamt13 = instamt13;
	}
	public BigDecimal getInstamt14() {
		return instamt14;
	}
	public void setInstamt14(BigDecimal instamt14) {
		this.instamt14 = instamt14;
	}
	public BigDecimal getInstamt15() {
		return instamt15;
	}
	public void setInstamt15(BigDecimal instamt15) {
		this.instamt15 = instamt15;
	}
	public String getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(String instjctl) {
		this.instjctl = instjctl;
	}
	public String getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(String grupkey) {
		this.grupkey = grupkey;
	}
	public String getMembsel() {
		return membsel;
	}
	public void setMembsel(String membsel) {
		this.membsel = membsel;
	}
	public String getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(String payrpfx) {
		this.payrpfx = payrpfx;
	}
	public String getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(String payrcoy) {
		this.payrcoy = payrcoy;
	}
	public String getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(String payrnum) {
		this.payrnum = payrnum;
	}
	public String getPayflag() {
		return payflag;
	}
	public void setPayflag(String payflag) {
		this.payflag = payflag;
	}
	public String getBilflag() {
		return bilflag;
	}
	public void setBilflag(String bilflag) {
		this.bilflag = bilflag;
	}
	public String getOutflag() {
		return outflag;
	}
	public void setOutflag(String outflag) {
		this.outflag = outflag;
	}
	public String getSupflag() {
		return supflag;
	}
	public void setSupflag(String supflag) {
		this.supflag = supflag;
	}
	public Integer getBillcd() {
		return billcd;
	}
	public void setBillcd(Integer billcd) {
		this.billcd = billcd;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getGlmap() {
		return glmap;
	}
	public void setGlmap(String glmap) {
		this.glmap = glmap;
	}
	public Integer getDdderef() {
		return ddderef;
	}
	public void setDdderef(Integer ddderef) {
		this.ddderef = ddderef;
	}
	public Integer getEffdatex() {
		return effdatex;
	}
	public void setEffdatex(Integer effdatex) {
		this.effdatex = effdatex;
	}
	public String getDdrsncde() {
		return ddrsncde;
	}
	public void setDdrsncde(String ddrsncde) {
		this.ddrsncde = ddrsncde;
	}
	public String getCanflag() {
		return canflag;
	}
	public void setCanflag(String canflag) {
		this.canflag = canflag;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Integer getJobno() {
		return jobno;
	}
	public void setJobno(Integer jobno) {
		this.jobno = jobno;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	

	
}