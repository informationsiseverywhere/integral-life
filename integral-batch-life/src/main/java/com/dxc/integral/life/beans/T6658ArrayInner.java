package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class T6658ArrayInner {
	private List<String> wsaaT6658Rec = new ArrayList<>();
	private List<Integer> wsaaT6658Currfrom = new ArrayList<>();
	private List<String> wsaaT6658Key = new ArrayList<>();
	private List<String> wsaaT6658Annmthd = new ArrayList<>();
	private List<String> wsaaT6658Data = new ArrayList<>();
	private List<String> wsaaT6658Billfreq = new ArrayList<>();
	private List<BigDecimal> wsaaT6658Fixdtrm = new ArrayList<>();
	private List<String> wsaaT6658Manopt = new ArrayList<>();
	private List<BigDecimal> wsaaT6658Maxpcnt = new ArrayList<>();
	private List<BigDecimal> wsaaT6658Minpcnt = new ArrayList<>();
	private List<String> wsaaT6658Optind = new ArrayList<>();
	private List<String> wsaaT6658Subprog = new ArrayList<>();
	private List<String> wsaaT6658Premsubr = new ArrayList<>();
	private List<String> wsaaT6658SimpInd = new ArrayList<>();

	public List<String> getWsaaT6658Rec() {
		return wsaaT6658Rec;
	}

	public void setWsaaT6658Rec(List<String> wsaaT6658Rec) {
		this.wsaaT6658Rec = wsaaT6658Rec;
	}

	public List<Integer> getWsaaT6658Currfrom() {
		return wsaaT6658Currfrom;
	}

	public void setWsaaT6658Currfrom(List<Integer> wsaaT6658Currfrom) {
		this.wsaaT6658Currfrom = wsaaT6658Currfrom;
	}

	public List<String> getWsaaT6658Key() {
		return wsaaT6658Key;
	}

	public void setWsaaT6658Key(List<String> wsaaT6658Key) {
		this.wsaaT6658Key = wsaaT6658Key;
	}

	public List<String> getWsaaT6658Annmthd() {
		return wsaaT6658Annmthd;
	}

	public void setWsaaT6658Annmthd(List<String> wsaaT6658Annmthd) {
		this.wsaaT6658Annmthd = wsaaT6658Annmthd;
	}

	public List<String> getWsaaT6658Data() {
		return wsaaT6658Data;
	}

	public void setWsaaT6658Data(List<String> wsaaT6658Data) {
		this.wsaaT6658Data = wsaaT6658Data;
	}

	public List<String> getWsaaT6658Billfreq() {
		return wsaaT6658Billfreq;
	}

	public void setWsaaT6658Billfreq(List<String> wsaaT6658Billfreq) {
		this.wsaaT6658Billfreq = wsaaT6658Billfreq;
	}

	public List<BigDecimal> getWsaaT6658Fixdtrm() {
		return wsaaT6658Fixdtrm;
	}

	public void setWsaaT6658Fixdtrm(List<BigDecimal> wsaaT6658Fixdtrm) {
		this.wsaaT6658Fixdtrm = wsaaT6658Fixdtrm;
	}

	public List<String> getWsaaT6658Manopt() {
		return wsaaT6658Manopt;
	}

	public void setWsaaT6658Manopt(List<String> wsaaT6658Manopt) {
		this.wsaaT6658Manopt = wsaaT6658Manopt;
	}

	public List<BigDecimal> getWsaaT6658Maxpcnt() {
		return wsaaT6658Maxpcnt;
	}

	public void setWsaaT6658Maxpcnt(List<BigDecimal> wsaaT6658Maxpcnt) {
		this.wsaaT6658Maxpcnt = wsaaT6658Maxpcnt;
	}

	public List<BigDecimal> getWsaaT6658Minpcnt() {
		return wsaaT6658Minpcnt;
	}

	public void setWsaaT6658Minpcnt(List<BigDecimal> wsaaT6658Minpcnt) {
		this.wsaaT6658Minpcnt = wsaaT6658Minpcnt;
	}

	public List<String> getWsaaT6658Optind() {
		return wsaaT6658Optind;
	}

	public void setWsaaT6658Optind(List<String> wsaaT6658Optind) {
		this.wsaaT6658Optind = wsaaT6658Optind;
	}

	public List<String> getWsaaT6658Subprog() {
		return wsaaT6658Subprog;
	}

	public void setWsaaT6658Subprog(List<String> wsaaT6658Subprog) {
		this.wsaaT6658Subprog = wsaaT6658Subprog;
	}

	public List<String> getWsaaT6658Premsubr() {
		return wsaaT6658Premsubr;
	}

	public void setWsaaT6658Premsubr(List<String> wsaaT6658Premsubr) {
		this.wsaaT6658Premsubr = wsaaT6658Premsubr;
	}

	public List<String> getWsaaT6658SimpInd() {
		return wsaaT6658SimpInd;
	}

	public void setWsaaT6658SimpInd(List<String> wsaaT6658SimpInd) {
		this.wsaaT6658SimpInd = wsaaT6658SimpInd;
	}
}
