package com.dxc.integral.life.beans.in;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String proxyTicket;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProxyTicket() {
		return proxyTicket;
	}
	public void setProxyTicket(String proxyTicket) {
		this.proxyTicket = proxyTicket;
	}
	@Override
	public String toString() {
		return "AuthenticationInfo []";
	}
	
}
