package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.T5671;
import com.dxc.integral.life.beans.L2CreateContractReaderDTO;
import com.dxc.integral.life.smarttable.pojo.T5673;
import com.dxc.integral.life.utils.L2CreateContractProcessorUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.commons.lang3.StringUtils;

@Service("l2CreateContractProcessorUtil")
public class L2CreateContractProcessorUtilImpl implements L2CreateContractProcessorUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CreateContractProcessorUtilImpl.class);
	private static final String ERROR_MESSAGES = "errorMessages";
	private static final String FIELD_NAME= "fieldName";
	private static final String MESSAGE= "message";
	private static final char END_MESSAGE= ';';
	private static final char END_FIELDNAME= '-';
	
	@Autowired
	private ItempfService itempfService;
	
	@Override
	public List<String> loadSmartTables(L2CreateContractReaderDTO l2CreateContractReaderDTO) {
	T5673 t5673 = null;
	List<String> result = new ArrayList<>();
	int date = Integer.parseInt(setDateFormat(l2CreateContractReaderDTO.getProposalDate()));
	t5673 = itempfService.readSmartTableByDate("T5673",l2CreateContractReaderDTO.getInsuranceTypeCode().trim(),date,date, T5673.class);
	if (t5673 == null) {
		throw new ItemNotfoundException("L2CreateContract: Item not found in Table T5673: " + l2CreateContractReaderDTO.getInsuranceTypeCode());
	}
	for (int i = 0; i < t5673.getCtables().size(); i++) {
		if(t5673.getCtables().get(i)!=null && !StringUtils.EMPTY.equals(t5673.getCtables().get(i)) && !result.contains(t5673.getCtables().get(i))
			&& l2CreateContractReaderDTO.getBaseContractAndRiderCode().contains(t5673.getCtables().get(i))){
				result.add(t5673.getCtables().get(i));
				break;
			}
	}
	for (int i = 0; i < t5673.getRtables().size(); i++) {
		if(t5673.getRtables().get(i)!=null && !StringUtils.EMPTY.equals(t5673.getRtables().get(i)) && !result.contains(t5673.getRtables().get(i))){
			if(l2CreateContractReaderDTO.getBaseContractAndRiderCode().contains(t5673.getRtables().get(i))){
				result.add(t5673.getRtables().get(i));
			}else result.add(StringUtils.SPACE);
		}
	}
	return result;
	}
	
	public String loadSmartTablesT600(String crtable){
		String t5671Item = "T600" + crtable.trim();
		T5671 t5671IO = null;
	
		try {
			t5671IO = itempfService.readT5671(Companies.LIFE, t5671Item);
		} catch (IOException e) {
			 LOGGER.error("Exception in loadSmartTablesT600: {0}", e);
		}
		if(null == t5671IO)
			throw new ItemNotfoundException("Item not found in Table T5671: " + crtable);

		if(!t5671IO.getPgms().isEmpty() && !t5671IO.getPgms().get(0).isEmpty())
			return t5671IO.getPgms().get(0);
		else
			return StringUtils.SPACE;
	}
	
	

	public String getErrorMessages(JsonNode valuesNodeOutput){
		
		StringBuilder errorMessage = new StringBuilder();
		if(valuesNodeOutput.isArray()){
			ArrayNode arrayNode = (ArrayNode) valuesNodeOutput.get(ERROR_MESSAGES);
			Iterator<JsonNode> node = arrayNode.elements();	
			while (node.hasNext()) {
				JsonNode dataField = node.next();
				if(null != dataField.get(FIELD_NAME)){
					errorMessage.append(dataField.get(FIELD_NAME).asText().trim());
					errorMessage.append(END_FIELDNAME);
				}
				if(null != dataField.get(MESSAGE)){
					errorMessage.append(dataField.get(MESSAGE).asText().trim());
					errorMessage.append(END_MESSAGE);
				}
			}
		} else if(valuesNodeOutput.isObject()){
			JsonNode dataField = valuesNodeOutput.get(ERROR_MESSAGES);
			if(null != dataField.get(FIELD_NAME)){
				errorMessage.append(dataField.get(FIELD_NAME).asText().trim());
				errorMessage.append(END_FIELDNAME);
			}
			if(null != dataField.get(MESSAGE)){
				errorMessage.append(dataField.get(MESSAGE).asText().trim());	
				errorMessage.append(END_MESSAGE);
			}
		}
		return errorMessage.toString();
	}
	
	public String setDateFormat(String fileDate){
		String regularDate = StringUtils.SPACE;
		if(null != fileDate){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			Date date;
			try {
				date = sdf.parse(fileDate);
				sdf.applyPattern("yyyyMMdd");
				regularDate = sdf.format(date);
			} catch (ParseException e) {
				return StringUtils.SPACE;	
			}
		}	
		return regularDate;
	}
	public String changeDateFormat(String fileDate){
		String regularDate = StringUtils.SPACE;
		if(null != fileDate){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date date;
			try {
				date = sdf.parse(fileDate);
				sdf.applyPattern("yyyyMMdd");
				regularDate = sdf.format(date);
			} catch (ParseException e) {
				return StringUtils.SPACE;
			}
		}	
		return regularDate;
	}
	public String setWaiverOfPrem(List<String> baseContractAndRiderCode){

		if(baseContractAndRiderCode!=null && !baseContractAndRiderCode.isEmpty()){
			for(String covritem : baseContractAndRiderCode){
				if(covritem!=null && !covritem.isEmpty()){
					List<Itempf> tr517List = itempfService.readTR517(covritem);
					if (tr517List != null && !tr517List.isEmpty()) {
						return "Y";	
					}
				}
			}
		}
		return "N";

	}
	
	@Override
	public String toString() {
		return "L2CreateContractProcessorUtilImpl []";
	}
	
}
