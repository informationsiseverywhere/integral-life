package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO for L2ANENDRLX processor.
 * 
 * @author mmalik25
 *
 */
public class L2ownchgProcessorDTO {

	private String chdrnum;
	private String chdrcoy;
	private String currto;
	private int tranno;
	private String tranid;
	private String cownnum;
	private long uniqueNumber;
	private String chdrpfx;
	private String recode;
	private Integer ptrneff;
	private Integer trdt;
	private Integer trtm;
	private String termid;
	private Integer userT;
	private String batcpfx;
	private String batccoy;
	private String batcbrn;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batctrcde;
	private String batcbatch;
	private String prtflg;
	private String validflagforInerst;
	private String validflagforUpdate;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private Integer datesub;
	private String crtuser;
	private boolean acmvRtrnFlag;
	private String Procflg;
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getCurrto() {
		return currto;
	}
	public void setCurrto(String currto) {
		this.currto = currto;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getTranid() {
		return tranid;
	}
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getRecode() {
		return recode;
	}
	public void setRecode(String recode) {
		this.recode = recode;
	}
	public Integer getPtrneff() {
		return ptrneff;
	}
	public void setPtrneff(Integer ptrneff) {
		this.ptrneff = ptrneff;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getPrtflg() {
		return prtflg;
	}
	public void setPrtflg(String prtflg) {
		this.prtflg = prtflg;
	}

	public String getValidflagforInerst() {
		return validflagforInerst;
	}
	public void setValidflagforInerst(String validflagforInerst) {
		this.validflagforInerst = validflagforInerst;
	}
	public String getValidflagforUpdate() {
		return validflagforUpdate;
	}
	public void setValidflagforUpdate(String validflagforUpdate) {
		this.validflagforUpdate = validflagforUpdate;
	}
	public String getProcflg() {
		return Procflg;
	}
	public void setProcflg(String procflg) {
		Procflg = procflg;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}
	public Integer getDatesub() {
		return datesub;
	}
	public void setDatesub(Integer datesub) {
		this.datesub = datesub;
	}
	public String getCrtuser() {
		return crtuser;
	}
	public void setCrtuser(String crtuser) {
		this.crtuser = crtuser;
	}
	public boolean isAcmvRtrnFlag() {
		return acmvRtrnFlag;
	}
	public void setAcmvRtrnFlag(boolean acmvRtrnFlag) {
		this.acmvRtrnFlag = acmvRtrnFlag;
	}

}
