package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.L2BillingProcessorDTO;
import com.dxc.integral.life.beans.L2BillingReaderDTO;

public interface L2billingProcessorUtil {
	public boolean validContract(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO);
	public boolean validateFlexibleContract(L2BillingReaderDTO readerDTO);
	public int readLeadDays(L2BillingReaderDTO readerDTO, L2BillingProcessorDTO l2BillingProcessorDTO);
	public void loadSmartTables(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO);
	public void updateRecord(L2BillingProcessorDTO l2BillingProcessorDTO, L2BillingReaderDTO readerDTO);
}
