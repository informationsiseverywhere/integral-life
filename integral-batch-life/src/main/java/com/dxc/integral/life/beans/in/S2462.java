package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2462 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String flagind;
	private String optind01;
	private String optind02;
	private String optind03;
	private String optind04;
	private String optind05;
	private String optind06;
	private String optind07;
	private String optind08;
	private String optind09;
	private String optind10;
	private String activeField;
	private S2462screensfl s2462screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}
	public String getFlagind() {
		return flagind;
	}
	public void setFlagind(String flagind) {
		this.flagind = flagind;
	}
	public String getOptind01() {
		return optind01;
	}
	public void setOptind01(String optind01) {
		this.optind01 = optind01;
	}
	public String getOptind02() {
		return optind02;
	}
	public void setOptind02(String optind02) {
		this.optind02 = optind02;
	}
	public String getOptind03() {
		return optind03;
	}
	public void setOptind03(String optind03) {
		this.optind03 = optind03;
	}
	public String getOptind04() {
		return optind04;
	}
	public void setOptind04(String optind04) {
		this.optind04 = optind04;
	}
	public String getOptind05() {
		return optind05;
	}
	public void setOptind05(String optind05) {
		this.optind05 = optind05;
	}
	public String getOptind06() {
		return optind06;
	}
	public void setOptind06(String optind06) {
		this.optind06 = optind06;
	}
	public String getOptind07() {
		return optind07;
	}
	public void setOptind07(String optind07) {
		this.optind07 = optind07;
	}
	public String getOptind08() {
		return optind08;
	}
	public void setOptind08(String optind08) {
		this.optind08 = optind08;
	}
	public String getOptind09() {
		return optind09;
	}
	public void setOptind09(String optind09) {
		this.optind09 = optind09;
	}
	public String getOptind10() {
		return optind10;
	}
	public void setOptind10(String optind10) {
		this.optind10 = optind10;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S2462screensfl getS2462screensfl() {
		return s2462screensfl;
	}
	public void setS2462screensfl(S2462screensfl s2462screensfl) {
		this.s2462screensfl = s2462screensfl;
	}


	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class S2462screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;
		
		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}

		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String select;
			private String tranno;
			private String datesubDisp;
			private String time;
			private String crtuser;
			private String general;
			private String shistory;
			private String sretention;
			private String bankruptcy;
			private String additional;
			private String amlprofiling;
			private String crs;
			private String vno;
			private String accessor;
			private String solicitor;
			private String repairer;
			private String flagind1;
			private String lsmc;

			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
			public String getTranno() {
				return tranno;
			}
			public void setTranno(String tranno) {
				this.tranno = tranno;
			}
			public String getDatesubDisp() {
				return datesubDisp;
			}
			public void setDatesubDisp(String datesubDisp) {
				this.datesubDisp = datesubDisp;
			}
			public String getTime() {
				return time;
			}
			public void setTime(String time) {
				this.time = time;
			}
			public String getCrtuser() {
				return crtuser;
			}
			public void setCrtuser(String crtuser) {
				this.crtuser = crtuser;
			}
			public String getGeneral() {
				return general;
			}
			public void setGeneral(String general) {
				this.general = general;
			}
			public String getShistory() {
				return shistory;
			}
			public void setShistory(String shistory) {
				this.shistory = shistory;
			}
			public String getSretention() {
				return sretention;
			}
			public void setSretention(String sretention) {
				this.sretention = sretention;
			}
			public String getBankruptcy() {
				return bankruptcy;
			}
			public void setBankruptcy(String bankruptcy) {
				this.bankruptcy = bankruptcy;
			}
			public String getAdditional() {
				return additional;
			}
			public void setAdditional(String additional) {
				this.additional = additional;
			}
			public String getAmlprofiling() {
				return amlprofiling;
			}
			public void setAmlprofiling(String amlprofiling) {
				this.amlprofiling = amlprofiling;
			}
			public String getCrs() {
				return crs;
			}
			public void setCrs(String crs) {
				this.crs = crs;
			}
			public String getVno() {
				return vno;
			}
			public void setVno(String vno) {
				this.vno = vno;
			}
			public String getAccessor() {
				return accessor;
			}
			public void setAccessor(String accessor) {
				this.accessor = accessor;
			}
			public String getSolicitor() {
				return solicitor;
			}
			public void setSolicitor(String solicitor) {
				this.solicitor = solicitor;
			}
			public String getRepairer() {
				return repairer;
			}
			public void setRepairer(String repairer) {
				this.repairer = repairer;
			}
			public String getFlagind1() {
				return flagind1;
			}
			public void setFlagind1(String flagind1) {
				this.flagind1 = flagind1;
			}
			public String getLsmc() {
				return lsmc;
			}
			public void setLsmc(String lsmc) {
				this.lsmc = lsmc;
			}
		} 
	}

	@Override
	public String toString() {
		return "S2462 []";
	}
	
}
