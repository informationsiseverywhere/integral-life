package com.dxc.integral.life.beans.servicebean;

import com.dxc.integral.life.beans.in.CheckInfoClientInput;
import java.io.Serializable;
import com.dxc.integral.life.beans.in.AuthenticationInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeName(value = "L2ContractCreateGroupedServiceInputBean") 
@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class L2ContractCreateGroupedServiceInputBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private AuthenticationInfo authenticationInfo;
	private CheckInfoClientInput checkInfoClientInput;
	@JsonProperty("LCreateNewContractIdvServiceInputBean")
	private LcreatenewcontractidvServiceInputBean lcreateNewContractIdvServiceInputBean;
	
	public L2ContractCreateGroupedServiceInputBean() {
		//does nothing
	}
	
	/**
	 * @return the authenticationInfo
	 */
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}
	/**
	 * @param authenticationInfo the authenticationInfo to set
	 */
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}
	/**
	 * @return the checkInfoClientInput
	 */
	public CheckInfoClientInput getCheckInfoClientInput() {
		return checkInfoClientInput;
	}
	/**
	 * @param checkInfoClientInput the checkInfoClientInput to set
	 */
	public void setCheckInfoClientInput(CheckInfoClientInput checkInfoClientInput) {
		this.checkInfoClientInput = checkInfoClientInput;
	}
	/**
	 * @return the lcreateNewContractIdvServiceInputBean
	 */
	public LcreatenewcontractidvServiceInputBean getLcreateNewContractIdvServiceInputBean() {
		return lcreateNewContractIdvServiceInputBean;
	}
	/**
	 * @param lcreateNewContractIdvServiceInputBean the lcreateNewContractIdvServiceInputBean to set
	 */
	public void setLcreateNewContractIdvServiceInputBean(
			LcreatenewcontractidvServiceInputBean lcreateNewContractIdvServiceInputBean) {
		this.lcreateNewContractIdvServiceInputBean = lcreateNewContractIdvServiceInputBean;
	}
}
