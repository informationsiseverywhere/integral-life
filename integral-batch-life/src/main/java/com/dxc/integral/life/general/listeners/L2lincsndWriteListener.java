package com.dxc.integral.life.general.listeners;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.file.FlatFileItemWriter;
import com.dxc.integral.life.beans.L2lincsndReaderDTO;
import com.dxc.integral.life.config.L2lincsndJsonMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.dxc.integral.life.utils.L2lincsndCommonsUtil;

/**
 * Exports records from Database to external flat file.
 * 
 * @author gsaluja2
 *
 */

public class L2lincsndWriteListener implements ItemWriteListener<L2lincsndReaderDTO> {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2lincsndWriteListener.class);
	
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	    
    @Autowired
    private L2lincsndCommonsUtil l2lincsndCommonsUtil;
    	
	@Value("#{stepExecutionContext['jsonData']}")
	private L2lincsndJsonMapping jsonData;
	
	@Value("#{stepExecutionContext['resetTranscds']}")
	private List<String> resetTranscdList;
    
    public L2lincsndWriteListener() {
    	//does nothing
    }
    
	@Override
	public void beforeWrite(List<? extends L2lincsndReaderDTO> readerDTOList) {
		LOGGER.info("Export Data Before write begins.");
		try {
			setFlatFileParams(readerDTOList);
		}
		catch(Exception e) {
			LOGGER.info("{0}", e);
		}
	}

	@Override
	public void afterWrite(List<? extends L2lincsndReaderDTO> items) {
		//does nothing
	}

	@Override
	public void onWriteError(Exception exception, List<? extends L2lincsndReaderDTO> items) {
		//does nothing
	}
	
	/**
	 * Filters data based on LINC record type.
	 * Generates a map based on LINC record type
	 * to segregate data for each FlatFileItemWriter 
	 * object.
	 * 
	 * @param readerDTOList
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void setFlatFileParams(List<? extends L2lincsndReaderDTO> readerDTOList) throws Exception {
		Map<String, List<L2lincsndReaderDTO>> readerDTOListMap;
		if(null != readerDTOList && !readerDTOList.isEmpty())
			readerDTOListMap = l2lincsndCommonsUtil.convertListToMap(readerDTOList, jsonData.getFile(), resetTranscdList);
		else
			return;
	    try {
		    for(Map.Entry<String, List<L2lincsndReaderDTO>> entry: readerDTOListMap.entrySet()) {
		    	if(entry.getValue()!=null && !entry.getValue().isEmpty() 
		    			&& stepExecution.getExecutionContext().get(entry.getKey())!=null) {
		    		flatFileItemWriter(entry.getValue(), 
		    					(FlatFileItemWriter<L2lincsndReaderDTO>) stepExecution.getExecutionContext().get(entry.getKey()));
		    	}
		    }
	    }
	    catch(Exception e) {
	    	LOGGER.info("Caught Exception while setting file params {0}.", e);
	    }
	}
    
    /**
     * Writes data to Flat file.
     * 
     * @param readerDTOList
     * @param flatFileObject
     * @throws Exception
     */
    public void flatFileItemWriter(List<? extends L2lincsndReaderDTO> readerDTOList, 
    		FlatFileItemWriter<L2lincsndReaderDTO> flatFileObject) throws Exception{
	   try {
		   LOGGER.info("Export Data Writing to file.");
		   if(flatFileObject!=null) {
			   flatFileObject.write(readerDTOList);
		   }
	   }
	   catch(Exception e) {
	      	LOGGER.info("Caught Exception while writing to file ", e);
	   }
    }
}
