package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.life.dao.model.Fpcopf;
import com.dxc.integral.life.dao.model.Fprmpf;
import com.dxc.integral.life.dao.model.Hdivpf;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T6654;
import com.dxc.integral.life.smarttable.pojo.Tr52d;
import com.dxc.integral.life.smarttable.pojo.Tr52e;

/**
 * DTO for L2BillingProcessor
 * 
 * @author yyang21
 *
 */
public class L2BillingProcessorDTO {

	private L2BillingControlTotalDTO l2BillingControlTotalDTO;
	private int effDate;
	int effdatePlusCntlead;
	private String lang;
	private T6654 t6654IO;
	private T5645 t5645IO;
	private T3695 t3695IO;
	private Tr52d tr52dIO;
	private T5679 t5679IO;
	private Tr52e tr52eIO;
	private int leadDays;
	private String bprdAuthCode;
	private String batchTrcde;
	private String prefix;
	private int actyear;
	private int actmonth;
	private String batch;
	private String branch;
	private Payrpf payrpf;
	private Chdrpf chdrpf;
	private Payrpf currPayrLif;
	private int oldBillcd = 0;
	private int oldBtdate = 0;
	private int oldNextdate = 0;
	private BigDecimal totalAmount = BigDecimal.ZERO;
	private BigDecimal oldOutstamt = BigDecimal.ZERO;

	private BigDecimal suspAvail = BigDecimal.ZERO;
	private BigDecimal premSusp = BigDecimal.ZERO;
	private BigDecimal dvdSusp = BigDecimal.ZERO;
	private BigDecimal increaseDue = BigDecimal.ZERO;
	private BigDecimal tax = BigDecimal.ZERO;
	private BigDecimal tfrAmt = BigDecimal.ZERO;
	private BigDecimal allDvdTot = BigDecimal.ZERO;
	private BigDecimal runDvdTot = BigDecimal.ZERO;

	private int cashdate = 0;
	private int billcd = 0;

	private String gotPayrAtBtdate = null;
	private int jrnseq;

	private String firstBill = "Y";
	boolean flexiblePremiumContract = false;
	private HdisArrayInner hdisArrayInner = new HdisArrayInner();

	private ConlinkOutputDTO conlinkOutputDTO = new ConlinkOutputDTO();
	private ConlinkInputDTO conlinkDTO = new ConlinkInputDTO();

	private BillreqDTO billreqDTO = new BillreqDTO();
	private PrasDTO prasDTO = new PrasDTO();
	private String userName;
	private String batchName;
	private Usrdpf usrdpf = null;
	private Batcpf batchDetail = null;
	private List<BigDecimal> taxAmts = new ArrayList<>();
	private List<String> taxTyps = new ArrayList<>();
	private List<String> taxAbsorbs = new ArrayList<>();
	private List<Taxdpf> taxdpfs = new ArrayList<>();
	private List<Linspf> linspfList = new ArrayList<>();
	private List<Ptrnpf> ptrnpfList = new ArrayList<>();
	private List<Hdivpf> hdivpfList = new ArrayList<>();
	private List<Fpcopf> fpcopfList = new ArrayList<>();
	private List<Fprmpf> fprmpfList = new ArrayList<>();

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public int getEffdatePlusCntlead() {
		return effdatePlusCntlead;
	}

	public void setEffdatePlusCntlead(int effdatePlusCntlead) {
		this.effdatePlusCntlead = effdatePlusCntlead;
	}

	public BigDecimal getOldOutstamt() {
		return oldOutstamt;
	}

	public void setOldOutstamt(BigDecimal oldOutstamt) {
		this.oldOutstamt = oldOutstamt;
	}

	public BigDecimal getSuspAvail() {
		return suspAvail;
	}

	public void setSuspAvail(BigDecimal suspAvail) {
		this.suspAvail = suspAvail;
	}

	public BigDecimal getPremSusp() {
		return premSusp;
	}

	public void setPremSusp(BigDecimal premSusp) {
		this.premSusp = premSusp;
	}

	public BigDecimal getDvdSusp() {
		return dvdSusp;
	}

	public void setDvdSusp(BigDecimal dvdSusp) {
		this.dvdSusp = dvdSusp;
	}

	public BigDecimal getIncreaseDue() {
		return increaseDue;
	}

	public void setIncreaseDue(BigDecimal increaseDue) {
		this.increaseDue = increaseDue;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getTfrAmt() {
		return tfrAmt;
	}

	public void setTfrAmt(BigDecimal tfrAmt) {
		this.tfrAmt = tfrAmt;
	}

	public BigDecimal getAllDvdTot() {
		return allDvdTot;
	}

	public void setAllDvdTot(BigDecimal allDvdTot) {
		this.allDvdTot = allDvdTot;
	}

	public BigDecimal getRunDvdTot() {
		return runDvdTot;
	}

	public void setRunDvdTot(BigDecimal runDvdTot) {
		this.runDvdTot = runDvdTot;
	}

	public int getCashdate() {
		return cashdate;
	}

	public void setCashdate(int cashdate) {
		this.cashdate = cashdate;
	}

	public String getGotPayrAtBtdate() {
		return gotPayrAtBtdate;
	}

	public void setGotPayrAtBtdate(String gotPayrAtBtdate) {
		this.gotPayrAtBtdate = gotPayrAtBtdate;
	}

	public int getJrnseq() {
		return jrnseq;
	}

	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}

	public String getFirstBill() {
		return firstBill;
	}

	public void setFirstBill(String firstBill) {
		this.firstBill = firstBill;
	}

	public HdisArrayInner getHdisArrayInner() {
		return hdisArrayInner;
	}

	public void setHdisArrayInner(HdisArrayInner hdisArrayInner) {
		this.hdisArrayInner = hdisArrayInner;
	}

	public int getLeadDays() {
		return leadDays;
	}

	public void setLeadDays(int leadDays) {
		this.leadDays = leadDays;
	}

	public L2BillingControlTotalDTO getL2BillingControlTotalDTO() {
		return l2BillingControlTotalDTO;
	}

	public void setL2BillingControlTotalDTO(L2BillingControlTotalDTO l2BillingControlTotalDTO) {
		this.l2BillingControlTotalDTO = l2BillingControlTotalDTO;
	}

	public int getEffDate() {
		return effDate;
	}

	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public T6654 getT6654IO() {
		return t6654IO;
	}

	public void setT6654IO(T6654 t6654io) {
		t6654IO = t6654io;
	}

	public T5645 getT5645IO() {
		return t5645IO;
	}

	public void setT5645IO(T5645 t5645io) {
		t5645IO = t5645io;
	}

	public T3695 getT3695IO() {
		return t3695IO;
	}

	public void setT3695IO(T3695 t3695io) {
		t3695IO = t3695io;
	}

	public Tr52d getTr52dIO() {
		return tr52dIO;
	}

	public void setTr52dIO(Tr52d tr52dIO) {
		this.tr52dIO = tr52dIO;
	}

	public T5679 getT5679IO() {
		return t5679IO;
	}

	public void setT5679IO(T5679 t5679io) {
		t5679IO = t5679io;
	}

	public Tr52e getTr52eIO() {
		return tr52eIO;
	}

	public void setTr52eIO(Tr52e tr52eIO) {
		this.tr52eIO = tr52eIO;
	}

	public String getBprdAuthCode() {
		return bprdAuthCode;
	}

	public void setBprdAuthCode(String bprdAuthCode) {
		this.bprdAuthCode = bprdAuthCode;
	}

	public String getBatchTrcde() {
		return batchTrcde;
	}

	public void setBatchTrcde(String batchTrcde) {
		this.batchTrcde = batchTrcde;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public int getActyear() {
		return actyear;
	}

	public void setActyear(int actyear) {
		this.actyear = actyear;
	}

	public int getActmonth() {
		return actmonth;
	}

	public void setActmonth(int actmonth) {
		this.actmonth = actmonth;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Payrpf getPayrpf() {
		return payrpf;
	}

	public void setPayrpf(Payrpf payrpf) {
		this.payrpf = payrpf;
	}

	public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}

	public Payrpf getCurrPayrLif() {
		return currPayrLif;
	}

	public void setCurrPayrLif(Payrpf currPayrLif) {
		this.currPayrLif = currPayrLif;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getBillcd() {
		return billcd;
	}

	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}

	public boolean isFlexiblePremiumContract() {
		return flexiblePremiumContract;
	}

	public void setFlexiblePremiumContract(boolean flexiblePremiumContract) {
		this.flexiblePremiumContract = flexiblePremiumContract;
	}

	public ConlinkOutputDTO getConlinkOutputDTO() {
		return conlinkOutputDTO;
	}

	public void setConlinkOutputDTO(ConlinkOutputDTO conlinkOutputDTO) {
		this.conlinkOutputDTO = conlinkOutputDTO;
	}

	public ConlinkInputDTO getConlinkDTO() {
		return conlinkDTO;
	}

	public void setConlinkDTO(ConlinkInputDTO conlinkDTO) {
		this.conlinkDTO = conlinkDTO;
	}

	public BillreqDTO getBillreqDTO() {
		return billreqDTO;
	}

	public void setBillreqDTO(BillreqDTO billreqDTO) {
		this.billreqDTO = billreqDTO;
	}

	public PrasDTO getPrasDTO() {
		return prasDTO;
	}

	public void setPrasDTO(PrasDTO prasDTO) {
		this.prasDTO = prasDTO;
	}

	public Usrdpf getUsrdpf() {
		return usrdpf;
	}

	public void setUsrdpf(Usrdpf usrdpf) {
		this.usrdpf = usrdpf;
	}

	public Batcpf getBatchDetail() {
		return batchDetail;
	}

	public void setBatchDetail(Batcpf batchDetail) {
		this.batchDetail = batchDetail;
	}

	public List<BigDecimal> getTaxAmts() {
		return taxAmts;
	}

	public void setTaxAmts(List<BigDecimal> taxAmts) {
		this.taxAmts = taxAmts;
	}

	public List<String> getTaxTyps() {
		return taxTyps;
	}

	public void setTaxTyps(List<String> taxTyps) {
		this.taxTyps = taxTyps;
	}

	public List<String> getTaxAbsorbs() {
		return taxAbsorbs;
	}

	public void setTaxAbsorbs(List<String> taxAbsorbs) {
		this.taxAbsorbs = taxAbsorbs;
	}

	public List<Taxdpf> getTaxdpfs() {
		return taxdpfs;
	}

	public void setTaxdpfs(List<Taxdpf> taxdpfs) {
		this.taxdpfs = taxdpfs;
	}

	public List<Linspf> getLinspfList() {
		return linspfList;
	}

	public void setLinspfList(List<Linspf> linspfList) {
		this.linspfList = linspfList;
	}

	public List<Ptrnpf> getPtrnpfList() {
		return ptrnpfList;
	}

	public void setPtrnpfList(List<Ptrnpf> ptrnpfList) {
		this.ptrnpfList = ptrnpfList;
	}

	public List<Hdivpf> getHdivpfList() {
		return hdivpfList;
	}

	public void setHdivpfList(List<Hdivpf> hdivpfList) {
		this.hdivpfList = hdivpfList;
	}

	public List<Fpcopf> getFpcopfList() {
		return fpcopfList;
	}

	public void setFpcopfList(List<Fpcopf> fpcopfList) {
		this.fpcopfList = fpcopfList;
	}

	public List<Fprmpf> getFprmpfList() {
		return fprmpfList;
	}

	public void setFprmpfList(List<Fprmpf> fprmpfList) {
		this.fprmpfList = fprmpfList;
	}

	public int getOldBillcd() {
		return oldBillcd;
	}

	public void setOldBillcd(int oldBillcd) {
		this.oldBillcd = oldBillcd;
	}

	public int getOldBtdate() {
		return oldBtdate;
	}

	public void setOldBtdate(int oldBtdate) {
		this.oldBtdate = oldBtdate;
	}

	public int getOldNextdate() {
		return oldNextdate;
	}

	public void setOldNextdate(int oldNextdate) {
		this.oldNextdate = oldNextdate;
	}
}
