package com.dxc.integral.life.beans.out;

import java.util.List;

public class L2ProposalsCreatedBean {

	String proposalNumber;
	String contractNumber;
	String contractType;
	String contractOwner;
	String newOrExistingOwner;
	String contractOwnerNameKana;
	String contractOwnerNameKanji;
	String contractCurrency;
	String contractDate;
	String billingFrequency;
	String methodOfPayment;
	String agentNumber;
	String agencyNumber;
	String lifeAssured;
	String neworExistingLA; 
	String lifeAssuredNameKana;
	String lifeAssuredNameKanji;
	List<String> beneficiary;
	List<String> newOrExistingBenef;
	List<String> beneficiaryNameKana;
	List<String> beneficiaryNameKanji;
	List<String> guardian;
	List<String> newOrExistingGuard;
	List<String> guardianNameKana;
	List<String> guardianNameKanji;
	String effectiveDateofBatchRun;
	
	L2CreateContractExceptionsBean l2CreateContractExceptionsBean;
	
	public String getProposalNumber() {
		return proposalNumber;
	}
	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getContractOwner() {
		return contractOwner;
	}
	public void setContractOwner(String contractOwner) {
		this.contractOwner = contractOwner;
	}
	public String getNewOrExistingOwner() {
		return newOrExistingOwner;
	}
	public void setNewOrExistingOwner(String newOrExistingOwner) {
		this.newOrExistingOwner = newOrExistingOwner;
	}
	public String getContractOwnerNameKana() {
		return contractOwnerNameKana;
	}
	public void setContractOwnerNameKana(String contractOwnerNameKana) {
		this.contractOwnerNameKana = contractOwnerNameKana;
	}
	public String getContractOwnerNameKanji() {
		return contractOwnerNameKanji;
	}
	public void setContractOwnerNameKanji(String contractOwnerNameKanji) {
		this.contractOwnerNameKanji = contractOwnerNameKanji;
	}
	public String getContractCurrency() {
		return contractCurrency;
	}
	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	public String getContractDate() {
		return contractDate;
	}
	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}
	public String getBillingFrequency() {
		return billingFrequency;
	}
	public void setBillingFrequency(String billingFrequency) {
		this.billingFrequency = billingFrequency;
	}
	public String getMethodOfPayment() {
		return methodOfPayment;
	}
	public void setMethodOfPayment(String methodOfPayment) {
		this.methodOfPayment = methodOfPayment;
	}
	public String getAgentNumber() {
		return agentNumber;
	}
	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}
	public String getAgencyNumber() {
		return agencyNumber;
	}
	public void setAgencyNumber(String agencyNumber) {
		this.agencyNumber = agencyNumber;
	}
	public String getLifeAssured() {
		return lifeAssured;
	}
	public void setLifeAssured(String lifeAssured) {
		this.lifeAssured = lifeAssured;
	}
	public String getNeworExistingLA() {
		return neworExistingLA;
	}
	public void setNeworExistingLA(String neworExistingLA) {
		this.neworExistingLA = neworExistingLA;
	}
	public String getLifeAssuredNameKana() {
		return lifeAssuredNameKana;
	}
	public void setLifeAssuredNameKana(String lifeAssuredNameKana) {
		this.lifeAssuredNameKana = lifeAssuredNameKana;
	}
	public String getLifeAssuredNameKanji() {
		return lifeAssuredNameKanji;
	}
	public void setLifeAssuredNameKanji(String lifeAssuredNameKanji) {
		this.lifeAssuredNameKanji = lifeAssuredNameKanji;
	}
	public List<String> getBeneficiary() {
		return beneficiary;
	}
	public void setBeneficiary(List<String> beneficiary) {
		this.beneficiary = beneficiary;
	}
	public List<String> getNewOrExistingBenef() {
		return newOrExistingBenef;
	}
	public void setNewOrExistingBenef(List<String> newOrExistingBenef) {
		this.newOrExistingBenef = newOrExistingBenef;
	}
	public List<String> getBeneficiaryNameKana() {
		return beneficiaryNameKana;
	}
	public void setBeneficiaryNameKana(List<String> beneficiaryNameKana) {
		this.beneficiaryNameKana = beneficiaryNameKana;
	}
	public List<String> getBeneficiaryNameKanji() {
		return beneficiaryNameKanji;
	}
	public void setBeneficiaryNameKanji(List<String> beneficiaryNameKanji) {
		this.beneficiaryNameKanji = beneficiaryNameKanji;
	}
	public List<String> getGuardian() {
		return guardian;
	}
	public void setGuardian(List<String> guardian) {
		this.guardian = guardian;
	}
	public List<String> getNewOrExistingGuard() {
		return newOrExistingGuard;
	}
	public void setNewOrExistingGuard(List<String> newOrExistingGuard) {
		this.newOrExistingGuard = newOrExistingGuard;
	}
	public List<String> getGuardianNameKana() {
		return guardianNameKana;
	}
	public void setGuardianNameKana(List<String> guardianNameKana) {
		this.guardianNameKana = guardianNameKana;
	}
	public List<String> getGuardianNameKanji() {
		return guardianNameKanji;
	}
	public void setGuardianNameKanji(List<String> guardianNameKanji) {
		this.guardianNameKanji = guardianNameKanji;
	}
	public String getEffectiveDateofBatchRun() {
		return effectiveDateofBatchRun;
	}
	public void setEffectiveDateofBatchRun(String effectiveDateofBatchRun) {
		this.effectiveDateofBatchRun = effectiveDateofBatchRun;
	}
	public L2CreateContractExceptionsBean getL2CreateContractExceptionsBean() {
		return l2CreateContractExceptionsBean;
	}
	public void setL2CreateContractExceptionsBean(L2CreateContractExceptionsBean l2CreateContractExceptionsBean) {
		this.l2CreateContractExceptionsBean = l2CreateContractExceptionsBean;
	}
	@Override
	public String toString() {
		return "L2ProposalsCreatedBean []";
	}
	
}
