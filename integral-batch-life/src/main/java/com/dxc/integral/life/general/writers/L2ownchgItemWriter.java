package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2anendrlxReaderDTO;
import com.dxc.integral.life.beans.L2ownchgReaderDTO;

@Async
public class L2ownchgItemWriter implements ItemWriter<L2ownchgReaderDTO>{

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2ownchgItemWriter.class);

	@Autowired
	private Sftlock sftlock;

	@Autowired
	private Batcup batcup = null;

	/** The batch transaction code for L2OWNCHG */
	public static final String BATCH_TRAN_CODE = "BR6E";

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	private int tranCount = 0;
	private ItemWriter<L2ownchgReaderDTO> chdrpfInsertWriter;
	private ItemWriter<L2ownchgReaderDTO> chdrpfUpdateWriter;
	private ItemWriter<L2ownchgReaderDTO> ptrnpfWriter;
	private ItemWriter<L2ownchgReaderDTO> hpadpfUpdateWriter;    

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	@Override
	public void write(List<? extends L2ownchgReaderDTO> readerDTOList) throws Exception {

		LOGGER.info("L2ownchgItemWriter starts.");
		chdrpfUpdateWriter.write(readerDTOList);
		chdrpfInsertWriter.write(readerDTOList);
		hpadpfUpdateWriter.write(readerDTOList);
		ptrnpfWriter.write(readerDTOList); 
		synchronized (L2ownchgItemWriter.class) {
			processBatcpf(readerDTOList);
		}
		if (!readerDTOList.isEmpty()) {
			if (0 >= readerDTOList.get(0).getChdrnum()
					.compareToIgnoreCase(readerDTOList.get(readerDTOList.size() - 1).getChdrnum())) {
				sftlock.unlockByRange("CH", readerDTOList.get(0).getChdrcoy(), readerDTOList.get(0).getChdrnum(),
						readerDTOList.get(readerDTOList.size() - 1).getChdrnum());
			} else {
				sftlock.unlockByRange("CH", readerDTOList.get(0).getChdrcoy(),
						readerDTOList.get(readerDTOList.size() - 1).getChdrnum(), readerDTOList.get(0).getChdrnum());

			}
		}	

		LOGGER.info("L2ownchgItemWriter ends.");
	}

	public void setChdrpfInsertWriter(ItemWriter<L2ownchgReaderDTO> chdrpfInsertWriter) {
		this.chdrpfInsertWriter = chdrpfInsertWriter;
	}
	public void setChdrpfUpdateWriter(ItemWriter<L2ownchgReaderDTO> chdrpfUpdateWriter) {
		this.chdrpfUpdateWriter = chdrpfUpdateWriter;
	}

	public void setPtrnpfWriter(ItemWriter<L2ownchgReaderDTO> ptrnpfWriter) {
		this.ptrnpfWriter = ptrnpfWriter;
	}

	public void setHpadpfUpdateWriter(ItemWriter<L2ownchgReaderDTO> hpadpfUpdateWriter) {
		this.hpadpfUpdateWriter = hpadpfUpdateWriter;
	}


	/**
	 * Process batcpf information
	 * Checks if a record is present in Batcpf table based on the input parameters , 
	 * if the record is not present, then inserts the record 
	 * finally updates the tranCnt of batcpf record based on the input parameter tranCount
	 * @param readerDTOList
	 */
	private void processBatcpf(List<? extends L2ownchgReaderDTO> readerDTOList) throws ParseException {

		if (!readerDTOList.isEmpty()) {
			L2ownchgReaderDTO readerDTO = readerDTOList.get(0);
			tranCount = batcup.processBatcpf(String.valueOf(readerDTO.getBatcactyr()),
					String.valueOf(readerDTO.getBatcactmn()), businessDate, readerDTO.getChdrcoy(),
					readerDTO.getBatctrcde(), readerDTO.getBatcbrn(), batchName, readerDTO.getBatcbatch(),
					readerDTO.getUsrprf(), String.valueOf(readerDTO.getUserNum()), readerDTOList.size(), tranCount);
			LOGGER.info("tranCount:{}", tranCount);//IJTI-1498
		}

	}

}
