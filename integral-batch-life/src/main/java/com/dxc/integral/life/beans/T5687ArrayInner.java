package com.dxc.integral.life.beans;

import java.util.ArrayList;
import java.util.List;

public class T5687ArrayInner {
	private List<Integer> wsaaT5687Currfrom = new ArrayList<>();
	private List<String> wsaaT5687Key = new ArrayList<>();
	private List<String> wsaaT5687Crtable = new ArrayList<>();
	private List<String> wsaaT5687Data = new ArrayList<>();
	private List<String> wsaaT5687Annmthd = new ArrayList<>();
	private List<String> wsaaT5687Bcmthd = new ArrayList<>();
	private List<String> wsaaT5687Bascpy = new ArrayList<>();
	private List<String> wsaaT5687Rnwcpy = new ArrayList<>();
	private List<String> wsaaT5687Srvcpy = new ArrayList<>();
	public List<Integer> getWsaaT5687Currfrom() {
		return wsaaT5687Currfrom;
	}
	public void setWsaaT5687Currfrom(List<Integer> wsaaT5687Currfrom) {
		this.wsaaT5687Currfrom = wsaaT5687Currfrom;
	}
	public List<String> getWsaaT5687Key() {
		return wsaaT5687Key;
	}
	public void setWsaaT5687Key(List<String> wsaaT5687Key) {
		this.wsaaT5687Key = wsaaT5687Key;
	}
	public List<String> getWsaaT5687Crtable() {
		return wsaaT5687Crtable;
	}
	public void setWsaaT5687Crtable(List<String> wsaaT5687Crtable) {
		this.wsaaT5687Crtable = wsaaT5687Crtable;
	}
	public List<String> getWsaaT5687Data() {
		return wsaaT5687Data;
	}
	public void setWsaaT5687Data(List<String> wsaaT5687Data) {
		this.wsaaT5687Data = wsaaT5687Data;
	}
	public List<String> getWsaaT5687Annmthd() {
		return wsaaT5687Annmthd;
	}
	public void setWsaaT5687Annmthd(List<String> wsaaT5687Annmthd) {
		this.wsaaT5687Annmthd = wsaaT5687Annmthd;
	}
	public List<String> getWsaaT5687Bcmthd() {
		return wsaaT5687Bcmthd;
	}
	public void setWsaaT5687Bcmthd(List<String> wsaaT5687Bcmthd) {
		this.wsaaT5687Bcmthd = wsaaT5687Bcmthd;
	}
	public List<String> getWsaaT5687Bascpy() {
		return wsaaT5687Bascpy;
	}
	public void setWsaaT5687Bascpy(List<String> wsaaT5687Bascpy) {
		this.wsaaT5687Bascpy = wsaaT5687Bascpy;
	}
	public List<String> getWsaaT5687Rnwcpy() {
		return wsaaT5687Rnwcpy;
	}
	public void setWsaaT5687Rnwcpy(List<String> wsaaT5687Rnwcpy) {
		this.wsaaT5687Rnwcpy = wsaaT5687Rnwcpy;
	}
	public List<String> getWsaaT5687Srvcpy() {
		return wsaaT5687Srvcpy;
	}
	public void setWsaaT5687Srvcpy(List<String> wsaaT5687Srvcpy) {
		this.wsaaT5687Srvcpy = wsaaT5687Srvcpy;
	}
}
