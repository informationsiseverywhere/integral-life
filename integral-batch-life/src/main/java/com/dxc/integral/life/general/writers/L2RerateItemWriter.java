package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2RerateReaderDTO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

/**
 * ItemWriter for L2RERATE batch step.
 * 
 * @author wli31
 *
 */
@Async
public class L2RerateItemWriter implements ItemWriter<L2RerateReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2RerateItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;
	@Autowired
	private IncrpfDAO incrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;	
	
    private ItemWriter<L2RerateReaderDTO> covrpfInsertWriter;   
	private ItemWriter<L2RerateReaderDTO> covrpfUpdateWriter;
	private ItemWriter<L2RerateReaderDTO> agcmpfUpdateWriter;
    private ItemWriter<L2RerateReaderDTO> agcmpfInsertWriter;   
    
    
    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
	@Override
	public void write(List<? extends L2RerateReaderDTO> readerDTOList) throws Exception {
		LOGGER.info("L2RERATEWrite starts.");
		List<L2RerateReaderDTO> readerDTOModifiedList = new ArrayList<L2RerateReaderDTO>();;
		for (L2RerateReaderDTO dto : readerDTOList) {
			if (null != dto.getAgcmInsert() && null != dto.getAgcmUpdate() && null != dto.getChdrpfInsert() && null != dto.getChdrpfUpdate()  
					&& null != dto.getCovrInsert() && null != dto.getCovrUpdate() ) {
				readerDTOModifiedList.add(dto);
			}	
		}
		if (!readerDTOModifiedList.isEmpty()) {
			covrpfUpdateWriter.write(readerDTOModifiedList);
			covrpfInsertWriter.write(readerDTOModifiedList);   
			agcmpfUpdateWriter.write(readerDTOModifiedList);
			agcmpfInsertWriter.write(readerDTOModifiedList);
		    processOtherData(readerDTOList);
		}
		
		if(!readerDTOModifiedList.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOModifiedList.size(); i++) {
				entityList.add(readerDTOModifiedList.get(i).getChdrnum());
			}			
			sftlock.unlockByList("CH", readerDTOModifiedList.get(0).getChdrcoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2RERATEWrite ends.");
	}
	private void processOtherData(List<? extends L2RerateReaderDTO> readerDTOList) throws ParseException {
		List<Incrpf> incrpfList = new ArrayList<Incrpf>();
		List<Payrpf> payrpfInsertList = new ArrayList<Payrpf>();
		List<Payrpf> payrpfUpdatetList = new ArrayList<Payrpf>();
		List<Ptrnpf> ptrnpftList = new ArrayList<Ptrnpf>();
		List<Chdrpf> chdrpftUpdateList = new ArrayList<Chdrpf>();
		List<Chdrpf> chdrpftInsertList = new ArrayList<Chdrpf>();
		String chdrnum = "";
		if (!readerDTOList.isEmpty()) {
			for(L2RerateReaderDTO dto : readerDTOList ){
				if(null != dto.getIncrInsert()){
					incrpfList.add(dto.getIncrInsert());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPayrInsert()){
					payrpfInsertList.add(dto.getPayrInsert());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPayrUpdate()){
					payrpfUpdatetList.add(dto.getPayrUpdate());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPtrnInsert()){
					ptrnpftList.add(dto.getPtrnInsert());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getChdrpfUpdate()){
					chdrpftUpdateList.add(dto.getChdrpfUpdate());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getChdrpfInsert()){
					chdrpftInsertList.add(dto.getChdrpfInsert());
				}
				chdrnum = dto.getChdrnum();
			}
			if(incrpfList.size() > 0){
				incrpfDAO.insertIncrpfRecords(incrpfList);
			}
			if(payrpfUpdatetList.size() > 0){
				payrpfDAO.updatePayrpfRecords(payrpfUpdatetList);
			}
			if(payrpfInsertList.size() > 0){
				payrpfDAO.insertPayrRecords(payrpfInsertList);
			}
			if(ptrnpftList.size() > 0){
				ptrnpfDAO.insertPtrnpfRecords(ptrnpftList);
			}
			if(chdrpftUpdateList.size() > 0){
				chdrpfDAO.updateChdrpfRecords(chdrpftUpdateList);
			}
			if(chdrpftInsertList.size() > 0){
				chdrpfDAO.insertChdrpfRecords(chdrpftInsertList);
			}
		}
	}
	
	public void setCovrpfInsertWriter(ItemWriter<L2RerateReaderDTO> covrpfInsertWriter) {
		this.covrpfInsertWriter = covrpfInsertWriter;
	}


	public void setCovrpfUpdateWriter(ItemWriter<L2RerateReaderDTO> covrpfUpdateWriter) {
		this.covrpfUpdateWriter = covrpfUpdateWriter;
	}



	public void setAgcmpfUpdateWriter(ItemWriter<L2RerateReaderDTO> agcmpfUpdateWriter) {
		this.agcmpfUpdateWriter = agcmpfUpdateWriter;
	}


	public void setAgcmpfInsertWriter(ItemWriter<L2RerateReaderDTO> agcmpfInsertWriter) {
		this.agcmpfInsertWriter = agcmpfInsertWriter;
	}
   
}
