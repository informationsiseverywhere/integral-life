package com.dxc.integral.life.beans;

import java.io.IOException;
import java.io.Writer;
import org.springframework.batch.item.file.FlatFileHeaderCallback;
import org.springframework.batch.item.file.LineCallbackHandler;
import org.springframework.util.Assert;

/**
 * @author sagrawal35
 * To create header in CSV file
 * 
 *
 */
public class HeaderForWriter implements LineCallbackHandler, FlatFileHeaderCallback {
	private String header = "";

	public HeaderForWriter(String columns) {
	      header=columns;
	   }

	@Override
	public void writeHeader(Writer writer) throws IOException {
		writer.write(header);
	}

	@Override
	public void handleLine(String line) {
		Assert.notNull(line);
		this.header = line;
	}
}