package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr562 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String prat;
	private String prmdetails;
	private String mlinsopt;
	private String mlresind;
	private String coverc;
	private String mbnkref;
	private String loandur;
	private String intcaltype;
	private String mlresindvpms;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getPrat() {
		return prat;
	}
	public void setPrat(String prat) {
		this.prat = prat;
	}
	public String getPrmdetails() {
		return prmdetails;
	}
	public void setPrmdetails(String prmdetails) {
		this.prmdetails = prmdetails;
	}
	public String getMlinsopt() {
		return mlinsopt;
	}
	public void setMlinsopt(String mlinsopt) {
		this.mlinsopt = mlinsopt;
	}
	public String getMlresind() {
		return mlresind;
	}
	public void setMlresind(String mlresind) {
		this.mlresind = mlresind;
	}
	public String getCoverc() {
		return coverc;
	}
	public void setCoverc(String coverc) {
		this.coverc = coverc;
	}
	public String getMbnkref() {
		return mbnkref;
	}
	public void setMbnkref(String mbnkref) {
		this.mbnkref = mbnkref;
	}
	public String getLoandur() {
		return loandur;
	}
	public void setLoandur(String loandur) {
		this.loandur = loandur;
	}
	public String getIntcaltype() {
		return intcaltype;
	}
	public void setIntcaltype(String intcaltype) {
		this.intcaltype = intcaltype;
	}
	public String getMlresindvpms() {
		return mlresindvpms;
	}
	public void setMlresindvpms(String mlresindvpms) {
		this.mlresindvpms = mlresindvpms;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "Sr562 []";
	}
	
}
