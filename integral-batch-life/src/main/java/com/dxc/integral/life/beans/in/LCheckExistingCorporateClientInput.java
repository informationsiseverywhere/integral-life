package com.dxc.integral.life.beans.in;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeName(value = "LCheckExistingCorporateClientInputBean")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
public class LCheckExistingCorporateClientInput {
	
	private AuthenticationInfo authenticationInfo;
	
	private String zkanasnmnor;
	
	private String postcode;
	
	public LCheckExistingCorporateClientInput() {
		//does nothing
	}

	/**
	 * @return the authenticationInfo
	 */
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}

	/**
	 * @param authenticationInfo the authenticationInfo to set
	 */
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}

	/**
	 * @return the zkanasnmnor
	 */
	public String getZkanasnmnor() {
		return zkanasnmnor;
	}

	/**
	 * @param zkanasnmnor the zkanasnmnor to set
	 */
	public void setZkanasnmnor(String zkanasnmnor) {
		this.zkanasnmnor = zkanasnmnor;
	}

	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
}
