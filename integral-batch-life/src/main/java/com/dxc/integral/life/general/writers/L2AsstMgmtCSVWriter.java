package com.dxc.integral.life.general.writers;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dxc.integral.life.beans.AssetMgmtOutput;
import com.dxc.integral.life.utils.CSVUtils;

@Component
@StepScope
public class L2AsstMgmtCSVWriter implements ItemWriter<AssetMgmtOutput>{	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AsstMgmtCSVWriter.class);	
	
	@Value("#{jobParameters['csvPath']}")
	private String csvPath;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['trandate']}")
	private String trandate;	
	
	private final String OUTPUT = "output";	

	@Override
	public void write(List<? extends AssetMgmtOutput> readerClasslist) throws Exception {		
		String outputFilepath = csvPath + batchName + "_" + OUTPUT + "_" + trandate + ".csv";		
        FileWriter writer = new FileWriter(FileSystems.getDefault().getPath(outputFilepath).toFile()); //IJTI-1324     
        //for header
        LOGGER.info("Inside L2AsstMgmtCSVWriter class : Number of records to write in CSV file: {}",
        		readerClasslist.size());//IJTI-1498
        CSVUtils.writeLine(writer, Arrays.asList("Agent Code", "Fund Code", "FundBalance","Commission","Total Commission"));
        List<AssetMgmtOutput> outputList = new ArrayList<>();
        for(AssetMgmtOutput out :  readerClasslist){
        	outputList.add(out);
        }
        Collections.sort(outputList, new Comparator<AssetMgmtOutput>() {

			@Override
			public int compare(AssetMgmtOutput a1, AssetMgmtOutput a2) {
				
				return a1.getOutputAdvisorCode().compareTo(a2.getOutputAdvisorCode());			
			}
        	
        });
        Map<String,BigDecimal> hmap = new HashMap<>();       
        
        for(AssetMgmtOutput c : outputList)
        {
        	if(!hmap.containsKey(c.getOutputAdvisorCode()))
        			hmap.put(c.getOutputAdvisorCode(),new BigDecimal(c.getOutputCommission()));
        	else
        		hmap.put(c.getOutputAdvisorCode(),new BigDecimal(hmap.get(c.getOutputAdvisorCode()).toString()).add(new BigDecimal(c.getOutputCommission())));
        	
        }
        List<String> dummy= new ArrayList<>();
        BigDecimal value= BigDecimal.ZERO;
          
        for (AssetMgmtOutput d : outputList) {        	
        	
        	List<String> list = new ArrayList<>();
        	BigDecimal commission=BigDecimal.ZERO;
        	
        	 for (Map.Entry<String,BigDecimal> entry : hmap.entrySet())  
        	 {
        		 if(entry.getKey().equals(d.getOutputAdvisorCode()))
        		 {
        			 value=entry.getValue();
        			 break;
        			 
        		 }
        		 else
        			 continue;
        		 
        	 }
        	
        	 commission=value;
        	 if(dummy.contains(d.getOutputAdvisorCode()))
        		{
        			list.add(" ");
        		}
        	else {
        		list.add(d.getOutputAdvisorCode());
              	}
        	
            list.add(d.getOutputFundCode());
            list.add(String.valueOf(d.getOutputFundValue())); 
            list.add(String.valueOf(d.getOutputCommission()));
            if(!dummy.contains(String.valueOf(commission)))
            	list.add(String.valueOf(commission));
            else
            	list.add(" ");                              	
                      
            dummy.addAll(list);
            addLines(writer,list);
            
        }

        writer.flush();
        writer.close();

	}
	
	public static void addLines(FileWriter writer,List<String> list) throws IOException
    {
    	   CSVUtils.writeLine(writer, list);
    }
		
	  
}
