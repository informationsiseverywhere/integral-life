package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2472 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String cltaddr01;
	private String cltaddr02;
	private String cltaddr03;
	private String cltaddr04;
	private String cltaddr05;
	private String cltpcode;
	private String ctrycode;
	private String addrtype;
	private String zstates;
	private String ztown;
	private String zkanasurname;
	private String zkanagivname;
	private String zkanacltaddr01;
	private String zkanacltaddr02;
	private String zkanacltaddr03;
	private String zkanacltaddr04;
	private String zkanacltaddr05;
	private String optind01;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getCltaddr01() {
		return cltaddr01;
	}
	public void setCltaddr01(String cltaddr01) {
		this.cltaddr01 = cltaddr01;
	}
	public String getCltaddr02() {
		return cltaddr02;
	}
	public void setCltaddr02(String cltaddr02) {
		this.cltaddr02 = cltaddr02;
	}
	public String getCltaddr03() {
		return cltaddr03;
	}
	public void setCltaddr03(String cltaddr03) {
		this.cltaddr03 = cltaddr03;
	}
	public String getCltaddr04() {
		return cltaddr04;
	}
	public void setCltaddr04(String cltaddr04) {
		this.cltaddr04 = cltaddr04;
	}
	public String getCltaddr05() {
		return cltaddr05;
	}
	public void setCltaddr05(String cltaddr05) {
		this.cltaddr05 = cltaddr05;
	}
	public String getCltpcode() {
		return cltpcode;
	}
	public void setCltpcode(String cltpcode) {
		this.cltpcode = cltpcode;
	}
	public String getCtrycode() {
		return ctrycode;
	}
	public void setCtrycode(String ctrycode) {
		this.ctrycode = ctrycode;
	}
	public String getAddrtype() {
		return addrtype;
	}
	public void setAddrtype(String addrtype) {
		this.addrtype = addrtype;
	}
	public String getZstates() {
		return zstates;
	}
	public void setZstates(String zstates) {
		this.zstates = zstates;
	}
	public String getZtown() {
		return ztown;
	}
	public void setZtown(String ztown) {
		this.ztown = ztown;
	}
	public String getZkanasurname() {
		return zkanasurname;
	}
	public void setZkanasurname(String zkanasurname) {
		this.zkanasurname = zkanasurname;
	}
	public String getZkanagivname() {
		return zkanagivname;
	}
	public void setZkanagivname(String zkanagivname) {
		this.zkanagivname = zkanagivname;
	}
	public String getZkanacltaddr01() {
		return zkanacltaddr01;
	}
	public void setZkanacltaddr01(String zkanacltaddr01) {
		this.zkanacltaddr01 = zkanacltaddr01;
	}
	public String getZkanacltaddr02() {
		return zkanacltaddr02;
	}
	public void setZkanacltaddr02(String zkanacltaddr02) {
		this.zkanacltaddr02 = zkanacltaddr02;
	}
	public String getZkanacltaddr03() {
		return zkanacltaddr03;
	}
	public void setZkanacltaddr03(String zkanacltaddr03) {
		this.zkanacltaddr03 = zkanacltaddr03;
	}
	public String getZkanacltaddr04() {
		return zkanacltaddr04;
	}
	public void setZkanacltaddr04(String zkanacltaddr04) {
		this.zkanacltaddr04 = zkanacltaddr04;
	}
	public String getZkanacltaddr05() {
		return zkanacltaddr05;
	}
	public void setZkanacltaddr05(String zkanacltaddr05) {
		this.zkanacltaddr05 = zkanacltaddr05;
	}
	public String getOptind01() {
		return optind01;
	}
	public void setOptind01(String optind01) {
		this.optind01 = optind01;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S2472 []";
	}
	
}
