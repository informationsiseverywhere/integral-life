package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2OverdueReaderDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.HpuapfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hpuapf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

/**
 * ItemWriter for L2RERATE batch step.
 * 
 * @author wli31
 *
 */
@Async
public class L2OverdueItemWriter implements ItemWriter<L2OverdueReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2OverdueItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;

	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	
	@Autowired
	private HpuapfDAO hpuapfDAO;
	
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;	

	@Override
	public void write(List<? extends L2OverdueReaderDTO> readerDTOList) throws Exception {
		LOGGER.info("L2OverdueWrite starts.");
		processData(readerDTOList);
		if(!readerDTOList.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOList.size(); i++) {
				entityList.add(readerDTOList.get(i).getChdrnum());
			}			
			sftlock.unlockByList("CH", readerDTOList.get(0).getChdrcoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2OverdueWrite ends.");
	}
	private void processData(List<? extends L2OverdueReaderDTO> readerDTOList) throws ParseException {
		List<Payrpf> payrpfInsertList = new ArrayList<>();
		List<Payrpf> payrpfUpdatetList = new ArrayList<>();
		List<Payrpf> payrpfPstcdeList = new ArrayList<>();
		List<Ptrnpf> ptrnpftList = new ArrayList<>();
		List<Chdrpf> chdrpftUpdateList = new ArrayList<>();
		List<Chdrpf> chdrpftInsertList = new ArrayList<>();
		List<Chdrpf> chdrpftUpdateList2 = new ArrayList<>();
		List<Covrpf> covrpftUpdateList = new ArrayList<>();
		List<Covrpf> covrpftInsertList = new ArrayList<>();
		List<Hpuapf> hpuapftUpdateList = new ArrayList<>();
		List<Hpuapf> hpuapftInsertList = new ArrayList<>();
		String chdrnum = "";
		if (!readerDTOList.isEmpty()) {
			for(L2OverdueReaderDTO dto : readerDTOList ){
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPayrInsert()){
					payrpfInsertList.add(dto.getPayrInsert());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPayrUpdate()){
					payrpfUpdatetList.add(dto.getPayrUpdate());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPayrpfPstcde()){
					payrpfPstcdeList.add(dto.getPayrpfPstcde());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getPtrnInsert()){
					ptrnpftList.add(dto.getPtrnInsert());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getChdrpfUpdate()){
					chdrpftUpdateList.add(dto.getChdrpfUpdate());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getChdrpfInsert()){
					chdrpftInsertList.add(dto.getChdrpfInsert());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getChdrpfUpdate2()){
					chdrpftUpdateList2.add(dto.getChdrpfUpdate2());
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getCovrInsert()){
					covrpftInsertList = dto.getCovrInsert();
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getCovrUpdate()){
					covrpftUpdateList = dto.getCovrUpdate();
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getHpuapfUpdate()){
					hpuapftInsertList = dto.getHpuapfUpdate();
				}
				if(!chdrnum.equals(dto.getChdrnum()) && null != dto.getHpuapfUpdate()){
					hpuapftUpdateList = dto.getHpuapfUpdate();
				}
				chdrnum = dto.getChdrnum();
			}
			if(payrpfUpdatetList.size() > 0){
				payrpfDAO.updatePayrpfRecords(payrpfUpdatetList);
			}
			
			if(payrpfInsertList.size() > 0){
				payrpfDAO.insertPayrRecords(payrpfInsertList);
			}
			if(payrpfPstcdeList.size() > 0){
				payrpfDAO.updatePayrPstCde(payrpfPstcdeList);
			}
			if(ptrnpftList.size() > 0){
				ptrnpfDAO.insertPtrnpfRecords(ptrnpftList);
			}
			if(chdrpftUpdateList.size() > 0){
				chdrpfDAO.updateChdrpfRecords(chdrpftUpdateList);
			}
			if(chdrpftInsertList.size() > 0){
				chdrpfDAO.insertChdrpfRecords(chdrpftInsertList);
			}
			if(chdrpftUpdateList2.size() > 0){
				chdrpfDAO.updateChdrLastInserted(chdrpftUpdateList2);
			}
			if(covrpftUpdateList.size() > 0){
				covrpfDAO.updateCovrpfRecords(covrpftUpdateList);
			}
			if(covrpftInsertList.size() > 0){
				covrpfDAO.insertCovrpfRecords(covrpftInsertList);
			}
			if(hpuapftUpdateList.size() > 0){
				hpuapfDAO.updateHpuapfRecords(hpuapftUpdateList);
			}
			if(hpuapftInsertList.size() > 0){
				hpuapfDAO.inertHpuapfRecords(hpuapftInsertList);
			}
		}
	}
	
}
