package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2RerateReaderDTO;
import com.dxc.integral.life.beans.L2ownchgReaderDTO;

/**
 * RowMapper for l2ownchgReaderDTO.
 * 
 * @author mmalik25
 */
public class L2RerateReaderRowMapper implements RowMapper<L2RerateReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2RerateReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2RerateReaderDTO l2RerateReaderDTO = new L2RerateReaderDTO();
		l2RerateReaderDTO.setChdrcoy(rs.getString("chdrcoy"));
		l2RerateReaderDTO.setChdrnum(rs.getString("chdrnum"));
		l2RerateReaderDTO.setLife(rs.getString("life"));
		l2RerateReaderDTO.setCoverage(rs.getString("coverage"));
		l2RerateReaderDTO.setRider(rs.getString("rider"));
		l2RerateReaderDTO.setPlnsfx(rs.getInt("plnsfx"));
		l2RerateReaderDTO.setStatcode(rs.getString("statcode"));
		l2RerateReaderDTO.setPstatcode(rs.getString("pstatcode"));
		return l2RerateReaderDTO;
	}

}