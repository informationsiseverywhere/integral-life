package com.dxc.integral.life.general.processors;


import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.life.beans.L2WaiverRerateControlTotalDTO;
import com.dxc.integral.life.beans.L2WaiverRerateProcessorDTO;
import com.dxc.integral.life.beans.L2WaiverRerateReaderDTO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.utils.L2WaiverRerateItemProcessorUtil;


@Scope(value= "step")
@Async
@Lazy
public class L2WaiverRerateItemProcessor extends BaseProcessor implements ItemProcessor<L2WaiverRerateReaderDTO, L2WaiverRerateReaderDTO>{

	private static final Logger LOGGER = LoggerFactory.getLogger(L2WaiverRerateItemProcessor.class);
			
	@Autowired
	L2WaiverRerateItemProcessorUtil l2waiverRerateItemProcessor;
	
	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}") 
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Autowired
	private Environment env;
	@Autowired
	private L2WaiverRerateControlTotalDTO l2WaiverRerateControlTotalDTO;	

	@Override
	public L2WaiverRerateReaderDTO process(L2WaiverRerateReaderDTO readerDTO) throws Exception{
		
		LOGGER.info("Waiver Rerate Batch Processing starts for contract Number: {}",readerDTO.getChdrNum());//IJTI-1498
		
		boolean vpmsFlag = env.getProperty("vpms.flag").equals("1");
		L2WaiverRerateProcessorDTO l2processorDTO = new L2WaiverRerateProcessorDTO();
		Usrdpf usrdpf = getUserDetails(userName);
		Batcpf batchDetail = getBatcpfInfo(transactionDate,businessDate,Companies.LIFE,"BH65",branch,usrdpf,batchName);
		l2processorDTO.setVpmsFlag(vpmsFlag);
		l2processorDTO.setActMonth(getActMonth(transactionDate));
		l2processorDTO.setActYear(getActYear(transactionDate));
		l2processorDTO.setLang(usrdpf.getLanguage());
		l2processorDTO.setPrefix(batchDetail.getBatcpfx());
		l2processorDTO.setBatch(batchDetail.getBatcbatch());
		l2processorDTO.setBatchtrancde(batchDetail.getBatctrcde());
		l2processorDTO.setBatchbranch(batchDetail.getBatcbrn());
		l2processorDTO.setChdrcoy(readerDTO.getChdrCoy());
		l2processorDTO.setChdrnum(readerDTO.getChdrNum());
		l2processorDTO.setUserName(userName);
		l2processorDTO.setBatchName(batchName);
		l2processorDTO.setTransactionDate(Integer.parseInt(getCobolDate()));
		l2processorDTO.setTranTime(getVrcmTime());
		l2processorDTO.setEffDate(Integer.parseInt(transactionDate));
		l2WaiverRerateControlTotalDTO.setControlTotal01(BigDecimal.ONE);
		l2processorDTO.setL2waiverRerateControlDTO(l2WaiverRerateControlTotalDTO);
		
		l2processorDTO.setCovrpf(setCovrValues(l2processorDTO,readerDTO));
		l2processorDTO = l2waiverRerateItemProcessor.loadTableDetails(l2processorDTO,readerDTO);
		l2processorDTO = l2waiverRerateItemProcessor.checkValidContract(l2processorDTO,readerDTO);
		//Check contract is in softlock
		boolean softLock = softlock(readerDTO.getChdrNum(),"BH65",batchName);
		if (!softLock) {
			LOGGER.info("Contract is already locked, chdrnum: {}", readerDTO.getChdrNum());//IJTI-1498
			return null;
		}
		l2processorDTO = l2waiverRerateItemProcessor.processValidCoverage(l2processorDTO,readerDTO);
		//release softlock
		releaseSoftlock(readerDTO.getChdrCoy(),readerDTO.getChdrNum(),"BH65",userName,batchName,batchDetail.getBatctrcde());
		
		if(l2processorDTO.getAgcmpfInsert()!= null){
			readerDTO.setAgcmpfInsert(l2processorDTO.getAgcmpfInsert());
		}
		if(l2processorDTO.getAgcmpfUpdate() != null){
			readerDTO.setAgcmpfUpdate(l2processorDTO.getAgcmpfUpdate());
		}
		if(l2processorDTO.getChdrpfInsert() != null){
			readerDTO.setChdrpfInsert(l2processorDTO.getChdrpfInsert());
		}
		if(l2processorDTO.getChdrpfUpdate() != null){
			readerDTO.setChdrpfUpdate(l2processorDTO.getChdrpfUpdate());
		}
		if(l2processorDTO.getIncrInsert() != null){
			readerDTO.setIncrpfInsert(l2processorDTO.getIncrInsert());
		}
		
		if(l2processorDTO.getPtrnInsert() != null){
			readerDTO.setPtrnpfInsert(l2processorDTO.getPtrnInsert());
		}
		
		if(l2processorDTO.getPayrpfUpdate() != null){
			readerDTO.setPayrpfUpdate(l2processorDTO.getPayrpfUpdate());
		}
		
		if(l2processorDTO.getPayrpfInsert()!= null){
			readerDTO.setPayrpfInsert(l2processorDTO.getPayrpfInsert());
		}
		
		if(l2processorDTO.getCovrInsert() != null){
			readerDTO.setCovrpfInsert(l2processorDTO.getCovrInsert());
		}
		if(l2processorDTO.getCovrUpdate() != null){
			readerDTO.setCovrpfUpdate(l2processorDTO.getCovrUpdate());
		}
		
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrNum());//IJTI-1498
		return readerDTO;
	}
	public Covrpf setCovrValues(L2WaiverRerateProcessorDTO l2processorDTO,L2WaiverRerateReaderDTO readerDTO)
	{
		Covrpf covrpf= l2processorDTO.getCovrpf();
		covrpf.setChdrcoy(readerDTO.getChdrCoy());
		covrpf.setChdrnum(readerDTO.getChdrNum());
		covrpf.setLife(readerDTO.getLife());
		covrpf.setJlife(readerDTO.getjLife());
		covrpf.setRider(readerDTO.getRider());
		covrpf.setCoverage(readerDTO.getCoverage());
		covrpf.setPlanSuffix(readerDTO.getPlnSfx());
		covrpf.setTranno(readerDTO.getTranNo());
		covrpf.setCurrfrom(readerDTO.getCurrFrom());
		covrpf.setCurrto(readerDTO.getCurrTo());
		covrpf.setStatcode(readerDTO.getStatCode());
		covrpf.setAnbAtCcd(readerDTO.getAnbccd());
		covrpf.setCrrcd(readerDTO.getCrrcd());
		covrpf.setPremCurrency(readerDTO.getPrmcur());
		covrpf.setTermid(readerDTO.getTermId());
		covrpf.setTrdt(readerDTO.getTrdt());
		covrpf.setTrtm(readerDTO.getTrtm());
		covrpf.setRrtdat(readerDTO.getRrtDat());
		covrpf.setValidflag(readerDTO.getValidFlag());
		covrpf.setCpiDate(readerDTO.getCpiDte());
		covrpf.setCpidte(readerDTO.getCpiDte());
		covrpf.setBcesage(readerDTO.getBcesage());
		covrpf.setPcesage(readerDTO.getPcesage());
		covrpf.setRcesage(readerDTO.getRcesage());
		covrpf.setBcestrm(readerDTO.getBcestrm());
		covrpf.setPcestrm(readerDTO.getPcestrm());
		covrpf.setRiskCessTerm(readerDTO.getRcestrm());
		covrpf.setCrtable(readerDTO.getCrtable());
		covrpf.setLiencd(readerDTO.getLiencd());
		covrpf.setMortcls(readerDTO.getMortCls());
		covrpf.setZclstate(readerDTO.getZclState());
		covrpf.setNxtdte(readerDTO.getNxtdte());
		covrpf.setPremCessDate(readerDTO.getPcesdte());
		covrpf.setBcesDte(readerDTO.getBcesdte());
		covrpf.setRcesDte(readerDTO.getRcesdte());
		covrpf.setSumins(readerDTO.getSumIns());
		covrpf.setPstatcode(readerDTO.getPstatCode());
		covrpf.setZstpduty01(readerDTO.getZstpDuty01());
		covrpf.setZbinstprem(readerDTO.getZbinstPrem());
		covrpf.setInstprem(readerDTO.getZinstPrem());
		covrpf.setZlinstprem(readerDTO.getZlinstPrem());
		covrpf.setUniqueNumber(readerDTO.getUniqueNumber());
		covrpf.setRrtfrm(readerDTO.getRrtfrm());
		covrpf.setPrmcur(readerDTO.getPrmcur());
		covrpf.setPcesdte(readerDTO.getPcesdte());
		return covrpf;
		
		
	}
}