package com.dxc.integral.life.general.writers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.L2WaiverRerateReaderDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

@Async
public class L2WaiverRerateItemWriter implements ItemWriter<L2WaiverRerateReaderDTO>{
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2WaiverRerateItemWriter.class);
	
	@Autowired
	private Sftlock sftlock;
	@Autowired
	private IncrpfDAO incrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;	
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	
	@Value("#{jobParameters['trandate']}")
	private String transactionDate;	
	
    private ItemWriter<L2WaiverRerateReaderDTO> covrpfInsertWriter;  
	private ItemWriter<L2WaiverRerateReaderDTO> agcmpfUpdateWriter;
    private ItemWriter<L2WaiverRerateReaderDTO> agcmpfInsertWriter;   
    
    
    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
	@Override
	public void write(List<? extends L2WaiverRerateReaderDTO> readerDTOList) throws Exception {
		LOGGER.info("L2WaiverRerateWriter starts.");
		List<L2WaiverRerateReaderDTO> readerDTOModifiedList = new ArrayList<L2WaiverRerateReaderDTO>();;
		for (L2WaiverRerateReaderDTO dto : readerDTOList) {
			if (null != dto.getAgcmpfInsert() && null != dto.getAgcmpfUpdate() && null != dto.getCovrpfInsert() ) {
				readerDTOModifiedList.add(dto);
			}	
		}
		if (!readerDTOModifiedList.isEmpty()) {			   
			agcmpfUpdateWriter.write(readerDTOModifiedList);
			agcmpfInsertWriter.write(readerDTOModifiedList);
		    processOtherData(readerDTOList);
		    covrpfInsertWriter.write(readerDTOModifiedList);
		}
		
		if(!readerDTOModifiedList.isEmpty()){
			List<String> entityList = new ArrayList<String>();
			for (int i = 0; i < readerDTOModifiedList.size(); i++) {
				entityList.add(readerDTOModifiedList.get(i).getChdrNum());
			}			
			sftlock.unlockByList("CH", readerDTOModifiedList.get(0).getChdrCoy(), entityList);	
			LOGGER.info("Unlock contracts - {}", entityList.toString());//IJTI-1498
		}
		LOGGER.info("L2RERATEWrite ends.");
	}
	private void processOtherData(List<? extends L2WaiverRerateReaderDTO> readerDTOList) throws ParseException {
		List<Incrpf> incrpfList = new ArrayList<>();
		List<Payrpf> payrpfInsertList = new ArrayList<>();
		List<Payrpf> payrpfUpdatetList = new ArrayList<>();
		List<Ptrnpf> ptrnpfList = new ArrayList<>();
		List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
		List<Chdrpf> chdrpfInsertList = new ArrayList<>();
		List<Covrpf> covrpfUpdateList = new ArrayList<>();
		List<Covrpf> covrpfInsertList = new ArrayList<>();
		String chdrnum = "";
		if (!readerDTOList.isEmpty()) {
			for(L2WaiverRerateReaderDTO dto : readerDTOList ){
				if(null != dto.getIncrpfInsert()){
					incrpfList.add(dto.getIncrpfInsert());
				}
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getPayrpfInsert()){
					payrpfInsertList.add(dto.getPayrpfInsert());
				}
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getPayrpfUpdate()){
					payrpfUpdatetList.add(dto.getPayrpfUpdate());
				}
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getPtrnpfInsert()){
					ptrnpfList.add(dto.getPtrnpfInsert());
				}
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getChdrpfUpdate()){
					chdrpfUpdateList.add(dto.getChdrpfUpdate());
				}
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getChdrpfInsert()){
					chdrpfInsertList.add(dto.getChdrpfInsert());
				}			
				
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getCovrpfUpdate()){
					covrpfUpdateList.add(dto.getCovrpfUpdate());
				}
				if(!chdrnum.equals(dto.getChdrNum()) && null != dto.getCovrpfInsert()){
					covrpfInsertList.add(dto.getCovrpfInsert());
				}
				chdrnum = dto.getChdrNum();
			}
			if(!incrpfList.isEmpty()){
				incrpfDAO.insertIncrpfRecords(incrpfList);
			}
			if(!payrpfUpdatetList.isEmpty()){
				payrpfDAO.updatePayrpfRecords(payrpfUpdatetList);
			}
			if(!payrpfInsertList.isEmpty()){
				payrpfDAO.insertPayrRecords(payrpfInsertList);
			}
			if(!ptrnpfList.isEmpty()){
				ptrnpfDAO.insertPtrnpfRecords(ptrnpfList);
			}
			if(!chdrpfUpdateList.isEmpty()){
				chdrpfDAO.updateChdrpfRecords(chdrpfUpdateList);
			}
			if(!chdrpfInsertList.isEmpty()){
				chdrpfDAO.insertChdrpfRecords(chdrpfInsertList);
			}
			
			if(!covrpfUpdateList.isEmpty()){
				covrpfDAO.updateCovrpfRecords(covrpfUpdateList);
			}
		}
	}
	
	
	public void setAgcmpfUpdateWriter(ItemWriter<L2WaiverRerateReaderDTO> agcmpfUpdateWriter) {
		this.agcmpfUpdateWriter = agcmpfUpdateWriter;
	}


	public void setAgcmpfInsertWriter(ItemWriter<L2WaiverRerateReaderDTO> agcmpfInsertWriter) {
		this.agcmpfInsertWriter = agcmpfInsertWriter;
	}
	
	public void setCovrpfInsertWriter(ItemWriter<L2WaiverRerateReaderDTO> covrpfInsertWriter) {
		this.covrpfInsertWriter = covrpfInsertWriter;
	}
   
}