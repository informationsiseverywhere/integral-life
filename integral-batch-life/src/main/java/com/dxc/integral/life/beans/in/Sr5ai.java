package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr5ai implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String excda;
	private String excltxt;
	private String exadtxt;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}
	public String getExcda() {
		return excda;
	}
	public void setExcda(String excda) {
		this.excda = excda;
	}
	public String getExcltxt() {
		return excltxt;
	}
	public void setExcltxt(String excltxt) {
		this.excltxt = excltxt;
	}
	public String getExadtxt() {
		return exadtxt;
	}
	public void setExadtxt(String exadtxt) {
		this.exadtxt = exadtxt;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "Sr5ai []";
	}
	
}
