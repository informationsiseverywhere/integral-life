package com.dxc.integral.life.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * RowMapper for L2BillingReaderDTO.
 * 
 * @author wli31
 */
public class L2BillingReaderRowMapper implements RowMapper<L2BillingReaderDTO> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public L2BillingReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2BillingReaderDTO L2BillingReader = new L2BillingReaderDTO();
		L2BillingReader.setChdrcoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
		L2BillingReader.setChdrnum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");
		L2BillingReader.setPayrseqno(rs.getInt("PAYRSEQNO"));
		L2BillingReader.setBillsupr(rs.getString("BILLSUPR")!= null ? (rs.getString("BILLSUPR").trim()) : "");
		L2BillingReader.setBillspfrom(rs.getInt("BILLSPFROM"));
		L2BillingReader.setBillspto(rs.getInt("BILLSPTO"));
		L2BillingReader.setBillchnl(rs.getString("BILLCHNL")!= null ? (rs.getString("BILLCHNL").trim()) : "");
		L2BillingReader.setBillcd(rs.getInt("BILLCD"));
		L2BillingReader.setMembername(rs.getString("MEMBER_NAME")!= null ? (rs.getString("MEMBER_NAME").trim()) : "");
		L2BillingReader.setChdrtranno(rs.getInt("PAYRTRANNO"));
		L2BillingReader.setPayrbillcd(rs.getInt("PAYRBILLCD"));
		L2BillingReader.setBtdate(rs.getInt("BTDATE"));
		L2BillingReader.setPtdate(rs.getInt("PTDATE"));
		L2BillingReader.setOutstamt(rs.getBigDecimal("OUTSTAMT"));
		L2BillingReader.setNextdate(rs.getInt("NEXTDATE"));
		L2BillingReader.setPayrbillcurr(rs.getString("PAYRBILLCURR")!= null ? (rs.getString("PAYRBILLCURR").trim()) : "");
		L2BillingReader.setBillfreq(rs.getString("BILLFREQ")!= null ? (rs.getString("BILLFREQ").trim()) : "");
		L2BillingReader.setDuedd(rs.getString("DUEDD") != null ? (rs.getString("DUEDD").trim()) : "");
		L2BillingReader.setDuemm(rs.getString("DUEMM")!= null ? (rs.getString("DUEMM").trim()) : "");
		L2BillingReader.setBillday(rs.getString("BILLDAY")!= null ? (rs.getString("BILLDAY").trim()) : "");
		L2BillingReader.setBillmonth(rs.getString("BILLMONTH"));
		L2BillingReader.setSinstamt01(rs.getBigDecimal("SINSTAMT01"));
		L2BillingReader.setSinstamt02(rs.getBigDecimal("SINSTAMT02"));	
		L2BillingReader.setSinstamt03(rs.getBigDecimal("SINSTAMT03"));
		L2BillingReader.setSinstamt04(rs.getBigDecimal("SINSTAMT04"));				
		L2BillingReader.setSinstamt05(rs.getBigDecimal("SINSTAMT05"));
		L2BillingReader.setSinstamt06(rs.getBigDecimal("SINSTAMT06"));				
		L2BillingReader.setPayrcntcurr(rs.getString("PAYRCNTCURR")!= null ? (rs.getString("PAYRCNTCURR").trim()) : "");
		L2BillingReader.setMandref(rs.getString("MANDREF") != null ? (rs.getString("MANDREF")) : "");				
		L2BillingReader.setGrupcoy(rs.getString("GRUPCOY")!= null ? (rs.getString("GRUPCOY").trim()) : "");
		L2BillingReader.setGrupnum(rs.getString("GRUPNUM")!= null ? (rs.getString("GRUPNUM").trim()) : "");				
		L2BillingReader.setMembsel(rs.getString("MEMBSEL")!= null ? (rs.getString("MEMBSEL").trim()) : "");
		L2BillingReader.setTaxrelmth(rs.getString("TAXRELMTH")!= null ? (rs.getString("TAXRELMTH").trim()) : "");				
		L2BillingReader.setIncseqno(rs.getInt("INCSEQNO"));
		L2BillingReader.setEffdate(rs.getInt("EFFDATE"));
		L2BillingReader.setChdruniqueno(rs.getLong("CHDRUNIQUENO"));				
		L2BillingReader.setChdrtranno(rs.getInt("CHDRTRANNO"));
		L2BillingReader.setCnttype(rs.getString("CNTTYPE")!= null ? (rs.getString("CNTTYPE").trim()) : "");				
		L2BillingReader.setOccdate(rs.getInt("OCCDATE"));
		L2BillingReader.setStatcode(rs.getString("STATCODE")!= null ? (rs.getString("STATCODE").trim()) : "");				
		L2BillingReader.setPstcde(rs.getString("PSTCDE")!= null ? (rs.getString("PSTCDE").trim()) : "");
		L2BillingReader.setReg(rs.getString("REG")!= null ? (rs.getString("REG").trim()) : "");				
		L2BillingReader.setChdrcntcurr(rs.getString("CHDRCNTCURR")!= null ? (rs.getString("CHDRCNTCURR").trim()) : "");
		L2BillingReader.setBillcurr(rs.getString("BILLCURR")!= null ? (rs.getString("BILLCURR").trim()) : "");	
		L2BillingReader.setChdrpfx(rs.getString("CHDRPFX")!= null ? (rs.getString("CHDRPFX").trim()) : "");
		L2BillingReader.setCowncoy(rs.getString("COWNCOY")!= null ? (rs.getString("COWNCOY").trim()) : "");
		L2BillingReader.setCownnum(rs.getString("COWNNUM")!= null ? (rs.getString("COWNNUM").trim()) : "");
		L2BillingReader.setCntbranch(rs.getString("CNTBRANCH")!= null ? (rs.getString("CNTBRANCH").trim()) : "");
		L2BillingReader.setServunit(rs.getString("SERVUNIT")!= null ? (rs.getString("SERVUNIT").trim()) : "");
		L2BillingReader.setCcdate(rs.getInt("CCDATE"));
		L2BillingReader.setCollchnl(rs.getString("COLLCHNL")!= null ? (rs.getString("COLLCHNL").trim()) : "");
		L2BillingReader.setCownpfx(rs.getString("COWNPFX")!= null ? (rs.getString("COWNPFX").trim()) : "");
		L2BillingReader.setAgntpfx(rs.getString("AGNTPFX")!= null ? (rs.getString("AGNTPFX").trim()) : "");
		L2BillingReader.setAgntcoy(rs.getString("AGNTCOY")!= null ? (rs.getString("AGNTCOY").trim()) : "");
		L2BillingReader.setAgntnum(rs.getString("AGNTNUM")!= null ? (rs.getString("AGNTNUM").trim()) : "");
		L2BillingReader.setChdrbillchnl(rs.getString("CHDRBILLCHNL")!= null ? (rs.getString("CHDRBILLCHNL").trim()) : "");				
		L2BillingReader.setAcctmeth(rs.getString("ACCTMETH")!= null ? (rs.getString("ACCTMETH").trim()) : "");
		L2BillingReader.setClntpfx(rs.getString("CLNTPFX")!= null ? (rs.getString("CLNTPFX").trim()) : "");
		L2BillingReader.setClntcoy(rs.getString("CLNTCOY")!= null ? (rs.getString("CLNTCOY").trim()) : "");
		L2BillingReader.setClntnum(rs.getString("CLNTNUM")!= null ? (rs.getString("CLNTNUM").trim()) : "");
		L2BillingReader.setBankkey(rs.getString("BANKKEY") != null ? (rs.getString("BANKKEY")) : "");
		L2BillingReader.setBankacckey(rs.getString("BANKACCKEY") != null ? (rs.getString("BANKACCKEY")) : "");
		L2BillingReader.setMandstat(rs.getString("MANDSTAT") != null ? (rs.getString("MANDSTAT")) : "");	
		L2BillingReader.setFacthous(rs.getString("FACTHOUS") != null ? (rs.getString("FACTHOUS")) : "");
		
		
		return L2BillingReader;
	}

}