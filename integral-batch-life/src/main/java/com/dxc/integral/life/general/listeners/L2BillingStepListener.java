package com.dxc.integral.life.general.listeners;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.L2BillingControlTotalDTO;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.smarttable.pojo.T6654;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Listener for L2polrnwlBillingStepListener batch step.
 * 
 * @author wli31
 *
 */
public class L2BillingStepListener extends IntegralStepListener {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2BillingStepListener.class);

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private L2BillingControlTotalDTO l2BillingControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;
	
	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['chdrcoy']}")
	private String company;
	
	@Value("#{jobParameters['trandate']}")
	private String trandate;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Value("#{jobParameters['businessDate']}") 
	private String businessDate;
	
	@Autowired
	private ElogpfDAO elogpfDAO;
	
	@Autowired
	private ItempfDAO itempfDAO;
	
	@Autowired
	private Datcon2 datcon2;

	private static String programName = "B5349";

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("L2polrnwlBillingStepListener::afterStep::Entry");
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.info("Control total update for L2POLRNWLBILLING batch starts");
			Sbcontotpf sbcontotpf = new Sbcontotpf();
			sbcontotpf.setStepname(stepExecution.getStepName());
			sbcontotpf.setUsrprf(userName);
			sbcontotpf.setBranch(branch);
			sbcontotpf.setCompany(company);
			sbcontotpf.setEffdate(Integer.parseInt(trandate));
			sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
			sbcontotpf.setJobnm(batchName);
			sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
			sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
			sbcontotpf.setContot2(l2BillingControlTotalDTO.getControlTotal02());
			sbcontotpf.setContot3(l2BillingControlTotalDTO.getControlTotal03());
			sbcontotpf.setContot4(l2BillingControlTotalDTO.getControlTotal04());
			sbcontotpf.setContot5(l2BillingControlTotalDTO.getControlTotal05());
			sbcontotpf.setContot6(l2BillingControlTotalDTO.getControlTotal06());
			sbcontotpf.setContot7(l2BillingControlTotalDTO.getControlTotal07());
			sbcontotpf.setContot8(l2BillingControlTotalDTO.getControlTotal08());
			sbcontotpf.setContot9(l2BillingControlTotalDTO.getControlTotal09());
			sbcontotpf.setContot10(l2BillingControlTotalDTO.getControlTotal10());
			sbcontotpf.setContot11(l2BillingControlTotalDTO.getControlTotal11());
			sbcontotpf.setContot12(l2BillingControlTotalDTO.getControlTotal12());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);
			LOGGER.info("Control total update for L2polrnwlBillingStepListener batch ends");
			LOGGER.info("L2polrnwlBillingStepListener batch step completed.");
		} else {
			LOGGER.error("Some exception occurred,logging it in Elogpf.{}", 
					exceptionList.get(0).getStackTrace().toString());//IJTI-1498
			if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", programName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("L2polrnwlBillingStepListener::afterStep::Exit");
		return stepExecution.getExitStatus();
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("L2polrnwlBillingStepListener::beforeStep::Entry");
		super.beforeStep(stepExecution);
		int billUpToDate = readMaxLeadDays();
		stepExecution.getExecutionContext().putInt("billUpToDate", billUpToDate);
		LOGGER.debug("L2polrnwlBillingStepListener::beforeStep::Exit");
	}

	private int readMaxLeadDays() {
		int maxLeadDays = 0;
		List<Itempf> t6654List = itempfDAO.readSmartTableListByTableName(company, "T6654", CommonConstants.IT_ITEMPFX);
		if (t6654List != null && !t6654List.isEmpty()) {
			Iterator<Itempf> iterator = t6654List.iterator();
			try {
				while (iterator.hasNext()) {
					Itempf itempf = iterator.next();
					if (itempf.getGenareaj() != null) {
						T6654 t6654IO = new ObjectMapper().readValue(itempf.getGenareaj(), T6654.class);
						if (t6654IO.getLeadDays() > maxLeadDays) {
							maxLeadDays = t6654IO.getLeadDays();
						}
					}
				}
			} catch (IOException e) {
				throw new ItemParseException("B5349: Item parsed failed in Table T6654.");
			}
		}
		return datcon2.plusDays(Integer.parseInt(trandate), maxLeadDays);
		
	}
}
