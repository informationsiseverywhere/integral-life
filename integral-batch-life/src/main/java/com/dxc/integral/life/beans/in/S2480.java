package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2480 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String clttwo;
	private String action;
	private String secuityno;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getClttwo() {
		return clttwo;
	}
	public void setClttwo(String clttwo) {
		this.clttwo = clttwo;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getSecuityno() {
		return secuityno;
	}
	public void setSecuityno(String secuityno) {
		this.secuityno = secuityno;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S2480 []";
	}
	
}
