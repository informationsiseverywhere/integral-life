package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;


@JsonTypeInfo(include=As.WRAPPER_OBJECT, use=Id.NAME)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5011 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String zdoctor;
	private String activeField;
	private S5011screensfl s5011screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getZdoctor() {
		return zdoctor;
	}
	public void setZdoctor(String zdoctor) {
		this.zdoctor = zdoctor;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S5011screensfl getS5011screensfl() {
		return s5011screensfl;
	}
	public void setS5011screensfl(S5011screensfl s5011screensfl) {
		this.s5011screensfl = s5011screensfl;
	}

	public static class S5011screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		public int size() {
			return rows.length;
		}

		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
		
	}
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String ind = "";
			private String select = "";
			private String fupcdes = "";
			private String lifeno = "";
			private String jlife = "";
			private String fupstat = "";
			private String fupremdtDisp = "";
			private String crtdateDisp = "";
			private String fupremk = "";
			private String fuptype = "";
			private String fuprcvdDisp = "";
			private String exprdateDisp = "";

			public String getInd() {
				return ind;
			}
			public void setInd(String ind) {
				this.ind = ind;
			}
			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
			public String getFupcdes() {
				return fupcdes;
			}
			public void setFupcdes(String fupcdes) {
				this.fupcdes = fupcdes;
			}
			public String getLifeno() {
				return lifeno;
			}
			public void setLifeno(String lifeno) {
				this.lifeno = lifeno;
			}
			public String getJlife() {
				return jlife;
			}
			public void setJlife(String jlife) {
				this.jlife = jlife;
			}
			public String getFupstat() {
				return fupstat;
			}
			public void setFupstat(String fupstat) {
				this.fupstat = fupstat;
			}
			public String getFupremdtDisp() {
				return fupremdtDisp;
			}
			public void setFupremdtDisp(String fupremdtDisp) {
				this.fupremdtDisp = fupremdtDisp;
			}
			public String getCrtdateDisp() {
				return crtdateDisp;
			}
			public void setCrtdateDisp(String crtdateDisp) {
				this.crtdateDisp = crtdateDisp;
			}
			public String getFupremk() {
				return fupremk;
			}
			public void setFupremk(String fupremk) {
				this.fupremk = fupremk;
			}
			public String getFuptype() {
				return fuptype;
			}
			public void setFuptype(String fuptype) {
				this.fuptype = fuptype;
			}
			public String getFuprcvdDisp() {
				return fuprcvdDisp;
			}
			public void setFuprcvdDisp(String fuprcvdDisp) {
				this.fuprcvdDisp = fuprcvdDisp;
			}
			public String getExprdateDisp() {
				return exprdateDisp;
			}
			public void setExprdateDisp(String exprdateDisp) {
				this.exprdateDisp = exprdateDisp;
			}
		}
		@Override
		public String toString() {
			return "S5011 []";
		} 
		
}
