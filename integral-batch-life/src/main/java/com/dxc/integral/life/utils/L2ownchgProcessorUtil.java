package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.life.beans.L2ownchgReaderDTO;
import com.dxc.integral.life.beans.L2ownchgReportDTO;

/**
 * Processor Utility for L2ownchg batch.
 * 
 * @author sparikh5
 *
 */
public interface L2ownchgProcessorUtil {
	public boolean checkValidContractStatus(L2ownchgReaderDTO readerDTO) throws IOException;
	
	public L2ownchgReportDTO initReportMap(L2ownchgReaderDTO readerDTO,L2ownchgReportDTO reportDTO,int wsaaNumber) throws IOException;
	public L2ownchgReaderDTO updateReaderDTOInfo(L2ownchgReaderDTO readerDTO,Batcpf batcpf);
	
	
}
