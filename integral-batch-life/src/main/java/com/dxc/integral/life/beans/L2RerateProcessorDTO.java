package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.List;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.pojo.T5671;
import com.dxc.integral.iaf.smarttable.pojo.T5687;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.smarttable.pojo.T5675;
import com.dxc.integral.life.smarttable.pojo.T5679;
/**
 * DTO for Itemreader
 * 
 * @author wli31
 *
 */
public class L2RerateProcessorDTO {

	private String chdrcoy;
	private String chdrnum;
	private String statcode;
	private String pstatcode;
	private int leadDays;
	private int billDate;
	private int effDate;
	private int rerateStore;
	private boolean covrUpdated;
	private boolean covrValid;
	private boolean stampDutyflag;
	private boolean rerateType;
	private BigDecimal instPrem;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private BigDecimal premDiff;
	private BigDecimal sumins;
	private String zsredtrm;
	private int newTranno;
	private List<String> t5655Cnttype;
	private List<Integer> t5655Itmfrm;
	private List<Integer> t5655Itmto;
	private List<Integer> t5655LeadDays;
	private T5679 t5679IO;
	private T5687 t5687IO;
	private T5675 t5675IO;
	private T5671 t5671IO;
	private int t5655IxMax;
	private String batchTrcde;
	private List<Itempf> t5655List;
	private int maxLeadDays;
	private String fullyPaid;
	private long uniqueNumber;
	private Chdrpf  chdrpf;
	private Chdrpf  chdrpfUpdate;
	private Chdrpf  chdrpfInsert;
	private Payrpf payrInsert;
	private Payrpf payrUpdate;
	private Covrpf covrInsert;
	private Covrpf covrUpdate;
	private Incrpf incrInsert;
	private Ptrnpf ptrnInsert;
	private Agcmpf agcmUpdate;
	private Agcmpf agcmInsert;
	private Payrpf payrpf;
	private Lifepf lifepf;
	private String batchName;
	private String userName;
	private int lastRrtDate;
	private PremiumDTO premiumrec;
	private VpxacblDTO vpxacblDTO;
	private VpxlextDTO vpxlextDTO;
	private String lang;
	private String prefix;
	private String batch;
	private String batcbrn;
	private int actyear;
	private int actmonth;
	private L2RerateControlTotalDTO l2RerateControlTotalDTO;
	private boolean vpmsFlag;
	private boolean covrsLive;
	private String billfreqx;
	private int transactionDate;
	private int transactionTime;
	private String life;
	private String coverage;
	private String rider;
	private int plnsfx;
	public L2RerateProcessorDTO(){
		this.chdrcoy = "";
		this.chdrnum = "";
		this.statcode = "";
		this.pstatcode = "";
		this.leadDays = 0;
		this.billDate = 0;
		this.effDate = 0;
		this.rerateStore = 0;
		this.covrUpdated = false;
		this.covrValid = false;
		this.stampDutyflag = false;
		this.rerateType = false;
		this.instPrem = BigDecimal.ZERO;
		this.zbinstprem = BigDecimal.ZERO;
		this.zlinstprem = BigDecimal.ZERO;
		this.premDiff = BigDecimal.ZERO;
		this.sumins = BigDecimal.ZERO;
		this.zsredtrm = "";
		this.newTranno = 0;
		this.t5655IxMax = 0;
		this.batchTrcde = "";
		this.maxLeadDays = 0;
		this.fullyPaid = "";
		this.batchName = "";
		this.userName = "";
		this.lastRrtDate = 0;
		this.lang = "";
		this.prefix = "";
		this.batch = "";
		this.batcbrn = "";
		this.actyear = 0;
		this.actmonth = 0;
		this.vpmsFlag = false;
		this.covrsLive = false;
		this.billfreqx = "";
		this.transactionDate = 0;
		this.transactionTime = 0;
		this.life = "";
		this.coverage = "";
		this.rider = "";
		this.plnsfx = 0;
	}
	public boolean isCovrsLive() {
		return covrsLive;
	}
	public void setCovrsLive(boolean covrsLive) {
		this.covrsLive = covrsLive;
	}
	public int getActyear() {
		return actyear;
	}
	public void setActyear(int actyear) {
		this.actyear = actyear;
	}
	public int getActmonth() {
		return actmonth;
	}
	public void setActmonth(int actmonth) {
		this.actmonth = actmonth;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getFullyPaid() {
		return fullyPaid;
	}
	public void setFullyPaid(String fullyPaid) {
		this.fullyPaid = fullyPaid;
	}
	public boolean isRerateType() {
		return rerateType;
	}
	public void setRerateType(boolean rerateType) {
		this.rerateType = rerateType;
	}
	public BigDecimal getInstPrem() {
		return instPrem;
	}
	public void setInstPrem(BigDecimal instPrem) {
		this.instPrem = instPrem;
	}
	public BigDecimal getZbinstprem() {
		return zbinstprem;
	}
	public void setZbinstprem(BigDecimal zbinstprem) {
		this.zbinstprem = zbinstprem;
	}
	public BigDecimal getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(BigDecimal zlinstprem) {
		this.zlinstprem = zlinstprem;
	}
	public int getRerateStore() {
		return rerateStore;
	}
	public void setRerateStore(int rerateStore) {
		this.rerateStore = rerateStore;
	}

	public boolean isCovrUpdated() {
		return covrUpdated;
	}
	public void setCovrUpdated(boolean covrUpdated) {
		this.covrUpdated = covrUpdated;
	}
	public int getEffDate() {
		return effDate;
	}
	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}
	public int getLeadDays() {
		return leadDays;
	}
	public void setLeadDays(int leadDays) {
		this.leadDays = leadDays;
	}
	public int getBillDate() {
		return billDate;
	}
	public void setBillDate(int billDate) {
		this.billDate = billDate;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPtatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getBatchTrcde() {
		return batchTrcde;
	}
	public void setBatchTrcde(String batchTrcde) {
		this.batchTrcde = batchTrcde;
	}
	public int getT5655IxMax() {
		return t5655IxMax;
	}
	public void setT5655IxMax(int t5655IxMax) {
		this.t5655IxMax = t5655IxMax;
	}
	public List<String> getT5655Cnttype() {
		return t5655Cnttype;
	}
	public void setT5655Cnttype(List<String> t5655Cnttype) {
		this.t5655Cnttype = t5655Cnttype;
	}
	public List<Integer> getT5655Itmfrm() {
		return t5655Itmfrm;
	}
	public void setT5655Itmfrm(List<Integer> t5655Itmfrm) {
		this.t5655Itmfrm = t5655Itmfrm;
	}
	public List<Integer> getT5655Itmto() {
		return t5655Itmto;
	}
	public void setT5655Itmto(List<Integer> t5655Itmto) {
		this.t5655Itmto = t5655Itmto;
	}
	public List<Integer> getT5655LeadDays() {
		return t5655LeadDays;
	}
	public void setT5655LeadDays(List<Integer> t5655LeadDays) {
		this.t5655LeadDays = t5655LeadDays;
	}
	public T5679 getT5679IO() {
		return t5679IO;
	}
	public void setT5679IO(T5679 t5679io) {
		t5679IO = t5679io;
	}
	public T5687 getT5687IO() {
		return t5687IO;
	}
	public void setT5687IO(T5687 t5687io) {
		t5687IO = t5687io;
	}
	public T5675 getT5675IO() {
		return t5675IO;
	}
	public void setT5675IO(T5675 t5675io) {
		t5675IO = t5675io;
	}
	public T5671 getT5671IO() {
		return t5671IO;
	}
	public void setT5671IO(T5671 t5671io) {
		t5671IO = t5671io;
	}
	public List<Itempf> getT5655List() {
		return t5655List;
	}
	public void setT5655List(List<Itempf> t5655List) {
		this.t5655List = t5655List;
	}
	public int getMaxLeadDays() {
		return maxLeadDays;
	}
	public void setMaxLeadDays(int maxLeadDays) {
		this.maxLeadDays = maxLeadDays;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}
	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}
	public Chdrpf getChdrpfInsert() {
		return chdrpfInsert;
	}
	public void setChdrpfInsert(Chdrpf chdrpfInsert) {
		this.chdrpfInsert = chdrpfInsert;
	}
	public Payrpf getPayrInsert() {
		return payrInsert;
	}
	public void setPayrInsert(Payrpf payrInsert) {
		this.payrInsert = payrInsert;
	}
	public Payrpf getPayrUpdate() {
		return payrUpdate;
	}
	public void setPayrUpdate(Payrpf payrUpdate) {
		this.payrUpdate = payrUpdate;
	}
	public Covrpf getCovrInsert() {
		return covrInsert;
	}
	public void setCovrInsert(Covrpf covrInsert) {
		this.covrInsert = covrInsert;
	}
	public Covrpf getCovrUpdate() {
		return covrUpdate;
	}
	public void setCovrUpdate(Covrpf covrUpdate) {
		this.covrUpdate = covrUpdate;
	}
	public Incrpf getIncrInsert() {
		return incrInsert;
	}
	public void setIncrInsert(Incrpf incrInsert) {
		this.incrInsert = incrInsert;
	}
	public Ptrnpf getPtrnInsert() {
		return ptrnInsert;
	}
	public void setPtrnInsert(Ptrnpf ptrnInsert) {
		this.ptrnInsert = ptrnInsert;
	}
	public Agcmpf getAgcmUpdate() {
		return agcmUpdate;
	}
	public void setAgcmUpdate(Agcmpf agcmUpdate) {
		this.agcmUpdate = agcmUpdate;
	}
	public Agcmpf getAgcmInsert() {
		return agcmInsert;
	}
	public void setAgcmInsert(Agcmpf agcmInsert) {
		this.agcmInsert = agcmInsert;
	}
	public Lifepf getLifepf() {
		return lifepf;
	}
	public void setLifepf(Lifepf lifepf) {
		this.lifepf = lifepf;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getLastRrtDate() {
		return lastRrtDate;
	}
	public void setLastRrtDate(int lastRrtDate) {
		this.lastRrtDate = lastRrtDate;
	}
	public Chdrpf getChdrpf() {
		return chdrpf;
	}
	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}
	public PremiumDTO getPremiumrec() {
		return premiumrec;
	}
	public void setPremiumrec(PremiumDTO premiumrec) {
		this.premiumrec = premiumrec;
	}
	public BigDecimal getPremDiff() {
		return premDiff;
	}
	public void setPremDiff(BigDecimal premDiff) {
		this.premDiff = premDiff;
	}
	public String getZsredtrm() {
		return zsredtrm;
	}
	public void setZsredtrm(String zsredtrm) {
		this.zsredtrm = zsredtrm;
	}
	public int getNewTranno() {
		return newTranno;
	}
	public void setNewTranno(int newTranno) {
		this.newTranno = newTranno;
	}
	public boolean isStampDutyflag() {
		return stampDutyflag;
	}
	public void setStampDutyflag(boolean stampDutyflag) {
		this.stampDutyflag = stampDutyflag;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public L2RerateControlTotalDTO getL2RerateControlTotalDTO() {
		return l2RerateControlTotalDTO;
	}
	public void setL2RerateControlTotalDTO(
			L2RerateControlTotalDTO l2RerateControlTotalDTO) {
		this.l2RerateControlTotalDTO = l2RerateControlTotalDTO;
	}
	public boolean isVpmsFlag() {
		return vpmsFlag;
	}
	public void setVpmsFlag(boolean vpmsFlag) {
		this.vpmsFlag = vpmsFlag;
	}
	public Payrpf getPayrpf() {
		return payrpf;
	}
	public void setPayrpf(Payrpf payrpf) {
		this.payrpf = payrpf;
	}
	public String getBillfreqx() {
		return billfreqx;
	}
	public void setBillfreqx(String billfreqx) {
		this.billfreqx = billfreqx;
	}
	public VpxacblDTO getVpxacblDTO() {
		return vpxacblDTO;
	}
	public void setVpxacblDTO(VpxacblDTO vpxacblDTO) {
		this.vpxacblDTO = vpxacblDTO;
	}
	public VpxlextDTO getVpxlextDTO() {
		return vpxlextDTO;
	}
	public void setVpxlextDTO(VpxlextDTO vpxlextDTO) {
		this.vpxlextDTO = vpxlextDTO;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}
	public boolean isCovrValid() {
		return covrValid;
	}
	public void setCovrValid(boolean covrValid) {
		this.covrValid = covrValid;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	
	
}