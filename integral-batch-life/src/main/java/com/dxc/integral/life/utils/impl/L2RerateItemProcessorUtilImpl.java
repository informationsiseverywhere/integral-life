package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.FeaConfg;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.T5671;
import com.dxc.integral.iaf.smarttable.pojo.T5687;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.AgecalcDTO;
import com.dxc.integral.life.beans.CrtundwrtDTO;
import com.dxc.integral.life.beans.IsuallDTO;
import com.dxc.integral.life.beans.L2RerateProcessorDTO;
import com.dxc.integral.life.beans.LifsttrDTO;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.AnnypfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.MbnspfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PcddpfDAO;
import com.dxc.integral.life.dao.RcvdpfDAO;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Annypf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Incrpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Mbnspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Pcddpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.dao.model.Rcvdpf;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T5655;
import com.dxc.integral.life.smarttable.pojo.T5675;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.utils.Agecalc;
import com.dxc.integral.life.utils.Crtundwrt;
import com.dxc.integral.life.utils.L2RerateItemProcessorUtil;
import com.dxc.integral.life.utils.Lifsttr;
import com.dxc.integral.life.utils.Premium;
import com.dxc.integral.life.utils.VPMScaller;
import com.dxc.integral.life.utils.Vpxacbl;
import com.dxc.integral.life.utils.Vpxchdr;
import com.dxc.integral.life.utils.Vpxlext;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * l2Rerate processor utility implementation.
 * 
 * @author wli31
 *
 */
@Service("l2RerateItemProcessorUtil")
@Lazy
public class L2RerateItemProcessorUtilImpl implements L2RerateItemProcessorUtil {

	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private Agecalc agecalc;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private AgcmpfDAO agcmpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private PcddpfDAO pcddpfDAO;
	@Autowired
	private LextpfDAO lextpfDAO;
	@Autowired
	private IncrpfDAO incrpfDAO;
	@Autowired
	private MbnspfDAO mbnspfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Vpxacbl vpxacbl;
	@Autowired
	private Vpxlext vpxlext;
	@Autowired
	private Vpxchdr vpxchdr;
	@Autowired
	private Crtundwrt crtundwrt;
	@Autowired
	private Lifsttr lifsttr;
	@Autowired
	private VPMScaller vpmscaller;
	@Autowired
	private Premium premium;
	@Autowired
	private AnnypfDAO annypfDAO;
	@Autowired
	private RcvdpfDAO rcvdpfDAO;
	public boolean validIncrpf(L2RerateProcessorDTO l2RerateProcessorDTO){
		boolean chdrValid = true;
		T5679 t5679IO = l2RerateProcessorDTO.getT5679IO();
		for (int i = 0; i < 12; i++) {
			if (t5679IO.getCnRiskStats().get(i).equals(l2RerateProcessorDTO.getStatcode())) {
				for (i = 0; i < 12; i++) {
					if (t5679IO.getCnPremStats().get(i).equals(l2RerateProcessorDTO.getPstatcode())) {
						chdrValid = true;
						break;
					}
				}
				if (chdrValid) {
					break;
				}
			}
		}
		List<Incrpf> incrpfList = incrpfDAO.readValidIncrpf(l2RerateProcessorDTO.getChdrcoy(), l2RerateProcessorDTO.getChdrnum());
		if (incrpfList != null && !incrpfList.isEmpty()) {
			chdrValid = false;
		}
		return chdrValid;
	}

	public L2RerateProcessorDTO validateChdrpf(L2RerateProcessorDTO l2RerateProcessorDTO) {
		Chdrpf chdrpf = chdrpfDAO.getLPContractInfo(l2RerateProcessorDTO.getChdrcoy(), l2RerateProcessorDTO.getChdrnum());
		int t5655Ix = l2RerateProcessorDTO.getT5655IxMax() - 1;
		while (!(t5655Ix < 0 || (chdrpf.getCnttype().equals(l2RerateProcessorDTO.getT5655Cnttype().get(t5655Ix))
				&& (chdrpf.getOccdate() >= l2RerateProcessorDTO.getT5655Itmfrm().get(t5655Ix))
				&& (chdrpf.getOccdate() <= l2RerateProcessorDTO.getT5655Itmto().get(t5655Ix))))) {
			t5655Ix--;
		}
		/* If the contract type specific entry is not found, use the */
		/* default entry of the transaction code plus '***'. */
		if (t5655Ix < 0) {
			t5655Ix = l2RerateProcessorDTO.getT5655IxMax() - 1;
			while (!(t5655Ix < 0 || "***".equals(l2RerateProcessorDTO.getT5655Cnttype().get(t5655Ix)))) {
				t5655Ix--;
			}
		}
		if (t5655Ix < 0) {
			throw new StopRunException("error for contract lead days");
		}
		l2RerateProcessorDTO.setLeadDays(l2RerateProcessorDTO.getT5655LeadDays().get(t5655Ix));
		l2RerateProcessorDTO.setChdrpf(chdrpf);
		l2RerateProcessorDTO.setBillDate(datcon2.plusDays(l2RerateProcessorDTO.getEffDate(), l2RerateProcessorDTO.getT5655LeadDays().get(t5655Ix)));
		return l2RerateProcessorDTO;
		
	}
	public L2RerateProcessorDTO loadSmartTables(L2RerateProcessorDTO l2RerateProcessorDTO){
		T5679 t5679IO = itempfService.readSmartTableByTrim(Companies.LIFE, "T5679", l2RerateProcessorDTO.getBatchTrcde(),T5679.class);
		if (t5679IO == null) {
			throw new ItemNotfoundException("Br613: Item not found in Table T5679: "+l2RerateProcessorDTO.getBatchTrcde() );
		}
		l2RerateProcessorDTO.setT5679IO(t5679IO);
		int t5655Ix = 0;
		int t5655IxMax = 0;
		List<String> t5655Cnttype = new ArrayList<String>();
		List<Integer> t5655Itmfrm = new ArrayList<Integer>();
		List<Integer> t5655Itmto = new ArrayList<Integer>();
		List<Integer> t5655LeadDays = new ArrayList<Integer>();
		List<Itempf> t5655List = itempfService.readT5655();
		for (Itempf t5655itempf : t5655List) {
			String itemitem = t5655itempf.getItemitem();
			if (itemitem.startsWith(l2RerateProcessorDTO.getBatchTrcde()) && t5655itempf.getItmfrm() <= 99999999) {
				if (t5655Ix > 1000) {
					throw new StopRunException("Br613: too many T5655 records");
				}
				T5655 t5655IO = null;
				try {
					t5655IO = new ObjectMapper().readValue(t5655itempf.getGenareaj(), T5655.class);
				} catch (IOException e) {
					throw new ItemParseException("Br613: Item parsed failed in Table T5655:"+l2RerateProcessorDTO.getBatchTrcde());
				}
				t5655Cnttype.add(itemitem.substring(4).trim());
				t5655Itmfrm.add(t5655itempf.getItmfrm());
				t5655Itmto.add(t5655itempf.getItmto());
				t5655LeadDays.add(t5655IO.getLeadDays());
				t5655Ix++;
			}
		}
		t5655IxMax = t5655Ix;
		l2RerateProcessorDTO.setT5655Cnttype(t5655Cnttype);
		l2RerateProcessorDTO.setT5655Itmfrm(t5655Itmfrm);
		l2RerateProcessorDTO.setT5655Itmto(t5655Itmto);
		l2RerateProcessorDTO.setT5655IxMax(t5655IxMax);
		l2RerateProcessorDTO.setT5655LeadDays(t5655LeadDays);
		return l2RerateProcessorDTO;
	}
	public L2RerateProcessorDTO updateChdrPayr(L2RerateProcessorDTO l2RerateProcessorDTO){
		if (l2RerateProcessorDTO.isCovrUpdated()) {
			l2RerateProcessorDTO = updateChdrPtrn(l2RerateProcessorDTO);
			l2RerateProcessorDTO = updatePayr(l2RerateProcessorDTO);
		}
		return l2RerateProcessorDTO;
		
	}
	public L2RerateProcessorDTO updateCovrAgcm(L2RerateProcessorDTO l2RerateProcessorDTO){
		List<Covrpf> covrList = covrpfDAO.searchValidCovrpfRecords(l2RerateProcessorDTO.getChdrcoy(),
				l2RerateProcessorDTO.getChdrnum(),l2RerateProcessorDTO.getLife(),l2RerateProcessorDTO.getCoverage(),l2RerateProcessorDTO.getRider());
		List<Agcmpf> agcmList = agcmpfDAO.searchAgcmstRecord(l2RerateProcessorDTO.getChdrcoy(), l2RerateProcessorDTO.getChdrnum(),
				l2RerateProcessorDTO.getLife(),l2RerateProcessorDTO.getCoverage(),l2RerateProcessorDTO.getRider(),l2RerateProcessorDTO.getPlnsfx(),"1");
		Covrpf covrpf = covrList.get(0);
		l2RerateProcessorDTO = validateCovrpf(covrpf,l2RerateProcessorDTO);
		if (l2RerateProcessorDTO.isCovrValid()) {
			try {
				l2RerateProcessorDTO = updateCovr(covrpf, l2RerateProcessorDTO);
			} catch (ParseException e) {
				throw new SubroutineIOException("ParseException :" + e.getMessage());
			}
			if (agcmList != null && !agcmList.isEmpty()) {
				l2RerateProcessorDTO = updateAgcm(covrpf, agcmList, l2RerateProcessorDTO);
			}
		//call UNLCHG,GRVULCHG, but it's blank for TEN&RUL, just ignore temporarily	
		//	l2RerateProcessorDTO = genericProcessing(covrpf,l2RerateProcessorDTO);
		}
		
		return l2RerateProcessorDTO;
	}
	private L2RerateProcessorDTO updateAgcm(Covrpf covrpf, List<Agcmpf> agcmList, L2RerateProcessorDTO l2RerateProcessorDTO) {
		for (Agcmpf agcmpf : agcmList) {
			if (agcmpf.getChdrnum().equals(covrpf.getChdrnum()) && agcmpf.getLife().equals(covrpf.getLife())
					&& agcmpf.getRider().equals(covrpf.getRider()) && agcmpf.getCoverage().equals(covrpf.getCoverage())
					&& agcmpf.getPlnsfx().equals(covrpf.getPlnsfx())) {
				if (!"Y".equals(agcmpf.getDormflag())) {
					Agcmpf agcmpfUpdate = new Agcmpf(agcmpf);
					agcmpfUpdate.setCurrto(l2RerateProcessorDTO.getRerateStore());
					agcmpfUpdate.setValidflag("2");
					agcmpfUpdate.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
					agcmpfUpdate.setUsrprf(l2RerateProcessorDTO.getUserName());
					l2RerateProcessorDTO.setAgcmUpdate(agcmpfUpdate);
					Agcmpf agcmpfInsert = new Agcmpf(agcmpf);
					agcmpfInsert.setTranno(l2RerateProcessorDTO.getNewTranno());
					agcmpfInsert.setCurrfrom(l2RerateProcessorDTO.getRerateStore());
					agcmpfInsert.setCurrto(99999999);
					agcmpfInsert.setValidflag("1");
					if (!covrpf.getPcesdte().equals(covrpf.getRrtdat())) {
						agcmpfInsert.setAnnprem(agentAnnprem(covrpf, agcmpfInsert, l2RerateProcessorDTO));
					}
					agcmpfInsert.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
					agcmpfInsert.setUsrprf(l2RerateProcessorDTO.getUserName());
					l2RerateProcessorDTO.setAgcmInsert(agcmpfInsert);
				}
				return l2RerateProcessorDTO;
			}
		}
		return l2RerateProcessorDTO;
	}
	private BigDecimal agentAnnprem(Covrpf covrpf, Agcmpf agcmpf, L2RerateProcessorDTO l2RerateProcessorDTO) {
		String agntnum = agcmpf.getAgntnum();
		if ("O".equals(agcmpf.getOvrdcat())) {
			boolean agntFound = false;
			String cedagent = agcmpf.getCedagent();
			while (!agntFound) {
				agcmpf.setChdrcoy(agcmpf.getChdrcoy());
				agcmpf.setChdrnum(agcmpf.getChdrnum());
				agcmpf.setLife(agcmpf.getLife());
				agcmpf.setCoverage(agcmpf.getCoverage());
				agcmpf.setRider(agcmpf.getRider());
				agcmpf.setPlnsfx(agcmpf.getPlnsfx());
				agcmpf.setAgntnum(cedagent);
				List<Agcmpf> agcmList = agcmpfDAO.getAgcmpfRecordByAgntnum(agcmpf);
				if (agcmList == null || agcmList.isEmpty()) {
					fatalError("Agcmpf mrnf:" + agcmpf.getChdrnum());
				}
				agcmpf = agcmList.get(0);
				if ("B".equals(agcmpf.getOvrdcat())) {
					agntFound = true;
					agntnum = agcmpf.getAgntnum();
				} else {
					cedagent = agcmpf.getCedagent();
				}
			}
		}
		List<Pcddpf> pcddpfList = pcddpfDAO.getPcddpfRecord(l2RerateProcessorDTO.getChdrcoy(), l2RerateProcessorDTO.getChdrnum());
		BigDecimal wsaaAnnprem;
		for (Pcddpf pcddpf : pcddpfList) {
			if (pcddpf.getAgntNum().equals(agntnum)) {
				wsaaAnnprem = agcmpf.getAnnprem();
				if (l2RerateProcessorDTO.getT5687IO().getZrrcombas() != null && !l2RerateProcessorDTO.getT5687IO().getZrrcombas().trim().isEmpty()) {
					wsaaAnnprem = covrpf.getZbinstprem().multiply(new BigDecimal(l2RerateProcessorDTO.getBillfreqx()))
							.multiply(pcddpf.getSplitC().divide(new BigDecimal(100)));
				} else {
					wsaaAnnprem = covrpf.getInstprem().multiply(new BigDecimal(l2RerateProcessorDTO.getBillfreqx()))
							.multiply(pcddpf.getSplitC().divide(new BigDecimal(100)));
				}
				if (wsaaAnnprem.compareTo(BigDecimal.ZERO) != 0) {
					wsaaAnnprem = callRounding(agcmpf.getAnnprem(),l2RerateProcessorDTO);
				}
				return wsaaAnnprem;
			}
		}
		fatalError("PCDDPF AGNT mrnf:" + agntnum);
		return null;
	}

	private L2RerateProcessorDTO updateCovr(Covrpf covrpf, L2RerateProcessorDTO l2RerateProcessorDTO) throws ParseException {
		l2RerateProcessorDTO.setCovrUpdated(true);
		l2RerateProcessorDTO.setRerateStore(covrpf.getRrtdat());
		l2RerateProcessorDTO.setRerateType(false);
		l2RerateProcessorDTO.setInstPrem(BigDecimal.ZERO);
		l2RerateProcessorDTO.setZbinstprem(BigDecimal.ZERO);
		l2RerateProcessorDTO.setZlinstprem(BigDecimal.ZERO);
		
		Lifepf lifepf = lifepfDAO.getValidLifeRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
		if (lifepf == null) {
			fatalError("lifepf MRNF");
		}
		if (covrpf.getPcesdte().equals(covrpf.getRrtdat())) {
			l2RerateProcessorDTO.setFullyPaid("Y");
			l2RerateProcessorDTO = pastCeaseDate(covrpf, l2RerateProcessorDTO);
			return l2RerateProcessorDTO;
		}
		l2RerateProcessorDTO.setLifepf(lifepf);
		l2RerateProcessorDTO.setFullyPaid("N");
		l2RerateProcessorDTO = calculatePremium(covrpf,l2RerateProcessorDTO);
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(l2RerateProcessorDTO.getRerateStore());
		covrpfUpdate.setValidflag("2");
		covrpfUpdate.setJobnm(l2RerateProcessorDTO.getBatchName());
		covrpfUpdate.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setCovrUpdate(covrpfUpdate);
		l2RerateProcessorDTO = checkLexts(covrpfUpdate, l2RerateProcessorDTO);
		statistics(l2RerateProcessorDTO);
		return l2RerateProcessorDTO;
	}
	private L2RerateProcessorDTO calculatePremium(Covrpf covrpf,L2RerateProcessorDTO l2RerateProcessorDTO) throws ParseException  {
		PremiumDTO premiumrec = new PremiumDTO();
		T5687 t5687IO = itempfService.readT5687(Companies.LIFE, covrpf.getCrtable(),l2RerateProcessorDTO.getEffDate());
		l2RerateProcessorDTO.setT5687IO(t5687IO);
		if (null == t5687IO) {
			throw new ItemNotfoundException("Br613: Item not found in Table T5687: " + covrpf.getCrtable());
		}
		l2RerateProcessorDTO.setZsredtrm(t5687IO.getZsredtrm());

		if (t5687IO.getPremmeth().trim().isEmpty()) {
			return l2RerateProcessorDTO;
		}
		l2RerateProcessorDTO = setRerateType(covrpf,l2RerateProcessorDTO);
		premiumrec.setLsex(l2RerateProcessorDTO.getLifepf().getCltsex());
		int anb = calculateAnb(l2RerateProcessorDTO.getLifepf(),l2RerateProcessorDTO);
		premiumrec.setLage(anb);
		List<Lifepf> lifelnbList = lifepfDAO.getLifeRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),
				"01");
		String t5675Item = "";
		if (lifelnbList != null && !lifelnbList.isEmpty()) {
			Lifepf lifelnb = lifelnbList.get(0);
			t5675Item = t5687IO.getJlPremMeth();
			premiumrec.setJlsex(lifelnb.getCltsex());
			anb = calculateAnb(lifelnb,l2RerateProcessorDTO);
			premiumrec.setJlage(anb);
		} else {
			t5675Item = t5687IO.getPremmeth();
			premiumrec.setJlsex("");
			premiumrec.setJlage(0);
		}
		T5675 t5675IO  = itempfService.readSmartTableByTrim(Companies.LIFE, "T5675", t5675Item,T5675.class);
		if (l2RerateProcessorDTO.isVpmsFlag()) {
			premiumrec.setPremMethod(t5675Item);
		}
		l2RerateProcessorDTO.setT5675IO(t5675IO);
		l2RerateProcessorDTO.setPremiumrec(premiumrec);
		l2RerateProcessorDTO = payr(covrpf,l2RerateProcessorDTO);
		return l2RerateProcessorDTO;
	}
	private int calculateAnb(Lifepf lifepf,L2RerateProcessorDTO l2RerateProcessorDTO) {
		AgecalcDTO agecalcPojo = new AgecalcDTO();
		agecalcPojo.setFunction("CALCP");
		agecalcPojo.setLanguage(l2RerateProcessorDTO.getLang());
		agecalcPojo.setCnttype(l2RerateProcessorDTO.getChdrpf().getCnttype());
		agecalcPojo.setIntDate1(lifepf.getCltdob() + "");
		agecalcPojo.setIntDate2(l2RerateProcessorDTO.getLastRrtDate() + "");
		agecalcPojo.setCompany(Companies.FSU);
		try {
			agecalcPojo = agecalc.processAgecalc(agecalcPojo);
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Br613: Exception in agecalc: " + e.getMessage());
		}
		return agecalcPojo.getAgerating();
	}
	private L2RerateProcessorDTO setRerateType(Covrpf covrpf,L2RerateProcessorDTO l2RerateProcessorDTO) {
		int durationInt = 0;
		int durationRem = 0;
		if (l2RerateProcessorDTO.getT5687IO().getRtrnwfreq() == 0) {
			l2RerateProcessorDTO.setRerateType(true);
		} else {
			long freqFactor;
			try {
				freqFactor = datcon3.getYearsDifference(covrpf.getCrrcd(), covrpf.getRrtdat());
			} catch (ParseException e) {
				throw new SubroutineIOException("Br613: ParseException in datcon3: " + e.getMessage());
			}
			durationInt = (int) freqFactor / l2RerateProcessorDTO.getT5687IO().getRtrnwfreq();
			durationRem = (int) freqFactor % l2RerateProcessorDTO.getT5687IO().getRtrnwfreq();
			if (durationRem != 0) {
				Lextpf lextbrr = new Lextpf();
				lextbrr.setChdrcoy(covrpf.getChdrcoy());
				lextbrr.setChdrnum(covrpf.getChdrnum());
				lextbrr.setLife(covrpf.getLife());
				lextbrr.setCoverage(covrpf.getCoverage());
				lextbrr.setRider(covrpf.getRider());
				lextbrr.setEcesdte(covrpf.getRrtdat());
				List<Lextpf> lextpfList = lextpfDAO.checkLextpfRecord(lextbrr);
				if (lextpfList != null && !lextpfList.isEmpty()) {
					l2RerateProcessorDTO.setRerateType(true);
				}
			}
		}
		if (l2RerateProcessorDTO.isRerateType()) {
			l2RerateProcessorDTO.setLastRrtDate(datcon2.plusYears(covrpf.getCrrcd(), l2RerateProcessorDTO.getT5687IO().getRtrnwfreq() * durationInt));
		} else {
			l2RerateProcessorDTO.setLastRrtDate(covrpf.getRrtdat());
		}
		return l2RerateProcessorDTO;
	}
	private L2RerateProcessorDTO pastCeaseDate(Covrpf covrpf, L2RerateProcessorDTO l2RerateProcessorDTO) {
		if (calc(covrpf, l2RerateProcessorDTO)) {
			if ("00".equals(covrpf.getRider()) || "  ".equals(covrpf.getRider())) {
				if (!l2RerateProcessorDTO.getT5679IO().getSetSngpCovStat().trim().isEmpty()) {
					covrpf.setPstatcode(l2RerateProcessorDTO.getT5679IO().getSetSngpCovStat());
				}
			} else {
				if (!l2RerateProcessorDTO.getT5679IO().getSetSngpRidStat().trim().isEmpty()) {
					covrpf.setPstatcode(l2RerateProcessorDTO.getT5679IO().getSetSngpRidStat());
				}
			}
		}
		Covrpf covrpfInsert = new Covrpf(covrpf);
		l2RerateProcessorDTO.setNewTranno(l2RerateProcessorDTO.getChdrpf().getTranno() + 1);
		covrpfInsert.setTranno(l2RerateProcessorDTO.getNewTranno());
		covrpfInsert.setCurrfrom(l2RerateProcessorDTO.getRerateStore());
		covrpfInsert.setCurrto(99999999);
		covrpfInsert.setValidflag("1");
//		if(riskPremflag){
		// covrpfInsert.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845
//		}
		covrpfInsert.setJobnm(l2RerateProcessorDTO.getBatchName());
		covrpfInsert.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setCovrInsert(covrpfInsert);
		l2RerateProcessorDTO.getL2RerateControlTotalDTO().setControlTotal04(BigDecimal.ONE);
		return l2RerateProcessorDTO;
	}

	private boolean calc(Covrpf covrpf, L2RerateProcessorDTO l2RerateProcessorDTO) {
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(l2RerateProcessorDTO.getRerateStore());
		covrpfUpdate.setValidflag("2");
		l2RerateProcessorDTO.setCovrUpdate(covrpfUpdate);
		/* Write a new COVR with an incremented transaction number, a */
		/* new premium status code, and new current from and current to */
		/* dates. */
		l2RerateProcessorDTO.setPremDiff((l2RerateProcessorDTO.getPremDiff() == null ? BigDecimal.ZERO : l2RerateProcessorDTO.getPremDiff()).
				add(BigDecimal.ZERO.subtract(covrpf.getInstprem())));
		if ("00".equals(l2RerateProcessorDTO.getChdrpf().getBillfreq())) {
			return true;
		}
		if ("01".equals(covrpf.getCoverage()) && "01".equals(covrpf.getLife()) && "00".equals(covrpf.getRider())) {
			if (!l2RerateProcessorDTO.getT5679IO().getSetCovPremStat().trim().isEmpty()) {
				covrpf.setPstatcode(l2RerateProcessorDTO.getT5679IO().getSetCovPremStat());
			}
		} else {
			if (!l2RerateProcessorDTO.getT5679IO().getSetRidPremStat().trim().isEmpty()) {
				covrpf.setPstatcode(l2RerateProcessorDTO.getT5679IO().getSetRidPremStat());
			}
		}
		return false;
	}

	
	private L2RerateProcessorDTO validateCovrpf(Covrpf covrpf,L2RerateProcessorDTO l2RerateProcessorDTO) {
		l2RerateProcessorDTO.setCovrValid(false);
		l2RerateProcessorDTO.setCovrsLive(false);
		if (covrpf.getRrtdat() != 0 && "1".equals(covrpf.getValidflag())) {
			if (covrpf.getPcesdte() >= l2RerateProcessorDTO.getBillDate()) {
				l2RerateProcessorDTO.setCovrsLive(true);
			}
			if (covrpf.getRrtdat() <= l2RerateProcessorDTO.getBillDate()) {
				for (int wsaaIndex = 0; wsaaIndex < 12; wsaaIndex++) {
					if (covrpf.getStatcode().equals(l2RerateProcessorDTO.getT5679IO().getCovRiskStats().get(wsaaIndex))) {
						for (int wsbbIndex = 0; wsbbIndex < 12; wsbbIndex++) {
							if (covrpf.getPstatcode().equals(l2RerateProcessorDTO.getT5679IO().getCovPremStats().get(wsbbIndex))) {
								l2RerateProcessorDTO.setCovrValid(true);
								break;
							}
						}
						if (l2RerateProcessorDTO.isCovrValid()) {
							break;
						}
					}
				}
			}
		}
		if (!l2RerateProcessorDTO.isCovrsLive()) {
			return l2RerateProcessorDTO;
		}
		String keyItemitem = covrpf.getCrtable().trim();
		List<Itempf> tr517List = itempfService.readTR517(keyItemitem);
		if (tr517List != null && !tr517List.isEmpty()) {
			l2RerateProcessorDTO.setCovrValid(false);
		}
		return l2RerateProcessorDTO;
	}
	private L2RerateProcessorDTO checkLexts(Covrpf covrpf, L2RerateProcessorDTO l2RerateProcessorDTO) {
		Covrpf covrpfInsert = new Covrpf(covrpf);
		int rateFrom = 0;
		if (l2RerateProcessorDTO.getT5687IO().getRtrnwfreq() == 0) {
			rateFrom = covrpf.getPcesdte();
		} else {
			rateFrom = datcon2.plusYears(l2RerateProcessorDTO.getLastRrtDate(), l2RerateProcessorDTO.getT5687IO().getRtrnwfreq());
		}
		List<Lextpf> lextpfList = lextpfDAO.getLextpfListRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),
				covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider());
		for (Lextpf l : lextpfList) {
			if (l.getEcesdte() < rateFrom && l.getEcesdte() > covrpf.getRrtdat()) {
				rateFrom = l.getEcesdte();
			}
		}
		covrpfInsert.setRrtdat(rateFrom);
		covrpfInsert.setRrtfrm(covrpf.getRrtdat());
		if (l2RerateProcessorDTO.getT5687IO().getPremGuarPeriod() == 0) {
			covrpf.setRrtfrm(covrpf.getRrtdat());
		} else {
			int intDate2 = datcon2.plusYears(covrpf.getCrrcd(), l2RerateProcessorDTO.getT5687IO().getPremGuarPeriod());
			if (intDate2 < covrpf.getRrtdat()) {
				covrpf.setRrtfrm(covrpf.getRrtdat());
			} else {
				covrpf.setRrtfrm(covrpf.getCrrcd());
			}
		}
		if (l2RerateProcessorDTO.getT5687IO().getPremmeth() != null && !l2RerateProcessorDTO.getT5687IO().getPremmeth().trim().isEmpty()) {
			l2RerateProcessorDTO.setInstPrem(l2RerateProcessorDTO.getPremiumrec().getCalcPrem().divide(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolinc())));
			l2RerateProcessorDTO.setZbinstprem(l2RerateProcessorDTO.getPremiumrec().getCalcBasPrem().divide(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolinc())));
			l2RerateProcessorDTO.setZlinstprem(l2RerateProcessorDTO.getPremiumrec().getCalcLoaPrem().divide(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolinc())));
			if (covrpf.getPlnsfx() == 0 && l2RerateProcessorDTO.getChdrpf().getPolinc() != 1) {
				l2RerateProcessorDTO.setInstPrem(l2RerateProcessorDTO.getInstPrem().multiply(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolsum())));
				l2RerateProcessorDTO.setZbinstprem(l2RerateProcessorDTO.getZbinstprem().multiply(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolsum())));
				l2RerateProcessorDTO.setZlinstprem(l2RerateProcessorDTO.getZlinstprem().multiply(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolsum())));
			}
			if (l2RerateProcessorDTO.getInstPrem().compareTo(BigDecimal.ZERO) != 0) {
				l2RerateProcessorDTO.setInstPrem(callRounding(l2RerateProcessorDTO.getInstPrem(),l2RerateProcessorDTO));
			}
			if (l2RerateProcessorDTO.getZbinstprem().compareTo(BigDecimal.ZERO) != 0) {
				l2RerateProcessorDTO.setZbinstprem(callRounding(l2RerateProcessorDTO.getZbinstprem(),l2RerateProcessorDTO));
			}
			if (l2RerateProcessorDTO.getZlinstprem().compareTo(BigDecimal.ZERO) != 0) {
				l2RerateProcessorDTO.setZlinstprem(callRounding(l2RerateProcessorDTO.getZlinstprem(),l2RerateProcessorDTO));
			}
			l2RerateProcessorDTO.setPremDiff((l2RerateProcessorDTO.getPremDiff() == null ? BigDecimal.ZERO : l2RerateProcessorDTO.getPremDiff())
					.add(l2RerateProcessorDTO.getInstPrem().subtract(covrpf.getInstprem())));
			if (!"Y".equals(l2RerateProcessorDTO.getZsredtrm())) {
				covrpfInsert.setInstprem(l2RerateProcessorDTO.getInstPrem());
			}
			covrpfInsert.setInstprem(l2RerateProcessorDTO.getInstPrem());
			covrpfInsert.setZbinstprem(l2RerateProcessorDTO.getZbinstprem());
			covrpfInsert.setZlinstprem(l2RerateProcessorDTO.getZlinstprem());

			l2RerateProcessorDTO = checkForIncrease(covrpfInsert, l2RerateProcessorDTO);
			if (covrpfInsert.getRrtdat() > covrpfInsert.getPcesdte()) {
				covrpfInsert.setRrtdat(covrpfInsert.getPcesdte());
			}
			if (covrpfInsert.getRrtfrm() > covrpfInsert.getPcesdte()) {
				covrpfInsert.setRrtfrm(covrpfInsert.getPcesdte());
			}
			if ("Y".equals(l2RerateProcessorDTO.getZsredtrm())) {
				long duration;
				try {
					duration = datcon3.getYearsDifference(l2RerateProcessorDTO.getChdrpf().getOccdate(), covrpf.getRrtdat());
				} catch (ParseException e) {
					throw new SubroutineIOException("Br613: IOException in datcon3: " + e.getMessage());
				}
				List<Mbnspf> mbnspfList = mbnspfDAO.getMbnspfRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),
						covrpf.getCoverage(), covrpf.getRider(), (int) duration);

				if (mbnspfList != null && !mbnspfList.isEmpty()) {
					l2RerateProcessorDTO.setSumins(mbnspfList.get(0).getSumins());
				}
				updateUndr(covrpfInsert,l2RerateProcessorDTO);
				covrpfInsert.setSumins(l2RerateProcessorDTO.getSumins());
			}
			covrpfInsert.setValidflag("1");
			l2RerateProcessorDTO.setNewTranno(l2RerateProcessorDTO.getChdrpf().getTranno() + 1);
			covrpfInsert.setTranno(l2RerateProcessorDTO.getNewTranno());
			covrpfInsert.setCurrfrom(l2RerateProcessorDTO.getRerateStore());
			covrpfInsert.setCurrto(99999999);
			covrpfInsert.setRiskprem(l2RerateProcessorDTO.getPremiumrec().getRiskPrem() == null ? BigDecimal.ZERO : l2RerateProcessorDTO.getPremiumrec().getRiskPrem());
			if (l2RerateProcessorDTO.isStampDutyflag()) {
				covrpfInsert.setZstpduty01(l2RerateProcessorDTO.getPremiumrec().getZstpduty01());
				covrpfInsert.setZclstate(l2RerateProcessorDTO.getPremiumrec().getRstate01());
			}
			l2RerateProcessorDTO.setCovrInsert(covrpfInsert);
			l2RerateProcessorDTO.getL2RerateControlTotalDTO().setControlTotal02(BigDecimal.ONE);
		}
		return l2RerateProcessorDTO;
	}
	private BigDecimal callRounding(BigDecimal amountIn,L2RerateProcessorDTO l2RerateProcessorDTO) {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(l2RerateProcessorDTO.getChdrpf().getCntcurr());
		zrdecplDTO.setCompany(Companies.LIFE);
		zrdecplDTO.setBatctrcde(l2RerateProcessorDTO.getBatchTrcde());
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = zrdecplc.convertAmount(zrdecplDTO);
		} catch (IOException e) {
			throw new ItemParseException("Br613: Parsed failed in zrdecplc:" + e.getMessage());
		}
		return result;
	}
	private L2RerateProcessorDTO checkForIncrease(Covrpf covrpf, L2RerateProcessorDTO l2RerateProcessorDTO) {
		Incrpf incrhstIO = null;
		List<Incrpf> incrpfList = incrpfDAO.readInValidIncrpf(covrpf.getChdrcoy(), covrpf.getChdrnum(),
				covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlnsfx());
		if (incrpfList != null && !incrpfList.isEmpty()) {
			incrhstIO = incrpfList.get(0);
		}

		if (incrhstIO == null) {
			return l2RerateProcessorDTO;
		}
		incrhstIO.setLastInst(incrhstIO.getNewinst());
		incrhstIO.setZblastinst(incrhstIO.getZbnewinst());
		incrhstIO.setZllastinst(incrhstIO.getZlnewinst());
		incrhstIO.setNewinst(incrhstIO.getNewinst().add(covrpf.getInstprem().subtract(covrpf.getInstprem())));
		incrhstIO.setZbnewinst(incrhstIO.getZbnewinst().add(covrpf.getZbinstprem().subtract(covrpf.getZbinstprem())));
		incrhstIO.setZlnewinst(incrhstIO.getZlnewinst().add(covrpf.getZlinstprem().subtract(covrpf.getZlinstprem())));
		incrhstIO.setTranno(l2RerateProcessorDTO.getChdrpf().getTranno() + 1);
		incrhstIO.setStatcode(covrpf.getStatcode());
		incrhstIO.setPstatcode(covrpf.getPstatcode());
		incrhstIO.setCrrcd(l2RerateProcessorDTO.getEffDate());
		incrhstIO.setTransactionDate(l2RerateProcessorDTO.getTransactionDate());
		incrhstIO.setTransactionTime(l2RerateProcessorDTO.getTransactionTime());
		incrhstIO.setTermid("");
		incrhstIO.setUser(0);
		incrhstIO.setZstpduty01(incrhstIO.getNewinst().add(covrpf.getZstpduty01().subtract(covrpf.getZstpduty01())));
		incrhstIO.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
		incrhstIO.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setIncrInsert(incrhstIO);
		return l2RerateProcessorDTO;
	}
	private void updateUndr(Covrpf covrpf,L2RerateProcessorDTO l2RerateProcessorDTO) {

		CrtundwrtDTO crtundwrec = new CrtundwrtDTO();
		crtundwrec.setClntnum(l2RerateProcessorDTO.getLifepf().getLifcnum());
		crtundwrec.setCoy(covrpf.getChdrcoy());
		crtundwrec.setCurrcode(l2RerateProcessorDTO.getChdrpf().getCntcurr());
		crtundwrec.setChdrnum(covrpf.getChdrnum());
		crtundwrec.setCrtable(covrpf.getCrtable());
		crtundwrec.setCnttyp(l2RerateProcessorDTO.getChdrpf().getCnttype());
		crtundwrec.setFunction("DEL");
		try {
			crtundwrt.processCrtundwrt(crtundwrec);
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Br613: Exception in crtundwrt: " + e.getMessage());
		}

		crtundwrec.setClntnum(l2RerateProcessorDTO.getLifepf().getLifcnum());
		crtundwrec.setCoy(covrpf.getChdrcoy());
		crtundwrec.setChdrnum(covrpf.getChdrnum());
		crtundwrec.setLife(covrpf.getLife());
		crtundwrec.setCrtable(covrpf.getCrtable());
		crtundwrec.setBatctrcde(l2RerateProcessorDTO.getBatchTrcde());
		crtundwrec.setSumins(l2RerateProcessorDTO.getSumins());
		crtundwrec.setCnttyp(l2RerateProcessorDTO.getChdrpf().getCnttype());
		crtundwrec.setCurrcode(l2RerateProcessorDTO.getChdrpf().getCntcurr());
		crtundwrec.setFunction("ADD");
		try {
			crtundwrt.processCrtundwrt(crtundwrec);
		} catch (NumberFormatException | ParseException | IOException e) {
			throw new SubroutineIOException("Br613: Exception in crtundwrt: " + e.getMessage());
		}
	}
	private void statistics(L2RerateProcessorDTO l2RerateProcessorDTO) {
		LifsttrDTO lifsttrrec = new LifsttrDTO();
		lifsttrrec.setBatccoy(Companies.LIFE);
		lifsttrrec.setBatcbrn(l2RerateProcessorDTO.getBatcbrn());
		lifsttrrec.setBatcacty(l2RerateProcessorDTO.getActmonth());
		lifsttrrec.setBatcactmn(l2RerateProcessorDTO.getActmonth());
		lifsttrrec.setBatctrcde(l2RerateProcessorDTO.getBatchTrcde());
		lifsttrrec.setBatcbatch(l2RerateProcessorDTO.getBatch());
		lifsttrrec.setChdrcoy(l2RerateProcessorDTO.getChdrpf().getChdrcoy());
		lifsttrrec.setChdrnum(l2RerateProcessorDTO.getChdrpf().getChdrnum());
		lifsttrrec.setTranno(l2RerateProcessorDTO.getNewTranno());
		lifsttrrec.setTrannor(99999);
		lifsttrrec.setAgntnum("");
		lifsttrrec.setOldAgntnum("");
		try {
			lifsttr.processLifsttr(lifsttrrec);
		} catch (IOException e) {
			throw new SubroutineIOException("Br613: IOException in lifsttr: " + e.getMessage());
		}
	}
	private L2RerateProcessorDTO payr(Covrpf covrpf,L2RerateProcessorDTO l2RerateProcessorDTO) throws ParseException {
		Payrpf payrIO = payrpfDAO.getPayrRecord(l2RerateProcessorDTO.getChdrcoy(), l2RerateProcessorDTO.getChdrnum());
		if (payrIO == null) {
			throw new StopRunException(
					"Br613: Exception in getPayrpf, payr record not found for" + l2RerateProcessorDTO.getChdrnum());
		}
		l2RerateProcessorDTO.setPayrpf(payrIO);
		l2RerateProcessorDTO.setBillfreqx(payrIO.getBillfreq());
		if (l2RerateProcessorDTO.getT5687IO().getPremmeth().isEmpty()) {
			return l2RerateProcessorDTO;
		}
		PremiumDTO premiumrec = l2RerateProcessorDTO.getPremiumrec();
		premiumrec.setCrtable(covrpf.getCrtable());
		premiumrec.setChdrChdrcoy(covrpf.getChdrcoy());
		premiumrec.setChdrChdrnum(covrpf.getChdrnum());
		premiumrec.setLifeLife(covrpf.getLife());
		premiumrec.setLifeJlife(covrpf.getJlife());
		premiumrec.setCovrRider(covrpf.getRider());
		premiumrec.setCovrCoverage(covrpf.getCoverage());
		premiumrec.setMop(payrIO.getBillchnl());
		premiumrec.setBillfreq(payrIO.getBillfreq());
		premiumrec.setEffectdt(l2RerateProcessorDTO.getLastRrtDate());
		premiumrec.setMortcls(covrpf.getMortcls());
		premiumrec.setCurrcode(covrpf.getPrmcur());
		premiumrec.setTermdate(covrpf.getPcesdte());
		
		long freqFactor;
		try {
			freqFactor = datcon3.getYearsDifference(premiumrec.getEffectdt(), premiumrec.getTermdate());
		} catch (ParseException e1) {
			throw new SubroutineIOException("Br613: IOException in datcon3: " + e1.getMessage());
		}
		premiumrec.setDuration((int) freqFactor);
		premiumrec.setRatingdate(covrpf.getRrtfrm());
		premiumrec.setReRateDate(covrpf.getRrtdat());
		premiumrec.setCalcPrem(covrpf.getInstprem());
		premiumrec.setCalcBasPrem(covrpf.getZbinstprem());
		premiumrec.setCalcLoaPrem(covrpf.getZlinstprem());
		if (covrpf.getPlnsfx() == 0 && l2RerateProcessorDTO.getChdrpf().getPolinc() != 1) {
			premiumrec.setSumin(covrpf.getSumins().divide(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolsum())));
		} else {
			premiumrec.setSumin(covrpf.getSumins());
		}
		premiumrec.setSumin(premiumrec.getSumin().multiply(new BigDecimal(l2RerateProcessorDTO.getChdrpf().getPolinc())));
		if ("Y".equals(l2RerateProcessorDTO.getZsredtrm())) {
			return l2RerateProcessorDTO;
		}
		premiumrec.setFunction("CALC");
		premiumrec = getAnny(covrpf,premiumrec);
		premiumrec.setLanguage(l2RerateProcessorDTO.getLang());

		if (!(l2RerateProcessorDTO.isVpmsFlag() && (vpmscaller.checkVPMSModel(l2RerateProcessorDTO.getT5675IO().getPremsubr().trim())
				|| l2RerateProcessorDTO.getT5675IO().getPremsubr().trim().equals("PMEX")))) {
			l2RerateProcessorDTO = callT5675Sub(l2RerateProcessorDTO);
		} else {
			l2RerateProcessorDTO.setVpxlextDTO(vpxlext.processVpxlext("INIT", premiumrec));
			VpxchdrDTO vpxchdrPojo = vpxchdr.callInit(premiumrec);
			premiumrec.setRstaflag(vpxchdrPojo.getRstaflag());
			premiumrec.setCnttype(l2RerateProcessorDTO.getChdrpf().getCnttype());
			try {
				l2RerateProcessorDTO.setVpxacblDTO(vpxacbl.processVpxacbl(premiumrec));
			} catch (IOException e) {
				throw new SubroutineIOException("Br613: IOException in vpxacbl: " + e.getMessage());
			}
			boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", "IT");
			boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", "IT");
			boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", "IT");

			if (incomeProtectionflag || premiumflag || dialdownFlag) {
				Rcvdpf rcvdPFObject = new Rcvdpf();
				rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
				rcvdPFObject.setChdrnum(covrpf.getChdrnum());
				rcvdPFObject.setLife(covrpf.getLife());
				rcvdPFObject.setCoverage(covrpf.getCoverage());
				rcvdPFObject.setRider(covrpf.getRider());
				rcvdPFObject.setCrtable(covrpf.getCrtable());
				rcvdPFObject = rcvdpfDAO.readRcvdpf(rcvdPFObject);
				if (rcvdPFObject != null) {
					premiumrec.setBentrm(rcvdPFObject.getBentrm());
					if (rcvdPFObject.getPrmbasis() != null && "S".equals(rcvdPFObject.getPrmbasis())) {
						premiumrec.setPrmbasis("Y");
					} else {
						premiumrec.setPrmbasis("");
					}
					premiumrec.setPoltyp(rcvdPFObject.getPoltyp());
					premiumrec.setOccpcode("");
					premiumrec.setDialdownoption(rcvdPFObject.getDialdownoption());
					premiumrec.setWaitperiod(rcvdPFObject.getWaitperiod());
				}
			}
			if (l2RerateProcessorDTO.getT5675IO().getPremsubr().trim().equals("PMEX")) {
				premiumrec.setSetPmexCall("Y");
				premiumrec.setUpdateRequired("N");
			}
			l2RerateProcessorDTO = callT5675Sub(l2RerateProcessorDTO);
		}
		premiumrec = l2RerateProcessorDTO.getPremiumrec();
		premiumrec.setRiskPrem(BigDecimal.ZERO);
		boolean riskPremflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP094", "IT");
		if (riskPremflag) {
			premiumrec.setCnttype(l2RerateProcessorDTO.getChdrpf().getCnttype());
			premiumrec.setCrtable(covrpf.getCrtable());
			premiumrec.setCalcTotPrem(premiumrec.getCalcPrem());
			// callProgram("RISKPREMIUM", premiumrec.premiumRec);
			VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
			vpmsinfoDTO.setCoy(l2RerateProcessorDTO.getChdrpf().getChdrcoy());
			vpmsinfoDTO.setTransDate(l2RerateProcessorDTO.getEffDate() + "");
			vpmsinfoDTO.setModelName("RISKPREMIUM");
			vpmscaller.callPRMPMREST(vpmsinfoDTO, premiumrec, null, null);
		}
		if (!"****".equals(premiumrec.getStatuz())) {
			fatalError("Br613: Exception in premiumrec:" + premiumrec.getStatuz());
		}
		l2RerateProcessorDTO.setPremiumrec(premiumrec);
		return l2RerateProcessorDTO;
	}
	private L2RerateProcessorDTO callT5675Sub(L2RerateProcessorDTO l2RerateProcessorDTO) throws ParseException {
		PremiumDTO premiumrec = l2RerateProcessorDTO.getPremiumrec();
		if (!l2RerateProcessorDTO.isVpmsFlag()) {
			if ("PRMPM01".equals(l2RerateProcessorDTO.getT5675IO().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm01(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("Br613: IOException in Prmpm01: " + e.getMessage());
				}
			} else if ("PRMPM03".equals(l2RerateProcessorDTO.getT5675IO().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm03(premiumrec);
				} catch (IOException | ParseException e) {
					throw new SubroutineIOException("Br613: IOException in Prmpm03: " + e.getMessage());
				}
			} else if ("PRMPM17".equals(l2RerateProcessorDTO.getT5675IO().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm17(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("Br613: IOException in Prmpm17: " + e.getMessage());
				}
			} else if ("PRMPM04".equals(l2RerateProcessorDTO.getT5675IO().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm04(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("Br613: IOException in Prmpm04: " + e.getMessage());
				} 
				
			}else if ("PRMPM18".equals(l2RerateProcessorDTO.getT5675IO().getPremsubr())) {
				try {
					premiumrec = premium.processPrmpm18(premiumrec);
				} catch (IOException e) {
					throw new SubroutineIOException("Br613: IOException in Prmpm18: " + e.getMessage());
				}
			}
		} else {
			VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
			vpmsinfoDTO.setCoy(l2RerateProcessorDTO.getChdrpf().getChdrcoy());
			vpmsinfoDTO.setTransDate(l2RerateProcessorDTO.getEffDate() + "");
			vpmsinfoDTO.setModelName(l2RerateProcessorDTO.getT5675IO().getPremsubr());
			vpmscaller.callPRMPMREST(vpmsinfoDTO, premiumrec, l2RerateProcessorDTO.getVpxacblDTO(),l2RerateProcessorDTO.getVpxlextDTO());
		}
		return l2RerateProcessorDTO;
	}
	
	private PremiumDTO getAnny(Covrpf covrpf,PremiumDTO premiumrec) {
		Annypf a = new Annypf();
		a.setChdrcoy(covrpf.getChdrcoy());
		a.setLife(covrpf.getLife());
		a.setCoverage(covrpf.getCoverage());
		a.setRider(covrpf.getRider());
		a.setPlnsfx(covrpf.getPlnsfx());
		List<Annypf> annyList = annypfDAO.getAnnypfRecord(a);
		Annypf annyIO = null;
		if (annyList != null && !annyList.isEmpty()) {
			annyIO = annyList.get(0);
		}
		if (annyIO != null) {
			premiumrec.setFreqann(annyIO.getFreqann());
			premiumrec.setAdvance(annyIO.getAdvance());
			premiumrec.setArrears(annyIO.getArrears());
			premiumrec.setGuarperd(annyIO.getGuarperd());
			premiumrec.setIntanny(annyIO.getIntanny());
			premiumrec.setCapcont(annyIO.getCapcont());
			premiumrec.setWithprop(annyIO.getWithprop());
			premiumrec.setWithoprop(annyIO.getWithoprop());
			premiumrec.setPpind(annyIO.getPpind());
			premiumrec.setNomlife(annyIO.getNomlife());
			premiumrec.setDthpercn(annyIO.getDthpercn());
			premiumrec.setDthperco(annyIO.getDthperco());
		} else {
			premiumrec.setAdvance("");
			premiumrec.setArrears("");
			premiumrec.setFreqann("");
			premiumrec.setWithprop("");
			premiumrec.setWithoprop("");
			premiumrec.setPpind("");
			premiumrec.setNomlife("");
			premiumrec.setGuarperd(0);
			premiumrec.setIntanny(BigDecimal.ZERO);
			premiumrec.setCapcont(BigDecimal.ZERO);
			premiumrec.setDthpercn(BigDecimal.ZERO);
			premiumrec.setDthperco(BigDecimal.ZERO);
		}
		return premiumrec;
	}
	private L2RerateProcessorDTO genericProcessing(Covrpf covrpf,L2RerateProcessorDTO l2RerateProcessorDTO) {
		String t5671Item = "B672" + covrpf.getCrtable();
		T5671 t5671IO = null;
		try {
			t5671IO = itempfService.readT5671(Companies.LIFE, t5671Item);
		} catch (IOException e) {
			throw new ItemParseException("Item parsed failed in Table T5687: " + t5671Item);
		}
		l2RerateProcessorDTO.setT5671IO(t5671IO);
		IsuallDTO isuallrec = new IsuallDTO();
		isuallrec.setFunction("ACTIN");
		isuallrec.setCompany(covrpf.getChdrcoy());
		isuallrec.setChdrnum(covrpf.getChdrnum());
		isuallrec.setLife(covrpf.getLife());
		isuallrec.setCoverage(covrpf.getCoverage());
		isuallrec.setRider(covrpf.getRider());
		isuallrec.setPlanSuffix(covrpf.getPlnsfx());
		isuallrec.setOldcovr(covrpf.getCoverage());
		isuallrec.setOldrider(covrpf.getRider());
		if ("Y".equals(l2RerateProcessorDTO.getFullyPaid())) {
			isuallrec.setFreqFactor(BigDecimal.ZERO);
		} else {
			isuallrec.setFreqFactor(BigDecimal.ONE);
		}
		isuallrec.setBatcpfx(l2RerateProcessorDTO.getPrefix());
		isuallrec.setBatccoy(Companies.LIFE);
		isuallrec.setBatcbrn(l2RerateProcessorDTO.getBatcbrn());
		isuallrec.setBatcactyr(l2RerateProcessorDTO.getActyear());
		isuallrec.setBatcactmn(l2RerateProcessorDTO.getActmonth());
		isuallrec.setBatctrcde(l2RerateProcessorDTO.getBatchTrcde());
		isuallrec.setBatcbatch(l2RerateProcessorDTO.getBatch());
		isuallrec.setTransactionDate(l2RerateProcessorDTO.getTransactionDate());
		isuallrec.setTransactionTime(l2RerateProcessorDTO.getTransactionTime());
		isuallrec.setUser(0);
		isuallrec.setTermid("");
		isuallrec.setEffdate(covrpf.getCrrcd());
		isuallrec.setNewTranno(covrpf.getTranno());
		isuallrec.setCovrSingp(BigDecimal.ZERO);
		isuallrec.setCovrInstprem(covrpf.getInstprem());
		isuallrec.setLanguage(l2RerateProcessorDTO.getLang());
		if (l2RerateProcessorDTO.getChdrpf().getPolinc() > 1) {
			isuallrec.setConvertUnlt("Y");
		}
		//call UNLCHG,GRVULCHG, but it's blank for TEN&RUL, just ignore temporarily
//		if (t5671IO != null) {
//			List<String> subprogs = t5671IO.getSubprogs();
//			if (subprogs != null && !subprogs.isEmpty()) {
//				for (String subprog : subprogs) {
//					
//				}
//			}
//		}
		return l2RerateProcessorDTO;
	}
	private L2RerateProcessorDTO updateChdrPtrn(L2RerateProcessorDTO l2RerateProcessorDTO) {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrpfx(l2RerateProcessorDTO.getChdrpf().getChdrpfx());
		ptrnIO.setChdrcoy(l2RerateProcessorDTO.getChdrcoy());
		ptrnIO.setChdrnum(l2RerateProcessorDTO.getChdrpf().getChdrnum());
		ptrnIO.setTranno(l2RerateProcessorDTO.getNewTranno());
		ptrnIO.setPtrneff(l2RerateProcessorDTO.getEffDate());
		ptrnIO.setTrdt(l2RerateProcessorDTO.getTransactionDate());
		ptrnIO.setTrtm(l2RerateProcessorDTO.getTransactionTime());
		ptrnIO.setValidflag("1");
		ptrnIO.setUserT(0);
		ptrnIO.setTermid("");
		ptrnIO.setBatcpfx(l2RerateProcessorDTO.getPrefix());
		ptrnIO.setBatccoy(Companies.LIFE);
		ptrnIO.setBatcbrn(l2RerateProcessorDTO.getBatcbrn());
		ptrnIO.setBatcactyr(l2RerateProcessorDTO.getActyear());
		ptrnIO.setBatctrcde(l2RerateProcessorDTO.getBatchTrcde());
		ptrnIO.setBatcactmn(l2RerateProcessorDTO.getActmonth());
		ptrnIO.setBatcbatch(l2RerateProcessorDTO.getBatch());
		ptrnIO.setDatesub(l2RerateProcessorDTO.getEffDate());
		ptrnIO.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
		ptrnIO.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setPtrnInsert(ptrnIO);
		Chdrpf chdrpf = getCopyChdrpf(l2RerateProcessorDTO.getChdrpf());
		chdrpf.setValidflag("2");
		chdrpf.setCurrto(l2RerateProcessorDTO.getRerateStore());
		chdrpf.setUniqueNumber(l2RerateProcessorDTO.getChdrpf().getUniqueNumber());
		chdrpf.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
		chdrpf.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setChdrpfUpdate(chdrpf);
		Chdrpf chdrpfInsert = l2RerateProcessorDTO.getChdrpf();
		chdrpfInsert.setValidflag("1");
		chdrpfInsert.setCurrto(99999999);
		chdrpfInsert.setCurrfrom(l2RerateProcessorDTO.getRerateStore());
		chdrpfInsert.setTranno(l2RerateProcessorDTO.getNewTranno());
		chdrpfInsert.setSinstamt01(chdrpfInsert.getSinstamt01().add(l2RerateProcessorDTO.getPremDiff()));
		if (!l2RerateProcessorDTO.isCovrsLive()) {
			l2RerateProcessorDTO.setPremDiff(l2RerateProcessorDTO.getPremDiff().subtract(chdrpfInsert.getSinstamt02()));
			chdrpfInsert.setSinstamt02(BigDecimal.ZERO);
			if (l2RerateProcessorDTO.getT5679IO().getSetCnPremStat() != null && !l2RerateProcessorDTO.getT5679IO().getSetCnPremStat().trim().isEmpty()) {
				chdrpfInsert.setPstcde(l2RerateProcessorDTO.getT5679IO().getSetCnPremStat());
			}
		}
		chdrpfInsert.setSinstamt06(chdrpfInsert.getSinstamt06().add(l2RerateProcessorDTO.getPremDiff()));
		chdrpfInsert.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
		chdrpfInsert.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setChdrpfInsert(chdrpfInsert);
		l2RerateProcessorDTO.getL2RerateControlTotalDTO().setControlTotal03(l2RerateProcessorDTO.getPremDiff());
		return l2RerateProcessorDTO;
	}
	private Chdrpf getCopyChdrpf(Chdrpf chdrpf) {
		Chdrpf chdrpfNew = new Chdrpf();
		chdrpfNew.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpfNew.setChdrpfx(chdrpf.getChdrpfx());
		chdrpfNew.setChdrcoy(chdrpf.getChdrcoy());
		chdrpfNew.setChdrnum(chdrpf.getChdrnum());
		chdrpfNew.setRecode(chdrpf.getRecode());
		chdrpfNew.setServunit(chdrpf.getServunit());
		chdrpfNew.setCnttype(chdrpf.getCnttype());
		chdrpfNew.setTranno(chdrpf.getTranno());
		chdrpfNew.setTranid(chdrpf.getTranid());
		chdrpfNew.setValidflag(chdrpf.getValidflag());
		chdrpfNew.setCurrfrom(chdrpf.getCurrfrom());
		chdrpfNew.setCurrto(chdrpf.getCurrto());
		chdrpfNew.setProctrancd(chdrpf.getProctrancd());
		chdrpfNew.setProcflag(chdrpf.getProcflag());
		chdrpfNew.setProcid(chdrpf.getProcid());
		chdrpfNew.setStatcode(chdrpf.getStatcode());
		chdrpfNew.setStatreasn(chdrpf.getStatreasn());
		chdrpfNew.setStatdate(chdrpf.getStatdate());
		chdrpfNew.setStattran(chdrpf.getStattran());
		chdrpfNew.setTranlused(chdrpf.getTranlused());
		chdrpfNew.setOccdate(chdrpf.getOccdate());
		chdrpfNew.setCcdate(chdrpf.getCcdate());
		chdrpfNew.setCrdate(chdrpf.getCrdate());
		chdrpfNew.setAnnamt01(chdrpf.getAnnamt01());
		chdrpfNew.setAnnamt02(chdrpf.getAnnamt02());
		chdrpfNew.setAnnamt03(chdrpf.getAnnamt03());
		chdrpfNew.setAnnamt04(chdrpf.getAnnamt04());
		chdrpfNew.setAnnamt05(chdrpf.getAnnamt05());
		chdrpfNew.setAnnamt06(chdrpf.getAnnamt06());
		chdrpfNew.setRnltype(chdrpf.getRnltype());
		chdrpfNew.setRnlnots(chdrpf.getRnlnots());
		chdrpfNew.setRnlnotto(chdrpf.getRnlnotto());
		chdrpfNew.setRnlattn(chdrpf.getRnlattn());
		chdrpfNew.setRnldurn(chdrpf.getRnldurn());
		chdrpfNew.setReptype(chdrpf.getReptype());
		chdrpfNew.setRepnum(chdrpf.getRepnum());
		chdrpfNew.setCownpfx(chdrpf.getCownpfx());
		chdrpfNew.setCowncoy(chdrpf.getCowncoy());
		chdrpfNew.setCownnum(chdrpf.getCownnum());
		chdrpfNew.setJownnum(chdrpf.getJownnum());
		chdrpfNew.setPayrpfx(chdrpf.getPayrpfx());
		chdrpfNew.setPayrcoy(chdrpf.getPayrcoy());
		chdrpfNew.setPayrnum(chdrpf.getPayrnum());
		chdrpfNew.setDesppfx(chdrpf.getDesppfx());
		chdrpfNew.setDespcoy(chdrpf.getDespcoy());
		chdrpfNew.setDespnum(chdrpf.getDespnum());
		chdrpfNew.setAsgnpfx(chdrpf.getAsgnpfx());
		chdrpfNew.setAsgncoy(chdrpf.getAsgncoy());
		chdrpfNew.setAsgnnum(chdrpf.getAsgnnum());
		chdrpfNew.setCntbranch(chdrpf.getCntbranch());
		chdrpfNew.setAgntpfx(chdrpf.getAgntpfx());
		chdrpfNew.setAgntcoy(chdrpf.getAgntcoy());
		chdrpfNew.setAgntnum(chdrpf.getAgntnum());
		chdrpfNew.setCntcurr(chdrpf.getCntcurr());
		chdrpfNew.setAcctccy(chdrpf.getAcctccy());
		chdrpfNew.setCrate(chdrpf.getCrate());
		chdrpfNew.setPayplan(chdrpf.getPayplan());
		chdrpfNew.setAcctmeth(chdrpf.getAcctmeth());
		chdrpfNew.setBillfreq(chdrpf.getBillfreq());
		chdrpfNew.setBillchnl(chdrpf.getBillchnl());
		chdrpfNew.setCollchnl(chdrpf.getCollchnl());
		chdrpfNew.setBillday(chdrpf.getBillday());
		chdrpfNew.setBillmonth(chdrpf.getBillmonth());
		chdrpfNew.setBillcd(chdrpf.getBillcd());
		chdrpfNew.setBtdate(chdrpf.getBtdate());
		chdrpfNew.setPtdate(chdrpf.getPtdate());
		chdrpfNew.setPayflag(chdrpf.getPayflag());
		chdrpfNew.setSinstfrom(chdrpf.getSinstfrom());
		chdrpfNew.setSinstto(chdrpf.getSinstto());
		chdrpfNew.setSinstamt01(chdrpf.getSinstamt01());
		chdrpfNew.setSinstamt02(chdrpf.getSinstamt02());
		chdrpfNew.setSinstamt03(chdrpf.getSinstamt03());
		chdrpfNew.setSinstamt04(chdrpf.getSinstamt04());
		chdrpfNew.setSinstamt05(chdrpf.getSinstamt05());
		chdrpfNew.setSinstamt06(chdrpf.getSinstamt06());
		chdrpfNew.setInstfrom(chdrpf.getInstfrom());
		chdrpfNew.setInstto(chdrpf.getInstto());
		chdrpfNew.setInstbchnl(chdrpf.getInstbchnl());
		chdrpfNew.setInstcchnl(chdrpf.getInstcchnl());
		chdrpfNew.setInstfreq(chdrpf.getInstfreq());
		chdrpfNew.setInsttot01(chdrpf.getInsttot01());
		chdrpfNew.setInsttot02(chdrpf.getInsttot02());
		chdrpfNew.setInsttot03(chdrpf.getInsttot03());
		chdrpfNew.setInsttot04(chdrpf.getInsttot04());
		chdrpfNew.setInsttot05(chdrpf.getInsttot05());
		chdrpfNew.setInsttot06(chdrpf.getInsttot06());
		chdrpfNew.setInstpast01(chdrpf.getInstpast01());
		chdrpfNew.setInstpast02(chdrpf.getInstpast02());
		chdrpfNew.setInstpast03(chdrpf.getInstpast03());
		chdrpfNew.setInstpast04(chdrpf.getInstpast04());
		chdrpfNew.setInstpast05(chdrpf.getInstpast05());
		chdrpfNew.setInstpast06(chdrpf.getInstpast06());
		chdrpfNew.setInstjctl(chdrpf.getInstjctl());
		chdrpfNew.setNofoutinst(chdrpf.getNofoutinst());
		chdrpfNew.setOutstamt(chdrpf.getOutstamt());
		chdrpfNew.setBilldate01(chdrpf.getBilldate01());
		chdrpfNew.setBilldate02(chdrpf.getBilldate02());
		chdrpfNew.setBilldate03(chdrpf.getBilldate03());
		chdrpfNew.setBilldate04(chdrpf.getBilldate04());
		chdrpfNew.setBillamt01(chdrpf.getBillamt01());
		chdrpfNew.setBillamt02(chdrpf.getBillamt02());
		chdrpfNew.setBillamt03(chdrpf.getBillamt03());
		chdrpfNew.setBillamt04(chdrpf.getBillamt04());
		chdrpfNew.setFacthous(chdrpf.getFacthous());
		chdrpfNew.setBankkey(chdrpf.getBankkey());
		chdrpfNew.setBankacckey(chdrpf.getBankacckey());
		chdrpfNew.setDiscode01(chdrpf.getDiscode01());
		chdrpfNew.setDiscode02(chdrpf.getDiscode02());
		chdrpfNew.setDiscode03(chdrpf.getDiscode03());
		chdrpfNew.setDiscode04(chdrpf.getDiscode04());
		chdrpfNew.setGrupkey(chdrpf.getGrupkey());
		chdrpfNew.setMembsel(chdrpf.getMembsel());
		chdrpfNew.setAplsupr(chdrpf.getAplsupr());
		chdrpfNew.setAplspfrom(chdrpf.getAplspfrom());
		chdrpfNew.setAplspto(chdrpf.getAplspto());
		chdrpfNew.setBillsupr(chdrpf.getBillsupr());
		chdrpfNew.setBillspfrom(chdrpf.getBillspfrom());
		chdrpfNew.setBillspto(chdrpf.getBillspto());
		chdrpfNew.setCommsupr(chdrpf.getCommsupr());
		chdrpfNew.setCommspfrom(chdrpf.getCommspfrom());
		chdrpfNew.setCommspto(chdrpf.getCommspto());
		chdrpfNew.setLapssupr(chdrpf.getLapssupr());
		chdrpfNew.setLapsspfrom(chdrpf.getLapsspfrom());
		chdrpfNew.setLapsspto(chdrpf.getLapsspto());
		chdrpfNew.setMailsupr(chdrpf.getMailsupr());
		chdrpfNew.setMailspfrom(chdrpf.getMailspfrom());
		chdrpfNew.setMailspto(chdrpf.getMailspto());
		chdrpfNew.setNotssupr(chdrpf.getNotssupr());
		chdrpfNew.setNotsspfrom(chdrpf.getNotsspfrom());
		chdrpfNew.setNotsspto(chdrpf.getNotsspto());
		chdrpfNew.setRnwlsupr(chdrpf.getRnwlsupr());
		chdrpfNew.setRnwlspfrom(chdrpf.getRnwlspfrom());
		chdrpfNew.setRnwlspto(chdrpf.getRnwlspto());
		chdrpfNew.setCampaign(chdrpf.getCampaign());
		chdrpfNew.setSrcebus(chdrpf.getSrcebus());
		chdrpfNew.setNofrisks(chdrpf.getNofrisks());
		chdrpfNew.setJacket(chdrpf.getJacket());
		chdrpfNew.setIsam01(chdrpf.getIsam01());
		chdrpfNew.setIsam02(chdrpf.getIsam02());
		chdrpfNew.setIsam03(chdrpf.getIsam03());
		chdrpfNew.setIsam04(chdrpf.getIsam04());
		chdrpfNew.setIsam05(chdrpf.getIsam05());
		chdrpfNew.setIsam06(chdrpf.getIsam06());
		chdrpfNew.setPstcde(chdrpf.getPstcde());
		chdrpfNew.setPstrsn(chdrpf.getPstrsn());
		chdrpfNew.setPsttrn(chdrpf.getPsttrn());
		chdrpfNew.setPstdat(chdrpf.getPstdat());
		chdrpfNew.setPdind(chdrpf.getPdind());
		chdrpfNew.setReg(chdrpf.getReg());
		chdrpfNew.setStca(chdrpf.getStca());
		chdrpfNew.setStcb(chdrpf.getStcb());
		chdrpfNew.setStcc(chdrpf.getStcc());
		chdrpfNew.setStcd(chdrpf.getStcd());
		chdrpfNew.setStce(chdrpf.getStce());
		chdrpfNew.setMplpfx(chdrpf.getMplpfx());
		chdrpfNew.setMplcoy(chdrpf.getMplcoy());
		chdrpfNew.setMplnum(chdrpf.getMplnum());
		chdrpfNew.setPoapfx(chdrpf.getPoapfx());
		chdrpfNew.setPoacoy(chdrpf.getPoacoy());
		chdrpfNew.setPoanum(chdrpf.getPoanum());
		chdrpfNew.setFinpfx(chdrpf.getFinpfx());
		chdrpfNew.setFincoy(chdrpf.getFincoy());
		chdrpfNew.setFinnum(chdrpf.getFinnum());
		chdrpfNew.setWvfdat(chdrpf.getWvfdat());
		chdrpfNew.setWvtdat(chdrpf.getWvtdat());
		chdrpfNew.setWvfind(chdrpf.getWvfind());
		chdrpfNew.setClupfx(chdrpf.getClupfx());
		chdrpfNew.setClucoy(chdrpf.getClucoy());
		chdrpfNew.setClunum(chdrpf.getClunum());
		chdrpfNew.setPolpln(chdrpf.getPolpln());
		chdrpfNew.setChgflag(chdrpf.getChgflag());
		chdrpfNew.setLaprind(chdrpf.getLaprind());
		chdrpfNew.setSpecind(chdrpf.getSpecind());
		chdrpfNew.setDueflg(chdrpf.getDueflg());
		chdrpfNew.setBfcharge(chdrpf.getBfcharge());
		chdrpfNew.setDishnrcnt(chdrpf.getDishnrcnt());
		chdrpfNew.setPdtype(chdrpf.getPdtype());
		chdrpfNew.setDishnrdte(chdrpf.getDishnrdte());
		chdrpfNew.setStmpdtyamt(chdrpf.getStmpdtyamt());
		chdrpfNew.setStmpdtydte(chdrpf.getStmpdtydte());
		chdrpfNew.setPolinc(chdrpf.getPolinc());
		chdrpfNew.setPolsum(chdrpf.getPolsum());
		chdrpfNew.setNxtsfx(chdrpf.getNxtsfx());
		chdrpfNew.setAvlisu(chdrpf.getAvlisu());
		chdrpfNew.setStmdte(chdrpf.getStmdte());
		chdrpfNew.setBillcurr(chdrpf.getBillcurr());
		chdrpfNew.setTfrswused(chdrpf.getTfrswused());
		chdrpfNew.setTfrswleft(chdrpf.getTfrswleft());
		chdrpfNew.setLastswdate(chdrpf.getLastswdate());
		chdrpfNew.setMandref(chdrpf.getMandref());
		chdrpfNew.setCntiss(chdrpf.getCntiss());
		chdrpfNew.setCntrcv(chdrpf.getCntrcv());
		chdrpfNew.setCoppn(chdrpf.getCoppn());
		chdrpfNew.setCotype(chdrpf.getCotype());
		chdrpfNew.setCovernt(chdrpf.getCovernt());
		chdrpfNew.setDocnum(chdrpf.getDocnum());
		chdrpfNew.setDtecan(chdrpf.getDtecan());
		chdrpfNew.setQuoteno(chdrpf.getQuoteno());
		chdrpfNew.setRnlsts(chdrpf.getRnlsts());
		chdrpfNew.setSustrcde(chdrpf.getSustrcde());
		chdrpfNew.setBankcode(chdrpf.getBankcode());
		chdrpfNew.setPndate(chdrpf.getPndate());
		chdrpfNew.setSubsflg(chdrpf.getSubsflg());
		chdrpfNew.setHrskind(chdrpf.getHrskind());
		chdrpfNew.setSlrypflg(chdrpf.getSlrypflg());
		chdrpfNew.setTakovrflg(chdrpf.getTakovrflg());
		chdrpfNew.setGprnltyp(chdrpf.getGprnltyp());
		chdrpfNew.setGprmnths(chdrpf.getGprmnths());
		chdrpfNew.setCoysrvac(chdrpf.getCoysrvac());
		chdrpfNew.setMrksrvac(chdrpf.getMrksrvac());
		chdrpfNew.setPolschpflg(chdrpf.getPolschpflg());
		chdrpfNew.setAdjdate(chdrpf.getAdjdate());
		chdrpfNew.setPtdateab(chdrpf.getPtdateab());
		chdrpfNew.setLmbrno(chdrpf.getLmbrno());
		chdrpfNew.setLheadno(chdrpf.getLheadno());
		chdrpfNew.setEffdcldt(chdrpf.getEffdcldt());
		chdrpfNew.setPntrcde(chdrpf.getPntrcde());
		chdrpfNew.setTaxflag(chdrpf.getTaxflag());
		chdrpfNew.setAgedef(chdrpf.getAgedef());
		chdrpfNew.setTermage(chdrpf.getTermage());
		chdrpfNew.setPersoncov(chdrpf.getPersoncov());
		chdrpfNew.setEnrolltyp(chdrpf.getEnrolltyp());
		chdrpfNew.setSplitsubs(chdrpf.getSplitsubs());
		chdrpfNew.setDtlsind(chdrpf.getDtlsind());
		chdrpfNew.setZrenno(chdrpf.getZrenno());
		chdrpfNew.setZendno(chdrpf.getZendno());
		chdrpfNew.setZresnpd(chdrpf.getZresnpd());
		chdrpfNew.setZrepolno(chdrpf.getZrepolno());
		chdrpfNew.setZcomtyp(chdrpf.getZcomtyp());
		chdrpfNew.setZrinum(chdrpf.getZrinum());
		chdrpfNew.setZschprt(chdrpf.getZschprt());
		chdrpfNew.setZpaymode(chdrpf.getZpaymode());
		chdrpfNew.setUsrprf(chdrpf.getUsrprf());
		chdrpfNew.setJobnme(chdrpf.getJobnme());
		chdrpfNew.setDatime(chdrpf.getDatime());
		chdrpfNew.setJobnm(chdrpf.getJobnm());
		chdrpfNew.setQuotetype(chdrpf.getQuotetype());
		chdrpfNew.setJobnum(chdrpf.getJobnum());
		chdrpfNew.setNlgflg(chdrpf.getNlgflg());
		return chdrpfNew;
	}
	private L2RerateProcessorDTO updatePayr(L2RerateProcessorDTO l2RerateProcessorDTO) {
		Payrpf payrIO = payrpfDAO.getPayrRecord(l2RerateProcessorDTO.getChdrcoy(), l2RerateProcessorDTO.getChdrnum());
		if (payrIO == null) {
			throw new StopRunException(
					"Br613: Exception in getPayrpf, payr record not found for" + l2RerateProcessorDTO.getChdrnum());
		}
		Payrpf payrpf = new Payrpf(payrIO);
		payrpf.setValidflag("2");
		payrpf.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
		payrpf.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setPayrUpdate(payrpf);
		
		payrIO.setValidflag("1");
		payrIO.setEffdate(l2RerateProcessorDTO.getRerateStore());
		payrIO.setTranno(l2RerateProcessorDTO.getNewTranno());
		payrIO.setSinstamt06(payrIO.getSinstamt06().add(l2RerateProcessorDTO.getPremDiff()));
		if (l2RerateProcessorDTO.isCovrsLive()) {
			payrIO.setSinstamt02(BigDecimal.ZERO);
			payrIO.setSinstamt01(BigDecimal.ZERO);
		} else {
			payrIO.setSinstamt01(payrIO.getSinstamt01().add(l2RerateProcessorDTO.getPremDiff()));
		}
		payrIO.setNextdate(datcon2.plusDays(l2RerateProcessorDTO.getChdrpf().getBillcd(), -1 * l2RerateProcessorDTO.getLeadDays()));
		payrIO.setJobnm(l2RerateProcessorDTO.getBatchName().toUpperCase());
		payrIO.setUsrprf(l2RerateProcessorDTO.getUserName());
		l2RerateProcessorDTO.setPayrInsert(payrIO);
		return l2RerateProcessorDTO;
	}
	
	public void fatalError(String status) {
		throw new StopRunException(status);
	}

}
