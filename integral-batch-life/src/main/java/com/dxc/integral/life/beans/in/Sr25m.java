package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr25m implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private Sr25mscreensfl sr25mscreensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public Sr25mscreensfl getSr25mscreensfl() {
		return sr25mscreensfl;
	}
	public void setSr25mscreensfl(Sr25mscreensfl sr25mscreensfl) {
		this.sr25mscreensfl = sr25mscreensfl;
	}

	public static class Sr25mscreensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		public int size() {
			return rows.length;
		}

		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
		
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String sel = "";
			private String mandref = "";
			private String crdtcard = "";
			private String shortdesc = "";
			private String crcardexpm = "";
			private String crcardexpy = "";
			private String stdescsh = "";
			private String currency = "";
			private String atm = "";
			private String ddop = "";

			public String getSel() {
				return sel;
			}
			public void setSel(String sel) {
				this.sel = sel;
			}
			public String getMandref() {
				return mandref;
			}
			public void setMandref(String mandref) {
				this.mandref = mandref;
			}
			public String getCrdtcard() {
				return crdtcard;
			}
			public void setCrdtcard(String crdtcard) {
				this.crdtcard = crdtcard;
			}
			public String getShortdesc() {
				return shortdesc;
			}
			public void setShortdesc(String shortdesc) {
				this.shortdesc = shortdesc;
			}
			public String getCrcardexpm() {
				return crcardexpm;
			}
			public void setCrcardexpm(String crcardexpm) {
				this.crcardexpm = crcardexpm;
			}
			public String getCrcardexpy() {
				return crcardexpy;
			}
			public void setCrcardexpy(String crcardexpy) {
				this.crcardexpy = crcardexpy;
			}
			public String getStdescsh() {
				return stdescsh;
			}
			public void setStdescsh(String stdescsh) {
				this.stdescsh = stdescsh;
			}
			public String getCurrency() {
				return currency;
			}
			public void setCurrency(String currency) {
				this.currency = currency;
			}
			public String getAtm() {
				return atm;
			}
			public void setAtm(String atm) {
				this.atm = atm;
			}
			public String getDdop() {
				return ddop;
			}
			public void setDdop(String ddop) {
				this.ddop = ddop;
			}
		} 
	}

	@Override
	public String toString() {
		return "Sr25m []";
	}
	
}
