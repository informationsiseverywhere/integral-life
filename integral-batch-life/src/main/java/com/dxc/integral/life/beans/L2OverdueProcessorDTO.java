package com.dxc.integral.life.beans;

import java.util.ArrayList;
import java.util.List;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hpadpf;
import com.dxc.integral.life.dao.model.Hpuapf;
import com.dxc.integral.life.dao.model.LifacmvPojo;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6647;
import com.dxc.integral.life.smarttable.pojo.T6654;
/**
 * DTO for Itemreader
 * 
 * @author wli31
 *
 */
public class L2OverdueProcessorDTO {

	private String chdrcoy;
	private String chdrnum;
	private String statcode;
	private String pstatcode;
	private String billchnl;
	private int btdate;
	private int ptdate;
	private int effDate;
	private int wsaaSub;
	private String lang;
	private String prefix;
	private String batch;
	private String batchcoy;
	private String batcbrn;
	private String batchTrcde;
	private int actyear;
	private int actmonth;
	private String batchName;
	private String userName;
//	private List<T6647Inner> t6647Inner;
	private T5687 t5687IO;
	private List<T6597Inner> t6597Inner;
//	private List<T6654Inner> t6654Inner;
	private L2OverdueControlTotalDTO l2OverdueControlTotalDTO;
	private int transactionDate;
	private int transactionTime;
	private String prevChdrnum;
	private List<T6647> t6647IOList;
	private T5679 t5679IO;
	private T6654 t6654IO;
	private LifacmvPojo lifacmvPojo;
	private Ptrnpf ptrnpf;
	private Chdrpf chdrpf;
	private Payrpf payrpf;
	private boolean validFlag;
	private boolean updateRequired;
	private int maxAge;
	private int maxYear;
	private int overdueDays;
	private int durationInForce;
	private String nlgflag;
	private String wssaType;
	private String lastPtrn;
	private boolean ignoreFlag;
	private boolean skipContract;
	private boolean aplFlag;
	private int t6654SubrIx;
	private int t6654ExpyIx;
//	private int t6654Ix;
	private int t6597Ix;
	private Chdrpf  chdrpfUpdate;
	private Chdrpf  chdrpfUpdate2;
	private Chdrpf  chdrpfInsert;
	private Payrpf payrInsert;
	private Payrpf payrUpdate;
	private Payrpf payrPstcde;
	private List<Covrpf> covrUpdate;
	private List<Covrpf> covrInsert;
	private List<Hpuapf> hpuapfUpdate = new ArrayList<>();
	private List<Hpuapf> hpuapfInsert = new ArrayList<>();
	private Ptrnpf ptrnInsert;
	private OvrdueDTO ovrdueDTO = new OvrdueDTO();
	private NlgcalcDTO nlgcalcDTO = new NlgcalcDTO();
	private Hpadpf hpadpf;
	private boolean chdrOk ;
	private NfloanDTO nfloanDTO;
	private Covrpf covrpf;
	private String covrrnlStatiiSet;
	private String covrrnlPstat;
	private String covrrnlStat;
	private boolean covrGoNext;
	private List<String> covrPremStats = new ArrayList<String>();
	private List<String> covrRiskStats = new ArrayList<String>();
	public L2OverdueProcessorDTO() {
		this.chdrcoy = "";
		this.chdrOk = false;
		this.chdrnum = "";
		this.statcode = "";
		this.pstatcode = "";
		this.billchnl = "";
		this.effDate = 0;
		this.lang = "";
		this.prefix = "";
		this.batch = "";
		this.batcbrn = "";
		this.batchTrcde = "";
		this.actyear = 0;
		this.actmonth = 0;
		this.durationInForce = 0;
		this.batchName = "";
		this.userName = "";
		this.l2OverdueControlTotalDTO = new L2OverdueControlTotalDTO();
		this.transactionDate = 0;
		this.transactionTime = 0;
		this.prevChdrnum = "";
		this.t6647IOList = new ArrayList<>();
		this.t5679IO = new T5679();
		this.lifacmvPojo = new LifacmvPojo();
		this.ptrnpf = new Ptrnpf();
		this.payrpf = new Payrpf();
		this.validFlag = false;
		this.skipContract = false;
		this.aplFlag = false;
		this.maxAge = 0;
		this.maxYear = 0;
		this.nlgflag = "";
		this.wssaType = "";
		this.lastPtrn = "";
		this.ignoreFlag = false;
		this.chdrpfInsert = new Chdrpf();
		this.chdrpfUpdate = new Chdrpf();
		this.chdrpf = new Chdrpf();
		this.payrInsert = new Payrpf();
		this.payrUpdate = new Payrpf();
		this.covrInsert = new ArrayList<>();
		this.covrUpdate = new ArrayList<>();
		this.hpuapfUpdate = new ArrayList<>();
		this.hpuapfInsert = new ArrayList<>();
		this.ptrnInsert = new Ptrnpf();
		this.ptrnpf = new Ptrnpf();
		this.ovrdueDTO = new OvrdueDTO();
		this.covrrnlStatiiSet = "";
		this.covrrnlPstat = "";
		this.covrrnlStat = "";
		this.covrGoNext = false;
		this.updateRequired = false;
	}

	public int getWsaaSub() {
		return wsaaSub;
	}

	public void setWsaaSub(int wsaaSub) {
		this.wsaaSub = wsaaSub;
	}

	public int getT6654SubrIx() {
		return t6654SubrIx;
	}

	public void setT6654SubrIx(int t6654SubrIx) {
		this.t6654SubrIx = t6654SubrIx;
	}

	public int getT6654ExpyIx() {
		return t6654ExpyIx;
	}

	public void setT6654ExpyIx(int t6654ExpyIx) {
		this.t6654ExpyIx = t6654ExpyIx;
	}

	public T6654 getT6654IO() {
		return t6654IO;
	}

	public void setT6654IO(T6654 t6654io) {
		t6654IO = t6654io;
	}

	public int getT6597Ix() {
		return t6597Ix;
	}

	public void setT6597Ix(int t6597Ix) {
		this.t6597Ix = t6597Ix;
	}

	public Payrpf getPayrPstcde() {
		return payrPstcde;
	}

	public void setPayrPstcde(Payrpf payrPstcde) {
		this.payrPstcde = payrPstcde;
	}

	public Chdrpf getChdrpfUpdate2() {
		return chdrpfUpdate2;
	}

	public void setChdrpfUpdate2(Chdrpf chdrpfUpdate2) {
		this.chdrpfUpdate2 = chdrpfUpdate2;
	}

	public List<Hpuapf> getHpuapfInsert() {
		return hpuapfInsert;
	}

	public void setHpuapfInsert(List<Hpuapf> hpuapfInsert) {
		this.hpuapfInsert = hpuapfInsert;
	}

	public List<Hpuapf> getHpuapfUpdate() {
		return hpuapfUpdate;
	}

	public void setHpuapfUpdate(List<Hpuapf> hpuapfUpdate) {
		this.hpuapfUpdate = hpuapfUpdate;
	}

	public List<Covrpf> getCovrUpdate() {
		return covrUpdate;
	}

	public void setCovrInsert(List<Covrpf> covrUpdate) {
		this.covrUpdate = covrUpdate;
	}

	public boolean getUpdateRequired() {
		return updateRequired;
	}

	public void setUpdateRequired(boolean updateRequired) {
		this.updateRequired = updateRequired;
	}

	public NlgcalcDTO getNlgcalcDTO() {
		return nlgcalcDTO;
	}

	public void setNlgcalcDTO(NlgcalcDTO nlgcalcDTO) {
		this.nlgcalcDTO = nlgcalcDTO;
	}

	public List<String> getCovrPremStats() {
		return covrPremStats;
	}

	public void setCovrPremStats(List<String> covrPremStats) {
		this.covrPremStats = covrPremStats;
	}

	public List<String> getCovrRiskStats() {
		return covrRiskStats;
	}

	public void setCovrRiskStats(List<String> covrRiskStats) {
		this.covrRiskStats = covrRiskStats;
	}

	public boolean getCovrGoNext() {
		return covrGoNext;
	}

	public void setCovrGoNext(boolean covrGoNext) {
		this.covrGoNext = covrGoNext;
	}

	public String getCovrrnlStatiiSet() {
		return covrrnlStatiiSet;
	}

	public void setCovrrnlStatiiSet(String covrrnlStatiiSet) {
		this.covrrnlStatiiSet = covrrnlStatiiSet;
	}

	public String getCovrrnlPstat() {
		return covrrnlPstat;
	}

	public void setCovrrnlPstat(String covrrnlPstat) {
		this.covrrnlPstat = covrrnlPstat;
	}

	public String getCovrrnlStat() {
		return covrrnlStat;
	}

	public void setCovrrnlStat(String covrrnlStat) {
		this.covrrnlStat = covrrnlStat;
	}

	public Covrpf getCovrpf() {
		return covrpf;
	}

	public void setCovrpf(Covrpf covrpf) {
		this.covrpf = covrpf;
	}

	public NfloanDTO getNfloanDTO() {
		return nfloanDTO;
	}

	public void setNfloanDTO(NfloanDTO nfloanDTO) {
		this.nfloanDTO = nfloanDTO;
	}

	public String getLastPtrn() {
		return lastPtrn;
	}

	public void setLastPtrn(String lastPtrn) {
		this.lastPtrn = lastPtrn;
	}

	public String getBatchcoy() {
		return batchcoy;
	}

	public void setBatchcoy(String batchcoy) {
		this.batchcoy = batchcoy;
	}

	public int getBtdate() {
		return btdate;
	}

	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}

	public int getPtdate() {
		return ptdate;
	}

	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	public boolean getAplFlag() {
		return aplFlag;
	}

	public void setAplFlag(boolean aplFlag) {
		this.aplFlag = aplFlag;
	}

	public int getDurationInForce() {
		return durationInForce;
	}

	public void setDurationInForce(int durationInForce) {
		this.durationInForce = durationInForce;
	}

	public boolean getChdrOk() {
		return chdrOk;
	}

	public void setChdrOk(boolean chdrOk) {
		this.chdrOk = chdrOk;
	}

	public boolean getSkipContract() {
		return skipContract;
	}

	public void setSkipContract(boolean skipContract) {
		this.skipContract = skipContract;
	}

	public Hpadpf getHpadpf() {
		return hpadpf;
	}

	public void setHpadpf(Hpadpf hpadpf) {
		this.hpadpf = hpadpf;
	}

	public OvrdueDTO getOvrdueDTO() {
		return ovrdueDTO;
	}

	public void setOvrdueDTO(OvrdueDTO ovrdueDTO) {
		this.ovrdueDTO = ovrdueDTO;
	}

	

	public boolean getIgnoreFlag() {
		return ignoreFlag;
	}

	public void setIgnoreFlag(boolean ignoreFlag) {
		this.ignoreFlag = ignoreFlag;
	}


	public String getWssaType() {
		return wssaType;
	}

	public void setWssaType(String wssaType) {
		this.wssaType = wssaType;
	}

	public String getBillchnl() {
		return billchnl;
	}

	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public int getEffDate() {
		return effDate;
	}
	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}
	public List<T6647> getT6647IOList() {
		return t6647IOList;
	}
	public void setT6647IOList(List<T6647> t6647ioList) {
		t6647IOList = t6647ioList;
	}
	public T5679 getT5679IO() {
		return t5679IO;
	}
	public void setT5679IO(T5679 t5679io) {
		t5679IO = t5679io;
	}
	

	
	public List<Covrpf> getCovrInsert() {
		return covrInsert;
	}

	public void setCovrUpdate(List<Covrpf> covrUpdate) {
		this.covrUpdate = covrUpdate;
	}

	public Chdrpf getChdrpfUpdate() {
		return chdrpfUpdate;
	}

	public void setChdrpfUpdate(Chdrpf chdrpfUpdate) {
		this.chdrpfUpdate = chdrpfUpdate;
	}

	public Chdrpf getChdrpfInsert() {
		return chdrpfInsert;
	}

	public void setChdrpfInsert(Chdrpf chdrpfInsert) {
		this.chdrpfInsert = chdrpfInsert;
	}

	public Payrpf getPayrInsert() {
		return payrInsert;
	}

	public void setPayrInsert(Payrpf payrInsert) {
		this.payrInsert = payrInsert;
	}

	public Payrpf getPayrUpdate() {
		return payrUpdate;
	}

	public void setPayrUpdate(Payrpf payrUpdate) {
		this.payrUpdate = payrUpdate;
	}

	public Ptrnpf getPtrnInsert() {
		return ptrnInsert;
	}

	public void setPtrnInsert(Ptrnpf ptrnInsert) {
		this.ptrnInsert = ptrnInsert;
	}

	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getActyear() {
		return actyear;
	}
	public void setActyear(int actyear) {
		this.actyear = actyear;
	}
	public int getActmonth() {
		return actmonth;
	}
	public void setActmonth(int actmonth) {
		this.actmonth = actmonth;
	}
	public L2OverdueControlTotalDTO getL2OverdueControlTotalDTO() {
		return l2OverdueControlTotalDTO;
	}
	public void setL2OverdueControlTotalDTO(
			L2OverdueControlTotalDTO l2OverdueControlTotalDTO) {
		this.l2OverdueControlTotalDTO = l2OverdueControlTotalDTO;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getBatchTrcde() {
		return batchTrcde;
	}
	public void setBatchTrcde(String batchTrcde) {
		this.batchTrcde = batchTrcde;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public T5687 getT5687IO() {
		return t5687IO;
	}
	public void setT5687IO(T5687 t5687IO) {
		this.t5687IO = t5687IO;
	}

	public List<T6597Inner> getT6597Inner() {
		return t6597Inner;
	}
	public void setT6597Inner(List<T6597Inner> t6597Inner) {
		this.t6597Inner = t6597Inner;
	}

	public String getPrevChdrnum() {
		return prevChdrnum;
	}
	public void setPrevChdrnum(String prevChdrnum) {
		this.prevChdrnum = prevChdrnum;
	}

	public LifacmvPojo getLifacmvPojo() {
		return lifacmvPojo;
	}
	public void setLifacmvPojo(LifacmvPojo lifacmvPojo) {
		this.lifacmvPojo = lifacmvPojo;
	}

	public Ptrnpf getPtrnpf() {
		return ptrnpf;
	}
	public void setPtrnpf(Ptrnpf ptrnpf) {
		this.ptrnpf = ptrnpf;
	}

	public Chdrpf getChdrpf() {
		return chdrpf;
	}
	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}

	public Payrpf getPayrpf() {
		return payrpf;
	}
	public void setPayrpf(Payrpf payrpf) {
		this.payrpf = payrpf;
	}

	public boolean getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(boolean validFlag) {
		this.validFlag = validFlag;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getMaxYear() {
		return maxYear;
	}

	public void setMaxYear(int maxYear) {
		this.maxYear = maxYear;
	}

	public String getNlgflag() {
		return nlgflag;
	}

	public void setNlgflag(String nlgflag) {
		this.nlgflag = nlgflag;
	}

	
	public int getOverdueDays() {
		return overdueDays;
	}

	public void setOverdueDays(int overdueDays) {
		this.overdueDays = overdueDays;
	}

	public static class T6647Inner{
		private String t6647Key;
		private int t6647Itmfrm;
		private String t6647Aloind;
		private String t6647Efdcode;
		private int t6647SeqNo;
		public String getT6647Key() {
			return t6647Key;
		}
		public int getT6647Itmfrm() {
			return t6647Itmfrm;
		}
		public void setT6647Itmfrm(int t6647Itmfrm) {
			this.t6647Itmfrm = t6647Itmfrm;
		}
		public String getT6647Aloind() {
			return t6647Aloind;
		}
		public void setT6647Aloind(String t6647Aloind) {
			this.t6647Aloind = t6647Aloind;
		}
		public String getT6647Efdcode() {
			return t6647Efdcode;
		}
		public void setT6647Efdcode(String t6647Efdcode) {
			this.t6647Efdcode = t6647Efdcode;
		}
		public int getT6647SeqNo() {
			return t6647SeqNo;
		}
		public void setT6647SeqNo(int t6647SeqNo) {
			this.t6647SeqNo = t6647SeqNo;
		}
		public void setT6647Key(String t6647Key) {
			this.t6647Key = t6647Key;
		}
		
	}
	public static class T5687Inner{
		private String t5687Crtable;
		private String t5687NonForMethod;
		private int t5687Itmfrm;
		private String t5687Bbmeth;
		private String t5687Pumeth;
		public String getT5687Crtable() {
			return t5687Crtable;
		}
		public void setT5687Crtable(String t5687Crtable) {
			this.t5687Crtable = t5687Crtable;
		}
		public String getT5687NonForMethod() {
			return t5687NonForMethod;
		}
		public void setT5687NonForMethod(String t5687NonForMethod) {
			this.t5687NonForMethod = t5687NonForMethod;
		}
		public int getT5687Itmfrm() {
			return t5687Itmfrm;
		}
		public void setT5687Itmfrm(int t5687Itmfrm) {
			this.t5687Itmfrm = t5687Itmfrm;
		}
		public String getT5687Bbmeth() {
			return t5687Bbmeth;
		}
		public void setT5687Bbmeth(String t5687Bbmeth) {
			this.t5687Bbmeth = t5687Bbmeth;
		}
		public String getT5687Pumeth() {
			return t5687Pumeth;
		}
		public void setT5687Pumeth(String t5687Pumeth) {
			this.t5687Pumeth = t5687Pumeth;
		}
	}
	public static class T6597Inner{
		private String t6597Method;
		private int t6597Itmfrm;
		private List<String> t6597Premsub;
		private List<String> t6597Cpstat;
		private List<String> t6597Crstat;
		private List<Integer> t6597Durmnth;
		public String getT6597Method() {
			return t6597Method;
		}
		public void setT6597Method(String t6597Method) {
			this.t6597Method = t6597Method;
		}
		public int getT6597Itmfrm() {
			return t6597Itmfrm;
		}
		public void setT6597Itmfrm(int t6597Itmfrm) {
			this.t6597Itmfrm = t6597Itmfrm;
		}
		public List<String> getT6597Premsub() {
			return t6597Premsub;
		}
		public void setT6597Premsub(List<String> t6597Premsub) {
			this.t6597Premsub = t6597Premsub;
		}
		public List<String> getT6597Cpstat() {
			return t6597Cpstat;
		}
		public void setT6597Cpstat(List<String> t6597Cpstat) {
			this.t6597Cpstat = t6597Cpstat;
		}
		public List<String> getT6597Crstat() {
			return t6597Crstat;
		}
		public void setT6597Crstat(List<String> t6597Crstat) {
			this.t6597Crstat = t6597Crstat;
		}
		
		public List<Integer> getT6597Durmnth() {
			return t6597Durmnth;
		}
		public void setT6597Durmnth(List<Integer> t6597Durmnth) {
			this.t6597Durmnth = t6597Durmnth;
		}
	}
	public static class T6654Inner{
		private String t6654Key;
		private String t6654Billchnl;
		private String t6654Cnttype;
		private String t6654Data;
		private int t6654NonForfDays;
		private String t6654ArrearsMethod;
		private String t6654Subroutine;
		private List<String> t6654Doctid;
		private String t6654Expiry;
		private List<Integer> t6654Daexpy;
		public String getT6654Key() {
			return t6654Key;
		}
		public void setT6654Key(String t6654Key) {
			this.t6654Key = t6654Key;
		}
		public String getT6654Billchnl() {
			return t6654Billchnl;
		}
		public void setT6654Billchnl(String t6654Billchnl) {
			this.t6654Billchnl = t6654Billchnl;
		}
		public String getT6654Cnttype() {
			return t6654Cnttype;
		}
		public void setT6654Cnttype(String t6654Cnttype) {
			this.t6654Cnttype = t6654Cnttype;
		}
		public String getT6654Data() {
			return t6654Data;
		}
		public void setT6654Data(String t6654Data) {
			this.t6654Data = t6654Data;
		}
		public int getT6654NonForfDays() {
			return t6654NonForfDays;
		}
		public void setT6654NonForfDays(int t6654NonForfDays) {
			this.t6654NonForfDays = t6654NonForfDays;
		}
		public String getT6654ArrearsMethod() {
			return t6654ArrearsMethod;
		}
		public void setT6654ArrearsMethod(String t6654ArrearsMethod) {
			this.t6654ArrearsMethod = t6654ArrearsMethod;
		}
		public String getT6654Subroutine() {
			return t6654Subroutine;
		}
		public void setT6654Subroutine(String t6654Subroutine) {
			this.t6654Subroutine = t6654Subroutine;
		}
		public List<String> getT6654Doctid() {
			return t6654Doctid;
		}
		public void setT6654Doctid(List<String> t6654Doctid) {
			this.t6654Doctid = t6654Doctid;
		}
		public String getT6654Expiry() {
			return t6654Expiry;
		}
		public void setT6654Expiry(String t6654Expiry) {
			this.t6654Expiry = t6654Expiry;
		}
		public List<Integer> getT6654Daexpy() {
			return t6654Daexpy;
		}
		public void setT6654Daexpy(List<Integer> t6654Daexpy) {
			this.t6654Daexpy = t6654Daexpy;
		}
		
	}
	
}