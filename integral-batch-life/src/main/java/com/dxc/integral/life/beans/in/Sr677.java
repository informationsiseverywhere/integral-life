package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr677 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private Sr677screensfl sr677screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public Sr677screensfl getSr677screensfl() {
		return sr677screensfl;
	}
	public void setSr677screensfl(Sr677screensfl sr677screensfl) {
		this.sr677screensfl = sr677screensfl;
	}
	
	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Sr677screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		public int size() {
			return rows.length;
		}

		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
	}
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String relation = "";
			private String clntnum = "";
			private String clntname = "";

			public String getRelation() {
				return relation;
			}
			public void setRelation(String relation) {
				this.relation = relation;
			}
			public String getClntnum() {
				return clntnum;
			}
			public void setClntnum(String clntnum) {
				this.clntnum = clntnum;
			}
			public String getClntname() {
				return clntname;
			}
			public void setClntname(String clntname) {
				this.clntname = clntname;
			}
		}
		@Override
		public String toString() {
			return "Sr677 []";
		} 
		
}
