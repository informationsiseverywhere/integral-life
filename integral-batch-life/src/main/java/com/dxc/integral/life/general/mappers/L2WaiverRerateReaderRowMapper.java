package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.dxc.integral.life.beans.L2WaiverRerateReaderDTO;

public class L2WaiverRerateReaderRowMapper implements RowMapper<L2WaiverRerateReaderDTO> {
	@Override
	public L2WaiverRerateReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		L2WaiverRerateReaderDTO reader = new L2WaiverRerateReaderDTO();
		
		reader.setChdrCoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
		reader.setChdrNum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");
		reader.setCoverage(rs.getString("COVERAGE"));
		reader.setRider(rs.getString("RIDER"));
		reader.setLife(rs.getString("LIFE"));
		reader.setjLife(rs.getString("JLIFE"));
		reader.setStatCode(rs.getString("STATCODE"));
		reader.setPlnSfx(rs.getInt("PLNSFX"));
		reader.setValidFlag(rs.getString("VALIDFLAG"));
		reader.setRrtDat(rs.getInt("RRTDAT"));
		reader.setTranNo(rs.getInt("TRANNO"));
		reader.setCpiDte(rs.getInt("CPIDTE"));
		reader.setAnbccd(rs.getInt("ANBCCD"));
		reader.setBcesage(rs.getInt("BCESAGE"));
		reader.setPcesage(rs.getInt("PCESAGE"));
		reader.setRcesage(rs.getInt("RCESAGE"));
		reader.setBcestrm(rs.getInt("BCESTRM"));
		reader.setPcestrm(rs.getInt("PCESTRM"));
		reader.setRcestrm(rs.getInt("RCESTRM"));
		reader.setCrtable(rs.getString("CRTABLE"));
		reader.setCrrcd(rs.getInt("CRRCD"));
		reader.setCurrFrom(rs.getInt("CURRFROM"));
		reader.setCurrTo(rs.getInt("CURRTO"));
		reader.setLiencd(rs.getString("LIENCD"));
		reader.setLnkgInd(rs.getString("LNKGIND"));
		reader.setLnkgNo(rs.getString("LNKGNO"));
		reader.setLnkgsubrefno(rs.getString("LNKGSUBREFNO"));
		reader.setMortCls(rs.getString("MORTCLS"));
		reader.setPrmcur(rs.getString("PRMCUR"));
		reader.setZclState(rs.getString("ZCLSTATE"));
		reader.setNxtdte(rs.getInt("NXTDTE"));	
		reader.setPcesdte(rs.getInt("PCESDTE"));
		reader.setBcesdte(rs.getInt("BCESDTE"));
		reader.setRcesdte(rs.getInt("RCESDTE"));
		reader.setPstatCode(rs.getString("PSTATCODE"));
		reader.setSumIns(rs.getBigDecimal("SUMINS"));
		reader.setTpdType(rs.getString("TPDTYPE"));
		reader.setZstpDuty01(rs.getBigDecimal("ZSTPDUTY01"));
		reader.setZinstPrem(rs.getBigDecimal("INSTPREM"));
		reader.setZlinstPrem(rs.getBigDecimal("ZLINSTPREM"));
		reader.setZbinstPrem(rs.getBigDecimal("ZBINSTPREM"));
		reader.setSumIns(rs.getBigDecimal("SUMINS"));
		reader.setTermId(rs.getString("TERMID"));
		reader.setRrtfrm(rs.getInt("RRTFRM"));
		reader.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
		
		
		return reader;
		
	}

}
