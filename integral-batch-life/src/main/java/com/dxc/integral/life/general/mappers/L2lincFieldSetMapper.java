package com.dxc.integral.life.general.mappers;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;
import com.dxc.integral.life.beans.L2lincReaderDTO;

public class L2lincFieldSetMapper implements FieldSetMapper<L2lincReaderDTO>{

	public L2lincReaderDTO mapFieldSet(FieldSet fieldSet) throws BindException {
		
		L2lincReaderDTO l2lincReader = new L2lincReaderDTO();
		
		l2lincReader.setZdatacls(fieldSet.readString(0));
		l2lincReader.setZreclass(fieldSet.readString(1));
		l2lincReader.setZmodtype(fieldSet.readString(2));
		l2lincReader.setZrsltcde(fieldSet.readString(3));
		l2lincReader.setZrsltrsn(fieldSet.readString(4));
		l2lincReader.setZerrorno(fieldSet.readString(5));
		l2lincReader.setZnofrecs(fieldSet.readString(6));
		l2lincReader.setKlinckey(fieldSet.readString(7));
		l2lincReader.setHcltnam(fieldSet.readString(8));
		l2lincReader.setCltsex(fieldSet.readString(9));
		l2lincReader.setZclntdob(fieldSet.readInt(10));
		l2lincReader.setZeffdate(fieldSet.readInt(11));
		l2lincReader.setFlag(fieldSet.readString(12));
		l2lincReader.setKglcmpcd(fieldSet.readString(13));
		l2lincReader.setAddrss(fieldSet.readString(14));
		l2lincReader.setZhospbnf(fieldSet.readBigDecimal(15));
		l2lincReader.setZsickbnf(fieldSet.readBigDecimal(16));
		l2lincReader.setZcancbnf(fieldSet.readBigDecimal(17));
		l2lincReader.setZothsbnf(fieldSet.readBigDecimal(18));
		l2lincReader.setZregdate(fieldSet.readInt(19));
		l2lincReader.setZlincdte(fieldSet.readInt(20));
		l2lincReader.setKjhcltnam(fieldSet.readString(21));
		l2lincReader.setZhospopt(fieldSet.readString(22));
		l2lincReader.setZrevdate(fieldSet.readInt(23));
		l2lincReader.setZlownnam(fieldSet.readString(24));
		l2lincReader.setZcsumins(fieldSet.readBigDecimal(25));
		l2lincReader.setZasumins(fieldSet.readBigDecimal(26));
		l2lincReader.setZdethopt(fieldSet.readString(27));
		l2lincReader.setZrqflag(fieldSet.readString(28));
		l2lincReader.setZrqsetdte(fieldSet.readInt(29));
		l2lincReader.setZrqcandte(fieldSet.readInt(30));
		l2lincReader.setZepflag(fieldSet.readString(31));
		l2lincReader.setZcvflag(fieldSet.readString(32));
		l2lincReader.setZcontsex(fieldSet.readString(33));
		l2lincReader.setZcontdob(fieldSet.readInt(34));
		l2lincReader.setZcontaddr(fieldSet.readString(35));
		l2lincReader.setLifnamkj(fieldSet.readString(36));
		l2lincReader.setOwnnamkj(fieldSet.readString(37));
		l2lincReader.setWflga(fieldSet.readString(38));
		l2lincReader.setZerrflg(fieldSet.readString(39));
		l2lincReader.setZoccdate(fieldSet.readInt(40));
		l2lincReader.setZtrdate(fieldSet.readInt(41));
		l2lincReader.setZadrddte(fieldSet.readInt(42));
		l2lincReader.setZexdrddte(fieldSet.readInt(43));
		l2lincReader.setDummy(fieldSet.readString(44));
		return l2lincReader;
	}

}
