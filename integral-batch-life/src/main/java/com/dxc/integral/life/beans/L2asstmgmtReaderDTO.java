package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class L2asstmgmtReaderDTO {
	/* Fund code*/
	private String vrtfnd; 
	/*Contract number */
	private String chdrnum;
	/* Product Code*/
	private String crtable;
	/* Component code*/
	private String compCode;
	/* Price Used*/
	private BigDecimal priceUsed;
	/* Fund Unit */
	private String noFundUnit;
	/* Agent LastName*/
	private String lastName;	
	/* Agent FirstName*/
	private String firstName;
	/* Agent Number */
	private String agentNum;
	/*Split Commision*/
	private String splitc;
	private String chdrcoy;
	/** The batcbrn */
	private String batcbrn;
	/** The termid */
	private String termid;
	/** The batcactyr */
	private Integer batcactyr;
	/** The batcactmn */
	private Integer batcactmn;
	/** The batctrcde */
	private String batctrcde;
	/** The batcbatch */
	private String batcbatch;
	/** The batcpfx */
	private String batcpfx;
	/** The batccoy */
	private String batccoy;
	/** The effectiveDate */
	private Integer effectiveDate;
	/** The trdt */
	private Integer trdt;
	/** The trtm */
	private Integer trtm;
	/** The userNum */
	private Integer userNum;
	/** The tranCount */
	private int tranCount;
	/** The usrprf */
	private String usrprf;
	/** The jobnm */
	private String jobnm;
	/** The datime */
	private Timestamp datime;
	/** The policyTranno */
	private Integer policyTranno;
	public L2asstmgmtReaderDTO() {		
	}
	public L2asstmgmtReaderDTO(String string, String string2, BigDecimal bigDecimal, int parseInt) {
	
	}
	//UTRNPF
	public String getVrtfnd() {
		return vrtfnd;
	}
	public void setVrtfnd(String vrtfnd) {
		this.vrtfnd = vrtfnd;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCompCode() {
		return compCode;
	}
	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}
	public BigDecimal getPriceUsed() {
		return priceUsed;
	}
	public void setPriceUsed(BigDecimal priceUsed) {
		this.priceUsed = priceUsed;
	}
	public String getNoFundUnit() {
		return noFundUnit;
	}
	public void setNoFundUnit(String noFundUnit) {
		this.noFundUnit = noFundUnit;
	}	
	
	//CLNTPF
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	//CHDRPF
	public String getAgentNum() {
		return agentNum;
	}
	public void setAgentNum(String agentNum) {
		this.agentNum = agentNum;
	}
	
	//PCDDPF
	public String getSplitc() {
		return splitc;
	}
	public void setSplitc(String splitc) {
		this.splitc = splitc;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public Integer getPolicyTranno() {
		return policyTranno;
	}
	public void setPolicyTranno(Integer policyTranno) {
		this.policyTranno = policyTranno;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public Integer getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Integer effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUserNum() {
		return userNum;
	}
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}
	public int getTranCount() {
		return tranCount;
	}
	public void setTranCount(int tranCount) {
		this.tranCount = tranCount;
	}
}
