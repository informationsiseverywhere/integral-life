package com.dxc.integral.life.beans;

import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.smarttable.pojo.T5519;
import com.dxc.integral.life.smarttable.pojo.T5540;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6658;

public class L2AnnyProcProcessorDTO {
	
	private boolean vpmsFlag;
	private int actMonth;
	private int actYear;
	private String lang;
	private boolean t5540Found;
	private String prefix;
	private String batch;
	private String batchtrancde;
	private String annvry;
	private String subProg;
	private boolean dateFound;
	private String batchbranch;
	private String chdrnum;
	private String chdrcoy;
	private String userName;
	private String batchName;
	private int transactionDate;
	private int tranTime;
	private int effDate;
	private int tranno;
	private String validComp;
	private boolean updateCovr;
	private T5679 t5679 = new T5679();
	private T5687 t5687 = new T5687();
	private T6658 t6658 = new T6658();
	private T5540 t5540 = new T5540();
	private T5519 t5519 = new T5519();
	private L2AnnyProcControlTotalDTO l2AnnyProcControlTotalDTO = new L2AnnyProcControlTotalDTO();
	
	private String life;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String crtable;
	private int crrcd;
	private String statcode;
	private Covrpf covrpf;
	
	
	public L2AnnyProcControlTotalDTO getL2AnnyProcControlTotalDTO() {
		return l2AnnyProcControlTotalDTO;
	}
	public void setL2AnnyProcControlTotalDTO(
			L2AnnyProcControlTotalDTO l2AnnyProcControlTotalDTO) {
		this.l2AnnyProcControlTotalDTO = l2AnnyProcControlTotalDTO;
	}
	public boolean getVpmsFlag() {
		return vpmsFlag;
	}
	public void setVpmsFlag(boolean vpmsFlag2) {
		this.vpmsFlag = vpmsFlag2;
	}
	public int getActMonth() {
		return actMonth;
	}
	public void setActMonth(int actMonth) {
		this.actMonth = actMonth;
	}
	public int getActYear() {
		return actYear;
	}
	public void setActYear(int actYear) {
		this.actYear = actYear;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBatchtrancde() {
		return batchtrancde;
	}
	public void setBatchtrancde(String batchtrancde) {
		this.batchtrancde = batchtrancde;
	}
	public String getBatchbranch() {
		return batchbranch;
	}
	public void setBatchbranch(String batchbranch) {
		this.batchbranch = batchbranch;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTranTime() {
		return tranTime;
	}
	public void setTranTime(int tranTime) {
		this.tranTime = tranTime;
	}
	public int getEffDate() {
		return effDate;
	}
	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}
	public T5679 getT5679() {
		return t5679;
	}
	public void setT5679(T5679 t5679) {
		this.t5679 = t5679;
	}
	public T5687 getT5687() {
		return t5687;
	}
	public void setT5687(T5687 t5687) {
		this.t5687 = t5687;
	}
	public T6658 getT6658() {
		return t6658;
	}
	public void setT6658(T6658 t6658) {
		this.t6658 = t6658;
	}
	public T5540 getT5540() {
		return t5540;
	}
	public void setT5540(T5540 t5540) {
		this.t5540 = t5540;
	}
	public T5519 getT5519() {
		return t5519;
	}	
	public void setT5519(T5519 t5519) {
		this.t5519 =t5519;
		
	}
	public String getValidComp() {
		return validComp;
	}
	public void setValidComp(String validComp) {
		this.validComp = validComp;
	}

	public boolean isUpdateCovr() {
		return updateCovr;
	}
	public void setUpdateCovr(boolean updateCovr) {
		this.updateCovr = updateCovr;
	}
	public boolean isDateFound() {
		return dateFound;
	}
	public void setDateFound(boolean dateFound) {
		this.dateFound = dateFound;
	}
	public String getAnnvry() {
		return annvry;
	}
	public void setAnnvry(String annvry) {
		this.annvry = annvry;
	}
	public String getSubProg() {
		return subProg;
	}
	public void setSubProg(String subProg) {
		this.subProg = subProg;
	}
	public boolean isT5540Found() {
		return t5540Found;
	}
	public void setT5540Found(boolean t5540Found) {
		this.t5540Found = t5540Found;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public Covrpf getCovrpf() {
		return covrpf;
	}
	public void setCovrpf(Covrpf covrpf) {
		this.covrpf = covrpf;
	}
	
	

}
