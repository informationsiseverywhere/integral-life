package com.dxc.integral.life.general.processors;

import java.math.BigDecimal;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.life.beans.L2OverdueControlTotalDTO;
import com.dxc.integral.life.beans.L2OverdueProcessorDTO;
import com.dxc.integral.life.beans.L2OverdueReaderDTO;
import com.dxc.integral.life.dao.model.LifacmvPojo;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.utils.L2OverdueItemProcessorUtil;

/**
 * ItemProcessor for L2POLRNWL Re-rating batch step.
 * 
 * @author yyang21
 *
 */
@Scope(value = "step")
@Async
@Lazy
public class L2OverdueItemProcessor extends BaseProcessor implements ItemProcessor<L2OverdueReaderDTO, L2OverdueReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2OverdueItemProcessor.class);
	
	@Autowired
	private L2OverdueItemProcessorUtil l2OverdueItemProcessorUtil;

	
	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	@Autowired
	private Environment env;
	@Autowired
	private L2OverdueControlTotalDTO l2OverdueControlTotalDTO;
	@Override
	public L2OverdueReaderDTO process(L2OverdueReaderDTO readerDTO) throws Exception {
		LOGGER.info("Processing starts for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		
		L2OverdueProcessorDTO l2OverdueProcessorDTO = this.init(readerDTO);
		l2OverdueProcessorDTO = l2OverdueItemProcessorUtil.loadSmartTables(l2OverdueProcessorDTO);

		releaseSoftlock(readerDTO.getChdrcoy(),readerDTO.getChdrnum(),"B673",userName,batchName,l2OverdueProcessorDTO.getBatchTrcde());
		l2OverdueProcessorDTO = l2OverdueItemProcessorUtil.validateContract(l2OverdueProcessorDTO);
		if(!l2OverdueProcessorDTO.getIgnoreFlag()){
			if(l2OverdueProcessorDTO.getWssaType().equals("0") || l2OverdueProcessorDTO.getWssaType().equals("1") || l2OverdueProcessorDTO.getWssaType().equals("2")
					|| l2OverdueProcessorDTO.getWssaType().equals("A")){
				l2OverdueProcessorDTO = l2OverdueItemProcessorUtil.processOverdueContract(l2OverdueProcessorDTO);
			}
			l2OverdueItemProcessorUtil.statistics(l2OverdueProcessorDTO);
			processControlTotal(l2OverdueProcessorDTO);
			
			if(l2OverdueProcessorDTO.getChdrpfInsert() != null){
				readerDTO.setChdrpfInsert(l2OverdueProcessorDTO.getChdrpfInsert());
			}
			if(l2OverdueProcessorDTO.getChdrpfUpdate() != null){
				readerDTO.setChdrpfUpdate(l2OverdueProcessorDTO.getChdrpfUpdate());
			}
			if(l2OverdueProcessorDTO.getChdrpfUpdate2() != null){
				readerDTO.setChdrpfUpdate2(l2OverdueProcessorDTO.getChdrpfUpdate2());
			}
			if(l2OverdueProcessorDTO.getHpuapfInsert() != null){
				readerDTO.setHpuapfInsert(l2OverdueProcessorDTO.getHpuapfInsert());
			}
			if(l2OverdueProcessorDTO.getHpuapfUpdate() != null){
				readerDTO.setHpuapfUpdate(l2OverdueProcessorDTO.getHpuapfUpdate());
			}
			if(l2OverdueProcessorDTO.getPayrInsert() != null){
				readerDTO.setPayrInsert(l2OverdueProcessorDTO.getPayrInsert());
			}
			if(l2OverdueProcessorDTO.getPayrUpdate() != null){
				readerDTO.setPayrUpdate(l2OverdueProcessorDTO.getPayrUpdate());
			}
			if(l2OverdueProcessorDTO.getPayrPstcde() != null){
				readerDTO.setPayrpfPstcde(l2OverdueProcessorDTO.getPayrPstcde());
			}
			if(l2OverdueProcessorDTO.getCovrInsert() != null){
				readerDTO.setCovrInsert(l2OverdueProcessorDTO.getCovrInsert());
			}
			
			if(l2OverdueProcessorDTO.getCovrUpdate() != null){
				readerDTO.setCovrUpdate(l2OverdueProcessorDTO.getCovrUpdate());
			}
			
			if(l2OverdueProcessorDTO.getPtrnInsert() != null){
				readerDTO.setPtrnInsert(l2OverdueProcessorDTO.getPtrnInsert());
			}
			
		}
		releaseSoftlock(readerDTO.getChdrcoy(),readerDTO.getChdrnum(),"B673",userName,batchName,l2OverdueProcessorDTO.getBatchTrcde());
		LOGGER.info("Processing ends for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		return readerDTO;
	}
	private void processControlTotal(L2OverdueProcessorDTO l2OverdueProcessorDTO){
		l2OverdueControlTotalDTO.setControlTotal01(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal02(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal03(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal04(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal05(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal06(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal07(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal08(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal09(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal10(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal11(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
		l2OverdueControlTotalDTO.setControlTotal12(l2OverdueProcessorDTO.getL2OverdueControlTotalDTO().getControlTotal01());
	}
	private L2OverdueProcessorDTO init(L2OverdueReaderDTO readerDTO) throws ParseException{
		Usrdpf usrdpf = getUserDetails(userName);
		Batcpf batchDetail = getBatcpfInfo(transactionDate,businessDate,Companies.LIFE,"B673",branch,usrdpf,batchName);
		L2OverdueProcessorDTO l2OverdueProcessorDTO = new L2OverdueProcessorDTO();
		l2OverdueProcessorDTO.setLang(usrdpf.getLanguage());
		l2OverdueProcessorDTO.setActmonth(getActMonth(transactionDate));
		l2OverdueProcessorDTO.setActyear(getActYear(transactionDate));
		l2OverdueProcessorDTO.setLang(usrdpf.getLanguage());
		l2OverdueProcessorDTO.setPrefix(batchDetail.getBatcpfx());
		l2OverdueProcessorDTO.setBatch(batchDetail.getBatcbatch());
		l2OverdueProcessorDTO.setBatchcoy(batchDetail.getBatccoy());
		l2OverdueProcessorDTO.setBatchTrcde(batchDetail.getBatctrcde());
		l2OverdueProcessorDTO.setBatcbrn(batchDetail.getBatcbrn());
		l2OverdueProcessorDTO.setChdrcoy(readerDTO.getChdrcoy());
		l2OverdueProcessorDTO.setChdrnum(readerDTO.getChdrnum());
		l2OverdueProcessorDTO.setUserName(userName);
		l2OverdueProcessorDTO.setBatchName(batchName);
		l2OverdueProcessorDTO.setTransactionDate(Integer.parseInt(getCobolDate()));
		l2OverdueProcessorDTO.setTransactionTime(getVrcmTime());
		l2OverdueProcessorDTO.setEffDate(Integer.parseInt(transactionDate));
		l2OverdueProcessorDTO.setPtdate(readerDTO.getPtdate());
		l2OverdueProcessorDTO.setBtdate(readerDTO.getBtdate());
		l2OverdueProcessorDTO.setBillchnl(readerDTO.getBillchnl());
		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setUserT(0);
		ptrnpf.setPtrneff(Integer.parseInt(transactionDate));
		ptrnpf.setTrdt(Integer.parseInt(transactionDate));
		ptrnpf.setBatccoy(batchDetail.getBatccoy());
		ptrnpf.setBatcactyr(batchDetail.getBatcactyr());
		ptrnpf.setBatcactmn(batchDetail.getBatcactmn());
		ptrnpf.setBatctrcde(batchDetail.getBatctrcde());
		ptrnpf.setBatcbatch(batchDetail.getBatcbatch());
		ptrnpf.setBatcbrn(batchDetail.getBatcbrn());
		ptrnpf.setTrtm(getVrcmTime());
		LifacmvPojo lifacmvPojo = new LifacmvPojo();
		lifacmvPojo.setBatccoy(batchDetail.getBatccoy());
		lifacmvPojo.setRldgcoy(batchDetail.getBatccoy());
		lifacmvPojo.setGenlcoy(batchDetail.getBatccoy());
		lifacmvPojo.setBatcactyr(batchDetail.getBatcactyr());
		lifacmvPojo.setBatcactmn(batchDetail.getBatcactmn());
		lifacmvPojo.setBatctrcde(batchDetail.getBatctrcde());
		lifacmvPojo.setBatcbatch(batchDetail.getBatcbatch());
		lifacmvPojo.setBatcbrn(batchDetail.getBatcbrn());
		lifacmvPojo.setRcamt(0);
		lifacmvPojo.setCrate(BigDecimal.ZERO);
		lifacmvPojo.setAcctamt(BigDecimal.ZERO);
		lifacmvPojo.setUser(0);
		lifacmvPojo.setFrcdate(99999999);
		l2OverdueProcessorDTO.setPtrnpf(ptrnpf);
		l2OverdueProcessorDTO.setLifacmvPojo(lifacmvPojo);
		return l2OverdueProcessorDTO;
	}
}