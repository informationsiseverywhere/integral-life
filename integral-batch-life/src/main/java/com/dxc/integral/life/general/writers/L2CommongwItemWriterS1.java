package com.dxc.integral.life.general.writers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.dxc.integral.life.beans.L2CommongwDTO;
import com.dxc.integral.life.dao.ComgwpfDAO;
import com.dxc.integral.life.dao.model.Comgwpf;

/**
 * 
 * @author vrathi4
 *
 */
public class L2CommongwItemWriterS1 implements ItemWriter<L2CommongwDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(L2CommongwItemWriterS1.class);

	@Autowired
	private ComgwpfDAO comgwpfDAO;

	private Comgwpf comgwpf;
	private List<Comgwpf> comgwpfList = new ArrayList<Comgwpf>();
	List<String> checkList;

	@Override
	public void write(List<? extends L2CommongwDTO> items) throws Exception {

		for (L2CommongwDTO commongwDto : items) {
			setCheckList(commongwDto);
			comgwpf = comgwpfDAO.checkDuplRecord(checkList);
			if (comgwpf == null)
				comgwpfList.add(setComgwpf(commongwDto));
		}
		insertComgwpf(comgwpfList);
	}

	private Comgwpf setComgwpf(L2CommongwDTO reader) {

		comgwpf = new Comgwpf();
		comgwpf.setAccno(reader.getAccno());
		comgwpf.setAcctyp(reader.getAcctyp());
		comgwpf.setAddscnt(reader.getAddscnt());
		comgwpf.setAffilcde(reader.getAffilcde());
		comgwpf.setAgcde01(reader.getAgcde01());
		comgwpf.setAgcde02(reader.getAgcde02());
		comgwpf.setAgregno(reader.getAgregno());
		comgwpf.setAmor(reader.getAmor());
		comgwpf.setAnpenpymnt(reader.getAnpenpymnt());
		comgwpf.setAntypymnt(reader.getAntypymnt());
		comgwpf.setAppcate(reader.getAppcate());
		comgwpf.setAppno(reader.getAppno());
		comgwpf.setAsmcde(reader.getAsmcde());
		comgwpf.setAttchngcls(reader.getAttchngcls());
		comgwpf.setBcagyr(reader.getBcagyr());
		comgwpf.setBckp(reader.getBckp());
		comgwpf.setBckup01(reader.getBckup01());
		comgwpf.setBckup02(reader.getBckup02());
		comgwpf.setBnsprem(reader.getBnsprem());
		comgwpf.setBokcls(reader.getBokcls());
		comgwpf.setBphno(reader.getBphno());
		comgwpf.setBphno02(reader.getBphno02());
		comgwpf.setBrnchnum(reader.getBrnchnum());
		comgwpf.setBrnchnum2(reader.getBrnchnum2());
		comgwpf.setCanknjiecflg(reader.getCanknjiecflg());
		comgwpf.setCnkjicflgscd(reader.getCnkjicflgscd());
		comgwpf.setCaflgadrs(reader.getCaflgadrs());
		comgwpf.setCaflgscde01(reader.getCaflgscde01());
		comgwpf.setCaflgscde02(reader.getCaflgscde02());
		comgwpf.setCaic(reader.getCaic());
		comgwpf.setCcps(reader.getCcps());
		comgwpf.setCgender(reader.getCgender());
		comgwpf.setCgrpc(reader.getCgrpc());
		comgwpf.setCifccc(reader.getCifccc());
		comgwpf.setCmec(reader.getCmec());
		comgwpf.setCmgntcde(reader.getCmgntcde());
		comgwpf.setCmpnycde(reader.getCmpnycde());
		comgwpf.setCmpnyter(reader.getCmpnyter());
		comgwpf.setCnflgscde(reader.getCnflgscde());
		comgwpf.setCntadd(reader.getCntadd());
		comgwpf.setCnticnrct(reader.getCnticnrct());
		comgwpf.setColcate(reader.getColcate());
		comgwpf.setCorpterty(reader.getCorpterty());
		comgwpf.setCrsnme(reader.getCrsnme());
		comgwpf.setDatatype(reader.getDatatype());
		comgwpf.setDcde(reader.getDcde());
		comgwpf.setDobph(reader.getDobph());
		comgwpf.setDpcde(reader.getDpcde());
		comgwpf.setDtechgprem(reader.getDtechgprem());
		comgwpf.setDteofapp(reader.getDteofapp());
		comgwpf.setDthbnft(reader.getDthbnft());
		comgwpf.setDtypcde(reader.getDtypcde());
		comgwpf.setDvndcls(reader.getDvndcls());
		comgwpf.setEmpcde(reader.getEmpcde());
		comgwpf.setExmcate(reader.getExmcate());
		comgwpf.setFpdfgh(reader.getFpdfgh());
		comgwpf.setFrstme(reader.getFrstme());
		comgwpf.setGrpcara(reader.getGrpcara());
		comgwpf.setGrpcde(reader.getGrpcde());
		comgwpf.setGrpcde02(reader.getGrpcde02());
		comgwpf.setGrpcrtlno(reader.getGrpcrtlno());
		comgwpf.setGrpinsnme(reader.getGrpinsnme());
		comgwpf.setIadrsknji(reader.getIadrsknji());
		comgwpf.setIaeflg(reader.getIaeflg());
		comgwpf.setIashtcde01(reader.getIashtcde01());
		comgwpf.setIashtcde02(reader.getIashtcde02());
		comgwpf.setInacviwe(reader.getInacviwe());
		comgwpf.setIncpn(reader.getIncpn());
		comgwpf.setIndcate(reader.getIndcate());
		comgwpf.setIndcprd(reader.getIndcprd());
		comgwpf.setInscmpnycde(reader.getInscmpnycde());
		comgwpf.setInsprocde(reader.getInsprocde());
		comgwpf.setInstal(reader.getInstal());
		comgwpf.setInsucate(reader.getInsucate());
		comgwpf.setInsyr(reader.getInsyr());
		comgwpf.setIpactc(reader.getIpactc());
		comgwpf.setIpage(reader.getIpage());
		comgwpf.setIpdob(reader.getIpdob());
		comgwpf.setIpecflg(reader.getIpecflg());
		comgwpf.setIpgndr(reader.getIpgndr());
		comgwpf.setIpnmknji(reader.getIpnmknji());
		comgwpf.setIpostcde(reader.getIpostcde());
		comgwpf.setIppyear(reader.getIppyear());
		comgwpf.setIprln(reader.getIprln());
		comgwpf.setIpsftcde01(reader.getIpsftcde01());
		comgwpf.setIpsftcde02(reader.getIpsftcde02());
		comgwpf.setKyoacs(reader.getKyoacs());
		comgwpf.setKyocde(reader.getKyocde());
		comgwpf.setLcode(reader.getLcode());
		comgwpf.setLircde(reader.getLircde());
		comgwpf.setLncde(reader.getLncde());
		comgwpf.setLocde(reader.getLocde());
		comgwpf.setLpcc(reader.getLpcc());
		comgwpf.setLsprem(reader.getLsprem());
		comgwpf.setLvnnds(reader.getLvnnds());
		comgwpf.setMargin(reader.getMargin());
		comgwpf.setMatrty(reader.getMatrty());
		comgwpf.setMccecflg(reader.getMccecflg());
		comgwpf.setMccip(reader.getMccip());
		comgwpf.setMccitc(reader.getMccitc());
		comgwpf.setMccnmc(reader.getMccnmc());
		comgwpf.setMccpp(reader.getMccpp());
		comgwpf.setMccppc(reader.getMccppc());
		comgwpf.setMccppa(reader.getMccppa());
		comgwpf.setMccscde01(reader.getMccscde01());
		comgwpf.setMccscde02(reader.getMccscde02());
		comgwpf.setMcctypcde(reader.getMcctypcde());
		comgwpf.setModinstprem(reader.getModinstprem());
		comgwpf.setModundrprd(reader.getModundrprd());
		comgwpf.setMop(reader.getMop());
		comgwpf.setNorids(reader.getNorids());
		comgwpf.setNounits(reader.getNounits());
		comgwpf.setNwccls(reader.getNwccls());
		comgwpf.setNwcmpncde(reader.getNwcmpncde());
		comgwpf.setOpcde(reader.getOpcde());
		comgwpf.setOprtcde(reader.getOprtcde());
		comgwpf.setOsnbn(reader.getOsnbn());
		comgwpf.setOsnmn(reader.getOsnmn());
		comgwpf.setOsnocc(reader.getOsnocc());
		comgwpf.setPacs(reader.getPacs());
		comgwpf.setPasbdy(reader.getPasbdy());
		comgwpf.setPayecate(reader.getPayecate());
		comgwpf.setPcde(reader.getPcde());
		comgwpf.setPdexntdvw(reader.getPdexntdvw());
		comgwpf.setPensncrtno(reader.getPensncrtno());
		comgwpf.setPensnfund(reader.getPensnfund());
		comgwpf.setPnum(reader.getPnum());
		comgwpf.setPostcde(reader.getPostcde());
		comgwpf.setPpendatme(reader.getPpendatme());
		comgwpf.setPprem(reader.getPprem());
		comgwpf.setPpsdte(reader.getPpsdte());
		comgwpf.setPremlncrt(reader.getPremlncrt());
		comgwpf.setProsqnc(reader.getProsqnc());
		comgwpf.setPrsnmekana(reader.getPrsnmekana());
		comgwpf.setPstrtflg(reader.getPstrtflg());
		comgwpf.setPymntdte(reader.getPymntdte());
		comgwpf.setPymntrt(reader.getPymntrt());
		comgwpf.setRacate(reader.getRacate());
		comgwpf.setRftrns(reader.getRftrns());
		comgwpf.setSa01typcde(reader.getSa01typcde());
		comgwpf.setSa01ecflg(reader.getSa01ecflg());
		comgwpf.setSa01insprd(reader.getSa01insprd());
		comgwpf.setSa01itc(reader.getSa01itc());
		comgwpf.setSa01nme(reader.getSa01nme());
		comgwpf.setSa01pp(reader.getSa01pp());
		comgwpf.setSa01ppc(reader.getSa01ppc());
		comgwpf.setSa01scde01(reader.getSa01scde01());
		comgwpf.setSa01scde02(reader.getSa01scde02());
		comgwpf.setSa01sins(reader.getSa01sins());
		comgwpf.setSa02typcde(reader.getSa02typcde());
		comgwpf.setSa02ecflg(reader.getSa02ecflg());
		comgwpf.setSa02insprd(reader.getSa02insprd());
		comgwpf.setSa02itc(reader.getSa02itc());
		comgwpf.setSa02nme(reader.getSa02nme());
		comgwpf.setSa02pp(reader.getSa02pp());
		comgwpf.setSa02ppc(reader.getSa02ppc());
		comgwpf.setSa02scde01(reader.getSa02scde01());
		comgwpf.setSa02scde02(reader.getSa02scde02());
		comgwpf.setSa02sins(reader.getSa02sins());
		comgwpf.setSa03typcde(reader.getSa03typcde());
		comgwpf.setSa03ecflg(reader.getSa03ecflg());
		comgwpf.setSa03insprd(reader.getSa03insprd());
		comgwpf.setSa03itc(reader.getSa03itc());
		comgwpf.setSa03nme(reader.getSa03nme());
		comgwpf.setSa03pp(reader.getSa03pp());
		comgwpf.setSa03ppc(reader.getSa03ppc());
		comgwpf.setSa03scde01(reader.getSa03scde01());
		comgwpf.setSa03scde02(reader.getSa03scde02());
		comgwpf.setSa03sins(reader.getSa03sins());
		comgwpf.setSa04typcde(reader.getSa04typcde());
		comgwpf.setSa04ecflg(reader.getSa04ecflg());
		comgwpf.setSa04insprd(reader.getSa04ecflg());
		comgwpf.setSa04itc(reader.getSa04itc());
		comgwpf.setSa04nme(reader.getSa04nme());
		comgwpf.setSa04pp(reader.getSa04pp());
		comgwpf.setSa04ppc(reader.getSa04ppc());
		comgwpf.setSa04scde01(reader.getSa04scde01());
		comgwpf.setSa04scde02(reader.getSa04scde02());
		comgwpf.setSa04sins(reader.getSa04sins());
		comgwpf.setSa05typcde(reader.getSa05typcde());
		comgwpf.setSa05insprd(reader.getSa05insprd());
		comgwpf.setSa05itc(reader.getSa05itc());
		comgwpf.setSa05nme(reader.getSa05nme());
		comgwpf.setSa05pp(reader.getSa05pp());
		comgwpf.setSa05ppc(reader.getSa05ppc());
		comgwpf.setSa05scde01(reader.getSa05scde01());
		comgwpf.setSa05scde02(reader.getSa05scde02());
		comgwpf.setSa05sins(reader.getSa05sins());
		comgwpf.setSa06typcde(reader.getSa06typcde());
		comgwpf.setSa06ecflg(reader.getSa06ecflg());
		comgwpf.setSa06insprd(reader.getSa06insprd());
		comgwpf.setSa06itc(reader.getSa06itc());
		comgwpf.setSa06nme(reader.getSa06nme());
		comgwpf.setSa06pp(reader.getSa06pp());
		comgwpf.setSa06ppc(reader.getSa06ppc());
		comgwpf.setSa06scde01(reader.getSa06scde01());
		comgwpf.setSa06scde02(reader.getSa06scde02());
		comgwpf.setSa06sins(reader.getSa06sins());
		comgwpf.setSaecflg5(reader.getSaecflg5());
		comgwpf.setScno(reader.getScno());
		comgwpf.setSecunum(reader.getSecunum());
		comgwpf.setSlfcrt(reader.getSlfcrt());
		comgwpf.setSpfip(reader.getSpfip());
		comgwpf.setSpltyp(reader.getSpltyp());
		comgwpf.setSpnmknji(reader.getSpnmknji());
		comgwpf.setSpnmscde(reader.getSpnmscde());
		comgwpf.setSubnme(reader.getSubnme());
		comgwpf.setTaxelg(reader.getTaxelg());
		comgwpf.setTelphno(reader.getTelphno());
		comgwpf.setTncdte(reader.getTncdte());
		comgwpf.setTrnendte(reader.getTrnendte());
		comgwpf.setTncp(reader.getTncp());
		comgwpf.setToi(reader.getToi());
		comgwpf.setTotprem(reader.getTotprem());
		comgwpf.setType(reader.getType());
		comgwpf.setUsrprf(reader.getUsrprf());
		comgwpf.setUstatcate(reader.getUstatcate());
		comgwpf.setWrkplc(reader.getWrkplc());
		comgwpf.setYnmrcd(reader.getYnmrcd());
		comgwpf.setAgncnum(reader.getAgncnum());
		comgwpf.setExprtflg(reader.getExprtflg());
		return comgwpf;
	}

	private void insertComgwpf(List<Comgwpf> comgwpfList) {

		try {
			comgwpfDAO.insertIntoComgwpf(comgwpfList);
		} catch (Exception ex) {
			LOGGER.error("Exception in insert {}", ex);
		}
	}

	private void setCheckList(L2CommongwDTO commongwDto) {

		checkList = new ArrayList<>();
		checkList.add(commongwDto.getMop());
		checkList.add(commongwDto.getTotprem());
		checkList.add(commongwDto.getDpcde());
		checkList.add(commongwDto.getCntadd());
		checkList.add(commongwDto.getPnum());
		checkList.add(commongwDto.getSubnme());
		checkList.add(commongwDto.getPrsnmekana());
		checkList.add(commongwDto.getIpnmknji());
		checkList.add(commongwDto.getIpdob());
		checkList.add(commongwDto.getIpage());
		checkList.add(commongwDto.getIpgndr());
		checkList.add(commongwDto.getIprln());
		checkList.add(commongwDto.getIpostcde());
		checkList.add(commongwDto.getIadrsknji());
		checkList.add(commongwDto.getPymntrt());
		checkList.add(commongwDto.getMccppa());
		checkList.add(commongwDto.getMccip());
		checkList.add(commongwDto.getSa01sins());
		checkList.add(commongwDto.getSa01insprd());
		checkList.add(commongwDto.getSa01pp());
		checkList.add(commongwDto.getAcctyp());
		checkList.add(commongwDto.getAccno());
		checkList.add(commongwDto.getDtypcde());
	}
}
