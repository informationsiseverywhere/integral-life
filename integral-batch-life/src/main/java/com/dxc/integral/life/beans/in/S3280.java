package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S3280 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S3280screensfl s3280screensfl = null;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S3280screensfl getS3280screensfl() {
		return s3280screensfl;
	}
	public void setS3280screensfl(S3280screensfl s3280screensfl) {
		this.s3280screensfl = s3280screensfl;
	}
	
	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class S3280screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;
	
		public List<KeyValueItem> getAttributeList() {
			return actions; 
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String select;
			private String effdateDisp;
			private String bankkey;
			private String bankacckey;
			private String mandref;
			private String mandstat;
			private String change;
			private String stdescsh;
			private String atm;
			private String ddop;
			private String bankacckeyhidden;

			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
			public String getEffdateDisp() {
				return effdateDisp;
			}
			public void setEffdateDisp(String effdateDisp) {
				this.effdateDisp = effdateDisp;
			}
			public String getBankkey() {
				return bankkey;
			}
			public void setBankkey(String bankkey) {
				this.bankkey = bankkey;
			}
			public String getBankacckey() {
				return bankacckey;
			}
			public void setBankacckey(String bankacckey) {
				this.bankacckey = bankacckey;
			}
			public String getMandref() {
				return mandref;
			}
			public void setMandref(String mandref) {
				this.mandref = mandref;
			}
			public String getMandstat() {
				return mandstat;
			}
			public void setMandstat(String mandstat) {
				this.mandstat = mandstat;
			}
			public String getChange() {
				return change;
			}
			public void setChange(String change) {
				this.change = change;
			}
			public String getStdescsh() {
				return stdescsh;
			}
			public void setStdescsh(String stdescsh) {
				this.stdescsh = stdescsh;
			}
			public String getAtm() {
				return atm;
			}
			public void setAtm(String atm) {
				this.atm = atm;
			}
			public String getDdop() {
				return ddop;
			}
			public void setDdop(String ddop) {
				this.ddop = ddop;
			}
			public String getBankacckeyhidden() {
				return bankacckeyhidden;
			}
			public void setBankacckeyhidden(String bankacckeyhidden) {
				this.bankacckeyhidden = bankacckeyhidden;
			}
		} 
	}

	@Override
	public String toString() {
		return "S3280 []";
	}
	
}
