package com.dxc.integral.life.beans.out;

public class L2CreateContractExceptionsBean {

	String proposalNumber;
	String contractType;
	String contractCurrency;
	String contractOwnerNameKana;
	String contractOwnerNameKanji;
	String agentNumber;
	String agencyNumber;
	String effDateOfBatchRun;
	String errorMessage;
	
	public String getProposalNumber() {
		return proposalNumber;
	}
	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getContractCurrency() {
		return contractCurrency;
	}
	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	public String getContractOwnerNameKana() {
		return contractOwnerNameKana;
	}
	public void setContractOwnerNameKana(String contractOwnerNameKana) {
		this.contractOwnerNameKana = contractOwnerNameKana;
	}
	public String getContractOwnerNameKanji() {
		return contractOwnerNameKanji;
	}
	public void setContractOwnerNameKanji(String contractOwnerNameKanji) {
		this.contractOwnerNameKanji = contractOwnerNameKanji;
	}
	public String getAgentNumber() {
		return agentNumber;
	}
	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}
	public String getAgencyNumber() {
		return agencyNumber;
	}
	public void setAgencyNumber(String agencyNumber) {
		this.agencyNumber = agencyNumber;
	}
	public String getEffDateOfBatchRun() {
		return effDateOfBatchRun;
	}
	public void setEffDateOfBatchRun(String effDateOfBatchRun) {
		this.effDateOfBatchRun = effDateOfBatchRun;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public String toString() {
		return "L2CreateContractExceptionsBean []";
	}

}
