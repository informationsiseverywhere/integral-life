package com.dxc.integral.life.general.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.dxc.integral.life.beans.L2asstmgmtReaderDTO;

public class L2asstmgmtReaderRowMapper implements RowMapper<L2asstmgmtReaderDTO> {

	@Override
	public L2asstmgmtReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		L2asstmgmtReaderDTO l2asstmgmtReader = new L2asstmgmtReaderDTO();
		l2asstmgmtReader.setAgentNum(rs.getString("ADVISORCODE"));
		l2asstmgmtReader.setChdrnum(rs.getString("CONTRACTNO"));		
		l2asstmgmtReader.setCrtable(rs.getString("PRODUCTCODE"));
		l2asstmgmtReader.setFirstName(rs.getString("ADVISORNAME"));
		l2asstmgmtReader.setLastName("");
		l2asstmgmtReader.setNoFundUnit(rs.getString("ACCNTBALANCE"));		
		l2asstmgmtReader.setSplitc(rs.getString("COMMISSIONSPLIT"));
		l2asstmgmtReader.setVrtfnd(rs.getString("FUNDCODE"));		
		return l2asstmgmtReader;
	}

}
