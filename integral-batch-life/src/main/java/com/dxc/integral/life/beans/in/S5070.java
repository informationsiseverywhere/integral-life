package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5070 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String mandref;
	private String bankkey;
	private String mrbnk;
	private String bnkbrn;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getMrbnk() {
		return mrbnk;
	}
	public void setMrbnk(String mrbnk) {
		this.mrbnk = mrbnk;
	}
	public String getBnkbrn() {
		return bnkbrn;
	}
	public void setBnkbrn(String bnkbrn) {
		this.bnkbrn = bnkbrn;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5070 []";
	}
	
}
