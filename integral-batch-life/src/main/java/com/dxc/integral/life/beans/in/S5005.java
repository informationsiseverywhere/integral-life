package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5005 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String lifesel;
	private String sex;
	private String dobDisp;
	private String anbage;
	private String selection;
	private String smoking;
	private String occup;
	private String pursuit01;
	private String pursuit02;
	private String relation;
	private String optind01;
	private String height;
	private String weight;
	private String optind02;
	private String optind03;
	private String relationwithowner;
	private String optind04;
	private String occupationClass;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getLifesel() {
		return lifesel;
	}
	public void setLifesel(String lifesel) {
		this.lifesel = lifesel;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getDobDisp() {
		return dobDisp;
	}
	public void setDobDisp(String dobDisp) {
		this.dobDisp = dobDisp;
	}
	public String getAnbage() {
		return anbage;
	}
	public void setAnbage(String anbage) {
		this.anbage = anbage;
	}
	public String getSelection() {
		return selection;
	}
	public void setSelection(String selection) {
		this.selection = selection;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getOccup() {
		return occup;
	}
	public void setOccup(String occup) {
		this.occup = occup;
	}
	public String getPursuit01() {
		return pursuit01;
	}
	public void setPursuit01(String pursuit01) {
		this.pursuit01 = pursuit01;
	}
	public String getPursuit02() {
		return pursuit02;
	}
	public void setPursuit02(String pursuit02) {
		this.pursuit02 = pursuit02;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getOptind01() {
		return optind01;
	}
	public void setOptind01(String optind01) {
		this.optind01 = optind01;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getOptind02() {
		return optind02;
	}
	public void setOptind02(String optind02) {
		this.optind02 = optind02;
	}
	public String getOptind03() {
		return optind03;
	}
	public void setOptind03(String optind03) {
		this.optind03 = optind03;
	}
	public String getRelationwithowner() {
		return relationwithowner;
	}
	public void setRelationwithowner(String relationwithowner) {
		this.relationwithowner = relationwithowner;
	}
	public String getOptind04() {
		return optind04;
	}
	public void setOptind04(String optind04) {
		this.optind04 = optind04;
	}
	public String getOccupationClass() {
		return occupationClass;
	}
	public void setOccupationClass(String occupationClass) {
		this.occupationClass = occupationClass;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5005 []";
	}
	
}
