package com.dxc.integral.life.general.writers;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import com.dxc.integral.life.beans.out.L2CreateContractExceptionsBean;
import com.dxc.integral.life.beans.out.L2ProposalsCreatedBean;
import com.dxc.integral.life.utils.CSVUtils;
import org.slf4j.Logger;

@StepScope
public class L2CreateContractItemWriter implements ItemWriter<L2ProposalsCreatedBean> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CreateContractItemWriter.class);
	private static final String NEWOREXISTING="(New/Existing)";
	
	@Autowired
	private Environment env;
	
	@Value("#{stepExecution}")
	private StepExecution stepExecution; 
	
	private List<String> printList; 
	private String batchname;
	private Long batchId;
	private File file;
	@Override
	public synchronized void write(List<? extends L2ProposalsCreatedBean> readerList) throws Exception {

		batchname = stepExecution.getJobExecution().getJobInstance().getJobName().toUpperCase(Locale.getDefault()); 
		batchId = stepExecution.getJobExecution().getJobInstance().getId();
		file =  FileSystems.getDefault().getPath(env.getProperty("report.csv.path").trim()).toFile();
		
		List<L2CreateContractExceptionsBean> errorList = new ArrayList<>(readerList.size());
		for(int idx=0; idx< readerList.size(); idx++){
			errorList.add(readerList.get(idx).getL2CreateContractExceptionsBean());
		}
		
		if(!errorList.isEmpty()){
			generateErrorData(errorList);
		}
		if(!readerList.isEmpty()){
			generateProposalData(readerList);
		}

	}
	public void generateErrorData(List<L2CreateContractExceptionsBean> errorList){
		String fileErrorName = batchname+"_0000"+batchId+"Exception";
		
		File errorCSV = FileSystems.getDefault().getPath(file.getPath() + "\\" 
				+ fileErrorName + ".csv").toFile();
		
		 try(OutputStream outputStream = Files.newOutputStream(errorCSV.toPath());){
		  Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8 );
		  CSVUtils.writeLine(writer, Arrays.asList("Proposal Number", "Contract Type", "Contract Currency","Contract Owner Name (Kana)","Contract Owner Name (Kanji)","Agent Number","Agency Number","Effective Date of Batch Run","Error Message"));
		  printList = new ArrayList<>();
		  for(int idx=0; idx< errorList.size(); idx++){
			 L2CreateContractExceptionsBean l2CreateContractExceptionsBean = errorList.get(idx);
			 if(null!=l2CreateContractExceptionsBean.getContractType() && null!=l2CreateContractExceptionsBean.getErrorMessage()){
			    	  
			  printList.add(l2CreateContractExceptionsBean.getProposalNumber()+","+
					  		l2CreateContractExceptionsBean.getContractType()+","+
					  		l2CreateContractExceptionsBean.getContractCurrency()+","+
					  		l2CreateContractExceptionsBean.getContractOwnerNameKana()+","+
					  		l2CreateContractExceptionsBean.getContractOwnerNameKanji()+","+
					  		l2CreateContractExceptionsBean.getAgentNumber()+","+
					  		l2CreateContractExceptionsBean.getAgencyNumber()+","+
					  		l2CreateContractExceptionsBean.getEffDateOfBatchRun()+","+
					  		l2CreateContractExceptionsBean.getErrorMessage()  	
					  );
			 }
		  }

		 CSVUtils.writeLine(writer, printList,'\n');
		
	     writer.close();
		} catch (IOException e) {	
			LOGGER.error("Exception thrown while writing CSV file : {}",e.getMessage().replaceAll("[\r\n]",""));
		}
	}
	public void generateProposalData(List<? extends L2ProposalsCreatedBean> readerList){
		String fileCreatedName = batchname+"_0000"+batchId+"ProposalsCreated";
		
		File outputCSV = FileSystems.getDefault().getPath(file.getPath() + "\\" 
				+ fileCreatedName + ".csv").toFile();
		try(OutputStream outputStream = Files.newOutputStream(outputCSV.toPath());){	
		 Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8 );
	     CSVUtils.writeLine(writer, Arrays.asList(
	    		 "Proposal Number",
	    		 "Contract Number",
	    		 "Contract Type",
	    		 "Contract Owner",
	    		 NEWOREXISTING,
	    		 "Contract Owner Name (Kana)",
	    		 "Contract Owner Name (Kanji)",
	    		 "Contract Currency",
	    		 "Contract Date",
	    		 "Billing Frequency",
	    		 "Method of Payment",
	    		 "Agent Number",
	    		 "Agency Number",
	    		 "Life Assured",
	    		 NEWOREXISTING,
	    		 "Life Assured Name (Kana)",
	    		 "Life Assured Name (Kanji)",
	    		 "Beneficiary 01",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 01",
	    		 "Beneficiary Name (Kanji) 01",
	    		 "Beneficiary 02",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 02",
	    		 "Beneficiary Name (Kanji) 02",
	    		 "Beneficiary 03",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 03",
	    		 "Beneficiary Name (Kanji) 03",
	    		 "Beneficiary 04",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 04",
	    		 "Beneficiary Name (Kanji) 04",
	    		 "Beneficiary 05",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 05",
	    		 "Beneficiary Name (Kanji) 05",
	    		 "Beneficiary 06",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 06",
	    		 "Beneficiary Name (Kanji) 06",
	    		 "Beneficiary 07",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 07",
	    		 "Beneficiary Name (Kanji) 07",
	    		 "Beneficiary 08",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 08",
	    		 "Beneficiary Name (Kanji) 08",
	    		 "Beneficiary 09",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 09",
	    		 "Beneficiary Name (Kanji) 09",
	    		 "Beneficiary 10",
	    		 NEWOREXISTING,
	    		 "Beneficiary Name (Kana) 10",
	    		 "Beneficiary Name (Kanji) 10",
	    		 "Guardian 01",
	    		 NEWOREXISTING,
	    		 "Guardian Name (Kana) 01",
	    		 "Guardian Name (Kanji) 01",
	    		 "Guardian 02",
	    		 NEWOREXISTING,
	    		 "Guardian Name (Kana) 02",
	    		 "Guardian Name (Kanji) 02",
	    		 "Effective Date of Batch Run"));
	     printList = new ArrayList<>();
	     for(int idx=0; idx< readerList.size(); idx++){
	    	 L2ProposalsCreatedBean l2ProposalsCreatedBean = readerList.get(idx);
	    	 if(null!=l2ProposalsCreatedBean.getProposalNumber() && null!=l2ProposalsCreatedBean.getContractType()){
	    	 printList.add(l2ProposalsCreatedBean.getProposalNumber()+","+
	    			 	   l2ProposalsCreatedBean.getContractNumber()+","+
	    			 	   l2ProposalsCreatedBean.getContractType()+","+
	    			 	   l2ProposalsCreatedBean.getContractOwner()+","+
	    			 	   l2ProposalsCreatedBean.getNewOrExistingOwner()+","+
	    			 	   l2ProposalsCreatedBean.getContractOwnerNameKana()+","+
	    			 	   l2ProposalsCreatedBean.getContractOwnerNameKanji()+","+
	    			 	   l2ProposalsCreatedBean.getContractCurrency()+","+
	    			 	   l2ProposalsCreatedBean.getContractDate()+","+
	    			 	   l2ProposalsCreatedBean.getBillingFrequency()+","+
	    			 	   l2ProposalsCreatedBean.getMethodOfPayment()+","+
	    			 	   l2ProposalsCreatedBean.getAgentNumber()+","+
	    			 	   l2ProposalsCreatedBean.getAgencyNumber()+","+
	    			 	   l2ProposalsCreatedBean.getLifeAssured()+","+
	    			 	   l2ProposalsCreatedBean.getNeworExistingLA()+","+
	    			 	   l2ProposalsCreatedBean.getLifeAssuredNameKana()+","+
	    			 	   l2ProposalsCreatedBean.getLifeAssuredNameKanji()+","+
	    			 	   getBeneficiaryData(l2ProposalsCreatedBean)+
	    			 	   getGuardianData(l2ProposalsCreatedBean)+
	    			 	   l2ProposalsCreatedBean.getEffectiveDateofBatchRun()
		    		 );
		  }
	    	
	     }
	     CSVUtils.writeLine(writer, printList,'\n');
	     
	     writer.close();

		} catch (IOException e) {
			LOGGER.error("Exception thrown while writing CSV file : {}",e.getMessage().replaceAll("[\r\n]",""));
		}
	}
	@Override
	public String toString() {
		return "L2CreateContractItemWriter []";
	}
	
	public String getBeneficiaryData(L2ProposalsCreatedBean l2ProposalsCreatedBean) {
		StringBuilder sb = new StringBuilder();
		if(!l2ProposalsCreatedBean.getBeneficiary().isEmpty()) {
			for(int i=0; i<10; i++) {
			 	  sb.append(l2ProposalsCreatedBean.getBeneficiary().get(i)).append(",");
			 	  sb.append(l2ProposalsCreatedBean.getNewOrExistingBenef().get(i)).append(",");
			 	  sb.append(l2ProposalsCreatedBean.getBeneficiaryNameKana().get(i)).append(",");
			 	  sb.append(l2ProposalsCreatedBean.getBeneficiaryNameKanji().get(i)).append(",");
		 	}
		}
		return sb.toString();
	}
	
	public String getGuardianData(L2ProposalsCreatedBean l2ProposalsCreatedBean) {
		StringBuilder sb = new StringBuilder();
		if(!l2ProposalsCreatedBean.getGuardian().isEmpty()) {
			for(int i=0; i<2; i++) {
			 	  sb.append(l2ProposalsCreatedBean.getGuardian().get(i)).append(",");
			 	  sb.append(l2ProposalsCreatedBean.getNewOrExistingGuard().get(i)).append(",");
			 	  sb.append(l2ProposalsCreatedBean.getGuardianNameKana().get(i)).append(",");
			 	  sb.append(l2ProposalsCreatedBean.getGuardianNameKanji().get(i)).append(",");
		 	 }
		}
		return sb.toString();
	}
	
}
