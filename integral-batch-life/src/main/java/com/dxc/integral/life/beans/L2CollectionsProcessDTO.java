package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.life.dao.model.LifacmvPojo;
import com.dxc.integral.life.dao.model.LifrtrnPojo;
import com.dxc.integral.life.smarttable.pojo.T3695;
import com.dxc.integral.life.smarttable.pojo.T5679;

public class L2CollectionsProcessDTO {

	private Integer[][] wsaaT5645Cnttot;
    private String[][] wsaaT5645Glmap;
    private String[][] wsaaT5645Sacscode;
    private String[][] wsaaT5645Sacstype;
    private String[][] wsaaT5645Sign;
    private RnlAllRec rnlallrec = new RnlAllRec();
    private LifacmvPojo lifacmvPojo = new LifacmvPojo();
    private LifrtrnDTO lifrtrnPojo = new LifrtrnDTO();
    private T3695 t3695;
    private T5679 t5679;
    private Map<String, String> wsaaT6687Taxrelsubr = new HashMap<>();
    private Map<String, String> wsaaT5644Comsub = new HashMap<>();
    private Map<String, String> wsaaT6654Collsub = new HashMap<>();
    private Map<String, String> wsaaT5534UnitFreq = new HashMap<>();
    private Map<String, List<String>> wsaaT5671Subprogs = new HashMap<>();
    private List<WsaaT5688Rec> wsaaT5688Recs = new ArrayList<>();
    private Map<String, WsaaT5667ArrayInner> wsaaT5667ArrayInner = new HashMap<>();
    private Map<String, WsaaT5687Rec> wsaaT5687Rec = new HashMap<>();
    private String th605recIndic;
    private boolean isSkiped = false;
    private String wsaaPrevChdrnum;
    private String wsaaPrevLins;
    private BigDecimal wsaaNetCbillamt = BigDecimal.ZERO;
    private BigDecimal wsaaSuspAvail = BigDecimal.ZERO;
    private BigDecimal wsaaWaiveAmtPaid = BigDecimal.ZERO;
    private BigDecimal wsaaAmtPaid = BigDecimal.ZERO;
    private BigDecimal wsaaToleranceAllowed = BigDecimal.ZERO;
    private BigDecimal wsaaSuspCbillDiff = BigDecimal.ZERO;
    private BigDecimal wsaaImRatio = BigDecimal.ZERO;
    private BigDecimal wsaaCntcurrReceived = BigDecimal.ZERO;
    private BigDecimal temptot = BigDecimal.ZERO;
    private boolean agtTerminated = false;
    private WsaaToleranceChk wsaaToleranceChk;
    private Descpf descpfT1688;
	public Integer[][] getWsaaT5645Cnttot() {
		return wsaaT5645Cnttot;
	}
	public void setWsaaT5645Cnttot(Integer[][] wsaaT5645Cnttot) {
		this.wsaaT5645Cnttot = wsaaT5645Cnttot;
	}
	public String[][] getWsaaT5645Glmap() {
		return wsaaT5645Glmap;
	}
	public void setWsaaT5645Glmap(String[][] wsaaT5645Glmap) {
		this.wsaaT5645Glmap = wsaaT5645Glmap;
	}
	public String[][] getWsaaT5645Sacscode() {
		return wsaaT5645Sacscode;
	}
	public void setWsaaT5645Sacscode(String[][] wsaaT5645Sacscode) {
		this.wsaaT5645Sacscode = wsaaT5645Sacscode;
	}
	public String[][] getWsaaT5645Sacstype() {
		return wsaaT5645Sacstype;
	}
	public void setWsaaT5645Sacstype(String[][] wsaaT5645Sacstype) {
		this.wsaaT5645Sacstype = wsaaT5645Sacstype;
	}
	public String[][] getWsaaT5645Sign() {
		return wsaaT5645Sign;
	}
	public void setWsaaT5645Sign(String[][] wsaaT5645Sign) {
		this.wsaaT5645Sign = wsaaT5645Sign;
	}
	public RnlAllRec getRnlallrec() {
		return rnlallrec;
	}
	public void setRnlallrec(RnlAllRec rnlallrec) {
		this.rnlallrec = rnlallrec;
	}
	public LifacmvPojo getLifacmvPojo() {
		return lifacmvPojo;
	}
	public void setLifacmvPojo(LifacmvPojo lifacmvPojo) {
		this.lifacmvPojo = lifacmvPojo;
	}
	public LifrtrnDTO getLifrtrnPojo() {
		return lifrtrnPojo;
	}
	public void setLifrtrnPojo(LifrtrnDTO lifrtrnPojo) {
		this.lifrtrnPojo = lifrtrnPojo;
	}
	public T3695 getT3695() {
		return t3695;
	}
	public void setT3695(T3695 t3695) {
		this.t3695 = t3695;
	}
	public Map<String, String> getWsaaT6687Taxrelsubr() {
		return wsaaT6687Taxrelsubr;
	}
	public void setWsaaT6687Taxrelsubr(Map<String, String> wsaaT6687Taxrelsubr) {
		this.wsaaT6687Taxrelsubr = wsaaT6687Taxrelsubr;
	}
	
	public List<WsaaT5688Rec> getWsaaT5688Recs() {
		return wsaaT5688Recs;
	}
	public void setWsaaT5688Recs(List<WsaaT5688Rec> wsaaT5688Recs) {
		this.wsaaT5688Recs = wsaaT5688Recs;
	}

	public T5679 getT5679() {
		return t5679;
	}
	public void setT5679(T5679 t5679) {
		this.t5679 = t5679;
	}

	public Map<String, String> getWsaaT5644Comsub() {
		return wsaaT5644Comsub;
	}
	public void setWsaaT5644Comsub(Map<String, String> wsaaT5644Comsub) {
		this.wsaaT5644Comsub = wsaaT5644Comsub;
	}

	public Map<String, String> getWsaaT6654Collsub() {
		return wsaaT6654Collsub;
	}
	public void setWsaaT6654Collsub(Map<String, String> wsaaT6654Collsub) {
		this.wsaaT6654Collsub = wsaaT6654Collsub;
	}

	public Map<String, WsaaT5667ArrayInner> getWsaaT5667ArrayInner() {
		return wsaaT5667ArrayInner;
	}
	public void setWsaaT5667ArrayInner(Map<String, WsaaT5667ArrayInner> wsaaT5667ArrayInner) {
		this.wsaaT5667ArrayInner = wsaaT5667ArrayInner;
	}

	public Map<String, List<String>> getWsaaT5671Subprogs() {
		return wsaaT5671Subprogs;
	}
	public void setWsaaT5671Subprogs(Map<String, List<String>> wsaaT5671Subprogs) {
		this.wsaaT5671Subprogs = wsaaT5671Subprogs;
	}

	public String getTh605recIndic() {
		return th605recIndic;
	}
	public void setTh605recIndic(String th605recIndic) {
		this.th605recIndic = th605recIndic;
	}

	public boolean isSkiped() {
		return isSkiped;
	}
	public void setSkiped(boolean isSkiped) {
		this.isSkiped = isSkiped;
	}

	public Map<String, WsaaT5687Rec> getWsaaT5687Rec() {
		return wsaaT5687Rec;
	}
	public void setWsaaT5687Rec(Map<String, WsaaT5687Rec> wsaaT5687Rec) {
		this.wsaaT5687Rec = wsaaT5687Rec;
	}

	public Map<String, String> getWsaaT5534UnitFreq() {
		return wsaaT5534UnitFreq;
	}
	public void setWsaaT5534UnitFreq(Map<String, String> wsaaT5534UnitFreq) {
		this.wsaaT5534UnitFreq = wsaaT5534UnitFreq;
	}

	public String getWsaaPrevChdrnum() {
		return wsaaPrevChdrnum;
	}
	public void setWsaaPrevChdrnum(String wsaaPrevChdrnum) {
		this.wsaaPrevChdrnum = wsaaPrevChdrnum;
	}
	public String getWsaaPrevLins() {
		return wsaaPrevLins;
	}
	public void setWsaaPrevLins(String wsaaPrevLins) {
		this.wsaaPrevLins = wsaaPrevLins;
	}

	public BigDecimal getWsaaNetCbillamt() {
		return wsaaNetCbillamt;
	}
	public void setWsaaNetCbillamt(BigDecimal wsaaNetCbillamt) {
		this.wsaaNetCbillamt = wsaaNetCbillamt;
	}
	public void addWsaaNetCbillamt(BigDecimal netCbillamt) {
		this.wsaaNetCbillamt = this.wsaaNetCbillamt.add(netCbillamt);
	}
	public BigDecimal getWsaaSuspAvail() {
		return wsaaSuspAvail;
	}
	public void setWsaaSuspAvail(BigDecimal wsaaSuspAvail) {
		this.wsaaSuspAvail = wsaaSuspAvail;
	}
	public void addWsaaSuspAvail(BigDecimal suspAvail) {
		this.wsaaSuspAvail.add(suspAvail);
	}
	public BigDecimal getWsaaWaiveAmtPaid() {
		return wsaaWaiveAmtPaid;
	}
	public void setWsaaWaiveAmtPaid(BigDecimal wsaaWaiveAmtPaid) {
		this.wsaaWaiveAmtPaid = wsaaWaiveAmtPaid;
	}
	public BigDecimal getWsaaAmtPaid() {
		return wsaaAmtPaid;
	}
	public void setWsaaAmtPaid(BigDecimal wsaaAmtPaid) {
		this.wsaaAmtPaid = wsaaAmtPaid;
	}
	public BigDecimal getWsaaToleranceAllowed() {
		return wsaaToleranceAllowed;
	}
	public void setWsaaToleranceAllowed(BigDecimal wsaaToleranceAllowed) {
		this.wsaaToleranceAllowed = wsaaToleranceAllowed;
	}
	public BigDecimal getWsaaSuspCbillDiff() {
		return wsaaSuspCbillDiff;
	}
	public void setWsaaSuspCbillDiff(BigDecimal wsaaSuspCbillDiff) {
		this.wsaaSuspCbillDiff = wsaaSuspCbillDiff;
	}
	public BigDecimal getWsaaImRatio() {
		return wsaaImRatio;
	}
	public void setWsaaImRatio(BigDecimal wsaaImRatio) {
		this.wsaaImRatio = wsaaImRatio;
	}
	public BigDecimal getWsaaCntcurrReceived() {
		return wsaaCntcurrReceived;
	}
	public void setWsaaCntcurrReceived(BigDecimal wsaaCntcurrReceived) {
		this.wsaaCntcurrReceived = wsaaCntcurrReceived;
	}

	public boolean isAgtTerminated() {
		return agtTerminated;
	}
	public void setAgtTerminated(boolean agtTerminated) {
		this.agtTerminated = agtTerminated;
	}

	public BigDecimal getTemptot() {
		return temptot;
	}
	public void setTemptot(BigDecimal temptot) {
		this.temptot = temptot;
	}

	public WsaaToleranceChk getWsaaToleranceChk() {
		return wsaaToleranceChk;
	}
	public void setWsaaToleranceChk(WsaaToleranceChk wsaaToleranceChk) {
		this.wsaaToleranceChk = wsaaToleranceChk;
	}

	public Descpf getDescpfT1688() {
		return descpfT1688;
	}
	public void setDescpfT1688(Descpf descpfT1688) {
		this.descpfT1688 = descpfT1688;
	}

	public static final class WsaaT5688Rec {
	    private String wsaaT5688Cnttype;
	    private int wsaaT5688Itmfrm;
	    private String wsaaT5688Comlvlacc;
	    private String wsaaT5688Revacc;
		public String getWsaaT5688Cnttype() {
			return wsaaT5688Cnttype;
		}
		public void setWsaaT5688Cnttype(String wsaaT5688Cnttype) {
			this.wsaaT5688Cnttype = wsaaT5688Cnttype;
		}
		public int getWsaaT5688Itmfrm() {
			return wsaaT5688Itmfrm;
		}
		public void setWsaaT5688Itmfrm(int wsaaT5688Itmfrm) {
			this.wsaaT5688Itmfrm = wsaaT5688Itmfrm;
		}
		public String getWsaaT5688Comlvlacc() {
			return wsaaT5688Comlvlacc;
		}
		public void setWsaaT5688Comlvlacc(String wsaaT5688Comlvlacc) {
			this.wsaaT5688Comlvlacc = wsaaT5688Comlvlacc;
		}
		public String getWsaaT5688Revacc() {
			return wsaaT5688Revacc;
		}
		public void setWsaaT5688Revacc(String wsaaT5688Revacc) {
			this.wsaaT5688Revacc = wsaaT5688Revacc;
		}
	}
	
	public static final class WsaaT5667ArrayInner {

		/* WSAA-T5667-ARRAY */
		private String wsaaT5667Key;
		private String wsaaT5667Sfind;
		private List<String> wsaaT5667Freqs;
		private List<BigDecimal> wsaaT5667MaxAmounts;
		private List<BigDecimal> wsaaT5667Prmtols;
		private List<BigDecimal> wsaaT5667Maxamts;
		private List<BigDecimal> wsaaT5667Prmtolns;
		public String getWsaaT5667Key() {
			return wsaaT5667Key;
		}
		public void setWsaaT5667Key(String wsaaT5667Key) {
			this.wsaaT5667Key = wsaaT5667Key;
		}
		public String getWsaaT5667Sfind() {
			return wsaaT5667Sfind;
		}
		public void setWsaaT5667Sfind(String wsaaT5667Sfind) {
			this.wsaaT5667Sfind = wsaaT5667Sfind;
		}
		public List<String> getWsaaT5667Freqs() {
			return wsaaT5667Freqs;
		}
		public void setWsaaT5667Freqs(List<String> wsaaT5667Freqs) {
			this.wsaaT5667Freqs = wsaaT5667Freqs;
		}
		public List<BigDecimal> getWsaaT5667MaxAmounts() {
			return wsaaT5667MaxAmounts;
		}
		public void setWsaaT5667MaxAmounts(List<BigDecimal> wsaaT5667MaxAmounts) {
			this.wsaaT5667MaxAmounts = wsaaT5667MaxAmounts;
		}
		public List<BigDecimal> getWsaaT5667Prmtols() {
			return wsaaT5667Prmtols;
		}
		public void setWsaaT5667Prmtols(List<BigDecimal> wsaaT5667Prmtols) {
			this.wsaaT5667Prmtols = wsaaT5667Prmtols;
		}
		public List<BigDecimal> getWsaaT5667Maxamts() {
			return wsaaT5667Maxamts;
		}
		public void setWsaaT5667Maxamts(List<BigDecimal> wsaaT5667Maxamts) {
			this.wsaaT5667Maxamts = wsaaT5667Maxamts;
		}
		public List<BigDecimal> getWsaaT5667Prmtolns() {
			return wsaaT5667Prmtolns;
		}
		public void setWsaaT5667Prmtolns(List<BigDecimal> wsaaT5667Prmtolns) {
			this.wsaaT5667Prmtolns = wsaaT5667Prmtolns;
		}

	}
	
	public static final class WsaaT5687Rec {
		private String wsaaT5687Crtable;
	    private int wsaaT5687Itmfrm;
	    private String wsaaT5687Bbmeth;
		public String getWsaaT5687Crtable() {
			return wsaaT5687Crtable;
		}
		public void setWsaaT5687Crtable(String wsaaT5687Crtable) {
			this.wsaaT5687Crtable = wsaaT5687Crtable;
		}
		public int getWsaaT5687Itmfrm() {
			return wsaaT5687Itmfrm;
		}
		public void setWsaaT5687Itmfrm(int wsaaT5687Itmfrm) {
			this.wsaaT5687Itmfrm = wsaaT5687Itmfrm;
		}
		public String getWsaaT5687Bbmeth() {
			return wsaaT5687Bbmeth;
		}
		public void setWsaaT5687Bbmeth(String wsaaT5687Bbmeth) {
			this.wsaaT5687Bbmeth = wsaaT5687Bbmeth;
		}
	}
	
	public static enum WsaaToleranceChk {
		toleranceLimit1, toleranceLimit2;
	}
}
