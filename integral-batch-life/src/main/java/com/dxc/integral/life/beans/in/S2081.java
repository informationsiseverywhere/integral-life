package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2081 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;	
	private String facthous;
	private String bankkey;
	private String bankacount;
	private String bankaccdsc;
	private String bnkactyp;
	private String currcode;
	private String datefromDisp;
	private String dattoDisp;
	private String sctycde;
	private String mrbnk;
	private String bnkbrn;
	private String bankCd;
	private String branchCd;
	private String bnkBrchCd;
	private String zpbcode;
	private String zpbacno;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacount() {
		return bankacount;
	}
	public void setBankacount(String bankacount) {
		this.bankacount = bankacount;
	}
	public String getBankaccdsc() {
		return bankaccdsc;
	}
	public void setBankaccdsc(String bankaccdsc) {
		this.bankaccdsc = bankaccdsc;
	}
	public String getBnkactyp() {
		return bnkactyp;
	}
	public void setBnkactyp(String bnkactyp) {
		this.bnkactyp = bnkactyp;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getDatefromDisp() {
		return datefromDisp;
	}
	public void setDatefromDisp(String datefromDisp) {
		this.datefromDisp = datefromDisp;
	}
	public String getDattoDisp() {
		return dattoDisp;
	}
	public void setDattoDisp(String dattoDisp) {
		this.dattoDisp = dattoDisp;
	}
	public String getSctycde() {
		return sctycde;
	}
	public void setSctycde(String sctycde) {
		this.sctycde = sctycde;
	}
	public String getMrbnk() {
		return mrbnk;
	}
	public void setMrbnk(String mrbnk) {
		this.mrbnk = mrbnk;
	}
	public String getBnkbrn() {
		return bnkbrn;
	}
	public void setBnkbrn(String bnkbrn) {
		this.bnkbrn = bnkbrn;
	}
	public String getBankCd() {
		return bankCd;
	}
	public void setBankCd(String bankCd) {
		this.bankCd = bankCd;
	}
	public String getBranchCd() {
		return branchCd;
	}
	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}
	public String getBnkBrchCd() {
		return bnkBrchCd;
	}
	public void setBnkBrchCd(String bnkBrchCd) {
		this.bnkBrchCd = bnkBrchCd;
	}
	public String getZpbcode() {
		return zpbcode;
	}
	public void setZpbcode(String zpbcode) {
		this.zpbcode = zpbcode;
	}
	public String getZpbacno() {
		return zpbacno;
	}
	public void setZpbacno(String zpbacno) {
		this.zpbacno = zpbacno;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S2081 []";
	}
	
}
