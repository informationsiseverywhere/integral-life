package com.dxc.integral.life.beans.in;
import java.io.Serializable;

public class S5239 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S5239screensfl s5239screensfl = null;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S5239screensfl getS5239screensfl() {
		return s5239screensfl;
	}
	public void setS5239screensfl(S5239screensfl s5239screensfl) {
		this.s5239screensfl = s5239screensfl;
	}

	public static class S5239screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		public int size() {
			return rows.length;
		}

		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
	
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String rgpynum = "";
			private String pymt = "";
			private String rgpytype = "";
			private String rptldesc = "";
			private String select = "";

			public String getRgpynum() {
				return rgpynum;
			}
			public void setRgpynum(String rgpynum) {
				this.rgpynum = rgpynum;
			}
			public String getPymt() {
				return pymt;
			}
			public void setPymt(String pymt) {
				this.pymt = pymt;
			}
			public String getRgpytype() {
				return rgpytype;
			}
			public void setRgpytype(String rgpytype) {
				this.rgpytype = rgpytype;
			}
			public String getRptldesc() {
				return rptldesc;
			}
			public void setRptldesc(String rptldesc) {
				this.rptldesc = rptldesc;
			}
			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
		} 
	}

	@Override
	public String toString() {
		return "S5239 []";
	}
	
}
