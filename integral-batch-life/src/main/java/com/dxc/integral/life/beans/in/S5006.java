package com.dxc.integral.life.beans.in;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S5006 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S5006screensfl s5006screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S5006screensfl getS5006screensfl() {
		return s5006screensfl;
	}
	public void setS5006screensfl(S5006screensfl s5006screensfl) {
		this.s5006screensfl = s5006screensfl;
	}
	
	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class S5006screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;

		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}

		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String select;
			private String ctable;
			private String rtable;
			private String longdesc;
			private String hrequired;

			public String getSelect() {
				return select;
			}
			public void setSelect(String select) {
				this.select = select;
			}
			public String getCtable() {
				return ctable;
			}
			public void setCtable(String ctable) {
				this.ctable = ctable;
			}
			public String getRtable() {
				return rtable;
			}
			public void setRtable(String rtable) {
				this.rtable = rtable;
			}
			public String getLongdesc() {
				return longdesc;
			}
			public void setLongdesc(String longdesc) {
				this.longdesc = longdesc;
			}
			public String getHrequired() {
				return hrequired;
			}
			public void setHrequired(String hrequired) {
				this.hrequired = hrequired;
			}
		} 
	}

	@Override
	public String toString() {
		return "S5006 []";
	}
	
}
