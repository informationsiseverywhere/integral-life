package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for L2NEWINTCP processor.
 * 
 * @author dpuhawan
 *
 */
public class L2newintcpProcessorDTO {

	private BigDecimal interestAmount;
	private Integer lstIntbdate;
	private Integer nxtIntbdate;
	private BigDecimal totalLoanDebtAmount;
	private BigDecimal currBal;
	private Integer nxtCapdate;

	public BigDecimal getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	public Integer getLstIntbdate() {
		return lstIntbdate;
	}

	public void setLstIntbdate(Integer lstIntbdate) {
		this.lstIntbdate = lstIntbdate;
	}

	public Integer getNxtIntbdate() {
		return nxtIntbdate;
	}

	public void setNxtIntbdate(Integer nxtIntbdate) {
		this.nxtIntbdate = nxtIntbdate;
	}

	public BigDecimal getTotalLoanDebtAmount() {
		return totalLoanDebtAmount;
	}

	public void setTotalLoanDebtAmount(BigDecimal totalLoanDebtAmount) {
		this.totalLoanDebtAmount = totalLoanDebtAmount;
	}

	public BigDecimal getCurrBal() {
		return currBal;
	}

	public void setCurrBal(BigDecimal currBal) {
		this.currBal = currBal;
	}

	public Integer getNxtCapdate() {
		return nxtCapdate;
	}

	public void setNxtCapdate(Integer nxtCapdate) {
		this.nxtCapdate = nxtCapdate;
	}
}
