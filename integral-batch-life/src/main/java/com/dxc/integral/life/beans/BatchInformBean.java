package com.dxc.integral.life.beans;

import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Bprdpf;
import com.dxc.integral.iaf.dao.model.Bsscpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;

public class BatchInformBean {

	private Bprdpf bprdpf;
	private Bsscpf bsscpf;
	private Batcpf batchDetail;
	private Usrdpf usrdpf;
	private String programName;
	public Bprdpf getBprdpf() {
		return bprdpf;
	}
	public void setBprdpf(Bprdpf bprdpf) {
		this.bprdpf = bprdpf;
	}
	public Bsscpf getBsscpf() {
		return bsscpf;
	}
	public void setBsscpf(Bsscpf bsscpf) {
		this.bsscpf = bsscpf;
	}
	public Batcpf getBatchDetail() {
		return batchDetail;
	}
	public void setBatchDetail(Batcpf batchDetail) {
		this.batchDetail = batchDetail;
	}
	public Usrdpf getUsrdpf() {
		return usrdpf;
	}
	public void setUsrdpf(Usrdpf usrdpf) {
		this.usrdpf = usrdpf;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}

}
