package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S6378 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String activeField;
	private S6378screensfl s6378screensfl;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public S6378screensfl getS6378screensfl() {
		return s6378screensfl;
	}
	public void setS6378screensfl(S6378screensfl s6378screensfl) {
		this.s6378screensfl = s6378screensfl;
	}

	public static class S6378screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private SFLRow[] rows = new SFLRow[0];
		public int size() {
			return rows.length;
		}

		public SFLRow get(int index) {
			SFLRow result = null;
			if (index > -1) {
				result = rows[index];
			}
			return result;
		}
		public static class SFLRow implements Serializable {
			private static final long serialVersionUID = 1L;
			private String erordsc = "";
			private String life = "";
			private String jlife = "";
			private String coverage = "";
			private String rider = "";
			private String payrseqno = "";

			public String getErordsc() {
				return erordsc;
			}
			public void setErordsc(String erordsc) {
				this.erordsc = erordsc;
			}
			public String getLife() {
				return life;
			}
			public void setLife(String life) {
				this.life = life;
			}
			public String getJlife() {
				return jlife;
			}
			public void setJlife(String jlife) {
				this.jlife = jlife;
			}
			public String getCoverage() {
				return coverage;
			}
			public void setCoverage(String coverage) {
				this.coverage = coverage;
			}
			public String getRider() {
				return rider;
			}
			public void setRider(String rider) {
				this.rider = rider;
			}
			public String getPayrseqno() {
				return payrseqno;
			}
			public void setPayrseqno(String payrseqno) {
				this.payrseqno = payrseqno;
			}
		} 
	}

	@Override
	public String toString() {
		return "S6378 []";
	}
	
}
