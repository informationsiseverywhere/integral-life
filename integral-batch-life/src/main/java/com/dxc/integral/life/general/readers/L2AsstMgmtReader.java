package com.dxc.integral.life.general.readers;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.beans.L2asstmgmtReaderDTO;
import com.dxc.integral.life.general.processors.L2asstmgmtReadStoredProc;

@Component
@StepScope
public class L2AsstMgmtReader  extends BaseDAOImpl implements ItemReader<L2asstmgmtReaderDTO>{

	private static final Logger LOGGER = LoggerFactory.getLogger(L2AsstMgmtReader.class);	
    private L2asstmgmtReadStoredProc readStoredProc;	
    @Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['trandate']}")
	private String trandate;

	@Value("#{jobParameters['businessDate']}")
	private String businessDate;
	
	@PostConstruct
	public void postConstruct() {
		readStoredProc = new L2asstmgmtReadStoredProc(jdbcTemplate,businessDate);
	}
	
	private int recordCount=0;
	
	private List<L2asstmgmtReaderDTO> asstmgmtRecordlist=null;	
	
	@Override
	public L2asstmgmtReaderDTO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {		
		if(asstmgmtRecordlist == null){
			asstmgmtRecordlist = readStoredProc.getAllDetails();			
			}
		if(!asstmgmtRecordlist.isEmpty()){
			LOGGER.info("Inside L2AsstMgmtReader class : Number of records read from Stored Procedure: {}",
					asstmgmtRecordlist.size());//IJTI-1498
			if(recordCount == asstmgmtRecordlist.size()){
				return null;
			}else{
				return asstmgmtRecordlist.get(recordCount++);
			}			
		}else {			
			return null;
		}			
	}	
	
}
