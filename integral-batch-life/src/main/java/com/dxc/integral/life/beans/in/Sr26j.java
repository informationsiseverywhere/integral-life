package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr26j implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String crdtcrd;
	private String mandAmt;
	private String detlsumm;
	private String mandstat;
	private String effdateDisp;
	private String atm;
	private String ddop;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getCrdtcrd() {
		return crdtcrd;
	}
	public void setCrdtcrd(String crdtcrd) {
		this.crdtcrd = crdtcrd;
	}
	public String getMandAmt() {
		return mandAmt;
	}
	public void setMandAmt(String mandAmt) {
		this.mandAmt = mandAmt;
	}
	public String getDetlsumm() {
		return detlsumm;
	}
	public void setDetlsumm(String detlsumm) {
		this.detlsumm = detlsumm;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getEffdateDisp() {
		return effdateDisp;
	}
	public void setEffdateDisp(String effdateDisp) {
		this.effdateDisp = effdateDisp;
	}
	public String getAtm() {
		return atm;
	}
	public void setAtm(String atm) {
		this.atm = atm;
	}
	public String getDdop() {
		return ddop;
	}
	public void setDdop(String ddop) {
		this.ddop = ddop;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "Sr26j []";
	}
	
}
