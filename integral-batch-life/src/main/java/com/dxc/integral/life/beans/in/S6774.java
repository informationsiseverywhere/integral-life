package com.dxc.integral.life.beans.in;
import java.io.Serializable;
public class S6774 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String undwflag;
	private String select;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getUndwflag() {
		return undwflag;
	}
	public void setUndwflag(String undwflag) {
		this.undwflag = undwflag;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S6774 []";
	}
	
}
