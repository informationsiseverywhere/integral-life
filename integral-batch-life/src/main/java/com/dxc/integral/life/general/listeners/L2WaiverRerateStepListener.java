package com.dxc.integral.life.general.listeners;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.L2WaiverRerateControlTotalDTO;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.smarttable.pojo.T5655;
import com.fasterxml.jackson.databind.ObjectMapper;


public class L2WaiverRerateStepListener extends IntegralStepListener {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2WaiverRerateStepListener.class);

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private L2WaiverRerateControlTotalDTO l2WaiverRerateControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;
	
	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['chdrcoy']}")
	private String company;
	
	@Value("#{jobParameters['trandate']}")
	private String effDate;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Value("#{jobParameters['transactionDate']}") 
	private String transactionDate;
	
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private ElogpfDAO elogpfDAO;

	private static String programName = "BR615";

	
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("L2WaiverRerateStepListener::afterStep::Entry");
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.info("Control total update for L2WaiverRerateStepListener batch starts");
			Sbcontotpf sbcontotpf = new Sbcontotpf();
			sbcontotpf.setStepname(stepExecution.getStepName());
			sbcontotpf.setUsrprf(userName);
			sbcontotpf.setBranch(branch);
			sbcontotpf.setCompany(company);
			sbcontotpf.setEffdate(Integer.parseInt(effDate));
			sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
			sbcontotpf.setJobnm(batchName);
			sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
			sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
			sbcontotpf.setContot2(l2WaiverRerateControlTotalDTO.getControlTotal02());
			sbcontotpf.setContot3(l2WaiverRerateControlTotalDTO.getControlTotal03());
			sbcontotpf.setContot4(l2WaiverRerateControlTotalDTO.getControlTotal04());
			sbcontotpf.setContot5(l2WaiverRerateControlTotalDTO.getControlTotal05());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);
			LOGGER.info("Control total update for L2WaiverRerateStepListener batch ends");
			LOGGER.info("L2WaiverRerateStepListener batch step completed.");
		} else {
			LOGGER.error("Some exception occurred,logging it in Elogpf.{}", 
					exceptionList.get(0).getStackTrace().toString());//IJTI-1498
			if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", programName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("L2WaiverRerateStepListener::afterStep::Exit");
		return stepExecution.getExitStatus();
	}
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("L2WaiverRerateStepListener::beforeStep::Entry");
		super.beforeStep(stepExecution);
		int trandate = readMaxLeadDays();
		stepExecution.getExecutionContext().putInt("trandate", trandate);
		LOGGER.debug("L2WaiverRerateStepListener::beforeStep::Exit");
	}

	private int readMaxLeadDays() {
		int maxLeadDays = 0;
		List<Itempf> t5655List = itempfDAO.readSmartTableListByTableName(company, "T5655", CommonConstants.IT_ITEMPFX);
		if (t5655List != null && !t5655List.isEmpty()) {
			Iterator<Itempf> iterator = t5655List.iterator();
			try {
				while (iterator.hasNext()) {
					Itempf itempf = iterator.next();
					if (itempf.getGenareaj() != null) {
						T5655 t5655IO = new ObjectMapper().readValue(itempf.getGenareaj(), T5655.class);
						if (itempf.getItemitem().contains("B672") && t5655IO.getLeadDays() > maxLeadDays) {
							maxLeadDays = t5655IO.getLeadDays();
						}
					}
				}
			} catch (IOException e) {
				throw new ItemParseException("Item parsing failed in Table T6654.");
			}
		}
		return datcon2.plusDays(Integer.parseInt(effDate), maxLeadDays);
		
	}
}
