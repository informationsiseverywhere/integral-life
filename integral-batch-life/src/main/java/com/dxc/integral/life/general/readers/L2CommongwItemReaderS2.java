package com.dxc.integral.life.general.readers;

import java.util.List;


import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.fsu.dao.AgncypfDAO;
import com.dxc.integral.fsu.dao.model.Agncypf;

/**
 * 
 * ItemReader for L2commongw batch step.
 *
 */
public class L2CommongwItemReaderS2 implements ItemReader<Agncypf> {

	@Autowired
	private AgncypfDAO agncypfDAO;
	@Value("#{jobParameters['effectiveDate']}")
	private String effectiveDate;

	private int recordCount = 0;
	private List<Agncypf> list;

	public void init() {
		list = agncypfDAO.getAgncypfByDate(effectiveDate);
	}

	@Override
	public Agncypf read() throws Exception {
		if (list != null && recordCount < list.size()) {
			return list.get(recordCount++);
		} else {
			if (list != null) {
				list = null;
			}
			recordCount = 0;
			return null;
		}
	}
}