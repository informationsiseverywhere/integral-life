package com.dxc.integral.life.beans.servicebean;
import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.dxc.integral.life.beans.in.*;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LcreatenewcontractidvServiceInputBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private AuthenticationInfo authenticationInfo;
	private S5002 s5002;
	private List<S5004> s5004;
	private List<S5011> s5011;
	private List<Sr53a> sr53a;
	private Sr25m sr25m;
	private Sr26j sr26j;
	private List<S5070> s5070;
	private List<Sr562> sr562;
	private Sr60d sr60d;
	private Sr5bc sr5bc;
	private List<S5005> s5005;
	private S5006 s5006;
	private List<S6326> s6326;
	private List<S5417> s5417;
	private List<S5123> s5123;
	private S6765 s6765;
	private S6774 s6774;
	private List<Sr516> sr516;
	private List<Sr676> sr676;
	private Sr677 sr677;
	private List<S5125> s5125;
	private List<Sh504> sh504;
	private List<S5220> s5220;
	private List<S5226> s5226;
	private List<S5239> s5239;
	private S6378 s6378;
	private List<S5003> s5003;
	private List<S5437> s5437;
	private List<S5438> s5438;
	private List<S2464> s2464;
	private List<S2465> s2465;
	private List<S3280> s3280;
	private List<S3281> s3281;
	private List<S2571> s2571;
	private List<S2081> s2081;
	private List<S5010> s5010;
	private List<Sr626> sr626;
	private List<Sr5ah> sr5ah;
	private List<Sr5ai> sr5ai;
	private List<S2466> s2466;
	private List<S5007> s5007;
	
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}
	public S5002 getS5002() {
		return s5002;
	}
	public void setS5002(S5002 s5002) {
		this.s5002 = s5002;
	}

	public Sr25m getSr25m() {
		return sr25m;
	}
	public void setSr25m(Sr25m sr25m) {
		this.sr25m = sr25m;
	}
	public Sr26j getSr26j() {
		return sr26j;
	}
	public void setSr26j(Sr26j sr26j) {
		this.sr26j = sr26j;
	}
	
	public Sr60d getSr60d() {
		return sr60d;
	}
	public void setSr60d(Sr60d sr60d) {
		this.sr60d = sr60d;
	}
	public Sr5bc getSr5bc() {
		return sr5bc;
	}
	public void setSr5bc(Sr5bc sr5bc) {
		this.sr5bc = sr5bc;
	}
	
	public S5006 getS5006() {
		return s5006;
	}
	public void setS5006(S5006 s5006) {
		this.s5006 = s5006;
	}
	public  List<S6326>  getS6326() {
		return s6326;
	}
	public void setS6326( List<S6326> s6326) {
		this.s6326 = s6326;
	}
	public List<S5417>  getS5417() {
		return s5417;
	}
	public void setS5417(List<S5417> s5417) {
		this.s5417 = s5417;
	}
	public List<S5123>  getS5123() {
		return s5123;
	}
	public void setS5123(List<S5123> s5123) {
		this.s5123 = s5123;
	}
	public S6765 getS6765() {
		return s6765;
	}
	public void setS6765(S6765 s6765) {
		this.s6765 = s6765;
	}
	public S6774 getS6774() {
		return s6774;
	}
	public void setS6774(S6774 s6774) {
		this.s6774 = s6774;
	}
	public List<Sr516>  getSr516() {
		return sr516;
	}
	public void setSr516(List<Sr516> sr516) {
		this.sr516 = sr516;
	}
	public List<Sr676>  getSr676() {
		return sr676;
	}
	public void setSr676(List<Sr676> sr676) {
		this.sr676 = sr676;
	}
	public Sr677 getSr677() {
		return sr677;
	}
	public void setSr677(Sr677 sr677) {
		this.sr677 = sr677;
	}
	public  List<S5125>  getS5125() {
		return s5125;
	}
	public void setS5125( List<S5125> s5125) {
		this.s5125 = s5125;
	}
	public List<S5004> getS5004() {
		return s5004;
	}
	public void setS5004(List<S5004> s5004) {
		this.s5004 = s5004;
	}
	public List<S5011> getS5011() {
		return s5011;
	}
	public void setS5011(List<S5011> s5011) {
		this.s5011 = s5011;
	}
	public List<Sr53a> getSr53a() {
		return sr53a;
	}
	public void setSr53a(List<Sr53a> sr53a) {
		this.sr53a = sr53a;
	}
	public List<S5070> getS5070() {
		return s5070;
	}
	public void setS5070(List<S5070> s5070) {
		this.s5070 = s5070;
	}
	public List<Sr562> getSr562() {
		return sr562;
	}
	public void setSr562(List<Sr562> sr562) {
		this.sr562 = sr562;
	}
	public List<S5005> getS5005() {
		return s5005;
	}
	public void setS5005(List<S5005> s5005) {
		this.s5005 = s5005;
	}
	public List<Sh504> getSh504() {
		return sh504;
	}
	public void setSh504(List<Sh504> sh504) {
		this.sh504 = sh504;
	}
	public List<S5220> getS5220() {
		return s5220;
	}
	public void setS5220(List<S5220> s5220) {
		this.s5220 = s5220;
	}
	public List<S5226> getS5226() {
		return s5226;
	}
	public void setS5226(List<S5226> s5226) {
		this.s5226 = s5226;
	}
	public List<S5239> getS5239() {
		return s5239;
	}
	public void setS5239(List<S5239> s5239) {
		this.s5239 = s5239;
	}
	public S6378 getS6378() {
		return s6378;
	}
	public void setS6378(S6378 s6378) {
		this.s6378 = s6378;
	}
	public List<S5003> getS5003() {
		return s5003;
	}
	public void setS5003(List<S5003> s5003) {
		this.s5003 = s5003;
	}
	public List<S5437> getS5437() {
		return s5437;
	}
	public void setS5437(List<S5437> s5437) {
		this.s5437 = s5437;
	}
	public List<S5438> getS5438() {
		return s5438;
	}
	public void setS5438(List<S5438> s5438) {
		this.s5438 = s5438;
	}
	public List<S2464> getS2464() {
		return s2464;
	}
	public void setS2464(List<S2464> s2464) {
		this.s2464 = s2464;
	}
	public List<S2465> getS2465() {
		return s2465;
	}
	public void setS2465(List<S2465> s2465) {
		this.s2465 = s2465;
	}
	public List<S3280> getS3280() {
		return s3280;
	}
	public void setS3280(List<S3280> s3280) {
		this.s3280 = s3280;
	}
	public List<S3281> getS3281() {
		return s3281;
	}
	public void setS3281(List<S3281> s3281) {
		this.s3281 = s3281;
	}
	public List<S2571> getS2571() {
		return s2571;
	}
	public void setS2571(List<S2571> s2571) {
		this.s2571 = s2571;
	}
	public List<S2081> getS2081() {
		return s2081;
	}
	public void setS2081(List<S2081> s2081) {
		this.s2081 = s2081;
	}
	public List<S5010> getS5010() {
		return s5010;
	}
	public void setS5010(List<S5010> s5010) {
		this.s5010 = s5010;
	}
	public List<Sr626> getSr626() {
		return sr626;
	}
	public void setSr626(List<Sr626> sr626) {
		this.sr626 = sr626;
	}
	public List<Sr5ah> getSr5ah() {
		return sr5ah;
	}
	public void setSr5ah(List<Sr5ah> sr5ah) {
		this.sr5ah = sr5ah;
	}
	public List<Sr5ai> getSr5ai() {
		return sr5ai;
	}
	public void setSr5ai(List<Sr5ai> sr5ai) {
		this.sr5ai = sr5ai;
	}
	public List<S2466> getS2466() {
		return s2466;
	}
	public void setS2466(List<S2466> s2466) {
		this.s2466 = s2466;
	}
	public List<S5007> getS5007() {
		return s5007;
	}
	public void setS5007(List<S5007> s5007) {
		this.s5007 = s5007;
	}
	@Override
	public String toString() {
		return "lcreatenewcontractidvServiceInputBean []";
	}

}
