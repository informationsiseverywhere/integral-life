package com.dxc.integral.life.beans.in;
import java.io.Serializable;
public class S5220 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String advance;
	private String arrears;
	private String intanny;
	private String capcont;
	private String withprop;
	private String withoprop;
	private String ppind;
	private String dthpercn;
	private String dthperco;
	private String guarperd;
	private String nomlife;
	private String freqann;
	private String activeField;
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}
	public String getArrears() {
		return arrears;
	}
	public void setArrears(String arrears) {
		this.arrears = arrears;
	}
	public String getIntanny() {
		return intanny;
	}
	public void setIntanny(String intanny) {
		this.intanny = intanny;
	}
	public String getCapcont() {
		return capcont;
	}
	public void setCapcont(String capcont) {
		this.capcont = capcont;
	}
	public String getWithprop() {
		return withprop;
	}
	public void setWithprop(String withprop) {
		this.withprop = withprop;
	}
	public String getWithoprop() {
		return withoprop;
	}
	public void setWithoprop(String withoprop) {
		this.withoprop = withoprop;
	}
	public String getPpind() {
		return ppind;
	}
	public void setPpind(String ppind) {
		this.ppind = ppind;
	}
	public String getDthpercn() {
		return dthpercn;
	}
	public void setDthpercn(String dthpercn) {
		this.dthpercn = dthpercn;
	}
	public String getDthperco() {
		return dthperco;
	}
	public void setDthperco(String dthperco) {
		this.dthperco = dthperco;
	}
	public String getGuarperd() {
		return guarperd;
	}
	public void setGuarperd(String guarperd) {
		this.guarperd = guarperd;
	}
	public String getNomlife() {
		return nomlife;
	}
	public void setNomlife(String nomlife) {
		this.nomlife = nomlife;
	}
	public String getFreqann() {
		return freqann;
	}
	public void setFreqann(String freqann) {
		this.freqann = freqann;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S5220 []";
	}
	
}
