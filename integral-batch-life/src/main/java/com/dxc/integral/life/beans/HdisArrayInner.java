package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class HdisArrayInner {
	private List<String> hdisLife = new ArrayList<>();
	private List<String> hdisCoverage = new ArrayList<>();
	private List<String> hdisRider = new ArrayList<>();
	private List<String> hdisJlife = new ArrayList<>();
	private List<Integer> hdisPlnsfx = new ArrayList<>();
	private List<String> zdivopt = new ArrayList<>();
	private List<String> zcshdivmth = new ArrayList<>();
	private List<Integer> nextCapDate = new ArrayList<>();
	private List<BigDecimal> dvdTot = new ArrayList<>();
	private List<BigDecimal> dvdShare = new ArrayList<>();

	public List<String> getHdisLife() {
		return hdisLife;
	}

	public void setHdisLife(List<String> hdisLife) {
		this.hdisLife = hdisLife;
	}

	public List<String> getHdisCoverage() {
		return hdisCoverage;
	}

	public void setHdisCoverage(List<String> hdisCoverage) {
		this.hdisCoverage = hdisCoverage;
	}

	public List<String> getHdisRider() {
		return hdisRider;
	}

	public void setHdisRider(List<String> hdisRider) {
		this.hdisRider = hdisRider;
	}

	public List<String> getHdisJlife() {
		return hdisJlife;
	}

	public void setHdisJlife(List<String> hdisJlife) {
		this.hdisJlife = hdisJlife;
	}

	public List<Integer> getHdisPlnsfx() {
		return hdisPlnsfx;
	}

	public void setHdisPlnsfx(List<Integer> hdisPlnsfx) {
		this.hdisPlnsfx = hdisPlnsfx;
	}

	public List<String> getZdivopt() {
		return zdivopt;
	}

	public void setZdivopt(List<String> zdivopt) {
		this.zdivopt = zdivopt;
	}

	public List<String> getZcshdivmth() {
		return zcshdivmth;
	}

	public void setZcshdivmth(List<String> zcshdivmth) {
		this.zcshdivmth = zcshdivmth;
	}

	public List<Integer> getNextCapDate() {
		return nextCapDate;
	}

	public void setNextCapDate(List<Integer> nextCapDate) {
		this.nextCapDate = nextCapDate;
	}

	public List<BigDecimal> getDvdTot() {
		return dvdTot;
	}

	public void setDvdTot(List<BigDecimal> dvdTot) {
		this.dvdTot = dvdTot;
	}

	public List<BigDecimal> getDvdShare() {
		return dvdShare;
	}

	public void setDvdShare(List<BigDecimal> dvdShare) {
		this.dvdShare = dvdShare;
	}
}
