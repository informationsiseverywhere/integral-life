package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sr2d2 implements Serializable  {
	private static final long serialVersionUID = 1L;
	private String actionKey;

	private String cltpcode;
	
	private String activeField;
	
	private Sr2d2screensfl sr2d2screensfl;
	
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}
	public String getCltpcode() {
		return cltpcode;
	}
	public void setCltpcode(String cltpcode) {
		this.cltpcode = cltpcode;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	public Sr2d2screensfl getSr2d2screensfl() {
		return sr2d2screensfl;
	}
	public void setSr2d2screensfl(Sr2d2screensfl sr2d2screensfl) {
		this.sr2d2screensfl = sr2d2screensfl;
	}
	
	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Sr2d2screensfl implements Serializable {
		private static final long serialVersionUID = 1L;
		private List<KeyValueItem> actions;
		
		public List<KeyValueItem> getAttributeList() {
			return actions;
		}
		public void setAttributeList(List<KeyValueItem> actions) {
			this.actions = actions;
		}
	
	}

	@Override
	public String toString() {
		return "Sr2d2 []";
	}
	
}
