package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.dxc.integral.life.beans.L2newintcpReaderDTO;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.life.beans.L2newintcpProcessorDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T6633;

/**
 * Processor Utility for L2NEWINTCP batch.
 * 
 * @author dpuhawan
 *
 */
public interface L2newintcpProcessorUtil {
	
	public L2newintcpProcessorDTO process(L2newintcpReaderDTO readerDTO, Integer tranDate, List<T6633> T6633List, List<T5645> t5645IOList, Batcpf batcpf) throws IOException, ParseException;
	
	public L2newintcpReaderDTO fillDTOInfo(L2newintcpProcessorDTO processorDTO, L2newintcpReaderDTO readerDTO, Batcpf batcpf, Integer tranDate);

}
