package com.dxc.integral.life.general.listeners;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.L2OverdueControlTotalDTO;

/**
 * Listener for L2polrnwlBillingStepListener batch step.
 * 
 * @author wli31
 *
 */
public class L2OverdueStepListener extends IntegralStepListener {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2OverdueStepListener.class);

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private L2OverdueControlTotalDTO l2OverdueControlTotalDTO;

	@Value("#{jobParameters['userName']}")
	private String userName;
	
	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Value("#{jobParameters['chdrcoy']}")
	private String company;
	
	@Value("#{jobParameters['trandate']}")
	private String effDate;
	
	@Value("#{jobParameters['batchName']}")
	private String batchName;
	
	@Value("#{jobParameters['transactionDate']}") 
	private String transactionDate;
	
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private ElogpfDAO elogpfDAO;

	private static String programName = "B5355";

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("L2OverdueStepListener::afterStep::Entry");
		List<Throwable> exceptionList=null;
		exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.info("Control total update for L2OverdueStepListener batch starts");
			Sbcontotpf sbcontotpf = new Sbcontotpf();
			sbcontotpf.setStepname(stepExecution.getStepName());
			sbcontotpf.setUsrprf(userName);
			sbcontotpf.setBranch(branch);
			sbcontotpf.setCompany(company);
			sbcontotpf.setEffdate(Integer.parseInt(effDate));
			sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
			sbcontotpf.setJobnm(batchName);
			sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
			sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
			sbcontotpf.setContot2(l2OverdueControlTotalDTO.getControlTotal02());
			sbcontotpf.setContot3(l2OverdueControlTotalDTO.getControlTotal03());
			sbcontotpf.setContot4(l2OverdueControlTotalDTO.getControlTotal04());
			sbcontotpf.setContot5(l2OverdueControlTotalDTO.getControlTotal05());
			sbcontotpf.setContot6(l2OverdueControlTotalDTO.getControlTotal06());
			sbcontotpf.setContot7(l2OverdueControlTotalDTO.getControlTotal07());
			sbcontotpf.setContot8(l2OverdueControlTotalDTO.getControlTotal08());
			sbcontotpf.setContot9(l2OverdueControlTotalDTO.getControlTotal09());
			sbcontotpf.setContot10(l2OverdueControlTotalDTO.getControlTotal10());
			sbcontotpf.setContot11(l2OverdueControlTotalDTO.getControlTotal11());
			sbcontotpf.setContot12(l2OverdueControlTotalDTO.getControlTotal12());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);
			LOGGER.info("Control total update for L2OverdueStepListener batch ends");
			LOGGER.info("L2OverdueStepListener batch step completed.");
		} else {
			LOGGER.error("Some exception occurred,logging it in Elogpf.{}", 
					exceptionList.get(0).getStackTrace().toString());//IJTI-1498
			if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (exceptionList.get(0).getClass() != null
					&& exceptionList.get(0).getClass().toString().contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", programName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", programName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("L2OverdueStepListener::afterStep::Exit");
		return stepExecution.getExitStatus();
	}
	@Override
	public void beforeStep(StepExecution stepExecution) {
		
		LOGGER.debug("L2OverdueStepListener::beforeStep::Entry");
		super.beforeStep(stepExecution);
		int overdueDate = readMaxLeadDays();
		stepExecution.getExecutionContext().putInt("overdueDate", overdueDate);
		LOGGER.debug("L2OverdueStepListener::beforeStep::Exit");
	}

	private int readMaxLeadDays() {
		return datcon2.plusDays(Integer.parseInt(effDate), 1);
		
	}
}
