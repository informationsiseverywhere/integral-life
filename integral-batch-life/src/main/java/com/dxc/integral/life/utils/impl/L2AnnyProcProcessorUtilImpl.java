package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.L2AnnyProcProcessorDTO;
import com.dxc.integral.life.beans.LifsttrDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.general.processors.BaseProcessor;
import com.dxc.integral.life.smarttable.pojo.T5519;
import com.dxc.integral.life.smarttable.pojo.T5540;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6658;
import com.dxc.integral.life.utils.L2AnnyProcProcessorUtil;
import com.dxc.integral.life.utils.Lifsttr;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
@Service("l2AnnyProcProcessorUtil")
@Lazy
public class L2AnnyProcProcessorUtilImpl extends BaseProcessor implements L2AnnyProcProcessorUtil {
	@Autowired
	private ItempfService itemService;
	
	@Autowired
	private CovrpfDAO covrpfDAO;
	
	@Autowired
	private Datcon2 datCon2;
	
	@Autowired
	private Lifsttr lifSttr;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AnnyProcProcessorUtilImpl.class);
	
	@Override
	public L2AnnyProcProcessorDTO loadTableDetails(L2AnnyProcProcessorDTO processorDTO) throws JsonParseException, JsonMappingException, IOException {
		T5679 t5679IO = itemService.readSmartTableByTrim(Companies.LIFE, "T5679", processorDTO.getBatchtrancde(),T5679.class);
		if (t5679IO == null) {
			throw new ItemNotfoundException("BR619: Item not found in Table T5679: "+processorDTO.getBatchtrancde() );
		}
		processorDTO.setT5679(t5679IO);
		return processorDTO;
	 }
	  public void checkT5687(L2AnnyProcProcessorDTO processorDTO) {
		  T5687 t5687IO = itemService.readSmartTableByTrim(Companies.LIFE, "T5687", processorDTO.getCrtable(),processorDTO.getEffDate(),T5687.class);
		  if (t5687IO == null) {
				throw new ItemNotfoundException("BR619: Item not found in Table t5687IO: "+processorDTO.getCrtable() );
		  }
		  processorDTO.setT5687(t5687IO);
		  if("".equals(t5687IO.getAnniversaryMethod())){
			  processorDTO.setUpdateCovr(true);
		  }
	  }
	  
	  public void checkT6658(L2AnnyProcProcessorDTO processorDTO){
		  T6658 t6658IO = itemService.readSmartTableByTrim(Companies.LIFE, "T6658", processorDTO.getT5687().getAnniversaryMethod(),processorDTO.getEffDate(),T6658.class);
		  if (t6658IO == null) {
				throw new ItemNotfoundException("BR619: Item not found in Table T6658: "+processorDTO.getT5687().getAnniversaryMethod() );
		  }
		  processorDTO.setT6658(t6658IO);
		  if("".equals(t6658IO.getSubprog())){
			  processorDTO.setUpdateCovr(true);
		  }
	  }
	  public void checkT5540(L2AnnyProcProcessorDTO processorDTO){
		  T5540 t5540IO = itemService.readSmartTableByTrim(Companies.LIFE, "T5540", processorDTO.getCrtable(),99999999,T5540.class);
		  if (t5540IO == null) {
			  processorDTO.setT5540Found(true);
			  return;
		  }
		  processorDTO.setT5540(t5540IO);
	 }
	  public void checkT5519(L2AnnyProcProcessorDTO processorDTO) {
		  
		  T5519 t5519IO = itemService.readSmartTableByTrim(Companies.LIFE, "T5519", processorDTO.getT5540().getIuDiscBasis(),processorDTO.getEffDate(),T5519.class);
		  if (t5519IO == null) {
			  throw new ItemNotfoundException("BR619: Item not found in Table T5519: "+processorDTO.getT5540().getIuDiscBasis() );
		  }
		  processorDTO.setT5519(t5519IO);
	  }
	@Override
	public L2AnnyProcProcessorDTO validComponent(L2AnnyProcProcessorDTO processorDTO) {
		processorDTO.setValidComp("N");
		if (!processorDTO.getRider().equals("00") && !processorDTO.getRider().equals(" ")) {
			validateRider(processorDTO);
		}
		for (int i=0; i<12 || !(processorDTO.getValidComp().equals("Y"));i++){
			if (processorDTO.getT5679().getCovRiskStats().get(i).equals(processorDTO.getStatcode())){
				processorDTO.setValidComp("Y");
				break;
			}
		}
		
		//if Not valid component:
		if(!processorDTO.getValidComp().equals("Y")) {
			processorDTO.getL2AnnyProcControlTotalDTO().setControlTotal02(processorDTO.getL2AnnyProcControlTotalDTO().getControlTotal02().add(BigDecimal.ONE));	
		}
		checkT5687(processorDTO);
		if (processorDTO.isUpdateCovr()) {
			callSoftlock(processorDTO);
			return processorDTO;
		}
		checkT6658(processorDTO);
		if (processorDTO.isUpdateCovr()) {
			callSoftlock(processorDTO);
			return processorDTO;
		}
		checkT5540(processorDTO);
		if (!processorDTO.isT5540Found()) {
			callSoftlock(processorDTO);
		}
		checkT5519(processorDTO);
		return processorDTO;
	}
	public void callSoftlock(L2AnnyProcProcessorDTO processorDTO){
		boolean softLock = softlock(processorDTO.getChdrnum(),"B675",processorDTO.getBatchName());
		if (!softLock) {
			LOGGER.info("Contract is already locked, chdrnum: {}", processorDTO.getChdrnum());//IJTI-1498
		}
		processorDTO.getL2AnnyProcControlTotalDTO().setControlTotal04(processorDTO.getL2AnnyProcControlTotalDTO().getControlTotal04().add(BigDecimal.ONE));	
	}	
	public void validateRider(L2AnnyProcProcessorDTO processorDTO) {
		for (int i=0; i<12 || !(processorDTO.getValidComp().equals("Y"));i++){
			if (processorDTO.getT5679().getRidRiskStats().get(i).equals(processorDTO.getStatcode())){
				processorDTO.setValidComp("Y");
				break;
			}
		}
	}
	
	@Override
	public void fatalError(String status) {
		throw new StopRunException(status);
	}
	
	
	public L2AnnyProcProcessorDTO updateCovrpf(L2AnnyProcProcessorDTO processorDTO) {
		int freqFactor=1;
		int datconResult=0;
		datconResult=datCon2.plusYears(processorDTO.getCovrpf().getCbanpr(), freqFactor);
		processorDTO.getCovrpf().setCbanpr(datconResult);
		processorDTO.getCovrpf().setUsrprf(processorDTO.getUserName());
		processorDTO.getCovrpf().setJobnm(processorDTO.getBatchName().toUpperCase());
		
		
		//release softlock		
		releaseSoftlock(processorDTO.getChdrcoy(),processorDTO.getChdrnum(),"B673",processorDTO.getUserName(),processorDTO.getBatchName(),processorDTO.getBatchtrancde());
		return processorDTO;
	}
	@Override
	public L2AnnyProcProcessorDTO processUpdate(L2AnnyProcProcessorDTO processorDTO) {
		Covrpf covrpf = new Covrpf();
		covrpf.setChdrcoy(processorDTO.getChdrcoy());
		covrpf.setChdrnum(processorDTO.getChdrnum());
		covrpf.setLife(processorDTO.getLife());
		covrpf.setCoverage(processorDTO.getCoverage());
		covrpf.setPlnsfx(processorDTO.getPlnsfx());
		covrpf.setRider(processorDTO.getRider());
		covrpf = covrpfDAO.readCovrenqData(covrpf);
		if(covrpf == null){
			fatalError("No Covrpf Records");
		}
		if (processorDTO.isUpdateCovr()) {
			processorDTO.setCovrpf(covrpf);
			return updateCovrpf(processorDTO);
		}
		return processorDTO;
	}
	public void statistics(L2AnnyProcProcessorDTO l2processorDTO){
		LifsttrDTO lifsttrrec = new LifsttrDTO();
		lifsttrrec.setBatccoy(Companies.LIFE);
		lifsttrrec.setBatcbrn(l2processorDTO.getBatchbranch());
		lifsttrrec.setBatcacty(l2processorDTO.getActYear());
		lifsttrrec.setBatcactmn(l2processorDTO.getActMonth());
		lifsttrrec.setBatctrcde(l2processorDTO.getBatchtrancde());
	    lifsttrrec.setBatcbatch(l2processorDTO.getBatch());
		lifsttrrec.setChdrcoy(l2processorDTO.getChdrcoy());
		lifsttrrec.setChdrnum(l2processorDTO.getChdrnum());
		lifsttrrec.setTranno(l2processorDTO.getTranno());
		lifsttrrec.setTrannor(99999);
		lifsttrrec.setAgntnum("");
		lifsttrrec.setOldAgntnum("");
		try {
			lifSttr.processLifsttr(lifsttrrec);
		} catch (IOException e) {
			throw new SubroutineIOException(" IOException in lifsttr: " + e.getMessage());
		}
	}
		
}
