package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.L2OverdueProcessorDTO;

public interface L2OverdueItemProcessorUtil {
	L2OverdueProcessorDTO loadSmartTables(L2OverdueProcessorDTO processorDTO) throws IOException;
	L2OverdueProcessorDTO validateContract(L2OverdueProcessorDTO processorDTO)throws ParseException;
	L2OverdueProcessorDTO processOverdueContract(L2OverdueProcessorDTO processorDTO)throws Exception;
	void statistics(L2OverdueProcessorDTO processorDTO);
}
