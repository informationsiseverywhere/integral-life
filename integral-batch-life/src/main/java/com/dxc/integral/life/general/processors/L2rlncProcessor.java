package com.dxc.integral.life.general.processors;

import java.sql.Timestamp;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.life.beans.L2lincReaderDTO;
import com.dxc.integral.life.dao.LincpfDAO;
import com.dxc.integral.life.dao.model.Lincpf;
import com.dxc.integral.life.smarttable.pojo.Tjl67;
import com.dxc.integral.fsu.dao.ZbsdpfDAO;
import com.dxc.integral.fsu.dao.model.Zbsdpf;

public class L2rlncProcessor implements ItemProcessor<L2lincReaderDTO , L2lincReaderDTO>{
	
	@Autowired
	private ZbsdpfDAO zbsdpfDAO;
	private Zbsdpf zbsdpfItem;
	
	private String jobname;
	
	@Value("#{stepExecution}")
	private StepExecution stepExecution; 
	
	@Autowired
	private LincpfDAO lincpfDAO;
	private Lincpf lincpfItem;
	
	@Autowired
	private UsrdpfDAO usrdpfDAO;
	
	@Autowired
	private ItempfService itempfService;
	
	public L2rlncProcessor() {
		//Added for removing Sonar Qube Errors
	};
	
	@Override
	public synchronized L2lincReaderDTO process(L2lincReaderDTO readerDTO) throws Exception {
		
		JobExecution jobExecution = stepExecution.getJobExecution();
		jobname = jobExecution.getJobInstance().getJobName().toUpperCase();
		String usrdpfItem = usrdpfDAO.getUserId(stepExecution.getExecutionContext().getString("username").trim());
		zbsdpfItem = zbsdpfDAO.readRecordZbsdpf(Companies.LIFE, usrdpfItem.trim());
		lincpfItem = lincpfDAO.readRecordOfLincpf(readerDTO.getZhospopt().trim());
		
		String tjl67 = "TJL67";
		TableItem<Tjl67> tjl67Object = itempfService.readSmartTableByTableNameAndItem(Companies.LIFE,
				tjl67, readerDTO.getZreclass().trim(), CommonConstants.IT_ITEMPFX, Tjl67.class);
		
		if(tjl67Object != null) {
			for(int i = 0; i < tjl67Object.getItemDetail().getResultcds().size(); i++) {
				if(readerDTO.getZrsltcde().equals(tjl67Object.getItemDetail().getResultcds().get(i))) {
					readerDTO.setRecveror(tjl67Object.getItemDetail().getRcverors().get(i));
					break;
				}
				if(readerDTO.getZrsltcde().isEmpty()) {
					readerDTO.setRecveror("");
				}
			}
		}
		else {
			readerDTO.setRecveror("");
		}
		
		readerDTO.setRecvdate(zbsdpfItem.getBusdate());
		readerDTO.setChdrnum(lincpfItem.getChdrnum());
		readerDTO.setUsrprf(stepExecution.getExecutionContext().getString("username").trim().toUpperCase());
		readerDTO.setJobnm(jobname);
		readerDTO.setDatime(new Timestamp(System.currentTimeMillis()));
		return readerDTO;
		
	}
}
