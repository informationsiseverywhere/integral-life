package com.dxc.integral.life.beans.in;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S2465 implements Serializable {
	private static final long serialVersionUID = 1L;
	private String actionKey;
	private String servbrh;
	private String clntnum;
	private String startDateDisp;
	private String lsurname;
	private String lgivname;
	private String salutl;
	private String cltsex;
	private String marryd;
	private String cltaddr01;
	private String cltaddr02;
	private String cltaddr03;
	private String cltaddr04;
	private String cltaddr05;
	private String cltpcode;
	private String ctrycode;
	private String addrtype;
	private String cltphone01;
	private String cltphone02;
	private String nmfmt;
	private String natlty;
	private String secuityno;
	private String idtype;
	private String zprovince;
	private String zseqno;
	private String occpcode;
	private String language;
	private String statcode;
	private String docno;
	private String cltdodxDisp;
	private String soe;
	private String cltdobxDisp;
	private String birthp;
	private String vip;
	private String mailing;
	private String dirmail;
	private String ukPensionInd;
	private String racrind;
	private String brupind;
	private String rextrfld;
	private String clprfind;
	private String zdoctind;
	private String taxflag;
	private String cltstat;
	private String rmblphone;
	private String rinternet;
	private String rinternet2;
	private String clnthistInd;
	private String sib001Flag;
	private String amlstatus;
	private String zkanasurname;
	private String zkanagivname;
	private String zkanacltaddr01;
	private String zkanacltaddr02;	
	private String zkanacltaddr03;
	private String zkanacltaddr04;
	private String zkanacltaddr05;
	private String excep;
	private String polownind;
	private String addrinda;
	private String optind;
	private String abusnumFlag;
	private String abusnum;
	private String branchid;
	private String cltphoneidd;
	private String cltphone02idd;
	private String cltphone01idd;
	private String birthcountry;
	private String cntryState;
	private String fatcaInd;
	private String zdlind;
	private String faxno;
	private String prefconmtd;
	private String dirmktmtd;
	private String zstates;
	private String ztown;
	private String fundadminflag;
	private String idExpireDateDisp;
	private String isPermanentID;
	private String company;
	private String lsmcInd;
	private String clnduplInd;
	private String alteradd;
	private String crsInd;
	private String activeField;
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}

	public String getServbrh() {
		return servbrh;
	}
	public void setServbrh(String servbrh) {
		this.servbrh = servbrh;
	}
	public String getStartDateDisp() {
		return startDateDisp;
	}
	public void setStartDateDisp(String startDateDisp) {
		this.startDateDisp = startDateDisp;
	}
	public String getLsurname() {
		return lsurname;
	}
	public void setLsurname(String lsurname) {
		this.lsurname = lsurname;
	}
	public String getLgivname() {
		return lgivname;
	}
	public void setLgivname(String lgivname) {
		this.lgivname = lgivname;
	}
	public String getSalutl() {
		return salutl;
	}
	public void setSalutl(String salutl) {
		this.salutl = salutl;
	}
	public String getCltsex() {
		return cltsex;
	}
	public void setCltsex(String cltsex) {
		this.cltsex = cltsex;
	}
	public String getMarryd() {
		return marryd;
	}
	public void setMarryd(String marryd) {
		this.marryd = marryd;
	}
	public String getCltaddr01() {
		return cltaddr01;
	}
	public void setCltaddr01(String cltaddr01) {
		this.cltaddr01 = cltaddr01;
	}
	public String getCltaddr02() {
		return cltaddr02;
	}
	public void setCltaddr02(String cltaddr02) {
		this.cltaddr02 = cltaddr02;
	}
	public String getCltaddr03() {
		return cltaddr03;
	}
	public void setCltaddr03(String cltaddr03) {
		this.cltaddr03 = cltaddr03;
	}
	public String getCltaddr04() {
		return cltaddr04;
	}
	public void setCltaddr04(String cltaddr04) {
		this.cltaddr04 = cltaddr04;
	}
	public String getCltaddr05() {
		return cltaddr05;
	}
	public void setCltaddr05(String cltaddr05) {
		this.cltaddr05 = cltaddr05;
	}
	public String getCltpcode() {
		return cltpcode;
	}
	public void setCltpcode(String cltpcode) {
		this.cltpcode = cltpcode;
	}
	public String getCtrycode() {
		return ctrycode;
	}
	public void setCtrycode(String ctrycode) {
		this.ctrycode = ctrycode;
	}
	public String getAddrtype() {
		return addrtype;
	}
	public void setAddrtype(String addrtype) {
		this.addrtype = addrtype;
	}
	public String getCltphone01() {
		return cltphone01;
	}
	public void setCltphone01(String cltphone01) {
		this.cltphone01 = cltphone01;
	}
	public String getCltphone02() {
		return cltphone02;
	}
	public void setCltphone02(String cltphone02) {
		this.cltphone02 = cltphone02;
	}
	public String getNmfmt() {
		return nmfmt;
	}
	public void setNmfmt(String nmfmt) {
		this.nmfmt = nmfmt;
	}
	public String getNatlty() {
		return natlty;
	}
	public void setNatlty(String natlty) {
		this.natlty = natlty;
	}
	public String getSecuityno() {
		return secuityno;
	}
	public void setSecuityno(String secuityno) {
		this.secuityno = secuityno;
	}
	public String getIdtype() {
		return idtype;
	}
	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}
	public String getZprovince() {
		return zprovince;
	}
	public void setZprovince(String zprovince) {
		this.zprovince = zprovince;
	}
	public String getZseqno() {
		return zseqno;
	}
	public void setZseqno(String zseqno) {
		this.zseqno = zseqno;
	}
	public String getOccpcode() {
		return occpcode;
	}
	public void setOccpcode(String occpcode) {
		this.occpcode = occpcode;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getDocno() {
		return docno;
	}
	public void setDocno(String docno) {
		this.docno = docno;
	}
	public String getCltdodxDisp() {
		return cltdodxDisp;
	}
	public void setCltdodxDisp(String cltdodxDisp) {
		this.cltdodxDisp = cltdodxDisp;
	}
	public String getSoe() {
		return soe;
	}
	public void setSoe(String soe) {
		this.soe = soe;
	}
	public String getCltdobxDisp() {
		return cltdobxDisp;
	}
	public void setCltdobxDisp(String cltdobxDisp) {
		this.cltdobxDisp = cltdobxDisp;
	}
	public String getBirthp() {
		return birthp;
	}
	public void setBirthp(String birthp) {
		this.birthp = birthp;
	}
	public String getVip() {
		return vip;
	}
	public void setVip(String vip) {
		this.vip = vip;
	}
	public String getMailing() {
		return mailing;
	}
	public void setMailing(String mailing) {
		this.mailing = mailing;
	}
	public String getDirmail() {
		return dirmail;
	}
	public void setDirmail(String dirmail) {
		this.dirmail = dirmail;
	}
	public String getUkPensionInd() {
		return ukPensionInd;
	}
	public void setUkPensionInd(String ukPensionInd) {
		this.ukPensionInd = ukPensionInd;
	}
	public String getRacrind() {
		return racrind;
	}
	public void setRacrind(String racrind) {
		this.racrind = racrind;
	}
	public String getBrupind() {
		return brupind;
	}
	public void setBrupind(String brupind) {
		this.brupind = brupind;
	}
	public String getRextrfld() {
		return rextrfld;
	}
	public void setRextrfld(String rextrfld) {
		this.rextrfld = rextrfld;
	}
	public String getClprfind() {
		return clprfind;
	}
	public void setClprfind(String clprfind) {
		this.clprfind = clprfind;
	}
	public String getZdoctind() {
		return zdoctind;
	}
	public void setZdoctind(String zdoctind) {
		this.zdoctind = zdoctind;
	}
	public String getTaxflag() {
		return taxflag;
	}
	public void setTaxflag(String taxflag) {
		this.taxflag = taxflag;
	}
	public String getCltstat() {
		return cltstat;
	}
	public void setCltstat(String cltstat) {
		this.cltstat = cltstat;
	}
	public String getRmblphone() {
		return rmblphone;
	}
	public void setRmblphone(String rmblphone) {
		this.rmblphone = rmblphone;
	}
	public String getRinternet() {
		return rinternet;
	}
	public void setRinternet(String rinternet) {
		this.rinternet = rinternet;
	}
	public String getRinternet2() {
		return rinternet2;
	}
	public void setRinternet2(String rinternet2) {
		this.rinternet2 = rinternet2;
	}
	public String getClnthistInd() {
		return clnthistInd;
	}
	public void setClnthistInd(String clnthistInd) {
		this.clnthistInd = clnthistInd;
	}
	public String getSib001Flag() {
		return sib001Flag;
	}
	public void setSib001Flag(String sib001Flag) {
		this.sib001Flag = sib001Flag;
	}
	public String getAmlstatus() {
		return amlstatus;
	}
	public void setAmlstatus(String amlstatus) {
		this.amlstatus = amlstatus;
	}
	public String getZkanasurname() {
		return zkanasurname;
	}
	public void setZkanasurname(String zkanasurname) {
		this.zkanasurname = zkanasurname;
	}
	public String getZkanagivname() {
		return zkanagivname;
	}
	public void setZkanagivname(String zkanagivname) {
		this.zkanagivname = zkanagivname;
	}
	public String getZkanacltaddr01() {
		return zkanacltaddr01;
	}
	public void setZkanacltaddr01(String zkanacltaddr01) {
		this.zkanacltaddr01 = zkanacltaddr01;
	}
	public String getZkanacltaddr02() {
		return zkanacltaddr02;
	}
	public void setZkanacltaddr02(String zkanacltaddr02) {
		this.zkanacltaddr02 = zkanacltaddr02;
	}
	public String getZkanacltaddr03() {
		return zkanacltaddr03;
	}
	public void setZkanacltaddr03(String zkanacltaddr03) {
		this.zkanacltaddr03 = zkanacltaddr03;
	}
	public String getZkanacltaddr04() {
		return zkanacltaddr04;
	}
	public void setZkanacltaddr04(String zkanacltaddr04) {
		this.zkanacltaddr04 = zkanacltaddr04;
	}
	public String getZkanacltaddr05() {
		return zkanacltaddr05;
	}
	public void setZkanacltaddr05(String zkanacltaddr05) {
		this.zkanacltaddr05 = zkanacltaddr05;
	}
	public String getExcep() {
		return excep;
	}
	public void setExcep(String excep) {
		this.excep = excep;
	}
	public String getPolownind() {
		return polownind;
	}
	public void setPolownind(String polownind) {
		this.polownind = polownind;
	}
	public String getAddrinda() {
		return addrinda;
	}
	public void setAddrinda(String addrinda) {
		this.addrinda = addrinda;
	}
	public String getOptind() {
		return optind;
	}
	public void setOptind(String optind) {
		this.optind = optind;
	}
	public String getAbusnumFlag() {
		return abusnumFlag;
	}
	public void setAbusnumFlag(String abusnumFlag) {
		this.abusnumFlag = abusnumFlag;
	}
	public String getAbusnum() {
		return abusnum;
	}
	public void setAbusnum(String abusnum) {
		this.abusnum = abusnum;
	}
	public String getBranchid() {
		return branchid;
	}
	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
	public String getCltphoneidd() {
		return cltphoneidd;
	}
	public void setCltphoneidd(String cltphoneidd) {
		this.cltphoneidd = cltphoneidd;
	}
	public String getCltphone02idd() {
		return cltphone02idd;
	}
	public void setCltphone02idd(String cltphone02idd) {
		this.cltphone02idd = cltphone02idd;
	}
	public String getCltphone01idd() {
		return cltphone01idd;
	}
	public void setCltphone01idd(String cltphone01idd) {
		this.cltphone01idd = cltphone01idd;
	}
	public String getBirthcountry() {
		return birthcountry;
	}
	public void setBirthcountry(String birthcountry) {
		this.birthcountry = birthcountry;
	}
	public String getCntryState() {
		return cntryState;
	}
	public void setCntryState(String cntryState) {
		this.cntryState = cntryState;
	}
	public String getFatcaInd() {
		return fatcaInd;
	}
	public void setFatcaInd(String fatcaInd) {
		this.fatcaInd = fatcaInd;
	}
	public String getZdlind() {
		return zdlind;
	}
	public void setZdlind(String zdlind) {
		this.zdlind = zdlind;
	}
	public String getFaxno() {
		return faxno;
	}
	public void setFaxno(String faxno) {
		this.faxno = faxno;
	}
	public String getPrefconmtd() {
		return prefconmtd;
	}
	public void setPrefconmtd(String prefconmtd) {
		this.prefconmtd = prefconmtd;
	}
	public String getDirmktmtd() {
		return dirmktmtd;
	}
	public void setDirmktmtd(String dirmktmtd) {
		this.dirmktmtd = dirmktmtd;
	}
	public String getZstates() {
		return zstates;
	}
	public void setZstates(String zstates) {
		this.zstates = zstates;
	}
	public String getZtown() {
		return ztown;
	}
	public void setZtown(String ztown) {
		this.ztown = ztown;
	}
	public String getFundadminflag() {
		return fundadminflag;
	}
	public void setFundadminflag(String fundadminflag) {
		this.fundadminflag = fundadminflag;
	}
	public String getIdExpireDateDisp() {
		return idExpireDateDisp;
	}
	public void setIdExpireDateDisp(String idExpireDateDisp) {
		this.idExpireDateDisp = idExpireDateDisp;
	}
	public String getIsPermanentID() {
		return isPermanentID;
	}
	public void setIsPermanentID(String isPermanentID) {
		this.isPermanentID = isPermanentID;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getLsmcInd() {
		return lsmcInd;
	}
	public void setLsmcInd(String lsmcInd) {
		this.lsmcInd = lsmcInd;
	}
	public String getClnduplInd() {
		return clnduplInd;
	}
	public void setClnduplInd(String clnduplInd) {
		this.clnduplInd = clnduplInd;
	}
	public String getAlteradd() {
		return alteradd;
	}
	public void setAlteradd(String alteradd) {
		this.alteradd = alteradd;
	}
	public String getCrsInd() {
		return crsInd;
	}
	public void setCrsInd(String crsInd) {
		this.crsInd = crsInd;
	}
	public String getActiveField() {
		return activeField;
	}
	public void setActiveField(String activeField) {
		this.activeField = activeField;
	}
	@Override
	public String toString() {
		return "S2465 []";
	}
	
}
