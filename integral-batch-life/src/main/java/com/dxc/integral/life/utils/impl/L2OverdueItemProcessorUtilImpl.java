package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.CrtundwrtDTO;
import com.dxc.integral.life.beans.L2OverdueProcessorDTO;
import com.dxc.integral.life.beans.LifsttrDTO;
import com.dxc.integral.life.beans.NfloanDTO;
import com.dxc.integral.life.beans.NlgcalcDTO;
import com.dxc.integral.life.beans.OverbillDTO;
import com.dxc.integral.life.beans.OvrdueDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.HpadpfDAO;
import com.dxc.integral.life.dao.HpuapfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.LinspfDAO;
import com.dxc.integral.life.dao.NlgtpfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hpadpf;
import com.dxc.integral.life.dao.model.Hpuapf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Nlgtpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.GOTOInterface;
import com.dxc.integral.life.exceptions.ItemParseException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T5399;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6597;
import com.dxc.integral.life.smarttable.pojo.T6647;
import com.dxc.integral.life.smarttable.pojo.T6654;
import com.dxc.integral.life.smarttable.pojo.Th584;
import com.dxc.integral.life.smarttable.pojo.Tr5b2;
import com.dxc.integral.life.utils.Calcfee;
import com.dxc.integral.life.utils.Crtloan;
import com.dxc.integral.life.utils.Crtundwrt;
import com.dxc.integral.life.utils.L2OverdueItemProcessorUtil;
import com.dxc.integral.life.utils.Lifsttr;
import com.dxc.integral.life.utils.Nfloan;
import com.dxc.integral.life.utils.Nftrad;
import com.dxc.integral.life.utils.Nlgcalc;
import com.dxc.integral.life.utils.Overbill;
/**
 * l2Overdue processor utility implementation.
 * 
 * @author wli31
 *
 */
@Service("l2OverdueItemProcessorUtil")
@Lazy
public class L2OverdueItemProcessorUtilImpl implements L2OverdueItemProcessorUtil{
	private static final Logger LOGGER = LoggerFactory.getLogger(L2OverdueItemProcessorUtilImpl.class);
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Calcfee calcfee;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private NlgtpfDAO nlgtpfDAO;
	@Autowired
	private Crtloan crtloan;
	@Autowired
	public Sftlock sftlock;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Nfloan nfloan;	
	@Autowired
	private HpadpfDAO hpadpfDAO;
	@Autowired
	private LinspfDAO linspfDAO;
	@Autowired
	private Nlgcalc nlgcalc;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private HpuapfDAO hpuapfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private Crtundwrt crtundwrt;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Lifsttr lifsttr;
	@Autowired
	private Overbill overbill;
	@Autowired
	private Nftrad nftrad;
	
	public L2OverdueProcessorDTO loadSmartTables(L2OverdueProcessorDTO processorDTO) {
		
		T5679 t5679IO = itempfService.readSmartTableByTrim(Companies.LIFE, "T5679", processorDTO.getBatchTrcde(),T5679.class);
		if (t5679IO == null) {
			throw new ItemNotfoundException("B5355: Item not found in Table T5679: "+processorDTO.getBatchTrcde() );
		}
		processorDTO.setT5679IO(t5679IO);
		
		
		int t5697Ix = 0;
		List<TableItem<T6597>> t6597List = itempfService.getAllitemsFromTable( "IT", Companies.LIFE, "T6597", T6597.class);
		List<L2OverdueProcessorDTO.T6597Inner> t6597Recs = new ArrayList<>();
		if(t6597List!=null){
			for (TableItem<T6597> item: t6597List) {
				if(item.getItmfrm() <= 99999999){
					if (t5697Ix > 1000) {
						throw new StopRunException("B5355: too many T6597 records");
					}
					L2OverdueProcessorDTO.T6597Inner t6597Rec = new L2OverdueProcessorDTO.T6597Inner();
					t6597Rec.setT6597Method(item.getItemitem());
					t6597Rec.setT6597Itmfrm(item.getItmfrm());
					t6597Rec.setT6597Premsub(item.getItemDetail().getPremsubrs());
					t6597Rec.setT6597Cpstat(item.getItemDetail().getCpstats());
					t6597Rec.setT6597Crstat(item.getItemDetail().getCrstats());
					t6597Rec.setT6597Durmnth(item.getItemDetail().getDurmnths());
					t6597Recs.add(t6597Rec);
					t5697Ix++;
				}
			}
		}
		processorDTO.setT6597Inner(t6597Recs);

		return processorDTO;
	}
	public L2OverdueProcessorDTO processOverdueContract(L2OverdueProcessorDTO processorDTO) throws Exception {
		if("".equals(processorDTO.getT6654IO().getDoctids().get(processorDTO.getT6654SubrIx()))){
			if("A".equals(processorDTO.getWssaType())){
				processorDTO = skipLetter(processorDTO);
				return processorDTO;
			}else{
				return controlTotal(processorDTO);
			}
		}
		if (!"A".equals(processorDTO.getWssaType())) {
			processorDTO = writeChdrValidflag(processorDTO);
		}
		processorDTO.getChdrpf().setTranno(processorDTO.getChdrpf().getTranno() + 1);
		processorDTO = setupOverdueRec(processorDTO);
		//Add Overdue1 Subroutine
//		callProgram(wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()], ovrduerec.ovrdueRec);
//		if (isNE(ovrduerec.statuz, varcom.oK)) {
//			syserrrec.subrname.set(wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()]);
//			syserrrec.params.set(ovrduerec.ovrdueRec);
//			syserrrec.statuz.set(ovrduerec.statuz);
//			fatalError600();
//		}
		processorDTO = skipLetter(processorDTO);
		return processorDTO;
	}
	private L2OverdueProcessorDTO writeChdrValidflag(L2OverdueProcessorDTO processorDTO){
		Chdrpf chdrpfInvalid = new Chdrpf(processorDTO.getChdrpf());
		chdrpfInvalid.setValidflag("2");
		chdrpfInvalid.setCurrto(processorDTO.getPayrpf().getPtdate());
		chdrpfInvalid.setJobnm(processorDTO.getBatchName().toUpperCase());
		chdrpfInvalid.setUsrprf(processorDTO.getUserName());
		processorDTO.setChdrpfUpdate(chdrpfInvalid);
		return processorDTO;
	}
	private L2OverdueProcessorDTO writeChdrValidflag15300(L2OverdueProcessorDTO processorDTO){
		Chdrpf chdrpf = new Chdrpf(processorDTO.getChdrpf());
		chdrpf.setValidflag("1");
		chdrpf.setCurrto(99999999);
		chdrpf.setCurrfrom(processorDTO.getPtdate());
		chdrpf.setJobnm(processorDTO.getBatchName().toUpperCase());
		chdrpf.setUsrprf(processorDTO.getUserName());
		processorDTO.setChdrpfInsert(chdrpf);
		return processorDTO;
	}
	private L2OverdueProcessorDTO skipLetter(L2OverdueProcessorDTO processorDTO) throws Exception {
		if("A".equals(processorDTO.getWssaType())){
			processorDTO = processArrears(processorDTO);
		}
		if ("0".equals(processorDTO.getWssaType()) || "1".equals(processorDTO.getWssaType()) || "2".equals(processorDTO.getWssaType())) {
			processorDTO = processOverdue(processorDTO);
			processorDTO.getPayrpf().setValidflag("2");
			processorDTO.getPayrpf().setJobnm(processorDTO.getBatchName().toUpperCase());
			processorDTO.getPayrpf().setUsrprf(processorDTO.getUserName());
			processorDTO.setPayrUpdate(processorDTO.getPayrpf());
			
			Payrpf payrInsert = new Payrpf(processorDTO.getPayrpf());
			payrInsert.setValidflag("1");
			payrInsert.setTranno(processorDTO.getChdrpf().getTranno());
			payrInsert.setAplsupr("Y");
			payrInsert.setAplspfrom(processorDTO.getEffDate());
			payrInsert.setAplspto(processorDTO.getChdrpf().getAplspto());
			processorDTO.getPayrpf().setJobnm(processorDTO.getBatchName().toUpperCase());
			processorDTO.getPayrpf().setUsrprf(processorDTO.getUserName());
			processorDTO.setPayrInsert(payrInsert);
		}
		return processorDTO;
	}
	private L2OverdueProcessorDTO processOverdue(L2OverdueProcessorDTO processorDTO){
		processorDTO.setAplFlag(true);
		processorDTO.getChdrpf().setAplspfrom(processorDTO.getEffDate());
		int freqFactor = 0;
		int wsaaSub = processorDTO.getWsaaSub();
		for (wsaaSub = Integer.parseInt(processorDTO.getWssaType()) - 1; !(wsaaSub > 1
				|| processorDTO.getT6654IO().getDaexpys().get(wsaaSub + 1) instanceof Integer
				&& processorDTO.getT6654IO().getDaexpys().get(wsaaSub + 1) != 0); wsaaSub++)
		{
		}
		if(wsaaSub <= 1){
			if(processorDTO.getT6654IO().getNonForfietureDays() == 0){
				freqFactor = 10;
			}
			freqFactor = processorDTO.getT6654IO().getNonForfietureDays();
		}else{
			freqFactor = processorDTO.getT6654IO().getDaexpys().get(wsaaSub + 1);
		}
		processorDTO.setWsaaSub(wsaaSub);
		int aplsptp= datcon2.plusDays(processorDTO.getPtdate(), freqFactor);
		processorDTO.getChdrpf().setAplspto(aplsptp);
		processorDTO = writeChdrValidflag15300(processorDTO);
		processorDTO = writePtrn(processorDTO);
		if(processorDTO.getChdrpf() != null && processorDTO.getChdrpf().getNlgflg() != null && "Y".equals(processorDTO.getChdrpf().getNlgflg())){
			processorDTO = writeNLGRec(processorDTO);
		}
		return controlTotal(processorDTO);
	}
	private L2OverdueProcessorDTO processArrears(L2OverdueProcessorDTO processorDTO) throws Exception{
		Hpadpf hpadpf = hpadpfDAO.readHpadpfByCompanyAndContractNumber(processorDTO.getChdrcoy(),processorDTO.getChdrnum());
		processorDTO.setHpadpf(hpadpf);
		if(hpadpf != null && !"".equals(hpadpf.getZnfopt().trim())){
			String itemitme = hpadpf.getZnfopt().trim().concat(processorDTO.getChdrpf().getCnttype());
			Th584 th584IO = itempfService.readSmartTableByTrim(processorDTO.getChdrcoy(), "TH584", itemitme, Th584.class);
			if(th584IO == null){
				throw new StopRunException("B5355: Item not found in Table T5679: "+itemitme);
			}else {
				if ("".equals(th584IO.getNonForfeitMethod())) {
					processorDTO.getT6654IO().setArrearsMethod("");
				}else {
					processorDTO.getT6654IO().setArrearsMethod(th584IO.getNonForfeitMethod());
				}
			}
		}
		processorDTO.setChdrOk(false);
		processorDTO.setSkipContract(false);
		int freqFactor = datcon3.getMonthsDifference(processorDTO.getChdrpf().getOccdate(), processorDTO.getPtdate()).intValue();
		processorDTO.setDurationInForce(freqFactor);
		if ("A".equals(processorDTO.getWssaType())
				&& !"".equals(processorDTO.getT6654IO().getArrearsMethod())) {
			processorDTO = contractLevelNf(processorDTO);
			if (!processorDTO.getAplFlag()) {
				processorDTO = lapsePolicy(processorDTO);
				return processorDTO;
			}
			if(processorDTO.getNfloanDTO().getStatuz().equals("APLA")){
	//			callOverbill9000
				Chdrpf chdrpf = chdrpfDAO.readRecordOfChdr(processorDTO.getChdrcoy(), processorDTO.getChdrnum());
				processorDTO.setChdrpf(chdrpf);
				processorDTO.getChdrpf().setTranno(processorDTO.getChdrpf().getTranno()-1);
				Integer wsaaTranno = processorDTO.getChdrpf().getTranno();
				processorDTO = writeChdrValidflag(processorDTO);
				processorDTO.getChdrpf().setTranno(processorDTO.getChdrpf().getTranno()+1);
				processorDTO = writeChdrValidflag15300(processorDTO);
				if ("A".equals(processorDTO.getWssaType())) {
					Payrpf payrpf = payrpfDAO.getPayrRecord(processorDTO.getChdrcoy(),processorDTO.getChdrnum());
					processorDTO.setPayrpf(payrpf);
					processorDTO.getPayrpf().setTranno(wsaaTranno);
				}
				processorDTO = updatePayr(processorDTO);
				processorDTO = writePtrn(processorDTO); 
				if(chdrpf != null && chdrpf.getNlgflg() != null && "Y".equals(chdrpf.getNlgflg())){
					processorDTO = writeNLGRec(processorDTO);
				}
			}else{
				processorDTO = updatePayr(processorDTO);
			}
			processorDTO.getL2OverdueControlTotalDTO().setControlTotal09(processorDTO.getL2OverdueControlTotalDTO()
					.getControlTotal09().add(processorDTO.getPayrpf().getOutstamt()));
			return processorDTO;
		}
		return processorDTO;
	}
	private L2OverdueProcessorDTO contractLevelNf(L2OverdueProcessorDTO processorDTO) throws IOException, ParseException {
		processorDTO.setAplFlag(true);
		int t6597Inx = 0;
//		for(t6597Inx = processorDTO.getT6597Inner().size() - 1;!(t6597Inx == 0 || (processorDTO.getT6597Inner().get(t6597Inx).getT6597Method().equals(processorDTO.getT6654IO().getArrearsMethod())
//				&& processorDTO.getT6597Inner().get(t6597Inx).getT6597Itmfrm() <= processorDTO.getEffDate()));t6597Inx--){}

		for(t6597Inx = 0 ; t6597Inx <= processorDTO.getT6597Inner().size() - 1 ; t6597Inx++ ){
			if(processorDTO.getT6597Inner().get(t6597Inx).getT6597Method().equals(processorDTO.getT6654IO().getArrearsMethod())
					&& processorDTO.getT6597Inner().get(t6597Inx).getT6597Itmfrm() <= processorDTO.getEffDate()){
				break;		
			}
		}
		if(t6597Inx == processorDTO.getT6597Inner().size() - 1){
			throw new StopRunException("B5355: Item not found in Table T6597: ");
		}
		int wsaaSub = 0;
		for (wsaaSub = 0; !(wsaaSub > 2 || (processorDTO.getDurationInForce() <=  processorDTO.getT6597Inner().get(t6597Inx).getT6597Durmnth().get(wsaaSub)
		 && !"".equals(processorDTO.getT6597Inner().get(t6597Inx).getT6597Premsub().get(wsaaSub)))); wsaaSub++){}
		if(wsaaSub <= 2 ){
			if ("".equals(processorDTO.getT6597Inner().get(t6597Inx).getT6597Cpstat().get(wsaaSub))
			&& "".equals(processorDTO.getT6597Inner().get(t6597Inx).getT6597Crstat().get(wsaaSub))) {
				processorDTO = processT6597(processorDTO);
			}else {
				processorDTO.setAplFlag(false);
			}
		}
		processorDTO.setT6597Ix(t6597Inx);
		return processorDTO;
	}
	private L2OverdueProcessorDTO processT6597(L2OverdueProcessorDTO processorDTO) throws IOException, ParseException {
		Linspf linspf = linspfDAO.readLinspfRecord(processorDTO.getChdrcoy(), processorDTO.getChdrnum(), processorDTO.getPtdate());
		if(linspf == null){
			throw new StopRunException("B5355: No Record found in Table Linspf: ");
		}
		NfloanDTO nfloanDTO = new NfloanDTO();
		nfloanDTO.setLanguage(processorDTO.getLang());
		nfloanDTO.setChdrcoy(processorDTO.getChdrcoy());
		nfloanDTO.setChdrnum(processorDTO.getChdrnum());
		nfloanDTO.setCntcurr(processorDTO.getPayrpf().getCntcurr());
		nfloanDTO.setEffdate(processorDTO.getEffDate());
		nfloanDTO.setPtdate(processorDTO.getPtdate());
		nfloanDTO.setBatchbrn(processorDTO.getBatch());
		nfloanDTO.setCompany(processorDTO.getBatchcoy());
		nfloanDTO.setBillfreq(processorDTO.getPayrpf().getBillfreq());
		nfloanDTO.setCnttype(processorDTO.getChdrpf().getCnttype());
		nfloanDTO.setPolsum(processorDTO.getChdrpf().getPolsum());
		nfloanDTO.setPolinc(processorDTO.getChdrpf().getPolinc());
		nfloanDTO.setBatctrcde(processorDTO.getBatchTrcde());
		nfloanDTO.setOutstamt(linspf.getInstamt06());
		//need call -Nfloan subroutine   //modified by fwang3
		try {
			nfloanDTO = nfloan.processNFLoan(nfloanDTO);
		} catch (IOException | ParseException e) {
			throw new StopRunException("Nfloan execution error: " + e.getMessage());
		}
		if (!"OK".equals(nfloanDTO.getStatuz()) && !"NAPL".equals(nfloanDTO.getStatuz()) && !"APLA".equals(nfloanDTO.getStatuz())) {
			throw new StopRunException("B5355: Exception happen in Nfloan: ");
		}
		if ("NAPL".equals(nfloanDTO.getStatuz())||"APLA".equals(nfloanDTO.getStatuz())){
			return processorDTO;
		}
		processorDTO.setNfloanDTO(nfloanDTO);
		processorDTO.getChdrpf().setTranno(processorDTO.getChdrpf().getTranno() + 1);
		processorDTO = writeChdrValidflag(processorDTO);
		processorDTO = writeChdrValidflag15300(processorDTO);
		callCrtloan(processorDTO,linspf);
//		ingore print letter;
//		writeLetc8500()
		processorDTO.setChdrOk(true);
		processorDTO.setLastPtrn(processorDTO.getChdrnum());
		processorDTO = writePtrn(processorDTO);
		processorDTO.getL2OverdueControlTotalDTO().setControlTotal11(processorDTO.getL2OverdueControlTotalDTO()
				.getControlTotal11().add(processorDTO.getPayrpf().getOutstamt()));
		return processorDTO;
	}
	private L2OverdueProcessorDTO readT6654(String itemItem,L2OverdueProcessorDTO processorDTO) {
		int strEffDate = processorDTO.getEffDate();
		T6654 t6654IO = itempfService.readSmartTableByTrim(Companies.LIFE, "T6654", itemItem,strEffDate,T6654.class);
//		List<L2OverdueProcessorDTO.T6654Inner> t6654Recs = new ArrayList<>();
//		L2OverdueProcessorDTO.T6654Inner t6654Rec = new L2OverdueProcessorDTO.T6654Inner();
//		t6654Rec.setT6654Key(k.getItemitem());
//		t6654Rec.setT6654Billchnl(k.getItemitem().substring(0,1));
//		t6654Rec.setT6654Cnttype(k.getItemitem().substring(1));
//		t6654Rec.setT6654NonForfDays(k.getItemDetail().getNonForfietureDays());
//		t6654Rec.setT6654ArrearsMethod(k.getItemDetail().getArrearsMethod());
//		t6654Rec.setT6654Doctid(k.getItemDetail().getDoctids());
//		t6654Rec.setT6654Daexpy(k.getItemDetail().getDaexpys());
//		t6654Recs.add(t6654Rec);
			
//		processorDTO.setT6654Ix(t6654Recs.size() - 1);
		processorDTO.setT6654ExpyIx(3);
		processorDTO.setT6654SubrIx(4);
		processorDTO.setT6654IO(t6654IO);
		return processorDTO;
	}
	
	public L2OverdueProcessorDTO validateContract(L2OverdueProcessorDTO processorDTO) throws ParseException {
		Chdrpf chdrpf = chdrpfDAO.readRecordOfChdr(processorDTO.getChdrcoy(),processorDTO.getChdrnum());
		Payrpf payrpf = payrpfDAO.getPayrRecord(processorDTO.getChdrcoy(),processorDTO.getChdrnum());
		processorDTO.setChdrpf(chdrpf);
		processorDTO.setPayrpf(payrpf);
		if (!processorDTO.getChdrnum().equals(processorDTO.getPrevChdrnum())) {
			for (int i = 0; !(i >  11 || processorDTO.getValidFlag()); i++){
				if(chdrpf.getStatcode().equals(processorDTO.getT5679IO().getCnRiskStats().get(i))){
					for (i = 0; !(i >  11 || processorDTO.getValidFlag()); i++){
						if(chdrpf.getPstcde().equals(processorDTO.getT5679IO().getCnPremStats().get(i))){
							processorDTO.setValidFlag(true);
							break;
						}
					}
				}
			}
			if (!processorDTO.getValidFlag()) {
				processorDTO.setIgnoreFlag(true);
				return processorDTO;
			}
			processorDTO.setPrevChdrnum(processorDTO.getChdrnum());
		}
		Tr5b2 tr5b2IO = itempfService.readSmartTableByTrim(chdrpf.getChdrcoy(), "TR5B2", chdrpf.getCnttype(), Tr5b2.class);
		processorDTO.setMaxAge(tr5b2IO == null ? 0:tr5b2IO.getNlgczag());
		processorDTO.setMaxYear(tr5b2IO == null ? 0:tr5b2IO.getMaxnlgyrs());
		processorDTO.setNlgflag(tr5b2IO == null ? "": tr5b2IO.getNlg());
		String key = payrpf.getBillchnl().trim().concat(chdrpf.getCnttype());
		processorDTO = readT6654(key, processorDTO);
		if(processorDTO.getT6654IO() == null){
			key = payrpf.getBillchnl().trim().concat("***");
			processorDTO = readT6654(key, processorDTO);
			if (processorDTO.getT6654IO() == null) {
				throw new StopRunException("B5355: no data in T6654 ");
			}
		}
		int freqFactor = datcon3.getDaysDifference(processorDTO.getPtdate(),processorDTO.getEffDate()).intValue();
		processorDTO.setOverdueDays(freqFactor);
		int wsaaSub = 0;
		for(wsaaSub = 0; !(wsaaSub > 2|| processorDTO.getT6654IO().getNonForfietureDays() != 0 
				 || processorDTO.getT6654IO().getDaexpys().get(wsaaSub) != 0); wsaaSub++){
		}
		if(wsaaSub > 3){
			processorDTO.setIgnoreFlag(true);
			return processorDTO;
		}
		processorDTO.setWsaaSub(wsaaSub);
		 if(processorDTO.getT6654IO().getNonForfietureDays() <= processorDTO.getOverdueDays()
				 && processorDTO.getT6654IO().getNonForfietureDays() != 0){
			 processorDTO.setT6654SubrIx(3);
			 processorDTO.setWssaType("A");
			 if(!softlockContract(processorDTO)){
				 processorDTO.setIgnoreFlag(true);
			 }
			 return processorDTO;
		 }
		 int t6654ExpyIx = 0;
		 for (t6654ExpyIx = 2; !(t6654ExpyIx < 0
				 ||(processorDTO.getT6654IO().getDaexpys().get(t6654ExpyIx) <= processorDTO.getOverdueDays()
				 && processorDTO.getT6654IO().getDaexpys().get(t6654ExpyIx) != 0));t6654ExpyIx--){
		}
		 if (t6654ExpyIx == 0) {
			 processorDTO.setIgnoreFlag(true);
			 return processorDTO;
		}else {
			processorDTO.setT6654SubrIx(t6654ExpyIx); //1
			processorDTO.setWssaType(String.valueOf(t6654ExpyIx));
		}
		 if(!softlockContract(processorDTO)){
			 processorDTO.setIgnoreFlag(true);
		 }
		return processorDTO;
	}

	private  boolean softlockContract(L2OverdueProcessorDTO processorDTO) {
		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(processorDTO.getChdrnum());
		slckpf.setEnttyp("CH");
		slckpf.setCompany(processorDTO.getChdrcoy());
		slckpf.setProctrancd(processorDTO.getBatchTrcde());
		slckpf.setUsrprf(processorDTO.getUserName());
		slckpf.setJobnm(processorDTO.getBatchName());
		if (!sftlock.lockEntity(slckpf)) {
			LOGGER.info("Contract [{}] is already locked.", processorDTO.getChdrnum());//IJTI-1498
			return false;
		}
		return true;
	}

	private L2OverdueProcessorDTO searchT6647(L2OverdueProcessorDTO processorDTO){
		OvrdueDTO ovrduDTO = new OvrdueDTO();

		String itemitem = processorDTO.getBatchTrcde().concat(processorDTO.getChdrpf().getCnttype());
		T6647 t6647IO = itempfService.readSmartTableByTrim(Companies.LIFE, "T6647", itemitem,processorDTO.getChdrpf().getOccdate(),T6647.class);
		if(t6647IO != null){
			ovrduDTO.setAloind(t6647IO.getAloind());
			ovrduDTO.setEfdcode(t6647IO.getEfdcode());
			ovrduDTO.setProcSeqNo(t6647IO.getProcSeqNo());
		}
		processorDTO.setOvrdueDTO(ovrduDTO);
		return processorDTO;
	}
	private L2OverdueProcessorDTO setupOverdueRec(L2OverdueProcessorDTO processorDTO){
		processorDTO = searchT6647(processorDTO);
		OvrdueDTO ovrdueDTO = processorDTO.getOvrdueDTO();
		ovrdueDTO.setLanguage(processorDTO.getLang());
		ovrdueDTO.setChdrcoy(processorDTO.getChdrcoy());
		ovrdueDTO.setChdrnum(processorDTO.getChdrnum());
		ovrdueDTO.setLife(processorDTO.getCovrpf() == null ? "" : processorDTO.getCovrpf().getLife());
		ovrdueDTO.setCoverage(processorDTO.getCovrpf() == null ? "" : processorDTO.getCovrpf().getCoverage());
		ovrdueDTO.setRider(processorDTO.getCovrpf() == null ? "" : processorDTO.getCovrpf().getRider());
		ovrdueDTO.setPlanSuffix(0);
		ovrdueDTO.setTranno(processorDTO.getChdrpf().getTranno());
		ovrdueDTO.setCntcurr(processorDTO.getPayrpf().getCntcurr());
		ovrdueDTO.setEffdate(processorDTO.getEffDate());
		ovrdueDTO.setOutstamt(processorDTO.getPayrpf().getOutstamt());
		ovrdueDTO.setPtdate(processorDTO.getPtdate());
		ovrdueDTO.setBtdate(processorDTO.getBtdate());
		ovrdueDTO.setOvrdueDays(processorDTO.getOverdueDays());
		ovrdueDTO.setAgntnum(processorDTO.getChdrpf().getAgntnum());
		ovrdueDTO.setCownnum(processorDTO.getChdrpf().getCownnum());
		ovrdueDTO.setTrancode("B673");
		ovrdueDTO.setAcctyear(processorDTO.getActyear());
		ovrdueDTO.setAcctmonth(processorDTO.getActmonth());
		ovrdueDTO.setBatcbrn(processorDTO.getBatcbrn());
		ovrdueDTO.setBatcbatch(processorDTO.getBatch());
		ovrdueDTO.setUser(processorDTO.getPayrpf().getUser_t());
		/* MOVE BSPR-COMPANY           TO OVRD-COMPANY.                 */
		ovrdueDTO.setCompany(processorDTO.getChdrpf().getCowncoy());
		ovrdueDTO.setTranDate(processorDTO.getTransactionDate());
		ovrdueDTO.setTranTime(processorDTO.getTransactionTime());
		ovrdueDTO.setTermid("");
		ovrdueDTO.setCrtable(processorDTO.getCovrpf() == null ? "" :processorDTO.getCovrpf().getCrtable() );
		ovrdueDTO.setPumeth(processorDTO.getT5687IO() == null ? "" :processorDTO.getT5687IO().getPumeth());
		ovrdueDTO.setBillfreq(processorDTO.getPayrpf().getBillfreq());
		ovrdueDTO.setInstprem(BigDecimal.ZERO);
		ovrdueDTO.setSumins(BigDecimal.ZERO);
		ovrdueDTO.setCrrcd(0);
		ovrdueDTO.setNewCrrcd(0);
		ovrdueDTO.setNewSingp(BigDecimal.ZERO);
		ovrdueDTO.setNewAnb(0);
		ovrdueDTO.setNewPua(BigDecimal.ZERO);
		ovrdueDTO.setNewInstprem(BigDecimal.ZERO);
		ovrdueDTO.setNewSumins(BigDecimal.ZERO);
		ovrdueDTO.setPremCessDate(0);
		ovrdueDTO.setCnttype(processorDTO.getChdrpf().getCnttype());
		ovrdueDTO.setOccdate(processorDTO.getChdrpf().getOccdate());
		ovrdueDTO.setPolsum(processorDTO.getChdrpf().getPolsum());
		/*    Call the CALCFEE subroutine to calculate OVRD-PUPFEE.*/
//		callProgram(Calcfee.class, ovrdueDTO.ovrdueRec);
//		if (isNE(ovrdueDTO.statuz, varcom.oK)) {
//			syserrrec.params(ovrdueDTO.ovrdueRec);
//			syserrrec.statuz.set(ovrdueDTO.statuz);
//			fatalError600();
//		}
		ovrdueDTO = calcfee.processCalcfee(ovrdueDTO);
		if(!ovrdueDTO.getStatuz().equals("****")){
			throw new StopRunException("Calcfee statcode is :"+ovrdueDTO.getStatuz());
		}
		ovrdueDTO.setNewRiskCessDate(99999999);
		ovrdueDTO.setNewPremCessDate(99999999);
		ovrdueDTO.setNewBonusDate(99999999);
		ovrdueDTO.setNewRerateDate(99999999);
		ovrdueDTO.setCvRefund(BigDecimal.ZERO);
		ovrdueDTO.setSurrenderValue(BigDecimal.ZERO);
		ovrdueDTO.setEtiYears(0);
		ovrdueDTO.setEtiDays(0);
		processorDTO.setOvrdueDTO(ovrdueDTO);
		return processorDTO;
	}
	private void callCrtloan(L2OverdueProcessorDTO processorDTO,Linspf linspf) throws IOException, ParseException{
		T5645 t5645IO = itempfService.readSmartTableByTrim(processorDTO.getChdrcoy(), "T5654", "B5355", T5645.class);
		if (t5645IO == null) {
			throw new ItemNotfoundException("B5355: Item not found in Table T5645:B5355 " );
		}
		Descpf t5645Desc = descpfDAO.getDescInfo(Companies.LIFE, "T5645", "B5355");
		CrtloanDTO crtloanDto = new CrtloanDTO();
		crtloanDto.setLongdesc(t5645Desc == null ?"":t5645Desc.getLongdesc());
		crtloanDto.setChdrcoy(processorDTO.getChdrpf().getChdrcoy());
		crtloanDto.setChdrnum(processorDTO.getChdrpf().getChdrnum());
		crtloanDto.setCnttype(processorDTO.getChdrpf().getCnttype());
		crtloanDto.setLoantype("A");
		crtloanDto.setCntcurr(processorDTO.getChdrpf().getCntcurr());
		crtloanDto.setBillcurr(processorDTO.getPayrpf().getBillcurr());
		crtloanDto.setTranno(processorDTO.getChdrpf().getTranno());
		crtloanDto.setOccdate(processorDTO.getChdrpf().getOccdate());
		crtloanDto.setEffdate(processorDTO.getEffDate());
		crtloanDto.setAuthCode(processorDTO.getBatchTrcde());
		crtloanDto.setLanguage(processorDTO.getLang());
		crtloanDto.setOutstamt(linspf.getInstamt06());
		crtloanDto.setCompany(processorDTO.getBatchcoy());
		crtloanDto.setPrefix(processorDTO.getPrefix());
		crtloanDto.setBatch(processorDTO.getBatch());
		crtloanDto.setBranch(processorDTO.getBatcbrn());
		crtloanDto.setTrcde(processorDTO.getBatchTrcde());
		crtloanDto.setActmonth(processorDTO.getActmonth());
		crtloanDto.setActyear(processorDTO.getActyear());
//		crtloanDto.setBatchkey(processorDTO.getBatchkey());
		crtloanDto.setSacscode01(t5645IO.getSacscodes().get(0));
		crtloanDto.setSacscode02(t5645IO.getSacscodes().get(1));
		crtloanDto.setSacscode03(t5645IO.getSacscodes().get(2));
		crtloanDto.setSacscode04(t5645IO.getSacscodes().get(3));
		crtloanDto.setSacstyp01(t5645IO.getSacstypes().get(0));
		crtloanDto.setSacstyp02(t5645IO.getSacstypes().get(1));
		crtloanDto.setSacstyp03(t5645IO.getSacstypes().get(2));
		crtloanDto.setSacstyp04(t5645IO.getSacstypes().get(3));
		crtloanDto.setGlcode01(t5645IO.getGlmaps().get(0));
		crtloanDto.setGlcode02(t5645IO.getGlmaps().get(1));
		crtloanDto.setGlcode03(t5645IO.getGlmaps().get(2));
		crtloanDto.setGlcode04(t5645IO.getGlmaps().get(3));
		crtloanDto.setGlsign01(t5645IO.getSigns().get(0));
		crtloanDto.setGlsign02(t5645IO.getSigns().get(1));
		crtloanDto.setGlsign03(t5645IO.getSigns().get(2));
		crtloanDto.setGlsign04(t5645IO.getSigns().get(3));
		crtloan.createLoan(crtloanDto);
	}
	private L2OverdueProcessorDTO writePtrn(L2OverdueProcessorDTO processorDTO) {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setBatcpfx(processorDTO.getPrefix());
		ptrnIO.setBatccoy(processorDTO.getBatchcoy());
		ptrnIO.setBatcbrn(processorDTO.getBatcbrn());
		ptrnIO.setBatcactyr(processorDTO.getActyear());
		ptrnIO.setBatcactmn(processorDTO.getActmonth());
		ptrnIO.setBatctrcde(processorDTO.getBatchTrcde());
		ptrnIO.setBatcbatch(processorDTO.getBatch());
		ptrnIO.setChdrpfx(processorDTO.getChdrpf().getChdrpfx());
		ptrnIO.setChdrcoy(processorDTO.getChdrpf().getChdrcoy());
		ptrnIO.setChdrnum(processorDTO.getChdrpf().getChdrnum());
		ptrnIO.setTranno(processorDTO.getChdrpf().getTranno());
		ptrnIO.setValidflag("1");
		ptrnIO.setTrdt(processorDTO.getTransactionDate());
		ptrnIO.setTrtm(processorDTO.getTransactionTime());
		ptrnIO.setPtrneff(processorDTO.getPtdate());
		ptrnIO.setUserT(processorDTO.getPayrpf().getUser_t());
		ptrnIO.setDatesub(processorDTO.getEffDate());
		ptrnIO.setJobnm(processorDTO.getBatchName().toUpperCase());
		ptrnIO.setUsrprf(processorDTO.getUserName());
		processorDTO.setPtrnInsert(ptrnIO);
		return processorDTO;
	}
	private L2OverdueProcessorDTO lapsePolicy(L2OverdueProcessorDTO processorDTO) {
		OverbillDTO ovrbillrec = new OverbillDTO();
		ovrbillrec.setCompany(processorDTO.getPayrpf().getChdrcoy());
		ovrbillrec.setChdrnum(processorDTO.getPayrpf().getChdrnum());
		
		StringBuilder batchkey = new StringBuilder();
		batchkey.append(processorDTO.getPrefix());
		batchkey.append(processorDTO.getBatchcoy());
		batchkey.append(processorDTO.getBatcbrn());
		batchkey.append(DatimeUtil.parseYear(processorDTO.getEffDate(), "yyyyMMdd"));
		batchkey.append(DatimeUtil.parseMonth(processorDTO.getEffDate(), "yyyyMMdd"));
		batchkey.append(processorDTO.getBatchTrcde());
		batchkey.append(processorDTO.getBatch());
		ovrbillrec.setBatchkey(batchkey.toString());
		ovrbillrec.setNewTranno(1 + processorDTO.getChdrpf().getTranno());
		ovrbillrec.setBtdate(0);
		ovrbillrec.setPtdate(0);
		ovrbillrec.setLanguage(processorDTO.getLang());
		ovrbillrec.setBatctrcde(processorDTO.getBatchTrcde());
		ovrbillrec.setTranDate(processorDTO.getTransactionDate());
		ovrbillrec.setTranTime(processorDTO.getTransactionTime());
		ovrbillrec.setUser(0);
		ovrbillrec.setTermid(processorDTO.getChdrpf().getTranid());
//		ovrbillrec.setTermid(wsaaTermid);
		try {
			overbill.execute(ovrbillrec);
		} catch (IOException | ParseException e) {
			throw new StopRunException("Ovrbill execution error: " + e.getMessage());
		}
		processorDTO.getPayrpf().setBtdate(ovrbillrec.getBtdate());
		processorDTO.getPayrpf().setPtdate(ovrbillrec.getPtdate());
//		payzpfRec.setBtdate(ovrbillrec.getBtdate());
//		payzpfRec.setPtdate(ovrbillrec.getPtdate());
		Integer tranno = processorDTO.getChdrpf().getTranno();
		processorDTO = writeChdrValidflag(processorDTO);
		processorDTO.getChdrpf().setTranno(processorDTO.getChdrpf().getTranno() + 1);
		processorDTO = writeChdrValidflag15300(processorDTO);
		if ("A".equals(processorDTO.getWssaType())) {
			Payrpf payrpf = payrpfDAO.getPayrRecord(processorDTO.getChdrcoy(), processorDTO.getChdrnum());
			payrpf.setTranno(tranno);
			processorDTO.setPayrpf(payrpf);
		}	
		processorDTO = updatePayr(processorDTO);
		if (processorDTO.getPayrpf().getPtdate() < processorDTO.getPayrpf().getBtdate()) {
			processorDTO = writePtrn(processorDTO);
			if(processorDTO.getChdrpf() != null && processorDTO.getChdrpf().getNlgflg() != null && "Y".equals(processorDTO.getChdrpf().getNlgflg())){
				processorDTO = writeNLGRec(processorDTO);
			}
			processorDTO.getL2OverdueControlTotalDTO().setControlTotal09(processorDTO.getL2OverdueControlTotalDTO()
					.getControlTotal09().add(processorDTO.getPayrpf().getOutstamt()));
			return processorDTO;
		}
		List<Covrpf> covrpfList = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(Companies.LIFE, processorDTO.getChdrnum());
		for(Covrpf covrpf : covrpfList){
			processorDTO.setCovrpf(covrpf);
			processorDTO = callCovrrnlio(processorDTO);
			if(processorDTO.getCovrGoNext())
				continue;
		}
		if (processorDTO.getSkipContract()) {
			processorDTO = writePtrn(processorDTO);
			//ILIFE-3997 -STARTS
			if(processorDTO.getChdrpf() != null && processorDTO.getChdrpf().getNlgflg() != null && "Y".equals(processorDTO.getChdrpf().getNlgflg())){
				processorDTO = writeNLGRec(processorDTO);
			}
			processorDTO.getL2OverdueControlTotalDTO().setControlTotal12(processorDTO.getL2OverdueControlTotalDTO()
					.getControlTotal12().add(processorDTO.getPayrpf().getOutstamt()));
			return processorDTO;
		}
		processorDTO = endOfCovrProcess(processorDTO);
		if (!processorDTO.getChdrpf().getChdrnum().equals(processorDTO.getLastPtrn())) {
			processorDTO.setLastPtrn(processorDTO.getChdrpf().getChdrnum());
			processorDTO = writePtrn(processorDTO);
			if(processorDTO.getChdrpf() != null && processorDTO.getChdrpf().getNlgflg() != null && "Y".equals(processorDTO.getChdrpf().getNlgflg())){
				processorDTO = writeNLGRec(processorDTO);
			}
				
			//ILIFE-3997-ENDS
		}
		delUnderwritting(processorDTO);
		return processorDTO;
	}
	private  L2OverdueProcessorDTO delUnderwritting(L2OverdueProcessorDTO processorDTO){
		if ("IF".equals(processorDTO.getChdrpf().getStatcode())) {
			return processorDTO;
		}
		List<Lifepf> lifeList = lifepfDAO.searchLifeRecordByChdrNum(processorDTO.getChdrcoy(), processorDTO.getChdrnum());
		 if(lifeList != null && !lifeList.isEmpty()){
			 for(Lifepf lifepf : lifeList){
				 CrtundwrtDTO crtundwrec = new CrtundwrtDTO();
				 crtundwrec.setClntnum(lifepf.getLifcnum());
				 crtundwrec.setCoy(processorDTO.getChdrcoy());
				 crtundwrec.setCurrcode(processorDTO.getChdrpf().getCntcurr());
				 crtundwrec.setChdrnum(processorDTO.getChdrnum());
				 crtundwrec.setCrtable("*");
				 crtundwrec.setFunction("DEL");
				 try {
					crtundwrt.processCrtundwrt(crtundwrec);
				} catch (NumberFormatException | ParseException | IOException e) {
					throw new SubroutineIOException("B5355: Exception in crtundwrt: " + e.getMessage());
				}
			 }
		 }
		 return processorDTO;
	}
	private L2OverdueProcessorDTO writeCovrValidflag(L2OverdueProcessorDTO processorDTO) {
		Covrpf covrpfInsert = new Covrpf(processorDTO.getCovrpf());
		if (processorDTO.getOvrdueDTO().getNewRiskCessDate() != 99999999 && processorDTO.getOvrdueDTO().getNewRiskCessDate() != 0){
			covrpfInsert.setRiskCessDate(processorDTO.getOvrdueDTO().getNewRiskCessDate());
		}
		if (processorDTO.getOvrdueDTO().getNewPremCessDate() != 99999999 && processorDTO.getOvrdueDTO().getNewPremCessDate() != 0){
			covrpfInsert.setPremCessDate(processorDTO.getOvrdueDTO().getNewPremCessDate());
		}
		if (processorDTO.getOvrdueDTO().getNewRerateDate() != 99999999 && processorDTO.getOvrdueDTO().getNewRerateDate() != 0){
			covrpfInsert.setRerateDate(processorDTO.getOvrdueDTO().getNewRerateDate());
		}
		if (processorDTO.getOvrdueDTO().getNewSumins() != BigDecimal.ZERO ){
			covrpfInsert.setSumins(processorDTO.getOvrdueDTO().getNewSumins());
			covrpfInsert.setVarSumInsured(BigDecimal.ZERO);
		} else if (covrpfInsert.getVarSumInsured().compareTo(BigDecimal.ZERO) == 1) {
			covrpfInsert.setVarSumInsured(covrpfInsert.getVarSumInsured().subtract(covrpfInsert.getSumins()).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		}
		
		if (covrpfInsert.getSumins().compareTo(BigDecimal.ZERO) != 0) {
			covrpfInsert.setSumins(callRounding(covrpfInsert.getSumins(),processorDTO.getPayrpf().getCntcurr(),Companies.LIFE));
		}
		if (processorDTO.getOvrdueDTO().getNewCrrcd() != 99999999 && processorDTO.getOvrdueDTO().getNewCrrcd() != 0){
			covrpfInsert.setCrrcd(processorDTO.getOvrdueDTO().getNewCrrcd());
		}
		if (processorDTO.getOvrdueDTO().getNewSingp().compareTo(BigDecimal.ZERO) != 0){
			covrpfInsert.setSingp(processorDTO.getOvrdueDTO().getNewSingp());
		}
		if (processorDTO.getOvrdueDTO().getNewAnb() != 0){
			covrpfInsert.setAnbAtCcd(processorDTO.getOvrdueDTO().getNewAnb());
		}
		covrpfInsert.setInstprem(processorDTO.getOvrdueDTO().getInstprem());
		if (covrpfInsert.getInstprem().compareTo(BigDecimal.ZERO) != 0){
			covrpfInsert.setInstprem(callRounding(covrpfInsert.getInstprem(),processorDTO.getPayrpf().getCntcurr(),Companies.LIFE));
		}
		if (processorDTO.getOvrdueDTO().getNewPua().compareTo(BigDecimal.ZERO) != 0){
			covrpfInsert.setVarSumInsured(covrpfInsert.getVarSumInsured().add(processorDTO.getOvrdueDTO().getNewPua()).setScale(2,BigDecimal.ROUND_HALF_DOWN));
		}
		
		if (covrpfInsert.getVarSumInsured().compareTo(BigDecimal.ZERO) != 0){
			covrpfInsert.setVarSumInsured(callRounding(covrpfInsert.getVarSumInsured(),processorDTO.getPayrpf().getCntcurr(),Companies.LIFE));
		}
		if (processorDTO.getOvrdueDTO().getNewBonusDate() != 99999999 && processorDTO.getOvrdueDTO().getNewBonusDate() != 0){
			covrpfInsert.setUnitStatementDate(processorDTO.getOvrdueDTO().getNewBonusDate());
		}
		
		covrpfInsert.setValidflag("1");
		covrpfInsert.setCurrto(99999999);
		covrpfInsert.setCurrfrom(processorDTO.getChdrpf().getPtdate());
		covrpfInsert.setTranno(processorDTO.getChdrpf().getTranno());
		covrpfInsert.setTransactionDate(processorDTO.getTransactionDate());
		covrpfInsert.setTransactionTime(processorDTO.getTransactionTime());
		covrpfInsert.setUser(0);
		processorDTO = lapsePua6600(processorDTO);
		covrpfInsert.setJobnm(processorDTO.getBatchName().toUpperCase());
		covrpfInsert.setUsrprf(processorDTO.getUserName());
		processorDTO.getCovrInsert().add(covrpfInsert);
		return processorDTO;
	}
	private L2OverdueProcessorDTO lapsePua6600(L2OverdueProcessorDTO processorDTO){
		List<Hpuapf> hpuapfList = hpuapfDAO.readHpuaRecord(processorDTO.getChdrcoy(), processorDTO.getChdrnum());
		for(Hpuapf hpuapf : hpuapfList){
			if(hpuapf.getTranno() == processorDTO.getOvrdueDTO().getTranno()){
				continue;
			}
			hpuapf.setValidflag("2");
			hpuapf.setJobName(processorDTO.getBatchName().toUpperCase());
			hpuapf.setUserProfile(processorDTO.getUserName());
			processorDTO.getHpuapfUpdate().add(hpuapf);
			if(processorDTO.getCovrpf().getVarSumInsured().compareTo(BigDecimal.ZERO) == 1){
				processorDTO.getCovrpf().setVarSumInsured(processorDTO.getCovrpf().getVarSumInsured().subtract(new BigDecimal(hpuapf.getSumin()).setScale(2)));
			}
			hpuapf.setValidflag("1");
			hpuapf.setTranno(processorDTO.getCovrpf().getTranno());
			hpuapf.setPstatcode(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Cpstat().get(3));
			hpuapf.setRstatcode(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Crstat().get(3));
			hpuapf.setRiskCessDate(processorDTO.getCovrpf().getRiskCessDate());
			hpuapf.setJobName(processorDTO.getBatchName().toUpperCase());
			hpuapf.setUserProfile(processorDTO.getUserName());
			processorDTO.getHpuapfInsert().add(hpuapf);
		}
		return processorDTO;
	}
	private L2OverdueProcessorDTO callCovrrnlio(L2OverdueProcessorDTO processorDTO) {
		if(!"1".equals(processorDTO.getCovrpf().getValidflag())){
			processorDTO.setCovrGoNext(true);
			return processorDTO;
		}
		boolean validCoverage = false;
		for (int wsaaT5679Ix = 0; !(wsaaT5679Ix > 11|| validCoverage); wsaaT5679Ix++){
			if(processorDTO.getT5679IO().getCovRiskStats().get(wsaaT5679Ix).equals(processorDTO.getCovrpf().getStatcode())){
				for(wsaaT5679Ix = 0; !(wsaaT5679Ix > 11|| validCoverage); wsaaT5679Ix++){
					if(processorDTO.getT5679IO().getCovPremStats().get(wsaaT5679Ix).equals(processorDTO.getCovrpf().getPstatcode())){
						validCoverage = true;
					}
				}
			}
		}
		if (!validCoverage) {
//			if (isEQ(ovrduerec.statuz, "OMIT")) {
//				return ;
//			}
			processorDTO.getCovrPremStats().add(processorDTO.getCovrpf().getPstatcode());
			processorDTO.getCovrRiskStats().add(processorDTO.getCovrpf().getStatcode());
			processorDTO.setCovrGoNext(true);
			return processorDTO;
		}
		T5687 t5687IO = itempfService.readSmartTableByTrim(Companies.LIFE, "T5687", processorDTO.getCovrpf().getCrtable(),processorDTO.getEffDate(),T5687.class);
		if(t5687IO == null){
			throw new StopRunException("B5355: Item not found in Table T5687: ");
		}
		processorDTO.setT5687IO(t5687IO);
		processorDTO = searchT6597NonForfeit(processorDTO);
		if ("".equals(processorDTO.getT5687IO().getNonForfeitMethod())) {
			processorDTO.setCovrGoNext(true);
			return processorDTO;
		}
		processorDTO.setCovrrnlPstat("");
		processorDTO.setCovrrnlStat("");
		processorDTO.setCovrrnlStatiiSet("");

		int wsaaSub = processorDTO.getWsaaSub();
		for (wsaaSub = 0; !(wsaaSub > 2|| (processorDTO.getDurationInForce() <= processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Durmnth().get(wsaaSub)
				&& !"".equals(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Premsub().get(wsaaSub).trim()))); wsaaSub++) {
		}
		if(wsaaSub > 2){
			processorDTO.getCovrPremStats().add(processorDTO.getCovrpf().getPstatcode());
			processorDTO.getCovrRiskStats().add(processorDTO.getCovrpf().getStatcode());
			processorDTO.setCovrGoNext(true);
			return processorDTO;
		}
		processorDTO.setWsaaSub(wsaaSub);
		processorDTO = checkT6597(processorDTO);
		if(processorDTO.getSkipContract()){
			processorDTO.setCovrGoNext(true);
			return processorDTO;
		}
		if ("OMIT".equals(processorDTO.getOvrdueDTO().getStatcode())) {
			processorDTO.setCovrGoNext(true);
			return processorDTO;
		}
		processorDTO = writeCovrHistory(processorDTO);
		if ("Y".equals(processorDTO.getCovrrnlStatiiSet())) {
			processorDTO.getCovrpf().setPstatcode(processorDTO.getCovrrnlPstat());
			processorDTO.getCovrpf().setStatcode(processorDTO.getCovrrnlStat());
		}
		if ("OMIT".equals(processorDTO.getOvrdueDTO().getStatuz())) {
			return processorDTO;
		}
		processorDTO.getCovrPremStats().add(processorDTO.getCovrpf().getPstatcode());
		processorDTO.getCovrRiskStats().add(processorDTO.getCovrpf().getStatcode());
		processorDTO = writeCovrValidflag(processorDTO);
		return processorDTO;
	}
	private L2OverdueProcessorDTO writeCovrHistory(L2OverdueProcessorDTO processorDTO){
		Covrpf covrrnlUpdate = new Covrpf(processorDTO.getCovrpf());
		covrrnlUpdate.setCurrto(processorDTO.getPtdate());
		covrrnlUpdate.setValidflag("2");
		covrrnlUpdate.setJobnm(processorDTO.getBatchName().toUpperCase());
		covrrnlUpdate.setUsrprf(processorDTO.getUserName());
		processorDTO.getCovrUpdate().add(covrrnlUpdate);
		return processorDTO;
	}
	private L2OverdueProcessorDTO checkT6597(L2OverdueProcessorDTO processorDTO) {
		if(processorDTO.getChdrpf() != null && processorDTO.getChdrpf().getNlgflg() != null && "Y".equals(processorDTO.getChdrpf().getNlgflg()))
			processorDTO = writeNLGRec(processorDTO);
			processorDTO = setupOverdueRec(processorDTO);
			if(processorDTO.getNlgflag().equals("N")){
				processorDTO.getOvrdueDTO().setStatcode(processorDTO.getChdrpf().getStatcode());
				processorDTO.setUpdateRequired(true);
			}
			processorDTO.getOvrdueDTO().setPlanSuffix(processorDTO.getCovrpf().getPlanSuffix());
			processorDTO.getOvrdueDTO().setInstprem(processorDTO.getCovrpf().getInstprem());
			processorDTO.getOvrdueDTO().setSumins(processorDTO.getCovrpf().getSumins());
			processorDTO.getOvrdueDTO().setCrrcd(processorDTO.getCovrpf().getCrrcd());
			processorDTO.getOvrdueDTO().setPremCessDate(processorDTO.getCovrpf().getPcesdte());
			processorDTO.getOvrdueDTO().setRiskCessDate(processorDTO.getCovrpf().getRcesdte());
			processorDTO.getOvrdueDTO().setNewInstprem(processorDTO.getCovrpf().getInstprem());
//			callProgram(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], ovrduerec.ovrdueRec);
			//Add  Nftrad Subroutine
			try {
				OvrdueDTO ovrdueDTO = nftrad.execute(processorDTO.getOvrdueDTO());
				processorDTO.setOvrdueDTO(ovrdueDTO);
			} catch (Exception e) {
				throw new StopRunException("Nftrad execution error: " + e.getMessage());
			}
			int index = processorDTO.getWsaaSub();
			if ("****".equals(processorDTO.getOvrdueDTO().getStatuz())) {
				if(!"".equals(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Cpstat().get(index))
				&& !"".equals(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Crstat().get(index)) ) {
					if(!processorDTO.getUpdateRequired() && processorDTO.getNlgcalcDTO().getNlgBalance().compareTo(BigDecimal.ZERO) <= 0){
						processorDTO.setCovrrnlStatiiSet("Y");
						processorDTO.getOvrdueDTO().setPstatcode(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Cpstat().get(index));
						processorDTO.setCovrrnlPstat(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Cpstat().get(index));
						processorDTO.getOvrdueDTO().setStatcode(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Crstat().get(index));
						processorDTO.setCovrrnlStat(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Crstat().get(index));
					}
				}
			}else{
				if ("LAPS".equals(processorDTO.getOvrdueDTO().getStatuz())) {
					//Add  NFTRAD Subroutine
					try {
						OvrdueDTO ovrdueDTO = nftrad.execute(processorDTO.getOvrdueDTO());
						processorDTO.setOvrdueDTO(ovrdueDTO);
					} catch (Exception e) {
						throw new StopRunException("Nftrad execution error: " + e.getMessage());
					}
					index = 3;
					processorDTO.setCovrrnlStatiiSet("Y");
					processorDTO.getOvrdueDTO().setPstatcode(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Cpstat().get(index));
					processorDTO.setCovrrnlPstat(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Cpstat().get(index));
					processorDTO.getOvrdueDTO().setStatcode(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Crstat().get(index));
					processorDTO.setCovrrnlStat(processorDTO.getT6597Inner().get(processorDTO.getT6597Ix()).getT6597Crstat().get(index));
				}else {
					if ("SKIP".equals(processorDTO.getOvrdueDTO().getStatuz())) {
						processorDTO.setSkipContract(true);
					}else {
						if(!"OMIT".equals(processorDTO.getOvrdueDTO().getStatuz())) {
							throw new StopRunException("B5355: Item not found in Table T6597: ");
						}
					}
					
				}
			}
			processorDTO.setWsaaSub(index);
			Chdrpf chdrpf = chdrpfDAO.readRecordOfChdr(processorDTO.getChdrcoy(),processorDTO.getChdrnum());
			processorDTO.setChdrOk(true);
			if (processorDTO.getChdrnum().equals(processorDTO.getLastPtrn())) {
				processorDTO.setLastPtrn(processorDTO.getChdrnum());
				/*        PERFORM 5800-WRITE-PTRN                          <C16N>  */
				if (!processorDTO.getSkipContract()) {
					chdrpf.setTranno(chdrpf.getTranno()+1);  //ILIFE-4323 by dpuhawan
					processorDTO = writePtrn(processorDTO);
				}
				processorDTO.setChdrpf(chdrpf);
			}
		
		return processorDTO;
	}

	private L2OverdueProcessorDTO searchT6597NonForfeit(L2OverdueProcessorDTO processorDTO){
		int t5697Inx = 0;
		if(!"".equals(processorDTO.getHpadpf().getZnfopt().trim())){
			String itemitme = processorDTO.getHpadpf().getZnfopt().trim().concat(processorDTO.getCovrpf().getCrtable());
			Th584 th584IO = itempfService.readSmartTableByTrim(processorDTO.getChdrcoy(), "TH584", itemitme, Th584.class);
			if(th584IO != null){
				if (!"".equals(th584IO.getNonForfeitMethod())) {
					processorDTO.getT5687IO().setNonForfeitMethod(th584IO.getNonForfeitMethod());
					for (t5697Inx = processorDTO.getT6597Inner().size() - 1; !(t5697Inx==0
							|| (processorDTO.getT6597Inner().get(t5697Inx).getT6597Method().equals(processorDTO.getT5687IO().getNonForfeitMethod())
									&&processorDTO.getT6597Inner().get(t5697Inx).getT6597Itmfrm() <= processorDTO.getEffDate()));t5697Inx--){
					}
					if(t5697Inx==0){
						throw new StopRunException("B5355: Item not found in Table T6597: ");
					}
					
					processorDTO.setT6597Ix(t5697Inx);
					return processorDTO;
				}
			}
		}

		for (t5697Inx = processorDTO.getT6597Inner().size() - 1; !(t5697Inx==0
				|| (processorDTO.getT6597Inner().get(t5697Inx).getT6597Method().equals(processorDTO.getT5687IO().getNonForfeitMethod())
						&&processorDTO.getT6597Inner().get(t5697Inx).getT6597Itmfrm() <= processorDTO.getEffDate()));t5697Inx--){
		}
		if(t5697Inx==0){
			throw new StopRunException("B5355: Item not found in Table T6597: ");
		}
		processorDTO.setT6597Ix(t5697Inx);
		return processorDTO;
	}

	private L2OverdueProcessorDTO writeNLGRec(L2OverdueProcessorDTO processorDTO) {
		List<Nlgtpf> nlgtpfList = nlgtpfDAO.readNlgtpf(processorDTO.getPayrpf().getChdrcoy(), processorDTO.getPayrpf().getChdrnum());
		for (Nlgtpf nlgtpf : nlgtpfList) {
			if (("B521".equals(nlgtpf.getBatctrcde()) && nlgtpf.getEffdate() == processorDTO.getPayrpf().getPtdate() 
					&& nlgtpf.getAmnt04().compareTo(BigDecimal.ZERO)!=0)) {
				return processorDTO;				
			}
		}
		NlgcalcDTO nlgcalcDTO = new NlgcalcDTO();
		nlgcalcDTO.setChdrcoy(processorDTO.getPtrnpf().getChdrcoy());
		nlgcalcDTO.setChdrnum(processorDTO.getPtrnpf().getChdrnum());
		nlgcalcDTO.setEffdate(processorDTO.getPtrnpf().getPtrneff());
//		nlgcalcDTO.setTranno(processorDTO.getPtrnpf().getTranno());
		nlgcalcDTO.setBatcactyr(processorDTO.getPtrnpf().getBatcactyr());
		nlgcalcDTO.setBatcactmn(processorDTO.getPtrnpf().getBatcactmn());
		nlgcalcDTO.setBatctrcde(processorDTO.getPtrnpf().getBatctrcde());
		nlgcalcDTO.setBillfreq(processorDTO.getChdrpf().getBillfreq());
		nlgcalcDTO.setCnttype(processorDTO.getChdrpf().getCnttype());
		nlgcalcDTO.setLanguage(processorDTO.getLang());
		nlgcalcDTO.setFrmdate(processorDTO.getChdrpf().getOccdate());
		nlgcalcDTO.setTodate(99999999);
		nlgcalcDTO.setOccdate(processorDTO.getChdrpf().getOccdate());
		nlgcalcDTO.setPtdate(processorDTO.getChdrpf().getPtdate());
		nlgcalcDTO.setInputAmt(processorDTO.getPayrpf().getSinstamt01());
		nlgcalcDTO.setInputAmt(nlgcalcDTO.getInputAmt().multiply(new BigDecimal(-1).setScale(0)));
		
		nlgcalcDTO.setOvduePrem(processorDTO.getPayrpf().getSinstamt01());
		nlgcalcDTO.setOvduePrem(nlgcalcDTO.getOvduePrem().multiply(new BigDecimal(-1).setScale(0)));
		nlgcalcDTO.setFunction("OVDUE");
		try {
			nlgcalcDTO = nlgcalc.processNlgcalOvdue(nlgcalcDTO);
		} catch (IOException e) {
			e.printStackTrace();
			throw new StopRunException("Nlgcalc error " + e.getMessage());
		} catch (ParseException e) {
			e.printStackTrace();
			throw new StopRunException("Nlgcalc error " + e.getMessage());
		}
		
		if ("****".equals(nlgcalcDTO.getStatus())) {
			throw new StopRunException("Nlgcalc error ");
		}
		processorDTO.setNlgcalcDTO(nlgcalcDTO);
		return processorDTO;
	}
	private L2OverdueProcessorDTO updatePayr(L2OverdueProcessorDTO processorDTO) {
		Payrpf payrUpdate = new Payrpf(processorDTO.getPayrpf());
		payrUpdate.setValidflag("2");
		processorDTO.setPayrUpdate(payrUpdate);
		
		Payrpf payrIOInsert = new Payrpf(processorDTO.getPayrpf());
		payrIOInsert.setValidflag("1");
		payrIOInsert.setTranno(processorDTO.getChdrpf().getTranno());
		payrIOInsert.setEffdate(processorDTO.getPayrpf().getPtdate());
		processorDTO.setPayrInsert(payrIOInsert);
		return processorDTO;
	}
	public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}	
	private BigDecimal callRounding(BigDecimal amountIn, String currency, String batchTrcde) {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(currency);
		zrdecplDTO.setCompany(Companies.LIFE);
		zrdecplDTO.setBatctrcde(batchTrcde);
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = zrdecplc.convertAmount(zrdecplDTO);
		} catch (IOException e) {
			throw new ItemParseException("L2overdue: Parsed failed in zrdecplc:" + e.getMessage());
		}
		return result;
	}
	
	private L2OverdueProcessorDTO endOfCovrProcess(L2OverdueProcessorDTO processorDTO){
		boolean covrRskMatch = false;
		boolean covrPrmMatch = false;

		String keyItem = processorDTO.getBatchTrcde().concat(processorDTO.getChdrpf().getCnttype().trim());
//		T5399 t5399IO = itempfService.readSmartTableByTableNameAndItem(company, smartTableName, itemItem, itemPrefix, pojo)("T5399", keyItem, processorDTO.getEffDate(),processorDTO.getEffDate(),T5399.class);
		T5399 t5399IO =itempfService.readSmartTableByTrim(Companies.LIFE, "T5399",keyItem,  T5399.class);
		if (t5399IO == null){
			throw new ItemParseException("L2overdue: no data in T5399");
		}
		
		if (t5399IO.getCovRiskStats() == null || t5399IO.getCovRiskStats().isEmpty()){
			 return processorDTO;
		} 
		for (String covrRisk : processorDTO.getCovrRiskStats()){
			int riskIndex  = t5399IO.getCovRiskStats().indexOf(covrRisk);
			if (riskIndex >= 0){
				processorDTO.getChdrpf().setStatcode(t5399IO.getSetCnRiskStats().get(t5399IO.getCovRiskStats().indexOf(covrRisk)));
				covrRskMatch = true;
				break;
			}		
		}
		
		if (!covrRskMatch && (!"Y".equals(processorDTO.getNlgflag())  && !"".equals(processorDTO.getNlgflag()) && ! processorDTO.getUpdateRequired())) {  
			processorDTO.getChdrpf().setStatcode(processorDTO.getT5679IO().getSetCnRiskStat());
		}
		if (t5399IO.getCovPremStat() == null || t5399IO.getCovPremStat().isEmpty()){
			 return processorDTO;
		} 
		for (String covrPrem : processorDTO.getCovrPremStats()){
			int premIndex  = t5399IO.getCovPremStat().indexOf(covrPrem);
			if (premIndex >= 0 && ! processorDTO.getUpdateRequired() && processorDTO.getNlgcalcDTO().getNlgBalance().compareTo(BigDecimal.ZERO) <= 0){ 
				processorDTO.getChdrpf().setPstcde(t5399IO.getSetCnPremStats().get(t5399IO.getCovPremStat().indexOf(covrPrem)));
				processorDTO.getPayrpf().setPstatcode(t5399IO.getSetCnPremStats().get(t5399IO.getCovPremStat().indexOf(covrPrem)));
				covrPrmMatch = true;
				break;			
			}		
		}
		
		if (!covrPrmMatch && !processorDTO.getUpdateRequired() && processorDTO.getNlgcalcDTO().getNlgBalance().compareTo(BigDecimal.ZERO) <= 0) {
			processorDTO.getChdrpf().setPstcde(processorDTO.getT5679IO().getSetCnPremStat());
			processorDTO.getPayrpf().setPstatcode(processorDTO.getT5679IO().getSetCnPremStat());
		}
		if (!processorDTO.getChdrOk()) {
			Chdrpf chdrpf = chdrpfDAO.readRecordOfChdr(processorDTO.getChdrcoy(),processorDTO.getChdrnum());
			processorDTO.setChdrpf(chdrpf);
		}
		Chdrpf chdrupdate2 = new Chdrpf(processorDTO.getChdrpf());
		chdrupdate2.setJobnm(processorDTO.getBatchName().toUpperCase());
		chdrupdate2.setUsrprf(processorDTO.getUserName());
		processorDTO.setChdrpfUpdate2(chdrupdate2);
		
		Payrpf payr = new Payrpf(processorDTO.getPayrpf()); 
//		payr.setBtdate(processorDTO.getPayrpf().getBtdate());
//		payr.setBillcd(processorDTO.getPayrpf().getBtdate());
		payr.setPstatcode(processorDTO.getPayrpf().getPstatcode()); 
		payr.setJobnm(processorDTO.getBatchName().toUpperCase());
		payr.setUsrprf(processorDTO.getUserName());
		processorDTO.setPayrPstcde(payr);
		
		return processorDTO;
	}
	private L2OverdueProcessorDTO controlTotal(L2OverdueProcessorDTO processorDTO){
		if (processorDTO.getT6654SubrIx() == 1){
			processorDTO.getL2OverdueControlTotalDTO().setControlTotal03(processorDTO.getL2OverdueControlTotalDTO()
					.getControlTotal03().add(processorDTO.getPayrpf().getOutstamt()));
		}else if (processorDTO.getT6654SubrIx() == 2){
			processorDTO.getL2OverdueControlTotalDTO().setControlTotal05(processorDTO.getL2OverdueControlTotalDTO()
					.getControlTotal05().add(processorDTO.getPayrpf().getOutstamt()));
		}else if (processorDTO.getT6654SubrIx() == 3){
			processorDTO.getL2OverdueControlTotalDTO().setControlTotal07(processorDTO.getL2OverdueControlTotalDTO().
					getControlTotal07().add(processorDTO.getPayrpf().getOutstamt()));
		}
		return processorDTO;
	}
	
	
	@Override
	public void statistics(L2OverdueProcessorDTO processorDTO) {
		LifsttrDTO lifsttrrec = new LifsttrDTO();
		lifsttrrec.setBatccoy(Companies.LIFE);
		lifsttrrec.setBatcbrn(processorDTO.getBatcbrn());
		lifsttrrec.setBatcacty(processorDTO.getActmonth());
		lifsttrrec.setBatcactmn(processorDTO.getActmonth());
		lifsttrrec.setBatctrcde(processorDTO.getBatchTrcde());
		lifsttrrec.setBatcbatch(processorDTO.getBatch());
		lifsttrrec.setChdrcoy(processorDTO.getChdrpf().getChdrcoy());
		lifsttrrec.setChdrnum(processorDTO.getChdrpf().getChdrnum());
		lifsttrrec.setTranno(processorDTO.getChdrpf().getTranno());
		lifsttrrec.setTrannor(99999);
		lifsttrrec.setAgntnum("");
		lifsttrrec.setOldAgntnum("");
		try {
			lifsttr.processLifsttr(lifsttrrec);
		} catch (IOException e) {
			throw new SubroutineIOException("Br613: IOException in lifsttr: " + e.getMessage());
		}
		
	}
}
