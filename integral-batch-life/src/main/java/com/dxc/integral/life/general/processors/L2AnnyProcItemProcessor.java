package com.dxc.integral.life.general.processors;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.life.beans.L2AnnyProcControlTotalDTO;
import com.dxc.integral.life.beans.L2AnnyProcProcessorDTO;
import com.dxc.integral.life.beans.L2AnnyProcReaderDTO;
import com.dxc.integral.life.utils.L2AnnyProcProcessorUtil;

public class L2AnnyProcItemProcessor extends BaseProcessor implements ItemProcessor<L2AnnyProcReaderDTO,L2AnnyProcReaderDTO>{
	private static final Logger LOGGER = LoggerFactory.getLogger(L2AnnyProcItemProcessor.class);
	
	@Autowired
	L2AnnyProcProcessorUtil l2AnnyProcProcessorUtil;
	
	@Value("#{jobParameters['userName']}")
	private String userName;

	@Value("#{jobParameters['trandate']}")
	private String transactionDate;

	@Value("#{jobParameters['businessDate']}") 
	private String businessDate;

	@Value("#{jobParameters['batchName']}")
	private String batchName;

	@Value("#{jobParameters['cntBranch']}")
	private String branch;
	
	@Autowired
	private Environment env;
	@Autowired
	private L2AnnyProcControlTotalDTO l2AnnyProcControlTotalDTO;	

	

	@Override
	public L2AnnyProcReaderDTO process(L2AnnyProcReaderDTO readerDTO) throws Exception {
		LOGGER.info("Anniversary Batch Processing starts for contract Number: {}",readerDTO.getChdrnum());//IJTI-1498
		
		L2AnnyProcProcessorDTO l2processorDTO = new L2AnnyProcProcessorDTO();
		Usrdpf usrdpf = getUserDetails(userName);
		Batcpf batchDetail = getBatcpfInfo(transactionDate,businessDate,Companies.LIFE,"B675",branch,usrdpf,batchName);
		l2processorDTO.setActMonth(getActMonth(transactionDate));
		l2processorDTO.setActYear(getActYear(transactionDate));
		l2processorDTO.setLang(usrdpf.getLanguage());
		l2processorDTO.setPrefix(batchDetail.getBatcpfx());
		l2processorDTO.setBatch(batchDetail.getBatcbatch());
		l2processorDTO.setBatchtrancde(batchDetail.getBatctrcde());
		l2processorDTO.setBatchbranch(batchDetail.getBatcbrn());
		l2processorDTO.setChdrcoy(readerDTO.getChdrcoy());
		l2processorDTO.setChdrnum(readerDTO.getChdrnum());
		l2processorDTO.setUserName(userName);
		l2processorDTO.setBatchName(batchName);
		l2processorDTO.setTransactionDate(Integer.parseInt(getCobolDate()));
		l2processorDTO.setTranTime(getVrcmTime());
		l2processorDTO.setEffDate(Integer.parseInt(transactionDate));
		l2processorDTO.setRider(readerDTO.getRider());
		l2processorDTO.setPlnsfx(readerDTO.getPlnsfx());
		l2processorDTO.setStatcode(readerDTO.getStatcode());
		l2processorDTO.setCrrcd(readerDTO.getCrrcd());
		l2processorDTO.setCrtable(readerDTO.getCrtable());
		l2processorDTO.setCoverage(readerDTO.getCoverage());
		l2processorDTO.setLife(readerDTO.getLife());
		l2AnnyProcControlTotalDTO.setControlTotal01(l2AnnyProcControlTotalDTO.getControlTotal01().add(BigDecimal.ONE));
		//initialise1000
		l2processorDTO = l2AnnyProcProcessorUtil.loadTableDetails(l2processorDTO);
		//edit2000		
		l2processorDTO = l2AnnyProcProcessorUtil.validComponent(l2processorDTO);
		//update3000
		l2processorDTO = l2AnnyProcProcessorUtil.processUpdate(l2processorDTO);
		readerDTO.setCovrUpdate(l2processorDTO.getCovrpf());
		l2AnnyProcControlTotalDTO.setControlTotal05(l2AnnyProcControlTotalDTO.getControlTotal05().add(BigDecimal.ONE));
		l2processorDTO.setL2AnnyProcControlTotalDTO(l2AnnyProcControlTotalDTO);
		releaseSoftlock(readerDTO.getChdrcoy(), readerDTO.getChdrnum(), "B675", userName, batchName,
				batchDetail.getBatctrcde());
		LOGGER.info("Anniversary Batch Processing ends for contract : {}", readerDTO.getChdrnum());//IJTI-1498
		return readerDTO;
	}
//	public Covrpf setCovrValues(L2AnnyProcProcessorDTO l2processorDTO,L2AnnyProcReaderDTO readerDTO)
//	{
//		Covrpf covrpf= l2processorDTO.getCovrpf();
//		covrpf.setChdrcoy(readerDTO.getChdrcoy());
//		covrpf.setChdrnum(readerDTO.getChdrnum());
//		covrpf.setLife(readerDTO.getLife());
//		covrpf.setJlife(readerDTO.getJlife());
//		covrpf.setRider(readerDTO.getRider());
//		covrpf.setCoverage(readerDTO.getCoverage());
//		covrpf.setPlanSuffix(readerDTO.getPlnsfx());
//		covrpf.setTranno(readerDTO.getTranno());
//		covrpf.setCurrfrom(readerDTO.getCurrFrom());
//		covrpf.setCurrto(readerDTO.getCurrTo());
//		covrpf.setStatcode(readerDTO.getStatCode());
//		covrpf.setAnbAtCcd(readerDTO.getAnbccd());
//		covrpf.setCrrcd(readerDTO.getCrrcd());
//		covrpf.setPremCurrency(readerDTO.getPrmcur());
//		covrpf.setTermid(readerDTO.getTermId());
//		covrpf.setTrdt(readerDTO.getTrdt());
//		covrpf.setTrtm(readerDTO.getTrtm());
//		covrpf.setRrtdat(readerDTO.getRrtdat());
//		covrpf.setValidflag(readerDTO.getValidFlag());
//		covrpf.setCpidte(readerDTO.getCpidte());
//		covrpf.setBcesage(readerDTO.getBcesage());
//		covrpf.setPcesage(readerDTO.getPcesage());
//		covrpf.setRcesage(readerDTO.getRcesage());
//		covrpf.setBcestrm(readerDTO.getBcestrm());
//		covrpf.setPcestrm(readerDTO.getPcestrm());
//		covrpf.setRiskCessTerm(readerDTO.getRcestrm());
//		covrpf.setCrtable(readerDTO.getCrtable());
//		covrpf.setLiencd(readerDTO.getLiencd());
//		covrpf.setMortcls(readerDTO.getMortcls());
//		covrpf.setZclstate(readerDTO.getZclstate());
//		covrpf.setNxtdte(readerDTO.getNxtdte());
//		covrpf.setPremCessDate(readerDTO.getPcesdte());
//		covrpf.setBcesDte(readerDTO.getBcesdte());
//		covrpf.setRcesDte(readerDTO.getRcesdte());
//		covrpf.setSumins(readerDTO.getSumins());
//		covrpf.setPstatcode(readerDTO.getPstatcode());
//		covrpf.setZstpduty01(readerDTO.getZstpduty01());
//		covrpf.setZbinstprem(readerDTO.getZbinstprem());
//		covrpf.setInstprem(readerDTO.getZinstprem());
//		covrpf.setZlinstprem(readerDTO.getZlinstprem());
//		covrpf.setRrtfrm(readerDTO.getRrtfrm());
//		covrpf.setPrmcur(readerDTO.getPrmcur());
//		covrpf.setPcesdte(readerDTO.getPcesdte());
//		return covrpf;
//	}
	

}
