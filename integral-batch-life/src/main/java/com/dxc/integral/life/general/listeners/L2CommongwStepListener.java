package com.dxc.integral.life.general.listeners;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.beans.factory.annotation.Autowired;

import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.listener.IntegralStepListener;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.L2CommongwReaderDTO;
import com.dxc.integral.life.config.L2commongwJsonMapping;
import com.dxc.integral.life.smarttable.pojo.Tr663;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * StepListener for L2commongw batch step.
 *
 */
public class L2CommongwStepListener extends IntegralStepListener {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2CommongwStepListener.class);

	@Autowired
	private ItempfService itempfService;

	private L2commongwJsonMapping jsonData;
	private Map<String, Integer> formattedStringMap;
	private FlatFileItemWriter<L2CommongwReaderDTO> flatFileWriter;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		super.beforeStep(stepExecution);
		ObjectMapper mapper = new ObjectMapper();
		String companyCode = "  ";
		Tr663 tr663Item = null;
		try {
			tr663Item = itempfService.readSmartTableByTrim(Companies.LIFE, "Tr663", "2", Tr663.class);
		} catch (NullPointerException e) {
			LOGGER.info("Exception in L2CommongwStepListener GENAREAJ is not avaiable for TR663", e);
		}
		if (tr663Item != null) {
			companyCode = tr663Item.getZprofile();
		}
		stepExecution.getExecutionContext().put("companyCode", companyCode);
		try {
			jsonData = mapper.readValue(
					new InputStreamReader(
							this.getClass().getClassLoader().getResourceAsStream("L2commongwJsonMapping.json")),
					L2commongwJsonMapping.class);
		} catch (IOException e) {
			LOGGER.info("Exception in L2CommongwStepListener :: beforeStep", e);
		}
		formattedStringMap = new LinkedHashMap<>();
		for (Map.Entry<String, String> jsonFields : jsonData.getDataFormat().entrySet())
			formattedStringMap.put(jsonFields.getKey(), Integer.parseInt(jsonFields.getValue()));
		flatFileWriter = new FlatFileItemWriter<>();
		LineAggregator<L2CommongwReaderDTO> lineAggregator = createFlatFileLineAggregator();
		setFileResources(flatFileWriter, lineAggregator, stepExecution);
	}

	public FormatterLineAggregator<L2CommongwReaderDTO> createFlatFileLineAggregator() {
		FormatterLineAggregator<L2CommongwReaderDTO> lineAggregator = new FormatterLineAggregator<>();
		FieldExtractor<L2CommongwReaderDTO> fieldExtractor = createFlatFileFieldExtractor();
		lineAggregator.setFieldExtractor(fieldExtractor);
		lineAggregator.setFormat(setDataFormat(formattedStringMap));
		return lineAggregator;
	}

	public FieldExtractor<L2CommongwReaderDTO> createFlatFileFieldExtractor() {
		BeanWrapperFieldExtractor<L2CommongwReaderDTO> extractor = new BeanWrapperFieldExtractor<>();
		extractor.setNames(new ArrayList<>(formattedStringMap.keySet()).toArray(new String[0]));
		return extractor;
	}

	public String setDataFormat(Map<String, Integer> formattedStringMap) {
		StringBuilder formatString = new StringBuilder();
		Field[] fields = null;
		try {
			fields = L2CommongwReaderDTO.class.getDeclaredFields();
		} catch (Exception e) {
			LOGGER.info("Exception in L2CommongwStepListener :: setDataFormat", e);
		}
		if (fields != null && fields.length > 0) {
			for (int i = 0; i < fields.length - 2; i++) {
				if (fields[i].getType().toString().indexOf("java.lang.String") >= 0) {
					formatString.append("%-" + formattedStringMap.get(fields[i].getName()) + "s");
				}
			}
		}
		return formatString.toString();
	}

	public void setFileResources(FlatFileItemWriter<L2CommongwReaderDTO> flatFileWriter,
			LineAggregator<L2CommongwReaderDTO> lineAggregator, StepExecution stepExecution) {
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HHmmss");
		int prcsdTime = Integer.parseInt(timeFormatter.format(System.currentTimeMillis()));
		flatFileWriter.setLineAggregator(lineAggregator);
		stepExecution.getExecutionContext().put("flatFile", flatFileWriter);
		stepExecution.getExecutionContext().put("jsonData", jsonData);
		stepExecution.getExecutionContext().put("prcsdTime", prcsdTime);
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.info(">>>>>>>>>>>>>>> L2CommongwStepListener afterStep() Start >>>>>>>>>>>>>>>");
		if (stepExecution.getReadCount() == stepExecution.getWriteCount() && null != flatFileWriter) {
			LOGGER.debug("L2CommongwStepListener::afterStep::Exit");
		}
		LOGGER.info(">>>>>>>>>>>>>>> L2CommongwStepListener afterStep() End >>>>>>>>>>>>>>>");
		return stepExecution.getExitStatus();
	}
}