import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.Date;

/**
 * This is test class to submit batch through console. This is just a sample
 * class developer can use to test any batch submission.
 * 
 * @author vhukumagrawa
 *
 */
@EnableBatchProcessing
public class TestBatchSubmit {
	
	/** Private Constructor*/
	private TestBatchSubmit(){
		
	}

	/** The logger for BatchJobLauncher */
	private static final Logger LOGGER = LoggerFactory.getLogger(TestBatchSubmit.class);

	public static void main(String[] args) {
		String[] springConfig = { "classpath:com/dxc/integral/jobs/job-l2anendrlx.xml" };
		LOGGER.info("Batch job starts");

		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(springConfig)) {
			JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
			Job job = (Job) context.getBean("l2anendrlx");

			JobParameters param = new JobParametersBuilder().addString("chdrcoy", "2")
					.addString("chdrnumFrom", "00013735").addString("chdrnumTo", "00013735")
					.addString("trandate", "20200101").addString("cntBranch", "10")
					.addString("transactionDate", "01-01-2020").addString("businessDate", "01-01-2020")
					.addString("userName", "ASHARMA228").addString("batchName", "L2ANENDRLX")
					.addDate("submitTime", new Date()).toJobParameters();

			JobExecution execution = jobLauncher.run(job, param);
			LOGGER.info("Exit Status : {}", execution.getStatus());//IJTI-1498
			LOGGER.info("Exit Status : {}", execution.getAllFailureExceptions());//IJTI-1498
			LOGGER.info("Batch job is completed.");

		} catch (Exception ex) {
			LOGGER.error("Error Occurred", ex);//IJTI-1498

		}
	}

}
