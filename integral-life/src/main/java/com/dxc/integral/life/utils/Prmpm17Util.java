package com.dxc.integral.life.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Clexpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.GOTOInterface;
import com.dxc.integral.life.smarttable.pojo.T5585;
import com.dxc.integral.life.smarttable.pojo.T5658;
import com.dxc.integral.life.smarttable.pojo.T5659;
import com.dxc.integral.life.smarttable.pojo.Th609;
import com.dxc.integral.life.smarttable.pojo.Tj698;
import com.dxc.integral.life.smarttable.pojo.Tj699;
@Service("prmpm17Util")
public class Prmpm17Util {
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ClexpfDAO clexpfDAO;
	@Autowired
	private LextpfDAO lextpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readLext210,
		checkT5658Insprm
		
	}
	private BigDecimal wsaaRatesPerMillieTot;
	private BigDecimal wsaaPremiumAdjustTot;
	private int wsaaAgeDifference;
	private int wsaaAge00Adjusted;
	private int wsaaAge01Adjusted;
	private int wsaaAgerateTot;
	private int wsaaAdjustedAge;
	private int wsaaAgeNoLoad;
	private BigDecimal wsaaDiscountAmt;
	private int wsaaSub;
	private int wsaaSub00;
	private int wsaaSub01;
	private BigDecimal wsaaBap;
	private BigDecimal wsaaBip;
	private BigDecimal wsaaModalFactor;
	private BigDecimal wsaaStaffDiscount;
	private String wsaaStaffFlag = "";
	private BigDecimal wsaaRoundNum;
	private BigDecimal wsaaRoundDec;
	private BigDecimal wsaaRound1;
	private BigDecimal wsaaRound10;
	private BigDecimal wsaaRound100;
	private BigDecimal[] wsaaLextOppcs = new BigDecimal[8];
	private T5585 t5585IO = null;
	private T5658 t5658IO = null;
	private T5659 t5659IO = null;
	private Th609 th609IO = null;
	private Tj698 tj698IO = null;
	private Tj699 tj699IO = null;
	private Chdrpf chdrlnb = null;
	private Clexpf clexpf = null;
	private String wsaaBasicPremium = "";
	private List<Lextpf> lextpfList = null;
	private BigDecimal wsaaRebatePcnt;
	private int ix;
	boolean isLoadingAva = false;
	public PremiumDTO processPrempm17(PremiumDTO premiumDTO){
		premiumDTO.setStatuz("****");
		initialize(premiumDTO);
		wsaaBasicPremium = "N";
		if ("****".equals(premiumDTO.getStatuz())) {
			staffDiscount(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO); 
			premiumDTO.setAdjustageamt(wsaaBap);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "B".equals(th609IO.getIndic())) {
			wsaaBap = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100).multiply(wsaaBap)).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			wsaaBap = wsaaRatesPerMillieTot.add(wsaaBap).setScale(2,BigDecimal.ROUND_HALF_UP);
			premiumDTO.setRateadj(wsaaRatesPerMillieTot.setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rebateOnFrequency(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			if(t5658IO.getUnit() == 0) {
				premiumDTO.setStatuz("F110");
			}else {
				wsaaBap = wsaaBap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5658IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
			}
			premiumDTO.setLoadper(wsaaBap.setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			percentageLoadings();
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "I".equals(th609IO.getIndic())) {
			wsaaBip = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100)).multiply(wsaaBip).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			premiumDTO.setCalcPrem(wsaaPremiumAdjustTot.add(premiumDTO.getCalcPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
			premiumDTO.setPremadj(wsaaPremiumAdjustTot.setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		wsaaBasicPremium = "Y";
		if ("****".equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO);
			premiumDTO.setAdjustageamt(premiumDTO.getAdjustageamt().subtract(wsaaBap));
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "B".equals(th609IO.getIndic())) {
			wsaaBap = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100)).multiply(wsaaBap).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			wsaaBap = wsaaRatesPerMillieTot.add(wsaaBap).setScale(2,BigDecimal.ROUND_HALF_UP);
			premiumDTO.setRateadj(wsaaRatesPerMillieTot.setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rebateOnFrequency(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			if(t5658IO.getUnit() == 0) {
				premiumDTO.setStatuz("F110");
			}else {
				wsaaBap = wsaaBap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5658IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
			}
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "I".equals(th609IO.getIndic())) {
		
			wsaaBip = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100)).multiply(wsaaBip).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		premiumDTO.setCalcLoaPrem(premiumDTO.getCalcPrem().subtract(premiumDTO.getCalcBasPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
		if(isLoadingAva == true)
			premiumDTO.setLoadper(premiumDTO.getLoadper().subtract(wsaaRatesPerMillieTot.add(premiumDTO.getAdjustageamt())).setScale(2,BigDecimal.ROUND_HALF_UP));
		return premiumDTO;
	}
	
	protected void rounding(PremiumDTO premiumDTO){
		wsaaRoundNum = wsaaBip;
		wsaaRound100 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(8));
		wsaaRound10 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(9));
		wsaaRound1 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(10));
		wsaaRoundDec = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(11));
		
		if (t5659IO.getRndfact() ==1  || t5659IO.getRndfact() == 0) {
			if (wsaaRoundDec.compareTo(new BigDecimal(0.5)) == -1) {
				wsaaRoundDec = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(1));
				wsaaRoundDec = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 10) {
			if (wsaaRound1.compareTo(new BigDecimal(5)) == -1) {
				wsaaRound1 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(10));
				wsaaRound1 =  BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 100) {
			if (wsaaRound10.compareTo(new BigDecimal(50)) == -1) {
				wsaaRound10 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(100));
				wsaaRound10 = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 1000) {
			if (wsaaRound100.compareTo(new BigDecimal(500)) == -1) {
				wsaaRound100 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(1000));
				wsaaRound100 = BigDecimal.ZERO;
			}
		}
		
		wsaaBip = wsaaRoundNum;
		if (t5658IO.getPremUnit() == 0) {
			premiumDTO.setCalcPrem(wsaaBip);
		}else {
			if ("Y".equals(wsaaBasicPremium)) {
				premiumDTO.setCalcBasPrem(wsaaBip.divide(new BigDecimal(t5658IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}else {
				premiumDTO.setCalcPrem(wsaaBip.divide(new BigDecimal(t5658IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
		}
	}
	protected void instalmentPremium(PremiumDTO premiumDTO){
		wsaaBip = BigDecimal.ZERO;
		wsaaModalFactor = BigDecimal.ZERO;
		String tj699Itemitem = premiumDTO.getCrtable().concat(premiumDTO.getMop());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		tj699IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TJ699", tj699Itemitem,itemFrm,Tj699.class);
		if(tj699IO == null && "***".equals(premiumDTO.getCrtable())){
			premiumDTO.setStatuz("JL01");
			return;
		}else if(tj699IO == null){
			tj699Itemitem = "***".concat(premiumDTO.getMop());
			tj699IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TJ699",tj699Itemitem,itemFrm,Tj699.class);
			if(tj699IO == null){
				premiumDTO.setStatuz("JL01");
				return;
			}
		}
		for (ix = 0; ix < 12; ix++){
			if (premiumDTO.getBillfreq().equals(tj699IO.getFreqcys().get(ix))) {
				wsaaModalFactor = tj699IO.getZlfacts().get(ix);
				break;
			}
		}
		
		if (wsaaModalFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("JL00");
		}else {
			wsaaBip = wsaaBap.multiply(wsaaModalFactor).setScale(4,BigDecimal.ROUND_HALF_UP);
		}
		/*EXIT*/
	}
	protected void percentageLoadings(){
		for(wsaaSub = 0 ; wsaaSub < 8; wsaaSub++)
			if (wsaaLextOppcs[wsaaSub].compareTo(BigDecimal.ZERO) == -1) {
				wsaaBap = wsaaBap.multiply(wsaaLextOppcs[wsaaSub]).divide(new BigDecimal(100));
				isLoadingAva = true;
			}
	}
	protected void rebateOnFrequency(PremiumDTO premiumDTO){
		wsaaRebatePcnt = BigDecimal.ZERO;
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		
		tj698IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TJ698",premiumDTO.getCrtable(),itemFrm,Tj698.class);
		if(tj698IO == null){
			return;
		}
		ix = 0;
		for (ix = 0 ; ix < 12; ix++){
			if (premiumDTO.getBillfreq().equals(tj698IO.getFreqcys().get(ix))) {
				wsaaRebatePcnt = tj698IO.getZlfacts().get(ix);
				break;
			}
		}
		wsaaBap = new BigDecimal(100).subtract(wsaaRebatePcnt).multiply(wsaaBap).divide(new BigDecimal(100));
	}
	protected void volumeDiscountBap(PremiumDTO premiumDTO){
		String t5659Itemitem= t5658IO.getDisccntmeth().concat(premiumDTO.getCurrcode());
		t5659IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5659",t5659Itemitem,premiumDTO.getRatingdate(),T5659.class);
		if(t5659IO == null){
			premiumDTO.setStatuz("F265");
			return;
		}
		for(wsaaSub = 0; wsaaSub < 4 ; wsaaSub ++){
			if(premiumDTO.getSumin().compareTo(t5659IO.getVolbanfrs().get(wsaaSub)) == -1
					|| premiumDTO.getSumin().compareTo(t5659IO.getVolbantos().get(wsaaSub)) ==1){
				continue;
			}else{
				wsaaDiscountAmt = t5659IO.getVolbanles().get(wsaaSub);
				break;
			}
		}
		wsaaBap = wsaaBap.subtract(wsaaDiscountAmt).setScale(2,BigDecimal.ROUND_HALF_UP);
	}
	private void staffDiscount(PremiumDTO premiumDTO){
		List<Chdrpf> chdrpfList = chdrpfDAO.readRecordOfChdrlnb(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum());
		if(chdrpfList == null || chdrpfList.isEmpty()){
			throw new DataNoFoundException("Br613: Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table Chdrpf while processing contract ");
		}else{
			chdrlnb = chdrpfList.get(0);
		}
		if (chdrlnb.getCownnum() == null ||"".equals(chdrlnb.getCownnum())) {
			return;
		}
		clexpf = clexpfDAO.getClexpfRecord(chdrlnb.getCownpfx(), chdrlnb.getCowncoy(), chdrlnb.getCownnum());
		if (null != clexpf && !"Y".equals(clexpf.getRstaflag()) && !"".equals(chdrlnb.getJownnum())){
			clexpf = clexpfDAO.getClexpfRecord(chdrlnb.getCownpfx(), chdrlnb.getCowncoy(), chdrlnb.getJownnum());
		}
		if(null != clexpf && "Y".equals(clexpf.getRstaflag())){
			th609IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TH609",chdrlnb.getCnttype(),Th609.class);
			if(th609IO != null){
				th609IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TH609","***",Th609.class);
			}
			if(th609IO == null){
				wsaaStaffDiscount = BigDecimal.ZERO;
			}else{
				wsaaStaffDiscount = th609IO.getPrcnt();
			}
			
		}
		
		wsaaStaffFlag = clexpf == null ? "" : clexpf.getRstaflag();
	}
	protected void initialize(PremiumDTO premiumDTO){
		wsaaRatesPerMillieTot =  BigDecimal.ZERO;
		wsaaPremiumAdjustTot =  BigDecimal.ZERO;
		wsaaStaffDiscount = BigDecimal.ZERO;
		wsaaBap = BigDecimal.ZERO;
		wsaaBip = BigDecimal.ZERO;
		wsaaAdjustedAge = 0;
		wsaaAgeNoLoad = 0;
		wsaaDiscountAmt =  BigDecimal.ZERO;
		wsaaAgerateTot = 0;
		wsaaSub = 0;
		premiumDTO.setFltmort(BigDecimal.ZERO);
		premiumDTO.setLoadper(BigDecimal.ZERO);
		for (wsaaSub = 0; wsaaSub < 8; wsaaSub ++){
			wsaaLextOppcs[wsaaSub] = BigDecimal.ZERO;
		}
		readT5585(premiumDTO);
		readT5658(premiumDTO); 
	}
	protected void readT5585(PremiumDTO premiumDTO){
		t5585IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5585",premiumDTO.getCnttype(),premiumDTO.getRatingdate(),T5585.class);
		if(t5585IO == null){
			throw new DataNoFoundException("Br613: Item "+premiumDTO.getCrtable()+"not found in Table T5585 while processing contract ");
		}
		wsaaSub = 0;
		wsaaSub00 = 0;
		wsaaSub01 = 0;
		wsaaAge00Adjusted = premiumDTO.getLage();
		wsaaAge01Adjusted = premiumDTO.getJlage();
		if (premiumDTO.getLsex().equals(t5585IO.getSexageadj())) {
			adjustAge(premiumDTO);
		}
		if (premiumDTO.getJlsex().equals(t5585IO.getSexageadj())) {
			adjustAge01195(premiumDTO);
		}
		wsaaAgeDifference = wsaaAge00Adjusted - wsaaAge01Adjusted;
		wsaaSub = 0;
		ageDifferance(premiumDTO);
	}
	protected void adjustAge(PremiumDTO premiumDTO){
		if (wsaaSub00 > 9) {
			premiumDTO.setStatuz("H043");
			return ;
		}
		if (premiumDTO.getLage() <= t5585IO.getAgelimits().get(wsaaSub00)) {
			wsaaAge00Adjusted = premiumDTO.getLage() + t5585IO.getAgeadjs().get(wsaaSub00);
		}
		else {
			wsaaSub00++;
			adjustAge(premiumDTO);
		}
	}
	protected void adjustAge01195(PremiumDTO premiumDTO){
		if (wsaaSub01 > 9) {
			premiumDTO.setStatuz("H043");
			return ;
		}
		if (premiumDTO.getJlage() <= t5585IO.getAgelimits().get(wsaaSub01)) {
			wsaaAge01Adjusted = premiumDTO.getJlage() + t5585IO.getAgeadjs().get(wsaaSub01);
		}
		else {
			wsaaSub01++;
			adjustAge(premiumDTO);
		}
	}
	protected void ageDifferance(PremiumDTO premiumDTO){
		if (wsaaSub >18) {
			premiumDTO.setStatuz("F262");
			return ;
		}
		if (wsaaAgeDifference >  t5585IO.getAgedifs().get(wsaaSub)) {
			ageDifferance(premiumDTO);
		}
		if ("H".equals(t5585IO.getHghlowage())) {
			if (wsaaAge00Adjusted > wsaaAge01Adjusted) {
				wsaaAdjustedAge = t5585IO.getAddages().get(wsaaSub) + wsaaAge00Adjusted;
			}
			else {
				wsaaAdjustedAge = t5585IO.getAddages().get(wsaaSub) + wsaaAge01Adjusted;
			}
		}
		if ("L".equals(t5585IO.getHghlowage())) {
			if (wsaaAge00Adjusted < wsaaAge01Adjusted) {
				wsaaAdjustedAge = t5585IO.getAddages().get(wsaaSub) + wsaaAge00Adjusted;
			}
			else {
				wsaaAdjustedAge = t5585IO.getAddages().get(wsaaSub) + wsaaAge01Adjusted;
			}
		}
	}
	protected void basicAnnualPremium(PremiumDTO premiumDTO){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey(premiumDTO);
				case checkT5658Insprm: 
					checkT5658Insprm(premiumDTO);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void setupLextKey(PremiumDTO premiumDTO){
		if( "Y".equals(wsaaBasicPremium)) {
			wsaaAdjustedAge = wsaaAgeNoLoad;
			goTo(GotoLabel.checkT5658Insprm);
		}
		else {
			wsaaAgeNoLoad = wsaaAdjustedAge;
		}
		lextpfList = lextpfDAO.getLextpfListRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(),
				premiumDTO.getCovrCoverage(), premiumDTO.getCovrRider());
		wsaaSub = 0;
		if (lextpfList == null || lextpfList.isEmpty()) {
			goTo(GotoLabel.checkT5658Insprm);
		}
		for(Lextpf lextIO : lextpfList){
			if ("2".equals(premiumDTO.getReasind()) && "2".equals(premiumDTO.getReasind())) {
				goTo(GotoLabel.checkT5658Insprm);
			}else {
				if (!"2".equals(premiumDTO.getReasind()) && "2".equals(lextIO.getReasind())) {
					goTo(GotoLabel.checkT5658Insprm);
				}
			}
			if (lextIO.getExtcd() <= premiumDTO.getRatingdate()) {
				continue;
			}
			wsaaLextOppcs[wsaaSub] = lextIO.getOppc();
			wsaaRatesPerMillieTot.add(new BigDecimal(lextIO.getInsprm()));
			wsaaPremiumAdjustTot.add(lextIO.getPremadj());
			wsaaAgerateTot +=lextIO.getAgerate();
			wsaaSub++;
		}
	}
	protected void checkT5658Insprm(PremiumDTO premiumDTO){
		wsaaAdjustedAge +=wsaaAgerateTot;
		if (wsaaAdjustedAge < 0|| wsaaAdjustedAge > 110) {
			premiumDTO.setStatuz("E107");
			return ;
		}
		if (wsaaAdjustedAge == 0) {
			if (t5658IO.getInsprem() == 0) {
				premiumDTO.setStatuz("E107");
			}else {
				wsaaBap = new BigDecimal(t5658IO.getInsprem());
			}
			return ;
		}
		
		if (wsaaAdjustedAge >= 100 && wsaaAdjustedAge <= 110) {
			if (t5658IO.getInstprs().get(wsaaAdjustedAge - 99) ==0) {
				premiumDTO.setStatuz("E107");
			}else {
				wsaaBap = new BigDecimal(t5658IO.getInstprs().get(wsaaAdjustedAge - 99)).setScale(2,BigDecimal.ROUND_HALF_UP);
			}
		}else {
			if (t5658IO.getInsprms().get(wsaaAdjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}else {
				if ("Y".equals(wsaaBasicPremium)) {
					wsaaBap = new BigDecimal(t5658IO.getInsprms().get(wsaaAgeNoLoad));
				}
				else {
					wsaaBap = new BigDecimal(t5658IO.getInsprms().get(wsaaAdjustedAge));
				}
			}
		}
	}
	protected void readT5658(PremiumDTO premiumDTO){
		String freqFactor = premiumDTO.getDuration() < 10 ? String.format("%02d", premiumDTO.getDuration()) : String.valueOf( premiumDTO.getDuration()); ;
		String t5658Itemitem = premiumDTO.getCrtable().concat(freqFactor).concat(premiumDTO.getMortcls());
		t5658IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5658",t5658Itemitem,premiumDTO.getRatingdate(),T5658.class);
		if(t5658IO == null){
			premiumDTO.setStatuz("F262");
		}
	}
	public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}
	private String formatRoundNum(BigDecimal obj) {
		DecimalFormat df=new DecimalFormat("0000000000000.00");
		String str2=df.format(obj);
		return str2;
	}
}
