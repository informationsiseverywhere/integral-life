package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.List;

/**
 * DTO for Licacmv utility.
 * 
 * @author vhukumagrawa
 *
 */
public class LifacmvDTO {

	private String batckey;
	/** The batccoy */
	private String batccoy;
	/** The batcbrn */
	private String batcbrn;
	/** The batcactyr */
	private int batcactyr;
	/** The batcactmn */
	private int batcactmn;
	/** The batctrcde */
	private String batctrcde;
	/** The batcbatch */
	private String batcbatch;
	/** The rdocnum */
	private String rdocnum;
	/** The tranno */
	private int tranno;
	/** The jrnseq */
	private int jrnseq;
	/** The rldgcoy */
	private String rldgcoy;
	/** The sacscode */
	private String sacscode;
	/** The rldgacct */
	private String rldgacct;
	/** The origcurr */
	private String origcurr;
	/** The sacstyp */
	private String sacstyp;
	/** The origamt */
	private BigDecimal origamt;
	/** The tranref */
	private String tranref;
	/** The trandesc */
	private String trandesc;
	/** The crate */
	private BigDecimal crate;
	/** The acctamt */
	private BigDecimal acctamt;
	/** The genlcoy */
	private String genlcoy;
	/** The genlcur */
	private String genlcur;
	/** The glcode */
	private String glcode;
	/** The glsign */
	private String glsign;
	/** The contot */
	private int contot;
	/** The postyear */
	private String postyear;
	/** The postmonth */
	private String postmonth;
	/** The effdate */
	private int effdate;
	/** The rcamt */
	private BigDecimal rcamt;
	/** The frcdate */
	private int frcdate;
	/** The transactionDate */
	private int transactionDate;
	/** The transactionTime */
	private int transactionTime;
	/** The user */
	private int user;
	/** The termid */
	private String termid;
	/** The suprflag */
	private String suprflag;
	/** The substituteCodes */
	private List<String> substituteCodes;
	/** The prefix */
	private String prefix;
	/** The ind */
	private String ind;
	/** The jobname */
	private String jobname;
	/** The usrprofile */
	private String usrprofile;

	/**
	 * @return the batccoy
	 */
	public String getBatccoy() {
		return batccoy;
	}

	/**
	 * @param batccoy
	 *            the batccoy to set
	 */
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	/**
	 * @return the batcbrn
	 */
	public String getBatcbrn() {
		return batcbrn;
	}

	/**
	 * @param batcbrn
	 *            the batcbrn to set
	 */
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	/**
	 * @return the batcactyr
	 */
	public int getBatcactyr() {
		return batcactyr;
	}

	/**
	 * @param batcactyr
	 *            the batcactyr to set
	 */
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}

	/**
	 * @return the batcactmn
	 */
	public int getBatcactmn() {
		return batcactmn;
	}

	/**
	 * @param batcactmn
	 *            the batcactmn to set
	 */
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}

	/**
	 * @return the batctrcde
	 */
	public String getBatctrcde() {
		return batctrcde;
	}

	/**
	 * @param batctrcde
	 *            the batctrcde to set
	 */
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	/**
	 * @return the batcbatch
	 */
	public String getBatcbatch() {
		return batcbatch;
	}

	/**
	 * @param batcbatch
	 *            the batcbatch to set
	 */
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	/**
	 * @return the rdocnum
	 */
	public String getRdocnum() {
		return rdocnum;
	}

	/**
	 * @param rdocnum
	 *            the rdocnum to set
	 */
	public void setRdocnum(String rdocnum) {
		this.rdocnum = rdocnum;
	}

	/**
	 * @return the tranno
	 */
	public int getTranno() {
		return tranno;
	}

	/**
	 * @param tranno
	 *            the tranno to set
	 */
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	/**
	 * @return the jrnseq
	 */
	public int getJrnseq() {
		return jrnseq;
	}

	/**
	 * @param jrnseq
	 *            the jrnseq to set
	 */
	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}

	/**
	 * @return the rldgcoy
	 */
	public String getRldgcoy() {
		return rldgcoy;
	}

	/**
	 * @param rldgcoy
	 *            the rldgcoy to set
	 */
	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}

	/**
	 * @return the sacscode
	 */
	public String getSacscode() {
		return sacscode;
	}

	/**
	 * @param sacscode
	 *            the sacscode to set
	 */
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}

	/**
	 * @return the rldgacct
	 */
	public String getRldgacct() {
		return rldgacct;
	}

	/**
	 * @param rldgacct
	 *            the rldgacct to set
	 */
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}

	/**
	 * @return the origcurr
	 */
	public String getOrigcurr() {
		return origcurr;
	}

	/**
	 * @param origcurr
	 *            the origcurr to set
	 */
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}

	/**
	 * @return the sacstyp
	 */
	public String getSacstyp() {
		return sacstyp;
	}

	/**
	 * @param sacstyp
	 *            the sacstyp to set
	 */
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}

	/**
	 * @return the origamt
	 */
	public BigDecimal getOrigamt() {
		return origamt;
	}

	/**
	 * @param origamt
	 *            the origamt to set
	 */
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}

	/**
	 * @return the tranref
	 */
	public String getTranref() {
		return tranref;
	}

	/**
	 * @param tranref
	 *            the tranref to set
	 */
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}

	/**
	 * @return the trandesc
	 */
	public String getTrandesc() {
		return trandesc;
	}

	/**
	 * @param trandesc
	 *            the trandesc to set
	 */
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}

	/**
	 * @return the crate
	 */
	public BigDecimal getCrate() {
		return crate;
	}

	/**
	 * @param crate
	 *            the crate to set
	 */
	public void setCrate(BigDecimal crate) {
		this.crate = crate;
	}

	/**
	 * @return the acctamt
	 */
	public BigDecimal getAcctamt() {
		return acctamt;
	}

	/**
	 * @param acctamt
	 *            the acctamt to set
	 */
	public void setAcctamt(BigDecimal acctamt) {
		this.acctamt = acctamt;
	}

	/**
	 * @return the genlcoy
	 */
	public String getGenlcoy() {
		return genlcoy;
	}

	/**
	 * @param genlcoy
	 *            the genlcoy to set
	 */
	public void setGenlcoy(String genlcoy) {
		this.genlcoy = genlcoy;
	}

	/**
	 * @return the genlcur
	 */
	public String getGenlcur() {
		return genlcur;
	}

	/**
	 * @param genlcur
	 *            the genlcur to set
	 */
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}

	/**
	 * @return the glcode
	 */
	public String getGlcode() {
		return glcode;
	}

	/**
	 * @param glcode
	 *            the glcode to set
	 */
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}

	/**
	 * @return the glsign
	 */
	public String getGlsign() {
		return glsign;
	}

	/**
	 * @param glsign
	 *            the glsign to set
	 */
	public void setGlsign(String glsign) {
		this.glsign = glsign;
	}

	/**
	 * @return the contot
	 */
	public int getContot() {
		return contot;
	}

	/**
	 * @param contot
	 *            the contot to set
	 */
	public void setContot(int contot) {
		this.contot = contot;
	}

	/**
	 * @return the postyear
	 */
	public String getPostyear() {
		return postyear;
	}

	/**
	 * @param postyear
	 *            the postyear to set
	 */
	public void setPostyear(String postyear) {
		this.postyear = postyear;
	}

	/**
	 * @return the postmonth
	 */
	public String getPostmonth() {
		return postmonth;
	}

	/**
	 * @param postmonth
	 *            the postmonth to set
	 */
	public void setPostmonth(String postmonth) {
		this.postmonth = postmonth;
	}

	/**
	 * @return the effdate
	 */
	public int getEffdate() {
		return effdate;
	}

	/**
	 * @param effdate
	 *            the effdate to set
	 */
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	/**
	 * @return the rcamt
	 */
	public BigDecimal getRcamt() {
		return rcamt;
	}

	/**
	 * @param rcamt
	 *            the rcamt to set
	 */
	public void setRcamt(BigDecimal rcamt) {
		this.rcamt = rcamt;
	}

	/**
	 * @return the frcdate
	 */
	public int getFrcdate() {
		return frcdate;
	}

	/**
	 * @param frcdate
	 *            the frcdate to set
	 */
	public void setFrcdate(int frcdate) {
		this.frcdate = frcdate;
	}

	/**
	 * @return the transactionDate
	 */
	public int getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate
	 *            the transactionDate to set
	 */
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the transactionTime
	 */
	public int getTransactionTime() {
		return transactionTime;
	}

	/**
	 * @param transactionTime
	 *            the transactionTime to set
	 */
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}

	/**
	 * @return the user
	 */
	public int getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(int user) {
		this.user = user;
	}

	/**
	 * @return the termid
	 */
	public String getTermid() {
		return termid;
	}

	/**
	 * @param termid
	 *            the termid to set
	 */
	public void setTermid(String termid) {
		this.termid = termid;
	}

	/**
	 * @return the suprflag
	 */
	public String getSuprflag() {
		return suprflag;
	}

	/**
	 * @param suprflag
	 *            the suprflag to set
	 */
	public void setSuprflag(String suprflag) {
		this.suprflag = suprflag;
	}

	/**
	 * @return the substituteCodes
	 */
	public List<String> getSubstituteCodes() {
		return substituteCodes;
	}

	/**
	 * @param substituteCodes
	 *            the substituteCodes to set
	 */
	public void setSubstituteCodes(List<String> substituteCodes) {
		this.substituteCodes = substituteCodes;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the ind
	 */
	public String getInd() {
		return ind;
	}

	/**
	 * @param ind
	 *            the ind to set
	 */
	public void setInd(String ind) {
		this.ind = ind;
	}

	/**
	 * @return the jobname
	 */
	public String getJobname() {
		return jobname;
	}

	/**
	 * @param jobname
	 *            the jobname to set
	 */
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	/**
	 * @return the usrprofile
	 */
	public String getUsrprofile() {
		return usrprofile;
	}

	/**
	 * @param usrprofile
	 *            the usrprofile to set
	 */
	public void setUsrprofile(String usrprofile) {
		this.usrprofile = usrprofile;
	}

	public String getBatckey() {
		return batckey;
	}

	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}

}
