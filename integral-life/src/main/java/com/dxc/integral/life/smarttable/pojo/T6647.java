package com.dxc.integral.life.smarttable.pojo;

public class T6647 {
  	public String aloind;
  	public String bidoffer;
  	public String dealin;
  	public String efdcode;
  	public String enhall;
  	public int monthsDebt;
  	public int monthsError;
  	public int monthsLapse;
  	public int monthsNegUnits;
  	public int procSeqNo;
  	public String unitStatMethod;
  	public String swmeth;
  	public String moniesDate;
	public String getAloind() {
		return aloind;
	}
	public void setAloind(String aloind) {
		this.aloind = aloind;
	}
	public String getBidoffer() {
		return bidoffer;
	}
	public void setBidoffer(String bidoffer) {
		this.bidoffer = bidoffer;
	}
	public String getDealin() {
		return dealin;
	}
	public void setDealin(String dealin) {
		this.dealin = dealin;
	}
	public String getEfdcode() {
		return efdcode;
	}
	public void setEfdcode(String efdcode) {
		this.efdcode = efdcode;
	}
	public String getEnhall() {
		return enhall;
	}
	public void setEnhall(String enhall) {
		this.enhall = enhall;
	}
	public int getMonthsDebt() {
		return monthsDebt;
	}
	public void setMonthsDebt(int monthsDebt) {
		this.monthsDebt = monthsDebt;
	}
	public int getMonthsError() {
		return monthsError;
	}
	public void setMonthsError(int monthsError) {
		this.monthsError = monthsError;
	}
	public int getMonthsLapse() {
		return monthsLapse;
	}
	public void setMonthsLapse(int monthsLapse) {
		this.monthsLapse = monthsLapse;
	}
	public int getMonthsNegUnits() {
		return monthsNegUnits;
	}
	public void setMonthsNegUnits(int monthsNegUnits) {
		this.monthsNegUnits = monthsNegUnits;
	}
	public int getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(int procSeqNo) {
		this.procSeqNo = procSeqNo;
	}
	public String getUnitStatMethod() {
		return unitStatMethod;
	}
	public void setUnitStatMethod(String unitStatMethod) {
		this.unitStatMethod = unitStatMethod;
	}
	public String getSwmeth() {
		return swmeth;
	}
	public void setSwmeth(String swmeth) {
		this.swmeth = swmeth;
	}
	public String getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(String moniesDate) {
		this.moniesDate = moniesDate;
	}

  	
}
