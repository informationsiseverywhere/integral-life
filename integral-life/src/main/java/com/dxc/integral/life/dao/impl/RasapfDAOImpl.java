package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RasapfDAO;
import com.dxc.integral.life.dao.model.Rasapf;

@Repository("rasapfDAO")
@Lazy
public class RasapfDAOImpl extends BaseDAOImpl implements RasapfDAO {
    @Override
	public Rasapf getRasaRecord(String rascoy, String rasnum) {
		String sql = "SELECT * FROM RACAPF WHERE RASCOY=? AND RASNUM=? ORDER BY RASCOY ASC, RASNUM ASC, UNIQUE_NUMBER DESC";
		return jdbcTemplate.queryForObject(sql, new Object[] {rascoy, rasnum},
					new BeanPropertyRowMapper<Rasapf>(Rasapf.class));
	}
}
