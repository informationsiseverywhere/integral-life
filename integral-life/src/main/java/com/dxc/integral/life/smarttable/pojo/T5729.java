package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class T5729 {
  public List<Integer> durationa;
  public List<Integer> durationb;
  public List<Integer> durationc;
  public List<Integer> durationd;
  public List<Integer> duratione;
  public List<Integer> durationf;
  public List<String> frqcy;
  public List<Integer> overdueMina;
  public List<Integer> overdueMinb;
  public List<Integer> overdueMinc;	
  public List<Integer> overdueMind;		
  public List<Integer> overdueMine;
  public List<Integer> overdueMinf;
  public List<Integer> targetMaxa;	
  public List<Integer> targetMaxb;	
  public List<Integer> targetMaxc; 
  public List<Integer> targetMaxd; 
  public List<Integer> targetMaxe; 
  public List<Integer> targetMaxf; 
  public List<Integer> targetMina; 
  public List<Integer> targetMinb;
  public List<Integer> targetMinc;
  public List<Integer> targetMind;
  public List<Integer> targetMine;
  public List<Integer> targetMinf;
  
public List<Integer> getDurationa() {
	return durationa;
}
public void setDurationa(List<Integer> durationa) {
	this.durationa = durationa;
}
public List<Integer> getDurationb() {
	return durationb;
}
public void setDurationb(List<Integer> durationb) {
	this.durationb = durationb;
}
public List<Integer> getDurationc() {
	return durationc;
}
public void setDurationc(List<Integer> durationc) {
	this.durationc = durationc;
}
public List<Integer> getDurationd() {
	return durationd;
}
public void setDurationd(List<Integer> durationd) {
	this.durationd = durationd;
}
public List<Integer> getDuratione() {
	return duratione;
}
public void setDuratione(List<Integer> duratione) {
	this.duratione = duratione;
}
public List<Integer> getDurationf() {
	return durationf;
}
public void setDurationf(List<Integer> durationf) {
	this.durationf = durationf;
}
public List<String> getFrqcy() {
	return frqcy;
}
public void setFrqcy(List<String> frqcy) {
	this.frqcy = frqcy;
}
public List<Integer> getOverdueMina() {
	return overdueMina;
}
public void setOverdueMina(List<Integer> overdueMina) {
	this.overdueMina = overdueMina;
}
public List<Integer> getOverdueMinb() {
	return overdueMinb;
}
public void setOverdueMinb(List<Integer> overdueMinb) {
	this.overdueMinb = overdueMinb;
}
public List<Integer> getOverdueMinc() {
	return overdueMinc;
}
public void setOverdueMinc(List<Integer> overdueMinc) {
	this.overdueMinc = overdueMinc;
}
public List<Integer> getOverdueMind() {
	return overdueMind;
}
public void setOverdueMind(List<Integer> overdueMind) {
	this.overdueMind = overdueMind;
}
public List<Integer> getOverdueMine() {
	return overdueMine;
}
public void setOverdueMine(List<Integer> overdueMine) {
	this.overdueMine = overdueMine;
}
public List<Integer> getOverdueMinf() {
	return overdueMinf;
}
public void setOverdueMinf(List<Integer> overdueMinf) {
	this.overdueMinf = overdueMinf;
}
public List<Integer> getTargetMaxa() {
	return targetMaxa;
}
public void setTargetMaxa(List<Integer> targetMaxa) {
	this.targetMaxa = targetMaxa;
}
public List<Integer> getTargetMaxb() {
	return targetMaxb;
}
public void setTargetMaxb(List<Integer> targetMaxb) {
	this.targetMaxb = targetMaxb;
}
public List<Integer> getTargetMaxc() {
	return targetMaxc;
}
public void setTargetMaxc(List<Integer> targetMaxc) {
	this.targetMaxc = targetMaxc;
}
public List<Integer> getTargetMaxd() {
	return targetMaxd;
}
public void setTargetMaxd(List<Integer> targetMaxd) {
	this.targetMaxd = targetMaxd;
}
public List<Integer> getTargetMaxe() {
	return targetMaxe;
}
public void setTargetMaxe(List<Integer> targetMaxe) {
	this.targetMaxe = targetMaxe;
}
public List<Integer> getTargetMaxf() {
	return targetMaxf;
}
public void setTargetMaxf(List<Integer> targetMaxf) {
	this.targetMaxf = targetMaxf;
}
public List<Integer> getTargetMina() {
	return targetMina;
}
public void setTargetMina(List<Integer> targetMina) {
	this.targetMina = targetMina;
}
public List<Integer> getTargetMinb() {
	return targetMinb;
}
public void setTargetMinb(List<Integer> targetMinb) {
	this.targetMinb = targetMinb;
}
public List<Integer> getTargetMinc() {
	return targetMinc;
}
public void setTargetMinc(List<Integer> targetMinc) {
	this.targetMinc = targetMinc;
}
public List<Integer> getTargetMind() {
	return targetMind;
}
public void setTargetMind(List<Integer> targetMind) {
	this.targetMind = targetMind;
}
public List<Integer> getTargetMine() {
	return targetMine;
}
public void setTargetMine(List<Integer> targetMine) {
	this.targetMine = targetMine;
}
public List<Integer> getTargetMinf() {
	return targetMinf;
}
public void setTargetMinf(List<Integer> targetMinf) {
	this.targetMinf = targetMinf;
}
  
  
}
