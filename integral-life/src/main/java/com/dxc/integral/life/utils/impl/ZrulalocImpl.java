package com.dxc.integral.life.utils.impl;


import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.RnlallDTO;
import com.dxc.integral.life.beans.TxcalcDTO;
import com.dxc.integral.life.beans.UntallDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.HitrpfDAO;
import com.dxc.integral.life.dao.IncipfDAO;
import com.dxc.integral.life.dao.TaxdpfDAO;
import com.dxc.integral.life.dao.UlnkpfDAO;
import com.dxc.integral.life.dao.UtrnpfDAO;
import com.dxc.integral.life.dao.ZrrapfDAO;
import com.dxc.integral.life.dao.ZrstpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hitrpf;
import com.dxc.integral.life.dao.model.Incipf;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.dao.model.Ulnkpf;
import com.dxc.integral.life.dao.model.Utrnpf;
import com.dxc.integral.life.dao.model.Zrrapf;
import com.dxc.integral.life.dao.model.Zrstpf;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.GOTOInterface;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.services.RulGenCollectService;
import com.dxc.integral.life.smarttable.pojo.T5515;
import com.dxc.integral.life.smarttable.pojo.T5519;
import com.dxc.integral.life.smarttable.pojo.T5539;
import com.dxc.integral.life.smarttable.pojo.T5540;
import com.dxc.integral.life.smarttable.pojo.T5545;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.T6646;
import com.dxc.integral.life.smarttable.pojo.T6647;
import com.dxc.integral.life.smarttable.pojo.Tr52d;
import com.dxc.integral.life.smarttable.pojo.Tr52e;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Servtax;
import com.dxc.integral.life.utils.VPMScaller;
import com.dxc.integral.life.utils.Zrulaloc;
@Service("zrulaloc")
@Lazy
public class ZrulalocImpl implements Zrulaloc{
	
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private RulGenCollectService rulGenCollectService;
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private ZrrapfDAO zrrapfDAO;
	@Autowired
	private IncipfDAO incipfDAO;
	@Autowired
	private UlnkpfDAO ulnkpfDAO;
	@Autowired
	private HitrpfDAO hitrpfDAO;
	@Autowired
	private UtrnpfDAO utrnpfDAO;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private TaxdpfDAO taxdpfDAO;
	@Autowired
	private ZrstpfDAO zrstpfDAO;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private Servtax servtax;
	@Autowired
	private Lifacmv lifacmv;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private VPMScaller vpmscaller;
	@Autowired
	private Environment env;
	
	private RnlallDTO rnlallDTO;
	private int wsaaRiderAlpha;
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();
	private final String wsaaSubr = "ZRULALOC";
	private T6647 t6647IO;
	private T5540 t5540IO;
	private T5645 t5645IO;
	private T5688 t5688IO;
	private T5519 t5519IO;
	private T5515 t5515IO;
	private Itempf itempf;
	private Zrrapf zrrapf;
	private Incipf incipf;
	private Ulnkpf ulnkpf;
	private Hitrpf hitrpf;
	private Utrnpf utrnpf;
	private Chdrpf chdrpf;
	private Taxdpf taxdpf;
	private Zrstpf zrstpf;
	private ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();

	private String wsaaFlexPrem = "N";
	private BigDecimal wsaaInciCurrPrem;
	private BigDecimal wsaaAmountOut;
	private int wsaaFact;
	private final BigDecimal wsaahurn = new BigDecimal(100);
	private boolean dberror = false;
	private int wsaaMoniesDate;
	private String wsaaTr52eKey;
	private BigDecimal[] wsaaTax = new BigDecimal[3];
	private BigDecimal wsaaTotCd;
	private int wsaaSeqno;
	private boolean vpmsFlag;	
	
	@Override
	public RnlallDTO processZrulaloc(RnlallDTO rnlallDTO) throws Exception{
		this.rnlallDTO = rnlallDTO;
		mainline100();
		if (this.dberror) {
			rnlallDTO.setStatuz("BOMB");
		}
//		initialisation();
		a200DeleteZrra();
//		getT5539();
//		getT6646();
//		systemError570();
		
		return rnlallDTO;
	}
	
	protected void initialisation() throws IOException{
		paraInit();
		readT6647();
		setSeqNo();
		readT5540();
		readT5645();
		readT5688();
		readT5519();
		readT5729();
		readZrra();
	
	}
	
	
	protected void mainline100() throws Exception{
		
		// init
		initialisation();
		// readIncirnl110
		List<Incipf> incipfList = incipfDAO.getIncipfRecords(rnlallDTO.getCompany(), rnlallDTO.getChdrnum(),
				rnlallDTO.getLife(), rnlallDTO.getCoverage(), rnlallDTO.getRider(), rnlallDTO.getPlanSuffix());
		if (incipfList == null || incipfList.size() <= 0) {
			a200DeleteZrra();
			return;
		} else {
			for (Incipf incipf1 : incipfList) {
				incipf = incipf1;
				wsaaAmountHoldersInner.wsaaNonInvestPremTot = BigDecimal.ZERO;
				wsaaAmountHoldersInner.wsaaTotUlSplit = BigDecimal.ZERO;
				wsaaAmountHoldersInner.wsaaTotIbSplit = BigDecimal.ZERO;
				wsaaAmountHoldersInner.wsaaAllocInitTot = BigDecimal.ZERO;
				wsaaAmountHoldersInner.wsaaAllocTot = BigDecimal.ZERO;
				wsaaAmountHoldersInner.wsaaAllocAccumTot = BigDecimal.ZERO;

				if (rnlallDTO.getTotrecd().compareTo(BigDecimal.ZERO) > 0) {
					wsaaInciCurrPrem = incipf.getCurrPrem();
					wsaaAmountHoldersInner.wsaaInciPrem = rnlallDTO.getCovrInstprem() == null ? BigDecimal.ZERO
							: rnlallDTO.getCovrInstprem();
				} else {
					wsaaAmountHoldersInner.wsaaInciPrem = incipf.getCurrPrem() == null ? BigDecimal.ZERO
							: incipf.getCurrPrem();
				}

				/* MOVE INCIRNL-CURR-PREM TO WSAA-INCI-PREM. <D9604> */
				wsaaAmountHoldersInner.wsaaIndex = 1;
				while (!((wsaaAmountHoldersInner.wsaaIndex > 7)
						|| (wsaaAmountHoldersInner.wsaaInciPrem.compareTo(BigDecimal.ZERO) == 0))) {
					premiumAllocation1000();
				}
				if (incipfDAO.updateIncipfPremcurr(incipf) <= 0) {
					dberror = true;
					return;
				}
				if (t6647IO.getEnhall().trim().equals("")) {
//					goTo(GotoLabel.fundAllocation180);
					fundAllocation();
					return;
				}

				if ("Y".equals(wsaaFlexPrem)) {
//					goTo(GotoLabel.fundAllocation180);
					fundAllocation();
					return;
				}
				/* Read T5545 to get the enhancement percentage. */
				TableItem<T5545> t5545 = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T5545",
						t6647IO.getEnhall().concat(rnlallDTO.getCntcurr()), CommonConstants.IT_ITEMPFX, T5545.class);
				T5545 t5545IO = null;
				if (t5545 != null) {
					t5545IO = t5545.getItemDetail();
				} else {
//					goTo(GotoLabel.fundAllocation180);
					fundAllocation();
					return;
				}

				/* Look for the correct percentage. */
				wsaaAmountHoldersInner.wsaaEnhancePerc = BigDecimal.ZERO;
				wsaaAmountHoldersInner.wsaaIndex2 = 1;
				/* Should use the total coverage premium instead of the current */
				/* INCI premium to decide if enhanced allocation is needed. */
				/* Store the current INCI premium away and put the total coverage */
				/* premium into it instead so that we dont't have to amend the code */
				/* in 3000-. */
				/* Make sure RNLA-COVR-INSTPREM is numeric first, just in case of */
				/* the calling program has not updated the field correctly. */

//				MIBT-279 STARTS
				if (rnlallDTO.getCovrInstprem().compareTo(rnlallDTO.getTotrecd()) == 0
						&& rnlallDTO.getTotrecd().compareTo(BigDecimal.ZERO) != 0) {
					incipf.setCurrPrem(wsaaInciCurrPrem);
				}
//				MIBT-279 Ends		
				while (!((wsaaAmountHoldersInner.wsaaIndex2 > 6)
						|| wsaaAmountHoldersInner.wsaaEnhancePerc.compareTo(BigDecimal.ZERO) != 0)) {
					// matchEnhanceBasis3000();
					// IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with
					// latest PA compatible START
					// Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation
					// and Enhanced Unit Allocation

					// VPMS Fix made for VEUAL calculation rule - start
					chdrpf = chdrpfDAO.getLPContractWithoutValidflag(rnlallDTO.getCompany(), rnlallDTO.getChdrnum());

					if (chdrpf == null) {
						this.dberror = true;
						return;
					}

					if (!checkVPMSFlag()) {
						List<BigDecimal> list = rulGenCollectService.matchEnhanceBasis(t5545IO,
								wsaaAmountHoldersInner.wsaaIndex2, wsaaAmountHoldersInner.wsaaEnhancePerc,
								incipf.getCurrPrem(), rnlallDTO.getBillfreq());
						wsaaAmountHoldersInner.wsaaIndex2 = list.get(0).intValue();
						wsaaAmountHoldersInner.wsaaEnhancePerc = list.get(1);
					} else {
						UntallDTO untallrec = new UntallDTO();

						untallrec.setUntaCompany(rnlallDTO.getCompany());
						untallrec.setUntaBatctrcde(rnlallDTO.getBatctrcde());
						untallrec.setUntaEnhall(t6647IO.getEnhall());
						untallrec.setUntaCntcurr(rnlallDTO.getCntcurr());

						untallrec.setUntaCalcType("S");
						untallrec.setUntaEffdate(rnlallDTO.getEffdate());
						untallrec.setUntaInstprem(incipf.getCurrPrem());
						untallrec.setUntaAllocInit(wsaaAmountHoldersInner.wsaaAllocInitTot);
						untallrec.setUntaAllocAccum(wsaaAmountHoldersInner.wsaaAllocAccumTot);
						untallrec.setUntaBillfreq(rnlallDTO.getBillfreq());

						// callProgram("VEUAL", untallrec, chdrpf);
						VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
						vpmsinfoDTO.setCoy(rnlallDTO.getCompany());
						vpmsinfoDTO.setTransDate(String.valueOf(rnlallDTO.getEffdate()));
						vpmsinfoDTO.setModelName("VEUAL");

						vpmscaller.callVEUAL(vpmsinfoDTO, untallrec);

						if (!"****".equals(untallrec.getUntaStatuz())) {
							fatalError600("Zrulaloc: Exception in untallrec:" + untallrec.getUntaStatuz());
						}

						wsaaAmountHoldersInner.wsaaEnhancePerc = untallrec.getUntaEnhancePerc();
						wsaaAmountHoldersInner.wsaaEnhanceAlloc = untallrec.getUntaEnhanceAlloc();
						wsaaAmountHoldersInner.wsaaIndex2++;

					}
				}

				/* Move the original amount back to current INCI premium. */
				incipf.setCurrPrem(wsaaInciCurrPrem);
				if (wsaaAmountHoldersInner.wsaaEnhancePerc.compareTo(BigDecimal.ZERO) == 0) {
//				goTo(GotoLabel.fundAllocation180);
					fundAllocation();
					return;
				}
				/* Calculate the enhanced premiums. */
				zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAllocInitTot);
				callRounding(zrdecplcDTO);
				wsaaAmountHoldersInner.wsaaAllocInitTot = wsaaAmountOut;
				wsaaAmountHoldersInner.wsaaOldAllocInitTot = wsaaAmountHoldersInner.wsaaAllocInitTot;
				wsaaAmountHoldersInner.wsaaAllocInitTot = wsaaAmountHoldersInner.wsaaAllocInitTot
						.multiply(wsaaAmountHoldersInner.wsaaEnhancePerc).divide(wsaahurn)
						.setScale(1, BigDecimal.ROUND_HALF_UP);

				zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAllocInitTot);
				callRounding(zrdecplcDTO);
				wsaaAmountHoldersInner.wsaaAllocInitTot = wsaaAmountOut;

				wsaaAmountHoldersInner.wsaaEnhanceAlloc = wsaaAmountHoldersInner.wsaaOldAllocInitTot
						.subtract(wsaaAmountHoldersInner.wsaaAllocInitTot).setScale(0);
				zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAllocInitTot);
				callRounding(zrdecplcDTO);
				wsaaAmountHoldersInner.wsaaAllocAccumTot = wsaaAmountOut;
				wsaaAmountHoldersInner.wsaaOldAllocAccumTot = wsaaAmountHoldersInner.wsaaAllocAccumTot;
				wsaaAmountHoldersInner.wsaaAllocAccumTot = wsaaAmountHoldersInner.wsaaAllocAccumTot
						.multiply(wsaaAmountHoldersInner.wsaaEnhancePerc).divide(wsaahurn, 1, BigDecimal.ROUND_HALF_UP);
				zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAllocInitTot);
				callRounding(zrdecplcDTO);
				wsaaAmountHoldersInner.wsaaAllocAccumTot = wsaaAmountOut;
				wsaaAmountHoldersInner.wsaaEnhanceAlloc = wsaaAmountHoldersInner.wsaaEnhanceAlloc.add(
						wsaaAmountHoldersInner.wsaaOldAllocAccumTot.subtract(wsaaAmountHoldersInner.wsaaAllocAccumTot))
						.setScale(0);
			}

		}
		fundAllocation();
//		GotoLabel nextMethod = GotoLabel.DEFAULT;
//		while (true) {
//			try {
//				switch (nextMethod) {
//				case DEFAULT: 
//					paraMain();
////				case readIncirnl110: 
////					readIncirnl();
//				case fundAllocation180: 
//					fundAllocation();
//				case exit190: 
//					exit190();
//				}
//				break;
//			}
//			catch (GOTOException e){
//				nextMethod = (GotoLabel) e.getNextMethod();
//			}
//		}
	}
	
	protected void paraMain() throws IOException{
		initialisation();	
	}

	protected T5539 getT5539() throws IOException{
	
		if (("").equals(t5540IO.getIuDiscFact().trim())) {
			return null;
		}
		
		TableItem<T5539> t5539= itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T5539", t5540IO.getIuDiscFact(), CommonConstants.IT_ITEMPFX, T5539.class);
		if(t5539!=null) {
			return t5539.getItemDetail();
		}else {
		   fatalError600("ZrulalocImpl T5539 MRNF".concat(rnlallDTO.getCompany()).concat(t5540IO.getIuDiscFact()));
		}
		return null;
	}
	

	
	protected T6646 getT6646() throws IOException{
		if ("".equals(t5540IO.getWholeIuDiscFact())) {
			return null;
		}
		TableItem<T6646> t6646 = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T6646", t5540IO.getWholeIuDiscFact(), CommonConstants.IT_ITEMPFX, T6646.class);
		if(t6646 == null) {
			fatalError600("ZrulalocImpl t6646 MRNF".concat(rnlallDTO.getCompany()).concat(t5540IO.getWholeIuDiscFact()));
			return null;
		}
		return t6646.getItemDetail();
	}
	
	protected void fundAllocation() throws Exception{
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkpf = new Ulnkpf();
		ulnkpf.setChdrcoy(rnlallDTO.getCompany());
		ulnkpf.setChdrnum(rnlallDTO.getChdrnum());
		ulnkpf.setLife(rnlallDTO.getLife());
		ulnkpf.setCoverage(rnlallDTO.getCoverage());
		ulnkpf.setRider(rnlallDTO.getRider());
		ulnkpf.setPlanSuffix(rnlallDTO.getPlanSuffix());
		ulnkpf = ulnkpfDAO.getUlnkrnlRecord(ulnkpf);
		if (ulnkpf==null) {
			this.dberror=true;
			return;
		}
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		if (!StringUtils.isBlank(t5540IO.getIuDiscBasis())) {
			List<BigDecimal> list = rulGenCollectService.initUnitDiscDetails(rnlallDTO, t5540IO, wsaaFact, t5519IO.getFixdtrm());
			wsaaFact=list.get(0).intValue();
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact=list.get(1);
		} else {
			wsaaFact=0;
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact=BigDecimal.ZERO;
		}
		
		
		wsaaAmountHoldersInner.wsaaIndex2=1;
		wsaaAmountHoldersInner.wsaaRunInitTot=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaRunTot=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaTotUlSplit=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaTotIbSplit=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaRunAccumTot=BigDecimal.ZERO;
		while ( !(wsaaAmountHoldersInner.wsaaIndex2>10|| StringUtils.isBlank(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2)))) {
			splitAllocation4000();
			if (dberror) return;
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot=wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaEnhanceAlloc).setScale(0);
		/* IF WSAA-NON-INVEST-PREM-TOT NOT = 0                  <INTBR> */
		/*     MOVE SPACE              TO UTRN-PARAMS           <INTBR> */
		/*     MOVE 'NVST'             TO UTRN-UNIT-SUB-ACCOUNT <INTBR> */
		/*     MOVE WSAA-NON-INVEST-PREM-TOT TO UTRN-CONTRACT-AMOUNT    */
		/*     MOVE 0                  TO UTRN-DISCOUNT-FACTOR  <INTBR> */
		/*     PERFORM 5000-SET-UP-WRITE-UTRN.                  <INTBR> */
		if (wsaaAmountHoldersInner.wsaaNonInvestPremTot.compareTo(BigDecimal.ZERO)!=0) {
			if (wsaaAmountHoldersInner.wsaaTotUlSplit.compareTo(BigDecimal.ZERO)>0) {
				utrnpf = new Utrnpf();			    
				utrnpf.setUnitSubAccount("NVST");
				utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPremTot.multiply(wsaaAmountHoldersInner.wsaaTotUlSplit).divide(wsaahurn,0,BigDecimal.ROUND_HALF_UP));
				utrnpf.setDiscountFactor(BigDecimal.ZERO);
				utrnpf.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1].multiply(wsaaAmountHoldersInner.wsaaTotUlSplit).divide(wsaahurn,0,BigDecimal.ROUND_HALF_UP));
				zrdecplcDTO.setAmountIn(utrnpf.getInciprm01());
				callRounding(zrdecplcDTO);
				utrnpf.setInciprm01(wsaaAmountOut);
				utrnpf.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[2].multiply(wsaaAmountHoldersInner.wsaaTotUlSplit).divide(wsaahurn,0,BigDecimal.ROUND_HALF_UP));
				zrdecplcDTO.setAmountIn(utrnpf.getInciprm02());
				callRounding(zrdecplcDTO);
				utrnpf.setInciprm02(wsaaAmountOut);
				if (!setUpWriteUtrn5000()) return;
				if (wsaaAmountHoldersInner.wsaaTotIbSplit.compareTo(BigDecimal.ZERO)>0) {
					hitrpf = new Hitrpf();
					hitrpf.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPremTot.subtract(utrnpf.getContractAmount().setScale(2)));
					hitrpf.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1].subtract(utrnpf.getInciprm01()).setScale(2));
					zrdecplcDTO.setAmountIn(hitrpf.getInciprm01());
					callRounding(zrdecplcDTO);
					hitrpf.setInciprm01(wsaaAmountOut);
					hitrpf.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[1].subtract(utrnpf.getInciprm02()).setScale(2));
					zrdecplcDTO.setAmountIn(hitrpf.getInciprm02());
					callRounding(zrdecplcDTO);
					hitrpf.setInciprm02(wsaaAmountOut);
					if (!setUpWriteHitr()) return;
				}
			}
			else {
				hitrpf = new Hitrpf();
				hitrpf.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPremTot);
				hitrpf.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1]);
				hitrpf.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[2]);
				zrdecplcDTO.setAmountIn(hitrpf.getInciprm01());
				callRounding(zrdecplcDTO);
				hitrpf.setInciprm01(wsaaAmountOut);
				zrdecplcDTO.setAmountIn(hitrpf.getInciprm02());
				callRounding(zrdecplcDTO);
				hitrpf.setInciprm02(wsaaAmountOut);
				if (!setUpWriteHitr()) return;
			}
		}
		/*  before going back to 105-NEXT-INCIRNL paragraph,               */
		/*  do the following;                                              */
		if (wsaaAmountHoldersInner.wsaaNonInvestPremTot.compareTo(BigDecimal.ZERO)!=0) {
			b100ProcessTax();
		}
	
	} 
	
	protected void b100ProcessTax() throws Exception{
		
		if (wsaaAmountHoldersInner.wsaaNonInvestPremTot.compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		chdrpf = chdrpfDAO.getLPContractWithoutValidflag(rnlallDTO.getCompany(), rnlallDTO.getChdrnum());		
		if (chdrpf==null) {
			dberror = true;
			return ;
		}
		/*  Read coverage/rider record.                                    */
		
		Covrpf covrpf= new Covrpf();
		covrpf.setChdrcoy(rnlallDTO.getCompany());
		covrpf.setChdrnum(rnlallDTO.getChdrnum());
		covrpf.setLife(rnlallDTO.getLife());
		covrpf.setCoverage(rnlallDTO.getCoverage());
		covrpf.setRider(rnlallDTO.getRider());
		covrpf.setPlanSuffix(rnlallDTO.getPlanSuffix());
		covrpf = covrpfDAO.readCovrenqData(covrpf);
		if(covrpf==null) {
			dberror = true;
			return ;
		}
	
		TableItem<Tr52d> tr52d = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "TR52D", chdrpf.getRegister(), CommonConstants.IT_ITEMPFX, Tr52d.class);
		if(tr52d == null) {
			tr52d = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "TR52D", "***", CommonConstants.IT_ITEMPFX, Tr52d.class);
			if(itempf == null) {
				fatalError600("TR52D is MRNF in ZRULALOC ".concat(rnlallDTO.getCompany()).concat("***"));
			}
		}
		
		Tr52d tr52dIO =  tr52d.getItemDetail();
		
		if ("".equals(tr52dIO.getTxcode().trim())) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey = tr52dIO.getTxcode().concat(chdrpf.getCnttype()).concat(rnlallDTO.getCrtable());
		TableItem<Tr52e> tr52e = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "TR52E", wsaaTr52eKey, CommonConstants.IT_ITEMPFX, Tr52e.class);
		if(tr52e == null) {
			wsaaTr52eKey = tr52dIO.getTxcode().concat(chdrpf.getCnttype()).concat("****");
			tr52e = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "TR52E", wsaaTr52eKey, CommonConstants.IT_ITEMPFX, Tr52e.class);
			if(itempf == null) {
				fatalError600("TR52E is MRNF in ZRULALOC ".concat(rnlallDTO.getCompany()).concat(wsaaTr52eKey));
			}
		}
		Tr52e tr52eIO =  tr52e.getItemDetail();
		
		if ( "Y".equals(tr52eIO.getTaxinds().get(4))) {
			return ;
		}
		TxcalcDTO txcalcDTO = new TxcalcDTO();
		txcalcDTO.setFunction("CPST");
		txcalcDTO.setStatuz("****");
		txcalcDTO.setChdrcoy(chdrpf.getChdrcoy());
		txcalcDTO.setChdrnum(chdrpf.getChdrnum());
		txcalcDTO.setLife(covrpf.getLife());
		txcalcDTO.setCoverage(covrpf.getCoverage());
		txcalcDTO.setRider(covrpf.getRider());
		txcalcDTO.setPlanSuffix(covrpf.getPlanSuffix());
		txcalcDTO.setCrtable(covrpf.getCrtable());
		txcalcDTO.setCnttype(chdrpf.getCnttype());
		txcalcDTO.setRegister(chdrpf.getRegister());
		txcalcDTO.setTaxrule(wsaaTr52eKey);
		txcalcDTO.setRateItem(chdrpf.getCntcurr().concat(tr52eIO.getTxitem()));
		txcalcDTO.setTransType("NINV");
		txcalcDTO.setEffdate(rnlallDTO.getEffdate());
		txcalcDTO.setTranno(chdrpf.getTranno());
		txcalcDTO.setLanguage(rnlallDTO.getLanguage());
		txcalcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaNonInvestPremTot);
		txcalcDTO.setBatckey(rnlallDTO.getBatchkey());
		txcalcDTO.setTaxType01("");
		txcalcDTO.setTaxType02("");
        txcalcDTO.setTaxAmt01(BigDecimal.ZERO);
        txcalcDTO.setTaxAmt02(BigDecimal.ZERO);
        txcalcDTO.setJrnseq(0);
        txcalcDTO.setTaxAbsorb01("");
        txcalcDTO.setTaxAbsorb02("");
		if(tr52dIO.getTxsubr().trim().equalsIgnoreCase("SERVTAX")) {
			servtax.processServtax(txcalcDTO);
		}
		if (!txcalcDTO.getStatuz().trim().equals("****")) {
			dberror=true;
			return ;
		}
		if (txcalcDTO.getTaxAmt01().compareTo(BigDecimal.ZERO)==0 && txcalcDTO.getTaxAmt02().compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		taxdpf = new Taxdpf();
		taxdpf.setChdrcoy(covrpf.getChdrcoy());
		taxdpf.setChdrnum(covrpf.getChdrnum());
		taxdpf.setLife(covrpf.getLife());
		taxdpf.setCoverage(covrpf.getCoverage());
		taxdpf.setRider(covrpf.getRider());
		taxdpf.setPlansfx(0);
		taxdpf.setEffdate(txcalcDTO.getEffdate());
		taxdpf.setInstto(99999999);
		taxdpf.setInstfrom(99999999);
		taxdpf.setTranref(txcalcDTO.getTaxrule().concat(txcalcDTO.getRateItem()));
		taxdpf.setTranno(rnlallDTO.getTranno());
		taxdpf.setTrantype("NINV");
		taxdpf.setBaseamt(txcalcDTO.getAmountIn());
		taxdpf.setInstfrom(rnlallDTO.getDuedate());
		taxdpf.setTaxamt01(txcalcDTO.getTaxAmt01());
		taxdpf.setTaxamt02(txcalcDTO.getTaxAmt02());
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setBillcd(rnlallDTO.getBillcd());
		taxdpf.setTxabsind01(txcalcDTO.getTaxAbsorb01());
		taxdpf.setTxabsind02(txcalcDTO.getTaxAbsorb02());
		taxdpf.setTxabsind03("");
		taxdpf.setTxtype01(txcalcDTO.getTaxType01());
		taxdpf.setTxtype02(txcalcDTO.getTaxType02());
		taxdpf.setTxtype03("");
		taxdpf.setPostflg("P");
		taxdpf.setDatime(new Timestamp(System.currentTimeMillis()));
		List<Taxdpf> list = new ArrayList<Taxdpf>();
		list.add(taxdpf);
		taxdpfDAO.insertTaxdpfRecords(list);
		
		if ("Y".equals(txcalcDTO.getTaxAbsorb01())) {
			wsaaTax[1]=BigDecimal.ZERO;
		}
		else {
			wsaaTax[1]=txcalcDTO.getTaxAmt01();
		}
		if ("Y".equals(txcalcDTO.getTaxAbsorb02())) {
			wsaaTax[2]=BigDecimal.ZERO;
		}
		else {
			wsaaTax[2]=txcalcDTO.getTaxAmt02();
		}
		if (wsaaTax[1].compareTo(BigDecimal.ZERO)==0 && wsaaTax[2].compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		List<Zrstpf> zrstpflist = zrstpfDAO.getZrstpfRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife());
		if(zrstpflist !=null && zrstpflist.size()>0) {
			for(Zrstpf tmpzrstpf : zrstpflist) {
				wsaaTotCd=wsaaTotCd.add(tmpzrstpf.getZramount01());
			}
		}
		wsaaTotCd=wsaaTax[1].add(wsaaTax[2]).add(wsaaTotCd).setScale(2);
		
		if (!b300WriteZrst(txcalcDTO.getTaxSacstyp01(),  txcalcDTO.getTaxSacstyp02(), covrpf)) return;
		zrstpf = new Zrstpf();
	
		zrstpf.setChdrcoy(covrpf.getChdrcoy());
		zrstpf.setChdrnum(covrpf.getChdrnum());
		zrstpf.setLife(covrpf.getLife());
		zrstpflist = zrstpfDAO.getZrstpfRecord(zrstpf.getChdrcoy(), zrstpf.getChdrnum(), zrstpf.getLife());
		
		if(zrstpflist !=null && zrstpflist.size()>0) {
			for(Zrstpf tmpzrstpf : zrstpflist) {			
				tmpzrstpf.setZramount02(wsaaTotCd);
				if(zrstpfDAO.updateZrstpfZramount(tmpzrstpf.getZramount02(), tmpzrstpf.getUniqueNumber())<=0) {
					dberror = true;
					return;
				}
				
			}
		}else {
			dberror = true;
			return;
		}
	
		/* Update the coverage file coverage debt.                         */		
		covrpf.setCoverageDebt(covrpf.getCoverageDebt().add(wsaaTax[1]).add(wsaaTax[2]).setScale(2));
		if(covrpfDAO.updateCovrpfCDebt(covrpf.getCoverageDebt(), covrpf.getUniqueNumber())<=0) {
			this.dberror = true;
			return;
		}
		
		/* Post coverage debt                                              */
		Descpf descpf = descpfDAO.getDescInfo(rnlallDTO.getCompany(), "T1688", rnlallDTO.getBatctrcde());
//		wsaaItemkey.set(SPACES);
//		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
//		wsaaItemkey.itemItemcoy.set(rnlallrec.company);
//		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
//		wsaaItemkey.itemItemitem.set(rnlallrec.batctrcde);
//		getdescrec.itemkey.set(wsaaItemkey);
//		getdescrec.language.set(rnlallrec.language);
//		getdescrec.function.set("CHECK");
//		callProgram(Getdesc.class, getdescrec.getdescRec);
//		if (isNE(getdescrec.statuz, varcom.oK)) {
//			getdescrec.longdesc.set(SPACES);
//		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		LifacmvDTO lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setRdocnum(rnlallDTO.getChdrnum());
		lifacmvDTO.setJrnseq(txcalcDTO.getJrnseq());
		lifacmvDTO.setBatccoy(rnlallDTO.getCompany());
		lifacmvDTO.setRldgcoy(rnlallDTO.getCompany());
		lifacmvDTO.setGenlcoy(rnlallDTO.getCompany());
		lifacmvDTO.setBatcactyr(rnlallDTO.getBatcactyr());
		lifacmvDTO.setBatctrcde(rnlallDTO.getBatctrcde());
		lifacmvDTO.setBatcactmn(rnlallDTO.getBatcactmn());
		lifacmvDTO.setBatcbatch(rnlallDTO.getBatcbatch());
		lifacmvDTO.setBatcbrn(rnlallDTO.getBatcbrn());
		lifacmvDTO.setOrigcurr(rnlallDTO.getCntcurr());
		lifacmvDTO.setOrigamt(wsaaTax[1].add(wsaaTax[2]).setScale(2));
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setTranno(0);
		lifacmvDTO.setCrate(BigDecimal.ZERO);
		lifacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifacmvDTO.setTranno(rnlallDTO.getTranno());
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setEffdate(rnlallDTO.getEffdate());
		/* Set up TRANREF to contain the Full component key of the         */
		/*  Coverage/Rider that is Creating the debt so that if a          */
		/*  reversal is required at a later date, the correct component    */
		/*  can be identified.                                             */
	
		lifacmvDTO.setTranref(rnlallDTO.getCompany().concat(rnlallDTO.getChdrnum()).concat(rnlallDTO.getLife()).concat(rnlallDTO.getCoverage())
				.concat(rnlallDTO.getRider()).concat(String.valueOf(rnlallDTO.getPlanSuffix())));
		lifacmvDTO.setUser(rnlallDTO.getUser());
		lifacmvDTO.setTrandesc(descpf.getLongdesc());

		/*        Check for Component level accounting & act accordingly   */
		if ("Y".equals(t5688IO.getComlvlacc())) {
            String temp = String.valueOf(rnlallDTO.getPlanSuffix());
            if(temp.length()>=2&&temp.length()<=4) {
            	temp= temp.substring(2);
            }
            List<String> scode=new ArrayList<String>();
            scode.add(rnlallDTO.getCnttype());
            for(int i = 0; i<=4;i++) {
            	if(i==4) {
            		scode.add(covrpf.getCrtable());
            	}else {
            	    scode.add("");
            	}
            	
            }
			lifacmvDTO.setRldgacct(rnlallDTO.getChdrnum().concat(rnlallDTO.getLife()).concat(rnlallDTO.getCoverage()).concat("00").concat(temp));
			lifacmvDTO.setSubstituteCodes(scode);
		}
		else {
		
			lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(8));
			lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(8));
			lifacmvDTO.setGlsign(t5645IO.getSigns().get(8));
			lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(8));
			lifacmvDTO.setContot(t5645IO.getCnttots().get(8));
			lifacmvDTO.setRldgacct(rnlallDTO.getChdrnum());
			List<String> scode=new ArrayList<String>();
			 scode.add(rnlallDTO.getCnttype());
            for(int i = 0; i<=4;i++) {    	
            	    scode.add("");          	
            }
            lifacmvDTO.setSubstituteCodes(scode);
		}
	
		lifacmvDTO.setTransactionDate(rnlallDTO.getEffdate());
		
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmssSS");
		lifacmvDTO.setTransactionTime(Integer.parseInt(sdf.format(d)));
		
		lifacmv.executePSTW(lifacmvDTO);
		
	}

	
	protected boolean b300WriteZrst(String taxSacstyp01, String taxSacstyp02, Covrpf covrpf)
	{
		zrstpf = new Zrstpf();
		zrstpf.setZramount01(BigDecimal.ZERO);
		zrstpf.setZramount02(BigDecimal.ZERO);
		zrstpf.setChdrcoy(covrpf.getChdrcoy());
		zrstpf.setChdrnum(covrpf.getChdrnum());
		zrstpf.setLife(covrpf.getLife());
		zrstpf.setJlife("");
		zrstpf.setCoverage(covrpf.getCoverage());
		zrstpf.setRider(covrpf.getRider());
		zrstpf.setBatctrcde(rnlallDTO.getBatctrcde());
		zrstpf.setTranno(rnlallDTO.getTranno());
		zrstpf.setTrandate(rnlallDTO.getEffdate());
		wsaaSeqno++;
		zrstpf.setXtranno(0);
		zrstpf.setSeqno(wsaaSeqno);
		zrstpf.setUstmno(0);
		if (wsaaTax[1].compareTo(BigDecimal.ZERO)==0) {
				
			if (wsaaTax[2].compareTo(BigDecimal.ZERO)==0) {
				return true;
			}
			zrstpf.setZramount01(wsaaTax[2]);
			zrstpf.setZramount02(wsaaTotCd);
			/*    MOVE TXCL-TAX-TYPE (2)      TO ZRST-SACSTYP          <LA4895>*/
			zrstpf.setSacstyp(taxSacstyp02);
			if(zrstpfDAO.insertZrstpf(zrstpf)<=0) {
				dberror=true;
				return false;
			}
			return true;
		}
		zrstpf.setZramount01(wsaaTax[1]);
		zrstpf.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP          <LA4895>*/
		zrstpf.setSacstyp(taxSacstyp01);
		zrstpf.setDatime(new Timestamp(System.currentTimeMillis()));
		if(zrstpfDAO.insertZrstpf(zrstpf)<=0) {
			dberror=true;
			return false;
		}
		return true;
	}
	
	protected void splitAllocation4000() throws IOException{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit4100();
				case next4150: 
					if(!next4150()) return;
				case accumUnit4200: 
					accumUnit4200();
				case next4250: 
					if(!next4250()) return;
				case nearExit4800: 
					nearExit4800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	


protected void initUnit4100() throws IOException{
	   TableItem<T5515> t5515 = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T5515",  ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2), CommonConstants.IT_ITEMPFX, T5515.class);
	   if(t5515!=null) {
			t5515IO = t5515.getItemDetail();
		}
		
		if(t5515IO != null && "D".equals(t5515IO.getZfundtyp().trim())) {
			wsaaAmountHoldersInner.wsaaAllocTot = wsaaAmountHoldersInner.wsaaAllocInitTot.add(wsaaAmountHoldersInner.wsaaAllocAccumTot).setScale(0);
			wsaaAmountHoldersInner.wsaaTotIbSplit = wsaaAmountHoldersInner.wsaaTotIbSplit.add(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2));

			if (wsaaAmountHoldersInner.wsaaAllocTot.compareTo(BigDecimal.ZERO)!=0) {
				calcHitrAmount5100();
				setUpWriteHitr();
			}
			goTo(GotoLabel.nearExit4800);
		}
		wsaaAmountHoldersInner.wsaaTotUlSplit=wsaaAmountHoldersInner.wsaaTotUlSplit.add(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2));
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (wsaaAmountHoldersInner.wsaaAllocInitTot.compareTo(BigDecimal.ZERO)==0) {
			goTo(GotoLabel.accumUnit4200);
		}
		utrnpf = new Utrnpf();
		if (wsaaAmountHoldersInner.wsaaIndex2==10) {
			utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocInitTot.subtract(wsaaAmountHoldersInner.wsaaRunInitTot).setScale(0));
			goTo(GotoLabel.next4150);
		}
		if ("".equals(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2+1).trim())) {
			utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocInitTot.subtract(wsaaAmountHoldersInner.wsaaRunInitTot).setScale(0));
		}
		else {
			utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocInitTot.multiply(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)).divide(wsaahurn, 1, BigDecimal.ROUND_HALF_UP));
		}
	}

protected boolean next4150() throws IOException{
	zrdecplcDTO.setAmountIn(utrnpf.getContractAmount());
	callRounding(zrdecplcDTO);
	utrnpf.setContractAmount(wsaaAmountOut);
	wsaaAmountHoldersInner.wsaaRunInitTot=wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnpf.getContractAmount());
	wsaaAmountHoldersInner.wsaaRunTot=wsaaAmountHoldersInner.wsaaRunTot.add(utrnpf.getContractAmount());
	utrnpf.setUnitVirtualFund(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
	utrnpf.setUnitType("I");
	utrnpf.setUnitSubAccount("INIT");
	utrnpf.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
	return setUpWriteUtrn5000();
}


protected void nearExit4800()
{
	wsaaAmountHoldersInner.wsaaIndex2++;
	/*EXIT*/
}

protected void accumUnit4200() throws IOException{
	if (wsaaAmountHoldersInner.wsaaAllocAccumTot.compareTo(BigDecimal.ZERO)==0) {
		goTo(GotoLabel.nearExit4800);
	}
	utrnpf = new Utrnpf();
	if (wsaaAmountHoldersInner.wsaaIndex2==10) {
		utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocAccumTot.subtract(wsaaAmountHoldersInner.wsaaRunAccumTot).setScale(0));
		goTo(GotoLabel.next4250);
	}
	if ("".equals(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2+1).trim())) {
		utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocAccumTot.subtract(wsaaAmountHoldersInner.wsaaRunAccumTot).setScale(0));
	}
	else {
		utrnpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocAccumTot.multiply(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2).divide(wsaahurn,1, BigDecimal.ROUND_HALF_UP)));
	}
}

protected boolean next4250() throws IOException{
	zrdecplcDTO.setAmountIn(utrnpf.getContractAmount());
	callRounding(zrdecplcDTO);
	utrnpf.setContractAmount(wsaaAmountOut);
	wsaaAmountHoldersInner.wsaaRunAccumTot=wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnpf.getContractAmount());
	wsaaAmountHoldersInner.wsaaRunTot=wsaaAmountHoldersInner.wsaaRunTot.add(utrnpf.getContractAmount());
	utrnpf.setUnitVirtualFund(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
	utrnpf.setUnitType("A");
	utrnpf.setUnitSubAccount("ACUM");
	utrnpf.setDiscountFactor(BigDecimal.ZERO);
	utrnpf.setFundPool(ulnkpf.getFundPool(wsaaAmountHoldersInner.wsaaIndex2));
	return setUpWriteUtrn5000();
}

protected boolean setUpWriteUtrn5000() throws IOException{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				para5100();
			case comlvlacc5105: 
				comlvlacc5105();
			case cont5110: 
				cont5110();
			case writrUtrn5800: 
				writrUtrn5800();
				if (dberror) return false;
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
	return true;
}

protected void para5100() throws IOException{
	
	  TableItem<T5515> t5515 = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T5515", utrnpf.getUnitVirtualFund(), CommonConstants.IT_ITEMPFX, T5515.class);
	  if(t5515 != null)	t5515IO = t5515.getItemDetail();

	utrnpf.setChdrcoy(rnlallDTO.getCompany());
	utrnpf.setChdrnum(rnlallDTO.getChdrnum());
	utrnpf.setCoverage(rnlallDTO.getCoverage());
	utrnpf.setLife(rnlallDTO.getLife());
	//ILAE-65
//	utrnIO.setRider("00");
	utrnpf.setRider(rnlallDTO.getRider());
	utrnpf.setPlanSuffix(rnlallDTO.getPlanSuffix());
	utrnpf.setTranno(rnlallDTO.getTranno());
	Date d = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("HHmmssSS");
	utrnpf.setTransactionTime(Integer.parseInt(sdf.format(d)));
	sdf = new SimpleDateFormat("yyMMdd");
	utrnpf.setTransactionDate(Integer.parseInt(sdf.format(d)));
	utrnpf.setUser(rnlallDTO.getUser());
	utrnpf.setBatccoy(rnlallDTO.getBatccoy());
	utrnpf.setBatcbrn(rnlallDTO.getBatcbrn());
	utrnpf.setBatcactyr(rnlallDTO.getBatcactyr());
	utrnpf.setBatcactmn(rnlallDTO.getBatcactmn());
	utrnpf.setBatctrcde(rnlallDTO.getBatctrcde());
	utrnpf.setBatcbatch(rnlallDTO.getBatcbatch());
	utrnpf.setCrtable(rnlallDTO.getCrtable());
	utrnpf.setCntcurr(rnlallDTO.getCntcurr());
	if ("Y".equals(t5688IO.getComlvlacc().trim())) {
		goTo(GotoLabel.comlvlacc5105);
	}
	if ("NVST".equals(utrnpf.getUnitSubAccount().trim())) {
		utrnpf.setSacscode(t5645IO.getSacscodes().get(1));
		utrnpf.setSacstyp(t5645IO.getSacstypes().get(1));
		utrnpf.setGenlcde(t5645IO.getGlmaps().get(1));
	}
	else {
		utrnpf.setSacscode(t5645IO.getSacscodes().get(0));
		utrnpf.setSacstyp(t5645IO.getSacstypes().get(0));
		utrnpf.setGenlcde(t5645IO.getGlmaps().get(0));
	}
	goTo(GotoLabel.cont5110);
}

protected void cont5110() throws IOException{
	utrnpf.setCnttyp(rnlallDTO.getCnttype());
	utrnpf.setSvp(BigDecimal.ONE);
	utrnpf.setCrComDate(new Long(rnlallDTO.getCrdate()));
	utrnpf.setFundCurrency(t5515IO.getCurrcode());
	if (utrnpf.getContractAmount().compareTo(BigDecimal.ZERO)<0) {
		utrnpf.setNowDeferInd(t6647IO.getDealin());
	}
	else {
		utrnpf.setNowDeferInd(t6647IO.getAloind());
	}
	/* Calculate the monies date.*/

	wsaaMoniesDate=Integer.parseInt(datcon1.todaysDate());
	if ("BD".equals(t6647IO.getEfdcode().trim())) {
		/*NEXT_SENTENCE*/
	}
	else {
		if ("DD".equals(t6647IO.getEfdcode().trim())) {
			wsaaMoniesDate=rnlallDTO.getDuedate();
		}
		else {
			if ("LO".equals(t6647IO.getEfdcode().trim())) {
				if (rnlallDTO.getDuedate()>wsaaMoniesDate) {
					wsaaMoniesDate=rnlallDTO.getDuedate();
				}
			}
		}
	}
	/*  AS ISSUE SETS UP THIS INFO CORRECTLY ,ITS SEEMS POINTLESS*/
	/*  TO REPEAT / OR REMOVE CORRECT CODING.*/
	/* If this instalment has been reversed before, use the original*/
	/* monies date to ensure no descrpency in unit prices for now*/
	/* and then.*/
	if (zrrapf != null) {
		utrnpf.setMoniesDate(new Long(zrrapf.getMoniesdt()));
	}
	else {
		if ("T642".equals(rnlallDTO.getBatctrcde().trim())) {
			utrnpf.setMoniesDate(new Long(rnlallDTO.getMoniesDate()));
		}
		else {
			utrnpf.setMoniesDate(new Long(wsaaMoniesDate));
		}
	}
	utrnpf.setProcSeqNo(wsaaAmountHoldersInner.wsaaSeqNo);
	utrnpf.setJobnoPrice(BigDecimal.ZERO);
	utrnpf.setFundRate(BigDecimal.ZERO);
	utrnpf.setStrpdate(new Long(0));
	utrnpf.setNofUnits(BigDecimal.ZERO);
	utrnpf.setNofDunits(BigDecimal.ZERO);
	utrnpf.setPriceDateUsed(new Long(0));
	utrnpf.setPriceUsed(BigDecimal.ZERO);
	utrnpf.setUnitBarePrice(BigDecimal.ZERO);
	utrnpf.setFundAmount(BigDecimal.ZERO);
	utrnpf.setSurrenderPercent(BigDecimal.ZERO);
	utrnpf.setUstmno(0);
	/*                                UTRN-INCIPRM01        <INTBR> */
	/*                                UTRN-INCIPRM02.       <INTBR> */
	utrnpf.setInciNum(incipf.getSeqno());
	utrnpf.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
	utrnpf.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
	utrnpf.setDatime(new Timestamp(System.currentTimeMillis()));
	if ("T642".equals(rnlallDTO.getBatctrcde().trim())) 
	{
		utrnpf.setFundPool("1");
	}
	
	if (wsaaAmountHoldersInner.wsaaEnhancePerc.compareTo(BigDecimal.ZERO)!=0) {
		wsaaAmountHoldersInner.wsaaContractAmount = utrnpf.getContractAmount().divide(wsaaAmountHoldersInner.wsaaEnhancePerc).multiply(wsaahurn).setScale(2);
	}
	else {
		wsaaAmountHoldersInner.wsaaContractAmount=utrnpf.getContractAmount();
	}
	zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaContractAmount);
	callRounding(zrdecplcDTO);
	wsaaAmountHoldersInner.wsaaContractAmount=wsaaAmountOut;
	/* Use the amounts previously stored for each allocation*/
	/* period rather than the  whole contract amounts.*/
	if ("NVST".equals(utrnpf.getUnitSubAccount().trim())) {
		/*     MOVE WSAA-INCI-N-INVS (01) TO UTRN-INCIPRM01             */
		/*     MOVE WSAA-INCI-N-INVS (02) TO UTRN-INCIPRM02             */
		if(utrnpfDAO.insertUtrnpfRecord(utrnpf)<=0) {
			dberror=true;
		}
	}
	if ("INIT".equals(utrnpf.getUnitSubAccount().trim())) {
		wsaaAmountHoldersInner.wsaaFundPc = utrnpf.getContractAmount().divide(wsaaAmountHoldersInner.wsaaAllocInitTot).multiply(wsaahurn).setScale(3, BigDecimal.ROUND_HALF_UP);
		if (wsaaAmountHoldersInner.wsaaInciInit[1].compareTo(BigDecimal.ZERO)>0) {
		
			utrnpf.setInciprm01(wsaaAmountHoldersInner.wsaaInciInit[1].multiply(wsaaAmountHoldersInner.wsaaFundPc).divide(wsaahurn,1, BigDecimal.ROUND_HALF_UP));
			zrdecplcDTO.setAmountIn(utrnpf.getInciprm01());
			callRounding(zrdecplcDTO);
			utrnpf.setInciprm01(wsaaAmountOut);
		}
		else {
			utrnpf.setInciprm01(BigDecimal.ZERO);
		}
		if (wsaaAmountHoldersInner.wsaaInciInit[2].compareTo(BigDecimal.ZERO)>0) {
			utrnpf.setInciprm02(wsaaAmountHoldersInner.wsaaInciInit[2].multiply(wsaaAmountHoldersInner.wsaaFundPc).divide(wsaahurn, 1, BigDecimal.ROUND_HALF_UP));
			zrdecplcDTO.setAmountIn(utrnpf.getInciprm02());
			callRounding(zrdecplcDTO);
			utrnpf.setInciprm02(wsaaAmountOut);
		}
		else {
			utrnpf.setInciprm02(BigDecimal.ZERO);
		}
		
		
		goTo(GotoLabel.writrUtrn5800);
	}
	
	
	
	if ("ACUM".equals(utrnpf.getUnitSubAccount().trim()))  {
		wsaaAmountHoldersInner.wsaaFundPc= utrnpf.getContractAmount().divide(wsaaAmountHoldersInner.wsaaAllocAccumTot).multiply(wsaahurn).setScale(3, BigDecimal.ROUND_HALF_UP);
		if (wsaaAmountHoldersInner.wsaaInciAlloc[1].compareTo(BigDecimal.ZERO)>0) {
			utrnpf.setInciprm01(wsaaAmountHoldersInner.wsaaInciAlloc[1].multiply(wsaaAmountHoldersInner.wsaaFundPc).divide(wsaahurn, 1, BigDecimal.ROUND_HALF_UP));
			zrdecplcDTO.setAmountIn(utrnpf.getInciprm01());
			callRounding(zrdecplcDTO);
			utrnpf.setInciprm01(wsaaAmountOut);
		}
		else {
			utrnpf.setInciprm01(BigDecimal.ZERO);
		}
		if (wsaaAmountHoldersInner.wsaaInciAlloc[2].compareTo(BigDecimal.ZERO)>0) {
			utrnpf.setInciprm02(wsaaAmountHoldersInner.wsaaInciAlloc[2].multiply(wsaaAmountHoldersInner.wsaaFundPc).divide(wsaahurn, 1, BigDecimal.ROUND_HALF_UP));
			zrdecplcDTO.setAmountIn(utrnpf.getInciprm02());
			callRounding(zrdecplcDTO);
			utrnpf.setInciprm02(wsaaAmountOut);
		}
		else {
			utrnpf.setInciprm02(BigDecimal.ZERO);
		}
	}
}

protected void writrUtrn5800()
{
	if(utrnpfDAO.insertUtrnpfRecord(utrnpf)<=0) {
		dberror=true;
//		goTo(GotoLabel.exit190);
	}
	/*EXIT*/
}

protected void comlvlacc5105()
{
	if ("NVST".equals(utrnpf.getUnitSubAccount())) {
		utrnpf.setSacscode(t5645IO.getSacscodes().get(3));
		utrnpf.setSacstyp(t5645IO.getSacstypes().get(3));
		utrnpf.setGenlcde(t5645IO.getGlmaps().get(3));
	}
	else {
		utrnpf.setSacscode(t5645IO.getSacscodes().get(2));
		utrnpf.setSacstyp(t5645IO.getSacstypes().get(2));
		utrnpf.setGenlcde(t5645IO.getGlmaps().get(2));
	}
}

protected boolean setUpWriteHitr() throws IOException{

	hitrpf.setChdrcoy(rnlallDTO.getCompany());
	hitrpf.setChdrnum(rnlallDTO.getChdrnum());
	hitrpf.setCoverage(rnlallDTO.getCoverage());
	hitrpf.setLife(rnlallDTO.getLife());
	hitrpf.setRider("00");
	hitrpf.setPlanSuffix(rnlallDTO.getPlanSuffix());
	hitrpf.setTranno(rnlallDTO.getTranno());
	hitrpf.setBatccoy(rnlallDTO.getBatccoy());
	hitrpf.setBatcbrn(rnlallDTO.getBatcbrn());
	hitrpf.setBatcactyr(rnlallDTO.getBatcactyr());
	hitrpf.setBatcactmn(rnlallDTO.getBatcactmn());
	hitrpf.setBatctrcde(rnlallDTO.getBatctrcde());
	hitrpf.setBatcbatch(rnlallDTO.getBatcbatch());
	hitrpf.setCrtable(rnlallDTO.getCrtable());
	hitrpf.setCntcurr(rnlallDTO.getCntcurr());
	if (("").equals(hitrpf.getZintbfnd().trim())) {
		if (t5688IO.getComlvlacc().equals("Y")) {
			hitrpf.setSacscode(t5645IO.getSacscodes().get(7));
			hitrpf.setSacstyp(t5645IO.getSacstypes().get(7));
			hitrpf.setGenlcde(t5645IO.getGlmaps().get(7));
		}
		else {
			hitrpf.setSacscode(t5645IO.getSacscodes().get(5));
			hitrpf.setSacstyp(t5645IO.getSacstypes().get(5));
			hitrpf.setGenlcde(t5645IO.getGlmaps().get(5));
		}
	}
	else {
		if (t5688IO.getComlvlacc().equals("Y")) {
			hitrpf.setSacscode(t5645IO.getSacscodes().get(6));
			hitrpf.setSacstyp(t5645IO.getSacstypes().get(6));
			hitrpf.setGenlcde(t5645IO.getGlmaps().get(6));
		}
		else {
			hitrpf.setSacscode(t5645IO.getSacscodes().get(4));
			hitrpf.setSacstyp(t5645IO.getSacstypes().get(4));
			hitrpf.setGenlcde(t5645IO.getGlmaps().get(4));
		}
	}
	hitrpf.setCnttyp(rnlallDTO.getCnttype());
	hitrpf.setSvp(BigDecimal.ONE);
	hitrpf.setFundCurrency(t5515IO.getCurrcode());
	hitrpf.setProcSeqNo(wsaaAmountHoldersInner.wsaaSeqNo);
	hitrpf.setFundAmount(BigDecimal.ZERO);
	hitrpf.setSurrenderPercent(BigDecimal.ZERO);
	hitrpf.setUstmno(0);
	hitrpf.setFundRate(BigDecimal.ZERO);
	hitrpf.setInciNum(incipf.getSeqno());
	hitrpf.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
	hitrpf.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
	/* Use the amounts previously stored for each allocation   <INTBR> */
	/* period rather than the  whole contract amounts.         <INTBR> */
	if ("".equals(hitrpf.getZintbfnd().trim())) {
		return writrHitr5250();
	}
	wsaaAmountHoldersInner.wsaaInciTot[1]=wsaaAmountHoldersInner.wsaaInciInit[1].add(wsaaAmountHoldersInner.wsaaInciAlloc[1]).setScale(0);
	wsaaAmountHoldersInner.wsaaInciTot[2]=wsaaAmountHoldersInner.wsaaInciInit[2].add(wsaaAmountHoldersInner.wsaaInciAlloc[2]).setScale(0);
	wsaaAmountHoldersInner.wsaaFundPc=hitrpf.getContractAmount().divide(wsaaAmountHoldersInner.wsaaAllocTot).multiply(wsaahurn).setScale(3, BigDecimal.ROUND_HALF_UP);

	if (wsaaAmountHoldersInner.wsaaInciTot[1].compareTo(BigDecimal.ZERO)>0) {

		hitrpf.setInciprm01(wsaaAmountHoldersInner.wsaaInciTot[1].multiply(wsaaAmountHoldersInner.wsaaFundPc).divide(wsaahurn).setScale(1, BigDecimal.ROUND_HALF_UP));
		zrdecplcDTO.setAmountIn(hitrpf.getInciprm01());
		callRounding(zrdecplcDTO);
		hitrpf.setInciprm01(wsaaAmountOut);
	}
	else {
		hitrpf.setInciprm01(BigDecimal.ZERO);
	}
	if (wsaaAmountHoldersInner.wsaaInciTot[2].compareTo(BigDecimal.ZERO)>0) {
		hitrpf.setInciprm02(wsaaAmountHoldersInner.wsaaInciTot[2].multiply(wsaaAmountHoldersInner.wsaaFundPc).divide(wsaahurn).setScale(1, BigDecimal.ROUND_HALF_UP));
		zrdecplcDTO.setAmountIn(hitrpf.getInciprm02());
		callRounding(zrdecplcDTO);
		hitrpf.setInciprm02(wsaaAmountOut);
	}
	else {
		hitrpf.setInciprm02(BigDecimal.ZERO);
	}
	return writrHitr5250();
}

protected boolean writrHitr5250()
{
	if ("T642".equals(rnlallDTO.getBatctrcde())) 
	{
		hitrpf.setFundpool("1");
	}
		
	if ("".equals(hitrpf.getZintbfnd().trim())) {
		hitrpf.setZrectyp("N");
	}
	else {
		hitrpf.setZrectyp("P");
	}
	hitrpf.setEffdate(rnlallDTO.getEffdate());
	hitrpf.setZintappind("");
	hitrpf.setZlstintdte(99999999);
	hitrpf.setZinteffdt(99999999);
	hitrpf.setZintrate(BigDecimal.ZERO);
	if(hitrpfDAO.insertHitrpfRecord(hitrpf)<=0) {
		dberror = true;
		return false;
	}
	return true;
}

protected void calcHitrAmount5100() throws IOException{
	hitrpf = new Hitrpf();
	if (wsaaAmountHoldersInner.wsaaIndex2==10) {
		hitrpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocTot.subtract(wsaaAmountHoldersInner.wsaaRunTot).setScale(0));
	}
	else {
		if ("".equals(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2+ 1))) {
			hitrpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocTot.subtract( wsaaAmountHoldersInner.wsaaRunTot).setScale(0));
		}
		else {
			hitrpf.setContractAmount(wsaaAmountHoldersInner.wsaaAllocTot.multiply(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)).divide(wsaahurn)
					.setScale(1, BigDecimal.ROUND_HALF_UP));
			wsaaAmountHoldersInner.wsaaInitAmount = wsaaAmountHoldersInner.wsaaAllocInitTot.multiply(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)).divide(wsaahurn)
					.setScale(1, BigDecimal.ROUND_HALF_UP);
			wsaaAmountHoldersInner.wsaaAccumAmount=wsaaAmountHoldersInner.wsaaAllocAccumTot.multiply(ulnkpf.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)).divide(wsaahurn)
					.setScale(1, BigDecimal.ROUND_HALF_UP);
		}
	}
	zrdecplcDTO.setAmountIn(hitrpf.getContractAmount());
	callRounding(zrdecplcDTO);
	hitrpf.setContractAmount(wsaaAmountOut);
	zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaInitAmount);
	callRounding(zrdecplcDTO);
	wsaaAmountHoldersInner.wsaaInitAmount=wsaaAmountOut;
	zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAccumAmount);
	callRounding(zrdecplcDTO);
	wsaaAmountHoldersInner.wsaaAccumAmount=wsaaAmountOut;
	wsaaAmountHoldersInner.wsaaRunTot=wsaaAmountHoldersInner.wsaaRunTot.add(hitrpf.getContractAmount());
	wsaaAmountHoldersInner.wsaaRunInitTot=wsaaAmountHoldersInner.wsaaRunInitTot.add(wsaaAmountHoldersInner.wsaaInitAmount);
	wsaaAmountHoldersInner.wsaaRunAccumTot=wsaaAmountHoldersInner.wsaaRunAccumTot.add(wsaaAmountHoldersInner.wsaaAccumAmount);
	hitrpf.setZintbfnd(ulnkpf.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
}
	
	protected void premiumAllocation1000() throws IOException{
		para1010();
		nearExit1800();
	}
	protected void para1010() throws IOException{
		if (incipf.getPremcurr(wsaaAmountHoldersInner.wsaaIndex).compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		if (wsaaAmountHoldersInner.wsaaInciPrem.compareTo(incipf.getPremcurr(wsaaAmountHoldersInner.wsaaIndex))>=0) {
			wsaaAmountHoldersInner.wsaaAllocAmt=incipf.getPremcurr(wsaaAmountHoldersInner.wsaaIndex);
		}
		if (incipf.getPremcurr(wsaaAmountHoldersInner.wsaaIndex).compareTo(wsaaAmountHoldersInner.wsaaInciPrem)>0) {
			wsaaAmountHoldersInner.wsaaAllocAmt=wsaaAmountHoldersInner.wsaaInciPrem;
		}
		calculateSplit2000();
	}
	
	protected void calculateSplit2000() throws IOException{
	/* Calculate the uninvested allocation.*/
	wsaaAmountHoldersInner.wsaaEnhanceAlloc=BigDecimal.ZERO;
	wsaaAmountHoldersInner.wsaaNonInvestPrem = wsaaAmountHoldersInner.wsaaAllocAmt.multiply(wsaahurn.subtract(incipf.getPcunit(wsaaAmountHoldersInner.wsaaIndex))).divide(wsaahurn, 1);
	zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaNonInvestPrem);
	callRounding(zrdecplcDTO);
	wsaaAmountHoldersInner.wsaaNonInvestPrem=wsaaAmountOut;
	/* Calculate the allocation for buying initial units.*/
	wsaaAmountHoldersInner.wsaaAllocInit=wsaaAmountHoldersInner.wsaaAllocAmt.subtract(wsaaAmountHoldersInner.wsaaNonInvestPrem).multiply(incipf.getUsplitpc(wsaaAmountHoldersInner.wsaaIndex)).divide(wsaahurn, 1);	
	zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAllocInit);
	callRounding(zrdecplcDTO);
	wsaaAmountHoldersInner.wsaaAllocInit=wsaaAmountOut;
	/* Calculate the allocation for buying accumulation units.*/
	wsaaAmountHoldersInner.wsaaAllocAccum = wsaaAmountHoldersInner.wsaaAllocAmt.subtract(wsaaAmountHoldersInner.wsaaNonInvestPrem.add( wsaaAmountHoldersInner.wsaaAllocInit)).setScale(1);
	zrdecplcDTO.setAmountIn(wsaaAmountHoldersInner.wsaaAllocAccum);
	callRounding(zrdecplcDTO);
	wsaaAmountHoldersInner.wsaaAllocAccum=wsaaAmountOut;
	/* Accumulate totals.*/
	wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
	wsaaAmountHoldersInner.wsaaAllocInitTot.add(wsaaAmountHoldersInner.wsaaAllocInit);
	wsaaAmountHoldersInner.wsaaAllocAccumTot.add(wsaaAmountHoldersInner.wsaaAllocAccum);
	/* Update the premium left to pay for that period.*/
	incipf.getPremcurr(wsaaAmountHoldersInner.wsaaIndex).setScale(1);
	
	incipf.setPremcurr(wsaaAmountHoldersInner.wsaaIndex, incipf.getPremcurr(wsaaAmountHoldersInner.wsaaIndex).subtract(wsaaAmountHoldersInner.wsaaAllocAmt));
	/* Store the INCI amounts in working storage.*/
	if (wsaaAmountHoldersInner.wsaaInciPerd[1]==0) {
		wsaaAmountHoldersInner.wsaaInciPerd[1]=wsaaAmountHoldersInner.wsaaIndex;
		wsaaAmountHoldersInner.wsaaInciInit[1]=wsaaAmountHoldersInner.wsaaInciInit[1].add(wsaaAmountHoldersInner.wsaaAllocInit);
		wsaaAmountHoldersInner.wsaaInciAlloc[1]=wsaaAmountHoldersInner.wsaaInciAlloc[1].add(wsaaAmountHoldersInner.wsaaAllocAccum);
		wsaaAmountHoldersInner.wsaaInciNInvs[1]=wsaaAmountHoldersInner.wsaaInciNInvs[1].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
	}
	else {
		wsaaAmountHoldersInner.wsaaInciPerd[2]=wsaaAmountHoldersInner.wsaaIndex;
		wsaaAmountHoldersInner.wsaaInciInit[2]=wsaaAmountHoldersInner.wsaaInciInit[2].add(wsaaAmountHoldersInner.wsaaAllocInit);
		wsaaAmountHoldersInner.wsaaInciAlloc[2]=wsaaAmountHoldersInner.wsaaInciAlloc[2].add(wsaaAmountHoldersInner.wsaaAllocAccum);
		wsaaAmountHoldersInner.wsaaInciNInvs[2]=wsaaAmountHoldersInner.wsaaInciNInvs[2].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
	}
	/* If the instalment premium is fully allocated, get out and*/
	/* rewrite the INCIRNL record. Otherwise work out premium left*/
	/* to allocate and go round the loop again.*/
	/* If we are allocating a pro-rata premium (ie RNLA-TOTRECD<D9604>*/
	/* not = zero) compare the coverage instalment premium     <D9604>*/
	/* passed not the INCIRNL-CURR-PREM as this is the         <D9604>*/
	/* amount we are allocating.                               <D9604>*/
	/*                                                         <D9604>*/
	if (rnlallDTO.getTotrecd().compareTo(BigDecimal.ZERO)==0) {
		notProrata2050();
		return ;
	}else if (rnlallDTO.getCovrInstprem().compareTo(wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaAllocInitTot)
			.add(wsaaAmountHoldersInner.wsaaAllocAccumTot))==0) {
		wsaaAmountHoldersInner.wsaaInciPrem=BigDecimal.ZERO;
	}
	else {
		wsaaAmountHoldersInner.wsaaInciPrem =rnlallDTO.getCovrInstprem().subtract(wsaaAmountHoldersInner.wsaaNonInvestPremTot).subtract(wsaaAmountHoldersInner.wsaaAllocInitTot)
		.subtract(wsaaAmountHoldersInner.wsaaAllocAccumTot).setScale(3, BigDecimal.ROUND_HALF_UP);
	}
}

protected void notProrata2050(){
	if (incipf.getCurrPrem().compareTo(wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaAllocInitTot).add(wsaaAmountHoldersInner.wsaaAllocAccumTot))==0) {
		wsaaAmountHoldersInner.wsaaInciPrem=BigDecimal.ZERO;
	}
	else {
	
		wsaaAmountHoldersInner.wsaaInciPrem = incipf.getCurrPrem().subtract(wsaaAmountHoldersInner.wsaaNonInvestPremTot).subtract(wsaaAmountHoldersInner.wsaaAllocInitTot)
				.subtract(wsaaAmountHoldersInner.wsaaAllocAccumTot).setScale(3, BigDecimal.ROUND_HALF_UP);
	}
}

protected void callRounding(ZrdecplcDTO zrdecplcDTO) throws IOException{
	/*CALL*/
//	zrdecplcDTO = new ZrdecplcDTO();
	zrdecplcDTO.setCompany(rnlallDTO.getCompany());
	zrdecplcDTO.setCurrency(rnlallDTO.getCntcurr());
	zrdecplcDTO.setBatctrcde(rnlallDTO.getBatctrcde());
	wsaaAmountOut = zrdecplc.convertAmount(zrdecplcDTO);

	/*EXIT*/
}

protected void nearExit1800()
	{
		wsaaAmountHoldersInner.wsaaIndex++;
		/*EXIT*/
	}
	
	protected void a200DeleteZrra()
	{
		/*A200-PARA*/
		/* Delete ZRRA.*/
		if (zrrapf==null) {
			return ;
		}
		
		zrrapfDAO.deleteZrrapfRecord(zrrapf.getUnique_number());
		
	}

	protected void paraInit() {
		rnlallDTO.setStatuz("****");
		if (env.getProperty("vpms.flag") == null) {
			vpmsFlag = false;
		} else {
			vpmsFlag = env.getProperty("vpms.flag").equals("1");
		}

		wsaaAmountHoldersInner.wsaaInciPerd[1]=0;
		wsaaAmountHoldersInner.wsaaInciPerd[2]=0;
		wsaaAmountHoldersInner.wsaaInciInit[1]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciInit[2]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciAlloc[1]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciAlloc[2]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciTot[1]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciTot[2]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciNInvs[1]=BigDecimal.ZERO;
		wsaaAmountHoldersInner.wsaaInciNInvs[2]=BigDecimal.ZERO;
	}
	
	protected void readT6647() throws IOException{
		/* Read T6647 for the relevant details.*/
		TableItem<T6647> t6647 = itempfService.readItemByItemFrm(rnlallDTO.getCompany(), "T6647", rnlallDTO.getBatctrcde().concat(rnlallDTO.getCnttype()),  CommonConstants.IT_ITEMPFX,
				rnlallDTO.getEffdate(), T6647.class);
		
		if(t6647==null) {
			t6647IO = new T6647();
			t6647IO.setProcSeqNo(0);
			return ;
		}else {
			t6647IO = t6647.getItemDetail();
		}
		
	}
	
	protected void readT5540() throws IOException{
		/* Read T5540 for the general unit linked details.*/
		TableItem<T5540> t5540 = itempfService.readItemByItemFrm(rnlallDTO.getCompany(), "T5540", rnlallDTO.getCrtable(),  CommonConstants.IT_ITEMPFX,
				rnlallDTO.getEffdate(), T5540.class);
				if(t5540!=null) {
			
			t5540IO = t5540.getItemDetail();
		}
		
	}
	
	protected void readT5645() throws IOException{
		/* Read T5645 for the SUB ACC details.*/
		TableItem<T5645> t5645 = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T5645", wsaaSubr, CommonConstants.IT_ITEMPFX, T5645.class);
		if(t5645!=null) {
			t5645IO  = t5645.getItemDetail();
		}
	}
	
	protected void readT5688() throws IOException{
		
		TableItem<T5688> t5688 = itempfService.readItemByItemFrm(rnlallDTO.getCompany(), "T5688", rnlallDTO.getCnttype(),  CommonConstants.IT_ITEMPFX,
				rnlallDTO.getMoniesDate(), T5688.class);
				if(t5688!=null) {
			
			t5688IO = t5688.getItemDetail();
		}
	}
	
	protected void readT5519() throws IOException{
		/* Read T5519 for initial unit discount details.*/
		if (("").equals(t5540IO.getIuDiscBasis().trim())) {
			/*        GO TO 290-EXIT.                                  <D9604>*/
			return ;
		}
		

		TableItem<T5519> t5519 = itempfService.readItemByItemFrm(rnlallDTO.getCompany(), "T5519", t5540IO.getIuDiscBasis(),  CommonConstants.IT_ITEMPFX,
				rnlallDTO.getMoniesDate(), T5519.class);
		
		if(t5519!=null) {
			t5519IO = t5519.getItemDetail();
		}
		
	}
	
	protected void readT5729() throws IOException{
		
		itempf = itempfDAO.readSmartTableByTableNameAndItem3(rnlallDTO.getCompany(), "T5729", rnlallDTO.getCnttype(), CommonConstants.IT_ITEMPFX,
				99999999);
		
		if(itempf == null) {
			return ;
		}
		
		wsaaFlexPrem="Y";
	}
	
	protected void readZrra()
	{
		zrrapf = new Zrrapf();
		zrrapf.setChdrcoy(rnlallDTO.getCompany());
		zrrapf.setChdrnum(rnlallDTO.getChdrnum());
		zrrapf.setLife(rnlallDTO.getLife());
		zrrapf.setCoverage(rnlallDTO.getCoverage());
		zrrapf.setRider(rnlallDTO.getRider());
		zrrapf.setPlnsfx(rnlallDTO.getPlanSuffix());
		zrrapf.setInstfrom(rnlallDTO.getDuedate());
		zrrapf.setBillfreq(rnlallDTO.getBillfreq());
		zrrapf = zrrapfDAO.getZrrapfRecord(zrrapf);
	}

	
	protected void setSeqNo()
	{
		/* Add Rider to T6647-SEQ-NO. This will be used to identify*/
		/* Coverages from Riders when reversing UTRN's.*/
		wsaaRiderAlpha=Integer.parseInt(rnlallDTO.getRider());
		wsaaAmountHoldersInner.wsaaSeqNo = wsaaRiderAlpha + t6647IO.getProcSeqNo();
	}
	

protected void systemError570(){
	rnlallDTO.setStatuz("BOMB");
	return ;
}

protected void exit190() {
	if(this.dberror) {
		rnlallDTO.setStatuz("BOMB");
	}
	return ;
}

private boolean checkVPMSFlag() {
	return vpmsFlag;
}

private void fatalError600(String status) {
	throw new StopRunException(status);
}

private enum GotoLabel implements GOTOInterface {
	DEFAULT,  
//	readIncirnl110, 
//	fundAllocation180, 
//	exit190, 
	readZrra285,   
	next4150, 
	accumUnit4200, 
	next4250, 
	nearExit4800, 
	comlvlacc5105, 
	cont5110, 
	writrUtrn5800
}
	/*
	 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
	 */
	private static final class WsaaAmountHoldersInner { 
			/* WSAA-AMOUNT-HOLDERS */
		private BigDecimal wsaaNonInvestPrem = BigDecimal.ZERO;
		private BigDecimal wsaaAllocAmt = BigDecimal.ZERO;
		private BigDecimal wsaaNonInvestPremTot = BigDecimal.ZERO;
		private BigDecimal wsaaAllocInit = BigDecimal.ZERO;
		private BigDecimal wsaaOldAllocInitTot = BigDecimal.ZERO;
		private BigDecimal wsaaAllocInitTot = BigDecimal.ZERO;
		private BigDecimal wsaaRunInitTot = BigDecimal.ZERO;
		private BigDecimal wsaaAllocAccum = BigDecimal.ZERO;
		private BigDecimal wsaaOldAllocAccumTot = BigDecimal.ZERO;
		private BigDecimal wsaaAllocAccumTot = BigDecimal.ZERO;
		private BigDecimal wsaaEnhanceAlloc = BigDecimal.ZERO;
		private BigDecimal wsaaRunAccumTot = BigDecimal.ZERO;
		private BigDecimal wsaaAllocTot = BigDecimal.ZERO;
		private BigDecimal wsaaRunTot = BigDecimal.ZERO;
		private BigDecimal wsaaInitAmount = BigDecimal.ZERO;
		private BigDecimal wsaaAccumAmount = BigDecimal.ZERO;
		private int wsaaIndex = 0;
		private BigDecimal wsaaInciPrem = BigDecimal.ZERO;
		private int wsaaIndex2 = 0;
		private BigDecimal wsaaEnhancePerc = BigDecimal.ZERO;
		private BigDecimal wsaaInitUnitDiscFact = BigDecimal.ZERO;
		private int[] wsaaInciPerd = new int[3];
		private BigDecimal[] wsaaInciprm ;
		private BigDecimal[] wsaaInciInit = new BigDecimal[3];
		private BigDecimal[] wsaaInciAlloc = new BigDecimal[3];
		private BigDecimal[] wsaaInciTot = new BigDecimal[3];
		private BigDecimal[] wsaaInciNInvs = new BigDecimal[3];
		private BigDecimal wsaaContractAmount;
		private BigDecimal wsaaFundPc = BigDecimal.ZERO;
		private int wsaaSeqNo = 0;
		private BigDecimal wsaaTotUlSplit = BigDecimal.ZERO;
		private BigDecimal wsaaTotIbSplit = BigDecimal.ZERO;
	}
	
	
	public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}
}
