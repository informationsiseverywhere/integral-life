package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T5399 {

	private List<String> covPremStats;	
	private List<String> covRiskStats;
	private List<String> setCnPremStats;
	private List<String> setCnRiskStats;
	
	public List<String> getCovPremStat() {
		return covPremStats;
	}
	public void setCovPremStats(List<String> covPremStats) {
		this.covPremStats = covPremStats;
	}
	public List<String> getCovRiskStats() {
		return covRiskStats;
	}
	public void setCovRiskStats(List<String> covRiskStats) {
		this.covRiskStats = covRiskStats;
	}
	public List<String> getSetCnPremStats() {
		return setCnPremStats;
	}
	public void setSetCnPremStats(List<String> setCnPremStats) {
		this.setCnPremStats = setCnPremStats;
	}
	public List<String> getSetCnRiskStats() {
		return setCnRiskStats;
	}
	public void setSetCnRiskStats(List<String> setCnRiskStats) {
		this.setCnRiskStats = setCnRiskStats;
	}

}
