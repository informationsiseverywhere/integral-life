package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Incrpf;

public interface IncrpfDAO {

	List<Incrpf> readValidIncrpf(String company, String contractNumber);
	public List<Incrpf> readInValidIncrpf(String company, String contractNumber, String life, String coverage,
			String rider, int plnsfx);
	public List<Incrpf> readIncrRecord(String company, String chdrnum);
	public void insertIncrpfRecords(List<Incrpf> incrpfList);
	public List<Incrpf> readIncrpfList(String company, String contractNumber, String life, String coverage,String rider, int plnsfx);
}
