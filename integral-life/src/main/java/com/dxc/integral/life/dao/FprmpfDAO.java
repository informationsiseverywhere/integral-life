package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Fprmpf;

public interface FprmpfDAO {
	List<Fprmpf> readFprmpfRecords(String company, String contractNumber);
	public Fprmpf readFprmpfBySeqno(String chdrcoy, String chdrnum, int payrseqno);
	public void updateFprmpfRecords(List<Fprmpf> fprmpfList);
}
