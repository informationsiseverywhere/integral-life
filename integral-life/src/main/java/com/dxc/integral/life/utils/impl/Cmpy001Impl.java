package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.AgentDAO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Agent;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5564;
import com.dxc.integral.life.smarttable.pojo.T5695;
import com.dxc.integral.life.utils.Cmpy001;
import com.dxc.integral.life.utils.VPMScaller;

@Service("cmpy001")
@Lazy
public class Cmpy001Impl implements Cmpy001{
	@Autowired
    private ChdrpfDAO chdrpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private AgentDAO agntpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private VPMScaller vpmscaller;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Environment env;
	
	private BigDecimal wsaaFreq;
	private T5695 t5695IO;
	private int wsaaX;
	private int wsaaY;
	private int wsaaFreqTo=0;
	private int wsaaFreqFrom=0;
	
	public ComlinkDTO processCmpy001(ComlinkDTO comlinkDTO) throws Exception{
		comlinkDTO.setStatuz("****");
		if (!obtainTables(comlinkDTO)) {
			return comlinkDTO;
		}
//		if(wsaaX==-1) {
//			return comlinkDTO;
//		}
		Chdrpf chdrpf;
		if (checkVPMSFlag() && "00".equals(comlinkDTO.getBillfreq())) {
			chdrpf = chdrpfDAO.getLPContractWithoutValidflag(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum());
		}	
		if (!(checkVPMSFlag()&& "01".equals(comlinkDTO.getCoverage())  && "00".equals(comlinkDTO.getRider()))) {
			comlinkDTO = calcCommamntPayable(comlinkDTO);
			comlinkDTO = calcEarnedComm(comlinkDTO);
		}else {
			String jlife = comlinkDTO.getJlife();
			
			Lifepf lifepf = lifepfDAO.getLifecmcRecord(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum(), comlinkDTO.getLife(), jlife.trim().equals("")?"00":jlife);
			if(lifepf == null) {
				return comlinkDTO;
			}
			chdrpf = chdrpfDAO.getLPContractWithoutValidflag(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum());
            Agent agntpf = agntpfDAO.readAgentByCompanyAndAgentNumber(chdrpf.getAgntcoy(), chdrpf.getAgntnum());
            if(agntpf == null) {
				return comlinkDTO;
			}
			
            comlinkDTO.setAgentype(agntpf.getAgtype());
			
			comlinkDTO.setPtdate(chdrpf.getPtdate().intValue());
			//ILIFE-6077
			//comlinkrec.ptdate.add(1);
			comlinkDTO.setSrcebus("");
			comlinkDTO.setCnttype(chdrpf.getCnttype());
			comlinkDTO.setCltdob(lifepf.getCltdob());
			comlinkDTO.setAnbccd(lifepf.getAnbAtCcd());
			if ("".equals(comlinkDTO.getAgentype().trim()))
				comlinkDTO.setAgentype("**");
			
			VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
			vpmsinfoDTO.setCoy(comlinkDTO.getChdrcoy());
			vpmsinfoDTO.setTransDate(String.valueOf(comlinkDTO.getEffdate()));
			vpmsinfoDTO.setModelName("VPMCMCPMT");
			
			comlinkDTO.setAgentype("**");
			vpmscaller.callPRMPMREST(vpmsinfoDTO, null, null, null);
			if (!"****".equals(comlinkDTO.getStatuz())) {
				fatalError("Cmpy001: Exception in premiumrec:" + comlinkDTO.getStatuz());
			}
			
		}
		return comlinkDTO;
	}
	
	protected ComlinkDTO calcEarnedComm(ComlinkDTO comlinkDTO){
		BigDecimal wsaaCommperc=BigDecimal.ZERO;
		BigDecimal wsaaCommAmnt1=BigDecimal.ZERO;
		BigDecimal wsaaCommAmnt2=BigDecimal.ZERO;
		BigDecimal wsaaCommPrev=BigDecimal.ZERO;
		BigDecimal wsaaComm=BigDecimal.ZERO;
		BigDecimal wsaaErndperc=BigDecimal.ZERO;
		BigDecimal wsaaCommErnd=BigDecimal.ZERO;
		int wsaaIndex=1;
		
		 for(; wsaaIndex<=wsaaY;wsaaIndex++) {
			 wsaaErndperc.add(t5695IO.getCommenpcs().get(wsaaIndex));
		}
		 wsaaCommAmnt1 = comlinkDTO.getIcommtot().multiply(wsaaCommperc).divide(new BigDecimal(100), 3, BigDecimal.ROUND_HALF_UP); 
		 if (wsaaY!=0) {
			wsaaCommAmnt2 = comlinkDTO.getAnnprem().multiply(new BigDecimal(wsaaY)).divide(new BigDecimal(100), 3, BigDecimal.ROUND_HALF_UP);
		 }
		 
		 if (wsaaCommAmnt1.compareTo(wsaaCommAmnt2)<0) {
				wsaaCommPrev=wsaaCommAmnt1;
			}else {
				wsaaCommPrev=wsaaCommAmnt2;
			}
		
		
		 wsaaErndperc = wsaaErndperc.add(t5695IO.getCommenpcs().get(wsaaX));
		 wsaaCommAmnt1 = comlinkDTO.getIcommtot().multiply(wsaaErndperc).divide(new BigDecimal(100) , 3, BigDecimal.ROUND_HALF_UP);
		 wsaaCommAmnt2 = comlinkDTO.getAnnprem().multiply(t5695IO.getPremaxpcs().get(wsaaX)).divide(new BigDecimal(100) , 3, BigDecimal.ROUND_HALF_UP);
		 if (wsaaCommAmnt1.compareTo(wsaaCommAmnt2)<0) {
			 wsaaComm=wsaaCommAmnt1;
			}
			else {
			wsaaComm=wsaaCommAmnt2;
			}
		 wsaaCommErnd=  (wsaaCommPrev.add(((wsaaComm.subtract(wsaaCommPrev)).multiply(wsaaFreq.subtract(new BigDecimal(wsaaFreqFrom)).add(BigDecimal.ONE)))
				 .divide(new BigDecimal(wsaaFreqTo-wsaaFreqFrom+1)))).subtract(comlinkDTO.getIcommernd()).setScale(3);
			
		 comlinkDTO.setErndamt(wsaaCommErnd);
		 
		 return comlinkDTO;
		
	}
	
	protected ComlinkDTO calcCommamntPayable(ComlinkDTO comlinkDTO){
		BigDecimal wsaaCommperc=BigDecimal.ZERO;
		BigDecimal wsaaCommAmnt1=BigDecimal.ZERO;
		BigDecimal wsaaCommAmnt2=BigDecimal.ZERO;
		BigDecimal wsaaCommPrev=BigDecimal.ZERO;
		BigDecimal wsaaComm=BigDecimal.ZERO;
		BigDecimal wsaaCommPybl=BigDecimal.ZERO;
		int wsaaIndex=1;
//		wsaaFreqTo=0;
//		wsaaFreqFrom=0;
		wsaaY = wsaaX;
        for(; wsaaIndex<=wsaaY;wsaaIndex++) {
        	
        	wsaaCommperc.add(t5695IO.getCmrates().get(wsaaIndex));
        }
     
        wsaaCommAmnt1 = comlinkDTO.getIcommtot().multiply(wsaaCommperc).divide(new BigDecimal(100), 3, BigDecimal.ROUND_HALF_UP);
		if (wsaaY!=0) {
		wsaaCommAmnt2 = comlinkDTO.getAnnprem().multiply(new BigDecimal(wsaaY)).divide(new BigDecimal(100), 3, BigDecimal.ROUND_HALF_UP);
		}
		if (wsaaCommAmnt1.compareTo(wsaaCommAmnt2)<0) {
			wsaaCommPrev=wsaaCommAmnt1;
		}
		else {
			wsaaCommPrev=wsaaCommAmnt2;
		}
		wsaaCommperc = wsaaCommperc.add(t5695IO.getCmrates().get(wsaaX));
		wsaaCommAmnt1 = comlinkDTO.getIcommtot().multiply(wsaaCommperc).divide(new BigDecimal(100) , 3, BigDecimal.ROUND_HALF_UP);
		wsaaCommAmnt2 = comlinkDTO.getAnnprem().multiply(t5695IO.getPremaxpcs().get(wsaaX)).divide(new BigDecimal(100) , 3, BigDecimal.ROUND_HALF_UP);
		if (wsaaCommAmnt1.compareTo(wsaaCommAmnt2)<0) {
			wsaaComm=wsaaCommAmnt1;
		}
		else {
			wsaaComm=wsaaCommAmnt2;
		}
		
		try {
			wsaaFreqTo=Integer.parseInt(t5695IO.getFreqTos().get(wsaaX));
			wsaaFreqFrom=Integer.parseInt(t5695IO.getFreqFroms().get(wsaaX));
		}catch(Exception e) {
			e.printStackTrace();
			wsaaFreqTo=0;
			wsaaFreqFrom=0;
		}

		wsaaCommPybl = wsaaCommPrev.add(((wsaaComm.subtract(wsaaCommPrev)).multiply(wsaaFreq.subtract(new BigDecimal(wsaaFreqFrom)).add(BigDecimal.ONE))).divide((new BigDecimal(wsaaFreqTo-wsaaFreqFrom)).add(BigDecimal.ONE)))
		.subtract(comlinkDTO.getIcommpd()).setScale(3);
		
		comlinkDTO.setPayamnt(wsaaCommPybl);
		
		return comlinkDTO;

	}
	
	protected boolean obtainTables(ComlinkDTO comlinkDTO) throws Exception{
        Chdrpf chdrpf = null;
		
		TableItem<T5564> t5564 =itempfService.readSmartTableByTableNameAndItem(comlinkDTO.getChdrcoy(), "T5564", comlinkDTO.getMethod().concat(comlinkDTO.getCrtable()), CommonConstants.IT_ITEMPFX, T5564.class);
        if(t5564==null) {
        	return false;
        }
		T5564 t5564IO = t5564.getItemDetail();
		String wsaaT5695Key="";
		for (int i=0; i<=11; i++){
			if (comlinkDTO.getBillfreq().equals(t5564IO.getFreqs().get(i))) {
				wsaaT5695Key=t5564IO.getItmkeys().get(i);
			}
		}
		
		TableItem<T5695> t5695 =itempfService.readSmartTableByTableNameAndItem(comlinkDTO.getChdrcoy(), "T5695", wsaaT5695Key, CommonConstants.IT_ITEMPFX, T5695.class);
		if(t5695==null) {
	        	return false;
	        }
		t5695IO = t5695.getItemDetail();
		if("00".equals(comlinkDTO.getBillfreq()) || "".equals(comlinkDTO.getBillfreq().trim())) {
			wsaaFreq = BigDecimal.ZERO;
			bypassDatcon(t5695IO);
			if(wsaaX==-1) return false;
			return true;
		}
		int date2;
	    if(comlinkDTO.getPtdate()!=0) {
	    	date2 = comlinkDTO.getPtdate();
	    }else {
	    	chdrpf = chdrpfDAO.getLPContractInfo(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum());
	    	date2 = chdrpf.getPtdate();
	    	
	    }
	    wsaaFreq = datcon3.getTerms(comlinkDTO.getEffdate(), date2, comlinkDTO.getBillfreq());

	    if(StringUtils.equalsIgnoreCase("ADDC", comlinkDTO.getFunction())) {
	    	wsaaFreq = wsaaFreq.add(new BigDecimal(0.99999)).setScale(5);
	    }
				
	    bypassDatcon(t5695IO);	
	    if(wsaaX==-1) return false;
	    return true;			
				
	}
	
	protected void bypassDatcon(T5695 t5695IO){
		wsaaX=-1;
		for (int i=0; i<=10; i++){
			if(!isNumeric(t5695IO.getFreqFroms().get(i)) || !isNumeric(t5695IO.getFreqFroms().get(i))) {
				continue;
			}
			wsaaFreqFrom=Integer.parseInt(t5695IO.getFreqFroms().get(i));
			wsaaFreqTo=Integer.parseInt(t5695IO.getFreqTos().get(i));
			
			if (wsaaFreq.compareTo(new BigDecimal(wsaaFreqFrom))>=0&&wsaaFreq.compareTo(new BigDecimal(wsaaFreqTo))<=0) {
				wsaaX=i;
			}
		}
		/*EXIT*/
	}
	

	

	protected boolean isNumeric(String wsaaNumberic) {
		return StringUtils.isNumeric(wsaaNumberic)&&!StringUtils.isBlank(wsaaNumberic);
	}
	
//	protected Long callDatcon3(int date1, int date2, String method) throws Exception{
//		switch(method){
//		case "01": 
//			return datcon3.getYearsDifference(date1, date2);
//		case "DY":
//			return datcon3.getDaysDifference(date1, date2);
//		case "12":
//			return datcon3.getMonthsDifference(date1, date2);
//		case "02":
//			return datcon3.getMonthsDifference(date1, date2);
////		case "04":
////			return datcon3.getMonthsDifference(date1, date2);
////		case "03": 
////			return datcon3.getYearsDifference(date1, date2);
////		case "05": 
////			return datcon3.getYearsDifference(date1, date2);
//		}
//		return null;
//	}
	
	private boolean checkVPMSFlag() {
		return env.getProperty("vpms.flag").equals("1");
	}
	
	private void fatalError(String status) {
		throw new StopRunException(status);
	}
}
