package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.TotloanInputDTO;
import com.dxc.integral.life.beans.TotloanOutputDTO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Hrtotlon;
import com.dxc.integral.life.utils.Intcalc;

/**
 * The Hrtotlon utility implementation.
 * 
 * Calculates and returns Loan principal and Loan interest to calling program.
 *
 * @author mmalik25
 */

@Service("hrtotlon")
@Lazy
public class HrtotlonImpl implements Hrtotlon {

	@Autowired
	private AcmvpfDAO acmvpfDAO;

	@Autowired
	private DescpfDAO descpfDAO;

	@Autowired
	private Xcvrt xcvrt;

	@Autowired
	private Intcalc intcalc;

	@Autowired
	private Zrdecplc zrdecplc;

	@Autowired
	private LifacmvImpl lifacmv;

	@Autowired
	private LoanpfDAO loanpfDAO;

	@Autowired
	private Datcon2 datcon2;
	
	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/

	private int sequenceNo;
	
	private String transactionCode;
	
	public static final String DAY28 = "28";
	public static final String DAY29 = "29";
	public static final String DAY30 = "30";
	public static final String DAY31 = "31";
	private static final Collection<Month> MONTH_WITH_30DAYS = Arrays.asList(Month.APRIL, Month.JUNE, Month.SEPTEMBER,
			Month.NOVEMBER);


	/*
	 * 
	 * @see com.dxc.integral.life.utils.Hrtotlon#processLoanList
	 * (com.dxc.integral.life.beans.TotloanDTO, java.lang.Integer,
	 * com.dxc.integral.iaf.dao.model.Chdrpf)
	 */
	@Override/*ILIFE-5977*/
	public TotloanOutputDTO processLoanList(TotloanInputDTO totloanDTO, Chdrpf chdrpf) throws IOException,ParseException {//ILIFE-5763
		sequenceNo = 0;
		transactionCode = totloanDTO.getBatctrcde();
		T5645 t5645IO = getT5645Item(totloanDTO.getChdrcoy());/*ILIFE-5977*/  /*IJTI-379*/

		List<Loanpf> loanpfList = loanpfDAO.getLoanpfListByCompanyAndContractNumber(totloanDTO.getChdrcoy(),
				totloanDTO.getChdrnum());

		TotloanOutputDTO totloanOutputDTO = new TotloanOutputDTO();
		//String longDesc = descpfDAO.getDescInfo(Companies.LIFE, "T5645", "HRTOTLON").getLongdesc();
		String longDesc = descpfDAO.getDescInfo(totloanDTO.getChdrcoy(), "T5645", "HRTOTLON").getLongdesc(); /*IJTI-379*/
		for (Loanpf loan : loanpfList) {
			totloanOutputDTO = processLoan(totloanDTO, loan, longDesc, t5645IO, chdrpf, totloanOutputDTO);/*ILIFE-5977*/
		}
		return totloanOutputDTO;
	}

	/**
	 * Process loan record.
	 * 
	 * @param totloanDTO
	 * @param loanpf
	 * @param longDescription
	 * @param t5645IO
	 * @param chdrpf
	 * @param totloanOutputDTO
	 * @return
	 * @throws IOException
	 *//*ILIFE-5977*/
	private TotloanOutputDTO processLoan(TotloanInputDTO totloanDTO, Loanpf loanpf, String longDescription, T5645 t5645IO,
			Chdrpf chdrpf, TotloanOutputDTO totloanOutputDTO) throws IOException,ParseException {//ILIFE-5763

		BigDecimal intcInterest = BigDecimal.ZERO;
		IntcalcDTO intercalDTO = new IntcalcDTO();

		if (!("A".equals(loanpf.getLoantype()) || "P".equals(loanpf.getLoantype()))) {
			return totloanOutputDTO;
		}

		totloanOutputDTO.setLoanCount(totloanOutputDTO.getLoanCount() + 1);
		int indx = 0;
		if ("P".equals(loanpf.getLoantype())) {
			indx = 0;
		} else if ("A".equals(loanpf.getLoantype())) {
			indx = 4;
		} else if ("E".equals(loanpf.getLoantype())) {
			indx = 8;
		}

		BigDecimal calculatedInterestAmount = sumInterestAcmvs(loanpf, chdrpf, t5645IO, indx);
		totloanOutputDTO.setInterest(totloanOutputDTO.getInterest().add(calculatedInterestAmount));
		if (loanpf.getLstintbdte() < totloanDTO.getTranDate()) {
			intercalDTO.setLoanNumber(loanpf.getLoannumber());
			intercalDTO.setChdrcoy(loanpf.getChdrcoy());
			intercalDTO.setChdrnum(loanpf.getChdrnum());
			intercalDTO.setCnttype(chdrpf.getCnttype());
			intercalDTO.setInterestTo(totloanDTO.getTranDate());
			intercalDTO.setInterestFrom(loanpf.getLstintbdte());
			intercalDTO.setLoanorigam(loanpf.getLstcaplamt());
			intercalDTO.setLastCaplsnDate(loanpf.getLstcapdate());
			intercalDTO.setLoanStartDate(loanpf.getLoanstdate());
			intercalDTO.setLoanCurrency(loanpf.getLoancurr());
			intercalDTO.setLoanType(loanpf.getLoantype());

			BigDecimal totalIntrest = intcalc.interestCalculation(intercalDTO);
			totalIntrest = callRounding(totalIntrest, loanpf.getLoancurr(), loanpf.getChdrcoy());
			intcInterest = totalIntrest;
			if (!loanpf.getLoancurr().equals(chdrpf.getCntcurr())) {
				ConlinkOutputDTO conlinkOutput = callXcvrt(totalIntrest, loanpf.getLoancurr(), chdrpf.getCntcurr(),
						loanpf.getChdrcoy());/*ILIFE-5977*/
				BigDecimal roundedAmount = callRounding(totalIntrest, loanpf.getLoancurr(), loanpf.getChdrcoy());
				conlinkOutput.setCalculatedAmount(roundedAmount);
				totalIntrest = conlinkOutput.getCalculatedAmount();
			}
			totloanOutputDTO.setInterest(totloanOutputDTO.getInterest().add(totalIntrest));
		}

		BigDecimal calculatedPrincipalAmount = sumLoanPrincipal(loanpf, chdrpf, t5645IO, indx);
		totloanOutputDTO.setPrincipal(totloanOutputDTO.getPrincipal().add(calculatedPrincipalAmount));
		if (totloanDTO.getTranDate() != loanpf.getLoanstdate()) {
			if (!(loanpf.getLoancurr().equals(chdrpf.getCntcurr()))) {

				ConlinkOutputDTO conlinkOutput = callXcvrt(loanpf.getLstcaplamt(), loanpf.getLoancurr(), chdrpf.getCntcurr(),
						loanpf.getChdrcoy());/*ILIFE-5977*/
				BigDecimal roundedAmount = callRounding(conlinkOutput.getCalculatedAmount(), loanpf.getLoancurr(), loanpf.getChdrcoy());
				conlinkOutput.setCalculatedAmount(roundedAmount);
				totloanOutputDTO.setPrincipal(totloanOutputDTO.getPrincipal().add(conlinkOutput.getCalculatedAmount()));
			} else {
				totloanOutputDTO.setPrincipal(totloanOutputDTO.getPrincipal().add(loanpf.getLstcaplamt()));
			}
		}
		if ("Y".equals(totloanDTO.getPostFlag())) {
			postInterestAcmv(totloanDTO, indx, intcInterest, longDescription, chdrpf.getCnttype(), t5645IO, loanpf);
			updateLoanpf(loanpf, chdrpf, totloanDTO.getEffectiveDate());/*ILIFE-5977*/
		}
		return totloanOutputDTO;
	}

	/**
	 * @param loanpf
	 * @param chdrpf
	 * @param t5645IO
	 * @param indx
	 * @return
	 * @throws IOException
	 */
	private BigDecimal sumInterestAcmvs(Loanpf loanpf, Chdrpf chdrpf, T5645 t5645IO, int indx)/*ILIFE-5977*/
			throws IOException {

		String sacscode = t5645IO.getSacscodes().get(indx + 1);
		String sacstype = t5645IO.getSacstypes().get(indx + 1);
		String sign = t5645IO.getSigns().get(indx + 1);

		StringBuilder rldgacct = new StringBuilder(loanpf.getChdrnum());
		if(loanpf.getLoannumber() < 10){
			rldgacct.append("0"+loanpf.getLoannumber().toString());
		} else {
			rldgacct.append(loanpf.getLoannumber().toString());
		}

		List<Acmvpf> acmvpfList = acmvpfDAO.getAcmvpfList(rldgacct.toString(), sacscode, sacstype,
				loanpf.getLstcapdate(), chdrpf.getChdrcoy());

		BigDecimal postedAmount = BigDecimal.ZERO;
		for (Acmvpf acmvpf : acmvpfList) {
			postedAmount = postedAmount
					.add(processAcmvloans(acmvpf, chdrpf, sign, loanpf.getChdrcoy()));
		}
		return postedAmount;
	}

	/**
	 * 
	 * @param acmv
	 * @param chdrpf
	 * @param t5645sign
	 * @param company
	 * @return
	 * @throws IOException
	 */
	private BigDecimal processAcmvloans(Acmvpf acmv, Chdrpf chdrpf, String t5645sign, String company)/*ILIFE-5977*/
			throws IOException {

		if ("+".equals(t5645sign)) {
			acmv.getOrigamt().round(new MathContext(2, RoundingMode.CEILING));
			acmv.setOrigamt(acmv.getOrigamt().multiply(new BigDecimal(-1)));
		}
		if (!acmv.getOrigcurr().equals(chdrpf.getCntcurr())) {
			ConlinkOutputDTO conlinkOutput = callXcvrt(acmv.getOrigamt(), acmv.getOrigcurr(), chdrpf.getCntcurr(),
					company);/*ILIFE-5977*/
			BigDecimal roundAmount = callRounding(conlinkOutput.getCalculatedAmount(), chdrpf.getCntcurr(), chdrpf.getChdrcoy());
			conlinkOutput.setCalculatedAmount(roundAmount);
			if ("-".equals(acmv.getGlsign())) {
				conlinkOutput.getCalculatedAmount().round(new MathContext(2, RoundingMode.CEILING));
				conlinkOutput.setCalculatedAmount(conlinkOutput.getCalculatedAmount().multiply(new BigDecimal(-1)));
			}
			return conlinkOutput.getCalculatedAmount();

		} else {
			if ("-".equals(acmv.getGlsign())) {
				acmv.getOrigamt().round(new MathContext(2, RoundingMode.CEILING));
				acmv.setOrigamt(acmv.getOrigamt().multiply(new BigDecimal(-1)));
			}
			return acmv.getOrigamt();
		}
	}

	/**
	 * Call Zrdecplc utility for rounding.
	 * 
	 * @param amountIn
	 * @param orignalCurr
	 * @return
	 * @throws IOException
	 */
	private BigDecimal callRounding(BigDecimal amountIn, String orignalCurr, String company) throws IOException {/*ILIFE-5977*/
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(orignalCurr);
		zrdecplDTO.setBatctrcde(transactionCode);
		zrdecplDTO.setCompany(company);
		return zrdecplc.convertAmount(zrdecplDTO);/*ILIFE-5977*/
	}

	/**
	 * Prepare sum of loan principal.
	 * 
	 * @param loanpf
	 * @param chdrpf
	 * @param t5645IO
	 * @param indx
	 * @return
	 * @throws IOException
	 */
	private BigDecimal sumLoanPrincipal(Loanpf loanpf, Chdrpf chdrpf, T5645 t5645IO, int indx)/*ILIFE-5977*/
			throws IOException {

		String sacscode = t5645IO.getSacscodes().get(indx);
		String sacstype = t5645IO.getSacstypes().get(indx);
		String sign = t5645IO.getSigns().get(indx);

		BigDecimal postedAmount = BigDecimal.ZERO;
		
		StringBuilder rldgacct = new StringBuilder(loanpf.getChdrnum());
		if(loanpf.getLoannumber() < 10){
			rldgacct.append("0"+loanpf.getLoannumber().toString());
		} else {
			rldgacct.append(loanpf.getLoannumber().toString());
		}

		List<Acmvpf> acmvpfloanList = acmvpfDAO.getAcmvpfList(rldgacct.toString(), sacscode, sacstype,
				loanpf.getLstcapdate(), chdrpf.getChdrcoy());
		for (Acmvpf acmv : acmvpfloanList) {
			postedAmount = postedAmount.add(processAcmvloans(acmv, chdrpf, sign, loanpf.getChdrcoy()));/*ILIFE-5977*/
		}
		return postedAmount;
	}

	/**
	 * Post ACMV record.
	 * 
	 * @param totloanDTO
	 * @param indx
	 * @param intcInterest
	 * @param longDescription
	 * @param cntType
	 * @param t5645IO
	 * @param loanpf
	 * @throws IOException
	 *//*ILIFE-5977*/
	private void postInterestAcmv(TotloanInputDTO totloanDTO, int indx, BigDecimal intcInterest, String longDescription,
			String cntType, T5645 t5645IO, Loanpf loanpf)
			throws IOException,ParseException {//ILIFE-5763

		if (intcInterest.compareTo(BigDecimal.valueOf(0)) == 0) {
			return;
		}
		LifacmvDTO lifeacmvDTO = new LifacmvDTO();
		lifeacmvDTO.setRdocnum(totloanDTO.getChdrnum());
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifeacmvDTO.setBatccoy(totloanDTO.getChdrcoy());
		lifeacmvDTO.setRldgcoy(totloanDTO.getChdrcoy());
		lifeacmvDTO.setGenlcoy(totloanDTO.getChdrcoy());

		lifeacmvDTO.setBatcactmn(totloanDTO.getBatcactmn());
		lifeacmvDTO.setBatcactyr(totloanDTO.getBatcactyr());
		lifeacmvDTO.setBatcbatch(totloanDTO.getBatcbatch());
		lifeacmvDTO.setBatcbrn(totloanDTO.getBatcbrn());
		lifeacmvDTO.setBatccoy(totloanDTO.getBatccoy());
		lifeacmvDTO.setBatcbrn(totloanDTO.getBatcbrn());
		lifeacmvDTO.setBatccoy(totloanDTO.getBatccoy());
		lifeacmvDTO.setGenlcur("");
		lifeacmvDTO.setOrigcurr(loanpf.getLoancurr());
		lifeacmvDTO.setOrigamt(intcInterest);
		lifeacmvDTO.setRcamt(new BigDecimal(0));
		lifeacmvDTO.setCrate(new BigDecimal(0));
		lifeacmvDTO.setAcctamt(new BigDecimal(0));
		lifeacmvDTO.setContot(0);
		lifeacmvDTO.setTranno(totloanDTO.getTranno());
		lifeacmvDTO.setFrcdate(99999999);
		lifeacmvDTO.setEffdate(totloanDTO.getEffectiveDate());
		lifeacmvDTO.setTranref(totloanDTO.getChdrnum());
		lifeacmvDTO.setTrandesc(longDescription);
		lifeacmvDTO.setRldgacct(totloanDTO.getChdrnum() + loanpf.getLoannumber());
		lifeacmvDTO.setTransactionDate(totloanDTO.getTranDate());
		lifeacmvDTO.setTransactionTime(totloanDTO.getTranTime());
		lifeacmvDTO.setUser(totloanDTO.getTranUser());
		lifeacmvDTO.setTermid(totloanDTO.getTranTerm());
		List<String> codeList = new ArrayList<>();
		codeList.add(cntType);
		lifeacmvDTO.setSubstituteCodes(codeList);
		lifeacmvDTO.setSacscode(t5645IO.getSacscodes().get(indx + 2));
		lifeacmvDTO.setSacstyp(t5645IO.getSacstypes().get(indx + 2));
		lifeacmvDTO.setGlcode(t5645IO.getGlmaps().get(indx + 2));
		lifeacmvDTO.setGlsign(t5645IO.getSigns().get(indx + 2));
		lifacmv.executePSTW(lifeacmvDTO);/*ILIFE-5977*/

		lifeacmvDTO.setSacscode(t5645IO.getSacscodes().get(indx + 3));
		lifeacmvDTO.setSacstyp(t5645IO.getSacstypes().get(indx + 3));
		lifeacmvDTO.setGlcode(t5645IO.getGlmaps().get(indx + 3));
		lifeacmvDTO.setGlsign(t5645IO.getSigns().get(indx + 3));
		sequenceNo++;
		lifeacmvDTO.setJrnseq(sequenceNo);
		lifacmv.executePSTW(lifeacmvDTO);/*ILIFE-5977*/
	}

	/**
	 * Updates LOANPF table.
	 * 
	 * @param loanpf
	 * @param chdrpf
	 * @param effectiveDate
	 * @throws IOException
	 */
	private void updateLoanpf(Loanpf loanpf, Chdrpf chdrpf, Integer effectiveDate)
			throws IOException {/*ILIFE-5977*/
		//Map<String, Itempf> t6633Map=itempfDAO.readSmartTableByTableName2(Companies.LIFE, "T6633", CommonConstants.IT_ITEMPFX);
		Map<String, Itempf> t6633Map=itempfDAO.readSmartTableByTableName2(loanpf.getChdrcoy(), "T6633", CommonConstants.IT_ITEMPFX);
		T6633 t6633Item = smartTableDataUtils.convertGenareaToPojo(t6633Map.get(chdrpf.getCnttype()+loanpf.getLoantype()).getGenareaj(), T6633.class); /*IJTI-398*/
		loanpf.setLstintbdte(effectiveDate);
		if ("Y".equals(t6633Item.getLoanAnnivInterest())) {
			Integer newDate = 0;
			while (newDate <= effectiveDate) {
				newDate = datcon2.plusYears(loanpf.getLoanstdate(), 1);
			}
			loanpf.setNxtintbdte(newDate);
			loanpfDAO.updateLoanpf(loanpf);
			return;
		}
		if ("Y".equals(t6633Item.getPolicyAnnivInterest())) {
			Integer newDate = 0;
			while (newDate <= effectiveDate) {
				newDate = datcon2.plusYears(chdrpf.getOccdate(), 1);
			}

			loanpf.setNxtintbdte(newDate);
			loanpfDAO.updateLoanpf(loanpf);
			return;
		}
		
		LocalDate loanDate = LocalDate.parse(loanpf.getLoanstdate().toString(), DateTimeFormatter.ofPattern("yyMMdd"));
		String newDay;
		String interestDay="";
		if (t6633Item.getInterestDay() != null && t6633Item.getInterestDay() != 0) {
			if (t6633Item.getInterestDay() < 10) {
				interestDay = "0" + t6633Item.getInterestDay().toString();
			} else {
				interestDay = t6633Item.getInterestDay().toString();
			}
			if (interestDay.compareTo(DAY28) > 0) {
				if (interestDay.equals(DAY31)  && MONTH_WITH_30DAYS.contains(loanDate.getMonth())) {
					newDay = DAY30;
				} else if (loanDate.getMonth().equals(Month.FEBRUARY)) {
					if (loanDate.isLeapYear()) {
						newDay = DAY29;
					} else {
						newDay = DAY28;
					}
				} else {
					newDay = interestDay;
				}
			} else {
				newDay = interestDay;
			}
		} else {
			newDay = loanpf.getLoanstdate().toString().substring(6, 8);
		}
		
		int nextIntBillDate = Integer.parseInt(Integer.toString(loanpf.getLoanstdate()).substring(0, 6) + newDay);
		int calculatedDate = 0;
		while (calculatedDate <= effectiveDate) {
			if (t6633Item.getInterestFrequency() == null || "".equals(t6633Item.getInterestFrequency())) {
				calculatedDate = datcon2.plusYears(nextIntBillDate, 1);
			} else {
				calculatedDate = getDatebyfrequency(nextIntBillDate, t6633Item.getInterestFrequency());
			}
		}
		
		loanpf.setNxtintbdte(calculatedDate);
		if (interestDay.compareTo(DAY28) > 0 && (loanDate.getMonth().equals(Month.FEBRUARY)
				|| (interestDay.equals(DAY31) && MONTH_WITH_30DAYS.contains(loanDate.getMonth())))){
			loanpf.setNxtintbdte(Integer.parseInt(Integer.toString(calculatedDate).substring(0, 6) + interestDay));
		}
		loanpfDAO.updateLoanpf(loanpf);
	}
	
	/**
	 * Get date by frequency.
	 * 
	 * @param nxtcapdate
	 * @param frequency
	 * @return
	 */
	private Integer getDatebyfrequency(Integer nxtcapdate, String frequency) {
		Integer nextdate = null;
		if ("12".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 1);
		}
		if ("02".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 6);
		}
		if ("04".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 3);
		}
		return nextdate;
	}

	/**
	 * Get T5645 smart table item.
	 * 
	 * @return
	 * @throws IOException
	 */ 
	private T5645 getT5645Item(String comapny) throws IOException {/*ILIFE-5977*/  /*IJTI-379*/
		Map<String, List<Itempf>> t5645Map=itempfDAO.readSmartTableByTableName(comapny, "T5645", CommonConstants.IT_ITEMPFX);  /*IJTI-379*/
		List<Itempf> t5645ItemList = t5645Map.get("HRTOTLON");
		return smartTableDataUtils.convertGenareaToPojo(t5645ItemList.get(0).getGenareaj(), T5645.class); /*IJTI-398*/
	}

	/**
	 * Calls Xcvrt utility.
	 * 
	 * @param amount
	 * @param fromCurr
	 * @param toCurr
	 * @param company
	 * @return
	 * @throws IOException
	 */
	private ConlinkOutputDTO callXcvrt(BigDecimal amount, String fromCurr, String toCurr, String company) throws IOException {/*ILIFE-5977*/
		ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
		conlinkInput.setAmount(amount);
		conlinkInput.setFromCurrency(fromCurr);
		conlinkInput.setCashdate(99999999);
		conlinkInput.setToCurrency(toCurr);
		conlinkInput.setCompany(company);
		return xcvrt.executeRealFunction(conlinkInput);/*ILIFE-5977*/

	}

}
