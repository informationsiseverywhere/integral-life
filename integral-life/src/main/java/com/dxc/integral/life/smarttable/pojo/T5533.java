package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5533 {
  	private List<BigDecimal> cmaxs;
  	private List<BigDecimal> cmins;
  	private BigDecimal casualContribMax;
  	private BigDecimal casualContribMin;
  	private List<String> frequencys;
  	private Integer premUnit;
  	private Integer rndfact;
	private List<BigDecimal> cIncrmaxs;
  	private List<BigDecimal>  cIncrmins;
	public List<BigDecimal> getCmaxs() {
		return cmaxs;
	}
	public void setCmaxs(List<BigDecimal> cmaxs) {
		this.cmaxs = cmaxs;
	}
	public List<BigDecimal> getCmins() {
		return cmins;
	}
	public void setCmins(List<BigDecimal> cmins) {
		this.cmins = cmins;
	}
	public BigDecimal getCasualContribMax() {
		return casualContribMax;
	}
	public void setCasualContribMax(BigDecimal casualContribMax) {
		this.casualContribMax = casualContribMax;
	}
	public BigDecimal getCasualContribMin() {
		return casualContribMin;
	}
	public void setCasualContribMin(BigDecimal casualContribMin) {
		this.casualContribMin = casualContribMin;
	}
	public List<String> getFrequencys() {
		return frequencys;
	}
	public void setFrequencys(List<String> frequencys) {
		this.frequencys = frequencys;
	}
	public Integer getPremUnit() {
		return premUnit;
	}
	public void setPremUnit(Integer premUnit) {
		this.premUnit = premUnit;
	}
	public Integer getRndfact() {
		return rndfact;
	}
	public void setRndfact(Integer rndfact) {
		this.rndfact = rndfact;
	}
	public List<BigDecimal> getcIncrmaxs() {
		return cIncrmaxs;
	}
	public void setcIncrmaxs(List<BigDecimal> cIncrmaxs) {
		this.cIncrmaxs = cIncrmaxs;
	}
	public List<BigDecimal> getcIncrmins() {
		return cIncrmins;
	}
	public void setcIncrmins(List<BigDecimal> cIncrmins) {
		this.cIncrmins = cIncrmins;
	}
}
