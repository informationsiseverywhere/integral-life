package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
public class T5540 {
	
	private String alfnds;
	private String allbas;
	private String fundSplitPlan;
	private String iuDiscFact;
	private String iuDiscBasis;
	private String ltypst;
  	private BigDecimal maxfnd;
  	private BigDecimal rvwproc;
	private String unitCancInit;
	private String wdmeth;
	private String wholeIuDiscFact;
	private String zafropt1;
	private String zvariance;
	
	public String getAlfnds() {
		return alfnds;
	}
	public void setAlfnds(String alfnds) {
		this.alfnds = alfnds;
	}
	public String getAllbas() {
		return allbas;
	}
	public void setAllbas(String allbas) {
		this.allbas = allbas;
	}
	public String getFundSplitPlan() {
		return fundSplitPlan;
	}
	public void setFundSplitPlan(String fundSplitPlan) {
		this.fundSplitPlan = fundSplitPlan;
	}
	public String getIuDiscFact() {
		return iuDiscFact;
	}
	public void setIuDiscFact(String iuDiscFact) {
		this.iuDiscFact = iuDiscFact;
	}
	public String getIuDiscBasis() {
		return iuDiscBasis;
	}
	public void setIuDiscBasis(String iuDiscBasis) {
		this.iuDiscBasis = iuDiscBasis;
	}
	public String getLtypst() {
		return ltypst;
	}
	public void setLtypst(String ltypst) {
		this.ltypst = ltypst;
	}
	public BigDecimal getMaxfnd() {
		return maxfnd;
	}
	public void setMaxfnd(BigDecimal maxfnd) {
		this.maxfnd = maxfnd;
	}
	public BigDecimal getRvwproc() {
		return rvwproc;
	}
	public void setRvwproc(BigDecimal rvwproc) {
		this.rvwproc = rvwproc;
	}
	public String getUnitCancInit() {
		return unitCancInit;
	}
	public void setUnitCancInit(String unitCancInit) {
		this.unitCancInit = unitCancInit;
	}
	public String getWdmeth() {
		return wdmeth;
	}
	public void setWdmeth(String wdmeth) {
		this.wdmeth = wdmeth;
	}
	public String getWholeIuDiscFact() {
		return wholeIuDiscFact;
	}
	public void setWholeIuDiscFact(String wholeIuDiscFact) {
		this.wholeIuDiscFact = wholeIuDiscFact;
	}
	public String getZafropt1() {
		return zafropt1;
	}
	public void setZafropt1(String zafropt1) {
		this.zafropt1 = zafropt1;
	}
	public String getZvariance() {
		return zvariance;
	}
	public void setZvariance(String zvariance) {
		this.zvariance = zvariance;
	}
	
}
