package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T6627{
	/** 
	 * @author wli31
	 *
	 */
  	private List<String> statbands;
  	private List<BigDecimal> statfroms;
  	private List<BigDecimal> statto;
	public List<String> getStatbands() {
		return statbands;
	}
	public void setStatbands(List<String> statbands) {
		this.statbands = statbands;
	}
	public List<BigDecimal> getStatfroms() {
		return statfroms;
	}
	public void setStatfroms(List<BigDecimal> statfroms) {
		this.statfroms = statfroms;
	}
	public List<BigDecimal> getStatto() {
		return statto;
	}
	public void setStatto(List<BigDecimal> statto) {
		this.statto = statto;
	}
  	
}