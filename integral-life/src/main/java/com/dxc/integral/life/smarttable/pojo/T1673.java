package com.dxc.integral.life.smarttable.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class T1673 {
	private String cmgwmop;
	private String cmgwspltyp;
	
	public String getCmgwmop() {
		return cmgwmop;
	}
	public void setCmgwmop(String cmgwmop) {
		this.cmgwmop = cmgwmop;
	}
	public String getCmgwspltyp() {
		return cmgwspltyp;
	}
	public void setCmgwspltyp(String cmgwspltyp) {
		this.cmgwspltyp = cmgwspltyp;
	}
}
