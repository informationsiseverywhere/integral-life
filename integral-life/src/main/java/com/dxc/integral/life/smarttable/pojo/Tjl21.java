package com.dxc.integral.life.smarttable.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tjl21 {
	private String cmgwdattyp;
	private String cmgwreason;
	private String cmgwcngcls;
	
	public String getCmgwdattyp() {
		return cmgwdattyp;
	}
	public void setCmgwdattyp(String cmgwdattyp) {
		this.cmgwdattyp = cmgwdattyp;
	}
	public String getCmgwreason() {
		return cmgwreason;
	}
	public void setCmgwreason(String cmgwreason) {
		this.cmgwreason = cmgwreason;
	}
	public String getCmgwcngcls() {
		return cmgwcngcls;
	}
	public void setCmgwcngcls(String cmgwcngcls) {
		this.cmgwcngcls = cmgwcngcls;
	}
	
}
