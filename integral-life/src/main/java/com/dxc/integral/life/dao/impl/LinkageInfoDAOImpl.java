package com.dxc.integral.life.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LinkageInfoDAO;
import com.dxc.integral.life.dao.model.Linkagepf;


@Repository("linkageInfoDAO")
public class LinkageInfoDAOImpl extends BaseDAOImpl implements LinkageInfoDAO {

	@Override
	public Linkagepf fetchLinkageInfo(String linkageNum) {
		
		StringBuilder sql = new StringBuilder("SELECT * FROM LINKAGEPF WHERE LINKREF=? ORDER BY UNIQUE_NUMBER DESC");

		return jdbcTemplate.queryForObject(sql.toString(), new Object[] { linkageNum },new BeanPropertyRowMapper<Linkagepf>(Linkagepf.class));
	
	}
	

}
