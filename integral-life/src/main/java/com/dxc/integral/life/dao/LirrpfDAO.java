package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Lirrpf;

public interface LirrpfDAO {
	Lirrpf search(Lirrpf lirrpf);

	void update(Lirrpf lirrpf);

	void insert(Lirrpf lirrpf);
}
