package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Zraepf {

	private int uniqueNumber;
	private String chdrcoy;
	private String life;
	private String chdrnum;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validflag;
	private Integer tranno;
	private Integer currfrom;
	private Integer currto;
	private Integer zrduedte01;
	private Integer zrduedte02;
	private Integer zrduedte03;
	private Integer zrduedte04;
	private Integer zrduedte05;
	private Integer zrduedte06;
	private Integer zrduedte07;
	private Integer zrduedte08;
	private BigDecimal prcnt01;
	private BigDecimal prcnt02;
	private BigDecimal prcnt03;
	private BigDecimal prcnt04;
	private BigDecimal prcnt05;
	private BigDecimal prcnt06;
	private BigDecimal prcnt07;
	private BigDecimal prcnt08;
	private Integer paydte01;
	private Integer paydte02;
	private Integer paydte03;
	private Integer paydte04;
	private Integer paydte05;
	private Integer paydte06;
	private Integer paydte07;
	private Integer paydte08;
	private BigDecimal paid01;
	private BigDecimal paid02;
	private BigDecimal paid03;
	private BigDecimal paid04;
	private BigDecimal paid05;
	private BigDecimal paid06;
	private BigDecimal paid07;
	private BigDecimal paid08;
	private String paymmeth01;
	private String paymmeth02;
	private String paymmeth03;
	private String paymmeth04;
	private String paymmeth05;
	private String paymmeth06;
	private String paymmeth07;
	private String paymmeth08;
	private String zrpayopt01;
	private String zrpayopt02;
	private String zrpayopt03;
	private String zrpayopt04;
	private String zrpayopt05;
	private String zrpayopt06;
	private String zrpayopt07;
	private String zrpayopt08;
	private BigDecimal totamnt;
	private Integer npaydate;
	private String payclt;
	private String bankkey;
	private String bankacckey;
	private String paycurr;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private Integer policyTranno;
	private BigDecimal sumins;
	private String statcode;
	private String pstatcode;
	private int plansuffix;
	private Integer date1;
	private Integer date2;
    
	public Integer getDate1() {
		return date1;
	}
	public void setDate1(Integer date1) {
		this.date1 = date1;
	}
	public int getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(int uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public Integer getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	public Integer getCurrto() {
		return currto;
	}
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}
	public Integer getZrduedte01() {
		return zrduedte01;
	}
	public void setZrduedte01(Integer zrduedte01) {
		this.zrduedte01 = zrduedte01;
	}
	public Integer getZrduedte02() {
		return zrduedte02;
	}
	public void setZrduedte02(Integer zrduedte02) {
		this.zrduedte02 = zrduedte02;
	}
	public Integer getZrduedte03() {
		return zrduedte03;
	}
	public void setZrduedte03(Integer zrduedte03) {
		this.zrduedte03 = zrduedte03;
	}
	public Integer getZrduedte04() {
		return zrduedte04;
	}
	public void setZrduedte04(Integer zrduedte04) {
		this.zrduedte04 = zrduedte04;
	}
	public Integer getZrduedte05() {
		return zrduedte05;
	}
	public void setZrduedte05(Integer zrduedte05) {
		this.zrduedte05 = zrduedte05;
	}
	public Integer getZrduedte06() {
		return zrduedte06;
	}
	public void setZrduedte06(Integer zrduedte06) {
		this.zrduedte06 = zrduedte06;
	}
	public Integer getZrduedte07() {
		return zrduedte07;
	}
	public void setZrduedte07(Integer zrduedte07) {
		this.zrduedte07 = zrduedte07;
	}
	public Integer getZrduedte08() {
		return zrduedte08;
	}
	public void setZrduedte08(Integer zrduedte08) {
		this.zrduedte08 = zrduedte08;
	}
	public BigDecimal getPrcnt01() {
		return prcnt01;
	}
	public void setPrcnt01(BigDecimal prcnt01) {
		this.prcnt01 = prcnt01;
	}
	public BigDecimal getPrcnt02() {
		return prcnt02;
	}
	public void setPrcnt02(BigDecimal prcnt02) {
		this.prcnt02 = prcnt02;
	}
	public BigDecimal getPrcnt03() {
		return prcnt03;
	}
	public void setPrcnt03(BigDecimal prcnt03) {
		this.prcnt03 = prcnt03;
	}
	public BigDecimal getPrcnt04() {
		return prcnt04;
	}
	public void setPrcnt04(BigDecimal prcnt04) {
		this.prcnt04 = prcnt04;
	}
	public BigDecimal getPrcnt05() {
		return prcnt05;
	}
	public void setPrcnt05(BigDecimal prcnt05) {
		this.prcnt05 = prcnt05;
	}
	public BigDecimal getPrcnt06() {
		return prcnt06;
	}
	public void setPrcnt06(BigDecimal prcnt06) {
		this.prcnt06 = prcnt06;
	}
	public BigDecimal getPrcnt07() {
		return prcnt07;
	}
	public void setPrcnt07(BigDecimal prcnt07) {
		this.prcnt07 = prcnt07;
	}
	public BigDecimal getPrcnt08() {
		return prcnt08;
	}
	public void setPrcnt08(BigDecimal prcnt08) {
		this.prcnt08 = prcnt08;
	}
	public Integer getPaydte01() {
		return paydte01;
	}
	public void setPaydte01(Integer paydte01) {
		this.paydte01 = paydte01;
	}
	public Integer getPaydte02() {
		return paydte02;
	}
	public void setPaydte02(Integer paydte02) {
		this.paydte02 = paydte02;
	}
	public Integer getPaydte03() {
		return paydte03;
	}
	public void setPaydte03(Integer paydte03) {
		this.paydte03 = paydte03;
	}
	public Integer getPaydte04() {
		return paydte04;
	}
	public void setPaydte04(Integer paydte04) {
		this.paydte04 = paydte04;
	}
	public Integer getPaydte05() {
		return paydte05;
	}
	public void setPaydte05(Integer paydte05) {
		this.paydte05 = paydte05;
	}
	public Integer getPaydte06() {
		return paydte06;
	}
	public void setPaydte06(Integer paydte06) {
		this.paydte06 = paydte06;
	}
	public Integer getPaydte07() {
		return paydte07;
	}
	public void setPaydte07(Integer paydte07) {
		this.paydte07 = paydte07;
	}
	public Integer getPaydte08() {
		return paydte08;
	}
	public void setPaydte08(Integer paydte08) {
		this.paydte08 = paydte08;
	}
	public BigDecimal getPaid01() {
		return paid01;
	}
	public void setPaid01(BigDecimal paid01) {
		this.paid01 = paid01;
	}
	public BigDecimal getPaid02() {
		return paid02;
	}
	public void setPaid02(BigDecimal paid02) {
		this.paid02 = paid02;
	}
	public BigDecimal getPaid03() {
		return paid03;
	}
	public void setPaid03(BigDecimal paid03) {
		this.paid03 = paid03;
	}
	public BigDecimal getPaid04() {
		return paid04;
	}
	public void setPaid04(BigDecimal paid04) {
		this.paid04 = paid04;
	}
	public BigDecimal getPaid05() {
		return paid05;
	}
	public void setPaid05(BigDecimal paid05) {
		this.paid05 = paid05;
	}
	public BigDecimal getPaid06() {
		return paid06;
	}
	public void setPaid06(BigDecimal paid06) {
		this.paid06 = paid06;
	}
	public BigDecimal getPaid07() {
		return paid07;
	}
	public void setPaid07(BigDecimal paid07) {
		this.paid07 = paid07;
	}
	public BigDecimal getPaid08() {
		return paid08;
	}
	public void setPaid08(BigDecimal paid08) {
		this.paid08 = paid08;
	}
	public String getPaymmeth01() {
		return paymmeth01;
	}
	public void setPaymmeth01(String paymmeth01) {
		this.paymmeth01 = paymmeth01;
	}
	public String getPaymmeth02() {
		return paymmeth02;
	}
	public void setPaymmeth02(String paymmeth02) {
		this.paymmeth02 = paymmeth02;
	}
	public String getPaymmeth03() {
		return paymmeth03;
	}
	public void setPaymmeth03(String paymmeth03) {
		this.paymmeth03 = paymmeth03;
	}
	public String getPaymmeth04() {
		return paymmeth04;
	}
	public void setPaymmeth04(String paymmeth04) {
		this.paymmeth04 = paymmeth04;
	}
	public String getPaymmeth05() {
		return paymmeth05;
	}
	public void setPaymmeth05(String paymmeth05) {
		this.paymmeth05 = paymmeth05;
	}
	public String getPaymmeth06() {
		return paymmeth06;
	}
	public void setPaymmeth06(String paymmeth06) {
		this.paymmeth06 = paymmeth06;
	}
	public String getPaymmeth07() {
		return paymmeth07;
	}
	public void setPaymmeth07(String paymmeth07) {
		this.paymmeth07 = paymmeth07;
	}
	public String getPaymmeth08() {
		return paymmeth08;
	}
	public void setPaymmeth08(String paymmeth08) {
		this.paymmeth08 = paymmeth08;
	}
	public String getZrpayopt01() {
		return zrpayopt01;
	}
	public void setZrpayopt01(String zrpayopt01) {
		this.zrpayopt01 = zrpayopt01;
	}
	public String getZrpayopt02() {
		return zrpayopt02;
	}
	public void setZrpayopt02(String zrpayopt02) {
		this.zrpayopt02 = zrpayopt02;
	}
	public String getZrpayopt03() {
		return zrpayopt03;
	}
	public void setZrpayopt03(String zrpayopt03) {
		this.zrpayopt03 = zrpayopt03;
	}
	public String getZrpayopt04() {
		return zrpayopt04;
	}
	public void setZrpayopt04(String zrpayopt04) {
		this.zrpayopt04 = zrpayopt04;
	}
	public String getZrpayopt05() {
		return zrpayopt05;
	}
	public void setZrpayopt05(String zrpayopt05) {
		this.zrpayopt05 = zrpayopt05;
	}
	public String getZrpayopt06() {
		return zrpayopt06;
	}
	public void setZrpayopt06(String zrpayopt06) {
		this.zrpayopt06 = zrpayopt06;
	}
	public String getZrpayopt07() {
		return zrpayopt07;
	}
	public void setZrpayopt07(String zrpayopt07) {
		this.zrpayopt07 = zrpayopt07;
	}
	public String getZrpayopt08() {
		return zrpayopt08;
	}
	public void setZrpayopt08(String zrpayopt08) {
		this.zrpayopt08 = zrpayopt08;
	}
	public BigDecimal getTotamnt() {
		return totamnt;
	}
	public void setTotamnt(BigDecimal totamnt) {
		this.totamnt = totamnt;
	}
	public Integer getNpaydate() {
		return npaydate;
	}
	public void setNpaydate(Integer npaydate) {
		this.npaydate = npaydate;
	}
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getPaycurr() {
		return paycurr;
	}
	public void setPaycurr(String paycurr) {
		this.paycurr = paycurr;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	public Integer getPolicyTranno() {
		return policyTranno;
	}
	public void setPolicyTranno(Integer policyTranno) {
		this.policyTranno = policyTranno;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public int getPlansuffix() {
		return plansuffix;
	}
	public void setPlansuffix(int plansuffix) {
		this.plansuffix = plansuffix;
	}
	
	
	public Integer getDate2() {
		return date2;
	}
	public void setDate2(Integer date2) {
		this.date2 = date2;
	}
	@Override
	public String toString() {
		return "Zraepf [uniqueNumber=" + uniqueNumber + ", chdrcoy=" + chdrcoy + ", life=" + life + ", chdrnum="
				+ chdrnum + ", coverage=" + coverage + ", rider=" + rider + ", plnsfx=" + plnsfx + ", validflag="
				+ validflag + ", tranno=" + tranno + ", currfrom=" + currfrom + ", currto=" + currto + ", zrduedte01="
				+ zrduedte01 + ", zrduedte02=" + zrduedte02 + ", zrduedte03=" + zrduedte03 + ", zrduedte04="
				+ zrduedte04 + ", zrduedte05=" + zrduedte05 + ", zrduedte06=" + zrduedte06 + ", zrduedte07="
				+ zrduedte07 + ", zrduedte08=" + zrduedte08 + ", prcnt01=" + prcnt01 + ", prcnt02=" + prcnt02
				+ ", prcnt03=" + prcnt03 + ", prcnt04=" + prcnt04 + ", prcnt05=" + prcnt05 + ", prcnt06=" + prcnt06
				+ ", prcnt07=" + prcnt07 + ", prcnt08=" + prcnt08 + ", paydte01=" + paydte01 + ", paydte02=" + paydte02
				+ ", paydte03=" + paydte03 + ", paydte04=" + paydte04 + ", paydte05=" + paydte05 + ", paydte06="
				+ paydte06 + ", paydte07=" + paydte07 + ", paydte08=" + paydte08 + ", paid01=" + paid01 + ", paid02="
				+ paid02 + ", paid03=" + paid03 + ", paid04=" + paid04 + ", paid05=" + paid05 + ", paid06=" + paid06
				+ ", paid07=" + paid07 + ", paid08=" + paid08 + ", paymmeth01=" + paymmeth01 + ", paymmeth02="
				+ paymmeth02 + ", paymmeth03=" + paymmeth03 + ", paymmeth04=" + paymmeth04 + ", paymmeth05="
				+ paymmeth05 + ", paymmeth06=" + paymmeth06 + ", paymmeth07=" + paymmeth07 + ", paymmeth08="
				+ paymmeth08 + ", zrpayopt01=" + zrpayopt01 + ", zrpayopt02=" + zrpayopt02 + ", zrpayopt03="
				+ zrpayopt03 + ", zrpayopt04=" + zrpayopt04 + ", zrpayopt05=" + zrpayopt05 + ", zrpayopt06="
				+ zrpayopt06 + ", zrpayopt07=" + zrpayopt07 + ", zrpayopt08=" + zrpayopt08 + ", totamnt=" + totamnt
				+ ", npaydate=" + npaydate + ", payclt=" + payclt + ", bankkey=" + bankkey + ", bankacckey="
				+ bankacckey + ", paycurr=" + paycurr + ", usrprf=" + usrprf + ", jobnm=" + jobnm + ", datime=" + datime
				+ ", policyTranno=" + policyTranno + ", sumins=" + sumins + ", statcode=" + statcode + ", pstatcode="
				+ pstatcode + ", plansuffix=" + plansuffix + "]";
	}
	
	

}