package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Fpcopf;

public interface FpcopfDAO {

	List<Fpcopf> readFpcopfRecords(String company, String contractNumber);
	List<Fpcopf> readFpcorevRecords(String company, String contractNumber, String life, String coverage, String rider, int plnsfx);
	public void updateFpcopfRecords(List<Fpcopf> fpcopfList);
	public int updateFpcopfRecords(Fpcopf fpcopf);
}
