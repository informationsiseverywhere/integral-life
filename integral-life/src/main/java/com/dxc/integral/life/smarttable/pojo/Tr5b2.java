package com.dxc.integral.life.smarttable.pojo;

public class Tr5b2 {

	private String nlg = "";
	private int nlgczag = 0;
	private int maxnlgyrs = 0;
	public String getNlg() {
		return nlg;
	}
	public void setNlg(String nlg) {
		this.nlg = nlg;
	}
	public int getNlgczag() {
		return nlgczag;
	}
	public void setNlgczag(int nlgczag) {
		this.nlgczag = nlgczag;
	}
	public int getMaxnlgyrs() {
		return maxnlgyrs;
	}
	public void setMaxnlgyrs(int maxnlgyrs) {
		this.maxnlgyrs = maxnlgyrs;
	}
	
	
}
