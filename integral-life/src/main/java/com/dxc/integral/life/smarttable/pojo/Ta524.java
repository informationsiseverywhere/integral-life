package com.dxc.integral.life.smarttable.pojo;

public class Ta524 {

	private int minmthif;
	private int maxphprd;
	private String prmstatus;
	private String znfopt;
	
	public int getMinmthif() {
		return minmthif;
	}
	public void setMinmthif(int minmthif) {
		this.minmthif = minmthif;
	}
	public int getMaxphprd() {
		return maxphprd;
	}
	public void setMaxphprd(int maxphprd) {
		this.maxphprd = maxphprd;
	}
	public String getPrmstatus() {
		return prmstatus;
	}
	public void setPrmstatus(String prmstatus) {
		this.prmstatus = prmstatus;
	}
	public String getZnfopt() {
		return znfopt;
	}
	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}
	
	
}
