package com.dxc.integral.life.utils.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.dao.LinkageInfoDAO;
import com.dxc.integral.life.dao.model.Linkagepf;
/**
 * 
 * @author lbarde
 *
 */
@Service
public class LinkageInfoService {
	
	@Autowired
	LinkageInfoDAO linkageInfoDAO;	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LinkageInfoService.class);
	
	public String getLinkageInfo(String linkNum)
	{
		String linkInfo = null;
		// Code to read LINKAGEPF table based on passed 'Linkage Number' and return linkInfo value in JSON format			
		try {
			Linkagepf linkagepfObj = linkageInfoDAO.fetchLinkageInfo(linkNum);
			if (linkagepfObj != null) {
				linkInfo = linkagepfObj.getLinkinfo();
			} 
		} catch (Exception e) {
			LOGGER.error("LinkageInfoService :: Exception found in getting Linked Coverage info :: ",e);//IJTI-1498
		}
		return linkInfo;
	}
	
}
