package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Annypf;

public interface AnnypfDAO {
	public List<Annypf> getAnnypfRecord(Annypf annypf);
}