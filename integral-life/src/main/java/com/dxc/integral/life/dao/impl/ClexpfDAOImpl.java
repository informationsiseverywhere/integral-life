package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.model.Clexpf;

/**
 * The ClextpfDAOImpl for database operations.
 * 
 * @author wli31
 *
 */
@Repository("clexpfDAO")
@Lazy
public class ClexpfDAOImpl  extends BaseDAOImpl implements ClexpfDAO {
	@Override
	public Clexpf getClexpfRecord(String clntpfx, String clntcoy,String clntnum) {
		StringBuilder sql = new StringBuilder("select * from clexpf where clntpfx=? and clntcoy=? and clntnum=? ");
		  sql.append("order by clntpfx asc, clntcoy asc, clntnum asc, unique_number desc");
		Clexpf clexpf = null;
		try {
		    clexpf =  jdbcTemplate.queryForObject(sql.toString(), new Object[] { clntpfx, clntcoy, clntnum },
					new BeanPropertyRowMapper<Clexpf>(Clexpf.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
		return clexpf;
	}

}
