package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Hpuapf;

public interface HpuapfDAO {
	List<Hpuapf> readHpuaRecord(String chdrcoy,String chdrnum);
	void updateHpuapfRecords(List<Hpuapf> hpuapfList);
	void inertHpuapfRecords(List<Hpuapf> hpuapfList);
	public List<Hpuapf> getHpuapfRecords(String coy, String chdrnum, String life, String coverage, String rider, int plnsfx, int tranno);
	public Hpuapf getHpuaList(String coy, String chdrnum, String life, String coverage, String rider, int plnsfx, int hpuanbr);
	public int updateHpuapf(Hpuapf hpuapf);
	public int deleteHpuapfRecord(Hpuapf hpuapf);
}
