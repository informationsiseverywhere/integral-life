package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * Maprpf model for MaprpfDAO.
 * 
 * @author vhukumagrawa
 *
 */
public class Maprpf {

	private long unique_number;
	private String agntcoy;
	private String agntnum;
	private int acctyr;
	private int mnth;
	private String cnttype;
	private int effdate;
	private List<BigDecimal> mlperpp;
	private List<BigDecimal> mlperpc;
	private List<BigDecimal> mldirpp;
	private List<BigDecimal> mldirpc;
	private List<BigDecimal> mlgrppp;
	private List<BigDecimal> mlgrppc;
	private int cntcount;
	private BigDecimal sumins;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public int getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(int acctyr) {
		this.acctyr = acctyr;
	}
	public int getMnth() {
		return mnth;
	}
	public void setMnth(int mnth) {
		this.mnth = mnth;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public List<BigDecimal> getMlperpp() {
		return mlperpp;
	}
	public void setMlperpp(List<BigDecimal> mlperpp) {
		this.mlperpp = mlperpp;
	}
	public List<BigDecimal> getMlperpc() {
		return mlperpc;
	}
	public void setMlperpc(List<BigDecimal> mlperpc) {
		this.mlperpc = mlperpc;
	}
	public List<BigDecimal> getMldirpp() {
		return mldirpp;
	}
	public void setMldirpp(List<BigDecimal> mldirpp) {
		this.mldirpp = mldirpp;
	}
	public List<BigDecimal> getMldirpc() {
		return mldirpc;
	}
	public void setMldirpc(List<BigDecimal> mldirpc) {
		this.mldirpc = mldirpc;
	}
	public List<BigDecimal> getMlgrppp() {
		return mlgrppp;
	}
	public void setMlgrppp(List<BigDecimal> mlgrppp) {
		this.mlgrppp = mlgrppp;
	}
	public List<BigDecimal> getMlgrppc() {
		return mlgrppc;
	}
	public void setMlgrppc(List<BigDecimal> mlgrppc) {
		this.mlgrppc = mlgrppc;
	}
	public int getCntcount() {
		return cntcount;
	}
	public void setCntcount(int cntcount) {
		this.cntcount = cntcount;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
}
