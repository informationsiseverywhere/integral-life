package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for NFLoan utility.
 * 
 * @author dpuhawan
 */
public class NfloanDTO {

	private String statuz;
	private String chdrcoy;
	private String chdrnum;
	private Integer effdate;
	private String openPrintFlag;
	private String batchbrn;
	private String language;
	private String company;
	private String cnttype;
	private String cntcurr;
	private String billfreq;
	private Integer polsum;
	private Integer polinc;
	private Integer ptdate;
	private String batctrcde;
	private BigDecimal outstamt;
	private String loantype;
	
	
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public String getOpenPrintFlag() {
		return openPrintFlag;
	}
	public void setOpenPrintFlag(String openPrintFlag) {
		this.openPrintFlag = openPrintFlag;
	}
	public String getBatchbrn() {
		return batchbrn;
	}
	public void setBatchbrn(String batchbrn) {
		this.batchbrn = batchbrn;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public Integer getPolsum() {
		return polsum;
	}
	public void setPolsum(Integer polsum) {
		this.polsum = polsum;
	}
	public Integer getPolinc() {
		return polinc;
	}
	public void setPolinc(Integer polinc) {
		this.polinc = polinc;
	}
	public Integer getPtdate() {
		return ptdate;
	}
	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public BigDecimal getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(BigDecimal outstamt) {
		this.outstamt = outstamt;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}

}
