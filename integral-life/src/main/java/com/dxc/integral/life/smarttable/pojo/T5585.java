package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5585 {
  	private List<Integer> addages;
  	private List<Integer> ageadjs;
  	private List<Integer> agedifs;
  	private List<Integer> agelimits;
  	private String hghlowage;
  	private String sexageadj;
	public List<Integer> getAddages() {
		return addages;
	}
	public void setAddages(List<Integer> addages) {
		this.addages = addages;
	}
	public List<Integer> getAgeadjs() {
		return ageadjs;
	}
	public void setAgeadjs(List<Integer> ageadjs) {
		this.ageadjs = ageadjs;
	}
	public List<Integer> getAgedifs() {
		return agedifs;
	}
	public void setAgedifs(List<Integer> agedifs) {
		this.agedifs = agedifs;
	}
	public List<Integer> getAgelimits() {
		return agelimits;
	}
	public void setAgelimits(List<Integer> agelimits) {
		this.agelimits = agelimits;
	}
	public String getHghlowage() {
		return hghlowage;
	}
	public void setHghlowage(String hghlowage) {
		this.hghlowage = hghlowage;
	}
	public String getSexageadj() {
		return sexageadj;
	}
	public void setSexageadj(String sexageadj) {
		this.sexageadj = sexageadj;
	}

}
