package com.dxc.integral.life.beans;

public class BatckeyDTO {
	
  	private String batcBatcpfx;
  	private String batcBatccoy;
  	private String batcBatcbrn;
  	private int batcBatcactyr;
  	private int batcBatcactmn;
  	private String batcBatctrcde;
  	private String batcBatcbatch;
  	
  	public void setAllkey(String batcKey) {
  		initDTO();
  		if(batcKey.length()>=2) {
  			this.batcBatcpfx = batcKey.substring(0, 2);
  		}
  		if(batcKey.length()>=3) {
  			this.batcBatccoy = batcKey.substring(2, 3);
  		}
  		if(batcKey.length()>=5) {
  			this.batcBatcbrn = batcKey.substring(3, 5);
  		}
  		if(batcKey.length()>=9) {
  			try {
  			this.batcBatcactyr = Integer.parseInt(batcKey.substring(5, 9));
  			} catch(Exception e) { 				
  			}finally {
  			this.batcBatcactyr=0;	
  			}
  		}
  		if(batcKey.length()>=11) {
  			try {
  			this.batcBatcactmn = Integer.parseInt(batcKey.substring(9, 11));
  			} catch(Exception e) { 				
  			}finally {
  			this.batcBatcactmn=0;	
  			}
  		}
  		if(batcKey.length()>=15) {
  			this.batcBatctrcde = batcKey.substring(11, 15);
  		}
  		if(batcKey.length()>=20) {
  			this.batcBatcbatch = batcKey.substring(15, 20);
  		}
  	}
  	
  	public void initDTO() {
  		batcBatcpfx="";
  		batcBatccoy="";
  		batcBatcbrn="";
  		batcBatcactyr=0;
  		batcBatcactmn=0;
  		batcBatctrcde="";
  		batcBatcbatch="";
  	}
  	
	public String getBatcBatcpfx() {
		return batcBatcpfx;
	}
	public void setBatcBatcpfx(String batcBatcpfx) {
		this.batcBatcpfx = batcBatcpfx;
	}
	public String getBatcBatccoy() {
		return batcBatccoy;
	}
	public void setBatcBatccoy(String batcBatccoy) {
		this.batcBatccoy = batcBatccoy;
	}
	public String getBatcBatcbrn() {
		return batcBatcbrn;
	}
	public void setBatcBatcbrn(String batcBatcbrn) {
		this.batcBatcbrn = batcBatcbrn;
	}
	public int getBatcBatcactyr() {
		return batcBatcactyr;
	}
	public void setBatcBatcactyr(int batcBatcactyr) {
		this.batcBatcactyr = batcBatcactyr;
	}
	public int getBatcBatcactmn() {
		return batcBatcactmn;
	}
	public void setBatcBatcactmn(int batcBatcactmn) {
		this.batcBatcactmn = batcBatcactmn;
	}
	public String getBatcBatctrcde() {
		return batcBatctrcde;
	}
	public void setBatcBatctrcde(String batcBatctrcde) {
		this.batcBatctrcde = batcBatctrcde;
	}
	public String getBatcBatcbatch() {
		return batcBatcbatch;
	}
	public void setBatcBatcbatch(String batcBatcbatch) {
		this.batcBatcbatch = batcBatcbatch;
	}
  	
  	
}
