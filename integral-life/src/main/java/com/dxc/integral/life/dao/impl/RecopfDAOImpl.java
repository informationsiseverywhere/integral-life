package com.dxc.integral.life.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RecopfDAO;
import com.dxc.integral.life.dao.model.Recopf;

@Repository("recopfDAO")
@Lazy
public class RecopfDAOImpl extends BaseDAOImpl implements RecopfDAO {
    @Override
	public List<Recopf> getRecoclmRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, int seqno){
		StringBuilder sql = new StringBuilder("SELECT * FROM RECOPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND SEQNO=? ");
		sql.append("COSTDATE ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO DESC, COSTDATE DESC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.query(sql.toString(), new Object[] { chdrcoy, chdrnum, life, coverage, rider, plnsfx, seqno},
					new BeanPropertyRowMapper<Recopf>(Recopf.class));
	}

	@Override
	public int insertRecopfRecord(Recopf recopf) {
		String sql = "INSERT INTO RECOPF (CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,SEQNO,VALIDFLAG,COSTDATE,RETYPE,RNGMNT,SRARAMT,RAAMOUNT,CTDATE,ORIGCURR,PREM,COMPAY,TAXAMT,REFUNDFE,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,TRANNO,RCSTFRQ,USRPRF,JOBNM,DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return jdbcTemplate.update(sql,
				new Object[] { recopf.getChdrcoy(), recopf.getChdrnum(), recopf.getLife(), recopf.getCoverage(),
						recopf.getRider(), recopf.getPlanSuffix(), recopf.getRasnum(), recopf.getSeqno(),
						recopf.getValidflag(), recopf.getCostdate(), recopf.getRetype(), recopf.getRngmnt(),
						recopf.getSraramt(), recopf.getRaAmount(), recopf.getCtdate(), recopf.getOrigcurr(),
						recopf.getPrem(), recopf.getCompay(), recopf.getTaxamt(), recopf.getRefundfe(),
						recopf.getBatccoy(), recopf.getBatcbrn(), recopf.getBatcactyr(), recopf.getBatcactmn(),
						recopf.getBatctrcde(), recopf.getBatcbatch(), recopf.getTranno(), recopf.getRcstfrq(),
						recopf.getUserProfile(), recopf.getJobName(), new Timestamp(System.currentTimeMillis()) },
				new BeanPropertyRowMapper<Recopf>(Recopf.class));
	}
}
