package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class Th549 {
	private BigDecimal expfactor;
  	private List<String> indcs;
  	private List<String>  opcdas;
  	private List<String>  subrtns;
	public BigDecimal getExpfactor() {
		return expfactor;
	}
	public void setExpfactor(BigDecimal expfactor) {
		this.expfactor = expfactor;
	}
	public List<String> getIndcs() {
		return indcs;
	}
	public void setIndcs(List<String> indcs) {
		this.indcs = indcs;
	}
	public List<String> getOpcdas() {
		return opcdas;
	}
	public void setOpcdas(List<String> opcdas) {
		this.opcdas = opcdas;
	}
	public List<String> getSubrtns() {
		return subrtns;
	}
	public void setSubrtns(List<String> subrtns) {
		this.subrtns = subrtns;
	}
  


}
