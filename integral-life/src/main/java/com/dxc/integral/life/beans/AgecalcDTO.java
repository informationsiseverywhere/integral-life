package com.dxc.integral.life.beans;

/**
 * @author wli31
 */
public class AgecalcDTO {

	private String function;
	private String statuz;
	private String cnttype;
	private String intDatex1;
	private String intDate1;
	private String intDatex2;
	private String intDate2;
	private int agerating;
	private String company;
	private String language;
	private int year1;
	private int year2;

	public AgecalcDTO() {
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getStatuz() {
		return statuz;
	}

	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getIntDatex1() {
		return intDatex1;
	}

	public void setIntDatex1(String intDatex1) {
		this.intDatex1 = intDatex1;
	}

	public String getIntDate1() {
		return intDate1;
	}

	public void setIntDate1(String intDate1) {
		this.intDate1 = intDate1;
	}

	public String getIntDatex2() {
		return intDatex2;
	}

	public void setIntDatex2(String intDatex2) {
		this.intDatex2 = intDatex2;
	}

	public String getIntDate2() {
		return intDate2;
	}

	public void setIntDate2(String intDate2) {
		this.intDate2 = intDate2;
	}

	public int getAgerating() {
		return agerating;
	}

	public void setAgerating(int agerating) {
		this.agerating = agerating;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getYear1() {
		return year1;
	}

	public void setYear1(int year1) {
		this.year1 = year1;
	}

	public int getYear2() {
		return year2;
	}

	public void setYear2(int year2) {
		this.year2 = year2;
	}

	@Override
	public String toString() {
		return "AgecalcPojo [function=" + function + ", statuz=" + statuz + ", cnttype=" + cnttype + ", intDatex1="
				+ intDatex1 + ", intDate1=" + intDate1 + ", intDatex2=" + intDatex2 + ", intDate2=" + intDate2
				+ ", agerating=" + agerating + ", company=" + company + ", language=" + language + ", year1=" + year1
				+ ", year2=" + year2 + "]";
	}

}
