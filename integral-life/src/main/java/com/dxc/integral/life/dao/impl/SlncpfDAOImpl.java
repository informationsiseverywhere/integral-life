package com.dxc.integral.life.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.SlncpfDAO;
import com.dxc.integral.life.dao.model.Slncpf;

/**
 * @author gsaluja2
 *
 */

@Repository("slncpfDAO")
public class SlncpfDAOImpl extends BaseDAOImpl implements SlncpfDAO {
	
	private static final Logger LOGER = LoggerFactory.getLogger(LincpfDAOImpl.class);
	
	private static final String TABLE = "VM1DTA.SLNCPF";

	@Override
	public List<Slncpf> readSlncpfData(String zreclass) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT UNIQUE_NUMBER, ZDATACLS,ZRECLASS,ZMODTYPE,ZRSLTCDE,ZRSLTRSN,ZERRORNO,ZNOFRECS,KLINCKEY,HCLTNAM,");
		sql.append("CLTSEX,ZCLNTDOB,ZEFFDATE,FLAG,KGLCMPCD,ADDRSS,ZHOSPBNF,ZSICKBNF,ZCANCBNF,ZOTHSBNF,ZREGDATE,");
		sql.append("ZLINCDTE,KJHCLTNAM,ZHOSPOPT,ZREVDATE,ZLOWNNAM,ZCSUMINS,ZASUMINS,ZDETHOPT,ZRQFLAG,ZRQSETDTE,");
		sql.append("ZRQCANDTE,ZEPFLAG,ZCVFLAG,ZCONTSEX,ZCONTDOB,ZCONTADDR,LIFNAMKJ,OWNNAMKJ,WFLGA,");
		sql.append("ZERRFLG,ZOCCDATE,ZTRDATE,ZADRDDTE,ZEXDRDDTE,USRPRF,JOBNM,DATIME FROM ").append(TABLE);
		sql.append(" WHERE ZRECLASS = ? ORDER BY ZRECLASS ASC, ZMODTYPE ASC, UNIQUE_NUMBER ASC");
		LOGER.info("readSlncpfData {}", sql);
		return jdbcTemplate.query(sql.toString(),new Object[] {zreclass},new BeanPropertyRowMapper<Slncpf>(Slncpf.class));
	}
	
	public List<String> readUniqueSlncpfRecords(){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT(ZRECLASS) FROM ").append(TABLE);
		LOGER.info("readUniqueSlncpfRecords {}", sql);
		List<String> uniqueRecordsList = new ArrayList<>();
		List<Map<String,Object>> rows=jdbcTemplate.queryForList(sql.toString());
		for (Map<String,Object> row : rows) {
			for (Map.Entry<String, Object> entry : row.entrySet()) {
				Object value = entry.getValue();
				if(value != null)
					uniqueRecordsList.add(value.toString());
			}
		}
		return uniqueRecordsList;	
	}
	
	public int deleteSlncpfData(String zreclass) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ").append(TABLE).append(" WHERE ZRECLASS = ?");
		LOGER.info("deleteSlncpfData {}", sql);
		return jdbcTemplate.update(sql.toString(), zreclass);
	}
}
