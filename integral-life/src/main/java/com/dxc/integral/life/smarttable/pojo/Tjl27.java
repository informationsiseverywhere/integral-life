package com.dxc.integral.life.smarttable.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tjl27 {
	private String cmgwcolcat;
	private String cmgwcpdte;
	private String cmgwcprot;
	
	public String getCmgwcolcat() {
		return cmgwcolcat;
	}
	public void setCmgwcolcat(String cmgwcolcat) {
		this.cmgwcolcat = cmgwcolcat;
	}
	public String getCmgwcpdte() {
		return cmgwcpdte;
	}
	public void setCmgwcpdte(String cmgwcpdte) {
		this.cmgwcpdte = cmgwcpdte;
	}
	public String getCmgwcprot() {
		return cmgwcprot;
	}
	public void setCmgwcprot(String cmgwcprot) {
		this.cmgwcprot = cmgwcprot;
	}
}
