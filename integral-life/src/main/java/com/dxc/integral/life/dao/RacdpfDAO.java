package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Racdpf;

/* 
 * 
 * @author wli31
 *
 */
public interface RacdpfDAO {
	public List<Racdpf> searchRacdstRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx, String validflag);

	Racdpf updateRacd(Racdpf racdpf);

	void insertRacd(Racdpf racdpf);

	public List<Racdpf> searchRacdmjaRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx, int seqno);

	public Racdpf searchRacdrcoRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx, int seqno);

	public Racdpf searchRacdrcoRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx, int seqno, int currfrom);

}
