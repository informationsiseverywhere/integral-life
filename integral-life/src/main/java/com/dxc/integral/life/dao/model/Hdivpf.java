
package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Hdivpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private Integer hpuanbr;
	private Integer tranno;
	private String batccoy;
	private String batcbrn;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batctrcde;
	private String batcbatch;
	private String cntcurr;
	private Integer effdate;
	private Integer hdvaldt;
	private String hdvtyp;
	private BigDecimal hdvamt;
	private BigDecimal hdvrate;
	private Integer hdveffdt;
	private String zdivopt;
	private String zcshdivmth;
	private Integer hdvopttx;
	private Integer hdvcaptx;
	private Integer hincapdt;
	private Integer hdvsmtno;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private Integer divdStmtNo;
	public Hdivpf(){
		this.chdrcoy = "";
		this.chdrnum = "";
		this.life = "";
		this.jlife = "";
		this.coverage = "";
		this.rider = "";
		this.plnsfx = 0;
		this.hpuanbr = 0;
		this.tranno = 0;
		this.batccoy = "";
		this.batcbrn = "";
		this.batcactyr = 0;
		this.batcactmn = 0;
		this.batctrcde = "";
		this.batcbatch = "";
		this.cntcurr = "";
		this.effdate = 0;
		this.hdvaldt = 0;
		this.hdvtyp = "";
		this.hdvamt = BigDecimal.ZERO;
		this.hdvrate = BigDecimal.ZERO;
		this.hdveffdt = 0;
		this.zdivopt = "";
		this.zcshdivmth = "";
		this.hdvopttx = 0;
		this.hdvcaptx = 0;
		this.hincapdt = 0;
		this.hdvsmtno = 0;
		this.usrprf = "";
		this.jobnm = "";
		this.divdStmtNo = 0;
	}

	// Get Methods
	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public String getChdrcoy() {
		return this.chdrcoy;
	}

	public String getChdrnum() {
		return this.chdrnum;
	}

	public String getLife() {
		return this.life;
	}

	public String getJlife() {
		return this.jlife;
	}

	public String getCoverage() {
		return this.coverage;
	}

	public String getRider() {
		return this.rider;
	}

	public Integer getPlnsfx() {
		return this.plnsfx;
	}

	public Integer getHpuanbr() {
		return this.hpuanbr;
	}

	public Integer getTranno() {
		return this.tranno;
	}

	public String getBatccoy() {
		return this.batccoy;
	}

	public String getBatcbrn() {
		return this.batcbrn;
	}

	public Integer getBatcactyr() {
		return this.batcactyr;
	}

	public Integer getBatcactmn() {
		return this.batcactmn;
	}

	public String getBatctrcde() {
		return this.batctrcde;
	}

	public String getBatcbatch() {
		return this.batcbatch;
	}

	public String getCntcurr() {
		return this.cntcurr;
	}

	public Integer getEffdate() {
		return this.effdate;
	}

	public Integer getHdvaldt() {
		return this.hdvaldt;
	}

	public String getHdvtyp() {
		return this.hdvtyp;
	}

	public BigDecimal getHdvamt() {
		return this.hdvamt;
	}

	public BigDecimal getHdvrate() {
		return this.hdvrate;
	}

	public Integer getHdveffdt() {
		return this.hdveffdt;
	}

	public String getZdivopt() {
		return this.zdivopt;
	}

	public String getZcshdivmth() {
		return this.zcshdivmth;
	}

	public Integer getHdvopttx() {
		return this.hdvopttx;
	}

	public Integer getHdvcaptx() {
		return this.hdvcaptx;
	}

	public Integer getHincapdt() {
		return this.hincapdt;
	}

	public Integer getHdvsmtno() {
		return this.hdvsmtno;
	}

	public String getUsrprf() {
		return this.usrprf;
	}

	public String getJobnm() {
		return this.jobnm;
	}

	public Date getDatime() {
		return new Date(this.datime.getTime());
	}

	// Set Methods
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	public void setHpuanbr(Integer hpuanbr) {
		this.hpuanbr = hpuanbr;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}

	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public void setHdvaldt(Integer hdvaldt) {
		this.hdvaldt = hdvaldt;
	}

	public void setHdvtyp(String hdvtyp) {
		this.hdvtyp = hdvtyp;
	}

	public void setHdvamt(BigDecimal hdvamt) {
		this.hdvamt = hdvamt;
	}

	public void setHdvrate(BigDecimal hdvrate) {
		this.hdvrate = hdvrate;
	}

	public void setHdveffdt(Integer hdveffdt) {
		this.hdveffdt = hdveffdt;
	}

	public void setZdivopt(String zdivopt) {
		this.zdivopt = zdivopt;
	}

	public void setZcshdivmth(String zcshdivmth) {
		this.zcshdivmth = zcshdivmth;
	}

	public void setHdvopttx(Integer hdvopttx) {
		this.hdvopttx = hdvopttx;
	}

	public void setHdvcaptx(Integer hdvcaptx) {
		this.hdvcaptx = hdvcaptx;
	}

	public void setHincapdt(Integer hincapdt) {
		this.hincapdt = hincapdt;
	}

	public void setHdvsmtno(Integer hdvsmtno) {
		this.hdvsmtno = hdvsmtno;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());
	}

	public Integer getDivdStmtNo() {
		return divdStmtNo;
	}

	public void setDivdStmtNo(Integer divdStmtNo) {
		this.divdStmtNo = divdStmtNo;
	}
	
	
}
