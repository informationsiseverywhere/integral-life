package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T5673 {

	private List<String> creqs;
	private String gitem;
	private List<String> ctables;
	private List<String> ctmaxcovs;
	private List<String> rtables;
	private List<String> zrlifinds;
	private List<String> rreqs;

	public List<String> getCreqs() {
		return creqs;
	}

	public void setCreqs(List<String> creqs) {
		this.creqs = creqs;
	}

	public String getGitem() {
		return gitem;
	}

	public void setGitem(String gitem) {
		this.gitem = gitem;
	}

	public List<String> getCtmaxcovs() {
		return ctmaxcovs;
	}

	public void setCtmaxcovs(List<String> ctmaxcovs) {
		this.ctmaxcovs = ctmaxcovs;
	}

	public List<String> getZrlifinds() {
		return zrlifinds;
	}

	public void setZrlifinds(List<String> zrlifinds) {
		this.zrlifinds = zrlifinds;
	}

	public List<String> getRreqs() {
		return rreqs;
	}

	public void setRreqs(List<String> rreqs) {
		this.rreqs = rreqs;
	}

	public List<String> getCtables() {
		return ctables;
	}

	public void setCtables(List<String> ctables) {
		this.ctables = ctables;
	}

	public List<String> getRtables() {
		return rtables;
	}

	public void setRtables(List<String> rtables) {
		this.rtables = rtables;
	}
	
}

