package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.csc.dip.jvpms.json.cmd.HttpRequest;
import com.dxc.integral.life.beans.Attribute;
import com.dxc.integral.life.beans.Calculation;
import com.dxc.integral.life.beans.Calculations;
import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.Requestmap;
import com.dxc.integral.life.beans.Responsemap;
import com.dxc.integral.life.beans.UntallDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.beans.VpxacblDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.beans.VpxlextDTO;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.utils.VPMScaller;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("vpmscaller")
@Lazy
public class VPMScallerImpl implements VPMScaller {
	private static Calculations calculations;
	static {
		if (calculations == null) {
			calculations = XmlUtils.xmlFileToObject("LifeExternalisedRulesMapping.xml", Calculations.class);
		}
	}

	private static final String INPUT_LANGUAGE_STR = "Input Language";
	private static final String INPUT_CARRIERCODE_STR = "Input CarrierCode";
	private static final String INPUT_CALLING_PROGRAM_STR = "Input Calling Program";
	private static final String INPUT_REGION_STR = "Input Region";
	private static final String INPUT_LOCALE_STR = "Input Locale";
	private static final String INPUT_CALLING_SYSTEM_STR = "Input Calling System";
	private static final String INPUT_TRANSEFFDATE_STR = "Input TransEffDate";
	private static final String INPUT_COMPANY_STR = "Input Company";

	private static final String REQUEST_TYPE_PREM = "Premiumrec";
	private static final String REQUEST_TYPE_LEXT = "Vpxlextrec";
	private static final String REQUEST_TYPE_ACBL = "Vpxacblrec";
	private static final String REQUEST_TYPE_CHDR = "Vpxchdrrec";
	private static final String REQUEST_TYPE_LINK = "Comlinkrec";
	private static final String REQUEST_TYPE_TALL = "Untallrec";
	private static final String RESPONSE_KEY = "=";
	private static final String RESPONSE_MSG_KEY = "=m";
	private static final String STATUS_OK = "****";

	private static final String VPMS_ERROR_MSG = "Exception in VPMScaller: ";

	@Autowired
	private Environment env;

	public boolean checkVPMSModel(String modelName) {
		for (Calculation calculation : calculations.getCalculation()) {
			if (modelName.equals(calculation.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * It used to call the PRMPM01,PRMPM03,PRMPM17
	 * 
	 * @param vpmsinfoDTO
	 * @param premiumrec
	 * @param vpxacbl
	 * @param vpxlext
	 */
	public void callPRMPMREST(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec, VpxacblDTO vpxacbl, VpxlextDTO vpxlext) {

		String vpmsIpAddress = env.getProperty("vpms.ip.address");
		String vpmsIpPort = env.getProperty("vpms.ip.port");

		StringBuilder vpmsUrl = new StringBuilder();
		vpmsUrl.append("http://");
		vpmsUrl.append(vpmsIpAddress);
		vpmsUrl.append(":");
		vpmsUrl.append(vpmsIpPort);
		vpmsUrl.append("/vpms/v2/rest/calc/INTLIFE_DEV");
		String result = HttpRequest.post(vpmsUrl.toString())
				.send(createJsonData(vpmsinfoDTO, premiumrec, vpxacbl, vpxlext).toString()).body();
		parseResponse(vpmsinfoDTO, result, premiumrec);
		premiumrec.setStatuz(STATUS_OK);
	}
	public void callPRMPMRESTNew(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec, VpxlextDTO vpxlext, VpxchdrDTO vpxchdr) {

		String vpmsIpAddress = env.getProperty("vpms.ip.address");
		String vpmsIpPort = env.getProperty("vpms.ip.port");

		StringBuilder vpmsUrl = new StringBuilder();
		vpmsUrl.append("http://");
		vpmsUrl.append(vpmsIpAddress);
		vpmsUrl.append(":");
		vpmsUrl.append(vpmsIpPort);
		vpmsUrl.append("/vpms/v2/rest/calc/INTLIFE_DEV");
		String result = HttpRequest.post(vpmsUrl.toString())
				.send(createJsonDataNew(vpmsinfoDTO, premiumrec,vpxlext,vpxchdr).toString()).body();
		parseResponse(vpmsinfoDTO, result, premiumrec);
		premiumrec.setStatuz(STATUS_OK);
	}
	
	public void callVPMCMCPMT(VPMSinfoDTO vpmsinfoDTO, ComlinkDTO comlink) {

		String vpmsIpAddress = env.getProperty("vpms.ip.address");
		String vpmsIpPort = env.getProperty("vpms.ip.port");

		StringBuilder vpmsUrl = new StringBuilder();
		vpmsUrl.append("http://");
		vpmsUrl.append(vpmsIpAddress);
		vpmsUrl.append(":");
		vpmsUrl.append(vpmsIpPort);
		vpmsUrl.append("/vpms/v2/rest/calc/INTLIFE_DEV");
		String result = HttpRequest.post(vpmsUrl.toString())
				.send(createJsonData(vpmsinfoDTO, null, null,null, comlink).toString()).body();
		parseResponse(vpmsinfoDTO, result, comlink);
		comlink.setStatuz(STATUS_OK);
	}
	
	public void callVEUAL(VPMSinfoDTO vpmsinfoDTO, UntallDTO untall) {
		
		String vpmsIpAddress = env.getProperty("vpms.ip.address");
		String vpmsIpPort = env.getProperty("vpms.ip.port");

		StringBuilder vpmsUrl = new StringBuilder();
		vpmsUrl.append("http://");
		vpmsUrl.append(vpmsIpAddress);
		vpmsUrl.append(":");
		vpmsUrl.append(vpmsIpPort);
		vpmsUrl.append("/vpms/v2/rest/calc/INTLIFE_DEV");
		String result = HttpRequest.post(vpmsUrl.toString())
				.send(createJsonData(vpmsinfoDTO, null, null,null, untall).toString()).body();
		parseResponse(vpmsinfoDTO, result, untall);
		untall.setStatuz(STATUS_OK);
	}

	private void parseResponse(VPMSinfoDTO vpmsinfoDTO, String result, PremiumDTO premiumrec) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map assetMgmtOutputList = null;
		Calculation currCalculation = null;
		for (Calculation calculation : calculations.getCalculation()) {
			if (vpmsinfoDTO.getModelName().equals(calculation.getName())) {
				currCalculation = calculation;
				break;
			}
		}
		try {
			assetMgmtOutputList = objectMapper.readValue(result, Map.class);
			Map computesMap = ((Map) assetMgmtOutputList.get("computes"));
			for (Responsemap resMap : currCalculation.getResponsemap()) {
				if (REQUEST_TYPE_PREM.equals(resMap.getType())) {
					for (int i = 0; i < resMap.getContent().size(); i++) {
						Object resKey = resMap.getContent().get(i);
						if (resKey instanceof String) {
							continue;
						}
						Attribute attr = (Attribute)resKey;
						if (computesMap.containsKey(attr.getValue())) {
							Object resultStr = ((Map) computesMap.get(attr.getValue())).get(RESPONSE_KEY);
							if ("".equals(resultStr.toString())) {
								Object msgStr = ((Map) computesMap.get(attr.getValue())).get(RESPONSE_MSG_KEY);
								//resultStr=BigDecimal.ONE;
								throw new SubroutineIOException(VPMS_ERROR_MSG + msgStr);
							}
							Field f = premiumrec.getClass().getDeclaredField(attr.getName());
							f.setAccessible(true);
							if (f.getType().isAssignableFrom(BigDecimal.class)) {
								f.set(premiumrec, new BigDecimal(resultStr.toString()));
							}
						}
					}
				}
			}
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| IOException e) {
			throw new SubroutineIOException(VPMS_ERROR_MSG + e.getMessage());
		}
	}
	
	private void parseResponse(VPMSinfoDTO vpmsinfoDTO, String result, UntallDTO untallrec) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map assetMgmtOutputList = null;
		Calculation currCalculation = null;
		for (Calculation calculation : calculations.getCalculation()) {
			if (vpmsinfoDTO.getModelName().equals(calculation.getName())) {
				currCalculation = calculation;
				break;
			}
		}
		try {
			assetMgmtOutputList = objectMapper.readValue(result, Map.class);
			Map computesMap = ((Map) assetMgmtOutputList.get("computes"));
			for (Responsemap resMap : currCalculation.getResponsemap()) {
				if (REQUEST_TYPE_TALL.equals(resMap.getType())) {
					for (int i = 0; i < resMap.getContent().size(); i++) {
						Object resKey = resMap.getContent().get(i);
						if (resKey instanceof String) {
							continue;
						}
						Attribute attr = (Attribute)resKey;
						if (computesMap.containsKey(attr.getValue())) {
							Object resultStr = ((Map) computesMap.get(attr.getValue())).get(RESPONSE_KEY);
							if ("".equals(resultStr.toString())) {
								Object msgStr = ((Map) computesMap.get(attr.getValue())).get(RESPONSE_MSG_KEY);
								//resultStr=BigDecimal.ONE;
								throw new SubroutineIOException(VPMS_ERROR_MSG + msgStr);
							}
							Field f = untallrec.getClass().getDeclaredField(attr.getName());
							f.setAccessible(true);
							if (f.getType().isAssignableFrom(BigDecimal.class)) {
								f.set(untallrec, new BigDecimal(resultStr.toString()));
							}
						}
					}
				}
			}
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| IOException e) {
			throw new SubroutineIOException(VPMS_ERROR_MSG + e.getMessage());
		}
	}
	
	private void parseResponse(VPMSinfoDTO vpmsinfoDTO, String result, ComlinkDTO comlinkrec) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map assetMgmtOutputList = null;
		Calculation currCalculation = null;
		for (Calculation calculation : calculations.getCalculation()) {
			if (vpmsinfoDTO.getModelName().equals(calculation.getName())) {
				currCalculation = calculation;
				break;
			}
		}
		try {
			assetMgmtOutputList = objectMapper.readValue(result, Map.class);
			Map computesMap = ((Map) assetMgmtOutputList.get("computes"));
			for (Responsemap resMap : currCalculation.getResponsemap()) {
				if (REQUEST_TYPE_LINK.equals(resMap.getType())) {
					for (int i = 0; i < resMap.getContent().size(); i++) {
						Object resKey = resMap.getContent().get(i);
						if (resKey instanceof String) {
							continue;
						}
						Attribute attr = (Attribute)resKey;
						if (computesMap.containsKey(attr.getValue())) {
							Object resultStr = ((Map) computesMap.get(attr.getValue())).get(RESPONSE_KEY);
							if ("".equals(resultStr.toString())) {
								Object msgStr = ((Map) computesMap.get(attr.getValue())).get(RESPONSE_MSG_KEY);
								//resultStr=BigDecimal.ONE;
								throw new SubroutineIOException(VPMS_ERROR_MSG + msgStr);
							}
							Field f = comlinkrec.getClass().getDeclaredField(attr.getName());
							f.setAccessible(true);
							if (f.getType().isAssignableFrom(BigDecimal.class)) {
								f.set(comlinkrec, new BigDecimal(resultStr.toString()));
							}
						}
					}
				}
			}
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| IOException e) {
			throw new SubroutineIOException(VPMS_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * The method returns a string consisting of all input parameters required by
	 * vp/ms
	 * 
	 * @return CMD_ARRAY String
	 */
	private JSONObject createJsonData(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec, VpxacblDTO vpxacbl,
			VpxlextDTO vpxlext) {
		return createJsonData(vpmsinfoDTO, premiumrec, vpxacbl, vpxlext, null);
	}
	private JSONObject createJsonData(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec, VpxacblDTO vpxacbl,
			VpxlextDTO vpxlext, Object object) {
		Calculation currCalculation = null;
		for (Calculation calculation : calculations.getCalculation()) {
			if (vpmsinfoDTO.getModelName().equals(calculation.getName())) {
				currCalculation = calculation;
				break;
			}
		}
		Map<String, String> parameters = new HashMap<>();
		try {
			for (Requestmap reqMap : currCalculation.getRequestmap()) {
				if (REQUEST_TYPE_PREM.equals(reqMap.getType())) {
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = premiumrec.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(premiumrec) != null ? f.get(premiumrec).toString() : "");
					}
				} else if (REQUEST_TYPE_LEXT.equals(reqMap.getType()) && vpxlext != null) {
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = vpxlext.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(vpxlext) != null ? f.get(vpxlext).toString() : "");
					}
				} else if (REQUEST_TYPE_ACBL.equals(reqMap.getType()) && vpxacbl != null) {
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = vpxacbl.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(vpxacbl) != null ? f.get(vpxacbl).toString() : "");
					}
				} else if(REQUEST_TYPE_LINK.equals(reqMap.getType()) && object != null) {
					ComlinkDTO comlink = (ComlinkDTO)object;
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = comlink.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(comlink) != null ? f.get(comlink).toString() : "");
					}
				}else if(REQUEST_TYPE_TALL.equals(reqMap.getType()) && object != null) {
					UntallDTO untallrec = (UntallDTO)object;
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = untallrec.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(untallrec) != null ? f.get(untallrec).toString() : "");
					}
				}
			}
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new SubroutineIOException(VPMS_ERROR_MSG + e.getMessage());
		}
		Map<String, String> inputMap = new HashMap<>();
		JSONObject restrequest = new JSONObject();
		JSONObject jtemp = new JSONObject();
		jtemp.put("vars", inputMap);
		restrequest.put("update", jtemp);
		jtemp = new JSONObject();
		addCommonField(parameters, vpmsinfoDTO.getTransDate(), vpmsinfoDTO.getCoy());
		jtemp.put("vars", parameters);
		restrequest.put("state", jtemp);

		jtemp = new JSONObject();
		JSONArray outputs = new JSONArray();
		jtemp.put("vars", outputs);
		outputs = new JSONArray();
		for (Responsemap resMap : currCalculation.getResponsemap()) {
			for (int i = 0; i < resMap.getContent().size(); i++) {
				Object resKey = resMap.getContent().get(i);
				if (resKey instanceof String) {
					continue;
				}
				outputs.put(resMap.getContent().get(i).getValue());
			}
		}
		jtemp.put("computes", outputs);
		restrequest.put("calc", jtemp);
		return restrequest;
	}		
	private JSONObject createJsonDataNew(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec,VpxlextDTO vpxlext,VpxchdrDTO vpxchdr) {
		Calculation currCalculation = null;
		for (Calculation calculation : calculations.getCalculation()) {
			if (vpmsinfoDTO.getModelName().equals(calculation.getName())) {
				currCalculation = calculation;
				break;
			}
		}
		Map<String, String> parameters = new HashMap<>();
		try {
			for (Requestmap reqMap : currCalculation.getRequestmap()) {
				if (REQUEST_TYPE_PREM.equals(reqMap.getType())) {
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = premiumrec.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(premiumrec) != null ? f.get(premiumrec).toString() : "");
					}
				} else if (REQUEST_TYPE_LEXT.equals(reqMap.getType()) && vpxlext != null) {
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = vpxlext.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(vpxlext) != null ? f.get(vpxlext).toString() : "");
					}
				} else if (REQUEST_TYPE_CHDR.equals(reqMap.getType()) && vpxchdr != null) {
					for (Attribute attr : reqMap.getAttribute()) {
						Field f = vpxchdr.getClass().getDeclaredField(attr.getName());
						f.setAccessible(true);
						parameters.put(attr.getValue(), f.get(vpxchdr) != null ? f.get(vpxchdr).toString() : "");
					}
				}
			}
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new SubroutineIOException(VPMS_ERROR_MSG + e.getMessage());
		}
		Map<String, String> inputMap = new HashMap<>();
		JSONObject restrequest = new JSONObject();
		JSONObject jtemp = new JSONObject();
		jtemp.put("vars", inputMap);
		restrequest.put("update", jtemp);
		jtemp = new JSONObject();
		addCommonField(parameters, vpmsinfoDTO.getTransDate(), vpmsinfoDTO.getCoy());
		jtemp.put("vars", parameters);
		restrequest.put("state", jtemp);

		jtemp = new JSONObject();
		JSONArray outputs = new JSONArray();
		jtemp.put("vars", outputs);
		outputs = new JSONArray();
		for (Responsemap resMap : currCalculation.getResponsemap()) {
			for (int i = 0; i < resMap.getContent().size(); i++) {
				Object resKey = resMap.getContent().get(i);
				if (resKey instanceof String) {
					continue;
				}
				outputs.put(resMap.getContent().get(i).getValue());
			}
		}
		jtemp.put("computes", outputs);
		restrequest.put("calc", jtemp);
		return restrequest;
	}

	private void addCommonField(Map<String, String> parameters, String transDate, String coy) {
		parameters.put(INPUT_LANGUAGE_STR, env.getProperty("input.language"));
		parameters.put(INPUT_CARRIERCODE_STR, env.getProperty("input.carriercode"));
		parameters.put(INPUT_CALLING_PROGRAM_STR, env.getProperty("input.calling.program"));
		parameters.put(INPUT_REGION_STR, env.getProperty("input.region"));
		parameters.put(INPUT_LOCALE_STR, env.getProperty("input.locale"));
		parameters.put(INPUT_CALLING_SYSTEM_STR, env.getProperty("input.calling.system"));
		parameters.put(INPUT_TRANSEFFDATE_STR, transDate);
		parameters.put(INPUT_COMPANY_STR, coy);
	}
}
