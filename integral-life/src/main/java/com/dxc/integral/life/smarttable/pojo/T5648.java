package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class T5648 {
  	private BigDecimal pctinc;

	public BigDecimal getPctinc() {
		return pctinc;
	}

	public void setPctinc(BigDecimal pctinc) {
		this.pctinc = pctinc;
	}

}
