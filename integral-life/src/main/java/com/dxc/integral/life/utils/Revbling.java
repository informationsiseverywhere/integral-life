package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.ReverseDTO;

public interface Revbling {
	public ReverseDTO processRevbilling(ReverseDTO reverseDTO) throws IOException,ParseException;
}
