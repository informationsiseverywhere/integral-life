package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AglfpfDAO;
import com.dxc.integral.life.dao.model.Aglfpf;
import com.dxc.integral.life.dao.model.Clexpf;
/* 
 * 
 * @author wli31
 *
 */
@Repository("aglfpfDAO")
@Lazy
public class AglfpfDAOImpl extends BaseDAOImpl implements AglfpfDAO {

	@Override
	public Aglfpf searchAglflnb(String coy, String agntNum) {
		String sql = "select * from aglfpf where agntcoy=? and agntnum=? order by agntcoy asc, agntnum asc, unique_number desc";
		Aglfpf aglfpf = null;
		try {
			aglfpf = jdbcTemplate.queryForObject(sql, new Object[] { coy, agntNum },
					new BeanPropertyRowMapper<Aglfpf>(Aglfpf.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
		return aglfpf;
	}

}
