package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.AgentDAO;
import com.dxc.integral.fsu.dao.model.Agent;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.RlagprdDTO;
import com.dxc.integral.life.dao.AgprpfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.MacfpfDAO;
import com.dxc.integral.life.dao.MaprpfDAO;
import com.dxc.integral.life.dao.model.Agprpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Macfpf;
import com.dxc.integral.life.dao.model.Maprpf;
import com.dxc.integral.life.smarttable.pojo.Tm603;
import com.dxc.integral.life.utils.Rlagprd;

/**
 * The Rlagprd utility implementation.
 * 
 * @author vhukumagrawa
 *
 */
@Service("rlagprd")
@Lazy
public class RlagprdImpl implements Rlagprd {

	/** Constant C */
	private static final String CONSTANT_C = "C";
	
	/** Constant P */
	private static final String CONSTANT_P = "P";

	/** Datcon3 subroutine. */
	@Autowired
	private Datcon3 datcon3;

	/** The AgentDAO */
	@Autowired
	private AgentDAO agentDAO;

	/** The MacfpfDAO */
	@Autowired
	private MacfpfDAO macfpfDAO;

	/** The CovrpfDAO */
	@Autowired
	private CovrpfDAO covrpfDAO;

	/** The AgprpfDAO */
	@Autowired
	private AgprpfDAO agprpfDAO;

	/** The MaprpfDAO */
	@Autowired
	private MaprpfDAO maprpfDAO;
	
	/** The ItempfDAO */
	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/

	private BigDecimal mlperpc;
	private BigDecimal mlperpp;
	private BigDecimal mlgrppc;
	private BigDecimal mlgrppp;
	private BigDecimal mldirpc;
	private BigDecimal mldirpp;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dxc.integral.life.utils.Rlagprd#calculateAndUpdateMAPR(com.dxc.
	 * integral.life.beans.RlagprdDTO)
	 */
	@Override/*ILIFE-5977*/
	public void calculateAndUpdateMAPR(RlagprdDTO rlagprdDTO) throws IOException,ParseException {//ILIFE-5763
		Map<String, Itempf> tm603Map=itempfDAO.readSmartTableByTableName2(rlagprdDTO.getAgntcoy(), "TM603", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/ /*IJTI-379*/
		// Get year difference
		int yearsDiff = datcon3.getYearsDifference(rlagprdDTO.getOccdate(), rlagprdDTO.getEffdate()).intValue();

		// read AGLFPF
		Agent agent = agentDAO.readAgentByCompanyAndAgentNumber(rlagprdDTO.getAgntcoy(), rlagprdDTO.getAgntnum());

		Macfpf macfpfRecord = macfpfDAO
				.readMacfpfByAgentNumberAndAgentCompany(rlagprdDTO.getAgntcoy(), rlagprdDTO.getAgntnum()).get(0);
		String agentType;
		if ("".equals(macfpfRecord.getMlagttyp().trim())) {
			agentType = agent.getAgtype();
		} else {
			agentType = macfpfRecord.getMlagttyp().trim();
		}

		Itempf itempf = tm603Map.get(agentType + "***");
		Tm603 tm603Item = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(), Tm603.class); /*IJTI-398*/

		boolean isActiveFlagOff = false;
		for (String activeFlag : tm603Item.getActives()) {
			if (!"Y".equals(activeFlag)) {
				isActiveFlagOff = true;
			} else {
				isActiveFlagOff = false;
			}
		}

		if (isActiveFlagOff) {
			return;
		}

		// personal production update
		updatePersonalProduction(rlagprdDTO, yearsDiff);

		// update production
		List<String> reportTags = new ArrayList<>();
		reportTags.add(macfpfRecord.getZrptga());
		reportTags.add(macfpfRecord.getZrptgb());
		reportTags.add(macfpfRecord.getZrptgc());
		reportTags.add(macfpfRecord.getZrptgd());
		updateProduction(reportTags, rlagprdDTO, tm603Item, yearsDiff);
	}

	/**
	 * Updates personal production figures.
	 * 
	 * @param rlagprdDTO
	 *            - RlagprdDTO
	 * @param yearsDiff
	 *            - difference between OCCDATE and effective date
	 */
	private void updatePersonalProduction(RlagprdDTO rlagprdDTO, int yearsDiff) {
		mlperpc = BigDecimal.ZERO;
		mlperpp = BigDecimal.ZERO;
		mlgrppc = BigDecimal.ZERO;
		mlgrppp = BigDecimal.ZERO;
		mldirpc = BigDecimal.ZERO;
		mldirpp = BigDecimal.ZERO;
		BigDecimal totalSumInsuranceAmount = BigDecimal.ZERO;
		int policyCount = 0;

		if (CONSTANT_C.equals(rlagprdDTO.getPrdflg())) {
			mlperpc = mlperpc.add(rlagprdDTO.getOrigamt());
			mldirpc = mldirpc.add(rlagprdDTO.getOrigamt());
			mlgrppc = mlgrppc.add(rlagprdDTO.getOrigamt());
		} else {
			mlperpp = mlperpp.add(rlagprdDTO.getOrigamt());
			mldirpp = mldirpp.add(rlagprdDTO.getOrigamt());
			mlgrppp = mlgrppp.add(rlagprdDTO.getOrigamt());
		}

		if (("T642".equals(rlagprdDTO.getBatctrcde()) || "T607".equals(rlagprdDTO.getBatctrcde()))
				&& CONSTANT_P.equals(rlagprdDTO.getPrdflg())) {
			totalSumInsuranceAmount = calculateSumInsuranceAmount(rlagprdDTO.getChdrcoy(), rlagprdDTO.getChdrnum());
		}

		if ("T642".equals(rlagprdDTO.getBatctrcde()) && CONSTANT_P.equals(rlagprdDTO.getPrdflg())) {
			policyCount += policyCount;
		}

		if ("T607".equals(rlagprdDTO.getBatctrcde()) && CONSTANT_P.equals(rlagprdDTO.getPrdflg())) {
			policyCount -= policyCount;
			totalSumInsuranceAmount = totalSumInsuranceAmount.multiply(new BigDecimal(-1));
		}

		update(rlagprdDTO, rlagprdDTO.getAgntnum(), rlagprdDTO.getEffdate(), yearsDiff, policyCount,
				totalSumInsuranceAmount);
	}

	/**
	 * Updates Agent Production Figures.
	 * 
	 * @param reportTags
	 *            - Reportees List
	 * @param rlagprdDTO
	 *            - RlagprdDTO
	 * @param tm603Item
	 *            - TM603 smart table item record
	 * @param yearsDiff
	 *            - difference between OCCDATE and effective date
	 */
	private void updateProduction(List<String> reportTags, RlagprdDTO rlagprdDTO, Tm603 tm603Item, int yearsDiff) {
		for (String reportTag : reportTags) {

			if ("".equals(reportTag.trim())) {
				break;
			}

			Macfpf macfpf = macfpfDAO.readMacfpfByAgentNumberAndAgentCompany(rlagprdDTO.getAgntcoy(), reportTag).get(0);
			mlgrppc = BigDecimal.ZERO;
			mlgrppp = BigDecimal.ZERO;
			mldirpc = BigDecimal.ZERO;
			mldirpp = BigDecimal.ZERO;
			mlperpc = BigDecimal.ZERO;
			mlperpp = BigDecimal.ZERO;

			if (isTeamFlagOn(tm603Item, macfpf.getMlagttyp())) {
				if (CONSTANT_C.equals(rlagprdDTO.getPrdflg())) {
					mlgrppc = rlagprdDTO.getOrigamt();
				} else {
					mlgrppp = rlagprdDTO.getOrigamt();
				}
			}

			if (isOvrFlagOn(tm603Item, macfpf.getMlagttyp())) {
				if (CONSTANT_C.equals(rlagprdDTO.getPrdflg())) {
					mldirpc = rlagprdDTO.getOrigamt();
				} else {
					mldirpp = rlagprdDTO.getOrigamt();
				}
			}
			update(rlagprdDTO, reportTag, macfpf.getEffdate(), yearsDiff, 0, BigDecimal.ZERO);
		}
	}

	/**
	 * Determines if team flag is on.
	 * 
	 * @param tm603Item
	 *            - TM603 smart table item
	 * @param mlagttype
	 *            - Agent Type
	 * @return - true/false
	 */
	private boolean isTeamFlagOn(Tm603 tm603Item, String mlagttype) {
		boolean teamFlag = false;
		for (String mlagttyp : tm603Item.getMlagttyps()) {
			if (mlagttype.equals(mlagttyp)) {
				teamFlag = true;
				break;
			}
		}
		return teamFlag;
	}

	/**
	 * Determines if over flag is on.
	 * 
	 * @param tm603Item
	 *            - TM603 smart table item
	 * @param mlagttype
	 *            - Agent Type
	 * @return - true/false
	 */
	private boolean isOvrFlagOn(Tm603 tm603Item, String mlagttype) {
		boolean ovrFlag = false;
		for (String agtype : tm603Item.getAgtypes()) {
			if (mlagttype.equals(agtype)) {
				ovrFlag = true;
				break;
			}
		}
		return ovrFlag;
	}

	/**
	 * Writes to AGPRPF or updates/writes to MAPRPF depending upon transaction
	 * code.
	 * 
	 * @param rlagprdDTO
	 *            - RlagprdDTO
	 * @param agentNumber
	 *            - agent number
	 * @param effectiveDate
	 *            - effective date
	 * @param yearsDiff
	 *            - difference between OCCDATE and effective date
	 * @param policyCount
	 *            - policy count
	 * @param totalSumInsured
	 *            - sum insured
	 */
	private void update(RlagprdDTO rlagprdDTO, String agentNumber, int effectiveDate, int yearsDiff, int policyCount,
			BigDecimal totalSumInsured) {
		if ("B".equals(rlagprdDTO.getBatctrcde().substring(0, 1))) {
			// deferred Update
			writeAgprpf(rlagprdDTO, agentNumber, effectiveDate, yearsDiff, totalSumInsured);
		} else {
			// write mapr
			updateMaprpf(rlagprdDTO, agentNumber, effectiveDate, yearsDiff, policyCount, totalSumInsured);
		}
	}

	/**
	 * Writes to AGPRPF
	 * 
	 * @param rlagprdDTO
	 *            - RlagprdDTO
	 * @param agentNumber
	 *            - agent number
	 * @param effectiveDate
	 *            - effective date
	 * @param yearsDiff
	 *            - difference between OCCDATE and effective date
	 * @param totalSumInsured
	 *            - sum insured
	 */
	private void writeAgprpf(RlagprdDTO rlagprdDTO, String agentNumber, int effectiveDate, int yearsDiff,
			BigDecimal totalSumInsured) {
		Agprpf agprpf = new Agprpf();
		List<BigDecimal> initialList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			initialList.add(BigDecimal.ZERO);
		}
		agprpf.setMlperpp(initialList);
		agprpf.setMlperpc(initialList);
		agprpf.setMldirpp(initialList);
		agprpf.setMldirpc(initialList);
		agprpf.setMlgrppp(initialList);
		agprpf.setMlgrppc(initialList);
		agprpf.setCntcount(0);
		agprpf.setAgntcoy(Companies.LIFE);
		agprpf.setAgntnum(agentNumber);
		agprpf.setAcctyr(rlagprdDTO.getActyear());
		agprpf.setMnth(rlagprdDTO.getActmnth());
		agprpf.setCnttype(rlagprdDTO.getCnttype());
		agprpf.setChdrnum(rlagprdDTO.getChdrnum());
		agprpf.setEffdate(effectiveDate);
		if (yearsDiff > 9) {
			agprpf.getMlperpp().set(9, mlperpp);
			agprpf.getMlperpc().set(9, mlperpc);
			agprpf.getMldirpp().set(9, mldirpp);
			agprpf.getMldirpc().set(9, mldirpc);
			agprpf.getMlgrppp().set(9, mlgrppp);
			agprpf.getMlgrppc().set(9, mlgrppc);
		} else {
			agprpf.getMlperpp().set(yearsDiff, mlperpp);
			agprpf.getMlperpc().set(yearsDiff, mlperpc);
			agprpf.getMldirpp().set(yearsDiff, mldirpp);
			agprpf.getMldirpc().set(yearsDiff, mldirpc);
			agprpf.getMlgrppp().set(yearsDiff, mlgrppp);
			agprpf.getMlgrppc().set(yearsDiff, mlgrppc);
		}

		agprpf.setSumins(totalSumInsured);
		agprpf.setDatime(new Timestamp(System.currentTimeMillis()));
		agprpfDAO.writeAgprpf(agprpf);
	}

	/**
	 * Updates MAPRPF if record already exists or inserts a new record.
	 * 
	 * @param rlagprdDTO
	 *            - RlagprdDTO
	 * @param agentNumber
	 *            - agent number
	 * @param effectiveDate
	 *            - effective date
	 * @param yearsDiff
	 *            - difference between OCCDATE and effective date
	 * @param policyCount
	 *            - policy count
	 * @param totalSumInsured
	 *            - sum insured
	 */
	private void updateMaprpf(RlagprdDTO rlagprdDTO, String agentNumber, int effectiveDate, int yearsDiff,
			int policyCount, BigDecimal totalSumInsured) {

		Maprpf maprpf = maprpfDAO.readMaprpfByAgentNumberCompanyAndEffDate(rlagprdDTO.getAgntcoy(), agentNumber,
				effectiveDate);
		if (maprpf != null) {
			List<BigDecimal> initialList = new ArrayList<>();
			for (int i = 0; i < 10; i++) {
				initialList.add(BigDecimal.ZERO);
			}
			maprpf.setMlperpp(initialList);
			maprpf.setMlperpc(initialList);
			maprpf.setMldirpp(initialList);
			maprpf.setMldirpc(initialList);
			maprpf.setMlgrppp(initialList);
			maprpf.setMlgrppc(initialList);
			if (yearsDiff > 9) {
				maprpf.getMlperpp().set(9, maprpf.getMlperpp().get(9).add(mlperpp));
				maprpf.getMlperpc().set(9, maprpf.getMlperpc().get(9).add(mlperpc));
				maprpf.getMldirpp().set(9, maprpf.getMldirpp().get(9).add(mldirpp));
				maprpf.getMldirpc().set(9, maprpf.getMldirpc().get(9).add(mldirpc));
				maprpf.getMlgrppp().set(9, maprpf.getMlgrppp().get(9).add(mlgrppp));
				maprpf.getMlgrppc().set(9, maprpf.getMlgrppc().get(9).add(mlgrppc));
			} else {
				maprpf.getMlperpp().set(yearsDiff, maprpf.getMlperpp().get(yearsDiff).add(mlperpp));
				maprpf.getMlperpc().set(yearsDiff, maprpf.getMlperpc().get(yearsDiff).add(mlperpc));
				maprpf.getMldirpp().set(yearsDiff, maprpf.getMldirpp().get(yearsDiff).add(mldirpp));
				maprpf.getMldirpc().set(yearsDiff, maprpf.getMldirpc().get(yearsDiff).add(mldirpc));
				maprpf.getMlgrppp().set(yearsDiff, maprpf.getMlgrppp().get(yearsDiff).add(mlgrppp));
				maprpf.getMlgrppc().set(yearsDiff, maprpf.getMlgrppc().get(yearsDiff).add(mlgrppc));
			}
			maprpf.setCntcount(maprpf.getCntcount() + policyCount);
			maprpf.setAcctyr(rlagprdDTO.getActyear());
			maprpf.setMnth(rlagprdDTO.getActmnth());
			maprpf.setCnttype(rlagprdDTO.getCnttype());
			maprpf.setSumins(maprpf.getSumins().add(totalSumInsured));
			maprpfDAO.updateMaprpfByAgentNumberCompanyAndEffDate(rlagprdDTO.getAgntcoy(), agentNumber, effectiveDate,
					maprpf);

		} else {
			Maprpf maprpfInsert = new Maprpf();
			List<BigDecimal> initialList = new ArrayList<>();
			for (int i = 0; i < 10; i++) {
				initialList.add(BigDecimal.ZERO);
			}
			maprpfInsert.setMlperpp(initialList);
			maprpfInsert.setMlperpc(initialList);
			maprpfInsert.setMldirpp(initialList);
			maprpfInsert.setMldirpc(initialList);
			maprpfInsert.setMlgrppp(initialList);
			maprpfInsert.setMlgrppc(initialList);
			if (yearsDiff > 9) {
				maprpfInsert.getMlperpp().set(9, mlperpp);
				maprpfInsert.getMlperpc().set(9, mlperpc);
				maprpfInsert.getMldirpp().set(9, mldirpp);
				maprpfInsert.getMldirpc().set(9, mldirpc);
				maprpfInsert.getMlgrppp().set(9, mlgrppp);
				maprpfInsert.getMlgrppc().set(9, mlgrppc);
			} else {
				maprpfInsert.getMlperpp().set(yearsDiff, mlperpp);
				maprpfInsert.getMlperpc().set(yearsDiff, mlperpc);
				maprpfInsert.getMldirpp().set(yearsDiff, mldirpp);
				maprpfInsert.getMldirpc().set(yearsDiff, mldirpc);
				maprpfInsert.getMlgrppp().set(yearsDiff, mlgrppp);
				maprpfInsert.getMlgrppc().set(yearsDiff, mlgrppc);
			}
			maprpfInsert.setCntcount(policyCount);
			maprpfInsert.setAgntcoy(agentNumber);
			maprpfInsert.setAcctyr(rlagprdDTO.getActyear());
			maprpfInsert.setMnth(rlagprdDTO.getActmnth());
			maprpfInsert.setCnttype(rlagprdDTO.getCnttype());
			maprpfInsert.setEffdate(effectiveDate);
			maprpfInsert.setSumins(totalSumInsured);

			maprpfDAO.insertMaprpfRecords(maprpfInsert);
		}

	}

	/**
	 * Calculates total insured value.
	 * 
	 * @param company
	 *            - company
	 * @param contractNumber
	 *            - contract number
	 * @return - total sum insured
	 */
	private BigDecimal calculateSumInsuranceAmount(String company, String contractNumber) {
		List<Covrpf> covrRecords = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(company, contractNumber);
		BigDecimal totalSumins = BigDecimal.ZERO;
		for (Covrpf covrpf : covrRecords) {
			String riderFirstCharacter = covrpf.getRider().substring(0, 1);
			if ("00".equals(covrpf.getRider()) || "D".equals(riderFirstCharacter) || "P".equals(riderFirstCharacter)
					|| "R".equals(riderFirstCharacter)) {
				totalSumins = totalSumins.add(covrpf.getSumins());
			}
		}

		return totalSumins;
	}

}
