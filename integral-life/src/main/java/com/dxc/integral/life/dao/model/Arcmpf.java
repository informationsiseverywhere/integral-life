package com.dxc.integral.life.dao.model;

public class Arcmpf {

	private String fileName = "";
	private int acctyr = 0;
	private int acctmnth = 0;
	private String userProfile = "";
	private String jobName = "";
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(int acctyr) {
		this.acctyr = acctyr;
	}
	public int getAcctmnth() {
		return acctmnth;
	}
	public void setAcctmnth(int acctmnth) {
		this.acctmnth = acctmnth;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	
	
}
