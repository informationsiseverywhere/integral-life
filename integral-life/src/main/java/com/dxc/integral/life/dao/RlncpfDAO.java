package com.dxc.integral.life.dao;

import java.util.List;
import com.dxc.integral.life.dao.model.Rlncpf;

public interface RlncpfDAO {
	
	public int deleteRlncpfData();
	public List<Rlncpf> readRecordOfRlncpf();

}
