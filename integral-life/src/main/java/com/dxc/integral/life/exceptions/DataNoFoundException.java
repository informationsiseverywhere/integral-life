/**
 * 
 */
package com.dxc.integral.life.exceptions;


/**
 * @author wli31
 *
 */
@SuppressWarnings("serial")
public class DataNoFoundException extends RuntimeException{
	
	private String errorCode;
	private String errorMsg;
	
	/**
	 * @param string the message
	 */
	public DataNoFoundException(String errorMsg) {
		super(errorMsg);
		this.errorMsg=errorMsg;
	}

	/**
	 * @param msg the cause
	 * @param t the message
	 */
	public DataNoFoundException(String msg, Throwable t) {
		super(msg, t);
	}
	
	/**
	 * @param code the Error code
	 * @param msg the Error message
	 */
	public DataNoFoundException(String errorCode, String errorMsg) {
		this.errorCode=errorCode;
		this.errorMsg=errorMsg;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
