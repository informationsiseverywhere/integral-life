package com.dxc.integral.life.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HitrpfDAO;
import com.dxc.integral.life.dao.model.Hitrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;

/**
 * 
 * @author xma3
 *
 */
@Repository("hitrpfDAO")
@Lazy
public class HitrpfDAOImpl extends BaseDAOImpl implements HitrpfDAO {

	@Override
	public int insertHitrpfRecord(Hitrpf hitrpf) {
		StringBuffer sql = new StringBuffer("INSERT INTO HITRPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,PRCSEQ,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,");
		sql.append("ZINTBFND,FUNDAMNT,FNDCURR,FUNDRATE,ZRECTYP,EFFDATE,TRANNO,CNTCURR,CNTAMNT,CNTTYP,CRTABLE,COVDBTIND,ZINTAPPIND,FDBKIND,TRIGER,TRIGKY,SWCHIND,PERSUR,");
		sql.append("SVP,ZLSTINTDTE,ZINTRATE,ZINTEFFDT,ZINTALLOC,INCINUM,INCIPERD01,INCIPERD02,INCIPRM01,INCIPRM02,USTMNO,SACSCODE,SACSTYP,GENLCDE,BSCHEDNAM,BSCHEDNUM,");
		sql.append("USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		return jdbcTemplate.update(sql.toString(), new Object[]{
				hitrpf.getChdrcoy(),hitrpf.getChdrnum(),hitrpf.getLife(),hitrpf.getCoverage(),hitrpf.getRider(),hitrpf.getPlanSuffix(),hitrpf.getProcSeqNo(),hitrpf.getBatccoy()
				,hitrpf.getBatcbrn(),hitrpf.getBatcactyr(),hitrpf.getBatcactmn(),hitrpf.getBatctrcde(),hitrpf.getBatcbatch(),hitrpf.getZintbfnd(),hitrpf.getFundAmount()
				,hitrpf.getFundCurrency(),hitrpf.getFundRate(),hitrpf.getZrectyp(),hitrpf.getEffdate(),hitrpf.getTranno(), hitrpf.getCntcurr(), hitrpf.getContractAmount()
				,hitrpf.getCnttyp(), hitrpf.getCrtable(), hitrpf.getCovdbtind(), hitrpf.getZintappind(), hitrpf.getFeedbackInd(), hitrpf.getTriggerModule(),
				hitrpf.getTriggerKey(), hitrpf.getSwitchIndicator(), hitrpf.getSurrenderPercent(), hitrpf.getSvp(), hitrpf.getZlstintdte(), hitrpf.getZintrate(),
				hitrpf.getZinteffdt(), hitrpf.getZintalloc(), hitrpf.getInciNum(), hitrpf.getInciPerd01(), hitrpf.getInciPerd02(), hitrpf.getInciprm01(), hitrpf.getInciprm02(),
				hitrpf.getUstmno(), hitrpf.getSacscode(), hitrpf.getSacstyp(), hitrpf.getGenlcde(), hitrpf.getScheduleName(), hitrpf.getScheduleNumber(),hitrpf.getUserProfile(),
				hitrpf.getJobName(),new Timestamp(System.currentTimeMillis())
			});


	}
	
	@Override
	public List<Hitrpf> getHitrpfRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, int tranno){
		StringBuffer sql = new StringBuffer("SELECT * FROM HITRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND TRANNO=? ");
		sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, TRANNO ASC,UNIQUE_NUMBER DESC");
		
		return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx, tranno}, new BeanPropertyRowMapper<Hitrpf>(Hitrpf.class));
		
	}
	
	@Override
	public int deleteHitrpfRecord(Hitrpf hitrpf) {
		StringBuffer sql = new StringBuffer("DELETE FROM HITRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND TRANNO=? AND PRCSEQ=?");
	    sql.append(" AND BATCCOY=? AND BATCBRN=? AND BATCACTYR=? AND BATCACTMN=? AND BATCTRCDE=? AND BATCBATCH=?");
	    
	    return jdbcTemplate.update(sql.toString(), new Object[] {hitrpf.getChdrcoy(), hitrpf.getChdrnum(), hitrpf.getLife(), hitrpf.getCoverage(), hitrpf.getRider(), 
	    		hitrpf.getPlanSuffix(), hitrpf.getTranno(), hitrpf.getProcSeqNo()});
	
	}

}
