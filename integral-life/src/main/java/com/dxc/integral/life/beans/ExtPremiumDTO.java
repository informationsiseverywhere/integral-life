package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class ExtPremiumDTO {

	private int freqFactor;
	private int mortRate;
	private int premium;
	private int sumass;
	private int exFactor;
	private int percent;
	private int term;
	private String freq;
	private int loading;
	private String statuz;
	

	public ExtPremiumDTO() {
	}


	public int getFreqFactor() {
		return freqFactor;
	}


	public void setFreqFactor(int freqFactor) {
		this.freqFactor = freqFactor;
	}


	public int getMortRate() {
		return mortRate;
	}


	public void setMortRate(int mortRate) {
		this.mortRate = mortRate;
	}


	public int getPremium() {
		return premium;
	}


	public void setPremium(int premium) {
		this.premium = premium;
	}


	public int getSumass() {
		return sumass;
	}


	public void setSumass(int sumass) {
		this.sumass = sumass;
	}


	public int getExFactor() {
		return exFactor;
	}


	public void setExFactor(int exFactor) {
		this.exFactor = exFactor;
	}


	public int getPercent() {
		return percent;
	}


	public void setPercent(int percent) {
		this.percent = percent;
	}


	public int getTerm() {
		return term;
	}


	public void setTerm(int term) {
		this.term = term;
	}


	public String getFreq() {
		return freq;
	}


	public void setFreq(String freq) {
		this.freq = freq;
	}


	public int getLoading() {
		return loading;
	}


	public void setLoading(int loading) {
		this.loading = loading;
	}


	public String getStatuz() {
		return statuz;
	}


	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}


}