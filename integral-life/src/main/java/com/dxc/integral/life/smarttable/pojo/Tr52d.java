package com.dxc.integral.life.smarttable.pojo;


public class Tr52d {

  	private String txcode;
  	private String txsubr;
  	private String filler;
	public String getTxcode() {
		return txcode;
	}
	public void setTxcode(String txcode) {
		this.txcode = txcode;
	}
	public String getTxsubr() {
		return txsubr;
	}
	public void setTxsubr(String txsubr) {
		this.txsubr = txsubr;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
}