package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Hdispf;

public interface HdispfDAO {

	List<Hdispf> readHdispfRecords(String company, String contractNumber);
	List<Hdispf> readHdispfRecords(String company, String contractNumber, int tranno);
	Hdispf readHdispf(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
	int updateHdispf(Hdispf hdispf);
	int deleteHdispfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
}
