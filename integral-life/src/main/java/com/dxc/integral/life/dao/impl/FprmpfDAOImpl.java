package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.FprmpfDAO;
import com.dxc.integral.life.dao.model.Fpcopf;
import com.dxc.integral.life.dao.model.Fprmpf;

/**
 * 
 * FprmpfDAO implementation for database table <b>Fprmpf</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("fprmpfDAO")
public class FprmpfDAOImpl extends BaseDAOImpl implements FprmpfDAO {

	@Override
	public List<Fprmpf> readFprmpfRecords(String company, String contractNumber){
		StringBuilder sql = new StringBuilder("SELECT * FROM FPRMPF WHERE VALIDFLAG = '1' AND CHDRCOY=? AND CHDRNUM=? ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, PAYRSEQNO, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Fprmpf>(Fprmpf.class));
	}
	
	@Override
	public Fprmpf readFprmpfBySeqno(String chdrcoy, String chdrnum, int payrseqno) {
		StringBuilder sql = new StringBuilder("SELECT * FROM VM1DTA.FPRMPF WHERE CHDRCOY=? AND CHDRNUM=? AND PAYRSEQNO=? ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, PAYRSEQNO, UNIQUE_NUMBER DESC ");
		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum, payrseqno}, 
				new BeanPropertyRowMapper<Fprmpf>(Fprmpf.class));
		
	}
	public void updateFprmpfRecords(List<Fprmpf> fprmpfList){
		List<Fprmpf> tempFprmpfList = fprmpfList;   
		StringBuilder sb = new StringBuilder(" update fprmpf set totbill= ?, minreqd=?, jobnm=?,usrprf=?,datime=? ");
		sb.append("where unique_number=?");   
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempFprmpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setBigDecimal(1, fprmpfList.get(i).getTotbill());
				ps.setBigDecimal(2, fprmpfList.get(i).getMinreqd());
				ps.setString(3, fprmpfList.get(i).getJobnm());
                ps.setString(4, fprmpfList.get(i).getUsrprf());
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(6, fprmpfList.get(i).getUnique_number());
            }  
      }); 
	}
}
