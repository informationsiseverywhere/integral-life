package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T5645 {

	/** The cnttots */
	private List<Integer> cnttots;
	/** The glmaps */
	private List<String> glmaps;
	/** The sacscodes */
	private List<String> sacscodes;
	/** The sacstypes */
	private List<String> sacstypes;
	/** The signs */
	private List<String> signs;

	/**
	 * @return the cnttots
	 */
	public List<Integer> getCnttots() {
		return cnttots;
	}

	/**
	 * @param cnttots
	 *            the cnttots to set
	 */
	public void setCnttots(List<Integer> cnttots) {
		this.cnttots = cnttots;
	}

	/**
	 * @return the glmaps
	 */
	public List<String> getGlmaps() {
		return glmaps;
	}

	/**
	 * @param glmaps
	 *            the glmaps to set
	 */
	public void setGlmaps(List<String> glmaps) {
		this.glmaps = glmaps;
	}

	/**
	 * @return the sacscodes
	 */
	public List<String> getSacscodes() {
		return sacscodes;
	}

	/**
	 * @param sacscodes
	 *            the sacscodes to set
	 */
	public void setSacscodes(List<String> sacscodes) {
		this.sacscodes = sacscodes;
	}

	/**
	 * @return the sacstypes
	 */
	public List<String> getSacstypes() {
		return sacstypes;
	}

	/**
	 * @param sacstypes
	 *            the sacstypes to set
	 */
	public void setSacstypes(List<String> sacstypes) {
		this.sacstypes = sacstypes;
	}

	/**
	 * @return the signs
	 */
	public List<String> getSigns() {
		return signs;
	}

	/**
	 * @param signs
	 *            the signs to set
	 */
	public void setSigns(List<String> signs) {
		this.signs = signs;
	}

}
