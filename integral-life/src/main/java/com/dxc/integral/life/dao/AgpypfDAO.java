package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Agpypf;

/**
 * The AgpypfDAO for database opertations.
 * 
 * @author vhukumagrawa
 *
 */
public interface AgpypfDAO {
	
	/**
	 * Insert into AGPYPF table.
	 * 
	 * @param agpypf 
	 * @return - Integer number of rows inserted
	 */
	public Integer insertAcmvpf(Agpypf agpypf);
}
