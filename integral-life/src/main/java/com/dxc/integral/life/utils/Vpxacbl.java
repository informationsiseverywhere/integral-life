package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VpxacblDTO;

public interface Vpxacbl {
	public VpxacblDTO processVpxacbl(PremiumDTO premiumDTO) throws IOException;
}
