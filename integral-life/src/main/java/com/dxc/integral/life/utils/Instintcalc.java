package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.IntcalcDTO;

public interface Instintcalc {

	public IntcalcDTO processInstintcalc(IntcalcDTO intcalcDTO) throws Exception;
}
