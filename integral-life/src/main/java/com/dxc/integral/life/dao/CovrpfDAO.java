package com.dxc.integral.life.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.dao.model.Covrpf;

/**
 * CovrpfDAO for database table <b>COVRPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */

public interface CovrpfDAO {

	/**
	 * Reads COVRPF records based on company and contract number.
	 * 
	 * @param company
	 *            - company
	 * @param contractNumber
	 *            - contract number
	 * @return - List of COVRPF records.
	 */
	List<Covrpf> readCoverageRecordsByCompanyAndContractNumber(String company, String contractNumber);
	public Covrpf readCovrenqData(Covrpf covrpfModel);
	public List<Covrpf> searchValidCovrpfRecords(String chdrcoy,String chdrnum, String life, String coverage, String rider);
	public List<Covrpf> searchCovrstaRecords(String chdrcoy,String chdrnum, String life, String coverage, String rider,int currfrom,int plnsfx,int tranno);
	public int updateCovrpfCDebt(BigDecimal crdebt, long uniqueNumber);
	public int insertCovrpf(Covrpf covrpf);
	public void updateCovrpfRecords(List<Covrpf> covrList);
	public List<Covrpf> fetchRecords(String company, String cpiDte ,String indxin , String chdrnumTo, String chdrnumFrom, String validFlag);
	public int deleteCovrpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
	public int updateFlagAndCurtoForCovrpf(Covrpf covrpf);
	void insertCovrpfRecords(List<Covrpf> covrpfList);
	public Map<String, List<Covrpf>> searchCovrpf(String chdrcoy, List<String> chdrnum);
}
