package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Loanpf;

/**
 * The LoanpfDAO for database operations.
 * 
 * @author asirvya
 */
public interface LoanpfDAO {
	/**
	 * Get last loan number by company and contract number.
	 * 
	 * @param chdrcoy - the chdrcoy
	 * @param chdrnum - the chdrnum
	 * @return - Loanpf Info
	 */
	public Loanpf getLastLoanNumberByCompanyAndContractNumber(String chdrcoy, String chdrnum);

	/**
	 * Get Loanpf by company, contract and loan number.
	 * 
	 * @param chdrcoy - the chdrcoy 
	 * @param chdrnum - the chdrnum 
	 * @param loanNumber - the loan Number
	 * @return - Loanpf Info
	 */
	public Loanpf getLoanpfByCompanyContractNumberAndLoanNumber(String chdrcoy, String chdrnum, int loanNumber);

	/**
	 * Insert loanpf record.
	 * 
	 * @param loanpf - the 
	 * @return - Integer number of rows inserted
	 */
	public int insertLoanpf(Loanpf loanpf);

	/**
	 * Updates loanpf record.
	 * 
	 * @param loanpf - the 
	 * @return - Update number of rows inserted
	 */
	public int updateLoanpf(Loanpf loanpf);

	/**
	 * Get loanpf list by company and contract number.
	 * 
	 * @param chdrcoy - the chdrcoy  
	 * @param chdrnum - the chdrnum  
	 * @return - List<Loanpf>
	 */
	public List<Loanpf> getLoanpfListByCompanyAndContractNumber(String chdrcoy, String chdrnum);
	public List<Loanpf> getLoanenqList(String chdrcoy, String chdrnum);
	public int updateLoanenq(Loanpf loanpf);
	public int deleteLoadnpf(String chdrcoy, String chdrnum, int loannumber);
	
	void updateLoanpfByUniqueNumberList(List<Loanpf> loanList);
	
}
