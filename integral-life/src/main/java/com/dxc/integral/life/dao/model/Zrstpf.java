package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Zrstpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int seqno;
	private int tranno;
	private int xtranno;
	private String batctrcde;
	private String sacstyp;
	private BigDecimal zramount01;
	private BigDecimal zramount02;
	private int trandate;
	private int ustmno;
	private String feedbackInd;
	private String userProfile;
	private String jobName;
	private Timestamp datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getXtranno() {
		return xtranno;
	}
	public void setXtranno(int xtranno) {
		this.xtranno = xtranno;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public BigDecimal getZramount01() {
		return zramount01;
	}
	public void setZramount01(BigDecimal zramount01) {
		this.zramount01 = zramount01;
	}
	public BigDecimal getZramount02() {
		return zramount02;
	}
	public void setZramount02(BigDecimal zramount02) {
		this.zramount02 = zramount02;
	}
	public int getTrandate() {
		return trandate;
	}
	public void setTrandate(int trandate) {
		this.trandate = trandate;
	}
	public int getUstmno() {
		return ustmno;
	}
	public void setUstmno(int ustmno) {
		this.ustmno = ustmno;
	}
	public String getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(String feedbackInd) {
		this.feedbackInd = feedbackInd;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
}