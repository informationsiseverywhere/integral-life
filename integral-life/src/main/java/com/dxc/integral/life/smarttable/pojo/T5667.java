package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5667 {

	private List<String> freqs;
	private List<BigDecimal> maxAmounts;
	private List<BigDecimal> prmtols;
	private List<BigDecimal> maxamts;
	private List<BigDecimal> prmtolns;
	private String sfind;
	public List<String> getFreqs() {
		return freqs;
	}
	public void setFreqs(List<String> freqs) {
		this.freqs = freqs;
	}
	public List<BigDecimal> getMaxAmounts() {
		return maxAmounts;
	}
	public void setMaxAmounts(List<BigDecimal> maxAmounts) {
		this.maxAmounts = maxAmounts;
	}
	public List<BigDecimal> getPrmtols() {
		return prmtols;
	}
	public void setPrmtols(List<BigDecimal> prmtols) {
		this.prmtols = prmtols;
	}
	public List<BigDecimal> getMaxamts() {
		return maxamts;
	}
	public void setMaxamts(List<BigDecimal> maxamts) {
		this.maxamts = maxamts;
	}
	public List<BigDecimal> getPrmtolns() {
		return prmtolns;
	}
	public void setPrmtolns(List<BigDecimal> prmtolns) {
		this.prmtolns = prmtolns;
	}
	public String getSfind() {
		return sfind;
	}
	public void setSfind(String sfind) {
		this.sfind = sfind;
	}
}
