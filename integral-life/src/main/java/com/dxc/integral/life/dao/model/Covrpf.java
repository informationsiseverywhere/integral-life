package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Covrpf model for CovprpfDAO.
 *  
 * @author vhukumagrawa
 *
 */
public class Covrpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private Integer planSuffix = new Integer(0);
	private String validflag;
	private Integer tranno;
	private Integer currfrom;
	private Integer currto;
	private String statcode;
	private String pstatcode;
	private String statreasn;
	private Integer crrcd;
	private Integer anbAtCcd;
	private Integer anbccd;
	private String sex;
	private String reptcd01;
	private String reptcd02;
	private String reptcd03;
	private String reptcd04;
	private String reptcd05;
	private String reptcd06;
	private BigDecimal crinst01;
	private BigDecimal crinst02;
	private BigDecimal crinst03;
	private BigDecimal crinst04;
	private BigDecimal crinst05;
	private String prmcur;
	private BigDecimal crInstamt01;
	private BigDecimal crInstamt02;
	private BigDecimal crInstamt03;
	private BigDecimal crInstamt04;
	private BigDecimal crInstamt05;
	private String premCurrency;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private String stfund;
	private String stsect;
	private String stssect;
	private Integer transactionDate;
	private Integer transactionTime;
	private Integer user;
	private String statFund;
	private String statSect;
	private String statSubsect;
	private String crtable;
	private Integer riskCessDate;
	private Integer premCessDate;
	private Integer rcesdte;
	private Integer pcesdte;
	private Integer bcesdte;
	private Integer nxtdte;
	private Integer rcesage;
	private Integer pcesage;
	private Integer bcesage;
	private Integer rcestrm;
	private Integer pcestrm;
	private Integer bcestrm;
	private Integer pcesDte;
	private Integer rcesDte;
	private Integer bcesDte;
	private Integer nxDte;
	private Integer rcesTrm;
	private Integer pcesTrm;
	private Integer bcesTrm;
	private Integer benCessDate;
	private Integer nextActDate;
	private Integer riskCessAge;
	private Integer premCessAge;
	private Integer benCessAge;
	private Integer riskCessTerm;
	private Integer premCessTerm;
	private Integer benCessTerm;
	private BigDecimal sumins;
	private String sicurr;
	private BigDecimal varsi;
	private BigDecimal varSumInsured;
	private String mortcls;
	private String liencd;
	private String ratingClass;
	private String indexationInd;
	private String bonusInd;
	private String deferPerdCode;
	private BigDecimal deferPerdAmt;
	private String deferPerdInd;
	private BigDecimal totMthlyBenefit;
	private BigDecimal estMatValue01;
	private BigDecimal estMatValue02;
	private Integer estMatDate01;
	private Integer estMatDate02;
	private BigDecimal estMatInt01;
	private BigDecimal estMatInt02;
	private String ratcls;
	private String indxin;
	private String bnusin;
	private String dpcd;
	private BigDecimal dpamt;
	private String dpind;
	private BigDecimal tmben;
	private BigDecimal emv01;
	private BigDecimal emv02;
	private Integer emvdte01;
	private Integer emvdte02;
	private BigDecimal emvint01;
	private BigDecimal emvint02;
	private String campaign;
	private BigDecimal statSumins;
	private Integer rtrnyrs;
	private String reserveUnitsInd;
	private Integer reserveUnitsDate;
	private String chargeOptionsInd;
	private String fundSplitPlan;
	private Integer premCessAgeMth;
	private Integer premCessAgeDay;
	private Integer premCessTermMth;
	private Integer premCessTermDay;
	private Integer riskCessAgeMth;
	private Integer riskCessAgeDay;
	private Integer riskCessTermMth;
	private Integer riskCessTermDay;
	private String jlLsInd;
	private BigDecimal stsmin;
	private String rsunin;
	private Integer rundte;
	private String chgopt;
	private String fundsp;
	private Integer pcamth;
	private Integer pcaday;
	private Integer pctmth;
	private Integer pctday;
	private Integer rcamth;
	private Integer rcaday;
	private Integer rctmth;
	private Integer rctday;
	private String jllsid;
	private BigDecimal instprem;
	private BigDecimal singp;
	private Integer rerateDate;
	private Integer rerateFromDate;
	private Integer benBillDate;
	private Integer annivProcDate;
	private Integer convertInitialUnits;
	private Integer reviewProcessing;
	private Integer unitStatementDate;
	private Integer cpiDate;
	private Integer initUnitCancDate;
	private Integer extraAllocDate;
	private Integer initUnitIncrsDate;
	private BigDecimal coverageDebt;
	private Integer rrtdat;
	private Integer rrtfrm;
	private Integer bbldat;
	private Integer cbanpr;
	private Integer cbcvin;
	private Integer cbrvpr;
	private Integer cbunst;
	private Integer cpidte;
	private Integer icandt;
	private Integer exaldt;
	private Integer iincdt;
	private BigDecimal crdebt;
	private Integer payrseqno;
	private String bappmeth;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private String userProfile;
	private String jobName;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private BigDecimal loadper;
	private BigDecimal rateadj;
	private BigDecimal fltmort;
	private BigDecimal premadj;
	private BigDecimal ageadj;
	private BigDecimal zstpduty01;
	private String zclstate;
	private String gmib;
	private String gmdb;
	private String gmwb;
	private BigDecimal riskprem;
	private String lnkgno;
	
	public String getLnkgno() {
		return lnkgno;
	}

	public void setLnkgno(String lnkgno) {
		this.lnkgno = lnkgno;
	}

	public Covrpf() {
		chdrcoy = "";
		chdrnum = "";
		life = "";
		jlife = "";
		coverage = "";
		rider = "";
		validflag = "";
		statcode = "";
		pstatcode = "";
		statreasn = "";
		sex = "";
		reptcd01 = "";
		reptcd02 = "";
		reptcd03 = "";
		reptcd04 = "";
		reptcd05 = "";
		reptcd06 = "";
		crInstamt01 =  BigDecimal.ZERO;
		crInstamt02 = BigDecimal.ZERO;
		crInstamt03 = BigDecimal.ZERO;
		crInstamt04 = BigDecimal.ZERO;
		crInstamt05 = BigDecimal.ZERO;
		premCurrency = "";
		termid = "";
		statFund = "";
		statSect = "";
		statSubsect = "";
		crtable = "";
		sumins = BigDecimal.ZERO;
		sicurr = "";
		varSumInsured = BigDecimal.ZERO;
		mortcls = "";
		liencd = "";
		ratingClass = "";
		indexationInd = "";
		bonusInd = "";
		deferPerdCode = "";
		deferPerdAmt = BigDecimal.ZERO;
		deferPerdInd = "";
		totMthlyBenefit = BigDecimal.ZERO;
		estMatValue01 =  BigDecimal.ZERO;
		estMatValue02 =  BigDecimal.ZERO;
		estMatInt01 =  BigDecimal.ZERO;
		estMatInt02 = BigDecimal.ZERO;
		campaign = "";
		statSumins = BigDecimal.ZERO;
		reserveUnitsInd = "";
		chargeOptionsInd = "";
		fundSplitPlan = "";
		jlLsInd = "";
		instprem = BigDecimal.ZERO;
		singp = BigDecimal.ZERO;
		coverageDebt = BigDecimal.ZERO;
		bappmeth = "";
		zbinstprem = BigDecimal.ZERO;
		zlinstprem = BigDecimal.ZERO;
		userProfile = "";
		jobName = "";
		loadper = BigDecimal.ZERO;
		rateadj = BigDecimal.ZERO; 	
		fltmort = BigDecimal.ZERO;
		premadj= BigDecimal.ZERO;
		ageadj = BigDecimal.ZERO;
		zstpduty01 = BigDecimal.ZERO;
		zclstate = "";
	}

	public Covrpf(Covrpf _covrpf) {
		super();
		this.uniqueNumber = _covrpf.uniqueNumber;
		this.chdrcoy = _covrpf.chdrcoy;
		this.chdrnum = _covrpf.chdrnum;
		this.life = _covrpf.life;
		this.jlife = _covrpf.jlife;
		this.coverage = _covrpf.coverage;
		this.rider = _covrpf.rider;
		this.plnsfx = _covrpf.plnsfx;
		this.validflag = _covrpf.validflag;
		this.tranno = _covrpf.tranno;
		this.currfrom = _covrpf.currfrom;
		this.currto = _covrpf.currto;
		this.statcode = _covrpf.statcode;
		this.pstatcode = _covrpf.pstatcode;
		this.statreasn = _covrpf.statreasn;
		this.crrcd = _covrpf.crrcd;
		this.anbAtCcd = _covrpf.anbAtCcd;
		this.anbccd = _covrpf.anbccd;
		this.sex = _covrpf.sex;
		this.userT = _covrpf.userT;
		this.reptcd01 = _covrpf.reptcd01;
		this.reptcd02 = _covrpf.reptcd02;
		this.reptcd03 = _covrpf.reptcd03;
		this.reptcd04 = _covrpf.reptcd04;
		this.reptcd05 = _covrpf.reptcd05;
		this.reptcd06 = _covrpf.reptcd06;
		this.crinst01 = _covrpf.crinst01;
		this.crinst02 = _covrpf.crinst02;
		this.crinst03 = _covrpf.crinst03;
		this.crinst04 = _covrpf.crinst04;
		this.crinst05 = _covrpf.crinst05;
		this.prmcur = _covrpf.prmcur;
		this.termid = _covrpf.termid;
		this.trdt = _covrpf.trdt;
		this.trtm = _covrpf.trtm;
		this.stfund = _covrpf.stfund;
		this.stsect = _covrpf.stsect;
		this.stssect = _covrpf.stssect;
		this.crtable = _covrpf.crtable;
		this.rcesdte = _covrpf.rcesdte;
		this.pcesdte = _covrpf.pcesdte;
		this.bcesdte = _covrpf.bcesdte;
		this.nxtdte = _covrpf.nxtdte;
		this.rcesage = _covrpf.rcesage;
		this.pcesage = _covrpf.pcesage;
		this.bcesage = _covrpf.bcesage;
		this.rcestrm = _covrpf.rcestrm;
		this.pcestrm = _covrpf.pcestrm;
		this.bcestrm = _covrpf.bcestrm;
		this.sumins = _covrpf.sumins;
		this.sicurr = _covrpf.sicurr;
		this.varsi = _covrpf.varsi;
		this.varSumInsured = _covrpf.varSumInsured;
		this.mortcls = _covrpf.mortcls;
		this.liencd = _covrpf.liencd;
		this.ratcls = _covrpf.ratcls;
		this.indxin = _covrpf.indxin;
		this.bnusin = _covrpf.bnusin;
		this.dpcd = _covrpf.dpcd;
		this.dpamt = _covrpf.dpamt;
		this.dpind = _covrpf.dpind;
		this.tmben = _covrpf.tmben;
		this.emv01 = _covrpf.emv01;
		this.emv02 = _covrpf.emv02;
		this.emvdte01 = _covrpf.emvdte01;
		this.emvdte02 = _covrpf.emvdte02;
		this.emvint01 = _covrpf.emvint01;
		this.emvint02 = _covrpf.emvint02;
		this.campaign = _covrpf.campaign;
		this.stsmin = _covrpf.stsmin;
		this.rtrnyrs = _covrpf.rtrnyrs;
		this.rsunin = _covrpf.rsunin;
		this.rundte = _covrpf.rundte;
		this.chgopt = _covrpf.chgopt;
		this.fundsp = _covrpf.fundsp;
		this.pcamth = _covrpf.pcamth;
		this.pcaday = _covrpf.pcaday;
		this.pctmth = _covrpf.pctmth;
		this.pctday = _covrpf.pctday;
		this.rcamth = _covrpf.rcamth;
		this.rcaday = _covrpf.rcaday;
		this.rctmth = _covrpf.rctmth;
		this.rctday = _covrpf.rctday;
		this.jllsid = _covrpf.jllsid;
		this.instprem = _covrpf.instprem;
		this.singp = _covrpf.singp;
		this.rrtdat = _covrpf.rrtdat;
		this.rrtfrm = _covrpf.rrtfrm;
		this.bbldat = _covrpf.bbldat;
		this.cbanpr = _covrpf.cbanpr;
		this.cbcvin = _covrpf.cbcvin;
		this.cbrvpr = _covrpf.cbrvpr;
		this.cbunst = _covrpf.cbunst;
		this.cpidte = _covrpf.cpidte;
		this.icandt = _covrpf.icandt;
		this.exaldt = _covrpf.exaldt;
		this.iincdt = _covrpf.iincdt;
		this.crdebt = _covrpf.crdebt;
		this.payrseqno = _covrpf.payrseqno;
		this.bappmeth = _covrpf.bappmeth;
		this.zbinstprem = _covrpf.zbinstprem;
		this.zlinstprem = _covrpf.zlinstprem;
		this.usrprf = _covrpf.usrprf;
		this.jobnm = _covrpf.jobnm;
		this.datime = _covrpf.datime;
		this.loadper = _covrpf.loadper;
		this.rateadj = _covrpf.rateadj;
		this.fltmort = _covrpf.fltmort;
		this.premadj = _covrpf.premadj;
		this.ageadj = _covrpf.ageadj;
		this.zstpduty01 = _covrpf.zstpduty01;
		this.zclstate = _covrpf.zclstate;
		this.gmib = _covrpf.gmib;
		this.gmdb = _covrpf.gmdb;
		this.gmwb = _covrpf.gmwb;
		this.riskprem = _covrpf.riskprem;
	}

	public Integer getAnbccd() {
		return anbccd;
	}

	public void setAnbccd(Integer anbccd) {
		this.anbccd = anbccd;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public Integer getPlanSuffix() {
		return planSuffix == null ? new Integer(0) : planSuffix;
	}

	public void setPlanSuffix(Integer planSuffix) {
		this.planSuffix = planSuffix;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public Integer getCurrfrom() {
		return currfrom;
	}

	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}

	public Integer getCurrto() {
		return currto;
	}

	public void setCurrto(Integer currto) {
		this.currto = currto;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstatcode() {
		return pstatcode;
	}

	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	public String getStatreasn() {
		return statreasn;
	}

	public void setStatreasn(String statreasn) {
		this.statreasn = statreasn;
	}

	public Integer getCrrcd() {
		return crrcd;
	}

	public void setCrrcd(Integer crrcd) {
		this.crrcd = crrcd;
	}

	public Integer getAnbAtCcd() {
		return anbAtCcd == null ? 0 : anbAtCcd;
	}

	public void setAnbAtCcd(Integer anbAtCcd) {
		this.anbAtCcd = anbAtCcd;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getReptcd01() {
		return reptcd01;
	}

	public void setReptcd01(String reptcd01) {
		this.reptcd01 = reptcd01;
	}

	public String getReptcd02() {
		return reptcd02;
	}

	public void setReptcd02(String reptcd02) {
		this.reptcd02 = reptcd02;
	}

	public String getReptcd03() {
		return reptcd03;
	}

	public void setReptcd03(String reptcd03) {
		this.reptcd03 = reptcd03;
	}

	public String getReptcd04() {
		return reptcd04;
	}

	public void setReptcd04(String reptcd04) {
		this.reptcd04 = reptcd04;
	}

	public String getReptcd05() {
		return reptcd05;
	}

	public void setReptcd05(String reptcd05) {
		this.reptcd05 = reptcd05;
	}

	public String getReptcd06() {
		return reptcd06;
	}

	public void setReptcd06(String reptcd06) {
		this.reptcd06 = reptcd06;
	}

	public BigDecimal getCrInstamt01() {
		return crInstamt01;
	}

	public void setCrInstamt01(BigDecimal crInstamt01) {
		this.crInstamt01 = crInstamt01;
	}

	public BigDecimal getCrInstamt02() {
		return crInstamt02;
	}

	public void setCrInstamt02(BigDecimal crInstamt02) {
		this.crInstamt02 = crInstamt02;
	}

	public BigDecimal getCrInstamt03() {
		return crInstamt03;
	}

	public void setCrInstamt03(BigDecimal crInstamt03) {
		this.crInstamt03 = crInstamt03;
	}

	public BigDecimal getCrInstamt04() {
		return crInstamt04;
	}

	public void setCrInstamt04(BigDecimal crInstamt04) {
		this.crInstamt04 = crInstamt04;
	}

	public BigDecimal getCrInstamt05() {
		return crInstamt05;
	}

	public void setCrInstamt05(BigDecimal crInstamt05) {
		this.crInstamt05 = crInstamt05;
	}

	public String getPremCurrency() {
		return premCurrency;
	}

	public void setPremCurrency(String premCurrency) {
		this.premCurrency = premCurrency;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public Integer getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Integer transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(Integer transactionTime) {
		this.transactionTime = transactionTime;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}

	public String getStatFund() {
		return statFund;
	}

	public void setStatFund(String statFund) {
		this.statFund = statFund;
	}

	public String getStatSect() {
		return statSect;
	}

	public void setStatSect(String statSect) {
		this.statSect = statSect;
	}

	public String getStatSubsect() {
		return statSubsect;
	}

	public void setStatSubsect(String statSubsect) {
		this.statSubsect = statSubsect;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public Integer getRiskCessDate() {
		return riskCessDate;
	}

	public void setRiskCessDate(Integer riskCessDate) {
		this.riskCessDate = riskCessDate;
	}

	public Integer getPremCessDate() {
		return premCessDate;
	}

	public void setPremCessDate(Integer premCessDate) {
		this.premCessDate = premCessDate;
	}

	public Integer getBenCessDate() {
		return benCessDate;
	}

	public void setBenCessDate(Integer benCessDate) {
		this.benCessDate = benCessDate;
	}

	public Integer getNextActDate() {
		return nextActDate;
	}

	public void setNextActDate(Integer nextActDate) {
		this.nextActDate = nextActDate;
	}

	public Integer getRiskCessAge() {
		return riskCessAge;
	}

	public void setRiskCessAge(Integer riskCessAge) {
		this.riskCessAge = riskCessAge;
	}

	public Integer getPremCessAge() {
		return premCessAge;
	}

	public void setPremCessAge(Integer premCessAge) {
		this.premCessAge = premCessAge;
	}

	public Integer getBenCessAge() {
		return benCessAge;
	}

	public void setBenCessAge(Integer benCessAge) {
		this.benCessAge = benCessAge;
	}

	public Integer getRiskCessTerm() {
		return riskCessTerm;
	}

	public void setRiskCessTerm(Integer riskCessTerm) {
		this.riskCessTerm = riskCessTerm;
	}

	public Integer getPremCessTerm() {
		return premCessTerm;
	}

	public void setPremCessTerm(Integer premCessTerm) {
		this.premCessTerm = premCessTerm;
	}

	public Integer getBenCessTerm() {
		return benCessTerm;
	}

	public void setBenCessTerm(Integer benCessTerm) {
		this.benCessTerm = benCessTerm;
	}

	public BigDecimal getSumins() {
		return sumins;
	}

	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	public String getSicurr() {
		return sicurr;
	}

	public void setSicurr(String sicurr) {
		this.sicurr = sicurr;
	}

	public BigDecimal getVarSumInsured() {
		return varSumInsured;
	}

	public void setVarSumInsured(BigDecimal varSumInsured) {
		this.varSumInsured = varSumInsured;
	}

	public String getMortcls() {
		return mortcls;
	}

	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}

	public String getLiencd() {
		return liencd;
	}

	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}

	public String getRatingClass() {
		return ratingClass;
	}

	public void setRatingClass(String ratingClass) {
		this.ratingClass = ratingClass;
	}

	public String getIndexationInd() {
		return indexationInd;
	}

	public void setIndexationInd(String indexationInd) {
		this.indexationInd = indexationInd;
	}

	public String getBonusInd() {
		return bonusInd;
	}

	public void setBonusInd(String bonusInd) {
		this.bonusInd = bonusInd;
	}

	public String getDeferPerdCode() {
		return deferPerdCode;
	}

	public void setDeferPerdCode(String deferPerdCode) {
		this.deferPerdCode = deferPerdCode;
	}

	public BigDecimal getDeferPerdAmt() {
		return deferPerdAmt;
	}

	public void setDeferPerdAmt(BigDecimal deferPerdAmt) {
		this.deferPerdAmt = deferPerdAmt;
	}

	public String getDeferPerdInd() {
		return deferPerdInd;
	}

	public void setDeferPerdInd(String deferPerdInd) {
		this.deferPerdInd = deferPerdInd;
	}

	public BigDecimal getTotMthlyBenefit() {
		return totMthlyBenefit;
	}

	public void setTotMthlyBenefit(BigDecimal totMthlyBenefit) {
		this.totMthlyBenefit = totMthlyBenefit;
	}

	public BigDecimal getEstMatValue01() {
		return estMatValue01;
	}

	public void setEstMatValue01(BigDecimal estMatValue01) {
		this.estMatValue01 = estMatValue01;
	}

	public BigDecimal getEstMatValue02() {
		return estMatValue02;
	}

	public void setEstMatValue02(BigDecimal estMatValue02) {
		this.estMatValue02 = estMatValue02;
	}

	public Integer getEstMatDate01() {
		return estMatDate01;
	}

	public void setEstMatDate01(Integer estMatDate01) {
		this.estMatDate01 = estMatDate01;
	}

	public Integer getEstMatDate02() {
		return estMatDate02;
	}

	public void setEstMatDate02(Integer estMatDate02) {
		this.estMatDate02 = estMatDate02;
	}

	public BigDecimal getEstMatInt01() {
		return estMatInt01;
	}

	public void setEstMatInt01(BigDecimal estMatInt01) {
		this.estMatInt01 = estMatInt01;
	}

	public BigDecimal getEstMatInt02() {
		return estMatInt02;
	}

	public void setEstMatInt02(BigDecimal estMatInt02) {
		this.estMatInt02 = estMatInt02;
	}

	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public BigDecimal getStatSumins() {
		return statSumins;
	}

	public void setStatSumins(BigDecimal statSumins) {
		this.statSumins = statSumins;
	}

	public Integer getRtrnyrs() {
		return rtrnyrs;
	}

	public void setRtrnyrs(Integer rtrnyrs) {
		this.rtrnyrs = rtrnyrs;
	}

	public String getReserveUnitsInd() {
		return reserveUnitsInd;
	}

	public void setReserveUnitsInd(String reserveUnitsInd) {
		this.reserveUnitsInd = reserveUnitsInd;
	}

	public Integer getReserveUnitsDate() {
		return reserveUnitsDate;
	}

	public void setReserveUnitsDate(Integer reserveUnitsDate) {
		this.reserveUnitsDate = reserveUnitsDate;
	}

	public String getChargeOptionsInd() {
		return chargeOptionsInd;
	}

	public void setChargeOptionsInd(String chargeOptionsInd) {
		this.chargeOptionsInd = chargeOptionsInd;
	}

	public String getFundSplitPlan() {
		return fundSplitPlan;
	}

	public void setFundSplitPlan(String fundSplitPlan) {
		this.fundSplitPlan = fundSplitPlan;
	}

	public Integer getPremCessAgeMth() {
		return premCessAgeMth;
	}

	public void setPremCessAgeMth(Integer premCessAgeMth) {
		this.premCessAgeMth = premCessAgeMth;
	}

	public Integer getPremCessAgeDay() {
		return premCessAgeDay;
	}

	public void setPremCessAgeDay(Integer premCessAgeDay) {
		this.premCessAgeDay = premCessAgeDay;
	}

	public Integer getPremCessTermMth() {
		return premCessTermMth;
	}

	public void setPremCessTermMth(Integer premCessTermMth) {
		this.premCessTermMth = premCessTermMth;
	}

	public Integer getPremCessTermDay() {
		return premCessTermDay;
	}

	public void setPremCessTermDay(Integer premCessTermDay) {
		this.premCessTermDay = premCessTermDay;
	}

	public Integer getRiskCessAgeMth() {
		return riskCessAgeMth;
	}

	public void setRiskCessAgeMth(Integer riskCessAgeMth) {
		this.riskCessAgeMth = riskCessAgeMth;
	}

	public Integer getRiskCessAgeDay() {
		return riskCessAgeDay;
	}

	public void setRiskCessAgeDay(Integer riskCessAgeDay) {
		this.riskCessAgeDay = riskCessAgeDay;
	}

	public Integer getRiskCessTermMth() {
		return riskCessTermMth;
	}

	public void setRiskCessTermMth(Integer riskCessTermMth) {
		this.riskCessTermMth = riskCessTermMth;
	}

	public Integer getRiskCessTermDay() {
		return riskCessTermDay;
	}

	public void setRiskCessTermDay(Integer riskCessTermDay) {
		this.riskCessTermDay = riskCessTermDay;
	}

	public String getJlLsInd() {
		return jlLsInd;
	}

	public void setJlLsInd(String jlLsInd) {
		this.jlLsInd = jlLsInd;
	}

	public BigDecimal getInstprem() {
		return instprem;
	}

	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}

	public BigDecimal getSingp() {
		return singp;
	}

	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}

	public Integer getRerateDate() {
		return rerateDate;
	}

	public void setRerateDate(Integer rerateDate) {
		this.rerateDate = rerateDate;
	}

	public Integer getRerateFromDate() {
		return rerateFromDate;
	}

	public void setRerateFromDate(Integer rerateFromDate) {
		this.rerateFromDate = rerateFromDate;
	}

	public Integer getBenBillDate() {
		return benBillDate;
	}

	public void setBenBillDate(Integer benBillDate) {
		this.benBillDate = benBillDate;
	}

	public Integer getAnnivProcDate() {
		return annivProcDate;
	}

	public void setAnnivProcDate(Integer annivProcDate) {
		this.annivProcDate = annivProcDate;
	}

	public Integer getConvertInitialUnits() {
		return convertInitialUnits;
	}

	public void setConvertInitialUnits(Integer convertInitialUnits) {
		this.convertInitialUnits = convertInitialUnits;
	}

	public Integer getReviewProcessing() {
		return reviewProcessing;
	}

	public void setReviewProcessing(Integer reviewProcessing) {
		this.reviewProcessing = reviewProcessing;
	}

	public Integer getUnitStatementDate() {
		return unitStatementDate;
	}

	public void setUnitStatementDate(Integer unitStatementDate) {
		this.unitStatementDate = unitStatementDate;
	}

	public Integer getCpiDate() {
		return cpiDate;
	}

	public void setCpiDate(Integer cpiDate) {
		this.cpiDate = cpiDate;
	}

	public Integer getInitUnitCancDate() {
		return initUnitCancDate;
	}

	public void setInitUnitCancDate(Integer initUnitCancDate) {
		this.initUnitCancDate = initUnitCancDate;
	}

	public Integer getExtraAllocDate() {
		return extraAllocDate;
	}

	public void setExtraAllocDate(Integer extraAllocDate) {
		this.extraAllocDate = extraAllocDate;
	}

	public Integer getInitUnitIncrsDate() {
		return initUnitIncrsDate;
	}

	public void setInitUnitIncrsDate(Integer initUnitIncrsDate) {
		this.initUnitIncrsDate = initUnitIncrsDate;
	}

	public BigDecimal getCoverageDebt() {
		return coverageDebt;
	}

	public void setCoverageDebt(BigDecimal coverageDebt) {
		this.coverageDebt = coverageDebt;
	}

	public Integer getPayrseqno() {
		return payrseqno;
	}

	public void setPayrseqno(Integer payrseqno) {
		this.payrseqno = payrseqno;
	}

	public String getBappmeth() {
		return bappmeth;
	}

	public void setBappmeth(String bappmeth) {
		this.bappmeth = bappmeth;
	}

	public BigDecimal getZbinstprem() {
		return zbinstprem;
	}

	public void setZbinstprem(BigDecimal zbinstprem) {
		this.zbinstprem = zbinstprem;
	}

	public BigDecimal getZlinstprem() {
		return zlinstprem;
	}

	public void setZlinstprem(BigDecimal zlinstprem) {
		this.zlinstprem = zlinstprem;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}

	public BigDecimal getLoadper() {
		return loadper;
	}

	public void setLoadper(BigDecimal loadper) {
		this.loadper = loadper;
	}

	public BigDecimal getRateadj() {
		return rateadj;
	}

	public void setRateadj(BigDecimal rateadj) {
		this.rateadj = rateadj;
	}

	public BigDecimal getFltmort() {
		return fltmort;
	}

	public void setFltmort(BigDecimal fltmort) {
		this.fltmort = fltmort;
	}

	public BigDecimal getPremadj() {
		return premadj;
	}

	public void setPremadj(BigDecimal premadj) {
		this.premadj = premadj;
	}

	public BigDecimal getAgeadj() {
		return ageadj;
	}

	public void setAgeadj(BigDecimal ageadj) {
		this.ageadj = ageadj;
	}

	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}

	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}

	public String getZclstate() {
		return zclstate;
	}

	public void setZclstate(String zclstate) {
		this.zclstate = zclstate;
	}

	public String getGmib() {
		return gmib;
	}

	public void setGmib(String gmib) {
		this.gmib = gmib;
	}

	public String getGmdb() {
		return gmdb;
	}

	public void setGmdb(String gmdb) {
		this.gmdb = gmdb;
	}

	public String getGmwb() {
		return gmwb;
	}

	public void setGmwb(String gmwb) {
		this.gmwb = gmwb;
	}

	public Integer getPcesDte() {
		return pcesDte;
	}

	public void setPcesDte(Integer pcesDte) {
		this.pcesDte = pcesDte;
	}

	public Integer getRcesDte() {
		return rcesDte;
	}

	public void setRcesDte(Integer rcesDte) {
		this.rcesDte = rcesDte;
	}

	public Integer getBcesDte() {
		return bcesDte;
	}

	public void setBcesDte(Integer bcesDte) {
		this.bcesDte = bcesDte;
	}

	public Integer getNxDte() {
		return nxDte;
	}

	public void setNxDte(Integer nxDte) {
		this.nxDte = nxDte;
	}

	public Integer getRcesTrm() {
		return rcesTrm;
	}

	public void setRcesTrm(Integer rcesTrm) {
		this.rcesTrm = rcesTrm;
	}

	public Integer getPcesTrm() {
		return pcesTrm;
	}

	public void setPcesTrm(Integer pcesTrm) {
		this.pcesTrm = pcesTrm;
	}

	public Integer getBcesTrm() {
		return bcesTrm;
	}

	public void setBcesTrm(Integer bcesTrm) {
		this.bcesTrm = bcesTrm;
	}

	public Integer getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	public BigDecimal getCrinst01() {
		return crinst01;
	}

	public void setCrinst01(BigDecimal crinst01) {
		this.crinst01 = crinst01;
	}

	public BigDecimal getCrinst02() {
		return crinst02;
	}

	public void setCrinst02(BigDecimal crinst02) {
		this.crinst02 = crinst02;
	}

	public BigDecimal getCrinst03() {
		return crinst03;
	}

	public void setCrinst03(BigDecimal crinst03) {
		this.crinst03 = crinst03;
	}

	public BigDecimal getCrinst04() {
		return crinst04;
	}

	public void setCrinst04(BigDecimal crinst04) {
		this.crinst04 = crinst04;
	}

	public BigDecimal getCrinst05() {
		return crinst05;
	}

	public void setCrinst05(BigDecimal crinst05) {
		this.crinst05 = crinst05;
	}

	public String getPrmcur() {
		return prmcur;
	}

	public void setPrmcur(String prmcur) {
		this.prmcur = prmcur;
	}

	public Integer getTrdt() {
		return trdt;
	}

	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}

	public Integer getTrtm() {
		return trtm;
	}

	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	public Integer getUserT() {
		return userT;
	}

	public void setUserT(Integer userT) {
		this.userT = userT;
	}

	public String getStfund() {
		return stfund;
	}

	public void setStfund(String stfund) {
		this.stfund = stfund;
	}

	public String getStsect() {
		return stsect;
	}

	public void setStsect(String stsect) {
		this.stsect = stsect;
	}

	public String getStssect() {
		return stssect;
	}

	public void setStssect(String stssect) {
		this.stssect = stssect;
	}

	public Integer getRcesdte() {
		return rcesdte;
	}

	public void setRcesdte(Integer rcesdte) {
		this.rcesdte = rcesdte;
	}

	public Integer getPcesdte() {
		return pcesdte;
	}

	public void setPcesdte(Integer pcesdte) {
		this.pcesdte = pcesdte;
	}

	public Integer getBcesdte() {
		return bcesdte;
	}

	public void setBcesdte(Integer bcesdte) {
		this.bcesdte = bcesdte;
	}

	public Integer getNxtdte() {
		return nxtdte;
	}

	public void setNxtdte(Integer nxtdte) {
		this.nxtdte = nxtdte;
	}

	public Integer getRcesage() {
		return rcesage;
	}

	public void setRcesage(Integer rcesage) {
		this.rcesage = rcesage;
	}

	public Integer getPcesage() {
		return pcesage;
	}

	public void setPcesage(Integer pcesage) {
		this.pcesage = pcesage;
	}

	public Integer getBcesage() {
		return bcesage;
	}

	public void setBcesage(Integer bcesage) {
		this.bcesage = bcesage;
	}

	public Integer getRcestrm() {
		return rcestrm;
	}

	public void setRcestrm(Integer rcestrm) {
		this.rcestrm = rcestrm;
	}

	public Integer getPcestrm() {
		return pcestrm;
	}

	public void setPcestrm(Integer pcestrm) {
		this.pcestrm = pcestrm;
	}

	public Integer getBcestrm() {
		return bcestrm;
	}

	public void setBcestrm(Integer bcestrm) {
		this.bcestrm = bcestrm;
	}

	public BigDecimal getVarsi() {
		return varsi;
	}

	public void setVarsi(BigDecimal varsi) {
		this.varsi = varsi;
	}

	public String getRatcls() {
		return ratcls;
	}

	public void setRatcls(String ratcls) {
		this.ratcls = ratcls;
	}

	public String getIndxin() {
		return indxin;
	}

	public void setIndxin(String indxin) {
		this.indxin = indxin;
	}

	public String getBnusin() {
		return bnusin;
	}

	public void setBnusin(String bnusin) {
		this.bnusin = bnusin;
	}

	public String getDpcd() {
		return dpcd;
	}

	public void setDpcd(String dpcd) {
		this.dpcd = dpcd;
	}

	public BigDecimal getDpamt() {
		return dpamt;
	}

	public void setDpamt(BigDecimal dpamt) {
		this.dpamt = dpamt;
	}

	public String getDpind() {
		return dpind;
	}

	public void setDpind(String dpind) {
		this.dpind = dpind;
	}

	public BigDecimal getTmben() {
		return tmben;
	}

	public void setTmben(BigDecimal tmben) {
		this.tmben = tmben;
	}

	public BigDecimal getEmv01() {
		return emv01;
	}

	public void setEmv01(BigDecimal emv01) {
		this.emv01 = emv01;
	}

	public BigDecimal getEmv02() {
		return emv02;
	}

	public void setEmv02(BigDecimal emv02) {
		this.emv02 = emv02;
	}

	public Integer getEmvdte01() {
		return emvdte01;
	}

	public void setEmvdte01(Integer emvdte01) {
		this.emvdte01 = emvdte01;
	}

	public Integer getEmvdte02() {
		return emvdte02;
	}

	public void setEmvdte02(Integer emvdte02) {
		this.emvdte02 = emvdte02;
	}

	public BigDecimal getEmvint01() {
		return emvint01;
	}

	public void setEmvint01(BigDecimal emvint01) {
		this.emvint01 = emvint01;
	}

	public BigDecimal getEmvint02() {
		return emvint02;
	}

	public void setEmvint02(BigDecimal emvint02) {
		this.emvint02 = emvint02;
	}

	public BigDecimal getStsmin() {
		return stsmin;
	}

	public void setStsmin(BigDecimal stsmin) {
		this.stsmin = stsmin;
	}

	public String getRsunin() {
		return rsunin;
	}

	public void setRsunin(String rsunin) {
		this.rsunin = rsunin;
	}

	public Integer getRundte() {
		return rundte;
	}

	public void setRundte(Integer rundte) {
		this.rundte = rundte;
	}

	public String getChgopt() {
		return chgopt;
	}

	public void setChgopt(String chgopt) {
		this.chgopt = chgopt;
	}

	public String getFundsp() {
		return fundsp;
	}

	public void setFundsp(String fundsp) {
		this.fundsp = fundsp;
	}

	public Integer getPcamth() {
		return pcamth;
	}

	public void setPcamth(Integer pcamth) {
		this.pcamth = pcamth;
	}

	public Integer getPcaday() {
		return pcaday;
	}

	public void setPcaday(Integer pcaday) {
		this.pcaday = pcaday;
	}

	public Integer getPctmth() {
		return pctmth;
	}

	public void setPctmth(Integer pctmth) {
		this.pctmth = pctmth;
	}

	public Integer getPctday() {
		return pctday;
	}

	public void setPctday(Integer pctday) {
		this.pctday = pctday;
	}

	public Integer getRcamth() {
		return rcamth;
	}

	public void setRcamth(Integer rcamth) {
		this.rcamth = rcamth;
	}

	public Integer getRcaday() {
		return rcaday;
	}

	public void setRcaday(Integer rcaday) {
		this.rcaday = rcaday;
	}

	public Integer getRctmth() {
		return rctmth;
	}

	public void setRctmth(Integer rctmth) {
		this.rctmth = rctmth;
	}

	public Integer getRctday() {
		return rctday;
	}

	public void setRctday(Integer rctday) {
		this.rctday = rctday;
	}

	public String getJllsid() {
		return jllsid;
	}

	public void setJllsid(String jllsid) {
		this.jllsid = jllsid;
	}

	public Integer getRrtdat() {
		return rrtdat;
	}

	public void setRrtdat(Integer rrtdat) {
		this.rrtdat = rrtdat;
	}

	public Integer getRrtfrm() {
		return rrtfrm;
	}

	public void setRrtfrm(Integer rrtfrm) {
		this.rrtfrm = rrtfrm;
	}

	public Integer getBbldat() {
		return bbldat;
	}

	public void setBbldat(Integer bbldat) {
		this.bbldat = bbldat;
	}

	public Integer getCbanpr() {
		return cbanpr;
	}

	public void setCbanpr(Integer cbanpr) {
		this.cbanpr = cbanpr;
	}

	public Integer getCbcvin() {
		return cbcvin;
	}

	public void setCbcvin(Integer cbcvin) {
		this.cbcvin = cbcvin;
	}

	public Integer getCbrvpr() {
		return cbrvpr;
	}

	public void setCbrvpr(Integer cbrvpr) {
		this.cbrvpr = cbrvpr;
	}

	public Integer getCbunst() {
		return cbunst;
	}

	public void setCbunst(Integer cbunst) {
		this.cbunst = cbunst;
	}

	public Integer getCpidte() {
		return cpidte;
	}

	public void setCpidte(Integer cpidte) {
		this.cpidte = cpidte;
	}

	public Integer getIcandt() {
		return icandt;
	}

	public void setIcandt(Integer icandt) {
		this.icandt = icandt;
	}

	public Integer getExaldt() {
		return exaldt;
	}

	public void setExaldt(Integer exaldt) {
		this.exaldt = exaldt;
	}

	public Integer getIincdt() {
		return iincdt;
	}

	public void setIincdt(Integer iincdt) {
		this.iincdt = iincdt;
	}

	public BigDecimal getCrdebt() {
		return crdebt;
	}

	public void setCrdebt(BigDecimal crdebt) {
		this.crdebt = crdebt;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public BigDecimal getRiskprem() {
		return riskprem;
	}

	public void setRiskprem(BigDecimal riskprem) {
		this.riskprem = riskprem;
	}

}
