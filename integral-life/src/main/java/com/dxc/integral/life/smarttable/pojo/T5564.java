package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T5564 {
	private List<String> freqs;
	private List<String> itmkeys;
	
	public List<String> getFreqs() {
		return freqs;
	}
	public void setFreqs(List<String> freqs) {
		this.freqs = freqs;
	}
	public List<String> getItmkeys() {
		return itmkeys;
	}
	public void setItmkeys(List<String> itmkeys) {
		this.itmkeys = itmkeys;
	}

}
