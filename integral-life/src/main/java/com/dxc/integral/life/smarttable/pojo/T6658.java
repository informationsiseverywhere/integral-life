package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class T6658 {
	private String addexist;
	private String addnew;
	private BigDecimal agemax;
	private String billfreq;
	private String comind;
	private String compoundInd;
	private BigDecimal fixdtrm;
	private String manopt;	
	private BigDecimal maxpcnt;
	private BigDecimal maxRefusals;
	private BigDecimal minctrm;
	private BigDecimal minpcnt;
	private String nocommind;
	private String nostatin;
	private String optind;
	private BigDecimal refusalPeriod;
	private String simpleInd;
	private String statind;
	private String subprog;
	private String premsubr;
	private String trevsub;
	public String getAddexist() {
		return addexist;
	}
	public void setAddexist(String addexist) {
		this.addexist = addexist;
	}
	public String getAddnew() {
		return addnew;
	}
	public void setAddnew(String addnew) {
		this.addnew = addnew;
	}
	public BigDecimal getAgemax() {
		return agemax;
	}
	public void setAgemax(BigDecimal agemax) {
		this.agemax = agemax;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getComind() {
		return comind;
	}
	public void setComind(String comind) {
		this.comind = comind;
	}
	public String getCompoundInd() {
		return compoundInd;
	}
	public void setCompoundInd(String compoundInd) {
		this.compoundInd = compoundInd;
	}
	public BigDecimal getFixdtrm() {
		return fixdtrm;
	}
	public void setFixdtrm(BigDecimal fixdtrm) {
		this.fixdtrm = fixdtrm;
	}
	public String getManopt() {
		return manopt;
	}
	public void setManopt(String manopt) {
		this.manopt = manopt;
	}
	public BigDecimal getMaxpcnt() {
		return maxpcnt;
	}
	public void setMaxpcnt(BigDecimal maxpcnt) {
		this.maxpcnt = maxpcnt;
	}
	public BigDecimal getMaxRefusals() {
		return maxRefusals;
	}
	public void setMaxRefusals(BigDecimal maxRefusals) {
		this.maxRefusals = maxRefusals;
	}
	public BigDecimal getMinctrm() {
		return minctrm;
	}
	public void setMinctrm(BigDecimal minctrm) {
		this.minctrm = minctrm;
	}
	public BigDecimal getMinpcnt() {
		return minpcnt;
	}
	public void setMinpcnt(BigDecimal minpcnt) {
		this.minpcnt = minpcnt;
	}
	public String getNocommind() {
		return nocommind;
	}
	public void setNocommind(String nocommind) {
		this.nocommind = nocommind;
	}
	public String getNostatin() {
		return nostatin;
	}
	public void setNostatin(String nostatin) {
		this.nostatin = nostatin;
	}
	public String getOptind() {
		return optind;
	}
	public void setOptind(String optind) {
		this.optind = optind;
	}
	public BigDecimal getRefusalPeriod() {
		return refusalPeriod;
	}
	public void setRefusalPeriod(BigDecimal refusalPeriod) {
		this.refusalPeriod = refusalPeriod;
	}
	public String getSimpleInd() {
		return simpleInd;
	}
	public void setSimpleInd(String simpleInd) {
		this.simpleInd = simpleInd;
	}
	public String getStatind() {
		return statind;
	}
	public void setStatind(String statind) {
		this.statind = statind;
	}
	public String getSubprog() {
		return subprog;
	}
	public void setSubprog(String subprog) {
		this.subprog = subprog;
	}
	public String getPremsubr() {
		return premsubr;
	}
	public void setPremsubr(String premsubr) {
		this.premsubr = premsubr;
	}
	public String getTrevsub() {
		return trevsub;
	}
	public void setTrevsub(String trevsub) {
		this.trevsub = trevsub;
	}
}
