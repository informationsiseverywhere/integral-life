package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Utrnpf;

public interface UtrnpfDAO {

	public int insertUtrnpfRecord(Utrnpf utrnpf);
	public int deleteUtrnpfRecord(Utrnpf utrnpf);
	public List<Utrnpf> getUtrnpfRecords(String chdrcoy, String chdrnum, int tranno, int plnsfx, String coverage, String rider, String life);
}
