package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Agcmpf;

public interface AgcmpfDAO {
	public List<Agcmpf> getAgcmpfRecord(String coy, String chdrnum);

	public List<Agcmpf> getAgcmrvsRecord(String coy, String chdrnum, int tranno);

	public List<Agcmpf> getAgcmpfRecordByAgntnum(Agcmpf agcmpf);

	public List<Agcmpf> searchAgcmstRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx, String validflag);

	public List<Agcmpf> getAgcmpfRecord(Agcmpf agcmpf);

	public void updateAcagpfRecords(List<Agcmpf> agcmvpfList);

	int updateAcagpfValidflag(Agcmpf agcmpf);

	void insertAgcmpfRecords(List<Agcmpf> agcmpfList);

	public int updateAgcmpfRecord(Agcmpf agcmpf);

	public int deleteAgcmpfRecord(Agcmpf agcmpf);
}
