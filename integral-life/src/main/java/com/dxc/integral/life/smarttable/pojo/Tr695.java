package com.dxc.integral.life.smarttable.pojo;

public class Tr695 {

	private String tr695Rec;
	private String basicCommMeth;
	private String bascpy;
	private String basscmth;
	private String basscpy;
	private String bastcmth;
	private String bastcpy;
	private String rnwcpy;
	private String srvcpy;
	private String filler;

	public String getTr695Rec() {
		return tr695Rec;
	}

	public void setTr695Rec(String tr695Rec) {
		this.tr695Rec = tr695Rec;
	}

	public String getBasicCommMeth() {
		return basicCommMeth;
	}

	public void setBasicCommMeth(String basicCommMeth) {
		this.basicCommMeth = basicCommMeth;
	}

	public String getBascpy() {
		return bascpy;
	}

	public void setBascpy(String bascpy) {
		this.bascpy = bascpy;
	}

	public String getBasscmth() {
		return basscmth;
	}

	public void setBasscmth(String basscmth) {
		this.basscmth = basscmth;
	}

	public String getBasscpy() {
		return basscpy;
	}

	public void setBasscpy(String basscpy) {
		this.basscpy = basscpy;
	}

	public String getBastcmth() {
		return bastcmth;
	}

	public void setBastcmth(String bastcmth) {
		this.bastcmth = bastcmth;
	}

	public String getBastcpy() {
		return bastcpy;
	}

	public void setBastcpy(String bastcpy) {
		this.bastcpy = bastcpy;
	}

	public String getRnwcpy() {
		return rnwcpy;
	}

	public void setRnwcpy(String rnwcpy) {
		this.rnwcpy = rnwcpy;
	}

	public String getSrvcpy() {
		return srvcpy;
	}

	public void setSrvcpy(String srvcpy) {
		this.srvcpy = srvcpy;
	}

	public String getFiller() {
		return filler;
	}

	public void setFiller(String filler) {
		this.filler = filler;
	}
}