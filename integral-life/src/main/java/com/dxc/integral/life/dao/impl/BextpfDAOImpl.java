package com.dxc.integral.life.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.BextpfDAO;
import com.dxc.integral.life.dao.model.Bextpf;
/**
 * @author wli31
 */
@Repository("bextpfDAO")
@Lazy
public class BextpfDAOImpl  extends BaseDAOImpl implements BextpfDAO {

	@Override
	public int insertBextPF(Bextpf bextpf) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO BEXTPF(CHDRPFX, CHDRCOY, CHDRNUM, SERVUNIT, CNTTYPE, CNTCURR, OCCDATE, CCDATE, PTDATE, BTDATE, BILLDATE, BILLCHNL, BANKCODE, INSTFROM, INSTTO, INSTBCHNL, INSTCCHNL, INSTFREQ, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTAMT07, INSTAMT08, INSTAMT09, INSTAMT10, INSTAMT11, INSTAMT12, INSTAMT13, INSTAMT14, INSTAMT15, INSTJCTL, GRUPKEY, MEMBSEL, FACTHOUS, BANKKEY, BANKACCKEY, COWNPFX, COWNCOY, COWNNUM, PAYRPFX, PAYRCOY, PAYRNUM, CNTBRANCH, AGNTPFX, AGNTCOY, AGNTNUM, PAYFLAG, BILFLAG, OUTFLAG, SUPFLAG, MANDREF, BILLCD, SACSCODE, SACSTYP, GLMAP, MANDSTAT, DDDEREF, EFFDATEX, DDRSNCDE, CANFLAG, COMPANY, JOBNO, USRPRF, JOBNM, DATIME, NEXTDATE) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		return jdbcTemplate.update(sb.toString(), 
				new Object[] {bextpf.getChdrpfx(),
				bextpf.getChdrcoy(),
				bextpf.getChdrnum(),
				bextpf.getServunit(),
				bextpf.getCnttype(),
				bextpf.getCntcurr(),
				bextpf.getOccdate(),
				bextpf.getCcdate(),
				bextpf.getPtdate(),
				bextpf.getBtdate(),
				bextpf.getBilldate(),
				bextpf.getBillchnl(),
				bextpf.getBankcode(),
				bextpf.getInstfrom(),
				bextpf.getInstto(),
				bextpf.getInstbchnl(),
				bextpf.getInstcchnl(),
				bextpf.getInstfreq(),
				bextpf.getInstamt01(),
				bextpf.getInstamt02(),
				bextpf.getInstamt03(),
				bextpf.getInstamt04(),
				bextpf.getInstamt05(),
				bextpf.getInstamt06(),
				bextpf.getInstamt07(),
				bextpf.getInstamt08(),
				bextpf.getInstamt09(),
				bextpf.getInstamt10(),
				bextpf.getInstamt11(),
				bextpf.getInstamt12(),
				bextpf.getInstamt13(),
				bextpf.getInstamt14(),
				bextpf.getInstamt15(),
				bextpf.getInstjctl(),
				bextpf.getGrupkey(),
				bextpf.getMembsel(),
				bextpf.getFacthous(),
				bextpf.getBankkey(),
				bextpf.getBankacckey(),
				bextpf.getCownpfx(),
				bextpf.getCowncoy(),
				bextpf.getCownnum(),
				bextpf.getPayrpfx(),
				bextpf.getPayrcoy(),
				bextpf.getPayrnum(),
				bextpf.getCntbranch(),
				bextpf.getAgntpfx(),
				bextpf.getAgntcoy(),
				bextpf.getAgntnum(),
				bextpf.getPayflag(),
				bextpf.getBilflag(),
				bextpf.getOutflag(),
				bextpf.getSupflag(),
				bextpf.getMandref(),
				bextpf.getBillcd(),
				bextpf.getSacscode(),
				bextpf.getSacstyp(),
				bextpf.getGlmap(),
				bextpf.getMandstat(),
				bextpf.getDdderef(),
				bextpf.getEffdatex(),
				bextpf.getDdrsncde(),
				bextpf.getCanflag(),
				bextpf.getCompany(),
				bextpf.getJobno(),
			    bextpf.getUserProfile(),
			    bextpf.getJobName(),
			    new Timestamp(System.currentTimeMillis()),						
				bextpf.getNextdate()});
		
	}
	@Override
	public int deleteBextpf(String chdrcoy, String chdrnum, int btdate) {
		String sql = "DELETE FROM VM1DTA.BEXTPF WHERE CHDRCOY=? AND CHDRNUM=? AND BTDATE>=?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, btdate});
	}
	@Override
	public void insertBextPFList(List<Bextpf> bextpfList) {
		if(!bextpfList.isEmpty()){
			for(Bextpf bextpf : bextpfList){
				this.insertBextPF(bextpf);
			}
		}
	} 

}
