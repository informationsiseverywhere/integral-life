package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Pcddpf {
	private long uniqueNumber;
	private Character chdrcoy;
	private String chdrnum;
	private String agntNum;
	private BigDecimal splitC;
	private BigDecimal splitB;
	private Character validFlag;
	private Integer tranNo;
	private Integer currFrom;
	private Integer currTo;
	private Integer userT;
	private String termId;
	private Integer trtM;
	private Integer trtD;
	private String usrprf;
	private String jobNm;
	private Date datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public Character getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Character chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public BigDecimal getSplitC() {
		return splitC;
	}
	public void setSplitC(BigDecimal splitC) {
		this.splitC = splitC;
	}
	public BigDecimal getSplitB() {
		return splitB;
	}
	public void setSplitB(BigDecimal splitB) {
		this.splitB = splitB;
	}
	public Character getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(Character validFlag) {
		this.validFlag = validFlag;
	}
	public Integer getTranNo() {
		return tranNo;
	}
	public void setTranNo(Integer tranNo) {
		this.tranNo = tranNo;
	}
	public Integer getCurrFrom() {
		return currFrom;
	}
	public void setCurrFrom(Integer currFrom) {
		this.currFrom = currFrom;
	}
	public Integer getCurrTo() {
		return currTo;
	}
	public void setCurrTo(Integer currTo) {
		this.currTo = currTo;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public Integer getTrtM() {
		return trtM;
	}
	public void setTrtM(Integer trtM) {
		this.trtM = trtM;
	}
	public Integer getTrtD() {
		return trtD;
	}
	public void setTrtD(Integer trtD) {
		this.trtD = trtD;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobNm() {
		return jobNm;
	}
	public void setJobNm(String jobNm) {
		this.jobNm = jobNm;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}
}