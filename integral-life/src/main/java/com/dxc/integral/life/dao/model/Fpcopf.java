/******************************************************************************
 * File Name 		: Fpcopf.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The Model Class for FPCOPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Date;

public class Fpcopf{

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validflag;
	private Integer currfrom;
	private Integer currto;
	private BigDecimal prmper;
	private BigDecimal prmrcdp;
	private Integer tranno;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String actind;
	private BigDecimal billedp;
	private BigDecimal ovrminreq;
	private Integer minovrpro;
	private String anproind;
	private Integer targfrom;
	private Integer targto;
	private Integer effdate;
	private Integer cbanpr;
	
	public Fpcopf(){
		this.uniqueNumber = 0;
		this.chdrcoy = "";
		this.chdrnum = "";
		this.life = "";
		this.jlife = "";
		this.coverage = "";
		this.rider = "";
		this.plnsfx = 0;
		this.validflag = "";
		this.currfrom = 0;
		this.currto = 0;
		this.prmper = BigDecimal.ZERO;
		this.prmrcdp = BigDecimal.ZERO;
		this.tranno = 0;
		this.usrprf = "";
		this.jobnm = "";
		this.actind = "";
		this.billedp = BigDecimal.ZERO;
		this.ovrminreq  = BigDecimal.ZERO;
		this.minovrpro = 0;
		this.anproind = "";
		this.targfrom = 0;
		this.targto = 0;
		this.effdate = 0;
		this.cbanpr = 0;
	}

	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public Integer getCurrfrom(){
		return this.currfrom;
	}
	public Integer getCurrto(){
		return this.currto;
	}
	public BigDecimal getPrmper(){
		return this.prmper;
	}
	public BigDecimal getPrmrcdp(){
		return this.prmrcdp;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());
	}
	public String getActind(){
		return this.actind;
	}
	public BigDecimal getBilledp(){
		return this.billedp;
	}
	public BigDecimal getOvrminreq(){
		return this.ovrminreq;
	}
	public Integer getMinovrpro(){
		return this.minovrpro;
	}
	public String getAnproind(){
		return this.anproind;
	}
	public Integer getTargfrom(){
		return this.targfrom;
	}
	public Integer getTargto(){
		return this.targto;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public Integer getCbanpr(){
		return this.cbanpr;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setCurrfrom( Integer currfrom ){
		 this.currfrom = currfrom;
	}
	public void setCurrto( Integer currto ){
		 this.currto = currto;
	}
	public void setPrmper( BigDecimal prmper ){
		 this.prmper = prmper;
	}
	public void setPrmrcdp( BigDecimal prmrcdp ){
		 this.prmrcdp = prmrcdp;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());
	}
	public void setActind( String actind ){
		 this.actind = actind;
	}
	public void setBilledp( BigDecimal billedp ){
		 this.billedp = billedp;
	}
	public void setOvrminreq( BigDecimal ovrminreq ){
		 this.ovrminreq = ovrminreq;
	}
	public void setMinovrpro( Integer minovrpro ){
		 this.minovrpro = minovrpro;
	}
	public void setAnproind( String anproind ){
		 this.anproind = anproind;
	}
	public void setTargfrom( Integer targfrom ){
		 this.targfrom = targfrom;
	}
	public void setTargto( Integer targto ){
		 this.targto = targto;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setCbanpr( Integer cbanpr ){
		 this.cbanpr = cbanpr;
	}
}
