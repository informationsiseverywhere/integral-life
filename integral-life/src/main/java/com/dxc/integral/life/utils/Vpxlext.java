package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VpxlextDTO;

public interface Vpxlext {
	public VpxlextDTO processVpxlext(String function,PremiumDTO premiumDTO);
}
