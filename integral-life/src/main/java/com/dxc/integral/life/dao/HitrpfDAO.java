package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Hitrpf;

public interface HitrpfDAO {

	public int insertHitrpfRecord(Hitrpf hitrpf);
	public List<Hitrpf> getHitrpfRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, int tranno);
	public int deleteHitrpfRecord(Hitrpf hitrpf);
}
