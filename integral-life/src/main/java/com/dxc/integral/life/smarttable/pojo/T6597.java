package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T6597 {

	private List<String> cpstats;
	private List<String> crstats;
	private List<Integer> durmnths;
	private List<String> premsubrs;
	
	public List<String> getCpstats() {
		return cpstats;
	}
	public void setCpstats(List<String> cpstats) {
		this.cpstats = cpstats;
	}
	public List<String> getCrstats() {
		return crstats;
	}
	public void setCrstats(List<String> crstats) {
		this.crstats = crstats;
	}
	public List<Integer> getDurmnths() {
		return durmnths;
	}
	public void setDurmnths(List<Integer> durmnths) {
		this.durmnths = durmnths;
	}
	public List<String> getPremsubrs() {
		return premsubrs;
	}
	public void setPremsubrs(List<String> premsubrs) {
		this.premsubrs = premsubrs;
	}
		
}
