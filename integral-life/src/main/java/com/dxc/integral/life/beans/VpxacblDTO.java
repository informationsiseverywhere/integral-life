package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class VpxacblDTO {
  	private String function;
  	private String statuz;
  	private String copybook;
  	private String dataRec;
  	private BigDecimal totRegPrem;
  	private BigDecimal totSingPrem;
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCopybook() {
		return copybook;
	}
	public void setCopybook(String copybook) {
		this.copybook = copybook;
	}
	public String getDataRec() {
		return dataRec;
	}
	public void setDataRec(String dataRec) {
		this.dataRec = dataRec;
	}
	public BigDecimal getTotRegPrem() {
		return totRegPrem;
	}
	public void setTotRegPrem(BigDecimal totRegPrem) {
		this.totRegPrem = totRegPrem;
	}
	public BigDecimal getTotSingPrem() {
		return totSingPrem;
	}
	public void setTotSingPrem(BigDecimal totSingPrem) {
		this.totSingPrem = totSingPrem;
	}

}
