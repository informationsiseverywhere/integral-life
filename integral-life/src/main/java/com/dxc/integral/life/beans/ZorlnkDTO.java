package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class ZorlnkDTO {

	private String function;
	private String statuz;
	private String agent;
	private String chdrcoy;
	private String chdrnum;
	private String crtable;
	private BigDecimal annprem;
	private int effdate;
	private int ptdate;
	private String origcurr;
	private BigDecimal crate;
	private BigDecimal origamt;
	private int tranno;
	private String trandesc;
	private String genlcur;
	private String sacstyp;
	private String termid;
	private String cnttype;
	private String batchKey;
	private String tranref;
	private String clawback;
	
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public BigDecimal getAnnprem() {
		return annprem;
	}
	public void setAnnprem(BigDecimal annprem) {
		this.annprem = annprem;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public BigDecimal getCrate() {
		return crate;
	}
	public void setCrate(BigDecimal crate) {
		this.crate = crate;
	}
	public BigDecimal getOrigamt() {
		return origamt;
	}
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getBatchKey() {
		return batchKey;
	}
	public void setBatchKey(String batchKey) {
		this.batchKey = batchKey;
	}
	public String getTranref() {
		return tranref;
	}
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}
	public String getClawback() {
		return clawback;
	}
	public void setClawback(String clawback) {
		this.clawback = clawback;
	}
	
	
}
