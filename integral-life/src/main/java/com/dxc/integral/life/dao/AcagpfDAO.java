package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Acagpf;
/**
 * @author wli31
 */
public interface AcagpfDAO {
    public int insertAcagpf(Acagpf acagpf);
    public void insertAcagpfRecords(List<Acagpf> acagpfList);
}