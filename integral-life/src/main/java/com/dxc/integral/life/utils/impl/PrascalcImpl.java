package com.dxc.integral.life.utils.impl;


import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.SoinpfDAO;
import com.dxc.integral.fsu.dao.model.Soinpf;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.life.beans.PrasDTO;
import com.dxc.integral.life.smarttable.pojo.T6697;
import com.dxc.integral.life.smarttable.pojo.T6698;
import com.dxc.integral.life.utils.Prascalc;

/**
 * 
 * @author xma3
 *
 */
@Service("prascalc")
@Lazy
public class PrascalcImpl implements Prascalc {

	@Autowired
	private ItempfService itempfService;
	@Autowired
	private SoinpfDAO soinpfDAO;


	@Override
	public PrasDTO processPrascalc(PrasDTO prasDTO) throws IOException, ParseException {
		Soinpf soinpf = new Soinpf();
		soinpf.setStatuz("O-K");
		if (prasDTO.getIncomeSeqNo()==0) {
			prasDTO.setTaxrelamt(BigDecimal.ZERO);
			prasDTO.setInrevnum("");
			return prasDTO;
		}
		soinpf = soinpfDAO.getSoinpfRecord(prasDTO.getClntcoy(), prasDTO.getClntnum(), prasDTO.getIncomeSeqNo());		
		if (soinpf == null) {
			return null;
		}
		
		T6697 t6697IO = null;
		TableItem<T6697> t6697 = itempfService.readSmartTableByTableNameAndItem(prasDTO.getCompany(), "T6697", prasDTO.getCnttype(), CommonConstants.IT_ITEMPFX, T6697.class);
		
		if(t6697 != null){
			t6697IO = t6697.getItemDetail();
		}else {
			return null;
		}
		
		T6698 t6698IO = null;
		
		if (soinpf.getPrasind().trim().equals("N") && t6697IO.getEmpcon().trim().equals("N")) {
			TableItem<T6698> t6698= itempfService.readItemByItemFrm(prasDTO.getCompany(), "T6698", prasDTO.getTaxrelmth(), CommonConstants.IT_ITEMPFX, prasDTO.getEffdate(), T6698.class);

	        if(t6698 == null) {
	        	return null;
	        }else {
	        	t6698IO = t6698.getItemDetail();
	        }
	        
	        prasDTO.setGrossprem(prasDTO.getGrossprem().multiply(t6698IO.getTaxrelpc()).divide(new BigDecimal(100), 2));
	        prasDTO.setInrevnum(t6698IO.getInrevnum());
					
		}else {
			 prasDTO.setGrossprem(BigDecimal.ZERO);
		     prasDTO.setInrevnum("");
		}

		
		return prasDTO;
	}

}
