package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RacdpfDAO;
import com.dxc.integral.life.dao.model.Racdpf;
/* 
 * RacdpfDAO implementation for database table <b>Racdpf</b> DB operations.
 * 
 * @author wli31
 *
 */
@Repository("racdpfDAO")
@Lazy
public class RacdpfDAOImpl extends BaseDAOImpl implements RacdpfDAO {

	@Override
	public List<Racdpf> searchRacdstRecord(String chdrcoy, String chdrnum,String life, String coverage, String rider, int plnsfx,String validflag) {
		String sql = "select * from racdpf where chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and plnsfx=? and validflag =?"
				+ " order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc, plnsfx asc, rasnum asc, tranno desc,  unique_number desc";
		return jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum,life, coverage, rider, plnsfx,validflag},
					new BeanPropertyRowMapper<Racdpf>(Racdpf.class));
	}
	
	@Override
	public Racdpf updateRacd(Racdpf racdpf) {
		String sqlSel = "select * from VM1DTA.RACDPF where chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and plnsfx=? and seqno=? and lrkcls=?";
		String sql = "update vm1dta.racdpf set validflag=?, currto=? where UNIQUE_NUMBER=?";
		
		Racdpf racdpfSel = jdbcTemplate.queryForObject(sqlSel, new Object[] {racdpf.getChdrcoy(), racdpf.getChdrnum(), racdpf.getLife(), racdpf.getCoverage(), 
				racdpf.getRider(), racdpf.getPlnsfx(), racdpf.getSeqno(), racdpf.getLrkcls() }, new BeanPropertyRowMapper<Racdpf>(Racdpf.class));
		if (null != racdpfSel) {
			jdbcTemplate.update(sql, new Object[] { racdpf.getValidflag(), racdpf.getCurrto(), racdpfSel.getUniqueNumber() });
		}
		return racdpfSel;
	}

	@Override
	public void insertRacd(Racdpf racdpf) {
		String sql = "INSERT INTO VM1DTA.RACDPF (CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,TRANNO,SEQNO,VALIDFLAG,CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,"
				+ "REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,LRKCLS,USRPRF,JOBNM,DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(sql, new Object[] { racdpf.getChdrcoy(), racdpf.getChdrnum(), racdpf.getLife(),
				racdpf.getCoverage(), racdpf.getRider(), racdpf.getPlnsfx(), racdpf.getRasnum(), racdpf.getTranno(),
				racdpf.getSeqno(), racdpf.getValidflag(), racdpf.getCurrcode(), racdpf.getCurrfrom(),
				racdpf.getCurrto(), racdpf.getRetype(), racdpf.getRngmnt(), racdpf.getRaamount(), racdpf.getCtdate(),
				racdpf.getCmdate(), racdpf.getReasper(), racdpf.getRrevdt(), racdpf.getRecovamt(), racdpf.getCestype(),
				racdpf.getOvrdind(), racdpf.getLrkcls(), racdpf.getUsrprf(), racdpf.getJobnm(), racdpf.getDatime() });
		
	}
	
	@Override
	public List<Racdpf> searchRacdmjaRecord(String chdrcoy, String chdrnum,String life, String coverage, String rider, int plnsfx, int seqno) {
		String sql = "select * from racdpf where chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and plnsfx=? and seqno =? and validflag =?"
				+ " order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc, plnsfx asc, seqno desc,  unique_number desc";
		return jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum,life, coverage, rider, plnsfx, seqno, "1"},
					new BeanPropertyRowMapper<Racdpf>(Racdpf.class));
	}
	
	@Override
	public Racdpf searchRacdrcoRecord(String chdrcoy, String chdrnum,String life, String coverage, String rider, int plnsfx, int seqno) {
		String sql = "select * from racdpf where chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and plnsfx=? and seqno =? "
				+ " order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc, plnsfx asc, seqno desc,  unique_number desc";
		return jdbcTemplate.queryForObject(sql, new Object[] { chdrcoy, chdrnum,life, coverage, rider, plnsfx, seqno},
					new BeanPropertyRowMapper<Racdpf>(Racdpf.class));
	}
	
	@Override
	public Racdpf searchRacdrcoRecord(String chdrcoy, String chdrnum,String life, String coverage, String rider, int plnsfx, int seqno, int currfrom) {
		String sql = "select * from racdpf where chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and plnsfx=? and seqno=? and currfrom=?"
				+ " order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc, plnsfx asc, seqno desc,  unique_number desc";
		return jdbcTemplate.queryForObject(sql, new Object[] { chdrcoy, chdrnum,life, coverage, rider, plnsfx, seqno},
					new BeanPropertyRowMapper<Racdpf>(Racdpf.class));
	}
}
