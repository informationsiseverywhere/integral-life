package com.dxc.integral.life.dao.impl;

import java.sql.Timestamp;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.UndrpfDAO;
import com.dxc.integral.life.dao.model.Undrpf;
/**
 * 
 * UndrpfDAO implementation for database table <b>UNDRPF</b> DB operations.
 * 
 * @author wli31
 *
 */
@Repository("undrpfDAO")
@Lazy
public class UndrpfDAOImpl extends BaseDAOImpl implements UndrpfDAO {
	@Override
	public int insertUndrpfRecord(Undrpf undrpf) { 
		StringBuilder sb = new StringBuilder();
		sb.append("insert into undrpf(clntnum,coy,lrkcls01,lrkcls02,chdrnum,life,cnttyp,crtable,currcode,sumins,effdate,adsc,usrprf,jobnm,datime)")
		.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		 return jdbcTemplate.update(sb.toString(),new Object[] {undrpf.getClntnum(),undrpf.getCoy(), undrpf.getLrkcls01(),undrpf.getLrkcls02(),undrpf.getChdrnum(),			    
					undrpf.getLife(),undrpf.getCnttyp(),undrpf.getCrtable(),undrpf.getCurrcode(), undrpf.getSumins(),undrpf.getEffdate(),undrpf.getAdsc(),undrpf.getUsrprf(),
				    undrpf.getJobnm(),new Timestamp(System.currentTimeMillis())});
	}
	public int deleteUndrpfRecord(String chdrnum, String chdrcoy){
		String sql = "delete undrpf where coy=? and chdrnum=?";
		return jdbcTemplate.update(sql, new Object[] { chdrcoy,chdrnum});
	}
	public int deleteUndrpfRecord(Undrpf undrpf){
		String sql = "delete undrpf where coy=? and chdrnum=? and clntnum=? and currcode=? and Crtable=? and lrkcls01=?";
		return jdbcTemplate.update(sql, new Object[] { undrpf.getCoy(),undrpf.getChdrnum(),undrpf.getClntnum(),undrpf.getCurrcode(),undrpf.getCrtable(),undrpf.getLrkcls01()});
	}
	
}
