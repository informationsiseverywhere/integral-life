package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Agpypf {

	private long uniqueNumber;
	private String agntpfx;
	private String agntcoy;
	private String agntnum;
	private String agntbr;
	private String aracde;
	private String batcpfx;
	private String batccoy;
	private String batcbrn;
	private Short batcactyr;
	private int batcactmn;
	private String batctrcde;
	private String batcbatch;
	private String paypfx;
	private String paycoy;
	private String bankkey;
	private String bankacckey;
	private String reqnpfx;
	private String reqncoy;
	private String reqnbcde;
	private String reqnno;
	private BigDecimal tranamt;
	private String sacscode;
	private String sacstyp;
	private String rdocnum;
	private Integer tranno;
	private Short jrnseq;
	private BigDecimal origamt;
	private String tranref;
	private String trandesc;
	private BigDecimal crate;
	private BigDecimal acctamt;
	private String genlcoy;
	private String genlcur;
	private String glcode;
	private String glsign;
	private String postyear;
	private String postmonth;
	private Integer effdate;
	private BigDecimal rcamt;
	private Integer frcdate;
	private Integer trdt;
	private Integer userT;
	private String termid;
	private String rldgcoy;
	private String rldgacct;
	private String origcurr;
	private String suprflg;
	private Integer trtm;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(String agntpfx) {
		this.agntpfx = agntpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getAgntbr() {
		return agntbr;
	}
	public void setAgntbr(String agntbr) {
		this.agntbr = agntbr;
	}
	public String getAracde() {
		return aracde;
	}
	public void setAracde(String aracde) {
		this.aracde = aracde;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public Short getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Short batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getPaypfx() {
		return paypfx;
	}
	public void setPaypfx(String paypfx) {
		this.paypfx = paypfx;
	}
	public String getPaycoy() {
		return paycoy;
	}
	public void setPaycoy(String paycoy) {
		this.paycoy = paycoy;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getReqnpfx() {
		return reqnpfx;
	}
	public void setReqnpfx(String reqnpfx) {
		this.reqnpfx = reqnpfx;
	}
	public String getReqncoy() {
		return reqncoy;
	}
	public void setReqncoy(String reqncoy) {
		this.reqncoy = reqncoy;
	}
	public String getReqnbcde() {
		return reqnbcde;
	}
	public void setReqnbcde(String reqnbcde) {
		this.reqnbcde = reqnbcde;
	}
	public String getReqnno() {
		return reqnno;
	}
	public void setReqnno(String reqnno) {
		this.reqnno = reqnno;
	}
	public BigDecimal getTranamt() {
		return tranamt;
	}
	public void setTranamt(BigDecimal tranamt) {
		this.tranamt = tranamt;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(String rdocnum) {
		this.rdocnum = rdocnum;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public Short getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(Short jrnseq) {
		this.jrnseq = jrnseq;
	}
	public BigDecimal getOrigamt() {
		return origamt;
	}
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}
	public String getTranref() {
		return tranref;
	}
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}
	public String getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}
	public BigDecimal getCrate() {
		return crate;
	}
	public void setCrate(BigDecimal crate) {
		this.crate = crate;
	}
	public BigDecimal getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(BigDecimal acctamt) {
		this.acctamt = acctamt;
	}
	public String getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(String genlcoy) {
		this.genlcoy = genlcoy;
	}
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	public String getGlcode() {
		return glcode;
	}
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	public String getGlsign() {
		return glsign;
	}
	public void setGlsign(String glsign) {
		this.glsign = glsign;
	}
	public String getPostyear() {
		return postyear;
	}
	public void setPostyear(String postyear) {
		this.postyear = postyear;
	}
	public String getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(String postmonth) {
		this.postmonth = postmonth;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getRcamt() {
		return rcamt;
	}
	public void setRcamt(BigDecimal rcamt) {
		this.rcamt = rcamt;
	}
	public Integer getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(Integer frcdate) {
		this.frcdate = frcdate;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public String getSuprflg() {
		return suprflg;
	}
	public void setSuprflg(String suprflg) {
		this.suprflg = suprflg;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	
	
}
