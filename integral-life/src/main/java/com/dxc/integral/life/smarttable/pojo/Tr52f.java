package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * @author wli31
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class Tr52f {
  	public List<BigDecimal> dtyamts;
  	public List<String> shortdss;
  	public List<String> txabsinds;
  	public List<BigDecimal> txfxamtas;
  	public List<BigDecimal> txfxamtbs;
  	public List<BigDecimal> txrateas;
  	public List<BigDecimal> txratebs;
  	public List<String> txtypes;
  	
	public List<BigDecimal> getDtyamts() {
		return dtyamts;
	}
	public void setDtyamts(List<BigDecimal> dtyamts) {
		this.dtyamts = dtyamts;
	}
	public List<String> getShortdss() {
		return shortdss;
	}
	public void setShortdss(List<String> shortdss) {
		this.shortdss = shortdss;
	}
	public List<String> getTxabsinds() {
		return txabsinds;
	}
	public void setTxabsinds(List<String> txabsinds) {
		this.txabsinds = txabsinds;
	}
	public List<BigDecimal> getTxfxamtas() {
		return txfxamtas;
	}
	public void setTxfxamtas(List<BigDecimal> txfxamtas) {
		this.txfxamtas = txfxamtas;
	}
	public List<BigDecimal> getTxfxamtbs() {
		return txfxamtbs;
	}
	public void setTxfxamtbs(List<BigDecimal> txfxamtbs) {
		this.txfxamtbs = txfxamtbs;
	}
	public List<BigDecimal> getTxrateas() {
		return txrateas;
	}
	public void setTxrateas(List<BigDecimal> txrateas) {
		this.txrateas = txrateas;
	}
	public List<BigDecimal> getTxratebs() {
		return txratebs;
	}
	public void setTxratebs(List<BigDecimal> txratebs) {
		this.txratebs = txratebs;
	}
	public List<String> getTxtypes() {
		return txtypes;
	}
	public void setTxtypes(List<String> txtypes) {
		this.txtypes = txtypes;
	}
  	
  	
}
