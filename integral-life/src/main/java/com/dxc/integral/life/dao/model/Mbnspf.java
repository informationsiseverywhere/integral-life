package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;


public class Mbnspf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int yrsinf;
	private BigDecimal sumins;
	private BigDecimal surrval;
	private String usrprf;
	private String jobnm;
	private String datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getYrsinf() {
		return yrsinf;
	}
	public void setYrsinf(int yrsinf) {
		this.yrsinf = yrsinf;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public BigDecimal getSurrval() {
		return surrval;
	}
	public void setSurrval(BigDecimal surrval) {
		this.surrval = surrval;
	}
	
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
}