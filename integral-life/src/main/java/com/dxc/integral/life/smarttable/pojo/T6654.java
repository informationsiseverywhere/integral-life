package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T6654 {

  	private String arrearsMethod;
  	private String collectsub;
  	private List<Integer> daexpys;
  	private List<String> doctids;
  	private int leadDays;
  	private int nonForfietureDays;
  	private List<String> renwdates;
  	private List<String> zrfreqs;
  	private List<Integer> zrincrs;
	public String getArrearsMethod() {
		return arrearsMethod;
	}
	public void setArrearsMethod(String arrearsMethod) {
		this.arrearsMethod = arrearsMethod;
	}
	public String getCollectsub() {
		return collectsub;
	}
	public void setCollectsub(String collectsub) {
		this.collectsub = collectsub;
	}
	public List<Integer> getDaexpys() {
		return daexpys;
	}
	public void setDaexpys(List<Integer> daexpys) {
		this.daexpys = daexpys;
	}
	public List<String> getDoctids() {
		return doctids;
	}
	public void setDoctids(List<String> doctids) {
		this.doctids = doctids;
	}
	public int getLeadDays() {
		return leadDays;
	}
	public void setLeadDays(int leadDays) {
		this.leadDays = leadDays;
	}
	public int getNonForfietureDays() {
		return nonForfietureDays;
	}
	public void setNonForfietureDays(int nonForfietureDays) {
		this.nonForfietureDays = nonForfietureDays;
	}
	public List<String> getRenwdates() {
		return renwdates;
	}
	public void setRenwdates(List<String> renwdates) {
		this.renwdates = renwdates;
	}
	public List<String> getZrfreqs() {
		return zrfreqs;
	}
	public void setZrfreqs(List<String> zrfreqs) {
		this.zrfreqs = zrfreqs;
	}
	public List<Integer> getZrincrs() {
		return zrincrs;
	}
	public void setZrincrs(List<Integer> zrincrs) {
		this.zrincrs = zrincrs;
	}
	
  	
}