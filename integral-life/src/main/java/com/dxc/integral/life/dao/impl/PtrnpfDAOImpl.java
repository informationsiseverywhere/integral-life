package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Ptrnpf;
/**
 * @author wli31
 */
@Repository("ptrnpfDAO")
@Lazy
public class PtrnpfDAOImpl extends BaseDAOImpl implements PtrnpfDAO {

	@Override
	public void insertPtrnpfRecords(List<Ptrnpf> ptrnList) {
		final List<Ptrnpf> tempPtrnpfList = ptrnList;   
		StringBuilder sb = new StringBuilder("");
		sb.append("insert into ptrnpf(chdrpfx, chdrcoy, chdrnum, recode, tranno, ptrneff, trdt, trtm, termid, user_t, batcpfx, batccoy, batcbrn, batcactyr, batcactmn, batctrcde, batcbatch, prtflg, validflag, usrprf, jobnm, datime, datesub, crtuser) "); 
		sb.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
	       jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {  
	            @Override
	            public int getBatchSize() {  
	                 return tempPtrnpfList.size();   
	            }  
	            @Override  
	            public void setValues(PreparedStatement ps, int i)  
	                    throws SQLException { 
	            	ps.setString(1, ptrnList.get(i).getChdrpfx());
					ps.setString(2, ptrnList.get(i).getChdrcoy());
				    ps.setString(3, ptrnList.get(i).getChdrnum());
				    ps.setString(4, ptrnList.get(i).getRecode());
				    ps.setInt(5, ptrnList.get(i).getTranno());			    
					ps.setInt(6, ptrnList.get(i).getPtrneff());
					ps.setInt(7, ptrnList.get(i).getTrdt());
				    ps.setInt(8, ptrnList.get(i).getTrtm());
				    ps.setString(9, ptrnList.get(i).getTermid()); 			    
				    ps.setInt(10, ptrnList.get(i).getUserT());		
				    ps.setString(11, ptrnList.get(i).getBatcpfx());
					ps.setString(12, ptrnList.get(i).getBatccoy());
					ps.setString(13, ptrnList.get(i).getBatcbrn());
				    ps.setInt(14, ptrnList.get(i).getBatcactyr());
				    ps.setInt(15, ptrnList.get(i).getBatcactmn());			    
				    ps.setString(16, ptrnList.get(i).getBatctrcde());	
					ps.setString(17, ptrnList.get(i).getBatcbatch());				
					ps.setString(18, ptrnList.get(i).getPrtflg());				
				    ps.setString(19,  ptrnList.get(i).getValidflag());			    
				    ps.setString(20, ptrnList.get(i).getUsrprf());
				    ps.setString(21, ptrnList.get(i).getJobnm());
				    ps.setTimestamp(22, new Timestamp(System.currentTimeMillis()));		    
				    ps.setDouble(23, ptrnList.get(i).getDatesub());			    
				    ps.setString(24, ptrnList.get(i).getCrtuser());	
	            }   
	      });  

	}

	@Override
	public List<Ptrnpf> searchPtrnenqRecord(String coy, String chdrnum) {
		StringBuilder sqlPtrnSelect1 = new StringBuilder(
				"SELECT PTRNEFF,CHDRCOY,CHDRNUM,VALIDFLAG,TRANNO,BATCACTYR,BATCACTMN,BATCTRCDE ");
		sqlPtrnSelect1.append("FROM PTRNPF WHERE TRANNO<=99999 AND CHDRCOY=? AND CHDRNUM = ? ");
		sqlPtrnSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, TRANNO DESC, BATCTRCDE ASC, UNIQUE_NUMBER DESC");
		Object[] para = new Object[] { coy, chdrnum};
		logSQL(sqlPtrnSelect1.toString(), para);
		return jdbcTemplate.query(sqlPtrnSelect1.toString(),
				para, new BeanPropertyRowMapper<Ptrnpf>(Ptrnpf.class));

	}

	@Override
	public Ptrnpf getPtrnData(String coy, String chdrnum, String batctrcde) {
		String sql_select  = "SELECT TRANNO,DATESUB FROM PTRNPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND BATCTRCDE=?";
		Object[] para = new Object[] { coy, chdrnum, batctrcde};
		logSQL(sql_select, para);//IJTI-1415
		return jdbcTemplate.queryForObject(sql_select,//IJTI-1415
				para, new BeanPropertyRowMapper<Ptrnpf>(Ptrnpf.class));
	}
	
	@Override
	public Ptrnpf getPtrnRecord(String chdrcoy, String chdrnum, int tranno) {
		String sql = "SELECT * FROM PTRNPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?";
		return jdbcTemplate.queryForObject(sql, new Object[] {chdrcoy, chdrnum, tranno}, new BeanPropertyRowMapper<Ptrnpf>(Ptrnpf.class));
	}
	
	@Override
	public int updatePtrnpf(Ptrnpf ptrnpf) {
		String sql = "UPDATE PTRNPF SET CRTUSER=?, VALIDFLAG=? WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?";
		return jdbcTemplate.update(sql,
				new Object[] { ptrnpf.getUsrprf(), ptrnpf.getValidflag(), ptrnpf.getChdrcoy(), ptrnpf.getChdrnum(), ptrnpf.getTranno().intValue() });
	}

	@Override
	public Ptrnpf getPtrnByTrn(String chdrcoy, String chdrnum) {
		String sql = "SELECT * FROM PTRNPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO = (SELECT MAX(TRANNO) FROM PTRNPF WHERE CHDRCOY=? AND CHDRNUM= ? ) ";
		return jdbcTemplate.queryForObject(sql, new Object[] {chdrcoy, chdrnum, chdrcoy, chdrnum}, new BeanPropertyRowMapper<Ptrnpf>(Ptrnpf.class));
	}
}
