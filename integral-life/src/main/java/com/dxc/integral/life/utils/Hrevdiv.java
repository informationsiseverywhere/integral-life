package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.GreversDTO;

public interface Hrevdiv {
	public GreversDTO processHrevdiv(GreversDTO greversDTO);
}
