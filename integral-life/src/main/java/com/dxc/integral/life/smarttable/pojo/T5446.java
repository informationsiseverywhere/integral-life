package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
/**
 * @author wli31
 */
public class T5446 {
  	private String currcode;
  	private BigDecimal discretn;
  	private BigDecimal retn;
  	private String lrkcls;
  	private BigDecimal maxAmount;
  	private BigDecimal fieldseq;
	private String rskcldisp;
  	private String filler;
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public BigDecimal getDiscretn() {
		return discretn;
	}
	public void setDiscretn(BigDecimal discretn) {
		this.discretn = discretn;
	}
	public BigDecimal getRetn() {
		return retn;
	}
	public void setRetn(BigDecimal retn) {
		this.retn = retn;
	}
	public String getLrkcls() {
		return lrkcls;
	}
	public void setLrkcls(String lrkcls) {
		this.lrkcls = lrkcls;
	}
	public BigDecimal getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}
	public BigDecimal getFieldseq() {
		return fieldseq;
	}
	public void setFieldseq(BigDecimal fieldseq) {
		this.fieldseq = fieldseq;
	}
	public String getRskcldisp() {
		return rskcldisp;
	}
	public void setRskcldisp(String rskcldisp) {
		this.rskcldisp = rskcldisp;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
  	
}
