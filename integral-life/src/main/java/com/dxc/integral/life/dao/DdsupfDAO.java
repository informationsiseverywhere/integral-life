package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Ddsupf;

public interface DdsupfDAO {
	List<Ddsupf> readDdsupfRecords(String company, String contractNumber);
}
