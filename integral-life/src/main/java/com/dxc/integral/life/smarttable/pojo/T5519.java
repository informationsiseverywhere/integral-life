package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class T5519 {

	private BigDecimal annchg;
	private String diffprice;
	private int fixdtrm;
	private String initUnitChargeFreq;
	private String subprog;
	private String unitadj;
	private String unitdeduc;
	
	public BigDecimal getAnnchg() {
		return annchg;
	}
	public void setAnnchg(BigDecimal annchg) {
		this.annchg = annchg;
	}
	public String getDiffprice() {
		return diffprice;
	}
	public void setDiffprice(String diffprice) {
		this.diffprice = diffprice;
	}
	public int getFixdtrm() {
		return fixdtrm;
	}
	public void setFixdtrm(int fixdtrm) {
		this.fixdtrm = fixdtrm;
	}
	public String getInitUnitChargeFreq() {
		return initUnitChargeFreq;
	}
	public void setInitUnitChargeFreq(String initUnitChargeFreq) {
		this.initUnitChargeFreq = initUnitChargeFreq;
	}
	public String getSubprog() {
		return subprog;
	}
	public void setSubprog(String subprog) {
		this.subprog = subprog;
	}
	public String getUnitadj() {
		return unitadj;
	}
	public void setUnitadj(String unitadj) {
		this.unitadj = unitadj;
	}
	public String getUnitdeduc() {
		return unitdeduc;
	}
	public void setUnitdeduc(String unitdeduc) {
		this.unitdeduc = unitdeduc;
	}
	
	
}
