package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.AgecalcDTO;
/**
 * @author wli31
 */
public interface Agecalc {

	public AgecalcDTO processAgecalc(AgecalcDTO agecalcDTO) throws NumberFormatException, ParseException,IOException;
}
