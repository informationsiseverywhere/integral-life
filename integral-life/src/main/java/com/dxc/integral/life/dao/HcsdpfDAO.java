package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Hcsdpf;

public interface HcsdpfDAO {

	List<Hcsdpf> readHcsdpfRecords(String company, String contractNumber);
	Hcsdpf readHcsdpfRecordsWithoutFlag(String company, String contractNumber);
	Hcsdpf readHcsdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
	int updateHcsdpfRecord(String validflag, String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx); 
	int deleteHcsdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
}
