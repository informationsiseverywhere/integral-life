package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class VpxlextDTO {
	private String function;
  	private String statuz;
  	private String copybook;
  	private String result;
  	private String resultField;
  	private String resultValue;
  	private int count;
  	private BigDecimal[] oppcs;
	private String tabsorsel;
	private Integer[] zmortpcts;
	private Integer[] agerates;
	private Integer[] insprms;
	private BigDecimal znadjperc;
	private String[] opcdas;
	private String zszprmcd ;
	private String prat;
	private String coverc;
	private String trmofcontract;
	private int hpropdte;
	private String premind ;
	private int tempprat;
	private int temptoc;
	private int noOfCoverages;
	private String aidsCoverInd ;
	private BigDecimal totalSumInsured;
	private String ncdCode;
	private String claimAutoIncrease ;
	private String syndicateCode;
	private String riskExpiryAge;
	private BigDecimal pratNew;
	private BigDecimal oppc01;
  	private BigDecimal oppc02;
  	private BigDecimal oppc03;
  	private BigDecimal oppc04;
  	private BigDecimal oppc05;
  	private BigDecimal oppc06;
  	private BigDecimal oppc07;
  	private BigDecimal oppc08;
	private int zmortpct01;
	private int zmortpct02;
	private int zmortpct03;
	private int zmortpct04;
	private int zmortpct05;
	private int zmortpct06;
	private int zmortpct07;
	private int zmortpct08;
	private String opcda01;
	private String opcda02;
	private String opcda03;
	private String opcda04;
	private String opcda05;
	private String opcda06;
	private String opcda07;
	private String opcda08;
	private int agerate01;
	private int agerate02;
	private int agerate03;
	private int agerate04;
	private int agerate05;
	private int agerate06;
	private int agerate07;
	private int agerate08;
	private int insprm01;
	private int insprm02;
	private int insprm03;
	private int insprm04;
	private int insprm05;
	private int insprm06;
	private int insprm07;
	private int insprm08;
	
	public BigDecimal getOppc01() {
		return oppc01;
	}
	public void setOppc01(BigDecimal oppc01) {
		this.oppc01 = oppc01;
	}
	public BigDecimal getOppc02() {
		return oppc02;
	}
	public void setOppc02(BigDecimal oppc02) {
		this.oppc02 = oppc02;
	}
	public BigDecimal getOppc03() {
		return oppc03;
	}
	public void setOppc03(BigDecimal oppc03) {
		this.oppc03 = oppc03;
	}
	public BigDecimal getOppc04() {
		return oppc04;
	}
	public void setOppc04(BigDecimal oppc04) {
		this.oppc04 = oppc04;
	}
	public BigDecimal getOppc05() {
		return oppc05;
	}
	public void setOppc05(BigDecimal oppc05) {
		this.oppc05 = oppc05;
	}
	public BigDecimal getOppc06() {
		return oppc06;
	}
	public void setOppc06(BigDecimal oppc06) {
		this.oppc06 = oppc06;
	}
	public BigDecimal getOppc07() {
		return oppc07;
	}
	public void setOppc07(BigDecimal oppc07) {
		this.oppc07 = oppc07;
	}
	public BigDecimal getOppc08() {
		return oppc08;
	}
	public void setOppc08(BigDecimal oppc08) {
		this.oppc08 = oppc08;
	}
	public int getZmortpct01() {
		return zmortpct01;
	}
	public void setZmortpct01(int zmortpct01) {
		this.zmortpct01 = zmortpct01;
	}
	public int getZmortpct02() {
		return zmortpct02;
	}
	public void setZmortpct02(int zmortpct02) {
		this.zmortpct02 = zmortpct02;
	}
	public int getZmortpct03() {
		return zmortpct03;
	}
	public void setZmortpct03(int zmortpct03) {
		this.zmortpct03 = zmortpct03;
	}
	public int getZmortpct04() {
		return zmortpct04;
	}
	public void setZmortpct04(int zmortpct04) {
		this.zmortpct04 = zmortpct04;
	}
	public int getZmortpct05() {
		return zmortpct05;
	}
	public void setZmortpct05(int zmortpct05) {
		this.zmortpct05 = zmortpct05;
	}
	public int getZmortpct06() {
		return zmortpct06;
	}
	public void setZmortpct06(int zmortpct06) {
		this.zmortpct06 = zmortpct06;
	}
	public int getZmortpct07() {
		return zmortpct07;
	}
	public void setZmortpct07(int zmortpct07) {
		this.zmortpct07 = zmortpct07;
	}
	public int getZmortpct08() {
		return zmortpct08;
	}
	public void setZmortpct08(int zmortpct08) {
		this.zmortpct08 = zmortpct08;
	}
	public String getOpcda01() {
		return opcda01;
	}
	public void setOpcda01(String opcda01) {
		this.opcda01 = opcda01;
	}
	public String getOpcda02() {
		return opcda02;
	}
	public void setOpcda02(String opcda02) {
		this.opcda02 = opcda02;
	}
	public String getOpcda03() {
		return opcda03;
	}
	public void setOpcda03(String opcda03) {
		this.opcda03 = opcda03;
	}
	public String getOpcda04() {
		return opcda04;
	}
	public void setOpcda04(String opcda04) {
		this.opcda04 = opcda04;
	}
	public String getOpcda05() {
		return opcda05;
	}
	public void setOpcda05(String opcda05) {
		this.opcda05 = opcda05;
	}
	public String getOpcda06() {
		return opcda06;
	}
	public void setOpcda06(String opcda06) {
		this.opcda06 = opcda06;
	}
	public String getOpcda07() {
		return opcda07;
	}
	public void setOpcda07(String opcda07) {
		this.opcda07 = opcda07;
	}
	public String getOpcda08() {
		return opcda08;
	}
	public void setOpcda08(String opcda08) {
		this.opcda08 = opcda08;
	}
	public int getAgerate01() {
		return agerate01;
	}
	public void setAgerate01(int agerate01) {
		this.agerate01 = agerate01;
	}
	public int getAgerate02() {
		return agerate02;
	}
	public void setAgerate02(int agerate02) {
		this.agerate02 = agerate02;
	}
	public int getAgerate03() {
		return agerate03;
	}
	public void setAgerate03(int agerate03) {
		this.agerate03 = agerate03;
	}
	public int getAgerate04() {
		return agerate04;
	}
	public void setAgerate04(int agerate04) {
		this.agerate04 = agerate04;
	}
	public int getAgerate05() {
		return agerate05;
	}
	public void setAgerate05(int agerate05) {
		this.agerate05 = agerate05;
	}
	public int getAgerate06() {
		return agerate06;
	}
	public void setAgerate06(int agerate06) {
		this.agerate06 = agerate06;
	}
	public int getAgerate07() {
		return agerate07;
	}
	public void setAgerate07(int agerate07) {
		this.agerate07 = agerate07;
	}
	public int getAgerate08() {
		return agerate08;
	}
	public void setAgerate08(int agerate08) {
		this.agerate08 = agerate08;
	}
	public int getInsprm01() {
		return insprm01;
	}
	public void setInsprm01(int insprm01) {
		this.insprm01 = insprm01;
	}
	public int getInsprm02() {
		return insprm02;
	}
	public void setInsprm02(int insprm02) {
		this.insprm02 = insprm02;
	}
	public int getInsprm03() {
		return insprm03;
	}
	public void setInsprm03(int insprm03) {
		this.insprm03 = insprm03;
	}
	public int getInsprm04() {
		return insprm04;
	}
	public void setInsprm04(int insprm04) {
		this.insprm04 = insprm04;
	}
	public int getInsprm05() {
		return insprm05;
	}
	public void setInsprm05(int insprm05) {
		this.insprm05 = insprm05;
	}
	public int getInsprm06() {
		return insprm06;
	}
	public void setInsprm06(int insprm06) {
		this.insprm06 = insprm06;
	}
	public int getInsprm07() {
		return insprm07;
	}
	public void setInsprm07(int insprm07) {
		this.insprm07 = insprm07;
	}
	public int getInsprm08() {
		return insprm08;
	}
	public void setInsprm08(int insprm08) {
		this.insprm08 = insprm08;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCopybook() {
		return copybook;
	}
	public void setCopybook(String copybook) {
		this.copybook = copybook;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultField() {
		return resultField;
	}
	public void setResultField(String resultField) {
		this.resultField = resultField;
	}
	public String getResultValue() {
		return resultValue;
	}
	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	public String getTabsorsel() {
		return tabsorsel;
	}
	public void setTabsorsel(String tabsorsel) {
		this.tabsorsel = tabsorsel;
	}
	
	public BigDecimal[] getOppcs() {
		return oppcs;
	}
	public void setOppcs(BigDecimal[] oppcs) {
		this.oppcs = oppcs;
	}
	public Integer[] getZmortpcts() {
		return zmortpcts;
	}
	public void setZmortpcts(Integer[] zmortpcts) {
		this.zmortpcts = zmortpcts;
	}
	public Integer[] getAgerates() {
		return agerates;
	}
	public void setAgerates(Integer[] agerates) {
		this.agerates = agerates;
	}
	public Integer[] getInsprms() {
		return insprms;
	}
	public void setInsprms(Integer[] insprms) {
		this.insprms = insprms;
	}
	public BigDecimal getZnadjperc() {
		return znadjperc;
	}
	public void setZnadjperc(BigDecimal znadjperc) {
		this.znadjperc = znadjperc;
	}
	public String getZszprmcd() {
		return zszprmcd;
	}
	public void setZszprmcd(String zszprmcd) {
		this.zszprmcd = zszprmcd;
	}
	public String getPrat() {
		return prat;
	}
	public void setPrat(String prat) {
		this.prat = prat;
	}
	public String getCoverc() {
		return coverc;
	}
	public void setCoverc(String coverc) {
		this.coverc = coverc;
	}
	public String getTrmofcontract() {
		return trmofcontract;
	}
	public void setTrmofcontract(String trmofcontract) {
		this.trmofcontract = trmofcontract;
	}
	public int getHpropdte() {
		return hpropdte;
	}
	public void setHpropdte(int hpropdte) {
		this.hpropdte = hpropdte;
	}
	public String getPremind() {
		return premind;
	}
	public void setPremind(String premind) {
		this.premind = premind;
	}
	public int getTempprat() {
		return tempprat;
	}
	public void setTempprat(int tempprat) {
		this.tempprat = tempprat;
	}
	public int getTemptoc() {
		return temptoc;
	}
	public void setTemptoc(int temptoc) {
		this.temptoc = temptoc;
	}
	public int getNoOfCoverages() {
		return noOfCoverages;
	}
	public void setNoOfCoverages(int noOfCoverages) {
		this.noOfCoverages = noOfCoverages;
	}
	public String getAidsCoverInd() {
		return aidsCoverInd;
	}
	public void setAidsCoverInd(String aidsCoverInd) {
		this.aidsCoverInd = aidsCoverInd;
	}
	public BigDecimal getTotalSumInsured() {
		return totalSumInsured;
	}
	public void setTotalSumInsured(BigDecimal totalSumInsured) {
		this.totalSumInsured = totalSumInsured;
	}
	public String getNcdCode() {
		return ncdCode;
	}
	public void setNcdCode(String ncdCode) {
		this.ncdCode = ncdCode;
	}
	public String getClaimAutoIncrease() {
		return claimAutoIncrease;
	}
	public void setClaimAutoIncrease(String claimAutoIncrease) {
		this.claimAutoIncrease = claimAutoIncrease;
	}
	public String getSyndicateCode() {
		return syndicateCode;
	}
	public void setSyndicateCode(String syndicateCode) {
		this.syndicateCode = syndicateCode;
	}
	public String getRiskExpiryAge() {
		return riskExpiryAge;
	}
	public void setRiskExpiryAge(String riskExpiryAge) {
		this.riskExpiryAge = riskExpiryAge;
	}
	public BigDecimal getPratNew() {
		return pratNew;
	}
	public void setPratNew(BigDecimal pratNew) {
		this.pratNew = pratNew;
	}
	public String[] getOpcdas() {
		return opcdas;
	}
	public void setOpcdas(String[] opcdas) {
		this.opcdas = opcdas;
	}
	
	
}
