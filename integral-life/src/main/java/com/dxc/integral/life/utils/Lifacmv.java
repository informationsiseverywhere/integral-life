package com.dxc.integral.life.utils;


import java.io.IOException;


import java.text.ParseException;
import com.dxc.integral.life.beans.LifacmvDTO;

/**
 * Lifacmv utility.
 * 
 * @author vhukumagrawa
 *
 */
public interface Lifacmv {

	/**
	 * Executes PSTW function.
	 * 
	 * @param lifeacmvdto
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	public void executePSTW(LifacmvDTO lifeacmvdto)throws IOException, ParseException;//ILIFE-5763
	
	/**
	 * Executes NPSTW function.
	 * 
	 * @param lifeacmvdto
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	public void executeNPSTW(LifacmvDTO lifeacmvdto)throws IOException, ParseException;//ILIFE-5763
}
