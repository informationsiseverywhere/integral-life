/**
 * 
 */
package com.dxc.integral.life.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.CovtpfDAO;
import com.dxc.integral.life.dao.model.Covtpf;

/**
 * @author gsaluja2
 *
 */

@Repository("covtpfDAO")
public class CovtpfDAOImpl extends BaseDAOImpl implements CovtpfDAO{

	@Override
	public Map<String, List<Covtpf>> searchCovtpf(int transactionDate) {
		Map<String, List<Covtpf>> covtListMap = new HashMap<>();
		List<Covtpf> covtList;
		StringBuilder sql = new StringBuilder("SELECT * FROM COVTPF WHERE CHDRNUM IN ");
		sql.append("(SELECT CHDRNUM FROM LINM WHERE PTRNEFF=?)");
		covtList = jdbcTemplate.query(sql.toString(),new Object[]{transactionDate},
				new BeanPropertyRowMapper<Covtpf>(Covtpf.class));
		for(Covtpf covt: covtList) {
			if(!covtListMap.isEmpty() && covtListMap.containsKey(covt.getChdrnum()))
				covtListMap.get(covt.getChdrnum()).add(covt);
			else {
				List<Covtpf> covtpfList = new ArrayList<>();
				covtpfList.add(covt);
				covtListMap.put(covt.getChdrnum(), covtpfList);
			}
		}
		return covtListMap;
	}

}
