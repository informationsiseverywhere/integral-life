/*

 * File: PtrnpfDAOImpl.java
 * Date: March 17, 2016
 * Author: CSC
 * Created by: dpuhawan
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.dxc.integral.life.dao;
 
import java.util.List;

import com.dxc.integral.life.dao.model.Ptrnpf;

public interface PtrnpfDAO {
	public void insertPtrnpfRecords(List<Ptrnpf> ptrnList);

	public List<Ptrnpf> searchPtrnenqRecord(String coy, String chdrnum);
	
	public Ptrnpf getPtrnData(String coy, String chdrnum, String batctrcde);
	
	public Ptrnpf getPtrnRecord(String chdrcoy, String chdrnum, int tranno);
	
	int updatePtrnpf(Ptrnpf ptrnpf);
	
	public Ptrnpf getPtrnByTrn(String chdrcoy, String chdrnum);
}
