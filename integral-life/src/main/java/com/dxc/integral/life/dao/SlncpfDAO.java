package com.dxc.integral.life.dao;

import java.util.List;
import com.dxc.integral.life.dao.model.Slncpf;

/**
 * @author gsaluja2
 *
 */

public interface SlncpfDAO {
	public List<Slncpf> readSlncpfData(String zreclass);
	public List<String> readUniqueSlncpfRecords();
	public int deleteSlncpfData(String zreclass);
}
