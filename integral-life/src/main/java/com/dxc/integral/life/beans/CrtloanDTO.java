package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for Crtloan utility.
 * 
 * @author asirvya
 */

public class CrtloanDTO {

	/** The chdrcoy */
	private String chdrcoy;
	/** The chdrnum */
	private String chdrnum;
	/** The tranno */
	private int tranno;
	/** The loanno */
	private int loanno;
	/** The cnttype */
	private String cnttype;
	/** The cntcurr */
	private String cntcurr;
	/** The billcurr */
	private String billcurr;
	/** The effdate */
	private int effdate;
	/** The occdate */
	private int occdate;
	/** The authCode */
	private String authCode;
	/** The language */
	private String language;
	/** The outstamt */
	private BigDecimal outstamt;
	/** The cbillamt */
	private BigDecimal cbillamt;
	/** The prefix */
	private String prefix;
	/** The company */
	private String company;
	/** The branch */
	private String branch;
	/** The actyear */
	private int actyear;
	/** The actmonth */
	private int actmonth;
	/** The trcde */
	private String trcde;
	/** The batch */
	private String batch;
	/** The sacscode01 */
	private String sacscode01;
	/** The sacscode02 */
	private String sacscode02;
	/** The sacscode03 */
	private String sacscode03;
	/** The sacscode04 */
	private String sacscode04;
	/** The sacstyp01 */
	private String sacstyp01;
	/** The sacstyp02 */
	private String sacstyp02;
	/** The sacstyp03 */
	private String sacstyp03;
	/** The sacstyp04 */
	private String sacstyp04;
	/** The glcode01 */
	private String glcode01;
	/** The glcode02 */
	private String glcode02;
	/** The glcode03 */
	private String glcode03;
	/** The glcode04 */
	private String glcode04;
	/** The glsign01 */
	private String glsign01;
	/** The glsign02 */
	private String glsign02;
	/** The glsign03 */
	private String glsign03;
	/** The glsign04 */
	private String glsign04;
	/** The longdesc */
	private String longdesc;
	/** The loantype */
	private String loantype;
	/** The jobname */
	private String jobname;
	/** The usrprofile */
	private String usrprofile;

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * @param chdrcoy
	 *            the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * @param chdrnum
	 *            the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * @return the tranno
	 */
	public int getTranno() {
		return tranno;
	}

	/**
	 * @param tranno
	 *            the tranno to set
	 */
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	/**
	 * @return the loanno
	 */
	public int getLoanno() {
		return loanno;
	}

	/**
	 * @param loanno
	 *            the loanno to set
	 */
	public void setLoanno(int loanno) {
		this.loanno = loanno;
	}

	/**
	 * @return the cnttype
	 */
	public String getCnttype() {
		return cnttype;
	}

	/**
	 * @param cnttype
	 *            the cnttype to set
	 */
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	/**
	 * @return the cntcurr
	 */
	public String getCntcurr() {
		return cntcurr;
	}

	/**
	 * @param cntcurr
	 *            the cntcurr to set
	 */
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	/**
	 * @return the billcurr
	 */
	public String getBillcurr() {
		return billcurr;
	}

	/**
	 * @param billcurr
	 *            the billcurr to set
	 */
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}

	/**
	 * @return the effdate
	 */
	public int getEffdate() {
		return effdate;
	}

	/**
	 * @param effdate
	 *            the effdate to set
	 */
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	/**
	 * @return the occdate
	 */
	public int getOccdate() {
		return occdate;
	}

	/**
	 * @param occdate
	 *            the occdate to set
	 */
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}

	/**
	 * @return the authCode
	 */
	public String getAuthCode() {
		return authCode;
	}

	/**
	 * @param authCode
	 *            the authCode to set
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the outstamt
	 */
	public BigDecimal getOutstamt() {
		return outstamt;
	}

	/**
	 * @param outstamt
	 *            the outstamt to set
	 */
	public void setOutstamt(BigDecimal outstamt) {
		this.outstamt = outstamt;
	}

	/**
	 * @return the cbillamt
	 */
	public BigDecimal getCbillamt() {
		return cbillamt;
	}

	/**
	 * @param cbillamt
	 *            the cbillamt to set
	 */
	public void setCbillamt(BigDecimal cbillamt) {
		this.cbillamt = cbillamt;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch
	 *            the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the actyear
	 */
	public int getActyear() {
		return actyear;
	}

	/**
	 * @param actyear
	 *            the actyear to set
	 */
	public void setActyear(int actyear) {
		this.actyear = actyear;
	}

	/**
	 * @return the actmonth
	 */
	public int getActmonth() {
		return actmonth;
	}

	/**
	 * @param actmonth
	 *            the actmonth to set
	 */
	public void setActmonth(int actmonth) {
		this.actmonth = actmonth;
	}

	/**
	 * @return the trcde
	 */
	public String getTrcde() {
		return trcde;
	}

	/**
	 * @param trcde
	 *            the trcde to set
	 */
	public void setTrcde(String trcde) {
		this.trcde = trcde;
	}

	/**
	 * @return the batch
	 */
	public String getBatch() {
		return batch;
	}

	/**
	 * @param batch
	 *            the batch to set
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * @return the sacscode01
	 */
	public String getSacscode01() {
		return sacscode01;
	}

	/**
	 * @param sacscode01
	 *            the sacscode01 to set
	 */
	public void setSacscode01(String sacscode01) {
		this.sacscode01 = sacscode01;
	}

	/**
	 * @return the sacscode02
	 */
	public String getSacscode02() {
		return sacscode02;
	}

	/**
	 * @param sacscode02
	 *            the sacscode02 to set
	 */
	public void setSacscode02(String sacscode02) {
		this.sacscode02 = sacscode02;
	}

	/**
	 * @return the sacscode03
	 */
	public String getSacscode03() {
		return sacscode03;
	}

	/**
	 * @param sacscode03
	 *            the sacscode03 to set
	 */
	public void setSacscode03(String sacscode03) {
		this.sacscode03 = sacscode03;
	}

	/**
	 * @return the sacscode04
	 */
	public String getSacscode04() {
		return sacscode04;
	}

	/**
	 * @param sacscode04
	 *            the sacscode04 to set
	 */
	public void setSacscode04(String sacscode04) {
		this.sacscode04 = sacscode04;
	}

	/**
	 * @return the sacstyp01
	 */
	public String getSacstyp01() {
		return sacstyp01;
	}

	/**
	 * @param sacstyp01
	 *            the sacstyp01 to set
	 */
	public void setSacstyp01(String sacstyp01) {
		this.sacstyp01 = sacstyp01;
	}

	/**
	 * @return the sacstyp02
	 */
	public String getSacstyp02() {
		return sacstyp02;
	}

	/**
	 * @param sacstyp02
	 *            the sacstyp02 to set
	 */
	public void setSacstyp02(String sacstyp02) {
		this.sacstyp02 = sacstyp02;
	}

	/**
	 * @return the sacstyp03
	 */
	public String getSacstyp03() {
		return sacstyp03;
	}

	/**
	 * @param sacstyp03
	 *            the sacstyp03 to set
	 */
	public void setSacstyp03(String sacstyp03) {
		this.sacstyp03 = sacstyp03;
	}

	/**
	 * @return the sacstyp04
	 */
	public String getSacstyp04() {
		return sacstyp04;
	}

	/**
	 * @param sacstyp04
	 *            the sacstyp04 to set
	 */
	public void setSacstyp04(String sacstyp04) {
		this.sacstyp04 = sacstyp04;
	}

	/**
	 * @return the glcode01
	 */
	public String getGlcode01() {
		return glcode01;
	}

	/**
	 * @param glcode01
	 *            the glcode01 to set
	 */
	public void setGlcode01(String glcode01) {
		this.glcode01 = glcode01;
	}

	/**
	 * @return the glcode02
	 */
	public String getGlcode02() {
		return glcode02;
	}

	/**
	 * @param glcode02
	 *            the glcode02 to set
	 */
	public void setGlcode02(String glcode02) {
		this.glcode02 = glcode02;
	}

	/**
	 * @return the glcode03
	 */
	public String getGlcode03() {
		return glcode03;
	}

	/**
	 * @param glcode03
	 *            the glcode03 to set
	 */
	public void setGlcode03(String glcode03) {
		this.glcode03 = glcode03;
	}

	/**
	 * @return the glcode04
	 */
	public String getGlcode04() {
		return glcode04;
	}

	/**
	 * @param glcode04
	 *            the glcode04 to set
	 */
	public void setGlcode04(String glcode04) {
		this.glcode04 = glcode04;
	}

	/**
	 * @return the glsign01
	 */
	public String getGlsign01() {
		return glsign01;
	}

	/**
	 * @param glsign01
	 *            the glsign01 to set
	 */
	public void setGlsign01(String glsign01) {
		this.glsign01 = glsign01;
	}

	/**
	 * @return the glsign02
	 */
	public String getGlsign02() {
		return glsign02;
	}

	/**
	 * @param glsign02
	 *            the glsign02 to set
	 */
	public void setGlsign02(String glsign02) {
		this.glsign02 = glsign02;
	}

	/**
	 * @return the glsign03
	 */
	public String getGlsign03() {
		return glsign03;
	}

	/**
	 * @param glsign03
	 *            the glsign03 to set
	 */
	public void setGlsign03(String glsign03) {
		this.glsign03 = glsign03;
	}

	/**
	 * @return the glsign04
	 */
	public String getGlsign04() {
		return glsign04;
	}

	/**
	 * @param glsign04
	 *            the glsign04 to set
	 */
	public void setGlsign04(String glsign04) {
		this.glsign04 = glsign04;
	}

	/**
	 * @return the longdesc
	 */
	public String getLongdesc() {
		return longdesc;
	}

	/**
	 * @param longdesc
	 *            the longdesc to set
	 */
	public void setLongdesc(String longdesc) {
		this.longdesc = longdesc;
	}

	/**
	 * @return the loantype
	 */
	public String getLoantype() {
		return loantype;
	}

	/**
	 * @param loantype
	 *            the loantype to set
	 */
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}

	/**
	 * @return the jobname
	 */
	public String getJobname() {
		return jobname;
	}

	/**
	 * @param jobname
	 *            the jobname to set
	 */
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	/**
	 * @return the usrprofile
	 */
	public String getUsrprofile() {
		return usrprofile;
	}

	/**
	 * @param usrprofile
	 *            the usrprofile to set
	 */
	public void setUsrprofile(String usrprofile) {
		this.usrprofile = usrprofile;
	}

}
