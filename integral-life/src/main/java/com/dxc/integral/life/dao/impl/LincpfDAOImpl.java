package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LincpfDAO;
import com.dxc.integral.life.dao.model.Lincpf;

/**
 * @author gsaluja2
 *
 */

@Repository("lincpfDAO")
public class LincpfDAOImpl extends BaseDAOImpl implements LincpfDAO{
	
	private static final Logger LOGER = LoggerFactory.getLogger(LincpfDAOImpl.class);
	
	private static final String LINCTABLE = "VM1DTA.LINCPF";
	private static final String SLNCTABLE = "VM1DTA.SLNCPF";
	
	public void updateLincData(List<Lincpf> lincpfUpdateList) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(LINCTABLE).append(" SET SENDDATE=?, RECVDATE=?, ZREGDATE=?, ZLINCDTE= ?, ");
		sql.append("USRPRF=?, JOBNM=?, DATIME=? WHERE UNIQUE_NUMBER=?");
		LOGER.info("updateLincData {}", sql);
		jdbcTemplate.batchUpdate(sql.toString(),new BatchPreparedStatementSetter() {
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException {
            	ps.setInt(1, lincpfUpdateList.get(i).getSenddate());
            	ps.setInt(2, lincpfUpdateList.get(i).getRecvdate());
            	ps.setInt(3, lincpfUpdateList.get(i).getZregdate());
            	ps.setInt(4, lincpfUpdateList.get(i).getZlincdte());
            	ps.setString(5, lincpfUpdateList.get(i).getUsrprf());
            	ps.setString(6, lincpfUpdateList.get(i).getJobnm());
            	ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            	ps.setLong(8, lincpfUpdateList.get(i).getUniqueNumber());
            }
            @Override
            public int getBatchSize() {  
                 return lincpfUpdateList.size();   
            }  
      });
	}
	
	public Lincpf readRecordOfLincpf(String contractNumber) {  
		String sql = "SELECT * FROM lincpf WHERE chdrnum=?";
		return jdbcTemplate.queryForObject(sql, new Object[]{contractNumber},new BeanPropertyRowMapper<Lincpf>(Lincpf.class));
	}
	
	public Map<String, Lincpf> readLincpfData(){
		List<Lincpf> lincpfList;
		Map<String, Lincpf> lincpfMap = new LinkedHashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM ").append(LINCTABLE).append(" WHERE CHDRNUM IN ");
		sql.append("(SELECT CHDRNUM FROM ").append(SLNCTABLE).append(")");
		LOGER.info("readLincpfData {}", sql);
		lincpfList = jdbcTemplate.query(sql.toString(),	new BeanPropertyRowMapper<Lincpf>(Lincpf.class));
		for(Lincpf lincpf: lincpfList) {
			if(lincpfMap.isEmpty() || !lincpfMap.containsKey(lincpf.getChdrnum()))
				lincpfMap.put(lincpf.getChdrnum(), lincpf);
		}
		return lincpfMap;
	}
}
