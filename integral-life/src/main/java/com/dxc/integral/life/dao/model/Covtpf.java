/**
 * 
 */
package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author gsaluja2
 *
 */
public class Covtpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private Integer planSuffix = new Integer(0);
	private String validflag;
	private Integer tranno;
	private Integer currfrom;
	private Integer currto;
	private String statcode;
	private String pstatcode;
	private String statreasn;
	private Integer crrcd;
	private Integer anbAtCcd;
	private Integer anbccd;
	private String sex;
	private String reptcd01;
	private String reptcd02;
	private String reptcd03;
	private String reptcd04;
	private String reptcd05;
	private String reptcd06;
	private BigDecimal crinst01;
	private BigDecimal crinst02;
	private BigDecimal crinst03;
	private BigDecimal crinst04;
	private BigDecimal crinst05;
	private String prmcur;
	private BigDecimal crInstamt01;
	private BigDecimal crInstamt02;
	private BigDecimal crInstamt03;
	private BigDecimal crInstamt04;
	private BigDecimal crInstamt05;
	private String premCurrency;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private String stfund;
	private String stsect;
	private String stssect;
	private Integer transactionDate;
	private Integer transactionTime;
	private Integer user;
	private String statFund;
	private String statSect;
	private String statSubsect;
	private String crtable;
	private Integer riskCessDate;
	private Integer premCessDate;
	private Integer rcesdte;
	private Integer pcesdte;
	private Integer bcesdte;
	private Integer nxtdte;
	private Integer rcesage;
	private Integer pcesage;
	private Integer bcesage;
	private Integer rcestrm;
	private Integer pcestrm;
	private Integer bcestrm;
	private Integer pcesDte;
	private Integer rcesDte;
	private Integer bcesDte;
	private Integer nxDte;
	private Integer rcesTrm;
	private Integer pcesTrm;
	private Integer bcesTrm;
	private Integer benCessDate;
	private Integer nextActDate;
	private Integer riskCessAge;
	private Integer premCessAge;
	private Integer benCessAge;
	private Integer riskCessTerm;
	private Integer premCessTerm;
	private Integer benCessTerm;
	private BigDecimal sumins;
	private String sicurr;
	private BigDecimal varsi;
	private BigDecimal varSumInsured;
	private String mortcls;
	private String liencd;
	private String ratingClass;
	private String indexationInd;
	private String bonusInd;
	private String deferPerdCode;
	private BigDecimal deferPerdAmt;
	private String deferPerdInd;
	private BigDecimal totMthlyBenefit;
	private BigDecimal estMatValue01;
	private BigDecimal estMatValue02;
	private Integer estMatDate01;
	private Integer estMatDate02;
	private BigDecimal estMatInt01;
	private BigDecimal estMatInt02;
	private String ratcls;
	private String indxin;
	private String bnusin;
	private String dpcd;
	private BigDecimal dpamt;
	private String dpind;
	private BigDecimal tmben;
	private BigDecimal emv01;
	private BigDecimal emv02;
	private Integer emvdte01;
	private Integer emvdte02;
	private BigDecimal emvint01;
	private BigDecimal emvint02;
	private String campaign;
	private BigDecimal statSumins;
	private Integer rtrnyrs;
	private String reserveUnitsInd;
	private Integer reserveUnitsDate;
	private String chargeOptionsInd;
	private String fundSplitPlan;
	private Integer premCessAgeMth;
	private Integer premCessAgeDay;
	private Integer premCessTermMth;
	private Integer premCessTermDay;
	private Integer riskCessAgeMth;
	private Integer riskCessAgeDay;
	private Integer riskCessTermMth;
	private Integer riskCessTermDay;
	private String jlLsInd;
	private BigDecimal stsmin;
	private String rsunin;
	private Integer rundte;
	private String chgopt;
	private String fundsp;
	private Integer pcamth;
	private Integer pcaday;
	private Integer pctmth;
	private Integer pctday;
	private Integer rcamth;
	private Integer rcaday;
	private Integer rctmth;
	private Integer rctday;
	private String jllsid;
	private BigDecimal instprem;
	private BigDecimal singp;
	private Integer rerateDate;
	private Integer rerateFromDate;
	private Integer benBillDate;
	private Integer annivProcDate;
	private Integer convertInitialUnits;
	private Integer reviewProcessing;
	private Integer unitStatementDate;
	private Integer cpiDate;
	private Integer initUnitCancDate;
	private Integer extraAllocDate;
	private Integer initUnitIncrsDate;
	private BigDecimal coverageDebt;
	private Integer rrtdat;
	private Integer rrtfrm;
	private Integer bbldat;
	private Integer cbanpr;
	private Integer cbcvin;
	private Integer cbrvpr;
	private Integer cbunst;
	private Integer cpidte;
	private Integer icandt;
	private Integer exaldt;
	private Integer iincdt;
	private BigDecimal crdebt;
	private Integer payrseqno;
	private String bappmeth;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private String userProfile;
	private String jobName;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private BigDecimal loadper;
	private BigDecimal rateadj;
	private BigDecimal fltmort;
	private BigDecimal premadj;
	private BigDecimal ageadj;
	private BigDecimal zstpduty01;
	private String zclstate;
	private String gmib;
	private String gmdb;
	private String gmwb;
	private BigDecimal riskprem;
	private String lnkgno;
	
	/**
	 * @return the uniqueNumber
	 */
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	/**
	 * @param uniqueNumber the uniqueNumber to set
	 */
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}
	/**
	 * @param chdrcoy the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}
	/**
	 * @param chdrnum the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	/**
	 * @return the life
	 */
	public String getLife() {
		return life;
	}
	/**
	 * @param life the life to set
	 */
	public void setLife(String life) {
		this.life = life;
	}
	/**
	 * @return the jlife
	 */
	public String getJlife() {
		return jlife;
	}
	/**
	 * @param jlife the jlife to set
	 */
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return coverage;
	}
	/**
	 * @param coverage the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	/**
	 * @return the rider
	 */
	public String getRider() {
		return rider;
	}
	/**
	 * @param rider the rider to set
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}
	/**
	 * @return the plnsfx
	 */
	public Integer getPlnsfx() {
		return plnsfx;
	}
	/**
	 * @param plnsfx the plnsfx to set
	 */
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	/**
	 * @return the planSuffix
	 */
	public Integer getPlanSuffix() {
		return planSuffix;
	}
	/**
	 * @param planSuffix the planSuffix to set
	 */
	public void setPlanSuffix(Integer planSuffix) {
		this.planSuffix = planSuffix;
	}
	/**
	 * @return the validflag
	 */
	public String getValidflag() {
		return validflag;
	}
	/**
	 * @param validflag the validflag to set
	 */
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	/**
	 * @return the tranno
	 */
	public Integer getTranno() {
		return tranno;
	}
	/**
	 * @param tranno the tranno to set
	 */
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	/**
	 * @return the currfrom
	 */
	public Integer getCurrfrom() {
		return currfrom;
	}
	/**
	 * @param currfrom the currfrom to set
	 */
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	/**
	 * @return the currto
	 */
	public Integer getCurrto() {
		return currto;
	}
	/**
	 * @param currto the currto to set
	 */
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}
	/**
	 * @return the statcode
	 */
	public String getStatcode() {
		return statcode;
	}
	/**
	 * @param statcode the statcode to set
	 */
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	/**
	 * @return the pstatcode
	 */
	public String getPstatcode() {
		return pstatcode;
	}
	/**
	 * @param pstatcode the pstatcode to set
	 */
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	/**
	 * @return the statreasn
	 */
	public String getStatreasn() {
		return statreasn;
	}
	/**
	 * @param statreasn the statreasn to set
	 */
	public void setStatreasn(String statreasn) {
		this.statreasn = statreasn;
	}
	/**
	 * @return the crrcd
	 */
	public Integer getCrrcd() {
		return crrcd;
	}
	/**
	 * @param crrcd the crrcd to set
	 */
	public void setCrrcd(Integer crrcd) {
		this.crrcd = crrcd;
	}
	/**
	 * @return the anbAtCcd
	 */
	public Integer getAnbAtCcd() {
		return anbAtCcd;
	}
	/**
	 * @param anbAtCcd the anbAtCcd to set
	 */
	public void setAnbAtCcd(Integer anbAtCcd) {
		this.anbAtCcd = anbAtCcd;
	}
	/**
	 * @return the anbccd
	 */
	public Integer getAnbccd() {
		return anbccd;
	}
	/**
	 * @param anbccd the anbccd to set
	 */
	public void setAnbccd(Integer anbccd) {
		this.anbccd = anbccd;
	}
	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * @return the reptcd01
	 */
	public String getReptcd01() {
		return reptcd01;
	}
	/**
	 * @param reptcd01 the reptcd01 to set
	 */
	public void setReptcd01(String reptcd01) {
		this.reptcd01 = reptcd01;
	}
	/**
	 * @return the reptcd02
	 */
	public String getReptcd02() {
		return reptcd02;
	}
	/**
	 * @param reptcd02 the reptcd02 to set
	 */
	public void setReptcd02(String reptcd02) {
		this.reptcd02 = reptcd02;
	}
	/**
	 * @return the reptcd03
	 */
	public String getReptcd03() {
		return reptcd03;
	}
	/**
	 * @param reptcd03 the reptcd03 to set
	 */
	public void setReptcd03(String reptcd03) {
		this.reptcd03 = reptcd03;
	}
	/**
	 * @return the reptcd04
	 */
	public String getReptcd04() {
		return reptcd04;
	}
	/**
	 * @param reptcd04 the reptcd04 to set
	 */
	public void setReptcd04(String reptcd04) {
		this.reptcd04 = reptcd04;
	}
	/**
	 * @return the reptcd05
	 */
	public String getReptcd05() {
		return reptcd05;
	}
	/**
	 * @param reptcd05 the reptcd05 to set
	 */
	public void setReptcd05(String reptcd05) {
		this.reptcd05 = reptcd05;
	}
	/**
	 * @return the reptcd06
	 */
	public String getReptcd06() {
		return reptcd06;
	}
	/**
	 * @param reptcd06 the reptcd06 to set
	 */
	public void setReptcd06(String reptcd06) {
		this.reptcd06 = reptcd06;
	}
	/**
	 * @return the crinst01
	 */
	public BigDecimal getCrinst01() {
		return crinst01;
	}
	/**
	 * @param crinst01 the crinst01 to set
	 */
	public void setCrinst01(BigDecimal crinst01) {
		this.crinst01 = crinst01;
	}
	/**
	 * @return the crinst02
	 */
	public BigDecimal getCrinst02() {
		return crinst02;
	}
	/**
	 * @param crinst02 the crinst02 to set
	 */
	public void setCrinst02(BigDecimal crinst02) {
		this.crinst02 = crinst02;
	}
	/**
	 * @return the crinst03
	 */
	public BigDecimal getCrinst03() {
		return crinst03;
	}
	/**
	 * @param crinst03 the crinst03 to set
	 */
	public void setCrinst03(BigDecimal crinst03) {
		this.crinst03 = crinst03;
	}
	/**
	 * @return the crinst04
	 */
	public BigDecimal getCrinst04() {
		return crinst04;
	}
	/**
	 * @param crinst04 the crinst04 to set
	 */
	public void setCrinst04(BigDecimal crinst04) {
		this.crinst04 = crinst04;
	}
	/**
	 * @return the crinst05
	 */
	public BigDecimal getCrinst05() {
		return crinst05;
	}
	/**
	 * @param crinst05 the crinst05 to set
	 */
	public void setCrinst05(BigDecimal crinst05) {
		this.crinst05 = crinst05;
	}
	/**
	 * @return the prmcur
	 */
	public String getPrmcur() {
		return prmcur;
	}
	/**
	 * @param prmcur the prmcur to set
	 */
	public void setPrmcur(String prmcur) {
		this.prmcur = prmcur;
	}
	/**
	 * @return the crInstamt01
	 */
	public BigDecimal getCrInstamt01() {
		return crInstamt01;
	}
	/**
	 * @param crInstamt01 the crInstamt01 to set
	 */
	public void setCrInstamt01(BigDecimal crInstamt01) {
		this.crInstamt01 = crInstamt01;
	}
	/**
	 * @return the crInstamt02
	 */
	public BigDecimal getCrInstamt02() {
		return crInstamt02;
	}
	/**
	 * @param crInstamt02 the crInstamt02 to set
	 */
	public void setCrInstamt02(BigDecimal crInstamt02) {
		this.crInstamt02 = crInstamt02;
	}
	/**
	 * @return the crInstamt03
	 */
	public BigDecimal getCrInstamt03() {
		return crInstamt03;
	}
	/**
	 * @param crInstamt03 the crInstamt03 to set
	 */
	public void setCrInstamt03(BigDecimal crInstamt03) {
		this.crInstamt03 = crInstamt03;
	}
	/**
	 * @return the crInstamt04
	 */
	public BigDecimal getCrInstamt04() {
		return crInstamt04;
	}
	/**
	 * @param crInstamt04 the crInstamt04 to set
	 */
	public void setCrInstamt04(BigDecimal crInstamt04) {
		this.crInstamt04 = crInstamt04;
	}
	/**
	 * @return the crInstamt05
	 */
	public BigDecimal getCrInstamt05() {
		return crInstamt05;
	}
	/**
	 * @param crInstamt05 the crInstamt05 to set
	 */
	public void setCrInstamt05(BigDecimal crInstamt05) {
		this.crInstamt05 = crInstamt05;
	}
	/**
	 * @return the premCurrency
	 */
	public String getPremCurrency() {
		return premCurrency;
	}
	/**
	 * @param premCurrency the premCurrency to set
	 */
	public void setPremCurrency(String premCurrency) {
		this.premCurrency = premCurrency;
	}
	/**
	 * @return the termid
	 */
	public String getTermid() {
		return termid;
	}
	/**
	 * @param termid the termid to set
	 */
	public void setTermid(String termid) {
		this.termid = termid;
	}
	/**
	 * @return the trdt
	 */
	public Integer getTrdt() {
		return trdt;
	}
	/**
	 * @param trdt the trdt to set
	 */
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	/**
	 * @return the trtm
	 */
	public Integer getTrtm() {
		return trtm;
	}
	/**
	 * @param trtm the trtm to set
	 */
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	/**
	 * @return the userT
	 */
	public Integer getUserT() {
		return userT;
	}
	/**
	 * @param userT the userT to set
	 */
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	/**
	 * @return the stfund
	 */
	public String getStfund() {
		return stfund;
	}
	/**
	 * @param stfund the stfund to set
	 */
	public void setStfund(String stfund) {
		this.stfund = stfund;
	}
	/**
	 * @return the stsect
	 */
	public String getStsect() {
		return stsect;
	}
	/**
	 * @param stsect the stsect to set
	 */
	public void setStsect(String stsect) {
		this.stsect = stsect;
	}
	/**
	 * @return the stssect
	 */
	public String getStssect() {
		return stssect;
	}
	/**
	 * @param stssect the stssect to set
	 */
	public void setStssect(String stssect) {
		this.stssect = stssect;
	}
	/**
	 * @return the transactionDate
	 */
	public Integer getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Integer transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the transactionTime
	 */
	public Integer getTransactionTime() {
		return transactionTime;
	}
	/**
	 * @param transactionTime the transactionTime to set
	 */
	public void setTransactionTime(Integer transactionTime) {
		this.transactionTime = transactionTime;
	}
	/**
	 * @return the user
	 */
	public Integer getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(Integer user) {
		this.user = user;
	}
	/**
	 * @return the statFund
	 */
	public String getStatFund() {
		return statFund;
	}
	/**
	 * @param statFund the statFund to set
	 */
	public void setStatFund(String statFund) {
		this.statFund = statFund;
	}
	/**
	 * @return the statSect
	 */
	public String getStatSect() {
		return statSect;
	}
	/**
	 * @param statSect the statSect to set
	 */
	public void setStatSect(String statSect) {
		this.statSect = statSect;
	}
	/**
	 * @return the statSubsect
	 */
	public String getStatSubsect() {
		return statSubsect;
	}
	/**
	 * @param statSubsect the statSubsect to set
	 */
	public void setStatSubsect(String statSubsect) {
		this.statSubsect = statSubsect;
	}
	/**
	 * @return the crtable
	 */
	public String getCrtable() {
		return crtable;
	}
	/**
	 * @param crtable the crtable to set
	 */
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	/**
	 * @return the riskCessDate
	 */
	public Integer getRiskCessDate() {
		return riskCessDate;
	}
	/**
	 * @param riskCessDate the riskCessDate to set
	 */
	public void setRiskCessDate(Integer riskCessDate) {
		this.riskCessDate = riskCessDate;
	}
	/**
	 * @return the premCessDate
	 */
	public Integer getPremCessDate() {
		return premCessDate;
	}
	/**
	 * @param premCessDate the premCessDate to set
	 */
	public void setPremCessDate(Integer premCessDate) {
		this.premCessDate = premCessDate;
	}
	/**
	 * @return the rcesdte
	 */
	public Integer getRcesdte() {
		return rcesdte;
	}
	/**
	 * @param rcesdte the rcesdte to set
	 */
	public void setRcesdte(Integer rcesdte) {
		this.rcesdte = rcesdte;
	}
	/**
	 * @return the pcesdte
	 */
	public Integer getPcesdte() {
		return pcesdte;
	}
	/**
	 * @param pcesdte the pcesdte to set
	 */
	public void setPcesdte(Integer pcesdte) {
		this.pcesdte = pcesdte;
	}
	/**
	 * @return the bcesdte
	 */
	public Integer getBcesdte() {
		return bcesdte;
	}
	/**
	 * @param bcesdte the bcesdte to set
	 */
	public void setBcesdte(Integer bcesdte) {
		this.bcesdte = bcesdte;
	}
	/**
	 * @return the nxtdte
	 */
	public Integer getNxtdte() {
		return nxtdte;
	}
	/**
	 * @param nxtdte the nxtdte to set
	 */
	public void setNxtdte(Integer nxtdte) {
		this.nxtdte = nxtdte;
	}
	/**
	 * @return the rcesage
	 */
	public Integer getRcesage() {
		return rcesage;
	}
	/**
	 * @param rcesage the rcesage to set
	 */
	public void setRcesage(Integer rcesage) {
		this.rcesage = rcesage;
	}
	/**
	 * @return the pcesage
	 */
	public Integer getPcesage() {
		return pcesage;
	}
	/**
	 * @param pcesage the pcesage to set
	 */
	public void setPcesage(Integer pcesage) {
		this.pcesage = pcesage;
	}
	/**
	 * @return the bcesage
	 */
	public Integer getBcesage() {
		return bcesage;
	}
	/**
	 * @param bcesage the bcesage to set
	 */
	public void setBcesage(Integer bcesage) {
		this.bcesage = bcesage;
	}
	/**
	 * @return the rcestrm
	 */
	public Integer getRcestrm() {
		return rcestrm;
	}
	/**
	 * @param rcestrm the rcestrm to set
	 */
	public void setRcestrm(Integer rcestrm) {
		this.rcestrm = rcestrm;
	}
	/**
	 * @return the pcestrm
	 */
	public Integer getPcestrm() {
		return pcestrm;
	}
	/**
	 * @param pcestrm the pcestrm to set
	 */
	public void setPcestrm(Integer pcestrm) {
		this.pcestrm = pcestrm;
	}
	/**
	 * @return the bcestrm
	 */
	public Integer getBcestrm() {
		return bcestrm;
	}
	/**
	 * @param bcestrm the bcestrm to set
	 */
	public void setBcestrm(Integer bcestrm) {
		this.bcestrm = bcestrm;
	}
	/**
	 * @return the pcesDte
	 */
	public Integer getPcesDte() {
		return pcesDte;
	}
	/**
	 * @param pcesDte the pcesDte to set
	 */
	public void setPcesDte(Integer pcesDte) {
		this.pcesDte = pcesDte;
	}
	/**
	 * @return the rcesDte
	 */
	public Integer getRcesDte() {
		return rcesDte;
	}
	/**
	 * @param rcesDte the rcesDte to set
	 */
	public void setRcesDte(Integer rcesDte) {
		this.rcesDte = rcesDte;
	}
	/**
	 * @return the bcesDte
	 */
	public Integer getBcesDte() {
		return bcesDte;
	}
	/**
	 * @param bcesDte the bcesDte to set
	 */
	public void setBcesDte(Integer bcesDte) {
		this.bcesDte = bcesDte;
	}
	/**
	 * @return the nxDte
	 */
	public Integer getNxDte() {
		return nxDte;
	}
	/**
	 * @param nxDte the nxDte to set
	 */
	public void setNxDte(Integer nxDte) {
		this.nxDte = nxDte;
	}
	/**
	 * @return the rcesTrm
	 */
	public Integer getRcesTrm() {
		return rcesTrm;
	}
	/**
	 * @param rcesTrm the rcesTrm to set
	 */
	public void setRcesTrm(Integer rcesTrm) {
		this.rcesTrm = rcesTrm;
	}
	/**
	 * @return the pcesTrm
	 */
	public Integer getPcesTrm() {
		return pcesTrm;
	}
	/**
	 * @param pcesTrm the pcesTrm to set
	 */
	public void setPcesTrm(Integer pcesTrm) {
		this.pcesTrm = pcesTrm;
	}
	/**
	 * @return the bcesTrm
	 */
	public Integer getBcesTrm() {
		return bcesTrm;
	}
	/**
	 * @param bcesTrm the bcesTrm to set
	 */
	public void setBcesTrm(Integer bcesTrm) {
		this.bcesTrm = bcesTrm;
	}
	/**
	 * @return the benCessDate
	 */
	public Integer getBenCessDate() {
		return benCessDate;
	}
	/**
	 * @param benCessDate the benCessDate to set
	 */
	public void setBenCessDate(Integer benCessDate) {
		this.benCessDate = benCessDate;
	}
	/**
	 * @return the nextActDate
	 */
	public Integer getNextActDate() {
		return nextActDate;
	}
	/**
	 * @param nextActDate the nextActDate to set
	 */
	public void setNextActDate(Integer nextActDate) {
		this.nextActDate = nextActDate;
	}
	/**
	 * @return the riskCessAge
	 */
	public Integer getRiskCessAge() {
		return riskCessAge;
	}
	/**
	 * @param riskCessAge the riskCessAge to set
	 */
	public void setRiskCessAge(Integer riskCessAge) {
		this.riskCessAge = riskCessAge;
	}
	/**
	 * @return the premCessAge
	 */
	public Integer getPremCessAge() {
		return premCessAge;
	}
	/**
	 * @param premCessAge the premCessAge to set
	 */
	public void setPremCessAge(Integer premCessAge) {
		this.premCessAge = premCessAge;
	}
	/**
	 * @return the benCessAge
	 */
	public Integer getBenCessAge() {
		return benCessAge;
	}
	/**
	 * @param benCessAge the benCessAge to set
	 */
	public void setBenCessAge(Integer benCessAge) {
		this.benCessAge = benCessAge;
	}
	/**
	 * @return the riskCessTerm
	 */
	public Integer getRiskCessTerm() {
		return riskCessTerm;
	}
	/**
	 * @param riskCessTerm the riskCessTerm to set
	 */
	public void setRiskCessTerm(Integer riskCessTerm) {
		this.riskCessTerm = riskCessTerm;
	}
	/**
	 * @return the premCessTerm
	 */
	public Integer getPremCessTerm() {
		return premCessTerm;
	}
	/**
	 * @param premCessTerm the premCessTerm to set
	 */
	public void setPremCessTerm(Integer premCessTerm) {
		this.premCessTerm = premCessTerm;
	}
	/**
	 * @return the benCessTerm
	 */
	public Integer getBenCessTerm() {
		return benCessTerm;
	}
	/**
	 * @param benCessTerm the benCessTerm to set
	 */
	public void setBenCessTerm(Integer benCessTerm) {
		this.benCessTerm = benCessTerm;
	}
	/**
	 * @return the sumins
	 */
	public BigDecimal getSumins() {
		return sumins;
	}
	/**
	 * @param sumins the sumins to set
	 */
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	/**
	 * @return the sicurr
	 */
	public String getSicurr() {
		return sicurr;
	}
	/**
	 * @param sicurr the sicurr to set
	 */
	public void setSicurr(String sicurr) {
		this.sicurr = sicurr;
	}
	/**
	 * @return the varsi
	 */
	public BigDecimal getVarsi() {
		return varsi;
	}
	/**
	 * @param varsi the varsi to set
	 */
	public void setVarsi(BigDecimal varsi) {
		this.varsi = varsi;
	}
	/**
	 * @return the varSumInsured
	 */
	public BigDecimal getVarSumInsured() {
		return varSumInsured;
	}
	/**
	 * @param varSumInsured the varSumInsured to set
	 */
	public void setVarSumInsured(BigDecimal varSumInsured) {
		this.varSumInsured = varSumInsured;
	}
	/**
	 * @return the mortcls
	 */
	public String getMortcls() {
		return mortcls;
	}
	/**
	 * @param mortcls the mortcls to set
	 */
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	/**
	 * @return the liencd
	 */
	public String getLiencd() {
		return liencd;
	}
	/**
	 * @param liencd the liencd to set
	 */
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	/**
	 * @return the ratingClass
	 */
	public String getRatingClass() {
		return ratingClass;
	}
	/**
	 * @param ratingClass the ratingClass to set
	 */
	public void setRatingClass(String ratingClass) {
		this.ratingClass = ratingClass;
	}
	/**
	 * @return the indexationInd
	 */
	public String getIndexationInd() {
		return indexationInd;
	}
	/**
	 * @param indexationInd the indexationInd to set
	 */
	public void setIndexationInd(String indexationInd) {
		this.indexationInd = indexationInd;
	}
	/**
	 * @return the bonusInd
	 */
	public String getBonusInd() {
		return bonusInd;
	}
	/**
	 * @param bonusInd the bonusInd to set
	 */
	public void setBonusInd(String bonusInd) {
		this.bonusInd = bonusInd;
	}
	/**
	 * @return the deferPerdCode
	 */
	public String getDeferPerdCode() {
		return deferPerdCode;
	}
	/**
	 * @param deferPerdCode the deferPerdCode to set
	 */
	public void setDeferPerdCode(String deferPerdCode) {
		this.deferPerdCode = deferPerdCode;
	}
	/**
	 * @return the deferPerdAmt
	 */
	public BigDecimal getDeferPerdAmt() {
		return deferPerdAmt;
	}
	/**
	 * @param deferPerdAmt the deferPerdAmt to set
	 */
	public void setDeferPerdAmt(BigDecimal deferPerdAmt) {
		this.deferPerdAmt = deferPerdAmt;
	}
	/**
	 * @return the deferPerdInd
	 */
	public String getDeferPerdInd() {
		return deferPerdInd;
	}
	/**
	 * @param deferPerdInd the deferPerdInd to set
	 */
	public void setDeferPerdInd(String deferPerdInd) {
		this.deferPerdInd = deferPerdInd;
	}
	/**
	 * @return the totMthlyBenefit
	 */
	public BigDecimal getTotMthlyBenefit() {
		return totMthlyBenefit;
	}
	/**
	 * @param totMthlyBenefit the totMthlyBenefit to set
	 */
	public void setTotMthlyBenefit(BigDecimal totMthlyBenefit) {
		this.totMthlyBenefit = totMthlyBenefit;
	}
	/**
	 * @return the estMatValue01
	 */
	public BigDecimal getEstMatValue01() {
		return estMatValue01;
	}
	/**
	 * @param estMatValue01 the estMatValue01 to set
	 */
	public void setEstMatValue01(BigDecimal estMatValue01) {
		this.estMatValue01 = estMatValue01;
	}
	/**
	 * @return the estMatValue02
	 */
	public BigDecimal getEstMatValue02() {
		return estMatValue02;
	}
	/**
	 * @param estMatValue02 the estMatValue02 to set
	 */
	public void setEstMatValue02(BigDecimal estMatValue02) {
		this.estMatValue02 = estMatValue02;
	}
	/**
	 * @return the estMatDate01
	 */
	public Integer getEstMatDate01() {
		return estMatDate01;
	}
	/**
	 * @param estMatDate01 the estMatDate01 to set
	 */
	public void setEstMatDate01(Integer estMatDate01) {
		this.estMatDate01 = estMatDate01;
	}
	/**
	 * @return the estMatDate02
	 */
	public Integer getEstMatDate02() {
		return estMatDate02;
	}
	/**
	 * @param estMatDate02 the estMatDate02 to set
	 */
	public void setEstMatDate02(Integer estMatDate02) {
		this.estMatDate02 = estMatDate02;
	}
	/**
	 * @return the estMatInt01
	 */
	public BigDecimal getEstMatInt01() {
		return estMatInt01;
	}
	/**
	 * @param estMatInt01 the estMatInt01 to set
	 */
	public void setEstMatInt01(BigDecimal estMatInt01) {
		this.estMatInt01 = estMatInt01;
	}
	/**
	 * @return the estMatInt02
	 */
	public BigDecimal getEstMatInt02() {
		return estMatInt02;
	}
	/**
	 * @param estMatInt02 the estMatInt02 to set
	 */
	public void setEstMatInt02(BigDecimal estMatInt02) {
		this.estMatInt02 = estMatInt02;
	}
	/**
	 * @return the ratcls
	 */
	public String getRatcls() {
		return ratcls;
	}
	/**
	 * @param ratcls the ratcls to set
	 */
	public void setRatcls(String ratcls) {
		this.ratcls = ratcls;
	}
	/**
	 * @return the indxin
	 */
	public String getIndxin() {
		return indxin;
	}
	/**
	 * @param indxin the indxin to set
	 */
	public void setIndxin(String indxin) {
		this.indxin = indxin;
	}
	/**
	 * @return the bnusin
	 */
	public String getBnusin() {
		return bnusin;
	}
	/**
	 * @param bnusin the bnusin to set
	 */
	public void setBnusin(String bnusin) {
		this.bnusin = bnusin;
	}
	/**
	 * @return the dpcd
	 */
	public String getDpcd() {
		return dpcd;
	}
	/**
	 * @param dpcd the dpcd to set
	 */
	public void setDpcd(String dpcd) {
		this.dpcd = dpcd;
	}
	/**
	 * @return the dpamt
	 */
	public BigDecimal getDpamt() {
		return dpamt;
	}
	/**
	 * @param dpamt the dpamt to set
	 */
	public void setDpamt(BigDecimal dpamt) {
		this.dpamt = dpamt;
	}
	/**
	 * @return the dpind
	 */
	public String getDpind() {
		return dpind;
	}
	/**
	 * @param dpind the dpind to set
	 */
	public void setDpind(String dpind) {
		this.dpind = dpind;
	}
	/**
	 * @return the tmben
	 */
	public BigDecimal getTmben() {
		return tmben;
	}
	/**
	 * @param tmben the tmben to set
	 */
	public void setTmben(BigDecimal tmben) {
		this.tmben = tmben;
	}
	/**
	 * @return the emv01
	 */
	public BigDecimal getEmv01() {
		return emv01;
	}
	/**
	 * @param emv01 the emv01 to set
	 */
	public void setEmv01(BigDecimal emv01) {
		this.emv01 = emv01;
	}
	/**
	 * @return the emv02
	 */
	public BigDecimal getEmv02() {
		return emv02;
	}
	/**
	 * @param emv02 the emv02 to set
	 */
	public void setEmv02(BigDecimal emv02) {
		this.emv02 = emv02;
	}
	/**
	 * @return the emvdte01
	 */
	public Integer getEmvdte01() {
		return emvdte01;
	}
	/**
	 * @param emvdte01 the emvdte01 to set
	 */
	public void setEmvdte01(Integer emvdte01) {
		this.emvdte01 = emvdte01;
	}
	/**
	 * @return the emvdte02
	 */
	public Integer getEmvdte02() {
		return emvdte02;
	}
	/**
	 * @param emvdte02 the emvdte02 to set
	 */
	public void setEmvdte02(Integer emvdte02) {
		this.emvdte02 = emvdte02;
	}
	/**
	 * @return the emvint01
	 */
	public BigDecimal getEmvint01() {
		return emvint01;
	}
	/**
	 * @param emvint01 the emvint01 to set
	 */
	public void setEmvint01(BigDecimal emvint01) {
		this.emvint01 = emvint01;
	}
	/**
	 * @return the emvint02
	 */
	public BigDecimal getEmvint02() {
		return emvint02;
	}
	/**
	 * @param emvint02 the emvint02 to set
	 */
	public void setEmvint02(BigDecimal emvint02) {
		this.emvint02 = emvint02;
	}
	/**
	 * @return the campaign
	 */
	public String getCampaign() {
		return campaign;
	}
	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}
	/**
	 * @return the statSumins
	 */
	public BigDecimal getStatSumins() {
		return statSumins;
	}
	/**
	 * @param statSumins the statSumins to set
	 */
	public void setStatSumins(BigDecimal statSumins) {
		this.statSumins = statSumins;
	}
	/**
	 * @return the rtrnyrs
	 */
	public Integer getRtrnyrs() {
		return rtrnyrs;
	}
	/**
	 * @param rtrnyrs the rtrnyrs to set
	 */
	public void setRtrnyrs(Integer rtrnyrs) {
		this.rtrnyrs = rtrnyrs;
	}
	/**
	 * @return the reserveUnitsInd
	 */
	public String getReserveUnitsInd() {
		return reserveUnitsInd;
	}
	/**
	 * @param reserveUnitsInd the reserveUnitsInd to set
	 */
	public void setReserveUnitsInd(String reserveUnitsInd) {
		this.reserveUnitsInd = reserveUnitsInd;
	}
	/**
	 * @return the reserveUnitsDate
	 */
	public Integer getReserveUnitsDate() {
		return reserveUnitsDate;
	}
	/**
	 * @param reserveUnitsDate the reserveUnitsDate to set
	 */
	public void setReserveUnitsDate(Integer reserveUnitsDate) {
		this.reserveUnitsDate = reserveUnitsDate;
	}
	/**
	 * @return the chargeOptionsInd
	 */
	public String getChargeOptionsInd() {
		return chargeOptionsInd;
	}
	/**
	 * @param chargeOptionsInd the chargeOptionsInd to set
	 */
	public void setChargeOptionsInd(String chargeOptionsInd) {
		this.chargeOptionsInd = chargeOptionsInd;
	}
	/**
	 * @return the fundSplitPlan
	 */
	public String getFundSplitPlan() {
		return fundSplitPlan;
	}
	/**
	 * @param fundSplitPlan the fundSplitPlan to set
	 */
	public void setFundSplitPlan(String fundSplitPlan) {
		this.fundSplitPlan = fundSplitPlan;
	}
	/**
	 * @return the premCessAgeMth
	 */
	public Integer getPremCessAgeMth() {
		return premCessAgeMth;
	}
	/**
	 * @param premCessAgeMth the premCessAgeMth to set
	 */
	public void setPremCessAgeMth(Integer premCessAgeMth) {
		this.premCessAgeMth = premCessAgeMth;
	}
	/**
	 * @return the premCessAgeDay
	 */
	public Integer getPremCessAgeDay() {
		return premCessAgeDay;
	}
	/**
	 * @param premCessAgeDay the premCessAgeDay to set
	 */
	public void setPremCessAgeDay(Integer premCessAgeDay) {
		this.premCessAgeDay = premCessAgeDay;
	}
	/**
	 * @return the premCessTermMth
	 */
	public Integer getPremCessTermMth() {
		return premCessTermMth;
	}
	/**
	 * @param premCessTermMth the premCessTermMth to set
	 */
	public void setPremCessTermMth(Integer premCessTermMth) {
		this.premCessTermMth = premCessTermMth;
	}
	/**
	 * @return the premCessTermDay
	 */
	public Integer getPremCessTermDay() {
		return premCessTermDay;
	}
	/**
	 * @param premCessTermDay the premCessTermDay to set
	 */
	public void setPremCessTermDay(Integer premCessTermDay) {
		this.premCessTermDay = premCessTermDay;
	}
	/**
	 * @return the riskCessAgeMth
	 */
	public Integer getRiskCessAgeMth() {
		return riskCessAgeMth;
	}
	/**
	 * @param riskCessAgeMth the riskCessAgeMth to set
	 */
	public void setRiskCessAgeMth(Integer riskCessAgeMth) {
		this.riskCessAgeMth = riskCessAgeMth;
	}
	/**
	 * @return the riskCessAgeDay
	 */
	public Integer getRiskCessAgeDay() {
		return riskCessAgeDay;
	}
	/**
	 * @param riskCessAgeDay the riskCessAgeDay to set
	 */
	public void setRiskCessAgeDay(Integer riskCessAgeDay) {
		this.riskCessAgeDay = riskCessAgeDay;
	}
	/**
	 * @return the riskCessTermMth
	 */
	public Integer getRiskCessTermMth() {
		return riskCessTermMth;
	}
	/**
	 * @param riskCessTermMth the riskCessTermMth to set
	 */
	public void setRiskCessTermMth(Integer riskCessTermMth) {
		this.riskCessTermMth = riskCessTermMth;
	}
	/**
	 * @return the riskCessTermDay
	 */
	public Integer getRiskCessTermDay() {
		return riskCessTermDay;
	}
	/**
	 * @param riskCessTermDay the riskCessTermDay to set
	 */
	public void setRiskCessTermDay(Integer riskCessTermDay) {
		this.riskCessTermDay = riskCessTermDay;
	}
	/**
	 * @return the jlLsInd
	 */
	public String getJlLsInd() {
		return jlLsInd;
	}
	/**
	 * @param jlLsInd the jlLsInd to set
	 */
	public void setJlLsInd(String jlLsInd) {
		this.jlLsInd = jlLsInd;
	}
	/**
	 * @return the stsmin
	 */
	public BigDecimal getStsmin() {
		return stsmin;
	}
	/**
	 * @param stsmin the stsmin to set
	 */
	public void setStsmin(BigDecimal stsmin) {
		this.stsmin = stsmin;
	}
	/**
	 * @return the rsunin
	 */
	public String getRsunin() {
		return rsunin;
	}
	/**
	 * @param rsunin the rsunin to set
	 */
	public void setRsunin(String rsunin) {
		this.rsunin = rsunin;
	}
	/**
	 * @return the rundte
	 */
	public Integer getRundte() {
		return rundte;
	}
	/**
	 * @param rundte the rundte to set
	 */
	public void setRundte(Integer rundte) {
		this.rundte = rundte;
	}
	/**
	 * @return the chgopt
	 */
	public String getChgopt() {
		return chgopt;
	}
	/**
	 * @param chgopt the chgopt to set
	 */
	public void setChgopt(String chgopt) {
		this.chgopt = chgopt;
	}
	/**
	 * @return the fundsp
	 */
	public String getFundsp() {
		return fundsp;
	}
	/**
	 * @param fundsp the fundsp to set
	 */
	public void setFundsp(String fundsp) {
		this.fundsp = fundsp;
	}
	/**
	 * @return the pcamth
	 */
	public Integer getPcamth() {
		return pcamth;
	}
	/**
	 * @param pcamth the pcamth to set
	 */
	public void setPcamth(Integer pcamth) {
		this.pcamth = pcamth;
	}
	/**
	 * @return the pcaday
	 */
	public Integer getPcaday() {
		return pcaday;
	}
	/**
	 * @param pcaday the pcaday to set
	 */
	public void setPcaday(Integer pcaday) {
		this.pcaday = pcaday;
	}
	/**
	 * @return the pctmth
	 */
	public Integer getPctmth() {
		return pctmth;
	}
	/**
	 * @param pctmth the pctmth to set
	 */
	public void setPctmth(Integer pctmth) {
		this.pctmth = pctmth;
	}
	/**
	 * @return the pctday
	 */
	public Integer getPctday() {
		return pctday;
	}
	/**
	 * @param pctday the pctday to set
	 */
	public void setPctday(Integer pctday) {
		this.pctday = pctday;
	}
	/**
	 * @return the rcamth
	 */
	public Integer getRcamth() {
		return rcamth;
	}
	/**
	 * @param rcamth the rcamth to set
	 */
	public void setRcamth(Integer rcamth) {
		this.rcamth = rcamth;
	}
	/**
	 * @return the rcaday
	 */
	public Integer getRcaday() {
		return rcaday;
	}
	/**
	 * @param rcaday the rcaday to set
	 */
	public void setRcaday(Integer rcaday) {
		this.rcaday = rcaday;
	}
	/**
	 * @return the rctmth
	 */
	public Integer getRctmth() {
		return rctmth;
	}
	/**
	 * @param rctmth the rctmth to set
	 */
	public void setRctmth(Integer rctmth) {
		this.rctmth = rctmth;
	}
	/**
	 * @return the rctday
	 */
	public Integer getRctday() {
		return rctday;
	}
	/**
	 * @param rctday the rctday to set
	 */
	public void setRctday(Integer rctday) {
		this.rctday = rctday;
	}
	/**
	 * @return the jllsid
	 */
	public String getJllsid() {
		return jllsid;
	}
	/**
	 * @param jllsid the jllsid to set
	 */
	public void setJllsid(String jllsid) {
		this.jllsid = jllsid;
	}
	/**
	 * @return the instprem
	 */
	public BigDecimal getInstprem() {
		return instprem;
	}
	/**
	 * @param instprem the instprem to set
	 */
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}
	/**
	 * @return the singp
	 */
	public BigDecimal getSingp() {
		return singp;
	}
	/**
	 * @param singp the singp to set
	 */
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}
	/**
	 * @return the rerateDate
	 */
	public Integer getRerateDate() {
		return rerateDate;
	}
	/**
	 * @param rerateDate the rerateDate to set
	 */
	public void setRerateDate(Integer rerateDate) {
		this.rerateDate = rerateDate;
	}
	/**
	 * @return the rerateFromDate
	 */
	public Integer getRerateFromDate() {
		return rerateFromDate;
	}
	/**
	 * @param rerateFromDate the rerateFromDate to set
	 */
	public void setRerateFromDate(Integer rerateFromDate) {
		this.rerateFromDate = rerateFromDate;
	}
	/**
	 * @return the benBillDate
	 */
	public Integer getBenBillDate() {
		return benBillDate;
	}
	/**
	 * @param benBillDate the benBillDate to set
	 */
	public void setBenBillDate(Integer benBillDate) {
		this.benBillDate = benBillDate;
	}
	/**
	 * @return the annivProcDate
	 */
	public Integer getAnnivProcDate() {
		return annivProcDate;
	}
	/**
	 * @param annivProcDate the annivProcDate to set
	 */
	public void setAnnivProcDate(Integer annivProcDate) {
		this.annivProcDate = annivProcDate;
	}
	/**
	 * @return the convertInitialUnits
	 */
	public Integer getConvertInitialUnits() {
		return convertInitialUnits;
	}
	/**
	 * @param convertInitialUnits the convertInitialUnits to set
	 */
	public void setConvertInitialUnits(Integer convertInitialUnits) {
		this.convertInitialUnits = convertInitialUnits;
	}
	/**
	 * @return the reviewProcessing
	 */
	public Integer getReviewProcessing() {
		return reviewProcessing;
	}
	/**
	 * @param reviewProcessing the reviewProcessing to set
	 */
	public void setReviewProcessing(Integer reviewProcessing) {
		this.reviewProcessing = reviewProcessing;
	}
	/**
	 * @return the unitStatementDate
	 */
	public Integer getUnitStatementDate() {
		return unitStatementDate;
	}
	/**
	 * @param unitStatementDate the unitStatementDate to set
	 */
	public void setUnitStatementDate(Integer unitStatementDate) {
		this.unitStatementDate = unitStatementDate;
	}
	/**
	 * @return the cpiDate
	 */
	public Integer getCpiDate() {
		return cpiDate;
	}
	/**
	 * @param cpiDate the cpiDate to set
	 */
	public void setCpiDate(Integer cpiDate) {
		this.cpiDate = cpiDate;
	}
	/**
	 * @return the initUnitCancDate
	 */
	public Integer getInitUnitCancDate() {
		return initUnitCancDate;
	}
	/**
	 * @param initUnitCancDate the initUnitCancDate to set
	 */
	public void setInitUnitCancDate(Integer initUnitCancDate) {
		this.initUnitCancDate = initUnitCancDate;
	}
	/**
	 * @return the extraAllocDate
	 */
	public Integer getExtraAllocDate() {
		return extraAllocDate;
	}
	/**
	 * @param extraAllocDate the extraAllocDate to set
	 */
	public void setExtraAllocDate(Integer extraAllocDate) {
		this.extraAllocDate = extraAllocDate;
	}
	/**
	 * @return the initUnitIncrsDate
	 */
	public Integer getInitUnitIncrsDate() {
		return initUnitIncrsDate;
	}
	/**
	 * @param initUnitIncrsDate the initUnitIncrsDate to set
	 */
	public void setInitUnitIncrsDate(Integer initUnitIncrsDate) {
		this.initUnitIncrsDate = initUnitIncrsDate;
	}
	/**
	 * @return the coverageDebt
	 */
	public BigDecimal getCoverageDebt() {
		return coverageDebt;
	}
	/**
	 * @param coverageDebt the coverageDebt to set
	 */
	public void setCoverageDebt(BigDecimal coverageDebt) {
		this.coverageDebt = coverageDebt;
	}
	/**
	 * @return the rrtdat
	 */
	public Integer getRrtdat() {
		return rrtdat;
	}
	/**
	 * @param rrtdat the rrtdat to set
	 */
	public void setRrtdat(Integer rrtdat) {
		this.rrtdat = rrtdat;
	}
	/**
	 * @return the rrtfrm
	 */
	public Integer getRrtfrm() {
		return rrtfrm;
	}
	/**
	 * @param rrtfrm the rrtfrm to set
	 */
	public void setRrtfrm(Integer rrtfrm) {
		this.rrtfrm = rrtfrm;
	}
	/**
	 * @return the bbldat
	 */
	public Integer getBbldat() {
		return bbldat;
	}
	/**
	 * @param bbldat the bbldat to set
	 */
	public void setBbldat(Integer bbldat) {
		this.bbldat = bbldat;
	}
	/**
	 * @return the cbanpr
	 */
	public Integer getCbanpr() {
		return cbanpr;
	}
	/**
	 * @param cbanpr the cbanpr to set
	 */
	public void setCbanpr(Integer cbanpr) {
		this.cbanpr = cbanpr;
	}
	/**
	 * @return the cbcvin
	 */
	public Integer getCbcvin() {
		return cbcvin;
	}
	/**
	 * @param cbcvin the cbcvin to set
	 */
	public void setCbcvin(Integer cbcvin) {
		this.cbcvin = cbcvin;
	}
	/**
	 * @return the cbrvpr
	 */
	public Integer getCbrvpr() {
		return cbrvpr;
	}
	/**
	 * @param cbrvpr the cbrvpr to set
	 */
	public void setCbrvpr(Integer cbrvpr) {
		this.cbrvpr = cbrvpr;
	}
	/**
	 * @return the cbunst
	 */
	public Integer getCbunst() {
		return cbunst;
	}
	/**
	 * @param cbunst the cbunst to set
	 */
	public void setCbunst(Integer cbunst) {
		this.cbunst = cbunst;
	}
	/**
	 * @return the cpidte
	 */
	public Integer getCpidte() {
		return cpidte;
	}
	/**
	 * @param cpidte the cpidte to set
	 */
	public void setCpidte(Integer cpidte) {
		this.cpidte = cpidte;
	}
	/**
	 * @return the icandt
	 */
	public Integer getIcandt() {
		return icandt;
	}
	/**
	 * @param icandt the icandt to set
	 */
	public void setIcandt(Integer icandt) {
		this.icandt = icandt;
	}
	/**
	 * @return the exaldt
	 */
	public Integer getExaldt() {
		return exaldt;
	}
	/**
	 * @param exaldt the exaldt to set
	 */
	public void setExaldt(Integer exaldt) {
		this.exaldt = exaldt;
	}
	/**
	 * @return the iincdt
	 */
	public Integer getIincdt() {
		return iincdt;
	}
	/**
	 * @param iincdt the iincdt to set
	 */
	public void setIincdt(Integer iincdt) {
		this.iincdt = iincdt;
	}
	/**
	 * @return the crdebt
	 */
	public BigDecimal getCrdebt() {
		return crdebt;
	}
	/**
	 * @param crdebt the crdebt to set
	 */
	public void setCrdebt(BigDecimal crdebt) {
		this.crdebt = crdebt;
	}
	/**
	 * @return the payrseqno
	 */
	public Integer getPayrseqno() {
		return payrseqno;
	}
	/**
	 * @param payrseqno the payrseqno to set
	 */
	public void setPayrseqno(Integer payrseqno) {
		this.payrseqno = payrseqno;
	}
	/**
	 * @return the bappmeth
	 */
	public String getBappmeth() {
		return bappmeth;
	}
	/**
	 * @param bappmeth the bappmeth to set
	 */
	public void setBappmeth(String bappmeth) {
		this.bappmeth = bappmeth;
	}
	/**
	 * @return the zbinstprem
	 */
	public BigDecimal getZbinstprem() {
		return zbinstprem;
	}
	/**
	 * @param zbinstprem the zbinstprem to set
	 */
	public void setZbinstprem(BigDecimal zbinstprem) {
		this.zbinstprem = zbinstprem;
	}
	/**
	 * @return the zlinstprem
	 */
	public BigDecimal getZlinstprem() {
		return zlinstprem;
	}
	/**
	 * @param zlinstprem the zlinstprem to set
	 */
	public void setZlinstprem(BigDecimal zlinstprem) {
		this.zlinstprem = zlinstprem;
	}
	/**
	 * @return the userProfile
	 */
	public String getUserProfile() {
		return userProfile;
	}
	/**
	 * @param userProfile the userProfile to set
	 */
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}
	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	/**
	 * @return the usrprf
	 */
	public String getUsrprf() {
		return usrprf;
	}
	/**
	 * @param usrprf the usrprf to set
	 */
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	/**
	 * @return the jobnm
	 */
	public String getJobnm() {
		return jobnm;
	}
	/**
	 * @param jobnm the jobnm to set
	 */
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	/**
	 * @return the datime
	 */
	public Timestamp getDatime() {
		return datime;
	}
	/**
	 * @param datime the datime to set
	 */
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	/**
	 * @return the loadper
	 */
	public BigDecimal getLoadper() {
		return loadper;
	}
	/**
	 * @param loadper the loadper to set
	 */
	public void setLoadper(BigDecimal loadper) {
		this.loadper = loadper;
	}
	/**
	 * @return the rateadj
	 */
	public BigDecimal getRateadj() {
		return rateadj;
	}
	/**
	 * @param rateadj the rateadj to set
	 */
	public void setRateadj(BigDecimal rateadj) {
		this.rateadj = rateadj;
	}
	/**
	 * @return the fltmort
	 */
	public BigDecimal getFltmort() {
		return fltmort;
	}
	/**
	 * @param fltmort the fltmort to set
	 */
	public void setFltmort(BigDecimal fltmort) {
		this.fltmort = fltmort;
	}
	/**
	 * @return the premadj
	 */
	public BigDecimal getPremadj() {
		return premadj;
	}
	/**
	 * @param premadj the premadj to set
	 */
	public void setPremadj(BigDecimal premadj) {
		this.premadj = premadj;
	}
	/**
	 * @return the ageadj
	 */
	public BigDecimal getAgeadj() {
		return ageadj;
	}
	/**
	 * @param ageadj the ageadj to set
	 */
	public void setAgeadj(BigDecimal ageadj) {
		this.ageadj = ageadj;
	}
	/**
	 * @return the zstpduty01
	 */
	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	/**
	 * @param zstpduty01 the zstpduty01 to set
	 */
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}
	/**
	 * @return the zclstate
	 */
	public String getZclstate() {
		return zclstate;
	}
	/**
	 * @param zclstate the zclstate to set
	 */
	public void setZclstate(String zclstate) {
		this.zclstate = zclstate;
	}
	/**
	 * @return the gmib
	 */
	public String getGmib() {
		return gmib;
	}
	/**
	 * @param gmib the gmib to set
	 */
	public void setGmib(String gmib) {
		this.gmib = gmib;
	}
	/**
	 * @return the gmdb
	 */
	public String getGmdb() {
		return gmdb;
	}
	/**
	 * @param gmdb the gmdb to set
	 */
	public void setGmdb(String gmdb) {
		this.gmdb = gmdb;
	}
	/**
	 * @return the gmwb
	 */
	public String getGmwb() {
		return gmwb;
	}
	/**
	 * @param gmwb the gmwb to set
	 */
	public void setGmwb(String gmwb) {
		this.gmwb = gmwb;
	}
	/**
	 * @return the riskprem
	 */
	public BigDecimal getRiskprem() {
		return riskprem;
	}
	/**
	 * @param riskprem the riskprem to set
	 */
	public void setRiskprem(BigDecimal riskprem) {
		this.riskprem = riskprem;
	}
	/**
	 * @return the lnkgno
	 */
	public String getLnkgno() {
		return lnkgno;
	}
	/**
	 * @param lnkgno the lnkgno to set
	 */
	public void setLnkgno(String lnkgno) {
		this.lnkgno = lnkgno;
	}
}
