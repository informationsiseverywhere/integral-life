/*********************  */
/*Author  :Liwei				  		*/
/*Purpose :Model for subroutine Batcup*/
/*Date    :2018.9.26				*/
package com.dxc.integral.life.beans;

public class BatcupDTO {
	private String function;
  	private String batchkey;
  	private String contkey;
  	private String batcpfx;
  	private String batccoy;
  	private String batcbrn;
  	private int batcactyr;
  	private int batcactmn;
  	private String batctrcde;
  	private String batcbatch;
  	private int trancnt;
  	private int etreqcnt;
  	private int sub;
  	private int bcnt;
  	private int bval;
  	private int ascnt;
  	private String statuz;
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getBatchkey() {
		return batchkey;
	}
	public void setBatchkey(String batchkey) {
		this.batchkey = batchkey;
	}
	public String getContkey() {
		return contkey;
	}
	public void setContkey(String contkey) {
		this.contkey = contkey;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	
	public int getTrancnt() {
		return trancnt;
	}
	public void setTrancnt(int trancnt) {
		this.trancnt = trancnt;
	}
	public int getEtreqcnt() {
		return etreqcnt;
	}
	public void setEtreqcnt(int etreqcnt) {
		this.etreqcnt = etreqcnt;
	}
	public int getSub() {
		return sub;
	}
	public void setSub(int sub) {
		this.sub = sub;
	}
	public int getBcnt() {
		return bcnt;
	}
	public void setBcnt(int bcnt) {
		this.bcnt = bcnt;
	}
	public int getBval() {
		return bval;
	}
	public void setBval(int bval) {
		this.bval = bval;
	}
	public int getAscnt() {
		return ascnt;
	}
	public void setAscnt(int ascnt) {
		this.ascnt = ascnt;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
  	
}
