package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true) 
public class T5679 {

	/** The cnPremStats */
	private List<String> cnPremStats;
	/** The cnRiskStats */
	private List<String> cnRiskStats;
	/** The covPremStats */
	private List<String> covPremStats;
	/** The covRiskStats */
	private List<String> covRiskStats;
	/** The jlifeStats */
	private List<String> jlifeStats;
	/** The lifeStats */
	private List<String> lifeStats;
	/** The ridPremStats */
	private List<String> ridPremStats;
	/** The ridRiskStats */
	private List<String> ridRiskStats;
	/** The setCnPremStat */
	private String setCnPremStat;
	/** The setCnRiskStat */
	private String setCnRiskStat;
	/** The setCovPremStat */
	private String setCovPremStat;
	/** The setCovRiskStat */
	private String setCovRiskStat;
	/** The setJlifeStat */
	private String setJlifeStat;
	/** The setLifeStat */
	private String setLifeStat;
	/** The setRidPremStat */
	private String setRidPremStat;
	/** The setRidRiskStat */
	private String setRidRiskStat;
	/** The setSngpCnStat */
	private String setSngpCnStat;
	/** The setSngpCovStat */
	private String setSngpCovStat;
	/** The setSngpRidStat */
	private String setSngpRidStat;

	/**
	 * @return the cnPremStats
	 */
	public List<String> getCnPremStats() {
		return cnPremStats;
	}

	/**
	 * @param cnPremStats
	 *            the cnPremStats to set
	 */
	public void setCnPremStats(List<String> cnPremStats) {
		this.cnPremStats = cnPremStats;
	}

	/**
	 * @return the cnRiskStats
	 */
	public List<String> getCnRiskStats() {
		return cnRiskStats;
	}

	/**
	 * @param cnRiskStats
	 *            the cnRiskStats to set
	 */
	public void setCnRiskStats(List<String> cnRiskStats) {
		this.cnRiskStats = cnRiskStats;
	}

	/**
	 * @return the covPremStats
	 */
	public List<String> getCovPremStats() {
		return covPremStats;
	}

	/**
	 * @param covPremStats
	 *            the covPremStats to set
	 */
	public void setCovPremStats(List<String> covPremStats) {
		this.covPremStats = covPremStats;
	}

	/**
	 * @return the covRiskStats
	 */
	public List<String> getCovRiskStats() {
		return covRiskStats;
	}

	/**
	 * @param covRiskStats
	 *            the covRiskStats to set
	 */
	public void setCovRiskStats(List<String> covRiskStats) {
		this.covRiskStats = covRiskStats;
	}

	/**
	 * @return the jlifeStats
	 */
	public List<String> getJlifeStats() {
		return jlifeStats;
	}

	/**
	 * @param jlifeStats
	 *            the jlifeStats to set
	 */
	public void setJlifeStats(List<String> jlifeStats) {
		this.jlifeStats = jlifeStats;
	}

	/**
	 * @return the lifeStats
	 */
	public List<String> getLifeStats() {
		return lifeStats;
	}

	/**
	 * @param lifeStats
	 *            the lifeStats to set
	 */
	public void setLifeStats(List<String> lifeStats) {
		this.lifeStats = lifeStats;
	}

	/**
	 * @return the ridPremStats
	 */
	public List<String> getRidPremStats() {
		return ridPremStats;
	}

	/**
	 * @param ridPremStats
	 *            the ridPremStats to set
	 */
	public void setRidPremStats(List<String> ridPremStats) {
		this.ridPremStats = ridPremStats;
	}

	/**
	 * @return the ridRiskStats
	 */
	public List<String> getRidRiskStats() {
		return ridRiskStats;
	}

	/**
	 * @param ridRiskStats
	 *            the ridRiskStats to set
	 */
	public void setRidRiskStats(List<String> ridRiskStats) {
		this.ridRiskStats = ridRiskStats;
	}

	/**
	 * @return the setCnPremStat
	 */
	public String getSetCnPremStat() {
		return setCnPremStat;
	}

	/**
	 * @param setCnPremStat
	 *            the setCnPremStat to set
	 */
	public void setSetCnPremStat(String setCnPremStat) {
		this.setCnPremStat = setCnPremStat;
	}

	/**
	 * @return the setCnRiskStat
	 */
	public String getSetCnRiskStat() {
		return setCnRiskStat;
	}

	/**
	 * @param setCnRiskStat
	 *            the setCnRiskStat to set
	 */
	public void setSetCnRiskStat(String setCnRiskStat) {
		this.setCnRiskStat = setCnRiskStat;
	}

	/**
	 * @return the setCovPremStat
	 */
	public String getSetCovPremStat() {
		return setCovPremStat;
	}

	/**
	 * @param setCovPremStat
	 *            the setCovPremStat to set
	 */
	public void setSetCovPremStat(String setCovPremStat) {
		this.setCovPremStat = setCovPremStat;
	}

	/**
	 * @return the setCovRiskStat
	 */
	public String getSetCovRiskStat() {
		return setCovRiskStat;
	}

	/**
	 * @param setCovRiskStat
	 *            the setCovRiskStat to set
	 */
	public void setSetCovRiskStat(String setCovRiskStat) {
		this.setCovRiskStat = setCovRiskStat;
	}

	/**
	 * @return the setJlifeStat
	 */
	public String getSetJlifeStat() {
		return setJlifeStat;
	}

	/**
	 * @param setJlifeStat
	 *            the setJlifeStat to set
	 */
	public void setSetJlifeStat(String setJlifeStat) {
		this.setJlifeStat = setJlifeStat;
	}

	/**
	 * @return the setLifeStat
	 */
	public String getSetLifeStat() {
		return setLifeStat;
	}

	/**
	 * @param setLifeStat
	 *            the setLifeStat to set
	 */
	public void setSetLifeStat(String setLifeStat) {
		this.setLifeStat = setLifeStat;
	}

	/**
	 * @return the setRidPremStat
	 */
	public String getSetRidPremStat() {
		return setRidPremStat;
	}

	/**
	 * @param setRidPremStat
	 *            the setRidPremStat to set
	 */
	public void setSetRidPremStat(String setRidPremStat) {
		this.setRidPremStat = setRidPremStat;
	}

	/**
	 * @return the setRidRiskStat
	 */
	public String getSetRidRiskStat() {
		return setRidRiskStat;
	}

	/**
	 * @param setRidRiskStat
	 *            the setRidRiskStat to set
	 */
	public void setSetRidRiskStat(String setRidRiskStat) {
		this.setRidRiskStat = setRidRiskStat;
	}

	/**
	 * @return the setSngpCnStat
	 */
	public String getSetSngpCnStat() {
		return setSngpCnStat;
	}

	/**
	 * @param setSngpCnStat
	 *            the setSngpCnStat to set
	 */
	public void setSetSngpCnStat(String setSngpCnStat) {
		this.setSngpCnStat = setSngpCnStat;
	}

	/**
	 * @return the setSngpCovStat
	 */
	public String getSetSngpCovStat() {
		return setSngpCovStat;
	}

	/**
	 * @param setSngpCovStat
	 *            the setSngpCovStat to set
	 */
	public void setSetSngpCovStat(String setSngpCovStat) {
		this.setSngpCovStat = setSngpCovStat;
	}

	/**
	 * @return the setSngpRidStat
	 */
	public String getSetSngpRidStat() {
		return setSngpRidStat;
	}

	/**
	 * @param setSngpRidStat
	 *            the setSngpRidStat to set
	 */
	public void setSetSngpRidStat(String setSngpRidStat) {
		this.setSngpRidStat = setSngpRidStat;
	}

}
