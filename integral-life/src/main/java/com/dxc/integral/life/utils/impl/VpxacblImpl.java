package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VpxacblDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.utils.Vpxacbl;
@Service("vpxacbl")
@Lazy
public class VpxacblImpl implements Vpxacbl {

	@Autowired
	private AcblpfDAO acblpfDAO;
	@Autowired
	private ItempfService itempfService;
	
	@Override
	public VpxacblDTO processVpxacbl(PremiumDTO premiumDTO) throws IOException {
		VpxacblDTO vpxacblDTO = new VpxacblDTO();
		vpxacblDTO.setStatuz("****");
		vpxacblDTO.setTotRegPrem(BigDecimal.ZERO);
		vpxacblDTO.setTotSingPrem(BigDecimal.ZERO);
		String rldgacct = "";
		if("TOPU".equals(premiumDTO.getFunction()) && !"00".equals(premiumDTO.getBillfreq())){
			int i = 0;
			T5688 t5688IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5688", premiumDTO.getCnttype(),T5688.class);
			if(t5688IO != null && "N".equals(t5688IO.getComlvlacc())){
				rldgacct = premiumDTO.getChdrChdrnum();
			} else{
				rldgacct = premiumDTO.getChdrChdrnum().concat(premiumDTO.getLifeLife()).concat(premiumDTO.getCovrCoverage()).concat(premiumDTO.getCovrRider())
						.concat(String.valueOf(premiumDTO.getPlnsfx()));
			}
			if(t5688IO != null && "N".equals(t5688IO.getComlvlacc())){
				i = 0;
			}else{
				i = 1;
			}
			Acblpf acblpf = calcPrem(premiumDTO,i,rldgacct);
			vpxacblDTO.setTotRegPrem(acblpf.getSacscurbal());
			if(t5688IO != null && "N".equals(t5688IO.getComlvlacc())){
				i = 2;
			}else{
				i = 3;
			}
			acblpf = calcPrem(premiumDTO,i,rldgacct);
			vpxacblDTO.setTotSingPrem(acblpf.getSacscurbal());
		}
		return vpxacblDTO;
	}
	protected Acblpf calcPrem(PremiumDTO premiumDTO,int i,String rldgacct) throws IOException{
		T5645 t5645IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5645", "PRMPM19",T5645.class);
		Acblpf acblpf = new Acblpf();
		acblpf.setRldgcoy(premiumDTO.getChdrChdrcoy());
		acblpf.setSacscode(t5645IO.getSacscodes().get(i));
		acblpf.setRldgacct(rldgacct);
		acblpf.setOrigcurr(premiumDTO.getCurrcode());
		acblpf.setSacstyp(t5645IO.getSacstypes().get(i));
		acblpf = acblpfDAO.readAcblpf(acblpf);
		if(acblpf == null){
			acblpf = new Acblpf();
			acblpf.setSacscurbal(BigDecimal.ZERO);
		}
		if("-".equals(t5645IO.getSigns().get(i))){
			acblpf.setSacscurbal(BigDecimal.ZERO.subtract(acblpf.getSacscurbal()).setScale(2));
		}
		return acblpf;
	}

}
