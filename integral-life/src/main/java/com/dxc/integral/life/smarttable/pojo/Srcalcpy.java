package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class Srcalcpy {

  	private String chdrChdrcoy;
  	private String chdrChdrnum;
  	private Integer planSuffix;
  	private Integer polsum;
  	private String lifeLife;
  	private String lifeJlife;
  	private String covrCoverage;
  	private String covrRider;
  	private String crtable;
  	private Integer crrcd;
  	private Integer ptdate;
  	private Integer effdate;
  	private Integer convUnits;
  	private String language;
  	private BigDecimal estimatedVal;
  	private BigDecimal actualVal;
  	private String currcode;
  	private String chdrCurr;
  	private String pstatcode;
  	private String element;
  	private String description;
  	private BigDecimal singp;
  	private String billfreq;
  	private String type;
  	private String fund;
  	private String status;
  	private String endf;
  	private String neUnits;
  	private String tmUnits;
  	private String psNotAllwd;
  	private BigDecimal tsvtot;
  	private BigDecimal tsv1tot;
	public String getChdrChdrcoy() {
		return chdrChdrcoy;
	}
	public void setChdrChdrcoy(String chdrChdrcoy) {
		this.chdrChdrcoy = chdrChdrcoy;
	}
	public String getChdrChdrnum() {
		return chdrChdrnum;
	}
	public void setChdrChdrnum(String chdrChdrnum) {
		this.chdrChdrnum = chdrChdrnum;
	}
	public Integer getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Integer planSuffix) {
		this.planSuffix = planSuffix;
	}
	public Integer getPolsum() {
		return polsum;
	}
	public void setPolsum(Integer polsum) {
		this.polsum = polsum;
	}
	public String getLifeLife() {
		return lifeLife;
	}
	public void setLifeLife(String lifeLife) {
		this.lifeLife = lifeLife;
	}
	public String getLifeJlife() {
		return lifeJlife;
	}
	public void setLifeJlife(String lifeJlife) {
		this.lifeJlife = lifeJlife;
	}
	public String getCovrCoverage() {
		return covrCoverage;
	}
	public void setCovrCoverage(String covrCoverage) {
		this.covrCoverage = covrCoverage;
	}
	public String getCovrRider() {
		return covrRider;
	}
	public void setCovrRider(String covrRider) {
		this.covrRider = covrRider;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public Integer getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Integer crrcd) {
		this.crrcd = crrcd;
	}
	public Integer getPtdate() {
		return ptdate;
	}
	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public Integer getConvUnits() {
		return convUnits;
	}
	public void setConvUnits(Integer convUnits) {
		this.convUnits = convUnits;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public BigDecimal getEstimatedVal() {
		return estimatedVal;
	}
	public void setEstimatedVal(BigDecimal estimatedVal) {
		this.estimatedVal = estimatedVal;
	}
	public BigDecimal getActualVal() {
		return actualVal;
	}
	public void setActualVal(BigDecimal actualVal) {
		this.actualVal = actualVal;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getChdrCurr() {
		return chdrCurr;
	}
	public void setChdrCurr(String chdrCurr) {
		this.chdrCurr = chdrCurr;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getSingp() {
		return singp;
	}
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFund() {
		return fund;
	}
	public void setFund(String fund) {
		this.fund = fund;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEndf() {
		return endf;
	}
	public void setEndf(String endf) {
		this.endf = endf;
	}
	public String getNeUnits() {
		return neUnits;
	}
	public void setNeUnits(String neUnits) {
		this.neUnits = neUnits;
	}
	public String getTmUnits() {
		return tmUnits;
	}
	public void setTmUnits(String tmUnits) {
		this.tmUnits = tmUnits;
	}
	public String getPsNotAllwd() {
		return psNotAllwd;
	}
	public void setPsNotAllwd(String psNotAllwd) {
		this.psNotAllwd = psNotAllwd;
	}
	public BigDecimal getTsvtot() {
		return tsvtot;
	}
	public void setTsvtot(BigDecimal tsvtot) {
		this.tsvtot = tsvtot;
	}
	public BigDecimal getTsv1tot() {
		return tsv1tot;
	}
	public void setTsv1tot(BigDecimal tsv1tot) {
		this.tsv1tot = tsv1tot;
	}
 
}
