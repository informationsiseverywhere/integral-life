package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AgorpfDAO;
import com.dxc.integral.life.dao.model.Agorpf;

@Repository("agorpfDAO")
@Lazy
public class AgorpfDAOImpl extends BaseDAOImpl implements AgorpfDAO {

	@Override
	public Agorpf getAgorpfByCoyAndNumAndtag(String angtcoy, String agntnum, String reportag) {
		StringBuilder sql = new StringBuilder("SELECT * FROM AGORPF WHERE ");
		sql.append(" AGNTCOY = ? and AGNTNUM = ? and REPORTAG=? order by  AGNTCOY ASC, AGNTNUM ASC, REPORTAG ASC, UNIQUE_NUMBER DESC ");

		List<Agorpf> result =  jdbcTemplate.query(sql.toString(), new Object[] {angtcoy, agntnum,reportag},new BeanPropertyRowMapper<Agorpf>(Agorpf.class));
		if (result.size() > 0) return result.get(0);
		return null;
	}
	
	@Override
	public List<Agorpf> getAgorpfByCoyAndNum(String angtcoy, String agntnum) {
		StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER, AGNTCOY, AGNTNUM, REPORTAG, AGTYPE01, AGTYPE02, EFFDATE01, EFFDATE02, DESC_T, USRPRF, JOBNM, DATIME");
		sql.append(" FROM AGORPF WHERE AGNTCOY = ? AND AGNTNUM = ? ORDER BY AGNTCOY ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC  ");

		return jdbcTemplate.query(sql.toString(), new Object[] {angtcoy, agntnum},new BeanPropertyRowMapper<Agorpf>(Agorpf.class));
	}

}
