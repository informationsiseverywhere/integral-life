package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.dxc.integral.fsu.dao.model.Acmvpf;

public class AcmvrevcpPojo {
	
	private long unique_number = 0;
	private String rdocpfx = "";
	private String rdoccoy = "";
	private String rdocnum = "";
	private int tranno = 0;
	private int jrnseq = 0;
	private String rldgpfx = "";
	private String rldgcoy = "";
	private String rldgacct = "";
	private String sacscode = "";
	private String sacstyp = "";
	private String batccoy = "";
	private String batcbrn = "";
	private int batcactyr = 0;
	private int batcactmn = 0;
	private String batctrcde = "";
	private String batcbatch = "";
	private String origcurr = "";
	private BigDecimal origamt = BigDecimal.ZERO;
	private String tranref = "";
	private String trandesc = "";
	private BigDecimal crate = BigDecimal.ZERO;
	private BigDecimal acctamt = BigDecimal.ZERO;
	private String genlcoy = "";
	private String genlcur = "";
	private String glcode = "";
	private String glsign = "";
	private String postyear = "";
	private String postmonth = "";
	private int effdate = 0;
	private int dateto = 0;
	private BigDecimal rcamt = BigDecimal.ZERO;
	private int frcdate = 0;
	private String intextind = "";
	private int trdt = 0;
	private int trtm = 0;
	private int user_t = 0;
	private String termid = "";
	private String reconsbr = "";
	private String suprflg = "";
	private String taxcode = "";
	private int reconref = 0;
	private String stmtsort = "";
	private int creddte = 0;
	private String usrprf = "";
	private String jobnm = "";
	private Timestamp datime;
	private String statuz = "";
	private String function = "";
	
	public void setAcmvrevcpPojoFromAcmvpf(Acmvpf acmvpf) {
		this.unique_number = acmvpf.getUnique_number();
		this.rdocpfx = acmvpf.getRdocpfx();
		this.rdoccoy = acmvpf.getRdoccoy();
		this.rdocnum = acmvpf.getRdocnum();
		this.tranno = acmvpf.getTranno();
		this.jrnseq = acmvpf.getJrnseq();
		this.rldgpfx = acmvpf.getRldgpfx();
		this.rldgcoy = acmvpf.getRldgcoy();
		this.rldgacct = acmvpf.getRldgacct();
		this.sacscode = acmvpf.getSacscode();
	    this.sacstyp = acmvpf.getSacstyp();
		this.batccoy = acmvpf.getBatccoy();
		this.batcbrn = acmvpf.getBatcbrn();
		this.batcactyr = acmvpf.getBatcactyr();
		this.batcactmn = acmvpf.getBatcactmn();
		this.batctrcde = acmvpf.getBatctrcde();
		this.batcbatch = acmvpf.getBatcbatch();
		this.origcurr = acmvpf.getOrigcurr();
		this.origamt = acmvpf.getOrigamt();
		this.tranref = acmvpf.getTranref();
		this.trandesc = acmvpf.getTrandesc();
		this.crate = acmvpf.getCrate();
		this.acctamt = acmvpf.getAcctamt();
		this.genlcoy = acmvpf.getGenlcoy();
		this.genlcur = acmvpf.getGenlcur();
		this.glcode = acmvpf.getGlcode();
		this.glsign = acmvpf.getGlsign();
		this.postyear = acmvpf.getPostyear();
		this.postmonth = acmvpf.getPostmonth();
		this.effdate = acmvpf.getEffdate();
		this.dateto = acmvpf.getDateto();
		this.rcamt = acmvpf.getRcamt();
		this.frcdate = acmvpf.getFrcdate();
		this.intextind = acmvpf.getIntextind();
		this.trdt = acmvpf.getTrdt();
		this.trtm = acmvpf.getTrtm();
		this.user_t = acmvpf.getUser_t();
		this.termid = acmvpf.getTermid();
		this.reconsbr = acmvpf.getReconsbr();
		this.suprflg = acmvpf.getSuprflg();
		this.taxcode = acmvpf.getTaxcode();
		this.reconref = acmvpf.getReconref();
		this.stmtsort = acmvpf.getStmtsort();
		this.creddte = acmvpf.getCreddte();
		this.usrprf = acmvpf.getUsrprf();
		this.jobnm = acmvpf.getJobnm();
		this.datime = acmvpf.getDatime();
	}
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getRdocpfx() {
		return rdocpfx;
	}
	public void setRdocpfx(String rdocpfx) {
		this.rdocpfx = rdocpfx;
	}
	public String getRdoccoy() {
		return rdoccoy;
	}
	public void setRdoccoy(String rdoccoy) {
		this.rdoccoy = rdoccoy;
	}
	public String getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(String rdocnum) {
		this.rdocnum = rdocnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}
	public String getRldgpfx() {
		return rldgpfx;
	}
	public void setRldgpfx(String rldgpfx) {
		this.rldgpfx = rldgpfx;
	}
	public String getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public BigDecimal getOrigamt() {
		return origamt;
	}
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}
	public String getTranref() {
		return tranref;
	}
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}
	public String getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}
	public BigDecimal getCrate() {
		return crate;
	}
	public void setCrate(BigDecimal crate) {
		this.crate = crate;
	}
	public BigDecimal getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(BigDecimal acctamt) {
		this.acctamt = acctamt;
	}
	public String getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(String genlcoy) {
		this.genlcoy = genlcoy;
	}
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	public String getGlcode() {
		return glcode;
	}
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	public String getGlsign() {
		return glsign;
	}
	public void setGlsign(String glsign) {
		this.glsign = glsign;
	}
	public String getPostyear() {
		return postyear;
	}
	public void setPostyear(String postyear) {
		this.postyear = postyear;
	}
	public String getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(String postmonth) {
		this.postmonth = postmonth;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getDateto() {
		return dateto;
	}
	public void setDateto(int dateto) {
		this.dateto = dateto;
	}
	public BigDecimal getRcamt() {
		return rcamt;
	}
	public void setRcamt(BigDecimal rcamt) {
		this.rcamt = rcamt;
	}
	public int getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(int frcdate) {
		this.frcdate = frcdate;
	}
	public String getIntextind() {
		return intextind;
	}
	public void setIntextind(String intextind) {
		this.intextind = intextind;
	}
	public int getTrdt() {
		return trdt;
	}
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	public int getTrtm() {
		return trtm;
	}
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	public int getUser_t() {
		return user_t;
	}
	public void setUser_t(int user_t) {
		this.user_t = user_t;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getReconsbr() {
		return reconsbr;
	}
	public void setReconsbr(String reconsbr) {
		this.reconsbr = reconsbr;
	}
	public String getSuprflg() {
		return suprflg;
	}
	public void setSuprflg(String suprflg) {
		this.suprflg = suprflg;
	}
	public String getTaxcode() {
		return taxcode;
	}
	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}
	public int getReconref() {
		return reconref;
	}
	public void setReconref(int reconref) {
		this.reconref = reconref;
	}
	public String getStmtsort() {
		return stmtsort;
	}
	public void setStmtsort(String stmtsort) {
		this.stmtsort = stmtsort;
	}
	public int getCreddte() {
		return creddte;
	}
	public void setCreddte(int creddte) {
		this.creddte = creddte;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
	
}
