
package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.BatcupDTO;

public interface BatLifecup {
	public void callBatcup(BatcupDTO batcupDTO);
}
