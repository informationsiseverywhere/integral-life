package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.NlgcalcDTO;
	/**
	 * Nlgcalc utility.
	 * 
	 * @author wli31
	 *
	 */
	public interface Nlgcalc {
		public NlgcalcDTO processNlgcalOvdue(NlgcalcDTO nlgcalcDTO) throws IOException, ParseException ;
		public NlgcalcDTO processNlgCollection(NlgcalcDTO nlgcalcDTO) throws IOException, ParseException ;
	}


