package com.dxc.integral.life.smarttable.pojo;

public class Td5j {

	private Integer allowperiod;
	private Integer daycheck;
	private String subroutine;
	public Integer getAllowperiod() {
		return allowperiod;
	}
	public void setAllowperiod(Integer allowperiod) {
		this.allowperiod = allowperiod;
	}
	public Integer getDaycheck() {
		return daycheck;
	}
	public void setDaycheck(Integer daycheck) {
		this.daycheck = daycheck;
	}
	public String getSubroutine() {
		return subroutine;
	}
	public void setSubroutine(String subroutine) {
		this.subroutine = subroutine;
	}
}
