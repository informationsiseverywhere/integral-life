package com.dxc.integral.life.utils.impl;

import static com.dxc.integral.iaf.constants.CommonConstants.IT_ITEMPFX;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.life.beans.LifsttrDTO;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.AglfpfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.RacdpfDAO;
import com.dxc.integral.life.dao.RegppfDAO;
import com.dxc.integral.life.dao.SttrpfDAO;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Aglfpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Racdpf;
import com.dxc.integral.life.dao.model.Regppf;
import com.dxc.integral.life.dao.model.Sttrpf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.GOTOInterface;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T6627;
import com.dxc.integral.life.smarttable.pojo.T6628;
import com.dxc.integral.life.smarttable.pojo.T6629;
import com.dxc.integral.life.smarttable.pojo.Tj688;
import com.dxc.integral.life.utils.Lifsttr;

/* 
 * 
 * @author wli31
 *
 */
@Service("lifsttr")
@Lazy
public class LifsttrImpl implements Lifsttr {
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;
	@Autowired
	private SttrpfDAO sttrpfDAO;
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private RegppfDAO regppfDAO;
	@Autowired
	private AglfpfDAO aglfpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private RacdpfDAO racdpfDAO;
	@Autowired
	private AgcmpfDAO agcmpfDAO;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		covr3020, 
		continue3021, 
		read3030, 
		cont3050, 
		agent3055, 
		next3060, 
		exit3090,
		check4420,
		next4450,
		exit4490,
		loop5120,
		next5150, 
		cont5160, 
		next5170, 
		exit5195, 
		loop3620,
		next3650,
		cont3660,
		next3670,
		exit3695,
		loop3120,
		next3130,
		loop3140,
		next3160,
		exit3190,
		loop5520,
		next5550,
		cont5560,
		next5570,
		loop5577,
		next5580,
		write5585,
		next5587,
		exit5590, 
		loop3920, 
		next3950, 
		cont3960, 
		next3970, 
		loop3977, 
		next3980, 
		write3985, 
		next3987, 
		exit3990, 
	}
	
	private List<Sttrpf> sttrrevList;
	private List<Sttrpf> insertSttrrevList;  
	private Sttrpf sttrpf;
	private Map<String, List<Itempf>> t6629Map = null;   
    private Map<String, List<Itempf>> t6628Map = null;
    private Map<String, List<Itempf>> t6627Map = null;
    private Map<String, List<Itempf>> t3629Map = null;
    private Map<String, List<Itempf>> t5540Map = null;
    private T6629 t6629IO = null;
    private T6627 t6627IO = null;
    private T6628 t6628IO = null;
    private T3629 t3629IO = null;
    private T5679 t5679IO = null;
    private Tj688 tj688IO = null;
	private String wsaaBandage;
	private String wsaaBandsa;
	private String wsaaBandprm;
	private String wsaaBandtrm;
	private String wsaaAgntage;
	private String wsaaAgntsa;
	private String wsaaAgntprm;
	private String wsaaAgnttrm;
	private String wsaaGovtage;
	private String wsaaGovtsa;
	private String wsaaGovtprm;
	private String wsaaGovttrm;
    private int wsaaStcmth;
	private int wsaaStcmthg;
	private int wsaaStlmth;
	private int wsaaStvmth;
	private int wsaaStumth;
	private int wsaaStnmth;
	private int wsaaStpmth;
	private int wsaaStsmth;
	private int wsaaStmmth;
	private int wsaaStimth;
	private int wsaaStpmthg;
	private int wsaaStsmthg;
	private int wsaaSub;
	private int wsaaSub1;
	private int wsaaTableSub = 0;
	private String subtractPrevious = "01,02,03,04,05,06,07,08,09,10,11";
	private String addPrevious = "12,13,14,15,16,17,18,19,20,21,22";
	private String subtractCurrent = "23,24,25,26,27,28,29,30,31,32,33";
	private String addCurrent = "34,35,36,37,38,39,40,41,42,43,44";
	private String subtractIncrease = "45,46,47,48,49,50,51,52,53,54,55";
	private String addIncrease = "56,57,58,59,60,61,62,63,64,65,66";
	private String subtractDecrease = "67,68,69,70,71,72,73,74,75,76,77";
	private String addDecrease = "78,79,80,81,82,83,84,85,86,87,88";
	private BigDecimal wsaaStbmthg;
	private BigDecimal wsaaStldmthg;
	private BigDecimal wsaaHdrPremium;
    private BigDecimal wsaaAnnTotCurrent = BigDecimal.ZERO;
    private BigDecimal wsaaAnnTotPrem = BigDecimal.ZERO;
    private BigDecimal wsaaAnnpremCurr = BigDecimal.ZERO;
    private BigDecimal wsaaAnnCurrent = BigDecimal.ZERO;
    private BigDecimal wsaaAnnpremPrev = BigDecimal.ZERO;
    private BigDecimal wsaaAnnPrevious = BigDecimal.ZERO;
    private BigDecimal wsaaStraamt = BigDecimal.ZERO;
    private BigDecimal wsaaCommission = BigDecimal.ZERO;
	private int wsaaChdrTranno;
	private String wsaaChdrAgntnum;
	private BigDecimal wsaaSinstamt02;
	private BigDecimal wsaaSinstamt03;
	private BigDecimal wsaaSinstamt04;
	private BigDecimal wsaaSinstamt06;
	private String wsaaStatA;
	private String wsaaStatB;
	private String wsaaStatC;
	private String wsaaStatD;
	private String wsaaStatE;
	private String wsaaChdrpfx;
	private String wsaaCnttype;
	private String wsaaSrcebus;
	private String wsaaChdrStatcode;
	private String wsaaChdrReg;
	private String wsaaChdrCntbranch;
	private String wsaaCharBillfreq;
	private String wsaaChdrPstatcode;
    private int wsaaLastTranno;
    private boolean wsaaFirstTime;
    private String wsaaStatcat;
    private String wsaaT6628Item;
    private int wsaaIdx;
    private int wsaaLives;
    private Chdrpf chdrpf = null;
    private Aglfpf aglfpf = null;
    private String wsaaHdrChg="";
    private String wsaaPrevCntcurr;
    private String wsaaSttrWritten;
    private String wsaaPrevLife;
    private Covrpf covrsta = null;
    private Covrpf covrsts = null;
	private BigDecimal wsaaCurrPrem;
	private BigDecimal wsaaPrevPrem;
	private BigDecimal wsaaPremium;
	private BigDecimal wsaaSumins;
	private BigDecimal wsaaBillfreq;
	private BigDecimal wsaaBillfreqPrev;
	private BigDecimal wsaaAnnprem;
	private int wsaaAnbAtCcd;
	private int wsaaRiskCessTerm;
	private BigDecimal wsaaInstprem;
	private BigDecimal wsaaSumin;
	private String wsaaBonusInd;
	private int wsaaTranno;
	private String wsaaAgntstat = "";
	private String wsaaGovtstat = "";
	private String wsaaAgntnum;
	private String wsaaHdrAccum;
	private String wsaaOvrdcat;
	private String wsaaValidflag;
	private String wsaaLife;
	private String wsaaCoverage;
	private String wsaaRider;
	private int wsaaPlanSuffix;
	private String wsaaValidStatus;
	private String wsaaStatcode;
	private String wsaaPstatcode;
	private String wsaaFound;
	private Agcmpf agcmsts = new Agcmpf();
	private Agcmpf agcmsta = new Agcmpf();
    private Iterator<Agcmpf> agcmstsIterator;
    private Iterator<Agcmpf> agcmstaIterator;
    private Iterator<Covrpf> covrstsIterator;
    private List<Sttrpf> insertSttrpfList;
    private String wsaaRacdstaFirstTime = "";
    private int wsaaRacdstaLastTranno;
    private int wsaaTransDate;
    private int wsaaTransTime;
	@Override
	public LifsttrDTO processLifsttr(LifsttrDTO lifsttrDTO) throws IOException {
        t6629Map = itempfDAO.readSmartTableByTableName(lifsttrDTO.getBatccoy(), "T6629",CommonConstants.IT_ITEMPFX);
        t6628Map = itempfDAO.readSmartTableByTableName(lifsttrDTO.getBatccoy(), "T6628",CommonConstants.IT_ITEMPFX);
        t6627Map = itempfDAO.readSmartTableByTableName(lifsttrDTO.getBatccoy(), "T6627",CommonConstants.IT_ITEMPFX);
        t3629Map = itempfDAO.readSmartTableByTableName(lifsttrDTO.getBatccoy(), "T3629",CommonConstants.IT_ITEMPFX);
        t5540Map = itempfDAO.readSmartTableByTableName(lifsttrDTO.getBatccoy(), "T5540",CommonConstants.IT_ITEMPFX);
        if (lifsttrDTO.getTranno() != 9999 && lifsttrDTO.getTrannor() < lifsttrDTO.getTranno()) {
            reverseSttr1000(lifsttrDTO);
            lifsttrDTO.setStatuz("****");
            return lifsttrDTO;
         }
        initialise(lifsttrDTO);
        componentSelect(lifsttrDTO);
        checkTranscation(lifsttrDTO);
        if (insertSttrrevList != null && !insertSttrrevList.isEmpty()) {
            sttrpfDAO.insertSttrpfRecord(insertSttrrevList);
        }
        if (insertSttrpfList != null && !insertSttrpfList.isEmpty()) {
            sttrpfDAO.insertSttrpfRecord(insertSttrpfList);
        }
        lifsttrDTO.setStatuz("****");
        return lifsttrDTO;
	}
	protected void componentSelect(LifsttrDTO lifsttrDTO) throws IOException{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				    start3010(lifsttrDTO);
					prev3015(lifsttrDTO);
				}
				case covr3020: {
					covr3020();
				}
				case continue3021: {
					continue3021();
				}
				case read3030: {
					read3030(lifsttrDTO);
				}
				case cont3050: {
					cont3050(lifsttrDTO);
				}
				case agent3055: {
					agent3055(lifsttrDTO);
				}
				case next3060: {
					next3060();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void start3010(LifsttrDTO lifsttrDTO){
        wsaaPrevCntcurr = "";
        wsaaSttrWritten = "N";
        List<Covrpf> covrstsList = covrpfDAO.searchValidCovrpfRecords(lifsttrDTO.getChdrcoy(), lifsttrDTO.getChdrnum(), "01", "01", "00");

        if (covrstsList == null || covrstsList.isEmpty()) {
            goTo(GotoLabel.exit3090);
        }
        covrstsIterator = covrstsList.iterator();
        covrsts = covrstsIterator.next();
        wsaaPrevLife = covrsts.getLife();
        a100CheckNoflives(lifsttrDTO);
    }

	protected void prev3015(LifsttrDTO lifsttrDTO) throws IOException{
		List<Covrpf> covrpfList = covrpfDAO.searchCovrstaRecords(covrsts.getChdrcoy(), covrsts.getChdrnum(), "01", "01", "00",covrsts.getCurrfrom(),9999,lifsttrDTO.getTranno());
		if(covrpfList != null && !covrpfList.isEmpty()){
			covrsta = covrpfList.get(0);
		}
		if (!"".equals(lifsttrDTO.getAgntnum())) {
			wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode);
			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
			}else{
				t6628IO = null;
			}
			if (t6628IO.getStatcatgs() != null) {
				wsaaPrevPrem = BigDecimal.ZERO;
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumin = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaTranno = lifsttrDTO.getTranno();
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum = wsaaChdrAgntnum;
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaStpmth = 0;
				wsaaStvmth = 0;
				wsaaStsmth = 0;
				wsaaStcmthg = 0;
				wsaaStlmth = 0;
				wsaaStmmth = 0;
				wsaaStcmth = 1;
				wsaaHdrPremium = BigDecimal.ZERO;
				wsaaHdrAccum = "Y";
				agentChange3900(lifsttrDTO,covrsts);
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaHdrAccum = "";
				wsaaAgntstat = "N";
			}
			wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode()); 
			if(t6628Map != null && t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
			    Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
			}else{
				t6628IO = null;
	             return;
			}
			if (t6628IO.getStatcatgs() == null) {
			    return;
			}
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum = lifsttrDTO.getAgntnum();
			wsaaValidflag = "1";
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaTranno = lifsttrDTO.getTranno();
			wsaaStvmth = 1;
			wsaaStcmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			agentChange3900(lifsttrDTO,covrsts);
			wsaaAgntstat = "N";
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			return;
		}
		wsaaCurrPrem = wsaaSinstamt06;
		if (covrsta == null) {
			if ("Y".equals(wsaaHdrChg)) {
				if (covrsts.getTranno() == lifsttrDTO.getTranno()) {
					 wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode); 
		            if(t6628Map != null && t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
					    Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
					    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
		            }else{
		            	t6628IO = null;
		            }
					if (t6628IO.getStatcatgs() != null) {
						wsaaPrevPrem = BigDecimal.ZERO;
						wsaaAnbAtCcd = covrsts.getAnbAtCcd();
						wsaaSumins = covrsts.getSumins();
						wsaaRiskCessTerm = covrsts.getRcestrm();
						wsaaInstprem = covrsts.getInstprem();
						wsaaStbmthg = covrsts.getZbinstprem();
						wsaaStldmthg = covrsts.getZlinstprem();
						wsaaBonusInd = covrsts.getBnusin();
						wsaaStlmth = wsaaLives;
						wsaaTranno = wsaaChdrTranno;
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaStpmth = 0;
						wsaaStvmth = 0;
						wsaaStcmth = 0;
						wsaaStsmth = 0;
						wsaaStmmth = 0;
						wsaaHdrAccum = "Y";
						wsaaValidflag = "1";
						headerPremium();
						updateRecord(lifsttrDTO);
						wsaaHdrPremium = BigDecimal.ZERO;
						wsaaAgntstat = "Y";
						wsaaGovtstat = "N";
						wsaaAgntnum = wsaaChdrAgntnum;
						wsaaAnbAtCcd = covrsts.getAnbAtCcd();
						wsaaSumins = covrsts.getSumins();
						wsaaRiskCessTerm = covrsts.getRcestrm();
						wsaaInstprem = covrsts.getInstprem();
						wsaaStbmthg = covrsts.getZbinstprem();
						wsaaStldmthg = covrsts.getZlinstprem();
						wsaaBonusInd = covrsts.getBnusin();
						wsaaStvmth = 0;
						wsaaStcmth = 1;
						processAgents3600(lifsttrDTO);
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaHdrAccum = "";
						wsaaAgntstat = "N";
					}
					wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode()); 
                    if(t6628Map != null && t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
                    	Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
 					    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
                    }else{
                    	t6628IO = null;
                        return;
                    }
					if (t6628IO == null) {
					    return;
					}
					wsaaValidflag = "1";
					wsaaCurrPrem = wsaaSinstamt06;
					wsaaPrevPrem = BigDecimal.ZERO;
					wsaaAnbAtCcd = covrsts.getAnbAtCcd();
					wsaaSumins = covrsts.getSumins();
					wsaaRiskCessTerm = covrsts.getRcestrm();
					wsaaInstprem = covrsts.getInstprem();
					wsaaStbmthg = covrsts.getZbinstprem();
					wsaaStldmthg = covrsts.getZlinstprem();
					wsaaBonusInd = covrsts.getBnusin();
					wsaaTranno = wsaaChdrTranno;
					wsaaGovtstat = "Y";
					wsaaAgntstat = "N";
					wsaaAgntnum = "";
					wsaaOvrdcat = "";
					wsaaStpmth = 0;
					wsaaStvmth = 0;
					wsaaStcmth = 0;
					wsaaStcmthg = 0;
					wsaaStlmth = 0;
					wsaaStsmth = 0;
					wsaaStmmth = 0;
					wsaaStraamt = BigDecimal.ZERO;
					wsaaLife = covrsts.getLife();
					wsaaCoverage = covrsts.getCoverage();
					wsaaRider = covrsts.getRider();
					wsaaPlanSuffix = covrsts.getPlnsfx();
					readRacdsts(lifsttrDTO);
					updateRecord(lifsttrDTO);
					wsaaHdrPremium = BigDecimal.ZERO;
					wsaaGovtstat = "N";
					wsaaAgntstat = "Y";
					wsaaGovtstat = "N";
					wsaaAgntnum = wsaaChdrAgntnum;
					wsaaAnbAtCcd = covrsts.getAnbAtCcd();
					wsaaSumins = covrsts.getSumins();
					wsaaRiskCessTerm = covrsts.getRcestrm();
					wsaaInstprem = covrsts.getInstprem();
					wsaaStbmthg = covrsts.getZbinstprem();
					wsaaStldmthg = covrsts.getZlinstprem();
					wsaaBonusInd = covrsts.getBnusin();
					wsaaStvmth = 1;
					wsaaStcmth = 0;
					wsaaStcmthg = 0;
					wsaaStlmth = 0;
					processAgents3600(lifsttrDTO);
					wsaaAgntnum = "";
					wsaaOvrdcat = "";
					wsaaAgntstat = "N";
					return;
				}
			}
		}
		if (covrsta == null) {
			if ("N".equals(wsaaHdrChg)) {
				if (covrsts.getTranno() == lifsttrDTO.getTranno()) {
                    wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode()); 
				    if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
				        Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
				        t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
				    }else{
				    	t6628IO = null;
                        goTo(GotoLabel.covr3020);
				    }
					if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
						goTo(GotoLabel.covr3020);
					}
					wsaaValidflag = "1";
					wsaaPrevPrem = BigDecimal.ZERO;
					wsaaAnbAtCcd = covrsts.getAnbAtCcd();
					wsaaSumins = covrsts.getSumins();
					wsaaRiskCessTerm = covrsts.getRcestrm();
					wsaaInstprem = covrsts.getInstprem();
					wsaaStbmthg = covrsts.getZbinstprem();
					wsaaStldmthg = covrsts.getZlinstprem();
					wsaaBonusInd = covrsts.getBnusin();
					wsaaTranno = covrsts.getTranno();
					wsaaGovtstat = "Y";
					wsaaAgntstat = "N";
					wsaaAgntnum = "";
					wsaaOvrdcat = "";
					wsaaStpmth = 0;
					wsaaStvmth = 0;
					wsaaStcmth = 0;
					wsaaStcmthg = 0;
					wsaaStlmth = 0;
					wsaaStsmth = 0;
					wsaaStmmth = 0;
					wsaaStraamt = BigDecimal.ZERO;
					wsaaLife = covrsts.getLife();
					wsaaCoverage = covrsts.getCoverage();
					wsaaRider = covrsts.getRider();
					wsaaPlanSuffix = covrsts.getPlnsfx();
					readRacdsts(lifsttrDTO);
					updateRecord(lifsttrDTO);
					wsaaGovtstat = "N";
					wsaaAgntstat = "Y";
					wsaaGovtstat = "N";
					wsaaAgntnum = wsaaChdrAgntnum;
					wsaaAnbAtCcd = covrsts.getAnbAtCcd();
					wsaaSumins = covrsts.getSumins();
					wsaaRiskCessTerm = covrsts.getRcestrm();
					wsaaInstprem = covrsts.getInstprem();
					wsaaStbmthg = covrsts.getZbinstprem();
					wsaaStldmthg = covrsts.getZlinstprem();
					wsaaBonusInd = covrsts.getBnusin();
					wsaaStvmth = 1;
					wsaaStcmth = 0;
					wsaaStcmthg = 0;
					wsaaStlmth = 0;
					processAgents3600(lifsttrDTO);
					wsaaAgntnum = "";
					wsaaOvrdcat = "";
					wsaaAgntstat = "N";
					goTo(GotoLabel.covr3020);
				}
			}
		}
		if ("Y".equals(lifsttrDTO.getMinorChg())) {
	        wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode()); 
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
            }else{
            	t6628IO = null;
                goTo(GotoLabel.covr3020);
            }
			if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
				goTo(GotoLabel.covr3020);
			}
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum = wsaaChdrAgntnum;
			wsaaTranno = lifsttrDTO.getTranno();
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaStvmth = 1;
			wsaaStcmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			agentCommChange4400(lifsttrDTO);
		}
		if (covrsta != null && "N".equals(wsaaHdrChg)){
			goTo(GotoLabel.covr3020);
		}
		if ("Y".equals(wsaaHdrChg)) {
			if (wsaaBillfreq.compareTo(wsaaBillfreqPrev) == 0) {
				if (covrsts.getTranno() != lifsttrDTO.getTranno()) {

                    wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode); 
		            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
		                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
		                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
		            }else{
		            	t6628IO = null;
		                goTo(GotoLabel.covr3020);
		            }
					if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
						goTo(GotoLabel.covr3020);
					}
					wsaaPrevPrem = BigDecimal.ZERO;
					wsaaAnbAtCcd = covrsts.getAnbAtCcd();
					wsaaSumins = covrsts.getSumins();
					wsaaRiskCessTerm = covrsts.getRcestrm();
					wsaaInstprem = covrsts.getInstprem();
					wsaaStbmthg = covrsts.getZbinstprem();
					wsaaStldmthg = covrsts.getZlinstprem();
					wsaaBonusInd = covrsts.getBnusin();
					wsaaTranno = wsaaChdrTranno;
					wsaaGovtstat = "Y";
					wsaaAgntstat = "N";
					wsaaAgntnum = "";
					wsaaOvrdcat = "";
					wsaaStpmth = 0;
					wsaaStvmth = 0;
					wsaaStcmth = 0;
					wsaaStsmth = 0;
					wsaaStmmth = 0;
					wsaaStcmthg = 1;
					wsaaStlmth = wsaaLives;
					wsaaHdrAccum = "Y";
					wsaaValidflag = "1";
					headerPremium();
					updateRecord(lifsttrDTO);
					if (covrsta!= null ) {
						wsaaPrevPrem = BigDecimal.ZERO;
						wsaaAnbAtCcd = covrsta.getAnbAtCcd();
						wsaaSumins = covrsta.getSumins();
						wsaaRiskCessTerm = covrsta.getRcestrm();
						wsaaInstprem = covrsta.getInstprem();
						wsaaStbmthg = covrsta.getZbinstprem();
						wsaaStldmthg = covrsta.getZlinstprem();
						wsaaBonusInd = covrsta.getBnusin();
						wsaaTranno = wsaaChdrTranno;
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaStpmth = 0;
						wsaaStvmth = 0;
						wsaaStcmth = 0;
						wsaaStsmth = 0;
						wsaaStmmth = 0;
						wsaaStcmthg = 1;
						wsaaStlmth = wsaaLives;
						wsaaValidflag = "2";
						previousHeaderPremium();
						updateRecord(lifsttrDTO);
						wsaaHdrPremium = BigDecimal.ZERO;
					}
					wsaaAgntstat = "Y";
					wsaaGovtstat = "N";
					wsaaAgntnum = wsaaChdrAgntnum;
					wsaaAnbAtCcd = covrsts.getAnbAtCcd();
					wsaaSumins = covrsts.getSumins();
					wsaaRiskCessTerm = covrsts.getRcestrm();
					wsaaInstprem = covrsts.getInstprem();
					wsaaStbmthg = covrsts.getZbinstprem();
					wsaaStldmthg = covrsts.getZlinstprem();
					wsaaBonusInd = covrsts.getBnusin();
					wsaaStvmth = 0;
					wsaaStcmthg = 0;
					wsaaStlmth = 0;
					wsaaStcmth = 1;
					processAgents(lifsttrDTO);
					wsaaAgntnum = "";
					wsaaOvrdcat = "";
					wsaaHdrAccum = "";
					wsaaAgntstat = "N";
					if (covrsta == null) {
						wsaaGovtstat = "Y";
						wsaaStraamt = BigDecimal.ZERO;
						wsaaLife = covrsts.getLife();
						wsaaCoverage = covrsts.getCoverage();
						wsaaRider = covrsts.getRider();
						wsaaPlanSuffix = covrsts.getPlnsfx();
						readRacdsts(lifsttrDTO);
						updateRecord(lifsttrDTO);
					}
					goTo(GotoLabel.covr3020);
				}
			}
		}
		if ("X".equals(wsaaHdrChg)) {
            wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode); 
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
            }else{
            	t6628IO = null;
                goTo(GotoLabel.covr3020);
            }
			if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
				goTo(GotoLabel.covr3020);
			}
			wsaaValidStatus = "N";
			wsaaStatcode = covrsts.getStatcode();
			wsaaPstatcode = covrsts.getPstatcode();
			for (int wsaaIndex = 0; wsaaIndex < 12 ||"Y".equals(wsaaValidStatus);wsaaIndex++ ){
				if (wsaaStatcode.equals(t5679IO.getCovRiskStats().get(wsaaIndex))) {
					for (wsaaIndex = 0; wsaaIndex < 12 ||"Y".equals(wsaaValidStatus);wsaaIndex++){
						if (wsaaPstatcode.equals(t5679IO.getCovPremStats().get(wsaaIndex))) {
							wsaaValidStatus = "Y";
						}
					}
				}
			}
			if ("N".equals(wsaaValidStatus)) {
				throw new SubroutineIOException("Lifsttr occur exceptio while processing contract ");
			}
			wsaaValidflag = "1";
			wsaaPrevPrem = chdrpf.getSinstamt06();
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaStpmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmth = 0;
			wsaaStvmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife = covrsts.getLife();
			wsaaCoverage = covrsts.getCoverage();
			wsaaRider = covrsts.getRider();
			wsaaPlanSuffix = covrsts.getPlnsfx();
			readRacdsts(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaValidflag = "2";
			wsaaAnbAtCcd = covrsta.getAnbAtCcd();
			wsaaSumins = covrsta.getSumins();
			wsaaRiskCessTerm = covrsta.getRcestrm();
			wsaaInstprem = covrsta.getInstprem();
			wsaaStbmthg = covrsta.getZbinstprem();
			wsaaStldmthg = covrsta.getZlinstprem();
			wsaaBonusInd = covrsta.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaStpmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmth = 0;
			wsaaStvmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaLife = covrsta.getLife();
			wsaaCoverage = covrsta.getCoverage();
			wsaaRider = covrsta.getRider();
			wsaaPlanSuffix = covrsta.getPlnsfx();
			a600ReadRacdsta(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum = wsaaChdrAgntnum;;
			wsaaTranno = covrsts.getTranno();
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaStvmth = 0;
			wsaaStcmth = 1;
			processAgents3600(lifsttrDTO);
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaAgntstat = "N";
			goTo(GotoLabel.covr3020);
		}
		if (wsaaBillfreq.compareTo(wsaaBillfreqPrev) != 0) {
			if ("Y".equals(wsaaHdrChg)) {
				wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode); 
	            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
	                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
	                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
	            }else{
	            	t6628IO = null;
                    goTo(GotoLabel.covr3020);
                }
				if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
					goTo(GotoLabel.covr3020);
				}
				wsaaPrevPrem = BigDecimal.ZERO;
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaTranno = wsaaChdrTranno;
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaStpmth = 0;
				wsaaStvmth = 0;
				wsaaStcmth = 0;
				wsaaStsmth = 0;
				wsaaStmmth = 0;
				wsaaStcmthg = 1;
				wsaaStlmth = wsaaLives;
				wsaaHdrAccum = "Y";
				wsaaValidflag = "1";
				headerPremium();
				updateRecord(lifsttrDTO);
				wsaaPrevPrem = BigDecimal.ZERO;
				wsaaAnbAtCcd = covrsta.getAnbAtCcd();
				wsaaSumins = covrsta.getSumins();
				wsaaRiskCessTerm = covrsta.getRcestrm();
				wsaaInstprem = covrsta.getInstprem();
				wsaaStbmthg = covrsta.getZbinstprem();
				wsaaStldmthg = covrsta.getZlinstprem();
				wsaaBonusInd = covrsta.getBnusin();
				wsaaTranno = wsaaChdrTranno;
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaStpmth = 0;
				wsaaStvmth = 0;
				wsaaStcmth = 0;
				wsaaStsmth = 0;
				wsaaStmmth = 0;
				wsaaStcmthg = 1;
				wsaaStlmth = wsaaLives;
				wsaaValidflag = "2";
				previousHeaderPremium();
				updateRecord(lifsttrDTO);
				wsaaHdrPremium = BigDecimal.ZERO;;
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum = wsaaChdrAgntnum;
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaStvmth = 0;
				wsaaStcmthg = 0;
				wsaaStlmth = 0;
				wsaaStcmth = 1;
				processAgents(lifsttrDTO);
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaHdrAccum = "";
				wsaaAgntstat = "N";
			}
			if (covrsts.getTranno() != lifsttrDTO.getTranno()) {
				goTo(GotoLabel.covr3020);
			}
			wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode()); 
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
            }else{
            	t6628IO = null;
                goTo(GotoLabel.covr3020);
            }
			if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
				goTo(GotoLabel.covr3020);
			}
			wsaaPrevPrem = chdrpf.getSinstamt06();
			wsaaValidflag = "1";
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaStpmth = 0;
			wsaaStvmth = 0;
			wsaaStcmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife = covrsts.getLife();
			wsaaCoverage = covrsts.getCoverage();
			wsaaRider = covrsts.getRider();
			wsaaPlanSuffix = covrsts.getPlnsfx();
			readRacdsts(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaValidflag = "2";
			wsaaAnbAtCcd = covrsta.getAnbAtCcd();
			wsaaSumins = covrsta.getSumins();
			wsaaRiskCessTerm = covrsta.getRcestrm();
			wsaaInstprem = covrsta.getInstprem();
			wsaaStbmthg = covrsta.getZbinstprem();
			wsaaStldmthg = covrsta.getZlinstprem();
			wsaaBonusInd = covrsta.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaStpmth = 0;
			wsaaStvmth = 0;
			wsaaStcmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife = covrsta.getLife();
			wsaaCoverage = covrsta.getCoverage();
			wsaaRider = covrsta.getRider();
			wsaaPlanSuffix = covrsta.getPlnsfx();
			a600ReadRacdsta(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum = wsaaChdrAgntnum;
			wsaaTranno = covrsts.getTranno();
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			if (!"Y".equals(wsaaHdrChg)) {
				wsaaStcmthg = 0;
				wsaaStcmth = 0;
				wsaaStlmth = 0;
			}
			wsaaStvmth = 1;
			processAgents(lifsttrDTO);
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaAgntstat = "N";
			goTo(GotoLabel.covr3020);
		}
		if ((covrsta != null && covrsts.getTranno() > covrsta.getTranno()) && covrsts.getTranno() == lifsttrDTO.getTranno()) {
			if ("Y".equals(wsaaHdrChg)) {
				wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode); 
	            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
	                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
	                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
	            }else{
	            	t6628IO = null;
	                goTo(GotoLabel.covr3020);
	            }
				if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
					goTo(GotoLabel.covr3020);
				}
				wsaaPrevPrem = BigDecimal.ZERO;
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaTranno = wsaaChdrTranno;
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaStpmth = 0;
				wsaaStvmth = 0;
				wsaaStcmth = 0;
				wsaaStsmth = 0;
				wsaaStmmth = 0;
				wsaaStcmthg = 1;
				wsaaStlmth = wsaaLives;
				wsaaHdrAccum = "Y";
				wsaaValidflag = "1";
				headerPremium();
				updateRecord(lifsttrDTO);
				wsaaPrevPrem = BigDecimal.ZERO;
				wsaaAnbAtCcd = covrsta.getAnbAtCcd();
				wsaaSumins = covrsta.getSumins();
				wsaaRiskCessTerm = covrsta.getRcestrm();
				wsaaInstprem = covrsta.getInstprem();
				wsaaStbmthg = covrsta.getZbinstprem();
				wsaaStldmthg = covrsta.getZlinstprem();
				wsaaBonusInd = covrsta.getBnusin();
				wsaaTranno = wsaaChdrTranno;
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaStpmth = 0;
				wsaaStvmth = 0;
				wsaaStcmth = 0;
				wsaaStsmth = 0;
				wsaaStmmth = 0;
				wsaaStcmthg = 1;
				wsaaStlmth = wsaaLives;
				wsaaValidflag = "2";
				previousHeaderPremium();
				updateRecord(lifsttrDTO);;
				wsaaHdrPremium = BigDecimal.ZERO;
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum = wsaaChdrAgntnum;;
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaStvmth = 0;
				wsaaStcmthg = 0;
				wsaaStlmth = 0;
				wsaaStcmth = 1;
				processAgents3600(lifsttrDTO);
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaHdrAccum = "";
				wsaaAgntstat = "N";
			}
			wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode); 
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){//IJTI-1415
                Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
                t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
            }else{
            	t6628IO = null;
                goTo(GotoLabel.covr3020);
            }
			if (t6628IO.getStatcatgs() != null || t6628IO.getStatcatgs().isEmpty()) {
				goTo(GotoLabel.covr3020);
			}
			wsaaPrevPrem = chdrpf.getSinstamt06();
			wsaaValidflag = "1";
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaStpmth = 0;
			wsaaStvmth = 0;
			wsaaStcmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			if ("N".equals(wsaaHdrChg)) {
				wsaaStcmthg = 1;
				wsaaStlmth = wsaaLives;
			}
			wsaaLife = covrsts.getLife();
			wsaaCoverage = covrsts.getCoverage();
			wsaaRider = covrsts.getRider();
			wsaaPlanSuffix = covrsts.getPlnsfx();
			readRacdsts(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaValidflag = "2";
			wsaaAnbAtCcd = covrsta.getAnbAtCcd();
			wsaaSumins = covrsta.getSumins();
			wsaaRiskCessTerm = covrsta.getRcestrm();
			wsaaInstprem = covrsta.getInstprem();
			wsaaStbmthg = covrsta.getZbinstprem();
			wsaaStldmthg = covrsta.getZlinstprem();
			wsaaBonusInd = covrsta.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaStpmth = 0;
			wsaaStvmth = 0;
			wsaaStcmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife = covrsta.getLife();
			wsaaCoverage = covrsta.getCoverage();
			wsaaRider = covrsta.getRider();
			wsaaPlanSuffix = covrsta.getPlnsfx();
			a600ReadRacdsta(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum = wsaaChdrAgntnum;
			wsaaTranno = covrsts.getTranno();
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			if (!"Y".equals(wsaaHdrChg)) {
				wsaaStcmthg = 0;
				wsaaStcmth = 0;
				wsaaStlmth = 0;
			}
			wsaaStvmth = 1;
			processAgents3600(lifsttrDTO);
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaAgntstat = "N";
		}
	}

	protected void covr3020(){
		if ("N".equals(wsaaSttrWritten)) {
			sttrpf.setStcmth(BigDecimal.ONE);
			sttrpf.setStcmthg(BigDecimal.ONE);
			wsaaStcmthg = 1;
			wsaaStcmth = 1;
			continue3021();
		}
		sttrpf.setStcmth(BigDecimal.ZERO);
		sttrpf.setStcmthg(BigDecimal.ZERO);
		wsaaStcmthg = 0;
		wsaaStcmth = 0;
	}

	protected void read3030(LifsttrDTO lifsttrDTO){
        if(covrstsIterator == null || !covrstsIterator.hasNext()){
            goTo(GotoLabel.exit3090);
        }
        covrsts = covrstsIterator.next();
		if ("Y".equals(wsaaSttrWritten)) {
			if (covrsts.getLife().equals(wsaaPrevLife)) {
				wsaaLives = 0;
			}
			else {
				a100CheckNoflives(lifsttrDTO);
				wsaaPrevLife = covrsts.getLife();
			}
		}
		List<Covrpf> covrpfList = covrpfDAO.searchCovrstaRecords(covrsts.getChdrcoy(), covrsts.getChdrnum(), covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(),
        		covrsts.getPlnsfx(),covrsts.getCurrfrom(),covrsts.getTranno());
        if(covrpfList == null || covrpfList.isEmpty()){
        	goTo(GotoLabel.cont3050);
        }
        covrsta = covrpfList.get(0);
	}

	protected void cont3050(LifsttrDTO lifsttrDTO) throws IOException{
		wsaaCurrPrem = wsaaSinstamt06;
		wsaaStvmth = 1;
		if (covrsta == null) {
			if ("Y".equals(wsaaHdrChg)) {
				if (covrsts.getTranno() == lifsttrDTO.getTranno()) {
	                wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode());
	    			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
	    				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
	    			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
	    			}else{
	    				t6628IO = null;
	    			}
					if (t6628IO.getStatcatgs() != null) {
						wsaaPrevPrem = BigDecimal.ZERO;
						wsaaValidflag = "1";
						wsaaAnbAtCcd = covrsts.getAnbAtCcd();
						wsaaSumins = covrsts.getSumins();
						wsaaRiskCessTerm = covrsts.getRcestrm();
						wsaaInstprem = covrsts.getInstprem();
						wsaaStbmthg = covrsts.getZbinstprem();
						wsaaStldmthg = covrsts.getZlinstprem();
						wsaaBonusInd = covrsts.getBnusin();
						wsaaTranno = wsaaChdrTranno;
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaStlmth = wsaaLives;
						wsaaStpmth = 0;
						wsaaStsmth = 0;
						wsaaStmmth = 0;
						wsaaStcmth = 0;
						wsaaStvmth = 0;
						wsaaStraamt = BigDecimal.ZERO;
						wsaaLife = covrsts.getLife();
						wsaaCoverage = covrsts.getCoverage();
						wsaaRider = covrsts.getRider();
						wsaaPlanSuffix = covrsts.getPlnsfx();
						readRacdsts(lifsttrDTO);
						updateRecord(lifsttrDTO);
						wsaaGovtstat = "N";
						wsaaAgntstat = "Y";
						wsaaGovtstat = "N";
						wsaaAgntnum = wsaaChdrAgntnum;
						wsaaTranno = covrsts.getTranno();
						wsaaAnbAtCcd = covrsts.getAnbAtCcd();
						wsaaSumins = covrsts.getSumins();
						wsaaRiskCessTerm = covrsts.getRcestrm();
						wsaaInstprem = covrsts.getInstprem();
						wsaaStbmthg = covrsts.getZbinstprem();
						wsaaStldmthg = covrsts.getZlinstprem();
						wsaaBonusInd = covrsts.getBnusin();
						wsaaStvmth = 1;
						wsaaStcmth = 0;
						processAgents3600(lifsttrDTO);
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaAgntstat = "N";
					}
					goTo(GotoLabel.next3060);
				}
			}
		}
		if (covrsta == null) {
			if ("N".equals(wsaaHdrChg)) {
				if (covrsts.getTranno() == lifsttrDTO.getTranno()) {
                    wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode());
	    			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
	    				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
	    			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
	    			}else{
	    				t6628IO = null;
	    			}
	    			if (t6628IO.getStatcatgs() != null) {
						wsaaValidflag = "1";
						wsaaPrevPrem = BigDecimal.ZERO;
						wsaaAnbAtCcd = covrsts.getAnbAtCcd();
						wsaaSumins = covrsts.getSumins();
						wsaaRiskCessTerm = covrsts.getRcestrm();
						wsaaInstprem = covrsts.getInstprem();
						wsaaStbmthg = covrsts.getZbinstprem();
						wsaaStldmthg = covrsts.getZlinstprem();
						wsaaBonusInd = covrsts.getBnusin();
						wsaaTranno = covrsts.getTranno();
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaStpmth = 0;
						wsaaStsmth = 0;
						wsaaStmmth = 0;
						wsaaStcmth = 0;
						wsaaStvmth = 0;
						wsaaStraamt = BigDecimal.ZERO;
						wsaaStlmth = wsaaLives;
						wsaaLife = covrsts.getLife();
						wsaaCoverage = covrsts.getCoverage();
						wsaaRider = covrsts.getRider();
						wsaaPlanSuffix = covrsts.getPlnsfx();
						readRacdsts(lifsttrDTO);
						updateRecord(lifsttrDTO);
						wsaaGovtstat = "N";
						wsaaAgntstat = "Y";
						wsaaGovtstat = "N";
						wsaaAgntnum = wsaaChdrAgntnum;
						wsaaTranno = covrsts.getTranno();
						wsaaAnbAtCcd = covrsts.getAnbAtCcd();
						wsaaSumins = covrsts.getSumins();
						wsaaRiskCessTerm = covrsts.getRcestrm();
						wsaaInstprem = covrsts.getInstprem();
						wsaaStbmthg = covrsts.getZbinstprem();
						wsaaStldmthg = covrsts.getZlinstprem();
						wsaaBonusInd = covrsts.getBnusin();
						wsaaStvmth = 1;
						wsaaStcmth = 0;
						processAgents3600(lifsttrDTO);
						wsaaAgntnum = "";
						wsaaOvrdcat = "";
						wsaaAgntstat = "N";
	    			}
					goTo(GotoLabel.next3060);
				}
			}
		}
		if ("Y".equals(lifsttrDTO.getMinorChg())) {
            wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode());
			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
			}else{
				t6628IO = null;
			}
			if (t6628IO.getStatcatgs() != null) {
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum = wsaaChdrAgntnum;
				wsaaTranno = lifsttrDTO.getTranno();
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaStcmthg = 0;
				wsaaStlmth = 0;
				wsaaStcmth = 0;
				wsaaStvmth = 1;
				agentCommChange4400(lifsttrDTO);
			}
		}
		if (covrsta == null) {
			goTo(GotoLabel.agent3055);
		}
		if (wsaaBillfreq.compareTo(wsaaBillfreqPrev) != 0 && covrsts.getTranno() == lifsttrDTO.getTranno()) {
			wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode());
			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
			}else{
				t6628IO = null;
                goTo(GotoLabel.next3060);
            }
			if (t6628IO.getStatcatgs() == null || t6628IO.getStatcatgs().isEmpty()) {
				goTo(GotoLabel.next3060);
			}
			wsaaPrevPrem = chdrpf.getSinstamt06();
			wsaaValidflag = "1";
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaStlmth = wsaaLives;
			wsaaStpmth = 0;
			wsaaStvmth = 0;
			wsaaStcmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmthg = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife = covrsts.getLife();
			wsaaCoverage = covrsts.getCoverage();
			wsaaRider = covrsts.getRider();
			wsaaPlanSuffix = covrsts.getPlnsfx();
			readRacdsts(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaValidflag = "2";
			wsaaAnbAtCcd = covrsta.getAnbAtCcd();
			wsaaSumins = covrsta.getSumins();
			wsaaRiskCessTerm = covrsta.getRcestrm();
			wsaaInstprem = covrsta.getInstprem();
			wsaaStbmthg = covrsta.getZbinstprem();
			wsaaStldmthg = covrsta.getZlinstprem();
			wsaaBonusInd = covrsta.getBnusin();
			wsaaTranno = covrsts.getTranno();
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaStpmth = 0;
			wsaaStvmth = 0;
			wsaaStcmth = 0;
			wsaaStsmth = 0;
			wsaaStmmth = 0;
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife = covrsta.getLife();
			wsaaCoverage = covrsta.getCoverage();
			wsaaRider = covrsta.getRider();
			wsaaPlanSuffix = covrsta.getPlnsfx();
			a600ReadRacdsta(lifsttrDTO);
			updateRecord(lifsttrDTO);
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum = wsaaChdrAgntnum;
			wsaaTranno = covrsts.getTranno();
			wsaaAnbAtCcd = covrsts.getAnbAtCcd();
			wsaaSumins = covrsts.getSumins();
			wsaaRiskCessTerm = covrsts.getRcestrm();
			wsaaInstprem = covrsts.getInstprem();
			wsaaStbmthg = covrsts.getZbinstprem();
			wsaaStldmthg = covrsts.getZlinstprem();
			wsaaBonusInd = covrsts.getBnusin();
			if (!"Y".equals(wsaaHdrChg)) {
				wsaaStcmthg = 0;
				wsaaStcmth = 0;
				wsaaStlmth = 0;
			}
			wsaaStvmth = 1;
			processAgents(lifsttrDTO);
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			wsaaAgntstat = "N";
			goTo(GotoLabel.next3060);
		}
		if (covrsts.getTranno() > covrsta.getTranno()&& covrsts.getTranno() == lifsttrDTO.getTranno()) {
			wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(wsaaChdrStatcode);
			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
			}else{
				t6628IO = null;
            }
			if (t6628IO.getStatcatgs() != null && !t6628IO.getStatcatgs().isEmpty()) {
				wsaaValidflag = "1";
				wsaaPrevPrem = chdrpf.getSinstamt06();
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaTranno = covrsts.getTranno();
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaStlmth = wsaaLives;
				wsaaStpmth = 0;
				wsaaStsmth = 0;
				wsaaStmmth = 0;
				wsaaStcmth = 0;
				wsaaStvmth = 0;
				wsaaStraamt = BigDecimal.ZERO;
				wsaaLife = covrsts.getLife();
				wsaaCoverage = covrsts.getCoverage();
				wsaaRider = covrsts.getRider();
				wsaaPlanSuffix = covrsts.getPlnsfx();
				readRacdsts(lifsttrDTO);
				updateRecord(lifsttrDTO);
				wsaaValidflag = "2";
				wsaaAnbAtCcd = covrsta.getAnbAtCcd();
				wsaaSumins = covrsta.getSumins();
				wsaaRiskCessTerm = covrsta.getRcestrm();
				wsaaInstprem = covrsta.getInstprem();
				wsaaStbmthg = covrsta.getZbinstprem();
				wsaaStldmthg = covrsta.getZlinstprem();
				wsaaBonusInd = covrsta.getBnusin();
				wsaaTranno = covrsts.getTranno();
				wsaaStpmth = 0;
				wsaaStsmth = 0;
				wsaaStmmth = 0;
				wsaaStcmth = 0;
				wsaaStvmth = 0;
				wsaaStraamt = BigDecimal.ZERO;
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaLife = covrsta.getLife();
				wsaaCoverage = covrsta.getCoverage();
				wsaaRider = covrsta.getRider();
				wsaaPlanSuffix = covrsta.getPlnsfx();
				if ("N".equals(wsaaHdrChg)) {
					wsaaStcmthg = 0;
					wsaaStlmth = 0;
				}
				a600ReadRacdsta(lifsttrDTO);
				updateRecord(lifsttrDTO);
				wsaaGovtstat = "N";
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum = wsaaChdrAgntnum;
				wsaaTranno = covrsts.getTranno();
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaStcmth = 0;
				wsaaStvmth = 1;
				processAgents3600(lifsttrDTO);
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaAgntstat = "N";
			}
			goTo(GotoLabel.next3060);
		}
	}

	protected void agent3055(LifsttrDTO lifsttrDTO) throws IOException{
		if (!"".equals(lifsttrDTO.getAgntnum())) {
            wsaaT6628Item = lifsttrDTO.getBatctrcde().concat(covrsts.getStatcode());
			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
				Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
			    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
			}else{
				t6628IO = null;
			}
			if (t6628IO.getStatcatgs() != null && !t6628IO.getStatcatgs().isEmpty()) {
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum = lifsttrDTO.getAgntnum();
				wsaaTranno = lifsttrDTO.getTranno();
				wsaaAnbAtCcd = covrsts.getAnbAtCcd();
				wsaaSumins = covrsts.getSumins();
				wsaaRiskCessTerm = covrsts.getRcestrm();
				wsaaInstprem = covrsts.getInstprem();
				wsaaStbmthg = covrsts.getZbinstprem();
				wsaaStldmthg = covrsts.getZlinstprem();
				wsaaBonusInd = covrsts.getBnusin();
				wsaaStvmth = 1;
				wsaaStcmth = 0;
				wsaaStcmthg = 0;
				wsaaStlmth = 0;
				agentChange3900(lifsttrDTO,covrsts);
				wsaaAgntnum = "";
				wsaaOvrdcat = "";
				wsaaAgntstat = "N";
			}
		}
	}

	protected void next3060(){
		goTo(GotoLabel.read3030);
	}
	protected void a100CheckNoflives(LifsttrDTO lifsttrDTO){
        wsaaLives = 0;
        List<Lifepf> lifeList = lifepfDAO.searchLifeRecordByChdrNum(lifsttrDTO.getChdrcoy(), lifsttrDTO.getChdrnum());
        if (lifeList != null && !lifeList.isEmpty()) {
            for (Lifepf lifeIO : lifeList) {
                if (!lifeIO.getLife().equals(covrsts.getLife())) {
                    continue;
                }
                if ("2".equals(lifeIO.getValidflag())) {
                    continue;
                }
                if (lifeIO.getCurrfrom() == covrsts.getCrrcd() || !"IF".equals(lifeIO.getStatcode())){
                    wsaaLives++;
                }
            }
        }
    }
	protected void initialise(LifsttrDTO lifsttrDTO) throws IOException{
		sttrpf = new Sttrpf();
        sttrpf.setCommyr(0);
        sttrpf.setStcmth(BigDecimal.ZERO);
        sttrpf.setStvmth(BigDecimal.ZERO);
        sttrpf.setStpmth(BigDecimal.ZERO);
        sttrpf.setStsmth(BigDecimal.ZERO);
        sttrpf.setStmmth(BigDecimal.ZERO);
        sttrpf.setStimth(BigDecimal.ZERO);
        sttrpf.setStcmthg(BigDecimal.ZERO);
        sttrpf.setStpmthg(BigDecimal.ZERO);
        sttrpf.setStsmthg(BigDecimal.ZERO);
        sttrpf.setStumth(BigDecimal.ZERO);
        sttrpf.setStnmth(BigDecimal.ZERO);
        sttrpf.setTranno(0);
        sttrpf.setTrannor(0);
        sttrpf.setBatcactyr(0);
        sttrpf.setBatcactmn(0);
        sttrpf.setStlmth(BigDecimal.ZERO);
        sttrpf.setStbmthg(BigDecimal.ZERO);
        sttrpf.setStldmthg(BigDecimal.ZERO);
        sttrpf.setStraamt(BigDecimal.ZERO);
        sttrpf.setTransactionDate(0);
        wsaaStcmth = 0;
        wsaaStvmth = 0;
        wsaaStpmth = 0;
        wsaaStsmth = 0;
        wsaaStmmth = 0;
        wsaaStimth = 0;
        wsaaStcmthg = 0;
        wsaaStlmth = 0;
        wsaaStpmthg = 0;
        wsaaStsmthg = 0;
        wsaaStumth = 0;
        wsaaStnmth = 0;
        wsaaStraamt = BigDecimal.ZERO;
        wsaaStbmthg = BigDecimal.ZERO;
        wsaaStldmthg = BigDecimal.ZERO;
        wsaaHdrPremium = BigDecimal.ZERO;
        Itempf itempf = itempfDAO.readSmartTableByTrimItem(lifsttrDTO.getChdrcoy(), "T5679", lifsttrDTO.getBatctrcde(), IT_ITEMPFX);
        t5679IO = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(), T5679.class);
        chdrpf = chdrpfDAO.readRecordOfChdrpf(lifsttrDTO.getChdrcoy(), lifsttrDTO.getChdrnum(), "CH", "1");
      
        if (chdrpf == null) {
        	throw new DataNoFoundException("chdrpf: Item "+lifsttrDTO.getChdrnum()+"not found in Chdrpf while processing contract ");
        }
        wsaaChdrTranno = chdrpf.getTranno();
        wsaaChdrAgntnum = chdrpf.getAgntnum();
        wsaaBillfreq = new BigDecimal(chdrpf.getBillfreq());
        wsaaBillfreqPrev = new BigDecimal(chdrpf.getBillfreq());
        wsaaSinstamt02 = chdrpf.getSinstamt02();
        wsaaSinstamt03 = chdrpf.getSinstamt03();
        wsaaSinstamt04 = chdrpf.getSinstamt04();
        wsaaSinstamt06 = chdrpf.getSinstamt06();
        wsaaChdrpfx = chdrpf.getChdrpfx();
        wsaaCnttype = chdrpf.getCnttype();
        wsaaSrcebus = chdrpf.getSrcebus();
        wsaaChdrStatcode = chdrpf.getStatcode();
        wsaaChdrPstatcode = chdrpf.getPstcde();
        wsaaChdrReg = chdrpf.getReg();
        wsaaChdrCntbranch = chdrpf.getCntbranch();
        wsaaCharBillfreq = chdrpf.getBillfreq();
        aglfpf = aglfpfDAO.searchAglflnb(lifsttrDTO.getChdrcoy(), wsaaChdrAgntnum);
        if(aglfpf == null){
        	throw new DataNoFoundException("aglfpf: Item "+wsaaChdrAgntnum+"not found in AGLFPF while processing contract ");
        }
        if ("Y".equals(lifsttrDTO.getMinorChg())) {
            wsaaStvmth = 0;
        }
        else {
            wsaaStvmth = 1;
        }
        wsaaChdrTranno = chdrpf.getTranno();
        if (chdrpf.getTranno() == lifsttrDTO.getTranno()) {
            checkHeaderChange2100(chdrpf);
        }
        if ("N".equals(wsaaHdrChg)) {
            Regppf regppf = regppfDAO.readRecord(lifsttrDTO.getChdrcoy(), lifsttrDTO.getChdrnum(), "01", "01", "00", 1);
            if (regppf == null) {
                return;
            }
            if (regppf.getTranno() == lifsttrDTO.getTranno()) {
                wsaaHdrChg = "Y";
            }
        }
	}
	
	protected void checkHeaderChange2100(Chdrpf chdrpf){
        wsaaChdrStatcode = chdrpf.getStatcode();
        wsaaChdrReg = chdrpf.getReg();
        wsaaChdrCntbranch = chdrpf.getCntbranch();
        wsaaStatA = chdrpf.getStca();
        wsaaStatB = chdrpf.getStcb();
        wsaaStatC = chdrpf.getStcc();
        wsaaStatD = chdrpf.getStcd();
        wsaaStatE = chdrpf.getStce();
        wsaaHdrChg = "N";
        wsaaStcmth = 0;
        wsaaStcmthg = 0;
        wsaaBillfreqPrev = new BigDecimal(chdrpf.getBillfreq());
	}
	
	private void reverseSttr1000(LifsttrDTO lifsttrDTO) throws IOException{
		sttrrevList = sttrpfDAO.searchSttrpfRecord(lifsttrDTO.getChdrcoy(), lifsttrDTO.getChdrnum());
        wsaaLastTranno = 0;
        wsaaFirstTime = true;
        if (sttrrevList != null && !sttrrevList.isEmpty()) {
        	for (Sttrpf sttrrevIO : sttrrevList) {
        		if (wsaaFirstTime && "Y".equals(sttrrevIO.getStatgov()) && "".equals(sttrrevIO.getRevtrcde())) {
        			wsaaStatcat = sttrrevIO.getStatcat();
                    wsaaFirstTime = false;
                }
        		 if (sttrrevIO.getTranno() < lifsttrDTO.getTrannor() || sttrrevIO.getTranno() >= lifsttrDTO.getTranno()) {
                     break;
                 }
                 if (wsaaLastTranno !=  0 && sttrrevIO.getTranno() != wsaaLastTranno) {
                     break;
                 }
                 if (!"".equals(sttrrevIO.getRevtrcde())) {
                     continue;
                 }
                 move1030(sttrrevIO,lifsttrDTO);
                 if (!sttrrevIO.getStatcat().equals(wsaaStatcat) || !"Y".equals(sttrrevIO.getStatgov())) {
                     continue;
                 }
                 wsaaT6628Item = lifsttrDTO.getBatctrcde().concat("**");
                 if (t6628Map != null && t6628Map.containsKey(wsaaT6628Item)) {
                     t6628IO = smartTableDataUtils.convertGenareaToPojo(t6628Map.get(wsaaT6628Item).get(0).getGenareaj(),T6628.class);
                 }
                 if (null == t6628IO.getStatcatgs() || t6628IO.getStatcatgs().isEmpty()) {
                     continue;
                 }
                 wsaaIdx = 0;
                 loopWrite1040(sttrrevIO,lifsttrDTO);
        	}
        }
	}
	protected void move1030(Sttrpf sttrrevIO,LifsttrDTO lifsttrDTO) throws IOException{
		sttrrevIO.setStcmth(sttrrevIO.getStcmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStvmth(sttrrevIO.getStvmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStpmth(sttrrevIO.getStpmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStsmth(sttrrevIO.getStsmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStmmth(sttrrevIO.getStmmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStimth(sttrrevIO.getStimth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStcmthg(sttrrevIO.getStcmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStpmthg(sttrrevIO.getStpmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStsmthg(sttrrevIO.getStsmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStumth(sttrrevIO.getStumth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStnmth(sttrrevIO.getStnmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStlmth(sttrrevIO.getStlmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStbmthg(sttrrevIO.getStbmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStldmthg(sttrrevIO.getStldmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStraamt(sttrrevIO.getStraamt().multiply(new BigDecimal("-1")));
		wsaaLastTranno = sttrrevIO.getTranno();
        sttrrevIO.setTranno(lifsttrDTO.getTranno());
        sttrrevIO.setBatccoy(lifsttrDTO.getBatccoy());
        sttrrevIO.setBatcbrn(lifsttrDTO.getBatcbrn());
        sttrrevIO.setBatcactyr(lifsttrDTO.getBatcactyr());
        sttrrevIO.setBatcactmn(lifsttrDTO.getBatcactmn());
        sttrrevIO.setBatctrcde(lifsttrDTO.getBatctrcde());
        sttrrevIO.setBatcbatch(lifsttrDTO.getBatcbatch());
		if(insertSttrrevList == null){
		    insertSttrrevList = new ArrayList<>();
		}
		insertSttrrevList.add(sttrrevIO);
	}
	protected void loopWrite1040(Sttrpf sttrrevIO,LifsttrDTO lifsttrDTO) throws IOException{
        do {
            if ("".equals(t6628IO.getStatcatgs().get(wsaaIdx))) {
            	wsaaIdx++;
                continue;
            }
            String itemKey = t6628IO.getStatcatgs().get(wsaaIdx);//IJTI-1415
            if (t6629Map != null && t6629Map.containsKey(itemKey)) {
                t6629IO = smartTableDataUtils.convertGenareaToPojo(t6629Map.get(itemKey).get(0).getGenareaj(),T6629.class);
                if ("Y".equals(t6629IO.getGovtstat())) {
                    continue;
                }
                sttrrevIO.setStatcat(t6628IO.getStatcatgs().get(wsaaIdx));
                sttrrevIO.setStatgov(t6629IO.getGovtstat());
                sttrrevIO.setStcmth(sttrrevIO.getStcmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStvmth(sttrrevIO.getStvmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStpmth(sttrrevIO.getStpmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStsmth(sttrrevIO.getStsmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStmmth(sttrrevIO.getStmmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStimth(sttrrevIO.getStimth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStcmthg(sttrrevIO.getStcmthg().multiply(new BigDecimal("-1")));
                sttrrevIO.setStpmthg(sttrrevIO.getStpmthg().multiply(new BigDecimal("-1")));
                sttrrevIO.setStsmthg(sttrrevIO.getStsmthg().multiply(new BigDecimal("-1")));
                sttrrevIO.setStumth(sttrrevIO.getStumth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStnmth(sttrrevIO.getStnmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStlmth(sttrrevIO.getStlmth().multiply(new BigDecimal("-1")));
                sttrrevIO.setStbmthg(sttrrevIO.getStbmthg().multiply(new BigDecimal("-1")));
                sttrrevIO.setStldmthg(sttrrevIO.getStldmthg().multiply(new BigDecimal("-1")));
                sttrrevIO.setStraamt(sttrrevIO.getStraamt().multiply(new BigDecimal("-1")));
                sttrrevIO.setTranno(lifsttrDTO.getTranno());
                sttrrevIO.setBatccoy(lifsttrDTO.getBatccoy());
                sttrrevIO.setBatcbrn(lifsttrDTO.getBatcbrn());
                sttrrevIO.setBatcactyr(lifsttrDTO.getBatcactyr());
                sttrrevIO.setBatcactmn(lifsttrDTO.getBatcactmn());
                sttrrevIO.setBatctrcde(lifsttrDTO.getBatctrcde());
                sttrrevIO.setBatcbatch(lifsttrDTO.getBatcbatch());
                sttrrevIO.setRevtrcde("");
                if(insertSttrrevList == null){
                    insertSttrrevList = new ArrayList<>();
                }
                insertSttrrevList.add(sttrrevIO);
            }
            wsaaIdx++;
        } while (wsaaIdx < 88);
    }
	protected void headerPremium(){
		wsaaHdrPremium = wsaaBillfreq.multiply(wsaaSinstamt02.add(wsaaSinstamt03).add(wsaaSinstamt04)).setScale(2);
	}
	protected void previousHeaderPremium(){
		wsaaHdrPremium = chdrpf.getSinstamt02().add(chdrpf.getSinstamt03()).add(chdrpf.getSinstamt04()).multiply(wsaaBillfreqPrev).setScale(2);
	}
	protected void readRacdsts(LifsttrDTO lifsttrDTO){
        List<Racdpf> racdList = racdpfDAO.searchRacdstRecord(lifsttrDTO.getBatccoy(),lifsttrDTO.getChdrnum(), wsaaLife,
        		wsaaCoverage, wsaaRider, wsaaPlanSuffix, "1");
        if (racdList != null && !racdList.isEmpty()) {
            for (Racdpf r : racdList) {
                wsaaStraamt = wsaaStraamt.add(r.getRaamount());
            }
        }
        if (BigDecimal.ZERO.compareTo(wsaaStraamt) == 0) {
            racdList = racdpfDAO.searchRacdstRecord(lifsttrDTO.getBatccoy(),lifsttrDTO.getChdrnum(), wsaaLife, wsaaCoverage, wsaaRider,
                    wsaaPlanSuffix, "4");
            if (racdList != null && !racdList.isEmpty()) {
                for (Racdpf r : racdList) {
                    if (r.getRecovamt().compareTo(BigDecimal.ZERO) > 0) {
                        wsaaStraamt = wsaaStraamt.add(r.getRecovamt());
                    } else {
                        wsaaStraamt = wsaaStraamt.add(r.getRaamount());
                    }
                }
            }
        }
    }
	protected void a600ReadRacdsta(LifsttrDTO lifsttrDTO) {
        wsaaRacdstaFirstTime = "Y";
        List<Racdpf> racdList = racdpfDAO.searchRacdstRecord(lifsttrDTO.getBatccoy(),lifsttrDTO.getChdrnum(), wsaaLife,
        		wsaaCoverage, wsaaRider, wsaaPlanSuffix, "1");

        if (racdList != null && !racdList.isEmpty()) {
            for (Racdpf r : racdList) {
                if ("Y".equals(wsaaRacdstaFirstTime)) {
                    wsaaRacdstaLastTranno = r.getTranno();
                    wsaaRacdstaFirstTime = "N";
                } else {
                    if (r.getTranno() != wsaaRacdstaLastTranno) {
                        break;
                    }
                }
                wsaaStraamt = wsaaStraamt.add(r.getRaamount());
            }
        }
    }
	protected void agentCommChange4400(LifsttrDTO lifsttrDTO) throws IOException
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start4410();
				}
				case check4420: {
					check4420(lifsttrDTO);
				}
				case next4450: {
					next4450();
				}
				case exit4490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start4410(){
        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "1");
        
        if(agcmstsList!=null){
            agcmstsIterator = agcmstsList.iterator();
        }
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.exit4490);
	    }
	    agcmsts = agcmstsIterator.next();
		wsaaAgntnum = agcmsts.getAgntnum();
	}
	
	protected void check4420(LifsttrDTO lifsttrDTO) throws IOException{
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.exit4490);
        }
		if (agcmsts.getTranno() <= covrsts.getTranno()) {
			goTo(GotoLabel.next4450);
		}
		if (agcmsts.getTranno() != lifsttrDTO.getTranno()) {
			goTo(GotoLabel.next4450);
		}
		singlePremAgents5100(lifsttrDTO);
	}
	
	protected void next4450(){
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.exit4490);
        }
        agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.check4420);
	}
	
	protected void checkTranscation(LifsttrDTO lifsttrDTO) throws IOException{   
		Itempf item = itempfDAO.readSmartTableByTrimItem(lifsttrDTO.getChdrcoy(),"TJ668",lifsttrDTO.getBatctrcde(),"IT");
		if(item == null){
			return;
		}
	    tj688IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),Tj688.class);
		wsaaLastTranno = lifsttrDTO.getTranno() - 1;
		sttrrevList = sttrpfDAO.searchSttrpfRecord(lifsttrDTO.getChdrcoy(), lifsttrDTO.getChdrnum());
		if (sttrrevList != null && !sttrrevList.isEmpty()) {
            for (Sttrpf sttrrevIO : sttrrevList) {
                if (sttrrevIO.getTranno() !=  wsaaLastTranno) {
                    continue;
                }
                wsaaFound = "";
                for (wsaaIdx = 0 ; wsaaIdx < 15 || "Y".equals(wsaaFound); wsaaIdx++) {
                    if (sttrrevIO.getBatctrcde().equals(tj688IO.getTrcodes().get(wsaaIdx))) {
                        wsaaFound = "Y";
                        a200CheckTrans(sttrrevIO,lifsttrDTO);
                    }
                }
            }
        }
	}
	protected void a200CheckTrans(Sttrpf sttrrevIO,LifsttrDTO lifsttrDTO) throws IOException
	{
		wsaaT6628Item = sttrrevIO.getBatctrcde().concat("**");
        if (t6628Map != null && t6628Map.containsKey(wsaaT6628Item)) {
        	Itempf item = t6628Map.get(wsaaT6628Item).get(0);//IJTI-1415
		    t6628IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6628.class);
        }else{
        	t6628IO = null;
            return;
        }
		
		if (t6628IO.getStatcatgs() == null) {
			return;
		}
		wsaaFound = "";
		for (wsaaIdx = 0 ; wsaaIdx < 88 || "Y".equals(wsaaFound); wsaaIdx++) {
			if (t6628IO.getStatcatgs().get(wsaaIdx).equals(sttrrevIO.getStatcat())) {
				wsaaFound = "Y";
			}
		}
		if ("Y".equals(wsaaFound)) {
		    return;
		}
		sttrrevIO.setRevtrcde(sttrrevIO.getBatctrcde());
		sttrrevIO.setStcmth(sttrrevIO.getStcmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStvmth(sttrrevIO.getStvmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStpmth(sttrrevIO.getStpmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStsmth(sttrrevIO.getStsmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStmmth(sttrrevIO.getStmmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStimth(sttrrevIO.getStimth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStcmthg(sttrrevIO.getStcmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStpmthg(sttrrevIO.getStpmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStsmthg(sttrrevIO.getStsmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStumth(sttrrevIO.getStumth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStnmth(sttrrevIO.getStnmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStlmth(sttrrevIO.getStlmth().multiply(new BigDecimal("-1")));
		sttrrevIO.setStbmthg(sttrrevIO.getStbmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStldmthg(sttrrevIO.getStldmthg().multiply(new BigDecimal("-1")));
		sttrrevIO.setStraamt(sttrrevIO.getStraamt().multiply(new BigDecimal("-1")));
		sttrrevIO.setTranno(lifsttrDTO.getTranno());
		sttrrevIO.setBatccoy(lifsttrDTO.getBatccoy());
		sttrrevIO.setBatcbrn(lifsttrDTO.getBatcbrn());
		sttrrevIO.setBatcactyr(lifsttrDTO.getBatcactyr());
		sttrrevIO.setBatcactmn(lifsttrDTO.getBatcactmn());
		sttrrevIO.setBatcbatch(lifsttrDTO.getBatcbatch());
		sttrrevIO.setBatctrcde(lifsttrDTO.getBatctrcde());
		
		if(insertSttrrevList== null){
		    insertSttrrevList = new ArrayList<>();
		}
		insertSttrrevList.add(sttrrevIO);
	}
	protected void singlePremAgents5100(LifsttrDTO lifsttrDTO) throws IOException{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5110();
					read5115();
				}
				case loop5120: {
					loop5120(lifsttrDTO);
				}
				case next5150: {
					next5150();
				}
				case cont5160: {
					cont5160(lifsttrDTO);
				}
				case next5170: {
					next5170();
				}
				case exit5195: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void start5110(){
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaStcmth = 0;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		
        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "1");
        if(agcmstsList!=null){
            agcmstsIterator = agcmstsList.iterator();
        }
	}

	protected void read5115(){
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.cont5160);
	    }
	    agcmsts = agcmstsIterator.next();
		wsaaAgntnum = agcmsts.getAgntnum();
	}

	protected void loop5120(LifsttrDTO lifsttrDTO){
		if (agcmsts.getTranno() != lifsttrDTO.getTranno()) {
			goTo(GotoLabel.next5150);
		}
		if (!agcmsts.getAgntnum().equals(wsaaAgntnum)) {
			goTo(GotoLabel.cont5160);
		}
		wsaaOvrdcat = agcmsts.getOvrdcat();
		if (!"Y".equals(agcmsts.getDormflag())) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
		}
		if (!"Y".equals(agcmsts.getDormflag())) {
			wsaaAnnpremCurr = wsaaAnnpremCurr.add(agcmsts.getAnnprem());
			wsaaAnnCurrent = wsaaAnnCurrent.add(agcmsts.getInitcom());
		}
	}

	protected void next5150(){
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont5160);
        }
        agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.loop5120);
	}

	protected void cont5160(LifsttrDTO lifsttrDTO) throws IOException{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next5170);
		}
		wsaaCommission = wsaaAnnTotCurrent;
		wsaaAnnprem = wsaaAnnTotPrem;
		wsaaValidflag = "1";
		wsaaAgntstat = "Y";
		wsaaGovtstat = "N";
		updateRecord(lifsttrDTO);
		wsaaAnnprem = wsaaAnnTotPrem.subtract(wsaaAnnpremCurr).setScale(2);
		wsaaCommission = wsaaAnnTotCurrent.subtract(wsaaAnnCurrent).setScale(2);
		wsaaValidflag = "2";
		wsaaAgntstat = "Y";
		wsaaGovtstat = "N";
		updateRecord(lifsttrDTO);
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
	}

	protected void next5170(){
		wsaaOvrdcat = agcmsts.getOvrdcat();
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			wsaaCommission = BigDecimal.ZERO;
			wsaaAnnTotPrem = BigDecimal.ZERO;
			wsaaAnnpremCurr = BigDecimal.ZERO;
			wsaaAnnpremPrev = BigDecimal.ZERO;
			wsaaAnnprem = BigDecimal.ZERO;
			wsaaAnnTotCurrent = BigDecimal.ZERO;
			wsaaAnnCurrent = BigDecimal.ZERO;
			wsaaAnnPrevious = BigDecimal.ZERO;
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			goTo(GotoLabel.exit5195);
		}
		wsaaAgntnum = agcmsts.getAgntnum();
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		goTo(GotoLabel.loop5120);
	}
	
	protected void updateRecord(LifsttrDTO lifsttrDTO) throws IOException
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3110();
				}
				case loop3120: {
					loop3120();
					cont3125(lifsttrDTO);
				}
				case next3130: {
					next3130(lifsttrDTO);
				}
				case loop3140: {
					loop3140(lifsttrDTO);
					next3150();
				}
				case next3160: {
					next3160();
				}
				case exit3190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void start3110(){
		wsaaBandage = "";
		wsaaBandsa = "";
		wsaaBandtrm = "";
		wsaaBandprm = "";
		wsaaAgntage = "";
		wsaaAgntsa = "";
		wsaaAgnttrm = "";
		wsaaAgntprm = "";
		wsaaGovtage = "";
		wsaaGovtsa = "";
		wsaaGovttrm = "";
		wsaaGovtprm = "";
		if ("1".equals(wsaaValidflag)) {
			wsaaSub = 22;
		}
		else {
			wsaaSub = 0;
		}
	}

	protected void loop3120() throws IOException{
	    wsaaSub++;
	    if (wsaaSub> 44) {
	        goTo(GotoLabel.next3130);
	    }
	    wsaaTableSub = wsaaSub;
	    if ("1".equals(wsaaValidflag)) {
	        if (subtractCurrent.indexOf(String.valueOf(wsaaTableSub)) < 0 && addCurrent.indexOf(String.valueOf(wsaaTableSub)) <0) {
	            goTo(GotoLabel.next3130);
	        }
	    }
        if ("2".equals(wsaaValidflag)) {
            if (subtractPrevious.indexOf(String.valueOf(wsaaTableSub)) < 0 && addPrevious.indexOf(String.valueOf(wsaaTableSub)) <0) {
	            goTo(GotoLabel.next3130);
	        }
	    }
        if ("".equals(t6628IO.getStatcatgs().get(wsaaSub))) {
	        goTo(GotoLabel.loop3120);
	    }
        String itemitem = t6628IO.getStatcatgs().get(wsaaSub);
        if (t6629Map != null && t6629Map.containsKey(itemitem)) {
            Itempf item = t6629Map.get(itemitem).get(0);
            t6629IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6629.class);
        } else {
        	t6629IO = null;
	        goTo(GotoLabel.loop3120);
	    }
        if (!"Y".equals(t6629IO.getAgntstat()) && !"Y".equals(t6629IO.getGovtstat())) {
	        goTo(GotoLabel.loop3120);
	    }
        if ("Y".equals(wsaaGovtstat) && !"Y".equals(t6629IO.getGovtstat())) {
	        goTo(GotoLabel.loop3120);
	    }
        if ("Y".equals(wsaaAgntstat) && !"Y".equals(t6629IO.getAgntstat())) {
	        goTo(GotoLabel.loop3120);
	    }
	}
	
	protected void next3130(LifsttrDTO lifsttrDTO){
		wsaaBandage = "";
		wsaaBandsa = "";
		wsaaBandtrm = "";
		wsaaBandprm = "";
		wsaaAgntage = "";
		wsaaAgntsa = "";
		wsaaAgnttrm = "";
		wsaaAgntprm = "";
		wsaaGovtage = "";
		wsaaGovtsa = "";
		wsaaGovttrm = "";
		wsaaGovtprm = "";
		if (covrsta == null && ("Y".equals((wsaaHdrChg)) || "N".equals((wsaaHdrChg))) && covrsts.getTranno() == lifsttrDTO.getTranno()) {
			goTo(GotoLabel.exit3190);
		}
		
		if ("Y".equals((wsaaHdrChg))) {
			goTo(GotoLabel.exit3190);
		}
		if (!"".equals(lifsttrDTO.getAgntnum())) {
			goTo(GotoLabel.exit3190);
		}
		if (wsaaCurrPrem.compareTo(wsaaPrevPrem) >= 0) {
			wsaaSub = 44;
		}
		else {
			wsaaSub = 66;
		}
	}

	protected void loop3140(LifsttrDTO lifsttrDTO) throws IOException{
		wsaaSub++;
		if (wsaaSub > 88) {
			goTo(GotoLabel.next3160);
		}
		wsaaTableSub = wsaaSub;
		if (wsaaCurrPrem.compareTo(wsaaPrevPrem) == -1 && wsaaSub < 66) {
			goTo(GotoLabel.next3160);
		}
		if (wsaaCurrPrem.compareTo(wsaaPrevPrem) >= 0 && wsaaSub > 66){
			goTo(GotoLabel.next3160);
		}
		if ("Y".equals(lifsttrDTO.getMinorChg()) && wsaaSub > 66){
			goTo(GotoLabel.next3160);
		}
		if (t6628IO == null || t6628IO.getStatcatgs() == null || t6628IO.getStatcatgs().isEmpty()) {
			goTo(GotoLabel.loop3140);
		}
		String itemitem = t6628IO.getStatcatgs().get(wsaaSub);
        if (t6629Map != null && t6629Map.containsKey(itemitem)) {
            Itempf item = t6629Map.get(itemitem).get(0);
            t6629IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6629.class);
        } else {
        	t6629IO = null;
	        goTo(GotoLabel.loop3140);
	    }
        if (!"Y".equals(t6629IO.getAgntstat()) && !"Y".equals(t6629IO.getGovtstat())) {
	        goTo(GotoLabel.loop3140);
	    }
        if ("Y".equals(wsaaGovtstat) && !"Y".equals(t6629IO.getGovtstat())) {
	        goTo(GotoLabel.loop3140);
	    }
        if ("Y".equals(wsaaAgntstat) && !"Y".equals(t6629IO.getAgntstat())) {
	        goTo(GotoLabel.loop3140);
	    }
		generateMovements3300(lifsttrDTO);
		writeSttrRecord3800(lifsttrDTO);
	}

	protected void next3150(){
		goTo(GotoLabel.loop3140);
	}
	
	protected void next3160(){
		wsaaAnbAtCcd = 0;
		wsaaSumins = BigDecimal.ZERO;
		wsaaRiskCessTerm = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaInstprem = BigDecimal.ZERO;
	}
	
	protected void cont3125(LifsttrDTO lifsttrDTO) throws IOException {
		generateMovements3300(lifsttrDTO);
		writeSttrRecord3800(lifsttrDTO);
		goTo(GotoLabel.loop3120);
	}
	protected void writeSttrRecord3800(LifsttrDTO lifsttrDTO) throws IOException{
        sttrpf.setBandage("");
        sttrpf.setBandageg("");
        sttrpf.setBandsa("");
        sttrpf.setBandsag("");
        sttrpf.setBandprm("");
        sttrpf.setBandprmg("");
        sttrpf.setBandtrm("");
        sttrpf.setCnPremStat("");
        sttrpf.setRevtrcde("");
        sttrpf.setBandtrmg("");
        sttrpf.setCommyr(0);
        sttrpf.setStmmth(BigDecimal.ZERO);
        sttrpf.setStumth(BigDecimal.ZERO);
        sttrpf.setStnmth(BigDecimal.ZERO);
        sttrpf.setStimth(BigDecimal.ZERO);
        sttrpf.setStvmth(BigDecimal.ZERO);
        sttrpf.setStsmth(BigDecimal.ZERO);
        sttrpf.setStsmthg(BigDecimal.ZERO);
        sttrpf.setStpmth(BigDecimal.ZERO);
        sttrpf.setStpmthg(BigDecimal.ZERO);
        sttrpf.setTranno(0);
        sttrpf.setTrannor(0);
        sttrpf.setBatcactyr(0);
        sttrpf.setBatcactmn(0);
        sttrpf.setPlanSuffix(0);
        sttrpf.setStlmth(BigDecimal.ZERO);
        sttrpf.setStbmthg(BigDecimal.ZERO);
        sttrpf.setStldmthg(BigDecimal.ZERO);
        sttrpf.setStraamt(BigDecimal.ZERO);
        sttrpf.setTransactionDate(0);
        sttrpf.setCnPremStat(wsaaChdrPstatcode);
        sttrpf.setBatccoy(lifsttrDTO.getBatccoy());
        sttrpf.setBatcbrn(lifsttrDTO.getBatcbrn());
        sttrpf.setBatcactyr(lifsttrDTO.getBatcactyr());
        sttrpf.setBatcactmn(lifsttrDTO.getBatcactmn());
        sttrpf.setBatctrcde(lifsttrDTO.getBatctrcde());
        sttrpf.setBatcbatch(lifsttrDTO.getBatcbatch());
        sttrpf.setReptcd01(covrsts.getReptcd01());
        sttrpf.setReptcd02(covrsts.getReptcd02());
        sttrpf.setReptcd03(covrsts.getReptcd03());
        sttrpf.setReptcd04(covrsts.getReptcd04());
        sttrpf.setReptcd05(covrsts.getReptcd05());
        sttrpf.setReptcd06(covrsts.getReptcd06());
        sttrpf.setChdrpfx(wsaaChdrpfx);//IJTI-1415
        sttrpf.setChdrcoy(covrsts.getChdrcoy());
        sttrpf.setChdrnum(covrsts.getChdrnum());
        sttrpf.setLife(covrsts.getLife());
        sttrpf.setCoverage(covrsts.getCoverage());
        sttrpf.setRider(covrsts.getRider());
        sttrpf.setPlanSuffix(covrsts.getPlnsfx());
        sttrpf.setTranno(wsaaTranno);
        sttrpf.setAgntnum(wsaaAgntnum);//IJTI-1415
        sttrpf.setOvrdcat(wsaaOvrdcat);//IJTI-1415
        sttrpf.setAracde(aglfpf.getAracde());
        sttrpf.setCntbranch(wsaaChdrCntbranch);//IJTI-1415
        sttrpf.setCnttype(wsaaCnttype);//IJTI-1415
        sttrpf.setCrtable(covrsts.getCrtable());
        sttrpf.setCommyr(covrsts.getCrrcd());
        sttrpf.setPstatcode(covrsts.getPstatcode());
        sttrpf.setCntcurr(covrsts.getPrmcur());
        sttrpf.setRegister(wsaaChdrReg);//IJTI-1415
        if (!sttrpf.getCntcurr().equals(wsaaPrevCntcurr)) {
        	Itempf item = t3629Map.get(sttrpf.getCntcurr()).get(0);
            t3629IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T3629.class);
            wsaaPrevCntcurr = sttrpf.getCntcurr();
        }
        sttrpf.setAcctccy(t3629IO.getLedgcurr());
        sttrpf.setStatFund(covrsts.getStfund());
        sttrpf.setStatSect(covrsts.getStsect());
        sttrpf.setStatSubsect(covrsts.getStssect());
        sttrpf.setSrcebus(wsaaSrcebus);//IJTI-1415
        sttrpf.setBillfreq(wsaaCharBillfreq);//IJTI-1415
        sttrpf.setSex(covrsts.getSex());
        sttrpf.setMortcls(covrsts.getMortcls());
        sttrpf.setChdrstcda(wsaaStatA);//IJTI-1415
        sttrpf.setChdrstcdb(wsaaStatB);//IJTI-1415
        sttrpf.setChdrstcdc(wsaaStatC);//IJTI-1415
        sttrpf.setChdrstcdd(wsaaStatD);//IJTI-1415
        sttrpf.setChdrstcde(wsaaStatE);//IJTI-1415
        sttrpf.setStatcat(t6628IO.getStatcatgs().get(wsaaSub));
        sttrpf.setTransactionDate(covrsts.getTrdt());
        sttrpf.setStatagt("N");
        sttrpf.setStatgov("N");
        if ("Y".equals(t6629IO.getAgntstat())) {
            if ("Y".equals(wsaaAgntstat)) {
                sttrpf.setStatagt("Y");
                wsaaStmmth = Integer.parseInt(wsaaCommission.toString());
                wsaaStimth = 0;
                sttrpf.setStimth(BigDecimal.ZERO);
                sttrpf.setBandage(wsaaAgntage);//IJTI-1415
                sttrpf.setBandsa(wsaaAgntsa);//IJTI-1415
                sttrpf.setBandtrm(wsaaAgnttrm);//IJTI-1415
                sttrpf.setBandprm(wsaaAgntprm);//IJTI-1415
            } else {
                sttrpf.setStatagt("N");
                wsaaStmmth = 0;
                sttrpf.setStimth(BigDecimal.ZERO);
                wsaaStsmth = 0;
                wsaaStpmth = 0;
                wsaaStvmth = 0;
                sttrpf.setBandage("");
                sttrpf.setBandsa("");
                sttrpf.setBandtrm("");
                sttrpf.setBandprm("");
            }
        }
        if ("Y".equals(t6629IO.getGovtstat())) {
            if ("Y".equals(wsaaGovtstat)) {
                sttrpf.setStatgov("Y");
                wsaaStimth = Integer.parseInt(wsaaSumins.toString());; 
                sttrpf.setBandageg(wsaaGovtage);//IJTI-1415
                sttrpf.setBandsag(wsaaGovtsa);//IJTI-1415
                sttrpf.setBandtrmg(wsaaGovttrm);//IJTI-1415
                sttrpf.setBandprmg(wsaaGovtprm);//IJTI-1415
            } else {
                sttrpf.setStatgov("N");
                wsaaStimth = 0;
                sttrpf.setStimth(BigDecimal.ZERO);
                wsaaStpmthg = 0;
                wsaaStsmthg = 0;
                wsaaStbmthg = BigDecimal.ZERO;
                wsaaStldmthg =  BigDecimal.ZERO;
                wsaaStraamt = BigDecimal.ZERO;
                sttrpf.setBandageg("");
                sttrpf.setBandsag("");
                sttrpf.setBandtrmg("");
                sttrpf.setBandprmg("");
            }
        }
        if ("Y".equals(wsaaHdrAccum)) {
            wsaaStpmth = 0;
            wsaaStsmth = 0;
            wsaaStsmthg = 0;
            wsaaStimth = 0;
            wsaaStmmth = 0;
            wsaaStvmth = 0;
            wsaaStbmthg =  BigDecimal.ZERO;
            wsaaStldmthg =  BigDecimal.ZERO;
            wsaaStraamt = BigDecimal.ZERO;
            wsaaStpmthg = Integer.parseInt(wsaaHdrPremium.toString());;
        }
        if (subtractPrevious.indexOf(wsaaTableSub) > 0 || subtractCurrent.indexOf(wsaaTableSub) > 0 ||(subtractIncrease.indexOf(wsaaTableSub) > 0 && 
        		"1".equals(wsaaValidflag)) || (subtractIncrease.indexOf(wsaaTableSub) > 0 && "2".equals(wsaaValidflag)) ||
        		(subtractDecrease.indexOf(wsaaTableSub) > 0 && "1".equals(wsaaValidflag)) || (addDecrease.indexOf(wsaaTableSub) > 0 && "2".equals(wsaaValidflag))) {

            sttrpf.setStcmth(new BigDecimal(wsaaStcmth).multiply(new BigDecimal(-1)));
            sttrpf.setStvmth(new BigDecimal(wsaaStvmth).multiply(new BigDecimal(-1)));
            sttrpf.setStpmth(new BigDecimal(wsaaStpmth).multiply(new BigDecimal(-1)));
            sttrpf.setStsmth(new BigDecimal(wsaaStsmth).multiply(new BigDecimal(-1)));
            sttrpf.setStmmth(new BigDecimal(wsaaStmmth).multiply(new BigDecimal(-1)));
            sttrpf.setStimth(new BigDecimal(wsaaStimth).multiply(new BigDecimal(-1)));
            sttrpf.setStcmthg(new BigDecimal(wsaaStcmthg).multiply(new BigDecimal(-1)));
            sttrpf.setStpmthg(new BigDecimal(wsaaStpmthg).multiply(new BigDecimal(-1)));
            sttrpf.setStlmth(new BigDecimal(wsaaStlmth).multiply(new BigDecimal(-1)));
            sttrpf.setStbmthg(wsaaStbmthg.multiply(new BigDecimal(-1)));
            sttrpf.setStldmthg(wsaaStldmthg.multiply(new BigDecimal(-1)));
            sttrpf.setStraamt(wsaaStraamt.multiply(new BigDecimal(-1)));
            sttrpf.setStsmthg(new BigDecimal(wsaaStsmthg).multiply(new BigDecimal(-1)));
            sttrpf.setStumth(new BigDecimal(wsaaStumth).multiply(new BigDecimal(-1)));
            sttrpf.setStnmth(new BigDecimal(wsaaStnmth).multiply(new BigDecimal(-1)));
            return ;
        }
        
        sttrpf.setStcmth(new BigDecimal(wsaaStcmth));
        sttrpf.setStvmth(new BigDecimal(wsaaStvmth));
        sttrpf.setStpmth(new BigDecimal(wsaaStpmth));
        sttrpf.setStsmth(new BigDecimal(wsaaStsmth));
        sttrpf.setStmmth(new BigDecimal(wsaaStmmth));
        sttrpf.setStimth(new BigDecimal(wsaaStimth));
        sttrpf.setStcmthg(new BigDecimal(wsaaStcmthg));
        sttrpf.setStpmthg(new BigDecimal(wsaaStpmthg));
        sttrpf.setStlmth(new BigDecimal(wsaaStlmth));
        sttrpf.setStbmthg(wsaaStbmthg);
        sttrpf.setStldmthg(wsaaStldmthg);
        sttrpf.setStraamt(wsaaStraamt);
        sttrpf.setStsmthg(new BigDecimal(wsaaStsmthg));
        sttrpf.setStumth(new BigDecimal(wsaaStumth));
        sttrpf.setStnmth(new BigDecimal(wsaaStnmth));
        if (!"".equals(wsaaBonusInd)) {
			sttrpf.setParind("P");
		}
		else {
			if(t5540Map!=null&&t5540Map.containsKey(covrsts.getCrtable())){
			    sttrpf.setParind("P");
			}else{
			    sttrpf.setParind("N");
			}
		}
		wsaaSttrWritten = "Y";
		if(insertSttrpfList == null){
		    insertSttrpfList = new ArrayList<Sttrpf>();
		}
		insertSttrpfList.add(new Sttrpf(sttrpf));
	}
	protected void generateMovements3300(LifsttrDTO lifsttrDTO) throws IOException{
        wsaaBandage = "";
        wsaaBandsa = "";
        wsaaBandtrm = "";
        wsaaBandprm = "";
        wsaaAgntage = "";
        wsaaAgntsa = "";
        wsaaAgnttrm = "";
        wsaaAgntprm = "";
        if (!"Y".equals(wsaaAgntstat)) {
            wsaaStcmth = 0;
            return;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getAgebands().get(0))) {
            Itempf item = t6627Map.get(t6629IO.getAgebands().get(0)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulateAge4000();
        } else {
        	t6627IO = null;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getSumbands().get(0))) {
            Itempf item = t6627Map.get(t6629IO.getSumbands().get(0)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulateSum4100();
        } else {
        	t6627IO = null;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getRskbands().get(0))) {
            Itempf item = t6627Map.get(t6629IO.getRskbands().get(0)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulateRsk4200();
        } else {
        	t6627IO = null;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getPrmbands().get(0))) {
            Itempf item = t6627Map.get(t6629IO.getPrmbands().get(0)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulatePrm4300();
        } else {
        	t6627IO = null;
        }

        if ("Y".equals(lifsttrDTO.getMinorChg())) {
            wsaaStsmth = Integer.parseInt(wsaaAnnprem.toString());
        } else {
            wsaaStpmth = Integer.parseInt(wsaaAnnprem.toString());
        }
        wsaaAgntage = wsaaBandage;
        wsaaAgntsa = wsaaBandsa;
        wsaaAgntprm = wsaaBandprm;
        wsaaAgnttrm = wsaaBandtrm;
        wsaaBandage = "";
        wsaaBandsa = "";
        wsaaBandtrm = "";
        wsaaBandprm = "";
        wsaaGovtage = "";
        wsaaGovtsa = "";
        wsaaGovttrm = "";
        wsaaGovtprm = "";
        if (!"Y".equals(wsaaGovtstat)) {
            wsaaStcmthg = 0;
            wsaaStlmth = 0;
            return;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getAgebands().get(1))) {
            Itempf item = t6627Map.get(t6629IO.getAgebands().get(1)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulateAge4000();
        } else {
           	t6627IO = null;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getSumbands().get(1))) {
            Itempf item = t6627Map.get(t6629IO.getSumbands().get(1)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulateSum4100();
        } else {
        	t6627IO = null;
        }

        wsaaStimth = Integer.parseInt(wsaaSumins.toString());
        if (t6627Map != null && t6627Map.containsKey(t6629IO.getRskbands().get(1))) {
            Itempf item = t6627Map.get(t6629IO.getRskbands().get(1)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            accumulateRsk4200();
        } else {
        	t6627IO = null;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629IO.getPrmbands().get(1))) {
            Itempf item = t6627Map.get(t6629IO.getPrmbands().get(1)).get(0);
            t6627IO = smartTableDataUtils.convertGenareaToPojo(item.getGenareaj(),T6627.class);
            if ("2".equals(wsaaValidflag) && "Y".equals(wsaaHdrChg)) {
                wsaaPremium = wsaaInstprem.multiply(wsaaBillfreqPrev).setScale(2);
            } else {
                wsaaPremium = wsaaInstprem.multiply(wsaaBillfreq).setScale(2);
            }
            accumulatePrm4300();
        } else {
        	t6627IO = null;
        }
        if ("2".equals(wsaaValidflag) &&  "Y".equals(wsaaHdrChg)) {
            wsaaPremium = wsaaInstprem.multiply(wsaaBillfreqPrev).setScale(2);
        } else {
            wsaaPremium = wsaaInstprem.multiply(wsaaBillfreq).setScale(2);
        }
        wsaaStsmthg = Integer.parseInt(covrsts.getSingp().toString());
        wsaaStpmthg = Integer.parseInt(wsaaPremium.toString());
        wsaaStpmthg = Integer.parseInt(wsaaPremium.toString()) + wsaaStpmthg;
        wsaaGovtage = (wsaaBandage);
        wsaaGovtsa = wsaaBandsa;
        wsaaGovtprm = wsaaBandprm;
        wsaaGovttrm = wsaaBandtrm;
    }
	
	protected void accumulateAge4000(){
		wsaaBandage = "";
		wsaaSub1 = 1;
		while ( wsaaSub1 != 16) {
			if ((new BigDecimal(wsaaAnbAtCcd).compareTo(t6627IO.getStatfroms().get(wsaaSub1)) == 0
			|| new BigDecimal(wsaaAnbAtCcd).compareTo(t6627IO.getStatfroms().get(wsaaSub1)) > 0 )
			&& 
			(new BigDecimal(wsaaAnbAtCcd).compareTo(t6627IO.getStatto().get(wsaaSub1)) == 0
			|| new BigDecimal(wsaaAnbAtCcd).compareTo(t6627IO.getStatto().get(wsaaSub1)) > 0 )) {
				wsaaBandage = t6627IO.getStatbands().get(wsaaSub1);
				wsaaSub1 = 16;
			}
			else {
				wsaaSub1++;
				wsaaBandage = "";
			}
		}
	}

protected void accumulateSum4100(){
		wsaaBandage = "";
		wsaaSub1 = 1;
		while ( wsaaSub1 != 16) {
			if ((wsaaSumins.compareTo(t6627IO.getStatfroms().get(wsaaSub1)) == 0
			|| wsaaSumins.compareTo(t6627IO.getStatfroms().get(wsaaSub1)) > 0 )
			&& 
			(wsaaSumins.compareTo(t6627IO.getStatto().get(wsaaSub1)) == 0
			|| wsaaSumins.compareTo(t6627IO.getStatto().get(wsaaSub1)) > 0 )) {
				wsaaBandage = t6627IO.getStatbands().get(wsaaSub1);
				wsaaSub1 = 16;
			}
			else {
				wsaaSub1++;
				wsaaBandage = "";
			}
		}
	}

protected void accumulateRsk4200(){
		wsaaBandage = "";
		wsaaSub1 = 1;
		while ( wsaaSub1 != 16) {
			if ((new BigDecimal(wsaaRiskCessTerm).compareTo(t6627IO.getStatfroms().get(wsaaSub1)) == 0
			|| new BigDecimal(wsaaRiskCessTerm).compareTo(t6627IO.getStatfroms().get(wsaaSub1)) > 0 )
			&& 
			(new BigDecimal(wsaaRiskCessTerm).compareTo(t6627IO.getStatto().get(wsaaSub1)) == 0
			|| new BigDecimal(wsaaRiskCessTerm).compareTo(t6627IO.getStatto().get(wsaaSub1)) > 0 )) {
				wsaaBandage = t6627IO.getStatbands().get(wsaaSub1);
				wsaaSub1 = 16;
			}
			else {
				wsaaSub1++;
				wsaaBandage = "";
			}
		}
	}

	protected void accumulatePrm4300(){
		wsaaBandage = "";
		wsaaSub1 = 1;
		while ( wsaaSub1 != 16) {
			if ((wsaaPremium.compareTo(t6627IO.getStatfroms().get(wsaaSub1)) == 0
			|| wsaaPremium.compareTo(t6627IO.getStatfroms().get(wsaaSub1)) > 0 )
			&& 
			(wsaaPremium.compareTo(t6627IO.getStatto().get(wsaaSub1)) == 0
			|| wsaaPremium.compareTo(t6627IO.getStatto().get(wsaaSub1)) > 0 )) {
				wsaaBandage = t6627IO.getStatbands().get(wsaaSub1);
				wsaaSub1 = 16;
			}
			else {
				wsaaSub1++;
				wsaaBandage = "";
			}
		}
	}
	protected void processAgents3600(LifsttrDTO lifsttrDTO) throws IOException{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3610();
					read3615();
				}
				case loop3620: {
					loop3620();
				}
				case next3650: {
					next3650();
				}
				case cont3660: {
					cont3660(lifsttrDTO);
				}
				case next3670: {
					next3670();
				}
				case exit3695: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void start3610(){
		wsaaStcmthg = 0;
        wsaaStlmth = 0;
        wsaaStraamt = BigDecimal.ZERO;
        wsaaStbmthg = BigDecimal.ZERO;
        wsaaStldmthg =BigDecimal.ZERO;
        wsaaStpmthg = 0;
        wsaaStsmthg = 0;
        wsaaStimth = 0;
        wsaaStumth = 0;
        wsaaStnmth = 0;
        wsaaAnnprem = BigDecimal.ZERO;
        wsaaAnnTotPrem = BigDecimal.ZERO;
        wsaaAnnpremCurr = BigDecimal.ZERO;
        wsaaAnnpremPrev = BigDecimal.ZERO;
        wsaaCommission = BigDecimal.ZERO;
        wsaaAnnTotCurrent = BigDecimal.ZERO;
        wsaaAnnCurrent = BigDecimal.ZERO;
        wsaaAnnPrevious = BigDecimal.ZERO;

        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "1");
        if (agcmstsList != null) {
            agcmstsIterator = agcmstsList.iterator();
        }
    }

	protected void read3615(){
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3660);
        }
        agcmsts = agcmstsIterator.next();
    	wsaaAgntnum = agcmsts.getAgntnum();
   }

	protected void loop3620(){
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3660);
        }
		if (agcmsts.getAgntnum().equals(wsaaAgntnum)) {
			goTo(GotoLabel.cont3660);
		}
		wsaaOvrdcat = agcmsts.getOvrdcat();
		if (!"Y".equals(agcmsts.getDormflag())) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
		}
		if (agcmsts.getTranno() != covrsts.getTranno()) {
			goTo(GotoLabel.next3650);
		}
		if (!"Y".equals(agcmsts.getDormflag())) {
			wsaaAnnpremCurr = wsaaAnnpremCurr.add(agcmsts.getAnnprem());
			wsaaAnnCurrent = wsaaAnnCurrent.add(agcmsts.getInitcom());
		}
		else {
			wsaaAnnpremPrev = wsaaAnnpremPrev.add(agcmsts.getAnnprem());
			wsaaAnnPrevious = wsaaAnnPrevious.add(agcmsts.getInitcom());
		}
	}
	protected void next3650(){
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3660);
        }
        agcmsts = agcmstsIterator.next();
        goTo(GotoLabel.loop3620);
	}

	protected void cont3660(LifsttrDTO lifsttrDTO) throws IOException{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0) {
			goTo(GotoLabel.next3670);
		}
		wsaaCommission = wsaaAnnTotCurrent;
		wsaaAnnprem = wsaaAnnTotPrem;
		wsaaValidflag = "1";
		updateRecord(lifsttrDTO);
		if (covrsta == null) {
			goTo(GotoLabel.next3670);
		}
		if ((BigDecimal.ZERO.compareTo(wsaaAnnpremPrev) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnPrevious) == 0)) {
			wsaaAnnprem = wsaaAnnTotPrem.subtract(wsaaAnnpremCurr).setScale(2);
			wsaaCommission = wsaaAnnTotCurrent.subtract(wsaaAnnCurrent).setScale(2);
		}
		else {
			wsaaAnnprem = wsaaAnnTotPrem.add(wsaaAnnpremCurr).setScale(2);
			wsaaCommission = wsaaAnnTotCurrent.add(wsaaAnnCurrent).setScale(2);
		}
		wsaaValidflag = "2";
		updateRecord(lifsttrDTO);
	}

	protected void next3670(){
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		if(agcmsts != null){
			wsaaOvrdcat = agcmsts.getOvrdcat();
		}
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			wsaaAgntnum = "";
			wsaaOvrdcat = "";
			goTo(GotoLabel.exit3695);
		}
		wsaaAgntnum = agcmsts.getAgntnum();
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;;
		wsaaStldmthg = BigDecimal.ZERO;;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		goTo(GotoLabel.loop3620);
	}
	protected void processAgents(LifsttrDTO lifsttrDTO) throws IOException
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5510();
					read5515();
				}
				case loop5520: {
					loop5520(lifsttrDTO);
				}
				case next5550: {
					next5550();
				}
				case cont5560: {
					cont5560(lifsttrDTO);
				}
				case next5570: {
					next5570();
					call5575();
				}
				case loop5577: {
					loop5577();
				}
				case next5580: {
					next5580();
				}
				case write5585: {
					write5585(lifsttrDTO);
				}
				case next5587: {
					next5587();
				}
				case exit5590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void start5510(){
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		
        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "1");
        
        if(agcmstsList!=null){
            agcmstsIterator = agcmstsList.iterator();
        }
	}

	protected void read5515(){
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.cont5560);
	    }
	    agcmsts = agcmstsIterator.next();
		wsaaAgntnum = agcmsts.getAgntnum();
	}
	
	protected void loop5520(LifsttrDTO lifsttrDTO){
		if (agcmsts.getTranno() != lifsttrDTO.getTranno()) {
			goTo(GotoLabel.next5550);
		}
		if (!agcmsts.getAgntnum().equals(wsaaAgntnum)) {
			goTo(GotoLabel.cont5560);
		}
		wsaaOvrdcat = agcmsts.getOvrdcat();
		wsaaTransDate = agcmsts.getTrdt();
		wsaaTransTime = agcmsts.getTrtm();
		if (!"Y".equals(agcmsts.getDormflag())) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
		}
	}
	
	protected void next5550(){
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.cont5560);
	    }
	    agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.loop5520);
	}
	
	protected void cont5560(LifsttrDTO lifsttrDTO) throws IOException{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next5570);
		}
		wsaaCommission = wsaaAnnTotCurrent;
		wsaaAnnprem = wsaaAnnTotPrem;
		wsaaValidflag = "1";
		updateRecord(lifsttrDTO);
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.next5570);
	    }
		wsaaAgntnum = agcmsts.getAgntnum();
		wsaaOvrdcat = agcmsts.getOvrdcat();
		goTo(GotoLabel.loop5520);
	}
	
	protected void next5570(){
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAgntnum = "";
		wsaaOvrdcat = "";
		
	    List<Agcmpf> agcmstaList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
	            covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "2");
	    
	    if(agcmstaList!=null){
	        agcmstaIterator = agcmstaList.iterator();
	    }
	}
	
	protected void call5575(){
	    if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
	        goTo(GotoLabel.write5585);
	    }
	    agcmsta = agcmstaIterator.next();
		wsaaAgntnum = agcmsta.getAgntnum();
		wsaaOvrdcat = agcmsta.getOvrdcat();
	}
	
	protected void loop5577(){
		if (agcmsta.getTrtm() != wsaaTransTime || agcmsta.getTrdt() != wsaaTransDate) {
			goTo(GotoLabel.next5580);
		}
		if (!agcmsta.getAgntnum().equals(wsaaAgntnum)) {
			goTo(GotoLabel.write5585);
		}
		if (!"Y".equals(agcmsta.getDormflag())) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsta.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsta.getAnnprem());
		}
	}
	
	protected void next5580(){
	    if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
	        goTo(GotoLabel.write5585);
	    }
	    agcmsta = agcmstaIterator.next();
		goTo(GotoLabel.loop5577);
	}
	
	protected void write5585(LifsttrDTO lifsttrDTO) throws IOException{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next5587);
		}
		wsaaAnnprem = wsaaAnnTotPrem;
		wsaaCommission = wsaaAnnTotCurrent;
		wsaaValidflag = "2";
		updateRecord(lifsttrDTO);
	}
	
	protected void next5587(){
		if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
			goTo(GotoLabel.exit5590);
		}
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaOvrdcat = agcmsta.getOvrdcat();
		wsaaAgntnum = agcmsta.getAgntnum();
		goTo(GotoLabel.loop5577);
	}
	protected void agentChange3900(LifsttrDTO lifsttrDTO, Covrpf covrsts) throws IOException{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3910(covrsts);
					read3915();
				}
				case loop3920: {
					loop3920(lifsttrDTO);
				}
				case next3950: {
					next3950();
				}
				case cont3960: {
					cont3960(lifsttrDTO);
				}
				case next3970: {
					next3970(covrsts);
					call3975();
				}
				case loop3977: {
					loop3977();
				}
				case next3980: {
					next3980();
				}
				case write3985: {
					write3985(lifsttrDTO);
				}
				case next3987: {
					next3987();
				}
				case exit3990: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start3910(Covrpf covrsts){
	    wsaaStcmthg = 0;
	    wsaaStlmth = 0;
	    wsaaStbmthg = BigDecimal.ZERO;
	    wsaaStldmthg = BigDecimal.ZERO;
	    wsaaStraamt = BigDecimal.ZERO;
	    wsaaStpmthg = 0;
	    wsaaStsmthg = 0;
	    wsaaStimth = 0;
	    wsaaStumth = 0;
	    wsaaStnmth = 0;
	    wsaaCommission = BigDecimal.ZERO;
	    wsaaAnnprem = BigDecimal.ZERO;
	    wsaaAnnpremCurr = BigDecimal.ZERO;
	    wsaaAnnpremPrev = BigDecimal.ZERO;
	    wsaaAnnTotCurrent = BigDecimal.ZERO;
	    wsaaAnnTotPrem = BigDecimal.ZERO;
	    wsaaAnnCurrent = BigDecimal.ZERO;
	    wsaaAnnPrevious = BigDecimal.ZERO;
	
	    List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
	            covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "1");
	
	    if (agcmstsList != null) {
	        agcmstsIterator = agcmstsList.iterator();
	    }
	}
	
	protected void read3915(){
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.cont3960);
	    }
	    agcmsts = agcmstsIterator.next();
	    wsaaAgntnum = agcmsts.getAgntnum();
	}
	
	protected void loop3920(LifsttrDTO lifsttrDTO){
		if (agcmsts.getTranno() != lifsttrDTO.getTranno()) {
			goTo(GotoLabel.next3950);
		}
	    if (!agcmsts.getAgntnum().equals(wsaaAgntnum)){
	        goTo(GotoLabel.cont3960);
	    }
	    wsaaOvrdcat = agcmsts.getOvrdcat();
	    if (!"Y".equals(agcmsts.getDormflag())) {
	        wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
	        wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
	    }
	}
	
	protected void next3950(){
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			goTo(GotoLabel.cont3960);
		}
		agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.loop3920);
	}
	
	protected void cont3960(LifsttrDTO lifsttrDTO) throws IOException{
			if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
			&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
				goTo(GotoLabel.next3970);
			}
			wsaaCommission = wsaaAnnTotCurrent;;
			wsaaAnnprem = wsaaAnnTotPrem;
			wsaaValidflag = "1";
			updateRecord(lifsttrDTO);
			wsaaStcmthg = 0;
			wsaaStlmth = 0;
			wsaaStbmthg = BigDecimal.ZERO;
			wsaaStldmthg = BigDecimal.ZERO;
			wsaaStraamt = BigDecimal.ZERO;
			wsaaStpmthg = 0;
			wsaaStsmthg = 0;
			wsaaStimth = 0;
			wsaaStumth = 0;
			wsaaStnmth = 0;
			wsaaAnnprem = BigDecimal.ZERO;
			wsaaCommission = BigDecimal.ZERO;
			wsaaAnnTotPrem = BigDecimal.ZERO;
			wsaaAnnTotCurrent = BigDecimal.ZERO;
			wsaaAnnCurrent = BigDecimal.ZERO;
			wsaaAnnPrevious = BigDecimal.ZERO;
			wsaaAnnpremPrev = BigDecimal.ZERO;
			wsaaAnnpremCurr = BigDecimal.ZERO;
			if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
				goTo(GotoLabel.next3970);
			}
			wsaaAgntnum = agcmsts.getAgntnum();
			wsaaOvrdcat = agcmsts.getOvrdcat();
			goTo(GotoLabel.loop3920);
		}
	
	protected void next3970(Covrpf covrsts){
	    wsaaStcmthg = 0;
	    wsaaStlmth = 0;
	    wsaaStbmthg = BigDecimal.ZERO;
	    wsaaStldmthg = BigDecimal.ZERO;
	    wsaaStraamt = BigDecimal.ZERO;
	    wsaaStpmthg = 0;
	    wsaaStsmthg = 0;
	    wsaaStimth = 0;
	    wsaaStumth = 0;
	    wsaaStnmth = 0;
	    wsaaAnnprem = BigDecimal.ZERO;
	    wsaaAnnpremCurr = BigDecimal.ZERO;
	    wsaaAnnpremPrev = BigDecimal.ZERO;
	    wsaaAnnTotPrem = BigDecimal.ZERO;
	    wsaaCommission = BigDecimal.ZERO;
	    wsaaAnnTotCurrent = BigDecimal.ZERO;
	    wsaaAnnCurrent = BigDecimal.ZERO;
	    wsaaAnnPrevious = BigDecimal.ZERO;
	    wsaaAgntnum = "";
	    wsaaOvrdcat = "";
	
	    List<Agcmpf> agcmstaList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
	            covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlnsfx(), "2");
	
	    if (agcmstaList != null) {
	        agcmstaIterator = agcmstaList.iterator();
	    }
	}
	
	protected void call3975(){
		if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
			goTo(GotoLabel.write3985);
		}
		agcmsta = agcmstaIterator.next();
		wsaaAgntnum = agcmsta.getAgntnum();
		wsaaOvrdcat = agcmsta.getOvrdcat();
	}
	
	protected void loop3977(){
		if ((agcmsta.getTrtm() != agcmsts.getTrtm())
		|| (agcmsta.getTrdt() != agcmsts.getTrdt())) {
			goTo(GotoLabel.next3980);
		}
	
		if (!agcmsta.getAgntnum().equals(wsaaAgntnum)) {
			goTo(GotoLabel.write3985);
		}
		if (!"Y".equals(agcmsta.getDormflag())) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsta.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsta.getAnnprem());
		}
	}
	
	protected void next3980(){
	    if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
	        goTo(GotoLabel.write3985);
	    }
	    agcmsta = agcmstaIterator.next();
		goTo(GotoLabel.loop3977);
	}
	
	protected void write3985(LifsttrDTO lifsttrDTO) throws IOException{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent)==0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next3987);
		}
		wsaaAnnprem = wsaaAnnTotPrem;
		wsaaCommission = wsaaAnnTotCurrent;
		wsaaValidflag = "2";
		updateRecord(lifsttrDTO);
	}
	
	protected void next3987(){
	    if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
	        goTo(GotoLabel.exit3990);
	    }
		wsaaStcmthg = 0;
		wsaaStlmth = 0;
		wsaaStbmthg = BigDecimal.ZERO;
		wsaaStldmthg = BigDecimal.ZERO;
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg = 0;
		wsaaStsmthg = 0;
		wsaaStimth = 0;
		wsaaStumth = 0;
		wsaaStnmth = 0;
		wsaaAnnprem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaCommission = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaOvrdcat = agcmsta.getOvrdcat();
		wsaaAgntnum = agcmsta.getAgntnum();
		goTo(GotoLabel.loop3977);
	}
	protected void continue3021(){
		return;
	}
	public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}	
	
}
