package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;

public class Lextpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private Integer seqnbr;
	private String validflag;
	private Integer tranno;
	private Integer currfrom;
	private Integer currto;
	private String opcda;
	private BigDecimal oppc;
	private Integer insprm;
	private Integer agerate;
	private String sbstdl;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private String jlife;
	private Integer extcd;
	private Integer ecestrm;
	private Integer ecesdte;
	private String reasind;
	private BigDecimal znadjperc;
	private Integer zmortpct;
	private String usrprf;
	private String jobnm;
	private String datime;
	private BigDecimal premadj;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getSeqnbr() {
		return seqnbr;
	}
	public void setSeqnbr(Integer seqnbr) {
		this.seqnbr = seqnbr;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public Integer getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	public Integer getCurrto() {
		return currto;
	}
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}
	public String getOpcda() {
		return opcda;
	}
	public void setOpcda(String opcda) {
		this.opcda = opcda;
	}
	public BigDecimal getOppc() {
		return oppc;
	}
	public void setOppc(BigDecimal oppc) {
		this.oppc = oppc;
	}
	public Integer getInsprm() {
		return insprm;
	}
	public void setInsprm(Integer insprm) {
		this.insprm = insprm;
	}
	public Integer getAgerate() {
		return agerate;
	}
	public void setAgerate(Integer agerate) {
		this.agerate = agerate;
	}
	public String getSbstdl() {
		return sbstdl;
	}
	public void setSbstdl(String sbstdl) {
		this.sbstdl = sbstdl;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public Integer getExtcd() {
		return extcd;
	}
	public void setExtcd(Integer extcd) {
		this.extcd = extcd;
	}
	public Integer getEcestrm() {
		return ecestrm;
	}
	public void setEcestrm(Integer ecestrm) {
		this.ecestrm = ecestrm;
	}
	public Integer getEcesdte() {
		return ecesdte;
	}
	public void setEcesdte(Integer ecesdte) {
		this.ecesdte = ecesdte;
	}
	public String getReasind() {
		return reasind;
	}
	public void setReasind(String reasind) {
		this.reasind = reasind;
	}
	public BigDecimal getZnadjperc() {
		return znadjperc;
	}
	public void setZnadjperc(BigDecimal znadjperc) {
		this.znadjperc = znadjperc;
	}
	public Integer getZmortpct() {
		return zmortpct;
	}
	public void setZmortpct(Integer zmortpct) {
		this.zmortpct = zmortpct;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public BigDecimal getPremadj() {
		return premadj;
	}
	public void setPremadj(BigDecimal premadj) {
		this.premadj = premadj;
	}

	

}