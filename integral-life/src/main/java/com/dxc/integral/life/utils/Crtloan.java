package com.dxc.integral.life.utils;

import java.io.IOException;

import java.text.ParseException;

import com.dxc.integral.life.beans.CrtloanDTO;

public interface Crtloan {
	/**
	 * Creates loan and return loan number.
	 * 
	 * @param crtloanDto
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	public int createLoan(CrtloanDTO crtloanDto) throws IOException, ParseException;//ILIFE-5763

	/**
	 * Creates loan and return loan number for NPSTW.
	 * 
	 * @param crtloanDto
	 * @return
	 * @throws IOException
	 */
	public int createLoanNPSTW(CrtloanDTO crtloanDto) throws IOException;/*ILIFE-5977*/

	/**
	 * Creates loan and return loan number for PSLN.
	 * 
	 * @param crtloanDto
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	public int createLoanPSLN(CrtloanDTO crtloanDto) throws IOException, ParseException;//ILIFE-5763

	/**
	 * Creates loan and return loan number for PSTW.
	 * 
	 * @param crtloanDto
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	public int createLoanPSTW(CrtloanDTO crtloanDto) throws IOException, ParseException;//ILIFE-5763
}
