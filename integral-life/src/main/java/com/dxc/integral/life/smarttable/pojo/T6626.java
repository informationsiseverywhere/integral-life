package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

/**
 * @author fwang3
 *
 */
public class T6626 {
	private List<String> transcds;
	private String trcode;
	private List<String> trncds;

	public List<String> getTranscds() {
		return transcds;
	}

	public void setTranscds(List<String> transcds) {
		this.transcds = transcds;
	}

	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}

	public List<String> getTrncds() {
		return trncds;
	}

	public void setTrncds(List<String> trncds) {
		this.trncds = trncds;
	}

}