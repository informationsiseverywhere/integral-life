package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.RcorfndDTO;
import com.dxc.integral.life.dao.RacdpfDAO;
import com.dxc.integral.life.dao.RecopfDAO;
import com.dxc.integral.life.dao.model.Racdpf;
import com.dxc.integral.life.dao.model.Recopf;
import com.dxc.integral.life.smarttable.pojo.T5445;
import com.dxc.integral.life.smarttable.pojo.T5449;
import com.dxc.integral.life.utils.Rcorfnd;

@Service("rcorfnd")
@Lazy
public class RcorfndImpl implements Rcorfnd{

 @Autowired
 private RacdpfDAO racdpfDAO;
 @Autowired
 private RecopfDAO recopfDAO;
 @Autowired
 private Datcon3 datcon3;
 @Autowired
 private ItempfService itempfService;
 
 private Racdpf racdpf;
 private T5449 t5449IO;
 private T5445 t5445IO;
 
 private int wsaaCtdate;
 private String wsaaReallyNotFound = "Y";
 private BigDecimal wsaaFirstFactor;
 private BigDecimal wsaaFreqFactor;
 private BigDecimal wsaaFirstFrequencyFactor;
 private BigDecimal wsaaSecondFactor;
 private BigDecimal wsaaSecondFrequencyFactor;
 private BigDecimal wsaaMasterFrequencyFactor;
 private BigDecimal wsaaRefundFactor;
 private BigDecimal wsaaRefundPrem;
 private BigDecimal wsaaRefundCompay;
 private BigDecimal wsaaRefundBalance;
 private int wsaaFirstFactorRnd;
 private int wsaaFrequencyFactor;
 private int wsaaSecondFactorRnd;
	
	public RcorfndDTO processRcorfnd(RcorfndDTO rcorfndDTO) throws ParseException{
		
		rcorfndDTO.setStatuz("****");
		rcorfndDTO.setRefundFee(BigDecimal.ZERO);
		rcorfndDTO.setRefundPrem(BigDecimal.ZERO);
		rcorfndDTO.setRefundComm(BigDecimal.ZERO);
		
		if("UPDT".equals(rcorfndDTO.getFunction())) {
			rcorfndDTO.setStatuz("E049");
		}
		
		rcorfndDTO = initialTasks(rcorfndDTO);
		if(!"****".equals(rcorfndDTO.getStatuz())) {
			return rcorfndDTO;
		}
		
		if("".equals(t5449IO.getPrefbas().trim())) {
			rcorfndDTO.setRefundFee(BigDecimal.ZERO);
			rcorfndDTO.setRefundPrem(BigDecimal.ZERO);
			rcorfndDTO.setRefundComm(BigDecimal.ZERO);
			return rcorfndDTO;
		}
		
		TableItem<T5445> t5445= itempfService.readItemByItemFrm(rcorfndDTO.getChdrcoy(), "T5445", t5449IO.getPrefbas(), CommonConstants.IT_ITEMPFX,  rcorfndDTO.getEffdate(), T5445.class);
		if(t5445 == null) {
			rcorfndDTO.setStatuz("R057");
			return rcorfndDTO;
		}
		t5445IO = t5445.getItemDetail();
		
		if (racdpf.getCtdate().intValue()<=	rcorfndDTO.getEffdate() || "".equals(t5449IO.getPrefbas().trim())) {
			return rcorfndDTO;
		}
		
		calculateRefund(rcorfndDTO);
		if ( "Y".equals(wsaaReallyNotFound)) {
			return rcorfndDTO;
		}
		//callRecopost();
				
		return rcorfndDTO;
	}
	
	private RcorfndDTO initialTasks(RcorfndDTO rcorfndDTO) {
		racdpf = racdpfDAO.searchRacdrcoRecord(rcorfndDTO.getChdrcoy(), rcorfndDTO.getChdrnum(), rcorfndDTO.getLife(), rcorfndDTO.getCoverage(), rcorfndDTO.getRider(),
				rcorfndDTO.getPlanSuffix(), rcorfndDTO.getSeqno(), rcorfndDTO.getCurrfrom());
		
		if(racdpf == null) {
			rcorfndDTO.setStatuz("BOMB");
			return rcorfndDTO;
		}	
		wsaaCtdate = racdpf.getCtdate().intValue();
		
		TableItem<T5449> t5449= itempfService.readItemByItemFrm(rcorfndDTO.getChdrcoy(), "T5449", racdpf.getRngmnt(), CommonConstants.IT_ITEMPFX,  rcorfndDTO.getEffdate(), T5449.class);
		if(t5449 == null) {
			rcorfndDTO.setStatuz("R057");
			return rcorfndDTO;
		}
		t5449IO = t5449.getItemDetail();
		
		return rcorfndDTO;
	}
		
	private void calculateRefund(RcorfndDTO rcorfndDTO) throws ParseException{
		List<Recopf> recopflist = recopfDAO.getRecoclmRecords(racdpf.getChdrcoy(), racdpf.getChdrnum(), racdpf.getLife(), racdpf.getCoverage(), racdpf.getRider(), 
				racdpf.getPlnsfx().intValue(), racdpf.getSeqno().intValue());
		if(recopflist == null || recopflist.size() == 0) {
			return ;
		}
		
		for(Recopf recopf : recopflist) {
			if(recopf.getPrem().compareTo(BigDecimal.ZERO)<0 || recopf.getCtdate() < rcorfndDTO.getEffdate()) {
				continue;
		    }
			
			wsaaReallyNotFound = "N";
			wsaaFirstFactor = datcon3.getTerms(rcorfndDTO.getEffdate(), wsaaCtdate, t5445IO.getFrequency());		
			if(!"".equals(t5445IO.getRndgrqd().trim())) {
				wsaaFirstFactorRnd = wsaaFirstFactor.intValue();
				wsaaFirstFrequencyFactor=new BigDecimal(wsaaFirstFactorRnd);
			}else {
				wsaaFreqFactor=wsaaFirstFactor;
				wsaaFirstFrequencyFactor= new BigDecimal(wsaaFrequencyFactor);
			}
			
			wsaaSecondFactor = datcon3.getTerms(recopf.getCostdate(), wsaaCtdate, t5445IO.getFrequency());
			if(!"".equals(t5445IO.getRndgrqd())) {
				wsaaSecondFactorRnd = wsaaSecondFactor.intValue();
				wsaaSecondFrequencyFactor = new BigDecimal(wsaaSecondFactorRnd);
			}else {
				wsaaFreqFactor=wsaaSecondFactor;
				wsaaSecondFrequencyFactor= new BigDecimal(wsaaFrequencyFactor);
			}
			
			wsaaMasterFrequencyFactor = wsaaFirstFrequencyFactor.divide(wsaaSecondFrequencyFactor, 5, BigDecimal.ROUND_HALF_UP);
			wsaaRefundFactor = rcorfndDTO.getRefund().multiply(wsaaMasterFrequencyFactor);
			wsaaRefundPrem = recopf.getPrem().multiply(wsaaRefundFactor);
			wsaaRefundCompay = recopf.getCompay().multiply(wsaaRefundFactor);
			wsaaRefundBalance = wsaaRefundPrem.subtract(wsaaRefundCompay);
			
			if(t5445IO.getRefundfe().compareTo(wsaaRefundBalance)>=0) {
				
			}
//			if (isGTE(t5445rec.refundfe,wsaaRefundBalance)) {
//				recopstrec.refundfe.set(ZERO);
//			}
//			else {
//				recopstrec.refundfe.set(t5445rec.refundfe);
//			}
//			
		}
		
		
	}	
		

}
