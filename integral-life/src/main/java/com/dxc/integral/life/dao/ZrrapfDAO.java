package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Zrrapf;

public interface ZrrapfDAO {
	public Zrrapf getZrrapfRecord(Zrrapf zrrapf);
	public int deleteZrrapfRecord(long uniqueNumber);
}
