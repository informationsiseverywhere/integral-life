package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.model.Agcmpf;

/**
 * 
 * AgcmpfDAO implementation for database table <b>Agcmpf</b> DB operations.
 * 
 *
 */
@Repository("agcmpfDAO")
@Lazy
public class AgcmpfDAOImpl extends BaseDAOImpl implements AgcmpfDAO{
	@Override
	public void updateAcagpfRecords(List<Agcmpf> agcmpfList) {
		final List<Agcmpf> tempAcagpfList = agcmpfList;   
		String sql = "UPDATE AGCMPF SET PTDATE=?,JOBNM=?,USRPRF=?,DATIME=?,COMPAY=?,COMERN=?,SCMEARN=?,SCMDUE=?,RNLCDUE=?,RNLCEARN=? WHERE UNIQUE_NUMBER=?";
		jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
            	return tempAcagpfList.size();
            }
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	ps.setInt(1, agcmpfList.get(i).getPtdate());
            	ps.setString(2, agcmpfList.get(i).getJobName());
            	ps.setString(3, agcmpfList.get(i).getUserProfile());
            	ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            	ps.setBigDecimal(5, agcmpfList.get(i).getCompay());
            	ps.setBigDecimal(6, agcmpfList.get(i).getComern());
            	ps.setBigDecimal(7, agcmpfList.get(i).getScmearn());
            	ps.setBigDecimal(8, agcmpfList.get(i).getScmdue());
            	ps.setBigDecimal(9, agcmpfList.get(i).getRnlcdue());
            	ps.setBigDecimal(10, agcmpfList.get(i).getRnlcearn());
            	ps.setLong(11, agcmpfList.get(i).getUniqueNumber());
            }
		});		
	}
	@Override
	public List<Agcmpf> getAgcmpfRecord(String coy, String chdrnum) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM AGCMPF WHERE ");
		sql.append(" CHDRCOY=? AND CHDRNUM=? AND (VALIDFLAG = '1' OR VALIDFLAG = ' ') ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,AGNTNUM ASC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { coy, chdrnum },
				new BeanPropertyRowMapper<Agcmpf>(Agcmpf.class));
	}

	@Override
	public List<Agcmpf> getAgcmrvsRecord(String coy, String chdrnum, int tranno) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM AGCMRVS WHERE  CHDRCOY=? AND CHDRNUM=? AND TRANNO=? ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, TRANNO ASC,UNIQUE_NUMBER ASC ");
		return jdbcTemplate.query(sql.toString(), new Object[] { coy, chdrnum, tranno },
				new BeanPropertyRowMapper<Agcmpf>(Agcmpf.class));
	}
	@Override
	public List<Agcmpf> getAgcmpfRecordByAgntnum(Agcmpf agcmpf) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM AGCMPF WHERE ");
		sql.append(
				" CHDRCOY=? AND CHDRNUM=? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND AGNTNUM = ? ");
		sql.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO ASC, UNIQUE_NUMBER DESC ");
		return jdbcTemplate.query(sql.toString(),
				new Object[] { agcmpf.getChdrcoy(), agcmpf.getChdrnum(), agcmpf.getLife(), agcmpf.getCoverage(),
						agcmpf.getRider(), agcmpf.getPlnsfx(), agcmpf.getAgntnum() },
				new BeanPropertyRowMapper<Agcmpf>(Agcmpf.class));
	}
	
	@Override
	public List<Agcmpf> searchAgcmstRecord(String chdrcoy, String chdrnum,String life, String coverage, String rider, int plnsfx,
			String validflag) {
        StringBuilder sqlSelect = new StringBuilder();
        sqlSelect.append("select * from agcmpf where chdrcoy = ? and chdrnum = ?  and life = ? and coverage = ? and rider = ? and plnsfx = ? and validflag = ? ");
        sqlSelect.append("order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc, plnsfx asc, agntnum asc, unique_number desc ");
		return jdbcTemplate.query(sqlSelect.toString(),
				new Object[] { chdrcoy, chdrnum,life, coverage, rider, plnsfx, validflag},
				new BeanPropertyRowMapper<Agcmpf>(Agcmpf.class));
	}

	@Override
	public int updateAcagpfValidflag(Agcmpf agcmpf) {
		String sql = "UPDATE AGCMPF SET VALIDFLAG=?, CURRTO=? WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?";
		return jdbcTemplate.update(sql,//IJTI-1415
				new Object[] { agcmpf.getValidflag(), agcmpf.getCurrto(), agcmpf.getChdrcoy(), agcmpf.getChdrnum(), agcmpf.getTranno() });
	}
	
	@Override
	public void insertAgcmpfRecords(List<Agcmpf> agcmpfList) {
		String sql = "INSERT INTO AGCMPF (CHDRCOY, CHDRNUM, AGNTNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, TRANNO, EFDATE, ANNPREM, BASCMETH, INITCOM, BASCPY, COMPAY, COMERN, SRVCPY, SCMDUE, SCMEARN, RNWCPY, RNLCDUE, RNLCEARN, AGCLS, TERMID, TRDT, TRTM, USER_T, CRTABLE, CURRFROM, CURRTO, VALIDFLAG, SEQNO, PTDATE, CEDAGENT, OVRDCAT, DORMFLAG, USRPRF, JOBNM, DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			@Override
			public int getBatchSize() {
				return agcmpfList.size();
			}

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				int j = 1;
				ps.setString(j++, agcmpfList.get(i).getChdrcoy());
				ps.setString(j++, agcmpfList.get(i).getChdrnum());
				ps.setString(j++, agcmpfList.get(i).getAgntnum());
				ps.setString(j++, agcmpfList.get(i).getLife());
				ps.setString(j++, agcmpfList.get(i).getJlife());
				ps.setString(j++, agcmpfList.get(i).getCoverage());
				ps.setString(j++, agcmpfList.get(i).getRider());
				ps.setInt(j++, agcmpfList.get(i).getPlnsfx());
				ps.setInt(j++, agcmpfList.get(i).getTranno());
				ps.setInt(j++, agcmpfList.get(i).getEfdate());
				ps.setBigDecimal(j++, agcmpfList.get(i).getAnnprem());
				ps.setString(j++, agcmpfList.get(i).getBascmeth());
				ps.setBigDecimal(j++, agcmpfList.get(i).getInitcom());
				ps.setString(j++, agcmpfList.get(i).getBascpy());
				ps.setBigDecimal(j++, agcmpfList.get(i).getCompay());
				ps.setBigDecimal(j++, agcmpfList.get(i).getComern());
				ps.setString(j++, agcmpfList.get(i).getSrvcpy());
				ps.setBigDecimal(j++, agcmpfList.get(i).getScmdue());
				ps.setBigDecimal(j++, agcmpfList.get(i).getScmearn());
				ps.setString(j++, agcmpfList.get(i).getRnwcpy());
				ps.setBigDecimal(j++, agcmpfList.get(i).getRnlcdue());
				ps.setBigDecimal(j++, agcmpfList.get(i).getRnlcearn());
				ps.setString(j++, agcmpfList.get(i).getAgcls());
				ps.setString(j++, agcmpfList.get(i).getTermid());
				ps.setInt(j++, agcmpfList.get(i).getTrdt());
				ps.setInt(j++, agcmpfList.get(i).getTrtm());
				ps.setInt(j++, agcmpfList.get(i).getUserT());
				ps.setString(j++, agcmpfList.get(i).getCrtable());
				ps.setInt(j++, agcmpfList.get(i).getCurrfrom());
				ps.setInt(j++, agcmpfList.get(i).getCurrto());
				ps.setString(j++, agcmpfList.get(i).getValidflag());
				ps.setInt(j++, agcmpfList.get(i).getSeqno());
				ps.setInt(j++, agcmpfList.get(i).getPtdate());
				ps.setString(j++, agcmpfList.get(i).getCedagent());
				ps.setString(j++, agcmpfList.get(i).getOvrdcat());
				ps.setString(j++, agcmpfList.get(i).getDormflag());
				ps.setString(j++, agcmpfList.get(i).getUsrprf());
				ps.setString(j++, agcmpfList.get(i).getJobnm());
				ps.setTimestamp(j, new Timestamp(System.currentTimeMillis()));
			}
		});
	}
	
	@Override
	public List<Agcmpf> getAgcmpfRecord(Agcmpf agcmpf) {
		StringBuilder sql = new StringBuilder("SELECT * FROM AGCMPF WHERE CHDRCOY=? AND CHDRNUM=? AND AGNTNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? ");
	    sql.append("AND PLNSFX=? AND SEQNO=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO ASC, UNIQUE_NUMBER DESC");
	    return jdbcTemplate.query(sql.toString(), new Object[] {agcmpf.getChdrcoy(), agcmpf.getChdrnum(), agcmpf.getAgntnum(), agcmpf.getLife(), 
	    		agcmpf.getCoverage(), agcmpf.getRider(), agcmpf.getPlnsfx(), agcmpf.getSeqno()}, new BeanPropertyRowMapper<Agcmpf>(Agcmpf.class));	    
	    
	}
	@Override
	public int updateAgcmpfRecord(Agcmpf agcmpf) {
		String sql = "UPDATE AGCMPF SET VALIDFLAG=?, CURRTO=? WHERE CHDRCOY=? AND CHDRNUM=? AND AGNTNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND SEQNO=?";
		
		return jdbcTemplate.update(sql, new Object[] {agcmpf.getValidflag(), agcmpf.getCurrto(), agcmpf.getChdrcoy(), agcmpf.getChdrnum(), agcmpf.getAgntnum(),  
				agcmpf.getLife(), agcmpf.getCoverage(), agcmpf.getRider(), agcmpf.getPlnsfx(), agcmpf.getSeqno()});
	}
	@Override
	public int deleteAgcmpfRecord(Agcmpf agcmpf) {
		String sql = "DELETE FROM AGCMPF WHERE CHDRCOY=? AND CHDRNUM=? AND AGNTNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND SEQNO=?";
		
		return jdbcTemplate.update(sql, new Object[] {agcmpf.getChdrcoy(), agcmpf.getChdrnum(), agcmpf.getAgntnum(), agcmpf.getLife(), agcmpf.getCoverage(), 
				agcmpf.getRider(), agcmpf.getPlnsfx(), agcmpf.getSeqno()});
	}
}
