package com.dxc.integral.life.dao.impl;

import org.springframework.stereotype.Repository;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RlncbkpfDAO;

@Repository("rlncbkpfDAO")
public class RlncbkpfDAOImpl extends BaseDAOImpl implements RlncbkpfDAO{
	
	public int deleteRlncbkpfRecord(int date) {		
		String sql = "DELETE FROM RLNCBKPF WHERE ZLINCDTE< ?";
		return jdbcTemplate.update(sql, new Object[] {date});
	}
	
}
