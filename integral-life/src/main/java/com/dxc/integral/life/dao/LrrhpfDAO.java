package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Lrrhpf;

public interface LrrhpfDAO {
	Lrrhpf search(Lrrhpf lrrhpf);

	void update(Lrrhpf lrrhpf);

	void insert(Lrrhpf lrrhpf);

}
