package com.dxc.integral.life.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.smarttable.pojo.T5533;
import com.dxc.integral.life.smarttable.pojo.T5646;
@Service("prmpm03Util")
public class Prmpm03Util {
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Datcon3 datcon3;
	private int wsaaYoungerLife = 0;
	private int wsaaYoungerDob = 0;
	private int wsaaRndfact;
	private BigDecimal wsaaTotPremium;
	private BigDecimal wsaaFactor;
	private BigDecimal wsaaFreqFactor = BigDecimal.ZERO;
	private BigDecimal wsaaRoundNum;
	private BigDecimal wsaaRound1000;
	private BigDecimal wsaaRound100;
	private BigDecimal wsaaRound10;
	private BigDecimal wsaaRound1;
	private BigDecimal wsaaRoundDec;
	private int wsaaAge00 = 0 ;
	private int wsaaAge01 = 0 ;
	private int wsaaDob00 = 0 ;
	private int wsaaDob01 = 0 ;
	private int wsaaDateToUse;
	
	private int wsaaSub;
	private T5533 t5533IO;
	private T5646 t5646IO;
	public PremiumDTO processPrempm03(PremiumDTO premiumDTO) throws ParseException{
		premiumDTO.setStatuz("****");
		initialize();
		if ("****".equals(premiumDTO.getStatuz())) {
			readT5533(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			readLifeDetails(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			t5646IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5646", premiumDTO.getCrtable(),premiumDTO.getRatingdate(),T5646.class);
			if(t5646IO == null){
				premiumDTO.setStatuz("H068");
			}
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			premiumMethods(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			determineFactor(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			suminRounded(premiumDTO);
		}
		return premiumDTO;
	}
	protected void suminRounded(PremiumDTO premiumDTO){
		wsaaTotPremium = wsaaTotPremium.multiply(wsaaFactor).setScale(4);
		wsaaRoundNum = wsaaTotPremium;
		wsaaRound1000 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(0,13).substring(7));
		wsaaRound100 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(12));
		wsaaRound10 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(13));
		wsaaRound1 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(14));
		wsaaRoundDec = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(15));
		if (wsaaRndfact ==1  || wsaaRndfact == 0) {
			if (wsaaRoundDec.compareTo(new BigDecimal(0.5)) == -1) {
				wsaaRoundDec = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(1));
				wsaaRoundDec = BigDecimal.ZERO;
			}
		}
		if (wsaaRndfact == 10) {
			if (wsaaRound1.compareTo(new BigDecimal(5)) == -1) {
				wsaaRound1 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(10));
				wsaaRound1 =  BigDecimal.ZERO;
			}
		}
		if (wsaaRndfact == 100) {
			if (wsaaRound10.compareTo(new BigDecimal(50)) == -1) {
				wsaaRound10 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(100));
				wsaaRound10 = BigDecimal.ZERO;
			}
		}
		if (wsaaRndfact == 1000) {
			if (wsaaRound100.compareTo(new BigDecimal(500)) == -1) {
				wsaaRound100 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(1000));
				wsaaRound100 = BigDecimal.ZERO;
			}
		}
		if (wsaaRndfact == 10000) {
			if (wsaaRound1000.compareTo(new BigDecimal(5000)) == -1) {
				wsaaRound1000 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(10000));
				wsaaRound1000 = BigDecimal.ZERO;
			}
		}
		premiumDTO.setSumin(wsaaRoundNum);
	}
	protected void determineFactor(PremiumDTO premiumDTO){
		for(wsaaSub = 0 ; wsaaSub < 12 ; wsaaSub++){
			if (wsaaYoungerLife >=t5646IO.getAgeIssageFrms().get(wsaaSub) 
					&& wsaaYoungerLife <=t5646IO.getAgeIssageTos().get(wsaaSub)) {
						break;
			}
		}
		wsaaFactor = t5646IO.getFactorsas().get(wsaaSub);
	}
	protected void initialize(){
		wsaaYoungerLife = 0 ;
		wsaaFactor = BigDecimal.ZERO;
		wsaaTotPremium = BigDecimal.ZERO;
		wsaaAge00 = 0;
		wsaaAge01 = 0;
		wsaaDob00 = 0;
		wsaaDob01 = 0;
		wsaaDateToUse = 0;
		wsaaRndfact = 0;
		wsaaSub = 0;
	}
	protected void premiumMethods(PremiumDTO premiumDTO) throws ParseException{
		if (t5646IO.getAgelimit() == 0|| "00".equals(premiumDTO.getBillfreq())){
			wsaaDateToUse = premiumDTO.getTermdate();
			calcNumInstalments(premiumDTO);
			return ;
		}
		wsaaDateToUse = datcon2.plusYears(wsaaYoungerDob, t5646IO.getAgelimit());
		if (wsaaDateToUse < premiumDTO.getEffectdt()) {
			premiumDTO.setStatuz("T041");
			return ;
		}
		if (premiumDTO.getEffectdt() < wsaaDateToUse) {
			wsaaDateToUse = premiumDTO.getEffectdt();
		}
		calcNumInstalments(premiumDTO);
	}
	protected void calcNumInstalments(PremiumDTO premiumDTO) throws ParseException{
		if ("00".equals(premiumDTO.getBillfreq())) {
			wsaaTotPremium = premiumDTO.getCalcPrem();
			return ;
		}
		int freqFactor = 0;
		if("12".equals(premiumDTO.getBillfreq())){
			freqFactor = datcon3.getMonthsDifference(premiumDTO.getEffectdt(), wsaaDateToUse).intValue();
		}else if("01".equals(premiumDTO.getBillfreq())){
			freqFactor = datcon3.getYearsDifference(premiumDTO.getEffectdt(), wsaaDateToUse).intValue();
		}else if("DY".equals(premiumDTO.getBillfreq())){
			freqFactor = datcon3.getDaysDifference(premiumDTO.getEffectdt(), wsaaDateToUse).intValue();
		}
		wsaaFreqFactor = new BigDecimal(freqFactor).add(new BigDecimal(0.99999)).setScale(5,BigDecimal.ROUND_HALF_UP);
		wsaaTotPremium = premiumDTO.getCalcPrem().multiply(wsaaFreqFactor).setScale(2,BigDecimal.ROUND_HALF_UP);
	}
	protected void readLifeDetails(PremiumDTO premiumDTO){
		List<Lifepf> lifeList = lifepfDAO.getLifeRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(), "00");
		if(lifeList == null || lifeList.isEmpty()){
			throw new DataNoFoundException("Br613: Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table LIFEPF while processing contract ");
		}
		Lifepf lifelnbIO = lifeList.get(0);
		wsaaAge00 = premiumDTO.getLage();
		wsaaDob00 = lifelnbIO.getCltdob();
		lifeList = lifepfDAO.getLifeRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(), "01");
		if(lifeList != null && !lifeList.isEmpty()){
			lifelnbIO = lifeList.get(0);
			wsaaAge01 = premiumDTO.getJlage();
			wsaaDob01 = lifelnbIO.getCltdob();
		}
		if (wsaaAge01 < wsaaAge00 && wsaaAge01 != 0) {
			wsaaYoungerLife = wsaaAge01;
			wsaaYoungerDob = wsaaDob01;
		} else {
			wsaaYoungerDob = wsaaDob00;
			wsaaYoungerLife = wsaaAge00;
		}
	}
	protected void readT5533(PremiumDTO premiumDTO) {
		String t5533Itemitem = premiumDTO.getCrtable().concat(premiumDTO.getCurrcode());
		t5533IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5533", t5533Itemitem,premiumDTO.getRatingdate(),T5533.class);
		if(t5533IO == null){
			premiumDTO.setStatuz("G071");
			return;
		}
		wsaaRndfact = t5533IO.getRndfact();
		
		for (wsaaSub = 0; !(wsaaSub > 7 || premiumDTO.getBillfreq().equals(t5533IO.getFrequencys().get(wsaaSub))); wsaaSub++){
			
		}
		
		if (wsaaSub > 7) {
			premiumDTO.setStatuz("G072");
		}
		else {
			if ((premiumDTO.getCalcPrem().compareTo(t5533IO.getCmins().get(wsaaSub)) == -1
			|| (premiumDTO.getCalcPrem().compareTo(t5533IO.getCmaxs().get(wsaaSub)) == 1))
			&& !"INCR".equals(premiumDTO.getFunction())) {
				premiumDTO.setStatuz("G070");
			}
		}
	}
	private String formatRoundNum(BigDecimal obj) {
		DecimalFormat df=new DecimalFormat("00000000000000000.00");
		String str2=df.format(obj);
		return str2;
	}
}
