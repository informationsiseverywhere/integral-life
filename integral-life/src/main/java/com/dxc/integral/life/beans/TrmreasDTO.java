package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class TrmreasDTO {
	
	private String function = "";
	private String chdrcoy = "";
	private String chdrnum = "";
	private String life = "";
	private String coverage = "";
	private String rider = "";
	private int planSuffix = 0;
	private int polsum = 0;
	private int effdate = 0;
	private String batckey = "";
	private int tranno = 0;
	private String cnttype = "";
	private String billfreq = "";
	private int ptdate = 0;
	private String origcurr = "";
	private String acctcurr = "";
	private String crtable = "";
	private int crrcd = 0;
	private int convUnits = 0;
	private BigDecimal singp = BigDecimal.ZERO;
	private BigDecimal oldSumins = BigDecimal.ZERO;
	private BigDecimal newSumins = BigDecimal.ZERO;
	private BigDecimal clmPercent = BigDecimal.ZERO;
	private String pstatcode = "";
	private String lrkcls = "";
	private String language = "";
	public String statuz = "";
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public int getPolsum() {
		return polsum;
	}
	public void setPolsum(int polsum) {
		this.polsum = polsum;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getBatckey() {
		return batckey;
	}
	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public String getAcctcurr() {
		return acctcurr;
	}
	public void setAcctcurr(String acctcurr) {
		this.acctcurr = acctcurr;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public int getConvUnits() {
		return convUnits;
	}
	public void setConvUnits(int convUnits) {
		this.convUnits = convUnits;
	}
	public BigDecimal getSingp() {
		return singp;
	}
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}
	public BigDecimal getOldSumins() {
		return oldSumins;
	}
	public void setOldSumins(BigDecimal oldSumins) {
		this.oldSumins = oldSumins;
	}
	public BigDecimal getNewSumins() {
		return newSumins;
	}
	public void setNewSumins(BigDecimal newSumins) {
		this.newSumins = newSumins;
	}
	public BigDecimal getClmPercent() {
		return clmPercent;
	}
	public void setClmPercent(BigDecimal clmPercent) {
		this.clmPercent = clmPercent;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getLrkcls() {
		return lrkcls;
	}
	public void setLrkcls(String lrkcls) {
		this.lrkcls = lrkcls;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	
}
