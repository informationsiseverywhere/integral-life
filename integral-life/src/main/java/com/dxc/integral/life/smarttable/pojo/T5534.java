package com.dxc.integral.life.smarttable.pojo;

public class T5534 {

	private String adfeemth;
	private String jlPremMeth;
	private String premmeth;
	private String subprog;
	private String svMethod;
	private String unitFreq;
	public String getAdfeemth() {
		return adfeemth;
	}
	public void setAdfeemth(String adfeemth) {
		this.adfeemth = adfeemth;
	}
	public String getJlPremMeth() {
		return jlPremMeth;
	}
	public void setJlPremMeth(String jlPremMeth) {
		this.jlPremMeth = jlPremMeth;
	}
	public String getPremmeth() {
		return premmeth;
	}
	public void setPremmeth(String premmeth) {
		this.premmeth = premmeth;
	}
	public String getSubprog() {
		return subprog;
	}
	public void setSubprog(String subprog) {
		this.subprog = subprog;
	}
	public String getSvMethod() {
		return svMethod;
	}
	public void setSvMethod(String svMethod) {
		this.svMethod = svMethod;
	}
	public String getUnitFreq() {
		return unitFreq;
	}
	public void setUnitFreq(String unitFreq) {
		this.unitFreq = unitFreq;
	}
}
