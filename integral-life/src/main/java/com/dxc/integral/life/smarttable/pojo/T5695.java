package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5695 {
	private List<BigDecimal> cmrates;
	private List<BigDecimal> commenpcs;
	private List<String> freqFroms;
  	private List<String> freqTos;
    private List<BigDecimal> premaxpcs;
	public List<BigDecimal> getCmrates() {
		return cmrates;
	}
	public void setCmrates(List<BigDecimal> cmrates) {
		this.cmrates = cmrates;
	}
	public List<BigDecimal> getCommenpcs() {
		return commenpcs;
	}
	public void setCommenpcs(List<BigDecimal> commenpcs) {
		this.commenpcs = commenpcs;
	}
	public List<String> getFreqFroms() {
		return freqFroms;
	}
	public void setFreqFroms(List<String> freqFroms) {
		this.freqFroms = freqFroms;
	}
	public List<String> getFreqTos() {
		return freqTos;
	}
	public void setFreqTos(List<String> freqTos) {
		this.freqTos = freqTos;
	}
	public List<BigDecimal> getPremaxpcs() {
		return premaxpcs;
	}
	public void setPremaxpcs(List<BigDecimal> premaxpcs) {
		this.premaxpcs = premaxpcs;
	}

    
}
