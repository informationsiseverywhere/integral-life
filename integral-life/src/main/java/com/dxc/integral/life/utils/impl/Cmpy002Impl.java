package com.dxc.integral.life.utils.impl;


import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.services.CalcRnwlCommissionService;
import com.dxc.integral.life.smarttable.pojo.T5694;
import com.dxc.integral.life.utils.Cmpy002;
@Service("cmpy002")
@Lazy
public class Cmpy002Impl implements Cmpy002{
	@Autowired
    private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private CalcRnwlCommissionService calcRnwlCommissionService;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Environment env;

	private BigDecimal wsaaT5694 = BigDecimal.ZERO;
	
	private BigDecimal wsaaTerm;
	private int afterDate;
	

	
    public ComlinkDTO processCmpy002(ComlinkDTO comlinkDTO) throws Exception{

    	comlinkDTO.setStatuz("****");
    	comlinkDTO.setPayamnt(BigDecimal.ZERO);
    	comlinkDTO.setErndamt(BigDecimal.ZERO);
		Chdrpf chdrpf = obtainPaymntRules(comlinkDTO);
		if (wsaaT5694.compareTo(BigDecimal.ZERO)==0) {
			return comlinkDTO;
		}
		boolean vpmsFlag;
		if (env.getProperty("vpms.flag") == null) {
			vpmsFlag = false;
		} else {
			vpmsFlag = env.getProperty("vpms.flag").equals("1");
		}
		comlinkDTO = calcRnwlCommissionService.calcRnwlCommission(comlinkDTO, wsaaT5694, chdrpf.getCnttype(), vpmsFlag);
    	return comlinkDTO;
    }
	
	protected Chdrpf obtainPaymntRules(ComlinkDTO comlinkDTO) throws Exception{
		Chdrpf chdrpf = chdrpfDAO.getLPContractInfo(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum());
		
		TableItem<T5694> t5694 =itempfService.readSmartTableByTableNameAndItem(chdrpf.getChdrcoy(), "T5694", comlinkDTO.getMethod().concat(chdrpf.getSrcebus()), CommonConstants.IT_ITEMPFX, T5694.class);
	
	    if(t5694 == null) {
	    	t5694 =itempfService.readSmartTableByTableNameAndItem(chdrpf.getChdrcoy(), "T5694", comlinkDTO.getMethod().concat("**"), CommonConstants.IT_ITEMPFX, T5694.class);
	    	if(t5694 == null) {
	    		comlinkDTO.setStatuz("BOMB");
	 	    	return null;
	    	 }
	    }
	    T5694 t5694IO = t5694.getItemDetail();
	    if("00".equals(comlinkDTO.getBillfreq().trim())) {
	    	wsaaTerm = BigDecimal.ZERO;
	    	bypassDatcon(t5694IO);
	    	return chdrpf;
	    }
		
	    if(comlinkDTO.getPtdate()!=0) {
	    	afterDate=comlinkDTO.getPtdate();
	    	cont(comlinkDTO.getEffdate());
	    	bypassDatcon(t5694IO);
	    	return chdrpf;
	    }
	    afterDate=chdrpf.getPtdate().intValue();
	    comlinkDTO.setPtdate(afterDate);
	    cont(comlinkDTO.getEffdate());
	    bypassDatcon(t5694IO);
	    return chdrpf;			
				
	}
	
	protected void bypassDatcon(T5694 t5694IO)
	{
		wsaaT5694=BigDecimal.ZERO;
		for (int i=1; i<=11; i++){
			if (wsaaTerm.compareTo(new BigDecimal(t5694IO.getToYears().get(i-1)))<=0) {
				wsaaT5694=t5694IO.getInstalpcs().get(i-1);
				i=13;
			}
		}
		/*EXIT*/
	}

	
	protected void cont(int effdate) throws Exception{
		wsaaTerm = new BigDecimal(datcon3.getYearsDifference(effdate, afterDate))
				.add(new BigDecimal(0.99999).setScale(0, BigDecimal.ROUND_HALF_UP));
	}

}
