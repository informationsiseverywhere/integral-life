package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.PremiumDTO;

public interface Premium {
	public PremiumDTO processPrmpm01(PremiumDTO premiumDTO) throws IOException;
	public PremiumDTO processPrmpm17(PremiumDTO premiumDTO) throws IOException;
	public PremiumDTO processPrmpm03(PremiumDTO premiumDTO) throws IOException, ParseException;
	public PremiumDTO processPrmpm04(PremiumDTO premiumDTO) throws IOException, ParseException;
	public PremiumDTO processPrmpm18(PremiumDTO premiumDTO) throws IOException;
}
