/* ********************  */
/*Author  :tsaxena3				  		*/
package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T5567 {
	
	private List<Integer> billfreqs;
	private List<Integer> cntfees;
	
	public List<Integer> getBillfreqs() {
		return billfreqs;
	}
	public void setBillfreqs(List<Integer> billfreqs) {
		this.billfreqs = billfreqs;
	}
	
	public List<Integer> getCntfees(){
		return cntfees;
	}
	public void setCntfees(List<Integer> cntfees) {
		this.cntfees = cntfees;
	}

}
