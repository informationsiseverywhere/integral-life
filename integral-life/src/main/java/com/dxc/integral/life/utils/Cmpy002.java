package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.ComlinkDTO;

public interface Cmpy002 {
	ComlinkDTO processCmpy002(ComlinkDTO comlinkDTO) throws Exception;
}
