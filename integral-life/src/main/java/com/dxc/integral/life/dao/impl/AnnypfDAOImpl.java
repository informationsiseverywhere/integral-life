package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AnnypfDAO;
import com.dxc.integral.life.dao.model.Annypf;

/**
 * 
 * LextpfDAO implementation for database table <b>Lextpf</b> DB operations.
 * 
 *
 */
@Repository("annypfDAO")
public class AnnypfDAOImpl extends BaseDAOImpl implements AnnypfDAO {

	@Override
	public List<Annypf> getAnnypfRecord(Annypf annypf) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM ANNYPF WHERE ");
		sql.append(" CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ");
		sql.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(
				sql.toString(), new Object[] { annypf.getChdrcoy(), annypf.getChdrnum(), annypf.getLife(),
						annypf.getCoverage(), annypf.getRider(), annypf.getPlnsfx() },
				new BeanPropertyRowMapper<Annypf>(Annypf.class));
	}
}
