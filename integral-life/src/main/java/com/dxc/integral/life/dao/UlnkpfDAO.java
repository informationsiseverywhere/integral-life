package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Ulnkpf;

public interface UlnkpfDAO {
	public Ulnkpf getUlnkrnlRecord(Ulnkpf ulnkpf);
}
