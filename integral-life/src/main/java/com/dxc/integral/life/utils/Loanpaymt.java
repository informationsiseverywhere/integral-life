package com.dxc.integral.life.utils;

import java.io.IOException;

import java.text.ParseException;

import com.dxc.integral.fsu.beans.CashedInputDTO;
import com.dxc.integral.fsu.beans.CashedOutputDTO;


/**
 * Loanpaymt utility.
 * 
 * @author mmalik25
 */
@FunctionalInterface
public interface Loanpaymt {

	/**
	 * 
	 * This method returns the Residual amount to the calling program
	 * 
	 * @param contractType
	 * @param cashedIO
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	CashedOutputDTO processLoanPayment(String contractType, CashedInputDTO cashedIO) throws IOException, ParseException ;//ILIFE-5763
	

}
