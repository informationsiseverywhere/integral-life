package com.dxc.integral.life.utils.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.beans.BatcupDTO;
import com.dxc.integral.life.utils.BatLifecup;

@Service("batLifeCup")
@Lazy
public class BatcupImpl extends BaseDAOImpl implements BatLifecup {

	
	public int numberOfParameters = 0;
	private int sub = 0;
	private int[] wsaaBcnt = new int[13];
	private int[] wsaaBval = new int[13];
	private int[] wsaaAscnt = new int[13];
	private int wsaaEtreqcnt;
	private boolean wsaaFirstTime = true;
	private String wsaaSubr = "BATCUP";
	private int wsaaTrancnt;
	@Override
	public void callBatcup(BatcupDTO batcupDTO) {
		start010(batcupDTO);
	
	}
	protected void start010(BatcupDTO batcupDTO) {
		batcupDTO.setStatuz("****");
		/* IF BCUP-FUNCTION = 'RQMOD' */
		/* PERFORM 200-ADJUST-THE-TOTALS */
		/* GO TO 090-EXIT. */
		if ("KEEPS".equals(batcupDTO.getFunction())) {
			keepCount300(batcupDTO);
			return;
		}
		
	}
	
	protected void keepCount300(BatcupDTO batcupDTO) {
		if (wsaaFirstTime) {
			wsaaTrancnt = 0;
			wsaaEtreqcnt = 0;
			sub = 1;
			for (int i = 0; i < 12; i++) {
				initialise500();
			}
			wsaaFirstTime = false;
		}
		wsaaTrancnt += batcupDTO.getTrancnt();
		wsaaEtreqcnt += batcupDTO.getEtreqcnt();
		if (batcupDTO.getSub() != 0) {
			sub = batcupDTO.getSub();
			wsaaBcnt[sub] +=batcupDTO.getBcnt();
			wsaaBval[sub] += batcupDTO.getBval();
			wsaaAscnt[sub] += batcupDTO.getAscnt();
		}
	}
	protected void initialise500() {
		/* SET-TO-ZERO */
		wsaaBcnt[sub] = 0;
		wsaaBval[sub] = 0;
		wsaaAscnt[sub] = 0;
		sub++;
	}


}
