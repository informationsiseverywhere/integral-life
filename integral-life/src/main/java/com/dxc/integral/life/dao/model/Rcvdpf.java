package com.dxc.integral.life.dao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Rcvdpf implements Serializable {
	private static final long serialVersionUID = 5408400790048204050L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private String crtable;
	private String waitperiod;
	private String bentrm;
	private String statcode;
	private String poltyp;
	private String prmbasis;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String dialdownoption;

	public String getDialdownoption() {
		return dialdownoption;
	}

	public void setDialdownoption(String dialdownoption) {
		this.dialdownoption = dialdownoption;
	}

	public Date getDatime() {
		return new Date(datime.getTime());
	}

	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getWaitperiod() {
		return waitperiod;
	}

	public void setWaitperiod(String waitperiod) {
		this.waitperiod = waitperiod;
	}

	public String getBentrm() {
		return bentrm;
	}

	public void setBentrm(String bentrm) {
		this.bentrm = bentrm;
	}

	public String getPoltyp() {
		return poltyp;
	}

	public void setPoltyp(String poltyp) {
		this.poltyp = poltyp;
	}

	public String getPrmbasis() {
		return prmbasis;
	}

	public void setPrmbasis(String prmbasis) {
		this.prmbasis = prmbasis;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
}
