package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Aglfpf;

public interface AglfpfDAO  {
	public Aglfpf searchAglflnb(String coy, String agntNum);

}