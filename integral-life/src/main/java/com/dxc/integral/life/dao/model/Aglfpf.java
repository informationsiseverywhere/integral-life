package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

import javax.persistence.Column;

public class Aglfpf{
	private long uniqueNumber;
	private String agntcoy;
	private String agntnum;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private Integer currfrom;
	private Integer currto;
	private Integer dteapp;
	private Integer dtetrm;
	private String trmcde;
	private Integer dteexp;
	private String pftflg;
	private String rasflg;
	private String bcmtab;
	private String rcmtab;
	private String scmtab;
	private String agcls;
	private String ocmtab;
	private String reportag;
	private BigDecimal ovcpc;
	private String taxmeth;
	private String irdno;
	private String taxcde;
	private BigDecimal taxalw;
	private String sprschm;
	private BigDecimal sprprc;
	private String payclt;
	private String paymth;
	private String payfrq;
	private String facthous;
	private String bankkey;
	private String bankacckey;
	private Integer dtepay;
	private String currcode;
	private BigDecimal intcrd;
	private BigDecimal fixprc;
	private String bmaflg;
	private String excagr;
	private String hseln;
	private String comln;
	private String carln;
	private String offrent;
	private String othln;
	private String aracde;
	private BigDecimal minsta;
	private String zrorcode;
	private Integer effdate;
	private String tagsusind;
	private String tlaglicno;
	private Integer tlicexpdt;
	private BigDecimal tcolprct;
	private BigDecimal tcolmax;
	private String tsalesunt;
	private String prdagent;
	private String agccqind;
	private String usrprf;
	private String jobnm;
	private String datime;
	private String validflag;
	private String zrecruit;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public Integer getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	public Integer getCurrto() {
		return currto;
	}
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}
	public Integer getDteapp() {
		return dteapp;
	}
	public void setDteapp(Integer dteapp) {
		this.dteapp = dteapp;
	}
	public Integer getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(Integer dtetrm) {
		this.dtetrm = dtetrm;
	}
	public String getTrmcde() {
		return trmcde;
	}
	public void setTrmcde(String trmcde) {
		this.trmcde = trmcde;
	}
	public Integer getDteexp() {
		return dteexp;
	}
	public void setDteexp(Integer dteexp) {
		this.dteexp = dteexp;
	}
	public String getPftflg() {
		return pftflg;
	}
	public void setPftflg(String pftflg) {
		this.pftflg = pftflg;
	}
	public String getRasflg() {
		return rasflg;
	}
	public void setRasflg(String rasflg) {
		this.rasflg = rasflg;
	}
	public String getBcmtab() {
		return bcmtab;
	}
	public void setBcmtab(String bcmtab) {
		this.bcmtab = bcmtab;
	}
	public String getRcmtab() {
		return rcmtab;
	}
	public void setRcmtab(String rcmtab) {
		this.rcmtab = rcmtab;
	}
	public String getScmtab() {
		return scmtab;
	}
	public void setScmtab(String scmtab) {
		this.scmtab = scmtab;
	}
	public String getAgcls() {
		return agcls;
	}
	public void setAgcls(String agcls) {
		this.agcls = agcls;
	}
	public String getOcmtab() {
		return ocmtab;
	}
	public void setOcmtab(String ocmtab) {
		this.ocmtab = ocmtab;
	}
	public String getReportag() {
		return reportag;
	}
	public void setReportag(String reportag) {
		this.reportag = reportag;
	}
	public BigDecimal getOvcpc() {
		return ovcpc;
	}
	public void setOvcpc(BigDecimal ovcpc) {
		this.ovcpc = ovcpc;
	}
	public String getTaxmeth() {
		return taxmeth;
	}
	public void setTaxmeth(String taxmeth) {
		this.taxmeth = taxmeth;
	}
	public String getIrdno() {
		return irdno;
	}
	public void setIrdno(String irdno) {
		this.irdno = irdno;
	}
	public String getTaxcde() {
		return taxcde;
	}
	public void setTaxcde(String taxcde) {
		this.taxcde = taxcde;
	}
	public BigDecimal getTaxalw() {
		return taxalw;
	}
	public void setTaxalw(BigDecimal taxalw) {
		this.taxalw = taxalw;
	}
	public String getSprschm() {
		return sprschm;
	}
	public void setSprschm(String sprschm) {
		this.sprschm = sprschm;
	}
	public BigDecimal getSprprc() {
		return sprprc;
	}
	public void setSprprc(BigDecimal sprprc) {
		this.sprprc = sprprc;
	}
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	public String getPaymth() {
		return paymth;
	}
	public void setPaymth(String paymth) {
		this.paymth = paymth;
	}
	public String getPayfrq() {
		return payfrq;
	}
	public void setPayfrq(String payfrq) {
		this.payfrq = payfrq;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public Integer getDtepay() {
		return dtepay;
	}
	public void setDtepay(Integer dtepay) {
		this.dtepay = dtepay;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public BigDecimal getIntcrd() {
		return intcrd;
	}
	public void setIntcrd(BigDecimal intcrd) {
		this.intcrd = intcrd;
	}
	public BigDecimal getFixprc() {
		return fixprc;
	}
	public void setFixprc(BigDecimal fixprc) {
		this.fixprc = fixprc;
	}
	public String getBmaflg() {
		return bmaflg;
	}
	public void setBmaflg(String bmaflg) {
		this.bmaflg = bmaflg;
	}
	public String getExcagr() {
		return excagr;
	}
	public void setExcagr(String excagr) {
		this.excagr = excagr;
	}
	public String getHseln() {
		return hseln;
	}
	public void setHseln(String hseln) {
		this.hseln = hseln;
	}
	public String getComln() {
		return comln;
	}
	public void setComln(String comln) {
		this.comln = comln;
	}
	public String getCarln() {
		return carln;
	}
	public void setCarln(String carln) {
		this.carln = carln;
	}
	public String getOffrent() {
		return offrent;
	}
	public void setOffrent(String offrent) {
		this.offrent = offrent;
	}
	public String getOthln() {
		return othln;
	}
	public void setOthln(String othln) {
		this.othln = othln;
	}
	public String getAracde() {
		return aracde;
	}
	public void setAracde(String aracde) {
		this.aracde = aracde;
	}
	public BigDecimal getMinsta() {
		return minsta;
	}
	public void setMinsta(BigDecimal minsta) {
		this.minsta = minsta;
	}
	public String getZrorcode() {
		return zrorcode;
	}
	public void setZrorcode(String zrorcode) {
		this.zrorcode = zrorcode;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public String getTagsusind() {
		return tagsusind;
	}
	public void setTagsusind(String tagsusind) {
		this.tagsusind = tagsusind;
	}
	public String getTlaglicno() {
		return tlaglicno;
	}
	public void setTlaglicno(String tlaglicno) {
		this.tlaglicno = tlaglicno;
	}
	public Integer getTlicexpdt() {
		return tlicexpdt;
	}
	public void setTlicexpdt(Integer tlicexpdt) {
		this.tlicexpdt = tlicexpdt;
	}
	public BigDecimal getTcolprct() {
		return tcolprct;
	}
	public void setTcolprct(BigDecimal tcolprct) {
		this.tcolprct = tcolprct;
	}
	public BigDecimal getTcolmax() {
		return tcolmax;
	}
	public void setTcolmax(BigDecimal tcolmax) {
		this.tcolmax = tcolmax;
	}
	public String getTsalesunt() {
		return tsalesunt;
	}
	public void setTsalesunt(String tsalesunt) {
		this.tsalesunt = tsalesunt;
	}
	public String getPrdagent() {
		return prdagent;
	}
	public void setPrdagent(String prdagent) {
		this.prdagent = prdagent;
	}
	public String getAgccqind() {
		return agccqind;
	}
	public void setAgccqind(String agccqind) {
		this.agccqind = agccqind;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getZrecruit() {
		return zrecruit;
	}
	public void setZrecruit(String zrecruit) {
		this.zrecruit = zrecruit;
	}
  
}