package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.PrasDTO;

@FunctionalInterface
public interface Prascalc {

	public PrasDTO processPrascalc(PrasDTO prasDTO) throws IOException, ParseException;
}
