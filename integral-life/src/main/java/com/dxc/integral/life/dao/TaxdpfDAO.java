package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Taxdpf;

public interface TaxdpfDAO {
	public void insertTaxdpfRecords(List<Taxdpf> taxdpfList);
	public void updateTaxdpfRecords(List<Taxdpf> taxdpfList);
	public int deleteTaxdpfRecords(String chdrcoy, String chdrnum, int instfrom, int instto);
}
