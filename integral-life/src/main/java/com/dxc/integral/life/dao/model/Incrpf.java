package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Incrpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx = 0;
	private Integer effdate = 0;
	private Integer tranno = 0;
	private String validflag;
	private String statcode;
	private String pstatcode;
	private Integer crrcd;
	private String crtable;
	private BigDecimal origInst = BigDecimal.ZERO;
	private BigDecimal lastInst = BigDecimal.ZERO;
	private BigDecimal newinst = BigDecimal.ZERO;
	private BigDecimal origSum = BigDecimal.ZERO;
	private BigDecimal lastSum = BigDecimal.ZERO;
	private BigDecimal newsum = BigDecimal.ZERO;
	private String annvry;
	private String bascmeth;
	private Integer pctinc;
	private String refflag;
	private Integer trdt = 0;
	private Integer trtm = 0;
	private Integer user = 0;
	private String termid;
	private String bascpy;
	private String ceaseInd;
	private String rnwcpy;
	private String srvcpy;
	private BigDecimal zboriginst = BigDecimal.ZERO;
	private BigDecimal zblastinst = BigDecimal.ZERO;
	private BigDecimal zbnewinst = BigDecimal.ZERO;
	private BigDecimal zloriginst = BigDecimal.ZERO;
	private BigDecimal zllastinst = BigDecimal.ZERO;
	private BigDecimal zlnewinst = BigDecimal.ZERO;
	private BigDecimal zstpduty01 = BigDecimal.ZERO;
	private String usrprf;
	private String jobnm;
	private Date datime;
	
	public String getAnnvry() {
		return annvry;
	}
	public void setAnnvry(String annvry) {
		this.annvry = annvry;
	}
	public String getBascmeth() {
		return bascmeth;
	}
	public void setBascmeth(String bascmeth) {
		this.bascmeth = bascmeth;
	}
	public String getRefflag() {
		return refflag;
	}
	public void setRefflag(String refflag) {
		this.refflag = refflag;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public Integer getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	public Integer getEffdate() {
		return effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstatcode() {
		return pstatcode;
	}

	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	public Integer getCrrcd() {
		return crrcd;
	}

	public void setCrrcd(Integer crrcd) {
		this.crrcd = crrcd;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public BigDecimal getOrigInst() {
		return origInst;
	}

	public void setOrigInst(BigDecimal origInst) {
		this.origInst = origInst;
	}

	public BigDecimal getLastInst() {
		return lastInst;
	}

	public void setLastInst(BigDecimal lastInst) {
		this.lastInst = lastInst;
	}

	public BigDecimal getNewinst() {
		return newinst;
	}

	public void setNewinst(BigDecimal newinst) {
		this.newinst = newinst;
	}

	public BigDecimal getOrigSum() {
		return origSum;
	}

	public void setOrigSum(BigDecimal origSum) {
		this.origSum = origSum;
	}

	public BigDecimal getLastSum() {
		return lastSum;
	}

	public void setLastSum(BigDecimal lastSum) {
		this.lastSum = lastSum;
	}

	public BigDecimal getNewsum() {
		return newsum;
	}

	public void setNewsum(BigDecimal newsum) {
		this.newsum = newsum;
	}

	public String getAnniversaryMethod() {
		return annvry;
	}

	public void setAnniversaryMethod(String annvry) {
		this.annvry = annvry;
	}

	public String getBasicCommMeth() {
		return bascmeth;
	}

	public void setBasicCommMeth(String bascmeth) {
		this.bascmeth = bascmeth;
	}

	public Integer getPctinc() {
		return pctinc;
	}

	public void setPctinc(Integer pctinc) {
		this.pctinc = pctinc;
	}

	public String getRefusalFlag() {
		return refflag;
	}

	public void setRefusalFlag(String refflag) {
		this.refflag = refflag;
	}

	public Integer getTransactionDate() {
		return trdt;
	}

	public void setTransactionDate(Integer transactionDate) {
		this.trdt = transactionDate;
	}

	public Integer getTransactionTime() {
		return trtm;
	}

	public void setTransactionTime(Integer transactionTime) {
		this.trtm = transactionTime;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public String getBascpy() {
		return bascpy;
	}

	public void setBascpy(String bascpy) {
		this.bascpy = bascpy;
	}

	public String getCeaseInd() {
		return ceaseInd;
	}

	public void setCeaseInd(String ceaseInd) {
		this.ceaseInd = ceaseInd;
	}

	public String getRnwcpy() {
		return rnwcpy;
	}

	public void setRnwcpy(String rnwcpy) {
		this.rnwcpy = rnwcpy;
	}

	public String getSrvcpy() {
		return srvcpy;
	}

	public void setSrvcpy(String srvcpy) {
		this.srvcpy = srvcpy;
	}

	public BigDecimal getZboriginst() {
		return zboriginst;
	}

	public void setZboriginst(BigDecimal zboriginst) {
		this.zboriginst = zboriginst;
	}

	public BigDecimal getZblastinst() {
		return zblastinst;
	}

	public void setZblastinst(BigDecimal zblastinst) {
		this.zblastinst = zblastinst;
	}

	public BigDecimal getZbnewinst() {
		return zbnewinst;
	}

	public void setZbnewinst(BigDecimal zbnewinst) {
		this.zbnewinst = zbnewinst;
	}

	public BigDecimal getZloriginst() {
		return zloriginst;
	}

	public void setZloriginst(BigDecimal zloriginst) {
		this.zloriginst = zloriginst;
	}

	public BigDecimal getZllastinst() {
		return zllastinst;
	}

	public void setZllastinst(BigDecimal zllastinst) {
		this.zllastinst = zllastinst;
	}

	public BigDecimal getZlnewinst() {
		return zlnewinst;
	}

	public void setZlnewinst(BigDecimal zlnewinst) {
		this.zlnewinst = zlnewinst;
	}

	public String getUserProfile() {
		return usrprf;
	}

	public void setUserProfile(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobName() {
		return jobnm;
	}

	public void setJobName(String jobName) {
		this.jobnm = jobName;
	}

	public Date getDatime() {
		return new Date(datime.getTime());
	}

	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());
	}
	
}
