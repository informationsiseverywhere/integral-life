package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5449 {

	private String currcode = "";
	private String facsh = "";
	private String letterType = "";
	private String lrkcls = "";
	private String prefbas = "";
	private BigDecimal qcoshr;
	private List<BigDecimal> qreshrs;
	private List<String> rasnums;
	private List<BigDecimal> relimits;
	private String retype = "";
	private List<String> rprmmths;
	private String rtytyp = "";
	private String sslivb = "";
	private String subliv = "";
	
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getFacsh() {
		return facsh;
	}
	public void setFacsh(String facsh) {
		this.facsh = facsh;
	}
	public String getLetterType() {
		return letterType;
	}
	public void setLetterType(String letterType) {
		this.letterType = letterType;
	}
	public String getLrkcls() {
		return lrkcls;
	}
	public void setLrkcls(String lrkcls) {
		this.lrkcls = lrkcls;
	}
	public String getPrefbas() {
		return prefbas;
	}
	public void setPrefbas(String prefbas) {
		this.prefbas = prefbas;
	}
	public BigDecimal getQcoshr() {
		return qcoshr;
	}
	public void setQcoshr(BigDecimal qcoshr) {
		this.qcoshr = qcoshr;
	}
	public List<BigDecimal> getQreshrs() {
		return qreshrs;
	}
	public void setQreshrs(List<BigDecimal> qreshrs) {
		this.qreshrs = qreshrs;
	}
	public List<String> getRasnums() {
		return rasnums;
	}
	public void setRasnums(List<String> rasnums) {
		this.rasnums = rasnums;
	}
	public List<BigDecimal> getRelimits() {
		return relimits;
	}
	public void setRelimits(List<BigDecimal> relimits) {
		this.relimits = relimits;
	}
	public String getRetype() {
		return retype;
	}
	public void setRetype(String retype) {
		this.retype = retype;
	}
	public List<String> getRprmmths() {
		return rprmmths;
	}
	public void setRprmmths(List<String> rprmmths) {
		this.rprmmths = rprmmths;
	}
	public String getRtytyp() {
		return rtytyp;
	}
	public void setRtytyp(String rtytyp) {
		this.rtytyp = rtytyp;
	}
	public String getSslivb() {
		return sslivb;
	}
	public void setSslivb(String sslivb) {
		this.sslivb = sslivb;
	}
	public String getSubliv() {
		return subliv;
	}
	public void setSubliv(String subliv) {
		this.subliv = subliv;
	}
	
	
}
