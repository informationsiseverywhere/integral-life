package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class Th622 {

  	public List<String> keyopts;
    public List<String> zryrpercs;
    
	public List<String> getKeyopts() {
		return keyopts;
	}
	public void setKeyopts(List<String> keyopts) {
		this.keyopts = keyopts;
	}
	public List<String> getZryrpercs() {
		return zryrpercs;
	}
	public void setZryrpercs(List<String> zryrpercs) {
		this.zryrpercs = zryrpercs;
	}
    
    

}
