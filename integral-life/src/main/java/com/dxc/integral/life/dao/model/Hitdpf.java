package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Hitdpf {

    private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String zintbfnd;
	private int tranno;
	private int effdate;
	private String validflag;
	private int zlstintdte;
	private int znxtintdte;
	private BigDecimal zlstfndval;
	private int zlstsmtdt;
	private int zlstsmtno;
	private BigDecimal zlstsmtbal;
	private String userProfile;
	private String jobName;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getZintbfnd() {
		return zintbfnd;
	}
	public void setZintbfnd(String zintbfnd) {
		this.zintbfnd = zintbfnd;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getZlstintdte() {
		return zlstintdte;
	}
	public void setZlstintdte(int zlstintdte) {
		this.zlstintdte = zlstintdte;
	}
	public int getZnxtintdte() {
		return znxtintdte;
	}
	public void setZnxtintdte(int znxtintdte) {
		this.znxtintdte = znxtintdte;
	}
	public BigDecimal getZlstfndval() {
		return zlstfndval;
	}
	public void setZlstfndval(BigDecimal zlstfndval) {
		this.zlstfndval = zlstfndval;
	}
	public int getZlstsmtdt() {
		return zlstsmtdt;
	}
	public void setZlstsmtdt(int zlstsmtdt) {
		this.zlstsmtdt = zlstsmtdt;
	}
	public int getZlstsmtno() {
		return zlstsmtno;
	}
	public void setZlstsmtno(int zlstsmtno) {
		this.zlstsmtno = zlstsmtno;
	}
	public BigDecimal getZlstsmtbal() {
		return zlstsmtbal;
	}
	public void setZlstsmtbal(BigDecimal zlstsmtbal) {
		this.zlstsmtbal = zlstsmtbal;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	
}
