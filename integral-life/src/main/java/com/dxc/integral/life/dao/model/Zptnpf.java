package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Zptnpf {
	private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int tranno;
    private int effdate;
    private BigDecimal origamt;
    private String transCode;
    private int billcd;
    private int instfrom;
    private int instto;
    private String zprflg;
    private int trandate;
    private String userProfile;
    private String jobName;
    private String datime;

    public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
        return chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public String getLife() {
        return life;
    }

    public String getCoverage() {
        return coverage;
    }

    public String getRider() {
        return rider;
    }

    public int getTranno() {
        return tranno;
    }

    public int getEffdate() {
        return effdate;
    }

    public BigDecimal getOrigamt() {
        return origamt;
    }

    public String getTransCode() {
        return transCode;
    }

    public int getBillcd() {
        return billcd;
    }

    public int getInstfrom() {
        return instfrom;
    }

    public int getInstto() {
        return instto;
    }

    public String getZprflg() {
        return zprflg;
    }

    public int getTrandate() {
        return trandate;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public void setOrigamt(BigDecimal origamt) {
        this.origamt = origamt;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public void setBillcd(int billcd) {
        this.billcd = billcd;
    }

    public void setInstfrom(int instfrom) {
        this.instfrom = instfrom;
    }

    public void setInstto(int instto) {
        this.instto = instto;
    }

    public void setZprflg(String zprflg) {
        this.zprflg = zprflg;
    }

    public void setTrandate(int trandate) {
        this.trandate = trandate;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }
}