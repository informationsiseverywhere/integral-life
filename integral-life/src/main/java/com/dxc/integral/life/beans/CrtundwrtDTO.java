package com.dxc.integral.life.beans;

import java.math.BigDecimal;
/**
 * @author wli31
 */
public class CrtundwrtDTO {
  	private String clntnum;
  	private String coy;
  	private String chdrnum;
  	private String life;
  	private String crtable;
  	private String batctrcde;
  	public BigDecimal sumins;
  	private String extra;
  	private String cnttyp;
  	private String currcode;
  	private String function;
  	private String status;
  	private String jobname;
  	private String userpf;
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getCoy() {
		return coy;
	}
	public void setCoy(String coy) {
		this.coy = coy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public String getCnttyp() {
		return cnttyp;
	}
	public void setCnttyp(String cnttyp) {
		this.cnttyp = cnttyp;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getUserpf() {
		return userpf;
	}
	public void setUserpf(String userpf) {
		this.userpf = userpf;
	}
  	
}
