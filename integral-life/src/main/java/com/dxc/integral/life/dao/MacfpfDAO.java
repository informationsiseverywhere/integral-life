package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Macfpf;

/**
 * MacfpfDAO for database table <b>MACFPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@FunctionalInterface
public interface MacfpfDAO {

	/**
	 * Reads MACFPF by comapny and agent number.
	 * 
	 * @param company
	 *            - company number
	 * @param agentNumber
	 *            - agent number
	 * @return - List of MACFPF records
	 */
	List<Macfpf> readMacfpfByAgentNumberAndAgentCompany(String company, String agentNumber);
}
