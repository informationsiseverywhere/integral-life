package com.dxc.integral.life.utils;

import java.text.ParseException;

import com.dxc.integral.life.beans.RcorfndDTO;

public interface Rcorfnd {

	public RcorfndDTO processRcorfnd(RcorfndDTO rcorfndDTO) throws ParseException;
}
