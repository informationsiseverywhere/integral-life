package com.dxc.integral.life.beans;

public class VPMSinfoDTO {
	private String modelName;
	private String transDate;
	private String coy;

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getTransDate() {
		return transDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public String getCoy() {
		return coy;
	}

	public void setCoy(String coy) {
		this.coy = coy;
	}
}
