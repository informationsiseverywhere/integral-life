package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Trwppf;

public interface TrwppfDAO {
	 public void insertTrwppfRecords(Trwppf trwppfList);
}
