package com.dxc.integral.life.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.NlgtpfDAO;
import com.dxc.integral.life.dao.model.Nlgtpf;
/**
 * @author wli31
 */
@Repository("nlgtpfDAO")
@Lazy
public class NlgtpfDAOImpl extends BaseDAOImpl implements NlgtpfDAO {
	

	@Override
	public List<Nlgtpf> readNlgtpf(String chdrcoy, String chdrnum) {
		StringBuilder sql  = new StringBuilder("select tranno,effdate,batcactyr,batcactmn,batctrcde,trandesc,tranamt,fromdate,todate,nlgflag,yrsinf,age,nlgbal,amt01,amt02,amt03,amt04");
		 sql.append(" from nlgtpf where (chdrcoy = ? and chdrnum = ? ) ");
		 return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum,},
					new BeanPropertyRowMapper<Nlgtpf>(Nlgtpf.class));
	}
	
	public Nlgtpf getNlgRecord(String chdrcoy,String chdrnum,String validflag){
		 StringBuilder sql  = new StringBuilder("select tranno,effdate,batcactyr,batcactmn,batctrcde,trandesc,tranamt,fromdate,todate,nlgflag,yrsinf,age,nlgbal,amt01,amt02,amt03,amt04");
		 sql.append(" from nlgtpf where(chdrcoy = ? and chdrnum = ? and validflag = ? ) order by tranno asc ");
		 return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum,validflag},
					new BeanPropertyRowMapper<Nlgtpf>(Nlgtpf.class));
	}
	
	public int insertNlgtpf(Nlgtpf nlgtpf){
		StringBuilder sb = new StringBuilder("");
		sb.append("insert into nlgtpf (chdrcoy, chdrnum, tranno, effdate, batcactyr, batcactmn, batctrcde, trandesc, tranamt, fromdate, todate, nlgflag, yrsinf, age, nlgbal, ");
		sb.append(" amt01, amt02, amt03, amt04, validflag, usrprf, jobnm ,datime)");
		sb.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		return jdbcTemplate.update(sb.toString(), new Object[]{
					nlgtpf.getChdrcoy(),nlgtpf.getChdrnum(),nlgtpf.getTranno(),nlgtpf.getEffdate(),nlgtpf.getBatcactyr(),nlgtpf.getBatcactmn(),nlgtpf.getBatctrcde()
					,nlgtpf.getTrandesc(),nlgtpf.getTranamt(),nlgtpf.getFromdate(),nlgtpf.getTodate(),nlgtpf.getNlgflag(),nlgtpf.getYrsinf(),nlgtpf.getAge(),nlgtpf.getNlgbal()
					,nlgtpf.getAmnt01(),nlgtpf.getAmnt02(),nlgtpf.getAmnt03(),nlgtpf.getAmnt04(),nlgtpf.getUsrprf(), nlgtpf.getJobnm(), new Timestamp(System.currentTimeMillis())
				});
		
	}

}
