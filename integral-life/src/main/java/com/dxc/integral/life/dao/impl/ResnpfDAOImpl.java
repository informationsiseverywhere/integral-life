package com.dxc.integral.life.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ResnpfDAO;

@Repository("resnpfDAO")
@Lazy
public class ResnpfDAOImpl extends BaseDAOImpl implements ResnpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ResnpfDAOImpl.class);

	@Override
	public String getReasoncd(String chdrcoy, String chdrnum, String trancde) {
		String reasoncd = null;
		String sql = "SELECT REASONCD FROM RESNPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANCDE=? ";
		try {
			reasoncd = jdbcTemplate.queryForObject(sql, new Object[] { chdrcoy, chdrnum, trancde }, String.class);
		} catch (Exception e) {
			LOGGER.info("zero or more than one REASONCD found in RESNPF ");
		}
		return reasoncd;
	}

}
