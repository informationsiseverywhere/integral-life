package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Pcddpf;

public interface PcddpfDAO {
	public List<Pcddpf> getPcddpfRecord(String coy, String chdrnum);
	public List<Pcddpf> getPcddByAgntnum(String coy,String agntnum);
}