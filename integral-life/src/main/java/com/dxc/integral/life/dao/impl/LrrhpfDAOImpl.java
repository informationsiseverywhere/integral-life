package com.dxc.integral.life.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LrrhpfDAO;
import com.dxc.integral.life.dao.model.Lrrhpf;

@Repository("lrrhpfDAO")
public class LrrhpfDAOImpl extends BaseDAOImpl implements LrrhpfDAO {

	@Override
	public Lrrhpf search(Lrrhpf lrrhpf) {
		String sql = "SELECT * FROM VM1DTA.LRRHPF WHERE COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND CLNTPFX=? AND CLNTCOY=? AND CLNTNUM=? AND LRKCLS=?";
		return jdbcTemplate.queryForObject(sql,
				new Object[] { lrrhpf.getCompany(), lrrhpf.getChdrnum(), lrrhpf.getLife(), lrrhpf.getCoverage(),
						lrrhpf.getRider(), lrrhpf.getPlanSuffix(), lrrhpf.getClntpfx(), lrrhpf.getClntcoy(),
						lrrhpf.getClntnum(), lrrhpf.getLrkcls() },
				new BeanPropertyRowMapper<Lrrhpf>(Lrrhpf.class));
	}

	@Override
	public void update(Lrrhpf lrrhpf) {
		String sql = "UPDATE VM1DTA.LRRHPF SET CURRTO=?, VALIDFLAG=?, SSRETN=? WHERE COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND CLNTPFX=? AND CLNTCOY=? AND CLNTNUM=? AND LRKCLS=?";
		jdbcTemplate.update(sql,
				new Object[] { lrrhpf.getCurrto(), lrrhpf.getValidflag(), lrrhpf.getSsretn(), lrrhpf.getCompany(),
						lrrhpf.getChdrnum(), lrrhpf.getLife(), lrrhpf.getCoverage(), lrrhpf.getRider(),
						lrrhpf.getPlanSuffix(), lrrhpf.getClntpfx(), lrrhpf.getClntcoy(), lrrhpf.getClntnum(),
						lrrhpf.getLrkcls() });
	}

	@Override
	public void insert(Lrrhpf lrrhpf) {
		String sql = "INSERT INTO VM1DTA.LRRHPF (CLNTPFX,CLNTCOY,CLNTNUM,COMPANY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,LRKCLS,CURRFROM,CURRTO,VALIDFLAG,TRANNO,CURRENCY,SSRETN,SSREAST,SSREASF,USRPRF,JOBNM,DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(sql, getLrrhpfArrForInsert(lrrhpf));
	}

	private Object[] getLrrhpfArrForInsert(Lrrhpf lrrhpf) {
		return new Object[] { lrrhpf.getClntpfx(), lrrhpf.getClntcoy(), lrrhpf.getClntnum(), lrrhpf.getCompany(),
				lrrhpf.getChdrnum(), lrrhpf.getLife(), lrrhpf.getCoverage(), lrrhpf.getRider(), lrrhpf.getPlanSuffix(),
				lrrhpf.getLrkcls(), lrrhpf.getCurrfrom(), lrrhpf.getCurrto(), lrrhpf.getValidflag(), lrrhpf.getTranno(),
				lrrhpf.getCurrency(), lrrhpf.getSsretn(), lrrhpf.getSsreast(), lrrhpf.getSsreasf(),
				lrrhpf.getUserProfile(), lrrhpf.getJobName(), lrrhpf.getDatime() };
	}

}
