package com.dxc.integral.life.utils;

import java.io.IOException;

import java.text.ParseException;

import com.dxc.integral.life.beans.ZrcshopDTO;

/**
 * Paydebts utility.
 * 
 * @author mmalik25
 *
 */
@FunctionalInterface
public interface Paydebts {

	/**
	 * 
	 * This method will use the Payments due from the Anticipated Endowment
	 * Release to pay the existing Loans.
	 * 
	 * @param zrcshopDTO
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	void payOffDebt(ZrcshopDTO zrcshopDTO) throws IOException, ParseException;//ILIFE-5763

}
