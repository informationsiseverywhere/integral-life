package com.dxc.integral.life.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.PostyymmInputDTO;
import com.dxc.integral.fsu.beans.PostyymmOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.RtrnpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Rtrnpf;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.utils.Postyymm;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.life.beans.LifrtrnDTO;

@Service("subLedgerPostingService")
public class SubLedgerPostingService{
	@Autowired
	private AcblpfDAO acblpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private RtrnpfDAO rtrnpfDAO;
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private Postyymm postyymm;
	@Autowired
	private Zrdecplc zrdecplc;
	
	private String wsaaLastOrigCcy = "";
	private String wsaaLedgerCcy = "";
	
    /*Post Sub Ledger Posting to RTRN*/
	public Rtrnpf postsubLedgRtrn(LifrtrnDTO lifrtrnPojo) throws IOException{
		
		String wsaaToday = "";
		int wsaaTransDate=0;
		int wsaaTranDateCc=0;
		int wsaaTranDateYy=0;
		BigDecimal wsaaAmountOut;
		BigDecimal wsaaNominalRate = BigDecimal.ZERO;
		lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("?", lifrtrnPojo.getBatccoy()));
		lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("##", lifrtrnPojo.getBatcbrn()));
		lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("&&&", lifrtrnPojo.getOrigcurr()));
		if (lifrtrnPojo.getSubstituteCodes()[0] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[0])) {
			lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("@@@", lifrtrnPojo.getSubstituteCodes()[0]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[1] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[1])) {
			lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("***", lifrtrnPojo.getSubstituteCodes()[1]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[2] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[2])) {
			lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("%%%", lifrtrnPojo.getSubstituteCodes()[2]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[3] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[3])) {
			lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("!!!", lifrtrnPojo.getSubstituteCodes()[3]));
		}
		if (lifrtrnPojo.getSubstituteCodes()[4] != null && !"".equals(lifrtrnPojo.getSubstituteCodes()[4])) {
			lifrtrnPojo.setGlcode(lifrtrnPojo.getGlcode().replace("+++", lifrtrnPojo.getSubstituteCodes()[4]));
		}
		/*  If accounting currency details not entered,*/
		/*   and the original currency has changed since the last time*/
		/*     - look up the nominal exchange rate.*/
		if ((lifrtrnPojo.getGenlcur() == null || "".equals(lifrtrnPojo.getGenlcur())) && !(lifrtrnPojo.getOrigcurr().equals(wsaaLastOrigCcy))) {
			wsaaNominalRate = getRate(lifrtrnPojo,lifrtrnPojo.getOrigcurr());
		}

		/*  Write movement record.*/
		Rtrnpf rtrnpf = new Rtrnpf();
		rtrnpf.setBatcpfx("BA");
		rtrnpf.setBatccoy(lifrtrnPojo.getBatccoy());
		rtrnpf.setBatcbrn(lifrtrnPojo.getBatcbrn());
		rtrnpf.setBatcactyr(lifrtrnPojo.getBatcactyr());
		rtrnpf.setBatcactmn(lifrtrnPojo.getBatcactmn());
		rtrnpf.setBatctrcde(lifrtrnPojo.getBatctrcde());
		rtrnpf.setBatcbatch(lifrtrnPojo.getBatcbatch());
		rtrnpf.setRdocnum(lifrtrnPojo.getRdocnum());
		rtrnpf.setTranseq(String.valueOf(lifrtrnPojo.getJrnseq()));
		rtrnpf.setRdocpfx("CA");
		rtrnpf.setRdoccoy(lifrtrnPojo.getBatccoy());
		rtrnpf.setRdocnum(lifrtrnPojo.getRdocnum());
		if ("LP".equals(lifrtrnPojo.getSacscode())) {
			rtrnpf.setRdocpfx("CH");
		}
		if ("SC".equals(lifrtrnPojo.getSacscode())) {
			rtrnpf.setRdocpfx("SC");
		}
		rtrnpf.setRldgcoy(lifrtrnPojo.getRldgcoy());
		rtrnpf.setSacscode(lifrtrnPojo.getSacscode());
		rtrnpf.setRldgacct(lifrtrnPojo.getRldgacct());
		rtrnpf.setOrigccy(lifrtrnPojo.getOrigcurr());
		rtrnpf.setSacstyp(lifrtrnPojo.getSacstyp());
		rtrnpf.setOrigamt(lifrtrnPojo.getOrigamt());
		rtrnpf.setTrandesc(lifrtrnPojo.getTrandesc());
		rtrnpf.setCrate(lifrtrnPojo.getCrate());
		rtrnpf.setAcctamt(lifrtrnPojo.getAcctamt());
		rtrnpf.setGenlpfx("GE");
		rtrnpf.setGenlcoy(lifrtrnPojo.getGenlcoy());
		rtrnpf.setGenlcur(lifrtrnPojo.getGenlcur());
		rtrnpf.setGlcode(lifrtrnPojo.getGlcode());
		rtrnpf.setGlsign(lifrtrnPojo.getGlsign());
		PostyymmInputDTO postyymmInput = new PostyymmInputDTO();
		
		postyymmInput.setCompany(lifrtrnPojo.getBatccoy());
		postyymmInput.setAccMonth(lifrtrnPojo.getBatcactmn());
		postyymmInput.setAccYear(lifrtrnPojo.getBatcactyr());
		postyymmInput.setEffDate(lifrtrnPojo.getEffdate());
		PostyymmOutputDTO postyymmOutputDTO=postyymm.getPostYearMonth(postyymmInput);
		if(postyymmOutputDTO==null) {
			throw new RuntimeException("LifrtrnUtils Error");
		}
			
		
		rtrnpf.setPostyear(String.valueOf(postyymmOutputDTO.getPostYear()));
		rtrnpf.setPostmonth(String.valueOf(postyymmOutputDTO.getPostMonth()));
		rtrnpf.setEffdate(lifrtrnPojo.getEffdate());
		rtrnpf.setTranno(lifrtrnPojo.getTranno());
		if ("".equals(wsaaToday)) {
			wsaaToday = datcon1.getBusDate().toString();
			wsaaTransDate = Integer.parseInt(wsaaToday);
		}
		else {
			wsaaTransDate = Integer.parseInt(wsaaToday);
		}
		if(wsaaToday.length()>=2) {
			wsaaTranDateCc = Integer.parseInt(wsaaToday.substring(0, 2));
		}
		if(wsaaToday.length()>=4) {
			wsaaTranDateYy = Integer.parseInt(wsaaToday.substring(2, 4));
		}
		if (wsaaTranDateCc==0) {
			if (wsaaTranDateYy>90) {
				wsaaTranDateCc=19;
			}
			else {
				wsaaTranDateCc=20;
			}
		}
		rtrnpf.setTrandate(wsaaTransDate);
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		rtrnpf.setTrantime(Integer.parseInt(sdf.format(new Date()))); 
		if (lifrtrnPojo.getGenlcur() == null || "".equals(lifrtrnPojo.getGenlcur())) {
			rtrnpf.setCrate(new BigDecimal(wsaaNominalRate.toString()));
			rtrnpf.setGenlcur(wsaaLedgerCcy);
			/*  Do not forget to round the ACCTAMT*/
			rtrnpf.setAcctamt(lifrtrnPojo.getOrigamt().multiply(wsaaNominalRate).setScale(10));			
			wsaaAmountOut = amountRounding(lifrtrnPojo, rtrnpf.getAcctamt(), rtrnpf.getGenlcur());
			rtrnpf.setAcctamt(wsaaAmountOut);
		}

		wsaaAmountOut = amountRounding(lifrtrnPojo, rtrnpf.getOrigamt(), lifrtrnPojo.getOrigcurr());
		rtrnpf.setOrigamt(wsaaAmountOut);

		if ("".equals(lifrtrnPojo.getPostmonth())) {
			rtrnpf.setPostmonth(rtrnpf.getBatcactmn().toString());
		}
		if ("".equals(lifrtrnPojo.getPostyear())) {
			rtrnpf.setPostyear(rtrnpf.getBatcactyr().toString());
		}
		rtrnpfDAO.insertRtrnRecord(rtrnpf);
		
		return rtrnpf;
		
	}
	
	public BigDecimal getRate(LifrtrnDTO lifrtrnPojo, String itemkey) throws IOException{
		
		T3629 t3629IO;
		TableItem<T3629> t3629 = itempfService.readSmartTableByTableNameAndItem(lifrtrnPojo.getBatccoy(), "T3629", itemkey, CommonConstants.IT_ITEMPFX, T3629.class);
		BigDecimal wsaaNominalRate = BigDecimal.ZERO; 
		if(t3629 != null){
			t3629IO = t3629.getItemDetail();
			wsaaLastOrigCcy = lifrtrnPojo.getOrigcurr();
			wsaaLedgerCcy = t3629IO.getLedgcurr();
			for(int i = 1; (i <=7 || wsaaNominalRate.compareTo(BigDecimal.ZERO)==0); i++ ){
				if (lifrtrnPojo.getEffdate()>=t3629IO.getFrmdates().get(i-1)
						&& lifrtrnPojo.getEffdate()<=t3629IO.getTodates().get(i-1)) {
							wsaaNominalRate=t3629IO.getScrates().get(i-1);
							break;
						}
			}
			if (wsaaNominalRate.compareTo(BigDecimal.ZERO)==0) {
				if ("".equals(t3629IO.getContitem().trim())) {
					getRate(lifrtrnPojo,t3629IO.getContitem());
				}
				else {
					throw new RuntimeException("SubLedgerPostingService T3629 Error");
				}
			}
		}
		
		return wsaaNominalRate;
	}

	/*Post CURRENT BALANCE AMT*/
	public void postCurBalAmount(LifrtrnDTO lifrtrnPojo) throws IOException{
		Acblpf acblpf = new Acblpf();
		acblpf.setRldgcoy(lifrtrnPojo.getRldgcoy());
		acblpf.setSacscode(lifrtrnPojo.getSacscode());
		acblpf.setRldgacct(lifrtrnPojo.getRldgacct());
		acblpf.setSacstyp(lifrtrnPojo.getSacstyp());
		acblpf.setOrigcurr(lifrtrnPojo.getOrigcurr());
		List<Acblpf> list = acblpfDAO.getAcblpfList(acblpf);
	 
		if (list == null || list.size()==0) {
			acblpf = new Acblpf();
			acblpf.setRldgcoy(lifrtrnPojo.getRldgcoy());
			acblpf.setSacscode(lifrtrnPojo.getSacscode());
			acblpf.setRldgacct(lifrtrnPojo.getRldgacct());
			acblpf.setSacstyp(lifrtrnPojo.getSacstyp());
			acblpf.setOrigcurr(lifrtrnPojo.getOrigcurr());
			acblpf.setSacscurbal(BigDecimal.ZERO);
			acblpf.setRldgpfx("");
		}else {
			acblpf = list.get(0);
		}
		if ("-".equals(lifrtrnPojo.getGlsign())) {
			acblpf.setSacscurbal(acblpf.getSacscurbal().subtract(lifrtrnPojo.getOrigamt()).setScale(2));
		}
		else {
			acblpf.setSacscurbal(acblpf.getSacscurbal().add(lifrtrnPojo.getOrigamt()).setScale(2));
		}

		BigDecimal wsaaAmountOut = amountRounding(lifrtrnPojo, acblpf.getSacscurbal(), acblpf.getOrigcurr());
		acblpf.setSacscurbal(new BigDecimal(wsaaAmountOut.toString().replace("}", "")));
		acblpf.setDatime(lifrtrnPojo.getDatime());
		acblpf.setUsrprf(lifrtrnPojo.getUsrprf());
		acblpf.setJobnm(lifrtrnPojo.getJobnm());
		if(acblpf.getUnique_Number()==0){
			acblpfDAO.insertAcblpf(acblpf);
		}else{
			acblpfDAO.updateAcblpf(acblpf);
		}
	}
	
	public BigDecimal amountRounding(LifrtrnDTO lifrtrnPojo, BigDecimal amountIn, String curr) throws IOException{
		ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();
		zrdecplcDTO.setAmountIn(amountIn);
		zrdecplcDTO.setCurrency(curr);
		zrdecplcDTO.setCompany(lifrtrnPojo.getBatccoy());
		zrdecplcDTO.setBatctrcde(lifrtrnPojo.getBatctrcde());

		return zrdecplc.convertAmount(zrdecplcDTO);
		
	}
}
