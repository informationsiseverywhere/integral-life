package com.dxc.integral.life.smarttable.pojo;

public class T3620{

  	private String accInd;
  	private String agntsdets;
  	private String ddind;
  	private String grpind;
  	private String mediaRequired;
  	private String payind;
  	private String sacscode;
  	private String sacstyp;
  	private String triggerRequired;
  	private String printLocation;
  	private String crcind;
	private String rollovrind;
	public String getAccInd() {
		return accInd;
	}
	public void setAccInd(String accInd) {
		this.accInd = accInd;
	}
	public String getAgntsdets() {
		return agntsdets;
	}
	public void setAgntsdets(String agntsdets) {
		this.agntsdets = agntsdets;
	}
	public String getDdind() {
		return ddind;
	}
	public void setDdind(String ddind) {
		this.ddind = ddind;
	}
	public String getGrpind() {
		return grpind;
	}
	public void setGrpind(String grpind) {
		this.grpind = grpind;
	}
	public String getMediaRequired() {
		return mediaRequired;
	}
	public void setMediaRequired(String mediaRequired) {
		this.mediaRequired = mediaRequired;
	}
	public String getPayind() {
		return payind;
	}
	public void setPayind(String payind) {
		this.payind = payind;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getTriggerRequired() {
		return triggerRequired;
	}
	public void setTriggerRequired(String triggerRequired) {
		this.triggerRequired = triggerRequired;
	}
	public String getPrintLocation() {
		return printLocation;
	}
	public void setPrintLocation(String printLocation) {
		this.printLocation = printLocation;
	}
	public String getCrcind() {
		return crcind;
	}
	public void setCrcind(String crcind) {
		this.crcind = crcind;
	}
	public String getRollovrind() {
		return rollovrind;
	}
	public void setRollovrind(String rollovrind) {
		this.rollovrind = rollovrind;
	}
}