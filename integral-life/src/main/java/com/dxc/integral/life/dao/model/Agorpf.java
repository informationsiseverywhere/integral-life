/******************************************************************************
 * File Name 		: Agorpf.java
 * Author			: smalchi2
 * Creation Date	: 12 January 2017
 * Project			: Integral Life
 * Description		: The Model Class for AGORPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Agorpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "AGORPF";

	//member variables for columns
	private long uniqueNumber;
	private String agntcoy;
	private String agntnum;
	private String reportag;
	private String agtype01;
	private String agtype02;
	private Integer effdate01;
	private Integer effdate02;
	private String descT;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Agorpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getAgntcoy(){
		return this.agntcoy;
	}
	public String getAgntnum(){
		return this.agntnum;
	}
	public String getReportag(){
		return this.reportag;
	}
	public String getAgtype01(){
		return this.agtype01;
	}
	public String getAgtype02(){
		return this.agtype02;
	}
	public Integer getEffdate01(){
		return this.effdate01;
	}
	public Integer getEffdate02(){
		return this.effdate02;
	}
	public String getDescT(){
		return this.descT;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setAgntcoy( String agntcoy ){
		 this.agntcoy = agntcoy;
	}
	public void setAgntnum( String agntnum ){
		 this.agntnum = agntnum;
	}
	public void setReportag( String reportag ){
		 this.reportag = reportag;
	}
	public void setAgtype01( String agtype01 ){
		 this.agtype01 = agtype01;
	}
	public void setAgtype02( String agtype02 ){
		 this.agtype02 = agtype02;
	}
	public void setEffdate01( Integer effdate01 ){
		 this.effdate01 = effdate01;
	}
	public void setEffdate02( Integer effdate02 ){
		 this.effdate02 = effdate02;
	}
	public void setDescT( String descT ){
		 this.descT = descT;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		 this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("AGNTCOY:		");
		output.append(getAgntcoy());
		output.append("\r\n");
		output.append("AGNTNUM:		");
		output.append(getAgntnum());
		output.append("\r\n");
		output.append("REPORTAG:		");
		output.append(getReportag());
		output.append("\r\n");
		output.append("AGTYPE01:		");
		output.append(getAgtype01());
		output.append("\r\n");
		output.append("AGTYPE02:		");
		output.append(getAgtype02());
		output.append("\r\n");
		output.append("EFFDATE01:		");
		output.append(getEffdate01());
		output.append("\r\n");
		output.append("EFFDATE02:		");
		output.append(getEffdate02());
		output.append("\r\n");
		output.append("DESC_T:		");
		output.append(getDescT());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
