package com.dxc.integral.life.smarttable.pojo;

public class T5655 {
  
  	private int leadDays;

	public int getLeadDays() {
		return leadDays;
	}

	public void setLeadDays(int leadDays) {
		this.leadDays = leadDays;
	}
}