package com.dxc.integral.life.dao.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ZrstpfDAO;
import com.dxc.integral.life.dao.model.Zrstpf;

/**
 * 
 * @author xma3
 *
 */
@Repository("zrstpfDAO")
@Lazy
public class ZrstpfDAOImpl extends BaseDAOImpl implements ZrstpfDAO {

	@Override
	public List<Zrstpf> getZrstpfRecord(String coy, String chdrnum, String life) {
		StringBuilder sql = new StringBuilder("SELECT * FROM ZRSTNUD WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, ");
		sql.append("RIDER ASC,UNIQUE_NUMBER DESC");
		return jdbcTemplate.query(sql.toString(), new Object[] {coy, chdrnum, life},new BeanPropertyRowMapper<Zrstpf>(Zrstpf.class));
	}
	
	@Override
	public int insertZrstpf(Zrstpf zrstpf) {
		StringBuilder sql = new StringBuilder("INSERT INTO ZRSTPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,");
		sql.append("USTMNO,FDBKIND,USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		return jdbcTemplate.update(sql.toString(), new Object[]{
				zrstpf.getChdrcoy(),zrstpf.getChdrnum(),zrstpf.getLife(),zrstpf.getJlife(),zrstpf.getCoverage(),zrstpf.getRider(),zrstpf.getSeqno(),zrstpf.getTranno()
				,zrstpf.getXtranno(),zrstpf.getBatctrcde(),zrstpf.getSacstyp(),zrstpf.getZramount01(),zrstpf.getZramount02(),zrstpf.getTrandate(),zrstpf.getUstmno()
				,zrstpf.getFeedbackInd(),zrstpf.getUserProfile(),zrstpf.getJobName(),new Timestamp(System.currentTimeMillis())
			});
	}
	
	@Override
	public int updateZrstpfZramount(BigDecimal zramount, long uniqueNumber) {
		String sql = "UPDATE ZRSTPF SET ZRAMOUNT02=? WHERE UNIQUE_NUMBER=? ";
		return jdbcTemplate.update(sql, new Object[] {zramount, uniqueNumber});
	}

}
