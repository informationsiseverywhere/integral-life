package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AcagpfDAO;
import com.dxc.integral.life.dao.model.Acagpf;
/**
 * @author wli31
 */
@Repository("acagpfDAO")
@Lazy
public class AcagpfDAOImpl extends BaseDAOImpl implements AcagpfDAO {

	@Override
	public int insertAcagpf(Acagpf acagpf) {
		String sql = "INSERT INTO Acagpf(RLDGCOY,SACSCODE,RLDGPFX,RLDGACCT,ORIGCURR,SACSTYP,SACSCURBAL,RDOCNUM,GLSIGN,USRPRF,JOBNM,DATIME)VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?)";
		return jdbcTemplate.update(sql, new Object[] { acagpf.getRldgcoy(), acagpf.getSacscode(),
			acagpf.getRldgpfx() , acagpf.getRldgacct(), acagpf.getOrigcurr(), acagpf.getSacstyp(),
			acagpf.getSacscurbal(), acagpf.getRdocnum(),acagpf.getGlsign(),acagpf.getUserProfile(),acagpf.getJobName(),acagpf.getDatime()});
	}
	
	@Override
	public void insertAcagpfRecords(List<Acagpf> acagpfList) {
		final List<Acagpf> tempAcagpfList = acagpfList;   
		StringBuffer sql = new StringBuffer("INSERT INTO Acagpf(RLDGCOY,SACSCODE,RLDGPFX,RLDGACCT,ORIGCURR,SACSTYP,SACSCURBAL,RDOCNUM,GLSIGN,USRPRF,JOBNM,DATIME) VALUES");
		sql.append("(?,?,?,?,?,?,?,?,?,?,?,?)");
		jdbcTemplate.batchUpdate(sql.toString(),new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
            	return tempAcagpfList.size();
            }
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	ps.setString(1, acagpfList.get(i).getRldgcoy());
            	ps.setString(2, acagpfList.get(i).getSacscode());
            	ps.setString(3, acagpfList.get(i).getRldgpfx());
            	ps.setString(4, acagpfList.get(i).getRldgacct());
            	ps.setString(5, acagpfList.get(i).getOrigcurr());
            	ps.setString(6, acagpfList.get(i).getSacstyp());
            	ps.setBigDecimal(7, acagpfList.get(i).getSacscurbal());
            	ps.setString(8, acagpfList.get(i).getRdocnum());
            	ps.setString(9, acagpfList.get(i).getGlsign());
            	ps.setString(10, acagpfList.get(i).getUserProfile());
            	ps.setString(11, acagpfList.get(i).getJobName());
            	ps.setTimestamp(12,new Timestamp(System.currentTimeMillis()));
            }
		});

	}

}
