package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.UntallDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.beans.VpxacblDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.beans.VpxlextDTO;

public interface VPMScaller {
	public void callPRMPMREST(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec,
			VpxacblDTO vpxacblDTO, VpxlextDTO vpxlextDTO);
	
	public void callPRMPMRESTNew(VPMSinfoDTO vpmsinfoDTO, PremiumDTO premiumrec, VpxlextDTO vpxlextDTO,VpxchdrDTO vpxchdrDTO);
	public void callVPMCMCPMT(VPMSinfoDTO vpmsinfoDTO, ComlinkDTO comlink);
	public void callVEUAL(VPMSinfoDTO vpmsinfoDTO, UntallDTO untall);	
	public boolean checkVPMSModel(String modelName);
}
