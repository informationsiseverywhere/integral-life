package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class ComlinkDTO {
	private String function;
	private String statuz;
	private String agent;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String crtable;
	private String jlife;
	private String method;
	private String agentClass;
	private BigDecimal annprem;
	private BigDecimal instprem;
	private BigDecimal icommtot;
	private BigDecimal icommpd;
	private BigDecimal icommernd;
	private BigDecimal payamnt;
	private BigDecimal erndamt;
	private int effdate;
	private String billfreq;
	private int currto;
	private BigDecimal targetPrem;
	private int seqno;
	private String zorcode;
	private String zcomcode;
//	private int efdate;
	private String language;
	private int ptdate;
	  	/*ILIFE-3771 Code Promotion for VPMS externalization of LIFE Basic Commission Calculation Start*/
	private int cltdob;
	private int premCessDate;
	  	/*ILIFE-3771 End*/
	 // ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization start
	private String srcebus;
	private String cnttype;
	private String agentype;
	private int anbccd;
	
	
	
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getAgentClass() {
		return agentClass;
	}
	public void setAgentClass(String agentClass) {
		this.agentClass = agentClass;
	}
	public BigDecimal getAnnprem() {
		return annprem;
	}
	public void setAnnprem(BigDecimal annprem) {
		this.annprem = annprem;
	}
	public BigDecimal getInstprem() {
		return instprem;
	}
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}
	public BigDecimal getIcommtot() {
		return icommtot;
	}
	public void setIcommtot(BigDecimal icommtot) {
		this.icommtot = icommtot;
	}
	public BigDecimal getIcommpd() {
		return icommpd;
	}
	public void setIcommpd(BigDecimal icommpd) {
		this.icommpd = icommpd;
	}
	public BigDecimal getIcommernd() {
		return icommernd;
	}
	public void setIcommernd(BigDecimal icommernd) {
		this.icommernd = icommernd;
	}
	public BigDecimal getPayamnt() {
		return payamnt;
	}
	public void setPayamnt(BigDecimal payamnt) {
		this.payamnt = payamnt;
	}
	public BigDecimal getErndamt() {
		return erndamt;
	}
	public void setErndamt(BigDecimal erndamt) {
		this.erndamt = erndamt;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public BigDecimal getTargetPrem() {
		return targetPrem;
	}
	public void setTargetPrem(BigDecimal targetPrem) {
		this.targetPrem = targetPrem;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getZorcode() {
		return zorcode;
	}
	public void setZorcode(String zorcode) {
		this.zorcode = zorcode;
	}
	public String getZcomcode() {
		return zcomcode;
	}
	public void setZcomcode(String zcomcode) {
		this.zcomcode = zcomcode;
	}
//	public int getEfdate() {
//		return efdate;
//	}
//	public void setEfdate(int efdate) {
//		this.efdate = efdate;
//	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public int getCltdob() {
		return cltdob;
	}
	public void setCltdob(int cltdob) {
		this.cltdob = cltdob;
	}
	public int getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(int premCessDate) {
		this.premCessDate = premCessDate;
	}
	public String getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(String srcebus) {
		this.srcebus = srcebus;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getAgentype() {
		return agentype;
	}
	public void setAgentype(String agentype) {
		this.agentype = agentype;
	}
	public int getAnbccd() {
		return anbccd;
	}
	public void setAnbccd(int anbccd) {
		this.anbccd = anbccd;
	}
	
	
}
