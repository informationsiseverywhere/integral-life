package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.RecopstDTO;

public interface Rfndpst {
	void execute(RecopstDTO recopstDTO) throws IOException, ParseException;
}
