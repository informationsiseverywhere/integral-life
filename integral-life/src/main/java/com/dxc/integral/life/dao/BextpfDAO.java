package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Bextpf;
/**
 * @author wli31
 */
public interface BextpfDAO {
	public int insertBextPF(Bextpf bextpf);
	public int deleteBextpf(String chdrcoy, String chdrnum, int btdate);
	
	void insertBextPFList(List<Bextpf> bextpfList);
}
