package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Sttrpf;

public interface SttrpfDAO {
	public List<Sttrpf> searchSttrpfRecord(String chdrcoy, String chdrnum);
	public void insertSttrpfRecord(List<Sttrpf> insertList);
}
