package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.PostyymmInputDTO;
import com.dxc.integral.fsu.beans.PostyymmOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.fsu.utils.Postyymm;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
//import com.dxc.integral.iaf.dao.BatcpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
//import com.dxc.integral.iaf.dao.model.Batcpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.RlagprdDTO;
import com.dxc.integral.life.dao.AgpypfDAO;
import com.dxc.integral.life.dao.model.Agpypf;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.LifacmvValidatorUtil;
import com.dxc.integral.life.utils.Rlagprd;

/**
 * The Lifacmv utility implementation.
 * 
 * @author vhukumagrawa
 *
 */
@Service("lifacmv")
@Lazy
public class LifacmvImpl implements Lifacmv {
	
	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(LifacmvImpl.class);

	@Autowired
	private AcmvpfDAO acmvpfDAO;

	@Autowired
	private AgpypfDAO agpypfDAO;

	@Autowired
	private AcblpfDAO acblpfDAO;

	@Autowired
	private ChdrpfDAO chdrpfDAO;

	@Autowired
	private Postyymm postyymm;

	@Autowired
	private Zrdecplc zrdecplc;

	@Autowired
	private Rlagprd rlagprd;

/*	@Autowired
	private BatcpfDAO batcpfDAO;*/
	
	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/

	private T3695 t3695Item = null;

	private String lastOriginalCurrency = "";
	private String ledgerCurrency = "";

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Lifacmv#executePSTW(com.dxc.integral.life.beans.LifacmvDTO)
	 */
	@Override/*ILIFE-5977*/
	public void executePSTW(LifacmvDTO lifeacmvdto)
			throws IOException,ParseException {//ILIFE-5763
		
		lastOriginalCurrency = "";

		writeAcmvpf(lifeacmvdto);/*ILIFE-5977*/

		if ("Y".equals(t3695Item.getTlprmflg()) || "Y".equals(t3695Item.getTlcomflg())) {
			Chdrpf chdrpf = readContractDetail(lifeacmvdto);
			RlagprdDTO rlagprdDTO = new RlagprdDTO();
			rlagprdDTO.setAgntcoy(lifeacmvdto.getBatccoy());
			rlagprdDTO.setAgntnum(chdrpf.getAgntnum());
			rlagprdDTO.setActyear(lifeacmvdto.getBatcactyr());
			rlagprdDTO.setActmnth(lifeacmvdto.getBatcactmn());
			rlagprdDTO.setAgtype("");
			rlagprdDTO.setChdrnum(chdrpf.getChdrnum());
			rlagprdDTO.setOccdate(chdrpf.getOccdate());
			rlagprdDTO.setCnttype(chdrpf.getCnttype());
			rlagprdDTO.setChdrcoy(chdrpf.getChdrcoy());
			rlagprdDTO.setBatctrcde(lifeacmvdto.getBatctrcde());
			rlagprdDTO.setEffdate(lifeacmvdto.getEffdate());
			BigDecimal wsaaOrigamt = lifeacmvdto.getOrigamt();
			if ("Y".equals(t3695Item.getTlprmflg())) {
				rlagprdDTO.setPrdflg("P");
				if ("-".equals(lifeacmvdto.getGlsign())) {
					wsaaOrigamt = wsaaOrigamt.setScale(2, RoundingMode.HALF_DOWN);
				}
				wsaaOrigamt = wsaaOrigamt.setScale(2, RoundingMode.HALF_DOWN);
			} else {
				rlagprdDTO.setPrdflg("C");
			}
			rlagprdDTO.setOrigamt(wsaaOrigamt);
			rlagprd.calculateAndUpdateMAPR(rlagprdDTO);/*ILIFE-5977*/
		}

		if (!"".equals(lifeacmvdto.getSacscode())) {
			updateAcbl(lifeacmvdto);/*ILIFE-5977*/
		}
		// callBatcup(lifeacmvdto);
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Lifacmv#executeNPSTW(com.dxc.integral.life.beans.LifacmvDTO)
	 */
	@Override/*ILIFE-5977*/
	public void executeNPSTW(LifacmvDTO lifeacmvdto)throws IOException,ParseException {//ILIFE-5763

		lastOriginalCurrency = "";

		writeAcmvpf(lifeacmvdto);/*ILIFE-5977*/

		if ("Y".equals(t3695Item.getTlprmflg()) || "Y".equals(t3695Item.getTlcomflg())) {
			Chdrpf chdrpf = readContractDetail(lifeacmvdto);

			RlagprdDTO rlagprdDTO = new RlagprdDTO();
			rlagprdDTO.setAgntcoy(lifeacmvdto.getBatccoy());
			rlagprdDTO.setAgntnum(chdrpf.getAgntnum());
			rlagprdDTO.setActyear(lifeacmvdto.getBatcactyr());
			rlagprdDTO.setActmnth(lifeacmvdto.getBatcactmn());
			rlagprdDTO.setAgtype("");
			rlagprdDTO.setChdrnum(chdrpf.getChdrnum());
			rlagprdDTO.setOccdate(chdrpf.getOccdate());
			rlagprdDTO.setCnttype(chdrpf.getCnttype());
			rlagprdDTO.setChdrcoy(chdrpf.getChdrcoy());
			rlagprdDTO.setBatctrcde(lifeacmvdto.getBatctrcde());
			rlagprdDTO.setEffdate(lifeacmvdto.getEffdate());
			BigDecimal wsaaOrigamt = lifeacmvdto.getOrigamt();
			if ("Y".equals(t3695Item.getTlprmflg())) {
				rlagprdDTO.setPrdflg("P");
				if ("-".equals(lifeacmvdto.getGlsign())) {
					wsaaOrigamt = wsaaOrigamt.setScale(2, RoundingMode.HALF_DOWN);
				}
				wsaaOrigamt = wsaaOrigamt.setScale(2, RoundingMode.HALF_DOWN);
			} else {
				rlagprdDTO.setPrdflg("C");
			}
			rlagprdDTO.setOrigamt(wsaaOrigamt);
			rlagprd.calculateAndUpdateMAPR(rlagprdDTO);/*ILIFE-5977*/
		}
		// callBatcup(lifeacmvdto);
	}
	/*ILIFE-5977*/
	private void writeAcmvpf(LifacmvDTO lifeacmvdto) throws IOException {//ILIFE-5763

		String glcode = replaceText(lifeacmvdto);
		lifeacmvdto.setGlcode(glcode);
		Acmvpf acmvpf = writeAcmv(lifeacmvdto);/*ILIFE-5977*/

		/* Write Agent Commission Payment record when T3695-COMIND = 'Y' */
		if ("Y".equals(t3695Item.getComind()) && "99999999".equals(String.valueOf(lifeacmvdto.getFrcdate()))
				&& "E".equals(t3695Item.getIntextind()) && "LA".equals(lifeacmvdto.getSacscode())) {
			writeAgpy(acmvpf);
		}
	}
	/*ILIFE-5977*/
	private Acmvpf writeAcmv(LifacmvDTO lifeacmvdto) throws IOException {//ILIFE-5763
		Map<String, Itempf> t3695map=itempfDAO.readSmartTableByTableName2(lifeacmvdto.getBatccoy(), "T3695", CommonConstants.IT_ITEMPFX);  /*IJTI-379*/
		BigDecimal nominalRate = getRate(lifeacmvdto);
		Acmvpf acmvpf = conevertToAcmvpf(lifeacmvdto, nominalRate);/*ILIFE-5977*/

		if (!"".equals(lifeacmvdto.getSacstyp())) {
			t3695Item = smartTableDataUtils.convertGenareaToPojo(t3695map.get(lifeacmvdto.getSacstyp()).getGenareaj(), T3695.class); /*IJTI-398*/
			acmvpf.setIntextind(t3695Item.getIntextind());
		} else {
			acmvpf.setIntextind("");
		}

		PostyymmInputDTO postyymmInput = new PostyymmInputDTO();
		postyymmInput.setCompany(lifeacmvdto.getBatccoy());
		postyymmInput.setAccMonth(lifeacmvdto.getBatcactmn());
		postyymmInput.setAccYear(lifeacmvdto.getBatcactyr());
		postyymmInput.setEffDate(lifeacmvdto.getEffdate());
		PostyymmOutputDTO postyymmOutputDTO = postyymm.getPostYearMonth(postyymmInput);
		acmvpf.setPostyear(String.valueOf(postyymmOutputDTO.getPostYear()));
		acmvpf.setPostmonth(String.valueOf(postyymmOutputDTO.getPostMonth()));
		LOGGER.info(toString(acmvpf));
		acmvpfDAO.insertAcmvpf(acmvpf);
		return acmvpf;
	}
	
	public String toString(Acmvpf acmvpf) {
		return "Acmvpf [unique_number=" + acmvpf.getUnique_number() + ", rdocpfx=" + acmvpf.getRdocpfx() + ", rdoccoy=" + acmvpf.getRdoccoy() + ", rdocnum="
				+ acmvpf.getRdocnum() + ", tranno=" + acmvpf.getTranno() + ", jrnseq=" + acmvpf.getJrnseq() + ", rldgpfx=" + acmvpf.getRldgpfx() + ", rldgcoy="
				+ acmvpf.getRldgcoy() + ", rldgacct=" + acmvpf.getRldgacct() + ", sacscode=" + acmvpf.getSacscode() + ", sacstyp=" + acmvpf.getSacstyp() + ", batccoy="
				+ acmvpf.getBatccoy() + ", batcbrn=" + acmvpf.getBatcbrn() + ", batcactyr=" + acmvpf.getBatcactyr() + ", batcactmn=" + acmvpf.getBatcactmn()
				+ ", batctrcde=" + acmvpf.getBatctrcde() + ", batcbatch=" + acmvpf.getBatcbatch() + ", origcurr=" + acmvpf.getOrigcurr() + ", origamt="
				+ acmvpf.getOrigamt() + ", tranref=" + acmvpf.getTranref() + ", trandesc=" + acmvpf.getTrandesc() + ", crate=" + acmvpf.getCrate() + ", acctamt="
				+ acmvpf.getAcctamt() + ", genlcoy=" + acmvpf.getGenlcoy() + ", genlcur=" + acmvpf.getGenlcur() + ", glcode=" + acmvpf.getGlcode() + ", glsign="
				+ acmvpf.getGlsign() + ", postyear=" + acmvpf.getPostyear() + ", postmonth=" + acmvpf.getPostmonth() + ", effdate=" + acmvpf.getEffdate() + ", dateto="
				+ acmvpf.getDateto() + ", rcamt=" + acmvpf.getRcamt() + ", frcdate=" + acmvpf.getFrcdate() + ", intextind=" + acmvpf.getIntextind() + ", trdt=" + acmvpf.getTrdt()
				+ ", trtm=" + acmvpf.getTrtm() + ", user_t=" + acmvpf.getUser_t() + ", termid=" + acmvpf.getTermid() + ", reconsbr=" + acmvpf.getReconsbr()
				+ ", suprflg=" + acmvpf.getSuprflg() + ", taxcode=" + acmvpf.getTaxcode() + ", reconref=" + acmvpf.getReconref() + ", stmtsort=" + acmvpf.getStmtsort()
				+ ", creddte=" + acmvpf.getCreddte() + ", usrprf=" + acmvpf.getUsrprf() + ", jobnm=" + acmvpf.getJobnm() + ", datime=" + acmvpf.getDatime() + "]";
	}
	
	/*ILIFE-5977*/
	private BigDecimal getRate(LifacmvDTO lifeacmvdto) throws IOException {//ILIFE-5763

		BigDecimal nominalRate = BigDecimal.ONE;
		if (StringUtils.isBlank(lifeacmvdto.getGenlcur()) && !lifeacmvdto.getOrigcurr().equals(lastOriginalCurrency)) {
			nominalRate = getRateSub(lifeacmvdto);/*ILIFE-5977*/
		}

		return nominalRate;
	}

	private BigDecimal getRateSub(LifacmvDTO lifeacmvdto) throws IOException {//ILIFE-5763
		Map<String, Itempf> t3629Map=itempfDAO.readSmartTableByTableName2(lifeacmvdto.getBatccoy(), "T3629", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/ /*IJTI-379*/ 
		String contitem = lifeacmvdto.getOrigcurr();
		boolean isNominalRateFound = false;
		BigDecimal nominalRate = BigDecimal.ONE;
		while (contitem != null && !contitem.isEmpty()) {
			if (t3629Map.containsKey(contitem)) {
				lastOriginalCurrency = lifeacmvdto.getOrigcurr();
				T3629 t3629 = smartTableDataUtils.convertGenareaToPojo(t3629Map.get(contitem).getGenareaj(), T3629.class); /*IJTI-398*/
				ledgerCurrency = t3629.getLedgcurr();
				contitem = t3629.getContitem();
				for (int i = 0; t3629.getFrmdates().size() > i; i++) {
					if (lifeacmvdto.getEffdate() >= t3629.getFrmdates().get(i)
							&& lifeacmvdto.getEffdate() <= t3629.getTodates().get(i)) {
						nominalRate = t3629.getScrates().get(i);
						isNominalRateFound = true;
						break;
					}
				}
				if (isNominalRateFound) {
					break;
				}
			}else{//ILIFE-5763 Starts
				throw new ItemNotfoundException("LifacmvImpl: Item "+contitem+" not found in Table T3629");
			}//ILIFE-5763 Ends
		}
		return nominalRate;
	}

	private void writeAgpy(Acmvpf acmvpf) {
		Agpypf agpypf = new Agpypf();
		agpypf.setBatccoy(acmvpf.getBatccoy());
		agpypf.setBatcbrn(acmvpf.getBatcbrn());
		agpypf.setBatcactyr(Short.valueOf(String.valueOf(acmvpf.getBatcactyr())));
		agpypf.setBatcactmn(Byte.valueOf(String.valueOf(acmvpf.getBatcactmn())));
		agpypf.setBatctrcde(acmvpf.getBatctrcde());
		agpypf.setBatcbatch(acmvpf.getBatcbatch());
		agpypf.setRdocnum(acmvpf.getRdocnum());
		agpypf.setTranno(acmvpf.getTranno());
		agpypf.setJrnseq(Short.valueOf(String.valueOf(acmvpf.getJrnseq())));
		agpypf.setRldgcoy(acmvpf.getRldgcoy());
		agpypf.setRldgacct(acmvpf.getRldgacct());
		agpypf.setOrigcurr(acmvpf.getOrigcurr());
		agpypf.setSacscode(acmvpf.getSacscode());
		agpypf.setSacstyp(acmvpf.getSacstyp());
		agpypf.setOrigamt(acmvpf.getOrigamt());
		agpypf.setTranref(acmvpf.getTranref());
		agpypf.setTrandesc(acmvpf.getTrandesc());
		agpypf.setCrate(acmvpf.getCrate());
		agpypf.setAcctamt(acmvpf.getAcctamt());
		agpypf.setGenlcoy(acmvpf.getGenlcoy());
		agpypf.setGenlcur(acmvpf.getGenlcur());
		agpypf.setGlcode(acmvpf.getGlcode());
		agpypf.setGlsign(acmvpf.getGlsign());
		agpypf.setPostyear(acmvpf.getPostyear());
		agpypf.setPostmonth(acmvpf.getPostmonth());
		agpypf.setEffdate(acmvpf.getEffdate());
		agpypf.setRcamt(acmvpf.getRcamt());
		agpypf.setFrcdate(acmvpf.getFrcdate());
		agpypf.setTrdt(acmvpf.getTrdt());
		agpypf.setTrtm(acmvpf.getTrtm());
		agpypf.setUserT(acmvpf.getUser_t());
		agpypf.setTermid(acmvpf.getTermid());
		agpypf.setSuprflg("" + acmvpf.getSuprflg());
		agpypf.setDatime(new Timestamp(System.currentTimeMillis()));
		agpypfDAO.insertAcmvpf(agpypf);
	}

	public Chdrpf readContractDetail(LifacmvDTO lifeacmvdto) {
		String contractNumber = lifeacmvdto.getRdocnum();
		Boolean cheqCodes = LifacmvValidatorUtil.cheqCodes(lifeacmvdto.getBatctrcde());
		Boolean cashCodes = LifacmvValidatorUtil.cashCodes(lifeacmvdto.getBatctrcde());
		Boolean jrnlCodes = LifacmvValidatorUtil.jrnlCodes(lifeacmvdto.getBatctrcde());
		if (cheqCodes || cashCodes || jrnlCodes) {
			contractNumber = lifeacmvdto.getRldgacct().substring(1, 8);
		}
		return chdrpfDAO.readRecordOfChdrpf(lifeacmvdto.getBatccoy(), contractNumber);
	}

	public void updateAcbl(LifacmvDTO lifeacmvdto)/*ILIFE-5977*/
			throws IOException {
		Acblpf acblpf = new Acblpf();
		acblpf.setRldgpfx("");
		acblpf.setRldgcoy(lifeacmvdto.getRldgcoy());
		acblpf.setSacscode(lifeacmvdto.getSacscode());
		acblpf.setRldgacct(lifeacmvdto.getRldgacct());
		acblpf.setOrigcurr(lifeacmvdto.getOrigcurr());
		acblpf.setSacstyp(lifeacmvdto.getSacstyp());
		acblpf.setUsrprf(lifeacmvdto.getUsrprofile());
		acblpf.setJobnm(lifeacmvdto.getJobname());
		acblpf.setDatime(new Timestamp(System.currentTimeMillis()));
		Acblpf acblpfRead = acblpfDAO.readAcblpf(acblpf);

		if(acblpf.getSacscurbal()== null){
			acblpf.setSacscurbal(BigDecimal.valueOf(0));
		}
		if ("-".equals(lifeacmvdto.getGlsign())) {
			acblpf.setSacscurbal(acblpf.getSacscurbal().subtract(lifeacmvdto.getOrigamt()).setScale(2, RoundingMode.HALF_DOWN));
		} else {
			acblpf.setSacscurbal(acblpf.getSacscurbal().add(lifeacmvdto.getOrigamt()).setScale(2, RoundingMode.HALF_DOWN));

		}
		BigDecimal outSacscurbal = callRounding(acblpf.getSacscurbal(), acblpf.getOrigcurr(), lifeacmvdto.getBatctrcde(), lifeacmvdto.getBatccoy());/*ILIFE-5977*//*IJTI-379*/
		acblpf.setSacscurbal(outSacscurbal);
		acblpf.setUsrprf(lifeacmvdto.getUsrprofile());
		acblpf.setJobnm(lifeacmvdto.getJobname());
		if (acblpfRead == null) {
			acblpfDAO.insertAcblpf(acblpf);
		} else {
			acblpf.setUniqueNumber(acblpfRead.getUniqueNumber());

			acblpfDAO.updateAcblpf(acblpf);
		}
	}

/*	void callBatcup(LifacmvDTO lifeacmvdto) {
		Batcpf batcup = new Batcpf();

		batcup.setBatcpfx("C");
		batcup.setBatccoy(lifeacmvdto.getBatccoy());
		batcup.setBatcbrn(lifeacmvdto.getBatcbrn());
		batcup.setBatcactyr(lifeacmvdto.getBatcactyr());
		batcup.setBatcactmn(lifeacmvdto.getBatcactmn());
		batcup.setBatctrcde(lifeacmvdto.getBatctrcde());
		batcup.setBatcbatch(lifeacmvdto.getBatcbatch());
		batcup.setTrancnt(Long.valueOf(0));
		batcup.setEtreqCnt(Long.valueOf(0));
		batcpfDAO.insertBatchDetails(batcup);

	}*/

	private Acmvpf conevertToAcmvpf(LifacmvDTO lifeacmvdto, BigDecimal nominalRate) throws IOException {/*ILIFE-5977*/

		Acmvpf acmvpf = new Acmvpf();
		acmvpf.setBatccoy(lifeacmvdto.getBatccoy());
		acmvpf.setBatcbrn(lifeacmvdto.getBatcbrn());
		acmvpf.setBatcactyr(lifeacmvdto.getBatcactyr());
		acmvpf.setBatcactmn(lifeacmvdto.getBatcactmn());
		acmvpf.setBatctrcde(lifeacmvdto.getBatctrcde());
		acmvpf.setBatcbatch(lifeacmvdto.getBatcbatch());
		acmvpf.setRdocnum(lifeacmvdto.getRdocnum());
		acmvpf.setTranno(lifeacmvdto.getTranno());
		acmvpf.setJrnseq(lifeacmvdto.getJrnseq());
		acmvpf.setRldgcoy(lifeacmvdto.getRldgcoy());
		acmvpf.setSacscode(lifeacmvdto.getSacscode());
		acmvpf.setRldgacct(lifeacmvdto.getRldgacct());
		acmvpf.setOrigcurr(lifeacmvdto.getOrigcurr());
		acmvpf.setSacstyp(lifeacmvdto.getSacstyp());
		acmvpf.setOrigamt(lifeacmvdto.getOrigamt());
		acmvpf.setTranref(lifeacmvdto.getTranref());
		acmvpf.setTrandesc(lifeacmvdto.getTrandesc());
		acmvpf.setCrate(lifeacmvdto.getCrate());
		acmvpf.setAcctamt(lifeacmvdto.getAcctamt());
		acmvpf.setGenlcoy(lifeacmvdto.getGenlcoy());
		acmvpf.setGenlcur(lifeacmvdto.getGenlcur());
		acmvpf.setGlcode(lifeacmvdto.getGlcode());
		acmvpf.setGlsign(lifeacmvdto.getGlsign());

		acmvpf.setEffdate(lifeacmvdto.getEffdate());
		acmvpf.setRcamt(lifeacmvdto.getRcamt());
		acmvpf.setFrcdate(lifeacmvdto.getFrcdate());
		acmvpf.setTrdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
		acmvpf.setTrtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
		acmvpf.setUser_t(lifeacmvdto.getUser());
		acmvpf.setTermid(lifeacmvdto.getTermid());
		acmvpf.setSuprflg(lifeacmvdto.getSuprflag());
		acmvpf.setReconref(0);
		acmvpf.setCreddte(99999999);
		acmvpf.setStmtsort("");
		acmvpf.setRdoccoy("");
		acmvpf.setRdocpfx("");
		acmvpf.setDateto(0);
		acmvpf.setReconsbr("");
		acmvpf.setRldgpfx("");
		acmvpf.setTaxcode("");
		acmvpf.setDatime(new Timestamp(System.currentTimeMillis()));
		acmvpf.setJobnm(lifeacmvdto.getJobname());
		acmvpf.setUsrprf(lifeacmvdto.getUsrprofile());
		if (lifeacmvdto.getGenlcur() == null || lifeacmvdto.getGenlcur().isEmpty()) {
			acmvpf.setCrate(nominalRate);
			acmvpf.setGenlcur(ledgerCurrency);
			if (lifeacmvdto.getAcctamt() == null || lifeacmvdto.getAcctamt().intValue() == 0) {
				acmvpf.setAcctamt(lifeacmvdto.getOrigamt().multiply(nominalRate));
				BigDecimal outAcctamt = callRounding(acmvpf.getAcctamt(), acmvpf.getGenlcur(), lifeacmvdto.getBatctrcde(), lifeacmvdto.getBatccoy());/*ILIFE-5977*/ /*IJTI-379*/
				acmvpf.setAcctamt(outAcctamt);
			}

		}
		BigDecimal outOrigamt = callRounding(acmvpf.getOrigamt(), acmvpf.getOrigcurr(), lifeacmvdto.getBatctrcde(), lifeacmvdto.getBatccoy());/*ILIFE-5977*/ /*IJTI-379*/
		acmvpf.setOrigamt(outOrigamt);
		if (lifeacmvdto.getTranno() == 0) {
			if ("G".equals(lifeacmvdto.getPrefix())) {
				acmvpf.setRldgpfx(lifeacmvdto.getPrefix());
			}
			if ("D".equals(lifeacmvdto.getPrefix())) {
				acmvpf.setRdocpfx(lifeacmvdto.getPrefix());
			}
		}
		return acmvpf;
	}

	private String replaceText(LifacmvDTO lifeacmvdto) {
		String glCode = lifeacmvdto.getGlcode();
		String[] strCode = new String[] { "@", "*", "%", "!", "+", "=" };
		if (glCode != null) {
			glCode = glCode.replaceAll("\\?", lifeacmvdto.getBatccoy());
			glCode = glCode.replaceAll("##", lifeacmvdto.getBatcbrn());
			glCode = glCode.replaceAll("&&&", lifeacmvdto.getOrigcurr());

			List<String> subsCodeList = lifeacmvdto.getSubstituteCodes();
			if (subsCodeList != null) {
				glCode = replaceTextSub(subsCodeList, strCode, glCode);
			}
		}
		return glCode;
	}

	private String replaceTextSub(List<String> subsCodeList, String[] strCode, String glCode) {
		String glCodeStr = glCode;
		for (int i = 0; i < subsCodeList.size(); i++) {
			String replaceStr = strCode[i];
			if ("+".equals(replaceStr)) replaceStr = "\\+";
			else if ("$".equals(replaceStr)) replaceStr = "\\$";
			else if ("?".equals(replaceStr)) replaceStr = "\\?"; 
			if (subsCodeList.get(i) != null && !subsCodeList.get(i).isEmpty()) {
				char[] code = subsCodeList.get(i).toCharArray();
				for (char cd : code) {
					glCodeStr = glCodeStr.replaceFirst(replaceStr, String.valueOf(cd));
				}
			}
		}
		return glCodeStr;
	}

	private BigDecimal callRounding(BigDecimal amountIn, String currency, String batctrcde, String company) throws IOException {/*ILIFE-5977*/ /*IJTI-379*/
		BigDecimal outAmount;
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setBatctrcde(batctrcde);
		zrdecplDTO.setCurrency(currency);
		zrdecplDTO.setCompany(company); /*IJTI-379*/
		outAmount = zrdecplc.convertAmount(zrdecplDTO);/*ILIFE-5977*/
		return outAmount;
	}
}
