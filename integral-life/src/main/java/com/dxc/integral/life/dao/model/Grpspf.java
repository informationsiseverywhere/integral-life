package com.dxc.integral.life.dao.model;
/**
 * @author wli31
 */
public class Grpspf {
	private long unique_Number;
	private String grupcoy;
	private String grupnum;
	private String clntnum;
	private String grupname;
	private int dtecrt;
	private String membreq;
	private String billfreq;
	private int billcd;
	private String billchnL;
	private String billday;
	private String billmonth;
	private int btdate;
	private int ptdate;
	private String facthous;
	private String bankkey;
	private String bankacckey;
	private String currcode;
	private String fao;
	private String billseq;
	private int billdate;
	private int leadys;
	private int grpduedy;
	private String premdisc;
	private String cashdisc;
	private String validflag;
	private int currfrom;
	private int currto;
	private String billnet;
	private String mandref;
	private String termid;
	private String user_T;
	private int trdt;
	private int trtm;
	private String usrprf;
	private String jobnm;
	private String datime;
	public long getUnique_Number() {
		return unique_Number;
	}
	public void setUnique_Number(long unique_Number) {
		this.unique_Number = unique_Number;
	}
	public String getGrupcoy() {
		return grupcoy;
	}
	public void setGrupcoy(String grupcoy) {
		this.grupcoy = grupcoy;
	}
	public String getGrupnum() {
		return grupnum;
	}
	public void setGrupnum(String grupnum) {
		this.grupnum = grupnum;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getGrupname() {
		return grupname;
	}
	public void setGrupname(String grupname) {
		this.grupname = grupname;
	}
	public int getDtecrt() {
		return dtecrt;
	}
	public void setDtecrt(int dtecrt) {
		this.dtecrt = dtecrt;
	}
	public String getMembreq() {
		return membreq;
	}
	public void setMembreq(String membreq) {
		this.membreq = membreq;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getBillchnL() {
		return billchnL;
	}
	public void setBillchnL(String billchnL) {
		this.billchnL = billchnL;
	}
	public String getBillday() {
		return billday;
	}
	public void setBillday(String billday) {
		this.billday = billday;
	}
	public String getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(String billmonth) {
		this.billmonth = billmonth;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getFao() {
		return fao;
	}
	public void setFao(String fao) {
		this.fao = fao;
	}
	public String getBillseq() {
		return billseq;
	}
	public void setBillseq(String billseq) {
		this.billseq = billseq;
	}
	public int getBilldate() {
		return billdate;
	}
	public void setBilldate(int billdate) {
		this.billdate = billdate;
	}
	public int getLeadys() {
		return leadys;
	}
	public void setLeadys(int leadys) {
		this.leadys = leadys;
	}
	public int getGrpduedy() {
		return grpduedy;
	}
	public void setGrpduedy(int grpduedy) {
		this.grpduedy = grpduedy;
	}
	public String getPremdisc() {
		return premdisc;
	}
	public void setPremdisc(String premdisc) {
		this.premdisc = premdisc;
	}
	public String getCashdisc() {
		return cashdisc;
	}
	public void setCashdisc(String cashdisc) {
		this.cashdisc = cashdisc;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public String getBillnet() {
		return billnet;
	}
	public void setBillnet(String billnet) {
		this.billnet = billnet;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getUser_T() {
		return user_T;
	}
	public void setUser_T(String user_T) {
		this.user_T = user_T;
	}
	public int getTrdt() {
		return trdt;
	}
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	public int getTrtm() {
		return trtm;
	}
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}

}