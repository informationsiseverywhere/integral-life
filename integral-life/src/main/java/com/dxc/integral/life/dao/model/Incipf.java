package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Incipf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private String validflag;
	private int tranno;
	private int rcdate;
	private int premCessDate;
	private BigDecimal origPrem;
	private BigDecimal currPrem;
	private BigDecimal premStart01;
	private BigDecimal premStart02;
	private BigDecimal premStart03;
	private BigDecimal premStart04;
	private BigDecimal premStart05;
	private BigDecimal premStart06;
	private BigDecimal premStart07;
	private BigDecimal premCurr01;
	private BigDecimal premCurr02;
	private BigDecimal premCurr03;
	private BigDecimal premCurr04;
	private BigDecimal premCurr05;
	private BigDecimal premCurr06;
	private BigDecimal premCurr07;
	private BigDecimal pcUnits01;
	private BigDecimal pcUnits02;
	private BigDecimal pcUnits03;
	private BigDecimal pcUnits04;
	private BigDecimal pcUnits05;
	private BigDecimal pcUnits06;
	private BigDecimal pcUnits07;
	private BigDecimal unitSplit01;
	private BigDecimal unitSplit02;
	private BigDecimal unitSplit03;
	private BigDecimal unitSplit04;
	private BigDecimal unitSplit05;
	private BigDecimal unitSplit06;
	private BigDecimal unitSplit07;
	private int inciNum;
	private String crtable;
	private int user;
	private int transactionTime;
	private int transactionDate;
	private String termid;
	private int planSuffix;
	private int seqno;
	private String dormantFlag;
	private String userProfile;
	private String jobName;
	private String datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public int getRcdate() {
		return rcdate;
	}

	public void setRcdate(int rcdate) {
		this.rcdate = rcdate;
	}

	public int getPremCessDate() {
		return premCessDate;
	}

	public void setPremCessDate(int premCessDate) {
		this.premCessDate = premCessDate;
	}

	public BigDecimal getOrigPrem() {
		return origPrem;
	}

	public void setOrigPrem(BigDecimal origPrem) {
		this.origPrem = origPrem;
	}

	public BigDecimal getCurrPrem() {
		return currPrem == null ? BigDecimal.ZERO : currPrem;
	}

	public void setCurrPrem(BigDecimal currPrem) {
		this.currPrem = currPrem;
	}

	public BigDecimal getPremStart01() {
		return premStart01;
	}

	public void setPremStart01(BigDecimal premStart01) {
		this.premStart01 = premStart01;
	}

	public BigDecimal getPremStart02() {
		return premStart02;
	}

	public void setPremStart02(BigDecimal premStart02) {
		this.premStart02 = premStart02;
	}

	public BigDecimal getPremStart03() {
		return premStart03;
	}

	public void setPremStart03(BigDecimal premStart03) {
		this.premStart03 = premStart03;
	}

	public BigDecimal getPremStart04() {
		return premStart04;
	}

	public void setPremStart04(BigDecimal premStart04) {
		this.premStart04 = premStart04;
	}

	public BigDecimal getPremCurr01() {
		return premCurr01;
	}

	public void setPremCurr01(BigDecimal premCurr01) {
		this.premCurr01 = premCurr01;
	}

	public BigDecimal getPremCurr02() {
		return premCurr02;
	}

	public void setPremCurr02(BigDecimal premCurr02) {
		this.premCurr02 = premCurr02;
	}

	public BigDecimal getPremCurr03() {
		return premCurr03;
	}

	public void setPremCurr03(BigDecimal premCurr03) {
		this.premCurr03 = premCurr03;
	}

	public BigDecimal getPremCurr04() {
		return premCurr04;
	}

	public void setPremCurr04(BigDecimal premCurr04) {
		this.premCurr04 = premCurr04;
	}

	public BigDecimal getPcUnits01() {
		return pcUnits01;
	}

	public void setPcUnits01(BigDecimal pcUnits01) {
		this.pcUnits01 = pcUnits01;
	}

	public BigDecimal getPcUnits02() {
		return pcUnits02;
	}

	public void setPcUnits02(BigDecimal pcUnits02) {
		this.pcUnits02 = pcUnits02;
	}

	public BigDecimal getPcUnits03() {
		return pcUnits03;
	}

	public void setPcUnits03(BigDecimal pcUnits03) {
		this.pcUnits03 = pcUnits03;
	}

	public BigDecimal getPcUnits04() {
		return pcUnits04;
	}

	public void setPcUnits04(BigDecimal pcUnits04) {
		this.pcUnits04 = pcUnits04;
	}

	public BigDecimal getUnitSplit01() {
		return unitSplit01;
	}

	public void setUnitSplit01(BigDecimal unitSplit01) {
		this.unitSplit01 = unitSplit01;
	}

	public BigDecimal getUnitSplit02() {
		return unitSplit02;
	}

	public void setUnitSplit02(BigDecimal unitSplit02) {
		this.unitSplit02 = unitSplit02;
	}

	public BigDecimal getUnitSplit03() {
		return unitSplit03;
	}

	public void setUnitSplit03(BigDecimal unitSplit03) {
		this.unitSplit03 = unitSplit03;
	}

	public BigDecimal getUnitSplit04() {
		return unitSplit04;
	}

	public void setUnitSplit04(BigDecimal unitSplit04) {
		this.unitSplit04 = unitSplit04;
	}

	public int getInciNum() {
		return inciNum;
	}

	public void setInciNum(int inciNum) {
		this.inciNum = inciNum;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public int getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}

	public int getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getSeqno() {
		return seqno;
	}

	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	public String getDormantFlag() {
		return dormantFlag;
	}

	public void setDormantFlag(String dormantFlag) {
		this.dormantFlag = dormantFlag;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public BigDecimal getPremStart05() {
		return premStart05;
	}

	public void setPremStart05(BigDecimal premStart05) {
		this.premStart05 = premStart05;
	}

	public BigDecimal getPremStart06() {
		return premStart06;
	}

	public void setPremStart06(BigDecimal premStart06) {
		this.premStart06 = premStart06;
	}

	public BigDecimal getPremStart07() {
		return premStart07;
	}

	public void setPremStart07(BigDecimal premStart07) {
		this.premStart07 = premStart07;
	}

	public BigDecimal getPremCurr05() {
		return premCurr05;
	}

	public void setPremCurr05(BigDecimal premCurr05) {
		this.premCurr05 = premCurr05;
	}

	public BigDecimal getPremCurr06() {
		return premCurr06;
	}

	public void setPremCurr06(BigDecimal premCurr06) {
		this.premCurr06 = premCurr06;
	}

	public BigDecimal getPremCurr07() {
		return premCurr07;
	}

	public void setPremCurr07(BigDecimal premCurr07) {
		this.premCurr07 = premCurr07;
	}

	public BigDecimal getPcUnits05() {
		return pcUnits05;
	}

	public void setPcUnits05(BigDecimal pcUnits05) {
		this.pcUnits05 = pcUnits05;
	}

	public BigDecimal getPcUnits06() {
		return pcUnits06;
	}

	public void setPcUnits06(BigDecimal pcUnits06) {
		this.pcUnits06 = pcUnits06;
	}

	public BigDecimal getPcUnits07() {
		return pcUnits07;
	}

	public void setPcUnits07(BigDecimal pcUnits07) {
		this.pcUnits07 = pcUnits07;
	}

	public BigDecimal getUnitSplit05() {
		return unitSplit05;
	}

	public void setUnitSplit05(BigDecimal unitSplit05) {
		this.unitSplit05 = unitSplit05;
	}

	public BigDecimal getUnitSplit06() {
		return unitSplit06;
	}

	public void setUnitSplit06(BigDecimal unitSplit06) {
		this.unitSplit06 = unitSplit06;
	}

	public BigDecimal getUnitSplit07() {
		return unitSplit07;
	}

	public void setUnitSplit07(BigDecimal unitSplit07) {
		this.unitSplit07 = unitSplit07;
	}
	
	public BigDecimal getPremcurr(int indx) {

		switch (indx) {
			case 1 : return premCurr01 == null ? BigDecimal.ZERO : premCurr01;
			case 2 : return premCurr02 == null ? BigDecimal.ZERO : premCurr02;
			case 3 : return premCurr03 == null ? BigDecimal.ZERO : premCurr03;
			case 4 : return premCurr04 == null ? BigDecimal.ZERO : premCurr04;
			case 5 : return premCurr05 == null ? BigDecimal.ZERO : premCurr05;
			case 6 : return premCurr06 == null ? BigDecimal.ZERO : premCurr06;
			case 7 : return premCurr07 == null ? BigDecimal.ZERO : premCurr07;
			default: return null; // Throw error instead?
		}
	}
	
	public BigDecimal getPcunit(int indx) {

		switch (indx) {
			case 1 : return pcUnits01 == null ? BigDecimal.ZERO : pcUnits01;
			case 2 : return pcUnits02 == null ? BigDecimal.ZERO : pcUnits02;
			case 3 : return pcUnits03 == null ? BigDecimal.ZERO : pcUnits03;
			case 4 : return pcUnits04 == null ? BigDecimal.ZERO : pcUnits04;
			case 5 : return pcUnits05 == null ? BigDecimal.ZERO : pcUnits05;
			case 6 : return pcUnits06 == null ? BigDecimal.ZERO : pcUnits06;
			case 7 : return pcUnits07 == null ? BigDecimal.ZERO : pcUnits07;
			default: return null; // Throw error instead?
		}
	
	}
	
	public BigDecimal getUsplitpc(int indx) {

		switch (indx) {
			case 1 : return unitSplit01 == null ? BigDecimal.ZERO : unitSplit01;
			case 2 : return unitSplit02 == null ? BigDecimal.ZERO : unitSplit02;
			case 3 : return unitSplit03 == null ? BigDecimal.ZERO : unitSplit03;
			case 4 : return unitSplit04 == null ? BigDecimal.ZERO : unitSplit04;
			case 5 : return unitSplit05 == null ? BigDecimal.ZERO : unitSplit05;
			case 6 : return unitSplit06 == null ? BigDecimal.ZERO : unitSplit06;
			case 7 : return unitSplit07 == null ? BigDecimal.ZERO : unitSplit07;
			default: return null; // Throw error instead?
		}
	
	}
	
	public void setPremcurr(int indx, BigDecimal what) {

		switch (indx) {
			case 1 : setPremCurr01(what);
					 break;
			case 2 : setPremCurr02(what);
					 break;
			case 3 : setPremCurr03(what);
					 break;
			case 4 : setPremCurr04(what);
					 break;
			case 5 : setPremCurr05(what);
			 		 break;
			case 6 : setPremCurr06(what);
					 break;
			case 7 : setPremCurr07(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}
	
	
}