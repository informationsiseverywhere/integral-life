package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.AgentDAO;
import com.dxc.integral.fsu.dao.model.Agent;
import com.dxc.integral.fsu.smarttable.pojo.T3695;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.BatckeyDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.ZorlnkDTO;
import com.dxc.integral.life.dao.AgorpfDAO;
import com.dxc.integral.life.dao.model.Agorpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.Th622;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Zorcompy;

/**
 * 
 * @author xma3
 * 

 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *   Overriding Commission Creation subroutine.
 *
 *   - First, need to check the Sub Account Code set up on T3695,
 *   if it's T3695- ZRORCOMM <> 'Y', then no overriding commission
 *   is required, just exit program.
 *
 *   - Check on AGORPSL, if found valid record, then check on
 *   Policy Year, check on TH622 to calculate Personal Sale  Overri e
 *   Commission.
 *
 *   - Check on AGORRPT to get valid report to agent, check against
 *   the effective date to ensure it is entitle  to have OR
 *   commission  Until End of File.Check on TH622 to calculate
 *   Override Commission.
 *
 *   - Conduct ACMV posting.
 *
 *   Some variables:
 *   For example,agent 60000463's type is AG,then WSAA-BASIC-TYPE = 'AG'
 *   It's OR details are:
 *
 *   Basic Agent Type/Agent/ Report To Type
 *    AG            60000465      UM
 *    UM            60000467      DM
 *
 *   Then :
 *   When treat first record in AGORRPT:
 *        WSAA-WORKING-TYPE ='AG'   WSAA-REPORTTO-TYPE = 'UM'
 *   When treat first record in AGORRPT:
 *        WSAA-WORKING-TYPE ='UM'   WSAA-REPORTTO-TYPE = 'DM'
 *
 ****************************************************************** ****
 *
 */

@Service("zorcompy")
@Lazy
public class ZorcompyImpl implements Zorcompy{
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private AgentDAO agntpfDAO;
	@Autowired
	private AgorpfDAO agorpfDAO;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Lifacmv lifacmv;

	private String wsaaBasicType;
	private BigDecimal wsaaCommAmt;
	private BigDecimal wsaaPercent;
	private int wsaaJrnseq;
	private int wsaaYear;
	private String wsaaReporttoAgnt;
	private String wsaaWorkingType;
	private String wsaaReptoType;
	
	@Override
	public ZorlnkDTO processZorcompy(ZorlnkDTO zorlnkDTO) throws Exception{
		
		zorlnkDTO.setStatuz("****");
		
		 T3695 t3695IO = readT3695(zorlnkDTO.getChdrcoy(), zorlnkDTO.getSacstyp().trim());
		if(t3695IO == null){
			return zorlnkDTO;
		}
		if (!t3695IO.getZrorcomm().equals("Y")) {
			return zorlnkDTO;
		}
		
		Agent agntpf = agntpfDAO.readAgentByCompanyAndAgentNumber(zorlnkDTO.getChdrcoy(), zorlnkDTO.getAgent());		
		if(agntpf != null) {
		wsaaBasicType=agntpf.getAgtype();
		}else{
			return zorlnkDTO;
		}
		
		T5645 t5645IO = readT5645(zorlnkDTO.getChdrcoy());
		if(t5645IO == null){
			return zorlnkDTO;
		}
		
		String  comlvlacc = readT5688(zorlnkDTO.getChdrcoy(), zorlnkDTO.getCnttype(), zorlnkDTO.getEffdate(), zorlnkDTO.getEffdate());

		wsaaCommAmt=BigDecimal.ZERO;
		wsaaPercent=BigDecimal.ZERO;
		wsaaJrnseq=0;
		wsaaYear = checkYear(zorlnkDTO.getEffdate(), zorlnkDTO.getPtdate());
		/* Calculate personal sale first*/
		Agorpf agorpf=agorpfDAO.getAgorpfByCoyAndNumAndtag(zorlnkDTO.getChdrcoy(), zorlnkDTO.getAgent(), zorlnkDTO.getAgent());
		if(agorpf == null) {
			return zorlnkDTO;
		}
		Th622 th622IO = new Th622();
		if (zorlnkDTO.getEffdate()>=agorpf.getEffdate01().intValue()&& zorlnkDTO.getEffdate()<=agorpf.getEffdate02().intValue()) {
			wsaaReporttoAgnt=zorlnkDTO.getAgent();
			wsaaBasicType=agorpf.getAgtype01();
			wsaaWorkingType=agorpf.getAgtype01();
			wsaaReptoType=agorpf.getAgtype01();
			th622IO = readTH622(zorlnkDTO);
			if(th622IO == null) {
				return null;
			}
			orCommCalc(zorlnkDTO, t5645IO, comlvlacc, th622IO);
			
			
		}

		wsaaPercent = BigDecimal.ZERO;
		wsaaCommAmt = BigDecimal.ZERO;
		/* Calculate override commission*/
		List<Agorpf> agorpfList = agorpfDAO.getAgorpfByCoyAndNum(zorlnkDTO.getChdrcoy(), zorlnkDTO.getAgent());
		if(agorpfList != null && agorpfList.size() > 0) {
			for(Agorpf tempAgorpf:agorpfList) {
				beginAgorrpt(tempAgorpf, zorlnkDTO, t5645IO, comlvlacc, th622IO);
			}
		}

		
		return zorlnkDTO;
		
		
	}
	
	protected void beginAgorrpt(Agorpf agor, ZorlnkDTO zorlnkDTO,  T5645 t5645IO, String comlvlacc, Th622 th622IO) throws Exception{

		/* Check whether the "Report To Agent" is valid*/
		if (zorlnkDTO.getEffdate()<agor.getEffdate01().intValue()||zorlnkDTO.getEffdate()>agor.getEffdate02().intValue()) {
			return;
		}
		if (!agor.getAgntnum().trim().equals(agor.getReportag().trim())) {
			wsaaReporttoAgnt = agor.getReportag();
			wsaaWorkingType = agor.getAgtype01();
			if (wsaaBasicType.trim().equals("")) {
				wsaaBasicType=agor.getAgtype01();
			}
			wsaaReptoType=agor.getAgtype02();
			orCommCalc(zorlnkDTO, t5645IO, comlvlacc, th622IO);
			/* To initialise WSAA-YEAR before next OR calculation.*/
			checkYear(zorlnkDTO.getEffdate(), zorlnkDTO.getPtdate());
		}
	}
	
	protected void orCommCalc(ZorlnkDTO zorlnkDTO, T5645 t5645IO, String comlvlacc, Th622 th622IO) throws Exception {
		if (wsaaBasicType.equals(wsaaWorkingType)) {
			/* Direct Agent*/
			wsaaPercent = new BigDecimal(th622IO.getZryrpercs().get(wsaaYear));
			if (th622IO != null && "P".equals(th622IO.getKeyopts().get(0))) {
				wsaaCommAmt = zorlnkDTO.getAnnprem().multiply(wsaaPercent).divide(new BigDecimal(100), 2);
			}
			else {
				wsaaCommAmt = zorlnkDTO.getOrigamt().multiply(wsaaPercent).divide(new BigDecimal(100), 2);
			}
		}
		else {
			/* Indirect Agent*/
			wsaaYear += 10;
			wsaaPercent=new BigDecimal(th622IO.getZryrpercs().get(wsaaYear));
			if (th622IO != null && th622IO.getKeyopts().size() >=2 && "P".equals(th622IO.getKeyopts().get(1))) {
				wsaaCommAmt = zorlnkDTO.getAnnprem().multiply(wsaaPercent).divide(new BigDecimal(100), 2);
			}
			else {
				wsaaCommAmt = zorlnkDTO.getOrigamt().multiply(wsaaPercent).divide(new BigDecimal(100), 2);
			}
		}
		if (wsaaCommAmt.compareTo(BigDecimal.ZERO)!=0) {
			
			callLifeacmv(zorlnkDTO, t5645IO, comlvlacc);
		}
		
	}
	
	protected void callLifeacmv(ZorlnkDTO zorlnkDTO, T5645 t5645IO, String comlvlacc) throws Exception{
		BatckeyDTO batckeyDTO = new BatckeyDTO();
		batckeyDTO.setAllkey(zorlnkDTO.getBatchKey());
		LifacmvDTO lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setJrnseq(0);
		lifacmvDTO.setBatccoy(zorlnkDTO.getChdrcoy());
		lifacmvDTO.setRldgcoy(zorlnkDTO.getChdrcoy());
		lifacmvDTO.setGenlcoy(zorlnkDTO.getChdrcoy());
		lifacmvDTO.setBatcactyr(batckeyDTO.getBatcBatcactyr());
		lifacmvDTO.setBatcactmn(batckeyDTO.getBatcBatcactmn());
		lifacmvDTO.setBatctrcde(batckeyDTO.getBatcBatctrcde());
		lifacmvDTO.setBatcbatch(batckeyDTO.getBatcBatcbatch());
		lifacmvDTO.setBatcbrn(batckeyDTO.getBatcBatcbrn());
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setCrate(BigDecimal.ZERO);
		lifacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifacmvDTO.setUser(0);
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setTrandesc(zorlnkDTO.getTrandesc());
		lifacmvDTO.setTranno(zorlnkDTO.getTranno());
		lifacmvDTO.setEffdate(zorlnkDTO.getEffdate());
		lifacmvDTO.setRdocnum(zorlnkDTO.getChdrnum());
		lifacmvDTO.setCrate(zorlnkDTO.getCrate());
		lifacmvDTO.setGenlcur(zorlnkDTO.getGenlcur());
		lifacmvDTO.setTermid(zorlnkDTO.getTermid());
		lifacmvDTO.setOrigcurr(zorlnkDTO.getOrigcurr());
		/* Write 'LA' 'OC'*/
		if(!zorlnkDTO.equals("Y")) {
			lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(0));
			lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(0));
			lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(0));
			lifacmvDTO.setGlsign(t5645IO.getSigns().get(0));
			lifacmvDTO.setContot(t5645IO.getCnttots().get(0));
		}else {
			lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(3));
			lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(3));
			lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(3));
			lifacmvDTO.setGlsign(t5645IO.getSigns().get(3));
			lifacmvDTO.setContot(t5645IO.getCnttots().get(3));
		}
		
		lifacmvDTO.setRldgacct(wsaaReporttoAgnt);
		lifacmvDTO.setOrigamt(wsaaCommAmt);
		lifacmvDTO.setAcctamt(wsaaCommAmt);
		lifacmvDTO.setTranref(zorlnkDTO.getTranref());//IJTI-1415
		wsaaJrnseq++;
		lifacmvDTO.setJrnseq(wsaaJrnseq);
		lifacmvDTO.setSuprflag("Y");
		List<String> list = new ArrayList<String>();
		list.add(zorlnkDTO.getCnttype());
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add(zorlnkDTO.getCrtable());
		
		lifacmvDTO.setSubstituteCodes(list);
		lifacmv.executePSTW(lifacmvDTO);

		/* Write 'LE' 'OE'*/
		if ("Y".equals(comlvlacc)) {
			if (!"Y".equals(zorlnkDTO.getClawback())) {
				lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(1));
				lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(1));
				lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(1));
				lifacmvDTO.setGlsign(t5645IO.getSigns().get(1));
				lifacmvDTO.setContot(t5645IO.getCnttots().get(1));
			}
			else {
				lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(4));
				lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(4));
				lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(4));
				lifacmvDTO.setGlsign(t5645IO.getSigns().get(4));
				lifacmvDTO.setContot(t5645IO.getCnttots().get(4));
			}
		}
		else {
			if (!"Y".equals(zorlnkDTO.getClawback())) {
				lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(2));
				lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(2));
				lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(2));
				lifacmvDTO.setGlsign(t5645IO.getSigns().get(2));
				lifacmvDTO.setContot(t5645IO.getCnttots().get(2));
			}
			else {
				lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(5));
				lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(5));
				lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(5));
				lifacmvDTO.setGlsign(t5645IO.getSigns().get(5));
				lifacmvDTO.setContot(t5645IO.getCnttots().get(5));
			}
		}
		list = new ArrayList<String>();
		list.add(zorlnkDTO.getCnttype());
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add(zorlnkDTO.getCrtable());
		lifacmvDTO.setSubstituteCodes(list);
		lifacmvDTO.setRldgacct(wsaaReporttoAgnt);
		lifacmvDTO.setOrigamt(wsaaCommAmt);
		lifacmvDTO.setAcctamt(wsaaCommAmt);
		lifacmvDTO.setTranref(zorlnkDTO.getTranref());
		wsaaJrnseq++;
		lifacmvDTO.setJrnseq(wsaaJrnseq);
		lifacmv.executePSTW(lifacmvDTO);
	}

	
	protected T3695 readT3695(String coy, String sacstyp) throws IOException {
		T3695 t3695IO = null;
		TableItem<T3695> t3695 = itempfService.readSmartTableByTableNameAndItem(coy, "T3695", sacstyp, CommonConstants.IT_ITEMPFX, T3695.class);
		if(t3695 != null){
			t3695IO = t3695.getItemDetail();
		}
		return t3695IO;
	}
	
	protected T5645 readT5645(String coy) throws IOException {
		T5645 t5645IO = null;
		TableItem<T5645> t5645 = itempfService.readSmartTableByTableNameAndItem(coy, "T5645", "ZORCOMPY", CommonConstants.IT_ITEMPFX, T5645.class);
		if(t5645 != null){
			t5645IO = t5645.getItemDetail();
		}
		return t5645IO;
	}
	
	protected String readT5688(String coy, String itemitem, int itmfrm, int itmto) throws IOException {
		TableItem<T5688> t5688 = itempfService.readItemByItemFrmTo(coy, "T5688", itemitem, CommonConstants.IT_ITEMPFX, itmfrm, itmto, T5688.class);
		if(t5688 != null) {
			T5688 t5688IO = t5688.getItemDetail();
			return t5688IO.getComlvlacc();
			}
		return "";
	}
	
	protected Th622 readTH622(ZorlnkDTO zorlnkDTO) throws IOException {
		Th622 th622IO = null;
		TableItem<Th622> th622 = itempfService.readItemByItemFrmTo(zorlnkDTO.getChdrcoy(), "TH622", zorlnkDTO.getCrtable().concat(wsaaWorkingType).concat(wsaaReptoType),
				CommonConstants.IT_ITEMPFX, zorlnkDTO.getEffdate(), zorlnkDTO.getEffdate(), Th622.class);
		if(th622 != null) {
			th622IO = th622.getItemDetail();
		}else {
			th622 = itempfService.readItemByItemFrmTo(zorlnkDTO.getChdrcoy(), "TH622", "****".concat(wsaaWorkingType).concat(wsaaReptoType), 
					CommonConstants.IT_ITEMPFX, zorlnkDTO.getEffdate(), zorlnkDTO.getEffdate(), Th622.class);
			if(th622 != null) {
			th622IO = th622.getItemDetail();	
			}
		}
		return th622IO;	
		
	}

	protected int checkYear(int effdate, int ptdate) throws Exception{
		int freqFactor = datcon3.getYearsDifference(effdate, ptdate).intValue();
		if (freqFactor<=1){
			wsaaYear=1;
		}
		else if (freqFactor<=2){
			wsaaYear=2;
		}
		else if (freqFactor<=3){
			wsaaYear=3;
		}
		else if (freqFactor<=4){
			wsaaYear=4;
		}
		else if (freqFactor<=5){
			wsaaYear=5;
		}
		else if (freqFactor<=6){
			wsaaYear=6;
		}
		else if (freqFactor<=7){
			wsaaYear=7;
		}
		else if (freqFactor<=8){
			wsaaYear=8;
		}
		else if (freqFactor<=9){
			wsaaYear=9;
		}
		else if (freqFactor>9){
			wsaaYear=10;
		}
		
		return wsaaYear;
	}
	
}
