package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Lifepf;
/**
 * @author wli31
 */

public interface LifepfDAO {
	public List<Lifepf> getLifeRecord(String chdrcoy, String chdrnum, String life, String jlife);
	public Lifepf getValidLifeRecord(String chdrcoy, String chdrnum, String life, String jlife);
	public Lifepf getLifeRecordByCurrfrom(String chdrcoy, String chdrnum, String life, String jlife,int currfrom);
	public List<Lifepf> searchLifeRecordByChdrNum(String chdrcoy, String chdrnum);
	public Lifepf getLifecmcRecord(String chdrcoy, String chdrnum, String life, String jlife);
	public Lifepf getRcntChngRecord(String chdrcoy, String chdrnum, String life, String jlife);
}