package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.TrmreasDTO;

public interface Trmreas {
	public TrmreasDTO processTrmreas(TrmreasDTO trmreasDTO);
}
