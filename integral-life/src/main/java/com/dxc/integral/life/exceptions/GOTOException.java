package com.dxc.integral.life.exceptions;


/**
 * An exception only for implementing GOTO operation. It holds the next label. This exception must be handled
 * somewhere.
 * 
 * @author max.wang
 */
public class GOTOException extends RuntimeException  {

	/**
	 * Auto-generated serial version UID
	 */
	private static final long serialVersionUID = -3566924971744053962L;
	
	/**
	 * Target label of GOTO statement
	 */
	private GOTOInterface label;

	/**
	 * Constructor with target label
	 * 
	 * @param aLabel - target label
	 */
	public GOTOException(GOTOInterface aLabel) {
		label = aLabel;
	}

	/**
	 * Getter of target label
	 * 
	 * @return - target label without type casted
	 */
	public GOTOInterface getNextMethod() {
		return label;
	}
	
	/**
	 * Setter of next methods
	 * @param aNextMethod
	 */
	public void setNextMethod(GOTOInterface aNextMethod) {
		label = aNextMethod;
	}
}
