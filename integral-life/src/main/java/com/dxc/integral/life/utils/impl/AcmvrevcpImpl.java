package com.dxc.integral.life.utils.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.beans.ColdrlaDTO;
import com.dxc.integral.life.dao.model.AcmvrevcpPojo;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.utils.Acmvrevcp;

@Service("acmvrevcp")
@Lazy
public class AcmvrevcpImpl implements Acmvrevcp {
	public ColdrlaDTO coldrlaDTO;
	public String sysStatuz = "";
	
	public AcmvrevcpPojo processAcmvrevcp(AcmvrevcpPojo acmvrevcpPojo) {
		acmvrevcpPojo.setStatuz("*****");
		if(acmvrevcpPojo.getFunction().equals("CLOSE")) {
			return acmvrevcpPojo;
		}
		if(acmvrevcpPojo.getFunction().trim().equals("BEGN")) {
			getOptical(acmvrevcpPojo);
			coldrlaDTO.setOperationCode("CHAIN");
			processAcmvOp();
		}
		if ("NEXTR".equals(acmvrevcpPojo.getFunction())) {
			coldrlaDTO.setOperationCode("READE");
			processAcmvOp();
		}
		acmvrevcpPojo = closeAcmvrevcp(acmvrevcpPojo);
		return acmvrevcpPojo;
	}
	public AcmvrevcpPojo closeAcmvrevcp(AcmvrevcpPojo acmvrevcpPojo) {
		if ( "CLOSE".equals(acmvrevcpPojo.getFunction())) {
					/* Set the Last Record indicator to '1' to force the end of file.*/
					/* Without this the CLOSE will work OK but the next time you attemp*/
					/* a call to COLDRLA it will fail.*/
			        coldrlaDTO.setLrIndicator("1");
			        coldrlaDTO.setOperationCode("CLOSE");
					coldrlaCall();
					removeLibl();
					acmvrevcpPojo.setStatuz("ENDP");
				}
				/*EXIT*/
			return acmvrevcpPojo;
		
	}
	
	protected void getOptical(AcmvrevcpPojo acmvrevcpPojo) {
		StringBuilder sb = new StringBuilder("ACMV");
		sb.append(acmvrevcpPojo.getBatcactyr()).append(acmvrevcpPojo.getBatcactmn());
		
		coldrlaDTO.setMemberName(sb.toString());
		coldrlaDTO.setFileName(coldrlaDTO.getMemberName());
		
		throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
		
	}
	
	protected void processAcmvOp() {
		throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
	
	}
	
	protected void coldrlaCall() {
		throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
	}
	
	protected void removeLibl()
	{
        throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
		/*EXIT*/
	}
	public void fatalError(String status, AcmvrevcpPojo acmvrevcpPojo) {
		
		if ("BOMB".equals(sysStatuz)) {
			return ;
		}
		acmvrevcpPojo.setStatuz("BOMB");
		throw new StopRunException(status);
	}

}
