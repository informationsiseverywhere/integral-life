package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Linkagepf;

public interface LinkageInfoDAO {

	public Linkagepf fetchLinkageInfo(String linkageNum);
}
