package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * @author fwang3
 *
 */
public class RexpupdDTO {

	private String function;
	private String statuz;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String clntcoy;
	private String l1Clntnum;
	private String l2Clntnum;
	private String riskClass;
	private String reassurer;
	private String arrangement;
	private String cestyp;
	private String currency;
	private BigDecimal sumins;
	private int effdate;
	private String batctrcde;

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getStatuz() {
		return statuz;
	}

	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getClntcoy() {
		return clntcoy;
	}

	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}

	public String getL1Clntnum() {
		return l1Clntnum;
	}

	public void setL1Clntnum(String l1Clntnum) {
		this.l1Clntnum = l1Clntnum;
	}

	public String getL2Clntnum() {
		return l2Clntnum;
	}

	public void setL2Clntnum(String l2Clntnum) {
		this.l2Clntnum = l2Clntnum;
	}

	public String getRiskClass() {
		return riskClass;
	}

	public void setRiskClass(String riskClass) {
		this.riskClass = riskClass;
	}

	public String getReassurer() {
		return reassurer;
	}

	public void setReassurer(String reassurer) {
		this.reassurer = reassurer;
	}

	public String getArrangement() {
		return arrangement;
	}

	public void setArrangement(String arrangement) {
		this.arrangement = arrangement;
	}

	public String getCestyp() {
		return cestyp;
	}

	public void setCestyp(String cestyp) {
		this.cestyp = cestyp;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getSumins() {
		return sumins;
	}

	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

}