package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;
import com.dxc.integral.life.beans.NfloanDTO;


/**
 * NFLoan utility.
 * 
 * @author dpuhawan
 *
 */
@FunctionalInterface
public interface Nfloan {

	public NfloanDTO processNFLoan(NfloanDTO nfloadDTO) throws IOException, ParseException ;//ILIFE-5763
}
