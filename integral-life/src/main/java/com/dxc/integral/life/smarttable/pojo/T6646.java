package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T6646 {

	private List<Integer> dfacts;
	private List<Integer> facts;
	
	public List<Integer> getDfacts() {
		return dfacts;
	}
	public void setDfacts(List<Integer> dfacts) {
		this.dfacts = dfacts;
	}
	public List<Integer> getFacts() {
		return facts;
	}
	public void setFacts(List<Integer> facts) {
		this.facts = facts;
	}
	
	
}
