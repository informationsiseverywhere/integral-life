/* ********************  */
/*Author  :tsaxena3				  		*/
package com.dxc.integral.life.smarttable.pojo;

public class Extprm {
	
	private Integer freqFactor;
	private Integer mortRate;
	private Integer premium;
	private Integer sumass;
	private Integer exFactor;
	private Integer percent;
	private Integer term;
	private Integer freq;
	private Integer loading;
	private String statuz;
	
	public Integer getFreqfactor() {
		return freqFactor;
	}
	public void setFreqfactor(Integer freqFactor) {
		this.freqFactor = freqFactor;
	}
	
	public Integer getMortrate() {
		return mortRate;
	}
	public void setMortRate(Integer mortRate) {
		this.mortRate = mortRate;
	}
	
	public Integer getpremium() {
		return premium;
	}
	public void setPremium(Integer premium) {
		this.premium = premium;
	}
	
	public Integer getSumass() {
		return sumass;
	}
	public void setSumass(Integer sumass) {
		this.sumass = sumass;
	}
	
	public Integer getExfactor() {
		return exFactor;
	}
	public void setExfactor(Integer exFactor) {
		this.exFactor = exFactor;
	}
	
	public Integer getPercent() {
		return percent;
	}
	public void setPercent(Integer percent) {
		this.percent = percent;
	}
	
	public Integer getTerm() {
		return term;
	}
	public void setTerm(Integer term) {
		this.term = term;
	}
	
	public Integer getFreq() {
		return freq;
	}
	public void setFreq(Integer freq) {
		this.freq = freq;
	}
	
	public Integer getLoading() {
		return loading;
	}
	public void setLoading(Integer loading) {
		this.loading = loading;
	}
	
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}

}
