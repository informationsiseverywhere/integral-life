package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class T5687 {

  	private String anniversaryMethod;
  	private String basicCommMeth;
  	private String bastCpy;
  	private String basscMth;
  	private String bassCpy;
  	private String bastcMth;
  	private String basCpy;
  	private String bbMeth;
  	private String dcMeth;	
  	private String defFupMeth;
  	private String jlPremMeth;
  	private String jlifePresent;
  	private String loanMeth;
  	private String maturityCalcMeth;
  	private String nonForfeitMethod;
  	private String partSurr;
  	private Integer premGuarPeriod;
  	private String premMeth;
  	private String puMeth;
  	private List<String> reptcds;
  	private String reptcd01;
  	private String reptcd02;
  	private String reptcd03;
  	private String reptcd04;
  	private String reptcd05;
  	private String reptcd06;
  	private String riInd;
  	private String rnwcpy;
  	private Integer rtrnwFreq;
  	private String schedCode;
  	private String sdtyPayMeth;
  	private String singlePremInd;
  	private String srvCpy;
  	private String statFund;
  	private String stampDutyMeth;
  	private String statSect;
  	private String statSubSect;
  	private String svMethod;
  	private String xfreqAltBasis;
  	private String zrorCmrg;
  	private String zrorCmsp;
  	private String zrorCmtu;
  	private String zrorPmrg;
  	private String zrorPmsp;
  	private String zrorPmtu;
  	private String zrrCombas;
  	private String zsbsMeth;
  	private String zsredTrm;
  	private String zszprMcd;
  	private String lnkgInd;
	
		
  	
	public String getAnniversaryMethod() {
		return anniversaryMethod;
	}
	public void setAnniversaryMethod(String anniversaryMethod) {
		this.anniversaryMethod = anniversaryMethod;
	}
	public String getBasicCommMeth() {
		return basicCommMeth;
	}
	public void setBasicCommMeth(String basicCommMeth) {
		this.basicCommMeth = basicCommMeth;
	}
	public String getBascpy() {
		return basCpy;
	}
	public void setBascpy(String bascpy) {
		this.basCpy = bascpy;
	}
	public String getBasscmth() {
		return basscMth;
	}
	public void setBasscmth(String basscmth) {
		this.basscMth = basscmth;
	}
	public String getBasscpy() {
		return bassCpy;
	}
	public void setBasscpy(String basscpy) {
		this.bassCpy = basscpy;
	}
	public String getBastcmth() {
		return bastcMth;
	}
	public void setBastcmth(String bastcmth) {
		this.bastcMth = bastcmth;
	}
	public String getBastcpy() {
		return bastCpy;
	}
	public void setBastcpy(String bastcpy) {
		this.bastCpy = bastcpy;
	}
	public String getBbmeth() {
		return bbMeth;
	}
	public void setBbmeth(String bbmeth) {
		this.bbMeth = bbmeth;
	}
	public String getDcmeth() {
		return dcMeth;
	}
	public void setDcmeth(String dcmeth) {
		this.dcMeth = dcmeth;
	}
	public String getDefFupMeth() {
		return defFupMeth;
	}
	public void setDefFupMeth(String defFupMeth) {
		this.defFupMeth = defFupMeth;
	}
	public String getJlPremMeth() {
		return jlPremMeth;
	}
	public void setJlPremMeth(String jlPremMeth) {
		this.jlPremMeth = jlPremMeth;
	}
	public String getJlifePresent() {
		return jlifePresent;
	}
	public void setJlifePresent(String jlifePresent) {
		this.jlifePresent = jlifePresent;
	}
	public String getLoanmeth() {
		return loanMeth;
	}
	public void setLoanmeth(String loanmeth) {
		this.loanMeth = loanmeth;
	}
	public String getMaturityCalcMeth() {
		return maturityCalcMeth;
	}
	public void setMaturityCalcMeth(String maturityCalcMeth) {
		this.maturityCalcMeth = maturityCalcMeth;
	}
	public String getNonForfeitMethod() {
		return nonForfeitMethod;
	}
	public void setNonForfeitMethod(String nonForfeitMethod) {
		this.nonForfeitMethod = nonForfeitMethod;
	}
	public String getPartsurr() {
		return partSurr;
	}
	public void setPartsurr(String partsurr) {
		this.partSurr = partsurr;
	}
	public Integer getPremGuarPeriod() {
		return premGuarPeriod;
	}
	public void setPremGuarPeriod(Integer premGuarPeriod) {
		this.premGuarPeriod = premGuarPeriod;
	}
	public String getPremmeth() {
		return premMeth;
	}
	public void setPremmeth(String premmeth) {
		this.premMeth = premmeth;
	}
	public String getPumeth() {
		return puMeth;
	}
	public void setPumeth(String pumeth) {
		this.puMeth = pumeth;
	}
	public List<String> getReptcds() {
		return reptcds;
	}
	public void setReptcds(List<String> reptcds) {
		this.reptcds = reptcds;
	}
	public String getReptcd01() {
		return reptcd01;
	}
	public void setReptcd01(String reptcd01) {
		this.reptcd01 = reptcd01;
	}
	public String getReptcd02() {
		return reptcd02;
	}
	public void setReptcd02(String reptcd02) {
		this.reptcd02 = reptcd02;
	}
	public String getReptcd03() {
		return reptcd03;
	}
	public void setReptcd03(String reptcd03) {
		this.reptcd03 = reptcd03;
	}
	public String getReptcd04() {
		return reptcd04;
	}
	public void setReptcd04(String reptcd04) {
		this.reptcd04 = reptcd04;
	}
	public String getReptcd05() {
		return reptcd05;
	}
	public void setReptcd05(String reptcd05) {
		this.reptcd05 = reptcd05;
	}
	public String getReptcd06() {
		return reptcd06;
	}
	public void setReptcd06(String reptcd06) {
		this.reptcd06 = reptcd06;
	}
	public String getRiind() {
		return riInd;
	}
	public void setRiind(String riind) {
		this.riInd = riind;
	}
	public String getRnwcpy() {
		return rnwcpy;
	}
	public void setRnwcpy(String rnwcpy) {
		this.rnwcpy = rnwcpy;
	}
	public Integer getRtrnwfreq() {
		return rtrnwFreq;
	}
	public void setRtrnwfreq(Integer rtrnwfreq) {
		this.rtrnwFreq = rtrnwfreq;
	}
	public String getSchedCode() {
		return schedCode;
	}
	public void setSchedCode(String schedCode) {
		this.schedCode = schedCode;
	}
	public String getSdtyPayMeth() {
		return sdtyPayMeth;
	}
	public void setSdtyPayMeth(String sdtyPayMeth) {
		this.sdtyPayMeth = sdtyPayMeth;
	}
	public String getSinglePremInd() {
		return singlePremInd;
	}
	public void setSinglePremInd(String singlePremInd) {
		this.singlePremInd = singlePremInd;
	}
	public String getSrvcpy() {
		return srvCpy;
	}
	public void setSrvcpy(String srvcpy) {
		this.srvCpy = srvcpy;
	}
	public String getStatFund() {
		return statFund;
	}
	public void setStatFund(String statFund) {
		this.statFund = statFund;
	}
	public String getStampDutyMeth() {
		return stampDutyMeth;
	}
	public void setStampDutyMeth(String stampDutyMeth) {
		this.stampDutyMeth = stampDutyMeth;
	}
	public String getStatSect() {
		return statSect;
	}
	public void setStatSect(String statSect) {
		this.statSect = statSect;
	}
	public String getStatSubSect() {
		return statSubSect;
	}
	public void setStatSubSect(String statSubSect) {
		this.statSubSect = statSubSect;
	}
	public String getSvMethod() {
		return svMethod;
	}
	public void setSvMethod(String svMethod) {
		this.svMethod = svMethod;
	}
	public String getXfreqAltBasis() {
		return xfreqAltBasis;
	}
	public void setXfreqAltBasis(String xfreqAltBasis) {
		this.xfreqAltBasis = xfreqAltBasis;
	}
	public String getZrorcmrg() {
		return zrorCmrg;
	}
	public void setZrorcmrg(String zrorcmrg) {
		this.zrorCmrg = zrorcmrg;
	}
	public String getZrorcmsp() {
		return zrorCmsp;
	}
	public void setZrorcmsp(String zrorcmsp) {
		this.zrorCmsp = zrorcmsp;
	}
	public String getZrorcmtu() {
		return zrorCmtu;
	}
	public void setZrorcmtu(String zrorcmtu) {
		this.zrorCmtu = zrorcmtu;
	}
	public String getZrorpmrg() {
		return zrorPmrg;
	}
	public void setZrorpmrg(String zrorpmrg) {
		this.zrorPmrg = zrorpmrg;
	}
	public String getZrorpmsp() {
		return zrorPmsp;
	}
	public void setZrorpmsp(String zrorpmsp) {
		this.zrorPmsp = zrorpmsp;
	}
	public String getZrorpmtu() {
		return zrorPmtu;
	}
	public void setZrorpmtu(String zrorpmtu) {
		this.zrorPmtu = zrorpmtu;
	}
	public String getZrrcombas() {
		return zrrCombas;
	}
	public void setZrrcombas(String zrrcombas) {
		this.zrrCombas = zrrcombas;
	}
	public String getZsbsmeth() {
		return zsbsMeth;
	}
	public void setZsbsmeth(String zsbsmeth) {
		this.zsbsMeth = zsbsmeth;
	}
	public String getZsredtrm() {
		return zsredTrm;
	}
	public void setZsredtrm(String zsredtrm) {
		this.zsredTrm = zsredtrm;
	}
	public String getZszprmcd() {
		return zszprMcd;
	}
	public void setZszprmcd(String zszprmcd) {
		this.zszprMcd = zszprmcd;
	}
	public String getLnkgind() {
		return lnkgInd;
	}
	public void setLnkgind(String lnkgind) {
		this.lnkgInd = lnkgind;
	}
 
}
