package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.LifsttrDTO;

public interface Lifsttr {
	public LifsttrDTO processLifsttr(LifsttrDTO lifsttrDTO)throws IOException;
}
