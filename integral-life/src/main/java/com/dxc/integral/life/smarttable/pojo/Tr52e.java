package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * @author wli31
 */

@JsonIgnoreProperties(ignoreUnknown = true) 
public class Tr52e {

  	private List<String> taxinds;
  	private String txitem;
  	private String zbastyp;
	public List<String> getTaxinds() {
		return taxinds;
	}
	public void setTaxinds(List<String> taxinds) {
		this.taxinds = taxinds;
	}
	public String getTxitem() {
		return txitem;
	}
	public void setTxitem(String txitem) {
		this.txitem = txitem;
	}
	public String getZbastyp() {
		return zbastyp;
	}
	public void setZbastyp(String zbastyp) {
		this.zbastyp = zbastyp;
	}
}