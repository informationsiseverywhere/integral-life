package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Zctnpf {
	private long uniqueNumber;
    private String agntcoy;
    private String agntnum;
    private int effdate;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int tranno;
    private String transCode;
    private BigDecimal commAmt = BigDecimal.ZERO;
    private BigDecimal premium = BigDecimal.ZERO;
    private BigDecimal splitBcomm = BigDecimal.ZERO;
    private String zprflg;
    private int trandate;
    private String userProfile;
    private String jobName;
    private String datime;

    public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getAgntcoy() {
        return agntcoy;
    }

    public String getAgntnum() {
        return agntnum;
    }

    public int getEffdate() {
        return effdate;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public String getLife() {
        return life;
    }

    public String getCoverage() {
        return coverage;
    }

    public String getRider() {
        return rider;
    }

    public int getTranno() {
        return tranno;
    }

    public String getTransCode() {
        return transCode;
    }

    public BigDecimal getCommAmt() {
        return commAmt;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public BigDecimal getSplitBcomm() {
        return splitBcomm;
    }

    public String getZprflg() {
        return zprflg;
    }

    public int getTrandate() {
        return trandate;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setAgntcoy(String agntcoy) {
        this.agntcoy = agntcoy;
    }

    public void setAgntnum(String agntnum) {
        this.agntnum = agntnum;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public void setCommAmt(BigDecimal commAmt) {
        this.commAmt = commAmt;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public void setSplitBcomm(BigDecimal splitBcomm) {
        this.splitBcomm = splitBcomm;
    }

    public void setZprflg(String zprflg) {
        this.zprflg = zprflg;
    }

    public void setTrandate(int trandate) {
        this.trandate = trandate;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }
}