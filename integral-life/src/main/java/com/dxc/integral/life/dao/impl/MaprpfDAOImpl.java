package com.dxc.integral.life.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.MaprpfDAO;
import com.dxc.integral.life.dao.model.Maprpf;

/**
 * MaprpfDAO implementation for database table <b>MAPRPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@Repository("maprpfDAO")
public class MaprpfDAOImpl extends BaseDAOImpl implements MaprpfDAO {
	
	private static final String CONSTANT_PLACEHOLDER_20 = "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,";

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.MaprpfDAO#readMaprpfByAgentNumberCompanyAndEffDate(java.lang.String, java.lang.String, int)
	 */
	@Override
	public Maprpf readMaprpfByAgentNumberCompanyAndEffDate(String company, String agentNumber, int effectiveDate) {
		StringBuilder sql = new StringBuilder("select * from MAPRPF where ");
		sql.append("AGNTCOY=? and AGNTNUM=? and EFFDATE=? ");
		sql.append(
				"order by AGNTCOY ASC, AGNTNUM ASC, EFFDATE ASC, ACCTYR ASC, MNTH ASC, CNTTYPE ASC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] { company, agentNumber, effectiveDate },
				new BeanPropertyRowMapper<Maprpf>(Maprpf.class));
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.MaprpfDAO#updateMaprpfByAgentNumberCompanyAndEffDate(java.lang.String, java.lang.String, int, com.dxc.integral.life.dao.model.Maprpf)
	 */
	@Override
	public int updateMaprpfByAgentNumberCompanyAndEffDate(String company, String agentNumber, int effectiveDate,
			Maprpf maprpf) {
		StringBuilder sql = new StringBuilder("update MAPRPF set ");
		sql.append(
				"MLPERPP01=?, MLPERPP02=?, MLPERPP03=?, MLPERPP04=?, MLPERPP05=?, MLPERPP06=?, MLPERPP07=?, MLPERPP08=?, MLPERPP09=?, MLPERPP10=?, ");
		sql.append(
				"MLPERPC01=?, MLPERPC02=?, MLPERPC03=?, MLPERPC04=?, MLPERPC05=?, MLPERPC06=?, MLPERPC07=?, MLPERPC08=?, MLPERPC09=?, MLPERPC10=?, ");
		sql.append(
				"MLDIRPP01=?, MLDIRPP02=?, MLDIRPP03=?, MLDIRPP04=?, MLDIRPP05=?, MLDIRPP06=?, MLDIRPP07=?, MLDIRPP08=?, MLDIRPP09=?, MLDIRPP10=?, ");
		sql.append(
				"MLDIRPC01=?, MLDIRPC02=?, MLDIRPC03=?, MLDIRPC04=?, MLDIRPC05=?, MLDIRPC06=?, MLDIRPC07=?, MLDIRPC08=?, MLDIRPC09=?, MLDIRPC10=?, ");
		sql.append(
				"MLGRPPP01=?, MLGRPPP02=?, MLGRPPP03=?, MLGRPPP04=?, MLGRPPP05=?, MLGRPPP06=?, MLGRPPP07=?, MLGRPPP08=?, MLGRPPP09=?, MLGRPPP10=?, ");
		sql.append(
				"MLGRPPC01=?, MLGRPPC02=?, MLGRPPC03=?, MLGRPPC04=?, MLGRPPC05=?, MLGRPPC06=?, MLGRPPC07=?, MLGRPPC08=?, MLGRPPC09=?, MLGRPPC10=?, ");
		sql.append("CNTCOUNT=?, SUMINS=?, USRPRF=?, JOBNM=?, DATIME=? ");
		sql.append("where AGNTCOY=? and AGNTNUM=? and EFFDATE=? ");
		return jdbcTemplate.update(sql.toString(),
				new Object[] { 
						maprpf.getMlperpp().get(0), maprpf.getMlperpp().get(1), maprpf.getMlperpp().get(2), maprpf.getMlperpp().get(3), maprpf.getMlperpp().get(4),
						maprpf.getMlperpp().get(5), maprpf.getMlperpp().get(6), maprpf.getMlperpp().get(7), maprpf.getMlperpp().get(8), maprpf.getMlperpp().get(9),
						maprpf.getMlperpc().get(0), maprpf.getMlperpc().get(1), maprpf.getMlperpc().get(2), maprpf.getMlperpc().get(3), maprpf.getMlperpc().get(4),
						maprpf.getMlperpc().get(5), maprpf.getMlperpc().get(6), maprpf.getMlperpc().get(7), maprpf.getMlperpc().get(8), maprpf.getMlperpc().get(9),
						maprpf.getMldirpp().get(0), maprpf.getMldirpp().get(1), maprpf.getMldirpp().get(2), maprpf.getMldirpp().get(3), maprpf.getMldirpp().get(4),
						maprpf.getMldirpp().get(5), maprpf.getMldirpp().get(6), maprpf.getMldirpp().get(7), maprpf.getMldirpp().get(8), maprpf.getMldirpp().get(9),
						maprpf.getMldirpc().get(0), maprpf.getMldirpc().get(1), maprpf.getMldirpc().get(2), maprpf.getMldirpc().get(3), maprpf.getMldirpc().get(4),
						maprpf.getMldirpc().get(5), maprpf.getMldirpc().get(6), maprpf.getMldirpc().get(7), maprpf.getMldirpc().get(8), maprpf.getMldirpc().get(9),
						maprpf.getMlgrppp().get(0), maprpf.getMlgrppp().get(1), maprpf.getMlgrppp().get(2), maprpf.getMlgrppp().get(3), maprpf.getMlgrppp().get(4),
						maprpf.getMlgrppp().get(5), maprpf.getMlgrppp().get(6), maprpf.getMlgrppp().get(7), maprpf.getMlgrppp().get(8), maprpf.getMlgrppp().get(9),
						maprpf.getMlgrppc().get(0), maprpf.getMlgrppc().get(1), maprpf.getMlgrppc().get(2), maprpf.getMlgrppc().get(3), maprpf.getMlgrppc().get(4),
						maprpf.getMlgrppc().get(5), maprpf.getMlgrppc().get(6), maprpf.getMlgrppc().get(7), maprpf.getMlgrppc().get(8), maprpf.getMlgrppc().get(9),
						maprpf.getCntcount(), maprpf.getSumins(), maprpf.getUsrprf(), maprpf.getJobnm(), maprpf.getDatime(), maprpf.getAgntcoy(), maprpf.getAgntnum(),
						maprpf.getEffdate()});
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.MaprpfDAO#insertMaprpfRecords(com.dxc.integral.life.dao.model.Maprpf)
	 */
	@Override
	public int insertMaprpfRecords(Maprpf maprpf) {

		StringBuilder sql = new StringBuilder("insert into MAPRPF (");
		sql.append("AGNTCOY, AGNTNUM, ACCTYR, MNTH, CNTTYPE, EFFDATE, ");
		sql.append(
				"MLPERPP01, MLPERPP02, MLPERPP03, MLPERPP04, MLPERPP05, MLPERPP06, MLPERPP07, MLPERPP08, MLPERPP09, MLPERPP10, ");
		sql.append(
				"MLPERPC01, MLPERPC02, MLPERPC03, MLPERPC04, MLPERPC05, MLPERPC06, MLPERPC07, MLPERPC08, MLPERPC09, MLPERPC10, ");
		sql.append(
				"MLDIRPP01, MLDIRPP02, MLDIRPP03, MLDIRPP04, MLDIRPP05, MLDIRPP06, MLDIRPP07, MLDIRPP08, MLDIRPP09, MLDIRPP10,");
		sql.append(
				"MLDIRPC01, MLDIRPC02, MLDIRPC03, MLDIRPC04, MLDIRPC05, MLDIRPC06, MLDIRPC07, MLDIRPC08, MLDIRPC09, MLDIRPC10,");
		sql.append(
				"MLGRPPP01, MLGRPPP02, MLGRPPP03, MLGRPPP04, MLGRPPP05, MLGRPPP06, MLGRPPP07, MLGRPPP08, MLGRPPP09, MLGRPPP10,");
		sql.append(
				"MLGRPPC01, MLGRPPC02, MLGRPPC03, MLGRPPC04, MLGRPPC05, MLGRPPC06, MLGRPPC07, MLGRPPC08, MLGRPPC09, MLGRPPC10,");
		sql.append("CNTCOUNT, SUMINS, USRPRF, JOBNM, DATIME) VALUES (");
		sql.append("?, ?, ?, ?, ?, ?,");
		sql.append(CONSTANT_PLACEHOLDER_20);
		sql.append(CONSTANT_PLACEHOLDER_20);
		sql.append(CONSTANT_PLACEHOLDER_20);
		sql.append("?, ?, ?, ?, ?)");

		return jdbcTemplate.update(sql.toString(),
				new Object[] { maprpf.getAgntcoy(), maprpf.getAgntnum(), maprpf.getAcctyr(), maprpf.getMnth(),
						maprpf.getCnttype(), maprpf.getEffdate(), 
						maprpf.getMlperpp().get(0), maprpf.getMlperpp().get(1), maprpf.getMlperpp().get(2), maprpf.getMlperpp().get(3), maprpf.getMlperpp().get(4),
						maprpf.getMlperpp().get(5), maprpf.getMlperpp().get(6), maprpf.getMlperpp().get(7), maprpf.getMlperpp().get(8), maprpf.getMlperpp().get(9),
						maprpf.getMlperpc().get(0), maprpf.getMlperpc().get(1), maprpf.getMlperpc().get(2), maprpf.getMlperpc().get(3), maprpf.getMlperpc().get(4),
						maprpf.getMlperpc().get(5), maprpf.getMlperpc().get(6), maprpf.getMlperpc().get(7), maprpf.getMlperpc().get(8), maprpf.getMlperpc().get(9),
						maprpf.getMldirpp().get(0), maprpf.getMldirpp().get(1), maprpf.getMldirpp().get(2), maprpf.getMldirpp().get(3), maprpf.getMldirpp().get(4),
						maprpf.getMldirpp().get(5), maprpf.getMldirpp().get(6), maprpf.getMldirpp().get(7), maprpf.getMldirpp().get(8), maprpf.getMldirpp().get(9),
						maprpf.getMldirpc().get(0), maprpf.getMldirpc().get(1), maprpf.getMldirpc().get(2), maprpf.getMldirpc().get(3), maprpf.getMldirpc().get(4),
						maprpf.getMldirpc().get(5), maprpf.getMldirpc().get(6), maprpf.getMldirpc().get(7), maprpf.getMldirpc().get(8), maprpf.getMldirpc().get(9),
						maprpf.getMlgrppp().get(0), maprpf.getMlgrppp().get(1), maprpf.getMlgrppp().get(2), maprpf.getMlgrppp().get(3), maprpf.getMlgrppp().get(4),
						maprpf.getMlgrppp().get(5), maprpf.getMlgrppp().get(6), maprpf.getMlgrppp().get(7), maprpf.getMlgrppp().get(8), maprpf.getMlgrppp().get(9),
						maprpf.getMlgrppc().get(0), maprpf.getMlgrppc().get(1), maprpf.getMlgrppc().get(2), maprpf.getMlgrppc().get(3), maprpf.getMlgrppc().get(4),
						maprpf.getMlgrppc().get(5), maprpf.getMlgrppc().get(6), maprpf.getMlgrppc().get(7), maprpf.getMlgrppc().get(8), maprpf.getMlgrppc().get(9),
						maprpf.getCntcount(), maprpf.getSumins(), maprpf.getUsrprf(), maprpf.getJobnm(), maprpf.getDatime() });
	
	}

}
