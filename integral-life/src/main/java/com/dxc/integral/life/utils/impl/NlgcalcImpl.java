package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.AgecalcDTO;
import com.dxc.integral.life.beans.NlgcalcDTO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.NlgtpfDAO;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Nlgtpf;
import com.dxc.integral.life.utils.Agecalc;
import com.dxc.integral.life.utils.Nlgcalc;
/**
 * @author wli31
 */
@Service("nlgcalc")
@Lazy
public class NlgcalcImpl implements Nlgcalc {
	@Autowired
	private Datcon1 datcon1;
	
	@Autowired
	private LifepfDAO lifepfDAO;
	
	@Autowired
	private NlgtpfDAO nlgtpfDAO;
	
	@Autowired
	private Datcon3 datcon3;
	
	@Autowired
	private DescpfDAO descpfDAO;
	
	@Autowired
	private Agecalc agecalc;
	
	private String wsaaProposal;
	private Nlgtpf nlgtpf;
	
	private boolean dberror;
	private boolean isexit;

	@Override
	public NlgcalcDTO processNlgcalOvdue(NlgcalcDTO nlgcalcDTO) throws IOException,ParseException {		
		init1010(nlgcalcDTO);
		if(dberror) {
			return null;
		}
		if(isexit) {
			return nlgcalcDTO;
		}		
		if (nlgcalcDTO.getCltdob() > 0) {
			nlgcalcDTO = callAgecalc1210(nlgcalcDTO);
		}
		process2000(nlgcalcDTO);
		return nlgcalcDTO;
	}
	
	@Override
	public NlgcalcDTO processNlgCollection(NlgcalcDTO nlgcalcDTO) throws IOException, ParseException{		
		nlgcalcDTO = init1010(nlgcalcDTO);
		if(dberror) {
			return null;
		}
		if(isexit) {
			return nlgcalcDTO;
		}
		
		if (nlgcalcDTO.getCltdob() > 0) {
			nlgcalcDTO = callAgecalc1210(nlgcalcDTO);
		}
		process2000(nlgcalcDTO);
		
		return nlgcalcDTO;
	
	}
	
	protected NlgcalcDTO init1010(NlgcalcDTO nlgcalcDTO) throws IOException, ParseException {
		dberror = false;
		isexit = false;
		nlgcalcDTO.setStatus("****");
		nlgcalcDTO.setCurrAge(0);
		nlgcalcDTO.setYrsInf(0);
		wsaaProposal = "N";
		nlgcalcDTO.setFsuco("9");
	

		List<Lifepf> lifepfList = lifepfDAO.getLifeRecord(nlgcalcDTO.getChdrcoy(), nlgcalcDTO.getChdrnum(), "01", "00");
		if (lifepfList == null || lifepfList.isEmpty()) {
			dberror = true;
			return nlgcalcDTO;
		}
		nlgcalcDTO.setCltdob(lifepfList.get(0).getCltdob());
		if ("L".equals(nlgcalcDTO.getTransMode())) {
			nlgcalcDTO.setNlgFlag("N");
			updateNlgt3000(nlgcalcDTO);
			isexit = true;
			return nlgcalcDTO;
		}
		nlgtpf = nlgtpfDAO.getNlgRecord(nlgcalcDTO.getChdrcoy(), nlgcalcDTO.getChdrnum(),"1");
		if (nlgtpf == null) {
			wsaaProposal = "Y";
		}
		
		int yearsDiff = datcon3.getYearsDifference(nlgcalcDTO.getOccdate(), Integer.parseInt(datcon1.todaysDate())).intValue();
		nlgcalcDTO.setYrsInf(yearsDiff);
		return nlgcalcDTO;
	}
	
	protected NlgcalcDTO callAgecalc1210(NlgcalcDTO nlgcalcDTO) throws NumberFormatException, ParseException, IOException {
		AgecalcDTO agecalcDTO = new AgecalcDTO();
		agecalcDTO.setFunction("CALCP");
		agecalcDTO.setLanguage(nlgcalcDTO.getLanguage());
		agecalcDTO.setCnttype(nlgcalcDTO.getCnttype());
		agecalcDTO.setIntDate1(nlgcalcDTO.getCltdob() + "");
		agecalcDTO.setIntDate2(datcon1.todaysDate());//IJTI-1415
		agecalcDTO.setCompany(nlgcalcDTO.getFsuco());
		agecalcDTO = agecalc.processAgecalc(agecalcDTO);
		if(null != agecalcDTO)
			nlgcalcDTO.setCurrAge(agecalcDTO.getAgerating());	
		return nlgcalcDTO;
		
	}

	protected void updateNlgt3000(NlgcalcDTO nlgcalcDTO) {
		Nlgtpf nlgtpf1 = new Nlgtpf();
		nlgtpf1.setChdrcoy(nlgcalcDTO.getChdrcoy());
		nlgtpf1.setChdrnum(nlgcalcDTO.getChdrnum());
		nlgtpf1.setTranno(nlgcalcDTO.getTranno());
		nlgtpf1.setEffdate(nlgcalcDTO.getEffdate());
		nlgtpf1.setBatcactyr(nlgcalcDTO.getBatcactyr());
		nlgtpf1.setBatcactmn(nlgcalcDTO.getBatcactmn());
		nlgtpf1.setBatctrcde(nlgcalcDTO.getBatctrcde());
		Descpf descpf = descpfDAO.getDescInfo(nlgcalcDTO.getChdrcoy(),"T1688", nlgcalcDTO.getBatctrcde());
		if (descpf != null) {
			nlgtpf1.setTrandesc(descpf.getLongdesc());
		}
		nlgtpf1.setTranamt(nlgcalcDTO.getInputAmt());
		nlgtpf1.setFromdate(nlgcalcDTO.getFrmdate());
		nlgtpf1.setTodate(nlgcalcDTO.getTodate());
		nlgtpf1.setNlgflag(nlgcalcDTO.getNlgFlag());
		nlgtpf1.setYrsinf(nlgcalcDTO.getYrsInf());
		nlgtpf1.setAge(nlgcalcDTO.getCurrAge());
		nlgtpf1.setNlgbal(nlgcalcDTO.getNlgBalance());
		nlgtpf1.setAmnt01(nlgcalcDTO.getTotTopup());
		nlgtpf1.setAmnt02(nlgcalcDTO.getTotWdrAmt());
		nlgtpf1.setAmnt03(nlgcalcDTO.getOvduePrem());
		nlgtpf1.setAmnt04(nlgcalcDTO.getUnpaidPrem());
		nlgtpf1.setValidflag("1");
		nlgtpf1.setUsrprf("0");
		nlgtpf1.setJobnm(nlgcalcDTO.getJobName());
		nlgtpfDAO.insertNlgtpf(nlgtpf1);
	}

	protected void process2000(NlgcalcDTO nlgcalcDTO) {

		nlgcalcDTO.setNlgFlag("Y");
		if ("Y".equals(wsaaProposal)) {
			updateNlgt3000(nlgcalcDTO);
			return;
		}
		nlgcalcDTO.setTotTopup(nlgtpf.getAmnt01());
		nlgcalcDTO.setTotWdrAmt(nlgtpf.getAmnt02());
		nlgcalcDTO.setOvduePrem(nlgtpf.getAmnt03());
		nlgcalcDTO.setUnpaidPrem(nlgtpf.getAmnt04());
		nlgcalcDTO.setOvduePrem(nlgcalcDTO.getOvduePrem().add(nlgcalcDTO.getInputAmt()));
		nlgcalcDTO.setNlgBalance(nlgcalcDTO.getTotTopup().add(nlgcalcDTO.getTotWdrAmt()).add(nlgcalcDTO.getOvduePrem()).add(nlgcalcDTO.getUnpaidPrem()));
		if (nlgcalcDTO.getNlgBalance().compareTo(BigDecimal.ZERO) < 0) {
			nlgcalcDTO.setNlgFlag("N");
		}
		
		if ("COLCT".equals(nlgcalcDTO.getFunction())) {
			nlgcalcDTO = setOverdueUnpaid(nlgcalcDTO);
		}

		if (nlgcalcDTO.getNlgBalance().compareTo(BigDecimal.ZERO)>0){
			nlgcalcDTO.setNlgFlag("N");
		}
		if ("CHECK".equals(nlgcalcDTO.getFunction())) {
			return;
		}
		
		updateNlgt3000(nlgcalcDTO);
	}
	
	protected NlgcalcDTO setOverdueUnpaid(NlgcalcDTO nlgcalcDTO){
		
		nlgcalcDTO.setOvduePrem(BigDecimal.ZERO);
		nlgcalcDTO.setUnpaidPrem(BigDecimal.ZERO);
		return nlgcalcDTO;
	}
}
