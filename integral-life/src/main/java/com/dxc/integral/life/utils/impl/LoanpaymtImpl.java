package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.dxc.integral.fsu.beans.AddacmvDTO;
import com.dxc.integral.fsu.beans.CashedInputDTO;
import com.dxc.integral.fsu.beans.CashedOutputDTO;
import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.smarttable.pojo.T3629;
import com.dxc.integral.fsu.utils.Addacmv;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.utils.Loanpaymt;

/**
 * The Loanpaymt utility implementation.
 * 
 * @author mmalik25
 *
 */
@Service("loanPaymt")
@Lazy
public class LoanpaymtImpl implements Loanpaymt {

	@Autowired
	private AcblpfDAO acblpfDAO;

	@Autowired
	private LoanpfDAO loanpfDAO;

	@Autowired
	private Xcvrt xcvrt;

	@Autowired
	private Addacmv addacmv;

	@Autowired
	private Zrdecplc zrdecplc;

	@Autowired
	private LifacmvImpl lifacmv;

	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/
	
	private int sequenceNo;

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Loanpaymt#processLoanPayment(java.lang.String, com.dxc.integral.fsu.beans.CashedInputDTO)
	 */
	@Override/*ILIFE-5977*/
	public CashedOutputDTO processLoanPayment(String contractType, CashedInputDTO cashedDTO) throws IOException,ParseException {//ILIFE-5763

		BigDecimal tempAmount = cashedDTO.getOrigamt();
		BigDecimal postAmount;
		String tempCurrency = cashedDTO.getOrigccy();
		sequenceNo = cashedDTO.getTranseq();

		CashedOutputDTO cashedOutputDTO = new CashedOutputDTO();

		T5645 t5645Item = getT5645Info(cashedDTO.getChdrcoy());/*ILIFE-5977*/  /*IJTI-379*/

		Acblpf acbl = new Acblpf();
		acbl.setRldgacct(cashedDTO.getChdrnum());
		acbl.setRldgcoy(cashedDTO.getChdrcoy());
		acbl.setSacscode(cashedDTO.getSacscode());
		acbl.setSacstyp(cashedDTO.getSacstyp());
		List<Acblpf> acblpfList = acblpfDAO.getAcblpfList(acbl);
		String rldgacct = "";
		for (Acblpf acblpf : acblpfList) {
			rldgacct = acbl.getRldgacct();
			String loanNumber = "";
			String extraInfo = "";
			if(rldgacct.length() > 8){
				int totalLenght = rldgacct.length();
				if(totalLenght <= 10) {
					loanNumber = rldgacct.substring(8, totalLenght);
				} else {
					loanNumber = rldgacct.substring(8, 10);
					extraInfo = rldgacct.substring(10, totalLenght);
				}
			} else {
				break;
			}
			
			if("".equals(loanNumber) || !"".equals(extraInfo)){
				break;
			}

			if (acblpf.getSacscurbal().compareTo(BigDecimal.ZERO) == 0) {
				checkAndProcessActiveLoan(acblpf, t5645Item, cashedDTO.getTranno());
				continue;
			}

			if (acblpf.getSacscurbal().compareTo(BigDecimal.ZERO) == -1) {
				acblpf.setSacscurbal(
						acblpf.getSacscurbal().multiply(new BigDecimal(-1)).setScale(2, RoundingMode.CEILING));
			}

			AddacmvDTO addacmvDTO = populateAddacmvDTo(contractType, rldgacct, cashedDTO);
			if (!tempCurrency.equals(acblpf.getOrigcurr())) {

				LifacmvDTO lifacmvDTO = populateLifeacmvDTO(tempAmount, tempCurrency, 5, t5645Item);

				//call Lifacmv
				currencyPosting(lifacmvDTO, rldgacct, cashedDTO);/*ILIFE-5977*/

				ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
				conlinkInput.setToCurrency(acblpf.getOrigcurr());
				conlinkInput.setAmount(tempAmount);

				//call Xcvrt
				ConlinkOutputDTO conlinkOutput = convertCurrency(conlinkInput, cashedDTO);
				tempCurrency = acblpf.getOrigcurr();
				tempAmount = conlinkOutput.getCalculatedAmount();
				
				lifacmvDTO = populateLifeacmvDTO(conlinkOutput.getCalculatedAmount(), conlinkInput.getToCurrency(), 6,
						t5645Item);
				//call Lifacmv
				currencyPosting(lifacmvDTO, rldgacct, cashedDTO);/*ILIFE-5977*/
			}
			
			if (tempAmount.compareTo(acblpf.getSacscurbal()) >= 0) {
				postAmount = acblpf.getSacscurbal();
				postAcmv(addacmvDTO, tempCurrency, postAmount, t5645Item);

				checkAndProcessActiveLoan(acblpf, t5645Item, cashedDTO.getTranno());
				tempAmount = tempAmount.subtract(acblpf.getSacscurbal()).setScale(2, RoundingMode.CEILING);
			} else {
				postAmount = tempAmount;
				postAcmv(addacmvDTO, tempCurrency, postAmount, t5645Item);/*ILIFE-5977*/
				tempAmount = BigDecimal.ZERO;
				break;
			}
		}
		
		//end processing
		cashedOutputDTO.setDocamt(BigDecimal.ZERO);
		if (tempAmount.compareTo(BigDecimal.ZERO) > 0) {
			if (tempCurrency != cashedDTO.getOrigccy()) {
				LifacmvDTO lifacmvDTO = populateLifeacmvDTO(tempAmount, tempCurrency, 5, t5645Item);
				//call Lifacmv
				currencyPosting(lifacmvDTO, rldgacct, cashedDTO);/*ILIFE-5977*/

				ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
				conlinkInput.setToCurrency(cashedDTO.getOrigccy());
				conlinkInput.setAmount(tempAmount);
				//call Xcvrt
				ConlinkOutputDTO conlinkOutput = convertCurrency(conlinkInput, cashedDTO);
				cashedOutputDTO.setDocamt(conlinkOutput.getCalculatedAmount());
				lifacmvDTO = populateLifeacmvDTO(conlinkOutput.getCalculatedAmount(), conlinkInput.getToCurrency(), 6,
						t5645Item);
				//call Lifacmv
				currencyPosting(lifacmvDTO, rldgacct, cashedDTO);/*ILIFE-5977*/
			} else {
				cashedOutputDTO.setDocamt(tempAmount);
			}
		}
		cashedOutputDTO.setTranseq(sequenceNo);
		return cashedOutputDTO;

	}

	private AddacmvDTO getGlCurrency(AddacmvDTO addacmvDTO, BigDecimal postAmount, String itemitem) throws IOException {
		String contitem = itemitem;
		boolean isNominalRateFound = false;
		BigDecimal nominalRate = BigDecimal.ZERO;
		String ledgerCurrency="";
		Map<String, Itempf> t3629Map=itempfDAO.readSmartTableByTableName2(addacmvDTO.getBatccoy(), "T3629", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/  /*IJTI-379*/
		while (contitem != null && !contitem.isEmpty()) {
			if (t3629Map.containsKey(contitem)) {
				T3629 t3629 = smartTableDataUtils.convertGenareaToPojo(t3629Map.get(contitem).getGenareaj(), T3629.class); /*IJTI-398*/
				ledgerCurrency = t3629.getLedgcurr();
				contitem = t3629.getContitem();
				for (int i = 0; t3629.getFrmdates().size() > i; i++) {
					if (addacmvDTO.getEffdate() >= t3629.getFrmdates().get(i)
							&& addacmvDTO.getEffdate() <= t3629.getTodates().get(i)) {
						nominalRate = t3629.getScrates().get(i);
						isNominalRateFound = true;
						break;
					}
				}
				if (isNominalRateFound) {
					break;
				}
			}
		}
		addacmvDTO.setCrate(nominalRate);
		addacmvDTO.setGenlcur(ledgerCurrency);
		addacmvDTO.setAcctamt(postAmount.multiply(nominalRate));
		return addacmvDTO;
	}

	private LifacmvDTO populateLifeacmvDTO(BigDecimal orgAmnt, String orgCurr, int i, T5645 t5645IO) {

		LifacmvDTO lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setOrigamt(orgAmnt);
		lifacmvDTO.setOrigcurr(orgCurr);
		lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(i));
		lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(i));
		lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(i));
		lifacmvDTO.setGlsign(t5645IO.getSigns().get(i));
		return lifacmvDTO;
	}
	/*ILIFE-5977*/
	private AddacmvDTO postAcmv(AddacmvDTO addacmvDTO, String tempCurrency, BigDecimal postAmount, T5645 t5645IO) throws IOException {

		addacmvDTO.setGlcode(t5645IO.getGlmaps().get(0));
		addacmvDTO.setSacscode(t5645IO.getSacscodes().get(0));
		addacmvDTO.setSacstyp(t5645IO.getSacstypes().get(0));
		addacmvDTO.setGlsign(t5645IO.getSigns().get(0));
		addacmvDTO.setOrigamt(postAmount);
		addacmvDTO.setOrigccy(tempCurrency);
		AddacmvDTO addacmvIO = getGlCurrency(addacmvDTO, postAmount, tempCurrency);/*ILIFE-5977*/
		addacmvIO.setRdocpfx("CA");
		addacmv.processAcmvpf(addacmvIO);/*ILIFE-5977*/
		return addacmvDTO;
	}

	private AddacmvDTO populateAddacmvDTo(String contractType, String rldgacct, CashedInputDTO cashedDTO) {

		AddacmvDTO addacmvDTO = new AddacmvDTO();
		addacmvDTO.setContot(0);
		addacmvDTO.setBatcpfx(cashedDTO.getBatcpfx());
		addacmvDTO.setBatccoy(cashedDTO.getBatccoy());
		addacmvDTO.setBatcbrn(cashedDTO.getBatcbrn());
		addacmvDTO.setBatcactyr(cashedDTO.getBatcactyr());
		addacmvDTO.setBatcactmn(cashedDTO.getBatcactmn());
		addacmvDTO.setBatctrcde(cashedDTO.getBatctrcde());
		addacmvDTO.setBatcbatch(cashedDTO.getBatcbatch());
		addacmvDTO.setPostmonth(cashedDTO.getBatcactmn());
		addacmvDTO.setPostyear(cashedDTO.getBatcactyr());
		addacmvDTO.setRdocnum(cashedDTO.getDoctNumber());
		addacmvDTO.setRldgcoy(cashedDTO.getDoctCompany());
		addacmvDTO.setRldgacct(rldgacct);
		addacmvDTO.setGenlcoy(cashedDTO.getGenlCompany());
		addacmvDTO.setGenlcur(cashedDTO.getGenlCurrency());
		sequenceNo++;
		addacmvDTO.setTranseq(sequenceNo);
		addacmvDTO.setEffdate(cashedDTO.getTrandate());
		addacmvDTO.setTransactionTime(0);
		addacmvDTO.setTransactionDate(0);
		addacmvDTO.setTrandesc(cashedDTO.getTrandesc());
		addacmvDTO.setChdrstcda(contractType);
		addacmvDTO.setAcctccy(cashedDTO.getAcctccy());
		addacmvDTO.setTranno(cashedDTO.getTranno());
		addacmvDTO.setTermid("");
		addacmvDTO.setUser(0);

		return addacmvDTO;

	}

	private void checkAndProcessActiveLoan(Acblpf acblpf, T5645 t5645IO, Integer tranno) {

		String chdrNum = acblpf.getRldgacct().substring(0, 8);
		String loanNum = acblpf.getRldgacct().substring(8, 10);
		Loanpf loanpf = loanpfDAO.getLoanpfByCompanyContractNumberAndLoanNumber(acblpf.getRldgcoy(), chdrNum, Integer.parseInt(loanNum));
		if (loanpf == null) {
			return;
		}
		String sacsCode;
		String sacsType;
		boolean turnOffLoan = false;
		
		if ("P".equals(loanpf.getLoantype())) {
			if (t5645IO.getSacscodes().get(1).equals(acblpf.getSacscode())
					&& t5645IO.getSacstypes().get(1).equals(acblpf.getSacstyp())) {
				sacsCode = t5645IO.getSacscodes().get(2);
				sacsType = t5645IO.getSacstypes().get(2);
				Acblpf acblIO = new Acblpf();
				acblIO.setSacscode(sacsCode);
				acblIO.setSacstyp(sacsType);
				acblIO.setRldgacct(acblpf.getRldgacct());
				acblIO.setOrigcurr(acblpf.getOrigcurr());
				turnOffLoan = checkAccountBalance(acblIO);
			}
			if (t5645IO.getSacscodes().get(2).equals(acblpf.getSacscode())
					&& t5645IO.getSacstypes().get(2).equals(acblpf.getSacstyp())) {
				sacsCode = t5645IO.getSacscodes().get(1);
				sacsType = t5645IO.getSacstypes().get(1);
				Acblpf acblIO = new Acblpf();
				acblIO.setSacscode(sacsCode);
				acblIO.setSacstyp(sacsType);
				acblIO.setRldgacct(acblpf.getRldgacct());
				acblIO.setOrigcurr(acblpf.getOrigcurr());
				turnOffLoan = checkAccountBalance(acblIO);
			}
		}
		
		if ("A".equals(loanpf.getLoantype())) {
			if (t5645IO.getSacscodes().get(3).equals(acblpf.getSacscode())
					&& t5645IO.getSacstypes().get(3).equals(acblpf.getSacstyp())) {
				sacsCode = t5645IO.getSacscodes().get(4);
				sacsType = t5645IO.getSacstypes().get(4);
				Acblpf acblIO = new Acblpf();
				acblIO.setSacscode(sacsCode);
				acblIO.setSacstyp(sacsType);
				acblIO.setRldgacct(acblpf.getRldgacct());
				acblIO.setOrigcurr(acblpf.getOrigcurr());
				turnOffLoan = checkAccountBalance(acblIO);
			}
			if (t5645IO.getSacscodes().get(4).equals(acblpf.getSacscode())
					&& t5645IO.getSacstypes().get(4).equals(acblpf.getSacstyp())) {
				sacsCode = t5645IO.getSacscodes().get(3);
				sacsType = t5645IO.getSacstypes().get(3);
				Acblpf acblIO = new Acblpf();
				acblIO.setSacscode(sacsCode);
				acblIO.setSacstyp(sacsType);
				acblIO.setRldgacct(acblpf.getRldgacct());
				acblIO.setOrigcurr(acblpf.getOrigcurr());
				turnOffLoan = checkAccountBalance(acblIO);
			}
		}
		
		if ("E".equals(loanpf.getLoantype())) {
			if (t5645IO.getSacscodes().get(7).equals(acblpf.getSacscode())
					&& t5645IO.getSacstypes().get(7).equals(acblpf.getSacstyp())) {
				sacsCode = t5645IO.getSacscodes().get(8);
				sacsType = t5645IO.getSacstypes().get(8);
				Acblpf acblIO = new Acblpf();
				acblIO.setSacscode(sacsCode);
				acblIO.setSacstyp(sacsType);
				acblIO.setRldgacct(acblpf.getRldgacct());
				acblIO.setOrigcurr(acblpf.getOrigcurr());
				turnOffLoan = checkAccountBalance(acblIO);
			}
			if (t5645IO.getSacscodes().get(8).equals(acblpf.getSacscode())
					&& t5645IO.getSacstypes().get(8).equals(acblpf.getSacstyp())) {
				sacsCode = t5645IO.getSacscodes().get(7);
				sacsType = t5645IO.getSacstypes().get(7);
				Acblpf acblIO = new Acblpf();
				acblIO.setSacscode(sacsCode);
				acblIO.setSacstyp(sacsType);
				acblIO.setRldgacct(acblpf.getRldgacct());
				acblIO.setOrigcurr(acblpf.getOrigcurr());
				turnOffLoan = checkAccountBalance(acblIO);
			}
		}
		
		if (turnOffLoan) {
			loanpf.setValidflag("2");
			loanpf.setLtranno(tranno);
			loanpfDAO.updateLoanpf(loanpf);
		} else {
			loanpfDAO.updateLoanpf(loanpf);
		}
	}
	
	private boolean checkAccountBalance(Acblpf acblIO){
		Acblpf acblOuput = acblpfDAO.readAcblpf(acblIO);
		if (acblOuput == null || acblOuput.getSacscurbal().compareTo(BigDecimal.ZERO) == 0) {
			return true;
		}
		return false;
	}
	/*ILIFE-5977*/
	private void currencyPosting(LifacmvDTO lifacmvDTO, String rldgacct, CashedInputDTO cashedDTO) throws IOException,ParseException {//ILIFE-5763

		lifacmvDTO.setContot(0);
		lifacmvDTO.setTermid("");
		lifacmvDTO.setUser(0);
		lifacmvDTO.setBatccoy(cashedDTO.getBatccoy());
		lifacmvDTO.setBatcbrn(cashedDTO.getBatcbrn());
		lifacmvDTO.setBatcactyr(cashedDTO.getBatcactyr());
		lifacmvDTO.setBatcactmn(cashedDTO.getBatcactmn());
		lifacmvDTO.setBatctrcde(cashedDTO.getBatctrcde());
		lifacmvDTO.setBatcbatch(cashedDTO.getBatcbatch());
		lifacmvDTO.setPostmonth(cashedDTO.getBatcactmn().toString());
		lifacmvDTO.setPostyear(cashedDTO.getBatcactyr().toString());
		lifacmvDTO.setRdocnum(cashedDTO.getDoctNumber());
		lifacmvDTO.setRldgcoy(cashedDTO.getDoctCompany());
		lifacmvDTO.setRldgacct(rldgacct);
		lifacmvDTO.setGenlcoy(cashedDTO.getGenlCompany());
		sequenceNo++;
		lifacmvDTO.setJrnseq(sequenceNo);
		lifacmvDTO.setEffdate(cashedDTO.getTrandate());
		lifacmvDTO.setTrandesc(cashedDTO.getTrandesc());
		lifacmvDTO.setTransactionDate(0);
		lifacmvDTO.setTransactionTime(0);
		lifacmvDTO.setTranref(cashedDTO.getChdrnum());
		lifacmvDTO.setCrate(BigDecimal.ZERO);
		lifacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setTranno(cashedDTO.getTranno());
		lifacmv.executePSTW(lifacmvDTO);/*ILIFE-5977*/
	}
	/*ILIFE-5977*/
	private ConlinkOutputDTO convertCurrency(ConlinkInputDTO conlinkInput, CashedInputDTO cashedDTO)
			throws IOException {

		conlinkInput.setFromCurrency(cashedDTO.getOrigccy());
		conlinkInput.setCashdate(99999999);
		conlinkInput.setCompany(cashedDTO.getChdrcoy());
		ConlinkOutputDTO conlinkOutput = xcvrt.executeRealFunction(conlinkInput);/*ILIFE-5977*/
		BigDecimal roundedAmount = callRounding(conlinkOutput.getCalculatedAmount(), cashedDTO);/*ILIFE-5977*/
		conlinkOutput.setCalculatedAmount(roundedAmount);
		return conlinkOutput;
	}

	private BigDecimal callRounding(BigDecimal amountIn, CashedInputDTO cashedDTO) throws IOException {/*ILIFE-5977*/

		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setCurrency(cashedDTO.getOrigccy());
		zrdecplDTO.setBatctrcde(cashedDTO.getBatctrcde());
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCompany(cashedDTO.getChdrcoy());
		return zrdecplc.convertAmount(zrdecplDTO);/*ILIFE-5977*/
	}

	private T5645 getT5645Info(String company) throws IOException {/*ILIFE-5977*/  /*IJTI-379*/
		Map<String, List<Itempf>> t5645Map=itempfDAO.readSmartTableByTableName(company, "T5645", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/  /*IJTI-379*/
		List<Itempf> t5645ItemList = t5645Map.get("LOANPYMT");
		if(null==t5645ItemList){//ILIFE-5763 Starts
			throw new ItemNotfoundException("Item LOANPYMT not found in Table T5645");
		}//ILIFE-5763 Ends
		return smartTableDataUtils.convertGenareaToPojo(t5645ItemList.get(0).getGenareaj(), T5645.class); /*IJTI-398*/
	}
}
