package com.dxc.integral.life.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HpadpfDAO;
import com.dxc.integral.life.dao.model.Hpadpf;

/**
 * HpadpfDAO implementation for database table <b>HPADPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@Repository("hpadpfDAO")
public class HpadpfDAOImpl extends BaseDAOImpl implements HpadpfDAO {

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.HpadpfDAO#readHpadpfByCompanyAndContractNumber(java.lang.String, java.lang.String)
	 */
	@Override
	public Hpadpf readHpadpfByCompanyAndContractNumber(String company, String contractNumber) {
		StringBuilder sql = new StringBuilder("select * from HPADPF where chdrcoy=? and chdrnum=? ");
		sql.append("order by chdrcoy asc, chdrnum asc, unique_number desc");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {company, contractNumber},
				new BeanPropertyRowMapper<Hpadpf>(Hpadpf.class));
	}

}
