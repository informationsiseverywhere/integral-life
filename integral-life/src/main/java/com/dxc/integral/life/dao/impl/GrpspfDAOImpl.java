package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.GrpspfDAO;
import com.dxc.integral.life.dao.model.Grpspf;
/**
 * @author wli31
 */
@Repository("grpspfDAO")
@Lazy
public class GrpspfDAOImpl extends BaseDAOImpl implements GrpspfDAO {

	public Grpspf readGrpspf(String grupcoy, String grupnum) {
		Grpspf grpspf = null;
		StringBuilder sql = new StringBuilder("SELECT * FROM GRPSPF WHERE GRUPCOY=? AND GRUPNUM=?" );
		List<Grpspf> grpspfList = jdbcTemplate.query(sql.toString(), new Object[] { grupcoy,grupnum},new BeanPropertyRowMapper<Grpspf>(Grpspf.class));
		for(Grpspf grpsIO: grpspfList){
			grpspf = grpsIO;
		}
		return grpspf;
	}

}
