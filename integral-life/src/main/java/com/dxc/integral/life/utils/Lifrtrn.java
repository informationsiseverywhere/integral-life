package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.LifrtrnDTO;

/**
 * 
 * @author xma3
 *
 */
public interface Lifrtrn {
	public void calcLitrtrn(LifrtrnDTO lifrtrnPojo)  throws IOException;
}
