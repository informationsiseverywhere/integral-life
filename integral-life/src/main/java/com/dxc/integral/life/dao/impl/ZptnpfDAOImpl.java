package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ZptnpfDAO;
import com.dxc.integral.life.dao.model.Zptnpf;

/**
 * 
 * @author xma3
 *
 */

@Repository("zptnpfDAO")
@Lazy
public class ZptnpfDAOImpl extends BaseDAOImpl implements ZptnpfDAO {

	@Override
	public void insertZptnpfRecords(List<Zptnpf> zptnpfList) {
		final List<Zptnpf> tempZptnpfList = zptnpfList;   
		StringBuffer sql = new StringBuffer("INSERT INTO ZPTNPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,EFFDATE,ORIGAMT,TRCDE,BILLCD,INSTFROM,INSTTO,ZPRFLG,TRANDATE,");
		sql.append("USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		jdbcTemplate.batchUpdate(sql.toString(),new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
            	return tempZptnpfList.size();
            }
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	ps.setString(1, zptnpfList.get(i).getChdrcoy());
            	ps.setString(2, zptnpfList.get(i).getChdrnum());
            	ps.setString(3, zptnpfList.get(i).getLife());
            	ps.setString(4, zptnpfList.get(i).getCoverage());
            	ps.setString(5, zptnpfList.get(i).getRider());
            	ps.setInt(6, zptnpfList.get(i).getTranno());
            	ps.setInt(7, zptnpfList.get(i).getEffdate());
            	ps.setBigDecimal(8, zptnpfList.get(i).getOrigamt());
            	ps.setString(9, zptnpfList.get(i).getTransCode());
            	ps.setInt(10, zptnpfList.get(i).getBillcd());
            	ps.setInt(11, zptnpfList.get(i).getInstfrom());
            	ps.setInt(12, zptnpfList.get(i).getInstto());
            	ps.setString(13, zptnpfList.get(i).getZprflg());
            	ps.setInt(14, zptnpfList.get(i).getTrandate());
            	ps.setString(15, zptnpfList.get(i).getUserProfile());
            	ps.setString(16, zptnpfList.get(i).getJobName());
            	ps.setTimestamp(17, new Timestamp(System.currentTimeMillis()));	
            }
            
	    });
	}
		
}
