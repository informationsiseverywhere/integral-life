package com.dxc.integral.life.dao;

import java.util.List;
import java.util.Map;
import com.dxc.integral.life.dao.model.Lincpf;

/**
 * @author gsaluja2
 *
 */
public interface LincpfDAO {
	public void updateLincData(List<Lincpf> lincpfUpdateList);
	public Lincpf readRecordOfLincpf(String contractNumber);
	public Map<String, Lincpf> readLincpfData();
}
