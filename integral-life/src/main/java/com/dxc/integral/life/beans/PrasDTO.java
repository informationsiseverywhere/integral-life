package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class PrasDTO {
  	private String prascalcRec;
  	private String statuz;
  	private String clntcoy;
  	private String clntnum;
  	private int incomeSeqNo;
  	private String cnttype;
  	private BigDecimal grossprem;
  	private int effdate;
  	private String taxrelmth;
  	private String company;
  	private String inrevnum;
  	private BigDecimal taxrelamt;
	
  	public PrasDTO() {
	}
	public String getPrascalcRec() {
		return prascalcRec;
	}
	public void setPrascalcRec(String prascalcRec) {
		this.prascalcRec = prascalcRec;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public int getIncomeSeqNo() {
		return incomeSeqNo;
	}
	public void setIncomeSeqNo(int incomeSeqNo) {
		this.incomeSeqNo = incomeSeqNo;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public BigDecimal getGrossprem() {
		return grossprem;
	}
	public void setGrossprem(BigDecimal grossprem) {
		this.grossprem = grossprem;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getTaxrelmth() {
		return taxrelmth;
	}
	public void setTaxrelmth(String taxrelmth) {
		this.taxrelmth = taxrelmth;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getInrevnum() {
		return inrevnum;
	}
	public void setInrevnum(String inrevnum) {
		this.inrevnum = inrevnum;
	}
	public BigDecimal getTaxrelamt() {
		return taxrelamt;
	}
	public void setTaxrelamt(BigDecimal taxrelamt) {
		this.taxrelamt = taxrelamt;
	}
}