package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class PremiumDTO {

	private String function;
	private String statuz;
	private String chdrChdrcoy;
	private String chdrChdrnum;
	private String lifeLife;
	private String covrCoverage;
	private String covrRider;
	private int plnsfx;
	private String billfreq;
	private String mop;
	private String lifeJlife;
	private String crtable;
	private String mortcls;
	private String currcode;
	private int effectdt;
	private int termdate;
	private BigDecimal sumin;
	private BigDecimal calcPrem;
	private BigDecimal calcBasPrem;
	private BigDecimal calcLoaPrem;
	private int ratingdate;
	private int reRateDate;
	private int lage;
	private int jlage;
	private String lsex;
	private String jlsex;
	private int duration;
	private String reasind;
	private String freqann;
	private String arrears;
	private String advance;
	private int guarperd;
	private BigDecimal intanny;
	private BigDecimal capcont;
	private String withprop;
	private String withoprop;
	private String ppind;
	private String nomlife;
	private BigDecimal dthpercn;
	private BigDecimal dthperco;
	private int riskCessTerm;
	private int benCessTerm;
	private String cnttype;
	private String language;
	private String benpln;
	private String premMethod;
	private String rstaflag;
	private String ccode;
	private String livesno;
	private String indic;
	private BigDecimal totprem;
	private BigDecimal percent;
	private int factor;
	private String liveno;
	private BigDecimal loadper;
	private BigDecimal rateadj;
	private BigDecimal fltmort;
	private BigDecimal premadj;
	private BigDecimal adjustageamt;
	private String rstate01;
	private String rstate02;
	private BigDecimal zstpduty01;
	private BigDecimal zstpduty02;
	private String waitperiod;
	private String bentrm;
	private String prmbasis;
	private String poltyp;
	private String occpcode;
	private String occpclass;
	private String dialdownoption;
	private String linkcov;
	private String liencd;
	private String tpdtype;
	private String setPmexCall;
	private String prevBillFreq;
	private String lnkgind;
	private String updateRequired;
	private String validflag;
	private String lnkgSubRefNo;
	private BigDecimal inputPrevPrem;
	private BigDecimal calcTotPrem;
	private BigDecimal riskPrem;

	public PremiumDTO() {
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getStatuz() {
		return statuz;
	}

	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}

	public String getChdrChdrcoy() {
		return chdrChdrcoy;
	}

	public void setChdrChdrcoy(String chdrChdrcoy) {
		this.chdrChdrcoy = chdrChdrcoy;
	}

	public String getChdrChdrnum() {
		return chdrChdrnum;
	}

	public void setChdrChdrnum(String chdrChdrnum) {
		this.chdrChdrnum = chdrChdrnum;
	}

	public String getLifeLife() {
		return lifeLife;
	}

	public void setLifeLife(String lifeLife) {
		this.lifeLife = lifeLife;
	}

	public String getCovrCoverage() {
		return covrCoverage;
	}

	public void setCovrCoverage(String covrCoverage) {
		this.covrCoverage = covrCoverage;
	}

	public String getCovrRider() {
		return covrRider;
	}

	public void setCovrRider(String covrRider) {
		this.covrRider = covrRider;
	}

	public int getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public String getMop() {
		return mop;
	}

	public void setMop(String mop) {
		this.mop = mop;
	}

	public String getLifeJlife() {
		return lifeJlife;
	}

	public void setLifeJlife(String lifeJlife) {
		this.lifeJlife = lifeJlife;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getMortcls() {
		return mortcls;
	}

	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}

	public String getCurrcode() {
		return currcode;
	}

	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}

	public int getEffectdt() {
		return effectdt;
	}

	public void setEffectdt(int effectdt) {
		this.effectdt = effectdt;
	}

	public int getTermdate() {
		return termdate;
	}

	public void setTermdate(int termdate) {
		this.termdate = termdate;
	}

	public BigDecimal getSumin() {
		return sumin;
	}

	public void setSumin(BigDecimal sumin) {
		this.sumin = sumin;
	}

	public BigDecimal getCalcPrem() {
		return calcPrem;
	}

	public void setCalcPrem(BigDecimal calcPrem) {
		this.calcPrem = calcPrem;
	}

	public BigDecimal getCalcBasPrem() {
		return calcBasPrem;
	}

	public void setCalcBasPrem(BigDecimal calcBasPrem) {
		this.calcBasPrem = calcBasPrem;
	}

	public BigDecimal getCalcLoaPrem() {
		return calcLoaPrem;
	}

	public void setCalcLoaPrem(BigDecimal calcLoaPrem) {
		this.calcLoaPrem = calcLoaPrem;
	}

	public int getRatingdate() {
		return ratingdate;
	}

	public void setRatingdate(int ratingdate) {
		this.ratingdate = ratingdate;
	}

	public int getReRateDate() {
		return reRateDate;
	}

	public void setReRateDate(int reRateDate) {
		this.reRateDate = reRateDate;
	}

	public int getLage() {
		return lage;
	}

	public void setLage(int lage) {
		this.lage = lage;
	}

	public int getJlage() {
		return jlage;
	}

	public void setJlage(int jlage) {
		this.jlage = jlage;
	}

	public String getLsex() {
		return lsex;
	}

	public void setLsex(String lsex) {
		this.lsex = lsex;
	}

	public String getJlsex() {
		return jlsex;
	}

	public void setJlsex(String jlsex) {
		this.jlsex = jlsex;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getReasind() {
		return reasind;
	}

	public void setReasind(String reasind) {
		this.reasind = reasind;
	}

	public String getFreqann() {
		return freqann;
	}

	public void setFreqann(String freqann) {
		this.freqann = freqann;
	}

	public String getArrears() {
		return arrears;
	}

	public void setArrears(String arrears) {
		this.arrears = arrears;
	}

	public String getAdvance() {
		return advance;
	}

	public void setAdvance(String advance) {
		this.advance = advance;
	}

	public int getGuarperd() {
		return guarperd;
	}

	public void setGuarperd(int guarperd) {
		this.guarperd = guarperd;
	}

	public BigDecimal getIntanny() {
		return intanny;
	}

	public void setIntanny(BigDecimal intanny) {
		this.intanny = intanny;
	}

	public BigDecimal getCapcont() {
		return capcont;
	}

	public void setCapcont(BigDecimal capcont) {
		this.capcont = capcont;
	}

	public String getWithprop() {
		return withprop;
	}

	public void setWithprop(String withprop) {
		this.withprop = withprop;
	}

	public String getWithoprop() {
		return withoprop;
	}

	public void setWithoprop(String withoprop) {
		this.withoprop = withoprop;
	}

	public String getPpind() {
		return ppind;
	}

	public void setPpind(String ppind) {
		this.ppind = ppind;
	}

	public String getNomlife() {
		return nomlife;
	}

	public void setNomlife(String nomlife) {
		this.nomlife = nomlife;
	}

	public BigDecimal getDthpercn() {
		return dthpercn;
	}

	public void setDthpercn(BigDecimal dthpercn) {
		this.dthpercn = dthpercn;
	}

	public BigDecimal getDthperco() {
		return dthperco;
	}

	public void setDthperco(BigDecimal dthperco) {
		this.dthperco = dthperco;
	}

	public int getRiskCessTerm() {
		return riskCessTerm;
	}

	public void setRiskCessTerm(int riskCessTerm) {
		this.riskCessTerm = riskCessTerm;
	}

	public int getBenCessTerm() {
		return benCessTerm;
	}

	public void setBenCessTerm(int benCessTerm) {
		this.benCessTerm = benCessTerm;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getBenpln() {
		return benpln;
	}

	public void setBenpln(String benpln) {
		this.benpln = benpln;
	}

	public String getPremMethod() {
		return premMethod;
	}

	public void setPremMethod(String premMethod) {
		this.premMethod = premMethod;
	}

	public String getRstaflag() {
		return rstaflag;
	}

	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}

	public String getCcode() {
		return ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	public String getLivesno() {
		return livesno;
	}

	public void setLivesno(String livesno) {
		this.livesno = livesno;
	}

	public String getIndic() {
		return indic;
	}

	public void setIndic(String indic) {
		this.indic = indic;
	}

	public BigDecimal getTotprem() {
		return totprem;
	}

	public void setTotprem(BigDecimal totprem) {
		this.totprem = totprem;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public int getFactor() {
		return factor;
	}

	public void setFactor(int factor) {
		this.factor = factor;
	}

	public String getLiveno() {
		return liveno;
	}

	public void setLiveno(String liveno) {
		this.liveno = liveno;
	}

	public BigDecimal getLoadper() {
		return loadper;
	}

	public void setLoadper(BigDecimal loadper) {
		this.loadper = loadper;
	}

	public BigDecimal getRateadj() {
		return rateadj;
	}

	public void setRateadj(BigDecimal rateadj) {
		this.rateadj = rateadj;
	}

	public BigDecimal getFltmort() {
		return fltmort;
	}

	public void setFltmort(BigDecimal fltmort) {
		this.fltmort = fltmort;
	}

	public BigDecimal getPremadj() {
		return premadj;
	}

	public void setPremadj(BigDecimal premadj) {
		this.premadj = premadj;
	}

	public BigDecimal getAdjustageamt() {
		return adjustageamt;
	}

	public void setAdjustageamt(BigDecimal adjustageamt) {
		this.adjustageamt = adjustageamt;
	}

	public String getRstate01() {
		return rstate01;
	}

	public void setRstate01(String rstate01) {
		this.rstate01 = rstate01;
	}

	public String getRstate02() {
		return rstate02;
	}

	public void setRstate02(String rstate02) {
		this.rstate02 = rstate02;
	}

	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}

	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}

	public BigDecimal getZstpduty02() {
		return zstpduty02;
	}

	public void setZstpduty02(BigDecimal zstpduty02) {
		this.zstpduty02 = zstpduty02;
	}

	public String getWaitperiod() {
		return waitperiod;
	}

	public void setWaitperiod(String waitperiod) {
		this.waitperiod = waitperiod;
	}

	public String getBentrm() {
		return bentrm;
	}

	public void setBentrm(String bentrm) {
		this.bentrm = bentrm;
	}

	public String getPrmbasis() {
		return prmbasis;
	}

	public void setPrmbasis(String prmbasis) {
		this.prmbasis = prmbasis;
	}

	public String getPoltyp() {
		return poltyp;
	}

	public void setPoltyp(String poltyp) {
		this.poltyp = poltyp;
	}

	public String getOccpcode() {
		return occpcode;
	}

	public void setOccpcode(String occpcode) {
		this.occpcode = occpcode;
	}

	public String getOccpclass() {
		return occpclass;
	}

	public void setOccpclass(String occpclass) {
		this.occpclass = occpclass;
	}

	public String getDialdownoption() {
		return dialdownoption;
	}

	public void setDialdownoption(String dialdownoption) {
		this.dialdownoption = dialdownoption;
	}

	public String getLinkcov() {
		return linkcov;
	}

	public void setLinkcov(String linkcov) {
		this.linkcov = linkcov;
	}

	public String getLiencd() {
		return liencd;
	}

	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}

	public String getTpdtype() {
		return tpdtype;
	}

	public void setTpdtype(String tpdtype) {
		this.tpdtype = tpdtype;
	}

	public String getSetPmexCall() {
		return setPmexCall;
	}

	public void setSetPmexCall(String setPmexCall) {
		this.setPmexCall = setPmexCall;
	}

	public String getPrevBillFreq() {
		return prevBillFreq;
	}

	public void setPrevBillFreq(String prevBillFreq) {
		this.prevBillFreq = prevBillFreq;
	}

	public String getLnkgind() {
		return lnkgind;
	}

	public void setLnkgind(String lnkgind) {
		this.lnkgind = lnkgind;
	}

	public String getUpdateRequired() {
		return updateRequired;
	}

	public void setUpdateRequired(String updateRequired) {
		this.updateRequired = updateRequired;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getLnkgSubRefNo() {
		return lnkgSubRefNo;
	}

	public void setLnkgSubRefNo(String lnkgSubRefNo) {
		this.lnkgSubRefNo = lnkgSubRefNo;
	}

	public BigDecimal getInputPrevPrem() {
		return inputPrevPrem;
	}

	public void setInputPrevPrem(BigDecimal inputPrevPrem) {
		this.inputPrevPrem = inputPrevPrem;
	}

	public BigDecimal getCalcTotPrem() {
		return calcTotPrem;
	}

	public void setCalcTotPrem(BigDecimal calcTotPrem) {
		this.calcTotPrem = calcTotPrem;
	}

	public BigDecimal getRiskPrem() {
		return riskPrem;
	}

	public void setRiskPrem(BigDecimal riskPrem) {
		this.riskPrem = riskPrem;
	}
}