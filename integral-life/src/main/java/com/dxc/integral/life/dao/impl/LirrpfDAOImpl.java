package com.dxc.integral.life.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LirrpfDAO;
import com.dxc.integral.life.dao.model.Lirrpf;

public class LirrpfDAOImpl extends BaseDAOImpl implements LirrpfDAO {

	@Override
	public Lirrpf search(Lirrpf lirrpf) {
		String sql = "SELECT * FROM VM1DTA.LIRRPF WHERE COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND RASNUM=? AND RNGMNT=? AND CLNTPFX=? AND CLNTCOY=? AND CLNTNUM=?";
		return jdbcTemplate.queryForObject(sql,
				new Object[] { lirrpf.getCompany(), lirrpf.getChdrnum(), lirrpf.getLife(), lirrpf.getCoverage(),
						lirrpf.getRider(), lirrpf.getPlanSuffix(), lirrpf.getRasnum(), lirrpf.getRngmnt(),
						lirrpf.getClntpfx(), lirrpf.getClntcoy(), lirrpf.getClntnum() },
				new BeanPropertyRowMapper<Lirrpf>(Lirrpf.class));
	}

	@Override
	public void update(Lirrpf lirrpf) {
		String sql = "UPDATE VM1DTA.LIRRPF SET CURRTO=?, VALIDFLAG=?, RAAMOUNT=? WHERE COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND RASNUM=? AND RNGMNT=? AND CLNTPFX=? AND CLNTCOY=? AND CLNTNUM=?";
		jdbcTemplate.update(sql,
				new Object[] { lirrpf.getCurrto(), lirrpf.getValidflag(), lirrpf.getRaAmount(), lirrpf.getCompany(),
						lirrpf.getChdrnum(), lirrpf.getLife(), lirrpf.getCoverage(), lirrpf.getRider(),
						lirrpf.getPlanSuffix(), lirrpf.getRasnum(), lirrpf.getRngmnt(), lirrpf.getClntpfx(),
						lirrpf.getClntcoy(), lirrpf.getClntnum() });
	}

	@Override
	public void insert(Lirrpf lirrpf) {
		String sql = "INSERT INTO VM1DTA.LIRRPF (COMPANY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,CLNTPFX,CLNTCOY,CLNTNUM,CURRFROM,CURRTO,VALIDFLAG,TRANNO,RASNUM,RNGMNT,CURRENCY,RAAMOUNT,USRPRF,JOBNM,DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(sql, getLirrpfArrForInsert(lirrpf));
	}

	private Object[] getLirrpfArrForInsert(Lirrpf lirrpf) {
		return new Object[] { lirrpf.getCompany(), lirrpf.getChdrnum(), lirrpf.getLife(), lirrpf.getCoverage(),
				lirrpf.getRider(), lirrpf.getPlanSuffix(), lirrpf.getClntpfx(), lirrpf.getClntcoy(),
				lirrpf.getClntnum(), lirrpf.getCurrfrom(), lirrpf.getCurrto(), lirrpf.getValidflag(),
				lirrpf.getTranno(), lirrpf.getRasnum(), lirrpf.getRngmnt(), lirrpf.getCurrency(), lirrpf.getRaAmount(),
				lirrpf.getUserProfile(), lirrpf.getJobName(), lirrpf.getDatime() };
	}

}