package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Rasapf;

public interface RasapfDAO {
	public Rasapf getRasaRecord(String rascoy, String rasnum);
}
