package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Agcmpf {
    private long uniqueNumber; 
	private String chdrcoy;
	private String chdrnum;
	private String agntnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer planSuffix = new Integer(0);
	private Integer plnsfx;
	private Integer tranno;
	private Integer efdate;
	private BigDecimal annprem;
	private String basicCommMeth;
	private String bascmeth;
	private BigDecimal initcom;
	private String bascpy;
	private BigDecimal compay;
	private BigDecimal comern;
	private String srvcpy;
	private BigDecimal scmdue;
	private BigDecimal scmearn;
	private String rnwcpy;
	private BigDecimal rnlcdue;
	private BigDecimal rnlcearn;
	private String agentClass;
	private String agcls;
	private String termid;
	private Integer transactionDate;
	private Integer transactionTime;
	private Integer user;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private String crtable;
	private Integer currfrom;
	private Integer currto;
	private String validflag;
	private Integer seqno;
	private Integer ptdate;
	private String cedagent;
	private String ovrdcat;
	private String dormantFlag;
	private String userProfile;
	private String jobName;
	private String dormflag;
	private String usrprf;
	private String jobnm;
	private String datime;
	
	public Agcmpf() {
	}
    public Agcmpf(Agcmpf agcmpf) {
		super();
		this.uniqueNumber = agcmpf.uniqueNumber;
		this.chdrcoy = agcmpf.chdrcoy;
		this.chdrnum = agcmpf.chdrnum;
		this.agntnum = agcmpf.agntnum;
		this.life = agcmpf.life;
		this.jlife = agcmpf.jlife;
		this.coverage = agcmpf.coverage;
		this.rider = agcmpf.rider;
		this.plnsfx = agcmpf.plnsfx;
		this.tranno = agcmpf.tranno;
		this.efdate = agcmpf.efdate;
		this.annprem = agcmpf.annprem;
		this.bascmeth = agcmpf.bascmeth;
		this.initcom = agcmpf.initcom;
		this.bascpy = agcmpf.bascpy;
		this.compay = agcmpf.compay;
		this.comern = agcmpf.comern;
		this.srvcpy = agcmpf.srvcpy;
		this.scmdue = agcmpf.scmdue;
		this.scmearn = agcmpf.scmearn;
		this.rnwcpy = agcmpf.rnwcpy;
		this.rnlcdue = agcmpf.rnlcdue;
		this.rnlcearn = agcmpf.rnlcearn;
		this.agcls = agcmpf.agcls;
		this.termid = agcmpf.termid;
		this.trdt = agcmpf.trdt;
		this.trtm = agcmpf.trtm;
		this.userT = agcmpf.userT;
		this.crtable = agcmpf.crtable;
		this.currfrom = agcmpf.currfrom;
		this.currto = agcmpf.currto;
		this.validflag = agcmpf.validflag;
		this.seqno = agcmpf.seqno;
		this.ptdate = agcmpf.ptdate;
		this.cedagent = agcmpf.cedagent;
		this.ovrdcat = agcmpf.ovrdcat;
		this.dormflag = agcmpf.dormflag;
		this.usrprf = agcmpf.usrprf;
		this.jobnm = agcmpf.jobnm;
		this.datime = agcmpf.datime;
	}
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public String getAgntnum() {
        return agntnum;
    }
    public String getLife() {
        return life;
    }
    public String getJlife() {
        return jlife;
    }
    public String getCoverage() {
        return coverage;
    }
    public String getRider() {
        return rider;
    }
    public Integer getPlanSuffix() {
        return planSuffix == null ? new Integer(0) : planSuffix;
    }
    public Integer getTranno() {
        return tranno;
    }
    public Integer getEfdate() {
        return efdate;
    }
    public BigDecimal getAnnprem() {
        return annprem;
    }
    public String getBasicCommMeth() {
        return basicCommMeth;
    }
    public BigDecimal getInitcom() {
        return initcom;
    }
    public String getBascpy() {
        return bascpy;
    }
    public BigDecimal getCompay() {
        return compay;
    }
    public BigDecimal getComern() {
        return comern;
    }
    public String getSrvcpy() {
        return srvcpy;
    }
    public BigDecimal getScmdue() {
        return scmdue;
    }
    public BigDecimal getScmearn() {
        return scmearn;
    }
    public String getRnwcpy() {
        return rnwcpy;
    }
    public BigDecimal getRnlcdue() {
        return rnlcdue;
    }
    public BigDecimal getRnlcearn() {
        return rnlcearn;
    }
    public String getAgentClass() {
        return agentClass;
    }
    public String getTermid() {
        return termid;
    }
    public Integer getTransactionDate() {
        return transactionDate;
    }
    public Integer getTransactionTime() {
        return transactionTime;
    }
    public Integer getUser() {
        return user;
    }
    public String getCrtable() {
        return crtable;
    }
    public Integer getCurrfrom() {
        return currfrom;
    }
    public Integer getCurrto() {
        return currto;
    }
    public String getValidflag() {
        return validflag;
    }
    public Integer getSeqno() {
        return seqno;
    }
    public Integer getPtdate() {
        return ptdate;
    }
    public String getCedagent() {
        return cedagent;
    }
    public String getOvrdcat() {
        return ovrdcat;
    }
    public String getDormantFlag() {
        return dormantFlag;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public void setAgntnum(String agntnum) {
        this.agntnum = agntnum;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public void setPlanSuffix(Integer planSuffix) {
        this.planSuffix = planSuffix;
    }
    public void setTranno(Integer tranno) {
        this.tranno = tranno;
    }
    public void setEfdate(Integer efdate) {
        this.efdate = efdate;
    }
    public void setAnnprem(BigDecimal annprem) {
        this.annprem = annprem;
    }
    public void setBasicCommMeth(String basicCommMeth) {
        this.basicCommMeth = basicCommMeth;
    }
    public void setInitcom(BigDecimal initcom) {
        this.initcom = initcom;
    }
    public void setBascpy(String bascpy) {
        this.bascpy = bascpy;
    }
    public void setCompay(BigDecimal compay) {
        this.compay = compay;
    }
    public void setComern(BigDecimal comern) {
        this.comern = comern;
    }
    public void setSrvcpy(String srvcpy) {
        this.srvcpy = srvcpy;
    }
    public void setScmdue(BigDecimal scmdue) {
        this.scmdue = scmdue;
    }
    public void setScmearn(BigDecimal scmearn) {
        this.scmearn = scmearn;
    }
    public void setRnwcpy(String rnwcpy) {
        this.rnwcpy = rnwcpy;
    }
    public void setRnlcdue(BigDecimal rnlcdue) {
        this.rnlcdue = rnlcdue;
    }
    public void setRnlcearn(BigDecimal rnlcearn) {
        this.rnlcearn = rnlcearn;
    }
    public void setAgentClass(String agentClass) {
        this.agentClass = agentClass;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public void setTransactionDate(Integer transactionDate) {
        this.transactionDate = transactionDate;
    }
    public void setTransactionTime(Integer transactionTime) {
        this.transactionTime = transactionTime;
    }
    public void setUser(Integer user) {
        this.user = user;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public void setCurrfrom(Integer currfrom) {
        this.currfrom = currfrom;
    }
    public void setCurrto(Integer currto) {
        this.currto = currto;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public void setSeqno(Integer seqno) {
        this.seqno = seqno;
    }
    public void setPtdate(Integer ptdate) {
        this.ptdate = ptdate;
    }
    public void setCedagent(String cedagent) {
        this.cedagent = cedagent;
    }
    public void setOvrdcat(String ovrdcat) {
        this.ovrdcat = ovrdcat;
    }
    public void setDormantFlag(String dormantFlag) {
        this.dormantFlag = dormantFlag;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getBascmeth() {
		return bascmeth;
	}
	public void setBascmeth(String bascmeth) {
		this.bascmeth = bascmeth;
	}
	public String getAgcls() {
		return agcls;
	}
	public void setAgcls(String agcls) {
		this.agcls = agcls;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getDormflag() {
		return dormflag;
	}
	public void setDormflag(String dormflag) {
		this.dormflag = dormflag;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
}