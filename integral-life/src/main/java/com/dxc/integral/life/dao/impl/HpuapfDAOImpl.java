package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HpuapfDAO;
import com.dxc.integral.life.dao.model.Hpuapf;

@Repository("hpuapfDAO")
@Lazy
public class HpuapfDAOImpl extends BaseDAOImpl implements HpuapfDAO {
	@Override
	public List<Hpuapf> readHpuaRecord(String chdrcoy,String chdrnum){
		StringBuilder sql = new StringBuilder("select * from hpuapf where chdrcoy=? and chdrnum=? ");
		sql.append("order by chdrcoy asc, chdrnum asc, unique_number desc");
		return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrcoy},
				new BeanPropertyRowMapper<Hpuapf>(Hpuapf.class));
	}
	
	@Override
	public void updateHpuapfRecords(List<Hpuapf> hpuapfList) {
			final List<Hpuapf> tempHpuapfList = hpuapfList;   
		       String sql = "UPDATE HPUAPF SET VALIDFLAG='2',JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
		       jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
		            @Override
		            public int getBatchSize() {  
		                 return tempHpuapfList.size();   
		            }  
		            @Override  
		            public void setValues(PreparedStatement ps, int i)  
		                    throws SQLException { 
		            	ps.setString(1, hpuapfList.get(i).getJobName());
						ps.setString(2, hpuapfList.get(i).getUserProfile());
					    ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
					    ps.setLong(4, hpuapfList.get(i).getUniqueNumber());
		            }   
		      });  
		
	}

	@Override
	public void inertHpuapfRecords(List<Hpuapf> hpuapfList) {
			final List<Hpuapf> tempHpuapfList = hpuapfList;   
		       String sql = "insert into hpuapf (chdrcoy,chdrnum,life,jlife,coverage,rider,plnsfx,hpuanbr,validflag,tranno,anbccd,sumin,singp,crrcd,rstatcode,pstatcode,"
		       		+ "rcesdte,crtable,hdvpart,usrprf,jobnm,datime) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		       jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
		            @Override
		            public int getBatchSize() {  
		                 return tempHpuapfList.size();   
		            }  
		            @Override  
		            public void setValues(PreparedStatement ps, int i)  
		                    throws SQLException { 
		            	ps.setString(1, hpuapfList.get(i).getChdrcoy());
						ps.setString(2, hpuapfList.get(i).getChdrnum());
					    ps.setString(3, hpuapfList.get(i).getLife());
					    ps.setString(4, hpuapfList.get(i).getJLife());
					    ps.setString(5, hpuapfList.get(i).getCoverage());
					    ps.setString(6, hpuapfList.get(i).getRider());	
					    ps.setInt(7, hpuapfList.get(i).getPlanSuffix());	
					    ps.setInt(8, hpuapfList.get(i).getPuAddNbr());	
						ps.setString(9, hpuapfList.get(i).getValidflag());
						ps.setInt(10, hpuapfList.get(i).getTranno());
						ps.setInt(11, hpuapfList.get(i).getAnbAtCcd());
						ps.setInt(12, hpuapfList.get(i).getSumin());
					    ps.setInt(13, hpuapfList.get(i).getSingp()); 	
					    ps.setInt(14, hpuapfList.get(i).getCrrcd());	
					    ps.setString(15, hpuapfList.get(i).getRstatcode()); 
					    ps.setString(16, hpuapfList.get(i).getPstatcode());
					    ps.setInt(17, hpuapfList.get(i).getRiskCessDate());
						ps.setString(18, hpuapfList.get(i).getCrtable());
						ps.setString(19, hpuapfList.get(i).getDivdParticipant());
						ps.setString(20, hpuapfList.get(i).getUserProfile());
						ps.setString(21, hpuapfList.get(i).getJobName());		
						
					    ps.setTimestamp(22, new Timestamp(System.currentTimeMillis()));	
		            }   
		      });  
		
	}
	
	@Override
	public List<Hpuapf> getHpuapfRecords(String coy, String chdrnum, String life, String coverage, String rider, int plnsfx, int tranno){
		StringBuilder sql = new StringBuilder("SELECT * FROM HPUAPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND TRANNO=? ");
		sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, TRANNO ASC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.query(sql.toString(), new Object[] { coy, chdrnum, life, coverage, rider, plnsfx, tranno },
				new BeanPropertyRowMapper<Hpuapf>(Hpuapf.class));
	}
	@Override
	public Hpuapf getHpuaList(String coy, String chdrnum, String life, String coverage, String rider, int plnsfx, int hpuanbr){
		StringBuilder sql = new StringBuilder("SELECT * FROM HPUAPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND HPUANBR=? ");
		sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, HPUANBR ASC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] { coy, chdrnum, life, coverage, rider, plnsfx, hpuanbr },
				new BeanPropertyRowMapper<Hpuapf>(Hpuapf.class));
	}
	@Override
	public int updateHpuapf(Hpuapf hpuapf){
		StringBuilder sql = new StringBuilder("UPDATE HPUAPF set VALIDFLAG=? WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND HPUANBR=? ");
		return jdbcTemplate.update(sql.toString(), new Object[] { hpuapf.getValidflag(), hpuapf.getChdrcoy(), hpuapf.getChdrnum(), hpuapf.getLife(), hpuapf.getCoverage(), 
				hpuapf.getRider(), hpuapf.getPlanSuffix(), hpuapf.getPuAddNbr() },
				new BeanPropertyRowMapper<Hpuapf>(Hpuapf.class));
	}
	@Override
	public int deleteHpuapfRecord(Hpuapf hpuapf) {
		String sql = "DELETE FROM HPUAPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND HPUANBR=? ";
		return jdbcTemplate.update(sql, new Object[] {hpuapf.getChdrcoy(), hpuapf.getChdrnum(), hpuapf.getLife(), hpuapf.getCoverage(), hpuapf.getRider(), 
				hpuapf.getPlanSuffix(), hpuapf.getPuAddNbr()});

	}
}
