/**
 * 
 */
package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.OvrdueDTO;

/**
 * @author fwang3
 *
 */
public interface Nftrad {
	OvrdueDTO execute(OvrdueDTO ovrdueDTO) throws Exception;
}
