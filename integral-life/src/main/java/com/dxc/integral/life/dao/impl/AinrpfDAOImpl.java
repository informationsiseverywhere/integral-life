package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AinrpfDAO;
import com.dxc.integral.life.dao.model.Ainrpf;

@Repository("ainrpfDAO")
public class AinrpfDAOImpl extends BaseDAOImpl implements AinrpfDAO{

	
	@Override
	public void insertAinrpfRecords(List<Ainrpf> ainrpfList) {
		final List<Ainrpf> tempAinrpfList = ainrpfList;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO AINRPF(CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, OLDSUM,NEWSUMI,OLDINST,NEWINST,PLNSFX,CMDATE,RASNUM,RNGMNT,RAAMOUNT,RCESDTE,CURRENCY,AINTYPE, USRPRF, JOBNM, DATIME) "); 
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "); 
		
		 jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {  
			 	@Override
	            public int getBatchSize() {  
	                 return tempAinrpfList.size();  
	            }
			 	@Override  
	            public void setValues(PreparedStatement ps, int i)throws SQLException 
			 	{ 
	            	ps.setString(1, ainrpfList.get(i).getChdrcoy());
					ps.setString(2, ainrpfList.get(i).getChdrnum());
					ps.setString(3, ainrpfList.get(i).getLife());
					ps.setString(4, ainrpfList.get(i).getCoverage());
					ps.setString(5, ainrpfList.get(i).getRider());
					ps.setString(6, ainrpfList.get(i).getCrtable());
					ps.setBigDecimal(7, ainrpfList.get(i).getOldsum());					
					ps.setBigDecimal(8, ainrpfList.get(i).getNewsumi());
					ps.setBigDecimal(9, ainrpfList.get(i).getOldinst());
					ps.setBigDecimal(10, ainrpfList.get(i).getNewinst());
					ps.setInt(11, ainrpfList.get(i).getPlnsfx());
					ps.setInt(12, ainrpfList.get(i).getCmdate());
					ps.setString(13, ainrpfList.get(i).getRasnum());	
					ps.setString(14, ainrpfList.get(i).getRngmnt());					
					ps.setBigDecimal(15, ainrpfList.get(i).getRaamount());
					ps.setInt(16, ainrpfList.get(i).getRcesdte());
					ps.setString(17, ainrpfList.get(i).getCurrency());
					ps.setString(18, ainrpfList.get(i).getAintype());					
					ps.setString(19, ainrpfList.get(i).getUserProfile());
					ps.setString(20, ainrpfList.get(i).getJobNm());
					ps.setString(21, ainrpfList.get(i).getDatime());
			 	}
		 });
	}

	@Override
	public void updateAinrpfRecords(List<Ainrpf> ainrpfList) {
		
	}

}
