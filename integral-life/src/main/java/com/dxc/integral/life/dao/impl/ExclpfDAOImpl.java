package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ExclpfDAO;

@Repository("exclpfDAO")
@Lazy
public class ExclpfDAOImpl extends BaseDAOImpl implements ExclpfDAO {

	@Override
	public boolean isExclExist(String company, String chdrnum, String effdate) {
		String sql = "SELECT COUNT(*) FROM EXCLPF WHERE CHDRCOY=? AND CHDRNUM=? AND EFFDATE<=? ";
		Integer count = jdbcTemplate.queryForObject(sql, new Object[] { company, chdrnum, effdate }, Integer.class);
		return count != null && count > 0;
	}

}
