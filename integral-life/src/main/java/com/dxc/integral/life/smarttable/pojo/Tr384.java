package com.dxc.integral.life.smarttable.pojo;

public class Tr384 {

	private String letterType;

	public String getLetterType() {
		return letterType;
	}

	public void setLetterType(String letterType) {
		this.letterType = letterType;
	}
}