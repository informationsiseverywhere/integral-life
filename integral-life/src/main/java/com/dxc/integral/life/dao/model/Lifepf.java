package com.dxc.integral.life.dao.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
/**
 * @author wli31
 */
public class Lifepf implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8903808297016121410L;
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String validflag;
    private String statcode;
    private Integer tranno;
    private Integer currfrom;
    private Integer currto;
    private String life;
    private String jlife;
    private Integer lcdte;
    private Integer lifeCommDate;
    private String lifcnum;
    private String liferel;
    private Integer cltdob;
    private String cltsex;
    private Integer anbccd = new Integer(0);
    private Integer anbAtCcd = new Integer(0);
    private String ageadm;
    private String selection;
    private String smoking;
    private String occup;
    private String pursuit01;
    private String pursuit02;
    private String martal;
    private String maritalState;
    private String sbstdl;
    private String termid;
    private Integer trdt;
    private Integer trtm;
    private Integer userT;
    private Integer transactionDate;
    private Integer transactionTime;
    private Integer user;
    private String relation;//ICIL-357
    public Lifepf() {
	}
    public Lifepf(Lifepf lifepf){
    	this.chdrcoy=lifepf.chdrcoy;
    	this.chdrnum=lifepf.chdrnum;
    	this.validflag=lifepf.validflag;
    	this.statcode=lifepf.statcode;
    	this.tranno=lifepf.tranno;
    	this.currfrom=lifepf.currfrom;
    	this.currto=lifepf.currto;
    	this.life=lifepf.life;
    	this.jlife=lifepf.jlife;
    	this.lifeCommDate=lifepf.lifeCommDate;
    	this.lifcnum=lifepf.lifcnum;
    	this.liferel=lifepf.liferel;
    	this.cltdob=lifepf.cltdob;
    	this.cltsex=lifepf.cltsex;
    	this.anbAtCcd=lifepf.anbAtCcd;
    	this.ageadm=lifepf.ageadm;
    	this.selection=lifepf.selection;
    	this.smoking=lifepf.smoking;
    	this.occup=lifepf.occup;
    	this.pursuit01=lifepf.pursuit01;
    	this.pursuit02=lifepf.pursuit02;
    	this.maritalState=lifepf.maritalState;
    	this.sbstdl=lifepf.sbstdl;
    	this.termid=lifepf.termid;
    	this.transactionDate=lifepf.transactionDate;
    	this.transactionTime=lifepf.transactionTime;
    	this.relation=lifepf.relation;
    	this.user=lifepf.user;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public String getStatcode() {
        return statcode;
    }

    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }

    public Integer getTranno() {
        return tranno;
    }

    public void setTranno(Integer tranno) {
        this.tranno = tranno;
    }

    public Integer getCurrfrom() {
        return currfrom;
    }

    public void setCurrfrom(Integer currfrom) {
        this.currfrom = currfrom;
    }

    public Integer getCurrto() {
        return currto;
    }

    public void setCurrto(Integer currto) {
        this.currto = currto;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getJlife() {
        return jlife;
    }

    public void setJlife(String jlife) {
        this.jlife = jlife;
    }

    public Integer getLifeCommDate() {
        return lifeCommDate;
    }

    public void setLifeCommDate(Integer lifeCommDate) {
        this.lifeCommDate = lifeCommDate;
    }

    public String getLifcnum() {
        return lifcnum;
    }

    public void setLifcnum(String lifcnum) {
        this.lifcnum = lifcnum;
    }

    public String getLiferel() {
        return liferel;
    }

    public void setLiferel(String liferel) {
        this.liferel = liferel;
    }

    public Integer getCltdob() {
        return cltdob;
    }

    public void setCltdob(Integer cltdob) {
        this.cltdob = cltdob;
    }

    public String getCltsex() {
        return cltsex;
    }

    public void setCltsex(String cltsex) {
        this.cltsex = cltsex;
    }

    public Integer getAnbAtCcd() {
        return anbAtCcd == null ? new Integer(0) : anbccd;
    }

    public void setAnbAtCcd(Integer anbAtCcd) {
        this.anbAtCcd = anbAtCcd;
    }

    public String getAgeadm() {
        return ageadm;
    }

    public void setAgeadm(String ageadm) {
        this.ageadm = ageadm;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getOccup() {
        return occup;
    }

    public void setOccup(String occup) {
        this.occup = occup;
    }

    public String getPursuit01() {
        return pursuit01;
    }

    public void setPursuit01(String pursuit01) {
        this.pursuit01 = pursuit01;
    }

    public String getPursuit02() {
        return pursuit02;
    }

    public void setPursuit02(String pursuit02) {
        this.pursuit02 = pursuit02;
    }

    public String getMaritalState() {
        return maritalState;
    }

    public void setMaritalState(String maritalState) {
        this.maritalState = maritalState;
    }

    public String getSbstdl() {
        return sbstdl;
    }

    public void setSbstdl(String sbstdl) {
        this.sbstdl = sbstdl;
    }

    public String getTermid() {
        return termid;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public Integer getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Integer transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Integer getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Integer transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public Integer getLcdte() {
		return lcdte;
	}
	public void setLcdte(Integer lcdte) {
		this.lcdte = lcdte;
	}
	public Integer getAnbccd() {
		return anbccd == null ? new Integer(0) : anbccd;
	}
	public void setAnbccd(Integer anbccd) {
		this.anbccd = anbccd;
	}
	public String getMartal() {
		return martal;
	}
	public void setMartal(String martal) {
		this.martal = martal;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}