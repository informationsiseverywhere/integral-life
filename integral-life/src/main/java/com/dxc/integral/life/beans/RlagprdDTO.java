package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for Rlagprd Utility.
 * 
 * @author vhukumagrawa
 *
 */
public class RlagprdDTO {

	/** The agntcoy */
	private String agntcoy;
	/** The agntnum */
	private String agntnum;
	/** The actyear */
	private int actyear;
	/** The actmnth */
	private int actmnth;
	/** The agtype */
	private String agtype;
	/** The cnttype */
	private String cnttype;
	/** The occdate */
	private int occdate;
	/** The effdate */
	private int effdate;
	/** The origamt */
	private BigDecimal origamt;
	/** The prdflg */
	private String prdflg;
	/** The chdrcoy */
	private String chdrcoy;
	/** The chdrnum */
	private String chdrnum;
	/** The batctrcde */
	private String batctrcde;

	/**
	 * @return the agntcoy
	 */
	public String getAgntcoy() {
		return agntcoy;
	}

	/**
	 * @param agntcoy
	 *            the agntcoy to set
	 */
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}

	/**
	 * @return the agntnum
	 */
	public String getAgntnum() {
		return agntnum;
	}

	/**
	 * @param agntnum
	 *            the agntnum to set
	 */
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}

	/**
	 * @return the actyear
	 */
	public int getActyear() {
		return actyear;
	}

	/**
	 * @param actyear
	 *            the actyear to set
	 */
	public void setActyear(int actyear) {
		this.actyear = actyear;
	}

	/**
	 * @return the actmnth
	 */
	public int getActmnth() {
		return actmnth;
	}

	/**
	 * @param actmnth
	 *            the actmnth to set
	 */
	public void setActmnth(int actmnth) {
		this.actmnth = actmnth;
	}

	/**
	 * @return the agtype
	 */
	public String getAgtype() {
		return agtype;
	}

	/**
	 * @param agtype
	 *            the agtype to set
	 */
	public void setAgtype(String agtype) {
		this.agtype = agtype;
	}

	/**
	 * @return the cnttype
	 */
	public String getCnttype() {
		return cnttype;
	}

	/**
	 * @param cnttype
	 *            the cnttype to set
	 */
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	/**
	 * @return the occdate
	 */
	public int getOccdate() {
		return occdate;
	}

	/**
	 * @param occdate
	 *            the occdate to set
	 */
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}

	/**
	 * @return the effdate
	 */
	public int getEffdate() {
		return effdate;
	}

	/**
	 * @param effdate
	 *            the effdate to set
	 */
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	/**
	 * @return the origamt
	 */
	public BigDecimal getOrigamt() {
		return origamt;
	}

	/**
	 * @param origamt
	 *            the origamt to set
	 */
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}

	/**
	 * @return the prdflg
	 */
	public String getPrdflg() {
		return prdflg;
	}

	/**
	 * @param prdflg
	 *            the prdflg to set
	 */
	public void setPrdflg(String prdflg) {
		this.prdflg = prdflg;
	}

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * @param chdrcoy
	 *            the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * @param chdrnum
	 *            the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * @return the batctrcde
	 */
	public String getBatctrcde() {
		return batctrcde;
	}

	/**
	 * @param batctrcde
	 *            the batctrcde to set
	 */
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

}
