package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.ComlinkDTO;


public interface Cmpy001 {
	public ComlinkDTO processCmpy001(ComlinkDTO comlinkDTO) throws Exception;
}
