/**
 * 
 */
package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

/**
 * @author fwang3
 *
 */
public class T6661 {
	private String contRevFlag;
	private List<String> subprogs;
	private String trcode;

	public String getContRevFlag() {
		return contRevFlag;
	}

	public void setContRevFlag(String contRevFlag) {
		this.contRevFlag = contRevFlag;
	}

	public List<String> getSubprogs() {
		return subprogs;
	}

	public void setSubprogs(List<String> subprogs) {
		this.subprogs = subprogs;
	}

	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}

}
