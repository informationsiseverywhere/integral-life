package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class Tm603 {

	/** The actives */
	private List<String> actives;
	/** The agtypes */
	private List<String> agtypes;
	/** The mlagttyps */
	private List<String> mlagttyps;

	/**
	 * @return the actives
	 */
	public List<String> getActives() {
		return actives;
	}

	/**
	 * @param actives
	 *            the actives to set
	 */
	public void setActives(List<String> actives) {
		this.actives = actives;
	}

	/**
	 * @return the agtypes
	 */
	public List<String> getAgtypes() {
		return agtypes;
	}

	/**
	 * @param agtypes
	 *            the agtypes to set
	 */
	public void setAgtypes(List<String> agtypes) {
		this.agtypes = agtypes;
	}

	/**
	 * @return the mlagttyps
	 */
	public List<String> getMlagttyps() {
		return mlagttyps;
	}

	/**
	 * @param mlagttyps
	 *            the mlagttyps to set
	 */
	public void setMlagttyps(List<String> mlagttyps) {
		this.mlagttyps = mlagttyps;
	}

}
