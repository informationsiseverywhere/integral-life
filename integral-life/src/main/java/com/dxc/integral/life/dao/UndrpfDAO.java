package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Undrpf;
/** 
 * @author wli31
 *
 */
public interface UndrpfDAO {
	public int insertUndrpfRecord(Undrpf undrpf);
	public int deleteUndrpfRecord(String chdrnum, String chdrcoy);
	public int deleteUndrpfRecord(Undrpf undrpf);
}
