package com.dxc.integral.life.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.life.beans.RnlallDTO;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5539;
import com.dxc.integral.life.smarttable.pojo.T5540;
import com.dxc.integral.life.smarttable.pojo.T5545;
import com.dxc.integral.life.smarttable.pojo.T6646;

@Service("rulGenCollectService")
public class RulGenCollectService {

	@Autowired
	private ItempfService itempfService;
	
	public BigDecimal getEnhancePerc(T5545 t5545IO, BigDecimal wsaaEnhancePerc, BigDecimal currPrem) {
		if (currPrem.compareTo(t5545IO.getEnhanaPrms().get(4))>=0 && t5545IO.getEnhanaPrms().get(4).compareTo(BigDecimal.ZERO)!=0) {
			wsaaEnhancePerc=t5545IO.getEnhanaPcs().get(4);
		}
		else {
			if (currPrem.compareTo(t5545IO.getEnhanaPrms().get(3))>=0 && t5545IO.getEnhanaPrms().get(3).compareTo(BigDecimal.ZERO)!=0) {
				wsaaEnhancePerc=t5545IO.getEnhanaPcs().get(3);
			}
			else {
				if (currPrem.compareTo(t5545IO.getEnhanaPrms().get(2))>=0 && t5545IO.getEnhanaPrms().get(2).compareTo(BigDecimal.ZERO)!=0) {
					wsaaEnhancePerc=t5545IO.getEnhanaPcs().get(2);
				}
				else {
					if (currPrem.compareTo(t5545IO.getEnhanaPrms().get(1))>=0 &&t5545IO.getEnhanaPrms().get(1).compareTo(BigDecimal.ZERO)!=0) {
						wsaaEnhancePerc=t5545IO.getEnhanaPcs().get(1);
					}
					else {
						wsaaEnhancePerc=t5545IO.getEnhanaPcs().get(0);
					}
				}
			}
		}
		return wsaaEnhancePerc;
	}
	

	
	public List<BigDecimal> matchEnhanceBasis(T5545 t5545IO, int wsaaIndex2, BigDecimal wsaaEnhancePerc, BigDecimal currPrem, String billfreq){
		List<BigDecimal> result = new ArrayList<BigDecimal>();
		/* First match the billing frequency.*/
		if (("").equals(t5545IO.getBillfreqs().get(wsaaIndex2>0?wsaaIndex2-1:0).trim())) {
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		if (!billfreq.equals(t5545IO.getBillfreqs().get(wsaaIndex2>0?wsaaIndex2-1:0))) {
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		if (wsaaIndex2 == 1) {
			wsaaEnhancePerc = getEnhancePerc(t5545IO, wsaaEnhancePerc, currPrem);
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		if (wsaaIndex2 == 2) {
			wsaaEnhancePerc = getEnhancePerc(t5545IO, wsaaEnhancePerc, currPrem);
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		if (wsaaIndex2 == 3) {
			wsaaEnhancePerc = getEnhancePerc(t5545IO, wsaaEnhancePerc, currPrem);
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		if (wsaaIndex2 == 4) {
			wsaaEnhancePerc = getEnhancePerc(t5545IO, wsaaEnhancePerc, currPrem);
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		if (wsaaIndex2 == 5) {
			wsaaEnhancePerc = getEnhancePerc(t5545IO, wsaaEnhancePerc, currPrem);
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		if (wsaaIndex2 == 6) {
			wsaaEnhancePerc = getEnhancePerc(t5545IO, wsaaEnhancePerc, currPrem);
			wsaaIndex2++;
			result.add(new BigDecimal(wsaaIndex2));
			result.add(wsaaEnhancePerc);
			return result;
		}
		
		wsaaIndex2++;
		result.add(new BigDecimal(wsaaIndex2));
		result.add(wsaaEnhancePerc);
		return result;

	}

	public List<BigDecimal> initUnitDiscDetails(RnlallDTO rnlallDTO, T5540 t5540IO, int wsaaFact, int fixdtrm) throws IOException{
		BigDecimal wsaaInitUnitDiscFact=BigDecimal.ONE;
		List<BigDecimal> result = new ArrayList<BigDecimal>();
		if(("".equals(t5540IO.getIuDiscBasis().trim())||"".equals(t5540IO.getIuDiscFact().trim()))&&("".equals(t5540IO.getWholeIuDiscFact().trim()))) {
			result.add(new BigDecimal(wsaaFact));
			result.add(wsaaInitUnitDiscFact);
			return result;
		}
		
		if ("".equals(t5540IO.getWholeIuDiscFact().trim())) {
			return wholeOfLife(rnlallDTO, wsaaInitUnitDiscFact, wsaaFact, t5540IO.getWholeIuDiscFact());
		}
		if (fixdtrm!=0) {
			return fixedTerm(rnlallDTO, wsaaInitUnitDiscFact, t5540IO.getIuDiscFact(), wsaaFact, fixdtrm);
		}
		
		if (rnlallDTO.getTermLeftToRun()==0) {
			result.add(new BigDecimal(wsaaFact));
			result.add(wsaaInitUnitDiscFact);
			return result;
		}
		
		T5539 t5539IO = getT5539(rnlallDTO,t5540IO.getIuDiscFact());
		/*    MOVE T5539-DFACT(RNLA-TERM-LEFT-TO-RUN) TO                   */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (rnlallDTO.getTermLeftToRun()<100) {
			wsaaInitUnitDiscFact=new BigDecimal(t5539IO.getDfacts().get(rnlallDTO.getTermLeftToRun()>0?rnlallDTO.getTermLeftToRun()-1:0));
		}
		else {
			wsaaFact = rnlallDTO.getTermLeftToRun()-99;
			wsaaInitUnitDiscFact=new BigDecimal(t5539IO.getFacts().get(wsaaFact>0?wsaaFact:0));
		}
		wsaaInitUnitDiscFact=wsaaInitUnitDiscFact.divide(new BigDecimal(100000)).setScale(1, BigDecimal.ROUND_HALF_UP);
		return fixedTerm(rnlallDTO, wsaaInitUnitDiscFact, t5540IO.getIuDiscFact(), wsaaFact, fixdtrm);	
		
	}
	
	public List<BigDecimal> fixedTerm(RnlallDTO rnlallDTO, BigDecimal wsaaInitUnitDiscFact, String iuDiscFact, int wsaaFact, int fixdtrm) throws IOException{
		/* Use this fixed term to read off T5539 to obtain the*/
		/* discount factor.*/
		List<BigDecimal> result = new ArrayList<BigDecimal>();
		T5539 t5539IO = getT5539(rnlallDTO, iuDiscFact);
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (fixdtrm<100) {
			wsaaInitUnitDiscFact= new BigDecimal(t5539IO.getDfacts().get(fixdtrm>0?fixdtrm-1:0));
		}
		else {
			wsaaFact=fixdtrm-99;
			wsaaInitUnitDiscFact= new BigDecimal(t5539IO.getFacts().get(wsaaFact>0?wsaaFact-1:0));
		}
		wsaaInitUnitDiscFact=wsaaInitUnitDiscFact.divide(new BigDecimal(100000)).setScale(1, BigDecimal.ROUND_HALF_UP);
		  result.add(new BigDecimal(wsaaFact));
		  result.add(wsaaInitUnitDiscFact);
		 return result;
	}

	protected List<BigDecimal> wholeOfLife(RnlallDTO rnlallDTO, BigDecimal wsaaInitUnitDiscFact, int wsaaFact, String wholeIudiscFact) throws IOException{
		/* Use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		List<BigDecimal> result = new ArrayList<BigDecimal>();
		
		if (rnlallDTO.getAnbAtCcd()==0) {
			result.add(new BigDecimal(wsaaFact));
			result.add(wsaaInitUnitDiscFact);
			return result;
		}
		T6646 t6646IO = getT6646(rnlallDTO, wholeIudiscFact);
		if(t6646IO == null) {
			wsaaInitUnitDiscFact = BigDecimal.ZERO;
			 if (rnlallDTO.getAnbAtCcd()>=100) {
				 wsaaFact=rnlallDTO.getAnbAtCcd()-99; 
			 }
		}else {
		
		
		  if (rnlallDTO.getAnbAtCcd()<100) {
			wsaaInitUnitDiscFact=new BigDecimal(t6646IO.getDfacts().get(rnlallDTO.getAnbAtCcd()>0?rnlallDTO.getAnbAtCcd()-1:0));
		  }
		  else {
			wsaaFact=rnlallDTO.getAnbAtCcd()-99;
			wsaaInitUnitDiscFact= new BigDecimal(t6646IO.getDfacts().get(wsaaFact>0?wsaaFact-1:0));
		  }
		}
	       wsaaInitUnitDiscFact= wsaaInitUnitDiscFact.divide(new BigDecimal(100000)).setScale(1, BigDecimal.ROUND_HALF_UP);
	       result.add(new BigDecimal(wsaaFact));
		   result.add(wsaaInitUnitDiscFact);
		 return result;
	}
	
	public T6646 getT6646(RnlallDTO rnlallDTO, String wholeIudiscFact) throws IOException{
		if ("".equals(wholeIudiscFact)) {
			return null;
		}
		TableItem<T6646> t6646 = itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T6646", wholeIudiscFact, CommonConstants.IT_ITEMPFX, T6646.class);
		if(t6646 == null) {
			fatalError("ZrulalocImpl t6646 MRNF".concat(rnlallDTO.getCompany()).concat(wholeIudiscFact));
		}
		return t6646.getItemDetail();
	}
	
	public T5539 getT5539(RnlallDTO rnlallDTO, String iuDiscFact) throws IOException{
		
		if (("").equals(iuDiscFact.trim())) {
			return null;
		}
		
		TableItem<T5539> t5539= itempfService.readSmartTableByTableNameAndItem(rnlallDTO.getCompany(), "T5539", iuDiscFact, CommonConstants.IT_ITEMPFX, T5539.class);
		if(t5539!=null) {
			return t5539.getItemDetail();
		}else {
		   fatalError("ZrulalocImpl T5539 MRNF".concat(rnlallDTO.getCompany()).concat(iuDiscFact));
		}
		return null;
	}

	private void fatalError(String status) {
		throw new StopRunException(status);
	}


	
}
