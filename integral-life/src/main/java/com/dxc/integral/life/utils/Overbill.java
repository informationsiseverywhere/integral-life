/**
 * 
 */
package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.OverbillDTO;

/**
 * @author fwang3
 *
 */
public interface Overbill {
	void execute(OverbillDTO overbillDTO) throws IOException, ParseException;
}
