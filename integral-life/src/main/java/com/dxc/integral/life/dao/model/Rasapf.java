package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Rasapf {
	
	private long uniqueNumber;
    private String raspfx;
    private String rascoy;
    private String rasnum;
    private String validflag;
    private String clntcoy;
    private String clntnum;
    private String payclt;
    private String rapaymth;
    private String rapayfrq;
    private String facthous;
    private String bankkey;
    private String bankacckey;
    private String currcode;
    private String dtepay;
    private String agntnum;
    private BigDecimal minsta;
    private Integer trdt;
    private Integer trtm;
    private Integer user_t;
    private Integer commdt;
    private Integer termdt;
    private String reasst;
    private Integer stmtdt;
    private Integer nxtpay;
    private String usrprf;
    private String jobnm;
    private Date datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getRaspfx() {
		return raspfx;
	}
	public void setRaspfx(String raspfx) {
		this.raspfx = raspfx;
	}
	public String getRascoy() {
		return rascoy;
	}
	public void setRascoy(String rascoy) {
		this.rascoy = rascoy;
	}
	public String getRasnum() {
		return rasnum;
	}
	public void setRasnum(String rasnum) {
		this.rasnum = rasnum;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	public String getRapaymth() {
		return rapaymth;
	}
	public void setRapaymth(String rapaymth) {
		this.rapaymth = rapaymth;
	}
	public String getRapayfrq() {
		return rapayfrq;
	}
	public void setRapayfrq(String rapayfrq) {
		this.rapayfrq = rapayfrq;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getDtepay() {
		return dtepay;
	}
	public void setDtepay(String dtepay) {
		this.dtepay = dtepay;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public BigDecimal getMinsta() {
		return minsta;
	}
	public void setMinsta(BigDecimal minsta) {
		this.minsta = minsta;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUser_t() {
		return user_t;
	}
	public void setUser_t(Integer user_t) {
		this.user_t = user_t;
	}
	public Integer getCommdt() {
		return commdt;
	}
	public void setCommdt(Integer commdt) {
		this.commdt = commdt;
	}
	public Integer getTermdt() {
		return termdt;
	}
	public void setTermdt(Integer termdt) {
		this.termdt = termdt;
	}
	public String getReasst() {
		return reasst;
	}
	public void setReasst(String reasst) {
		this.reasst = reasst;
	}
	public Integer getStmtdt() {
		return stmtdt;
	}
	public void setStmtdt(Integer stmtdt) {
		this.stmtdt = stmtdt;
	}
	public Integer getNxtpay() {
		return nxtpay;
	}
	public void setNxtpay(Integer nxtpay) {
		this.nxtpay = nxtpay;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}
    
}
