package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.pojo.T1693;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.AgecalcDTO;
import com.dxc.integral.life.beans.IncrsumDTO;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.beans.VpxacblDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.beans.VpxlextDTO;
import com.dxc.integral.life.dao.AnnypfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Annypf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.exceptions.SubroutineIOException;
import com.dxc.integral.life.smarttable.pojo.T5648;
import com.dxc.integral.life.smarttable.pojo.T5675;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6658;
import com.dxc.integral.life.utils.Agecalc;
import com.dxc.integral.life.utils.Incrsum;
import com.dxc.integral.life.utils.Premium;
import com.dxc.integral.life.utils.VPMScaller;
import com.dxc.integral.life.utils.Vpxacbl;
import com.dxc.integral.life.utils.Vpxchdr;
import com.dxc.integral.life.utils.Vpxlext;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author lbarde
 *
 */
@Service("incrsum")
@Lazy
public class IncrsumImpl implements Incrsum {

	@Autowired
	private ItempfDAO itemDAO;

	@Autowired
	Agecalc ageCalc;

	@Autowired
	private AnnypfDAO annypfDAO;

	@Autowired
	private ChdrpfDAO chdrDAO;

	@Autowired
	private LifepfDAO lifeDAO;

	@Autowired
	private Environment env;

	@Autowired
	private VPMScaller vpmsCaller;

	@Autowired
	private Datcon3 datCon3;

	@Autowired
	private Premium premium;

	@Autowired
	private Vpxlext vpxLext;

	@Autowired
	private Vpxacbl vpxAcbl;

	@Autowired
	private Vpxchdr vpxChdr;

	private T5648 t5648;
	private T6658 t6658;
	private T5687 t5687;
	private T1693 t1693;
	private T5675 t5675;

	private Chdrpf chdr;

	private static final String STATUS_OK = "****";

	private BigDecimal increaseVal = BigDecimal.ZERO;
	private int anbValue;

	@Override
	public void calcIncrPrem(IncrsumDTO incrsumDTO) throws IOException, ParseException {

		readT5648(incrsumDTO);
		readT6658(incrsumDTO);
		readT5687(incrsumDTO);

		if (!t6658.getSimpleInd().trim().isEmpty()) {
			callIncrPrem1(incrsumDTO);
		} else {
			if (!t6658.getCompoundInd().trim().isEmpty()) {
				callIncrPrem2(incrsumDTO);
			}
		}
		recalcPrem(incrsumDTO);

	}

	public void callIncrPrem1(IncrsumDTO incrsumDTO) {

		increaseVal = (incrsumDTO.getOrigsum().multiply(incrsumDTO.getPctinc())).divide(BigDecimal.valueOf(100));
		incrsumDTO.setNewsum(incrsumDTO.getCurrsum().add(increaseVal));
	}

	public void callIncrPrem2(IncrsumDTO incrsumDTO) {
		increaseVal = (incrsumDTO.getCurrsum().multiply(incrsumDTO.getPctinc())).divide(BigDecimal.valueOf(100));
		incrsumDTO.setNewsum(incrsumDTO.getCurrsum().add(increaseVal));
	}

	public void recalcPrem(IncrsumDTO incrsumDTO)
			throws JsonParseException, JsonMappingException, NumberFormatException, IOException, ParseException {
		PremiumDTO premiumDTO = new PremiumDTO();
		premiumDTO.setFunction("INCR");
		premiumDTO.setChdrChdrcoy(incrsumDTO.getChdrcoy());
		premiumDTO.setChdrChdrnum(incrsumDTO.getChdrnum());
		premiumDTO.setLifeLife(incrsumDTO.getLife());
		premiumDTO.setLifeJlife("00");
		premiumDTO.setCovrCoverage(incrsumDTO.getCoverage());
		premiumDTO.setCovrRider(incrsumDTO.getRider());
		premiumDTO.setCrtable(incrsumDTO.getCrtable());
		premiumDTO.setEffectdt(incrsumDTO.getEffdate());
		premiumDTO.setTermdate(incrsumDTO.getRcesdte());
		premiumDTO.setLanguage(incrsumDTO.getLanguage());
		getLifeDetails(incrsumDTO, premiumDTO);

		double freqFactor = datCon3.getYearsDifference(incrsumDTO.getEffdate(), premiumDTO.getTermdate());
		premiumDTO.setDuration((int) (freqFactor + 0.99999));
		premiumDTO.setCurrcode(incrsumDTO.getCntcurr());
		premiumDTO.setSumin(increaseVal);
		premiumDTO.setMortcls(incrsumDTO.getMortcls());
		premiumDTO.setBillfreq(incrsumDTO.getBillfreq());
		premiumDTO.setMop(incrsumDTO.getMop());
		premiumDTO.setRatingdate(incrsumDTO.getOccdate());
		premiumDTO.setReRateDate(incrsumDTO.getOccdate());
		premiumDTO.setCalcPrem(new BigDecimal(0));
		premiumDTO.setCalcBasPrem(new BigDecimal(0));
		premiumDTO.setCalcLoaPrem(new BigDecimal(0));

		if (t5675.getPremsubr() == null || t5675.getPremsubr().trim().isEmpty()) {
			incrsumDTO.setNewinst01(incrsumDTO.getCurrinst01());
			return;
		}
		getAnnyDetails(incrsumDTO, premiumDTO);

		VpxacblDTO vpxacblDTO = new VpxacblDTO();
		VpxlextDTO vpxlextDTO = new VpxlextDTO();
		VpxchdrDTO vpxchdrDTO = new VpxchdrDTO();
		VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
		vpmsinfoDTO.setModelName(t5675.getPremsubr());

		if (!(checkVPMSFlag()) && (vpmsCaller.checkVPMSModel(t5675.getPremsubr().trim()))) {
			premiumDTO = callSubroutine(t5675.getPremsubr(), premiumDTO, vpmsinfoDTO, null, null);
		} else {

			vpxlextDTO.setFunction("INIT");
			vpxlextDTO = vpxLext.processVpxlext(vpxlextDTO.getFunction(), premiumDTO);
			vpxchdrDTO.setFunction("INIT");
			vpxchdrDTO = vpxChdr.callInit(premiumDTO);
			premiumDTO.setRstaflag(vpxchdrDTO.getRstaflag());
			premiumDTO.setCnttype(chdr.getCnttype());

			vpxacblDTO = vpxAcbl.processVpxacbl(premiumDTO);
			premiumDTO = callSubroutine(t5675.getPremsubr(), premiumDTO, vpmsinfoDTO, vpxlextDTO, vpxacblDTO);

		}

		if (!premiumDTO.getStatuz().equals(STATUS_OK)) {
			fatalError(premiumDTO.getStatuz());
		}
		incrsumDTO.setNewinst01(premiumDTO.getCalcPrem().add(incrsumDTO.getCurrinst01()));
		incrsumDTO.setNewinst02(premiumDTO.getCalcBasPrem().add(incrsumDTO.getCurrinst02()));
		incrsumDTO.setNewinst03(premiumDTO.getCalcLoaPrem().add(incrsumDTO.getCurrinst03()));
	}

	public void getAnnyDetails(IncrsumDTO incrsumDTO, PremiumDTO premiumDTO) {
		Annypf annypf = new Annypf();
		annypf.setChdrcoy(incrsumDTO.getChdrcoy());
		annypf.setChdrnum(incrsumDTO.getChdrnum());
		annypf.setLife(incrsumDTO.getLife());
		annypf.setCoverage(incrsumDTO.getCoverage());
		annypf.setRider(incrsumDTO.getRider());
		annypf.setPlnsfx(Integer.parseInt(incrsumDTO.getPlnsfx()));

		List<Annypf> annypfList = annypfDAO.getAnnypfRecord(annypf);
		if (!annypfList.isEmpty()) {
			for (Annypf annyData : annypfList) {
				premiumDTO.setFreqann(annyData.getFreqann());
				premiumDTO.setArrears(annyData.getArrears());
				premiumDTO.setAdvance(annyData.getAdvance());
				premiumDTO.setGuarperd(annyData.getGuarperd());
				premiumDTO.setIntanny(annyData.getIntanny());
				premiumDTO.setCapcont(annyData.getCapcont());
				premiumDTO.setWithprop(annyData.getWithprop());
				premiumDTO.setWithoprop(annyData.getWithoprop());
				premiumDTO.setPpind(annyData.getPpind());
				premiumDTO.setNomlife(annyData.getNomlife());
				premiumDTO.setDthpercn(annyData.getDthpercn());
				premiumDTO.setDthperco(annyData.getDthperco());
			}
		} else {
			premiumDTO.setAdvance(" ");
			premiumDTO.setArrears(" ");
			premiumDTO.setFreqann(" ");
			premiumDTO.setWithprop(" ");
			premiumDTO.setWithoprop(" ");
			premiumDTO.setPpind(" ");
			premiumDTO.setNomlife(" ");
			premiumDTO.setGuarperd(0);
			premiumDTO.setIntanny(BigDecimal.valueOf((0)));
			premiumDTO.setCapcont(BigDecimal.valueOf((0)));
			premiumDTO.setDthpercn(BigDecimal.valueOf((0)));
			premiumDTO.setDthperco(BigDecimal.valueOf((0)));
		}

	}

	public PremiumDTO callSubroutine(String premSubr, PremiumDTO premiumDTO, VPMSinfoDTO vpmsinfoDTO,
			VpxlextDTO vpxlextDTO, VpxacblDTO vpxacblDTO) throws ParseException {

		if (!checkVPMSFlag()) {
			if ("PRMPM04".equals(premSubr)) {
				try {
					premiumDTO = premium.processPrmpm04(premiumDTO);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm04: " + e.getMessage());
				}
			} else if ("PRMPM03".equals(premSubr)) {
				try {
					premiumDTO = premium.processPrmpm03(premiumDTO);
				} catch (IOException | ParseException e) {
					throw new SubroutineIOException("IOException in Prmpm03: " + e.getMessage());
				}
			} else if ("PRMPM17".equals(premSubr)) {
				try {
					premiumDTO = premium.processPrmpm17(premiumDTO);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm18: " + e.getMessage());
				}
			} else if ("PRMPM01".equals(premSubr)) {
				try {
					premiumDTO = premium.processPrmpm01(premiumDTO);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm01: " + e.getMessage());
				}
			} else if ("PRMPM18".equals(premSubr)) {
				try {
					premiumDTO = premium.processPrmpm18(premiumDTO);
				} catch (IOException e) {
					throw new SubroutineIOException("IOException in Prmpm18: " + e.getMessage());
				}
			}
		} else {
			premiumDTO.setPremMethod(premSubr);
			vpmsCaller.callPRMPMREST(vpmsinfoDTO, premiumDTO, vpxacblDTO, vpxlextDTO);
		}
		return premiumDTO;
	}

	public void getLifeDetails(IncrsumDTO incrsumDTO, PremiumDTO premiumDTO)
			throws JsonParseException, JsonMappingException, NumberFormatException, IOException, ParseException {
		Lifepf lifepf = new Lifepf();
		lifepf.setChdrcoy(incrsumDTO.getChdrcoy());
		lifepf.setChdrnum(incrsumDTO.getChdrnum());
		lifepf.setLife(incrsumDTO.getLife());
		lifepf.setJlife("00");
		lifepf = lifeDAO.getValidLifeRecord(lifepf.getChdrcoy(), lifepf.getChdrnum(), lifepf.getLife(),
				lifepf.getJlife());
		calculateAnb(lifepf, incrsumDTO);
		premiumDTO.setLage(anbValue);
		premiumDTO.setLsex(lifepf.getCltsex());
 
		lifepf = new Lifepf();
		lifepf.setChdrcoy(incrsumDTO.getChdrcoy());
		lifepf.setChdrnum(incrsumDTO.getChdrnum());
		lifepf.setLife(incrsumDTO.getLife());
		lifepf.setJlife("01");
		List<Lifepf> lifepfList = lifeDAO.getLifeRecord(lifepf.getChdrcoy(), lifepf.getChdrnum(), lifepf.getLife(),
				lifepf.getJlife());
		if (lifepfList != null && !lifepfList.isEmpty()) {
			for (Lifepf life : lifepfList) {
				if ("1".equals(life.getValidflag())) {
					lifepf = life;
					calculateAnb(lifepf, incrsumDTO);
					premiumDTO.setJlage(anbValue);
					premiumDTO.setJlsex(lifepf.getCltsex());
					break;
				}
			}
		} else {
			premiumDTO.setJlage(0);
			premiumDTO.setJlsex(" ");
		}

		// get Itempf details for table T5675
		Itempf itempf = new Itempf();
		itempf.setItemcoy(incrsumDTO.getChdrcoy());
		itempf.setItemtabl("T5675");
		itempf.setItempfx("IT");

		if (lifepf != null) {
			itempf.setItemitem(t5687.getPremmeth());
		} else {
			itempf.setItemitem(t5687.getJlPremMeth());
		}
		List<Itempf> itemList = itemDAO.readItdmByTablItem(itempf.getItemcoy(), itempf.getItemtabl(),
				itempf.getItemitem(), itempf.getItempfx());
		if (null != itemList && !itemList.isEmpty()) {
			Iterator<Itempf> iterator = itemList.iterator();
			while (iterator.hasNext()) {
				Itempf item = iterator.next();
				if (checkVPMSFlag()) {
					premiumDTO.setPremMethod(item.getItemitem());
				}
				t5675 = new ObjectMapper().readValue(item.getGenareaj(), T5675.class);
			}
		}

	}

	private boolean checkVPMSFlag() {
		return env.getProperty("vpms.flag").equals("1");
	}

	public void calculateAnb(Lifepf lifepf, IncrsumDTO incrsumDTO)
			throws JsonParseException, JsonMappingException, IOException, NumberFormatException, ParseException {
		chdr = new Chdrpf();
		chdr.setChdrcoy(incrsumDTO.getChdrcoy());
		chdr.setChdrnum(incrsumDTO.getChdrnum());
		chdr = chdrDAO.readRecordOfChdrpf(chdr.getChdrcoy(), chdr.getChdrnum());

		// Fetch Item details from table T1693
		Itempf item = new Itempf();
		item.setItemcoy("0");
		item.setItempfx("IT");
		item.setItemtabl("T1693");
		item.setItemitem(incrsumDTO.getChdrcoy());
		item = itemDAO.readSmartTableByTableNameAndItem(item.getItemcoy(), item.getItemtabl(), item.getItemitem(),
				item.getItempfx());

		if (item != null) {
			t1693 = new ObjectMapper().readValue(item.getGenareaj(), T1693.class);
		}
		AgecalcDTO ageCalcDTO = new AgecalcDTO();
		ageCalcDTO.setFunction("CALCP");
		ageCalcDTO.setCnttype(chdr.getCnttype());
		ageCalcDTO.setIntDate1(lifepf.getCltdob().toString());
		ageCalcDTO.setIntDate2(String.valueOf(incrsumDTO.getEffdate()));
		ageCalcDTO.setCompany(t1693.getFsuco());
		ageCalcDTO.setLanguage(incrsumDTO.getLanguage());

		try {
			ageCalcDTO = ageCalc.processAgecalc(ageCalcDTO);
		} catch (IOException e) {
			throw new SubroutineIOException("L2autoincr: IOException in agecalc: " + e.getMessage());
		}
		anbValue = ageCalcDTO.getAgerating();
	}

	public void fatalError(String status) {
		throw new StopRunException(status);
	}

	public void readT5648(IncrsumDTO incrsumDTO) throws JsonParseException, JsonMappingException, IOException {

		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(incrsumDTO.getChdrcoy());
		itempf.setItemitem(incrsumDTO.getAnnvmeth());
		itempf.setItemtabl("T5648");
		itempf.setItmfrm(incrsumDTO.getEffdate());

		itempf = itemDAO.readSmartTableByTableNameAndItem3(itempf.getItemcoy(), itempf.getItemtabl(),
				itempf.getItemitem(), itempf.getItempfx(), itempf.getItmfrm());
		if (itempf != null) {
			t5648 = new ObjectMapper().readValue(itempf.getGenareaj(), T5648.class);
			if (t5648.getPctinc().compareTo(incrsumDTO.getMaxpcnt()) > 0) {
				incrsumDTO.setPctinc(incrsumDTO.getMaxpcnt());
			} else {
				incrsumDTO.setPctinc(t5648.getPctinc());
			}
			if (t5648.getPctinc().compareTo(incrsumDTO.getMinpcnt()) < 0) {
				incrsumDTO.setPctinc(BigDecimal.ZERO);
			}
		} else {
			incrsumDTO.setStatuz("H065");
		}

	}

	public void readT6658(IncrsumDTO incrsumDTO) throws JsonParseException, JsonMappingException, IOException {

		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(incrsumDTO.getChdrcoy());
		itempf.setItemitem(incrsumDTO.getAnnvmeth());
		itempf.setItemtabl("T6658");
		itempf.setItmfrm(incrsumDTO.getEffdate());

		itempf = itemDAO.readSmartTableByTableNameAndItem3(itempf.getItemcoy(), itempf.getItemtabl(),
				itempf.getItemitem(), itempf.getItempfx(), itempf.getItmfrm());
		if (itempf != null) {
			t6658 = new ObjectMapper().readValue(itempf.getGenareaj(), T6658.class);
		} else {
			incrsumDTO.setStatuz("H036");
		}
	}

	public void readT5687(IncrsumDTO incrsumDTO) throws JsonParseException, JsonMappingException, IOException {
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(incrsumDTO.getChdrcoy());
		itempf.setItemitem(incrsumDTO.getCrtable());
		itempf.setItemtabl("T5687");
		itempf.setItmfrm(incrsumDTO.getEffdate());
		itempf = itemDAO.readSmartTableByTableNameAndItem3(itempf.getItemcoy(), itempf.getItemtabl(),
				itempf.getItemitem(), itempf.getItempfx(), itempf.getItmfrm());
		if (itempf != null) {
			t5687 = new ObjectMapper().readValue(itempf.getGenareaj(), T5687.class);
		} else {
			incrsumDTO.setStatuz("E652");
		}

	}

}
