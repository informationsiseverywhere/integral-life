package com.dxc.integral.life.utils.impl;



import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.utils.Datcon4;
/**
 * @author wli31
 */
@Service("datcon4")
@Lazy
public class Datcon4Impl implements Datcon4 {
	private static final String FREQUENCY_YEARLY = "01";
	private static final String FREQUENCY_HALF_YEARLY = "02";
	private static final String FREQUENCY_HALF_QUARTERLY = "04";
	private static final String FREQUENCY_MONTHLY = "12";	
	private static final String FREQUENCY_DAILY = "DY";
	private static final String FREQUENCY_4WEEKLY = "13";
	private static final String FREQUENCY_WEEKLY = "52";
	@Override
	public Integer getIntDate2(String frequency, String intDate1, int freqFactor) {
		LocalDate checkInDate = LocalDate.parse(intDate1, DateTimeFormatter.BASIC_ISO_DATE);
		if(frequency.equals(FREQUENCY_YEARLY)){
			String intDate2 = checkInDate.plusYears(freqFactor).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}else if(frequency.equals(FREQUENCY_HALF_YEARLY)){
			String intDate2 = checkInDate.plusMonths(freqFactor * 6).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}else if(frequency.equals(FREQUENCY_HALF_QUARTERLY)){
			String intDate2 = checkInDate.plusMonths(freqFactor * 3).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}else if(frequency.equals(FREQUENCY_MONTHLY)){
			String intDate2 = checkInDate.plusMonths(freqFactor).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}else if(frequency.equals(FREQUENCY_DAILY)){
			String intDate2 = checkInDate.plusDays(freqFactor).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}else if(frequency.equals(FREQUENCY_4WEEKLY)){
			String intDate2 = checkInDate.plusWeeks(freqFactor * 4).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}else if(frequency.equals(FREQUENCY_WEEKLY)){
			String intDate2 = checkInDate.plusWeeks(freqFactor).format(DateTimeFormatter.BASIC_ISO_DATE);
			return Integer.parseInt(intDate2);
		}
		return 0;
	}

}
