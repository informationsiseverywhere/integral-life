package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for Intcalc utility.
 * 
 * @author mmalik25
 */
public class IntcalcDTO {

	/** The loanNumber */
	private Integer loanNumber;
	/** The chdrcoy */
	private String chdrcoy;
	/** The chdrnum */
	private String chdrnum;
	/** The cnttype */
	private String cnttype;
	/** The interestFrom */
	private Integer interestFrom;
	/** The interestTo */
	private Integer interestTo;
	/** The loanorigam */
	private BigDecimal loanorigam;
	/** The lastCaplsnDate */
	private Integer lastCaplsnDate;
	/** The loanStartDate */
	private Integer loanStartDate;
	/** The loanCurrency */
	private String loanCurrency;
	/** The loanType */
	private String loanType;
	private BigDecimal interestAmount;

	/**
	 * @return the loanNumber
	 */
	public Integer getLoanNumber() {
		return loanNumber;
	}

	/**
	 * @param loanNumber
	 *            the loanNumber to set
	 */
	public void setLoanNumber(Integer loanNumber) {
		this.loanNumber = loanNumber;
	}

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * @param chdrcoy
	 *            the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * @param chdrnum
	 *            the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * @return the cnttype
	 */
	public String getCnttype() {
		return cnttype;
	}

	/**
	 * @param cnttype
	 *            the cnttype to set
	 */
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	/**
	 * @return the interestFrom
	 */
	public Integer getInterestFrom() {
		return interestFrom;
	}

	/**
	 * @param interestFrom
	 *            the interestFrom to set
	 */
	public void setInterestFrom(Integer interestFrom) {
		this.interestFrom = interestFrom;
	}

	/**
	 * @return the interestTo
	 */
	public Integer getInterestTo() {
		return interestTo;
	}

	/**
	 * @param interestTo
	 *            the interestTo to set
	 */
	public void setInterestTo(Integer interestTo) {
		this.interestTo = interestTo;
	}

	/**
	 * @return the loanorigam
	 */
	public BigDecimal getLoanorigam() {
		return loanorigam;
	}

	/**
	 * @param loanorigam
	 *            the loanorigam to set
	 */
	public void setLoanorigam(BigDecimal loanorigam) {
		this.loanorigam = loanorigam;
	}

	/**
	 * @return the lastCaplsnDate
	 */
	public Integer getLastCaplsnDate() {
		return lastCaplsnDate;
	}

	/**
	 * @param lastCaplsnDate
	 *            the lastCaplsnDate to set
	 */
	public void setLastCaplsnDate(Integer lastCaplsnDate) {
		this.lastCaplsnDate = lastCaplsnDate;
	}

	/**
	 * @return the loanStartDate
	 */
	public Integer getLoanStartDate() {
		return loanStartDate;
	}

	/**
	 * @param loanStartDate
	 *            the loanStartDate to set
	 */
	public void setLoanStartDate(Integer loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	/**
	 * @return the loanCurrency
	 */
	public String getLoanCurrency() {
		return loanCurrency;
	}

	/**
	 * @param loanCurrency
	 *            the loanCurrency to set
	 */
	public void setLoanCurrency(String loanCurrency) {
		this.loanCurrency = loanCurrency;
	}

	/**
	 * @return the loanType
	 */
	public String getLoanType() {
		return loanType;
	}

	/**
	 * @param loanType
	 *            the loanType to set
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
		
	}
	
	public BigDecimal getInterestAmount() {
		return interestAmount;
	}
	
	
}
