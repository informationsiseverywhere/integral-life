package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.beans.NfloanDTO;
import com.dxc.integral.life.beans.TotloanInputDTO;
import com.dxc.integral.life.beans.TotloanOutputDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.smarttable.pojo.Srcalcpy;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5687;
import com.dxc.integral.life.smarttable.pojo.T6598;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.life.utils.Hrtotlon;
import com.dxc.integral.life.utils.Nfloan;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * The NFLoan utility implementation.
 * 
 * @author dpuhawan
 *
 */
@Service("nfloan")
@Lazy
public class NfloanImpl implements Nfloan {

	
	@Autowired
	private ItempfDAO itempfDAO;
	
	
	@Autowired
	private CovrpfDAO covrpfDAO;	
	
	@Autowired
	private Zrdecplc zrdecplc;	
	
	@Autowired
	private Hrtotlon hrtotlon;	
	
	private Map<String, List<Itempf>> t6633ListMap = new HashMap<String, List<Itempf>>();
	private List<T6633> t6633IOList = new ArrayList<T6633>();	
	private T6633 t6633IO = null;
	
	private Map<String, List<Itempf>> t5679ListMap = new HashMap<String, List<Itempf>>();
	private List<T5679> t5679IOList = new ArrayList<T5679>();	
	private T5679 t5679IO = null;
	
	private Map<String, List<Itempf>> t5687ListMap = new HashMap<String, List<Itempf>>();
	private List<T5687> t5687IOList = new ArrayList<T5687>();	
	private T5687 t5687IO = null;
	
	private Map<String, List<Itempf>> t6598ListMap = new HashMap<String, List<Itempf>>();
	private List<T6598> t6598IOList = new ArrayList<T6598>();	
	private T6598 t6598IO = null;	
	
	private Covrpf covrpf = null;
	
	String yesToLoan = "";
	String life = "";
	Integer polSumCount = 0;
	Srcalcpy srcalcpy;
	String premStatus = "";
	String riskStatus = "";
	BigDecimal surrValTotal = BigDecimal.ZERO;
	BigDecimal totalCash = BigDecimal.ZERO;
	BigDecimal heldCurrLoans = BigDecimal.ZERO;
	BigDecimal totalDebt = BigDecimal.ZERO;
	BigDecimal heldSurrVal = BigDecimal.ZERO;


	public NfloanDTO processNFLoan(NfloanDTO nfloanDTO) throws IOException,ParseException {
		NfloanDTO newNFLoanDTO = nfloanDTO;
		
		newNFLoanDTO = initialise(nfloanDTO);
		if (!"Y".equals(yesToLoan)) return null;
		getSurrValues(nfloanDTO);
		sumLoans(nfloanDTO);
		surrvalLoansumCalc(nfloanDTO);
		return newNFLoanDTO;
	}
	
	private NfloanDTO initialise(NfloanDTO nfloanDTO) throws IOException,ParseException{
		NfloanDTO newNFLoanDTO = nfloanDTO;
		
		newNFLoanDTO.setStatuz("OK");
		
		getT6633Info(nfloanDTO);
		
		if (!t6633IOList.isEmpty()) {
			yesToLoan = "N";
			newNFLoanDTO.setStatuz("NAPL");
		}else {
			yesToLoan = "Y";
		}
		
		getT5679Info(nfloanDTO);	
		
		return newNFLoanDTO;
	}
	
	private void getSurrValues(NfloanDTO nfloanDTO) throws IOException {
		List<Covrpf> list = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(nfloanDTO.getChdrcoy(), nfloanDTO.getChdrnum());
		
		if (!(list == null) && !list.isEmpty()) {
			Iterator<Covrpf> iterator = list.iterator();
			boolean first = true;
			while (iterator.hasNext()) {
				covrpf = iterator.next();
				if(first){
					life= covrpf.getLife();
					first = false;
				}
				if(life.equalsIgnoreCase(covrpf.getLife())){
					break;
				}
				processComponents(nfloanDTO);
			}		
			heldSurrVal = surrValTotal;
		}
	}
	
	private void processComponents(NfloanDTO nfloanDTO) throws IOException {
		getSurrenderValueMethod(nfloanDTO);
		
		srcalcpy = new Srcalcpy();
		srcalcpy.setTsvtot(BigDecimal.ZERO);
		srcalcpy.setTsv1tot(BigDecimal.ZERO);
		srcalcpy.setChdrChdrcoy(covrpf.getChdrcoy());
		srcalcpy.setChdrChdrnum(covrpf.getChdrnum());
		srcalcpy.setPlanSuffix(covrpf.getPlnsfx() == null ? covrpf.getPlanSuffix() : covrpf.getPlnsfx());
		srcalcpy.setPolsum(nfloanDTO.getPolsum());
		srcalcpy.setLifeLife(covrpf.getLife());
		srcalcpy.setLifeJlife(covrpf.getJlife());
		srcalcpy.setCovrCoverage(covrpf.getCoverage());
		srcalcpy.setCovrRider(covrpf.getRider());
		srcalcpy.setCrtable(covrpf.getCrtable());
		srcalcpy.setCrrcd(covrpf.getCrrcd());
		srcalcpy.setPtdate(nfloanDTO.getPtdate());
		srcalcpy.setEffdate(nfloanDTO.getEffdate());
		srcalcpy.setConvUnits(covrpf.getCbcvin() == null ? covrpf.getConvertInitialUnits() : covrpf.getCbcvin());
		srcalcpy.setLanguage(nfloanDTO.getLanguage());
		srcalcpy.setCurrcode(covrpf.getPremCurrency() == null ? covrpf.getPrmcur() : covrpf.getPremCurrency());
		srcalcpy.setChdrCurr(nfloanDTO.getCntcurr());
		srcalcpy.setPstatcode(covrpf.getPstatcode());

		if (covrpf.getInstprem().compareTo(BigDecimal.ZERO) > 0  ) {
			srcalcpy.setSingp(covrpf.getInstprem());
		}
		else {
			srcalcpy.setSingp(covrpf.getSingp());
		}
		srcalcpy.setBillfreq(nfloanDTO.getBillfreq());
		srcalcpy.setStatus("");	
		
		if ((covrpf.getPlanSuffix() == 0)
		&& !(nfloanDTO.getPolsum().equals(nfloanDTO.getPolinc()))
		&& !(nfloanDTO.getPolinc() == 1)) {
			for (polSumCount=1; !(polSumCount.compareTo(nfloanDTO.getPolsum()) > 0); polSumCount++){
				sumpolSvalCalc2150(nfloanDTO);
			}
		}
		else {
			while (!"ENDP".equalsIgnoreCase(srcalcpy.getStatus())) {
				calculateSurrValue(nfloanDTO);
			}	
		}			
	}	
	
	private void sumpolSvalCalc2150(NfloanDTO nfloanDTO) throws IOException{
		/* Calculate the surrender value of a summarised policy in a*/
		/*  part broken out plan*/
		srcalcpy.setPlanSuffix(polSumCount);
		srcalcpy.setStatus("OK");
		while (!"ENDP".equalsIgnoreCase(srcalcpy.getStatus())) {
			calculateSurrValue(nfloanDTO);
		}		
	}	
	
	private void calculateSurrValue(NfloanDTO nfloanDTO) throws IOException{
		if ("".equals(t6598IOList.get(0).getCalcprog())){
			srcalcpy.setStatus("ENDP");
			return;
		}

		srcalcpy.setEstimatedVal(BigDecimal.ZERO);
		srcalcpy.setActualVal(BigDecimal.ZERO);
		/* We have a subroutine to call so let's do it*/
		/* But first we must check that the chosen COVRSUR record has a*/
		/*  valid premium & risk status by checking in T5679*/
		checkCovrsurStatii2350();
		/* if either the Premium or Risk status of the Coverage is*/
		/*  invalid, then we won't calculate a Surrender value for it*/
		if ("N".equalsIgnoreCase(premStatus) || "N".equalsIgnoreCase(riskStatus)) {
			srcalcpy.setStatus("ENDP");
			return;
		}

		
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);		
		/*
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))){
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}else{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}
		*/

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (!"OK".equalsIgnoreCase(srcalcpy.getStatus()) && !"ENDP".equalsIgnoreCase(srcalcpy.getStatus())){

		}
		
		/* Add values returned to our running Surrender value total*/
		surrValTotal = callRounding(srcalcpy.getActualVal(), nfloanDTO.getCntcurr(), nfloanDTO.getBatctrcde());

	}
	
	private void sumLoans(NfloanDTO nfloanDTO) throws IOException, ParseException{
		
		Chdrpf chdrpf = new Chdrpf();
		chdrpf.setCntcurr(nfloanDTO.getCntcurr());
		chdrpf.setCnttype(nfloanDTO.getCnttype());
		chdrpf.setChdrcoy(nfloanDTO.getChdrcoy());
		
		TotloanInputDTO totloanDTO = new TotloanInputDTO();
		totloanDTO.setChdrcoy(nfloanDTO.getChdrcoy());
		totloanDTO.setChdrnum(nfloanDTO.getChdrnum());
		totloanDTO.setEffectiveDate(nfloanDTO.getEffdate());
		totloanDTO.setLanguage(nfloanDTO.getLanguage());
		
		TotloanOutputDTO totloanOutput = new TotloanOutputDTO();
		
		totloanOutput = hrtotlon.processLoanList(totloanDTO, chdrpf);
		heldCurrLoans = totloanOutput.getPrincipal().add(totloanOutput.getInterest());
		totalCash = heldCurrLoans;
	}
	
	private void surrvalLoansumCalc(NfloanDTO nfloanDTO){
		totalDebt = heldCurrLoans.add(nfloanDTO.getOutstamt());

		if (heldSurrVal.add(totalCash).compareTo(totalDebt) > 0) {
			
		}else {
			nfloanDTO.setStatuz("NAPL");
		}
	}	
	
	
	private BigDecimal callRounding(BigDecimal amountIn, String orignalCurr, String batchTranCode) throws IOException {
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setAmountIn(amountIn);
		zrdecplDTO.setCurrency(orignalCurr);
		zrdecplDTO.setBatctrcde(batchTranCode);
		return zrdecplc.convertAmount(zrdecplDTO);
	}
	
	private void checkCovrsurStatii2350()
	{
		/*START*/
		premStatus = "N";
		riskStatus = "N";
		Integer idx = 0;
		/* check Coverage premium status*/
		for (idx=1; !((idx > 12) || "Y".equals(premStatus)); idx++){
			premiumStatus(idx);
		}
		/* check Coverage Risk status*/
		for (idx=1; !((idx > 12) || "Y".equals(riskStatus)); idx++){
			riskStatus2390(idx);
		}
		/*EXIT*/
	}	
	
	private void premiumStatus(Integer idx){
		if ("0".equals(covrpf.getRider())) {
			if (!"".equals(t5679IOList.get(0).getCovPremStats().get(idx))) {
				if (covrpf.getPstatcode().equals(t5679IOList.get(0).getCovPremStats().get(idx))) {
					premStatus = "Y";
					return;
				}
			}
		}
		
		if ("0".equals(covrpf.getRider())) {
			if (!"".equals(t5679IOList.get(0).getRidPremStats().get(idx))) {
				if (covrpf.getPstatcode().equals(t5679IOList.get(0).getRidPremStats().get(idx))) {
					premStatus = "Y";
					return;
				}
			}
		}		
	}
	
	private void riskStatus2390(Integer idx){
		if ("0".equals(covrpf.getRider())) {
			if (!"".equals(t5679IOList.get(0).getCovRiskStats().get(idx))) {
				if (covrpf.getStatcode().equals(t5679IOList.get(0).getCovRiskStats().get(idx))) {
					riskStatus = "Y";
					return;
				}
			}
		}
		
		if ("0".equals(covrpf.getRider())) {
			if (!"".equals(t5679IOList.get(0).getRidRiskStats().get(idx))) {
				if (covrpf.getStatcode().equals(t5679IOList.get(0).getRidRiskStats().get(idx))) {
					riskStatus = "Y";
					return;
				}
			}
		}		
	}	
	
	
	
	private void getSurrenderValueMethod(NfloanDTO nfloanDTO) throws IOException {
		getT5687Info(nfloanDTO);
		getT6598Info(nfloanDTO);
		
	}	
	
	private void getT5687Info(NfloanDTO nfloanDTO) throws IOException {
		//Variable keyitem
		if (t5687IOList.isEmpty()) t5687ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T5687", CommonConstants.IT_ITEMPFX);
		
		String t5687Itemkey = covrpf.getCrtable();
		
		if(!t5687ListMap.isEmpty() && t5687ListMap.containsKey(t5687Itemkey)) {
			List<Itempf> t5687ItemList = t5687ListMap.get(t5687Itemkey);
			for (int i = 0; i < t5687ItemList.size(); i++) {
				if (t5687ItemList.get(i).getItmfrm() > 0) {
					if (covrpf.getCrrcd() >= t5687ItemList.get(i).getItmfrm() && covrpf.getCrrcd() <= t5687ItemList.get(i).getItmto()) {
						t5687IO = new ObjectMapper().readValue(t5687ItemList.get(i).getGenareaj(), T5687.class);
						t5687IOList.add(t5687IO);						
					}					
				}else {
					t5687IO = new ObjectMapper().readValue(t5687ItemList.get(i).getGenareaj(), T5687.class);
					t5687IOList.add(t5687IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+t5687Itemkey+" not found in Table T5687 while processing contract "+nfloanDTO.getChdrnum());
		}		
	}
	
	private void getT6598Info(NfloanDTO nfloanDTO) throws IOException {
		//Variable keyitem
		if (t6598IOList.isEmpty()) t6598ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T6598", CommonConstants.IT_ITEMPFX);
		
		String t6598Itemkey = t5687IOList.get(0).getSvMethod();
		
		if(!t6598ListMap.isEmpty() && t6598ListMap.containsKey(t6598Itemkey)) {
			List<Itempf> t6598ItemList = t6598ListMap.get(t6598Itemkey);
			for (int i = 0; i < t6598ItemList.size(); i++) {
				if (t6598ItemList.get(i).getItmfrm() > 0) {
					if (nfloanDTO.getEffdate() >= t6598ItemList.get(i).getItmfrm() && nfloanDTO.getEffdate() <= t6598ItemList.get(i).getItmto()) {
						t6598IO = new ObjectMapper().readValue(t6598ItemList.get(i).getGenareaj(), T6598.class);
						t6598IOList.add(t6598IO);						
					}					
				}else {
					t6598IO = new ObjectMapper().readValue(t6598ItemList.get(i).getGenareaj(), T6598.class);
					t6598IOList.add(t6598IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+t6598Itemkey+" not found in Table T6598 while processing contract "+nfloanDTO.getChdrnum());
		}		
	}		
	
	private void getT6633Info(NfloanDTO nfloanDTO) throws IOException {
		//Variable keyitem
		if (t6633IOList.isEmpty()) t6633ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T6633", CommonConstants.IT_ITEMPFX);
		
		String t6633Itemkey = nfloanDTO.getCnttype().concat(nfloanDTO.getLoantype());
		
		if(!t6633ListMap.isEmpty() && t6633ListMap.containsKey(t6633Itemkey)) {
			List<Itempf> t6633ItemList = t6633ListMap.get(t6633Itemkey);
			for (int i = 0; i < t6633ItemList.size(); i++) {
				if (t6633ItemList.get(i).getItmfrm() > 0) {
					if (nfloanDTO.getEffdate() >= t6633ItemList.get(i).getItmfrm() && nfloanDTO.getEffdate() <= t6633ItemList.get(i).getItmto()) {
						t6633IO = new ObjectMapper().readValue(t6633ItemList.get(i).getGenareaj(), T6633.class);
						t6633IOList.add(t6633IO);						
					}					
				}else {
					t6633IO = new ObjectMapper().readValue(t6633ItemList.get(i).getGenareaj(), T6633.class);
					t6633IOList.add(t6633IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+t6633Itemkey+" not found in Table T6633 while processing contract "+nfloanDTO.getChdrnum());
		}		
	}	
	
	private void getT5679Info(NfloanDTO nfloanDTO) throws IOException {
		//Variable keyitem
		if (t5679IOList.isEmpty()) t5679ListMap=itempfDAO.readSmartTableByTableName(Companies.LIFE, "T5679", CommonConstants.IT_ITEMPFX);
		
		String t5679Itemkey = nfloanDTO.getBatctrcde();
		
		if(!t5679ListMap.isEmpty() && t5679ListMap.containsKey(t5679Itemkey)) {
			List<Itempf> t5679ItemList = t5679ListMap.get(t5679Itemkey);
			for (int i = 0; i < t5679ItemList.size(); i++) {
				if (t5679ItemList.get(i).getItmfrm() > 0) {
					if (nfloanDTO.getEffdate() >= t5679ItemList.get(i).getItmfrm() && nfloanDTO.getEffdate() <= t5679ItemList.get(i).getItmto()) {
						t5679IO = new ObjectMapper().readValue(t5679ItemList.get(i).getGenareaj(), T5679.class);
						t5679IOList.add(t5679IO);						
					}					
				}else {
					t5679IO = new ObjectMapper().readValue(t5679ItemList.get(i).getGenareaj(), T5679.class);
					t5679IOList.add(t5679IO);
				}
			}							
		}else {
			throw new ItemNotfoundException("Br535: Item "+t5679Itemkey+" not found in Table T5679 while processing contract "+nfloanDTO.getChdrnum());
		}		
	}	
}
