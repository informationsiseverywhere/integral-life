package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author gsaluja2
 *
 */
public class Slncpf {
	private long uniqueNumber;
	private String zdatacls;
	private String zreclass;
	private String zmodtype;
	private String zrsltcde;
	private String zrsltrsn;
	private String zerrorno;
	private String znofrecs;
	private String klinckey;
	private String hcltnam;
	private String cltsex;
	private int zclntdob;
	private int zeffdate;
	private String flag;
	private String kglcmpcd;
	private String addrss;
	private BigDecimal zhospbnf;
	private BigDecimal zsickbnf;
	private BigDecimal zcancbnf;
	private BigDecimal zothsbnf;
	private int zregdate;
	private int zlincdte;
	private String kjhcltnam;
	private String zhospopt;
	private int zrevdate; 
	private String zlownnam;
	private BigDecimal zcsumins;
	private BigDecimal zasumins;
	private String zdethopt;
	private String zrqflag;
	private int zrqsetdte;
	private int zrqcandte;
	private String zepflag;
	private String zcvflag;
	private String zcontsex;
	private int zcontdob;
	private String zcontaddr;
	private String lifnamkj;
	private String ownnamkj;
	private String wflga;
	private String zerrflg;
	private int zoccdate;
	private int ztrdate;
	private int zadrddte;
	private int zexdrddte;
	private String dummy;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	
	/*
	 * Replace dummy with new fields
	 * for future use only
	 * 
	 * */
	
	public Slncpf() {
		//does nothing
	}
	
	/**
	 * @return the uniqueNumber
	 */
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	/**
	 * @param uniqueNumber the uniqueNumber to set
	 */
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	/**
	 * @return the zdatacls
	 */
	public String getZdatacls() {
		return zdatacls;
	}
	/**
	 * @param zdatacls the zdatacls to set
	 */
	public void setZdatacls(String zdatacls) {
		this.zdatacls = zdatacls;
	}
	/**
	 * @return the zreclass
	 */
	public String getZreclass() {
		return zreclass;
	}
	/**
	 * @param zreclass the zreclass to set
	 */
	public void setZreclass(String zreclass) {
		this.zreclass = zreclass;
	}
	/**
	 * @return the zmodtype
	 */
	public String getZmodtype() {
		return zmodtype;
	}
	/**
	 * @param zmodtype the zmodtype to set
	 */
	public void setZmodtype(String zmodtype) {
		this.zmodtype = zmodtype;
	}
	/**
	 * @return the zrsltcde
	 */
	public String getZrsltcde() {
		return zrsltcde;
	}
	/**
	 * @param zrsltcde the zrsltcde to set
	 */
	public void setZrsltcde(String zrsltcde) {
		this.zrsltcde = zrsltcde;
	}
	/**
	 * @return the zrsltrsn
	 */
	public String getZrsltrsn() {
		return zrsltrsn;
	}
	/**
	 * @param zrsltrsn the zrsltrsn to set
	 */
	public void setZrsltrsn(String zrsltrsn) {
		this.zrsltrsn = zrsltrsn;
	}
	/**
	 * @return the zerrorno
	 */
	public String getZerrorno() {
		return zerrorno;
	}
	/**
	 * @param zerrorno the zerrorno to set
	 */
	public void setZerrorno(String zerrorno) {
		this.zerrorno = zerrorno;
	}
	/**
	 * @return the znofrecs
	 */
	public String getZnofrecs() {
		return znofrecs;
	}
	/**
	 * @param znofrecs the znofrecs to set
	 */
	public void setZnofrecs(String znofrecs) {
		this.znofrecs = znofrecs;
	}
	/**
	 * @return the klinckey
	 */
	public String getKlinckey() {
		return klinckey;
	}
	/**
	 * @param klinckey the klinckey to set
	 */
	public void setKlinckey(String klinckey) {
		this.klinckey = klinckey;
	}
	/**
	 * @return the hcltnam
	 */
	public String getHcltnam() {
		return hcltnam;
	}
	/**
	 * @param hcltnam the hcltnam to set
	 */
	public void setHcltnam(String hcltnam) {
		this.hcltnam = hcltnam;
	}
	/**
	 * @return the cltsex
	 */
	public String getCltsex() {
		return cltsex;
	}
	/**
	 * @param cltsex the cltsex to set
	 */
	public void setCltsex(String cltsex) {
		this.cltsex = cltsex;
	}
	/**
	 * @return the zclntdob
	 */
	public int getZclntdob() {
		return zclntdob;
	}
	/**
	 * @param zclntdob the zclntdob to set
	 */
	public void setZclntdob(int zclntdob) {
		this.zclntdob = zclntdob;
	}
	/**
	 * @return the zeffdate
	 */
	public int getZeffdate() {
		return zeffdate;
	}
	/**
	 * @param zeffdate the zeffdate to set
	 */
	public void setZeffdate(int zeffdate) {
		this.zeffdate = zeffdate;
	}
	/**
	 * @return the flag
	 */
	public String getFlag() {
		return flag;
	}
	/**
	 * @param flag the flag to set
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}
	/**
	 * @return the kglcmpcd
	 */
	public String getKglcmpcd() {
		return kglcmpcd;
	}
	/**
	 * @param kglcmpcd the kglcmpcd to set
	 */
	public void setKglcmpcd(String kglcmpcd) {
		this.kglcmpcd = kglcmpcd;
	}
	/**
	 * @return the addrss
	 */
	public String getAddrss() {
		return addrss;
	}
	/**
	 * @param addrss the addrss to set
	 */
	public void setAddrss(String addrss) {
		this.addrss = addrss;
	}
	/**
	 * @return the zhospbnf
	 */
	public BigDecimal getZhospbnf() {
		return zhospbnf;
	}
	/**
	 * @param zhospbnf the zhospbnf to set
	 */
	public void setZhospbnf(BigDecimal zhospbnf) {
		this.zhospbnf = zhospbnf;
	}
	/**
	 * @return the zsickbnf
	 */
	public BigDecimal getZsickbnf() {
		return zsickbnf;
	}
	/**
	 * @param zsickbnf the zsickbnf to set
	 */
	public void setZsickbnf(BigDecimal zsickbnf) {
		this.zsickbnf = zsickbnf;
	}
	/**
	 * @return the zcancbnf
	 */
	public BigDecimal getZcancbnf() {
		return zcancbnf;
	}
	/**
	 * @param zcancbnf the zcancbnf to set
	 */
	public void setZcancbnf(BigDecimal zcancbnf) {
		this.zcancbnf = zcancbnf;
	}
	/**
	 * @return the zothsbnf
	 */
	public BigDecimal getZothsbnf() {
		return zothsbnf;
	}
	/**
	 * @param zothsbnf the zothsbnf to set
	 */
	public void setZothsbnf(BigDecimal zothsbnf) {
		this.zothsbnf = zothsbnf;
	}
	/**
	 * @return the zregdate
	 */
	public int getZregdate() {
		return zregdate;
	}
	/**
	 * @param zregdate the zregdate to set
	 */
	public void setZregdate(int zregdate) {
		this.zregdate = zregdate;
	}
	/**
	 * @return the zlincdte
	 */
	public int getZlincdte() {
		return zlincdte;
	}
	/**
	 * @param zlincdte the zlincdte to set
	 */
	public void setZlincdte(int zlincdte) {
		this.zlincdte = zlincdte;
	}
	/**
	 * @return the kjhcltnam
	 */
	public String getKjhcltnam() {
		return kjhcltnam;
	}
	/**
	 * @param kjhcltnam the kjhcltnam to set
	 */
	public void setKjhcltnam(String kjhcltnam) {
		this.kjhcltnam = kjhcltnam;
	}
	/**
	 * @return the zhospopt
	 */
	public String getZhospopt() {
		return zhospopt;
	}
	/**
	 * @param zhospopt the zhospopt to set
	 */
	public void setZhospopt(String zhospopt) {
		this.zhospopt = zhospopt;
	}
	/**
	 * @return the zrevdate
	 */
	public int getZrevdate() {
		return zrevdate;
	}
	/**
	 * @param zrevdate the zrevdate to set
	 */
	public void setZrevdate(int zrevdate) {
		this.zrevdate = zrevdate;
	}
	/**
	 * @return the zlownnam
	 */
	public String getZlownnam() {
		return zlownnam;
	}
	/**
	 * @param zlownnam the zlownnam to set
	 */
	public void setZlownnam(String zlownnam) {
		this.zlownnam = zlownnam;
	}
	/**
	 * @return the zcsumins
	 */
	public BigDecimal getZcsumins() {
		return zcsumins;
	}
	/**
	 * @param zcsumins the zcsumins to set
	 */
	public void setZcsumins(BigDecimal zcsumins) {
		this.zcsumins = zcsumins;
	}
	/**
	 * @return the zasumins
	 */
	public BigDecimal getZasumins() {
		return zasumins;
	}
	/**
	 * @param zasumins the zasumins to set
	 */
	public void setZasumins(BigDecimal zasumins) {
		this.zasumins = zasumins;
	}
	/**
	 * @return the zdethopt
	 */
	public String getZdethopt() {
		return zdethopt;
	}
	/**
	 * @param zdethopt the zdethopt to set
	 */
	public void setZdethopt(String zdethopt) {
		this.zdethopt = zdethopt;
	}
	/**
	 * @return the zrqflag
	 */
	public String getZrqflag() {
		return zrqflag;
	}
	/**
	 * @param zrqflag the zrqflag to set
	 */
	public void setZrqflag(String zrqflag) {
		this.zrqflag = zrqflag;
	}
	/**
	 * @return the zrqsetdte
	 */
	public int getZrqsetdte() {
		return zrqsetdte;
	}
	/**
	 * @param zrqsetdte the zrqsetdte to set
	 */
	public void setZrqsetdte(int zrqsetdte) {
		this.zrqsetdte = zrqsetdte;
	}
	/**
	 * @return the zrqcandte
	 */
	public int getZrqcandte() {
		return zrqcandte;
	}
	/**
	 * @param zrqcandte the zrqcandte to set
	 */
	public void setZrqcandte(int zrqcandte) {
		this.zrqcandte = zrqcandte;
	}
	/**
	 * @return the zepflag
	 */
	public String getZepflag() {
		return zepflag;
	}
	/**
	 * @param zepflag the zepflag to set
	 */
	public void setZepflag(String zepflag) {
		this.zepflag = zepflag;
	}
	/**
	 * @return the zcvflag
	 */
	public String getZcvflag() {
		return zcvflag;
	}
	/**
	 * @param zcvflag the zcvflag to set
	 */
	public void setZcvflag(String zcvflag) {
		this.zcvflag = zcvflag;
	}
	/**
	 * @return the zcontsex
	 */
	public String getZcontsex() {
		return zcontsex;
	}
	/**
	 * @param zcontsex the zcontsex to set
	 */
	public void setZcontsex(String zcontsex) {
		this.zcontsex = zcontsex;
	}
	/**
	 * @return the zcontdob
	 */
	public int getZcontdob() {
		return zcontdob;
	}
	/**
	 * @param zcontdob the zcontdob to set
	 */
	public void setZcontdob(int zcontdob) {
		this.zcontdob = zcontdob;
	}
	/**
	 * @return the zcontaddr
	 */
	public String getZcontaddr() {
		return zcontaddr;
	}
	/**
	 * @param zcontaddr the zcontaddr to set
	 */
	public void setZcontaddr(String zcontaddr) {
		this.zcontaddr = zcontaddr;
	}
	/**
	 * @return the lifnamkj
	 */
	public String getLifnamkj() {
		return lifnamkj;
	}
	/**
	 * @param lifnamkj the lifnamkj to set
	 */
	public void setLifnamkj(String lifnamkj) {
		this.lifnamkj = lifnamkj;
	}
	/**
	 * @return the ownnamkj
	 */
	public String getOwnnamkj() {
		return ownnamkj;
	}
	/**
	 * @param ownnamkj the ownnamkj to set
	 */
	public void setOwnnamkj(String ownnamkj) {
		this.ownnamkj = ownnamkj;
	}
	/**
	 * @return the wflga
	 */
	public String getWflga() {
		return wflga;
	}
	/**
	 * @param wflga the wflga to set
	 */
	public void setWflga(String wflga) {
		this.wflga = wflga;
	}
	/**
	 * @return the zerrflg
	 */
	public String getZerrflg() {
		return zerrflg;
	}
	/**
	 * @param zerrflg the zerrflg to set
	 */
	public void setZerrflg(String zerrflg) {
		this.zerrflg = zerrflg;
	}
	/**
	 * @return the zoccdate
	 */
	public int getZoccdate() {
		return zoccdate;
	}
	/**
	 * @param zoccdate the zoccdate to set
	 */
	public void setZoccdate(int zoccdate) {
		this.zoccdate = zoccdate;
	}
	/**
	 * @return the ztrdate
	 */
	public int getZtrdate() {
		return ztrdate;
	}
	/**
	 * @param ztrdate the ztrdate to set
	 */
	public void setZtrdate(int ztrdate) {
		this.ztrdate = ztrdate;
	}
	/**
	 * @return the zadrddte
	 */
	public int getZadrddte() {
		return zadrddte;
	}
	/**
	 * @param zadrddte the zadrddte to set
	 */
	public void setZadrddte(int zadrddte) {
		this.zadrddte = zadrddte;
	}
	/**
	 * @return the zexdrddte
	 */
	public int getZexdrddte() {
		return zexdrddte;
	}
	/**
	 * @param zexdrddte the zexdrddte to set
	 */
	public void setZexdrddte(int zexdrddte) {
		this.zexdrddte = zexdrddte;
	}
	/**
	 * @return the usrprf
	 */
	public String getUsrprf() {
		return usrprf;
	}
	/**
	 * @param usrprf the usrprf to set
	 */
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	/**
	 * @return the jobnm
	 */
	public String getJobnm() {
		return jobnm;
	}
	/**
	 * @param jobnm the jobnm to set
	 */
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	/**
	 * @return the datime
	 */
	public Timestamp getDatime() {
		return datime;
	}
	/**
	 * @param datime the datime to set
	 */
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
}