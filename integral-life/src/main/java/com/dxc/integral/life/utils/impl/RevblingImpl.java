package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.FeaConfg;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.ReverseDTO;
import com.dxc.integral.life.dao.ArcmpfDAO;
import com.dxc.integral.life.dao.BextpfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.FpcopfDAO;
import com.dxc.integral.life.dao.FprmpfDAO;
import com.dxc.integral.life.dao.HdivpfDAO;
import com.dxc.integral.life.dao.LinspfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.TaxdpfDAO;
import com.dxc.integral.life.dao.model.AcmvrevcpPojo;
import com.dxc.integral.life.dao.model.Arcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Fpcopf;
import com.dxc.integral.life.dao.model.Fprmpf;
import com.dxc.integral.life.dao.model.Hdivpf;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5679;
import com.dxc.integral.life.smarttable.pojo.T5729;
import com.dxc.integral.life.smarttable.pojo.Ta524;
import com.dxc.integral.life.utils.Acmvrevcp;
import com.dxc.integral.life.utils.Datcon4;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Revbling;

@Service("revbling")
@Lazy
public class RevblingImpl implements Revbling{
	
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Datcon4 datcon4;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private BextpfDAO bextpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private FpcopfDAO fpcopfDAO;
	@Autowired
	private AcmvpfDAO acmvpfDAO;
	@Autowired
	private FprmpfDAO fprmpfDAO;
	@Autowired
	private LinspfDAO linspfDAO;
	@Autowired
	private TaxdpfDAO taxdpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	@Autowired
	private ArcmpfDAO arcmpfDAO;
	@Autowired
	private HdivpfDAO hdivpfDAO;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Acmvrevcp acmvrevcp;
	@Autowired
	private Lifacmv lifacmv;
	@Autowired
	private ItempfService itempfService;
	private String wsaaTransDesc;
	private T5679 t5679IO;
	private String currency="";
	private Chdrpf chdrpf = null;
	private Payrpf payrpf = null;
	private int wsaaBillspfrm=0;
	private String datcon1date;
	private String wsbbPeriod;
	private String wsaaPeriod;
	private LifacmvDTO lifacmvDTO;


	public ReverseDTO processRevbilling(ReverseDTO reverseDTO) throws IOException,ParseException{
		
		reverseDTO.setStatuz("****");
		datcon1date = datcon1.todaysDate();
		Descpf descpf = descpfDAO.getDescInfo(reverseDTO.getCompany(), "T1688", reverseDTO.getBatctrcde(), reverseDTO.getLanguage());
		if(descpf == null) {
			wsaaTransDesc="????????????????????";
		}else {
			wsaaTransDesc=descpf.getLongdesc();
		}
		TableItem<T5679> t5679=itempfService.readSmartTableByTrimItem(reverseDTO.getCompany(), "T5679", reverseDTO.getOldBatctrcde(), "IT", T5679.class);
		if(t5679==null) {
			reverseDTO.setStatuz("BOMB");
			return reverseDTO;
		}
		t5679IO = t5679.getItemDetail();
		chdrpf = chdrpfDAO.getChdrrnlRecord(reverseDTO.getCompany(), reverseDTO.getChdrnum());
		if(chdrpf == null) {
			reverseDTO.setStatuz("BOMB");
			return reverseDTO;
		}

		boolean flexCheckflag = flexPremCheck();
		payrpf = payrpfDAO.getPayrRecord(reverseDTO.getCompany(), chdrpf.getChdrnum());
		bextpfDAO.deleteBextpf(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getPtrneff());
		
		if(flexCheckflag) {
			wsaaBillspfrm=calcBillspfrm(reverseDTO.getPtrneff());
			readCovrs(reverseDTO);
		}else {
			processLins(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getPtrneff());
		}
		
		updateContractHeader(reverseDTO);
		updatePayrFile();
		processAcmvs(reverseDTO);
		processAcmvOptical(reverseDTO);
		/* For Cash Dividend, reverse records created in premium           */
		/* settlement dividend processing option                           */
		premSettlement(reverseDTO);
		return reverseDTO;
		
	}
	
	private boolean flexPremCheck() {
		TableItem<T5729> t5729=itempfService.readSmartTableByTrimItem(chdrpf.getChdrcoy(), "T5729", chdrpf.getCnttype(), "IT", T5729.class);
		if(t5729 != null) {
			return true;
		}else {
			return false;
		}
	}
	
	private int calcBillspfrm(int ptrneff) {
		int result = datcon2.plusMonths(ptrneff, 1);
		return datcon2.plusDays(result, -1);
		
	}
	
	private void readCovrs(ReverseDTO reverseDTO) throws IOException{
		List<Covrpf> list = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(reverseDTO.getCompany(), reverseDTO.getChdrnum());
		if(list == null || list.size()<=0) {
			return ;
		}
		String wsaaValidCoverage = "N";
		int wsaaT5679Sub=0;
		for(Covrpf covrpf : list) {
			wsaaValidCoverage = "N";
			
			for (wsaaT5679Sub=0; wsaaT5679Sub<12; wsaaT5679Sub++){
				if (t5679IO.getCovPremStats().get(wsaaT5679Sub).equals(covrpf.getStatcode())) {
					for (wsaaT5679Sub=0; wsaaT5679Sub<12; wsaaT5679Sub++){
						if (t5679IO.getCovPremStats().get(wsaaT5679Sub).equals(covrpf.getPstatcode())) {
							wsaaT5679Sub=13;
							wsaaValidCoverage="Y";
						}
					}
				}
			}
			
			if (!wsaaValidCoverage.equals("Y")) {
				continue;
			}
			if (covrpf.getInstprem().compareTo(BigDecimal.ZERO)==0) {
				continue;
			}
			readFpco(covrpf, reverseDTO);
		}
	}
	
	private boolean readFpco(Covrpf covrpf, ReverseDTO reverseDTO) throws IOException{
		List<Fpcopf> list = fpcopfDAO.readFpcorevRecords(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlnsfx());
        if(list == null || list.size() <=0) {
        	return false;
        }
        
        Iterator<Fpcopf> fp = list.iterator();
        Fpcopf fpcopf = new Fpcopf();
        while(fp.hasNext()) {
        	fpcopf = fp.next();
        	if(fpcopf.getBilledp().compareTo(BigDecimal.ZERO)==0) {
        		while(fp.hasNext()) {
        			fpcopf = fp.next();
        		}
        	}
        	
        	fpcopf.setBilledp(fpcopf.getBilledp().subtract(covrpf.getInstprem()).setScale(2));
        	fpcopf.setOvrminreq((fpcopf.getBilledp().multiply(new BigDecimal(fpcopf.getMinovrpro())).divide(new BigDecimal(100))).setScale(2));
    		/* Now we need to update  the minimum required for overdue <D9604>*/
    		/* processing:                                             <D9604>*/
        	ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();
        	zrdecplcDTO.setAmountIn(fpcopf.getOvrminreq());

    		fpcopf.setOvrminreq(c000CallRounding(fpcopf.getOvrminreq(), reverseDTO.getCompany(), currency, reverseDTO.getBatctrcde()));
 
    		/*                                                         <D9604> */
    		/* The active indicator should be reset to 'Y' if the prem <D9604>*/
    		/* billed drops below the target                           <D9604>*/
    		/*                                                         <D9604> */
    		if (fpcopf.getBilledp().compareTo(fpcopf.getPrmper())<0) {
    			fpcopf.setActind("Y");
    		}
    		
    		if(fpcopfDAO.updateFpcopfRecords(fpcopf)<=0) {
    			return false;
    		}
        	
    		Fprmpf fprmpf = fprmpfDAO.readFprmpfBySeqno(reverseDTO.getCompany(), reverseDTO.getChdrnum(), payrpf.getPayrseqno().intValue());
    		if(fprmpf == null) {
    			return false;
    		}
    		
    		fprmpf.setTotbill(fprmpf.getTotbill().subtract(covrpf.getInstprem()).setScale(2));
    		fprmpf.setMinreqd((fprmpf.getMinreqd().subtract(covrpf.getInstprem().multiply(new BigDecimal(fpcopf.getMinovrpro()/100)))).setScale(2));
    		fprmpf.setMinreqd(c000CallRounding(fprmpf.getMinreqd(), reverseDTO.getCompany(), currency, reverseDTO.getBatctrcde()));
    		List<Fprmpf> flist = new ArrayList<Fprmpf>();
    		flist.add(fprmpf);
    		fprmpfDAO.updateFprmpfRecords(flist);
    		
        }
        
        return true;
		
	}
	
	private BigDecimal c000CallRounding(BigDecimal amountIn, String company, String currency, String batctrcde) throws IOException{
		ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();
    	zrdecplcDTO.setAmountIn(amountIn);
    	zrdecplcDTO.setCompany(company);
    	zrdecplcDTO.setCurrency(currency);
    	zrdecplcDTO.setBatctrcde(batctrcde);
    	return zrdecplc.convertAmount(zrdecplcDTO);
    
	}
	
	private void processLins(String chdrcoy, String chdrnum, int ptrneff) {
		List<Linspf> linspfList = linspfDAO.searchLinsrevRecords(chdrcoy, chdrnum);
		if(linspfList==null || linspfList.size()<=0) {
			return ;
		}
		
		for(Linspf linspf : linspfList) {
			if(linspf.getInstto()<=ptrneff) {
				continue;
			}else {
				chdrpf.setOutstamt((chdrpf.getOutstamt().subtract(linspf.getInstamt06())).setScale(2));
				payrpf.setOutstamt((payrpf.getOutstamt().subtract(linspf.getInstamt02())).setScale(2));
				wsaaBillspfrm = datcon2.plusDays(linspf.getInstto(), -1);
				if(linspfDAO.deleteLinspf(linspf.getUnique_number())<=0) {
					continue;
				}
				taxdpfDAO.deleteTaxdpfRecords(chdrcoy, chdrnum, linspf.getInstfrom(), linspf.getInstto());
				
			}
		}
		

	
	}
	
	private void updateContractHeader(ReverseDTO reverseDTO) throws ParseException{
		BigDecimal datcon3factor = datcon3.getTerms(reverseDTO.getPtrneff(), payrpf.getBtdate().intValue(), payrpf.getBillfreq());
		BigDecimal datcon4factor, datcon2factor;
		if (datcon3factor.compareTo(BigDecimal.ONE)<1 && datcon3factor.compareTo(BigDecimal.ONE)>-1) {
			datcon4factor = (datcon3factor.multiply(new BigDecimal(-1))).setScale(6);
		}else {
			datcon4factor = (datcon3factor.multiply(new BigDecimal(-1))).setScale(5);	
		}
		
		int datcon4result = datcon4.getIntDate2(payrpf.getBillfreq(), payrpf.getBtdate().toString(), datcon4factor.intValue()).intValue();
		
		/* IF FEBRUARY AND                                      <LA3432>*/
		/*    PAYR-DUEDD               NOT = WSAA-BTDATE-DD     <LA3432>*/
		/*    MOVE DTC4-INT-DATE-2     TO WSAA-DTC4-INT-DATE-2  <LA3432>*/
		/*    MOVE PAYR-DUEDD          TO WSAA-DTC4-DD          <LA3432>*/
		/*    MOVE WSAA-DTC4-INT-DATE-2                         <LA3432>*/
		/*                             TO DTC4-INT-DATE-2       <LA3432>*/
		/* END-IF.                                              <LA3432>*/
		/* Update the BTDATE on the PAYR & CHDR records*/
		payrpf.setBtdate(datcon4result);
		chdrpf.setBtdate(datcon4result);
		/* Rewind the BILLCD next*/
		/*   Calculate the difference between the BILLCD and the Effective*/
		/*    date.Then use DATCON4 to wind the BILLCD back.*/
		datcon3factor = datcon3.getTerms(reverseDTO.getPtrneff(), payrpf.getBillcd().intValue(), payrpf.getBillfreq());
		if (datcon3factor.compareTo(BigDecimal.ONE)<1 && datcon3factor.compareTo(BigDecimal.ONE)>-1) {
			datcon4factor = (datcon3factor.multiply(new BigDecimal(-1))).setScale(6);
		}else {
			datcon4factor = (datcon3factor.multiply(new BigDecimal(-1))).setScale(5);	
		}
		
		datcon4result = datcon4.getIntDate2(payrpf.getBillfreq(), payrpf.getBillcd().toString(), datcon4factor.intValue()).intValue();
		
		
		payrpf.setBillcd(datcon4result);
		chdrpf.setBillcd(datcon4result);
		/* Wind back PAYR-NEXTDATE the same number of instalments as*/
		/* the billing date. (PAYR-BILLCD).*/
		
	
		
		if (datcon3factor.compareTo(BigDecimal.ONE)<1 && datcon3factor.compareTo(BigDecimal.ONE)>-1) {
			datcon2factor = (datcon3factor.multiply(new BigDecimal(-1))).setScale(6);
		}else {
			datcon2factor = (datcon3factor.multiply(new BigDecimal(-1))).setScale(5);	
		}
		int datcon2date2 = datcon2.plusDate(payrpf.getBillfreq(), payrpf.getNextdate(), datcon2factor.intValue());
	
		payrpf.setNextdate(datcon2date2);
		/* Update Billing Suppression To Date and Billing Suppression*/
		/* Flag on the PAYR record*/
		if (reverseDTO.getEffdate2()!=99999999) {
			payrpf.setBillspto(reverseDTO.getEffdate2());
			payrpf.setBillspfrom(wsaaBillspfrm);
			payrpf.setBillsupr("Y");
			chdrpf.setBillspto(reverseDTO.getEffdate2());
			chdrpf.setBillspfrom(wsaaBillspfrm);
			chdrpf.setBillsupr("Y");
		}
		if(chdrpfDAO.updateChdrrnlRecord(chdrpf)<=0) {
			fatalError("BOMB");
		}
		checkPH(reverseDTO.getTranno());
		
		
	}
	
	protected void checkPH(int tranno) {

		Ta524 ta524IO = new Ta524();
		Ptrnpf ptrnpf;
		boolean prmhldtrad = FeaConfg.isFeatureExist(chdrpf.getChdrcoy(), "CSOTH010", "IT");
		TableItem<Ta524> ta524=itempfService.readSmartTableByTrimItem(chdrpf.getChdrcoy(), "TA524", chdrpf.getCnttype(), "IT", Ta524.class);
		if(ta524!=null) {
			ta524IO = ta524.getItemDetail();
		}
			

		if(prmhldtrad && "".equals(ta524IO.getPrmstatus().trim())&&"".equals(ta524IO.getZnfopt().trim())&&ta524IO.getMaxphprd()==0&&ta524IO.getMinmthif()==0) {
			ptrnpf = ptrnpfDAO.getPtrnRecord(chdrpf.getChdrcoy(), chdrpf.getChdrnum(), tranno-1);
			if("TR7H".equals(ptrnpf.getBatctrcde()))
				payrpf.setTranno(payrpf.getTranno()-1);
		}
	}
	
	protected void updatePayrFile() {
		payrpfDAO.updatePayrpfRecord(payrpf);
	}
	
	protected void processAcmvs(ReverseDTO reverseDTO) throws ParseException, IOException{
		List<Acmvpf> acmvlist = acmvpfDAO.getAcmvpfList(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getTranno());
		if(acmvlist == null || acmvlist.size() ==0) {
			return ;
		}
		
		
		for(Acmvpf acmvpf : acmvlist) {

			if(!reverseDTO.getOldBatctrcde().equals(acmvpf.getBatctrcde())) {
				continue;
			}
			
			processLifacmv(acmvpf, reverseDTO);
		}
	}
	
	private void processLifacmv(Acmvpf acmvpf, ReverseDTO reverseDTO) throws ParseException, IOException{
		lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setContot(0);
		lifacmvDTO.setFrcdate(0);
		lifacmvDTO.setTransactionDate(0);
		lifacmvDTO.setTransactionTime(0);
		lifacmvDTO.setUser(0);
		lifacmvDTO.setBatckey(reverseDTO.getBatchkey());
		lifacmvDTO.setRdocnum(acmvpf.getRdocnum());
		lifacmvDTO.setTranno(reverseDTO.getNewTranno());
		lifacmvDTO.setSacscode(acmvpf.getSacscode());
		lifacmvDTO.setSacstyp(acmvpf.getSacstyp());
		lifacmvDTO.setGlcode(acmvpf.getGlcode());
		lifacmvDTO.setGlsign(acmvpf.getGlsign());
		lifacmvDTO.setJrnseq(acmvpf.getJrnseq());
		lifacmvDTO.setRldgcoy(acmvpf.getRldgcoy());
		lifacmvDTO.setGenlcoy(acmvpf.getGenlcoy());
		lifacmvDTO.setRldgacct(acmvpf.getRldgacct());
		lifacmvDTO.setOrigcurr(acmvpf.getOrigcurr());
		lifacmvDTO.setAcctamt((acmvpf.getAcctamt().multiply(new BigDecimal(-1))).setScale(2));
		lifacmvDTO.setOrigamt((acmvpf.getOrigamt().multiply(new BigDecimal(-1))).setScale(2));
		lifacmvDTO.setGenlcur(acmvpf.getGenlcur());
		lifacmvDTO.setCrate(acmvpf.getCrate());
		lifacmvDTO.setPostyear("");
		lifacmvDTO.setPostmonth("");
		lifacmvDTO.setTranref(acmvpf.getTranref());
		lifacmvDTO.setTrandesc(wsaaTransDesc);
		lifacmvDTO.setEffdate(Integer.parseInt(datcon1date));
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setTermid(reverseDTO.getTermid());
		lifacmvDTO.setUser(reverseDTO.getUser());
		lifacmvDTO.setTransactionTime(reverseDTO.getTransTime());
		lifacmvDTO.setTransactionDate(reverseDTO.getTransDate());
		lifacmv.executePSTW(lifacmvDTO);
	}
	

   private void processAcmvOptical(ReverseDTO reverseDTO) throws ParseException, IOException{
	   Arcmpf arcmpf = arcmpfDAO.getArcmpfRecords("ACMV");
	   if(arcmpf == null) {
		   wsbbPeriod = "0";
	   }else {
		   wsbbPeriod = String.valueOf(arcmpf.getAcctyr()).concat(String.valueOf(arcmpf.getAcctmnth()));
	   }
	   wsaaPeriod = String.valueOf(reverseDTO.getPtrnBatcactyr()).concat(String.valueOf(reverseDTO.getPtrnBatcactmn()));
	   if(Integer.parseInt(wsaaPeriod)>Integer.parseInt(wsbbPeriod)) {
		   return ;
	   }
	   AcmvrevcpPojo acmvrevcpPojo = new AcmvrevcpPojo();
	   acmvrevcpPojo.setRldgcoy(reverseDTO.getCompany());
	   acmvrevcpPojo.setRdocnum(reverseDTO.getChdrnum());
	   acmvrevcpPojo.setTranno(reverseDTO.getTranno());
	   acmvrevcpPojo.setBatctrcde(reverseDTO.getOldBatctrcde());
	   acmvrevcpPojo.setBatcactyr(reverseDTO.getPtrnBatcactyr());
	   acmvrevcpPojo.setBatcactmn(reverseDTO.getPtrnBatcactmn());
		acmvrevcpPojo.setFunction("BEGN");
		acmvrevcpPojo = acmvrevcp.processAcmvrevcp(acmvrevcpPojo);
		if (!reverseDTO.getCompany().equals(acmvrevcpPojo.getRldgcoy()) || !reverseDTO.getChdrnum().equals(acmvrevcpPojo.getRdocnum())
				|| reverseDTO.getTranno()!=acmvrevcpPojo.getTranno()||acmvrevcpPojo.getStatuz().equals("ENDP")) {
			acmvrevcpPojo.setStatuz("ENDP");
		}
		
		while ( !acmvrevcpPojo.getStatuz().equals("ENDP")) {
			if(reverseDTO.getOldBatctrcde().equals(acmvrevcpPojo.getBatctrcde())) {
				
				processLifacmvOption(acmvrevcpPojo, reverseDTO);
			}
			acmvrevcpPojo = acmvrevcp.processAcmvrevcp(acmvrevcpPojo);
			if (!reverseDTO.getCompany().equals(acmvrevcpPojo.getRldgcoy()) || !reverseDTO.getChdrnum().equals(acmvrevcpPojo.getRdocnum())
					|| reverseDTO.getTranno()!=acmvrevcpPojo.getTranno()||acmvrevcpPojo.getStatuz().equals("ENDP")) {
				acmvrevcpPojo.setStatuz("ENDP");
			}
			
	   }
	
	   
	   
   }

   private void processLifacmvOption(AcmvrevcpPojo acmvrevcpPojo, ReverseDTO reverseDTO) throws ParseException, IOException{
		lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setContot(0);
		lifacmvDTO.setFrcdate(0);
		lifacmvDTO.setTransactionDate(0);
		lifacmvDTO.setTransactionTime(0);
		lifacmvDTO.setUser(0);
		lifacmvDTO.setBatckey(reverseDTO.getBatchkey());
		lifacmvDTO.setRdocnum(acmvrevcpPojo.getRdocnum());
		lifacmvDTO.setTranno(reverseDTO.getNewTranno());
		lifacmvDTO.setSacscode(acmvrevcpPojo.getSacscode());
		lifacmvDTO.setSacstyp(acmvrevcpPojo.getSacstyp());
		lifacmvDTO.setGlcode(acmvrevcpPojo.getGlcode());
		lifacmvDTO.setGlsign(acmvrevcpPojo.getGlsign());
		lifacmvDTO.setJrnseq(acmvrevcpPojo.getJrnseq());
		lifacmvDTO.setRldgcoy(acmvrevcpPojo.getRldgcoy());
		lifacmvDTO.setGenlcoy(acmvrevcpPojo.getGenlcoy());
		lifacmvDTO.setRldgacct(acmvrevcpPojo.getRldgacct());
		lifacmvDTO.setOrigcurr(acmvrevcpPojo.getOrigcurr());
		lifacmvDTO.setAcctamt((acmvrevcpPojo.getAcctamt().multiply(new BigDecimal(-1))).setScale(2));
		lifacmvDTO.setOrigamt((acmvrevcpPojo.getOrigamt().multiply(new BigDecimal(-1))).setScale(2));
		lifacmvDTO.setGenlcur(acmvrevcpPojo.getGenlcur());
		lifacmvDTO.setCrate(acmvrevcpPojo.getCrate());
		lifacmvDTO.setPostyear("");
		lifacmvDTO.setPostmonth("");
		lifacmvDTO.setTranref(acmvrevcpPojo.getTranref());
		lifacmvDTO.setTrandesc(wsaaTransDesc);
		lifacmvDTO.setEffdate(Integer.parseInt(datcon1date));
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setTermid(reverseDTO.getTermid());
		lifacmvDTO.setUser(reverseDTO.getUser());
		lifacmvDTO.setTransactionTime(reverseDTO.getTransTime());
		lifacmvDTO.setTransactionDate(reverseDTO.getTransDate());
		lifacmv.executePSTW(lifacmvDTO);
	}
   
   private void premSettlement(ReverseDTO reverseDTO) {
	   List<Hdivpf> hdivlist = hdivpfDAO.readHdivpfRecords(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getTranno());
	   if(hdivlist==null || hdivlist.size()==0) {
		   return ;
	   }
	   Hdivpf insertHdivpf;
	   List<Hdivpf> insertHdivpfList = new ArrayList<Hdivpf>();
	   for(Hdivpf hdivpf : hdivlist) {
		   if (hdivpf.getDivdStmtNo().intValue()==0) {
				if(hdivpfDAO.deleteHdivpfRecord(hdivpf)<=0) {
					fatalError("BOMB");
				}
			}else {
				insertHdivpf = new Hdivpf();
				insertHdivpf.setChdrcoy(hdivpf.getChdrcoy());
				insertHdivpf.setChdrnum(hdivpf.getChdrnum());
				insertHdivpf.setLife(hdivpf.getLife());
				insertHdivpf.setJlife(hdivpf.getJlife());
				insertHdivpf.setCoverage(hdivpf.getCoverage());
				insertHdivpf.setRider(hdivpf.getRider());
				insertHdivpf.setPlnsfx(hdivpf.getPlnsfx());
				insertHdivpf.setHpuanbr(hdivpf.getHpuanbr());
				insertHdivpf.setTranno(reverseDTO.getNewTranno());
				insertHdivpf.setBatccoy(reverseDTO.getBatccoy());
				insertHdivpf.setBatcbrn(reverseDTO.getBatcbrn());
				insertHdivpf.setBatcactyr(reverseDTO.getBatcactyr());
				insertHdivpf.setBatcactmn(reverseDTO.getBatcactmn());
				insertHdivpf.setBatctrcde(reverseDTO.getBatctrcde());
				insertHdivpf.setBatcbatch(reverseDTO.getBatcbatch());
				insertHdivpf.setCntcurr(hdivpf.getCntcurr());
				insertHdivpf.setEffdate(hdivpf.getEffdate());
				insertHdivpf.setHdvaldt(reverseDTO.getEffdate1());
				insertHdivpf.setHdvtyp(hdivpf.getHdvtyp());
				insertHdivpf.setHdvamt((hdivpf.getHdvamt().multiply(new BigDecimal(-1))).setScale(2));
				insertHdivpf.setHdvrate(hdivpf.getHdvrate());
				insertHdivpf.setHdveffdt(hdivpf.getHdveffdt());
				insertHdivpf.setZdivopt(hdivpf.getZdivopt());
				insertHdivpf.setZcshdivmth(hdivpf.getZcshdivmth());
				insertHdivpf.setHdvopttx(hdivpf.getHdvopttx());
				insertHdivpf.setHdvcaptx(hdivpf.getHdvcaptx());
				insertHdivpf.setHincapdt(hdivpf.getHincapdt());
				insertHdivpf.setDivdStmtNo(0);
				insertHdivpfList.add(insertHdivpf);
			}
	   }
	   
	  if(insertHdivpfList.size()>0) {
		  hdivpfDAO.insertHdivpfRecords(insertHdivpfList);
	  } 
   }
	
	private void fatalError(String status) {
		throw new StopRunException(status);
	}
}
