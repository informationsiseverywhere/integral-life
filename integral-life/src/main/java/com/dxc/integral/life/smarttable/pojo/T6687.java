package com.dxc.integral.life.smarttable.pojo;

public class T6687{
  
  	private String taxrelsub;

	public String getTaxrelsub() {
		return taxrelsub;
	}

	public void setTaxrelsub(String taxrelsub) {
		this.taxrelsub = taxrelsub;
	}
}