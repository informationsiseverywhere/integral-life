package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.BillreqDTO;
/**
 * @author wli31
 */
public interface Billreq1 {
	BillreqDTO processBillreq1(BillreqDTO billreqDTO)throws Exception;
}
