package com.dxc.integral.life.utils.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.model.Clexpf;
import com.dxc.integral.life.utils.Vpxchdr;
@Service("vpxchdr")
@Lazy
public class VpxchdrImpl implements Vpxchdr{
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ClexpfDAO clexpfDAO;
	
	private VpxchdrDTO vpxchdrDTO = new VpxchdrDTO();
	private Chdrpf chdrlnbIO = null;
	private Clexpf clexpf = null;
	public VpxchdrDTO callInit(PremiumDTO premiumDTO){
		vpxchdrDTO.setStatuz("****");
		initializvpxchdrDTO();
		findChdrlif(premiumDTO);
		return vpxchdrDTO;
	}
	 protected void findChdrlif(PremiumDTO premiumDTO) {
		 List<Chdrpf> chdrpfList = chdrpfDAO.readRecordOfChdrlnb(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum());
		 if(chdrpfList == null || chdrpfList.isEmpty()) {
			 vpxchdrDTO.setStatuz("MRNF");
			return ;
		}
		chdrlnbIO = chdrpfList.get(0);
		vpxchdrDTO.setCopybook("VPXCHDRREC");
		vpxchdrDTO.setOccdate(chdrlnbIO.getOccdate());
		vpxchdrDTO.setBillchnl(chdrlnbIO.getBillchnl());
		vpxchdrDTO.setCownnum(chdrlnbIO.getCownnum());
		vpxchdrDTO.setJownnum(chdrlnbIO.getJownnum());
		vpxchdrDTO.setCnttype(chdrlnbIO.getCnttype());
		if (!"".equals(chdrlnbIO.getCownnum())){
			findClex();
		}
		
	}
	 protected void findClex(){
		 clexpf = clexpfDAO.getClexpfRecord(chdrlnbIO.getCownpfx(), chdrlnbIO.getCowncoy(), chdrlnbIO.getCownnum());
		 
	   	 if (clexpf == null) {
	   	   	return ;
	   	 }
	   	 if (!"Y".equals(clexpf.getRstaflag()) && !"".equals(chdrlnbIO.getJownnum())) {
	   		clexpf = clexpfDAO.getClexpfRecord(chdrlnbIO.getCownpfx(), chdrlnbIO.getCowncoy(), chdrlnbIO.getJownnum());
	   		 if (clexpf == null) {
	   	   	   	 return ;
	   		 }
	   	 }
	   	 if ("Y".equals(clexpf.getRstaflag())){
	   		vpxchdrDTO.setRstaflag("Y");
	   	 } else {
	   		vpxchdrDTO.setRstaflag("N");
	   	 }
	}
	protected void initializvpxchdrDTO(){
		vpxchdrDTO.setOccdate(99991231);
		vpxchdrDTO.setBillchnl(" ");
		vpxchdrDTO.setCownnum(" ");
		vpxchdrDTO.setJownnum(" ");
		vpxchdrDTO.setCnttype(" ");
		vpxchdrDTO.setRstaflag("  ");
   }
}
