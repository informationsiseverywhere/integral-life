package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Instintcalc;

@Service("instintcalc")
@Lazy
public class InstintcalcImpl implements Instintcalc {
	
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Datcon3 datcon3;
	@Autowired
	private Xcvrt xcvrt;
	@Autowired
	private AcmvpfDAO acmvpfDAO;
	
	
	private ConlinkInputDTO conlinkInputDTO;
	private ConlinkOutputDTO conlinkOutputDTO;
	
	private T6633 t6633;
	private T5645 t5645;
	private String wsaaSubr = "INTCALC";
	private BigDecimal wsaaTotalInterest;
	private BigDecimal wsaaInterest;
	private BigDecimal wsaaLoanCurrVal;
	private String wsaaSacscode="";
	private String wsaaSacstype = "";
	private String wsaaSign = "";
	private String wsaaRldgacct = "";
	
	public IntcalcDTO processInstintcalc(IntcalcDTO intcalcDTO) throws Exception {
					
		
		initInstintcalc(intcalcDTO);
		
		intcalcDTO = calcAmount(intcalcDTO);
		
		return intcalcDTO;
	}
	
	public void initInstintcalc(IntcalcDTO intcalcDTO) {
		readT5645(intcalcDTO.getChdrcoy());
		readT6633(intcalcDTO.getChdrcoy(), intcalcDTO.getCnttype().concat(intcalcDTO.getLoanType()), intcalcDTO.getLoanStartDate().intValue(), 
				intcalcDTO.getLoanStartDate().intValue());
		if("F".equals(t6633.getInttype())) {
			int date2 = datcon2.plusMonths(intcalcDTO.getLoanStartDate(), t6633.getMperiod());
			if (intcalcDTO.getInterestTo().intValue()<=date2) {
				return ;
			}
		}
		readT6633(intcalcDTO.getChdrcoy(), intcalcDTO.getCnttype().concat(intcalcDTO.getLoanType()), intcalcDTO.getInterestTo().intValue(), 
				intcalcDTO.getInterestTo().intValue());
	}
	
	public IntcalcDTO calcAmount(IntcalcDTO intcalcDTO) throws Exception{
		
		wsaaTotalInterest=BigDecimal.ZERO;
		if("P".equals(intcalcDTO.getLoanType())) {
			wsaaSacscode = t5645.getSacscodes().get(0);
			wsaaSacstype = t5645.getSacstypes().get(0);
			wsaaSign = t5645.getSigns().get(0);
		}else if("A".equals(intcalcDTO.getLoanType())) {
			wsaaSacscode = t5645.getSacscodes().get(1);
			wsaaSacstype = t5645.getSacstypes().get(1);
			wsaaSign = t5645.getSigns().get(1);
		}else if("E".equals(intcalcDTO.getLoanType())) {
			wsaaSacscode = t5645.getSacscodes().get(2);
			wsaaSacstype = t5645.getSacstypes().get(2);
			wsaaSign = t5645.getSigns().get(2);
		}else if("D".equals(intcalcDTO.getLoanType())) {
			wsaaSacscode = t5645.getSacscodes().get(3);
			wsaaSacstype = t5645.getSacstypes().get(3);
			wsaaSign = t5645.getSigns().get(3);
		}
		if(intcalcDTO.getLoanNumber().intValue()<10) {
			wsaaRldgacct = intcalcDTO.getChdrnum().concat("0").concat(String.valueOf(intcalcDTO.getLoanNumber().intValue()));
		}else {
			wsaaRldgacct = intcalcDTO.getChdrnum().concat(String.valueOf(intcalcDTO.getLoanNumber().intValue()));
		}
		List<Acmvpf> acmvpfList = acmvpfDAO.getAcmvpfList(wsaaRldgacct, wsaaSacscode, wsaaSacstype, intcalcDTO.getLastCaplsnDate().intValue(), intcalcDTO.getChdrcoy());
		int intDate1 = 0;
		
		if(acmvpfList == null || acmvpfList.size() == 0) {		
			return intcalcDTO;
		}
		int i=0;
		for(Acmvpf acmvpf : acmvpfList) {
			i++;
			if(acmvpf.getEffdate()<intcalcDTO.getInterestFrom()) {
				intDate1 = intcalcDTO.getInterestFrom();
			}else {
				intDate1 = acmvpf.getEffdate();
			}
			
			long datediff = datcon3.getDaysDifference(intDate1, intcalcDTO.getInterestTo().intValue());
			wsaaInterest = BigDecimal.ZERO;
			wsaaLoanCurrVal = BigDecimal.ZERO;
			if(!intcalcDTO.getLoanCurrency().equals(acmvpf.getOrigcurr())) {
				currencyConvert(intcalcDTO, acmvpf);
			}else {
				wsaaLoanCurrVal = acmvpf.getOrigamt();
			}
			
			wsaaInterest = wsaaLoanCurrVal.multiply(t6633.getIntRate().divide(new BigDecimal(100),5,BigDecimal.ROUND_DOWN)).multiply(new BigDecimal(datediff).divide(new BigDecimal(365), 5, BigDecimal.ROUND_DOWN));
			wsaaInterest = wsaaInterest.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
			if(wsaaSign.equals(acmvpf.getGlsign())) {
				wsaaTotalInterest = wsaaTotalInterest.add(wsaaInterest);
			}else {
				wsaaTotalInterest= wsaaTotalInterest.subtract(wsaaInterest);
			}

		}
		intcalcDTO.setLoanNumber(i);
		intcalcDTO.setInterestAmount(wsaaTotalInterest);
		return intcalcDTO;
	}
	
	protected void currencyConvert(IntcalcDTO intcalcDTO, Acmvpf acmvpf) throws IOException{
		
		conlinkInputDTO = new ConlinkInputDTO();
		conlinkInputDTO.setAmount(acmvpf.getOrigamt());
		conlinkInputDTO.setFromCurrency(acmvpf.getOrigcurr());
		conlinkInputDTO.setCashdate(99999999);
		conlinkInputDTO.setToCurrency(intcalcDTO.getLoanCurrency());
		conlinkInputDTO.setCompany(intcalcDTO.getChdrcoy());
		
		conlinkOutputDTO = xcvrt.executeCvrtFunction(conlinkInputDTO);
		
		if(conlinkOutputDTO == null) {
			fatalError("currencyConvert Error in Instintcalc subroutine");
		}else {
			wsaaLoanCurrVal = conlinkOutputDTO.getCalculatedAmount();
		}
	}
	
	protected void readT5645(String coy) {
		t5645=itempfService.readSmartTableByTrim(coy, "T5645", wsaaSubr, T5645.class);
	}
	
	protected void readT6633(String coy, String item, int frm, int to) {
		
		TableItem<T6633> t6633IO = itempfService.readItemByItemFrmTo(coy, "T6633", item,  CommonConstants.IT_ITEMPFX,
				frm, to, T6633.class);
		if(t6633IO == null) {
			t6633 = new T6633();
		}else {
			t6633 = t6633IO.getItemDetail();
		}
		
	}
	
	public void fatalError(String status) {
		throw new StopRunException(status);
	}
	
	
	
}
