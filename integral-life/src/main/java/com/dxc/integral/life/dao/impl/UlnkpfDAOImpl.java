package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.UlnkpfDAO;
import com.dxc.integral.life.dao.model.Ulnkpf;

/**
 * 
 * @author xma3
 *
 */
@Repository("ulnkpfDAO")
@Lazy
public class UlnkpfDAOImpl extends BaseDAOImpl implements UlnkpfDAO{
	
	@Override	
	public Ulnkpf getUlnkrnlRecord(Ulnkpf ulnkpf){
		StringBuilder sb = new StringBuilder("SELECT * FROM Ulnkpf WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ?  AND COVERAGE = ? AND RIDER=? AND PLNSFX=? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC,UNIQUE_NUMBER DESC");

		 return jdbcTemplate.queryForObject(sb.toString(), new Object[] {ulnkpf.getChdrcoy(), ulnkpf.getChdrnum(), ulnkpf.getLife(), ulnkpf.getCoverage(), ulnkpf.getRider(), ulnkpf.getPlanSuffix()},
					new BeanPropertyRowMapper<Ulnkpf>(Ulnkpf.class));
	}


}
