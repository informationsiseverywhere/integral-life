package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.ZorlnkDTO;

public interface Zorcompy {
	public ZorlnkDTO processZorcompy(ZorlnkDTO zorlnkDTO) throws Exception;
}
