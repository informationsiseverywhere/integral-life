package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.TrwppfDAO;
import com.dxc.integral.life.dao.model.Trwppf;

/**
 * 
 * @author xma3
 *
 */
@Repository("trwppfDAO")
@Lazy
public class TrwppfDAOImpl extends BaseDAOImpl implements TrwppfDAO {

	@Override
	public void insertTrwppfRecords(Trwppf trwppfList) {
		  
		StringBuffer sql = new StringBuffer("INSERT INTO TRWPPF(CHDRCOY,CHDRNUM,EFFDATE,TRANNO,VALIDFLAG,RDOCPFX,RDOCCOY,RDOCNUM,ORIGCURR,ORIGAMT,USRPRF,JOBNM,DATIME) ");
		sql.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
		jdbcTemplate.batchUpdate(sql.toString(),new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
            	return 1;
            }
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	ps.setString(1, trwppfList.getChdrcoy());
            	ps.setString(2, trwppfList.getChdrnum());
            	ps.setInt(3, trwppfList.getEffdate());
            	ps.setInt(4, trwppfList.getTranno());
            	ps.setString(5, trwppfList.getValidflag());
            	ps.setString(6, trwppfList.getRdocpfx());
            	ps.setString(7, trwppfList.getRdoccoy());
            	ps.setString(8, trwppfList.getRdocnum());
            	ps.setString(9, trwppfList.getOrigcurr());
            	ps.setBigDecimal(10, trwppfList.getOrigamt());
            	ps.setString(11, trwppfList.getUserProfile());
            	ps.setString(12, trwppfList.getJobName());
            	ps.setTimestamp(13,new Timestamp(System.currentTimeMillis()));
            }
		});

	}

}
