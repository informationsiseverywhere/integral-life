/* ********************  */
/*Author  :tsaxena3				  		*/
/*Date    :2019.04.17				*/
package com.dxc.integral.life.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Clexpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.GOTOInterface;
import com.dxc.integral.life.smarttable.pojo.Extprm;
import com.dxc.integral.life.smarttable.pojo.T5659;
import com.dxc.integral.life.smarttable.pojo.T5664;
import com.dxc.integral.life.smarttable.pojo.Th549;
import com.dxc.integral.life.smarttable.pojo.Th606;
import com.dxc.integral.life.smarttable.pojo.Th609;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("prmpm04Util")

public class Prmpm04Util {
	
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ClexpfDAO clexpfDAO;
	@Autowired
	private LextpfDAO lextpfDAO;
	
	private int sub;
	private BigDecimal modalFactor;
	private int count;
	private int roundNum;
	private int round10000;
	private int round1000;
	private int round100;
	private int round10;
	private int round1;
	private int roundDec;
	
	private int adjustedAge = 0;
	private int index = 0;
	private int discountAmt = 0;
	private int agerateTot = 0;
	
	private BigDecimal bap;
	private BigDecimal bip;
	private BigDecimal bapBuff;
	private BigDecimal mortRate;
	private BigDecimal mortFactor;
	private BigDecimal staffDiscount;
	private BigDecimal premiumAdjustTot;
	private BigDecimal ratesPerMillieTot;
	
	private String basicPremium = "";
	private String mortalityLoad = "";
	private String staffFlag = "";
	private BigDecimal lextOppc[] = new BigDecimal[10];
	private Integer lextZmortpct[] = new Integer[10];
	private String lextOpcda[] = new String[10];
	
	boolean isLoadingAva = false;
	
	private T5659 t5659IO;
	private T5664 t5664IO;
	private Th609 th609IO;
	private Th549 th549IO;
	private Th606 th606IO;
	private Extprm extprm;
	private Chdrpf chdrpf = null;
	private Clexpf clexpf = null;
	//private Lextpf lextpf = null;
	private List<Lextpf> lextpfList;
	private Iterator<Lextpf> lextpfIterator;
	private BigDecimal extLoading;
	private enum GotoLabel implements GOTOInterface {
		loopForAdjustedAge,
		checkT5664Insprm,
		DEFAULT,
		calcMortLoadings950,
		calcMortLoadings960,
		callSubroutine,
		exit950
	}
	public PremiumDTO processPrempm04(PremiumDTO premiumDTO) throws IOException, ParseException {
		premiumDTO.setStatuz("****");
		sub = 0;
		bapBuff = BigDecimal.ZERO;
		basicPremium = "N";
		initialize(premiumDTO);
		if (("****").equals(premiumDTO.getStatuz())) {
			staffDiscount(premiumDTO);
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO);
			premiumDTO.setAdjustageamt(bap);
		}
		if (("****").equals(premiumDTO.getStatuz())
				&& ("Y".equals(staffFlag))
				&& (staffDiscount.compareTo(BigDecimal.ZERO) != 0)
				&& "B".equals(th609IO.getIndic())) {
			
			bap = new BigDecimal(100).subtract(staffDiscount).divide(new BigDecimal(100).multiply(bap)).setScale(3,BigDecimal.ROUND_HALF_UP);
		
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			
			bap = ratesPerMillieTot.add(bap).setScale(2);
			premiumDTO.setRateadj(ratesPerMillieTot.setScale(2));
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			
			bap = bap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5664IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
			
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			mortalityLoadings(premiumDTO);
			bapBuff = bap;
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			
			for(sub = 0; sub < 8; sub++) {
				if (lextOppc[sub].compareTo(BigDecimal.ZERO) == -1) {
					bap = bap.multiply(lextOppc[sub]).divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
					isLoadingAva = true;
				}
			}
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if (("****").equals(premiumDTO.getStatuz())
				&& ("Y".equals(staffFlag))
				&& (staffDiscount.compareTo(BigDecimal.ZERO) != 0)
				&& "I".equals(th609IO.getIndic())) {
			
			bip = (new BigDecimal(100)).subtract(staffDiscount).divide(new BigDecimal(100)).multiply(bip).setScale(3,BigDecimal.ROUND_HALF_UP);
			
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			
			premiumDTO.setCalcPrem(premiumAdjustTot.add(premiumDTO.getCalcPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
			premiumDTO.setPremadj(premiumAdjustTot.setScale(2,BigDecimal.ROUND_HALF_UP));
			
		}
		basicPremium = "Y";
		if (("****").equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO);
			premiumDTO.setAdjustageamt(premiumDTO.getAdjustageamt().subtract(bap));
		}
		if (("****").equals(premiumDTO.getStatuz())
				&& ("Y".equals(staffFlag))
				&& (staffDiscount.compareTo(BigDecimal.ZERO) != 0)
				&& "B".equals(th609IO.getIndic())) {
			bap = new BigDecimal(100).subtract(staffDiscount).divide(new BigDecimal(100).multiply(bap)).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			
			bap = bap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5664IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
		
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if (("****").equals(premiumDTO.getStatuz())
				&& ("Y".equals(staffFlag))
				&& (staffDiscount.compareTo(BigDecimal.ZERO) != 0)
				&& "I".equals(th609IO.getIndic())) {
		
			bip = (new BigDecimal(100)).subtract(staffDiscount).divide(new BigDecimal(100)).multiply(bip).setScale(3,BigDecimal.ROUND_HALF_UP);
			
		}
		if (("****").equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		
		premiumDTO.setCalcLoaPrem(premiumDTO.getCalcPrem().subtract(premiumDTO.getCalcBasPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
		if(isLoadingAva == true)
			premiumDTO.setLoadper(premiumDTO.getCalcLoaPrem().subtract(premiumDTO.getAdjustageamt().add(ratesPerMillieTot.add(premiumAdjustTot))).setScale(2,BigDecimal.ROUND_HALF_UP));

		if (bapBuff.compareTo(BigDecimal.ZERO) == -1 && adjustedAge == 0) {
			if (premiumDTO.getCalcPrem().compareTo(premiumDTO.getCalcBasPrem()) == -1) {
				premiumDTO.setCalcPrem(premiumDTO.getCalcBasPrem());
				premiumDTO.setCalcLoaPrem((premiumDTO.getCalcPrem().add(bapBuff)).subtract(premiumDTO.getCalcBasPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
				if (premiumDTO.getCalcLoaPrem().compareTo(premiumDTO.getCalcPrem()) == 1) {
					premiumDTO.setCalcLoaPrem(bapBuff);
				}
			}
		}
		
		return premiumDTO;
	}
	
	protected void staffDiscount(PremiumDTO premiumDTO) throws IOException{
		chdrpf = chdrpfDAO.readRecordOfChdrpf(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum());
		if(chdrpf == null){
			throw new DataNoFoundException("Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table Chdrpf while processing contract ");
		}
		if (chdrpf.getCownnum() == null || "".equals(chdrpf.getCownnum())) {
			return;
		}
		clexpf = clexpfDAO.getClexpfRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy(), chdrpf.getCownnum());
		if (null != clexpf && !"Y".equals(clexpf.getRstaflag()) && !"".equals(chdrpf.getJownnum())){
			clexpf = clexpfDAO.getClexpfRecord(chdrpf.getCownpfx(), chdrpf.getCowncoy(), chdrpf.getJownnum());
		}
		if (null != clexpf && "Y".equals(clexpf.getRstaflag())) {
			Itempf itempf = itempfDAO.readSmartTableByTrimItem(premiumDTO.getChdrChdrcoy(), "TH609", chdrpf.getCnttype(),CommonConstants.IT_ITEMPFX);
			if(itempf == null){
				itempf = itempfDAO.readSmartTableByTrimItem(premiumDTO.getChdrChdrcoy(), "TH609", "***",CommonConstants.IT_ITEMPFX);
				staffDiscount = BigDecimal.ZERO;
			}else{
				th609IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th609.class);
				staffDiscount = th609IO.getPrcnt();
			}
			th609IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th609.class);
			staffDiscount = th609IO.getPrcnt();
		}
		staffFlag = clexpf == null ? "" : clexpf.getRstaflag();
		
	}

	protected void initialize(PremiumDTO premiumDTO) throws IOException{
		
		ratesPerMillieTot = BigDecimal.ZERO;
		premiumAdjustTot = BigDecimal.ZERO;
		bap = BigDecimal.ZERO;
		bip = BigDecimal.ZERO;
		adjustedAge = 0;
		discountAmt = 0;
		agerateTot = 0;
		premiumDTO.setFltmort(BigDecimal.ZERO);
		premiumDTO.setLoadper(BigDecimal.ZERO);
		sub = 0;
		for (sub = 0; sub < 8; sub ++){
			lextOppc[sub] = BigDecimal.ZERO;
			lextZmortpct[sub] = 0;
			lextOpcda[sub] = "0";
		}
		
		String wsaaT5664Key = premiumDTO.getCrtable().concat(premiumDTO.getMortcls()).concat(premiumDTO.getLsex());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "T5664", wsaaT5664Key, CommonConstants.IT_ITEMPFX, itemFrm);
		if(itempf == null){
			premiumDTO.setStatuz("F358");
			return;
		}else{
			t5664IO = new ObjectMapper().readValue(itempf.getGenareaj(), T5664.class);
		}
	}
	
	protected void basicAnnualPremium(PremiumDTO premiumDTO)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey(premiumDTO);
					readLext(premiumDTO);
				case loopForAdjustedAge: 
					loopForAdjustedAge(premiumDTO);
				case checkT5664Insprm: 
					checkT5664Insprm(premiumDTO);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void setupLextKey(PremiumDTO premiumDTO){
		if( "Y".equals(basicPremium)) {
			adjustedAge = premiumDTO.getLage();
			goTo(GotoLabel.loopForAdjustedAge);
		}
		lextpfList = lextpfDAO.getLextpfListRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(),
				premiumDTO.getCovrCoverage(), premiumDTO.getCovrRider());
	}
	protected void readLext(PremiumDTO premiumDTO){
		if(null == lextpfList || lextpfList.isEmpty()){
			goTo(GotoLabel.loopForAdjustedAge);
		}else{
			sub = 0;
			for(Lextpf lextIO : lextpfList){
				if ("2".equals(premiumDTO.getReasind())&& "1".equals(lextIO.getReasind())) {
					goTo(GotoLabel.loopForAdjustedAge);
				}
				if (!"2".equals(premiumDTO.getReasind())&& "2".equals(lextIO.getReasind())) {
					goTo(GotoLabel.loopForAdjustedAge);
				}
				if (lextIO.getExtcd() <= premiumDTO.getReRateDate()) {
					continue;
				}
				lextOppc[sub] = lextIO.getOppc();
				lextZmortpct[sub] = lextIO.getZmortpct();
				lextOpcda[sub] = lextIO.getOpcda();
				ratesPerMillieTot = ratesPerMillieTot.add(new BigDecimal(lextIO.getInsprm())) ; 
				premiumAdjustTot =  premiumAdjustTot.add(lextIO.getPremadj());
				agerateTot += lextIO.getAgerate();
				sub++;
			}
		}
	}	
	protected void loopForAdjustedAge(PremiumDTO premiumDTO){
		adjustedAge = agerateTot + premiumDTO.getLage();
	}
	
	
	protected void checkT5664Insprm(PremiumDTO premiumDTO) {

		if (adjustedAge < 0) {
			adjustedAge = 110;
		}
		if ((adjustedAge < 0)
		|| (adjustedAge > 110)) {
			premiumDTO.setStatuz("E107");
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (adjustedAge == 0) {
			if (t5664IO.getInsprem().equals(0)) {
				premiumDTO.setStatuz("E107");
			}
			else {
				bap = BigDecimal.valueOf(t5664IO.getInsprem());
			}
			return ;
		}

		if ((adjustedAge >= 100)
		&& (adjustedAge <= 110)) {
			index = adjustedAge-99;
			if (t5664IO.getInstprs().get(index) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				bap = BigDecimal.valueOf(t5664IO.getInstprs().get(index));
			}
		}
		else {
			if (t5664IO.getInsprms().get(adjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				bap = BigDecimal.valueOf(t5664IO.getInsprms().get(adjustedAge));
			}
		}
	
	}
	
	protected void volumeDiscountBap(PremiumDTO premiumDTO) throws IOException{
	
		String t5659Itemitem= t5664IO.getDisccntmeth().concat(premiumDTO.getCurrcode());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "T5659", t5659Itemitem,
				CommonConstants.IT_ITEMPFX,itemFrm);
		if(itempf == null){
			premiumDTO.setStatuz("F264");
			return;
		}
		t5659IO = new ObjectMapper().readValue(itempf.getGenareaj(), T5659.class);

		for(sub = 0; sub < 4 ; sub ++){
		if(premiumDTO.getSumin().compareTo(t5659IO.getVolbanfrs().get(sub)) == -1
					|| premiumDTO.getSumin().compareTo(t5659IO.getVolbantos().get(sub)) == 1){
				continue;
			}else{
				discountAmt = t5659IO.getVolbanles().get(sub).intValue();
			}
		}
		bap = bap.subtract(new BigDecimal(discountAmt)).setScale(2,BigDecimal.ROUND_HALF_UP);
		
	}

	protected void mortalityLoadings(PremiumDTO premiumDTO) throws IOException{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para(premiumDTO);
				case calcMortLoadings950: 
					calcMortLoadings950(premiumDTO);
					calcMortLoadings960(premiumDTO);
				case callSubroutine: 
					callSubroutine(premiumDTO);
				case exit950: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	private void para(PremiumDTO premiumDTO) {
		premiumDTO.setFltmort(BigDecimal.ZERO);
		mortalityLoad = "N";
		for (sub = 0; !(sub > 7 || "Y".equals(mortalityLoad)); sub++){
			if (lextZmortpct[sub] != 0) {
				mortalityLoad = "Y";
			}
		}
		if ("N".equals(mortalityLoad)) {
			goTo(GotoLabel.exit950);
		}
		sub = 0;
	}
	
		
	protected void calcMortLoadings950(PremiumDTO premiumDTO) throws  IOException{
		sub = sub + 1;
		if (sub > 8) {
			goTo(GotoLabel.exit950);
		}
		if (lextZmortpct[sub] == 0) {
			goTo(GotoLabel.calcMortLoadings950);
		}
		String th549Itemitem= premiumDTO.getCrtable().concat(String.valueOf(lextZmortpct[sub])).concat(premiumDTO.getLsex());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TH549", th549Itemitem,
				CommonConstants.IT_ITEMPFX,itemFrm);
		if(itempf == null){
			premiumDTO.setStatuz("HL27");
			goTo(GotoLabel.exit950);
		}
		th549IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th549.class);
		Lifepf lifepf = lifepfDAO.getLifeRecordByCurrfrom(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(), premiumDTO.getLifeJlife(), 99999999);
		if(lifepf == null){
			throw new DataNoFoundException("Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table LIFEPF while processing contract ");
		}
		String wsaaTh606Indic = "S".equals(lifepf.getSmoking())?th549IO.getIndcs().get(0):th549IO.getIndcs().get(1);
		String th606Itemitem = String.valueOf(premiumDTO.getDuration()).concat(premiumDTO.getCrtable()).concat(wsaaTh606Indic);
		itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TH606", th606Itemitem,
				CommonConstants.IT_ITEMPFX,itemFrm);
		if(itempf == null){
			premiumDTO.setStatuz("HL26");
			mortFactor = BigDecimal.ZERO;
			mortRate = BigDecimal.ZERO;
			goTo(GotoLabel.callSubroutine);
		}else{
			th606IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th606.class);
		}
	}
	protected void calcMortLoadings960(PremiumDTO premiumDTO){
		if (lextZmortpct[sub] == 0) {
			goTo(GotoLabel.calcMortLoadings950);
		}
		getValues(premiumDTO);
	}
	protected void getValues(PremiumDTO premiumDTO) {
	
		if (adjustedAge < 0) {
			adjustedAge = 110;
		}
		if ((adjustedAge < 0)
		|| (adjustedAge > 110)) {
			premiumDTO.setStatuz("E107");
			return;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (adjustedAge == 0) {
			if (th606IO.getInsprem() == 0) {
				premiumDTO.setStatuz("E107");
				return;
			}
			else {
				mortRate = new BigDecimal(th606IO.getInsprem());
				getModalFactor(premiumDTO);
			}
		}
		if ((adjustedAge >= 100)
		&& (adjustedAge <= 110)) {
			index = adjustedAge - 99;
			if (th606IO.getInstprs().get(index) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				mortRate = new BigDecimal(th606IO.getInstprs().get(index));
			}
		}
		else {
			if (th606IO.getInsprms().get(adjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				mortRate = new BigDecimal(th606IO.getInsprms().get(adjustedAge));
			}
		}
	}

	protected void getModalFactor(PremiumDTO premiumDTO){
		mortFactor = BigDecimal.ZERO;
		if ("01".equals(premiumDTO.getBillfreq())|| "00".equals(premiumDTO.getBillfreq())) {
			mortFactor = th606IO.getMfacty();
		}
		else {
			if ("02".equals(premiumDTO.getBillfreq())) {
				mortFactor = th606IO.getMfacthy();
			}
			else {
				if ("04".equals(premiumDTO.getBillfreq())) {
					mortFactor = th606IO.getMfactq();
				}
				else {
					if ("12".equals(premiumDTO.getBillfreq())) {
						mortFactor  = th606IO.getMfactm();
					}
					else {
						if ("13".equals(premiumDTO.getBillfreq())) {
							mortFactor = th606IO.getMfact4w();
						}
						else {
							if ("24".equals(premiumDTO.getBillfreq())) {
								mortFactor = th606IO.getMfacthm();
							}
							else {
								if ("26".equals(premiumDTO.getBillfreq())) {
									mortFactor = th606IO.getMfact2w();
								}
								else {
									if ("52".equals(premiumDTO.getBillfreq())) {
										mortFactor= th606IO.getMfactw();
									}
								}
							}
						}
					}
				}
			}
		}
		if (mortFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("F272");
		}
	}
	
	protected void callSubroutine(PremiumDTO premiumDTO) throws  IOException{
		

		for (count = 0; !(count > 5 || ("".equals(th549IO.getOpcdas().get(count)))); count++){
			if (lextOpcda[sub].equals(th549IO.getOpcdas().get(count))) {
				if(th549IO.getSubrtns().get(count).equalsIgnoreCase("EXTPPMIL")){
					extLoading = mortRate.multiply(mortFactor).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000))).setScale(4);
				}
				if(th549IO.getSubrtns().get(count).equalsIgnoreCase("EXTPDLVL")){
					extLoading = mortFactor.multiply(new BigDecimal(100)).multiply(th549IO.getExpfactor()).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000)))
							.multiply(new BigDecimal(lextZmortpct[sub]).divide(new BigDecimal(100))).setScale(5);
				}
				break;
			}
			else {
				if ("**".equals(th549IO.getOpcdas().get(count))) {
					if(th549IO.getSubrtns().get(count).equalsIgnoreCase("EXTPPMIL")){
						extLoading = mortRate.multiply(mortFactor).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000))).setScale(4);
					}
					if(th549IO.getSubrtns().get(count).equalsIgnoreCase("EXTPDLVL")){
						extLoading = mortFactor.multiply(new BigDecimal(100)).multiply(th549IO.getExpfactor()).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000)))
								.multiply(new BigDecimal(lextZmortpct[sub]).divide(new BigDecimal(100))).setScale(5);
					}
					break;
				}						
				}
			}
		bap = bap.add(extLoading).setScale(2,BigDecimal.ROUND_HALF_UP);
		premiumDTO.setFltmort(extLoading.divide(new BigDecimal(t5664IO.getPremUnit())));
		goTo(GotoLabel.calcMortLoadings950);
		
	}
	
	protected void instalmentPremium(PremiumDTO premiumDTO) {
		

		bip = BigDecimal.ZERO;
		modalFactor = BigDecimal.ZERO;
		if ("01".equals(premiumDTO.getBillfreq())|| "00".equals(premiumDTO.getBillfreq())) {
			modalFactor = t5664IO.getMfacty();
		}else {
			if ("02".equals(premiumDTO.getBillfreq())) {
				modalFactor = t5664IO.getMfacthy();
			}else {
				if ("04".equals(premiumDTO.getBillfreq())) {
					modalFactor = t5664IO.getMfactq();
				}else {
					if ("12".equals(premiumDTO.getBillfreq())) {
						modalFactor  = t5664IO.getMfactm();
					}else {
						if ("13".equals(premiumDTO.getBillfreq())) {
							modalFactor = t5664IO.getMfact4w();
						}else {
							if ("24".equals(premiumDTO.getBillfreq())) {
								modalFactor = t5664IO.getMfacthm();
							}else {
								if ("26".equals(premiumDTO.getBillfreq())) {
									modalFactor = t5664IO.getMfact2w();
								}else {
									if ("52".equals(premiumDTO.getBillfreq())) {
										modalFactor= t5664IO.getMfactw();
									}
								}
							}
						}
					}
				}
			}
		}
		if(modalFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("T070");
		}
		if (modalFactor.compareTo(BigDecimal.ZERO) == 0 && premiumDTO.getBillfreq().equals("26")) {
			premiumDTO.setStatuz("****");
		}else {
			bip = bap.multiply(modalFactor).setScale(4,BigDecimal.ROUND_HALF_UP);
		}
	}
	
	protected void rounding(PremiumDTO premiumDTO) {

		roundNum = bip.intValue();
		
		if ((t5659IO.getRndfact() == 1)
		|| (t5659IO.getRndfact() == 0)) {
			if (roundDec < .5) {
				roundDec = 0;
			}
			else {
				roundNum += 1;
				roundDec = 0;
			}
		}
		if (t5659IO.getRndfact() == 10) {
			if (round1 < 5) {
				round1 = 0;
			}
			else {
				roundNum += 10;
				round1 = 0;
			}
		}
		if (t5659IO.getRndfact() == 100) {
			if (round10 < 50) {
				round10 = 0;
			}
			else {
				roundNum += 100;
				round10 = 0;
			}
		}
		if (t5659IO.getRndfact() == 1000) {
			if (round100 < 500) {
				round100 = 0;
			}
			else {
				roundNum += 1000;
				round100 = 0;
			}
		}
		if (t5659IO.getRndfact() == 10000) {
			if (round1000 < 5000) {
				round1000 = 0;
			}
			else {
				roundNum += 10000;
				round1000 = 0;
			}
		}
		if (t5659IO.getRndfact() == 100000) {
			if (round10000 < 50000) {
				round10000 = 0;
			}
			else {
				roundNum += 100000;
				round10000 = 0;
			}
		}
	

		bip = new BigDecimal(roundNum);
		if (t5664IO.getPremUnit() == 0) {
			premiumDTO.setCalcPrem(bip);
		}
		else {
			if ("Y".equals(basicPremium)) {
				premiumDTO.setCalcBasPrem(bip.divide(new BigDecimal(t5664IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
			else {
				premiumDTO.setCalcPrem(bip.divide(new BigDecimal(t5664IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
		}
	}
	public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}
}
