package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LinspfDAO;
import com.dxc.integral.life.dao.model.Linspf;
/**
 * @author wli31
 */
@Repository("linspfDAO")
@Lazy
public class LinspfDAOImpl extends BaseDAOImpl implements LinspfDAO {

	@Override
	public void insertLinspfRecords(List<Linspf> linsList) {
		List<Linspf> tempLinspfList = linsList;   
		StringBuilder sb = new StringBuilder("");
		sb.append("insert into linspf(chdrcoy, chdrnum, cntcurr, validflag, branch, instfrom, instto, instamt01, instamt02, instamt03, instamt04, instamt05, instamt06,");
		sb.append(" instfreq, instjctl, billchnl, payflag, dueflg, transcode, cbillamt, billcurr, mandref, billcd, payrseqno, taxrelmth, acctmeth, usrprf, jobname, datime,  "); 
		sb.append(" jobnm) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempLinspfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, linsList.get(i).getChdrcoy());
				ps.setString(2, linsList.get(i).getChdrnum());
			    ps.setString(3, linsList.get(i).getCntcurr());
			    ps.setString(4, linsList.get(i).getValidflag());
			    ps.setString(5, linsList.get(i).getBranch());			    
				ps.setInt(6, linsList.get(i).getInstfrom());
				ps.setInt(7, linsList.get(i).getInstto());
				ps.setBigDecimal(8, linsList.get(i).getInstamt01());
				ps.setBigDecimal(9, linsList.get(i).getInstamt02());
				ps.setBigDecimal(10, linsList.get(i).getInstamt03());
				ps.setBigDecimal(11, linsList.get(i).getInstamt04());
				ps.setBigDecimal(12, linsList.get(i).getInstamt05());
				ps.setBigDecimal(13, linsList.get(i).getInstamt06());	
			    ps.setString(14, linsList.get(i).getInstfreq());				
			    ps.setString(15, linsList.get(i).getInstjctl()); 				    
			    ps.setString(16, linsList.get(i).getBillchnl());	
			    ps.setString(17, linsList.get(i).getPayflag());
				ps.setString(18, linsList.get(i).getDueflg());
				ps.setString(19, linsList.get(i).getTranscode());
			    ps.setBigDecimal(20, linsList.get(i).getCbillamt());	
			    ps.setString(21, linsList.get(i).getBillcurr());			    
			    ps.setString(22, linsList.get(i).getMandref());	
				ps.setInt(23, linsList.get(i).getBillcd());				
				ps.setInt(24, linsList.get(i).getPayrseqno());						
			    ps.setString(25, linsList.get(i).getTaxrelmth());	
			    ps.setString(26, linsList.get(i).getAcctmeth());
			    ps.setString(27, linsList.get(i).getUserProfile());
			    ps.setString(28, linsList.get(i).getJobName());
			    ps.setTimestamp(29, new Timestamp(System.currentTimeMillis()));
			    ps.setString(30, linsList.get(i).getJobName());
            }  
      }); 
	}
	
	@Override
	public void updateLinspfRecords(List<Linspf> linsList) {
		List<Linspf> tempLinspfList = linsList;   
		String sql = "UPDATE LINSPF SET PAYFLAG='P',JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
	    jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempLinspfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, linsList.get(i).getJobName());
				ps.setString(2, linsList.get(i).getUserProfile());
			    ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			    ps.setLong(4, linsList.get(i).getUnique_number());

            }  
      }); 
	}
	
	@Override
	public List<Linspf> searchLinspfRecords(String chdrcoy, String chdrnum) {
		String sql = "select * from linspf where instto<=99999999 and chdrcoy=? and chdrnum = ?";
		return jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum }, new BeanPropertyRowMapper<Linspf>(Linspf.class));
	}
	
	public Linspf readLinspfRecord(String chdrcoy,String chdrnum, int instFrom){
		String sql = "select * from linspf  where payflag<>'P' and chdrcoy = ? and chdrnum = ? and instFrom=?  order by chdrcoy,chdrnum";
		return jdbcTemplate.queryForObject(sql, new Object[] { chdrcoy, chdrnum,instFrom},
					new BeanPropertyRowMapper<Linspf>(Linspf.class));
	}
	
	@Override
	public List<Linspf> searchLinsrevRecords(String chdrcoy, String chdrnum){
		String sql = "SELECT * FROM LINSPF WHERE CHDRCOY=? AND CHDRNUM=? AND PAYFLAG <> 'P' ORDER BY CHDRCOY, CHDRNUM, INSTTO, UNIQUE_NUMBER";
		return jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum }, new BeanPropertyRowMapper<Linspf>(Linspf.class));
	}
	
	@Override
	public int deleteLinspf(long unique_number) {
		String sql = "DELETE FROM LINSPF WHERE UNIQUE_NUMBER=?";
		return jdbcTemplate.update(sql, new Object[] {unique_number});
	}
}
