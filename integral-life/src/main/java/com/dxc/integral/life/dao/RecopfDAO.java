package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Recopf;

public interface RecopfDAO {

	public List<Recopf> getRecoclmRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, int seqno);
	
	int insertRecopfRecord(Recopf recopf);
}
