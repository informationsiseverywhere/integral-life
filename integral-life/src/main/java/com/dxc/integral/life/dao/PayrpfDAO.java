package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Payrpf;

/**
 * @author wli31
 */
public interface PayrpfDAO {
	public Payrpf getPayrRecord(String chdrcoy, String chdrnum);

	public List<Payrpf> searchPayrRecord(String chdrcoy, String chdrnum);

	public void insertPayrRecords(List<Payrpf> payrpfList);

	public void updatePayrpfRecords(List<Payrpf> payrpfList);

	int updatePayrRecord(Payrpf payrpf);

	public int deletePayrpfRecord(String chdrcoy, String chdrnum, int payrseqno);

	public Payrpf getPayrRecord(String chdrcoy, String chdrnum, int payrseqno);

	public int updatePayrpfRecord(String chdrcoy, String chdrnum, int payrseqno, String validflag);

	void updatePayrPstCde(List<Payrpf> payrpfList);
	
	public int updatePayrpfRecord(Payrpf payrpf);
	
	public List<Payrpf> searchPayrlifRecord(String chdrcoy, String chdrnum);
}
