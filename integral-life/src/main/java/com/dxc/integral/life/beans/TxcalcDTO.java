package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;



public class TxcalcDTO {

  	private String function;
  	private String statuz;
  	private String chdrcoy;
  	private String chdrnum;
  	private String life;
  	private String coverage;
  	private String rider;
  	private int planSuffix;
  	private String crtable;
  	private String cnttype;
  	private String register;
  	private String taxrule;
  	private String rateItem;
  	private String cntTaxInd;
  	private BigDecimal amountIn;
  	private String transType;
  	private int effdate;
  	private int tranno;
  	private int jrnseq;
  	private String batckey;
  	private String ccy;
  	private String output;
  	private List<String> taxTypes;
  	private List<BigDecimal> taxAmts;
  	private List<String> taxAbsorbs;
  	private List<String> taxSacstyps;
  	private String txcode;
  	private String language;
  	private String taxType01;
  	private String taxType02;
  	private BigDecimal taxAmt01;
  	private BigDecimal taxAmt02;
  	private String taxAbsorb01;
  	private String taxAbsorb02;
  	private String taxSacstyp01;
  	private String taxSacstyp02;
  	private String item;
  	private String vpmtaxrule;
  	private String batctrcde;
  	public TxcalcDTO(){
  	  	this.function = "";
  	  	this.statuz = "";
  	  	this.chdrcoy = "";
  	  	this.chdrnum = "";
  	  	this.life = "";
  	  	this.coverage = "";
  	  	this.rider = "";
  	  	this.planSuffix = 0;
  	  	this.crtable = "";
  	  	this.cnttype = "";
  	  	this.register = "";
  	  	this.taxrule = "";
  	  	this.rateItem = "";
  	  	this.cntTaxInd = "";
  	  	this.amountIn = BigDecimal.ZERO;
  	  	this.transType = "";
  	  	this.effdate = 0;
  	  	this.tranno = 0;
  	  	this.jrnseq = 0;
  	  	this.batckey = "";
  	  	this.ccy = "";
  	  	this.output = "";
  	  	this.taxTypes = new ArrayList<String>();
  	  	this.taxAmts= new ArrayList<BigDecimal>();
  	  	this.taxAbsorbs= new ArrayList<String>();
  		this.taxSacstyps= new ArrayList<String>();
  	  	this.txcode = "";
  	  	this.language = "";
  	  	this.taxType01 = "";
  	  	this.taxType02 = "";
  	  	this.taxAmt01 = BigDecimal.ZERO;
  	  	this.taxAmt02 = BigDecimal.ZERO;
  	  	this.taxAbsorb01 = "";
  	  	this.taxAbsorb02 = "";
  	  	this.taxSacstyp01 = "";
  	  	this.taxSacstyp02 = "";
  	  	this.item = "";
  	  	this.vpmtaxrule = "";
  	  	this.batctrcde = "";
  	}

	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public String getTaxrule() {
		return taxrule;
	}
	public void setTaxrule(String taxrule) {
		this.taxrule = taxrule;
	}
	public String getRateItem() {
		return rateItem;
	}
	public void setRateItem(String rateItem) {
		this.rateItem = rateItem;
	}
	public String getCntTaxInd() {
		return cntTaxInd;
	}
	public void setCntTaxInd(String cntTaxInd) {
		this.cntTaxInd = cntTaxInd;
	}
	public BigDecimal getAmountIn() {
		return amountIn;
	}
	public void setAmountIn(BigDecimal amountIn) {
		this.amountIn = amountIn;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}
	public String getBatckey() {
		return batckey;
	}
	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	public List<String> getTaxTypes() {
		return taxTypes;
	}
	public void setTaxTypes(List<String> taxTypes) {
		this.taxTypes = taxTypes;
	}
	public List<BigDecimal> getTaxAmts() {
		return taxAmts;
	}
	public void setTaxAmts(List<BigDecimal> taxAmts) {
		this.taxAmts = taxAmts;
	}
	public List<String> getTaxAbsorbs() {
		return taxAbsorbs;
	}
	public void setTaxAbsorbs(List<String> taxAbsorbs) {
		this.taxAbsorbs = taxAbsorbs;
	}
	public List<String> getTaxSacstyps() {
		return taxSacstyps;
	}
	public void setTaxSacstyps(List<String> taxSacstyps) {
		this.taxSacstyps = taxSacstyps;
	}
	public String getTxcode() {
		return txcode;
	}
	public void setTxcode(String txcode) {
		this.txcode = txcode;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTaxType01() {
		return taxType01;
	}
	public void setTaxType01(String taxType01) {
		this.taxType01 = taxType01;
	}
	public String getTaxType02() {
		return taxType02;
	}
	public void setTaxType02(String taxType02) {
		this.taxType02 = taxType02;
	}
	public BigDecimal getTaxAmt01() {
		return taxAmt01;
	}
	public void setTaxAmt01(BigDecimal taxAmt01) {
		this.taxAmt01 = taxAmt01;
	}
	public BigDecimal getTaxAmt02() {
		return taxAmt02;
	}
	public void setTaxAmt02(BigDecimal taxAmt02) {
		this.taxAmt02 = taxAmt02;
	}
	public String getTaxAbsorb01() {
		return taxAbsorb01;
	}
	public void setTaxAbsorb01(String taxAbsorb01) {
		this.taxAbsorb01 = taxAbsorb01;
	}
	public String getTaxAbsorb02() {
		return taxAbsorb02;
	}
	public void setTaxAbsorb02(String taxAbsorb02) {
		this.taxAbsorb02 = taxAbsorb02;
	}
	public String getTaxSacstyp01() {
		return taxSacstyp01;
	}
	public void setTaxSacstyp01(String taxSacstyp01) {
		this.taxSacstyp01 = taxSacstyp01;
	}
	public String getTaxSacstyp02() {
		return taxSacstyp02;
	}
	public void setTaxSacstyp02(String taxSacstyp02) {
		this.taxSacstyp02 = taxSacstyp02;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getVpmtaxrule() {
		return vpmtaxrule;
	}
	public void setVpmtaxrule(String vpmtaxrule) {
		this.vpmtaxrule = vpmtaxrule;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}



}
