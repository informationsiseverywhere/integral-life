package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class RnlAllRec {

  	private String statuz;
  	private String company;
  	private String chdrnum;
  	private String life;
  	private String coverage;
  	private String rider;
  	private String planSuffix;
  	private String billfreq;
  	private int billcd;
  	private String tranno;
  	private String sacscode;
  	private String sacstyp;
  	private String genlcde ;
  	private String cntcurr;
  	private String cnttype;
  	private String batchkey;
  	private String contkey;
  	private String batcpfx;
  	private String batccoy;
  	private String batcbrn;
  	private int batcactyr;
  	private int batcactmn;
  	private String batctrcde;
  	private String batcbatch;
  	private int effdate;
  	private int user;
  	private String crtable;
  	private int crdate;
  	private int moniesDate;
  	private String anbAtCcd;
  	private String termLeftToRun;
  	private BigDecimal covrInstprem = new BigDecimal(0);
  	private String sacscode02;
  	private String sacstyp02;
  	private String genlcde02;
  	private int duedate;
  	private BigDecimal totrecd = new BigDecimal(0);
  	private String language;
  	private String filler;
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(String planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getTranno() {
		return tranno;
	}
	public void setTranno(String tranno) {
		this.tranno = tranno;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getGenlcde() {
		return genlcde;
	}
	public void setGenlcde(String genlcde) {
		this.genlcde = genlcde;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getBatchkey() {
		return batchkey;
	}
	public void setBatchkey(String batchkey) {
		this.batchkey = batchkey;
	}
	public String getContkey() {
		return contkey;
	}
	public void setContkey(String contkey) {
		this.contkey = contkey;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getCrdate() {
		return crdate;
	}
	public void setCrdate(int crdate) {
		this.crdate = crdate;
	}
	public int getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(int moniesDate) {
		this.moniesDate = moniesDate;
	}
	public String getAnbAtCcd() {
		return anbAtCcd;
	}
	public void setAnbAtCcd(String anbAtCcd) {
		this.anbAtCcd = anbAtCcd;
	}
	public String getTermLeftToRun() {
		return termLeftToRun;
	}
	public void setTermLeftToRun(String termLeftToRun) {
		this.termLeftToRun = termLeftToRun;
	}
	public BigDecimal getCovrInstprem() {
		return covrInstprem;
	}
	public void setCovrInstprem(BigDecimal covrInstprem) {
		this.covrInstprem = covrInstprem;
	}
	public String getSacscode02() {
		return sacscode02;
	}
	public void setSacscode02(String sacscode02) {
		this.sacscode02 = sacscode02;
	}
	public String getSacstyp02() {
		return sacstyp02;
	}
	public void setSacstyp02(String sacstyp02) {
		this.sacstyp02 = sacstyp02;
	}
	public String getGenlcde02() {
		return genlcde02;
	}
	public void setGenlcde02(String genlcde02) {
		this.genlcde02 = genlcde02;
	}
	public int getDuedate() {
		return duedate;
	}
	public void setDuedate(int duedate) {
		this.duedate = duedate;
	}
	public BigDecimal getTotrecd() {
		return totrecd;
	}
	public void setTotrecd(BigDecimal totrecd) {
		this.totrecd = totrecd;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
}
