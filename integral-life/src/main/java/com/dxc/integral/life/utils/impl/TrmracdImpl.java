package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.RexpupdDTO;
import com.dxc.integral.life.beans.TrmracdDTO;
import com.dxc.integral.life.dao.RacdpfDAO;
import com.dxc.integral.life.dao.model.Racdpf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5446;
import com.dxc.integral.life.smarttable.pojo.T5448;
import com.dxc.integral.life.utils.Rexpupd;
import com.dxc.integral.life.utils.Trmracd;

/**
 * Termination of Reassurance Cessions Subroutine.
 * 
 * @author fwang3
 *
 */
@Service("trmracd")
@Lazy
public class TrmracdImpl extends BaseDAOImpl implements Trmracd {

	private static final String T5446 = "T5446";
	private static final String T5448 = "T5448";

	@Autowired
	private ItempfService itempfService;
	@Autowired
	private RacdpfDAO racdpfDAO;
	@Autowired
	private Rexpupd rexpupd;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private Xcvrt xcvrt;
	
	private TrmracdDTO trmracdDTO;
	private Racdpf racdrskIO;
	
	
	@Override
	public void executeTRMR(TrmracdDTO trmracdDTO) throws IOException {
		before();
		writeValidflag4Racdrsk();
	}
	
	@Override
	public void executeCHGR(TrmracdDTO trmracdDTO) throws IOException {
		before();
		if (trmracdDTO.getNewRaAmt().compareTo(BigDecimal.ZERO)>0) {
			writeValidflag3Racdrsk();
		}
	}
	
	protected void before() throws IOException {
		readT5448();
		terminateRacdrsk();
		updateReassurerExposure();
	}

	protected void readT5448() {
		String item = trmracdDTO.getCnttype() + trmracdDTO.getCrtable();
		T5448 t5448 = itempfService.readSmartTableByTrim(trmracdDTO.getChdrcoy(), T5448, item, trmracdDTO.getCrrcd(), T5448.class);
		if (null == t5448) {
			throw new ItemNotfoundException("Item " + item + " not found in Table T5448");
		}
	}

	protected void terminateRacdrsk() {
		racdrskIO = new Racdpf();
		racdrskIO.setChdrcoy(trmracdDTO.getChdrcoy());
		racdrskIO.setChdrnum(trmracdDTO.getChdrnum());
		racdrskIO.setLife(trmracdDTO.getLife());
		racdrskIO.setCoverage(trmracdDTO.getCoverage());
		racdrskIO.setRider(trmracdDTO.getRider());
		racdrskIO.setPlnsfx(trmracdDTO.getPlanSuffix());
		racdrskIO.setSeqno(trmracdDTO.getSeqno());
		racdrskIO.setLrkcls(trmracdDTO.getLrkcls());

		racdrskIO.setValidflag("2");
		racdrskIO.setCurrto(trmracdDTO.getEffdate());

		racdrskIO = racdpfDAO.updateRacd(racdrskIO);
		if (racdrskIO == null) {
			throw new StopRunException("Racdpf not found");
		}
	}

	protected void updateReassurerExposure() throws IOException {
		RexpupdDTO rexpupdrec = new RexpupdDTO();
		rexpupdrec.setChdrcoy(trmracdDTO.getChdrcoy());
		rexpupdrec.setChdrnum(trmracdDTO.getChdrnum());
		rexpupdrec.setLife(trmracdDTO.getLife());
		rexpupdrec.setCoverage(trmracdDTO.getCoverage());
		rexpupdrec.setRider(trmracdDTO.getRider());
		rexpupdrec.setPlanSuffix(trmracdDTO.getPlanSuffix());
		rexpupdrec.setTranno(trmracdDTO.getTranno());
		rexpupdrec.setClntcoy(trmracdDTO.getClntcoy());
		rexpupdrec.setL1Clntnum(trmracdDTO.getL1Clntnum());
		if (!trmracdDTO.getJlife().equals("00")) {
			rexpupdrec.setL2Clntnum(trmracdDTO.getL2Clntnum());
		}
		rexpupdrec.setReassurer(racdrskIO.getRasnum());
		rexpupdrec.setArrangement(racdrskIO.getRngmnt());
		rexpupdrec.setSumins(racdrskIO.getRaamount());
		rexpupdrec.setCestyp(racdrskIO.getCestype());
		rexpupdrec.setEffdate(trmracdDTO.getEffdate());
		rexpupdrec.setRiskClass(racdrskIO.getLrkcls());
		rexpupdrec.setCurrency(readT5446().getCurrcode());
		rexpupdrec.setBatctrcde(trmracdDTO.getBatctrcde());
		if (!rexpupdrec.getCurrency().equals(racdrskIO.getCurrcode())) {
			ConlinkInputDTO conlinkInputDTO = new ConlinkInputDTO();
			conlinkInputDTO.setAmount(rexpupdrec.getSumins());
			conlinkInputDTO.setFromCurrency(racdrskIO.getCurrcode());;
			conlinkInputDTO.setToCurrency(rexpupdrec.getCurrency());
			rexpupdrec.setSumins(callXcvrt(conlinkInputDTO, rexpupdrec.getCurrency()));
		}
		rexpupd.decrFunction(rexpupdrec);
	}

	protected T5446 readT5446() {
		T5446 t5446 = itempfService.readSmartTableByTrim(trmracdDTO.getChdrcoy(), T5446, racdrskIO.getLrkcls(),
				CommonConstants.MAXDATE, T5446.class);
		if (null == t5446) {
			throw new ItemNotfoundException("Item " + racdrskIO.getLrkcls() + " not found in Table T5446");
		}
		return t5446;
	}

	protected BigDecimal callXcvrt(ConlinkInputDTO conlinkInputDTO, String currency) throws IOException {
		conlinkInputDTO.setCashdate(trmracdDTO.getEffdate());
		conlinkInputDTO.setCompany(trmracdDTO.getChdrcoy());
		ConlinkOutputDTO outputDto = xcvrt.executeRealFunction(conlinkInputDTO);
		ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();
		zrdecplcDTO.setAmountIn(outputDto.getCalculatedAmount());
		return callRounding(zrdecplcDTO, currency);
	}

	protected void writeValidflag3Racdrsk() {
		racdrskIO.setRaamount(trmracdDTO.getNewRaAmt());
		racdrskIO.setValidflag("3");
		racdrskIO.setCurrfrom(trmracdDTO.getEffdate());
		racdrskIO.setCurrto(CommonConstants.MAXDATE);
		racdrskIO.setTranno(trmracdDTO.getTranno());
		racdrskIO.setReasper(BigDecimal.ZERO);
		racdrskIO.setOvrdind("");
		racdpfDAO.insertRacd(racdrskIO);
	}

	protected void writeValidflag4Racdrsk() {
		racdrskIO.setRecovamt(trmracdDTO.getRecovamt());
		racdrskIO.setValidflag("4");
		racdrskIO.setCurrfrom(trmracdDTO.getEffdate());
		racdrskIO.setCurrto(CommonConstants.MAXDATE);
		racdrskIO.setTranno(trmracdDTO.getTranno());
		racdpfDAO.insertRacd(racdrskIO);
	}

	protected BigDecimal callRounding(ZrdecplcDTO zrdecplcDTO, String currency) throws IOException {
		zrdecplcDTO.setCompany(trmracdDTO.getChdrcoy());
		zrdecplcDTO.setCurrency(currency);
		zrdecplcDTO.setBatctrcde(trmracdDTO.getBatctrcde());
		return zrdecplc.convertAmount(zrdecplcDTO);
	}

}
