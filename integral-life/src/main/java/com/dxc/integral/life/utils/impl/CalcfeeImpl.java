package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.life.beans.OvrdueDTO;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T6651;
import com.dxc.integral.life.utils.Calcfee;

@Service("calcfee")
@Lazy
public class CalcfeeImpl implements Calcfee{
	
	@Autowired
	private ItempfService itempfService;
    private String wsaaGotFreq;


	
	public OvrdueDTO processCalcfee(OvrdueDTO ovrdueDTO) {
		
		ovrdueDTO.setStatuz("****");
		ovrdueDTO.setPupfee(BigDecimal.ZERO);
		if (ovrdueDTO.getPumeth()==null || "".equals(ovrdueDTO.getPumeth().trim())) {
			ovrdueDTO.setPupfee(BigDecimal.ZERO);
			return ovrdueDTO;
		}
		return getCalcFee(ovrdueDTO);
	}
	
	public OvrdueDTO getCalcFee(OvrdueDTO ovrdueDTO) {
		TableItem<T6651> t6651 =itempfService.readItemByItemFrm(ovrdueDTO.getChdrcoy(), "T6651", ovrdueDTO.getPumeth().concat(ovrdueDTO.getCntcurr()),  CommonConstants.IT_ITEMPFX, ovrdueDTO.getBtdate(), T6651.class);
	    if(t6651 == null) {
	    	dbError("MRNF");
	    	ovrdueDTO.setStatuz("MRNF");
	    	return ovrdueDTO;
	    }
	    T6651 t6651IO = t6651.getItemDetail(); 
	    wsaaGotFreq="N";
		int wsaaInd=getFreq(ovrdueDTO);				
		if (!wsaaGotFreq.trim().equals("Y")) {
			dbError("I036");
	    	ovrdueDTO.setStatuz("I036");
	    	return ovrdueDTO;
		}
		if ((t6651IO.getPuffamts().get(wsaaInd).compareTo(BigDecimal.ZERO)==0)
		&& (t6651IO.getFeepcs().get(wsaaInd).compareTo(BigDecimal.ZERO)==0)) {
			return ovrdueDTO;
		}
		if ((t6651IO.getPuffamts().get(wsaaInd).compareTo(BigDecimal.ZERO)!=0)) {
			ovrdueDTO.setPupfee(t6651IO.getPuffamts().get(wsaaInd));
			return ovrdueDTO;
		}
		
		if (!isNumeric(ovrdueDTO.getBillfreq())) {
			
			dbError("I037");
			ovrdueDTO.setStatuz("I037");
	    	return ovrdueDTO;
		}
		BigDecimal wsaaBillfreq= new BigDecimal(ovrdueDTO.getBillfreq());
		ovrdueDTO.setPupfee(ovrdueDTO.getInstprem().multiply(wsaaBillfreq).multiply(t6651IO.getFeepcs().get(wsaaInd)).divide(new BigDecimal(100)));

		if (t6651IO.getPufeemins().get(wsaaInd).compareTo(BigDecimal.ZERO)==0
				&& t6651IO.getPufeemaxs().get(wsaaInd).compareTo(BigDecimal.ZERO)==0) {
			      return ovrdueDTO;
				}
				if (ovrdueDTO.getPupfee().compareTo(t6651IO.getPufeemins().get(wsaaInd))>0) {
					ovrdueDTO.setPupfee(t6651IO.getPufeemins().get(wsaaInd));
				}
				else {
					if (ovrdueDTO.getPupfee().compareTo(t6651IO.getPufeemaxs().get(wsaaInd))>0) {
						ovrdueDTO.setPupfee(t6651IO.getPufeemaxs().get(wsaaInd));
					}
				}
	    
	    return ovrdueDTO;
	}
	
	public int getFreq(OvrdueDTO ovrdueDTO) {
		int wsaaInd = 0;
		String[] wsaaFreq = {"00","01", "02", "04", "12"};
		while ( !(wsaaGotFreq.trim().equals("Y") || wsaaInd==4)) {
			
			if (wsaaFreq[wsaaInd].equals(ovrdueDTO.getBillfreq())) {
				wsaaGotFreq="Y";
			}
			wsaaInd++;
		}
		
		if(wsaaInd<4) {
			wsaaInd--;
		}
		
		return wsaaInd;
	}
	
	public static boolean isNumeric(String str){
	    Pattern pattern = Pattern.compile("[0-9]*");
	    return pattern.matcher(str).matches();   
	}
	
	private void dbError(String status) {
		throw new StopRunException(status);
	}
}
