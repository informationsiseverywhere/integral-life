package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.T1693;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.LifsttrDTO;
import com.dxc.integral.life.beans.OverbillDTO;
import com.dxc.integral.life.beans.ReverseDTO;
import com.dxc.integral.life.dao.LinspfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PtrnpfDAO;
import com.dxc.integral.life.dao.model.Linspf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.dao.model.Ptrnpf;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T3699;
import com.dxc.integral.life.smarttable.pojo.T6626;
import com.dxc.integral.life.smarttable.pojo.T6654;
import com.dxc.integral.life.smarttable.pojo.T6661;
import com.dxc.integral.life.utils.Lifsttr;
import com.dxc.integral.life.utils.Overbill;
import com.dxc.integral.life.utils.Revbling;


/**
 * OVERDUE PROCESSING - BTDATE CORRECTION PROCESSING 
 *  
 * @author fwang3
 *
 */
@Service("overbill")
@Lazy
public class OverbillImpl implements Overbill {

	private String wsaaOverflow = "Y";
	private String wsaaCollection = "";
	private String wsaaIssue = "";
	private String wsaaStop = "N";
	private String wsaaBillTran = "N";
	private int wsaaToday;
	private String wsaaNoLinsFound = "";
	private int wsaaSub;

//	private String repdate = "";
	private String company = "";	
	private String companynm = "";
//	private String sdate = "";
	

	private String chdrnum = "";
	private String batctrcde = "";
	private String trandesc = "";
	private int tranno;
//	private String effdate = "";
	
	private String wsaaChdrAplsupr;
	private String wsaaChdrBillsupr;
	private String wsaaChdrCommsupr;
	private String wsaaChdrNotssupr;
	private String wsaaChdrRnwlsupr;
	private int wsaaChdrAplspfrom;
	private int wsaaChdrBillspfrom;
	private int wsaaChdrCommspfrom;
	private int wsaaChdrNotsspfrom;
	private int wsaaChdrRnwlspfrom;
	private int wsaaChdrAplspto;
	private int wsaaChdrBillspto;
	private int wsaaChdrCommspto;
	private int wsaaChdrNotsspto;
	private int wsaaChdrRnwlspto;
	private String wsaaChdrCownpfx;
	private String wsaaChdrCowncoy;
	private String wsaaChdrCownnum;
	private String wsaaChdrJownnum;
	private int wsaaChdrPolsum ;
	private String wsaaChdrDesppfx;
	private String wsaaChdrDespcoy;
	private String wsaaChdrDespnum;
	private String wsaaChdrAgntpfx;
	private String wsaaChdrAgntcoy;
	private String wsaaChdrAgntnum;
	private String wsaaPayrAplsupr;
	private String wsaaPayrBillsupr;
	private String wsaaPayrNotssupr;
	private int wsaaPayrAplspfrom;
	private int wsaaPayrBillspfrom;
	private int wsaaPayrNotsspfrom;
	private int wsaaPayrAplspto;
	private int wsaaPayrBillspto;
	private int wsaaPayrNotsspto;
	private int wsaaPayrBillcd;

//	private FixedLengthStringData wsaaThreeYears = new FixedLengthStringData(1116);
//	private FixedLengthStringData wsaa3Year = new FixedLengthStringData(1116).isAPartOf(wsaaThreeYears, 0);
//	private FixedLengthStringData wsaa3YearR = new FixedLengthStringData(1116).isAPartOf(wsaa3Year, 0, REDEFINE);
//	private FixedLengthStringData[] wsaa3YearR1 = FLSArrayPartOfStructure(36, 31, wsaa3YearR, 0);
	private String[][] wsaa3Dd = new String[36][31];

	private int wsaaLastYear;
	private int wsaaThisYear;
	private int wsaaNextYear;
//	private int lastStart;
//	private int thisStart = 12;
//	private int nextStart = 24;
	private int wsaaCountMm;
	private int wsaaCountDd;

	private int wsaaAddToDate;

	private int wsaaYear;
	private int wsaaMm;
	private int wsaaDd;
	
	private T1693 t1693;
	private T6626 t6626;
	private T6654 t6654;
	private T6661 t6661;
	
	private Chdrpf chdrlifIO = new Chdrpf();
	
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private Datcon2 datcon2;
	@Autowired
	private Revbling revbling;
	@Autowired
	private Lifsttr lifsttr;
	
	private Descpf descIO = new Descpf();
	
	private OverbillDTO overbillDTO = new OverbillDTO();
	
	private Payrpf payrIO = new Payrpf();
	private Ptrnpf ptrnrevIO = new Ptrnpf();
	private List<Ptrnpf> ptrnList;
		
	/* DAO */
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private PtrnpfDAO ptrnpfDAO;
	@Autowired
	private LinspfDAO linspfDAO;
	@Autowired
	private DescpfDAO descpfDAO;
	

	@Override
	public void execute(OverbillDTO overbillDTO) throws IOException, ParseException {
		this.overbillDTO = overbillDTO;
		initialise();
		for (Ptrnpf ptrn : ptrnList) {
			ptrnrevIO = ptrn;
			processPtrn();
		}
		processChdrPayr();
		housekeeping();
		statistics();
	}

	protected void initialise() {
		//wsaaBatckey.set(overbillDTO.getBatchkey());
		wsaaToday = Integer.parseInt(datcon1.todaysDate());
		//printerFile.openOutput();
		readChdr();
		readPayr();
		readPtrn();
		readT6626();
		getLins(); 
	}


	protected void readChdr() {
		chdrlifIO = chdrpfDAO.readRecordOfChdr(overbillDTO.getCompany(), overbillDTO.getChdrnum());
		if (chdrlifIO == null) {
			throw new StopRunException("Chdrlif not found");
		}
		saveChdrMinorAlt();
	}


	protected void readPayr() {
		payrIO = payrpfDAO.getPayrRecord(chdrlifIO.getChdrcoy(), chdrlifIO.getChdrnum());
		if (payrIO == null) {
			throw new StopRunException("Payrpf not found");
		}
		savePayrMinorAlt();
		wsaaPayrBillcd = payrIO.getBillcd();
	}

	protected void readPtrn() {
		ptrnList = this.ptrnpfDAO.searchPtrnenqRecord(chdrlifIO.getChdrcoy(), chdrlifIO.getChdrnum());
		if (CollectionUtils.isEmpty(ptrnList)) {
			throw new StopRunException("Ptrnpf list not found");
		}
	}


	protected void saveChdrMinorAlt() {
		wsaaChdrAplsupr = chdrlifIO.getAplsupr()==null?"":chdrlifIO.getAplsupr();
		wsaaChdrBillsupr = chdrlifIO.getBillsupr()==null?"":chdrlifIO.getBillsupr();
		wsaaChdrCommsupr = chdrlifIO.getCommsupr()==null?"":chdrlifIO.getCommsupr();
		wsaaChdrNotssupr = chdrlifIO.getNotssupr()==null?"":chdrlifIO.getNotssupr();
		wsaaChdrRnwlsupr = chdrlifIO.getRnwlsupr()==null?"":chdrlifIO.getRnwlsupr();
		wsaaChdrAplspfrom = chdrlifIO.getAplspfrom()==null?0:chdrlifIO.getAplspfrom();
		wsaaChdrBillspfrom = chdrlifIO.getBillspfrom()==null?0:chdrlifIO.getBillspfrom();
		wsaaChdrCommspfrom = chdrlifIO.getCommspfrom()==null?0:chdrlifIO.getCommspfrom();
		wsaaChdrNotsspfrom = chdrlifIO.getNotsspfrom()==null?0:chdrlifIO.getNotsspfrom();
		wsaaChdrRnwlspfrom = chdrlifIO.getRnwlspfrom()==null?0:chdrlifIO.getRnwlspfrom();
		wsaaChdrAplspto = chdrlifIO.getAplspto()==null?0:chdrlifIO.getAplspto();
		wsaaChdrBillspto = chdrlifIO.getBillspto()==null?0:chdrlifIO.getBillspto();
		wsaaChdrCommspto = chdrlifIO.getCommspto()==null?0:chdrlifIO.getCommspto();
		wsaaChdrNotsspto = chdrlifIO.getNotsspto()==null?0:chdrlifIO.getNotsspto();
		wsaaChdrRnwlspto = chdrlifIO.getRnwlspto()==null?0:chdrlifIO.getRnwlspto();
		wsaaChdrCownpfx = chdrlifIO.getCownpfx()==null?"":chdrlifIO.getCownpfx();
		wsaaChdrCowncoy = chdrlifIO.getCowncoy()==null?"":chdrlifIO.getCowncoy();
		wsaaChdrCownnum = chdrlifIO.getCownnum()==null?"":chdrlifIO.getCownnum();
		wsaaChdrJownnum = chdrlifIO.getJownnum()==null?"":chdrlifIO.getJownnum();
		wsaaChdrPolsum = chdrlifIO.getPolsum()==null?0:chdrlifIO.getPolsum();
		wsaaChdrDesppfx = chdrlifIO.getDesppfx()==null?"":chdrlifIO.getDesppfx();
		wsaaChdrDespcoy = chdrlifIO.getDespcoy()==null?"":chdrlifIO.getDespcoy();
		wsaaChdrDespnum = chdrlifIO.getDespnum()==null?"":chdrlifIO.getDespnum();
		wsaaChdrAgntpfx = chdrlifIO.getAgntpfx()==null?"":chdrlifIO.getAgntpfx();
		wsaaChdrAgntcoy = chdrlifIO.getAgntcoy()==null?"":chdrlifIO.getAgntcoy();
		wsaaChdrAgntnum = chdrlifIO.getAgntnum()==null?"":chdrlifIO.getAgntnum();
	}

	protected void savePayrMinorAlt() {
		wsaaPayrAplsupr = payrIO.getAplsupr()==null?"":payrIO.getAplsupr();
		wsaaPayrBillsupr = payrIO.getBillsupr()==null?"":payrIO.getBillsupr();
		wsaaPayrNotssupr = payrIO.getNotssupr()==null?"":payrIO.getNotssupr();
		wsaaPayrAplspfrom = payrIO.getAplspfrom()==null?0:payrIO.getAplspfrom();
		wsaaPayrBillspfrom = payrIO.getBillspfrom()==null?0:payrIO.getBillspfrom();
		wsaaPayrNotsspfrom = payrIO.getNotsspfrom()==null?0:payrIO.getNotsspfrom();
		wsaaPayrAplspto = payrIO.getAplspto()==null?0:payrIO.getAplspto();
		wsaaPayrBillspto = payrIO.getBillspto()==null?0:payrIO.getBillspto();
		wsaaPayrNotsspto = payrIO.getNotsspto()==null?0:payrIO.getNotsspto();
	}

	protected void readT6626() {
		t6626=itempfService.readSmartTableByTrim(overbillDTO.getCompany(), "T6626", overbillDTO.getBatctrcde(), T6626.class);
	}


	protected void getLins() {
		List<Linspf> linspfList = linspfDAO.searchLinspfRecords(overbillDTO.getCompany(), overbillDTO.getChdrnum());
		if (CollectionUtils.isEmpty(linspfList)) {
			wsaaNoLinsFound = "Y";
			return ;
		}
		
		if (linspfList.get(0).getInstto() != payrIO.getPtdate()) {
			throw new StopRunException("Chdrnum: " + overbillDTO.getChdrnum() + " No LINS found for PTDATE");
		}
		
	}

	protected void processPtrn() throws IOException, ParseException {
		wsaaBillTran = "N";
		validateBatctrcde();
		if (wsaaIssue.equals("Y") || wsaaCollection.equals("Y")) {
			wsaaStop = "Y";
			return;
		}
		callGenericProcessing();
		if (StringUtils.equals(wsaaStop, "Y")) {
			return;
		}
		if (!StringUtils.equals(t6661.getContRevFlag(), "N")) {
			printDetailLine();
			rewritePtrn();
		}
		if (StringUtils.equals(wsaaBillTran, "Y") && (ptrnrevIO.getPtrneff() <= payrIO.getPtdate())) {
			return;
		}
	}

	protected void callGenericProcessing() throws IOException, ParseException {
		try {
			t6661 = this.itempfService.readSmartTableByTrim(overbillDTO.getCompany(), "T6661",
					ptrnrevIO.getBatctrcde(), T6661.class);
			if (t6661 == null) {
				throw new StopRunException("Item " + ptrnrevIO.getBatctrcde() + " not found in T6661");
			}
			if (StringUtils.equals(t6661.getContRevFlag(), "N")) {
				return;
			}
			if (StringUtils.isBlank(t6661.getContRevFlag())) {
				wsaaStop = "Y";
				return;
			}
			/*
			 *  depends on Maxia's code 
			 *  
			 *  Revodue  implement
			 */
			ReverseDTO reverserec = new ReverseDTO();
			reverserec.setChdrnum(chdrlifIO.getChdrnum());
			reverserec.setCompany(chdrlifIO.getChdrcoy());
			reverserec.setTranno(ptrnrevIO.getTranno());
			reverserec.setPtrneff(ptrnrevIO.getPtrneff());
			reverserec.setOldBatctrcde(ptrnrevIO.getBatctrcde());
			reverserec.setLanguage(overbillDTO.getLanguage());
			reverserec.setNewTranno(overbillDTO.getNewTranno());
			reverserec.setEffdate1(ptrnrevIO.getPtrneff());
			reverserec.setEffdate2(0);
			reverserec.setBatchkey(overbillDTO.getBatchkey());
			reverserec.setBatctrcde(overbillDTO.getBatctrcde());
			reverserec.setBatcactmn(overbillDTO.getBatcactmn());
			reverserec.setBatcactyr(overbillDTO.getBatcactyr());
			reverserec.setBatcbrn(overbillDTO.getBatcbrn());
			reverserec.setBatccoy(overbillDTO.getBatccoy()); 
			reverserec.setBatcpfx(overbillDTO.getBatcpfx());
			reverserec.setBatcbatch(overbillDTO.getBatcbatch());
			reverserec.setTransDate(overbillDTO.getTranDate());
			reverserec.setTransTime(overbillDTO.getTranTime()); 
			reverserec.setUser(overbillDTO.getUser());
			reverserec.setTermid(overbillDTO.getTermid());
			reverserec.setPlanSuffix(0);
			reverserec.setPtrnBatcpfx(ptrnrevIO.getBatcpfx());
			reverserec.setPtrnBatccoy(ptrnrevIO.getBatccoy());
			reverserec.setPtrnBatcbrn(ptrnrevIO.getBatcbrn());
			reverserec.setPtrnBatcactyr(ptrnrevIO.getBatcactyr());
			reverserec.setPtrnBatcactmn(ptrnrevIO.getBatcactmn());
			reverserec.setPtrnBatctrcde(ptrnrevIO.getBatctrcde());
			reverserec.setPtrnBatcbatch(ptrnrevIO.getBatcbatch());
			if (StringUtils.isNotBlank(t6661.getSubprogs().get(0))) {
				//re
				//callProgram(t6661rec.subprog01, reverserec.reverseRec);
				//if(Revodue.class.getSimpleName().equals(t6661.getSubprogs().get(0))) {
				reverserec = revbling.processRevbilling(reverserec);
//				}
				//Class.forName(name, initialize, loader)
//				if(revodue instanceof String) {
//					reverserec = revodue.processRevodue(reverserec);
//				}
//				if (isNE(reverserec.statuz, varcom.oK)) {
//					syserrrec.params.set(reverserec.reverseRec);
//					syserrrec.statuz.set(reverserec.statuz);
//					xxxxFatalError();
//				}
			}
			//reverserec.statuz.set(varcom.oK);
			if (StringUtils.isNotBlank(t6661.getSubprogs().get(1))) {
				//callProgram(t6661rec.subprog02, reverserec.reverseRec);
				/*
				 * call something
				 * 
				 */
//				if (isNE(reverserec.statuz, varcom.oK)) {
//					syserrrec.params.set(reverserec.reverseRec);
//					syserrrec.statuz.set(reverserec.statuz);
//					xxxxFatalError();
//				}
			}
			/*
			 *  need to confirm end
			 */
		} catch (GOTOException e) {
		}
	}


	protected void validateBatctrcde() {
		wsaaCollection = "N";
		wsaaIssue = "N";
		if (StringUtils.equals(ptrnrevIO.getBatctrcde(), t6626.getTrcode())) {
			wsaaBillTran = "Y";
		}
		if (StringUtils.equals(wsaaNoLinsFound, "Y")) {
			for (wsaaSub = 5; wsaaSub < 10; wsaaSub++) {
				if (StringUtils.equals(ptrnrevIO.getBatctrcde(), t6626.getTranscds().get(wsaaSub))) {
					wsaaIssue = "Y";
					wsaaSub = 10;
				}
			}
			return;
		}
		for (wsaaSub = 0; wsaaSub < 5; wsaaSub++) {
			if (StringUtils.equals(ptrnrevIO.getBatctrcde(), t6626.getTranscds().get(wsaaSub))) {
				wsaaCollection = "Y";
				wsaaSub = 5;
			}
		}
	}

	protected void printDetailLine() {
		start();
		detailLine();
	}

	protected void start() {
		if (!StringUtils.equals(wsaaOverflow, "Y")) {
			return;
		}
		// printerRec.set(SPACES);
		company = chdrlifIO.getChdrcoy();
		// datcon1rec.function.set("CONV");
		// datcon1rec.intDate.set(wsaaToday);
		// Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		// if (isNE(datcon1rec.statuz,varcom.oK)) {
		// syserrrec.params.set(datcon1rec.datcon1Rec);
		// syserrrec.statuz.set(datcon1rec.statuz);
		// xxxxFatalError();
		// }
//		repdate = datcon1.todaysDate();
//		sdate = datcon1.todaysDate();
		descIO = descpfDAO.getDescInfo("0", "T1693", ptrnrevIO.getChdrcoy());
		if (null == descIO) {
			companynm = "?";
			throw new StopRunException("Descitem " + ptrnrevIO.getChdrcoy() + " not found in Descpf");
		}
		companynm = descIO.getLongdesc();
		wsaaOverflow = "N";
		// printerFile.printR5062h01(r5062h01Record);
	}

protected void detailLine()
	{
		//printerRec.set(SPACES);
		chdrnum = chdrlifIO.getChdrnum();
		batctrcde = ptrnrevIO.getBatctrcde();
		descIO = descpfDAO.getDescInfo(ptrnrevIO.getChdrcoy(), "T1688", ptrnrevIO.getBatctrcde());
		if (null == descIO) {
			trandesc = "?";
		} else {
			trandesc = descIO.getLongdesc();
		}
		tranno = ptrnrevIO.getTranno();
//		datcon1rec.function.set("CONV");
//		datcon1rec.intDate.set(ptrnrevIO.getPtrneff());
//		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
//		if (isNE(datcon1rec.statuz,varcom.oK)) {
//			syserrrec.params.set(datcon1rec.datcon1Rec);
//			syserrrec.statuz.set(datcon1rec.statuz);
//			xxxxFatalError();
//		}
//		effdate.set(datcon1rec.extDate);
		wsaaOverflow = "N";
		//printerFile.printR5062d01(r5062d01Record);
	}

	protected void rewritePtrn() {
		ptrnrevIO.setCrtuser(ptrnrevIO.getUsrprf());
		ptrnrevIO.setValidflag("2");
		int row = ptrnpfDAO.updatePtrnpf(ptrnrevIO);
		if (row < 1) {
			throw new StopRunException("Ptrnpf not found");
		}
	}

protected void processChdrPayr()
	{
		readhChdr();
		readhPayr();
		updateFields();
		rewrtChdr();
		rewrtPayr();
	}

	protected void readhChdr() {
		restoreChdrMinorAlt();
	}

	protected void readhPayr() {
		restorePayrMinorAlt();
	}

	protected void updateFields() {
		if (payrIO.getBillcd() != wsaaPayrBillcd) {
			updatePayrNextdate();
		}
	}

	protected void rewrtChdr() {
		int oldtranno = chdrlifIO.getTranno().intValue();
		chdrlifIO.setTranno(overbillDTO.getNewTranno());
		chdrlifIO.setCurrto(CommonConstants.MAXDATE);
		StringBuilder sb = new StringBuilder();
		sb.append(overbillDTO.getTermid());
		sb.append(overbillDTO.getTranDate());
		sb.append(overbillDTO.getTranTime());
		sb.append(overbillDTO.getUser());
		chdrlifIO.setTranid(sb.toString());
		int row = chdrpfDAO.updateChdrpfRecord(chdrlifIO, oldtranno);
		if (row < 1) {
			throw new StopRunException("Chdrpf not updated");
		}
	}

	protected void rewrtPayr() {
		payrIO.setTranno(overbillDTO.getNewTranno());
		payrIO.setTransactionDate(overbillDTO.getTranDate());
		payrIO.setTransactionTime(overbillDTO.getTranTime());
		payrIO.setTermid(overbillDTO.getTermid());
		payrIO.setUser(overbillDTO.getUser());

		int row = payrpfDAO.updatePayrRecord(payrIO);
		if (row < 1) {
			throw new StopRunException("Payrpf not updated");
		}
		overbillDTO.setBtdate(payrIO.getBtdate());
		overbillDTO.setPtdate(payrIO.getPtdate());
	}

	protected void restoreChdrMinorAlt() {
		chdrlifIO.setAplsupr(wsaaChdrAplsupr);
		chdrlifIO.setBillsupr(wsaaChdrBillsupr);
		chdrlifIO.setCommsupr(wsaaChdrCommsupr);
		chdrlifIO.setNotssupr(wsaaChdrNotssupr);
		chdrlifIO.setRnwlsupr(wsaaChdrRnwlsupr);
		chdrlifIO.setAplspfrom(wsaaChdrAplspfrom);
		chdrlifIO.setBillspfrom(wsaaChdrBillspfrom);
		chdrlifIO.setCommspfrom(wsaaChdrCommspfrom);
		chdrlifIO.setNotsspfrom(wsaaChdrNotsspfrom);
		chdrlifIO.setRnwlspfrom(wsaaChdrRnwlspfrom);
		chdrlifIO.setAplspto(wsaaChdrAplspto);
		chdrlifIO.setBillspto(wsaaChdrBillspto);
		chdrlifIO.setCommspto(wsaaChdrCommspto);
		chdrlifIO.setNotsspto(wsaaChdrNotsspto);
		chdrlifIO.setRnwlspto(wsaaChdrRnwlspto);
		chdrlifIO.setCownpfx(wsaaChdrCownpfx);
		chdrlifIO.setCowncoy(wsaaChdrCowncoy);
		chdrlifIO.setCownnum(wsaaChdrCownnum);
		chdrlifIO.setJownnum(wsaaChdrJownnum);
		chdrlifIO.setPolsum(wsaaChdrPolsum);
		chdrlifIO.setDesppfx(wsaaChdrDesppfx);
		chdrlifIO.setDespcoy(wsaaChdrDespcoy);
		chdrlifIO.setDespnum(wsaaChdrDespnum);
		chdrlifIO.setAgntpfx(wsaaChdrAgntpfx);
		chdrlifIO.setAgntcoy(wsaaChdrAgntcoy);
		chdrlifIO.setAgntnum(wsaaChdrAgntnum);
	}

	protected void restorePayrMinorAlt() {
		payrIO.setAplsupr(wsaaPayrAplsupr);
		payrIO.setBillsupr(wsaaPayrBillsupr);
		payrIO.setNotssupr(wsaaPayrNotssupr);
		payrIO.setAplspfrom(wsaaPayrAplspfrom);
		payrIO.setBillspfrom(wsaaPayrBillspfrom);
		payrIO.setNotsspfrom(wsaaPayrNotsspfrom);
		payrIO.setAplspto(wsaaPayrAplspto);
		payrIO.setBillspto(wsaaPayrBillspto);
		payrIO.setNotsspto(wsaaPayrNotsspto);
	}

	protected void updatePayrNextdate() {
		start3301();
		continue3340();
	}

	protected void start3301() {
		String t6654Item = payrIO.getBillchnl() + chdrlifIO.getCnttype();
		t6654 = itempfService.readSmartTableByTrim(payrIO.getChdrcoy(), "T6654", t6654Item, T6654.class);
		if (t6654 != null) {
			continue3340();
		}
		t6654Item =  payrIO.getBillchnl() + "***";
		t6654 = itempfService.readSmartTableByTrim(payrIO.getChdrcoy(), "T6654", t6654Item, T6654.class);
		if (t6654 == null) {
			throw new ItemNotfoundException("Item " + t6654Item + " not found in T6654");
		}
	}

	protected void continue3340() {
		wsaaAddToDate = payrIO.getBillcd();
		wsaaYear = Integer.parseInt(DatimeUtil.parseYear(wsaaAddToDate));
		wsaaThisYear = wsaaYear;
		T3699 t3699 = itempfService.readSmartTableByTrim(chdrlifIO.getCowncoy(), "T3699", String.valueOf(wsaaThisYear),
				T3699.class);
		if (t3699 == null) {
			throw new ItemNotfoundException("Item " + wsaaThisYear + " not found in T3699");
		}

		wsaaLastYear = wsaaYear - 1;
		t3699 = itempfService.readSmartTableByTrim(chdrlifIO.getCowncoy(), "T3699", String.valueOf(wsaaLastYear),
				T3699.class);
		if (t3699 == null) {
			throw new ItemNotfoundException("Item " + wsaaLastYear + " not found in T3699");
		}

		wsaaNextYear = wsaaYear + 1;
		t3699 = itempfService.readSmartTableByTrim(chdrlifIO.getCowncoy(), "T3699", String.valueOf(wsaaNextYear),
				T3699.class);
		if (t3699 == null) {
			throw new ItemNotfoundException("Item " + wsaaNextYear + " not found in T3699");
		}

		for (int i = 1; i <= t6654.getLeadDays(); i++) {
			wsaaAddToDate = datcon2.plusDays(wsaaAddToDate, -1);
			checkHolidays();
		}
		payrIO.setNextdate(wsaaAddToDate);
	}

	protected void checkHolidays() {
		setUp();
		startCheck();
		setDate();
	}

	protected void setUp() {
		if ((wsaaYear == wsaaLastYear)) {
			wsaaCountMm = 0;
		}
		if ((wsaaYear == wsaaThisYear)) {
			wsaaCountMm = 12;
		}
		if ((wsaaYear == wsaaNextYear)) {
			wsaaCountMm = 24;
		}
		wsaaMm = Integer.parseInt(DatimeUtil.parseMonth(wsaaAddToDate));
		wsaaDd = Integer.parseInt(DatimeUtil.parseDay(wsaaAddToDate));
		wsaaCountMm = wsaaCountMm + wsaaMm;
		wsaaCountDd = wsaaDd;
	}

	protected void startCheck() {
		if (StringUtils.equals(wsaa3Dd[wsaaCountMm][wsaaCountDd], ".")
				|| StringUtils.equals(wsaa3Dd[wsaaCountMm][wsaaCountDd], "D")) {
			return;
		}
		if (StringUtils.equals(wsaa3Dd[wsaaCountMm][wsaaCountDd], "W")
				|| StringUtils.equals(wsaa3Dd[wsaaCountMm][wsaaCountDd], "E")
				|| StringUtils.equals(wsaa3Dd[wsaaCountMm][wsaaCountDd], "H")
				|| StringUtils.equals(wsaa3Dd[wsaaCountMm][wsaaCountDd], "X")) {
			if ((--wsaaCountDd < 1)) {
				wsaaCountDd = 31;
				wsaaCountMm--;
			}
		} else {
			throw new StopRunException("Illegal value error code: H842");
		}
		startCheck();
	}

	protected void setDate() {
		wsaaDd = wsaaCountDd;
		wsaaMm = wsaaCountMm;
		if ((wsaaCountMm < 13)) {
			wsaaMm = wsaaCountMm;
			wsaaYear = wsaaLastYear;
		}
		if ((wsaaCountMm > 12) && (wsaaCountMm < 25)) {
			wsaaMm = wsaaCountMm - 12;
			wsaaYear = wsaaThisYear;
		}
		if ((wsaaCountMm > 24)) {
			wsaaMm = wsaaCountMm - 24;
			wsaaYear = wsaaNextYear;
		}
	}

	protected void housekeeping() {
		if ((overbillDTO.getBtdate() != overbillDTO.getPtdate())) {
			errorLine();
		}
		// printerFile.close();
	}

	protected void errorLine() {
		start4100();
	}

	protected void start4100() {
		if (!StringUtils.equals(wsaaOverflow, "Y")) {
			// printerFile.printR5062d02(r5062d02Record);
			return;
		}
		// printerRec.set(SPACES);
		company = chdrlifIO.getChdrcoy();
		// datcon1rec.function.set("CONV");
		// datcon1rec.intDate.set(wsaaToday);
		// Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		// if (isNE(datcon1rec.statuz,varcom.oK)) {
		// syserrrec.params.set(datcon1rec.datcon1Rec);
		// syserrrec.statuz.set(datcon1rec.statuz);
		// xxxxFatalError();
		// }
		// repdate.set(datcon1rec.extDate);
		descIO = descpfDAO.getDescInfo("0", "T1693", ptrnrevIO.getChdrcoy());
		if (null == descIO) {
			companynm = "?";
		} else {
			companynm = descIO.getLongdesc();
		}
		wsaaOverflow = "N";
		// printerFile.printR5062h01(r5062h01Record);
		// printerFile.printR5062d02(r5062d02Record);
	}

	protected void statistics() throws IOException {
		LifsttrDTO lifsttrrec = new LifsttrDTO();
		lifsttrrec.setBatccoy(overbillDTO.getBatccoy());
		lifsttrrec.setBatcbrn(overbillDTO.getBatcbrn());
		lifsttrrec.setBatcacty(overbillDTO.getBatcactyr());
		lifsttrrec.setBatcactmn(overbillDTO.getBatcactmn());
		lifsttrrec.setBatctrcde(overbillDTO.getBatctrcde());
		lifsttrrec.setBatcbatch(overbillDTO.getBatcbatch());
		lifsttrrec.setChdrcoy(chdrlifIO.getChdrcoy());
		lifsttrrec.setChdrnum(chdrlifIO.getChdrnum());
		lifsttrrec.setTranno(overbillDTO.getNewTranno());
		lifsttrrec.setTrannor(ptrnrevIO.getTranno());
		lifsttrrec.setAgntnum("");
		lifsttrrec.setOldAgntnum("");
		lifsttr.processLifsttr(lifsttrrec);
	}

}
