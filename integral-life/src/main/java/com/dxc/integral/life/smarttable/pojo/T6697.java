package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T6697 {
	private String adminref;
	private List<String> contypes;
    private List<String> crcodes;
	private String empcon;
	private BigDecimal empconpc;
	private int issdate;
	private List<String> maxfunds;
	private int sfodate;
	private String sfonum;
	public String getAdminref() {
		return adminref;
	}
	public void setAdminref(String adminref) {
		this.adminref = adminref;
	}
	public List<String> getContypes() {
		return contypes;
	}
	public void setContypes(List<String> contypes) {
		this.contypes = contypes;
	}
	public List<String> getCrcodes() {
		return crcodes;
	}
	public void setCrcodes(List<String> crcodes) {
		this.crcodes = crcodes;
	}
	public String getEmpcon() {
		return empcon;
	}
	public void setEmpcon(String empcon) {
		this.empcon = empcon;
	}
	public BigDecimal getEmpconpc() {
		return empconpc;
	}
	public void setEmpconpc(BigDecimal empconpc) {
		this.empconpc = empconpc;
	}
	public int getIssdate() {
		return issdate;
	}
	public void setIssdate(int issdate) {
		this.issdate = issdate;
	}
	public List<String> getMaxfunds() {
		return maxfunds;
	}
	public void setMaxfunds(List<String> maxfunds) {
		this.maxfunds = maxfunds;
	}
	public int getSfodate() {
		return sfodate;
	}
	public void setSfodate(int sfodate) {
		this.sfodate = sfodate;
	}
	public String getSfonum() {
		return sfonum;
	}
	public void setSfonum(String sfonum) {
		this.sfonum = sfonum;
	}
	
	
}
