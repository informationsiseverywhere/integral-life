package com.dxc.integral.life.utils;

import java.io.IOException;

import java.text.ParseException;

import com.dxc.integral.life.beans.RlagprdDTO;

/**
 * Rlagprd Utility.
 * 
 * @author vhukumagrawa
 *
 */
@FunctionalInterface
public interface Rlagprd {

	/**
	 * Calculates and updates the agency production figures in Agency Production
	 * file MAPR.
	 * 
	 * @param rlagprdDTO
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	void calculateAndUpdateMAPR(RlagprdDTO rlagprdDTO) throws IOException, ParseException;//ILIFE-5763
}
