package com.dxc.integral.life.smarttable.pojo;

public class T5654 {
	private String indxflg;
	private String cpidef;
	private String cpiallwd;
	private String predef;
	private String predefallwd;
	private String filler;

	public String getIndxflg() {
		return indxflg;
	}

	public void setIndxflg(String indxflg) {
		this.indxflg = indxflg;
	}

	public String getCpidef() {
		return cpidef;
	}

	public void setCpidef(String cpidef) {
		this.cpidef = cpidef;
	}

	public String getCpiallwd() {
		return cpiallwd;
	}

	public void setCpiallwd(String cpiallwd) {
		this.cpiallwd = cpiallwd;
	}

	public String getPredef() {
		return predef;
	}

	public void setPredef(String predef) {
		this.predef = predef;
	}

	public String getPredefallwd() {
		return predefallwd;
	}

	public void setPredefallwd(String predefallwd) {
		this.predefallwd = predefallwd;
	}

	public String getFiller() {
		return filler;
	}

	public void setFiller(String filler) {
		this.filler = filler;
	}
}