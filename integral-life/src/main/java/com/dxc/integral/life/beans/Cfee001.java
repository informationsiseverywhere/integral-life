/* ********************  	*/
/*Author  :tsaxena3			*/
/*Purpose :Model for Cfee001*/
/*Date    :2019.04.09	 	*/

package com.dxc.integral.life.beans;
public class Cfee001 {
	
	private String function;
	private String statuz;
	private String cntType;
	private Integer billFreq;
	private Integer effDate;
	private Integer mgFee;
	private String cntCurr;
	private String company;
	private String filler;
	
	public Cfee001() {
		function = "";
		statuz = "";
		cntType = "";
		billFreq = 0;
		int effdate = 0;
		mgFee = 0;
		cntCurr = "";
		company = "";
		filler = "";
	}
	
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	
	public String getCnttype() {
		return cntType;
	}
	public void setCnttype(String cnttype) {
		this.cntType = cnttype;
	}
	
	public Integer getBillfreq() {
		return billFreq;
	}
	public void setBillfreq(Integer billfreq) {
		this.billFreq = billfreq;
	}
	
	public Integer getEffdate() {
		return effDate;
	}
	public void setEffdate(Integer effdate) {
		this.effDate = effdate;
	}
	
	public Integer getMgfee() {
		return mgFee;
	}
	public void setMgfee(Integer integer) {
		this.mgFee = integer;
	}
	
	public String getCntcurr() {
		return cntCurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntCurr = cntcurr;
	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
	@Override
	public String toString() {
		return "Cfee001 [function = " + function + ", statuz = " + statuz +", cnttype = " + cntType + ", billfreq = " + billFreq + ", effdate = " + effDate + ", mgfee = " + mgFee + ", cntcurr = " + cntCurr + ", comapny = " + company + ", filler = " + filler + "]";
	}
}
