package com.dxc.integral.life.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.beans.VPMSinfoDTO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5692;
import com.dxc.integral.life.utils.VPMScaller;

@Service("calcRnwlCommissionService")
public class CalcRnwlCommissionService {
	
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private VPMScaller vpmscaller;
	@Autowired
	private Environment env;
	
	private BigDecimal wsaaReprempc;
	private BigDecimal wsaaRwcmrate;
	

	public ComlinkDTO calcRnwlCommission(ComlinkDTO comlinkDTO, BigDecimal wsaaT5694, String cnttype, boolean t5629InVPMSCondition) throws Exception{
		String agentClassStr = comlinkDTO.getAgentClass() == null ? "    " : comlinkDTO.getAgentClass();
		TableItem<T5692> t5692 = itempfService.readSmartTableByTableNameAndItem(comlinkDTO.getChdrcoy(), "T5692", agentClassStr.concat(comlinkDTO.getCrtable()), CommonConstants.IT_ITEMPFX, T5692.class);
		T5692 t5692IO = new T5692();
		if(t5692 != null) {
			t5692IO = t5692.getItemDetail();			
		}else {
			t5692 = itempfService.readSmartTableByTableNameAndItem(comlinkDTO.getChdrcoy(), "T5692", agentClassStr.concat("****"), CommonConstants.IT_ITEMPFX, T5692.class);
			if(t5692 != null) {
				t5692IO = t5692.getItemDetail();
			}else {
			
			List<Integer> ageslist = new ArrayList<Integer>();
			ageslist.add(0);
			ageslist.add(0);
			ageslist.add(0);
			ageslist.add(0);
			t5692IO.setAges(ageslist);
			}
	
		}
		
		return callCont(comlinkDTO, t5692IO, wsaaT5694, cnttype, t5629InVPMSCondition);

	}
	
   public ComlinkDTO callCont(ComlinkDTO comlinkDTO,T5692 t5692IO, BigDecimal wsaaT5694, String cnttype, boolean t5629InVPMSCondition) {
	      if(t5629InVPMSCondition) {
			comlinkDTO=contforCmpy002(comlinkDTO, t5692IO, wsaaT5694, cnttype);
			}else {
			comlinkDTO=cont(comlinkDTO, t5692IO, wsaaT5694, cnttype);
			}
	      return comlinkDTO;
   }
	
	public ComlinkDTO cont(ComlinkDTO comlinkDTO,T5692 t5692IO, BigDecimal wsaaT5694, String cnttype){
		BigDecimal wsaaRenComm;
		
		
		Lifepf lifepf = getLifepf(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum(), comlinkDTO.getLife(), comlinkDTO.getJlife());
		if(lifepf == null) {
		    return comlinkDTO;
		}

//		if(getT5692Value(lifepf, t5692IO)) {
//			return comlinkDTO;
//		}
		getT5692Value(lifepf, t5692IO);
		// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization start
		if (!(checkVPMSFlag() && comlinkDTO.getCoverage().equals("00") && comlinkDTO.getRider().equals("00") && !comlinkDTO.getMethod().equals("RCP7"))) {
			wsaaT5694 = wsaaT5694.multiply(wsaaReprempc).divide(new BigDecimal(100), 3);
			wsaaRenComm = comlinkDTO.getInstprem().multiply(wsaaT5694).divide(new BigDecimal(100), 3);
			wsaaRenComm = wsaaRenComm.multiply(wsaaRwcmrate.divide(new BigDecimal(100)));
			comlinkDTO.setPayamnt(wsaaRenComm);
			comlinkDTO.setErndamt(wsaaRenComm);
		} else {
			comlinkDTO = callVPMS(comlinkDTO, lifepf, cnttype);
		}
		
		return comlinkDTO;
	}
	
	public ComlinkDTO contforCmpy002(ComlinkDTO comlinkDTO, T5692 t5692IO,  BigDecimal wsaaT5694, String cnttype){
		Lifepf lifepf = getLifepf(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum(), comlinkDTO.getLife(), comlinkDTO.getJlife());
		if(lifepf == null) {
		    return comlinkDTO;
		}

		if (!(checkVPMSFlag()
				&& "01".equals(comlinkDTO.getCoverage().trim()) && "00".equals(comlinkDTO.getRider().trim())
				&& !"RCP7".equals(comlinkDTO.getMethod().trim()))) {
			
//			if(getT5692Value(lifepf, t5692IO)) {
//				return comlinkDTO;
//			}
			getT5692Value(lifepf, t5692IO);
			BigDecimal wsaaRenComm = comlinkDTO.getInstprem().multiply(wsaaReprempc).divide(new BigDecimal(100)).multiply(wsaaRwcmrate)
			.divide(new BigDecimal(100)).multiply(wsaaT5694).divide(new BigDecimal(100), 3, BigDecimal.ROUND_HALF_UP);
			
			comlinkDTO.setPayamnt(wsaaRenComm);
			comlinkDTO.setErndamt(wsaaRenComm);
		} else {
			comlinkDTO = callVPMS(comlinkDTO, lifepf, cnttype);
	}
		return comlinkDTO;
 }
	
	public ComlinkDTO callVPMS(ComlinkDTO comlinkDTO, Lifepf lifepf, String cnttype) {
		comlinkDTO.setPtdate(comlinkDTO.getPtdate()+1);   
		comlinkDTO.setSrcebus("");
		comlinkDTO.setCnttype(cnttype);
		comlinkDTO.setCltdob(lifepf.getCltdob());
		comlinkDTO.setAnbccd(lifepf.getAnbAtCcd());
		VPMSinfoDTO vpmsinfoDTO = new VPMSinfoDTO();
		vpmsinfoDTO.setCoy(comlinkDTO.getChdrcoy());
		vpmsinfoDTO.setTransDate(String.valueOf(comlinkDTO.getEffdate()));
		vpmsinfoDTO.setModelName("VPMCMCPMT");
		if ("".equals(comlinkDTO.getAgentype())){
			comlinkDTO.setAgentype("**");
			vpmscaller.callVPMCMCPMT(vpmsinfoDTO, comlinkDTO);
			if (!"****".equals(comlinkDTO.getStatuz())) {
				fatalError600("Cmpy002: Exception in premiumrec:" + comlinkDTO.getStatuz());
			}
	  }
		return comlinkDTO;
	}
	public Lifepf getLifepf(String coy, String chdrnum, String life, String jlife) {
		if(jlife==null || "".equals(jlife.trim())){
			jlife="00";
		}
		List<Lifepf> lifepfList = lifepfDAO.getLifeRecord(coy, chdrnum, life, jlife);
		if(lifepfList == null || lifepfList.size()==0) {
			return null;
		}
		Lifepf lifepf = lifepfList.get(0);
		return lifepf;
	}
	
	public void getT5692Value(Lifepf lifepf, T5692 t5692IO){

		wsaaReprempc=new BigDecimal(100);
		wsaaRwcmrate=new BigDecimal(100);
		for (int i = 0; i < t5692IO.getAges().size(); i++) {
			if (lifepf.getAnbccd()<=t5692IO.getAges().get(i).intValue()) {
				wsaaRwcmrate=t5692IO.getIncmrates().get(i);
				wsaaReprempc=t5692IO.getReprempcs().get(i);
			}
		}
//		if (lifepf.getAnbccd()<=t5692IO.getAges().get(0).intValue()) {
//			wsaaRwcmrate=t5692IO.getIncmrates().get(0);
//			wsaaReprempc=t5692IO.getReprempcs().get(0);
//			return true;
//		}
//		if (lifepf.getAnbccd()<=t5692IO.getAges().get(1).intValue()) {
//			wsaaRwcmrate=t5692IO.getIncmrates().get(1);
//			wsaaReprempc=t5692IO.getReprempcs().get(1);
//			return true;
//		}
//		if (lifepf.getAnbccd()<=t5692IO.getAges().get(2).intValue()) {
//			wsaaRwcmrate=t5692IO.getIncmrates().get(2);
//			wsaaReprempc=t5692IO.getReprempcs().get(2);
//			return true;
//		}
//		if (lifepf.getAnbccd()<=t5692IO.getAges().get(3).intValue()) {
//			wsaaRwcmrate=t5692IO.getIncmrates().get(3);
//			wsaaReprempc=t5692IO.getReprempcs().get(3);
//			return true;
//		}
//		return false;
	}
	
	private void fatalError600(String status) {
		throw new StopRunException(status);
	}
	
	private boolean checkVPMSFlag() {
		return env.getProperty("vpms.flag").equals("1");
	}
}
