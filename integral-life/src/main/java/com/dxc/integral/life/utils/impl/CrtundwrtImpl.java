package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.life.beans.CrtundwrtDTO;
import com.dxc.integral.life.dao.UndrpfDAO;
import com.dxc.integral.life.dao.model.Undrpf;
import com.dxc.integral.life.smarttable.pojo.T5446;
import com.dxc.integral.life.smarttable.pojo.T5448;
import com.dxc.integral.life.smarttable.pojo.Th618;
import com.dxc.integral.life.utils.Crtundwrt;
/**
 * @author wli31
 */
@Service("crtundwrt")
@Lazy
public class CrtundwrtImpl implements Crtundwrt {
	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private UndrpfDAO undrpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Override
	public CrtundwrtDTO processCrtundwrt(CrtundwrtDTO crtundwrtDTO)throws NumberFormatException, ParseException, IOException {
		crtundwrtDTO.setStatus("****");
		if ("DEL".equals(crtundwrtDTO.getFunction()) && crtundwrtDTO.getCrtable().indexOf('*') > -1) {
			deleSect2000(crtundwrtDTO,"");
			return crtundwrtDTO;
		}
		String t5448Itemitem = crtundwrtDTO.getCnttyp().concat(crtundwrtDTO.getCrtable());
		T5448 t5448IO = itempfService.readSmartTableByTrim(crtundwrtDTO.getCoy(), "T5448", t5448Itemitem,T5448.class);
		if (t5448Itemitem == null) {
			crtundwrtDTO.setStatus("EDNP");
			return crtundwrtDTO;
		}
		Th618 th618IO = itempfService.readSmartTableByTrim(crtundwrtDTO.getCoy(), "Th618", t5448IO.getRrsktyp().trim(),Th618.class); 
		if (th618IO == null &&  th618IO.getLrkclss() == null) {
			crtundwrtDTO.setStatus("EDNP");
			return crtundwrtDTO;
		}

		for(String lrkcls : th618IO.getLrkclss()){
			if("".equals(lrkcls.trim())){
				break;
			}
			eachTh618Lrkcls(crtundwrtDTO,lrkcls);
		}
		
		return crtundwrtDTO;
	}
	private void eachTh618Lrkcls(CrtundwrtDTO crtundwrtDTO,String lrkcls) throws IOException{
		if (null == lrkcls && "".equals(lrkcls)) {
			return;
		}
		T5446 t5446IO = itempfService.readSmartTableByTrim(crtundwrtDTO.getCoy(), "T5446", lrkcls.trim(),T5446.class); 
		if (t5446IO == null) {
			crtundwrtDTO.setStatus("EDNP");
			return;
		}
		if (null == t5446IO.getLrkcls() && "".equals(t5446IO.getLrkcls())) {
			t5446IO.setLrkcls(lrkcls);
		}
		if ("ADD".equals(crtundwrtDTO.getFunction())){
			Undrpf undrpf = new Undrpf();
			undrpf.setClntnum(crtundwrtDTO.getClntnum());
			undrpf.setCoy(crtundwrtDTO.getCoy());
			undrpf.setCurrcode(crtundwrtDTO.getCurrcode());
			undrpf.setChdrnum(crtundwrtDTO.getChdrnum());
			undrpf.setCrtable(crtundwrtDTO.getCrtable());
			undrpf.setLrkcls01(lrkcls);
			undrpf.setLrkcls02(t5446IO.getLrkcls());
			undrpf.setCnttyp(crtundwrtDTO.getCnttyp());
			undrpf.setLife(crtundwrtDTO.getLife());
			undrpf.setSumins(crtundwrtDTO.getSumins());
			undrpf.setEffdate(Integer.parseInt(datcon1.todaysDate()));
			undrpf.setUsrprf(crtundwrtDTO.getJobname());
			undrpf.setJobnm(crtundwrtDTO.getJobname());
			undrpfDAO.insertUndrpfRecord(undrpf);
		}
		else if ("DEL".equals(crtundwrtDTO.getFunction())){
			deleSect2000(crtundwrtDTO,lrkcls);
		}else{
			crtundwrtDTO.setStatus("INVF");
		}
		return;
	}
	protected void deleSect2000(CrtundwrtDTO crtundwrtDTO, String lrkcls){
		if("".equals(crtundwrtDTO.getClntnum())){
			undrpfDAO.deleteUndrpfRecord(crtundwrtDTO.getChdrnum(), crtundwrtDTO.getCoy());
			return;
		}
		if (crtundwrtDTO.getCrtable().indexOf('*') < 1) {
			Undrpf undrpf = new Undrpf();
			undrpf.setClntnum(crtundwrtDTO.getClntnum());
			undrpf.setCoy(crtundwrtDTO.getCoy());
			undrpf.setCurrcode(crtundwrtDTO.getCurrcode());
			undrpf.setChdrnum(crtundwrtDTO.getChdrnum());
			undrpf.setCrtable(crtundwrtDTO.getCrtable());
			undrpf.setLrkcls01(lrkcls);
			undrpfDAO.deleteUndrpfRecord(undrpf);
		}
	}
}
