package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Ainrpf;

public interface AinrpfDAO {
	
	public void insertAinrpfRecords(List<Ainrpf> ainrpfList);
	
	public void updateAinrpfRecords(List<Ainrpf> ainrpfList);

}
