/*********************  */
/*Author  :Liwei						*/
/*Purpose :Instead of subroutine rlpdlon*/
/*Date    :2018.10.26					*/
package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.RlpdlonDTO;
/**
 * @author wli31
 */
public interface Rlpdlon {
	public RlpdlonDTO processRlpdlonInfo(RlpdlonDTO rlpdlonDTO) throws IOException;
}
