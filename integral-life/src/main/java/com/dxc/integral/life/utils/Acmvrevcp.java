package com.dxc.integral.life.utils;

import com.dxc.integral.life.dao.model.AcmvrevcpPojo;

public interface Acmvrevcp {
	public AcmvrevcpPojo processAcmvrevcp(AcmvrevcpPojo acmvrevcpPojo);
}
