package com.dxc.integral.life.dao.model;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "CLEXPF", schema = "VM1DTA")
public class Clexpf implements Serializable{
	private String clntpfx ;	
	private String clntcoy ;
	@Id
	private String clntnum;
	@Id
	private long unique_number;
	private String rdidtelno;
	private String rmblphone;
	private String rpager;
	private String faxno;
	private String rinternet;
	private String rtaxidnum;
		
	private String rstaflag;
	private String splindic;
	private String zspecind;
	private String oldidno;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String validflag;
	private String amlstatus;
	@Id
	private int tranno;
	
	private String action;
	public String getClntpfx() {
		return clntpfx;
	}

	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}

	public String getClntcoy() {
		return clntcoy;
	}

	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}

	public String getClntnum() {
		return clntnum;
	}

	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}

	public long getUnique_number() {
		return unique_number;
	}

	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}

	public String getRdidtelno() {
		return rdidtelno;
	}

	public void setRdidtelno(String rdidtelno) {
		this.rdidtelno = rdidtelno;
	}

	public String getRmblphone() {
		return rmblphone;
	}

	public void setRmblphone(String rmblphone) {
		this.rmblphone = rmblphone;
	}

	public String getRpager() {
		return rpager;
	}

	public void setRpager(String rpager) {
		this.rpager = rpager;
	}

	public String getFaxno() {
		return faxno;
	}

	public void setFaxno(String faxno) {
		this.faxno = faxno;
	}

	public String getRinternet() {
		return rinternet;
	}

	public void setRinternet(String rinternet) {
		this.rinternet = rinternet;
	}

	public String getRtaxidnum() {
		return rtaxidnum;
	}

	public void setRtaxidnum(String rtaxidnum) {
		this.rtaxidnum = rtaxidnum;
	}

	public String getRstaflag() {
		return rstaflag;
	}

	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}

	public String getSplindic() {
		return splindic;
	}

	public void setSplindic(String splindic) {
		this.splindic = splindic;
	}

	public String getZspecind() {
		return zspecind;
	}

	public void setZspecind(String zspecind) {
		this.zspecind = zspecind;
	}

	public String getOldidno() {
		return oldidno;
	}

	public void setOldidno(String oldidno) {
		this.oldidno = oldidno;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Date getDatime() {
		return datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAmlstatus() {
		return amlstatus;
	}

	public void setAmlstatus(String amlstatus) {
		this.amlstatus = amlstatus;
	}

	

}