package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.DdsupfDAO;
import com.dxc.integral.life.dao.model.Ddsupf;

/**
 * 
 * DdsupfDAO implementation for database table <b>Ddsupf</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("ddsupfDAO")
public class DdsupfDAOImpl extends BaseDAOImpl implements DdsupfDAO {

	@Override
	public List<Ddsupf> readDdsupfRecords(String company, String contractNumber){
		StringBuilder sql = new StringBuilder("SELECT * FROM DDSUPF WHERE DHNFLAG != '1' AND PAYRCOY=? AND PAYRNUM=? ");
		sql.append(" ORDER BY PAYRCOY, PAYRNUM, MANDREF, BILLCD DESC, MANDSTAT DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Ddsupf>(Ddsupf.class));
	}
}
