package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Loanpf {

	private Long unique_number;
	private String chdrcoy;
	private String chdrnum;
  	private Integer loannumber;
  	private String loantype;
  	private String loancurr;
  	private String validflag;
  	private Integer ftranno;
  	private Integer ltranno;
  	private BigDecimal loanorigam;
  	private Integer loanstdate;
  	private BigDecimal lstcaplamt;
    private Integer lstcapdate; 
  	private Integer nxtcapdate;
  	private Integer lstintbdte;
  	private Integer nxtintbdte;
  	private BigDecimal tplstmdty;
  	private String termid;
  	private Integer user_t;
  	private Integer trdt;
  	private Integer trtm;
  	private String usrprf;
  	private String jobnm;
  	private Timestamp datime;
  	
  	
	public Long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(Long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public Integer getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(Integer loannumber) {
		this.loannumber = loannumber;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public String getLoancurr() {
		return loancurr;
	}
	public void setLoancurr(String loancurr) {
		this.loancurr = loancurr;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public Integer getFtranno() {
		return ftranno;
	}
	public void setFtranno(Integer ftranno) {
		this.ftranno = ftranno;
	}
	public Integer getLtranno() {
		return ltranno;
	}
	public void setLtranno(Integer ltranno) {
		this.ltranno = ltranno;
	}
	public BigDecimal getLoanorigam() {
		return loanorigam;
	}
	public void setLoanorigam(BigDecimal loanorigam) {
		this.loanorigam = loanorigam;
	}
	public Integer getLoanstdate() {
		return loanstdate;
	}
	public void setLoanstdate(Integer loanstdate) {
		this.loanstdate = loanstdate;
	}
	public BigDecimal getLstcaplamt() {
		return lstcaplamt;
	}
	public void setLstcaplamt(BigDecimal lstcaplamt) {
		this.lstcaplamt = lstcaplamt;
	}
	public Integer getLstcapdate() {
		return lstcapdate;
	}
	public void setLstcapdate(Integer lstcapdate) {
		this.lstcapdate = lstcapdate;
	}
	public Integer getNxtcapdate() {
		return nxtcapdate;
	}
	public void setNxtcapdate(Integer nxtcapdate) {
		this.nxtcapdate = nxtcapdate;
	}
	public Integer getLstintbdte() {
		return lstintbdte;
	}
	public void setLstintbdte(Integer lstintbdte) {
		this.lstintbdte = lstintbdte;
	}
	public Integer getNxtintbdte() {
		return nxtintbdte;
	}
	public void setNxtintbdte(Integer nxtintbdte) {
		this.nxtintbdte = nxtintbdte;
	}
	public BigDecimal getTplstmdty() {
		return tplstmdty;
	}
	public void setTplstmdty(BigDecimal tplstmdty) {
		this.tplstmdty = tplstmdty;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getUser_t() {
		return user_t;
	}
	public void setUser_t(Integer user_t) {
		this.user_t = user_t;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
  		
}
