package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.IncipfDAO;
import com.dxc.integral.life.dao.model.Incipf;

/**
 * 
 * @author xma3
 *
 */
@Repository("incipfDAO")
@Lazy
public class IncipfDAOImpl extends BaseDAOImpl implements IncipfDAO {

	@Override
	public List<Incipf> getIncipfRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx) {
		StringBuilder sql = new StringBuilder("SELECT * FROM INCIPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
		sql.append("AND (VALIDFLAG = '1' AND DORMFLAG = 'N' OR VALIDFLAG = '' AND DORMFLAG ='N') ");
		sql.append("order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx},new BeanPropertyRowMapper<Incipf>(Incipf.class));
	}
	
	@Override
	public int updateIncipfPremcurr(Incipf incipf) {
		StringBuilder sql = new StringBuilder("UPDATE INCIPF SET PREMCURR01=?, PREMCURR02=?, PREMCURR03=?, PREMCURR04=?, PREMCURR05=?, PREMCURR06=?, PREMCURR07=? ");
		sql.append("WHERE UNIQUE_NUMBER=?");
		return jdbcTemplate.update(sql.toString(), new Object[]{incipf.getPremCurr01(), incipf.getPremCurr02(), incipf.getPremCurr03(), incipf.getPremCurr04(),
				incipf.getPremCurr05(), incipf.getPremCurr06(), incipf.getPremCurr07(), incipf.getUniqueNumber()});
	}

}
