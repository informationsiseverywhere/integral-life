package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.OvrdueDTO;
import com.dxc.integral.life.beans.TrmreasDTO;
import com.dxc.integral.life.beans.ZorlnkDTO;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.Th605;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Nftrad;
import com.dxc.integral.life.utils.Trmreas;
import com.dxc.integral.life.utils.Zorcompy;

/**
 * @author fwang3
 *
 */
@Service("nftrad")
@Lazy
public class NftradImpl extends BaseDAOImpl implements Nftrad {
	@Autowired
	private AgcmpfDAO agcmpfDAO;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private LoanpfDAO loanpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private Lifacmv lifacmv;
	@Autowired
	private Zorcompy zorcompy;
	@Autowired
	private Trmreas trmreas;
	
	private BigDecimal wsaaCommRecovered = BigDecimal.ZERO;
	private String wsaaTransDesc = "";
	private Agcmpf agcmIO = new Agcmpf();
	private T5645 t5645 = new T5645();
	private T5688 t5688 = new T5688();
	private Th605 th605 = new Th605();
	private LifacmvDTO lifacmvrec = new LifacmvDTO();
	private OvrdueDTO ovrdueDTO = new OvrdueDTO();
	
	
	@Override
	public OvrdueDTO execute(OvrdueDTO dto) throws Exception {
		ovrdueDTO = dto;
		ovrdueDTO.setStatuz("****");
		lifacmvrec.setJrnseq(0);
		commClawBack();
		loans();
		processReassurance();
		ovrdueDTO.setNewPremCessDate(ovrdueDTO.getPremCessDate());
		ovrdueDTO.setNewSumins(ovrdueDTO.getSumins());
		return ovrdueDTO;
	}

	protected void commClawBack() throws Exception {
		readTableTh605();
		readT5645T1688();
		readT5688();
		List<Agcmpf> agcmList = this.agcmpfDAO.getAgcmpfRecord(ovrdueDTO.getChdrcoy(), ovrdueDTO.getChdrnum());
		if (CollectionUtils.isEmpty(agcmList)) {
			return ;
		}
		for (Agcmpf pf : agcmList) {
			agcmIO = pf;
			callAgcmio();
		}
	}

	protected void callAgcmio() throws Exception {
		updateAgcmFile();
		insertAgcmFile();
	}

	protected void updateAgcmFile() {
		agcmIO.setValidflag("2");
		agcmIO.setCurrto(ovrdueDTO.getEffdate());
		int row = agcmpfDAO.updateAcagpfValidflag(agcmIO);
		if (row < 1) {
			throw new StopRunException("Agcmpf not updated");
		}
	}

	protected void insertAgcmFile() throws Exception {
		wsaaCommRecovered = agcmIO.getCompay().subtract(agcmIO.getComern());
		
		if (StringUtils.equals(agcmIO.getOvrdcat(), "O")) {
			overrideCommAcmv();
		}
		else {
			commClawbackAcmv();
		}
		agcmIO.setCompay(agcmIO.getComern());
		agcmIO.setValidflag("1");
		agcmIO.setTranno(ovrdueDTO.getTranno());
		agcmIO.setCurrto(CommonConstants.MAXDATE);
		
		List<Agcmpf> list = new ArrayList<>();
		list.add(agcmIO);
		agcmpfDAO.insertAgcmpfRecords(list);
	}



	protected void readTableTh605() {
		th605=itempfService.readSmartTableByTrim(ovrdueDTO.getChdrcoy(), "TH605", ovrdueDTO.getChdrcoy(), Th605.class);
		if(th605 == null) {
			throw new ItemNotfoundException("Item " + ovrdueDTO.getChdrcoy() + " not found in Table TH605");
		}
	}

	protected void readT5645T1688() {
		t5645 = itempfService.readSmartTableByTrim(ovrdueDTO.getChdrcoy(), "T5645", "NFTRAD", T5645.class);
		if (t5645 == null) {
			throw new ItemNotfoundException("Item " + "NFTRAD" + " not found in Table T5645");
		}

		Descpf descIO = descpfDAO.getDescInfo(ovrdueDTO.getChdrcoy(), "T1688", ovrdueDTO.getTrancode());
		if (descIO == null) {
			throw new StopRunException("Item " + ovrdueDTO.getTrancode() + " not found in Descpf T1688");
		}

		wsaaTransDesc = descIO.getLongdesc();
	}


	protected void readT5688() {
		t5688 = itempfService.readSmartTableByTrim(ovrdueDTO.getChdrcoy(), "T5688", ovrdueDTO.getCnttype(), ovrdueDTO.getPtdate() ,T5688.class);
		if (t5688 == null) {
			throw new ItemNotfoundException("Item " + ovrdueDTO.getCnttype() + " not found in Table T5688");
		}
	}


	protected void commClawbackAcmv() throws Exception {
		if (wsaaCommRecovered.compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		lifacmvrec.setRdocnum(ovrdueDTO.getChdrnum());
		lifacmvrec.setJrnseq(lifacmvrec.getJrnseq() + 1);
		lifacmvrec.setBatccoy(ovrdueDTO.getChdrcoy());
		lifacmvrec.setRldgcoy(ovrdueDTO.getChdrcoy());
		lifacmvrec.setGenlcoy(ovrdueDTO.getChdrcoy());
		lifacmvrec.setBatcactyr(ovrdueDTO.getAcctyear());
		lifacmvrec.setBatctrcde(ovrdueDTO.getTrancode());
		lifacmvrec.setBatcactmn(ovrdueDTO.getAcctmonth());
		lifacmvrec.setBatcbatch(ovrdueDTO.getBatcbatch());
		lifacmvrec.setBatcbrn(ovrdueDTO.getBatcbrn());
		lifacmvrec.setTranno(ovrdueDTO.getTranno());
		lifacmvrec.setOrigcurr(ovrdueDTO.getCntcurr());
		lifacmvrec.setRcamt(BigDecimal.ZERO);
		lifacmvrec.setCrate(BigDecimal.ZERO);
		lifacmvrec.setAcctamt(BigDecimal.ZERO);
		lifacmvrec.setFrcdate(CommonConstants.MAXDATE);
		lifacmvrec.setEffdate(ovrdueDTO.getPtdate());
		lifacmvrec.setTranref(ovrdueDTO.getChdrnum());
		lifacmvrec.setUser(ovrdueDTO.getUser());
		lifacmvrec.setTrandesc(wsaaTransDesc);
		lifacmvrec.setRldgacct(agcmIO.getAgntnum());
		lifacmvrec.setTransactionDate(ovrdueDTO.getTranDate());
		lifacmvrec.setTransactionTime(ovrdueDTO.getTranTime());
		lifacmvrec.setTermid(ovrdueDTO.getTermid());
		lifacmvrec.setOrigamt(wsaaCommRecovered);
		lifacmvrec.setSacscode(t5645.getSacscodes().get(0));
		lifacmvrec.setSacstyp(t5645.getSacstypes().get(0));
		lifacmvrec.setGlsign(t5645.getSigns().get(0));
		lifacmvrec.setGlcode(t5645.getGlmaps().get(0));
		lifacmvrec.setContot(t5645.getCnttots().get(0));
		lifacmvrec.getSubstituteCodes().set(0, ovrdueDTO.getCnttype());
		lifacmvrec.getSubstituteCodes().set(5, "");
		lifacmv.executePSTW(lifacmvrec);

		if (StringUtils.equals(th605.getIndic(), "Y")) {
			callZorcompy();
		}

		if (StringUtils.equals(t5688.getComlvlacc(), "Y")) {
			lifacmvrec.setSacscode(t5645.getSacscodes().get(4));
			lifacmvrec.setSacstyp(t5645.getSacstypes().get(4));
			lifacmvrec.setGlsign(t5645.getSigns().get(4));
			lifacmvrec.setGlcode(t5645.getGlmaps().get(4));
			lifacmvrec.setContot(t5645.getCnttots().get(4));

			StringBuilder wsaaRldgacct = new StringBuilder(ovrdueDTO.getChdrnum());
			wsaaRldgacct.append(ovrdueDTO.getLife()).append(ovrdueDTO.getCoverage()).append(ovrdueDTO.getRider())
					.append(ovrdueDTO.getPlanSuffix());
			lifacmvrec.setRldgacct(wsaaRldgacct.toString());
			lifacmvrec.getSubstituteCodes().set(5, ovrdueDTO.getCrtable());
		} else {
			lifacmvrec.setSacscode(t5645.getSacscodes().get(1));
			lifacmvrec.setSacstyp(t5645.getSacstypes().get(1));
			lifacmvrec.setGlsign(t5645.getSigns().get(1));
			lifacmvrec.setGlcode(t5645.getGlmaps().get(1));
			lifacmvrec.setContot(t5645.getCnttots().get(1));
			lifacmvrec.getSubstituteCodes().set(5, "");
			lifacmvrec.setRldgacct(ovrdueDTO.getChdrnum());
		}
		lifacmvrec.getSubstituteCodes().set(0, ovrdueDTO.getCnttype());
		lifacmv.executePSTW(lifacmvrec);
	}


	protected void overrideCommAcmv() throws IOException, ParseException {
		if (wsaaCommRecovered.compareTo(BigDecimal.ZERO)==0) {
			return;
		}
		/* Write a ACMV for the clawback commission. */
		lifacmvrec.setRldgacct("");
		lifacmvrec.setRdocnum(ovrdueDTO.getChdrnum());
		lifacmvrec.setJrnseq(lifacmvrec.getJrnseq()+1);
		lifacmvrec.setBatccoy(ovrdueDTO.getChdrcoy());
		lifacmvrec.setRldgcoy(ovrdueDTO.getChdrcoy());
		lifacmvrec.setGenlcoy(ovrdueDTO.getChdrcoy());
		lifacmvrec.setBatcactyr(ovrdueDTO.getAcctyear());
		lifacmvrec.setBatctrcde(ovrdueDTO.getTrancode());
		lifacmvrec.setBatcactmn(ovrdueDTO.getAcctmonth());
		lifacmvrec.setBatcbatch(ovrdueDTO.getBatcbatch());
		lifacmvrec.setBatcbrn(ovrdueDTO.getBatcbrn());
		lifacmvrec.setTranno(ovrdueDTO.getTranno());
		lifacmvrec.setOrigcurr(ovrdueDTO.getCntcurr());
		lifacmvrec.setRcamt(BigDecimal.ZERO);
		lifacmvrec.setCrate(BigDecimal.ZERO);
		lifacmvrec.setAcctamt(BigDecimal.ZERO);
		lifacmvrec.setFrcdate(CommonConstants.MAXDATE);
		lifacmvrec.setEffdate(ovrdueDTO.getPtdate());
		lifacmvrec.setTranref(ovrdueDTO.getChdrnum());
		lifacmvrec.setUser(ovrdueDTO.getUser());
		lifacmvrec.setTrandesc(wsaaTransDesc);
		/* MOVE OVRD-AGNTNUM TO LIFA-RLDGACCT. <002> */
		/* MOVE AGCMLAP-AGNTNUM TO LIFA-RLDGACCT. <007> */
		lifacmvrec.setRldgacct(agcmIO.getAgntnum());
		lifacmvrec.setTransactionDate(ovrdueDTO.getTranDate());
		lifacmvrec.setTransactionTime(ovrdueDTO.getTranTime());
		lifacmvrec.setTermid(ovrdueDTO.getTermid());
		lifacmvrec.setOrigamt(wsaaCommRecovered);
		lifacmvrec.setSacscode(t5645.getSacscodes().get(2));
		lifacmvrec.setSacstyp(t5645.getSacstypes().get(2));
		lifacmvrec.setGlsign(t5645.getSigns().get(2));
		lifacmvrec.setGlcode(t5645.getGlmaps().get(2));
		lifacmvrec.setContot(t5645.getCnttots().get(2));
		lifacmvrec.getSubstituteCodes().set(0, ovrdueDTO.getCnttype());
		lifacmvrec.getSubstituteCodes().set(5, "");
		
		lifacmv.executePSTW(lifacmvrec);
		
		if (StringUtils.equals(t5688.getComlvlacc(), "Y")) {
			lifacmvrec.setSacscode(t5645.getSacscodes().get(5));
			lifacmvrec.setSacstyp(t5645.getSacstypes().get(5));
			lifacmvrec.setGlsign(t5645.getSigns().get(5));
			lifacmvrec.setGlcode(t5645.getGlmaps().get(5));
			lifacmvrec.setContot(t5645.getCnttots().get(5));
			
			StringBuilder wsaaRldgacct = new StringBuilder(ovrdueDTO.getChdrnum());
			wsaaRldgacct.append(ovrdueDTO.getLife()).append(ovrdueDTO.getCoverage()).append(ovrdueDTO.getRider())
					.append(ovrdueDTO.getPlanSuffix());
			lifacmvrec.setRldgacct(wsaaRldgacct.toString());
			
			lifacmvrec.getSubstituteCodes().set(5, ovrdueDTO.getCrtable());
		} else {
			lifacmvrec.setSacscode(t5645.getSacscodes().get(3));
			lifacmvrec.setSacstyp(t5645.getSacstypes().get(3));
			lifacmvrec.setGlsign(t5645.getSigns().get(3));
			lifacmvrec.setGlcode(t5645.getGlmaps().get(3));
			lifacmvrec.setContot(t5645.getCnttots().get(3));
			lifacmvrec.getSubstituteCodes().set(5, "");
			lifacmvrec.setRldgacct(ovrdueDTO.getChdrnum());
		}
		lifacmvrec.getSubstituteCodes().set(0, ovrdueDTO.getCnttype());
		lifacmv.executePSTW(lifacmvrec);
	}

	protected void loans() {
		List<Loanpf> loanpfList = loanpfDAO.getLoanpfListByCompanyAndContractNumber(ovrdueDTO.getChdrcoy(),
				ovrdueDTO.getChdrnum());
		if (CollectionUtils.isEmpty(loanpfList)) {
			return ;
		}
		for (Loanpf pf : loanpfList) {
			if (pf.getLoannumber() == 0) {
				processLoans(pf);
			}
		}
	}


	protected void processLoans(Loanpf loanenqIO) {
		if (StringUtils.equals(loanenqIO.getValidflag(), "1")) {
			loanenqIO.setLtranno(ovrdueDTO.getTranno());
			loanenqIO.setValidflag("2");
		}

		int row = loanpfDAO.insertLoanpf(loanenqIO);
		if (row < 1) {
			throw new StopRunException("Loanpf not inserted");
		}
	}

	protected void processReassurance() {
		StringBuilder wsaaBatckey = new StringBuilder();
		wsaaBatckey.append("BA");
		wsaaBatckey.append(ovrdueDTO.getChdrcoy());
		wsaaBatckey.append(ovrdueDTO.getBatcbrn());
		wsaaBatckey.append(ovrdueDTO.getAcctyear());
		wsaaBatckey.append(ovrdueDTO.getAcctmonth());
		wsaaBatckey.append(ovrdueDTO.getTrancode());
		wsaaBatckey.append(ovrdueDTO.getBatcbatch());

		TrmreasDTO trmreasrec = new TrmreasDTO();
		trmreasrec.setStatuz("****");
		trmreasrec.setFunction("TRMR");
		trmreasrec.setChdrcoy(ovrdueDTO.getChdrcoy());
		trmreasrec.setChdrnum(ovrdueDTO.getChdrnum());
		trmreasrec.setLife(ovrdueDTO.getLife());
		trmreasrec.setCoverage(ovrdueDTO.getCoverage());
		trmreasrec.setRider(ovrdueDTO.getRider());
		trmreasrec.setPlanSuffix(ovrdueDTO.getPlanSuffix());
		trmreasrec.setCnttype(ovrdueDTO.getCnttype());
		trmreasrec.setCrtable(ovrdueDTO.getCrtable());
		trmreasrec.setPolsum(ovrdueDTO.getPolsum());
		trmreasrec.setEffdate(ovrdueDTO.getPtdate());
		trmreasrec.setBatckey(wsaaBatckey.toString());
		trmreasrec.setTranno(ovrdueDTO.getTranno());
		trmreasrec.setLanguage(ovrdueDTO.getLanguage());
		trmreasrec.setBillfreq(ovrdueDTO.getBillfreq());
		trmreasrec.setPtdate(ovrdueDTO.getPtdate());
		trmreasrec.setOrigcurr(ovrdueDTO.getCntcurr());
		trmreasrec.setAcctcurr(ovrdueDTO.getCntcurr());
		trmreasrec.setCrrcd(ovrdueDTO.getCrrcd());
		trmreasrec.setConvUnits(0);
		//trmreasrec.setJlife("");
		trmreasrec.setSingp(ovrdueDTO.getInstprem());
		trmreasrec.setOldSumins(ovrdueDTO.getSumins());
		trmreasrec.setPstatcode(ovrdueDTO.getPstatcode());
		trmreasrec.setClmPercent(BigDecimal.ZERO);
		trmreasrec.setNewSumins(BigDecimal.ZERO);
		trmreas.processTrmreas(trmreasrec);
	}

	protected void callZorcompy() throws Exception {
		ZorlnkDTO zorlnkrec = new ZorlnkDTO();
		zorlnkrec.setAnnprem(agcmIO.getAnnprem());
		zorlnkrec.setEffdate(agcmIO.getEfdate());
		zorlnkrec.setAgent(lifacmvrec.getRldgacct());
		zorlnkrec.setChdrcoy(lifacmvrec.getRldgcoy());
		zorlnkrec.setChdrnum(lifacmvrec.getRdocnum());
		zorlnkrec.setCrtable(lifacmvrec.getSubstituteCodes().get(5));
		zorlnkrec.setPtdate(ovrdueDTO.getPtdate());
		zorlnkrec.setOrigcurr(lifacmvrec.getOrigcurr());
		zorlnkrec.setCrate(lifacmvrec.getCrate());
		zorlnkrec.setOrigamt(lifacmvrec.getOrigamt());
		zorlnkrec.setTranno(ovrdueDTO.getTranno());
		zorlnkrec.setTrandesc(lifacmvrec.getTrandesc());
		zorlnkrec.setTranref(lifacmvrec.getTranref());
		zorlnkrec.setGenlcur(lifacmvrec.getGenlcur());
		zorlnkrec.setSacstyp(lifacmvrec.getSacstyp());
		zorlnkrec.setTermid(lifacmvrec.getTermid());
		zorlnkrec.setCnttype(lifacmvrec.getSubstituteCodes().get(0));
		zorlnkrec.setBatchKey(lifacmvrec.getBatckey());
		zorlnkrec.setClawback("Y");
		zorcompy.processZorcompy(zorlnkrec);
	}
}
