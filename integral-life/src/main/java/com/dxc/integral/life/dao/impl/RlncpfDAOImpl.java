package com.dxc.integral.life.dao.impl;

import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RlncpfDAO;
import com.dxc.integral.life.dao.model.Rlncpf;

@Repository("rlncpfDAO")
public class RlncpfDAOImpl extends BaseDAOImpl implements RlncpfDAO{
	@Override
	public int deleteRlncpfData() {
		String sql = "DELETE FROM RLNCPF";
		return jdbcTemplate.update(sql);
	}
	
	@Override
	public List<Rlncpf> readRecordOfRlncpf() {
		String sql = "select zhospopt from rlncpf";
		return jdbcTemplate.query(sql, new Object[] {},new BeanPropertyRowMapper<Rlncpf>(Rlncpf.class));
	}
}



