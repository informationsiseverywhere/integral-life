package com.dxc.integral.life.dao.impl;

/**
 * 
 * @author asirvya
 *
 */

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.model.Loanpf;

@Repository("loanpfDAO")
public class LoanpfDAOImpl extends BaseDAOImpl implements LoanpfDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.dxc.integral.life.dao.LoanpfDAO#insertLoanpf(com.dxc.integral.life.
	 * dao.model.Loanpf)
	 */
	@Override
	public int insertLoanpf(Loanpf loanpf) {
		StringBuilder query = new StringBuilder(
				"INSERT INTO Loanpf ( chdrcoy,chdrnum,loannumber,loanType, loancurr,  validflag, ");
		query.append("ftranno,ltranno,loanorigam,loanstdate,lstcaplamt, lstcapdate, nxtcapdate, lstintbdte, ");
		query.append("nxtintbdte,tplstmdty, termid, user_t, trdt, trtm, usrprf, jobnm,datime ) ");
		query.append("VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )");
		return jdbcTemplate.update(query.toString(), setLoanpfValues(loanpf));
	}

	private Object[] setLoanpfValues(Loanpf loanpf) {
		return new Object[] { loanpf.getChdrcoy(), loanpf.getChdrnum(), loanpf.getLoannumber(), loanpf.getLoantype(),
				loanpf.getLoancurr(), loanpf.getValidflag(), loanpf.getFtranno(), loanpf.getLtranno(),
				loanpf.getLoanorigam(), loanpf.getLoanstdate(), loanpf.getLstcaplamt(), loanpf.getLstcapdate(),
				loanpf.getNxtcapdate(), loanpf.getLstintbdte(), loanpf.getNxtintbdte(), loanpf.getTplstmdty(),
				loanpf.getTermid(), loanpf.getUser_t(), loanpf.getTrdt(), loanpf.getTrtm(), loanpf.getUsrprf(),
				loanpf.getJobnm(), loanpf.getDatime() };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.dxc.integral.life.dao.LoanpfDAO#updateLoanpf(com.dxc.integral.life.
	 * dao.model.Loanpf)
	 */
	@Override
	public int updateLoanpf(Loanpf loanpf) {
		StringBuilder query = new StringBuilder("UPDATE Loanpf SET ");
		query.append(
				"chdrcoy=?,chdrnum=?,loanNumber=?, loanType=?, loanCurr=?, validflag=?, ftTranno=?, ltranno=?, loanorigam=?, loanstdate=?,lstcaplamt=?, ");
		query.append(
				"lstcapdate=?,nxtcapdate=?,lstintbdte=?,nxtintbdte=?,tplstmdty=?, termid=?, user_t=?, trdt=?, trtm=?,  usrprf=?,jobnm=?, datime=?  ");
		query.append("WHERE unique_number=? ");
		return jdbcTemplate.update(query.toString(), getLoanpfArrForUpdate(loanpf));
	}

	/**
	 * Prepares object array for update loanpf.
	 * 
	 * @param loanpf
	 * @return object array 
	 */
	private Object[] getLoanpfArrForUpdate(Loanpf loanpf) {
		return new Object[] { loanpf.getChdrcoy(), loanpf.getChdrnum(), loanpf.getLoannumber(), loanpf.getLoantype(),
				loanpf.getLoancurr(), loanpf.getValidflag(), loanpf.getFtranno(), loanpf.getLtranno(),
				loanpf.getLoanorigam(), loanpf.getLoanstdate(), loanpf.getLstcaplamt(), loanpf.getLstcapdate(),
				loanpf.getNxtcapdate(), loanpf.getLstintbdte(), loanpf.getNxtintbdte(), loanpf.getTplstmdty(),
				loanpf.getTermid(), loanpf.getUser_t(), loanpf.getTrdt(), loanpf.getTrtm(), loanpf.getUsrprf(),
				loanpf.getJobnm(), loanpf.getDatime(), loanpf.getUnique_number() };
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.LoanpfDAO#getLastLoanNumberByCompanyAndContractNumber(java.lang.String, java.lang.String)
	 */
	@Override
	public Loanpf getLastLoanNumberByCompanyAndContractNumber(String chdrcoy, String chdrnum) {
		StringBuilder query = new StringBuilder("SELECT * FROM LOANPF WHERE CHDRCOY=? AND CHDRNUM=? ");
		query.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER DESC, UNIQUE_NUMBER DESC");
		List<Loanpf> loanpfRecords = jdbcTemplate.query(query.toString(),
				new Object[] { chdrcoy, chdrnum }, new BeanPropertyRowMapper<Loanpf>(Loanpf.class));

		if (loanpfRecords != null && !loanpfRecords.isEmpty()) {
			return loanpfRecords.get(0);
		}
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.LoanpfDAO#getLoanpfByCompanyContractNumberAndLoanNumber(java.lang.String, java.lang.String, int)
	 */
	@Override
	public Loanpf getLoanpfByCompanyContractNumberAndLoanNumber(String chdrcoy, String chdrnum, int loanNumber) {
		StringBuilder query = new StringBuilder("SELECT * FROM LOANPF WHERE AND CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=? ");
		query.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER DESC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.queryForObject(query.toString(),
				new Object[] { chdrcoy, chdrnum, loanNumber }, new BeanPropertyRowMapper<Loanpf>(Loanpf.class));
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.LoanpfDAO#getLoanpfListByCompanyAndContractNumber(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Loanpf> getLoanpfListByCompanyAndContractNumber(String chdrcoy, String chdrnum) {
		StringBuilder query = new StringBuilder("SELECT * FROM LOANPF WHERE CHDRCOY=? AND CHDRNUM=? ");
		query.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER DESC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.query(query.toString(),
				new Object[] { chdrcoy, chdrnum }, new BeanPropertyRowMapper<Loanpf>(Loanpf.class));
	}
	
	@Override
	public List<Loanpf> getLoanenqList(String chdrcoy, String chdrnum) {
		StringBuilder query = new StringBuilder("SELECT * FROM LOANENQ WHERE CHDRCOY=? AND CHDRNUM=? ");
		query.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER ASC, UNIQUE_NUMBER DESC");
		return jdbcTemplate.query(query.toString(),
				new Object[] { chdrcoy, chdrnum }, new BeanPropertyRowMapper<Loanpf>(Loanpf.class));
	}
	
	@Override
	public int updateLoanenq(Loanpf loanpf) {
		StringBuilder sql = new StringBuilder("UPDATE LOANPF SET LSTINTBDTE=?, NXTINTBDTE=?, VALIDFLAG=?, LOANNUMBER=? WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=?");
		sql.append(" order by CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER ASC, UNIQUE_NUMBER DESC");
        
		return jdbcTemplate.update(sql.toString(), new Object[] {loanpf.getLstintbdte(), loanpf.getNxtintbdte(), loanpf.getValidflag(), loanpf.getLoannumber(), 
				loanpf.getChdrcoy(), loanpf.getChdrnum(), loanpf.getLoannumber()});
	}
	
	@Override
	public int deleteLoadnpf(String chdrcoy, String chdrnum, int loannumber) {
		String sql = "DELETE FROM LOANPF WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=? AND VALIDFLAG= ?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, loannumber, "1"});
	}
	
	@Override
	public void updateLoanpfByUniqueNumberList(List<Loanpf> loanList) {
		List<Loanpf> tempLoanpfList = loanList;   
		StringBuilder sb = new StringBuilder(" UPDATE LOANPF SET VALIDFLAG=?, LSTINTBDTE=?, NXTINTBDTE=?, TRDT=?, TRTM=?,");
		sb.append(" USRPRF=?, JOBNM=?, DATIME=? WHERE UNIQUE_NUMBER=?");     
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempLoanpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, tempLoanpfList.get(i).getValidflag());
            	ps.setInt(2, tempLoanpfList.get(i).getLstintbdte());
            	ps.setInt(3, tempLoanpfList.get(i).getNxtintbdte());
            	ps.setInt(4, tempLoanpfList.get(i).getTrdt());
            	ps.setInt(5, tempLoanpfList.get(i).getTrtm());
            	ps.setString(6, tempLoanpfList.get(i).getUsrprf());
            	ps.setString(7, tempLoanpfList.get(i).getJobnm());
                ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(9, tempLoanpfList.get(i).getUnique_number());
            }  
      }); 
		
	}

}
