package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

/**
 * @author wli31
 */
public class T3684 {
  	public String assumePaid;
  	public String bankaccdsc;
  	public String bankacount;
  	public String bankcode;
  	public String bankdesc;
  	public String bankkey;
  	public String bnkcdedsc;
  	public String branchdesc;
  	public int btapseqno;
  	public String currcode;
  	public String mandamtType;
  	public String mandstat;
  	public String pduserno;
  	public BigDecimal tdcmncg;
  	public BigDecimal tddmncg;
  	public String bnkacctyp;
  	public String mandopt;
  	public String collnmethtype;
	public String getAssumePaid() {
		return assumePaid;
	}
	public void setAssumePaid(String assumePaid) {
		this.assumePaid = assumePaid;
	}
	public String getBankaccdsc() {
		return bankaccdsc;
	}
	public void setBankaccdsc(String bankaccdsc) {
		this.bankaccdsc = bankaccdsc;
	}
	public String getBankacount() {
		return bankacount;
	}
	public void setBankacount(String bankacount) {
		this.bankacount = bankacount;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public String getBankdesc() {
		return bankdesc;
	}
	public void setBankdesc(String bankdesc) {
		this.bankdesc = bankdesc;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBnkcdedsc() {
		return bnkcdedsc;
	}
	public void setBnkcdedsc(String bnkcdedsc) {
		this.bnkcdedsc = bnkcdedsc;
	}
	public String getBranchdesc() {
		return branchdesc;
	}
	public void setBranchdesc(String branchdesc) {
		this.branchdesc = branchdesc;
	}
	public int getBtapseqno() {
		return btapseqno;
	}
	public void setBtapseqno(int btapseqno) {
		this.btapseqno = btapseqno;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getMandamtType() {
		return mandamtType;
	}
	public void setMandamtType(String mandamtType) {
		this.mandamtType = mandamtType;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getPduserno() {
		return pduserno;
	}
	public void setPduserno(String pduserno) {
		this.pduserno = pduserno;
	}
	public BigDecimal getTdcmncg() {
		return tdcmncg;
	}
	public void setTdcmncg(BigDecimal tdcmncg) {
		this.tdcmncg = tdcmncg;
	}
	public BigDecimal getTddmncg() {
		return tddmncg;
	}
	public void setTddmncg(BigDecimal tddmncg) {
		this.tddmncg = tddmncg;
	}
	public String getBnkacctyp() {
		return bnkacctyp;
	}
	public void setBnkacctyp(String bnkacctyp) {
		this.bnkacctyp = bnkacctyp;
	}
	public String getMandopt() {
		return mandopt;
	}
	public void setMandopt(String mandopt) {
		this.mandopt = mandopt;
	}
	public String getCollnmethtype() {
		return collnmethtype;
	}
	public void setCollnmethtype(String collnmethtype) {
		this.collnmethtype = collnmethtype;
	}
  	
  	
}
