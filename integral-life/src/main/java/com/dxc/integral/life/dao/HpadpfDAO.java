package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Hpadpf;

/**
 * HpadpfDAO for database table <b>HPADPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@FunctionalInterface
public interface HpadpfDAO {

	/**
	 * Reads HPADPF based on company and contract number.
	 * 
	 * @param company
	 *            - company number
	 * @param contractNumber
	 *            - contract number
	 * @return - HPADPF record
	 */
	Hpadpf readHpadpfByCompanyAndContractNumber(String company, String contractNumber);
}
