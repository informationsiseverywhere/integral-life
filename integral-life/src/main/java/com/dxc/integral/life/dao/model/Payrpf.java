package com.dxc.integral.life.dao.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * @author wli31
 */
public class Payrpf implements Serializable{
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private Integer payrseqno;
	private Integer effdate;
	private String validflag;
	private String billchnl;
	private String billfreq;
	private Integer billcd;
	private Integer nextdate;
	private Integer btdate;
	private Integer ptdate;
	private String billcurr;
	private String cntcurr;
	private BigDecimal sinstamt01;
	private BigDecimal sinstamt02;
	private BigDecimal sinstamt03;
	private BigDecimal sinstamt04;
	private BigDecimal sinstamt05;
	private BigDecimal sinstamt06;
	private List<BigDecimal> sinsAmtList;
	private BigDecimal outstamt;
	private String taxrelmth;
	private String billsupr;
	private String aplsupr;
	private String notssupr;
    private Integer billspto;
    private Integer billspfrom;
    private String mandref;
	private Integer aplspfrom;
	private Integer notsspfrom;
	private Integer aplspto;
	private Integer notsspto;

	private String grupkey;

	private Integer incomeSeqNo;
	private Integer tranno;
	private String grupcoy;
	private String grupnum;
	private String membsel;

	private String billnet;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer user_t;
	private String pstcde;
	private String usrprf;
	private String jobnm;
	private Integer transactionDate;
	private Integer transactionTime;
	private Integer user;
	private String pstatcode;
	private String userProfile;
	private String jobName;
	private String datime;
    private String billday;
    private String billmonth;
    private String duedd;
    private String duemm;
    private String zmandref;
	public Payrpf() {
	}
	public Payrpf(Payrpf payrpf) {
		this.uniqueNumber = payrpf.uniqueNumber; 
		this.chdrcoy = payrpf.chdrcoy;
		this.chdrnum = payrpf.chdrnum;
		this.payrseqno = payrpf.payrseqno;
		this.effdate = payrpf.effdate;
		this.validflag = payrpf.validflag;
		this.billchnl = payrpf.billchnl;
		this.billfreq = payrpf.billfreq;
		this.billcd = payrpf.billcd;
		this.nextdate = payrpf.nextdate;
		this.btdate = payrpf.btdate;
		this.ptdate = payrpf.ptdate;
		this.billcurr = payrpf.billcurr;
		this.cntcurr = payrpf.cntcurr;
		this.sinstamt01 = payrpf.sinstamt01;
		this.sinstamt02 = payrpf.sinstamt02;
		this.sinstamt03 = payrpf.sinstamt03;
		this.sinstamt04 = payrpf.sinstamt04;
		this.sinstamt05 = payrpf.sinstamt05;
		this.sinstamt06 = payrpf.sinstamt06;
		this.taxrelmth = payrpf.taxrelmth;
		this.billsupr = payrpf.billsupr;
		this.billspto = payrpf.billspto;
		this.billspfrom = payrpf.billspfrom;
		this.mandref = payrpf.mandref;
		this.grupkey = payrpf.grupkey;
		this.incomeSeqNo = payrpf.incomeSeqNo;
		this.notssupr = payrpf.notssupr;
		this.notsspfrom = payrpf.notsspfrom;
		this.notsspto = payrpf.notsspto;
		this.aplsupr = payrpf.aplsupr;
		this.aplspfrom = payrpf.aplspfrom;
		this.aplspto = payrpf.aplspto;
		this.tranno = payrpf.tranno;
		this.outstamt = payrpf.outstamt;
		this.pstatcode = payrpf.pstatcode;
		this.grupcoy = payrpf.grupcoy;
		this.grupnum = payrpf.grupnum;
		this.membsel = payrpf.membsel;
		this.billnet = payrpf.billnet;
		this.termid = payrpf.termid;
		this.transactionDate = payrpf.transactionDate;
		this.transactionTime = payrpf.transactionTime;
		this.trdt = payrpf.trdt;
		this.trtm = payrpf.trtm;
		this.user = payrpf.user;
		this.user_t = payrpf.user_t;
		this.userProfile = payrpf.userProfile;
		this.jobName = payrpf.jobName;
		this.datime = payrpf.datime;
		this.billday = payrpf.billday;
		this.billmonth = payrpf.billmonth;
		this.duedd = payrpf.duedd;
		this.duemm = payrpf.duemm;
		this.pstcde = payrpf.pstcde;
		this.zmandref = payrpf.zmandref;
		this.usrprf = payrpf.usrprf;
		this.jobnm = payrpf.jobnm;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public Integer getPayrseqno() {
		return payrseqno;
	}

	public void setPayrseqno(Integer payrseqno) {
		this.payrseqno = payrseqno;
	}

	public Integer getEffdate() {
		return effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getBillchnl() {
		return billchnl;
	}

	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public Integer getBillcd() {
		return billcd;
	}

	public void setBillcd(Integer billcd) {
		this.billcd = billcd;
	}

	public Integer getNextdate() {
		return nextdate;
	}

	public void setNextdate(Integer nextdate) {
		this.nextdate = nextdate;
	}

	public Integer getBtdate() {
		return btdate;
	}

	public void setBtdate(Integer btdate) {
		this.btdate = btdate;
	}

	public Integer getPtdate() {
		return ptdate;
	}

	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}

	public String getBillcurr() {
		return billcurr;
	}

	public void setBillcurr(String statreasn) {
		this.billcurr = statreasn;
	}

	public String getCntcurr() {
		return cntcurr;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	public BigDecimal getSinstamt01() {
		return sinstamt01;
	}

	public void setSinstamt01(BigDecimal sinstamt01) {
		this.sinstamt01 = sinstamt01;
	}

	public BigDecimal getSinstamt02() {
		return sinstamt02;
	}

	public void setSinstamt02(BigDecimal sinstamt02) {
		this.sinstamt02 = sinstamt02;
	}

	public BigDecimal getSinstamt03() {
		return sinstamt03;
	}

	public void setSinstamt03(BigDecimal sinstamt03) {
		this.sinstamt03 = sinstamt03;
	}

	public BigDecimal getSinstamt04() {
		return sinstamt04;
	}

	public void setSinstamt04(BigDecimal sinstamt04) {
		this.sinstamt04 = sinstamt04;
	}

	public BigDecimal getSinstamt05() {
		return sinstamt05;
	}

	public void setSinstamt05(BigDecimal sinstamt05) {
		this.sinstamt05 = sinstamt05;
	}

	public BigDecimal getSinstamt06() {
		return sinstamt06;
	}

	public void setSinstamt06(BigDecimal sinstamt06) {
		this.sinstamt06 = sinstamt06;
	}

	public BigDecimal getOutstamt() {
		return outstamt;
	}

	public void setOutstamt(BigDecimal outstamt) {
		this.outstamt = outstamt;
	}

	public String getTaxrelmth() {
		return taxrelmth;
	}

	public void setTaxrelmth(String taxrelmth) {
		this.taxrelmth = taxrelmth;
	}

	public String getBillsupr() {
		return billsupr;
	}

	public void setBillsupr(String billsupr) {
		this.billsupr = billsupr;
	}

	public String getAplsupr() {
		return aplsupr;
	}

	public void setAplsupr(String aplsupr) {
		this.aplsupr = aplsupr;
	}

	public String getNotssupr() {
		return notssupr;
	}

	public void setNotssupr(String notssupr) {
		this.notssupr = notssupr;
	}
	public Integer getAplspfrom() {
		return aplspfrom;
	}

	public void setAplspfrom(Integer aplspfrom) {
		this.aplspfrom = aplspfrom;
	}

	public Integer getNotsspfrom() {
		return notsspfrom;
	}

	public void setNotsspfrom(Integer notsspfrom) {
		this.notsspfrom = notsspfrom;
	}

	public Integer getBillspto() {
		return billspto;
	}

	public void setBillspto(Integer billspto) {
		this.billspto = billspto;
	}

	public Integer getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(Integer billspfrom) {
		this.billspfrom = billspfrom;
	}
	public Integer getAplspto() {
		return aplspto;
	}

	public void setAplspto(Integer aplspto) {
		this.aplspto = aplspto;
	}

	public Integer getNotsspto() {
		return notsspto;
	}

	public void setNotsspto(Integer notsspto) {
		this.notsspto = notsspto;
	}

	public String getMandref() {
		return mandref;
	}

	public void setMandref(String mandref) {
		this.mandref = mandref;
	}

	public String getGrupkey() {
		return grupkey;
	}

	public void setGrupkey(String grupkey) {
		this.grupkey = grupkey;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public Integer getIncomeSeqNo() {
		return incomeSeqNo;
	}

	public void setIncomeSeqNo(Integer incomeSeqNo) {
		this.incomeSeqNo = incomeSeqNo;
	}

	public String getGrupcoy() {
		return grupcoy;
	}

	public void setGrupcoy(String grupcoy) {
		this.grupcoy = grupcoy;
	}

	public String getGrupnum() {
		return grupnum;
	}

	public void setGrupnum(String grupnum) {
		this.grupnum = grupnum;
	}

	public String getMembsel() {
		return membsel;
	}

	public void setMembsel(String membsel) {
		this.membsel = membsel;
	}

	public String getBillnet() {
		return billnet;
	}

	public void setBillnet(String billnet) {
		this.billnet = billnet;
	}

	
	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUser_t() {
		return user_t;
	}
	public void setUser_t(Integer user_t) {
		this.user_t = user_t;
	}
	public String getPstcde() {
		return pstcde;
	}
	public void setPstcde(String pstcde) {
		this.pstcde = pstcde;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public void setSinsAmtList(List<BigDecimal> sinsAmtList) {
		this.sinsAmtList = sinsAmtList;
	}
	public Integer getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Integer transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Integer getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(Integer transactionTime) {
		this.transactionTime = transactionTime;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}

	public String getPstatcode() {
		return pstatcode;
	}

	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getBillday() {
        return billday;
    }
	public String getBillmonth() {
        return billmonth;
    }
    public String getDuedd() {
        return duedd;
    }
    public String getDuemm() {
        return duemm;
    }
    public String getZmandref() {
        return zmandref;
    }
	public List<BigDecimal> getSinsAmtList() {
		sinsAmtList = new ArrayList<>();
		sinsAmtList.add(0, sinstamt01);
		sinsAmtList.add(1, sinstamt02);
		sinsAmtList.add(2, sinstamt03);
		sinsAmtList.add(3, sinstamt04);
		sinsAmtList.add(4, sinstamt05);
		sinsAmtList.add(5, sinstamt06);
		return sinsAmtList;
	}    
	public void setBillday(String billday) {
        this.billday = billday;
    }
	public void setBillmonth(String billmonth) {
        this.billmonth = billmonth;
    }
    public void setDuedd(String duedd) {
        this.duedd = duedd;
    }
    public void setDuemm(String duemm) {
        this.duemm = duemm;
    }
    public void setZmandref(String zmandref) {
        this.zmandref = zmandref;
    }
}
