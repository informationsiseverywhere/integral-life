package com.dxc.integral.life.dao.model;

public class Prmhpf {

	private long unique_Number;
	private String chdrcoy;
	private String chdrnum;
	private String validflag;
	private String actind;
	private int frmdate;
	private int todate;
	private int tranno;
	private int effdate;
	private String apind;
	private String logtype;
	private String crtuser;
	private int ltranno;
	private String billfreq;
	private int zmthos;
	private String usrprf;
	private String jobnm;
	
	public long getUnique_Number() {
		return unique_Number;
	}
	public void setUnique_Number(long unique_Number) {
		this.unique_Number = unique_Number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getActind() {
		return actind;
	}
	public void setActind(String actind) {
		this.actind = actind;
	}
	public int getFrmdate() {
		return frmdate;
	}
	public void setFrmdate(int frmdate) {
		this.frmdate = frmdate;
	}
	public int getTodate() {
		return todate;
	}
	public void setTodate(int todate) {
		this.todate = todate;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getApind() {
		return apind;
	}
	public void setApind(String apind) {
		this.apind = apind;
	}
	public String getLogtype() {
		return logtype;
	}
	public void setLogtype(String logtype) {
		this.logtype = logtype;
	}
	public String getCrtuser() {
		return crtuser;
	}
	public void setCrtuser(String crtuser) {
		this.crtuser = crtuser;
	}
	public int getLtranno() {
		return ltranno;
	}
	public void setLtranno(int ltranno) {
		this.ltranno = ltranno;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getZmthos() {
		return zmthos;
	}
	public void setZmthos(int zmthos) {
		this.zmthos = zmthos;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	
}
