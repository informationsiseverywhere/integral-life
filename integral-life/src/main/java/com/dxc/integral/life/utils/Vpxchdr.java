package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VpxchdrDTO;

public interface Vpxchdr {
	public VpxchdrDTO callInit(PremiumDTO premiumDTO);
}
