package com.dxc.integral.life.beans;


/**
 * 	
 * @author: wli31
 *
 */
public class LifsttrDTO{
  	private String function;
  	private String statuz;
  	private String batckey;
  	private String filler;
  	private String batccoy;
  	private String batcbrn;
  	private Integer batcactyr;
  	private Integer batcactmn;
  	private String batctrcde;
  	private String batcbatch;
  	private String chdrcoy;
  	private String chdrnum;
  	private String agntnum;
  	private String oldAgntnum;
  	private String minorChg;
  	private Integer tranno;
  	private Integer trannor;
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getBatckey() {
		return batckey;
	}
	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcacty(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getOldAgntnum() {
		return oldAgntnum;
	}
	public void setOldAgntnum(String oldAgntnum) {
		this.oldAgntnum = oldAgntnum;
	}
	public String getMinorChg() {
		return minorChg;
	}
	public void setMinorChg(String minorChg) {
		this.minorChg = minorChg;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public Integer getTrannor() {
		return trannor;
	}
	public void setTrannor(Integer trannor) {
		this.trannor = trannor;
	}


}