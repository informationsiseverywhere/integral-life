package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.beans.VpxlextDTO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.utils.Vpxlext;
@Service("vpxlext")
@Lazy
public class VpxlextImpl implements Vpxlext {
	@Autowired
	private LextpfDAO lextpfDAO;
	
	VpxlextDTO vpxlextDTO = new VpxlextDTO();
	@Override
	public VpxlextDTO processVpxlext(String function, PremiumDTO premiumDTO) {
		vpxlextDTO.setStatuz("****");
		if ("INIT".equals(function)){
			initialize(premiumDTO);
			findLext(premiumDTO);
			vpxlextDTO.setCount(vpxlextDTO.getCount() - 1);
			vpxlextDTO.setTabsorsel("N");
		}
		return vpxlextDTO;
	}
	protected void initialize(PremiumDTO premiumDTO){
		if("".equals(premiumDTO.getDialdownoption()))
			premiumDTO.setDialdownoption("100");
		vpxlextDTO.setCount(0);
		vpxlextDTO.setOppc01(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct01(0);
		vpxlextDTO.setAgerate01(0);
		vpxlextDTO.setInsprm01(0);
		vpxlextDTO.setOpcda01("");
		
		vpxlextDTO.setOppc02(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct02(0);
		vpxlextDTO.setAgerate02(0);
		vpxlextDTO.setInsprm02(0);
		vpxlextDTO.setOpcda02("");
		
		vpxlextDTO.setOppc03(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct03(0);
		vpxlextDTO.setAgerate03(0);
		vpxlextDTO.setInsprm03(0);
		vpxlextDTO.setOpcda03("");
		
		vpxlextDTO.setOppc04(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct04(0);
		vpxlextDTO.setAgerate04(0);
		vpxlextDTO.setInsprm04(0);
		vpxlextDTO.setOpcda04("");
		
		vpxlextDTO.setOppc05(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct05(0);
		vpxlextDTO.setAgerate05(0);
		vpxlextDTO.setInsprm05(0);
		vpxlextDTO.setOpcda05("");
		
		vpxlextDTO.setOppc06(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct06(0);
		vpxlextDTO.setAgerate06(0);
		vpxlextDTO.setInsprm06(0);
		vpxlextDTO.setOpcda06("");
		
		vpxlextDTO.setOppc07(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct07(0);
		vpxlextDTO.setAgerate07(0);
		vpxlextDTO.setInsprm07(0);
		vpxlextDTO.setOpcda07("");
		
		vpxlextDTO.setOppc08(BigDecimal.ZERO);
		vpxlextDTO.setZmortpct08(0);
		vpxlextDTO.setAgerate08(0);
		vpxlextDTO.setInsprm08(0);
		vpxlextDTO.setOpcda08("");
		
		vpxlextDTO.setNoOfCoverages(0);
		vpxlextDTO.setAidsCoverInd("");
		vpxlextDTO.setNcdCode("");
		vpxlextDTO.setClaimAutoIncrease("");
		vpxlextDTO.setSyndicateCode("");
		vpxlextDTO.setRiskExpiryAge("");
		vpxlextDTO.setTotalSumInsured(premiumDTO.getSumin());
	}
	protected void findLext(PremiumDTO premiumDTO){
		List<Lextpf> lextpfList = lextpfDAO.getLextpfListRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(),
				premiumDTO.getCovrCoverage(), premiumDTO.getCovrRider());
		if(lextpfList == null || lextpfList.isEmpty()){
			return;
		}
		
		for(int wsaaSub = 0; wsaaSub < lextpfList.size(); wsaaSub++){
			Lextpf lextIO = lextpfList.get(wsaaSub);
			if ("2".equals(premiumDTO.getReasind()) && "1".equals(lextIO.getReasind())){
				break;
			}
			if (!"2".equals(premiumDTO.getReasind()) && "2".equals(lextIO.getReasind())){
				break;
			}
			if (lextIO.getExtcd() < premiumDTO.getReRateDate()){
				continue;
			}
			if(wsaaSub == 0){
				vpxlextDTO.setOppc01(lextIO.getOppc());
				vpxlextDTO.setZmortpct01(lextIO.getZmortpct());
				vpxlextDTO.setOpcda01(lextIO.getOpcda());
				vpxlextDTO.setAgerate01(lextIO.getAgerate());
				vpxlextDTO.setInsprm01(lextIO.getInsprm());
				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 1){
				vpxlextDTO.setOppc02(lextIO.getOppc());
				vpxlextDTO.setZmortpct02(lextIO.getZmortpct());
				vpxlextDTO.setOpcda02(lextIO.getOpcda());
				vpxlextDTO.setAgerate02(lextIO.getAgerate());
				vpxlextDTO.setInsprm02(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 2){
				vpxlextDTO.setOppc03(lextIO.getOppc());
				vpxlextDTO.setZmortpct03(lextIO.getZmortpct());
				vpxlextDTO.setOpcda03(lextIO.getOpcda());
				vpxlextDTO.setAgerate03(lextIO.getAgerate());
				vpxlextDTO.setInsprm03(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 3){
				vpxlextDTO.setOppc04(lextIO.getOppc());
				vpxlextDTO.setZmortpct04(lextIO.getZmortpct());
				vpxlextDTO.setOpcda04(lextIO.getOpcda());
				vpxlextDTO.setAgerate04(lextIO.getAgerate());
				vpxlextDTO.setInsprm04(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 4){
				vpxlextDTO.setOppc05(lextIO.getOppc());
				vpxlextDTO.setZmortpct05(lextIO.getZmortpct());
				vpxlextDTO.setOpcda05(lextIO.getOpcda());
				vpxlextDTO.setAgerate05(lextIO.getAgerate());
				vpxlextDTO.setInsprm05(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 5){
				vpxlextDTO.setOppc06(lextIO.getOppc());
				vpxlextDTO.setZmortpct06(lextIO.getZmortpct());
				vpxlextDTO.setOpcda06(lextIO.getOpcda());
				vpxlextDTO.setAgerate06(lextIO.getAgerate());
				vpxlextDTO.setInsprm06(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 6){
				vpxlextDTO.setOppc07(lextIO.getOppc());
				vpxlextDTO.setZmortpct07(lextIO.getZmortpct());
				vpxlextDTO.setOpcda07(lextIO.getOpcda());
				vpxlextDTO.setAgerate07(lextIO.getAgerate());
				vpxlextDTO.setInsprm07(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}else if(wsaaSub == 7){
				vpxlextDTO.setOppc08(lextIO.getOppc());
				vpxlextDTO.setZmortpct08(lextIO.getZmortpct());
				vpxlextDTO.setOpcda08(lextIO.getOpcda());
				vpxlextDTO.setAgerate08(lextIO.getAgerate());
				vpxlextDTO.setInsprm08(lextIO.getInsprm());

				vpxlextDTO.setCount(wsaaSub);
			}
		}
	}
}
