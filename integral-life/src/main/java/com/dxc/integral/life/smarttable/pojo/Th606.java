package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class Th606 {
  	private String disccntmeth;
  	private List<Integer> insprms;
  	private List<Integer> instprs;
  	public BigDecimal mfacthm;
  	public BigDecimal mfacthy;
  	public BigDecimal mfactm;
  	public BigDecimal mfactq;
  	public BigDecimal mfactw;
  	public BigDecimal mfact2w;
  	public BigDecimal mfact4w;
  	public BigDecimal mfacty;
  	public Integer premUnit;
  	public Integer unit;
  	public Integer insprem;
	public String getDisccntmeth() {
		return disccntmeth;
	}
	public void setDisccntmeth(String disccntmeth) {
		this.disccntmeth = disccntmeth;
	}
	public List<Integer> getInsprms() {
		return insprms;
	}
	public void setInsprms(List<Integer> insprms) {
		this.insprms = insprms;
	}
	
	public List<Integer> getInstprs() {
		return instprs;
	}
	public void setInstprs(List<Integer> instprs) {
		this.instprs = instprs;
	}
	public BigDecimal getMfacthm() {
		return mfacthm;
	}
	public void setMfacthm(BigDecimal mfacthm) {
		this.mfacthm = mfacthm;
	}
	public BigDecimal getMfacthy() {
		return mfacthy;
	}
	public void setMfacthy(BigDecimal mfacthy) {
		this.mfacthy = mfacthy;
	}
	public BigDecimal getMfactm() {
		return mfactm;
	}
	public void setMfactm(BigDecimal mfactm) {
		this.mfactm = mfactm;
	}
	public BigDecimal getMfactq() {
		return mfactq;
	}
	public void setMfactq(BigDecimal mfactq) {
		this.mfactq = mfactq;
	}
	public BigDecimal getMfactw() {
		return mfactw;
	}
	public void setMfactw(BigDecimal mfactw) {
		this.mfactw = mfactw;
	}
	public BigDecimal getMfact2w() {
		return mfact2w;
	}
	public void setMfact2w(BigDecimal mfact2w) {
		this.mfact2w = mfact2w;
	}
	public BigDecimal getMfact4w() {
		return mfact4w;
	}
	public void setMfact4w(BigDecimal mfact4w) {
		this.mfact4w = mfact4w;
	}
	public BigDecimal getMfacty() {
		return mfacty;
	}
	public void setMfacty(BigDecimal mfacty) {
		this.mfacty = mfacty;
	}
	public Integer getPremUnit() {
		return premUnit;
	}
	public void setPremUnit(Integer premUnit) {
		this.premUnit = premUnit;
	}
	public Integer getUnit() {
		return unit;
	}
	public void setUnit(Integer unit) {
		this.unit = unit;
	}
	public Integer getInsprem() {
		return insprem;
	}
	public void setInsprem(Integer insprem) {
		this.insprem = insprem;
	}
  	
}
