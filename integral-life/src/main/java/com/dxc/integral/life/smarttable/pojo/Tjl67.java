package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class Tjl67 {

	private List<String> resultcds;
	private List<String> rcverors;
	
	/**
	 * @return the resultcds
	 */
	public List<String> getResultcds() {
		return resultcds;
	}
	/**
	 * @param resultcds the resultcds to set
	 */
	public void setResultcds(List<String> resultcds) {
		this.resultcds = resultcds;
	}
	/**
	 * @return the rcverors
	 */
	public List<String> getRcverors() {
		return rcverors;
	}
	/**
	 * @param rcverors the rcverors to set
	 */
	public void setRcverors(List<String> rcverors) {
		this.rcverors = rcverors;
	}
	
}
