package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Hitrpf {
    private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int planSuffix;
    private int procSeqNo;
    private String batccoy;
    private String batcbrn;
    private int batcactyr;
    private int batcactmn;
    private String batctrcde;
    private String batcbatch;
    private String zintbfnd;
    private BigDecimal fundAmount;
    private String fundCurrency;
    private BigDecimal fundRate;
    private String zrectyp;
    private int effdate;
    private int tranno;
    private String cntcurr;
    private BigDecimal contractAmount;
    private String cnttyp;
    private String crtable;
    private String covdbtind;
    private String zintappind;
    private String feedbackInd;
    private String triggerModule;
    private String triggerKey;
    private String switchIndicator;
    private BigDecimal surrenderPercent;
    private BigDecimal svp;
    private int zlstintdte;
    private BigDecimal zintrate;
    private int zinteffdt;
    private String zintalloc;
    private int inciNum;
    private int inciPerd01;
    private int inciPerd02;
    private BigDecimal inciprm01;
    private BigDecimal inciprm02;
    private int ustmno;
    private String sacscode;
    private String sacstyp;
    private String genlcde;
    private String scheduleName;
    private int scheduleNumber;
    private String userProfile;
    private String jobName;
    private String datime;
    private String fundpool;

    public long getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getRider() {
        return rider;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public int getPlanSuffix() {
        return planSuffix;
    }

    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }

    public int getProcSeqNo() {
        return procSeqNo;
    }

    public void setProcSeqNo(int procSeqNo) {
        this.procSeqNo = procSeqNo;
    }

    public String getBatccoy() {
        return batccoy;
    }

    public void setBatccoy(String batccoy) {
        this.batccoy = batccoy;
    }

    public String getBatcbrn() {
        return batcbrn;
    }

    public void setBatcbrn(String batcbrn) {
        this.batcbrn = batcbrn;
    }

    public int getBatcactyr() {
        return batcactyr;
    }

    public void setBatcactyr(int batcactyr) {
        this.batcactyr = batcactyr;
    }

    public int getBatcactmn() {
        return batcactmn;
    }

    public void setBatcactmn(int batcactmn) {
        this.batcactmn = batcactmn;
    }

    public String getBatctrcde() {
        return batctrcde;
    }

    public void setBatctrcde(String batctrcde) {
        this.batctrcde = batctrcde;
    }

    public String getBatcbatch() {
        return batcbatch;
    }

    public void setBatcbatch(String batcbatch) {
        this.batcbatch = batcbatch;
    }

    public String getZintbfnd() {
        return zintbfnd;
    }

    public void setZintbfnd(String zintbfnd) {
        this.zintbfnd = zintbfnd;
    }

    public BigDecimal getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(BigDecimal fundAmount) {
        this.fundAmount = fundAmount;
    }

    public String getFundCurrency() {
        return fundCurrency;
    }

    public void setFundCurrency(String fundCurrency) {
        this.fundCurrency = fundCurrency;
    }

    public BigDecimal getFundRate() {
        return fundRate;
    }

    public void setFundRate(BigDecimal fundRate) {
        this.fundRate = fundRate;
    }

    public String getZrectyp() {
        return zrectyp;
    }

    public void setZrectyp(String zrectyp) {
        this.zrectyp = zrectyp;
    }

    public int getEffdate() {
        return effdate;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public int getTranno() {
        return tranno;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public String getCntcurr() {
        return cntcurr;
    }

    public void setCntcurr(String cntcurr) {
        this.cntcurr = cntcurr;
    }

    public BigDecimal getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(BigDecimal contractAmount) {
        this.contractAmount = contractAmount;
    }

    public String getCnttyp() {
        return cnttyp;
    }

    public void setCnttyp(String cnttyp) {
        this.cnttyp = cnttyp;
    }

    public String getCrtable() {
        return crtable;
    }

    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }

    public String getCovdbtind() {
        return covdbtind;
    }

    public void setCovdbtind(String covdbtind) {
        this.covdbtind = covdbtind;
    }

    public String getZintappind() {
        return zintappind;
    }

    public void setZintappind(String zintappind) {
        this.zintappind = zintappind;
    }

    public String getFeedbackInd() {
        return feedbackInd;
    }

    public void setFeedbackInd(String feedbackInd) {
        this.feedbackInd = feedbackInd;
    }

    public String getTriggerModule() {
        return triggerModule;
    }

    public void setTriggerModule(String triggerModule) {
        this.triggerModule = triggerModule;
    }

    public String getTriggerKey() {
        return triggerKey;
    }

    public void setTriggerKey(String triggerKey) {
        this.triggerKey = triggerKey;
    }

    public String getSwitchIndicator() {
        return switchIndicator;
    }

    public void setSwitchIndicator(String switchIndicator) {
        this.switchIndicator = switchIndicator;
    }

    public BigDecimal getSurrenderPercent() {
        return surrenderPercent;
    }

    public void setSurrenderPercent(BigDecimal surrenderPercent) {
        this.surrenderPercent = surrenderPercent;
    }

    public BigDecimal getSvp() {
        return svp;
    }

    public void setSvp(BigDecimal svp) {
        this.svp = svp;
    }

    public int getZlstintdte() {
        return zlstintdte;
    }

    public void setZlstintdte(int zlstintdte) {
        this.zlstintdte = zlstintdte;
    }

    public BigDecimal getZintrate() {
        return zintrate;
    }

    public void setZintrate(BigDecimal zintrate) {
        this.zintrate = zintrate;
    }

    public int getZinteffdt() {
        return zinteffdt;
    }

    public void setZinteffdt(int zinteffdt) {
        this.zinteffdt = zinteffdt;
    }

    public String getZintalloc() {
        return zintalloc;
    }

    public void setZintalloc(String zintalloc) {
        this.zintalloc = zintalloc;
    }

    public int getInciNum() {
        return inciNum;
    }

    public void setInciNum(int inciNum) {
        this.inciNum = inciNum;
    }

    public int getInciPerd01() {
        return inciPerd01;
    }

    public void setInciPerd01(int inciPerd01) {
        this.inciPerd01 = inciPerd01;
    }

    public int getInciPerd02() {
        return inciPerd02;
    }

    public void setInciPerd02(int inciPerd02) {
        this.inciPerd02 = inciPerd02;
    }

    public BigDecimal getInciprm01() {
        return inciprm01;
    }

    public void setInciprm01(BigDecimal inciprm01) {
        this.inciprm01 = inciprm01;
    }

    public BigDecimal getInciprm02() {
        return inciprm02;
    }

    public void setInciprm02(BigDecimal inciprm02) {
        this.inciprm02 = inciprm02;
    }

    public int getUstmno() {
        return ustmno;
    }

    public void setUstmno(int ustmno) {
        this.ustmno = ustmno;
    }

    public String getSacscode() {
        return sacscode;
    }

    public void setSacscode(String sacscode) {
        this.sacscode = sacscode;
    }

    public String getSacstyp() {
        return sacstyp;
    }

    public void setSacstyp(String sacstyp) {
        this.sacstyp = sacstyp;
    }

    public String getGenlcde() {
        return genlcde;
    }

    public void setGenlcde(String genlcde) {
        this.genlcde = genlcde;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public int getScheduleNumber() {
        return scheduleNumber;
    }

    public void setScheduleNumber(int scheduleNumber) {
        this.scheduleNumber = scheduleNumber;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }

	public String getFundpool() {
		return fundpool;
	}

	public void setFundpool(String fundpool) {
		this.fundpool = fundpool;
	}
    
}