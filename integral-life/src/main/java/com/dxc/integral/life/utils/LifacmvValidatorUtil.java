package com.dxc.integral.life.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Validator Utility.
 * 
 * @author vhukumagrawa
 *
 */
public class LifacmvValidatorUtil {

	private LifacmvValidatorUtil() {

	}

	private static final String[] CASH_CODES = new String[] { "T204", "T205", "B228", "B253", "B254", "T2A3", "BRAU",
			"T469", "T470" };
	private static final String[] CHEQ_CODES = new String[] { "B202", "T209", "T206", "T350", "T207", "B273" };
	private static final String[] JRNL_CODES = new String[] { "T301", "T425", "B447", "TRLN" };

	/**
	 * Checks for cash codes.
	 * 
	 * @param trancodeCpy
	 *            - the trancodeCpy
	 * @return boolean - true / false
	 */
	public static Boolean cashCodes(String trancodeCpy) {
		Boolean cashCode = Boolean.FALSE;
		List<String> list = Arrays.asList(CASH_CODES);
		if (list.contains(trancodeCpy)) {
			cashCode = Boolean.TRUE;
		}
		return cashCode;
	}

	/**
	 * Checks for cheqCodes.
	 * 
	 * @param trancodeCpy
	 *            - the trancodeCpy
	 * @return boolean - true / false
	 */
	public static Boolean cheqCodes(String trancodeCpy) {
		Boolean cheqCode = Boolean.FALSE;
		List<String> list = Arrays.asList(CHEQ_CODES);
		if (list.contains(trancodeCpy)) {
			cheqCode = Boolean.TRUE;
		}
		return cheqCode;
	}

	/**
	 * Checks for jrnlCodes.
	 * 
	 * @param trancodeCpy
	 *            - the trancodeCpy
	 * @return boolean - true / false
	 */
	public static Boolean jrnlCodes(String trancodeCpy) {
		Boolean jrnlCode = Boolean.FALSE;
		List<String> list = Arrays.asList(JRNL_CODES);
		if (list.contains(trancodeCpy)) {
			jrnlCode = Boolean.TRUE;
		}
		return jrnlCode;
	}
}
