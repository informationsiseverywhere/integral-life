package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T6651 {
	
	public String adjustiu = "";
  	public String bidoffer = "";
  	public List<BigDecimal> feepcs;
  	public String ovrsuma = "";
  	public List<BigDecimal> pufeemaxs;
  	public List<BigDecimal> pufeemins;
  	public List<BigDecimal> puffamts;
  	
	public String getAdjustiu() {
		return adjustiu;
	}
	public void setAdjustiu(String adjustiu) {
		this.adjustiu = adjustiu;
	}
	public String getBidoffer() {
		return bidoffer;
	}
	public void setBidoffer(String bidoffer) {
		this.bidoffer = bidoffer;
	}
	public List<BigDecimal> getFeepcs() {
		return feepcs;
	}
	public void setFeepcs(List<BigDecimal> feepcs) {
		this.feepcs = feepcs;
	}
	public String getOvrsuma() {
		return ovrsuma;
	}
	public void setOvrsuma(String ovrsuma) {
		this.ovrsuma = ovrsuma;
	}
	public List<BigDecimal> getPufeemaxs() {
		return pufeemaxs;
	}
	public void setPufeemaxs(List<BigDecimal> pufeemaxs) {
		this.pufeemaxs = pufeemaxs;
	}
	public List<BigDecimal> getPufeemins() {
		return pufeemins;
	}
	public void setPufeemins(List<BigDecimal> pufeemins) {
		this.pufeemins = pufeemins;
	}
	public List<BigDecimal> getPuffamts() {
		return puffamts;
	}
	public void setPuffamts(List<BigDecimal> puffamts) {
		this.puffamts = puffamts;
	}
  	
  	
  
}
