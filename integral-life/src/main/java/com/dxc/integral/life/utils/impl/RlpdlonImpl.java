/*********************  */
/*Author  :Liwei						*/
/*Purpose :Instead of subroutine rlpdlon*/
/*Date    :2019.1.31					*/
package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.AcblpfDAO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Acblpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.life.beans.RlpdlonDTO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.Th623;
import com.dxc.integral.life.utils.Rlpdlon;
/**
 * @author wli31
 */
@Service("rlpdlon")
@Lazy
public class RlpdlonImpl implements Rlpdlon {
	
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	
	@Autowired
	private ItempfDAO itempfDAO;
	
	@Autowired
	private PayrpfDAO payrpfDAO;
	
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;
	
	@Autowired
	private AcblpfDAO acblpfDAO;
	
	
	private Chdrpf chdrenq;
	boolean wsaaEof = false;
	boolean endOfFile ;
	Iterator<Acblpf> iterator ;

	private Payrpf payrpf = null;
	
	private Th623 th623IO = null;
	private T5645 t5645IO = null;
	private Acblpf acblpf = null;
	private String wsaaRldgacct = "";
	private List<Acblpf> acblpfList = null;
	public RlpdlonDTO processRlpdlonInfo(RlpdlonDTO rlpdlonDTO) throws IOException  {
		initialize1000(rlpdlonDTO); 
		if ((null == chdrenq || wsaaEof)){
			return rlpdlonDTO;
		}
		rlpdlonDTO = checkApa2000(rlpdlonDTO);
		return rlpdlonDTO;
	}
	
	protected RlpdlonDTO checkApa2000(RlpdlonDTO rlpdlonDTO){
		
		rlpdlonDTO.setCurrency("");
		rlpdlonDTO.setPrmdepst(BigDecimal.ZERO);
		if("".equals(t5645IO.getSacscodes().get(1)) || "".equals(t5645IO.getSacstypes().get(1)) || 
				"".equals(t5645IO.getSacscodes().get(4)) || "".equals(t5645IO.getSacstypes().get(4))){
			return null;
		}
		rlpdlonDTO = readPrincipalApa(rlpdlonDTO);
		return rlpdlonDTO;
	}
	protected RlpdlonDTO readPrincipalApa(RlpdlonDTO rlpdlonDTO){
		wsaaRldgacct = rlpdlonDTO.getChdrnum() + "00";

		Acblpf acbl = new Acblpf();
		acbl.setRldgacct(wsaaRldgacct);
		acbl.setRldgcoy(rlpdlonDTO.getChdrcoy());
		acbl.setSacscode(t5645IO.getSacscodes().get(1));
		acbl.setSacstyp(t5645IO.getSacstypes().get(1));
		acblpfList = acblpfDAO.getAcblpfList(acbl);
		while (!endOfFile) {
			rlpdlonDTO = processAcbls2100(rlpdlonDTO,acblpfList);
		}
		acblpfList.clear();
		endOfFile = false;
		wsaaRldgacct = rlpdlonDTO.getChdrnum() + "00";
		acbl = new Acblpf();
		acbl.setRldgacct(wsaaRldgacct);
		acbl.setRldgcoy(rlpdlonDTO.getChdrcoy());
		acbl.setSacscode(t5645IO.getSacscodes().get(4));
		acbl.setSacstyp(t5645IO.getSacstypes().get(4));
		acblpfList = acblpfDAO.getAcblpfList(acbl);
		while (!endOfFile) {
			rlpdlonDTO = processAcbls2100(rlpdlonDTO,acblpfList);
		}
		return rlpdlonDTO;
	}
	
	
	protected RlpdlonDTO processAcbls2100(RlpdlonDTO rlpdlonDTO,List<Acblpf> acblpfList){
		iterator = acblpfList.iterator();
		if(iterator.hasNext()){
			acblpf = iterator.next();
			wsaaRldgacct = acblpf.getRldgacct();	
		}else{
			endOfFile = true;
			return rlpdlonDTO;	
		}
		if (acblpf.getSacscurbal() == BigDecimal.ZERO) {
			if(iterator.hasNext()){
				acblpf = iterator.next();
			}else{
				endOfFile = true;
				return rlpdlonDTO;
			}
		}
		if (acblpf.getSacscurbal().compareTo(BigDecimal.ZERO) == -1) {
			acblpf.setSacscurbal(acblpf.getSacscurbal().setScale(2).multiply(new BigDecimal(-1)));
		}
		rlpdlonDTO.setCurrency(acblpf.getOrigcurr());
		rlpdlonDTO.setPrmdepst(acblpf.getSacscurbal().add(rlpdlonDTO.getPrmdepst()));
		if(iterator.hasNext()){
			acblpf = iterator.next();
		}else{
			endOfFile = true;
		}
		return rlpdlonDTO;
	}
	
	protected void initialize1000(RlpdlonDTO rlpdlonDTO) throws IOException {
		rlpdlonDTO.setProcessed("Y");
		rlpdlonDTO.setStatuz("****");
		wsaaEof = false;
		endOfFile = false;
		chdrenq = chdrpfDAO.readRecordOfChdrpf(rlpdlonDTO.getChdrcoy(),rlpdlonDTO.getChdrnum());
		if (null == chdrenq) {
			return;
		}
		
		List<Itempf> th623List = itempfDAO.readSmartTableByTableNameAndItem2(rlpdlonDTO.getChdrcoy(), "TH623", chdrenq.getCnttype(),
				CommonConstants.IT_ITEMPFX);
		if(null==th623List || th623List.size() <= 0){
			wsaaEof = true;
			return ;
		}
		
		th623IO = smartTableDataUtils.convertGenareaToPojo(th623List.get(0).getGenareaj(), Th623.class);

		payrpf = payrpfDAO.getPayrRecord(chdrenq.getChdrcoy(),chdrenq.getChdrnum());
		if(payrpf == null){
			throw new DataNoFoundException("No Data in Payrpf");
		}

		List<Itempf> t5645List = itempfDAO.readSmartTableByTableNameAndItem2(rlpdlonDTO.getChdrcoy(), "T5645", "RLPDLON",
				CommonConstants.IT_ITEMPFX);
		if(null == t5645List || t5645List.size() <= 0){//ILIFE-5763 Starts
			throw new ItemNotfoundException("Item RLPDLON not found in Table T5645");
		}
		t5645IO = smartTableDataUtils.convertGenareaToPojo(t5645List.get(0).getGenareaj(), T5645.class); 
	}

}
