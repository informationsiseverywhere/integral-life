package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HitdpfDAO;
import com.dxc.integral.life.dao.model.Hitdpf;

@Repository("hitdpfDAO")
public class HitdpfDAOImpl extends BaseDAOImpl implements HitdpfDAO {

	@Override
	public List<Hitdpf> getHitdpfRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, int tranno) {
		
		StringBuilder sql = new StringBuilder("SELECT * FROM HITDPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND TRANNO=? ");
		sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, TRANNO ASC, UNIQUE_NUMBER DESC");
		
		return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx, tranno}, new BeanPropertyRowMapper<Hitdpf>(Hitdpf.class));
	}
	@Override
	public Hitdpf getHitdRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, String zintbfnd) {
		StringBuilder sql = new StringBuilder("SELECT * FROM HITDPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND ZINTBFND=? ");
		sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, ZINTBFND ASC, UNIQUE_NUMBER DESC");
		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx, zintbfnd}, 
				new BeanPropertyRowMapper<Hitdpf>(Hitdpf.class));
	}
	@Override
	public int updateHitdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, String zintbfnd) {
		String sql = "UPDATE HITDPF SET VALIDFLAG='1' WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND ZINTBFND=?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx, zintbfnd});
		
	}
	@Override
	public int deleteHitdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, String zintbfnd) {
		String sql = "DELETE FROM HITDPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND ZINTBFND=?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx, zintbfnd});
		
	}

}
