
package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.TrmracdDTO;

/**
 * Termination of Reassurance Cessions Subroutine.
 * 
 * @author fwang3
 *
 */
public interface Trmracd {
	void executeTRMR(TrmracdDTO trmracdDTO) throws IOException;

	void executeCHGR(TrmracdDTO trmracdDTO) throws IOException;
}
