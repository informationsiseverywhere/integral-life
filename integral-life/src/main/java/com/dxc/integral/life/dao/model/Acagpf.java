package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
/**
 * @author wli31
 */
public class Acagpf {

    private String rldgcoy;
    private String sacscode;
    private String rldgpfx;
    private String rldgacct;
    private String origcurr;
    private String sacstyp;
    private BigDecimal sacscurbal;
    private String rdocnum;
    private String glsign;
    private String userProfile;
    private String jobName;
    private String datime;

    public String getRldgcoy() {
        return rldgcoy;
    }

    public String getSacscode() {
        return sacscode;
    }

    public String getRldgpfx() {
        return rldgpfx;
    }

    public String getRldgacct() {
        return rldgacct;
    }

    public String getOrigcurr() {
        return origcurr;
    }

    public String getSacstyp() {
        return sacstyp;
    }

    public BigDecimal getSacscurbal() {
        return sacscurbal;
    }

    public String getRdocnum() {
        return rdocnum;
    }

    public String getGlsign() {
        return glsign;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setRldgcoy(String rldgcoy) {
        this.rldgcoy = rldgcoy;
    }

    public void setSacscode(String sacscode) {
        this.sacscode = sacscode;
    }

    public void setRldgpfx(String rldgpfx) {
        this.rldgpfx = rldgpfx;
    }

    public void setRldgacct(String rldgacct) {
        this.rldgacct = rldgacct;
    }

    public void setOrigcurr(String origcurr) {
        this.origcurr = origcurr;
    }

    public void setSacstyp(String sacstyp) {
        this.sacstyp = sacstyp;
    }

    public void setSacscurbal(BigDecimal sacscurbal) {
        this.sacscurbal = sacscurbal;
    }

    public void setRdocnum(String rdocnum) {
        this.rdocnum = rdocnum;
    }

    public void setGlsign(String glsign) {
        this.glsign = glsign;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }
}