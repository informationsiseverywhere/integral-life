package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Clexpf;
/**
 * The ClextpfDAOImpl for database operations.
 * 
 * @author wli31
 *
 */
public interface ClexpfDAO {
	public Clexpf getClexpfRecord(String clntpfx, String clntcoy, String clntnum);
}
