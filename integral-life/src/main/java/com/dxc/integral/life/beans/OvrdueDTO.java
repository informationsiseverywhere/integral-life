/**
 * 
 */
package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * @author fwang3
 *
 */
public class OvrdueDTO {
	private String function;
  	private String statuz;
	private String language;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String cntcurr;
	private int effdate;
	private BigDecimal outstamt;
	private int ptdate;
	private int btdate;
	private int ovrdueDays;
	private String agntnum;
	private String cownnum;
	private String statcode;
	private String pstatcode;
	private String trancode;
	private int acctyear;
	private int acctmonth;
	private String batcbrn;
	private String batcbatch;
	private int user;
	private String company;
	private int tranDate;
	private int tranTime;
	private String termid;
	private String crtable;
	private String pumeth;
	private String billfreq;
	private BigDecimal instprem;
	private BigDecimal sumins;
	private int crrcd;
	private int premCessDate;
	private BigDecimal pupfee;
	private String cnttype;
	private int occdate;
	private String aloind;
	private String efdcode;
	private int procSeqNo;
	private BigDecimal newSumins;
	private String filler;
	private int polsum;
	private BigDecimal actualVal;
	private String description;
	private int riskCessDate;
	private int newRiskCessDate;
	private int newPremCessDate;
	private int newRerateDate;
	private BigDecimal cvRefund;
	private BigDecimal surrenderValue;
	private int etiYears;
	private int etiDays;
	private int newCrrcd;
	private BigDecimal newSingp;
	private int newAnb;
	private BigDecimal newInstprem;
	private BigDecimal newPua;
	private int newBonusDate;
	private int runDate;

	public OvrdueDTO() {
		this.function = "";
		this.statuz = "";
		this.language = "";
		this.chdrcoy = "";
		this.chdrnum = "";
		this.life = "";
		this.coverage = "";
		this.rider = "";
		this.planSuffix = 0;
		this.tranno = 0;
		this.cntcurr = "";
		this.effdate = 0;
		this.outstamt = BigDecimal.ZERO;
		this.ptdate = 0;
		this.btdate = btdate;
		this.ovrdueDays = 0;
		this.agntnum = "";
		this.cownnum = "";
		this.statcode = "";
		this.pstatcode = "";
		this.trancode = "";
		this.acctyear = 0;
		this.acctmonth = 0;
		this.batcbrn = "";
		this.batcbatch = "";
		this.user = 0;
		this.company = "";
		this.tranDate = 0;
		this.tranTime = 0;
		this.termid = "";
		this.crtable = "";
		this.pumeth = "";
		this.billfreq = "";
		this.instprem = BigDecimal.ZERO;
		this.sumins = BigDecimal.ZERO;
		this.crrcd = 0;
		this.premCessDate = 0;
		this.pupfee = BigDecimal.ZERO;
		this.cnttype = "";
		this.occdate = 0;
		this.aloind = "";
		this.efdcode = "";
		this.procSeqNo = 0;
		this.newSumins = BigDecimal.ZERO;
		this.filler = "";
		this.polsum = 0;
		this.actualVal = BigDecimal.ZERO;
		this.description = "";
		this.riskCessDate = 0;
		this.newRiskCessDate = 0;
		this.newPremCessDate = 0;
		this.newRerateDate = 0;
		this.cvRefund = BigDecimal.ZERO;
		this.surrenderValue = BigDecimal.ZERO;
		this.etiYears = 0;
		this.etiDays = 0;
		this.newCrrcd = 0;
		this.newSingp = BigDecimal.ZERO;
		this.newAnb = 0;
		this.newInstprem = BigDecimal.ZERO;
		this.newPua = BigDecimal.ZERO;
		this.newBonusDate = 0;
		this.runDate = 0;
	}
	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getCntcurr() {
		return cntcurr;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public BigDecimal getOutstamt() {
		return outstamt;
	}

	public void setOutstamt(BigDecimal outstamt) {
		this.outstamt = outstamt;
	}

	public int getPtdate() {
		return ptdate;
	}

	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	public int getBtdate() {
		return btdate;
	}

	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}

	public int getOvrdueDays() {
		return ovrdueDays;
	}

	public void setOvrdueDays(int ovrdueDays) {
		this.ovrdueDays = ovrdueDays;
	}

	public String getAgntnum() {
		return agntnum;
	}

	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}

	public String getCownnum() {
		return cownnum;
	}

	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstatcode() {
		return pstatcode;
	}

	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	public String getTrancode() {
		return trancode;
	}

	public void setTrancode(String trancode) {
		this.trancode = trancode;
	}

	public int getAcctyear() {
		return acctyear;
	}

	public void setAcctyear(int acctyear) {
		this.acctyear = acctyear;
	}

	public int getAcctmonth() {
		return acctmonth;
	}

	public void setAcctmonth(int acctmonth) {
		this.acctmonth = acctmonth;
	}

	public String getBatcbrn() {
		return batcbrn;
	}

	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	public String getBatcbatch() {
		return batcbatch;
	}

	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getTranDate() {
		return tranDate;
	}

	public void setTranDate(int tranDate) {
		this.tranDate = tranDate;
	}

	public int getTranTime() {
		return tranTime;
	}

	public void setTranTime(int tranTime) {
		this.tranTime = tranTime;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getPumeth() {
		return pumeth;
	}

	public void setPumeth(String pumeth) {
		this.pumeth = pumeth;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public BigDecimal getInstprem() {
		return instprem;
	}

	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}

	public BigDecimal getSumins() {
		return sumins;
	}

	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	public int getCrrcd() {
		return crrcd;
	}

	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}

	public int getPremCessDate() {
		return premCessDate;
	}

	public void setPremCessDate(int premCessDate) {
		this.premCessDate = premCessDate;
	}

	public BigDecimal getPupfee() {
		return pupfee;
	}

	public void setPupfee(BigDecimal pupfee) {
		this.pupfee = pupfee;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public int getOccdate() {
		return occdate;
	}

	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}

	public String getAloind() {
		return aloind;
	}

	public void setAloind(String aloind) {
		this.aloind = aloind;
	}

	public String getEfdcode() {
		return efdcode;
	}

	public void setEfdcode(String efdcode) {
		this.efdcode = efdcode;
	}

	public int getProcSeqNo() {
		return procSeqNo;
	}

	public void setProcSeqNo(int procSeqNo) {
		this.procSeqNo = procSeqNo;
	}

	public BigDecimal getNewSumins() {
		return newSumins;
	}

	public void setNewSumins(BigDecimal newSumins) {
		this.newSumins = newSumins;
	}

	public String getFiller() {
		return filler;
	}

	public void setFiller(String filler) {
		this.filler = filler;
	}

	public int getPolsum() {
		return polsum;
	}

	public void setPolsum(int polsum) {
		this.polsum = polsum;
	}

	public BigDecimal getActualVal() {
		return actualVal;
	}

	public void setActualVal(BigDecimal actualVal) {
		this.actualVal = actualVal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRiskCessDate() {
		return riskCessDate;
	}

	public void setRiskCessDate(int riskCessDate) {
		this.riskCessDate = riskCessDate;
	}

	public int getNewRiskCessDate() {
		return newRiskCessDate;
	}

	public void setNewRiskCessDate(int newRiskCessDate) {
		this.newRiskCessDate = newRiskCessDate;
	}

	public int getNewPremCessDate() {
		return newPremCessDate;
	}

	public void setNewPremCessDate(int newPremCessDate) {
		this.newPremCessDate = newPremCessDate;
	}

	public int getNewRerateDate() {
		return newRerateDate;
	}

	public void setNewRerateDate(int newRerateDate) {
		this.newRerateDate = newRerateDate;
	}

	public BigDecimal getCvRefund() {
		return cvRefund;
	}

	public void setCvRefund(BigDecimal cvRefund) {
		this.cvRefund = cvRefund;
	}

	public BigDecimal getSurrenderValue() {
		return surrenderValue;
	}

	public void setSurrenderValue(BigDecimal surrenderValue) {
		this.surrenderValue = surrenderValue;
	}

	public int getEtiYears() {
		return etiYears;
	}

	public void setEtiYears(int etiYears) {
		this.etiYears = etiYears;
	}

	public int getEtiDays() {
		return etiDays;
	}

	public void setEtiDays(int etiDays) {
		this.etiDays = etiDays;
	}

	public int getNewCrrcd() {
		return newCrrcd;
	}

	public void setNewCrrcd(int newCrrcd) {
		this.newCrrcd = newCrrcd;
	}

	public BigDecimal getNewSingp() {
		return newSingp;
	}

	public void setNewSingp(BigDecimal newSingp) {
		this.newSingp = newSingp;
	}

	public int getNewAnb() {
		return newAnb;
	}

	public void setNewAnb(int newAnb) {
		this.newAnb = newAnb;
	}

	public BigDecimal getNewInstprem() {
		return newInstprem;
	}

	public void setNewInstprem(BigDecimal newInstprem) {
		this.newInstprem = newInstprem;
	}

	public BigDecimal getNewPua() {
		return newPua;
	}

	public void setNewPua(BigDecimal newPua) {
		this.newPua = newPua;
	}

	public int getNewBonusDate() {
		return newBonusDate;
	}

	public void setNewBonusDate(int newBonusDate) {
		this.newBonusDate = newBonusDate;
	}

	public int getRunDate() {
		return runDate;
	}

	public void setRunDate(int runDate) {
		this.runDate = runDate;
	}
}
