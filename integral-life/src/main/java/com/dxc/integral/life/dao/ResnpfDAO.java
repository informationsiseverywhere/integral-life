package com.dxc.integral.life.dao;

public interface ResnpfDAO {
	public String getReasoncd(String chdrcoy, String chdrnum, String trancde);
}
