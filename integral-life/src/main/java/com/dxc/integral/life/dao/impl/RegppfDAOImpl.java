package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RegppfDAO;
import com.dxc.integral.life.dao.model.Regppf;
@Repository("regppfDAO")
@Lazy
public class RegppfDAOImpl extends BaseDAOImpl implements RegppfDAO {

	@Override
	public Regppf readRecord(String chdrcoy, String chdrnum, String life,String coverage, String rider, int rgpynum) {
		String sql = "select * from regppf  where chdrcoy = ? and chdrnum = ? and life=? and coverage=? and rider=? and rgpynum = ? and validflag = '1' order by rgpynum";
		Regppf regppf = null;
        try {
        	regppf = jdbcTemplate.queryForObject(sql, new Object[] { chdrcoy, chdrnum,life,coverage ,rider,rgpynum},
					new BeanPropertyRowMapper<Regppf>(Regppf.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return regppf;
	}

	@Override
	public List<Regppf> readRegpByRgpytype(String chdrnum, String crtable) {
		String sql = "select * from regppf  where chdrnum = ? and crtable = ? ";
		return jdbcTemplate.query(sql, new Object[] { chdrnum, crtable},new BeanPropertyRowMapper<Regppf>(Regppf.class));
	}
}
