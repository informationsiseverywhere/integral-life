package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.ComlinkDTO;

public interface Cmpy2rv {
	public ComlinkDTO process(ComlinkDTO comlinkDTO) throws Exception;
}
