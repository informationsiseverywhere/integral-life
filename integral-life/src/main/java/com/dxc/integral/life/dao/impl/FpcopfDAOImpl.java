package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.FpcopfDAO;
import com.dxc.integral.life.dao.model.Fpcopf;
import com.dxc.integral.life.dao.model.Linspf;

/**
 * 
 * FpcopfDAO implementation for database table <b>FPCOPF</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("fpcopfDAO")
@Lazy
public class FpcopfDAOImpl extends BaseDAOImpl implements FpcopfDAO {

	@Override
	public List<Fpcopf> readFpcopfRecords(String company, String contractNumber){
		StringBuilder sql = new StringBuilder("SELECT * FROM FPCOPF WHERE VALIDFLAG = '1' AND ACTIND = 'Y' AND CHDRCOY=? AND CHDRNUM=? ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TARGFROM, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Fpcopf>(Fpcopf.class));
	}
	@Override
	public List<Fpcopf> readFpcorevRecords(String company, String contractNumber, String life, String coverage, String rider, int plnsfx){
		StringBuilder sql = new StringBuilder("SELECT * FROM FPCOPF WHERE VALIDFLAG = '1' AND CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, PAYRSEQNO, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber, life, coverage, rider, plnsfx },
				new BeanPropertyRowMapper<Fpcopf>(Fpcopf.class));
	}
	@Override
	public void updateFpcopfRecords(List<Fpcopf> fpcopfList){
		List<Fpcopf> tempFpcopfList = fpcopfList;   
		StringBuilder sb = new StringBuilder(" update fpcopf set billedp= ?, ovrminreq=?, jobnm=?,usrprf=?,datime=? ");
		sb.append("where unique_number=?");    
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempFpcopfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setBigDecimal(1, fpcopfList.get(i).getBilledp());
				ps.setBigDecimal(2, fpcopfList.get(i).getOvrminreq());
				ps.setString(3,fpcopfList.get(i).getUsrprf());
                ps.setString(4, fpcopfList.get(i).getJobnm());
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(6, fpcopfList.get(i).getUniqueNumber());
            }  
      }); 
	}
	@Override
	public int updateFpcopfRecords(Fpcopf fpcopf){  
		StringBuilder sql= new StringBuilder(" UPDATE FPCOPF SET BILLEDP= ?, OVRMINREQ=?, ACTIND=?, DATIME=? where CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? ");
		sql.append("AND RIDER=? AND PLNSFX=? AND TARGFROM=? AND TRANNO=?");    
	    return jdbcTemplate.update(sql.toString(),new Object[] {fpcopf.getBilledp(), fpcopf.getOvrminreq(), fpcopf.getActind(), System.currentTimeMillis(), fpcopf.getChdrcoy(), 
	    		fpcopf.getChdrnum(), fpcopf.getLife(), fpcopf.getCoverage(), fpcopf.getRider(), fpcopf.getPlnsfx(), fpcopf.getTargfrom(), fpcopf.getTranno()});
	    
	  
	}
}
