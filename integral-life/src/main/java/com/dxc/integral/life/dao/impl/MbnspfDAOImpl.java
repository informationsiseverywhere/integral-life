package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.MbnspfDAO;
import com.dxc.integral.life.dao.model.Mbnspf;

/**
 * 
 * MbnspfDAO implementation for database table <b>Mbnspf</b> DB operations.
 * 
 *
 */
@Repository("mbnspfDAO")
public class MbnspfDAOImpl extends BaseDAOImpl implements MbnspfDAO {

	@Override
	public List<Mbnspf> getMbnspfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int yrsinf) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM MBNSPF WHERE ");
		sql.append(" CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND YRSINF = ? ");
		sql.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, YRSINF ASC, UNIQUE_NUMBER DESC");

		return jdbcTemplate.query(sql.toString(), new Object[] { chdrcoy, chdrnum, life, coverage, rider, yrsinf },
				new BeanPropertyRowMapper<Mbnspf>(Mbnspf.class));
	}
}
