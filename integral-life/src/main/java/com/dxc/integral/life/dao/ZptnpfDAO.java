package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Zptnpf;

public interface ZptnpfDAO {
	public void insertZptnpfRecords(List<Zptnpf> zptnpfList);
}
