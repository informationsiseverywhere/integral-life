package com.dxc.integral.life.smarttable.pojo;


public class T3695 {

  	private String comind;
  	private String intextind;
  	private String rollmnth;
  	private String sign;
  	private String tlcomflg;
  	private String tlprmflg;
  	private String zrorcomm;
	public String getComind() {
		return comind;
	}
	public void setComind(String comind) {
		this.comind = comind;
	}
	public String getIntextind() {
		return intextind;
	}
	public void setIntextind(String intextind) {
		this.intextind = intextind;
	}
	public String getRollmnth() {
		return rollmnth;
	}
	public void setRollmnth(String rollmnth) {
		this.rollmnth = rollmnth;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getTlcomflg() {
		return tlcomflg;
	}
	public void setTlcomflg(String tlcomflg) {
		this.tlcomflg = tlcomflg;
	}
	public String getTlprmflg() {
		return tlprmflg;
	}
	public void setTlprmflg(String tlprmflg) {
		this.tlprmflg = tlprmflg;
	}
	public String getZrorcomm() {
		return zrorcomm;
	}
	public void setZrorcomm(String zrorcomm) {
		this.zrorcomm = zrorcomm;
	}
}
