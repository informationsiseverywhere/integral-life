package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Agorpf;

public interface AgorpfDAO {

	public Agorpf getAgorpfByCoyAndNumAndtag(String angtcoy, String agntnum, String reportag);
	public List<Agorpf> getAgorpfByCoyAndNum(String angtcoy, String agntnum);
}
