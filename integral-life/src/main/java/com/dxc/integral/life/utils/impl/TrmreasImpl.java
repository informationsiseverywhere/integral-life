package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.T1693;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.life.beans.TrmreasDTO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.RacdpfDAO;
import com.dxc.integral.life.dao.RasapfDAO;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.dao.model.Racdpf;
import com.dxc.integral.life.dao.model.Rasapf;
import com.dxc.integral.life.smarttable.pojo.T5448;
import com.dxc.integral.life.smarttable.pojo.T5472;
import com.dxc.integral.life.smarttable.pojo.T6598;
import com.dxc.integral.life.smarttable.pojo.Th618;
import com.dxc.integral.life.utils.Trmreas;

@Service("trmreas")
@Lazy
public class TrmreasImpl implements Trmreas{

	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	@Autowired
	private RacdpfDAO racdpfDAO;
	@Autowired
	private RasapfDAO rasapfDAO;
	@Autowired
	private Xcvrt xcvrt;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private ItempfService itempfService;
	
	
	private String wsaaL1Clntnum;
	private String wsaaL2Clntnum;
	private T1693 t1693IO;
	private T5448 t5448IO;
	private T5472 t5472IO;
	private Th618 th618IO;
	private T6598 t6598IO = new T6598();
	private String wsaaNoReassurance;
	private String wsaaFirstRead;
	private String wsxxLrkcls;
	private BigDecimal wsaaReduced = BigDecimal.ZERO;
	
	
	public TrmreasDTO processTrmreas(TrmreasDTO trmreasDTO) {
		trmreasDTO.setStatuz("****");
		if(!"TRMR".equals(trmreasDTO.getFunction()) && !"CHGR".equals(trmreasDTO.getFunction())) {
			trmreasDTO.setStatuz("BOMB");
			return trmreasDTO;
		}
		Itempf item = itempfDAO.readSmartTableByTrimItem(trmreasDTO.getChdrcoy(), "T5447",trmreasDTO.getCnttype(), CommonConstants.IT_ITEMPFX);
		if(item == null) {
			return trmreasDTO;
		}
		
		wsaaNoReassurance = "N";
		if(!tableLifeReads(trmreasDTO)) {
			trmreasDTO.setStatuz("BOMB");
			return trmreasDTO;
		}
		if("Y".equals(wsaaNoReassurance)) {
			return trmreasDTO;
		}
		
		wsaaReduced = trmreasDTO.getOldSumins().subtract(trmreasDTO.getNewSumins());

		wsaaFirstRead="Y";
		wsaaNoReassurance="N";
		
		if(!readRacds(trmreasDTO)){
			trmreasDTO.setStatuz("BOMB");
			return trmreasDTO;
		}
	//Subroutines	
//		rexpupdrec.reassurer.set(SPACES);
		for (int i =0 ; th618IO.getLrkclss().get(i) != null && !th618IO.getLrkclss().get(i).trim().equals("") && i<5; i++){
			wsxxLrkcls=th618IO.getLrkclss().get(i);
			//experienceUpdate500();
		}
		
		return trmreasDTO;
	}
	private boolean tableLifeReads(TrmreasDTO trmreasDTO) {
		if(!tableReads(trmreasDTO)) {
			return false;
		}
		if(!lifeRead(trmreasDTO)) {
			return false;
		}
		
		return true;
	}
	private boolean tableReads(TrmreasDTO trmreasDTO) {
		TableItem<T1693> t1693 =itempfService.readSmartTableByTableNameAndItem("0", "T1693", trmreasDTO.getChdrcoy(), CommonConstants.IT_ITEMPFX, T1693.class);
		if(t1693 == null) {
			return false;
		}
		
		t1693IO = t1693.getItemDetail();
		
		TableItem<T5448> t5448 =itempfService.readSmartTableByTableNameAndItem(trmreasDTO.getChdrcoy(), "T5448", trmreasDTO.getCnttype().concat(trmreasDTO.getCrtable()), 
				CommonConstants.IT_ITEMPFX, T5448.class);
		if(t5448 == null) {
			wsaaNoReassurance = "Y";
			return true;
		}
		t5448IO = t5448.getItemDetail();
		if(t5448IO.getRclmbas() == null || "".equals(t5448IO.getRclmbas().trim())) {
			return false;
		}
		
		TableItem<T5472> t5472 =itempfService.readSmartTableByTableNameAndItem(trmreasDTO.getChdrcoy(), "T5472", t5448IO.getRclmbas().concat(trmreasDTO.getBatckey().substring(10, 14)), 
				CommonConstants.IT_ITEMPFX, T5472.class);
		if(t5472 == null) {
			return false;
		}
		
		t5472IO = t5472.getItemDetail();
		
		if(t5448IO.getRresmeth() == null || "".equals(t5448IO.getRresmeth().trim())) {
			t6598IO.setCalcprog("");
			return true;
		}
		
		TableItem<T6598> t6598 =itempfService.readSmartTableByTableNameAndItem(trmreasDTO.getChdrcoy(), "T6598", t5448IO.getRresmeth(),	CommonConstants.IT_ITEMPFX, T6598.class);
	    if(t6598 == null) {
	    	return false;
	    }
	    
	    t6598IO = t6598.getItemDetail();
	    
	    return true;
	}
	
	private boolean lifeRead(TrmreasDTO trmreasDTO){
		List<Lifepf> list = lifepfDAO.getLifeRecord(trmreasDTO.getChdrcoy(), trmreasDTO.getChdrnum(), trmreasDTO.getLife(), "00");
		if(list == null || list.size() ==0) {
			return false;
		}

		Lifepf lifepf = list.get(0);
		wsaaL1Clntnum = lifepf.getLifcnum();

		
		list = lifepfDAO.getLifeRecord(trmreasDTO.getChdrcoy(), trmreasDTO.getChdrnum(), trmreasDTO.getLife(), "01");
		if(list == null || list.size() ==0) {
			wsaaL2Clntnum = "";
		}else {
			wsaaL2Clntnum = list.get(0).getLifcnum();
		}
		
		/* Read Risk Type Table, TH618, for multiple risk classes.      */
		
		TableItem<Th618> th618 =itempfService.readSmartTableByTableNameAndItem(trmreasDTO.getChdrcoy(), "TH618", t5448IO.getRrsktyp(),	CommonConstants.IT_ITEMPFX, Th618.class);
		 if(th618 != null) {
			 th618IO = th618.getItemDetail();
		 }
		 return true;
	}
   private boolean readRacds(TrmreasDTO trmreasDTO) {
	   List<Racdpf> list = racdpfDAO.searchRacdmjaRecord(trmreasDTO.getChdrcoy(), trmreasDTO.getChdrnum(), trmreasDTO.getLife(), trmreasDTO.getCoverage(), 
			   trmreasDTO.getRider(), trmreasDTO.getPlanSuffix(), 99);
	 
	   if(list == null || list.size() == 0) {
		   if("Y".equals(wsaaFirstRead)) {
			   wsaaNoReassurance = "Y";			  
		   }
		   return true;
	   }
	   
	  
	   Rasapf rasapf;
	   for(Racdpf racdpf : list) {
		   
		 if ("Y".equals(wsaaFirstRead)) {
				wsaaFirstRead = "N";
		 }
	     rasapf = rasapfDAO.getRasaRecord(racdpf.getChdrcoy(), racdpf.getRasnum());
	     if(rasapf == null) {
	    	 return false;
	     }
	    // callSubroutines();/////////////////////////////////////Subroutine

	     wsaaReduced = trmreasDTO.getOldSumins().subtract(trmreasDTO.getNewSumins());
	     wsxxLrkcls = racdpf.getLrkcls();
	  //  experienceUpdate();//////////////////Subroutine
	   }
	 return true; 
   }
	
   private BigDecimal callXcvrt(String rexpupdrecCurrency, String trmreasDTOCompany, String trmreasrecbatckey) throws Exception
	{
	   
		ConlinkInputDTO conlinkInputDTO = new ConlinkInputDTO();
		//conlinkInputDTO.setCashdate(trmreasrec.effdate);
		//conlinkInputDTO.setCompany(trmreasrec.chdrcoy);
		ConlinkOutputDTO conlinkOutputDTO = xcvrt.executeRealFunction(conlinkInputDTO);
	
		ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();
		zrdecplcDTO.setAmountIn(conlinkOutputDTO.getCalculatedAmount());
		zrdecplcDTO.setCurrency(rexpupdrecCurrency);
		zrdecplcDTO.setCompany(trmreasDTOCompany);
		zrdecplcDTO.setBatctrcde(trmreasrecbatckey);
	    return zrdecplc.convertAmount(zrdecplcDTO);
	   
		
	}	

}
