package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Maprpf;

/**
 * MaprpfDAO for database table <b>MAPRPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
public interface MaprpfDAO {

	/**
	 * Reads MAPRPF based on company, agent number and effective date.
	 * 
	 * @param company
	 *            - company
	 * @param agentNumber
	 *            - agent number
	 * @param effectiveDate
	 *            - effective date
	 * @return - Maprpf record
	 */
	Maprpf readMaprpfByAgentNumberCompanyAndEffDate(String company, String agentNumber, int effectiveDate);

	/**
	 * Updates MAPRPF based on company, agent number and effective date.
	 * 
	 * @param company
	 *            - company
	 * @param agentNumber
	 *            - agent number
	 * @param effectiveDate
	 *            - effective date
	 * @param maprpf
	 *            - Maprpf model
	 * @return - number of rows updated
	 */
	int updateMaprpfByAgentNumberCompanyAndEffDate(String company, String agentNumber, int effectiveDate,
			Maprpf maprpf);

	/**
	 * Inserts record into MAPRPF database table.
	 * 
	 * @param maprpf
	 *            - Maprpf model
	 * @return - number of rows inserted
	 */
	int insertMaprpfRecords(Maprpf maprpf);
}
