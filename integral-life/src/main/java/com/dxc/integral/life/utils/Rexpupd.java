/**
 * 
 */
package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.RexpupdDTO;


/**
 * Life Experience Update.
 * 
 * The life reassurance experience data is stored on two files, LIRR and LRRH.
 * This subroutine is called when cessions are activated (ACTVRES) or terminated
 * (TRMRACD) to update these files with the existing in-force cession
 * information. It is used even if there are no cessions for a component (i.e.
 * the entire sum assured is retained) and will update both the life and the
 * joint life for a joint life risk.
 *
 * There are tow valid functions, increase and decrease, used to increase and
 * decrease the value of reassurance respectively.
 *
 * @author fwang3
 *
 */
public interface Rexpupd {
	/**
	 * Perform increase function.
	 * 
	 * @param rexpupdDTO
	 * @throws IOException
	 */
	void incrFunction(RexpupdDTO rexpupdDTO) throws IOException;

	/**
	 * Perform decrease function.
	 * 
	 * @param rexpupdDTO
	 * @throws IOException
	 */
	void decrFunction(RexpupdDTO rexpupdDTO) throws IOException;
}
