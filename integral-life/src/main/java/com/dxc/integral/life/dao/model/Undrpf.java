package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;
/**
 * @author wli31
 */
public class Undrpf {
	private long unique_number;
	private String clntnum;
	private String coy;
	private String lrkcls01;
	private String lrkcls02;
	private String chdrnum;
	private String  life;
	private String cnttyp;
	private String crtable;
	private String currcode;
	private BigDecimal sumins;
	private int effdate;
	private String adsc;
	private String usrprf;
	private String jobnm;
	private String datime;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getCoy() {
		return coy;
	}
	public void setCoy(String coy) {
		this.coy = coy;
	}
	public String getLrkcls01() {
		return lrkcls01;
	}
	public void setLrkcls01(String lrkcls01) {
		this.lrkcls01 = lrkcls01;
	}
	public String getLrkcls02() {
		return lrkcls02;
	}
	public void setLrkcls02(String lrkcls02) {
		this.lrkcls02 = lrkcls02;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCnttyp() {
		return cnttyp;
	}
	public void setCnttyp(String cnttyp) {
		this.cnttyp = cnttyp;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getAdsc() {
		return adsc;
	}
	public void setAdsc(String adsc) {
		this.adsc = adsc;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}


}
