package com.dxc.integral.life.smarttable.pojo;

import java.util.List;
/**
 * @author wli31
 */
public class Th618 {
  	private List<String> lrkclss;
  	private String filler1;
	public List<String> getLrkclss() {
		return lrkclss;
	}
	public void setLrkclss(List<String> lrkclss) {
		this.lrkclss = lrkclss;
	}
	public String getFiller1() {
		return filler1;
	}
	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}
  	
}
