package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Ainrpf {
	    private long unique_Number;
		private String chdrcoy;
		private String chdrnum;
		private String life;
		private String coverage;
		private String rider;
		private String crtable;
		private BigDecimal oldsum;
		private BigDecimal newsumi;
		private BigDecimal oldinst;
		private BigDecimal newinst;
		private int plnsfx;
		private int cmdate;
		private String rasnum;
		private String rngmnt;
		private BigDecimal raamount;
		private int rcesdte;
		private String currency;
		private String aintype;
		private String userProfile;
		private String jobNm;
		private String datime;
		public long getUnique_Number() {
			return unique_Number;
		}
		public void setUnique_Number(long unique_Number) {
			this.unique_Number = unique_Number;
		}
		public String getChdrcoy() {
			return chdrcoy;
		}
		public void setChdrcoy(String chdrcoy) {
			this.chdrcoy = chdrcoy;
		}
		public String getChdrnum() {
			return chdrnum;
		}
		public void setChdrnum(String chdrnum) {
			this.chdrnum = chdrnum;
		}
		public String getLife() {
			return life;
		}
		public void setLife(String life) {
			this.life = life;
		}
		public String getCoverage() {
			return coverage;
		}
		public void setCoverage(String coverage) {
			this.coverage = coverage;
		}
		public String getRider() {
			return rider;
		}
		public void setRider(String rider) {
			this.rider = rider;
		}
		public String getCrtable() {
			return crtable;
		}
		public void setCrtable(String crtable) {
			this.crtable = crtable;
		}
		public BigDecimal getOldsum() {
			return oldsum;
		}
		public void setOldsum(BigDecimal oldsum) {
			this.oldsum = oldsum;
		}
		public BigDecimal getNewsumi() {
			return newsumi;
		}
		public void setNewsumi(BigDecimal newsumi) {
			this.newsumi = newsumi;
		}
		public BigDecimal getOldinst() {
			return oldinst;
		}
		public void setOldinst(BigDecimal oldinst) {
			this.oldinst = oldinst;
		}
		public BigDecimal getNewinst() {
			return newinst;
		}
		public void setNewinst(BigDecimal newinst) {
			this.newinst = newinst;
		}
		public int getPlnsfx() {
			return plnsfx;
		}
		public void setPlnsfx(int plnsfx) {
			this.plnsfx = plnsfx;
		}
		public int getCmdate() {
			return cmdate;
		}
		public void setCmdate(int cmdate) {
			this.cmdate = cmdate;
		}
		public String getRasnum() {
			return rasnum;
		}
		public void setRasnum(String rasnum) {
			this.rasnum = rasnum;
		}
		public String getRngmnt() {
			return rngmnt;
		}
		public void setRngmnt(String rngmnt) {
			this.rngmnt = rngmnt;
		}
		public BigDecimal getRaamount() {
			return raamount;
		}
		public void setRaamount(BigDecimal raamount) {
			this.raamount = raamount;
		}
		public int getRcesdte() {
			return rcesdte;
		}
		public void setRcesdte(int rcesdte) {
			this.rcesdte = rcesdte;
		}
		public String getCurrency() {
			return currency;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public String getAintype() {
			return aintype;
		}
		public void setAintype(String aintype) {
			this.aintype = aintype;
		}
		public String getUserProfile() {
			return userProfile;
		}
		public void setUserProfile(String userProfile) {
			this.userProfile = userProfile;
		}
		public String getJobNm() {
			return jobNm;
		}
		public void setJobNm(String jobNm) {
			this.jobNm = jobNm;
		}
		public String getDatime() {
			return datime;
		}
		public void setDatime(String datime) {
			this.datime = datime;
		}
}

