package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Hdispf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validflag;
	private Integer tranno;
	private String cntcurr;
	private Integer hdv1stdt;
	private Integer hdvldt;
	private Integer hintldt;
	private Integer hintndt;
	private Integer hcapldt;
	private Integer hcapndt;
	private BigDecimal hdvball;
	private BigDecimal hdvbalc;
	private BigDecimal hintos;
	private Integer hdvsmtdt;
	private Integer hdvsmtno;
	private BigDecimal hdvbalst;
	private String sacscode;
	private String sacstyp;
	private String hintduem;
	private String hintdued;
	private String hcapduem;
	private String hcapdued;
	private String usrprf;
	private String jobnm;
	private Date datime;

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public String getChdrcoy() {
		return this.chdrcoy;
	}

	public String getChdrnum() {
		return this.chdrnum;
	}

	public String getLife() {
		return this.life;
	}

	public String getJlife() {
		return this.jlife;
	}

	public String getCoverage() {
		return this.coverage;
	}

	public String getRider() {
		return this.rider;
	}

	public Integer getPlnsfx() {
		return this.plnsfx;
	}

	public String getValidflag() {
		return this.validflag;
	}

	public Integer getTranno() {
		return this.tranno;
	}

	public String getCntcurr() {
		return this.cntcurr;
	}

	public Integer getHdv1stdt() {
		return this.hdv1stdt;
	}

	public Integer getHdvldt() {
		return this.hdvldt;
	}

	public Integer getHintldt() {
		return this.hintldt;
	}

	public Integer getHintndt() {
		return this.hintndt;
	}

	public Integer getHcapldt() {
		return this.hcapldt;
	}

	public Integer getHcapndt() {
		return this.hcapndt;
	}

	public BigDecimal getHdvball() {
		return this.hdvball;
	}

	public BigDecimal getHdvbalc() {
		return this.hdvbalc;
	}

	public BigDecimal getHintos() {
		return this.hintos;
	}

	public Integer getHdvsmtdt() {
		return this.hdvsmtdt;
	}

	public Integer getHdvsmtno() {
		return this.hdvsmtno;
	}

	public BigDecimal getHdvbalst() {
		return this.hdvbalst;
	}

	public String getSacscode() {
		return this.sacscode;
	}

	public String getSacstyp() {
		return this.sacstyp;
	}

	public String getHintduem() {
		return this.hintduem;
	}

	public String getHintdued() {
		return this.hintdued;
	}

	public String getHcapduem() {
		return this.hcapduem;
	}

	public String getHcapdued() {
		return this.hcapdued;
	}

	public String getUsrprf() {
		return this.usrprf;
	}

	public String getJobnm() {
		return this.jobnm;
	}

	public Date getDatime() {
		return new Date(this.datime.getTime());
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	public void setHdv1stdt(Integer hdv1stdt) {
		this.hdv1stdt = hdv1stdt;
	}

	public void setHdvldt(Integer hdvldt) {
		this.hdvldt = hdvldt;
	}

	public void setHintldt(Integer hintldt) {
		this.hintldt = hintldt;
	}

	public void setHintndt(Integer hintndt) {
		this.hintndt = hintndt;
	}

	public void setHcapldt(Integer hcapldt) {
		this.hcapldt = hcapldt;
	}

	public void setHcapndt(Integer hcapndt) {
		this.hcapndt = hcapndt;
	}

	public void setHdvball(BigDecimal hdvball) {
		this.hdvball = hdvball;
	}

	public void setHdvbalc(BigDecimal hdvbalc) {
		this.hdvbalc = hdvbalc;
	}

	public void setHintos(BigDecimal hintos) {
		this.hintos = hintos;
	}

	public void setHdvsmtdt(Integer hdvsmtdt) {
		this.hdvsmtdt = hdvsmtdt;
	}

	public void setHdvsmtno(Integer hdvsmtno) {
		this.hdvsmtno = hdvsmtno;
	}

	public void setHdvbalst(BigDecimal hdvbalst) {
		this.hdvbalst = hdvbalst;
	}

	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}

	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}

	public void setHintduem(String hintduem) {
		this.hintduem = hintduem;
	}

	public void setHintdued(String hintdued) {
		this.hintdued = hintdued;
	}

	public void setHcapduem(String hcapduem) {
		this.hcapduem = hcapduem;
	}

	public void setHcapdued(String hcapdued) {
		this.hcapdued = hcapdued;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());
	}
}
