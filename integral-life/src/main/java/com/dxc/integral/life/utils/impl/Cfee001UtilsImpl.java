/* ********************  */
/*Author  :tsaxena3				  		*/
/*Purpose :Instead of subroutine Cfee001*/
/*Date    :2019.04.09				*/

package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.life.beans.Cfee001;
import com.dxc.integral.life.smarttable.pojo.T5567;
import com.dxc.integral.life.utils.Cfee001Utils;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("cfeeUtils")
@Lazy
public class Cfee001UtilsImpl implements Cfee001Utils {
	
		/* TABLES */
	private String t5567 = "T5567";	
	private T5567 t5567IO;

	@Autowired
	private ItempfDAO itempfDAO;
	
	public void calCfee001(Cfee001 cfee001) throws IOException, ParseException {
		
		cfee001.setStatuz("****");
		int wsaaSub = 0;
		String wsaaItemitem = cfee001.getCnttype().concat(cfee001.getCntcurr());
		
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(cfee001.getCompany());
		itempf.setItemitem(wsaaItemitem);
		itempf.setItemtabl(t5567);
		itempf.setItmfrm(cfee001.getEffdate());
		itempf.setItmto(cfee001.getEffdate());
		
		List<Itempf> itempfList = itempfDAO.readSmartTableListByDates(itempf.getItemcoy(), itempf.getItemtabl(), itempf.getItemitem(), 
															itempf.getItempfx(),itempf.getItmfrm(), itempf.getItmto());
		if(!(itempfList.isEmpty())) {
			for (Itempf it : itempfList) {	
				if((!(it.getItemitem().trim().equals(wsaaItemitem))))
				{
					cfee001.setStatuz("H966");
					return;
				}
				t5567IO = new ObjectMapper().readValue(it.getGenareaj(), T5567.class);
				
			}
		} 
		if (!(cfee001.getStatuz().equals("****"))) {
			return;
		}
		if (t5567IO.getBillfreqs().get(wsaaSub).equals(cfee001.getBillfreq())) {
			cfee001.setMgfee(t5567IO.getCntfees().get(wsaaSub));
		}
		else {
			cfee001.setMgfee(0);
		}
	}
}