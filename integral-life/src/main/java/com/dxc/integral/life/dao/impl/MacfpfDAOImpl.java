package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.MacfpfDAO;
import com.dxc.integral.life.dao.model.Macfpf;

/**
 * MacfpfDAO for database table <b>MACFPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@Repository("macfpfDAO")
public class MacfpfDAOImpl extends BaseDAOImpl implements MacfpfDAO {

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.MacfpfDAO#readMacfpfByAgentNumberAndAgentCompany(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Macfpf> readMacfpfByAgentNumberAndAgentCompany(String company, String agentNumber) {
		StringBuilder sql = new StringBuilder("select * from MACFPF where AGNTCOY=? and AGNTNUM=? ");
		sql.append("order by AGNTCOY asc, AGNTNUM asc, EFFDATE desc, UNIQUE_NUMBER desc");
		return jdbcTemplate.query(sql.toString(), new Object[] { company, agentNumber },
				new BeanPropertyRowMapper<Macfpf>(Macfpf.class));
	}

}
