package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.DglxpfDAO;
import com.dxc.integral.fsu.dao.model.Dglxpf;
import com.dxc.integral.fsu.dao.model.Rtrnpf;
import com.dxc.integral.fsu.smarttable.pojo.Tr362;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.life.beans.BatcupDTO;
import com.dxc.integral.life.beans.LifrtrnDTO;
import com.dxc.integral.life.services.SubLedgerPostingService;
import com.dxc.integral.life.utils.BatLifecup;
import com.dxc.integral.life.utils.Lifrtrn;

/**
* <pre>
*REMARKS.
*         Life system version of Subsidary Ledger Posting
*
* This  subroutine  updates  the  batch totals then writes an
* RTRN record. Depending on the calling function sub-accounts
* balance can also be posted to.
*
*  Functions:
*
*     PSTW  - Write the RTRN and update the SACS balance.
*     NPSTW - Write the RTRN record only.
*
*  Both functions update the running batch header totals kept
*  by calling BATCUP with a KEEPS function.
*
*  On completion of the transaction, BATCUP must be called with
*  a WRITS function to apply the accumulated totals to the
*  database.
*
*  Statuses:
*
*     **** - successful completion of routine
*     BOMB - database error
*     E049 - Invalid linkage into subroutine
*
*  Linkage Area:
*
*  LIFR-BATCKEY   The key of the opened batch.
*  LIFR-RDOCNUM   Document number for this record. Varies
*                 with the calling subsystem.
*  LIFA-TRANNO    This is the sequence number of the transaction
*                 being done to the document
*  LIFA-JRNSEQ    This is the sequence number user to uniquely
*                 identify a specific manual journal posting.
*  LIFR-RLDGCOY   Ledger kay for this record. Varies with
*  LIFR_RLDGACCT  the calling subsystem.
*  LIFR-GENLCOY   General ledger key. Set up from T5645 prior
*  LIFR_GLCODE    to calling this routine.
*  LIFR-GLSIGN
*  LIFR-CONTOT
*  LIFR-SACSCODE
*  LIFR-SACSTYP
*  LIFR-TRANREF   Internal reference number (e.g. secondary
*                 key). Varies with the calling subsystem.
*  LIFR-TRANDESC  Meaningful description of tranaction.
*  LIFR-ORIGCURR  Original (foreign) currency.
*  LIFR-ACCTCCY   Accounting (local) currency.
*  LIFR-CRATE     Exchange rate at time of transaction between
*                 original and accounting currency.
*  LIFR-EFFDATE   The effective date of the transaction.
*  LIFR-ORIGAMT   The transaction amount in original currency.
*  LIFR-ACCTAMT   The transaction amount in accounting currency.
*  LIFR-SUBSTITUTE-CODES
*                 Used to substitute in GL code if passed
*                     1 - @@@ - Contract type
*                     2 - ***
*                     3 - %%%
*                     4 - !!!
*                     5 - +++
*  "LIFR-TRANID"  Date/time stamp
*
/
***********************************************************************
*                                                                     *
* </pre>
*/
@Service("lifrtrn")
@Lazy
public class LifrtrnImpl extends BaseDAOImpl implements Lifrtrn {
	@Autowired
	SubLedgerPostingService subLedgerPostingService;
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private DglxpfDAO dglxpfDAO;
	@Autowired
	private BatLifecup batlifecup;
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;


	private static final Logger LOGGER = LoggerFactory.getLogger(LifrtrnImpl.class); // IJTI-1724
	
	
	public void calcLitrtrn(LifrtrnDTO lifrtrnPojo) throws IOException{
		LOGGER.info("Lifrtrn processing start...... !");
		lifrtrnPojo.setStatuz("****");
		//syserrrec.subrname.set(wsaaProg);
		if (!"PSTW".equals(lifrtrnPojo.getFunction())&& !"NPSTW".equals(lifrtrnPojo.getFunction())) {
			lifrtrnPojo.setStatuz("E409");
			return ;
		}
		
		String appflag = checkSchedule24x7Setup(lifrtrnPojo.getBatccoy());
	
		Rtrnpf rtrnpf = subLedgerPostingService.postsubLedgRtrn(lifrtrnPojo);
		if ( "Y".equals(appflag.trim())) {
			diaryUpdate5000(rtrnpf);
		}
		if ("PSTW".equals(lifrtrnPojo.getFunction()) && !"".equals(lifrtrnPojo.getSacscode())) {
			subLedgerPostingService.postCurBalAmount(lifrtrnPojo);
		}
		int accamt=0;
		if(rtrnpf !=null && rtrnpf.getAcctamt() != null) {
			accamt = rtrnpf.getAcctamt().intValue();
		}
		callBatcup(lifrtrnPojo, accamt);
		LOGGER.info("Lifrtrn processing completely...... !");
	}

	
	
	protected void diaryUpdate5000(Rtrnpf rtrnpf)
	{
		if (rtrnpf.getOrigamt().compareTo(BigDecimal.ZERO) == 0
		&& rtrnpf.getAcctamt().compareTo(BigDecimal.ZERO) == 0) {
			return ;
		}
		if ("".equals(rtrnpf.getGlcode())) {
			return ;
		}
		Dglxpf dglxpf = new Dglxpf();
		dglxpf.setBatccoy(rtrnpf.getBatccoy());
		dglxpf.setBatcbrn(rtrnpf.getBatcbrn());
		dglxpf.setBatcactyr(rtrnpf.getBatcactyr());
		dglxpf.setBatcactmn(rtrnpf.getBatcactmn());
		dglxpf.setBatctrcde(rtrnpf.getBatctrcde());
		dglxpf.setBatcbatch(rtrnpf.getBatcbatch());
		dglxpf.setGenlcoy(rtrnpf.getGenlcoy());
		dglxpf.setGenlcur(rtrnpf.getGenlcur());
		dglxpf.setGenlcde(rtrnpf.getGlcode());
		dglxpf.setEffdate(rtrnpf.getEffdate());
		dglxpf.setOrigcurr(rtrnpf.getOrigccy());
		dglxpf.setPostyear(Integer.parseInt(rtrnpf.getPostyear()));
		dglxpf.setPostmonth(Integer.parseInt(rtrnpf.getPostmonth()));
		dglxpf.setRldgacct(rtrnpf.getRldgacct());
		if ("-".equals(rtrnpf.getGlsign().trim())) {
			dglxpf.setOrigamt(BigDecimal.ZERO.subtract(rtrnpf.getOrigamt()).setScale(2));
			dglxpf.setAcctamt(BigDecimal.ZERO.subtract(rtrnpf.getAcctamt()).setScale(2));
		}
		else {
			dglxpf.setOrigamt(rtrnpf.getOrigamt());
			dglxpf.setAcctamt(rtrnpf.getAcctamt());
		}
		dglxpf.setCrtdatime(new Timestamp(System.currentTimeMillis()));
		dglxpfDAO.insertDglxpf(dglxpf);
	}
	
	protected void callBatcup(LifrtrnDTO lifrtrnPojo, int bval){
		
		BatcupDTO batcupDTO  = new BatcupDTO();
		batcupDTO.setBatcpfx("BA");
		batcupDTO.setBatccoy(lifrtrnPojo.getBatccoy());
		batcupDTO.setBatcbrn(lifrtrnPojo.getBatcbrn());
		batcupDTO.setBatcactyr(lifrtrnPojo.getBatcactyr());
		batcupDTO.setBatcactmn(lifrtrnPojo.getBatcactmn());
		batcupDTO.setBatctrcde(lifrtrnPojo.getBatctrcde());
		batcupDTO.setBatcbatch(lifrtrnPojo.getBatcbatch());
		batcupDTO.setTrancnt(0);
		batcupDTO.setEtreqcnt(0);
		batcupDTO.setSub(lifrtrnPojo.getContot());
		batcupDTO.setAscnt(0);
		batcupDTO.setBcnt(1);
		/* MOVE LIFR-ACCTAMT           TO BCUP-BVAL.                    */
		batcupDTO.setBval(bval);
		batcupDTO.setFunction("KEEPS");
		batlifecup.callBatcup(batcupDTO);

		
		if (batcupDTO.getStatuz().equals("*****")) {
			throw new RuntimeException("LifrtrnUtils Batcup Error");
		}
	}
	
	public String checkSchedule24x7Setup(String coy) throws IOException{
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem(coy, "TR362", "***", CommonConstants.IT_ITEMPFX);
		String appflag = "";
		if(itempf!=null) {
		  	Tr362 tr362IO  = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(),Tr362.class);
		  	appflag = tr362IO.getAppflag();
		}
		return appflag;
	}

}
