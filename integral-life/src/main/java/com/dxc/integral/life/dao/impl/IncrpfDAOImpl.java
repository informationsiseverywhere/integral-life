package com.dxc.integral.life.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.IncrpfDAO;
import com.dxc.integral.life.dao.model.Incrpf;

/**
 * 
 * IncrpfDAO implementation for database table <b>INCRPF</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("incrpfDAO")
public class IncrpfDAOImpl extends BaseDAOImpl implements IncrpfDAO {

	@Override
	public List<Incrpf> readValidIncrpf(String company, String contractNumber) {
		StringBuilder sql = new StringBuilder(
				"SELECT * FROM INCRPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG = '1' ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Incrpf>(Incrpf.class));
	}

	@Override
	public List<Incrpf> readInValidIncrpf(String company, String contractNumber, String life, String coverage,
			String rider, int plnsfx) {
		StringBuilder sql = new StringBuilder(
				"SELECT * FROM INCRPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG = '2' AND REFFLAG='' ");
		sql.append(" AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?  ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(),
				new Object[] { company, contractNumber, life, coverage, rider, plnsfx },
				new BeanPropertyRowMapper<Incrpf>(Incrpf.class));
	}
	
	@Override
	public List<Incrpf> readIncrRecord(String company, String chdrnum) {
		String sql ="SELECT * FROM INCRPF WHERE CHDRCOY=? AND CHDRNUM=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC";
		return jdbcTemplate.query(sql, new Object[] {company, chdrnum}, new BeanPropertyRowMapper<Incrpf>(Incrpf.class));
	}

	
	public void insertIncrpfRecords(List<Incrpf> incrpfList){
		final List<Incrpf> tempIncrpfList = incrpfList;   
	       String sql = "insert into incrpf(chdrcoy, chdrnum, life, jlife, coverage, rider, plnsfx, effdate, tranno, validflag, statcode, pstatcode, crrcd, crtable, originst, lastinst," 
					+"newinst, origsum, lastsum, newsum, annvry, bascmeth, pctinc, refflag, trdt, trtm, user_t, termid, bascpy, ceaseind, rnwcpy, srvcpy, zboriginst, zblastinst,"
					+"zbnewinst, zloriginst, zllastinst, zlnewinst,zstpduty01, usrprf, jobnm, datime)"
		    		+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	       jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
	            @Override
	            public int getBatchSize() {  
	                 return tempIncrpfList.size();  
	            }  
	            @Override  
	            public void setValues(PreparedStatement ps, int i)  
	                    throws SQLException { 
	            	ps.setString(1, incrpfList.get(i).getChdrcoy());
					ps.setString(2, incrpfList.get(i).getChdrnum());
					ps.setString(3, incrpfList.get(i).getLife());
					ps.setString(4, incrpfList.get(i).getJlife());
					ps.setString(5, incrpfList.get(i).getCoverage());
					ps.setString(6, incrpfList.get(i).getRider());
					ps.setInt(7, incrpfList.get(i).getPlnsfx());
					ps.setInt(8, incrpfList.get(i).getEffdate());
					ps.setInt(9, incrpfList.get(i).getTranno());
					ps.setString(10, incrpfList.get(i).getValidflag());
					ps.setString(11, incrpfList.get(i).getStatcode());
					ps.setString(12, incrpfList.get(i).getPstatcode());
					ps.setInt(13, incrpfList.get(i).getCrrcd());
					ps.setString(14, incrpfList.get(i).getCrtable());
					ps.setBigDecimal(15, incrpfList.get(i).getOrigInst());
					ps.setBigDecimal(16, incrpfList.get(i).getLastInst());
					ps.setBigDecimal(17, incrpfList.get(i).getNewinst());
					ps.setBigDecimal(18, incrpfList.get(i).getOrigSum());
					ps.setBigDecimal(19, incrpfList.get(i).getLastSum());
					ps.setBigDecimal(20, incrpfList.get(i).getNewsum());
					ps.setString(21, incrpfList.get(i).getAnniversaryMethod());
					ps.setString(22, incrpfList.get(i).getBasicCommMeth());
					ps.setInt(23, incrpfList.get(i).getPctinc());
					ps.setString(24, incrpfList.get(i).getRefusalFlag());
					ps.setInt(25, incrpfList.get(i).getTransactionDate());
					ps.setInt(26, incrpfList.get(i).getTransactionTime());
					ps.setInt(27, incrpfList.get(i).getUser());
					ps.setString(28, incrpfList.get(i).getTermid());
					ps.setString(29, incrpfList.get(i).getBascpy());
					ps.setString(30, incrpfList.get(i).getCeaseInd());
					ps.setString(31, incrpfList.get(i).getRnwcpy());
					ps.setString(32, incrpfList.get(i).getSrvcpy());
					ps.setBigDecimal(33, incrpfList.get(i).getZboriginst());
					ps.setBigDecimal(34, incrpfList.get(i).getZblastinst());
					ps.setBigDecimal(35, incrpfList.get(i).getZbnewinst());
					ps.setBigDecimal(36, incrpfList.get(i).getZloriginst());
					ps.setBigDecimal(37, incrpfList.get(i).getZllastinst());
					ps.setBigDecimal(38, incrpfList.get(i).getZlnewinst());
					ps.setBigDecimal(39, incrpfList.get(i).getZstpduty01() == null ? BigDecimal.ZERO : incrpfList.get(i).getZstpduty01());
					ps.setString(40, incrpfList.get(i).getUsrprf());
					ps.setString(41, incrpfList.get(i).getJobnm());
					ps.setTimestamp(42, new Timestamp(System.currentTimeMillis()));
	            }   
	      });  
	}
	
	@Override
	public List<Incrpf> readIncrpfList(String company, String contractNumber, String life, String coverage,String rider, int plnsfx) {
		StringBuilder sql = new StringBuilder(
				"SELECT * FROM INCRPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG = '1' ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(),
				new Object[] { company, contractNumber, life, coverage, rider, plnsfx },
				new BeanPropertyRowMapper<Incrpf>(Incrpf.class));
	}
}
