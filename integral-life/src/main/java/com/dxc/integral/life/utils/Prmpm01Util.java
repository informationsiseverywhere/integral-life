package com.dxc.integral.life.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Clexpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.exceptions.GOTOException;
import com.dxc.integral.life.exceptions.GOTOInterface;
import com.dxc.integral.life.smarttable.pojo.T5658;
import com.dxc.integral.life.smarttable.pojo.T5659;
import com.dxc.integral.life.smarttable.pojo.Th549;
import com.dxc.integral.life.smarttable.pojo.Th606;
import com.dxc.integral.life.smarttable.pojo.Th609;
@Service("prmpm01Util")
public class Prmpm01Util {
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ClexpfDAO clexpfDAO;
	@Autowired
	private LextpfDAO lextpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		loopForAdjustedAge, 
		checkT5658Insprm, 
		exit490,
		calcMortLoadings,
		callSubroutine,
		exit950
	}
	private BigDecimal wsaaBapBuff;
	private int wsaaRatesPerMillieTot = 0;
	private int wsaaAdjustedAge = 0;
	private int wsaaIndex = 0;
	private BigDecimal wsaaDiscountAmt = BigDecimal.ZERO;
	private BigDecimal wsaaBap;
	private BigDecimal wsaaBip;
	private BigDecimal wsaaModalFactor;
	private BigDecimal wsaaPremiumAdjustTot;
	private int wsaaAgerateTot;
	private int wsaaSub;
	private BigDecimal[] wsaaLextOppcs = new BigDecimal[8];
	private Integer[] wsaaLextZmortpcts = new Integer[8];
	private String[] wsaaLextOpcdas = new String[8];
	private String wsaaBasicPremium = "";
	private String wsaaStaffFlag = "";
	private String wsaaMortalityLoad = "";
	private String wsaaTh606Indic = "";
	private T5658 t5658IO = null;
	private T5659 t5659IO = null;
	private Th609 th609IO = null;
	private Th606 th606IO = null;
	private Th549 th549IO = null;
	private Chdrpf chdrlnb = null;
	private Clexpf clexpf = null;
	private BigDecimal wsaaStaffDiscount;
	private BigDecimal wsaaMortRate;
	private BigDecimal wsaaMortFactor;
	private BigDecimal wsaaRoundNum;
	private BigDecimal wsaaRoundDec;
	private BigDecimal wsaaRound1;
	private BigDecimal wsaaRound10;
	private BigDecimal wsaaRound100;
	private BigDecimal wsaaRound1000;
	private BigDecimal wsaaRound10000;
	private List<Lextpf> lextpfList = null;
	private Iterator<Lextpf> lextpfIterator;
	private BigDecimal extLoading;
	boolean isLoadingAva = false;
	public PremiumDTO processPrempm01(PremiumDTO premiumDTO){
		premiumDTO.setStatuz("****");
		wsaaBapBuff = BigDecimal.ZERO;
		wsaaBasicPremium = "N";
		initialize(premiumDTO);
		if ("****".equals(premiumDTO.getStatuz())) {
			staffDiscount(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO);
			premiumDTO.setAdjustageamt(wsaaBap);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "B".equals(th609IO.getIndic())) {
			wsaaBap = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100).multiply(wsaaBap)).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			wsaaBap = new BigDecimal(wsaaRatesPerMillieTot).add(wsaaBap).setScale(2,BigDecimal.ROUND_HALF_UP);
			premiumDTO.setRateadj(new BigDecimal(wsaaRatesPerMillieTot).setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			if(t5658IO.getUnit() == 0) {
				premiumDTO.setStatuz("F110");
			}
			else {
				wsaaBap = wsaaBap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5658IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
			}
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			mortalityLoadings(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			percentageLoadings();
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "I".equals(th609IO.getIndic())) {
			wsaaBip = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100)).multiply(wsaaBip).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			premiumDTO.setCalcPrem(wsaaPremiumAdjustTot.add(premiumDTO.getCalcPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
			premiumDTO.setPremadj(wsaaPremiumAdjustTot.setScale(2));
		}
		wsaaBasicPremium = "Y";
		if ("****".equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO);
			premiumDTO.setAdjustageamt(premiumDTO.getAdjustageamt().subtract(wsaaBap));
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "B".equals(th609IO.getIndic())) {
			wsaaBap = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100)).multiply(wsaaBap).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			wsaaBap = new BigDecimal(wsaaRatesPerMillieTot).add(wsaaBap).setScale(2,BigDecimal.ROUND_HALF_UP);
			premiumDTO.setRateadj(new BigDecimal(wsaaRatesPerMillieTot).setScale(2,BigDecimal.ROUND_HALF_UP));
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			if (t5658IO.getUnit() == 0) {
				premiumDTO.setStatuz("F110");
			}
			else {
				wsaaBap = wsaaBap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5658IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
			}
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(wsaaStaffFlag) && wsaaStaffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "I".equals(th609IO.getIndic())) {
		
			wsaaBip = new BigDecimal(100).subtract(wsaaStaffDiscount).divide(new BigDecimal(100)).multiply(wsaaBip).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		premiumDTO.setCalcLoaPrem(premiumDTO.getCalcPrem().subtract(premiumDTO.getCalcBasPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
		if(isLoadingAva == true)
			premiumDTO.setLoadper(premiumDTO.getLoadper().subtract(new BigDecimal(wsaaRatesPerMillieTot).add(premiumDTO.getAdjustageamt())).setScale(2,BigDecimal.ROUND_HALF_UP));
		if (wsaaBapBuff.compareTo(BigDecimal.ZERO) == -1 && wsaaAdjustedAge == 0) {
			if (premiumDTO.getCalcPrem().compareTo(premiumDTO.getCalcBasPrem()) == -1) {
				premiumDTO.setCalcPrem(premiumDTO.getCalcBasPrem());
				premiumDTO.setCalcLoaPrem(premiumDTO.getCalcPrem().add(wsaaBapBuff).subtract(premiumDTO.getCalcBasPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
				if (premiumDTO.getCalcLoaPrem().compareTo(premiumDTO.getCalcPrem()) == 1) {
					premiumDTO.setCalcLoaPrem(wsaaBapBuff);
				}
			}
		}

		return premiumDTO;
		
	}
	protected void rounding(PremiumDTO premiumDTO){
		wsaaRoundNum = wsaaBip;
		wsaaRound10000 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(12));
		wsaaRound1000 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(13));
		wsaaRound100 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(14));
		wsaaRound10 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(15));
		wsaaRound1 = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(16));
		wsaaRoundDec = new BigDecimal(formatRoundNum(wsaaRoundNum).substring(17));

		if (t5659IO.getRndfact() ==1  || t5659IO.getRndfact() == 0) {
			if (wsaaRoundDec.compareTo(new BigDecimal(0.5)) == -1) {
				wsaaRoundDec = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(1));
				wsaaRoundDec = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 10) {
			if (wsaaRound1.compareTo(new BigDecimal(5)) == -1) {
				wsaaRound1 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(10));
				wsaaRound1 =  BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 100) {
			if (wsaaRound10.compareTo(new BigDecimal(50)) == -1) {
				wsaaRound10 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(100));
				wsaaRound10 = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 1000) {
			if (wsaaRound100.compareTo(new BigDecimal(500)) == -1) {
				wsaaRound100 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(1000));
				wsaaRound100 = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 10000) {
			if (wsaaRound1000.compareTo(new BigDecimal(5000)) == -1) {
				wsaaRound1000 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(10000));
				wsaaRound1000 = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 100000) {
			if (wsaaRound10000.compareTo(new BigDecimal(50000)) == -1) {
				wsaaRound10000 = BigDecimal.ZERO;
			}else {
				wsaaRoundNum = wsaaRoundNum.add(new BigDecimal(100000));
				wsaaRound10000 = BigDecimal.ZERO;
			}
		}
		wsaaBip = wsaaRoundNum;
		if (t5658IO.getPremUnit() == 0) {
			premiumDTO.setCalcPrem(wsaaBip);
		} else {
			if ("Y".equals(wsaaBasicPremium)) {
				premiumDTO.setCalcBasPrem(wsaaBip.divide(new BigDecimal(t5658IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			} else {
				premiumDTO.setCalcPrem(wsaaBip.divide(new BigDecimal(t5658IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
		}
	}
	protected void instalmentPremium(PremiumDTO premiumDTO){
		wsaaBip = BigDecimal.ZERO;
		wsaaModalFactor = BigDecimal.ZERO;
		if ("01".equals(premiumDTO.getBillfreq())|| "00".equals(premiumDTO.getBillfreq())) {
			wsaaModalFactor = t5658IO.getMfacty();
		}else {
			if ("02".equals(premiumDTO.getBillfreq())) {
				wsaaModalFactor = t5658IO.getMfacthy();
			}else {
				if ("04".equals(premiumDTO.getBillfreq())) {
					wsaaModalFactor = t5658IO.getMfactq();
				}else {
					if ("12".equals(premiumDTO.getBillfreq())) {
						wsaaModalFactor  = t5658IO.getMfactm();
					}else {
						if ("13".equals(premiumDTO.getBillfreq())) {
							wsaaModalFactor = t5658IO.getMfact4w();
						}else {
							if ("24".equals(premiumDTO.getBillfreq())) {
								wsaaModalFactor = t5658IO.getMfacthm();
							}else {
								if ("26".equals(premiumDTO.getBillfreq())) {
									wsaaModalFactor = t5658IO.getMfact2w();
								}else {
									if ("52".equals(premiumDTO.getBillfreq())) {
										wsaaModalFactor= th606IO.getMfactw();
									}
								}
							}
						}
					}
				}
			}
		}
		if (wsaaModalFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("F272");
		}else {
			wsaaBip = wsaaBap.multiply(wsaaModalFactor).setScale(4,BigDecimal.ROUND_HALF_UP);
		}
		
		
	}
	protected void percentageLoadings(){
		for(wsaaSub = 0 ; wsaaSub < 8; wsaaSub++)
			if (wsaaLextOppcs[wsaaSub].compareTo(BigDecimal.ZERO) == -1) {
				wsaaBap = wsaaBap.multiply(wsaaLextOppcs[wsaaSub]).divide(new BigDecimal(100));
				isLoadingAva = true;
			}
	}
	protected void volumeDiscountBap(PremiumDTO premiumDTO){
		String t5659Itemitem= t5658IO.getDisccntmeth().concat(premiumDTO.getCurrcode());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		t5659IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5659", t5659Itemitem,itemFrm,T5659.class);
		if(t5659IO == null){
			premiumDTO.setStatuz("F265");
			return;
		}
		for(wsaaSub = 0; wsaaSub < 4 ; wsaaSub ++){
			if(premiumDTO.getSumin().compareTo(t5659IO.getVolbanfrs().get(wsaaSub)) == -1
					|| premiumDTO.getSumin().compareTo(t5659IO.getVolbantos().get(wsaaSub)) ==1){
				continue;
			}else{
				wsaaDiscountAmt = t5659IO.getVolbanles().get(wsaaSub);
				break;
			}
		}
		wsaaBap = wsaaBap.subtract(wsaaDiscountAmt).setScale(2,BigDecimal.ROUND_HALF_UP);
	}
	protected void basicAnnualPremium(PremiumDTO premiumDTO){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey(premiumDTO);
					readLext(premiumDTO);
				case loopForAdjustedAge: 
					loopForAdjustedAge(premiumDTO); 
				case checkT5658Insprm: 
					checkT5658Insprm(premiumDTO);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}

	}
	protected void readLext(PremiumDTO premiumDTO){
		if(null == lextpfList || lextpfList.isEmpty()){
			goTo(GotoLabel.loopForAdjustedAge);
		}else{
			wsaaSub = 0;
			for(Lextpf lextIO : lextpfList){
				if ("2".equals(premiumDTO.getReasind())&& "1".equals(lextIO.getReasind())) {
					goTo(GotoLabel.loopForAdjustedAge);
				}
				if (!"2".equals(premiumDTO.getReasind())&& "2".equals(lextIO.getReasind())) {
					goTo(GotoLabel.loopForAdjustedAge);
				}
				if (lextIO.getExtcd() <= premiumDTO.getReRateDate()) {
					continue;
				}
				wsaaLextOppcs[wsaaSub] = lextIO.getOppc();
				wsaaLextZmortpcts[wsaaSub] = lextIO.getZmortpct();
				wsaaLextOpcdas[wsaaSub] = lextIO.getOpcda();
				wsaaRatesPerMillieTot += lextIO.getInsprm();
				wsaaPremiumAdjustTot =  wsaaPremiumAdjustTot.add(lextIO.getPremadj());
				wsaaAgerateTot += lextIO.getAgerate();
				wsaaSub++;
			}
		}
	}
	protected void loopForAdjustedAge(PremiumDTO premiumDTO){
		wsaaAdjustedAge = wsaaAgerateTot + premiumDTO.getLage();
	}
	protected void setupLextKey(PremiumDTO premiumDTO){
		if( "Y".equals(wsaaBasicPremium)) {
			wsaaAdjustedAge = premiumDTO.getLage();
			goTo(GotoLabel.checkT5658Insprm);
		}
		lextpfList = lextpfDAO.getLextpfListRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(),
				premiumDTO.getCovrCoverage(), premiumDTO.getCovrRider());
	}
	protected void checkT5658Insprm(PremiumDTO premiumDTO){

		if (wsaaAdjustedAge < 0) {
			wsaaAdjustedAge = 110;
		}
		if (wsaaAdjustedAge < 0 || wsaaAdjustedAge > 110) {
			premiumDTO.setStatuz("E107");
			return ;
		}
		if (wsaaAdjustedAge == 0) {
			if (t5658IO.getInsprem() == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				wsaaBap = new BigDecimal(t5658IO.getInsprem());
			}
			return ;
		}
		
		if (wsaaAdjustedAge >= 100 && wsaaAdjustedAge <= 110) {
			wsaaIndex = wsaaAdjustedAge - 99;
			if (t5658IO.getInstprs().get(wsaaIndex) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				wsaaBap = new BigDecimal(t5658IO.getInstprs().get(wsaaIndex));
			}
		}
		else {
			if (t5658IO.getInsprms().get(wsaaAdjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				wsaaBap = new BigDecimal(t5658IO.getInsprms().get(wsaaAdjustedAge));
			}
		}
	}
	private void staffDiscount(PremiumDTO premiumDTO){
		List<Chdrpf> chdrpfList = chdrpfDAO.readRecordOfChdrlnb(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum());
		if(chdrpfList == null || chdrpfList.isEmpty()){
			throw new DataNoFoundException("Rerate processor: Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table Chdrpf while processing contract ");
		}else{
			chdrlnb = chdrpfList.get(0); 
		}
		if (chdrlnb.getCownnum() == null ||"".equals(chdrlnb.getCownnum())) {
			return;
		}
		clexpf = clexpfDAO.getClexpfRecord(chdrlnb.getCownpfx(), chdrlnb.getCowncoy(), chdrlnb.getCownnum());
//		if(clexpf == null){
//			throw new DataNoFoundException("Rerate processor: Client no"+chdrlnb.getCownnum()+" not found in table clexpf while processing contract");
//		}
		if (null != clexpf && !"Y".equals(clexpf.getRstaflag()) && !"".equals(chdrlnb.getJownnum())){
			clexpf = clexpfDAO.getClexpfRecord(chdrlnb.getCownpfx(), chdrlnb.getCowncoy(), chdrlnb.getJownnum());
		}
		if(null != clexpf && "Y".equals(clexpf.getRstaflag())){
			th609IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TH609", chdrlnb.getCnttype(),Th609.class);
			if(th609IO != null){
				th609IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TH609", "***",Th609.class);
				wsaaStaffDiscount = th609IO.getPrcnt();
			}else{
				wsaaStaffDiscount = BigDecimal.ZERO;
			}
		}
		
		wsaaStaffFlag = clexpf == null ? "" : clexpf.getRstaflag();
	}
	private void initialize(PremiumDTO premiumDTO){
		wsaaRatesPerMillieTot = 0;
		wsaaPremiumAdjustTot = BigDecimal.ZERO;
		wsaaStaffDiscount = BigDecimal.ZERO;
		wsaaBap = BigDecimal.ZERO;
		wsaaBip = BigDecimal.ZERO;
		wsaaAdjustedAge = 0;
		wsaaDiscountAmt = BigDecimal.ZERO;
		wsaaAgerateTot = 0;
		wsaaSub = 0;
		/*BRD-306 START */
		premiumDTO.setFltmort(BigDecimal.ZERO);
		premiumDTO.setLoadper(BigDecimal.ZERO);
		/*BRD-306 END */
		for (wsaaSub = 0; wsaaSub < 8; wsaaSub ++){
			wsaaLextOppcs[wsaaSub] = BigDecimal.ZERO;
			wsaaLextZmortpcts[wsaaSub] = 0;
			wsaaLextOpcdas[wsaaSub] = "";
		}
		String freqFactor = premiumDTO.getDuration() < 10 ? String.format("%02d", premiumDTO.getDuration()) : String.valueOf( premiumDTO.getDuration()); ;
		String t5658Itemitem= premiumDTO.getCrtable().concat(freqFactor).concat(premiumDTO.getMortcls()).concat(premiumDTO.getLsex());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();

		t5658IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "T5658", t5658Itemitem,itemFrm,T5658.class);
		if(t5658IO == null){
			premiumDTO.setStatuz("F265");
		}
		
	}
	protected void mortalityLoadings(PremiumDTO premiumDTO){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para(premiumDTO);
				case calcMortLoadings: 
					calcMortLoadings(premiumDTO);
				case callSubroutine: 
					callSubroutine(premiumDTO);
				case exit950: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void para(PremiumDTO premiumDTO){
		premiumDTO.setFltmort(BigDecimal.ZERO);
		wsaaMortalityLoad = "N";
		for (wsaaSub = 0; !(wsaaSub > 7 || "Y".equals(wsaaMortalityLoad)); wsaaSub++){
			if (wsaaLextZmortpcts[wsaaSub] != 0) {
				wsaaMortalityLoad = "Y";
			}
		}
		if ("N".equals(wsaaMortalityLoad)) {
			goTo(GotoLabel.exit950);
		}
		wsaaSub = 0;
	}
	protected void calcMortLoadings(PremiumDTO premiumDTO){
		for(wsaaSub = 0 ; wsaaSub < 8; wsaaSub++){
			if (wsaaLextZmortpcts[wsaaSub] != 0){
				String th549Itemitem= premiumDTO.getCrtable().concat(String.valueOf(wsaaLextZmortpcts[wsaaSub])).concat(premiumDTO.getLsex());
				int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
				
				th549IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TH549", th549Itemitem,itemFrm,Th549.class);
				if(th549IO == null){
					premiumDTO.setStatuz("HL27");
					goTo(GotoLabel.exit950);
				}
				Lifepf lifepf = lifepfDAO.getLifeRecordByCurrfrom(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(), premiumDTO.getLifeJlife(), 99999999);
				if(lifepf == null){
					throw new DataNoFoundException("Br613: Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table LIFEPF while processing contract ");
				}
				wsaaTh606Indic = "S".equals(lifepf.getSmoking())?th549IO.getIndcs().get(0):th549IO.getIndcs().get(1);
				String th606Itemitem = String.valueOf(premiumDTO.getDuration()).concat(premiumDTO.getCrtable()).concat(wsaaTh606Indic);
				
				th606IO = itempfService.readSmartTableByTrim(premiumDTO.getChdrChdrcoy(), "TH606", th606Itemitem,itemFrm,Th606.class);
				if(th606IO == null){
					premiumDTO.setStatuz("HL26");
					wsaaMortFactor =BigDecimal.ZERO;
					wsaaMortRate = BigDecimal.ZERO;
					goTo(GotoLabel.callSubroutine);
				}
				getValues(premiumDTO);
			}
		}
	}

	protected void callSubroutine(PremiumDTO premiumDTO){
		for (int wsaaCount = 0; wsaaCount < 5 || "".equals(th549IO.getOpcdas().get(wsaaCount)); wsaaCount++){
			if (th549IO.getOpcdas().get(wsaaCount).equals(wsaaLextOpcdas[wsaaSub])) {
				if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPPMIL")){
					extLoading = wsaaMortRate.multiply(wsaaMortFactor).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000))).setScale(4);
				}
				if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPDLVL")){
					extLoading = wsaaMortFactor.multiply(new BigDecimal(100)).multiply(th549IO.getExpfactor()).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000)))
							.multiply(new BigDecimal(wsaaLextZmortpcts[wsaaSub]).divide(new BigDecimal(100))).setScale(5);
				}
				break;
			}
			else {
				if ("**".equals(th549IO.getOpcdas().get(wsaaCount))) {
					if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPPMIL")){
						extLoading = wsaaMortRate.multiply(wsaaMortFactor).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000))).setScale(4);
					}
					if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPDLVL")){
						extLoading = wsaaMortFactor.multiply(new BigDecimal(100)).multiply(th549IO.getExpfactor()).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000)))
								.multiply(new BigDecimal(wsaaLextZmortpcts[wsaaSub]).divide(new BigDecimal(100))).setScale(5);
					}
					break;
				}
			}
		}
		wsaaBap = wsaaBap.add(extLoading).setScale(2,BigDecimal.ROUND_HALF_UP);
		premiumDTO.setFltmort(extLoading.divide(new BigDecimal(t5658IO.getPremUnit())));
		goTo(GotoLabel.calcMortLoadings);
	}
	protected void getValues(PremiumDTO premiumDTO){
		if (wsaaAdjustedAge < 0) {
			wsaaAdjustedAge = 110;
		}
		if (wsaaAdjustedAge < 0 || wsaaAdjustedAge > 110) {
			premiumDTO.setStatuz("E107");
			return;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (wsaaAdjustedAge == 0) {
			if (th606IO.getInsprem() == 0) {
				premiumDTO.setStatuz("E107");
				return;
			}
			else {
				wsaaBap = new BigDecimal(th606IO.getInsprem());
				wsaaMortRate =  new BigDecimal(th606IO.getInsprem());
				getModalFactor(premiumDTO);
			}
		}
		
		if (wsaaAdjustedAge >= 100 && wsaaAdjustedAge <= 110) {
			wsaaIndex = wsaaAdjustedAge - 99;
			if (th606IO.getInstprs().get(wsaaIndex) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				wsaaMortRate = new BigDecimal(th606IO.getInstprs().get(wsaaIndex));
			}
		}
		else {
			if (th606IO.getInsprms().get(wsaaAdjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				wsaaMortRate = new BigDecimal(th606IO.getInsprms().get(wsaaAdjustedAge));
			}
		}
	}
	protected void getModalFactor(PremiumDTO premiumDTO){
		wsaaMortFactor = BigDecimal.ZERO;
		if ("01".equals(premiumDTO.getBillfreq())|| "00".equals(premiumDTO.getBillfreq())) {
			wsaaMortFactor = th606IO.getMfacty();
		}
		else {
			if ("02".equals(premiumDTO.getBillfreq())) {
				wsaaMortFactor = th606IO.getMfacthy();
			}
			else {
				if ("04".equals(premiumDTO.getBillfreq())) {
					wsaaMortFactor = th606IO.getMfactq();
				}
				else {
					if ("12".equals(premiumDTO.getBillfreq())) {
						wsaaMortFactor  = th606IO.getMfactm();
					}
					else {
						if ("13".equals(premiumDTO.getBillfreq())) {
							wsaaMortFactor = th606IO.getMfact4w();
						}
						else {
							if ("24".equals(premiumDTO.getBillfreq())) {
								wsaaMortFactor = th606IO.getMfacthm();
							}
							else {
								if ("26".equals(premiumDTO.getBillfreq())) {
									wsaaMortFactor = th606IO.getMfact2w();
								}
								else {
									if ("52".equals(premiumDTO.getBillfreq())) {
										wsaaMortFactor= th606IO.getMfactw();
									}
								}
							}
						}
					}
				}
			}
		}
		if (wsaaMortFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("F272");
		}
	}
	public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}	
	private String formatRoundNum(BigDecimal obj) {
		DecimalFormat df=new DecimalFormat("0000000000000000000.00");
		String str2=df.format(obj);
		return str2;
	}
}
