package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
/**
 * @author wli31
 */
public class Nlgtpf{
private long uniqueNumber;
private String chdrcoy;
private String chdrnum;
private int tranno;
private int effdate;
private int batcactyr;
private int batcactmn;
private String batctrcde;
private String trandesc;
private BigDecimal tranamt;
private int fromdate;
private int todate;
private String nlgflag;
private int yrsinf;
private int age;
private BigDecimal nlgbal;

private BigDecimal amnt01;
private BigDecimal amnt02;
private BigDecimal amnt03;
private BigDecimal amnt04;
private String validflag;
private String usrprf;
private String jobnm;
private String datime;
/**
 * @return the uniqueNumber
 */
public long getUniqueNumber() {
	return uniqueNumber;
}
/**
 * @param uniqueNumber the uniqueNumber to set
 */
public void setUniqueNumber(long uniqueNumber) {
	this.uniqueNumber = uniqueNumber;
}
/**
 * @return the chdrcoy
 */
public String getChdrcoy() {
	return chdrcoy;
}
/**
 * @param chdrcoy the chdrcoy to set
 */
public void setChdrcoy(String chdrcoy) {
	this.chdrcoy = chdrcoy;
}
/**
 * @return the chdrnum
 */
public String getChdrnum() {
	return chdrnum;
}
/**
 * @param chdrnum the chdrnum to set
 */
public void setChdrnum(String chdrnum) {
	this.chdrnum = chdrnum;
}
/**
 * @return the tranno
 */
public int getTranno() {
	return tranno;
}
/**
 * @param tranno the tranno to set
 */
public void setTranno(int tranno) {
	this.tranno = tranno;
}
/**
 * @return the effdate
 */
public int getEffdate() {
	return effdate;
}
/**
 * @param effdate the effdate to set
 */
public void setEffdate(int effdate) {
	this.effdate = effdate;
}
/**
 * @return the batcactyr
 */
public int getBatcactyr() {
	return batcactyr;
}
/**
 * @param batcactyr the batcactyr to set
 */
public void setBatcactyr(int batcactyr) {
	this.batcactyr = batcactyr;
}
/**
 * @return the batcactmn
 */
public int getBatcactmn() {
	return batcactmn;
}
/**
 * @param batcactmn the batcactmn to set
 */
public void setBatcactmn(int batcactmn) {
	this.batcactmn = batcactmn;
}
/**
 * @return the batctrcde
 */
public String getBatctrcde() {
	return batctrcde;
}
/**
 * @param batctrcde the batctrcde to set
 */
public void setBatctrcde(String batctrcde) {
	this.batctrcde = batctrcde;
}
/**
 * @return the trandesc
 */
public String getTrandesc() {
	return trandesc;
}
/**
 * @param trandesc the trandesc to set
 */
public void setTrandesc(String trandesc) {
	this.trandesc = trandesc;
}
/**
 * @return the tranamt
 */
public BigDecimal getTranamt() {
	return tranamt;
}
/**
 * @param tranamt the tranamt to set
 */
public void setTranamt(BigDecimal tranamt) {
	this.tranamt = tranamt;
}
/**
 * @return the fromdate
 */
public int getFromdate() {
	return fromdate;
}
/**
 * @param fromdate the fromdate to set
 */
public void setFromdate(int fromdate) {
	this.fromdate = fromdate;
}
/**
 * @return the todate
 */
public int getTodate() {
	return todate;
}
/**
 * @param todate the todate to set
 */
public void setTodate(int todate) {
	this.todate = todate;
}
/**
 * @return the nlgflag
 */
public String getNlgflag() {
	return nlgflag;
}
/**
 * @param nlgflag the nlgflag to set
 */
public void setNlgflag(String nlgflag) {
	this.nlgflag = nlgflag;
}
/**
 * @return the yrsinf
 */
public int getYrsinf() {
	return yrsinf;
}
/**
 * @param yrsinf the yrsinf to set
 */
public void setYrsinf(int yrsinf) {
	this.yrsinf = yrsinf;
}
/**
 * @return the age
 */
public int getAge() {
	return age;
}
/**
 * @param age the age to set
 */
public void setAge(int age) {
	this.age = age;
}
/**
 * @return the nlgbal
 */
public BigDecimal getNlgbal() {
	return nlgbal;
}
/**
 * @param nlgbal the nlgbal to set
 */
public void setNlgbal(BigDecimal nlgbal) {
	this.nlgbal = nlgbal;
}
/**
 * @return the amnt01
 */
public BigDecimal getAmnt01() {
	return amnt01;
}
/**
 * @param amnt01 the amnt01 to set
 */
public void setAmnt01(BigDecimal amnt01) {
	this.amnt01 = amnt01;
}
/**
 * @return the amnt02
 */
public BigDecimal getAmnt02() {
	return amnt02;
}
/**
 * @param amnt02 the amnt02 to set
 */
public void setAmnt02(BigDecimal amnt02) {
	this.amnt02 = amnt02;
}
/**
 * @return the amnt03
 */
public BigDecimal getAmnt03() {
	return amnt03;
}
/**
 * @param amnt03 the amnt03 to set
 */
public void setAmnt03(BigDecimal amnt03) {
	this.amnt03 = amnt03;
}
/**
 * @return the amnt04
 */
public BigDecimal getAmnt04() {
	return amnt04;
}
/**
 * @param amnt04 the amnt04 to set
 */
public void setAmnt04(BigDecimal amnt04) {
	this.amnt04 = amnt04;
}
/**
 * @return the validflag
 */
public String getValidflag() {
	return validflag;
}
/**
 * @param validflag the validflag to set
 */
public void setValidflag(String validflag) {
	this.validflag = validflag;
}
/**
 * @return the usrprf
 */
public String getUsrprf() {
	return usrprf;
}
/**
 * @param usrprf the usrprf to set
 */
public void setUsrprf(String usrprf) {
	this.usrprf = usrprf;
}
/**
 * @return the jobnm
 */
public String getJobnm() {
	return jobnm;
}
/**
 * @param jobnm the jobnm to set
 */
public void setJobnm(String jobnm) {
	this.jobnm = jobnm;
}
/**
 * @return the datime
 */
public String getDatime() {
	return datime;
}
/**
 * @param datime the datime to set
 */
public void setDatime(String datime) {
	this.datime = datime;
}



}