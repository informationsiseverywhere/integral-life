package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.CrtundwrtDTO;

/**
 * @author wli31
 */
@FunctionalInterface
public interface Crtundwrt {
	public CrtundwrtDTO processCrtundwrt(CrtundwrtDTO crtundwrtDTO) throws NumberFormatException, ParseException,IOException;
}
