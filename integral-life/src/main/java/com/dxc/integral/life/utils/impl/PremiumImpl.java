package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.utils.Premium;
import com.dxc.integral.life.utils.Prmpm01Util;
import com.dxc.integral.life.utils.Prmpm03Util;
import com.dxc.integral.life.utils.Prmpm04Util;
import com.dxc.integral.life.utils.Prmpm17Util;
import com.dxc.integral.life.utils.Prmpm18Util;
/* 
 * 
 * @author wli31
 *
 */
@Service("premium")
@Lazy
public class PremiumImpl implements Premium {
	@Autowired
	private Prmpm17Util prmpm17Util;
	@Autowired
	private Prmpm03Util prmpm03Util;
	@Autowired
	private Prmpm01Util prmpm01Util;
	@Autowired
	private Prmpm04Util prmpm04Util;
	@Autowired
	private Prmpm18Util prmpm18Util;
	
	@Override
	public PremiumDTO processPrmpm01(PremiumDTO premiumDTO){
		return prmpm01Util.processPrempm01(premiumDTO);
	}
	public PremiumDTO processPrmpm17(PremiumDTO premiumDTO){
		return prmpm17Util.processPrempm17(premiumDTO);
	}
	public PremiumDTO processPrmpm03(PremiumDTO premiumDTO) throws ParseException{
		return prmpm03Util.processPrempm03(premiumDTO);
	}
	@Override
	public PremiumDTO processPrmpm04(PremiumDTO premiumDTO) throws ParseException, IOException {
		return prmpm04Util.processPrempm04(premiumDTO);
	}
	@Override
	public PremiumDTO processPrmpm18(PremiumDTO premiumDTO) throws IOException {
		return prmpm18Util.processPrempm18(premiumDTO);
	}
}
