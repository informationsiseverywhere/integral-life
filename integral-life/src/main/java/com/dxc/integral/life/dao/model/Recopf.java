package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Recopf {

	private String chdrcoy = "";
	private String chdrnum = "";
	private String life = "";
	private String coverage = "";
	private String rider = "";
	public int planSuffix = 0;
	private String rasnum = "";
	public int seqno = 0;
	private String validflag = "";
	public int costdate = 0;
	private String retype = "";
	private String rngmnt = "";
	public BigDecimal sraramt;
	public BigDecimal raAmount;
	public int ctdate = 0;
	private String origcurr = "";
	public BigDecimal prem;
	public BigDecimal compay;
	public BigDecimal taxamt;
	public BigDecimal refundfe;
	private String batccoy = "";
	private String batcbrn = "";
	public int batcactyr = 0;
	public int batcactmn = 0;
	private String batctrcde = "";
	private String batcbatch = "";
	public int tranno = 0;
	private String rcstfrq = "";
	private String userProfile = "";
	private String jobName = "";
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getRasnum() {
		return rasnum;
	}
	public void setRasnum(String rasnum) {
		this.rasnum = rasnum;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getCostdate() {
		return costdate;
	}
	public void setCostdate(int costdate) {
		this.costdate = costdate;
	}
	public String getRetype() {
		return retype;
	}
	public void setRetype(String retype) {
		this.retype = retype;
	}
	public String getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(String rngmnt) {
		this.rngmnt = rngmnt;
	}
	public BigDecimal getSraramt() {
		return sraramt;
	}
	public void setSraramt(BigDecimal sraramt) {
		this.sraramt = sraramt;
	}
	public BigDecimal getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(BigDecimal raAmount) {
		this.raAmount = raAmount;
	}
	public int getCtdate() {
		return ctdate;
	}
	public void setCtdate(int ctdate) {
		this.ctdate = ctdate;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public BigDecimal getPrem() {
		return prem;
	}
	public void setPrem(BigDecimal prem) {
		this.prem = prem;
	}
	public BigDecimal getCompay() {
		return compay;
	}
	public void setCompay(BigDecimal compay) {
		this.compay = compay;
	}
	public BigDecimal getTaxamt() {
		return taxamt;
	}
	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}
	public BigDecimal getRefundfe() {
		return refundfe;
	}
	public void setRefundfe(BigDecimal refundfe) {
		this.refundfe = refundfe;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getRcstfrq() {
		return rcstfrq;
	}
	public void setRcstfrq(String rcstfrq) {
		this.rcstfrq = rcstfrq;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	
}
