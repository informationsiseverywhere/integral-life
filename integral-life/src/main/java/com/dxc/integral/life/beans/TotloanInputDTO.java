package com.dxc.integral.life.beans;

/**
 * Input DTO for Hrtotloan utility.
 * 
 * @author mmalik25
 */
public class TotloanInputDTO {

	/** The chdrcoy */
	private String chdrcoy;
	/** The chdrnum */
	private String chdrnum;
	/** The effectiveDate */
	private int effectiveDate;
	/** The batccoy */
	private String batccoy;
	/** The batcbrn */
	private String batcbrn;
	/** The batcactyr */
	private int batcactyr;
	/** The batcactmn */
	private int batcactmn;
	/** The batctrcde */
	private String batctrcde;
	/** The batcbatch */
	private String batcbatch;
	/** The tranno */
	private int tranno;
	/** The tranid */
	private String tranid;
	/** The tranTerm */
	private String tranTerm;
	/** The tranDate */
	private int tranDate;
	/** The tranTime */
	private int tranTime;
	/** The tranUser */
	private int tranUser;
	/** The language */
	private String language;
	/** The postFlag */
	private String postFlag;

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * @param chdrcoy
	 *            the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * @param chdrnum
	 *            the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * @return the effectiveDate
	 */
	public int getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(int effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the batccoy
	 */
	public String getBatccoy() {
		return batccoy;
	}

	/**
	 * @param batccoy
	 *            the batccoy to set
	 */
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	/**
	 * @return the batcbrn
	 */
	public String getBatcbrn() {
		return batcbrn;
	}

	/**
	 * @param batcbrn
	 *            the batcbrn to set
	 */
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	/**
	 * @return the batcactyr
	 */
	public int getBatcactyr() {
		return batcactyr;
	}

	/**
	 * @param batcactyr
	 *            the batcactyr to set
	 */
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}

	/**
	 * @return the batcactmn
	 */
	public int getBatcactmn() {
		return batcactmn;
	}

	/**
	 * @param batcactmn
	 *            the batcactmn to set
	 */
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}

	/**
	 * @return the batctrcde
	 */
	public String getBatctrcde() {
		return batctrcde;
	}

	/**
	 * @param batctrcde
	 *            the batctrcde to set
	 */
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	/**
	 * @return the batcbatch
	 */
	public String getBatcbatch() {
		return batcbatch;
	}

	/**
	 * @param batcbatch
	 *            the batcbatch to set
	 */
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	/**
	 * @return the tranno
	 */
	public int getTranno() {
		return tranno;
	}

	/**
	 * @param tranno
	 *            the tranno to set
	 */
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	/**
	 * @return the tranid
	 */
	public String getTranid() {
		return tranid;
	}

	/**
	 * @param tranid
	 *            the tranid to set
	 */
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}

	/**
	 * @return the tranTerm
	 */
	public String getTranTerm() {
		return tranTerm;
	}

	/**
	 * @param tranTerm
	 *            the tranTerm to set
	 */
	public void setTranTerm(String tranTerm) {
		this.tranTerm = tranTerm;
	}

	/**
	 * @return the tranDate
	 */
	public int getTranDate() {
		return tranDate;
	}

	/**
	 * @param tranDate
	 *            the tranDate to set
	 */
	public void setTranDate(int tranDate) {
		this.tranDate = tranDate;
	}

	/**
	 * @return the tranTime
	 */
	public int getTranTime() {
		return tranTime;
	}

	/**
	 * @param tranTime
	 *            the tranTime to set
	 */
	public void setTranTime(int tranTime) {
		this.tranTime = tranTime;
	}

	/**
	 * @return the tranUser
	 */
	public int getTranUser() {
		return tranUser;
	}

	/**
	 * @param tranUser
	 *            the tranUser to set
	 */
	public void setTranUser(int tranUser) {
		this.tranUser = tranUser;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the postFlag
	 */
	public String getPostFlag() {
		return postFlag;
	}

	/**
	 * @param postFlag
	 *            the postFlag to set
	 */
	public void setPostFlag(String postFlag) {
		this.postFlag = postFlag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TotloanInputDTO [chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", effectiveDate=" + effectiveDate
				+ ", batccoy=" + batccoy + ", batcbrn=" + batcbrn + ", batcactyr=" + batcactyr + ", batcactmn="
				+ batcactmn + ", batctrcde=" + batctrcde + ", batcbatch=" + batcbatch + ", tranno=" + tranno
				+ ", tranid=" + tranid + ", tranTerm=" + tranTerm + ", tranDate=" + tranDate + ", tranTime=" + tranTime
				+ ", tranUser=" + tranUser + ", language=" + language + ", postFlag=" + postFlag + "]";
	}

}
