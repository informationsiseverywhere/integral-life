package com.dxc.integral.life.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.life.beans.PremiumDTO;
import com.dxc.integral.life.dao.ClexpfDAO;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Clexpf;
import com.dxc.integral.life.dao.model.Lextpf;
import com.dxc.integral.life.dao.model.Lifepf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.smarttable.pojo.T5659;
import com.dxc.integral.life.smarttable.pojo.T5664;
import com.dxc.integral.life.smarttable.pojo.Th549;
import com.dxc.integral.life.smarttable.pojo.Th606;
import com.dxc.integral.life.smarttable.pojo.Th609;
import com.dxc.integral.life.smarttable.pojo.Tj698;
import com.dxc.integral.life.smarttable.pojo.Tj699;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("prmpm18Util")
public class Prmpm18Util {
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private ClexpfDAO clexpfDAO;
	@Autowired
	private LextpfDAO lextpfDAO;
	@Autowired
	private LifepfDAO lifepfDAO;
	
	private BigDecimal[] lextOppcs = new BigDecimal[8];
	private Integer[] lextZmortpcts = new Integer[8];
	private String[] lextOpcdas = new String[8];
	private int sub;
	private int agerateTot;
	private int ratesPerMillieTot = 0;
	private int adjustedAge;
	
	private int index;
	private BigDecimal discountAmt;
	private BigDecimal bap;
	private BigDecimal bip;
	private BigDecimal modalFactor;
	private BigDecimal rebatePcnt;
	private int ix;
	private String basicPremium = "";
	private String mortalityLoad = "";
	private BigDecimal staffDiscount;
	private String staffFlag = "";
	private BigDecimal roundNum;
	private BigDecimal roundDec;
	private BigDecimal round1;
	private BigDecimal round10;
	private BigDecimal round100;
	private T5659 t5659IO = null;
	private T5664 t5664IO = null;
	private Th609 th609IO = null;
	private Th549 th549IO = null;
	private Th606 th606IO = null;
	private Tj698 tj698IO = null;
	private Tj699 tj699IO = null;
	private String th606Indic = "";
	private BigDecimal mortRate;
	private BigDecimal mortFactor;
	private BigDecimal extLoading;
	private Iterator<Lextpf> lextpfIterator;
	private String t5659Key = "";
	
	private Chdrpf chdrlnb = null;
	private Clexpf clexpf = null;
	private List<Lextpf> lextpfList = null;
	
	public PremiumDTO processPrempm18(PremiumDTO premiumDTO) throws IOException {
		premiumDTO.setStatuz("****");
		basicPremium = "N";
		initialize(premiumDTO);
		
		if ("****".equals(premiumDTO.getStatuz())) {
			staffDiscount(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO); 
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(staffFlag) && staffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "B".equals(th609IO.getIndic())) {
			bap = new BigDecimal(100).subtract(staffDiscount).divide(new BigDecimal(100).multiply(bap)).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			bap = new BigDecimal(ratesPerMillieTot).add(bap).setScale(2,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rebateOnFrequency(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			if(t5664IO.getUnit() == 0) {
				premiumDTO.setStatuz("F110");
			}else {
				bap = bap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5664IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
			}
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			mortalityLoadings(premiumDTO);
			}
		if ("****".equals(premiumDTO.getStatuz())) {
			percentageLoadings();
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(staffFlag) && staffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "I".equals(th609IO.getIndic())) {
			bip = new BigDecimal(100).subtract(staffDiscount).divide(new BigDecimal(100)).multiply(bip).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		basicPremium = "Y";
		if ("****".equals(premiumDTO.getStatuz())) {
			basicAnnualPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(staffFlag) && staffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "B".equals(th609IO.getIndic())) {
			bap = new BigDecimal(100).subtract(staffDiscount).divide(new BigDecimal(100)).multiply(bap).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		
		if ("****".equals(premiumDTO.getStatuz())) {
			volumeDiscountBap(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rebateOnFrequency(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			
				bap = bap.multiply(premiumDTO.getSumin()).divide(new BigDecimal(t5664IO.getUnit())).setScale(2,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			instalmentPremium(premiumDTO);
		}
		if ("****".equals(premiumDTO.getStatuz()) && "Y".equals(staffFlag) && staffDiscount.compareTo(BigDecimal.ZERO) != 0
				&& "I".equals(th609IO.getIndic())) {
		
			bip = new BigDecimal(100).subtract(staffDiscount).divide(new BigDecimal(100)).multiply(bip).setScale(3,BigDecimal.ROUND_HALF_UP);
		}
		if ("****".equals(premiumDTO.getStatuz())) {
			rounding(premiumDTO);
		}
		premiumDTO.setCalcLoaPrem(premiumDTO.getCalcPrem().subtract(premiumDTO.getCalcBasPrem()).setScale(2,BigDecimal.ROUND_HALF_UP));
		return premiumDTO;
	}
	
	protected void rounding(PremiumDTO premiumDTO){
		roundNum = bip;
		round100 = new BigDecimal(formatRoundNum(roundNum).substring(8));
		round10 = new BigDecimal(formatRoundNum(roundNum).substring(9));
		round1 = new BigDecimal(formatRoundNum(roundNum).substring(10));
		roundDec = new BigDecimal(formatRoundNum(roundNum).substring(11));
		
		if (t5659IO.getRndfact() ==1  || t5659IO.getRndfact() == 0) {
			if (roundDec.compareTo(new BigDecimal(0.5)) == -1) {
				roundDec = BigDecimal.ZERO;
			}else {
				roundNum = roundNum.add(new BigDecimal(1));
				roundDec = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 10) {
			if (round1.compareTo(new BigDecimal(5)) == -1) {
				round1 = BigDecimal.ZERO;
			}else {
				roundNum = roundNum.add(new BigDecimal(10));
				round1 =  BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 100) {
			if (round10.compareTo(new BigDecimal(50)) == -1) {
				round10 = BigDecimal.ZERO;
			}else {
				roundNum = roundNum.add(new BigDecimal(100));
				round10 = BigDecimal.ZERO;
			}
		}
		if (t5659IO.getRndfact() == 1000) {
			if (round100.compareTo(new BigDecimal(500)) == -1) {
				round100 = BigDecimal.ZERO;
			}else {
				roundNum = roundNum.add(new BigDecimal(1000));
				round100 = BigDecimal.ZERO;
			}
		}
		
		bip = roundNum;
		if (t5664IO.getPremUnit() == 0) {
			premiumDTO.setCalcPrem(bip);
		}else {
			if ("Y".equals(basicPremium)) {
				premiumDTO.setCalcBasPrem(bip.divide(new BigDecimal(t5664IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}else {
				premiumDTO.setCalcPrem(bip.divide(new BigDecimal(t5664IO.getPremUnit())).setScale(2,BigDecimal.ROUND_HALF_UP));
			}
		}
	}
	protected void instalmentPremium(PremiumDTO premiumDTO) throws IOException{
		bip = BigDecimal.ZERO;
		modalFactor = BigDecimal.ZERO;
		String tj699Itemitem = premiumDTO.getCrtable().concat(premiumDTO.getMop());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TJ699", tj699Itemitem,
				CommonConstants.IT_ITEMPFX,itemFrm);
		if(itempf == null && "***".equals(premiumDTO.getCrtable())){
			premiumDTO.setStatuz("JL01");
			return;
		}else if(itempf == null){
			tj699Itemitem = "***".concat(premiumDTO.getMop());
			itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TJ699", tj699Itemitem,
					CommonConstants.IT_ITEMPFX,itemFrm);
			if(itempf == null){
				premiumDTO.setStatuz("JL01");
				return;
			}
		}
		tj699IO = new ObjectMapper().readValue(itempf.getGenareaj(), Tj699.class);
		for (ix = 0; ix < 12; ix++){
			if (premiumDTO.getBillfreq().equals(tj699IO.getFreqcys().get(ix))) {
				modalFactor = tj699IO.getZlfacts().get(ix);
				break;
			}
		}
		
		if (modalFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("JL00");
		}else {
			bip = bap.multiply(modalFactor).setScale(4,BigDecimal.ROUND_HALF_UP);
		}
		/*EXIT*/
	}
	protected void percentageLoadings(){
		for(sub = 0 ; sub < 8; sub++)
			if (lextOppcs[sub].compareTo(BigDecimal.ZERO) == -1) {
				bap = bap.multiply(lextOppcs[sub]).divide(new BigDecimal(100));
			}
	}
	protected void rebateOnFrequency(PremiumDTO premiumDTO) throws IOException{
		rebatePcnt = BigDecimal.ZERO;
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TJ698", premiumDTO.getCrtable(),
				CommonConstants.IT_ITEMPFX,itemFrm);
		if(itempf == null){
			return;
		}else{
			tj698IO = new ObjectMapper().readValue(itempf.getGenareaj(), Tj698.class);
		}
		ix = 0;
		for (ix = 0 ; ix < 12; ix++){
			if (premiumDTO.getBillfreq().equals(tj698IO.getFreqcys().get(ix))) {
				rebatePcnt = tj698IO.getZlfacts().get(ix);
				break;
			}
		}
		bap = new BigDecimal(100).subtract(rebatePcnt).multiply(bap).divide(new BigDecimal(100));
	}
	protected void volumeDiscountBap(PremiumDTO premiumDTO) throws IOException{
		String t5659Itemitem= t5664IO.getDisccntmeth().concat(premiumDTO.getCurrcode());
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "T5659", t5659Itemitem,
				CommonConstants.IT_ITEMPFX,premiumDTO.getRatingdate());
		if(itempf == null){
			premiumDTO.setStatuz("F265");
			return;
		}else{
			t5659IO = new ObjectMapper().readValue(itempf.getGenareaj(), T5659.class);
		}
		for(sub = 0; sub < 4 ; sub ++){
			if(premiumDTO.getSumin().compareTo(t5659IO.getVolbanfrs().get(sub)) == -1
					|| premiumDTO.getSumin().compareTo(t5659IO.getVolbantos().get(sub)) ==1){
				continue;
			}else{
				discountAmt = t5659IO.getVolbanles().get(sub);
				break;
			}
		}
		bap = bap.subtract(discountAmt).setScale(2,BigDecimal.ROUND_HALF_UP);
	}
	private void staffDiscount(PremiumDTO premiumDTO) throws IOException{
		chdrlnb = chdrpfDAO.readRecordOfChdr(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum());
		if(chdrlnb == null){
			throw new DataNoFoundException("Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table Chdrpf while processing contract ");
		}
		if (chdrlnb.getCownnum() == null ||"".equals(chdrlnb.getCownnum())) {
			return;
		}
		clexpf = clexpfDAO.getClexpfRecord(chdrlnb.getCownpfx(), chdrlnb.getCowncoy(), chdrlnb.getCownnum());
		if (null != clexpf && !"Y".equals(clexpf.getRstaflag()) && !"".equals(chdrlnb.getJownnum())){
			clexpf = clexpfDAO.getClexpfRecord(chdrlnb.getCownpfx(), chdrlnb.getCowncoy(), chdrlnb.getJownnum());
		}
		if (null != clexpf && "Y".equals(clexpf.getRstaflag())) {
			Itempf itempf = itempfDAO.readSmartTableByTrimItem(premiumDTO.getChdrChdrcoy(), "TH609", chdrlnb.getCnttype(),CommonConstants.IT_ITEMPFX);
			if(itempf != null){
				itempf = itempfDAO.readSmartTableByTrimItem(premiumDTO.getChdrChdrcoy(), "TH609", "***",CommonConstants.IT_ITEMPFX);
			}
			if(itempf == null){
				staffDiscount = BigDecimal.ZERO;
			}
			th609IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th609.class);
			staffDiscount = th609IO.getPrcnt();
		}
		staffFlag = clexpf == null ? "" : clexpf.getRstaflag();
	}
	protected void initialize(PremiumDTO premiumDTO) throws IOException{
		ratesPerMillieTot = 0;
		bap = BigDecimal.ZERO;
		bip = BigDecimal.ZERO;
		adjustedAge = 0;
		discountAmt = BigDecimal.ZERO;
		agerateTot = 0;
		sub = 0;
		t5659Key = "";
		/*BRD-306 START */
		/*BRD-306 END */
		for (int loopVar1 = 0; loopVar1 > 8; loopVar1++){
			lextOppcs[sub] = BigDecimal.ZERO;
			lextZmortpcts[sub] = 0;
			lextOpcdas[sub] = "";
		}
		
		String t5664Itemitem= premiumDTO.getCrtable().concat(String.valueOf(premiumDTO.getDuration())).concat(premiumDTO.getMortcls()).concat(premiumDTO.getLsex());
		int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
		Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "T5664", t5664Itemitem,
				CommonConstants.IT_ITEMPFX,itemFrm);
		if(itempf == null){
			premiumDTO.setStatuz("F265");
		}else{
			t5664IO = new ObjectMapper().readValue(itempf.getGenareaj(), T5664.class);
		}
	}
	
	protected void basicAnnualPremium(PremiumDTO premiumDTO){
		setupLextKey(premiumDTO);
		readLext(premiumDTO);

	}
	protected void setupLextKey(PremiumDTO premiumDTO){
		if( "Y".equals(basicPremium)) {
			adjustedAge = premiumDTO.getLage();
			checkT5664Insprm(premiumDTO);
		}
		lextpfList = lextpfDAO.getLextpfListRecord(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(),
				premiumDTO.getCovrCoverage(), premiumDTO.getCovrRider());
	}
	
	protected void readLext(PremiumDTO premiumDTO){
		if(lextpfList!=null && lextpfList.size() > 0){
			lextpfIterator = lextpfList.iterator();
        }
	    if (lextpfIterator == null || !lextpfIterator.hasNext()) {
			loopForAdjustedAge(premiumDTO);
		}
	    Lextpf lextIO = lextpfIterator.next();
	    if ("2".equals(premiumDTO.getReasind() )&& "1".equals(lextIO.getReasind())) {
	    	loopForAdjustedAge(premiumDTO);
		}
	    if (!"2".equals(premiumDTO.getReasind() )&& "2".equals(lextIO.getReasind())) {
	    	loopForAdjustedAge(premiumDTO);
		}
		
		if (lextIO.getEcesdte() <= premiumDTO.getReRateDate()) {
			readLext(premiumDTO);
			return ;
		}
		lextOppcs[sub] = lextIO.getOppc();
		lextZmortpcts[sub] = lextIO.getZmortpct();
		lextOpcdas[sub] = lextIO.getOpcda();
		ratesPerMillieTot += lextIO.getInsprm();
		agerateTot += lextIO.getAgerate();
		readLext(premiumDTO);
		sub++;
		return;
	}
	protected void loopForAdjustedAge(PremiumDTO premiumDTO){
		adjustedAge = agerateTot + premiumDTO.getLage();
	}
	
protected void checkT5664Insprm(PremiumDTO premiumDTO){
		
		if (adjustedAge < 0) {
			adjustedAge = 110;
		}
		if (adjustedAge < 0 || adjustedAge > 110) {
			premiumDTO.setStatuz("E107");
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate*/
		if (adjustedAge == 0) {
			if (t5664IO.getInsprem() == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				bap = new BigDecimal(t5664IO.getInsprem());
			}
			return ;
		}
		
		if (adjustedAge >= 100 && adjustedAge <= 110) {
			index = adjustedAge - 99;
			if (t5664IO.getInstprs().get(index) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				bap = new BigDecimal(t5664IO.getInstprs().get(index));
			}
		}
		else {
			if (t5664IO.getInsprms().get(adjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				bap = new BigDecimal(t5664IO.getInsprms().get(adjustedAge));
			}
		}
	}
	
	protected void mortalityLoadings(PremiumDTO premiumDTO) throws IOException{
		
		para(premiumDTO);
	}
	protected void para(PremiumDTO premiumDTO){
		mortalityLoad = "N";
		for (sub = 0; sub < 8 || "Y".equals(mortalityLoad); sub++){
			if (lextZmortpcts[sub] != 0) {
				mortalityLoad = "Y";
			}
		}
		if ("N".equals(mortalityLoad)) {
			return;
		}
		sub = 0;
	}
	
	
	
	protected void calcMortLoadings(PremiumDTO premiumDTO) throws  IOException{
		for(sub = 0 ; sub < 8; sub++){
			if (lextZmortpcts[sub] != 0){
				String th549Itemitem= premiumDTO.getCrtable().concat(String.valueOf(lextZmortpcts[sub])).concat(premiumDTO.getLsex());
				int itemFrm = premiumDTO.getRatingdate() == 0 ? 99999999:premiumDTO.getRatingdate();
				Itempf itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TH549", th549Itemitem,
						CommonConstants.IT_ITEMPFX,itemFrm);
				if(itempf == null){
					premiumDTO.setStatuz("HL27");
					return;
				}else{
					th549IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th549.class);
				}
				Lifepf lifepf = lifepfDAO.getLifeRecordByCurrfrom(premiumDTO.getChdrChdrcoy(), premiumDTO.getChdrChdrnum(), premiumDTO.getLifeLife(), premiumDTO.getLifeJlife(), 99999999);
				if(lifepf == null){
					throw new DataNoFoundException("Contract no: "+ premiumDTO.getChdrChdrnum()+"not found in Table LIFEPF while processing contract ");
				}
				th606Indic = "S".equals(lifepf.getSmoking())?th549IO.getIndcs().get(0):th549IO.getIndcs().get(1);
				String th606Itemitem = String.valueOf("00".concat(premiumDTO.getCrtable()).concat(th606Indic));
				itempf = itempfDAO.readSmartTableByTableNameAndItem3(premiumDTO.getChdrChdrcoy(), "TH606", th606Itemitem,
						CommonConstants.IT_ITEMPFX,itemFrm);
				if(itempf == null){
					premiumDTO.setStatuz("HL26");
					modalFactor =BigDecimal.ZERO;
					mortRate = BigDecimal.ZERO;
					callSubroutine(premiumDTO);
				}else{
					th606IO = new ObjectMapper().readValue(itempf.getGenareaj(), Th606.class);
				}
				getValues(premiumDTO);
			}
		}
	}
	protected void callSubroutine(PremiumDTO premiumDTO) throws IOException{
		for (int wsaaCount = 0; wsaaCount < 5 || "".equals(th549IO.getOpcdas().get(wsaaCount)); wsaaCount++){
			if (th549IO.getOpcdas().get(wsaaCount).equals(lextOpcdas[sub])) {
				if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPPMIL")){
					extLoading = mortRate.multiply(mortFactor).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000))).setScale(4);
				}
				if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPDLVL")){
					extLoading = mortFactor.multiply(new BigDecimal(100)).multiply(th549IO.getExpfactor()).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000)))
							.multiply(new BigDecimal(lextZmortpcts[sub]).divide(new BigDecimal(100))).setScale(5);
				}
				break;
			}
			else {
				if ("**".equals(th549IO.getOpcdas().get(wsaaCount))) {
					if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPPMIL")){
						extLoading = mortRate.multiply(mortFactor).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000))).setScale(4);
					}
					if(th549IO.getSubrtns().get(wsaaCount).equalsIgnoreCase("EXTPDLVL")){
						extLoading = mortFactor.multiply(new BigDecimal(100)).multiply(th549IO.getExpfactor()).multiply(premiumDTO.getSumin().divide(new BigDecimal(1000)))
								.multiply(new BigDecimal(lextZmortpcts[sub]).divide(new BigDecimal(100))).setScale(5);
					}
					break;
				}
			}
		}
		bap = bap.add(extLoading).setScale(2,BigDecimal.ROUND_HALF_UP);
		calcMortLoadings(premiumDTO);
	}
	
	protected void getValues(PremiumDTO premiumDTO){
		if (adjustedAge < 0) {
			adjustedAge = 110;
		}
		if (adjustedAge < 0 || adjustedAge > 110) {
			premiumDTO.setStatuz("E107");
			return;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (adjustedAge == 0) {
			if (th606IO.getInsprem() == 0) {
				premiumDTO.setStatuz("E107");
				return;
			}
			else {
				mortRate =  new BigDecimal(th606IO.getInsprem());
				getModalFactor(premiumDTO);
			}
		}
		
		if (adjustedAge >= 100 && adjustedAge <= 110) {
			index = adjustedAge - 99;
			if (th606IO.getInstprs().get(index) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				mortRate = new BigDecimal(th606IO.getInstprs().get(index));
			}
		}
		else {
			if (th606IO.getInsprms().get(adjustedAge) == 0) {
				premiumDTO.setStatuz("E107");
			}
			else {
				mortRate = new BigDecimal(th606IO.getInsprms().get(adjustedAge));
			}
		}
	}
	protected void getModalFactor(PremiumDTO premiumDTO){
		modalFactor = BigDecimal.ZERO;
		if ("01".equals(premiumDTO.getBillfreq())|| "00".equals(premiumDTO.getBillfreq())) {
			mortFactor = th606IO.getMfacty();
		}
		else {
			if ("02".equals(premiumDTO.getBillfreq())) {
				mortFactor = th606IO.getMfacthy();
			}
			else {
				if ("04".equals(premiumDTO.getBillfreq())) {
					mortFactor = th606IO.getMfactq();
				}
				else {
					if ("12".equals(premiumDTO.getBillfreq())) {
						mortFactor  = th606IO.getMfactm();
					}
					else {
						if ("13".equals(premiumDTO.getBillfreq())) {
							mortFactor = th606IO.getMfact4w();
						}
						else {
							if ("24".equals(premiumDTO.getBillfreq())) {
								mortFactor = th606IO.getMfacthm();
							}
							else {
								if ("26".equals(premiumDTO.getBillfreq())) {
									mortFactor = th606IO.getMfact2w();
								}
								else {
									if ("52".equals(premiumDTO.getBillfreq())) {
										mortFactor= th606IO.getMfactw();
									}
								}
							}
						}
					}
				}
			}
		}
		if (mortFactor.compareTo(BigDecimal.ZERO) == 0) {
			premiumDTO.setStatuz("F272");
		}
	}
	/*public static void goTo(final GOTOInterface aLabel) {
		GOTOException gotoExceptionInstance = new GOTOException(aLabel);
		gotoExceptionInstance.setNextMethod(aLabel);
		throw gotoExceptionInstance;
	}*/
	private String formatRoundNum(BigDecimal obj) {
		DecimalFormat df=new DecimalFormat("0000000000000.00");
		String str2=df.format(obj);
		return str2;
	}
}
