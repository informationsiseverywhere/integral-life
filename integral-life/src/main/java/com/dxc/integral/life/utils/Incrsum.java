package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.IncrsumDTO;

public interface Incrsum {
	
	public void calcIncrPrem(IncrsumDTO incrsumDTO) throws IOException, ParseException;

}
