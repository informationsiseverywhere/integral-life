package com.dxc.integral.life.smarttable.pojo;

import java.util.List;
/** 
 * @author wli31
 *
 */
public class T6629{
  	private List<String> agebands;
  	private String agntstat;
  	private String govtstat;
  	private List<String> prmbands;
  	private List<String> rskbands;
  	private List<String> sumbands;
	public List<String> getAgebands() {
		return agebands;
	}
	public void setAgebands(List<String> agebands) {
		this.agebands = agebands;
	}
	public String getAgntstat() {
		return agntstat;
	}
	public void setAgntstat(String agntstat) {
		this.agntstat = agntstat;
	}
	public String getGovtstat() {
		return govtstat;
	}
	public void setGovtstat(String govtstat) {
		this.govtstat = govtstat;
	}
	public List<String> getPrmbands() {
		return prmbands;
	}
	public void setPrmbands(List<String> prmbands) {
		this.prmbands = prmbands;
	}
	public List<String> getRskbands() {
		return rskbands;
	}
	public void setRskbands(List<String> rskbands) {
		this.rskbands = rskbands;
	}
	public List<String> getSumbands() {
		return sumbands;
	}
	public void setSumbands(List<String> sumbands) {
		this.sumbands = sumbands;
	}
}