package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.CashedInputDTO;
import com.dxc.integral.fsu.beans.CashedOutputDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.TotloanInputDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.utils.Paydebts;

/**
 * The Paydebts utility implementation.
 * 
 * This program will use the Payments due from the Anticipated Endowment Release
 * to pay the existing Loans.
 * 
 * @author mmalik25
 *
 */
@Service("paydebts")
@Lazy
public class PaydebtsImpl implements Paydebts {

	@Autowired
	private LoanpfDAO loanpfDAO;

	@Autowired
	private HrtotlonImpl hrtotlon;

	@Autowired
	private ChdrpfDAO chdrpfDAO;

	@Autowired
	private LoanpaymtImpl loanPaymt;

	@Autowired
	private LifacmvImpl lifacmv;

	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/
	
	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Paydebts#payOffDebt(com.dxc.integral.life.beans.ZrcshopDTO)
	 */
	@Override/*ILIFE-5977*/
	public void payOffDebt(ZrcshopDTO zrcshopDTO) throws IOException,ParseException {//ILIFE-5763

		T5645 t5645IO = getT5645Info(zrcshopDTO.getChdrcoy());/*ILIFE-5977*/  /*IJTI-379*/

		Loanpf loanpf = loanpfDAO.getLastLoanNumberByCompanyAndContractNumber(zrcshopDTO.getChdrcoy(),
				zrcshopDTO.getChdrnum());
		if (loanpf == null) {
			return;
		}
		Chdrpf chdrpf = chdrpfDAO.readRecordOfChdrpf(zrcshopDTO.getChdrcoy(), zrcshopDTO.getChdrnum());
		callHrtotloan(zrcshopDTO, chdrpf);/*ILIFE-5977*/

		BigDecimal contractAmt = zrcshopDTO.getPymt();
		CashedInputDTO cashedInputDTO = populatedCashedDTO(zrcshopDTO, loanpf.getLoancurr());
		CashedOutputDTO cashedOutputDTO = new CashedOutputDTO();
		for (int count = 0; count < 4 || contractAmt == BigDecimal.ZERO; count++) {
			cashedInputDTO.setGenlAccount(t5645IO.getGlmaps().get(count));
			cashedInputDTO.setSign(t5645IO.getSigns().get(count));
			cashedInputDTO.setSacscode(t5645IO.getSacscodes().get(count));
			cashedInputDTO.setSacstyp(t5645IO.getSacstypes().get(count));
			cashedInputDTO.setOrigamt(zrcshopDTO.getPymt());
			cashedOutputDTO = loanPaymt.processLoanPayment(chdrpf.getCnttype(), cashedInputDTO);/*ILIFE-5977*/
			contractAmt = cashedOutputDTO.getDocamt();
		}
		int sequenceNo = cashedOutputDTO.getTranseq();
		BigDecimal loanPaid = zrcshopDTO.getPymt().subtract(contractAmt);
		if (loanPaid.compareTo(BigDecimal.ZERO) > 0) {
			processCashAccount(zrcshopDTO, sequenceNo, loanPaid, loanpf.getLoancurr(),
					loanpf.getLoannumber().toString());/*ILIFE-5977*/
		}
	}

	/**
	 * Calls Hrtotlon utility.
	 * 
	 * @param zrcshopDTO
	 * @param chdrpf
	 * @throws IOException
	 *//*ILIFE-5977*/
	private void callHrtotloan(ZrcshopDTO zrcshopDTO, Chdrpf chdrpf) throws IOException,ParseException {//ILIFE-5763

		TotloanInputDTO totloanDTO = new TotloanInputDTO();
		totloanDTO.setChdrcoy(zrcshopDTO.getChdrcoy());
		totloanDTO.setChdrnum(zrcshopDTO.getChdrnum());
		totloanDTO.setEffectiveDate(zrcshopDTO.getEffdate());
		totloanDTO.setTranno(zrcshopDTO.getTranno());
		totloanDTO.setBatccoy(zrcshopDTO.getBatccoy());
		totloanDTO.setBatcbrn(zrcshopDTO.getBatcbrn());
		totloanDTO.setBatcactyr(zrcshopDTO.getBatcactyr());
		totloanDTO.setBatcactmn(zrcshopDTO.getBatcactmn());
		totloanDTO.setBatctrcde(zrcshopDTO.getBatctrcde());
		totloanDTO.setBatcbatch(zrcshopDTO.getBatcbatch());
		totloanDTO.setTranTerm(zrcshopDTO.getTermid());
		totloanDTO.setTranDate(Integer.valueOf(DatimeUtil.getCurrentDate()));
		totloanDTO.setTranTime(999999);
		totloanDTO.setTranUser(zrcshopDTO.getUser());
		totloanDTO.setLanguage(zrcshopDTO.getLanguage());
		totloanDTO.setPostFlag("Y");
		hrtotlon.processLoanList(totloanDTO, chdrpf);/*ILIFE-5977*/
	}

	/**
	 * Process Cash Account.
	 * 
	 * @param zrcshopDTO
	 * @param sequenceNo
	 * @param loanPaid
	 * @param loancurr
	 * @param loannum
	 * @throws IOException
	 *//*ILIFE-5977*/
	private void processCashAccount(ZrcshopDTO zrcshopDTO, int sequenceNo, BigDecimal loanPaid, String loancurr,
			String loannum) throws IOException,ParseException {//ILIFE-5763

		LifacmvDTO lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setBatccoy(zrcshopDTO.getBatccoy());
		lifacmvDTO.setBatcbrn(zrcshopDTO.getBatcbrn());
		lifacmvDTO.setBatcactyr(zrcshopDTO.getBatcactyr());
		lifacmvDTO.setBatcactmn(zrcshopDTO.getBatcactmn());
		lifacmvDTO.setBatctrcde(zrcshopDTO.getBatctrcde());
		lifacmvDTO.setBatcbatch(zrcshopDTO.getBatcbatch());
		lifacmvDTO.setRldgcoy(zrcshopDTO.getChdrcoy());
		lifacmvDTO.setGenlcoy(zrcshopDTO.getChdrcoy());
		lifacmvDTO.setRdocnum(zrcshopDTO.getChdrnum());
		lifacmvDTO.setGenlcur("");
		lifacmvDTO.setOrigcurr(loancurr);
		lifacmvDTO.setOrigamt(loanPaid);
		lifacmvDTO.setJrnseq(sequenceNo + 1);
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setCrate(BigDecimal.ZERO);
		lifacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifacmvDTO.setContot(0);
		lifacmvDTO.setTranno(zrcshopDTO.getTranno());
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setEffdate(zrcshopDTO.getEffdate());
		lifacmvDTO.setTranref(zrcshopDTO.getChdrnum());
		lifacmvDTO.setTrandesc(zrcshopDTO.getTrandesc());
		lifacmvDTO.setRldgacct(zrcshopDTO.getChdrnum() + loannum);
		lifacmvDTO.setTermid(zrcshopDTO.getTermid());
		lifacmvDTO.setTransactionDate(Integer.valueOf(DatimeUtil.getCurrentDate()));
		lifacmvDTO.setTransactionTime(999999);
		lifacmvDTO.setUser(zrcshopDTO.getUser());
		lifacmvDTO.setSacscode(zrcshopDTO.getSacscode());
		lifacmvDTO.setSacstyp(zrcshopDTO.getSacstyp());
		lifacmvDTO.setGlcode(zrcshopDTO.getGlcode());
		lifacmvDTO.setGlsign(zrcshopDTO.getSign());
		lifacmv.executePSTW(lifacmvDTO);/*ILIFE-5977*/

	}

	/**
	 * Populate CashedInputDTO.
	 * 
	 * @param zrcshopDTO
	 * @param loancurr
	 * @return
	 */
	private CashedInputDTO populatedCashedDTO(ZrcshopDTO zrcshopDTO, String loancurr) {

		CashedInputDTO cashedDTO = new CashedInputDTO();
		cashedDTO.setGenlCompany(zrcshopDTO.getChdrcoy());
		cashedDTO.setGenlCurrency("");
		cashedDTO.setBatcpfx(zrcshopDTO.getBatcpfx());
		cashedDTO.setBatcactmn(zrcshopDTO.getBatcactmn());
		cashedDTO.setBatcactyr(zrcshopDTO.getBatcactyr());
		cashedDTO.setBatcbatch(zrcshopDTO.getBatcbatch());
		cashedDTO.setBatcbrn(zrcshopDTO.getBatcbrn());
		cashedDTO.setBatctrcde(zrcshopDTO.getBatctrcde());
		cashedDTO.setBatccoy(zrcshopDTO.getBatccoy());
		cashedDTO.setDoctNumber(zrcshopDTO.getChdrnum());
		cashedDTO.setDoctCompany(zrcshopDTO.getBatccoy());
		cashedDTO.setTrandate(zrcshopDTO.getEffdate());
		cashedDTO.setContot(0);
		cashedDTO.setTranseq(1);
		cashedDTO.setAcctamt(BigDecimal.ZERO);
		cashedDTO.setOrigccy(loancurr);
		cashedDTO.setDissrate(0);
		cashedDTO.setTrandesc(zrcshopDTO.getTrandesc());
		cashedDTO.setChdrcoy(zrcshopDTO.getChdrcoy());
		cashedDTO.setChdrnum(zrcshopDTO.getChdrnum());
		cashedDTO.setTranno(zrcshopDTO.getTranno());
		cashedDTO.setTrandate(Integer.valueOf(DatimeUtil.getCurrentDate()));
		String tranPrefix = "  ";
		cashedDTO.setTrankey(tranPrefix + zrcshopDTO.getChdrcoy() + zrcshopDTO.getChdrnum());
		cashedDTO.setLanguage(zrcshopDTO.getLanguage());
		return cashedDTO;
	}

	/**
	 * Gets T5645 smart table item.
	 * 
	 * @return
	 * @throws IOException
	 */
	private T5645 getT5645Info(String company) throws IOException {/*ILIFE-5977*/  /*IJTI-379*/
		Map<String, List<Itempf>> t5645Map=itempfDAO.readSmartTableByTableName(company, "T5645", CommonConstants.IT_ITEMPFX);/*IJTI-379*/ 
		List<Itempf> t5645ItemList = t5645Map.get("PAYDEBTS");
		return smartTableDataUtils.convertGenareaToPojo(t5645ItemList.get(0).getGenareaj(), T5645.class); /*IJTI-398*/
	}
}
