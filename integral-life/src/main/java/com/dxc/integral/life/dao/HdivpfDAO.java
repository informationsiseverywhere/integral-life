package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Hdivpf;

/**
 * HdivpfDAO for database table <b>Hdivpf</b> DB operations.
 * 
 * @author yyang21
 *
 */
public interface HdivpfDAO {

	List<Hdivpf> readHdivpfRecords(String company, String contractNumber);
	public List<Hdivpf> readHdivpfRecords(String company, String chdrnum, int tranno);
	public int deleteHdivpfRecord(Hdivpf hdivpf);
	public void insertHdivpfRecords(List<Hdivpf> hdivpfList);
	public void insertHdivpfRecords(Hdivpf hdivpfList);
}
