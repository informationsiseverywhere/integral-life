
package com.dxc.integral.life.dao;



import java.util.List;

import com.dxc.integral.life.dao.model.Comgwpf;

public interface ComgwpfDAO  {

	 List<Comgwpf> searchComgwpfRecord(List<String> agncnum);
	 void insertIntoComgwpf(List<Comgwpf> comgwpfList);
	 void updateExprtflg(List<String> agncnum);
	 Comgwpf checkDuplRecord(List<String> checkList);

}
