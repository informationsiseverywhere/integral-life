package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for Paydebts utility.
 * 
 * @author mmalik25
 *
 */
public class ZrcshopDTO {

	/** The effdate */
	private Integer effdate;
	/** The chdrcoy */
	private String chdrcoy;
	/** The chdrnum */
	private String chdrnum;
	/** The paycurr */
	private String paycurr;
	/** The cntcurr */
	private String cntcurr;
	/** The pymt */
	private BigDecimal pymt;
	/** The tranno */
	private Integer tranno;
	/** The trandesc */
	private String trandesc;
	/** The bankkey */
	private String bankkey;
	/** The bankacckey */
	private String bankacckey;
	/** The frmGlDet */
	private String frmGlDet;
	/** The sacscode */
	private String sacscode;
	/** The sacstyp */
	private String sacstyp;
	/** The glcode */
	private String glcode;
	/** The sign */
	private String sign;
	/** The cnttot */
	private Integer cnttot;
	/** The termid */
	private String termid;
	/** The user */
	private Integer user;
	/** The reqntype */
	private String reqntype;
	/** The clntcoy */
	private String clntcoy;
	/** The clntnum */
	private String clntnum;
	/** The bankcode */
	private String bankcode;
	/** The cheqno */
	private String cheqno;
	/** The batcpfx */
	private String batcpfx;
	/** The batccoy */
	private String batccoy;
	/** The batcbrn */
	private String batcbrn;
	/** The batcactyr */
	private Integer batcactyr;
	/** The batcactmn */
	private Integer batcactmn;
	/** The batctrcde */
	private String batctrcde;
	/** The batcbatch */
	private String batcbatch;
	/** The tranref */
	private String tranref;
	/** The errorFormat */
	private String errorFormat;
	/** The language */
	private String language;

	/**
	 * @return the effdate
	 */
	public Integer getEffdate() {
		return effdate;
	}

	/**
	 * @param effdate
	 *            the effdate to set
	 */
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * @param chdrcoy
	 *            the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * @param chdrnum
	 *            the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * @return the paycurr
	 */
	public String getPaycurr() {
		return paycurr;
	}

	/**
	 * @param paycurr
	 *            the paycurr to set
	 */
	public void setPaycurr(String paycurr) {
		this.paycurr = paycurr;
	}

	/**
	 * @return the cntcurr
	 */
	public String getCntcurr() {
		return cntcurr;
	}

	/**
	 * @param cntcurr
	 *            the cntcurr to set
	 */
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	/**
	 * @return the pymt
	 */
	public BigDecimal getPymt() {
		return pymt;
	}

	/**
	 * @param pymt
	 *            the pymt to set
	 */
	public void setPymt(BigDecimal pymt) {
		this.pymt = pymt;
	}

	/**
	 * @return the tranno
	 */
	public Integer getTranno() {
		return tranno;
	}

	/**
	 * @param tranno
	 *            the tranno to set
	 */
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	/**
	 * @return the trandesc
	 */
	public String getTrandesc() {
		return trandesc;
	}

	/**
	 * @param trandesc
	 *            the trandesc to set
	 */
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}

	/**
	 * @return the bankkey
	 */
	public String getBankkey() {
		return bankkey;
	}

	/**
	 * @param bankkey
	 *            the bankkey to set
	 */
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}

	/**
	 * @return the bankacckey
	 */
	public String getBankacckey() {
		return bankacckey;
	}

	/**
	 * @param bankacckey
	 *            the bankacckey to set
	 */
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}

	/**
	 * @return the frmGlDet
	 */
	public String getFrmGlDet() {
		return frmGlDet;
	}

	/**
	 * @param frmGlDet
	 *            the frmGlDet to set
	 */
	public void setFrmGlDet(String frmGlDet) {
		this.frmGlDet = frmGlDet;
	}

	/**
	 * @return the sacscode
	 */
	public String getSacscode() {
		return sacscode;
	}

	/**
	 * @param sacscode
	 *            the sacscode to set
	 */
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}

	/**
	 * @return the sacstyp
	 */
	public String getSacstyp() {
		return sacstyp;
	}

	/**
	 * @param sacstyp
	 *            the sacstyp to set
	 */
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}

	/**
	 * @return the glcode
	 */
	public String getGlcode() {
		return glcode;
	}

	/**
	 * @param glcode
	 *            the glcode to set
	 */
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}

	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * @param sign
	 *            the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * @return the cnttot
	 */
	public Integer getCnttot() {
		return cnttot;
	}

	/**
	 * @param cnttot
	 *            the cnttot to set
	 */
	public void setCnttot(Integer cnttot) {
		this.cnttot = cnttot;
	}

	/**
	 * @return the termid
	 */
	public String getTermid() {
		return termid;
	}

	/**
	 * @param termid
	 *            the termid to set
	 */
	public void setTermid(String termid) {
		this.termid = termid;
	}

	/**
	 * @return the user
	 */
	public Integer getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(Integer user) {
		this.user = user;
	}

	/**
	 * @return the reqntype
	 */
	public String getReqntype() {
		return reqntype;
	}

	/**
	 * @param reqntype
	 *            the reqntype to set
	 */
	public void setReqntype(String reqntype) {
		this.reqntype = reqntype;
	}

	/**
	 * @return the clntcoy
	 */
	public String getClntcoy() {
		return clntcoy;
	}

	/**
	 * @param clntcoy
	 *            the clntcoy to set
	 */
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}

	/**
	 * @return the clntnum
	 */
	public String getClntnum() {
		return clntnum;
	}

	/**
	 * @param clntnum
	 *            the clntnum to set
	 */
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}

	/**
	 * @return the bankcode
	 */
	public String getBankcode() {
		return bankcode;
	}

	/**
	 * @param bankcode
	 *            the bankcode to set
	 */
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	/**
	 * @return the cheqno
	 */
	public String getCheqno() {
		return cheqno;
	}

	/**
	 * @param cheqno
	 *            the cheqno to set
	 */
	public void setCheqno(String cheqno) {
		this.cheqno = cheqno;
	}

	/**
	 * @return the batcpfx
	 */
	public String getBatcpfx() {
		return batcpfx;
	}

	/**
	 * @param batcpfx
	 *            the batcpfx to set
	 */
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}

	/**
	 * @return the batccoy
	 */
	public String getBatccoy() {
		return batccoy;
	}

	/**
	 * @param batccoy
	 *            the batccoy to set
	 */
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	/**
	 * @return the batcbrn
	 */
	public String getBatcbrn() {
		return batcbrn;
	}

	/**
	 * @param batcbrn
	 *            the batcbrn to set
	 */
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	/**
	 * @return the batcactyr
	 */
	public Integer getBatcactyr() {
		return batcactyr;
	}

	/**
	 * @param batcactyr
	 *            the batcactyr to set
	 */
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}

	/**
	 * @return the batcactmn
	 */
	public Integer getBatcactmn() {
		return batcactmn;
	}

	/**
	 * @param batcactmn
	 *            the batcactmn to set
	 */
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}

	/**
	 * @return the batctrcde
	 */
	public String getBatctrcde() {
		return batctrcde;
	}

	/**
	 * @param batctrcde
	 *            the batctrcde to set
	 */
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	/**
	 * @return the batcbatch
	 */
	public String getBatcbatch() {
		return batcbatch;
	}

	/**
	 * @param batcbatch
	 *            the batcbatch to set
	 */
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	/**
	 * @return the tranref
	 */
	public String getTranref() {
		return tranref;
	}

	/**
	 * @param tranref
	 *            the tranref to set
	 */
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}

	/**
	 * @return the errorFormat
	 */
	public String getErrorFormat() {
		return errorFormat;
	}

	/**
	 * @param errorFormat
	 *            the errorFormat to set
	 */
	public void setErrorFormat(String errorFormat) {
		this.errorFormat = errorFormat;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ZrcshopDTO [effdate=" + effdate + ", chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", paycurr="
				+ paycurr + ", cntcurr=" + cntcurr + ", pymt=" + pymt + ", tranno=" + tranno + ", trandesc=" + trandesc
				+ ", bankkey=" + bankkey + ", bankacckey=" + bankacckey + ", frmGlDet=" + frmGlDet + ", sacscode="
				+ sacscode + ", sacstyp=" + sacstyp + ", glcode=" + glcode + ", sign=" + sign + ", cnttot=" + cnttot
				+ ", termid=" + termid + ", user=" + user + ", reqntype=" + reqntype + ", clntcoy=" + clntcoy
				+ ", clntnum=" + clntnum + ", bankcode=" + bankcode + ", cheqno=" + cheqno + ", batcpfx=" + batcpfx
				+ ", batccoy=" + batccoy + ", batcbrn=" + batcbrn + ", batcactyr=" + batcactyr + ", batcactmn="
				+ batcactmn + ", batctrcde=" + batctrcde + ", batcbatch=" + batcbatch + ", tranref=" + tranref
				+ ", errorFormat=" + errorFormat + ", language=" + language + "]";
	}

}
