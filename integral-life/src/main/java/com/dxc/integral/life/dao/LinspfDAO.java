package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Linspf;

public interface LinspfDAO  {
    public void insertLinspfRecords(List<Linspf> linsList);
    public void updateLinspfRecords(List<Linspf> linsList);
    List<Linspf> searchLinspfRecords(String chdrcoy, String chdrnum);
    public Linspf readLinspfRecord(String chdrcoy,String chdrnum, int instFrom);
    public List<Linspf> searchLinsrevRecords(String chdrcoy, String chdrnum);
    public int deleteLinspf(long unique_number);
}