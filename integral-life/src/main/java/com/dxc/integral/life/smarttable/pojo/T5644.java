package com.dxc.integral.life.smarttable.pojo;


public class T5644 {

  	private String compysubr;
  	private String subrev;
	public String getSubrev() {
		return subrev;
	}
	public void setSubrev(String subrev) {
		this.subrev = subrev;
	}
	public String getCompysubr() {
		return compysubr;
	}
	public void setCompysubr(String compysubr) {
		this.compysubr = compysubr;
	}
}
