package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
/* 
 * 
 * @author wli31
 *
 */
public class Racdpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String rasnum;
	private Integer tranno;
	private Integer seqno;
	private String validflag;
	private String currcode;
	private Integer currfrom;
	private Integer currto;
	private String retype;
	private String rngmnt;
	private BigDecimal raamount;
	private Integer ctdate;
	private Integer cmdate;
	private BigDecimal reasper;
	private Integer rrevdt;
	private BigDecimal recovamt;
	private String cestype;
	private String ovrdind;
	private String lrkcls;
	private String usrprf;
	private String jobnm;
	private String datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getRasnum() {
		return rasnum;
	}
	public void setRasnum(String rasnum) {
		this.rasnum = rasnum;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public Integer getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	public Integer getCurrto() {
		return currto;
	}
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}
	public String getRetype() {
		return retype;
	}
	public void setRetype(String retype) {
		this.retype = retype;
	}
	public String getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(String rngmnt) {
		this.rngmnt = rngmnt;
	}
	public BigDecimal getRaamount() {
		return raamount;
	}
	public void setRaamount(BigDecimal raamount) {
		this.raamount = raamount;
	}
	public Integer getCtdate() {
		return ctdate;
	}
	public void setCtdate(Integer ctdate) {
		this.ctdate = ctdate;
	}
	public Integer getCmdate() {
		return cmdate;
	}
	public void setCmdate(Integer cmdate) {
		this.cmdate = cmdate;
	}
	public BigDecimal getReasper() {
		return reasper;
	}
	public void setReasper(BigDecimal reasper) {
		this.reasper = reasper;
	}
	public Integer getRrevdt() {
		return rrevdt;
	}
	public void setRrevdt(Integer rrevdt) {
		this.rrevdt = rrevdt;
	}
	public BigDecimal getRecovamt() {
		return recovamt;
	}
	public void setRecovamt(BigDecimal recovamt) {
		this.recovamt = recovamt;
	}
	public String getCestype() {
		return cestype;
	}
	public void setCestype(String cestype) {
		this.cestype = cestype;
	}
	public String getOvrdind() {
		return ovrdind;
	}
	public void setOvrdind(String ovrdind) {
		this.ovrdind = ovrdind;
	}
	public String getLrkcls() {
		return lrkcls;
	}
	public void setLrkcls(String lrkcls) {
		this.lrkcls = lrkcls;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}


}
