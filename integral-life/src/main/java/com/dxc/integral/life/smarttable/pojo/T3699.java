/**
 * 
 */
package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

/**
 * @author fwang3
 *
 */
public class T3699 {
	private List<String> daywhs;

	public List<String> getDaywhs() {
		return daywhs;
	}

	public void setDaywhs(List<String> daywhs) {
		this.daywhs = daywhs;
	}

}
