package com.dxc.integral.life.utils;

import java.io.IOException;

import com.dxc.integral.life.beans.TxcalcDTO;

public interface Servtax {
	public TxcalcDTO processServtax(TxcalcDTO txcalcDTO)throws IOException;
}
