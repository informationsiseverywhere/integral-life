package com.dxc.integral.life.smarttable.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tjl29 {
	private String cmgwgndr;

	public String getCmgwgndr() {
		return cmgwgndr;
	}
	public void setCmgwgndr(String cmgwgndr) {
		this.cmgwgndr = cmgwgndr;
	}
}
