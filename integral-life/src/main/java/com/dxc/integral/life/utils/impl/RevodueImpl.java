package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon1;
import com.dxc.integral.life.beans.GreversDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.ReverseDTO;
import com.dxc.integral.life.dao.AgcmpfDAO;
import com.dxc.integral.life.dao.ArcmpfDAO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.HcsdpfDAO;
import com.dxc.integral.life.dao.HpuapfDAO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.PrmhpfDAO;
import com.dxc.integral.life.dao.model.AcmvrevcpPojo;
import com.dxc.integral.life.dao.model.Agcmpf;
import com.dxc.integral.life.dao.model.Arcmpf;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.dao.model.Hcsdpf;
import com.dxc.integral.life.dao.model.Hpuapf;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.dao.model.Payrpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5671;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Acmvrevcp;
import com.dxc.integral.life.utils.Datcon4;
import com.dxc.integral.life.utils.Grevutrn;
import com.dxc.integral.life.utils.Hrevdiv;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Revodue;


@Service("revodue")
@Lazy
public class RevodueImpl implements Revodue{

	@Autowired
	private Datcon1 datcon1;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private HpuapfDAO hpuapfDAO;
	@Autowired
	private AcmvpfDAO acmvpfDAO;
	@Autowired
	private ArcmpfDAO arcmpfDAO;
	@Autowired
	private AgcmpfDAO agcmpfDAO;
	@Autowired
	private LoanpfDAO loanpfDAO;
	@Autowired
	private PayrpfDAO payrpfDAO;
	@Autowired
	private HcsdpfDAO hcsdpfDAO;
	@Autowired
	private PrmhpfDAO prmhpfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private Hrevdiv hrevdiv;
	@Autowired
	private Grevutrn grevutrn;
	@Autowired
	private Lifacmv lifacmv;
	@Autowired
	private Acmvrevcp acmvrevcp;
	@Autowired
	private Datcon4 datcon4;
	
	private String wsaaRevTrandesc="";
	private String wsaaLoanFlag="";
	private String wsaaRldgLoanno="";
	private String wsaaRldgRest="";
	private String wsaaLoanLoanno="";
	private String wsaaIoCall="";
	private String wsaaRldgacct = "";
	private String wsaaSacscode = "";
	private String wsaaSacstype = "";
	private int wsaaLstInterestDate = 0;
	private int wsaaReversedTranno = 0;
	private int wsaaLoanDate = 0;
	private int wsaaEffdate = 0;
	private int wsaaContractDate=0;
	private String wsaaRldgChdrnum = "";
	private String wsaaAcmvTrcde = "";
	private int wsaaNxtInterestDate=0;
	private int wsaaNewDate=0;
	private int wsaaDayCheck=0;
	private int wsaaMonthCheck=0;
	private Chdrpf chdrpf;
	private T5671 t5671IO;

	public ReverseDTO processRevodue(ReverseDTO reverseDTO) throws IOException, ParseException{

		Integer.parseInt(datcon1.todaysDate());
		Descpf descpf = descpfDAO.getDescInfo(reverseDTO.getCompany(), "T1688", reverseDTO.getBatctrcde(),
				reverseDTO.getLanguage());

		if (descpf == null) {
			reverseDTO.setStatuz("BOMB");
			return reverseDTO;
		}
		wsaaRevTrandesc = descpf.getLongdesc();
		wsaaLoanFlag = "N";
		
		if(!processAll(reverseDTO)) {
			reverseDTO.setStatuz("BOMB");
			return reverseDTO;
		}

		
		if(reverseDTO.getStatuz().equals("BOMB")) {
			return reverseDTO;
		}
		
		return reverseDTO;
	}
	
	private boolean processAll(ReverseDTO reverseDTO) throws IOException, ParseException{
		if(!processChdrs(reverseDTO)) {
			return false;
		}
		if(!processCovrs(reverseDTO)) {
			return false;
		}
		if(!processAcmvs(reverseDTO)) {
			 return false;
		 }
		if(!processAcmvOptical(reverseDTO)){
			 return false;
		 }
		if(!processAgcms(reverseDTO)){
			 return false;
		 }
		if ("Y".equals(wsaaLoanFlag)) {
			processLoan(reverseDTO);
		}
		if(!reinstateLoans(reverseDTO)) {
			return false;
		}
		if(!processPayrs(reverseDTO)) {
			return false;
		}
		if(!processHcsds(reverseDTO)) {
			return false;
		}
		if(!processPrmhact(reverseDTO)) {
			return false;
		}
		return true;
	}
	

	private boolean processChdrs(ReverseDTO reverseDTO) {
		/* delete valid flag 1 record */
		chdrpf = chdrpfDAO.getLPContractInfo(reverseDTO.getCompany(), reverseDTO.getChdrnum());
	    boolean erorflag = false;
		if (chdrpf == null) {
			return false;
		}
		wsaaReversedTranno = chdrpf.getTranno().intValue();
		
		if(chdrpfDAO.deleteChdrpfRecord(chdrpf.getChdrcoy(), chdrpf.getChdrnum(), chdrpf.getValidflag(), chdrpf.getServunit(), chdrpf.getTranno().intValue())<=0) {
			return false;
		}
		
		
		/* Select the next record which will be the valid flag 2 record */
	
        chdrpf = chdrpfDAO.getLPContractWithoutValidflag(reverseDTO.getCompany(), reverseDTO.getChdrnum());
        if (chdrpf == null || !chdrpf.getValidflag().equals("2")) {
        	return false;
        }       
        chdrpf.setValidflag("1");
	    if(chdrpfDAO.updateValidChdrpfRecord(chdrpf.getValidflag(), chdrpf.getChdrcoy(), chdrpf.getChdrnum(), chdrpf.getTranno().intValue())<=0) {
			return false;
	    }

		return true;
	}
	
	private boolean processCovrs(ReverseDTO reverseDTO) throws IOException, ParseException {
		List<Covrpf> list = covrpfDAO.readCoverageRecordsByCompanyAndContractNumber(reverseDTO.getCompany(), reverseDTO.getChdrnum());
		if(list == null || list.size() == 0) {
			return true;
		}
		Iterator<Covrpf> covrItor = list.iterator();
		
		Covrpf covrpf = covrItor.next();
		
		if(covrpf.getTranno().intValue()==reverseDTO.getTranno()) {
			
			if(!deleteComp(covrpf)) {
				return false;
			}
			if(!covrItor.hasNext()) {
				return false;
			}
			
			if(!updateComp(covrItor.next())) {
				return false;
			}
			
			if(!h100ProcessHpua(reverseDTO, covrpf)) {
				 return false;
			 }
			 
			
		}
		
		
		return true;
		
		
	  
		
	}
	
	private boolean deleteComp(Covrpf covrpf) {
		if(!covrpf.getValidflag().equals("1")) {
			return false;
	    }
		if(covrpfDAO.deleteCovrpfRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlnsfx())<=0) {
			return false;
		}
		
		return true;
		
	}
	
	private boolean updateComp(Covrpf covrpf) {
		if(!covrpf.getValidflag().equals("2")) {
			return false;
		}
		covrpf.setValidflag("1");
        covrpf.setCurrto(99999999);
	   if(covrpfDAO.updateFlagAndCurtoForCovrpf(covrpf)<=0) {
		   return false;  
	   }
		return true;
	}
	
	private boolean h100ProcessHpua(ReverseDTO reverseDTO, Covrpf covrpf) {
		boolean correctflag = true;
		List<Hpuapf> list = hpuapfDAO.getHpuapfRecords(reverseDTO.getCompany(), reverseDTO.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), 
				covrpf.getPlnsfx(), reverseDTO.getTranno());

		if(list == null || list.size()==0) {
			  return false;  
		}
		
		for(Hpuapf hpuapf : list) {
			correctflag = h170DeleteUpdateHpua(hpuapf);
			if(!correctflag) break;
			correctflag = processGenericSubr(reverseDTO, covrpf);
			if(!correctflag) break;
		}
		
		return correctflag;
		
	}
 
 private boolean h170DeleteUpdateHpua(Hpuapf hpuapf) {
	 if(hpuapfDAO.deleteHpuapfRecord(hpuapf)<=0) {
		 return false;
	 }
	 
	 Hpuapf preHpuapf = hpuapfDAO.getHpuaList(hpuapf.getChdrcoy(), hpuapf.getChdrnum(), hpuapf.getLife(), hpuapf.getCoverage(), hpuapf.getRider(), hpuapf.getPlanSuffix(), hpuapf.getPuAddNbr());
	 if(preHpuapf==null) {
		 return true;
	 }else {
		if("2".equals(preHpuapf.getValidflag())) {
			preHpuapf.setValidflag("1");
			if(hpuapfDAO.updateHpuapf(preHpuapf)<=0) {
				return false;
			}
		} 
	 }
	 
	 
	 return true;
 }
 
 private boolean processGenericSubr(ReverseDTO reverseDTO, Covrpf covrpf) {
	 boolean correctflag = true;
	 TableItem<T5671> t5671 =itempfService.readSmartTableByTableNameAndItem(reverseDTO.getCompany(), "T5671",reverseDTO.getOldBatctrcde().concat(covrpf.getCrtable()), CommonConstants.IT_ITEMPFX, T5671.class);
     if(t5671 == null ) {
    	 return correctflag;
     }
     t5671IO = t5671.getItemDetail();
     GreversDTO greversDTO = new GreversDTO();
     greversDTO.setChdrcoy(covrpf.getChdrcoy());
     greversDTO.setChdrnum(covrpf.getChdrnum());
     greversDTO.setTranno(reverseDTO.getTranno());
     greversDTO.setPlanSuffix(covrpf.getPlnsfx());
     greversDTO.setLife(covrpf.getLife());
     greversDTO.setCoverage(covrpf.getCoverage());
     greversDTO.setRider(covrpf.getRider());
     greversDTO.setBatckey(reverseDTO.getBatchkey());
     greversDTO.setTermid(reverseDTO.getTermid());
     greversDTO.setUser(reverseDTO.getUser());
     greversDTO.setNewTranno(reverseDTO.getNewTranno());
     greversDTO.setTransDate(reverseDTO.getTransDate());
     greversDTO.setTransTime(reverseDTO.getTransTime());
     greversDTO.setLanguage(reverseDTO.getLanguage());
     for (int i =0; i<=3; i++){
    	 greversDTO.setStatuz("****");
    	 if (!"HREVDIV".equalsIgnoreCase(t5671IO.getTrevsubs().get(i).trim())) {
    		 greversDTO = hrevdiv.processHrevdiv(greversDTO);
 		}else if (!"GREVUTRN".equalsIgnoreCase(t5671IO.getTrevsubs().get(i).trim())) {
   	  	     greversDTO = grevutrn.processGrevutrn(greversDTO);
		}
 		if (!"****".equals(greversDTO.getStatuz())) {
 			correctflag = false;
 			break;
 		}
	}
     
     return correctflag;

 }
 
 private boolean processAcmvs(ReverseDTO reverseDTO) throws IOException, ParseException{
   
	 List<Acmvpf> list = acmvpfDAO.getAcmvpfList(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getTranno());
	 if(list != null && list.size() >=0) {
		 for(Acmvpf acmvpf : list) {
			
			if(!reverseAcmvs(acmvpf, reverseDTO)) {
				return false;
			}

		 }
	 }
			
	 return true;
 }
 
 private boolean reverseAcmvs(Acmvpf acmvpf, ReverseDTO reverseDTO) throws IOException, ParseException{
	 if(!acmvpf.getBatctrcde().equals(reverseDTO.getOldBatctrcde())) {
			return true;
		}
		if (acmvpf.getRcamt().compareTo(BigDecimal.ZERO)==0) {
			return true;
		}
		
		LifacmvDTO lifacmvDTO  = new LifacmvDTO();
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setContot(0);
		lifacmvDTO.setFrcdate(0);
		lifacmvDTO.setTransactionDate(0);
		lifacmvDTO.setTransactionTime(0);
		lifacmvDTO.setUser(0);
		lifacmvDTO.setBatckey(reverseDTO.getBatchkey());
		lifacmvDTO.setRdocnum(acmvpf.getRdocnum());
		lifacmvDTO.setTranno(reverseDTO.getNewTranno());
		lifacmvDTO.setSacscode(acmvpf.getSacscode());
		lifacmvDTO.setSacstyp(acmvpf.getSacstyp());
		lifacmvDTO.setGlcode(acmvpf.getGlcode());
		lifacmvDTO.setGlsign(acmvpf.getGlsign());
		lifacmvDTO.setJrnseq(acmvpf.getJrnseq());
		lifacmvDTO.setRldgcoy(acmvpf.getRldgcoy());
		lifacmvDTO.setGenlcoy(acmvpf.getGenlcoy());
		lifacmvDTO.setRldgacct(acmvpf.getRldgacct());
		lifacmvDTO.setOrigcurr(acmvpf.getOrigcurr());
		lifacmvDTO.setAcctamt(acmvpf.getAcctamt().multiply(new BigDecimal(-1)));
		lifacmvDTO.setOrigamt(acmvpf.getOrigamt().multiply(new BigDecimal(-1)));
		lifacmvDTO.setGenlcur(acmvpf.getGenlcur());
		lifacmvDTO.setCrate(acmvpf.getCrate());
		lifacmvDTO.setPostyear("    ");
		lifacmvDTO.setPostyear("  ");
		lifacmvDTO.setTranref(acmvpf.getTranref());
		lifacmvDTO.setTrandesc(wsaaRevTrandesc);
		lifacmvDTO.setEffdate(acmvpf.getEffdate());
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setTermid(reverseDTO.getTermid());
		lifacmvDTO.setUser(reverseDTO.getUser());
		lifacmvDTO.setTransactionTime(reverseDTO.getTransTime());
		lifacmvDTO.setTransactionDate(reverseDTO.getTransDate());
		
		lifacmv.executePSTW(lifacmvDTO);
		
		if (!"".equals(acmvpf.getRldgacct().trim())) {
			wsaaRldgacct=acmvpf.getRldgacct();
		}
		
		if (!"".equals(wsaaRldgLoanno.trim()) && "".equals(wsaaRldgRest.trim())) {
			wsaaLoanLoanno=wsaaRldgLoanno;
			wsaaLoanFlag="Y";
		}
		if ("CP".equals(wsaaIoCall)) {
			AcmvrevcpPojo acmvrevcpPojo = new AcmvrevcpPojo();
			acmvrevcpPojo.setUnique_number(acmvpf.getUnique_number());
			acmvrevcpPojo.setAcctamt(acmvpf.getAcctamt());
			acmvrevcpPojo.setBatcactmn(acmvpf.getBatcactmn());
			acmvrevcpPojo.setBatcactyr(acmvpf.getBatcactyr());
			acmvrevcpPojo.setBatcbatch(acmvpf.getBatcbatch());
			acmvrevcpPojo.setBatcbrn(acmvpf.getBatcbrn());
			acmvrevcpPojo.setBatccoy(acmvpf.getBatccoy());
			acmvrevcpPojo.setBatctrcde(acmvpf.getBatctrcde());
			acmvrevcpPojo.setCrate(acmvpf.getCrate());
			acmvrevcpPojo.setCreddte(acmvpf.getCreddte());
			acmvrevcpPojo.setDateto(acmvpf.getDateto());
			acmvrevcpPojo.setEffdate(acmvpf.getEffdate());
			acmvrevcpPojo.setFrcdate(acmvpf.getFrcdate());
			acmvrevcpPojo.setGenlcoy(acmvpf.getGenlcoy());
			acmvrevcpPojo.setGenlcur(acmvpf.getGenlcur());
			acmvrevcpPojo.setGlcode(acmvpf.getGlcode());
			acmvrevcpPojo.setGlsign(acmvpf.getGlsign());
			acmvrevcpPojo = acmvrevcp.processAcmvrevcp(acmvrevcpPojo);
			if(!acmvrevcpPojo.getStatuz().equals("****") || !acmvrevcpPojo.getStatuz().equals("BOMB")) {
				return false;
			}
		}
		return true;
 }
	
 private boolean processAcmvOptical(ReverseDTO reverseDTO) throws IOException, ParseException{
	 int wsbbPeriod = 0;
	 int wsaaPeriod = 0;
	 Arcmpf arcmpf = arcmpfDAO.getArcmpfRecords("ACMV");
	 if(arcmpf == null) {
		 wsbbPeriod =0;
	 }else {
		 wsbbPeriod = Integer.parseInt(arcmpf.getAcctyr()+""+arcmpf.getAcctmnth());
	 }
	 
	 wsaaPeriod = Integer.parseInt(reverseDTO.getPtrnBatcactyr()+""+reverseDTO.getPtrnBatcactmn());

	 if(wsaaPeriod > wsbbPeriod) {
		 return true;
	 }
	 
	 AcmvrevcpPojo acmvrevcpPojo = new AcmvrevcpPojo();
	 acmvrevcpPojo.setRldgcoy(reverseDTO.getCompany());
	 acmvrevcpPojo.setRdocnum(reverseDTO.getChdrnum());
	 acmvrevcpPojo.setTranno(reverseDTO.getTranno());
	 acmvrevcpPojo.setBatctrcde(reverseDTO.getOldBatctrcde()); 
	 acmvrevcpPojo.setBatcactyr(reverseDTO.getPtrnBatcactyr());
	 acmvrevcpPojo.setBatcactmn(reverseDTO.getPtrnBatcactmn());
     wsaaIoCall = "CP";
     acmvrevcpPojo.setFunction("BEGN");
     acmvrevcpPojo = acmvrevcp.processAcmvrevcp(acmvrevcpPojo);
 	if (!"****".equals(acmvrevcpPojo.getStatuz()) && !"ENDP".equals(acmvrevcpPojo.getStatuz())) {
     return false;
     }
 	Acmvpf acmvpf = new Acmvpf();
 	acmvpf.setAcctamt(acmvrevcpPojo.getAcctamt());
 	acmvpf.setBatcactmn(acmvrevcpPojo.getBatcactmn());
 	acmvpf.setBatcactyr(acmvrevcpPojo.getBatcactyr());
 	acmvpf.setBatcbatch(acmvrevcpPojo.getBatcbatch());
 	acmvpf.setBatcbrn(acmvrevcpPojo.getBatcbrn());
 	acmvpf.setBatccoy(acmvrevcpPojo.getBatccoy());
 	acmvpf.setBatctrcde(acmvrevcpPojo.getBatctrcde());
 	acmvpf.setRcamt(acmvrevcpPojo.getRcamt());
 	acmvpf.setUnique_number(acmvrevcpPojo.getUnique_number());
 	acmvpf.setRdocnum(acmvrevcpPojo.getRdocnum());
 	acmvpf.setSacscode(acmvrevcpPojo.getSacscode());
	acmvpf.setSacstyp(acmvrevcpPojo.getSacstyp());
	acmvpf.setGlcode(acmvrevcpPojo.getGlcode());
	acmvpf.setGlsign(acmvrevcpPojo.getGlsign());
	acmvpf.setJrnseq(acmvrevcpPojo.getJrnseq());
	acmvpf.setRldgacct(acmvrevcpPojo.getRldgacct());
	acmvpf.setRldgcoy(acmvrevcpPojo.getRldgcoy());
	acmvpf.setGenlcoy(acmvrevcpPojo.getGenlcoy());
	acmvpf.setOrigcurr(acmvrevcpPojo.getOrigcurr());
	acmvpf.setOrigamt(acmvrevcpPojo.getOrigamt());
	acmvpf.setGenlcur(acmvrevcpPojo.getGenlcur());
	acmvpf.setCrate(acmvrevcpPojo.getCrate());
	acmvpf.setTranref(acmvrevcpPojo.getTranref());
	acmvpf.setEffdate(acmvrevcpPojo.getEffdate());
	acmvpf.setCreddte(acmvrevcpPojo.getCreddte());
	acmvpf.setDateto(acmvrevcpPojo.getDateto());
	acmvpf.setFrcdate(acmvrevcpPojo.getFrcdate());
	
 	if(!reverseAcmvs(acmvpf, reverseDTO)) return false;
 	/* again with a function of CLOSE.                                 */
 	
 	if(!reverseDTO.getCompany().equals(acmvpf.getRldgcoy()) 
 			|| !reverseDTO.getChdrnum().equals(acmvpf.getRdocnum())
 			|| reverseDTO.getTranno() != acmvpf.getTranno()) {
 		acmvrevcpPojo.setFunction("CLOSE");;
 		acmvrevcp.processAcmvrevcp(acmvrevcpPojo);
 		if(!acmvrevcpPojo.getStatuz().equals("****") || !acmvrevcpPojo.getStatuz().equals("BOMB")) {
			return false;
		}
 	}
		
	 return true;
 }
 
 private boolean processAgcms(ReverseDTO reverseDTO) {

	 List<Agcmpf> list = agcmpfDAO.getAgcmrvsRecord(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getTranno());
	 if(list != null && list.size() !=0) {
		 for(Agcmpf agcmpf : list) {
			 if(!reverseAgcm(agcmpf)) return false;;
		 }
	 }
	return true;
 }
 
 private boolean reverseAgcm(Agcmpf agcmpf) {
	 if(agcmpfDAO.deleteAgcmpfRecord(agcmpf)==0) {
		 return false;
	 }
	 
	 List<Agcmpf> list = agcmpfDAO.getAgcmpfRecord(agcmpf);
	
	 if(list == null || list.size()==0) {
		 return false;
	 }
	 
	 for(Agcmpf agcmdbc : list) {
		 if(!agcmdbc.getValidflag().trim().equals("2")) {
			 continue;
		 }else {
			 agcmdbc.setValidflag("1");
			 agcmdbc.setCurrto(99999999);
			 agcmpfDAO.getAgcmpfRecordByAgntnum(agcmdbc);
		 }
	 }
	 
	 return true;
 }
 
 private void processLoan(ReverseDTO reverseDTO) throws ParseException{
	 loanpfDAO.deleteLoadnpf(reverseDTO.getCompany(), reverseDTO.getChdrnum(), Integer.parseInt(wsaaLoanLoanno));
	 
 }

 private boolean reinstateLoans(ReverseDTO reverseDTO) {
	 List<Loanpf> list = loanpfDAO.getLoanenqList(reverseDTO.getCompany(), reverseDTO.getChdrnum());
	 if(list !=null && list.size()>0) {
		 
		 TableItem<T5645> t5645 =itempfService.readSmartTableByTableNameAndItem(reverseDTO.getCompany(), "T5645","REVODUE", CommonConstants.IT_ITEMPFX, T5645.class);
	     if(t5645 == null ) {
	    	 return false;
	     }
	     T5645 t5645IO = t5645.getItemDetail(); 
	    
	     
		 for(Loanpf loanpf : list) {
			 if(wsaaReversedTranno == loanpf.getLtranno()) {
				 
				 if("P".equals(loanpf.getLoantype())) {
			    	 wsaaSacscode = t5645IO.getSacscodes().get(0);
			    	 wsaaSacstype = t5645IO.getSacstypes().get(0);
			     }else {
			    	 wsaaSacscode = t5645IO.getSacscodes().get(1);
			    	 wsaaSacstype = t5645IO.getSacstypes().get(1);
			     }
					wsaaRldgacct="";
					wsaaLstInterestDate=0;
					wsaaRldgChdrnum = loanpf.getChdrnum();
					wsaaRldgLoanno = loanpf.getLoannumber().intValue()+""; 
					searchAcmvRecs(loanpf, reverseDTO);
					if (wsaaLstInterestDate==0) {
						wsaaLstInterestDate=loanpf.getLoanstdate().intValue();
					}
				
					 
					if(!calcNextInterestDate(loanpf, reverseDTO))
						return false;
					loanpf.setLstintbdte(wsaaLstInterestDate);
					loanpf.setNxtintbdte(wsaaNxtInterestDate);
					loanpf.setValidflag("1");
					loanpf.setLtranno(0);
					
					loanpfDAO.updateLoanenq(loanpf);
			 }
		 }
	 }
	 return true;
 }
 
 
 private void searchAcmvRecs(Loanpf loanpf, ReverseDTO reverseDTO) {
	 List<Acmvpf> acmvpfList = acmvpfDAO.getAcmvpfList(reverseDTO.getCompany(), loanpf.getChdrnum());
	 if(acmvpfList == null || acmvpfList.size()==0) {
  	   return ;
     }
	 for(Acmvpf acmvpf: acmvpfList) {
		 wsaaAcmvTrcde=acmvpf.getBatctrcde();
		 if (acmvpf.getTranno()>reverseDTO.getTranno() && wsaaSacscode.equals(acmvpf.getSacscode()) && wsaaSacstype.equals(acmvpf.getSacstyp())
                && wsaaRldgacct.equals(acmvpf.getRldgacct()) && ("BA69".equals(wsaaAcmvTrcde) || "BA68".equals(wsaaAcmvTrcde))){
			 wsaaLstInterestDate = acmvpf.getEffdate();
			 break;
		 }
				
	 }
 }
 
 private boolean calcNextInterestDate(Loanpf loanpf, ReverseDTO reverseDTO) {
	 
	 TableItem<T6633> t6633 =itempfService.readItemByItemFrm(reverseDTO.getCompany(), "T6633",chdrpf.getCnttype(), CommonConstants.IT_ITEMPFX, wsaaLstInterestDate, T6633.class);
	 if(t6633 == null ) {
    	 return false;
     }
	T6633 t6633IO = t6633.getItemDetail();
	
    wsaaLoanDate=loanpf.getLoanstdate().intValue();
	wsaaEffdate=wsaaLstInterestDate;
	wsaaContractDate=chdrpf.getOccdate().intValue();
	
	int intDate2=0;
	int intDate1=0;
	if ("Y".equals(t6633IO.getLoanAnnivInterest())) {
		
      intDate1=loanpf.getLoanstdate().intValue();
		while ( intDate2<=wsaaEffdate) {
			intDate2 = datcon4.getIntDate2("01", String.valueOf(intDate1), 1);
			intDate1 = intDate2;
		}

		wsaaNxtInterestDate=intDate2;
		return true;
	}
	if ("Y".equals(t6633IO.getPolicyAnnivInterest())) {
		intDate1=chdrpf.getOccdate().intValue();
		while ( intDate2<=wsaaEffdate) {
			intDate2 = datcon4.getIntDate2("01", String.valueOf(intDate1), 1);
			intDate1 = intDate2;
		}
		wsaaNxtInterestDate=intDate2;
		return true;
	}

		
	wsaaNewDate=0;
	/* check if table T6633 has a fixed day of the month specified     */
	/* ...if not, use the Loan day                                     */
	wsaaDayCheck = t6633IO.getInterestDay().intValue();
	wsaaMonthCheck = Integer.parseInt(String.valueOf(wsaaLoanDate).substring(4, 6));
    int wsaaNewDay = 0;
	if (wsaaDayCheck>=0 && wsaaDayCheck<=28) {
		wsaaNewDay = dateSet();
	}
	else {
		if (t6633IO.getInterestDay().intValue()!=0) {
			wsaaNewDay=t6633IO.getInterestDay().intValue();
		}
		else {
			wsaaNewDay=Integer.parseInt(String.valueOf(wsaaLoanDate).substring(6, 8));
		}
	}
	wsaaNewDate = Integer.parseInt(String.valueOf(wsaaLoanDate).substring(0, 6).concat(String.valueOf(wsaaNewDay)));

	String frequency = "";
	if ("".equals(t6633IO.getInterestFrequency().trim())) {
		frequency="01";
	}
	else {
		frequency=t6633IO.getInterestFrequency();
	}
	
	
	intDate1 = wsaaNewDate;
	while ( intDate1 <= wsaaEffdate ) {
		intDate2 = datcon4.getIntDate2(frequency, String.valueOf(intDate1), 1);
		intDate1 = intDate2;
	}

	wsaaNxtInterestDate=intDate2;
	return true;
 }
 
 protected int dateSet()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon4     */
		/*  with is a valid from-date... ie IF the interest/capn day in    */
		/*  T6633 is > 28, we have to make sure the from-date isn't        */
		/*  something like 31/02/nnnn or 31/06/nnnn                        */
		if (wsaaMonthCheck == 1
		|| wsaaMonthCheck == 3
		|| wsaaMonthCheck == 5
		|| wsaaMonthCheck == 7
		|| wsaaMonthCheck == 8
		|| wsaaMonthCheck == 10
		|| wsaaMonthCheck == 12) {
			return wsaaDayCheck;
		}
		if (wsaaMonthCheck == 4
		|| wsaaMonthCheck == 6
		|| wsaaMonthCheck == 9
		|| wsaaMonthCheck == 11) {
			if (wsaaDayCheck == 31) {
				return 30;
			}
			else {
				return wsaaDayCheck;
			}
		}
		if (wsaaMonthCheck==2) {
			return 28;
		}
		return 0;
		/*EXIT*/
	}
 
 private boolean processPayrs(ReverseDTO reverseDTO){
		/* Read PAYR file for the validflag '1' record.                    */
	    Payrpf payrpf = payrpfDAO.getPayrRecord(reverseDTO.getCompany(), reverseDTO.getChdrnum());
		
		if (payrpf == null) {
			return false;
		}
		/* If the record was not created by the transaction being          */
		/* reversed, then there are no PAYR records to reverse.            */
		if (payrpf.getTranno().intValue()!= reverseDTO.getTranno()) {
			return true;
		}
		if(payrpfDAO.deletePayrpfRecord(payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getPayrseqno())<=0) {
			return false;
		}
		
		payrpf = payrpfDAO.getPayrRecord(reverseDTO.getCompany(), reverseDTO.getChdrnum(), 1);
		
		if(payrpf==null || !("2").equals(payrpf.getValidflag())){
			return false;
		}
		payrpf.setValidflag("1");
		if(payrpfDAO.updatePayrpfRecord(payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getPayrseqno(), payrpf.getValidflag())<=0) {
			return false;
		}
		return true;
	}
 
 private boolean processHcsds(ReverseDTO reverseDTO){

		/* Read HCSD file for the validflag '1' record.                    */
	    Hcsdpf hcsdpf = hcsdpfDAO.readHcsdpfRecord(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getLife(), reverseDTO.getCoverage(), 
	    		reverseDTO.getRider(), reverseDTO.getPlanSuffix());
		if(hcsdpf == null) {
			return true;
		}
		
		if (!"1".equals(hcsdpf.getValidflag())) {
			return false;
		}
		/* If the record was not created by the transaction being          */
		/* reversed, then there are no HCSD records to reverse.            */
		if (reverseDTO.getTranno() != hcsdpf.getTranno().intValue()) {
			return true;
		}
	
		if(hcsdpfDAO.deleteHcsdpfRecord(hcsdpf.getChdrcoy(), hcsdpf.getChdrnum(), hcsdpf.getLife(), hcsdpf.getCoverage(), hcsdpf.getRider(), hcsdpf.getPlnsfx())<=0) {
			return false;
		}
		
		hcsdpf = hcsdpfDAO.readHcsdpfRecordsWithoutFlag(reverseDTO.getCompany(), reverseDTO.getChdrnum());
		
		if (hcsdpf == null || !"2".equals(hcsdpf.getValidflag())){
			return false;
		}
				
		
		hcsdpf.setValidflag("1");
		/* MOVE REWRT                  TO HCSD-FUNCTION.        <LA3993>*/
		if(hcsdpfDAO.updateHcsdpfRecord("1", hcsdpf.getChdrcoy(), hcsdpf.getChdrnum(), hcsdpf.getLife(), hcsdpf.getCoverage(), hcsdpf.getRider(), hcsdpf.getPlnsfx())<=0) {
			return false;
		}
	  return true;
	}
 
 private boolean processPrmhact(ReverseDTO reverseDTO) {
	 
	 if(prmhpfDAO.deletePrmhpf(reverseDTO.getCompany(), reverseDTO.getChdrnum(), reverseDTO.getTranno())<=0) {
		 return false;
	 }
	return true;
	}
 


}
