package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.dao.LoanpfDAO;
import com.dxc.integral.life.dao.model.Loanpf;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Crtloan;
import com.dxc.integral.life.utils.Lifacmv;

/**
 * The Crtloan utility implementation.
 * 
 * @author vhukumagrawa
 *
 */
@Service("crtloan")
@Lazy
public class CrtloanImpl implements Crtloan {

	public static final String DAY28 = "28";
	public static final String DAY29 = "29";
	public static final String DAY30 = "30";
	public static final String DAY31 = "31";
	private static final Collection<Month> MONTH_WITH_30DAYS = Arrays.asList(Month.APRIL, Month.JUNE, Month.SEPTEMBER,
			Month.NOVEMBER);

	@Autowired
	private LoanpfDAO loanDAO;

	@Autowired
	private Datcon2 datcon2;

	@Autowired
	private Lifacmv lifacmv;

	@Autowired
	private Xcvrt xcvrt;
	
	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Crtloan#createLoan(com.dxc.integral.life.beans.CrtloanDTO)
	 */
	@Override/*ILIFE-5977*/
	public int createLoan(CrtloanDTO crtloanDto) throws IOException,ParseException {//ILIFE-5763
		Loanpf loanpf = createLoanRecord(crtloanDto);/*ILIFE-5977*/
		String rldgacct = crtloanDto.getChdrnum();
		if(!"A".equals(loanpf.getLoantype()) && !"D".equals(loanpf.getLoantype())){
			if (loanpf.getLoannumber() <10){
				rldgacct+= "0"+ Integer.toString(loanpf.getLoannumber());
			} else {
				rldgacct+= Integer.toString(loanpf.getLoannumber());
			}
		} 
		LifacmvDTO lifacmvdto = prepareLifacmvDTO(crtloanDto, rldgacct);/*ILIFE-5977*/
		postAcmvpf(crtloanDto, lifacmvdto);/*ILIFE-5977*/
		postPrincipal(crtloanDto, lifacmvdto, loanpf);/*ILIFE-5977*/
		return crtloanDto.getLoanno();
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Crtloan#createLoanNPSTW(com.dxc.integral.life.beans.CrtloanDTO)
	 */
	@Override
	public int createLoanNPSTW(CrtloanDTO crtloanDto) throws IOException {
		createLoanRecord(crtloanDto);/*ILIFE-5977*/
		return crtloanDto.getLoanno();
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Crtloan#createLoanPSLN(com.dxc.integral.life.beans.CrtloanDTO)
	 */
	@Override/*ILIFE-5977*/
	public int createLoanPSLN(CrtloanDTO crtloanDto) throws IOException,ParseException {//ILIFE-5763
		Loanpf loanpf = createLoanRecord(crtloanDto);/*ILIFE-5977*/
		String rldgacct = crtloanDto.getChdrnum();
		if(!"A".equals(loanpf.getLoantype()) && !"D".equals(loanpf.getLoantype())){
			if (loanpf.getLoannumber() <10){
				rldgacct+= "0"+ Integer.toString(loanpf.getLoannumber());
			} else {
				rldgacct+= Integer.toString(loanpf.getLoannumber());
			}
		} 
		LifacmvDTO lifacmvdto = prepareLifacmvDTO(crtloanDto, rldgacct);/*ILIFE-5977*/
		postPrincipal(crtloanDto, lifacmvdto, loanpf);/*ILIFE-5977*/
		return crtloanDto.getLoanno();
	}

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.utils.Crtloan#createLoanPSTW(com.dxc.integral.life.beans.CrtloanDTO)
	 */
	@Override/*ILIFE-5977*/
	public int createLoanPSTW(CrtloanDTO crtloanDto) throws IOException,ParseException {//ILIFE-5763
		Loanpf loanpf = createLoanRecord(crtloanDto);/*ILIFE-5977*/
		String rldgacct = crtloanDto.getChdrnum();
		if(!"A".equals(loanpf.getLoantype()) && !"D".equals(loanpf.getLoantype())){
			if (loanpf.getLoannumber() <10){
				rldgacct+= "0"+ Integer.toString(loanpf.getLoannumber());
			} else {
				rldgacct+= Integer.toString(loanpf.getLoannumber());
			}
		} 
		LifacmvDTO lifacmvdto = prepareLifacmvDTO(crtloanDto, rldgacct);/*ILIFE-5977*/
		postAcmvpf(crtloanDto, lifacmvdto);/*ILIFE-5977*/
		postPrincipal(crtloanDto, lifacmvdto, loanpf);/*ILIFE-5977*/
		return crtloanDto.getLoanno();
	}

	/**
	 * Creates loan record by inserting into loanpf table.
	 * 
	 * @param crtloanDto
	 * @return
	 * @throws IOException
	 */
	private Loanpf createLoanRecord(CrtloanDTO crtloanDto) throws IOException {/*ILIFE-5977*/
		Map<String, Itempf> t6633Map=itempfDAO.readSmartTableByTableName2(crtloanDto.getChdrcoy(), "T6633", CommonConstants.IT_ITEMPFX);  /*IJTI-379*/
		Loanpf loanpf = loanDAO.getLastLoanNumberByCompanyAndContractNumber(crtloanDto.getChdrcoy(), crtloanDto.getChdrnum());
		if (loanpf != null) {
			loanpf.setLoannumber(loanpf.getLoannumber() + 1);
			crtloanDto.setLoanno(loanpf.getLoannumber());
		} else {
			loanpf = new Loanpf();
			loanpf.setChdrcoy(crtloanDto.getChdrcoy());
			loanpf.setChdrnum(crtloanDto.getChdrnum());
			loanpf.setLoannumber(1);
		}

		String key = crtloanDto.getCnttype().concat(crtloanDto.getLoantype());
		T6633 t6633 = smartTableDataUtils.convertGenareaToPojo(t6633Map.get(key).getGenareaj(), T6633.class); /*IJTI-398*/
		loanpf.setNxtcapdate(calculateCapitalizationDate(crtloanDto.getOccdate(), crtloanDto.getEffdate(), t6633));
		loanpf.setNxtintbdte(calculateNextIntBillDate(crtloanDto.getOccdate(), crtloanDto.getEffdate(), t6633));
		loanpf = writeLoanRecord(crtloanDto, loanpf);
		return loanpf;

	}

	/**
	 * Writes into loanpf.
	 * 
	 * @param crtloanDto
	 * @param loanpf
	 * @return
	 */
	private Loanpf writeLoanRecord(CrtloanDTO crtloanDto, Loanpf loanpf) {
		loanpf.setFtranno(crtloanDto.getTranno());
		loanpf.setLtranno(0);
		loanpf.setLoantype(crtloanDto.getLoantype());
		loanpf.setLoancurr(crtloanDto.getCntcurr());
		loanpf.setLoanorigam(crtloanDto.getOutstamt());
		loanpf.setLstcaplamt(crtloanDto.getOutstamt());
		loanpf.setLoanstdate(crtloanDto.getEffdate());
		loanpf.setLstcapdate(crtloanDto.getEffdate());
		loanpf.setLstintbdte(crtloanDto.getEffdate());
		loanpf.setTplstmdty(BigDecimal.ZERO);
		loanpf.setTrdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
		loanpf.setUser_t(0);
		loanpf.setValidflag("1");
		loanpf.setTrtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
		loanpf.setTermid("");
		loanpf.setDatime(new Timestamp(System.currentTimeMillis()));
		loanpf.setJobnm(crtloanDto.getJobname());
		loanpf.setLoantype(crtloanDto.getLoantype());
		loanpf.setUsrprf(crtloanDto.getUsrprofile());
		loanDAO.insertLoanpf(loanpf);
		return loanpf;

	}

	/**
	 * Calculates capitalization date.
	 * 
	 * @param occdate
	 * @param effectiveDate
	 * @param t6633
	 * @return
	 */
	private int calculateCapitalizationDate(int occdate, int effectiveDate, T6633 t6633) {
		
		String contractMonth = DatimeUtil.parseMonth(occdate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String contractDay = DatimeUtil.parseDay(occdate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String contractMD = contractMonth + contractDay;

		String loanYear = DatimeUtil.parseYear(effectiveDate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String loanMonth = DatimeUtil.parseMonth(effectiveDate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String loanDay = DatimeUtil.parseDay(effectiveDate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String loanMD = contractMonth + contractDay;

		if ("Y".equals(t6633.getAnnloan()) || ("Y".equals(t6633.getAnnpoly()) && occdate == effectiveDate)) {
			return datcon2.plusYears(effectiveDate, 1);
		} else if ("Y".equals(t6633.getAnnloan()) && contractMD.compareTo(loanMD) > 0) {
			return Integer.valueOf(loanYear + contractMD);
		} else if ("Y".equals(t6633.getAnnloan()) && contractMD.compareTo(loanMD) <= 0) {
			Integer nxtcapdate = Integer.valueOf(loanYear + contractMD);
			return datcon2.plusYears(nxtcapdate, 1);
		} else {
			return calculateNxtCapdate(loanYear, loanMonth, loanDay, t6633);
		}

	}

	/**
	 * Calculates NextIntBill date.
	 * 
	 * @param occdate
	 * @param effectiveDate
	 * @param t6633
	 * @return
	 */
	private int calculateNextIntBillDate(int occdate, int effectiveDate, T6633 t6633) {

		String contractMonth = DatimeUtil.parseMonth(occdate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String contractDay = DatimeUtil.parseDay(occdate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String contractMD = contractMonth + contractDay;

		String loanYear = DatimeUtil.parseYear(effectiveDate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String loanMonth = DatimeUtil.parseMonth(effectiveDate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String loanDay = DatimeUtil.parseDay(effectiveDate, CommonConstants.DATE_FORMAT_YYYYMMDD);
		String loanMD = contractMonth + contractDay;

		if ("Y".equals(t6633.getLoanAnnivInterest())
				|| ("Y".equals(t6633.getLoanAnnivInterest()) && occdate == effectiveDate)) {
			return datcon2.plusYears(effectiveDate, 1);
		} else if ("Y".equals(t6633.getLoanAnnivInterest()) && contractMD.compareTo(loanMD) > 0) {
			return Integer.valueOf(loanYear + contractMD);
		} else if ("Y".equals(t6633.getLoanAnnivInterest()) && contractMD.compareTo(loanMD) <= 0) {
			Integer nextIntBillDate = Integer.valueOf(loanYear + contractMD);
			return datcon2.plusYears(nextIntBillDate, 1);
		} else {
			return calculateNextIntBillDateSub(loanYear, loanMonth, loanDay, t6633);
		}
	}

	/**
	 * Internal calculation logic for NxtCap date.
	 * 
	 * @param loanYear
	 * @param loanMonth
	 * @param loanDay
	 * @param t6633
	 * @return
	 */
	private int calculateNxtCapdate(String loanYear, String loanMonth, String loanDay, T6633 t6633) {
		LocalDate loanDate = LocalDate.parse(loanYear + loanMonth + loanDay, DateTimeFormatter.ofPattern("yyyyMMdd"));
		String loanDayStr;
		if (t6633.getDay() != null && t6633.getDay() != 0) {
			String day;
			if (t6633.getDay() < 10) {
				day = "0" + t6633.getDay().toString();
			} else {
				day = t6633.getDay().toString();
			}
			if (day.compareTo(DAY28) > 0) {
				if (day.equals(DAY31) && MONTH_WITH_30DAYS.contains(loanDate.getMonth())) {
					loanDayStr = DAY30;
				} else if (loanDate.getMonth().equals(Month.FEBRUARY)) {
					if (loanDate.isLeapYear()) {
						loanDayStr = DAY29;
					} else {
						loanDayStr = DAY28;
					}
				} else {
					loanDayStr = day;
				}
			} else {
				loanDayStr = day;
			}
		} else {
			loanDayStr = loanDay;
		}

		int nxtcapdate = Integer.parseInt(loanYear + loanMonth + loanDayStr);
		if (t6633.getCompfreq() == null || "".equals(t6633.getCompfreq().trim())) {
			nxtcapdate = datcon2.plusYears(nxtcapdate, 1);
		} else {
			nxtcapdate = getDatebyfrequency(nxtcapdate, t6633.getCompfreq());
		}

		return nxtcapdate;
	}

	/**
	 * Internal logic for NextIntBillDate calculation.
	 * 
	 * @param loanYear
	 * @param loanMonth
	 * @param loanDay
	 * @param t6633
	 * @return
	 */
	private int calculateNextIntBillDateSub(String loanYear, String loanMonth, String loanDay, T6633 t6633) {
		LocalDate loanDate = LocalDate.parse(loanYear + loanMonth + loanDay, DateTimeFormatter.ofPattern("yyyyMMdd"));
		String loanDayStr;
		if (t6633.getInterestDay() != null && t6633.getInterestDay() != 0) {
			String day;
			if (t6633.getInterestDay() < 10) {
				day = "0" + t6633.getInterestDay().toString();
			} else {
				day = t6633.getInterestDay().toString();
			}

			if (day.compareTo(DAY28) > 0) {
				if (day.equals(DAY31) && MONTH_WITH_30DAYS.contains(loanDate.getMonth())) {
					loanDayStr = DAY30;
				} else if (loanDate.getMonth().equals(Month.FEBRUARY)) {
					if (loanDate.isLeapYear()) {
						loanDayStr = DAY29;
					} else {
						loanDayStr = DAY28;
					}
				} else {
					loanDayStr = day;
				}
			} else {
				loanDayStr = day;
			}
		} else {
			loanDayStr = loanDay;
		}
		int nextIntBillDate = Integer.parseInt(loanYear + loanMonth + loanDayStr);
		if (t6633.getCompfreq() == null || "".equals(t6633.getCompfreq().trim())) {
			nextIntBillDate = datcon2.plusYears(nextIntBillDate, 1);
		} else {
			nextIntBillDate = getDatebyfrequency(nextIntBillDate, t6633.getCompfreq());
		}
		return nextIntBillDate;
	}

	/**
	 * Gets date by frequency.
	 * 
	 * @param nxtcapdate
	 * @param frequency
	 * @return
	 */
	private Integer getDatebyfrequency(Integer nxtcapdate, String frequency) {
		Integer nextdate = null;
		if ("12".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 1);
		}
		if ("02".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 6);
		}
		if ("04".equals(frequency)) {
			nextdate = datcon2.plusMonths(nxtcapdate, 3);
		}
		return nextdate;
	}

	/**
	 * Prepares LifacmvDTO.
	 * 
	 * @param crtloanDto
	 * @param rldgacct
	 * @return
	 * @throws IOException
	 */
	private LifacmvDTO prepareLifacmvDTO(CrtloanDTO crtloanDto, String rldgacct) throws IOException {/*ILIFE-5977*/

		LifacmvDTO lifacmvdto = new LifacmvDTO();
		lifacmvdto.setRdocnum(crtloanDto.getChdrnum());
		lifacmvdto.setJrnseq(0);
		lifacmvdto.setBatccoy(crtloanDto.getChdrcoy());
		lifacmvdto.setRldgcoy(crtloanDto.getChdrcoy());
		lifacmvdto.setGenlcoy(crtloanDto.getChdrcoy());
		// set key
		lifacmvdto.setBatccoy(crtloanDto.getChdrcoy());
		lifacmvdto.setBatcbrn(crtloanDto.getBranch());
		lifacmvdto.setBatcactyr(crtloanDto.getActyear());
		lifacmvdto.setBatcactmn(crtloanDto.getActmonth());
		lifacmvdto.setBatctrcde(crtloanDto.getTrcde());
		lifacmvdto.setBatcbatch(crtloanDto.getBatch());

		lifacmvdto.setBatctrcde(crtloanDto.getAuthCode());
		lifacmvdto.setGenlcur("");
		lifacmvdto.setRcamt(BigDecimal.ZERO);
		lifacmvdto.setCrate(BigDecimal.ZERO);
		lifacmvdto.setAcctamt(BigDecimal.ZERO);
		lifacmvdto.setContot(0);
		lifacmvdto.setTranno(crtloanDto.getTranno());
		lifacmvdto.setFrcdate(99999999); // varcom.vrcmMaxDate
		lifacmvdto.setEffdate(crtloanDto.getEffdate());
		lifacmvdto.setTranref(crtloanDto.getChdrnum());
		List<String> list = new ArrayList<>();
		list.add(crtloanDto.getCnttype());
		lifacmvdto.setSubstituteCodes(list);
		lifacmvdto.setTrandesc(crtloanDto.getLongdesc());
		lifacmvdto.setRldgacct(rldgacct);
		lifacmvdto.setTransactionDate(crtloanDto.getEffdate());
		lifacmvdto.setTransactionTime(Integer.valueOf(DatimeUtil.getCurrentTime())); // wsaaTime
		lifacmvdto.setUser(0);
		lifacmvdto.setTermid(""); // varcom.vrcmTermid)
		lifacmvdto.setUsrprofile(crtloanDto.getUsrprofile());
		lifacmvdto.setJobname(crtloanDto.getJobname());

		if (crtloanDto.getCntcurr().compareTo(crtloanDto.getBillcurr()) != 0) {
			
			lifacmvdto.setOrigcurr(crtloanDto.getBillcurr());
			lifacmvdto.setOrigamt(convertLoan(crtloanDto).getCalculatedAmount());
		} else {
			lifacmvdto.setOrigcurr(crtloanDto.getCntcurr());
			lifacmvdto.setOrigamt(crtloanDto.getOutstamt());
		}

		return lifacmvdto;
	}

	/**
	 * Calls Xcvrt utility.
	 * 
	 * @param crtloanDto
	 * @return
	 * @throws IOException
	 */
	private ConlinkOutputDTO convertLoan(CrtloanDTO crtloanDto) throws IOException {/*ILIFE-5977*/
		ConlinkInputDTO conlinkDTO = new ConlinkInputDTO();
		conlinkDTO.setCompany(crtloanDto.getChdrcoy());
		conlinkDTO.setCashdate(crtloanDto.getEffdate());
		conlinkDTO.setFromCurrency(crtloanDto.getCntcurr());
		conlinkDTO.setToCurrency(crtloanDto.getBillcurr());
		conlinkDTO.setAmount(crtloanDto.getOutstamt());
		return xcvrt.executeSurrFunction(conlinkDTO);/*ILIFE-5977*/
	}
	
	/**
	 * Calls Lifacmv.executePSTW()
	 * 
	 * @param crtloanDto
	 * @param lifacmvdto
	 * @throws IOException
	 *//*ILIFE-5977*/
	private void postAcmvpf(CrtloanDTO crtloanDto, LifacmvDTO lifacmvdto) throws IOException,ParseException{//ILIFE-5763
		lifacmvdto.setSacscode(crtloanDto.getSacscode01());
		lifacmvdto.setSacstyp(crtloanDto.getSacstyp01());
		lifacmvdto.setGlcode(crtloanDto.getGlcode01());
		lifacmvdto.setGlsign(crtloanDto.getGlsign01());
		lifacmv.executePSTW(lifacmvdto);/*ILIFE-5977*/
	}
	
	/**
	 * Post principal.
	 * 
	 * @param crtloanDto
	 * @param lifacmvdto
	 * @param loanpf
	 * @throws IOException
	 *//*ILIFE-5977*/
	private void postPrincipal(CrtloanDTO crtloanDto, LifacmvDTO lifacmvdto, Loanpf loanpf) throws IOException,ParseException {//ILIFE-5763
		lifacmvdto.setSacscode(crtloanDto.getSacscode02());
		lifacmvdto.setSacstyp(crtloanDto.getSacstyp02());
		lifacmvdto.setGlcode(crtloanDto.getGlcode02());
		lifacmvdto.setGlsign(crtloanDto.getGlsign02());
		lifacmvdto.setOrigcurr(crtloanDto.getCntcurr());
		lifacmvdto.setOrigamt(crtloanDto.getOutstamt());
		lifacmv.executePSTW(lifacmvdto);/*ILIFE-5977*/
		
		if (!crtloanDto.getCntcurr().equals(crtloanDto.getBillcurr())) {
			lifacmvdto.setSacscode(crtloanDto.getSacscode03());
			lifacmvdto.setSacstyp(crtloanDto.getSacstyp03());
			lifacmvdto.setGlcode(crtloanDto.getGlcode03());
			lifacmvdto.setGlsign(crtloanDto.getGlsign03());
			lifacmvdto.setOrigcurr(crtloanDto.getCntcurr());
			lifacmvdto.setOrigamt(crtloanDto.getOutstamt());
			lifacmvdto.setRldgacct("");
			lifacmvdto.setJrnseq(lifacmvdto.getJrnseq() + 1);
			lifacmv.executePSTW(lifacmvdto);/*ILIFE-5977*/

			lifacmvdto.setSacscode(crtloanDto.getSacscode04());
			lifacmvdto.setSacstyp(crtloanDto.getSacstyp04());
			lifacmvdto.setGlcode(crtloanDto.getGlcode04());
			lifacmvdto.setGlsign(crtloanDto.getGlsign04());
			lifacmvdto.setOrigcurr(crtloanDto.getCntcurr());
			lifacmvdto.setOrigamt(crtloanDto.getOutstamt());
			lifacmvdto.setRldgacct("");
			lifacmvdto.setJrnseq(lifacmvdto.getJrnseq() + 1);
			lifacmv.executePSTW(lifacmvdto);/*ILIFE-5977*/

		}
	}
}
