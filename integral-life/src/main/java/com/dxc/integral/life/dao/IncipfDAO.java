package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Incipf;

public interface IncipfDAO {

public List<Incipf> getIncipfRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
public int updateIncipfPremcurr(Incipf incipf);

}
