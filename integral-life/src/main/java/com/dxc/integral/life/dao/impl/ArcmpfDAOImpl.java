package com.dxc.integral.life.dao.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ArcmpfDAO;
import com.dxc.integral.life.dao.model.Arcmpf;

@Repository("arcmpfDAO")
public class ArcmpfDAOImpl extends BaseDAOImpl  implements ArcmpfDAO {
	@Override
	public Arcmpf getArcmpfRecords(String filn) {
		String sql ="select * from ARCMPF where FILN=? ORDER BY FILN ASC, UNIQUE_NUMBER ASC";

		try {
		return jdbcTemplate.queryForObject(sql, new Object[] { filn},
				new BeanPropertyRowMapper<Arcmpf>(Arcmpf.class));
		}catch(EmptyResultDataAccessException e) {
			return null;
		}
	}
}
