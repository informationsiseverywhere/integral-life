package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.dao.AcmvpfDAO;
import com.dxc.integral.fsu.dao.model.Acmvpf;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.iaf.constants.CommonConstants;
//import com.dxc.integral.iaf.constants.Companies;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.IntcalcDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T6633;
import com.dxc.integral.life.utils.Intcalc;

/**
 * The Intcalc utility implementation.
 * 
 * This program is called to calculate Interest due on a given Loan between 2
 * specified dates passed in the INTCALC parameter area. The interest is
 * calculated in the Loan Currency and returned in the Loan currency to the
 * calling program.
 * 
 * @author mmalik25
 *
 */
@Service("intcalc")
@Lazy
public class IntcalcImpl implements Intcalc {

	@Autowired
	private AcmvpfDAO acmvpfDAO;

	@Autowired
	private Datcon2 datcon2;

	@Autowired
	private Datcon3 datcon3;

	@Autowired
	private Xcvrt xcvrt;
	
	@Autowired
	private ItempfDAO itempfDAO;/*ILIFE-5977*/
	T6633 t6633IO = null;
	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/*IJTI-398*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dxc.integral.life.utils.Intcalc#
	 * interestCalculation(com.dxc.integral.fsu.beans.IntcalcDTO)
	 */
	@Override/*ILIFE-5977*/
	public BigDecimal interestCalculation(IntcalcDTO intcalcDTO) throws IOException,ParseException {//ILIFE-5763

		BigDecimal totalInterest = new BigDecimal(0);

		T6633 t6633IO = getT6633Info(intcalcDTO.getLoanStartDate(), intcalcDTO.getLoanStartDate(),
				intcalcDTO.getCnttype(), intcalcDTO.getLoanType(), intcalcDTO.getChdrcoy());/*ILIFE-5977*/
		long daysDiff = datcon3.getDaysDifference(intcalcDTO.getInterestFrom(), intcalcDTO.getInterestTo());

		if ("F".equals(t6633IO.getInttype())) {
			Integer newdate = datcon2.plusMonths(intcalcDTO.getLoanStartDate(), t6633IO.getMperiod());
			if (intcalcDTO.getInterestTo() <= newdate) {
				return totalInterest;
			}
		}

		t6633IO = getT6633Info(intcalcDTO.getInterestTo(), intcalcDTO.getInterestTo(),
				intcalcDTO.getCnttype(), intcalcDTO.getLoanType(), intcalcDTO.getChdrcoy());/*ILIFE-5977*/  /*IJTI-379*/

		BigDecimal loanPrinInt = (intcalcDTO.getLoanorigam().multiply(t6633IO.getIntRate().divide(new BigDecimal(100))))
				.multiply(BigDecimal.valueOf(daysDiff).divide(new BigDecimal(365),9,BigDecimal.ROUND_HALF_UP));
		totalInterest = totalInterest.add(loanPrinInt);

		totalInterest = processAcmvList(totalInterest, t6633IO.getIntRate(),intcalcDTO);/*ILIFE-5977*/
		return totalInterest.setScale(2,BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * Prepares ACMVPF list.
	 * 
	 * @param totInterest
	 * @param interestRate
	 * @param intcalcDTO
	 * @return
	 * @throws IOException
	 *//*ILIFE-5977*/
	private BigDecimal processAcmvList(BigDecimal totInterest, BigDecimal interestRate, IntcalcDTO intcalcDTO) throws IOException, ParseException {//ILIFE-5763

		String sacscode = "";
		String sacstype = "";
		String sign = "";
		BigDecimal loanCurrVal;
		long daysDiff;
		BigDecimal totalInterest = totInterest;

		T5645 t5645IO = getT5645Info("INTCALC", intcalcDTO.getChdrcoy());/*ILIFE-5977*/  /*IJTI-379*/

		if ("P".equals(intcalcDTO.getLoanType())) {
			sacscode = t5645IO.getSacscodes().get(0);
			sacstype = t5645IO.getSacstypes().get(0);
			sign = t5645IO.getSigns().get(0);
		} else if ("A".equals(intcalcDTO.getLoanType())) {
			sacscode = t5645IO.getSacscodes().get(1);
			sacstype = t5645IO.getSacstypes().get(1);
			sign = t5645IO.getSigns().get(1);

		} else if ("E".equals(intcalcDTO.getLoanType())) {
			sacscode = t5645IO.getSacscodes().get(2);
			sacstype = t5645IO.getSacstypes().get(2);
			sign = t5645IO.getSigns().get(2);

		} else if ("D".equals(intcalcDTO.getLoanType())) {
			sacscode = t5645IO.getSacscodes().get(3);
			sacstype = t5645IO.getSacstypes().get(3);
			sign = t5645IO.getSigns().get(3);
		}
		StringBuilder loanNumber = new StringBuilder("");
		loanNumber.append(String.format("%02d", Integer.parseInt(intcalcDTO.getLoanNumber().toString().trim())));

		List<Acmvpf> acmvpfList = acmvpfDAO.getAcmvpfList(intcalcDTO.getChdrnum() + intcalcDTO.getLoanNumber(),
				sacscode, sacstype, intcalcDTO.getLastCaplsnDate(), intcalcDTO.getChdrcoy());

		for (Acmvpf acmv : acmvpfList) {
			if (acmv.getEffdate() < intcalcDTO.getInterestFrom()) {
				daysDiff = datcon3.getDaysDifference(intcalcDTO.getInterestFrom(), intcalcDTO.getInterestTo());
			} else {
				daysDiff = datcon3.getDaysDifference(acmv.getEffdate(), intcalcDTO.getInterestTo());
			}
			if (!intcalcDTO.getLoanCurrency().equals(acmv.getOrigcurr())) {
				ConlinkOutputDTO conlinkOutput = currencyConvert(acmv, intcalcDTO);/*ILIFE-5977*/
				loanCurrVal = conlinkOutput.getCalculatedAmount();
			} else {
				loanCurrVal = acmv.getOrigamt();
			}
			BigDecimal interest = loanCurrVal.multiply(interestRate.divide(new BigDecimal(100)))
					.multiply(new BigDecimal(Float.valueOf(daysDiff)/Float.valueOf(365)));
			if (acmv.getGlsign().equals(sign)) {
				totalInterest = totalInterest.add(interest);
			} else {
				totalInterest = totalInterest.subtract(interest);
			}
		}
		return totalInterest;
	}

	/**
	 * Calls Xcvrt utility for currency conversion.
	 * 
	 * @param acmvpf
	 * @param intcalcDTO
	 * @return
	 * @throws IOException
	 */
	private ConlinkOutputDTO currencyConvert(Acmvpf acmvpf,IntcalcDTO intcalcDTO)/*ILIFE-5977*/
			throws IOException {

		ConlinkInputDTO conlinkInputDTO = new ConlinkInputDTO();
		conlinkInputDTO.setAmount(acmvpf.getOrigamt());
		conlinkInputDTO.setFromCurrency(acmvpf.getOrigcurr());
		conlinkInputDTO.setCashdate(99999999);
		conlinkInputDTO.setToCurrency(intcalcDTO.getLoanCurrency());
		conlinkInputDTO.setCompany(intcalcDTO.getChdrcoy());
		return xcvrt.executeRealFunction(conlinkInputDTO);/*ILIFE-5977*/
	}

	/**
	 * Gets T6633 smart table item.
	 * 
	 * @param itmFrm
	 * @param itmTo
	 * @param contractType
	 * @param loanType
	 * @return
	 * @throws IOException
	 */
	private T6633 getT6633Info(int itmFrm, int itmTo, String contractType,
			String loanType, String company) throws IOException {/*ILIFE-5977*/

		T6633 t6633IO = new T6633();
		StringBuilder itemitem = new StringBuilder(contractType);
		itemitem.append(loanType);
		Map<String, Itempf> t6633Map=itempfDAO.readSmartTableByTableName2(company, "T6633", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/  /*IJTI-379*/
		if (t6633Map.containsKey(itemitem.toString())) {
			Itempf t6633Item = t6633Map.get(itemitem.toString());
			if (t6633Item.getItmfrm() <= itmFrm && t6633Item.getItmto() >= itmTo) {
				t6633IO = smartTableDataUtils.convertGenareaToPojo(t6633Item.getGenareaj(), T6633.class); /*IJTI-398*/
			}
		}else{//ILIFE-5763 Starts
			throw new ItemNotfoundException("Item "+itemitem.toString()+" not found in Table T6633");
		}//ILIFE-5763 Ends
		return t6633IO;
	}

	/**
	 * Get T5645 smart table item.
	 *
	 * @param itemitem
	 * @return
	 * @throws IOException
	 */
	private T5645 getT5645Info(String itemitem, String company) throws IOException {/*ILIFE-5977*/ /*IJTI-379*/
		Map<String, List<Itempf>> t5645Map=itempfDAO.readSmartTableByTableName(company, "T5645", CommonConstants.IT_ITEMPFX);/*ILIFE-5977*/ /*IJTI-379*/
		T5645 t5645IO = null;//ILIFE-5763
		if (t5645Map.containsKey(itemitem)) {
			List<Itempf> t5645ItemList = t5645Map.get(itemitem);
			t5645IO = smartTableDataUtils.convertGenareaToPojo(t5645ItemList.get(0).getGenareaj(), T5645.class); /*IJTI-398*/
		}else{//ILIFE-5763 Starts
			throw new ItemNotfoundException("Item "+itemitem+" not found in Table T5645");
		}//ILIFE-5763 Ends
		return t5645IO;
	}
}
