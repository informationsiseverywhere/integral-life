package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Lrrhpf {

	private long uniqueNumber;
	private String clntpfx;
	private String clntcoy;
	private String clntnum;
	private String company;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String lrkcls;
	private int currfrom;
	private int currto;
	private String validflag;
	private int tranno;
	private String currency;
	private BigDecimal ssretn;
	private BigDecimal ssreast;
	private BigDecimal ssreasf;
	private String userProfile;
	private String jobName;
	private String datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getClntpfx() {
		return clntpfx;
	}

	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}

	public String getClntcoy() {
		return clntcoy;
	}

	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}

	public String getClntnum() {
		return clntnum;
	}

	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public String getLrkcls() {
		return lrkcls;
	}

	public void setLrkcls(String lrkcls) {
		this.lrkcls = lrkcls;
	}

	public int getCurrfrom() {
		return currfrom;
	}

	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}

	public int getCurrto() {
		return currto;
	}

	public void setCurrto(int currto) {
		this.currto = currto;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getSsretn() {
		return ssretn;
	}

	public void setSsretn(BigDecimal ssretn) {
		this.ssretn = ssretn;
	}

	public BigDecimal getSsreast() {
		return ssreast;
	}

	public void setSsreast(BigDecimal ssreast) {
		this.ssreast = ssreast;
	}

	public BigDecimal getSsreasf() {
		return ssreasf;
	}

	public void setSsreasf(BigDecimal ssreasf) {
		this.ssreasf = ssreasf;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
}