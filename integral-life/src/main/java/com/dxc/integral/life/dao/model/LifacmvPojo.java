/*********************  */
/*Author  :Liwei					*/
/*Purpose :Model for Lifacmv		*/
/*Date    :2018.10.26				*/
package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class LifacmvPojo {
  	
	private String lifacmvRec;
  	private String function;
  	private String statuz;
  	private String batckey;
  	private String filler;
  	private String batccoy;
  	private String batcbrn;
  	private int batcactyr;
  	private int batcactmn;
  	private String batctrcde;
  	private String batcbatch;
  	private String rdocnum;
  	private int tranno;
  	private int jrnseq;
  	private String rldgcoy;
  	private String sacscode;
  	private String rldgacct;
  	private String origcurr;
  	private String sacstyp;
  	private BigDecimal origamt;
  	private String tranref;
  	private String trandesc;
  	private BigDecimal crate;
  	private BigDecimal acctamt;
  	private String genlcoy;
  	private String genlcur;
  	private String glcode;
  	private String glsign;
  	private int contot;
  	private String postyear;
  	private String postmonth;
  	private int effdate;
  	private int rcamt;
  	private int frcdate;
  	private int transactionDate;
  	private int transactionTime;
  	private int user;
  	private String termid;
  	private String suprflag;
  	private String[] substituteCodes;
  	private int threadNumber;
  	private String prefix;
  	private String ind;
  	
	public LifacmvPojo() {
		lifacmvRec = "";
		function = "";
		statuz = "";
		batckey = "";
		filler = "";
		batccoy = "";
		batcbrn = "";
		batcactyr = 0;
		batcactmn = 0;
		batctrcde = "";
		batcbatch = "";
		rdocnum = "";
		tranno = 0;
		jrnseq = 0;
		rldgcoy = "";
		sacscode = "";
		rldgacct = "";
		origcurr = "";
		sacstyp = "";
		origamt = BigDecimal.ZERO;
		tranref = "";
		trandesc = "";
		crate = BigDecimal.ZERO;
		acctamt = BigDecimal.ZERO;
		genlcoy = "";
		genlcur = "";
		glcode = "";
		glsign = "";
		contot = 0;
		postyear = "";
		postmonth = "";
		effdate = 0;
		rcamt = 0;
		frcdate = 0;
		transactionDate = 0;
		transactionTime = 0;
		user = 0;
		termid = "";
		suprflag = "";
		threadNumber = 0;
		prefix = "";
		ind = "";
	}
	public String getLifacmvRec() {
		return lifacmvRec;
	}
	public void setLifacmvRec(String lifacmvRec) {
		this.lifacmvRec = lifacmvRec;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getBatckey() {
		return batckey;
	}
	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(String rdocnum) {
		this.rdocnum = rdocnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}
	public String getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public BigDecimal getOrigamt() {
		return origamt;
	}
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}
	public String getTranref() {
		return tranref;
	}
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}
	public String getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}
	public BigDecimal getCrate() {
		return crate;
	}
	public void setCrate(BigDecimal crate) {
		this.crate = crate;
	}
	public BigDecimal getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(BigDecimal acctamt) {
		this.acctamt = acctamt;
	}
	public String getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(String genlcoy) {
		this.genlcoy = genlcoy;
	}
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	public String getGlcode() {
		return glcode;
	}
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	public String getGlsign() {
		return glsign;
	}
	public void setGlsign(String glsign) {
		this.glsign = glsign;
	}
	public int getContot() {
		return contot;
	}
	public void setContot(int contot) {
		this.contot = contot;
	}
	public String getPostyear() {
		return postyear;
	}
	public void setPostyear(String postyear) {
		this.postyear = postyear;
	}
	public String getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(String postmonth) {
		this.postmonth = postmonth;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getRcamt() {
		return rcamt;
	}
	public void setRcamt(int rcamt) {
		this.rcamt = rcamt;
	}
	public int getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(int frcdate) {
		this.frcdate = frcdate;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getSuprflag() {
		return suprflag;
	}
	public void setSuprflag(String suprflag) {
		this.suprflag = suprflag;
	}
	public String[] getSubstituteCodes() {
		return substituteCodes;
	}
	public void setSubstituteCodes(String[] substituteCodes) {
		this.substituteCodes = substituteCodes;
	}
	public int getThreadNumber() {
		return threadNumber;
	}
	public void setThreadNumber(int threadNumber) {
		this.threadNumber = threadNumber;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getInd() {
		return ind;
	}
	public void setInd(String ind) {
		this.ind = ind;
	}
	
  	
  	
}
