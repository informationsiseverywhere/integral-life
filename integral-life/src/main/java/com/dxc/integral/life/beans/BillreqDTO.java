package com.dxc.integral.life.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * @author wli31
 */
public class BillreqDTO {

  	private String function;
  	private String statuz;
  	private String company;
  	private String language;
  	private String branch;
  	private int effdate;
  	private int acctyear;
  	private int acctmonth;
  	private String trancode;
  	private String batch;
  	private int tranno;
  	private String termid;
  	private int time;
  	private int date_var;
  	private int user;
  	private String chdrpfx;
  	private String chdrcoy;
  	private String chdrnum;
  	private String servunit;
  	private String cnttype;
  	private String cntcurr;
  	private int occdate;
  	private int ccdate;
  	private int ptdate;
  	private int btdate;
  	private int billdate;
  	private String billchnl;
  	private String bankcode;
  	private int instfrom;
  	private int instto;
  	private String instbchnl;
  	private String instcchnl;
  	private String instfreq;
  	private String instjctl;
  	private String grpscoy;
  	private String grpsnum;
  	private String membsel;
  	private String facthous;
  	private String bankkey;
  	private String bankacckey;
  	private String cownpfx;
  	private String cowncoy;
  	private String cownnum;
  	private String payrpfx;
  	private String payrcoy;
  	private String payrnum;
  	private String cntbranch;
  	private String agntpfx;
  	private String agntcoy;
  	private String agntnum;
  	private String payflag;
  	private String bilflag;
  	private String outflag;
  	private String supflag;
  	private int billcd;
  	private String mandref;
  	private String billcurr;
  	private String sacscode01;
  	private String sacstype01;
  	private String glmap01;
  	private String glsign01;
  	private String sacscode02;
  	private String sacstype02;
  	private String glmap02;
  	private String glsign02;
  	private String mandstat;
  	private double contot01;
  	private double contot02;
  	private List<BigDecimal> instamts;
  	private String payername;
  	private int duedate;
  	private String modeInd;
  	private String fsuco;
  	private int nextdate;
  	public BillreqDTO(){
  	  	this.function = "";
  	  	this.statuz = "";
  	  	this.company = "";
  	  	this.language = "";
  	  	this.branch = "";
  	  	this.effdate = 0;
  	  	this.acctyear = 0;
  	  	this.acctmonth = 0;
  	  	this.trancode = "";
  	  	this.batch = "";
  	  	this.tranno = 0;
  	  	this.termid = "";
  	  	this.time = 0;
  	  	this.date_var = 0;
  	  	this.user = 0;
  	  	this.chdrpfx = "";
  	  	this.chdrcoy = "";
  	  	this.chdrnum = "";
  	  	this.servunit = "";
  	  	this.cnttype = "";
  	  	this.cntcurr = "";
  	  	this.occdate = 0;
  	  	this.ccdate = 0;
  	  	this.ptdate = 0;
  	  	this.btdate = 0;
  	  	this.billdate = 0;
  	  	this.billchnl = "";
  	  	this.bankcode = "";
  	  	this.instfrom = 0;
  	  	this.instto = 0;
  	  	this.instbchnl = "";
  	  	this.instcchnl = "";
  	  	this.instfreq = "";
  	  	this.instjctl = "";
  	  	this.grpscoy = "";
  	  	this.grpsnum = "";
  	  	this.membsel = "";
  	  	this.facthous = "";
  	  	this.bankkey = "";
  	  	this.bankacckey = "";
  	  	this.cownpfx = "";
  	  	this.cowncoy = "";
  	  	this.cownnum = "";
  	  	this.payrpfx = "";
  	  	this.payrcoy = "";
  	  	this.payrnum = "";
  	  	this.cntbranch = "";
  	  	this.agntpfx = "";
  	  	this.agntcoy = "";
  	  	this.agntnum = "";
  	  	this.payflag = "";
  	  	this.bilflag = "";
  	  	this.outflag = "";
  	  	this.supflag = "";
  	  	this.billcd = 0;
  	  	this.mandref = "";
  	  	this.billcurr = "";
  	  	this.sacscode01 = "";
  	  	this.sacstype01 = "";
  	  	this.glmap01 = "";
  	  	this.glsign01 = "";
  	  	this.sacscode02 = "";
  	  	this.sacstype02 = "";
  	  	this.glmap02 = "";
  	  	this.glsign02 = "";
  	  	this.mandstat = "";
  	  	this.contot01 = 0;
  	  	this.contot02 = 0;
  	  	this.instamts = new ArrayList<BigDecimal>();
  	  	this.payername = "";
  	  	this.duedate = 0;
  	  	this.modeInd = "";
  	  	this.fsuco = "";
  	  	this.nextdate = 0;
  	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getAcctyear() {
		return acctyear;
	}
	public void setAcctyear(int acctyear) {
		this.acctyear = acctyear;
	}
	public int getAcctmonth() {
		return acctmonth;
	}
	public void setAcctmonth(int acctmonth) {
		this.acctmonth = acctmonth;
	}
	public String getTrancode() {
		return trancode;
	}
	public void setTrancode(String trancode) {
		this.trancode = trancode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getDate_var() {
		return date_var;
	}
	public void setDate_var(int date_var) {
		this.date_var = date_var;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getServunit() {
		return servunit;
	}
	public void setServunit(String servunit) {
		this.servunit = servunit;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public int getCcdate() {
		return ccdate;
	}
	public void setCcdate(int ccdate) {
		this.ccdate = ccdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getBilldate() {
		return billdate;
	}
	public void setBilldate(int billdate) {
		this.billdate = billdate;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public int getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(int instfrom) {
		this.instfrom = instfrom;
	}
	public int getInstto() {
		return instto;
	}
	public void setInstto(int instto) {
		this.instto = instto;
	}
	public String getInstbchnl() {
		return instbchnl;
	}
	public void setInstbchnl(String instbchnl) {
		this.instbchnl = instbchnl;
	}
	public String getInstcchnl() {
		return instcchnl;
	}
	public void setInstcchnl(String instcchnl) {
		this.instcchnl = instcchnl;
	}
	public String getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(String instfreq) {
		this.instfreq = instfreq;
	}
	public String getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(String instjctl) {
		this.instjctl = instjctl;
	}
	public String getGrpscoy() {
		return grpscoy;
	}
	public void setGrpscoy(String grpscoy) {
		this.grpscoy = grpscoy;
	}
	public String getGrpsnum() {
		return grpsnum;
	}
	public void setGrpsnum(String grpsnum) {
		this.grpsnum = grpsnum;
	}
	public String getMembsel() {
		return membsel;
	}
	public void setMembsel(String membsel) {
		this.membsel = membsel;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}
	public String getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(String payrpfx) {
		this.payrpfx = payrpfx;
	}
	public String getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(String payrcoy) {
		this.payrcoy = payrcoy;
	}
	public String getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(String payrnum) {
		this.payrnum = payrnum;
	}
	public String getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}
	public String getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(String agntpfx) {
		this.agntpfx = agntpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getPayflag() {
		return payflag;
	}
	public void setPayflag(String payflag) {
		this.payflag = payflag;
	}
	public String getBilflag() {
		return bilflag;
	}
	public void setBilflag(String bilflag) {
		this.bilflag = bilflag;
	}
	public String getOutflag() {
		return outflag;
	}
	public void setOutflag(String outflag) {
		this.outflag = outflag;
	}
	public String getSupflag() {
		return supflag;
	}
	public void setSupflag(String supflag) {
		this.supflag = supflag;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}
	public String getSacscode01() {
		return sacscode01;
	}
	public void setSacscode01(String sacscode01) {
		this.sacscode01 = sacscode01;
	}
	public String getSacstype01() {
		return sacstype01;
	}
	public void setSacstype01(String sacstype01) {
		this.sacstype01 = sacstype01;
	}
	public String getGlmap01() {
		return glmap01;
	}
	public void setGlmap01(String glmap01) {
		this.glmap01 = glmap01;
	}
	public String getGlsign01() {
		return glsign01;
	}
	public void setGlsign01(String glsign01) {
		this.glsign01 = glsign01;
	}
	public String getSacscode02() {
		return sacscode02;
	}
	public void setSacscode02(String sacscode02) {
		this.sacscode02 = sacscode02;
	}
	public String getSacstype02() {
		return sacstype02;
	}
	public void setSacstype02(String sacstype02) {
		this.sacstype02 = sacstype02;
	}
	public String getGlmap02() {
		return glmap02;
	}
	public void setGlmap02(String glmap02) {
		this.glmap02 = glmap02;
	}
	public String getGlsign02() {
		return glsign02;
	}
	public void setGlsign02(String glsign02) {
		this.glsign02 = glsign02;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public double getContot01() {
		return contot01;
	}
	public void setContot01(double contot01) {
		this.contot01 = contot01;
	}
	public double getContot02() {
		return contot02;
	}
	public void setContot02(double contot02) {
		this.contot02 = contot02;
	}
	public List<BigDecimal> getInstamts() {
		return instamts;
	}
	public void setInstamts(List<BigDecimal> instamts) {
		this.instamts = instamts;
	}
	public String getPayername() {
		return payername;
	}
	public void setPayername(String payername) {
		this.payername = payername;
	}
	public int getDuedate() {
		return duedate;
	}
	public void setDuedate(int duedate) {
		this.duedate = duedate;
	}
	public String getModeInd() {
		return modeInd;
	}
	public void setModeInd(String modeInd) {
		this.modeInd = modeInd;
	}
	public String getFsuco() {
		return fsuco;
	}
	public void setFsuco(String fsuco) {
		this.fsuco = fsuco;
	}
	public int getNextdate() {
		return nextdate;
	}
	public void setNextdate(int nextdate) {
		this.nextdate = nextdate;
	}
  	
}
