package com.dxc.integral.life.exceptions;

/**
 * Marker interface for GOTO labels.
 * <p>
 * Classes should use this interface as follows:
 * <pre>
 *   private enum GotoLabel implements GOTOInterface {
 *       DEFAULT, label1, label2, ... }
 * </pre>
 * 
 * @author andrew
 */
public interface GOTOInterface {
  // marker interface
}
