package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5692 {

private List<Integer> ages;
private List<BigDecimal> incmrates;
private List<BigDecimal> inprempcs;
private List<BigDecimal> rwcmrates;
private List<BigDecimal> reprempcs;
public List<Integer> getAges() {
	return ages;
}
public void setAges(List<Integer> ages) {
	this.ages = ages;
}
public List<BigDecimal> getIncmrates() {
	return incmrates;
}
public void setIncmrates(List<BigDecimal> incmrates) {
	this.incmrates = incmrates;
}
public List<BigDecimal> getInprempcs() {
	return inprempcs;
}
public void setInprempcs(List<BigDecimal> inprempcs) {
	this.inprempcs = inprempcs;
}
public List<BigDecimal> getRwcmrates() {
	return rwcmrates;
}
public void setRwcmrates(List<BigDecimal> rwcmrates) {
	this.rwcmrates = rwcmrates;
}
public List<BigDecimal> getReprempcs() {
	return reprempcs;
}
public void setReprempcs(List<BigDecimal> reprempcs) {
	this.reprempcs = reprempcs;
}




}
