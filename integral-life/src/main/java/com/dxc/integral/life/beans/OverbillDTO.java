package com.dxc.integral.life.beans;

/**
 * 
 * @author fwang3
 *
 */
public class OverbillDTO {

	private String function;
	private String company;
	private String chdrnum;
	private String batchkey;
	private String batcpfx;
	private String batccoy;
	private String batcbrn;
	private int batcactyr;
	private int batcactmn;
	private String batctrcde;
	private String batcbatch;
	private int newTranno;
	private String language;
	private int ptdate;
	private int btdate;
	private int tranDate;
	private int tranTime;
	private int user;
	private String termid;

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getBatchkey() {
		return batchkey;
	}

	public void setBatchkey(String batchkey) {
		this.batchkey = batchkey;
	}

	public String getBatcpfx() {
		return batcpfx;
	}

	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}

	public String getBatccoy() {
		return batccoy;
	}

	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	public String getBatcbrn() {
		return batcbrn;
	}

	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	public int getBatcactyr() {
		return batcactyr;
	}

	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}

	public int getBatcactmn() {
		return batcactmn;
	}

	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	public String getBatcbatch() {
		return batcbatch;
	}

	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	public int getNewTranno() {
		return newTranno;
	}

	public void setNewTranno(int newTranno) {
		this.newTranno = newTranno;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getPtdate() {
		return ptdate;
	}

	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	public int getBtdate() {
		return btdate;
	}

	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}

	public int getTranDate() {
		return tranDate;
	}

	public void setTranDate(int tranDate) {
		this.tranDate = tranDate;
	}

	public int getTranTime() {
		return tranTime;
	}

	public void setTranTime(int tranTime) {
		this.tranTime = tranTime;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

}