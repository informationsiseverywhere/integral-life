package com.dxc.integral.life.smarttable.pojo;

public class T6598 {

  	private String addprocess;
  	private String calcprog;
  	private String procesprog;
	public String getAddprocess() {
		return addprocess;
	}
	public void setAddprocess(String addprocess) {
		this.addprocess = addprocess;
	}
	public String getCalcprog() {
		return calcprog;
	}
	public void setCalcprog(String calcprog) {
		this.calcprog = calcprog;
	}
	public String getProcesprog() {
		return procesprog;
	}
	public void setProcesprog(String procesprog) {
		this.procesprog = procesprog;
	}
 
}
