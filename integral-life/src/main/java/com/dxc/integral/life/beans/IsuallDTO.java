package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class IsuallDTO{
  	private String statuz;
  	private String company;
  	private String chdrnum;
  	private String life;
  	private String coverage;
  	private String rider;
  	private int planSuffix;
  	private BigDecimal freqFactor;
  	private String batcpfx;
  	private String batccoy;
  	private String batcbrn;
  	private int batcactyr;
  	private int batcactmn;
  	private String batctrcde;
  	private String batcbatch;
  	private int transactionDate;
  	private int transactionTime;
  	private int user;
  	private String termid;
  	private String convertUnlt;
  	private BigDecimal covrInstprem;
  	private BigDecimal covrSingp;
  	private int newTranno;
  	private int effdate;
  	private String function;
  	private String oldcovr;
  	private String oldrider;
  	private String language;
  	private int runDate;
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getConvertUnlt() {
		return convertUnlt;
	}
	public void setConvertUnlt(String convertUnlt) {
		this.convertUnlt = convertUnlt;
	}
	public BigDecimal getCovrInstprem() {
		return covrInstprem;
	}
	public void setCovrInstprem(BigDecimal covrInstprem) {
		this.covrInstprem = covrInstprem;
	}
	public BigDecimal getCovrSingp() {
		return covrSingp;
	}
	public void setCovrSingp(BigDecimal covrSingp) {
		this.covrSingp = covrSingp;
	}
	public int getNewTranno() {
		return newTranno;
	}
	public void setNewTranno(int newTranno) {
		this.newTranno = newTranno;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getOldcovr() {
		return oldcovr;
	}
	public void setOldcovr(String oldcovr) {
		this.oldcovr = oldcovr;
	}
	public String getOldrider() {
		return oldrider;
	}
	public void setOldrider(String oldrider) {
		this.oldrider = oldrider;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public int getRunDate() {
		return runDate;
	}
	public void setRunDate(int runDate) {
		this.runDate = runDate;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public BigDecimal getFreqFactor() {
		return freqFactor;
	}
	public void setFreqFactor(BigDecimal freqFactor) {
		this.freqFactor = freqFactor;
	}
}