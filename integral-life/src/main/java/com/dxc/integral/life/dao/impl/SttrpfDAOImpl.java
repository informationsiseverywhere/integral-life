package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.SttrpfDAO;
import com.dxc.integral.life.dao.model.Sttrpf;
import com.dxc.integral.life.dao.model.Taxdpf;
import com.dxc.integral.life.dao.model.Undrpf;
/* 
 * SttrpfDAO implementation for database table <b>Sttrpf</b> DB operations.
 * 
 * @author wli31
 *
 */
@Repository("sttrpfDAO")
@Lazy
public class SttrpfDAOImpl extends BaseDAOImpl implements SttrpfDAO {

	@Override
	public List<Sttrpf> searchSttrpfRecord(String chdrcoy, String chdrnum) {
		String sql = "SELECT * FROM STTRPF WHERE CHDRCOY = ? AND CHDRNUM = ? ORDER BY CHDRCOY ASC, CHDRNUM ASC, TRANNO DESC, UNIQUE_NUMBER ASC";
		return jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum },
					new BeanPropertyRowMapper<Sttrpf>(Sttrpf.class));
		}
	public void insertSttrpfRecord(List<Sttrpf> sttrpfList) { 
		final List<Sttrpf> tempSttrpfList = sttrpfList;   
		StringBuilder insertSql = new StringBuilder();
        insertSql.append(" INSERT INTO STTRPF (CHDRPFX,CHDRCOY,CHDRNUM,TRANNO,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,REVTRCDE,BILLFREQ,STATGOV,STATAGT,")
                .append("AGNTNUM,ARACDE,CNTBRANCH,BANDAGE,BANDSA,BANDPRM,BANDTRM,CNTTYPE,CRTABLE,PARIND,COMMYR,CNPSTAT,PSTATCODE,CNTCURR,REG,STFUND,STSECT,STSSECT,BANDAGEG,")
                .append("BANDSAG,BANDPRMG,BANDTRMG,SRCEBUS,SEX,MORTCLS,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,STCA,STCB,STCC,STCD,STCE,OVRDCAT,STATCAT,")
                .append("STCMTH,STVMTH,STPMTH,STSMTH,STMMTH,STIMTH,STCMTHG,STPMTHG,STSMTHG,STUMTH,STNMTH,TRDT,TRANNOR,LIFE,COVERAGE,RIDER,PLNSFX,ACCTCCY,STLMTH,STBMTHG,")
                .append("STLDMTHG,STRAAMT)")
        		.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
        				.append("?,?,?,?,?)");
        	jdbcTemplate.batchUpdate(insertSql.toString(),new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
                 return tempSttrpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	  ps.setString(i++, sttrpfList.get(i).getChdrpfx());
                  ps.setString(i++, sttrpfList.get(i).getChdrcoy());
                  ps.setString(i++, sttrpfList.get(i).getChdrnum());
                  ps.setInt(i++, sttrpfList.get(i).getTranno());
                  ps.setString(i++, sttrpfList.get(i).getBatccoy());
                  ps.setString(i++, sttrpfList.get(i).getBatcbrn());
                  ps.setInt(i++, sttrpfList.get(i).getBatcactyr());
                  ps.setInt(i++, sttrpfList.get(i).getBatcactmn());
                  ps.setString(i++, sttrpfList.get(i).getBatctrcde());
                  ps.setString(i++, sttrpfList.get(i).getBatcbatch());
                  ps.setString(i++, sttrpfList.get(i).getRevtrcde());
                  ps.setString(i++, sttrpfList.get(i).getBillfreq());
                  ps.setString(i++, sttrpfList.get(i).getStatgov());
                  ps.setString(i++, sttrpfList.get(i).getStatagt());
                  ps.setString(i++, sttrpfList.get(i).getAgntnum());
                  ps.setString(i++, sttrpfList.get(i).getAracde());
                  ps.setString(i++, sttrpfList.get(i).getCntbranch());
                  ps.setString(i++, sttrpfList.get(i).getBandage());
                  ps.setString(i++, sttrpfList.get(i).getBandsa());
                  ps.setString(i++, sttrpfList.get(i).getBandprm());
                  ps.setString(i++, sttrpfList.get(i).getBandtrm());
                  ps.setString(i++, sttrpfList.get(i).getCnttype());
                  ps.setString(i++, sttrpfList.get(i).getCrtable());
                  ps.setString(i++, sttrpfList.get(i).getParind());
                  ps.setInt(i++, sttrpfList.get(i).getCommyr());
                  ps.setString(i++, sttrpfList.get(i).getCnPremStat());
                  ps.setString(i++, sttrpfList.get(i).getPstatcode());
                  ps.setString(i++, sttrpfList.get(i).getCntcurr());
                  ps.setString(i++, sttrpfList.get(i).getRegister());
                  ps.setString(i++, sttrpfList.get(i).getStatFund());
                  ps.setString(i++, sttrpfList.get(i).getStatSect());
                  ps.setString(i++, sttrpfList.get(i).getStatSubsect());
                  ps.setString(i++, sttrpfList.get(i).getBandageg());
                  ps.setString(i++, sttrpfList.get(i).getBandsag());
                  ps.setString(i++, sttrpfList.get(i).getBandprmg());
                  ps.setString(i++, sttrpfList.get(i).getBandtrmg());
                  ps.setString(i++, sttrpfList.get(i).getSrcebus());
                  ps.setString(i++, sttrpfList.get(i).getSex());
                  ps.setString(i++, sttrpfList.get(i).getMortcls());
                  ps.setString(i++, sttrpfList.get(i).getReptcd01());
                  ps.setString(i++, sttrpfList.get(i).getReptcd02());
                  ps.setString(i++, sttrpfList.get(i).getReptcd03());
                  ps.setString(i++, sttrpfList.get(i).getReptcd04());
                  ps.setString(i++, sttrpfList.get(i).getReptcd05());
                  ps.setString(i++, sttrpfList.get(i).getReptcd06());
                  ps.setString(i++, sttrpfList.get(i).getChdrstcda());
                  ps.setString(i++, sttrpfList.get(i).getChdrstcdb());
                  ps.setString(i++, sttrpfList.get(i).getChdrstcdc());
                  ps.setString(i++, sttrpfList.get(i).getChdrstcdd());
                  ps.setString(i++, sttrpfList.get(i).getChdrstcde());
                  ps.setString(i++, sttrpfList.get(i).getOvrdcat());
                  ps.setString(i++, sttrpfList.get(i).getStatcat());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStcmth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStvmth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStpmth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStsmth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStmmth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStimth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStcmthg());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStpmthg());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStsmthg());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStumth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStnmth());
                  ps.setInt(i++, sttrpfList.get(i).getTransactionDate());
                  ps.setInt(i++, sttrpfList.get(i).getTrannor());
                  ps.setString(i++, sttrpfList.get(i).getLife());
                  ps.setString(i++, sttrpfList.get(i).getCoverage());
                  ps.setString(i++, sttrpfList.get(i).getRider());
                  ps.setInt(i++, sttrpfList.get(i).getPlanSuffix());
                  ps.setString(i++, sttrpfList.get(i).getAcctccy());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStlmth());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStbmthg());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStldmthg());
                  ps.setBigDecimal(i++, sttrpfList.get(i).getStraamt());
            }   
      });  
	}

}
