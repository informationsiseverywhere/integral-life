package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5646 {
  	private List<Integer> ageIssageFrms;
  	private List<Integer> ageIssageTos;
  	private Integer agelimit;
  	private List<BigDecimal> factorsas;
	public List<Integer> getAgeIssageFrms() {
		return ageIssageFrms;
	}
	public void setAgeIssageFrms(List<Integer> ageIssageFrms) {
		this.ageIssageFrms = ageIssageFrms;
	}
	public List<Integer> getAgeIssageTos() {
		return ageIssageTos;
	}
	public void setAgeIssageTos(List<Integer> ageIssageTos) {
		this.ageIssageTos = ageIssageTos;
	}
	public Integer getAgelimit() {
		return agelimit;
	}
	public void setAgelimit(Integer agelimit) {
		this.agelimit = agelimit;
	}
	public List<BigDecimal> getFactorsas() {
		return factorsas;
	}
	public void setFactorsas(List<BigDecimal> factorsas) {
		this.factorsas = factorsas;
	}
  	

}
