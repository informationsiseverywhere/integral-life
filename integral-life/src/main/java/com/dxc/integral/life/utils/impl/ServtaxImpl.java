package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.TxcalcDTO;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.smarttable.pojo.Tr52e;
import com.dxc.integral.life.smarttable.pojo.Tr52f;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Servtax;

/**
 * @author wli31
 */
@Service("servtax")
@Lazy
public class ServtaxImpl implements Servtax {
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;
	@Autowired
	private CovrpfDAO covrpfDAO;
	@Autowired
	private Zrdecplc zrdecplc;
	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private Lifacmv lifacmv;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	
	private String wsaaItmitm = "";
	private String wsaaRldgacct="";
	private Tr52e tr52eIO = null;
	private Tr52f tr52fIO = null;
	private T5645 t5645IO = null;
	private T5688 t5688IO = null;
	private Descpf descpf = new Descpf();
	private List<String> taxSacstyps = null;
	private List<String> substituteCodes = null;
	private int wsaaCount = 0;
	@Override
	public TxcalcDTO processServtax(TxcalcDTO txcalcDTO) throws IOException {
		txcalcDTO.setStatuz("****");
		if ("".equals(txcalcDTO.getRegister())) {
			readChdrenq100(txcalcDTO);
		}
		if ("****".equals(txcalcDTO.getStatuz())) {
			if("".equals(txcalcDTO.getTaxrule())) {
				taxruleCrit200(txcalcDTO);
			}
			mainProc400(txcalcDTO);
			return txcalcDTO;
		} 
		return txcalcDTO;
	}
	
	protected void readChdrenq100(TxcalcDTO txcalcDTO){
		Chdrpf chdrenqIO = chdrpfDAO.getLPContractInfo(txcalcDTO.getChdrcoy(), txcalcDTO.getChdrnum());
		if (chdrenqIO == null) {
			txcalcDTO.setStatuz("MRNF");
			return;
		}
		txcalcDTO.setRegister(chdrenqIO.getReg());
		txcalcDTO.setCnttype(chdrenqIO.getCnttype());
	}
	
	protected void mainProc400(TxcalcDTO txcalcDTO) throws IOException
	{
		if ("".equals(wsaaItmitm)) {
			wsaaItmitm = txcalcDTO.getTaxrule();
			tr52eIO = readTr52e300(txcalcDTO,wsaaItmitm);
		}
		if ("PREM".equals(txcalcDTO.getTransType())){
			wsaaCount = 1;
		}
		if ((wsaaCount != 0 && !"Y".equals(tr52eIO.getTaxinds().get(wsaaCount))) || wsaaCount == 0) {
			List<BigDecimal> taxAmts = new ArrayList<BigDecimal>();
			taxAmts.add(0, BigDecimal.ZERO);
			taxAmts.add(1, BigDecimal.ZERO);
			txcalcDTO.setTaxAmts(taxAmts);
			return ;
		}
		tr52fIO = readTr52f500(txcalcDTO);
		calcCpst400(txcalcDTO);
		return ;
	}
	protected void calcCpst400(TxcalcDTO txcalcDTO) throws IOException{
		if ("CALC".equals(txcalcDTO.getFunction()) || "CPST".equals(txcalcDTO.getFunction())) {
			if ("PREM".equals(txcalcDTO.getTransType()) && txcalcDTO.getAmountIn() == BigDecimal.ZERO) {
				Covrpf covrenqIO = new Covrpf();
				covrenqIO.setChdrcoy(txcalcDTO.getChdrcoy());
				covrenqIO.setChdrnum(txcalcDTO.getChdrnum());
				covrenqIO.setLife(txcalcDTO.getLife());
				covrenqIO.setCoverage(txcalcDTO.getCoverage());
				covrenqIO.setRider(txcalcDTO.getRider());
				covrenqIO.setPlanSuffix(0);
				covrenqIO.setPlnsfx(0);
				Covrpf covr = covrpfDAO.readCovrenqData(covrenqIO);
				if (covr == null) {
					txcalcDTO.setAmountIn(BigDecimal.ZERO);
					return ;
				}
				if (covr.getInstprem().compareTo(BigDecimal.ZERO) == 0) {
					txcalcDTO.setAmountIn(covr.getSingp());
				}
				else {
					if ("Y".equals(tr52eIO.getZbastyp())) {
						txcalcDTO.setAmountIn(covr.getInstprem().subtract(covr.getZlinstprem()).setScale(2));
					}
					else {
						txcalcDTO.setAmountIn(covr.getInstprem());
					}
				}
			}
			for(int i = 0; i < 5; i++ ){
				if (tr52fIO.getDtyamts().get(i).compareTo(txcalcDTO.getAmountIn()) != -1 ) {
					List<BigDecimal> taxamts = new ArrayList<BigDecimal>();
					if (tr52fIO.getTxrateas().get(i).compareTo(BigDecimal.ZERO) != 0 ) {
						txcalcDTO.setTaxAmt01(txcalcDTO.getAmountIn().multiply(tr52fIO.getTxrateas().get(i)).divide(new BigDecimal(100).setScale(3,BigDecimal.ROUND_HALF_UP)));
						//txcalcDTO.setTaxAmts(taxamts);
						
					}
					if (tr52fIO.getTxrateas().get(i).compareTo(BigDecimal.ZERO) == 0) {
						txcalcDTO.setTaxAmt01(tr52fIO.getTxfxamtas().get(i));
						//txcalcDTO.setTaxAmts(taxamts);
					}
					
					List<String> taxTypes = new ArrayList<String>();
					taxTypes.add(0,tr52fIO.getTxtypes().get(0));
					List<String> taxAbsorbs = new ArrayList<String>();
					taxAbsorbs.add(0,tr52fIO.getTxabsinds().get(0));
					txcalcDTO.setTaxTypes(taxTypes);
					txcalcDTO.setTaxAbsorbs(taxAbsorbs);
					
					if (tr52fIO.getTxratebs().get(i).compareTo(BigDecimal.ZERO) != 0) {
						txcalcDTO.setTaxAmt02(txcalcDTO.getAmountIn().multiply(tr52fIO.getTxratebs().get(i)).divide(new BigDecimal(100)).setScale(3, BigDecimal.ROUND_HALF_UP));
//						txcalcDTO.setTaxAmts(taxamts);
					}
					if (tr52fIO.getTxratebs().get(i).compareTo(BigDecimal.ZERO) == 0) {
						txcalcDTO.setTaxAmt02(tr52fIO.getTxfxamtbs().get(i));
//						txcalcDTO.setTaxAmts(taxamts);
					}
					if (txcalcDTO.getTaxAmts().get(0).compareTo(BigDecimal.ZERO) != 0 && !"".equals(txcalcDTO.getCcy())) {
						BigDecimal newAmount = a000CallRounding(txcalcDTO,txcalcDTO.getTaxAmts().get(0));
						txcalcDTO.setTaxAmt01(newAmount);
//						txcalcDTO.setTaxAmts(taxamts);
					}
					if (txcalcDTO.getTaxAmts().get(1).compareTo(BigDecimal.ZERO) != 0 && !"".equals(txcalcDTO.getCcy())) {
						BigDecimal newAmount = a000CallRounding(txcalcDTO,txcalcDTO.getTaxAmts().get(1));
						txcalcDTO.setTaxAmt02(newAmount);
//						txcalcDTO.setTaxAmts(taxamts);
					}
					
					taxamts.add(0,txcalcDTO.getTaxAmt01());
					taxamts.add(1,txcalcDTO.getTaxAmt02());
					txcalcDTO.setTaxAmts(taxamts);
					taxTypes.add(1,tr52fIO.getTxtypes().get(1));
					taxAbsorbs.add(1,tr52fIO.getTxabsinds().get(1));
					txcalcDTO.setTaxTypes(taxTypes);
					txcalcDTO.setTaxAbsorbs(taxAbsorbs);
					break;
				}
			}
			if (!("POST".equals(txcalcDTO.getFunction()) || "CPST".equals(txcalcDTO.getFunction()))) {	
				return ;
			}
//			else {
//				readT5645700(txcalcDTO);
//				readT5688800(txcalcDTO);
//				if ("Y".equals(t5688IO.getComlvlacc()) && !"".equals(txcalcDTO.getLife()) && !"".equals(txcalcDTO.getCoverage())
//						 && !"".equals(txcalcDTO.getRider())){
//					wsaaRldgacct = txcalcDTO.getChdrnum().concat(txcalcDTO.getLife()).concat(txcalcDTO.getCoverage()).concat(txcalcDTO.getRider())
//							.concat(String.valueOf(txcalcDTO.getPlanSuffix()));
//				}else {
//					wsaaRldgacct = txcalcDTO.getChdrnum();
//				}
//				if ("Y".equals(t5688IO.getComlvlacc())){
//					if ("Y".equals(txcalcDTO.getCntTaxInd())) {
//						//contractLevelPosting400();
//						return;
//					}
//				}
//				else {
//					//contractLevelPosting400();
//					return;
//				}
//			}
		}
	}
	
	protected void taxruleCrit200(TxcalcDTO txcalcDTO) throws IOException{
		if ("".equals(txcalcDTO.getCrtable())) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(txcalcDTO.getTxcode() == null ? "" :txcalcDTO.getTxcode());
			stringVariable1.append(txcalcDTO.getCnttype() == null ? "" :txcalcDTO.getCnttype() );
			stringVariable1.append("****");
			wsaaItmitm = stringVariable1.toString();
			tr52eIO = readTr52e300(txcalcDTO,wsaaItmitm);
			if (tr52eIO == null) {
				StringBuilder stringVariable2 = new StringBuilder();
				stringVariable2.append(txcalcDTO.getTxcode() == null ? "" :txcalcDTO.getTxcode());
				stringVariable2.append("***");
				stringVariable2.append("****");
				wsaaItmitm = stringVariable2.toString();
				tr52eIO = readTr52e300(txcalcDTO,wsaaItmitm);
			}
		}
		if (!"".equals(txcalcDTO.getCrtable())) {
			StringBuilder stringVariable3 = new StringBuilder();
			stringVariable3.append(txcalcDTO.getTxcode() == null ? "" :txcalcDTO.getTxcode());
			stringVariable3.append(txcalcDTO.getCnttype() == null ? "" :txcalcDTO.getCnttype() );
			stringVariable3.append(txcalcDTO.getCrtable() == null ? "" :txcalcDTO.getCrtable());
			wsaaItmitm = stringVariable3.toString();
			tr52eIO = readTr52e300(txcalcDTO,wsaaItmitm);
			if (tr52eIO == null) {
				StringBuilder stringVariable4 = new StringBuilder();
				stringVariable4.append(txcalcDTO.getTxcode() == null ? "" :txcalcDTO.getTxcode());
				stringVariable4.append(txcalcDTO.getCnttype() == null ? "" :txcalcDTO.getCnttype() );
				stringVariable4.append("****");
				wsaaItmitm = stringVariable4.toString();
				tr52eIO = readTr52e300(txcalcDTO,wsaaItmitm);
				if (tr52eIO == null) {
					StringBuilder stringVariable5 = new StringBuilder();
					stringVariable5.append(txcalcDTO.getTxcode() == null ? "" :txcalcDTO.getTxcode());
					stringVariable5.append("***");
					stringVariable5.append("****");
					wsaaItmitm = stringVariable5.toString();
					tr52eIO = readTr52e300(txcalcDTO,wsaaItmitm);
				}
			}
		}
		txcalcDTO.setTaxrule(wsaaItmitm);
		StringBuilder stringVariable6 = new StringBuilder();
		stringVariable6.append(txcalcDTO.getCcy() == null ? "" :txcalcDTO.getCcy());
		stringVariable6.append(tr52eIO.getTxitem() == null ? "" :tr52eIO.getTxitem());
		txcalcDTO.setRateItem(stringVariable6.toString());
	}
	protected Tr52e readTr52e300(TxcalcDTO txcalcDTO, String itemitem) throws IOException{
		List<Itempf> tr52eList = itempfDAO.readSmartTableByTrimItemList(txcalcDTO.getChdrcoy(), "TR52E", itemitem,CommonConstants.IT_ITEMPFX);
		if(tr52eList == null || tr52eList.size() <= 0){
			throw new ItemNotfoundException("Item "+itemitem+" not found in Table TR52E");
		}
		tr52eIO = smartTableDataUtils.convertGenareaToPojo(tr52eList.get(0).getGenareaj(),Tr52e.class);
		return tr52eIO;
	}
	protected Tr52f readTr52f500(TxcalcDTO txcalcDTO) throws IOException{
		List<Itempf> tr52fList = itempfDAO.readSmartTableByTrimItemList(txcalcDTO.getChdrcoy(), "TR52F", txcalcDTO.getRateItem().trim(),CommonConstants.IT_ITEMPFX);
		/*if(tr52fList == null || tr52fList.size() <= 0){
			throw new ItemNotfoundException("Item "+wsaaItmitm+" not found in Table TR52E");
		}*/
		if(!tr52fList.isEmpty())
		tr52fIO = smartTableDataUtils.convertGenareaToPojo(tr52fList.get(0).getGenareaj(),Tr52f.class);
		return tr52fIO;
	}
	protected BigDecimal a000CallRounding(TxcalcDTO txcalcDTO,BigDecimal amountIn) throws IOException{
		ZrdecplcDTO zrdecplDTO = new ZrdecplcDTO();
		zrdecplDTO.setCompany(txcalcDTO.getChdrcoy());
		zrdecplDTO.setCurrency(txcalcDTO.getCcy());
		zrdecplDTO.setBatctrcde(txcalcDTO.getBatctrcde());
		zrdecplDTO.setAmountIn(amountIn);
		return zrdecplc.convertAmount(zrdecplDTO);
	}
	protected void readT5645700(TxcalcDTO txcalcDTO) throws IOException{
		List<Itempf> t5645List = itempfDAO.readSmartTableByTrimItemList(txcalcDTO.getChdrcoy(), "T5645", "SERVTAX",CommonConstants.IT_ITEMPFX);
		if(t5645List == null || t5645List.size() <= 0){
			throw new ItemNotfoundException("Item SERVTAX not found in Table T5645");
		}
		t5645IO = smartTableDataUtils.convertGenareaToPojo(t5645List.get(0).getGenareaj(),T5645.class);
		descpf = descpfDAO.getDescInfo(txcalcDTO.getChdrcoy(), "T5645", "SERVTAX");
		if (null==descpf){ 
			descpf = new Descpf();
			descpf.setLongdesc("??????????????????????????????");
		}
	}
	protected void readT5688800(TxcalcDTO txcalcDTO) throws IOException{
		List<Itempf> t5688List = itempfDAO.readSmartTableByTrimItemList(txcalcDTO.getChdrcoy(), "T5688", txcalcDTO.getCnttype(),CommonConstants.IT_ITEMPFX);
		if(t5688List == null || t5688List.size() <= 0){
			throw new ItemNotfoundException("Item "+txcalcDTO.getCnttype()+" not found in Table T5688");
		}
		t5688IO = smartTableDataUtils.convertGenareaToPojo(t5688List.get(0).getGenareaj(),T5688.class);
	}
	protected void contractLevelPosting400(TxcalcDTO txcalcDTO) throws IOException, ParseException{
		LifacmvDTO lifacmvDTO = new LifacmvDTO();
		lifacmvDTO.setOrigamt(txcalcDTO.getTaxAmts().get(0));
		lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(2));
		lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(2));
		taxSacstyps = new ArrayList<String>();
		taxSacstyps.add(0,t5645IO.getSacstypes().get(2));
		txcalcDTO.setTaxSacstyps(taxSacstyps);
		lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(2));
		lifacmvDTO.setGlsign(t5645IO.getSigns().get(2));
		lifacmvDTO.setContot(t5645IO.getCnttots().get(2));
		lifacmvDTO.setRldgacct(wsaaRldgacct);
		substituteCodes = new ArrayList<String>();
		substituteCodes.add(0,txcalcDTO.getCnttype());
		substituteCodes.add(5,"");
		lifacmvDTO.setSubstituteCodes(substituteCodes);
		writeAcmv900(lifacmvDTO,txcalcDTO);
		lifacmvDTO.setOrigamt(txcalcDTO.getTaxAmts().get(1));
		lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(1));
		lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(3));
		taxSacstyps.add(1,t5645IO.getSacstypes().get(3));
		txcalcDTO.setTaxSacstyps(taxSacstyps);
		
		lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(3));
		lifacmvDTO.setGlsign(t5645IO.getSigns().get(3));
		lifacmvDTO.setContot(t5645IO.getCnttots().get(3));
		lifacmvDTO.setRldgacct(wsaaRldgacct);
		lifacmvDTO.setSubstituteCodes(substituteCodes);
		writeAcmv900(lifacmvDTO,txcalcDTO);
		
		if ("Y".equals(txcalcDTO.getTaxAbsorbs().get(0))) {
			lifacmvDTO.setOrigamt(txcalcDTO.getTaxAmts().get(0));
			lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(6));
			lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(6));
			lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(6));
			lifacmvDTO.setGlsign(t5645IO.getSigns().get(6));
			lifacmvDTO.setContot(t5645IO.getCnttots().get(6));
			lifacmvDTO.setRldgacct(wsaaRldgacct);
			lifacmvDTO.setRldgacct(txcalcDTO.getChdrnum());
			lifacmvDTO.setSubstituteCodes(substituteCodes);

			writeAcmv900(lifacmvDTO,txcalcDTO);
		}
		if ("Y".equals(txcalcDTO.getTaxAbsorbs().get(1))) {
			lifacmvDTO.setOrigamt(txcalcDTO.getTaxAmts().get(1));
			lifacmvDTO.setSacscode(t5645IO.getSacscodes().get(7));
			lifacmvDTO.setSacstyp(t5645IO.getSacstypes().get(7));
			lifacmvDTO.setGlcode(t5645IO.getGlmaps().get(7));
			lifacmvDTO.setGlsign(t5645IO.getSigns().get(7));
			lifacmvDTO.setContot(t5645IO.getCnttots().get(7));
			lifacmvDTO.setRldgacct(wsaaRldgacct);
			lifacmvDTO.setSubstituteCodes(substituteCodes);

			writeAcmv900(lifacmvDTO,txcalcDTO);
		}
	}
	protected void writeAcmv900(LifacmvDTO lifacmvDTO,TxcalcDTO txcalcDTO) throws IOException, ParseException{
		if (lifacmvDTO.getOrigamt().compareTo(BigDecimal.ZERO) == 0) {
			return ;
		}
		
		lifacmvDTO.setCrate(BigDecimal.ZERO);
		lifacmvDTO.setRcamt(BigDecimal.ZERO);
		lifacmvDTO.setUser(0);
		lifacmvDTO.setGenlcur("");
		lifacmvDTO.setAcctamt(BigDecimal.ZERO);
		lifacmvDTO.setFrcdate(99999999);
		lifacmvDTO.setEffdate(txcalcDTO.getEffdate());
		txcalcDTO.setJrnseq(txcalcDTO.getJrnseq() + 1);
		
		lifacmvDTO.setJrnseq(txcalcDTO.getJrnseq());
		lifacmvDTO.setTranno(txcalcDTO.getTranno());
		lifacmvDTO.setRdocnum(txcalcDTO.getChdrnum());
		lifacmvDTO.setBatccoy(txcalcDTO.getChdrcoy());
		lifacmvDTO.setRldgcoy(txcalcDTO.getChdrcoy());
		lifacmvDTO.setGenlcoy(txcalcDTO.getChdrcoy());
		lifacmvDTO.setTrandesc(descpf.getLongdesc());
		lifacmvDTO.setOrigcurr(txcalcDTO.getCcy());
		lifacmv.executePSTW(lifacmvDTO);
	}
}
