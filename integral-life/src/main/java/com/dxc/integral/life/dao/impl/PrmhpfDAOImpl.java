package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.PrmhpfDAO;

@Repository("prmhpfDAO")
@Lazy
public class PrmhpfDAOImpl extends BaseDAOImpl implements PrmhpfDAO {
	@Override
	public int deletePrmhpf(String chdrcoy, String chdrnum, int tranno) {
		String sql = "DELETE FROM PRMHPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? AND VALIDFLAG=? AND ACTIND=?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, tranno, "1", "A"});
	}
	
	

}
