package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Nlgtpf;
/**
 * @author wli31
 */
public interface NlgtpfDAO  {
	public List<Nlgtpf> readNlgtpf(String chdrcoy, String chdrnum);
	public Nlgtpf getNlgRecord(String chdrcoy,String chdrnum,String validflag);
	public int insertNlgtpf(Nlgtpf nlgtpf);
	
}
