package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class T5671 {

	private List<String> edtitms;
	private List<String> pgms;
	private List<String> subprogs;
	private List<String> trevsubs;
	public List<String> getEdtitms() {
		return edtitms;
	}
	public void setEdtitms(List<String> edtitms) {
		this.edtitms = edtitms;
	}
	public List<String> getPgms() {
		return pgms;
	}
	public void setPgms(List<String> pgms) {
		this.pgms = pgms;
	}
	public List<String> getSubprogs() {
		return subprogs;
	}
	public void setSubprogs(List<String> subprogs) {
		this.subprogs = subprogs;
	}
	public List<String> getTrevsubs() {
		return trevsubs;
	}
	public void setTrevsubs(List<String> trevsubs) {
		this.trevsubs = trevsubs;
	}
	
}