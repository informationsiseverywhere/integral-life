package com.dxc.integral.life.utils;

import java.io.IOException;

import java.text.ParseException;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.life.beans.TotloanInputDTO;
import com.dxc.integral.life.beans.TotloanOutputDTO;

/**
 * Hrtotlon utility.
 * 
 * @author mmalik25
 *
 */

@FunctionalInterface
public interface Hrtotlon {


	/**
	 * Calculates the total loan and interest
	 * 
	 * @param totloanDTO
	 * @param chdr
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	TotloanOutputDTO processLoanList(TotloanInputDTO totloanDTO, Chdrpf chdr) throws IOException, ParseException ;//ILIFE-5763

	
}
