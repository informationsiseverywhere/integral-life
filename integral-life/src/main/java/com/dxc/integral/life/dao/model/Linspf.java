package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class Linspf {
    private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String cntcurr;
	private String validflag;
	private String branch;
	private int instfrom;
	private int instto;
	private BigDecimal instamt01;
	private BigDecimal instamt02;
	private BigDecimal instamt03;
	private BigDecimal instamt04;
	private BigDecimal instamt05;
	private BigDecimal instamt06;
	private String instfreq;
	private String instjctl;
	private String billchnl;
	private String payflag;
	private String dueflg;
	private String transcode;
	private BigDecimal cbillamt;
	private String billcurr;
	private String mandref;
	private int billcd;
	private int payrseqno;
	private String taxrelmth;
	private String acctmeth;
	private String userProfile;
	private String datime;
	private String jobName;
	
    public long getUnique_number() {
        return unique_number;
    }
    public void setUnique_number(long unique_number) {
        this.unique_number = unique_number;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public String getCntcurr() {
        return cntcurr;
    }
    public String getValidflag() {
        return validflag;
    }
    public String getBranch() {
        return branch;
    }
    public int getInstfrom() {
        return instfrom;
    }
    public int getInstto() {
        return instto;
    }
    public BigDecimal getInstamt01() {
        return instamt01;
    }
    public BigDecimal getInstamt02() {
        return instamt02;
    }
    public BigDecimal getInstamt03() {
        return instamt03;
    }
    public BigDecimal getInstamt04() {
        return instamt04;
    }
    public BigDecimal getInstamt05() {
        return instamt05;
    }
    public BigDecimal getInstamt06() {
        return instamt06;
    }
    public String getInstfreq() {
        return instfreq;
    }
    public String getInstjctl() {
        return instjctl;
    }
    public String getBillchnl() {
        return billchnl;
    }
    public String getPayflag() {
        return payflag;
    }
    public String getDueflg() {
        return dueflg;
    }
    public String getTranscode() {
        return transcode;
    }
    public BigDecimal getCbillamt() {
        return cbillamt;
    }
    public String getBillcurr() {
        return billcurr;
    }
    public String getMandref() {
        return mandref;
    }
    public int getBillcd() {
        return billcd;
    }
    public int getPayrseqno() {
        return payrseqno;
    }
    public String getTaxrelmth() {
        return taxrelmth;
    }
    public String getAcctmeth() {
        return acctmeth;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public String getDatime() {
        return datime;
    }
    public String getJobName() {
        return jobName;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public void setCntcurr(String cntcurr) {
        this.cntcurr = cntcurr;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public void setInstfrom(int instfrom) {
        this.instfrom = instfrom;
    }
    public void setInstto(int instto) {
        this.instto = instto;
    }
    public void setInstamt01(BigDecimal instamt01) {
        this.instamt01 = instamt01;
    }
    public void setInstamt02(BigDecimal instamt02) {
        this.instamt02 = instamt02;
    }
    public void setInstamt03(BigDecimal instamt03) {
        this.instamt03 = instamt03;
    }
    public void setInstamt04(BigDecimal instamt04) {
        this.instamt04 = instamt04;
    }
    public void setInstamt05(BigDecimal instamt05) {
        this.instamt05 = instamt05;
    }
    public void setInstamt06(BigDecimal instamt06) {
        this.instamt06 = instamt06;
    }
    public void setInstfreq(String instfreq) {
        this.instfreq = instfreq;
    }
    public void setInstjctl(String instjctl) {
        this.instjctl = instjctl;
    }
    public void setBillchnl(String billchnl) {
        this.billchnl = billchnl;
    }
    public void setPayflag(String payflag) {
        this.payflag = payflag;
    }
    public void setDueflg(String dueflg) {
        this.dueflg = dueflg;
    }
    public void setTranscode(String transcode) {
        this.transcode = transcode;
    }
    public void setCbillamt(BigDecimal cbillamt) {
        this.cbillamt = cbillamt;
    }
    public void setBillcurr(String billcurr) {
        this.billcurr = billcurr;
    }
    public void setMandref(String mandref) {
        this.mandref = mandref;
    }
    public void setBillcd(int billcd) {
        this.billcd = billcd;
    }
    public void setPayrseqno(int payrseqno) {
        this.payrseqno = payrseqno;
    }
    public void setTaxrelmth(String taxrelmth) {
        this.taxrelmth = taxrelmth;
    }
    public void setAcctmeth(String acctmeth) {
        this.acctmeth = acctmeth;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
}