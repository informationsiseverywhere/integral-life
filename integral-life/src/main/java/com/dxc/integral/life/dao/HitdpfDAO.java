package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Hitdpf;

public interface HitdpfDAO {

	public List<Hitdpf> getHitdpfRecords(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, int tranno);
	public Hitdpf getHitdRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, String zintbfnd);
	public int updateHitdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, String zintbfnd);
	public int deleteHitdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx, String zintbfnd);
}
