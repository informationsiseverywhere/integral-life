package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class Th609 {
 	
  	private String fupcdes;
  	private String indic;
  	private String language;
  	private BigDecimal prcnt;
  	private String taxind;
  	private String filler;
	public String getFupcdes() {
		return fupcdes;
	}
	public void setFupcdes(String fupcdes) {
		this.fupcdes = fupcdes;
	}
	public String getIndic() {
		return indic;
	}
	public void setIndic(String indic) {
		this.indic = indic;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public BigDecimal getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(BigDecimal prcnt) {
		this.prcnt = prcnt;
	}
	public String getTaxind() {
		return taxind;
	}
	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
  	

}
