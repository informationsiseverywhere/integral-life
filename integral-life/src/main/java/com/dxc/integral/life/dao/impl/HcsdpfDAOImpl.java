package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HcsdpfDAO;
import com.dxc.integral.life.dao.model.Hcsdpf;

/**
 * 
 * HcsdpfDAO implementation for database table <b>Hcsdpf</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("hcsdpfDAO")
public class HcsdpfDAOImpl extends BaseDAOImpl implements HcsdpfDAO {

	@Override
	public List<Hcsdpf> readHcsdpfRecords(String company, String contractNumber){
		StringBuilder sql = new StringBuilder("SELECT * FROM HCSDPF WHERE VALIDFLAG = '1' AND CHDRCOY=? AND CHDRNUM=? ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Hcsdpf>(Hcsdpf.class));
	}
	
	@Override
	public Hcsdpf readHcsdpfRecordsWithoutFlag(String company, String contractNumber){
		String sql = "SELECT * FROM HCSDPF WHERE CHDRCOY=? AND CHDRNUM=? ORDER BY CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC";

		return jdbcTemplate.queryForObject(sql, new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Hcsdpf>(Hcsdpf.class));
	}
	
	@Override
	public Hcsdpf readHcsdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
		StringBuilder sql = new StringBuilder("SELECT * FROM HCSDPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.queryForObject(sql.toString(), new Object[] { chdrcoy, chdrnum, life, coverage, rider, plnsfx},
				new BeanPropertyRowMapper<Hcsdpf>(Hcsdpf.class));
	}
	
	@Override
	public int updateHcsdpfRecord(String validflag, String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
		String sql = "UPDATE HCSDPF SET VALIDFLAG=? WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?";
		return jdbcTemplate.update(sql, new Object[] { chdrcoy, chdrnum, life, coverage, rider, plnsfx});//IJTI-1415
	}
	
	@Override
	public int deleteHcsdpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
		String sql = "DELETE FROM WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ";

		return jdbcTemplate.update(sql, new Object[] { chdrcoy, chdrnum, life, coverage, rider, plnsfx});
	}
}
