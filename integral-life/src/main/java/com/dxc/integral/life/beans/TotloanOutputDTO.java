package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * Output DTO for Hrtotloan utility.
 * 
 * @author mmalik25
 */
public class TotloanOutputDTO {

	/** The loanCount */
	private int loanCount;
	/** The principal */
	private BigDecimal principal;
	/** The interest */
	private BigDecimal interest;

	/**
	 * @return the loanCount
	 */
	public int getLoanCount() {
		return loanCount;
	}

	/**
	 * @param loanCount
	 *            the loanCount to set
	 */
	public void setLoanCount(int loanCount) {
		this.loanCount = loanCount;
	}

	/**
	 * @return the principal
	 */
	public BigDecimal getPrincipal() {
		return principal;
	}

	/**
	 * @param principal
	 *            the principal to set
	 */
	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	/**
	 * @return the interest
	 */
	public BigDecimal getInterest() {
		return interest;
	}

	/**
	 * @param interest
	 *            the interest to set
	 */
	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

}
