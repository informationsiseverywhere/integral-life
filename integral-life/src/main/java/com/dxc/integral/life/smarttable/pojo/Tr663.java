package com.dxc.integral.life.smarttable.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class Tr663 {
	private int dteapp;
	private String zparam;
	private String zprofile;
	private String ztaxid;
	private String filler;
	
	public int getDteapp() {
		return dteapp;
	}
	public void setDteapp(int dteapp) {
		this.dteapp = dteapp;
	}
	public String getZparam() {
		return zparam;
	}
	public void setZparam(String zparam) {
		this.zparam = zparam;
	}
	public String getZprofile() {
		return zprofile;
	}
	public void setZprofile(String zprofile) {
		this.zprofile = zprofile;
	}
	public String getZtaxid() {
		return ztaxid;
	}
	public void setZtaxid(String ztaxid) {
		this.ztaxid = ztaxid;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
}
