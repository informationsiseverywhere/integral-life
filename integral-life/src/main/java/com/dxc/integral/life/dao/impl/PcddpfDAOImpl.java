package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.PcddpfDAO;
import com.dxc.integral.life.dao.model.Pcddpf;

/**
 * 
 * PcddpfDAO implementation for database table <b>Pcddpf</b> DB operations.
 * 
 *
 */
@Repository("pcddpfDAO")
public class PcddpfDAOImpl extends BaseDAOImpl implements PcddpfDAO {

	@Override
	public List<Pcddpf> getPcddpfRecord(String coy, String chdrnum) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM PCDDPF WHERE ");
		sql.append(" CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG = '1' ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,AGNTNUM ASC,UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { coy, chdrnum },
				new BeanPropertyRowMapper<Pcddpf>(Pcddpf.class));
	}
	@Override
	public List<Pcddpf> getPcddByAgntnum(String coy,String agntnum) {
		String sql = "SELECT * FROM PCDDPF WHERE CHDRCOY=? AND AGNTNUM=? and VALIDFLAG = ?";
		return jdbcTemplate.query(sql, new Object[] { coy, agntnum, 1 },
				new BeanPropertyRowMapper<Pcddpf>(Pcddpf.class));
	}
	
}
