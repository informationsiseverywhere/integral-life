package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LextpfDAO;
import com.dxc.integral.life.dao.model.Lextpf;

/**
 * 
 * LextpfDAO implementation for database table <b>Lextpf</b> DB operations.
 * 
 *
 */
@Repository("lextpfDAO")
public class LextpfDAOImpl extends BaseDAOImpl implements LextpfDAO {

	@Override
	public List<Lextpf> checkLextpfRecord(Lextpf lextpf) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM LEXTPF WHERE ");
		sql.append(" CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND ECESDTE=? ");
		sql.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(
				sql.toString(), new Object[] { lextpf.getChdrcoy(), lextpf.getChdrnum(), lextpf.getLife(),
						lextpf.getCoverage(), lextpf.getRider(), lextpf.getEcesdte() },
				new BeanPropertyRowMapper<Lextpf>(Lextpf.class));
	}

	@Override
	public List<Lextpf> getLextpfListRecord(String chdrcoy, String chdrnum, String life, String coverage,
			String rider) {
		StringBuilder sql = new StringBuilder(" SELECT * FROM LEXTPF WHERE ");
		sql.append(" CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? ");
		sql.append(
				" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { chdrcoy, chdrnum, life, coverage, rider },
				new BeanPropertyRowMapper<Lextpf>(Lextpf.class));
	}
}
