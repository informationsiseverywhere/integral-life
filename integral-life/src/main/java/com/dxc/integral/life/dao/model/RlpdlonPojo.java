/*********************  */
/*Author  :Liwei					*/
/*Purpose :Model for Rlpdlon		*/
/*Date    :2018.10.26				*/
package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;

public class RlpdlonPojo {
  	private String function;
  	private String statuz;
  	private String processed;
  	private String validate;
  	private String pstw;
  	private String sacstype;
  	private String chdrcoy;
  	private String chdrnum;
  	private int tranno;
  	private int transeq;
  	private String longdesc;
  	private int effdate;
  	private String authCode;
  	private String language;
  	private String currency;
  	private BigDecimal prmdepst;
  	private int loanno;
  	private String loantype;
  	private String batchkey;
  	private String contkey;
  	private String prefix;
  	private String company;
  	private String branch;
  	private int actyear;
  	private int actmonth;
  	private String trcde;
  	private String batch;
  	private String filler;
  	private String tranid;
  	private String termid;
  	private int tranidN;
  	private int tranidX;
  	private int date_var;
  	private int time;
  	private int user;
  	private String doctkey;
  	private String doctPrefix;
  	private String doctCompany;
  	private String doctNumber;
  	
	public RlpdlonPojo() {
		function = "";
		statuz = "";
		processed = "";
		validate = "";
		pstw = "";
		sacstype = "";
		chdrcoy = "";
		chdrnum = "";
		tranno = 0;
		transeq = 0;
		longdesc = "";
		effdate = 0;
		authCode = "";
		language = "";
		currency = "";
		prmdepst = BigDecimal.ZERO;
		loanno = 0;
		loantype = "";
		batchkey = "";
		contkey = "";
		prefix = "";
		company = "";
		branch = "";
		actyear = 0;
		actmonth = 0;
		trcde = "";
		batch = "";
		filler = "";
		tranid = "";
		termid = "";
		tranidN = 0;
		tranidX = 0;
		date_var = 0;
		time = 0;
		user = 0;
		doctkey = "";
		doctPrefix = "";
		doctCompany = "";
		doctNumber = "";
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getProcessed() {
		return processed;
	}
	public void setProcessed(String processed) {
		this.processed = processed;
	}
	public String getValidate() {
		return validate;
	}
	public void setValidate(String validate) {
		this.validate = validate;
	}
	public String getPstw() {
		return pstw;
	}
	public void setPstw(String pstw) {
		this.pstw = pstw;
	}
	public String getSacstype() {
		return sacstype;
	}
	public void setSacstype(String sacstype) {
		this.sacstype = sacstype;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getTranseq() {
		return transeq;
	}
	public void setTranseq(int transeq) {
		this.transeq = transeq;
	}
	public String getLongdesc() {
		return longdesc;
	}
	public void setLongdesc(String longdesc) {
		this.longdesc = longdesc;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getPrmdepst() {
		return prmdepst;
	}
	public void setPrmdepst(BigDecimal prmdepst) {
		this.prmdepst = prmdepst;
	}
	public int getLoanno() {
		return loanno;
	}
	public void setLoanno(int loanno) {
		this.loanno = loanno;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public String getBatchkey() {
		return batchkey;
	}
	public void setBatchkey(String batchkey) {
		this.batchkey = batchkey;
	}
	public String getContkey() {
		return contkey;
	}
	public void setContkey(String contkey) {
		this.contkey = contkey;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getActyear() {
		return actyear;
	}
	public void setActyear(int actyear) {
		this.actyear = actyear;
	}
	public int getActmonth() {
		return actmonth;
	}
	public void setActmonth(int actmonth) {
		this.actmonth = actmonth;
	}
	public String getTrcde() {
		return trcde;
	}
	public void setTrcde(String trcde) {
		this.trcde = trcde;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	public String getTranid() {
		return tranid;
	}
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getTranidN() {
		return tranidN;
	}
	public void setTranidN(int tranidN) {
		this.tranidN = tranidN;
	}
	public int getTranidX() {
		return tranidX;
	}
	public void setTranidX(int tranidX) {
		this.tranidX = tranidX;
	}
	public int getDate_var() {
		return date_var;
	}
	public void setDate_var(int date_var) {
		this.date_var = date_var;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getDoctkey() {
		return doctkey;
	}
	public void setDoctkey(String doctkey) {
		this.doctkey = doctkey;
	}
	public String getDoctPrefix() {
		return doctPrefix;
	}
	public void setDoctPrefix(String doctPrefix) {
		this.doctPrefix = doctPrefix;
	}
	public String getDoctCompany() {
		return doctCompany;
	}
	public void setDoctCompany(String doctCompany) {
		this.doctCompany = doctCompany;
	}
	public String getDoctNumber() {
		return doctNumber;
	}
	public void setDoctNumber(String doctNumber) {
		this.doctNumber = doctNumber;
	}
  	
  	
}
