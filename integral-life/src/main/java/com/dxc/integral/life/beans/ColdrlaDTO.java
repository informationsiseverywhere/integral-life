package com.dxc.integral.life.beans;

public class ColdrlaDTO {
	public String memberName = "";
	public String fileName = "";
	public String libraryName = "";
	public String databaseNumber = "";
	public String keyString = "";
	public String numberKeyFields = "";
	public String operationCode = "";
	public String lrIndicator = "";
	public String highIndicator = "";
	public String lowIndicator = "";
	public String equalIndicator = "";
	public String rrn = "";
	public String returnCode = "";
	public String recordBuffer = "";
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getLibraryName() {
		return libraryName;
	}
	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}
	public String getDatabaseNumber() {
		return databaseNumber;
	}
	public void setDatabaseNumber(String databaseNumber) {
		this.databaseNumber = databaseNumber;
	}
	public String getKeyString() {
		return keyString;
	}
	public void setKeyString(String keyString) {
		this.keyString = keyString;
	}
	public String getNumberKeyFields() {
		return numberKeyFields;
	}
	public void setNumberKeyFields(String numberKeyFields) {
		this.numberKeyFields = numberKeyFields;
	}
	public String getOperationCode() {
		return operationCode;
	}
	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}
	public String getLrIndicator() {
		return lrIndicator;
	}
	public void setLrIndicator(String lrIndicator) {
		this.lrIndicator = lrIndicator;
	}
	public String getHighIndicator() {
		return highIndicator;
	}
	public void setHighIndicator(String highIndicator) {
		this.highIndicator = highIndicator;
	}
	public String getLowIndicator() {
		return lowIndicator;
	}
	public void setLowIndicator(String lowIndicator) {
		this.lowIndicator = lowIndicator;
	}
	public String getEqualIndicator() {
		return equalIndicator;
	}
	public void setEqualIndicator(String equalIndicator) {
		this.equalIndicator = equalIndicator;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getRecordBuffer() {
		return recordBuffer;
	}
	public void setRecordBuffer(String recordBuffer) {
		this.recordBuffer = recordBuffer;
	}
	
	
}
