package com.dxc.integral.life.dao.model;


public class Hpuapf {
	
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String  life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int puAddNbr;
	private String validflag;
	private int tranno;
	private int anbAtCcd;
	private int sumin;
	private int singp;
	private int crrcd;
    private String rstatcode;
    private String pstatcode;
    private int riskCessDate;
    private String crtable;
    private String divdParticipant;
    private String userProfile;
    private String  jobName;
    private String datime;
    
	
	public Hpuapf() {
	}
	public Hpuapf(Hpuapf hpuapf){
		this.chdrcoy=hpuapf.chdrcoy;
    	this.chdrnum=hpuapf.chdrnum;
    	this.life=hpuapf.life;
    	this.jlife=hpuapf.jlife;
    	this.coverage=hpuapf.coverage;
    	this.rider=hpuapf.rider;
    	this.planSuffix=hpuapf.planSuffix;
    	this.puAddNbr=hpuapf.puAddNbr;
    	this.validflag=hpuapf.validflag;
    	this.tranno=hpuapf.tranno;
    	this.anbAtCcd=hpuapf.anbAtCcd;
    	this.sumin=hpuapf.sumin;
    	this.singp=hpuapf.singp;
    	this.crrcd=hpuapf.crrcd;
    	this.rstatcode=hpuapf.rstatcode;
    	this.pstatcode=hpuapf.pstatcode;
    	this.riskCessDate=hpuapf.riskCessDate;
    	this.crtable=hpuapf.crtable;
    	this.divdParticipant=hpuapf.divdParticipant;
    	this.userProfile=hpuapf.userProfile;
    	this.jobName=hpuapf.jobName;
    	this.datime=hpuapf.datime;
    
	}
	
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    
    public String getChdrnum() {
        return chdrnum;
    }
    
    public void setLife(String life) {
        this.life = life;
    }
    
    public String getLife() {
        return life;
    }
    
    public void setJLife(String jlife) {
        this.jlife = jlife;
    }
    
    public String getJLife() {
        return jlife;
    }
    
    public String getCoverage() {
        return coverage;
    }
    
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    
    public String getRider() {
        return rider;
    }
    
    public void setRider(String rider) {
        this.rider = rider;
    }
    
    public int getPlanSuffix() {
        return planSuffix;
    }
    
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    
    public int getPuAddNbr() {
		return puAddNbr;
	}
	public void setPuAddNbr(int puAddNbr) {
		this.puAddNbr = puAddNbr;
	}
	public String getValidflag() {
	        return validflag;
    }
	 
	public void setValidflag(String validflag) {
	        this.validflag = validflag;
	}
	public int getTranno() {
			return tranno;
	}
	public void setTranno(int tranno) {
			this.tranno = tranno;
	}
	public int getAnbAtCcd() {
			return anbAtCcd;
	}
	public void setAnbAtCcd(int anbAtCcd) {
			this.anbAtCcd = anbAtCcd;
	}
		
    public int getSumin() {
			return sumin;
	}
	public void setSumin(int sumin) {
			this.sumin = sumin;
	}
				
     public int getSingp() {
		    return singp;
	 }
	public void setSingp(int singp) {
			this.singp = singp;
	}				
				
	public int getCrrcd() {
			return crrcd;
	}
	public void setCrrcd(int crrcd) {
			this.crrcd = crrcd;
	}	
	public String getRstatcode() {
		    return rstatcode;
	}
	public void setRstatcode(String rstatcode) {
			this.rstatcode = rstatcode;
	}
	
	public String getPstatcode() {
		    return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		    this.pstatcode = pstatcode;
	}
	
	public int getRiskCessDate() {
            return riskCessDate;
    }
    public void setRiskCessDate(int riskCessDate) {
            this.riskCessDate = riskCessDate;
    }
    
    public String getCrtable() {
            return crtable;
    }
    public void setCrtable(String crtable) {
            this.crtable = crtable;
    }
    
    public String getDivdParticipant() {
            return divdParticipant;
    }
    public void setDivdParticipant(String divdParticipant) {
            this.divdParticipant = divdParticipant;
    }
    public String getUserProfile() {
            return userProfile;
    }
    public void setUserProfile(String userProfile) {
            this.userProfile = userProfile;
    }
    public String getJobName() {
            return jobName;
    }
    public void setJobName(String jobName) {
            this.jobName = jobName;
    }
    public String getDatime() {
            return datime;
    }
    public void setDatime(String datime) {
            this.datime = datime;
    }
    
}