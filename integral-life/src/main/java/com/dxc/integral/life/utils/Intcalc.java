package com.dxc.integral.life.utils;

import java.io.IOException;

import java.math.BigDecimal;
import java.text.ParseException;

import com.dxc.integral.life.beans.IntcalcDTO;

/**
 * Intcalc utility.
 * 
 * @author mmalik25
 *
 */
@FunctionalInterface
public interface Intcalc {

	/**
	 * Calculate Interest due on a given Loan 
	 * 
	 * @param intcalc
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 *//*ILIFE-5977*/
	BigDecimal interestCalculation(IntcalcDTO intcalc) throws IOException, ParseException ;//ILIFE-5763
}
