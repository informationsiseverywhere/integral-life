package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Rcvdpf;

public interface RcvdpfDAO {
	public Rcvdpf readRcvdpf(Rcvdpf rcvdpf);
}
