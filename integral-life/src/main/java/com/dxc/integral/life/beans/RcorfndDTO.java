package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class RcorfndDTO {
	private String function = "";
	private String statuz = "";
	private String chdrcoy = "";
	private String chdrnum = "";
	private String life = "";
	private String coverage = "";
	private String rider = "";
	private int seqno = 0;
	private int planSuffix = 0;
	private String crtable = "";
	private String cnttype = "";
	private int polsum = 0;
	private int effdate = 0;
	private int currfrom = 0;
	private String batckey = "";
	private String filler = "";
	private String batccoy = "";
	private String batcbrn = "";
	private int batcactyr = 0;
	private int batcactmn = 0;
	private String batctrcde = "";
	private String batcbatch = "";
	private int tranno = 0;
	private BigDecimal newRaAmt;
	private BigDecimal refund;
	private String language = "";
	private BigDecimal refundFee;
	private BigDecimal refundPrem;
	private BigDecimal refundComm;
	
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getPolsum() {
		return polsum;
	}
	public void setPolsum(int polsum) {
		this.polsum = polsum;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public String getBatckey() {
		return batckey;
	}
	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getNewRaAmt() {
		return newRaAmt;
	}
	public void setNewRaAmt(BigDecimal newRaAmt) {
		this.newRaAmt = newRaAmt;
	}
	public BigDecimal getRefund() {
		return refund;
	}
	public void setRefund(BigDecimal refund) {
		this.refund = refund;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public BigDecimal getRefundFee() {
		return refundFee;
	}
	public void setRefundFee(BigDecimal refundFee) {
		this.refundFee = refundFee;
	}
	public BigDecimal getRefundPrem() {
		return refundPrem;
	}
	public void setRefundPrem(BigDecimal refundPrem) {
		this.refundPrem = refundPrem;
	}
	public BigDecimal getRefundComm() {
		return refundComm;
	}
	public void setRefundComm(BigDecimal refundComm) {
		this.refundComm = refundComm;
	}
	
	
}
