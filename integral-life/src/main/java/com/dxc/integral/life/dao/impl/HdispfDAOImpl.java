package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HdispfDAO;
import com.dxc.integral.life.dao.model.Hdispf;
import com.dxc.integral.life.dao.model.Lifepf;

/**
 * 
 * HdispfDAO implementation for database table <b>Hdispf</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("hdispfDAO")
public class HdispfDAOImpl extends BaseDAOImpl implements HdispfDAO {

	@Override
	public List<Hdispf> readHdispfRecords(String company, String contractNumber){
		StringBuilder sql = new StringBuilder("SELECT * FROM HDISPF WHERE VALIDFLAG = '1' AND CHDRCOY=? AND CHDRNUM=? ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Hdispf>(Hdispf.class));
	}
	
	@Override
	public List<Hdispf> readHdispfRecords(String company, String contractNumber, int tranno){
	StringBuilder sql = new StringBuilder("SELECT * FROM HDISPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?");
	sql.append(" ORDER BY CHDRCOY, CHDRNUM, TRANNO, UNIQUE_NUMBER DESC ");

	return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
			new BeanPropertyRowMapper<Hdispf>(Hdispf.class));
   }

   @Override
   public Hdispf readHdispf(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
	   StringBuilder sql = new StringBuilder("SELECT * FROM HDISPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");   
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx}, 
				new BeanPropertyRowMapper<Hdispf>(Hdispf.class));
   }
   @Override 
   public int updateHdispf(Hdispf hdispf) {
	   String sql = "UPDATE HDISPF SET VALIDFLAG=?, HDVSMTDT=?, HDVBALST=?  WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=?  AND PLNSFX=?";
       return jdbcTemplate.update(sql, new Object[] {hdispf.getValidflag(), hdispf.getHdvsmtdt(), hdispf.getHdvbalst(), hdispf.getChdrcoy(), hdispf.getChdrnum(), 
    		   hdispf.getCoverage(), hdispf.getRider(), hdispf.getPlnsfx()});
   }
	

  @Override
  public int deleteHdispfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
	  String sql = "DELETE FROM HDISPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?";
	  return jdbcTemplate.update(sql,  new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx});
  }
}
