package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class T5515 {
	private BigDecimal acumbof;
	private BigDecimal accumRounding;
	private String btobid;
	private String btodisc;
	private BigDecimal discountOfferPercent;
	private String btooff;
	private String currcode;
	private BigDecimal initBidOffer;
	private BigDecimal initialRounding;
	private int initAccumSame;
	private BigDecimal managementCharge;
	private String otoff;
	private BigDecimal tolerance;
	private String unitType;
	private String zfundtyp;
	public BigDecimal getAcumbof() {
		return acumbof;
	}
	public void setAcumbof(BigDecimal acumbof) {
		this.acumbof = acumbof;
	}
	public BigDecimal getAccumRounding() {
		return accumRounding;
	}
	public void setAccumRounding(BigDecimal accumRounding) {
		this.accumRounding = accumRounding;
	}
	public String getBtobid() {
		return btobid;
	}
	public void setBtobid(String btobid) {
		this.btobid = btobid;
	}
	public String getBtodisc() {
		return btodisc;
	}
	public void setBtodisc(String btodisc) {
		this.btodisc = btodisc;
	}
	public BigDecimal getDiscountOfferPercent() {
		return discountOfferPercent;
	}
	public void setDiscountOfferPercent(BigDecimal discountOfferPercent) {
		this.discountOfferPercent = discountOfferPercent;
	}
	public String getBtooff() {
		return btooff;
	}
	public void setBtooff(String btooff) {
		this.btooff = btooff;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public BigDecimal getInitBidOffer() {
		return initBidOffer;
	}
	public void setInitBidOffer(BigDecimal initBidOffer) {
		this.initBidOffer = initBidOffer;
	}
	public BigDecimal getInitialRounding() {
		return initialRounding;
	}
	public void setInitialRounding(BigDecimal initialRounding) {
		this.initialRounding = initialRounding;
	}
	public int getInitAccumSame() {
		return initAccumSame;
	}
	public void setInitAccumSame(int initAccumSame) {
		this.initAccumSame = initAccumSame;
	}
	public BigDecimal getManagementCharge() {
		return managementCharge;
	}
	public void setManagementCharge(BigDecimal managementCharge) {
		this.managementCharge = managementCharge;
	}
	public String getOtoff() {
		return otoff;
	}
	public void setOtoff(String otoff) {
		this.otoff = otoff;
	}
	public BigDecimal getTolerance() {
		return tolerance;
	}
	public void setTolerance(BigDecimal tolerance) {
		this.tolerance = tolerance;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getZfundtyp() {
		return zfundtyp;
	}
	public void setZfundtyp(String zfundtyp) {
		this.zfundtyp = zfundtyp;
	}
	
	
}
