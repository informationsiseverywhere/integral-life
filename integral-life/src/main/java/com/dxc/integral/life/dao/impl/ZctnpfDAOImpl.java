package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ZctnpfDAO;
import com.dxc.integral.life.dao.model.Zctnpf;

/**
 * 
 * @author xma3
 *
 */
@Repository("zctnpfDAO")
@Lazy
public class ZctnpfDAOImpl extends BaseDAOImpl implements ZctnpfDAO {

	@Override
	public void insertZctnpfRecords(List<Zctnpf> zctnpfList) {
		
		final List<Zctnpf> tempZctnpfList = zctnpfList;   
		StringBuffer sql = new StringBuffer("INSERT INTO ZCTNPF(AGNTCOY,AGNTNUM,EFFDATE,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,TRCDE,CMAMT,PREMIUM,SPLITC,ZPRFLG,");
		sql.append("TRANDATE,USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		jdbcTemplate.batchUpdate(sql.toString(),new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
                 return tempZctnpfList.size();   
            } 
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	ps.setString(1, zctnpfList.get(i).getAgntcoy());
            	ps.setString(2, zctnpfList.get(i).getAgntnum());
            	ps.setInt(3, zctnpfList.get(i).getEffdate());
            	ps.setString(4, zctnpfList.get(i).getChdrcoy());
            	ps.setString(5, zctnpfList.get(i).getChdrnum());
            	ps.setString(6, zctnpfList.get(i).getLife());
            	ps.setString(7, zctnpfList.get(i).getCoverage());
            	ps.setString(8, zctnpfList.get(i).getRider());
            	ps.setInt(9, zctnpfList.get(i).getTranno());
            	ps.setString(10, zctnpfList.get(i).getTransCode());
            	ps.setBigDecimal(11, zctnpfList.get(i).getCommAmt());
            	ps.setBigDecimal(12, zctnpfList.get(i).getPremium());
            	ps.setBigDecimal(13, zctnpfList.get(i).getSplitBcomm());
            	ps.setString(14, zctnpfList.get(i).getZprflg());
            	ps.setInt(15, zctnpfList.get(i).getTrandate());
            	ps.setString(16, zctnpfList.get(i).getUserProfile());
            	ps.setString(17, zctnpfList.get(i).getJobName());
            	ps.setTimestamp(18,new Timestamp(System.currentTimeMillis()));
            }
            
		});
	}

}
