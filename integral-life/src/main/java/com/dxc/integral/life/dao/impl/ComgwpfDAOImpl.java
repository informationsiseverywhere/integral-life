
package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ComgwpfDAO;
import com.dxc.integral.life.dao.model.Comgwpf;

@Repository("comgwpfDAO")
@Lazy
public class ComgwpfDAOImpl extends BaseDAOImpl implements ComgwpfDAO {

	public void insertIntoComgwpf(List<Comgwpf> comgwpfList) {
		final List<Comgwpf> tempComgwpfList = comgwpfList;
		StringBuilder sqlInsert = new StringBuilder();
		sqlInsert.append("INSERT INTO COMGWPF (");
		sqlInsert.append("DATATYPE, INSUCATE, BOKCLS, SECUNUM, BRNCHNUM, ");
		sqlInsert.append("TOI, MOP, COLCATE, INSTAL, KYOCDE, ");
		sqlInsert.append("KYOACS, PCDE, PACS, GRPCDE, AFFILCDE, ");
		sqlInsert.append("EMPCDE, CGRPC, TOTPREM, BCKP, DPCDE, ");
		sqlInsert.append("BRNCHNUM2, INCPN, MATRTY, TNCDTE, TRNENDTE, ");
		sqlInsert.append("DTEOFAPP, SLFCRT, LPCC, DCDE, OPRTCDE, ");
		sqlInsert.append("POSTCDE, CNTADD, PNUM, SUBNME, SPFIP, ");
		sqlInsert.append("SPLTYP, PYMNTDTE, FPDFGH, INSYR, FRSTME, ");
		sqlInsert.append("PPREM, PPENDATME, NWCCLS, CCPS, IPACTC, ");
		sqlInsert.append("AMOR, GRPCARA, TELPHNO, NWCMPNCDE, RFTRNS, ");
		sqlInsert.append("MARGIN, INACVIWE, PROSQNC, YNMRCD, INSCMPNYCDE, ");
		sqlInsert.append("INSPROCDE, CMPNYCDE, PRSNMEKANA, IPECFLG, IPSFTCDE01, ");
		sqlInsert.append("IPNMKNJI, IPSFTCDE02, IPDOB, IPAGE, IPGNDR, ");
		sqlInsert.append("IPRLN, IPOSTCDE, IAEFLG, IASHTCDE01, IADRSKNJI, ");
		sqlInsert.append("IASHTCDE02, PYMNTRT, LSPREM, MODUNDRPRD, INDCATE, ");
		sqlInsert.append("INDCPRD, DVNDCLS, PDEXNTDVW, ATTCHNGCLS, APPCATE, ");
		sqlInsert.append("IPPYEAR, PPSDTE, PASBDY, ADDSCNT, NORIDS, ");
		sqlInsert.append("GRPCDE02, LOCDE, EXMCATE, TAXELG, LVNNDS, ");
		sqlInsert.append("BNSPREM, BCAGYR, OPCDE, MCCTYPCDE, MCCECFLG, ");
		sqlInsert.append("MCCSCDE01, MCCNMC, MCCSCDE02, MCCPPA, MCCITC, ");
		sqlInsert.append("MCCIP, MCCPPC, MCCPP, SA01TYPCDE, SA01ECFLG, ");
		sqlInsert.append("SA01SCDE01, SA01SCDE02, SA01NME, SA01SINS, SA01ITC, ");
		sqlInsert.append("SA01INSPRD, SA01PPC, SA01PP, SA02TYPCDE, SA02ECFLG, ");
		sqlInsert.append("SA02SCDE01, SA02SCDE02, SA02NME, SA02SINS, SA02ITC, ");
		sqlInsert.append("SA02INSPRD, SA02PPC, SA02PP, SA03TYPCDE, SA03ECFLG, ");
		sqlInsert.append("SA03SCDE01, SA03SCDE02, SA03NME, SA03SINS, SA03ITC, ");
		sqlInsert.append("SA03INSPRD, SA03PPC, SA03PP, SA04TYPCDE, SA04ECFLG, ");
		sqlInsert.append("SA04SCDE01, SA04SCDE02, SA04NME, SA04SINS, SA04ITC, ");
		sqlInsert.append("SA04INSPRD, SA04PPC, SA04PP, SA05TYPCDE, SAECFLG5, ");
		sqlInsert.append("SA05SCDE01, SA05SCDE02, SA05NME, SA05SINS, SA05ITC, ");
		sqlInsert.append("SA05INSPRD, SA05PPC, SA05PP, SA06TYPCDE, SA06ECFLG, ");
		sqlInsert.append("SA06SCDE01, SA06SCDE02, SA06NME, SA06SINS, SA06ITC, ");
		sqlInsert.append("SA06INSPRD, SA06PPC, SA06PP, USTATCATE, CMEC, ");
		sqlInsert.append("CIFCCC, ASMCDE, LIRCDE, ACCTYP, ACCNO, ");
		sqlInsert.append("LCODE, DTHBNFT, RACATE, DTYPCDE, CAIC, ");
		sqlInsert.append("CNTICNRCT, TNCP, WRKPLC, BPHNO, CMGNTCDE, ");
		sqlInsert.append("LNCDE, OSNOCC, OSNMN, OSNBN, GRPCRTLNO, ");
		sqlInsert.append("SCNO, GRPINSNME, CRSNME, TYPE, NOUNITS, ");
		sqlInsert.append("AGREGNO, AGCDE01, AGCDE02,CANKNJIECFLG, CAFLGSCDE01, CAFLGADRS, ");
		sqlInsert.append("CAFLGSCDE02, CNKJICFLGSCD,CNFLGSCDE, SPNMKNJI, SPNMSCDE, DTECHGPREM, ");
		sqlInsert.append("MODINSTPREM, PREMLNCRT, DOBPH, CGENDER, BPHNO02, ");
		sqlInsert.append("BCKUP01, CMPNYTER, PSTRTFLG, ANTYPYMNT, ANPENPYMNT, ");
		sqlInsert.append("PENSNFUND, PENSNCRTNO, PAYECATE, APPNO, BCKUP02, ");
		sqlInsert.append("CORPTERTY,AGNCNUM,EXPRTFLG, USRPRF, DATIME ");
		sqlInsert.append(
				") VALUES ( ?,?,?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		jdbcTemplate.batchUpdate(sqlInsert.toString(), new BatchPreparedStatementSetter() {
			@Override
			public int getBatchSize() {
				return tempComgwpfList.size();
			}

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setString(1, comgwpfList.get(i).getDatatype());
				ps.setString(2, comgwpfList.get(i).getInsucate());
				ps.setString(3, comgwpfList.get(i).getBokcls());
				ps.setString(4, comgwpfList.get(i).getSecunum());
				ps.setString(5, comgwpfList.get(i).getBrnchnum());
				ps.setString(6, comgwpfList.get(i).getToi());
				ps.setString(7, comgwpfList.get(i).getMop());
				ps.setString(8, comgwpfList.get(i).getColcate());
				ps.setString(9, comgwpfList.get(i).getInstal());
				ps.setString(10, comgwpfList.get(i).getKyocde());
				ps.setString(11, comgwpfList.get(i).getKyoacs());
				ps.setString(12, comgwpfList.get(i).getPcde());
				ps.setString(13, comgwpfList.get(i).getPacs());
				ps.setString(14, comgwpfList.get(i).getGrpcde());
				ps.setString(15, comgwpfList.get(i).getAffilcde());
				ps.setString(16, comgwpfList.get(i).getEmpcde());
				ps.setString(17, comgwpfList.get(i).getCgrpc());
				ps.setString(18, comgwpfList.get(i).getTotprem());
				ps.setString(19, comgwpfList.get(i).getBckp());
				ps.setString(20, comgwpfList.get(i).getDpcde());
				ps.setString(21, comgwpfList.get(i).getBrnchnum2());
				ps.setString(22, comgwpfList.get(i).getIncpn());
				ps.setString(23, comgwpfList.get(i).getMatrty());
				ps.setString(24, comgwpfList.get(i).getTncdte());
				ps.setString(25, comgwpfList.get(i).getTrnendte());
				ps.setString(26, comgwpfList.get(i).getDteofapp());
				ps.setString(27, comgwpfList.get(i).getSlfcrt());
				ps.setString(28, comgwpfList.get(i).getLpcc());
				ps.setString(29, comgwpfList.get(i).getDcde());
				ps.setString(30, comgwpfList.get(i).getOprtcde());
				ps.setString(31, comgwpfList.get(i).getPostcde());
				ps.setString(32, comgwpfList.get(i).getCntadd());
				ps.setString(33, comgwpfList.get(i).getPnum());
				ps.setString(34, comgwpfList.get(i).getSubnme());
				ps.setString(35, comgwpfList.get(i).getSpfip());
				ps.setString(36, comgwpfList.get(i).getSpltyp());
				ps.setString(37, comgwpfList.get(i).getPymntdte());
				ps.setString(38, comgwpfList.get(i).getFpdfgh());
				ps.setString(39, comgwpfList.get(i).getInsyr());
				ps.setString(40, comgwpfList.get(i).getFrstme());
				ps.setString(41, comgwpfList.get(i).getPprem());
				ps.setString(42, comgwpfList.get(i).getPpendatme());
				ps.setString(43, comgwpfList.get(i).getNwccls());
				ps.setString(44, comgwpfList.get(i).getCcps());
				ps.setString(45, comgwpfList.get(i).getIpactc());
				ps.setString(46, comgwpfList.get(i).getAmor());
				ps.setString(47, comgwpfList.get(i).getGrpcara());
				ps.setString(48, comgwpfList.get(i).getTelphno());
				ps.setString(49, comgwpfList.get(i).getNwcmpncde());
				ps.setString(50, comgwpfList.get(i).getRftrns());
				ps.setString(51, comgwpfList.get(i).getMargin());
				ps.setString(52, comgwpfList.get(i).getInacviwe());
				ps.setString(53, comgwpfList.get(i).getProsqnc());
				ps.setString(54, comgwpfList.get(i).getYnmrcd());
				ps.setString(55, comgwpfList.get(i).getInscmpnycde());
				ps.setString(56, comgwpfList.get(i).getInsprocde());
				ps.setString(57, comgwpfList.get(i).getCmpnycde());
				ps.setString(58, comgwpfList.get(i).getPrsnmekana());
				ps.setString(59, comgwpfList.get(i).getIpecflg());
				ps.setString(60, comgwpfList.get(i).getIpsftcde01());
				ps.setString(61, comgwpfList.get(i).getIpnmknji());
				ps.setString(62, comgwpfList.get(i).getIpsftcde02());
				ps.setString(63, comgwpfList.get(i).getIpdob());
				ps.setString(64, comgwpfList.get(i).getIpage());
				ps.setString(65, comgwpfList.get(i).getIpgndr());
				ps.setString(66, comgwpfList.get(i).getIprln());
				ps.setString(67, comgwpfList.get(i).getIpostcde());
				ps.setString(68, comgwpfList.get(i).getIaeflg());
				ps.setString(69, comgwpfList.get(i).getIashtcde01());
				ps.setString(70, comgwpfList.get(i).getIadrsknji());
				ps.setString(71, comgwpfList.get(i).getIashtcde02());
				ps.setString(72, comgwpfList.get(i).getPymntrt());
				ps.setString(73, comgwpfList.get(i).getLsprem());
				ps.setString(74, comgwpfList.get(i).getModundrprd());
				ps.setString(75, comgwpfList.get(i).getIndcate());
				ps.setString(76, comgwpfList.get(i).getIndcprd());
				ps.setString(77, comgwpfList.get(i).getDvndcls());
				ps.setString(78, comgwpfList.get(i).getPdexntdvw());
				ps.setString(79, comgwpfList.get(i).getAttchngcls());
				ps.setString(80, comgwpfList.get(i).getAppcate());
				ps.setString(81, comgwpfList.get(i).getIppyear());
				ps.setString(82, comgwpfList.get(i).getPpsdte());
				ps.setString(83, comgwpfList.get(i).getPasbdy());
				ps.setString(84, comgwpfList.get(i).getAddscnt());
				ps.setString(85, comgwpfList.get(i).getNorids());
				ps.setString(86, comgwpfList.get(i).getGrpcde02());
				ps.setString(87, comgwpfList.get(i).getLocde());
				ps.setString(88, comgwpfList.get(i).getExmcate());
				ps.setString(89, comgwpfList.get(i).getTaxelg());
				ps.setString(90, comgwpfList.get(i).getLvnnds());
				ps.setString(91, comgwpfList.get(i).getBnsprem());
				ps.setString(92, comgwpfList.get(i).getBcagyr());
				ps.setString(93, comgwpfList.get(i).getOpcde());
				ps.setString(94, comgwpfList.get(i).getMcctypcde());
				ps.setString(95, comgwpfList.get(i).getMccecflg());
				ps.setString(96, comgwpfList.get(i).getMccscde01());
				ps.setString(97, comgwpfList.get(i).getMccnmc());
				ps.setString(98, comgwpfList.get(i).getMccscde02());
				ps.setString(99, comgwpfList.get(i).getMccppa());
				ps.setString(100, comgwpfList.get(i).getMccitc());
				ps.setString(101, comgwpfList.get(i).getMccip());
				ps.setString(102, comgwpfList.get(i).getMccppc());
				ps.setString(103, comgwpfList.get(i).getMccpp());
				ps.setString(104, comgwpfList.get(i).getSa01typcde());
				ps.setString(105, comgwpfList.get(i).getSa01ecflg());
				ps.setString(106, comgwpfList.get(i).getSa01scde01());
				ps.setString(107, comgwpfList.get(i).getSa01scde02());
				ps.setString(108, comgwpfList.get(i).getSa01nme());
				ps.setString(109, comgwpfList.get(i).getSa01sins());
				ps.setString(110, comgwpfList.get(i).getSa01itc());
				ps.setString(111, comgwpfList.get(i).getSa01insprd());
				ps.setString(112, comgwpfList.get(i).getSa01ppc());
				ps.setString(113, comgwpfList.get(i).getSa01pp());
				ps.setString(114, comgwpfList.get(i).getSa02typcde());
				ps.setString(115, comgwpfList.get(i).getSa02ecflg());
				ps.setString(116, comgwpfList.get(i).getSa02scde01());
				ps.setString(117, comgwpfList.get(i).getSa02scde02());
				ps.setString(118, comgwpfList.get(i).getSa02nme());
				ps.setString(119, comgwpfList.get(i).getSa02sins());
				ps.setString(120, comgwpfList.get(i).getSa02itc());
				ps.setString(121, comgwpfList.get(i).getSa02insprd());
				ps.setString(122, comgwpfList.get(i).getSa02ppc());
				ps.setString(123, comgwpfList.get(i).getSa02pp());
				ps.setString(124, comgwpfList.get(i).getSa03typcde());
				ps.setString(125, comgwpfList.get(i).getSa03ecflg());
				ps.setString(126, comgwpfList.get(i).getSa03scde01());
				ps.setString(127, comgwpfList.get(i).getSa03scde02());
				ps.setString(128, comgwpfList.get(i).getSa03nme());
				ps.setString(129, comgwpfList.get(i).getSa03sins());
				ps.setString(130, comgwpfList.get(i).getSa03itc());
				ps.setString(131, comgwpfList.get(i).getSa03insprd());
				ps.setString(132, comgwpfList.get(i).getSa03ppc());
				ps.setString(133, comgwpfList.get(i).getSa03pp());
				ps.setString(134, comgwpfList.get(i).getSa04typcde());
				ps.setString(135, comgwpfList.get(i).getSa04ecflg());
				ps.setString(136, comgwpfList.get(i).getSa04scde01());
				ps.setString(137, comgwpfList.get(i).getSa04scde02());
				ps.setString(138, comgwpfList.get(i).getSa04nme());
				ps.setString(139, comgwpfList.get(i).getSa04sins());
				ps.setString(140, comgwpfList.get(i).getSa04itc());
				ps.setString(141, comgwpfList.get(i).getSa04insprd());
				ps.setString(142, comgwpfList.get(i).getSa04ppc());
				ps.setString(143, comgwpfList.get(i).getSa04pp());
				ps.setString(144, comgwpfList.get(i).getSa05typcde());
				ps.setString(145, comgwpfList.get(i).getSaecflg5());
				ps.setString(146, comgwpfList.get(i).getSa05scde01());
				ps.setString(147, comgwpfList.get(i).getSa05scde02());
				ps.setString(148, comgwpfList.get(i).getSa05nme());
				ps.setString(149, comgwpfList.get(i).getSa05sins());
				ps.setString(150, comgwpfList.get(i).getSa05itc());
				ps.setString(151, comgwpfList.get(i).getSa05insprd());
				ps.setString(152, comgwpfList.get(i).getSa05ppc());
				ps.setString(153, comgwpfList.get(i).getSa05pp());
				ps.setString(154, comgwpfList.get(i).getSa06typcde());
				ps.setString(155, comgwpfList.get(i).getSa06ecflg());
				ps.setString(156, comgwpfList.get(i).getSa06scde01());
				ps.setString(157, comgwpfList.get(i).getSa06scde02());
				ps.setString(158, comgwpfList.get(i).getSa06nme());
				ps.setString(159, comgwpfList.get(i).getSa06sins());
				ps.setString(160, comgwpfList.get(i).getSa06itc());
				ps.setString(161, comgwpfList.get(i).getSa06insprd());
				ps.setString(162, comgwpfList.get(i).getSa06ppc());
				ps.setString(163, comgwpfList.get(i).getSa06pp());
				ps.setString(164, comgwpfList.get(i).getUstatcate());
				ps.setString(165, comgwpfList.get(i).getCmec());
				ps.setString(166, comgwpfList.get(i).getCifccc());	//ILJ-699
				ps.setString(167, comgwpfList.get(i).getAsmcde());
				ps.setString(168, comgwpfList.get(i).getLircde());
				ps.setString(169, comgwpfList.get(i).getAcctyp());
				ps.setString(170, comgwpfList.get(i).getAccno());
				ps.setString(171, comgwpfList.get(i).getLcode());
				ps.setString(172, comgwpfList.get(i).getDthbnft());
				ps.setString(173, comgwpfList.get(i).getRacate());
				ps.setString(174, comgwpfList.get(i).getDtypcde());
				ps.setString(175, comgwpfList.get(i).getCaic());
				ps.setString(176, comgwpfList.get(i).getCnticnrct());
				ps.setString(177, comgwpfList.get(i).getTncp());
				ps.setString(178, comgwpfList.get(i).getWrkplc());
				ps.setString(179, comgwpfList.get(i).getBphno());
				ps.setString(180, comgwpfList.get(i).getCmgntcde());
				ps.setString(181, comgwpfList.get(i).getLncde());
				ps.setString(182, comgwpfList.get(i).getOsnocc());
				ps.setString(183, comgwpfList.get(i).getOsnmn());
				ps.setString(184, comgwpfList.get(i).getOsnbn());
				ps.setString(185, comgwpfList.get(i).getGrpcrtlno());
				ps.setString(186, comgwpfList.get(i).getScno());
				ps.setString(187, comgwpfList.get(i).getGrpinsnme());
				ps.setString(188, comgwpfList.get(i).getCrsnme());
				ps.setString(189, comgwpfList.get(i).getType());
				ps.setString(190, comgwpfList.get(i).getNounits());
				ps.setString(191, comgwpfList.get(i).getAgregno());
				ps.setString(192, comgwpfList.get(i).getAgcde01());
				ps.setString(193, comgwpfList.get(i).getAgcde02());
				ps.setString(194, comgwpfList.get(i).getCanknjiecflg());
				ps.setString(195, comgwpfList.get(i).getCaflgscde01());
				ps.setString(196, comgwpfList.get(i).getCaflgadrs());
				ps.setString(197, comgwpfList.get(i).getCaflgscde02());
				ps.setString(198, comgwpfList.get(i).getCnkjicflgscd());
				ps.setString(199, comgwpfList.get(i).getCnflgscde());
				ps.setString(200, comgwpfList.get(i).getSpnmknji());
				ps.setString(201, comgwpfList.get(i).getSpnmscde());
				ps.setString(202, comgwpfList.get(i).getDtechgprem());
				ps.setString(203, comgwpfList.get(i).getModinstprem());
				ps.setString(204, comgwpfList.get(i).getPremlncrt());
				ps.setString(205, comgwpfList.get(i).getDobph());
				ps.setString(206, comgwpfList.get(i).getCgender());
				ps.setString(207, comgwpfList.get(i).getBphno02());
				ps.setString(208, comgwpfList.get(i).getBckup01());
				ps.setString(209, comgwpfList.get(i).getCmpnyter());
				ps.setString(210, comgwpfList.get(i).getPstrtflg());
				ps.setString(211, comgwpfList.get(i).getAntypymnt());
				ps.setString(212, comgwpfList.get(i).getAnpenpymnt());
				ps.setString(213, comgwpfList.get(i).getPensnfund());
				ps.setString(214, comgwpfList.get(i).getPensncrtno());
				ps.setString(215, comgwpfList.get(i).getPayecate());
				ps.setString(216, comgwpfList.get(i).getAppno());
				ps.setString(217, comgwpfList.get(i).getBckup02());
				ps.setString(218, comgwpfList.get(i).getCorpterty());
				ps.setString(219, comgwpfList.get(i).getAgncnum());
				ps.setString(220, comgwpfList.get(i).getExprtflg());
				ps.setString(221, comgwpfList.get(i).getUsrprf());
				ps.setTimestamp(222, new Timestamp(System.currentTimeMillis()));
			}
		});
	}

	@Override
	public List<Comgwpf> searchComgwpfRecord(List<String> agncnum) {
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("agncnum", agncnum);
		String sql = "select * from Comgwpf where agncnum in (:agncnum) and exprtflg='' ";
		return namedParameterJdbcTemplate.query(sql, parameters, new BeanPropertyRowMapper<Comgwpf>(Comgwpf.class));

	}

	@Override
	public void updateExprtflg(List<String> agncnum) {
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("agncnum", agncnum);
		String sql = "update Comgwpf set exprtflg='Y' where agncnum in (:agncnum) and exprtflg='' ";
		namedParameterJdbcTemplate.update(sql, parameters);
	}

	public Comgwpf checkDuplRecord(List<String> checkList) {

		String sql = "SELECT * FROM COMGWPF WHERE MOP=? AND TOTPREM=? AND DPCDE = ? AND CNTADD=? AND PNUM=? AND SUBNME=?"
				+ " AND PRSNMEKANA=? AND IPNMKNJI=? AND IPDOB=? AND IPAGE=? AND IPGNDR=? AND IPRLN=? AND IPOSTCDE=? AND IADRSKNJI=? AND "
				+ "PYMNTRT=? AND MCCPPA=? AND MCCIP=? AND SA01SINS=? AND SA01INSPRD=? AND SA01PP=? AND ACCTYP=? AND ACCNO=? AND DTYPCDE=?";
		List<Comgwpf> results= jdbcTemplate.query(sql,
				new Object[] { checkList.get(0), checkList.get(1), checkList.get(2), checkList.get(3), checkList.get(4),
						checkList.get(5), checkList.get(6), checkList.get(7), checkList.get(8), checkList.get(9),
						checkList.get(10), checkList.get(11), checkList.get(12), checkList.get(13), checkList.get(14),
						checkList.get(15), checkList.get(16), checkList.get(17), checkList.get(18), checkList.get(19),
						checkList.get(20), checkList.get(21),checkList.get(22)},
				new BeanPropertyRowMapper<Comgwpf>(Comgwpf.class));
				
		if (results.size() > 0) return results.get(0);
		return null;
	}
}
