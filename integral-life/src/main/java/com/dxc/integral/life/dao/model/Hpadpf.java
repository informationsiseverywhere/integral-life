package com.dxc.integral.life.dao.model;

import java.sql.Timestamp;

/**
 * Hpadpf model for HpadpfDAO.
 * 
 * @author vhukumagrawa
 *
 */
public class Hpadpf {

	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String validflag;
	private int hpropdte;
	private int hprrcvdt;
	private int hissdte;
	private int huwdcdte;
	private int hoissdte;
	private String zdoctor;
	private String znfopt;
	private int zsufcdte;
	private String procflag;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private String dlvrmode;
	private int despdate;
	private int packdate;
	private int remdte;
	private int deemdate;
	private int nxtdte;
	private String incexc;

	public long getUnique_number() {
		return unique_number;
	}

	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getHpropdte() {
		return hpropdte;
	}

	public void setHpropdte(int hpropdte) {
		this.hpropdte = hpropdte;
	}

	public int getHprrcvdt() {
		return hprrcvdt;
	}

	public void setHprrcvdt(int hprrcvdt) {
		this.hprrcvdt = hprrcvdt;
	}

	public int getHissdte() {
		return hissdte;
	}

	public void setHissdte(int hissdte) {
		this.hissdte = hissdte;
	}

	public int getHuwdcdte() {
		return huwdcdte;
	}

	public void setHuwdcdte(int huwdcdte) {
		this.huwdcdte = huwdcdte;
	}

	public int getHoissdte() {
		return hoissdte;
	}

	public void setHoissdte(int hoissdte) {
		this.hoissdte = hoissdte;
	}

	public String getZdoctor() {
		return zdoctor;
	}

	public void setZdoctor(String zdoctor) {
		this.zdoctor = zdoctor;
	}

	public String getZnfopt() {
		return znfopt;
	}

	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}

	public int getZsufcdte() {
		return zsufcdte;
	}

	public void setZsufcdte(int zsufcdte) {
		this.zsufcdte = zsufcdte;
	}

	public String getProcflag() {
		return procflag;
	}

	public void setProcflag(String procflag) {
		this.procflag = procflag;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}

	public String getDlvrmode() {
		return dlvrmode;
	}

	public void setDlvrmode(String dlvrmode) {
		this.dlvrmode = dlvrmode;
	}

	public int getDespdate() {
		return despdate;
	}

	public void setDespdate(int despdate) {
		this.despdate = despdate;
	}

	public int getPackdate() {
		return packdate;
	}

	public void setPackdate(int packdate) {
		this.packdate = packdate;
	}

	public int getRemdte() {
		return remdte;
	}

	public void setRemdte(int remdte) {
		this.remdte = remdte;
	}

	public int getDeemdate() {
		return deemdate;
	}

	public void setDeemdate(int deemdate) {
		this.deemdate = deemdate;
	}

	public int getNxtdte() {
		return nxtdte;
	}

	public void setNxtdte(int nxtdte) {
		this.nxtdte = nxtdte;
	}

	public String getIncexc() {
		return incexc;
	}

	public void setIncexc(String incexc) {
		this.incexc = incexc;
	}
}
