/* ********************   */
/*Author  :tsaxena3		  */
/*Purpose :Util of Cfee001*/
/*Date    :2019.04.09	  */

package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;
import com.dxc.integral.life.beans.Cfee001;

public interface Cfee001Utils {
	
	public void calCfee001(Cfee001 cfee001) throws IOException, ParseException;

}
