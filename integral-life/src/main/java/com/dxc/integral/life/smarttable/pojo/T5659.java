package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5659 {
  	private Integer rndfact;
  	private List<BigDecimal> volbanfrs;
  	private List<BigDecimal> volbanles;
  	private  List<BigDecimal> volbantos;
	public Integer getRndfact() {
		return rndfact;
	}
	public void setRndfact(Integer rndfact) {
		this.rndfact = rndfact;
	}
	public List<BigDecimal> getVolbanfrs() {
		return volbanfrs;
	}
	public void setVolbanfrs(List<BigDecimal> volbanfrs) {
		this.volbanfrs = volbanfrs;
	}
	public List<BigDecimal> getVolbanles() {
		return volbanles;
	}
	public void setVolbanles(List<BigDecimal> volbanles) {
		this.volbanles = volbanles;
	}
	public List<BigDecimal> getVolbantos() {
		return volbantos;
	}
	public void setVolbantos(List<BigDecimal> volbantos) {
		this.volbantos = volbantos;
	}
  

}
