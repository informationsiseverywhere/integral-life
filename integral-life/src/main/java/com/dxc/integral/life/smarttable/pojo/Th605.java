package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class Th605 {
  	private String agccqind;
  	private String bonusInd;
  	private String comind;
  	private String crtind;
  	private String indic;
  	private BigDecimal intRate;
  	private String liacoy;
  	private String ratebas;
  	private int tdayno;
  	private String tsalesind;
  	private String uwind;
	public String getAgccqind() {
		return agccqind;
	}
	public void setAgccqind(String agccqind) {
		this.agccqind = agccqind;
	}
	public String getBonusInd() {
		return bonusInd;
	}
	public void setBonusInd(String bonusInd) {
		this.bonusInd = bonusInd;
	}
	public String getComind() {
		return comind;
	}
	public void setComind(String comind) {
		this.comind = comind;
	}
	public String getCrtind() {
		return crtind;
	}
	public void setCrtind(String crtind) {
		this.crtind = crtind;
	}
	public String getIndic() {
		return indic;
	}
	public void setIndic(String indic) {
		this.indic = indic;
	}
	public BigDecimal getIntRate() {
		return intRate;
	}
	public void setIntRate(BigDecimal intRate) {
		this.intRate = intRate;
	}
	public String getLiacoy() {
		return liacoy;
	}
	public void setLiacoy(String liacoy) {
		this.liacoy = liacoy;
	}
	public String getRatebas() {
		return ratebas;
	}
	public void setRatebas(String ratebas) {
		this.ratebas = ratebas;
	}
	public int getTdayno() {
		return tdayno;
	}
	public void setTdayno(int tdayno) {
		this.tdayno = tdayno;
	}
	public String getTsalesind() {
		return tsalesind;
	}
	public void setTsalesind(String tsalesind) {
		this.tsalesind = tsalesind;
	}
	public String getUwind() {
		return uwind;
	}
	public void setUwind(String uwind) {
		this.uwind = uwind;
	}
  	
}
