package com.dxc.integral.life.dao.impl;

import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AgpypfDAO;
import com.dxc.integral.life.dao.model.Agpypf;

/**
 * The AgpypfDAOImpl for database operations.
 * 
 * @author vhukumagrawa
 *
 */
@Repository("agpypfDAO")
public class AgpypfDAOImpl extends BaseDAOImpl implements AgpypfDAO{
	 
	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.AgpypfDAO#insertAcmvpf(com.dxc.integral.life.dao.model.Agpypf)
	 */
	@Override
	public Integer insertAcmvpf(Agpypf agpypf) {
		
		StringBuilder query = new StringBuilder("INSERT INTO agpypf ( agntpfx, agntcoy, agntnum,agntbr,aracde,batcpfx,batccoy,batcbrn,batcactyr, ");
		query.append("batcactmn,batctrcde,batcbatch,paypfx,paycoy,bankkey,bankacckey,reqnpfx,reqncoy,reqnbcde,reqnno,tranamt,sacscode,sacstyp,rdocnum,");
		query.append("tranno,jrnseq,origamt,tranref,trandesc,crate,acctamt,genlcoy,genlcur,glcode,glsign,postyear,postmonth,effdate,rcamt,frcdate,trdt,termid,rldgcoy,rldgacct,origcurr,suprflg,trtm,usrprf,jobnm,datime )");
		query.append(" VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		return jdbcTemplate.update(query.toString(), new Object[]{
				agpypf.getAgntpfx(),agpypf.getAgntcoy(),agpypf.getAgntnum(),agpypf.getAgntbr(),agpypf.getAracde(),agpypf.getBatcpfx(),agpypf.getBatccoy(),agpypf.getBatcbrn(),agpypf.getBatcactyr(),agpypf.getBatcactmn(),
					agpypf.getBatctrcde(),agpypf.getBatcbatch(),agpypf.getPaypfx(),agpypf.getPaycoy(),agpypf.getBankkey(),agpypf.getBankacckey(),agpypf.getReqnpfx(),agpypf.getReqncoy(),agpypf.getReqnbcde() ,agpypf.getReqnno(),
					agpypf.getTranamt(),agpypf.getSacscode(),agpypf.getSacstyp(),agpypf.getRdocnum(),agpypf.getTranno(),agpypf.getJrnseq(),agpypf.getOrigamt(),agpypf.getTranref(),agpypf.getTrandesc(),agpypf.getCrate(),agpypf.getAcctamt(),
					agpypf.getGenlcoy(), agpypf.getGenlcur(),agpypf.getGlcode(),agpypf.getGlsign(),agpypf.getPostyear(),agpypf.getPostmonth(),agpypf.getEffdate(),agpypf.getRcamt(),agpypf.getFrcdate(),
					agpypf.getTrdt(),agpypf.getTermid(),agpypf.getRldgcoy(), agpypf.getRldgacct(),agpypf.getOrigcurr(),agpypf.getSuprflg(), agpypf.getTrtm(), agpypf.getUsrprf(), agpypf.getJobnm(), agpypf.getDatime()
				});
	}

}
