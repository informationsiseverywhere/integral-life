package com.dxc.integral.life.beans;

import java.math.BigDecimal;

public class IncrsumDTO {
	
  	private String function ;
  	private String  statuz ;
  	private String  chdrcoy ;
  	private String chdrnum;
  	private String life;
  	private String  coverage ;
  	private String  rider;
  	private String  plnsfx ;
  	private int  effdate ;
  	private String  crtable ;
  	private String  annvmeth ;
  	private BigDecimal  origsum ;
  	private BigDecimal lastsum ;
  	private BigDecimal currsum ;
  	private BigDecimal newsum ;
  	private BigDecimal originst01;
  	private BigDecimal lastinst01 ;
  	private BigDecimal currinst01;
  	private BigDecimal newinst01 ;
  	private BigDecimal originst02;
  	private BigDecimal lastinst02;
  	private BigDecimal currinst02;
  	private BigDecimal newinst02 ;
  	private BigDecimal originst03;
  	private BigDecimal lastinst03;
  	private BigDecimal currinst03;
  	private BigDecimal newinst03;
  	private BigDecimal pctinc ;
  	private BigDecimal maxpcnt ;
  	private BigDecimal minpcnt;
  	private BigDecimal fixdtrm;
  	private int rcesdte ;
  	private int occdate;
  	private String  cntcurr;
  	private String  mortcls;
  	private String  billfreq; 
  	private String  mop;
  	private String  language; 
  	private String  autoincreaseindicator ;
  	private String  simpleInd;
  	private BigDecimal zstpduty01 ;
  	private BigDecimal zstpduty02 ;
  	private BigDecimal zstpduty03 ;
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(String plnsfx) {
		this.plnsfx = plnsfx;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getAnnvmeth() {
		return annvmeth;
	}
	public void setAnnvmeth(String annvmeth) {
		this.annvmeth = annvmeth;
	}
	public BigDecimal getOrigsum() {
		return origsum;
	}
	public void setOrigsum(BigDecimal origsum) {
		this.origsum = origsum;
	}
	public BigDecimal getLastsum() {
		return lastsum;
	}
	public void setLastsum(BigDecimal lastsum) {
		this.lastsum = lastsum;
	}
	public BigDecimal getCurrsum() {
		return currsum;
	}
	public void setCurrsum(BigDecimal currsum) {
		this.currsum = currsum;
	}
	public BigDecimal getNewsum() {
		return newsum;
	}
	public void setNewsum(BigDecimal newsum) {
		this.newsum = newsum;
	}
	public BigDecimal getOriginst01() {
		return originst01;
	}
	public void setOriginst01(BigDecimal originst01) {
		this.originst01 = originst01;
	}
	public BigDecimal getLastinst01() {
		return lastinst01;
	}
	public void setLastinst01(BigDecimal lastinst01) {
		this.lastinst01 = lastinst01;
	}
	public BigDecimal getCurrinst01() {
		return currinst01;
	}
	public void setCurrinst01(BigDecimal currinst01) {
		this.currinst01 = currinst01;
	}
	public BigDecimal getNewinst01() {
		return newinst01;
	}
	public void setNewinst01(BigDecimal newinst01) {
		this.newinst01 = newinst01;
	}
	public BigDecimal getOriginst02() {
		return originst02;
	}
	public void setOriginst02(BigDecimal originst02) {
		this.originst02 = originst02;
	}
	public BigDecimal getLastinst02() {
		return lastinst02;
	}
	public void setLastinst02(BigDecimal lastinst02) {
		this.lastinst02 = lastinst02;
	}
	public BigDecimal getCurrinst02() {
		return currinst02;
	}
	public void setCurrinst02(BigDecimal currinst02) {
		this.currinst02 = currinst02;
	}
	public BigDecimal getNewinst02() {
		return newinst02;
	}
	public void setNewinst02(BigDecimal newinst02) {
		this.newinst02 = newinst02;
	}
	public BigDecimal getOriginst03() {
		return originst03;
	}
	public void setOriginst03(BigDecimal originst03) {
		this.originst03 = originst03;
	}
	public BigDecimal getLastinst03() {
		return lastinst03;
	}
	public void setLastinst03(BigDecimal lastinst03) {
		this.lastinst03 = lastinst03;
	}
	public BigDecimal getCurrinst03() {
		return currinst03;
	}
	public void setCurrinst03(BigDecimal currinst03) {
		this.currinst03 = currinst03;
	}
	public BigDecimal getNewinst03() {
		return newinst03;
	}
	public void setNewinst03(BigDecimal newinst03) {
		this.newinst03 = newinst03;
	}
	public BigDecimal getPctinc() {
		return pctinc;
	}
	public void setPctinc(BigDecimal pctinc) {
		this.pctinc = pctinc;
	}
	public BigDecimal getMaxpcnt() {
		return maxpcnt;
	}
	public void setMaxpcnt(BigDecimal maxpcnt) {
		this.maxpcnt = maxpcnt;
	}
	public BigDecimal getMinpcnt() {
		return minpcnt;
	}
	public void setMinpcnt(BigDecimal minpcnt) {
		this.minpcnt = minpcnt;
	}
	public BigDecimal getFixdtrm() {
		return fixdtrm;
	}
	public void setFixdtrm(BigDecimal fixdtrm) {
		this.fixdtrm = fixdtrm;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getMortcls() {
		return mortcls;
	}
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getMop() {
		return mop;
	}
	public void setMop(String mop) {
		this.mop = mop;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getAutoincreaseindicator() {
		return autoincreaseindicator;
	}
	public void setAutoincreaseindicator(String autoincreaseindicator) {
		this.autoincreaseindicator = autoincreaseindicator;
	}
	public String getSimpleInd() {
		return simpleInd;
	}
	public void setSimpleInd(String simpleInd) {
		this.simpleInd = simpleInd;
	}
	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}
	public BigDecimal getZstpduty02() {
		return zstpduty02;
	}
	public void setZstpduty02(BigDecimal zstpduty02) {
		this.zstpduty02 = zstpduty02;
	}
	public BigDecimal getZstpduty03() {
		return zstpduty03;
	}
	public void setZstpduty03(BigDecimal zstpduty03) {
		this.zstpduty03 = zstpduty03;
	}

}
