package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Agprpf;

/**
 * AgprpfDAO for database table <b>AGPRPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@FunctionalInterface
public interface AgprpfDAO {

	/**
	 * Inserts record into AGPRPF table.
	 * 
	 * @param agprpf
	 *            - Agprpf model
	 * @return - number of rows inserted
	 */
	int writeAgprpf(Agprpf agprpf);
}
