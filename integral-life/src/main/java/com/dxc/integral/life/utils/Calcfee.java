package com.dxc.integral.life.utils;

import com.dxc.integral.life.beans.OvrdueDTO;

public interface Calcfee {
	public OvrdueDTO processCalcfee(OvrdueDTO ovrdueDTO);
}
