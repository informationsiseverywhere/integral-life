package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5694 {

	private List<BigDecimal> instalpcs;
	private List<Integer> toYears;
	
	public List<BigDecimal> getInstalpcs() {
		return instalpcs;
	}
	public void setInstalpcs(List<BigDecimal> instalpcs) {
		this.instalpcs = instalpcs;
	}
	public List<Integer> getToYears() {
		return toYears;
	}
	public void setToYears(List<Integer> toYears) {
		this.toYears = toYears;
	}
	
	
}
