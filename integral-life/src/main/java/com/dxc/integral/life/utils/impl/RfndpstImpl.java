package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.life.beans.LifacmvDTO;
import com.dxc.integral.life.beans.RecopstDTO;
import com.dxc.integral.life.dao.RecopfDAO;
import com.dxc.integral.life.dao.model.Recopf;
import com.dxc.integral.life.exceptions.StopRunException;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.smarttable.pojo.T5688;
import com.dxc.integral.life.utils.Lifacmv;
import com.dxc.integral.life.utils.Rfndpst;

/**
 * Reassurance Refund Posting Subroutine.
 * 
 * @author fwang3
 *
 */
@Service("rfndpst")
@Lazy
public class RfndpstImpl extends BaseDAOImpl implements Rfndpst {

	@Autowired
	private DescpfDAO descpfDAO;
	@Autowired
	private RecopfDAO recopfDAO;
	@Autowired
	private ItempfService itempfService;
	@Autowired
	private Lifacmv lifacmv;
	
	private BigDecimal wsaaNetPrem = BigDecimal.ZERO;
	private int wsaaSub1;
	
	private T5645 t5645 = new T5645();
	private T5688 t5688  = new T5688();
	
	private LifacmvDTO lifacmvrec1 = new LifacmvDTO();
	private RecopstDTO recopstrec = new RecopstDTO();
	StringBuilder wsaaRldgacct = new StringBuilder();
	
	
	@Override
	public void execute(RecopstDTO recopstDTO) throws IOException, ParseException {
		initialise();
		//compute(wsaaNetPrem, 2).set(sub(v,recopstrec.refundfe));
		wsaaNetPrem = recopstrec.getPrem().subtract(recopstrec.getCompay()).subtract(recopstrec.getRefundfe()).setScale(2);
		createReco();
		postingProcessing();
	}

	protected void initialise() {
		t5645 = itempfService.readSmartTableByTrim(recopstrec.getChdrcoy(), "T5645", "RFNDPST", T5645.class);
		if (t5645 == null) {
			throw new ItemNotfoundException("Item RFNDPST not found in Table T5645");
		}

		t5688 = itempfService.readSmartTableByTrim(recopstrec.getChdrcoy(), "T5688", recopstrec.getCnttype(),
				recopstrec.getEffdate(), T5688.class);
		if (t5688 == null) {
			throw new ItemNotfoundException("Item " + recopstrec.getCnttype() + " not found in Table T5688");
		}
	}

	protected void createReco() {
		Recopf recoIO = new Recopf();
		recoIO.setChdrcoy(recopstrec.getChdrcoy());
		recoIO.setChdrnum(recopstrec.getChdrnum());
		recoIO.setLife(recopstrec.getLife());
		recoIO.setCoverage(recopstrec.getCoverage());
		recoIO.setRider(recopstrec.getRider());
		recoIO.setPlanSuffix(recopstrec.getPlanSuffix());
		recoIO.setRasnum(recopstrec.getRasnum());
		recoIO.setSeqno(recopstrec.getSeqno());
		recoIO.setCostdate(recopstrec.getCostdate());
		recoIO.setValidflag(recopstrec.getValidflag());
		recoIO.setRetype(recopstrec.getRetype());
		recoIO.setRngmnt(recopstrec.getRngmnt());
		recoIO.setSraramt(recopstrec.getSraramt());
		recoIO.setRaAmount(recopstrec.getRaAmount());
		recoIO.setCtdate(recopstrec.getCtdate());
		recoIO.setOrigcurr(recopstrec.getOrigcurr());
		recoIO.setPrem(recopstrec.getPrem().setScale(2).negate());
		recoIO.setCompay(recopstrec.getCompay().setScale(2).negate());
		recoIO.setTaxamt(BigDecimal.ZERO);
		recoIO.setRefundfe(recopstrec.getRefundfe());
		recoIO.setBatccoy(recopstrec.getBatccoy());
		recoIO.setBatcbrn(recopstrec.getBatcbrn());
		recoIO.setBatcactyr(recopstrec.getBatcactyr());
		recoIO.setBatcactmn(recopstrec.getBatcactmn());
		recoIO.setBatctrcde(recopstrec.getBatctrcde());
		recoIO.setBatcbatch(recopstrec.getBatcbatch());
		recoIO.setTranno(recopstrec.getTranno());
		recoIO.setRcstfrq(recopstrec.getRcstfrq());
		int row = recopfDAO.insertRecopfRecord(recoIO);
		if (row < 1) {
			throw new StopRunException("Recopf insert failed");
		}
	}

	protected void postingProcessing() throws IOException, ParseException {
		Descpf descIO = descpfDAO.getDescInfo(recopstrec.getChdrcoy(), "T1688", recopstrec.getBatctrcde());
		lifacmvrec1.setJrnseq(recopstrec.getSeqno());
		lifacmvrec1.setTrandesc(descIO.getLongdesc());
		lifacmvrec1.setBatccoy(recopstrec.getBatccoy());
		lifacmvrec1.setBatcbrn(recopstrec.getBatcbrn());
		lifacmvrec1.setBatcactyr(recopstrec.getBatcactyr());
		lifacmvrec1.setBatcactmn(recopstrec.getBatcactmn());
		lifacmvrec1.setBatctrcde(recopstrec.getBatctrcde());
		lifacmvrec1.setBatcbatch(recopstrec.getBatcbatch());
		lifacmvrec1.setRdocnum(recopstrec.getChdrnum());
		lifacmvrec1.setTranno(recopstrec.getTranno());
		lifacmvrec1.setRldgcoy(recopstrec.getChdrcoy());
		lifacmvrec1.setOrigcurr(recopstrec.getOrigcurr());
		lifacmvrec1.setCrate(BigDecimal.ZERO);
		lifacmvrec1.setAcctamt(BigDecimal.ZERO);
		lifacmvrec1.setGenlcoy(recopstrec.getChdrcoy());
		lifacmvrec1.setEffdate(recopstrec.getCostdate());
		lifacmvrec1.setRcamt(BigDecimal.ZERO);
		lifacmvrec1.setFrcdate(CommonConstants.MAXDATE);
		lifacmvrec1.setTransactionDate(recopstrec.getEffdate());
		lifacmvrec1.setTransactionTime(DatimeUtil.getCurrentTimeHHmmssSS());
		lifacmvrec1.setUser(0);
		lifacmvrec1.setTermid("");
		lifacmvrec1.getSubstituteCodes().set(0, recopstrec.getCnttype());
		lifacmvrec1.setTranref(recopstrec.getChdrnum());
		
		wsaaRldgacct.append(recopstrec.getChdrnum());
		wsaaRldgacct.append(recopstrec.getLife());
		wsaaRldgacct.append(recopstrec.getCoverage());
		wsaaRldgacct.append(recopstrec.getRider());
		wsaaRldgacct.append(recopstrec.getPlanSuffix());
		
		if (StringUtils.equals(t5688.getComlvlacc(), "Y")) {
			lifacmvrec1.getSubstituteCodes().set(5, recopstrec.getCrtable());
		} else {
			lifacmvrec1.getSubstituteCodes().set(5, "");
		}
		costingPosting();
	}

	protected void costingPosting() throws IOException, ParseException {
		postPremium();
		postCommission();
		postNetPrem();
		postRefundFee();
	}

	protected void postPremium() throws IOException, ParseException {
		lifacmvrec1.setOrigamt(recopstrec.getPrem());
		if (StringUtils.equals(t5688.getComlvlacc(), "Y")) {
			wsaaSub1 = 4;
			lifacmvrec1.setRldgacct(wsaaRldgacct.toString());
		} else {
			wsaaSub1 = 1;
			lifacmvrec1.setRldgacct(recopstrec.getChdrnum());
		}
		callLifacmv();
	}

	protected void postCommission() throws IOException, ParseException {
		lifacmvrec1.setOrigamt(recopstrec.getCompay());
		if (StringUtils.equals(t5688.getComlvlacc(), "Y")) {
			wsaaSub1 = 5;
			lifacmvrec1.setRldgacct(wsaaRldgacct.toString());
		} else {
			wsaaSub1 = 2;
			lifacmvrec1.setRldgacct(recopstrec.getChdrnum());
		}
		callLifacmv();
	}

	protected void postNetPrem() throws IOException, ParseException {
		lifacmvrec1.setOrigamt(wsaaNetPrem);
		lifacmvrec1.setRldgacct(recopstrec.getRasnum());
		wsaaSub1 = 3;
		callLifacmv();
	}

	protected void postRefundFee() throws IOException, ParseException {
		lifacmvrec1.setOrigamt(recopstrec.getRefundfe());
		if (StringUtils.equals(t5688.getComlvlacc(), "Y")) {
			wsaaSub1 = 7;
			lifacmvrec1.setRldgacct(wsaaRldgacct.toString());
		} else {
			wsaaSub1 = 6;
			lifacmvrec1.setRldgacct(recopstrec.getChdrnum());
		}
		callLifacmv();
	}

	protected void callLifacmv() throws IOException, ParseException {
		if (lifacmvrec1.getOrigamt().compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		lifacmvrec1.setSacscode(t5645.getSacscodes().get(wsaaSub1-1));
		lifacmvrec1.setSacstyp(t5645.getSacstypes().get(wsaaSub1-1));
		lifacmvrec1.setGlcode(t5645.getGlmaps().get(wsaaSub1-1));
		lifacmvrec1.setGlsign(t5645.getSigns().get(wsaaSub1-1));
		lifacmvrec1.setContot(t5645.getCnttots().get(wsaaSub1-1));
		lifacmv.executePSTW(lifacmvrec1);
	}

}
