package com.dxc.integral.life.dao;

import java.math.BigDecimal;
import java.util.List;

import com.dxc.integral.life.dao.model.Zrstpf;

/**
 * 
 * @author xma3
 *
 */

public interface ZrstpfDAO {

	public List<Zrstpf> getZrstpfRecord(String coy, String chdrnum, String life);
	public int insertZrstpf(Zrstpf zrstpf);
	public int updateZrstpfZramount(BigDecimal zramount, long uniqueNumber);

}
