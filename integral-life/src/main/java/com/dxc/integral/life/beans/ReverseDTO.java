package com.dxc.integral.life.beans;

public class ReverseDTO {
	
	public String statuz = "";
	public String company = "";
	public String chdrnum = "";
	public String life = "";
	public String coverage = "";
	public String rider = "";
	public int planSuffix = 0;
	public String batchkey = "";
	public String contkey = "";
	public String batcpfx = "";
	public String batccoy = "";
	public String batcbrn = "";
	public int batcactyr = 0;
	public int batcactmn = 0;
	public String batctrcde = "";
	public String batcbatch = "";
	public int effdate1 = 0;
	public int effdate2 = 0;
	public int tranno = 0;
	public int newTranno = 0;
	public String oldBatctrcde = "";
	public String language = "";
	public int transDate = 0;
	public int transTime = 0;
	public int user = 0;
	public String termid = "";
	public int ptrneff = 0;
	public String ptrnBatchkey = "";
	public String ptrnBatcpfx = "";
	public String ptrnBatccoy = "";
	public String ptrnBatcbrn = "";
	public int ptrnBatcactyr = 0;
	public int ptrnBatcactmn = 0;
	public String ptrnBatctrcde = "";
	public String ptrnBatcbatch = "";
	public int bbldat = 0;
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getBatchkey() {
		return batchkey;
	}
	public void setBatchkey(String batchkey) {
		this.batchkey = batchkey;
	}
	public String getContkey() {
		return contkey;
	}
	public void setContkey(String contkey) {
		this.contkey = contkey;
	}
	public String getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getEffdate1() {
		return effdate1;
	}
	public void setEffdate1(int effdate1) {
		this.effdate1 = effdate1;
	}
	public int getEffdate2() {
		return effdate2;
	}
	public void setEffdate2(int effdate2) {
		this.effdate2 = effdate2;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getNewTranno() {
		return newTranno;
	}
	public void setNewTranno(int newTranno) {
		this.newTranno = newTranno;
	}
	public String getOldBatctrcde() {
		return oldBatctrcde;
	}
	public void setOldBatctrcde(String oldBatctrcde) {
		this.oldBatctrcde = oldBatctrcde;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public int getTransDate() {
		return transDate;
	}
	public void setTransDate(int transDate) {
		this.transDate = transDate;
	}
	public int getTransTime() {
		return transTime;
	}
	public void setTransTime(int transTime) {
		this.transTime = transTime;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getPtrneff() {
		return ptrneff;
	}
	public void setPtrneff(int ptrneff) {
		this.ptrneff = ptrneff;
	}
	public String getPtrnBatchkey() {
		return ptrnBatchkey;
	}
	public void setPtrnBatchkey(String ptrnBatchkey) {
		this.ptrnBatchkey = ptrnBatchkey;
	}
	public String getPtrnBatcpfx() {
		return ptrnBatcpfx;
	}
	public void setPtrnBatcpfx(String ptrnBatcpfx) {
		this.ptrnBatcpfx = ptrnBatcpfx;
	}
	public String getPtrnBatccoy() {
		return ptrnBatccoy;
	}
	public void setPtrnBatccoy(String ptrnBatccoy) {
		this.ptrnBatccoy = ptrnBatccoy;
	}
	public String getPtrnBatcbrn() {
		return ptrnBatcbrn;
	}
	public void setPtrnBatcbrn(String ptrnBatcbrn) {
		this.ptrnBatcbrn = ptrnBatcbrn;
	}
	public int getPtrnBatcactyr() {
		return ptrnBatcactyr;
	}
	public void setPtrnBatcactyr(int ptrnBatcactyr) {
		this.ptrnBatcactyr = ptrnBatcactyr;
	}
	public int getPtrnBatcactmn() {
		return ptrnBatcactmn;
	}
	public void setPtrnBatcactmn(int ptrnBatcactmn) {
		this.ptrnBatcactmn = ptrnBatcactmn;
	}
	public String getPtrnBatctrcde() {
		return ptrnBatctrcde;
	}
	public void setPtrnBatctrcde(String ptrnBatctrcde) {
		this.ptrnBatctrcde = ptrnBatctrcde;
	}
	public String getPtrnBatcbatch() {
		return ptrnBatcbatch;
	}
	public void setPtrnBatcbatch(String ptrnBatcbatch) {
		this.ptrnBatcbatch = ptrnBatcbatch;
	}
	public int getBbldat() {
		return bbldat;
	}
	public void setBbldat(int bbldat) {
		this.bbldat = bbldat;
	}
    
	
}
