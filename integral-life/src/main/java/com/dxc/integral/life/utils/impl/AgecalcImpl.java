package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.AgecalcDTO;
import com.dxc.integral.life.smarttable.pojo.T2240;
import com.dxc.integral.life.utils.Agecalc;
/**
 * @author wli31
 */
@Service("agecalc")
@Lazy
public class AgecalcImpl implements Agecalc {
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;
	@Autowired
	private Datcon3 datcon3;

	public AgecalcDTO processAgecalc(AgecalcDTO agecalcDTO) throws IOException, NumberFormatException, ParseException{
		T2240 t2240 = readAgeCalcTable2000(agecalcDTO);
		int freqFactor = datcon3.getYearsDifference(Integer.parseInt(agecalcDTO.getIntDate1()), Integer.parseInt(agecalcDTO.getIntDate2())).intValue();
		if (null != t2240.getAgecodes().get(0) && !"".equals(t2240.getAgecodes().get(0))) {
			int agerating = new BigDecimal(freqFactor).add(new BigDecimal(.99999)).intValue();
			agecalcDTO.setAgerating(agerating);
		} else {
			if (null != t2240.getAgecodes().get(1) && !"".equals(t2240.getAgecodes().get(1))) {
				calculateAgeNearest3200(agecalcDTO, freqFactor);
			} else {
				if (null != t2240.getAgecodes().get(2) && !"".equals(t2240.getAgecodes().get(2))) {
					agecalcDTO.setAgerating(freqFactor);
				} else {
					if (null != t2240.getAgecodes().get(3) && !"".equals(t2240.getAgecodes().get(3))) {
						calculateAgeCalendarYear3400(agecalcDTO);
					}
				}
			}
		}
		return agecalcDTO;
	}

	private void calculateAgeCalendarYear3400(AgecalcDTO agecalcDTO) {
		/* CALCULATE-AGE */
		agecalcDTO.setYear1(Integer.parseInt(agecalcDTO.getIntDate1().substring(0, 4)));
		agecalcDTO.setYear2(Integer.parseInt(agecalcDTO.getIntDate2().substring(0, 4)));
		agecalcDTO.setAgerating(agecalcDTO.getYear2() - agecalcDTO.getYear1());
		/* EXIT */
	}

	private void calculateAgeNearest3200(AgecalcDTO agecalcDTO, int wsaaCompleteNoYr) throws NumberFormatException, ParseException  {
		int freqFactor = datcon3.getMonthsDifference(Integer.parseInt(agecalcDTO.getIntDate1()), Integer.parseInt(agecalcDTO.getIntDate2())).intValue();
		int wsaaOsMth = freqFactor - (wsaaCompleteNoYr * 12);
		if (wsaaOsMth >= 6) {
			agecalcDTO.setAgerating(wsaaCompleteNoYr + 1);
		} else {
			agecalcDTO.setAgerating(wsaaCompleteNoYr);
		}

	}

	private T2240 readAgeCalcTable2000(AgecalcDTO agecalcDTO) throws IOException {
		String itemitem = agecalcDTO.getLanguage().concat(agecalcDTO.getCnttype());

		Map<String, Itempf> t2240Map = itempfDAO.readSmartTableByTableName2(agecalcDTO.getCompany(), "T2240", CommonConstants.IT_ITEMPFX);
		if(null == t2240Map.get(itemitem)){
			itemitem = agecalcDTO.getLanguage().concat("***");
			if(null == t2240Map.get(itemitem)){
				throw new ItemNotfoundException("Item "+itemitem+" not found in Table T2240");
			}
		}
		T2240 t2240 = smartTableDataUtils.convertGenareaToPojo(t2240Map.get(itemitem).getGenareaj(), T2240.class);
		int wsaaCount = 0;
		for (int wsaaIndex = 0; wsaaIndex < 4; wsaaIndex++) {
			if (!t2240.getAgecodes().get(wsaaIndex).trim().isEmpty()) {
				wsaaCount++;
			}
		}
		if (wsaaCount != 1) {
			throw new RuntimeException();
		}
		return t2240;
	}

}
