package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T6698 {
	
	private List<Integer> ageIssageTos;
	private String dssnum;
	private BigDecimal earningCap;
	private String inrevnum;
	private List<String> pclimits;
	private BigDecimal taxrelpc;
	public List<Integer> getAgeIssageTos() {
		return ageIssageTos;
	}
	public void setAgeIssageTos(List<Integer> ageIssageTos) {
		this.ageIssageTos = ageIssageTos;
	}
	public String getDssnum() {
		return dssnum;
	}
	public void setDssnum(String dssnum) {
		this.dssnum = dssnum;
	}
	public BigDecimal getEarningCap() {
		return earningCap;
	}
	public void setEarningCap(BigDecimal earningCap) {
		this.earningCap = earningCap;
	}
	public String getInrevnum() {
		return inrevnum;
	}
	public void setInrevnum(String inrevnum) {
		this.inrevnum = inrevnum;
	}
	public List<String> getPclimits() {
		return pclimits;
	}
	public void setPclimits(List<String> pclimits) {
		this.pclimits = pclimits;
	}
	public BigDecimal getTaxrelpc() {
		return taxrelpc;
	}
	public void setTaxrelpc(BigDecimal taxrelpc) {
		this.taxrelpc = taxrelpc;
	}
	
	

}
