package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.PayrpfDAO;
import com.dxc.integral.life.dao.model.Payrpf;
/**
 * @author wli31
 */
@Repository("payrpfDAO")
@Lazy
public class PayrpfDAOImpl extends BaseDAOImpl implements PayrpfDAO{
	public Payrpf getPayrRecord(String chdrcoy,String chdrnum){
		 StringBuilder sb = new StringBuilder("SELECT * FROM PAYRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND PAYRSEQNO = '1'  AND VALIDFLAG = '1'");
         sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
		 return jdbcTemplate.queryForObject(sb.toString(), new Object[] {chdrcoy, chdrnum},
					new BeanPropertyRowMapper<Payrpf>(Payrpf.class));
	}
	
	// by yyang21
	@Override
	public List<Payrpf> searchPayrRecord(String chdrcoy, String chdrnum) {
		List<Payrpf> payrList = null;
		String sql = "SELECT * FROM PAYRPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1' ORDER BY CHDRCOY, CHDRNUM ";
		payrList = jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum },
				new BeanPropertyRowMapper<Payrpf>(Payrpf.class));
		return payrList;
	}
	
	public void updatePayrpfRecords(List<Payrpf> payrpfList){
		List<Payrpf> tempPayrpfList = payrpfList;   
		StringBuilder sb = new StringBuilder("update payrpf set validflag=?,usrprf=?,");
        sb.append("jobnm=?,datime=? where unique_number=?");
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempPayrpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, tempPayrpfList.get(i).getValidflag());
				ps.setString(2, tempPayrpfList.get(i).getUsrprf());
                ps.setString(3, tempPayrpfList.get(i).getJobnm());
                ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(5, tempPayrpfList.get(i).getUniqueNumber());
            }  
      }); 
	}
	
	public void insertPayrRecords(List<Payrpf> payrpfList){
		final List<Payrpf> tempPayrpfList = payrpfList;   
	       String sql = "insert into payrpf(chdrcoy,chdrnum,payrseqno,effdate,validflag,billchnl,billfreq,billcd,nextdate,btdate,ptdate,billcurr,cntcurr,sinstamt01,sinstamt02,"
					+" sinstamt03,sinstamt04,sinstamt05,sinstamt06,outstamt,taxrelmth,billsupr,aplsupr,notssupr,billspfrom,aplspfrom,notsspfrom,billspto,aplspto,notsspto,"
					+"mandref,grupkey,incseqno,tranno,grupcoy,grupnum,membsel,billnet,billday,billmonth,duedd,duemm,termid,trdt,trtm,user_t,pstcde,zmandref,usrprf,jobnm,datime)"
		    		+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	       jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
	            @Override
	            public int getBatchSize() {  
	                 return tempPayrpfList.size();  
	            }  
	            @Override  
	            public void setValues(PreparedStatement ps, int i)  
	                    throws SQLException { 
	            	ps.setString(1, tempPayrpfList.get(i).getChdrcoy());
					ps.setString(2, tempPayrpfList.get(i).getChdrnum());
					ps.setInt(3, tempPayrpfList.get(i).getPayrseqno());
					ps.setInt(4, tempPayrpfList.get(i).getEffdate());
					ps.setString(5, tempPayrpfList.get(i).getValidflag());
					ps.setString(6, tempPayrpfList.get(i).getBillchnl());
					ps.setString(7, tempPayrpfList.get(i).getBillfreq());
					ps.setInt(8, tempPayrpfList.get(i).getBillcd());
					ps.setInt(9, tempPayrpfList.get(i).getNextdate());
					ps.setInt(10, tempPayrpfList.get(i).getBtdate());
					ps.setInt(11, tempPayrpfList.get(i).getPtdate());
					ps.setString(12, tempPayrpfList.get(i).getBillcurr());
					ps.setString(13, tempPayrpfList.get(i).getCntcurr());
					ps.setBigDecimal(14, tempPayrpfList.get(i).getSinstamt01());
					ps.setBigDecimal(15, tempPayrpfList.get(i).getSinstamt02());
					ps.setBigDecimal(16, tempPayrpfList.get(i).getSinstamt03());
					ps.setBigDecimal(17, tempPayrpfList.get(i).getSinstamt04());
					ps.setBigDecimal(18, tempPayrpfList.get(i).getSinstamt05());
					ps.setBigDecimal(19, tempPayrpfList.get(i).getSinstamt06());
					ps.setBigDecimal(20, tempPayrpfList.get(i).getOutstamt());
					ps.setString(21, tempPayrpfList.get(i).getTaxrelmth());
					ps.setString(22, tempPayrpfList.get(i).getBillsupr());
					ps.setString(23, tempPayrpfList.get(i).getAplsupr());
					ps.setString(24, tempPayrpfList.get(i).getNotssupr());
					ps.setInt(25, tempPayrpfList.get(i).getBillspfrom());
					ps.setInt(26, tempPayrpfList.get(i).getAplspfrom());
					ps.setInt(27, tempPayrpfList.get(i).getNotsspfrom());
					ps.setInt(28, tempPayrpfList.get(i).getBillspto());
					ps.setInt(29, tempPayrpfList.get(i).getAplspto());
					ps.setInt(30, tempPayrpfList.get(i).getNotsspto());
					ps.setString(31, tempPayrpfList.get(i).getMandref());
					ps.setString(32, tempPayrpfList.get(i).getGrupkey());
					ps.setInt(33, tempPayrpfList.get(i).getIncomeSeqNo() == null ? 0 : tempPayrpfList.get(i).getIncomeSeqNo());
					ps.setInt(34, tempPayrpfList.get(i).getTranno());
					ps.setString(35, tempPayrpfList.get(i).getGrupcoy());
					ps.setString(36, tempPayrpfList.get(i).getGrupnum());
					ps.setString(37, tempPayrpfList.get(i).getMembsel());
					ps.setString(38, tempPayrpfList.get(i).getBillnet());
					ps.setString(39, tempPayrpfList.get(i).getBillday());
					ps.setString(40, tempPayrpfList.get(i).getBillmonth());
					ps.setString(41, tempPayrpfList.get(i).getDuedd());
					ps.setString(42, tempPayrpfList.get(i).getDuemm());
					ps.setString(43, tempPayrpfList.get(i).getTermid());
					ps.setInt(44, tempPayrpfList.get(i).getTrdt());
					ps.setInt(45, tempPayrpfList.get(i).getTrtm());
					ps.setInt(46, tempPayrpfList.get(i).getUser_t());
					ps.setString(47, tempPayrpfList.get(i).getPstcde());
					ps.setString(48, tempPayrpfList.get(i).getZmandref());
					ps.setString(49, tempPayrpfList.get(i).getUsrprf());
					ps.setString(50, tempPayrpfList.get(i).getJobnm());
					ps.setTimestamp(51,new Timestamp(System.currentTimeMillis()));
	            }   
	      });  
	}

	@Override
	public int updatePayrRecord(Payrpf payrpf) {
		String sql = "UPDATE PAYRPF SET TRANNO=?, TRDT=?, TRTM=?, USER_T=? WHERE CHDRCOY = ? AND CHDRNUM = ? AND PAYRSEQNO = ? ";
		return jdbcTemplate.update(sql,
				new Object[] { payrpf.getTranno(), payrpf.getTransactionDate(), payrpf.getTransactionTime(), payrpf.getUser_t(), payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getPayrseqno() });
	}
	
	@Override
	public int deletePayrpfRecord(String chdrcoy, String chdrnum, int payrseqno) {
		String sql = "DELETE FROM PAYRPF WHERE CHDRCOY=? AND CHDRNUM=? AND PAYRSEQNO=? ";
		return jdbcTemplate.update(sql, new Object[]{chdrcoy, chdrnum, payrseqno});
		
	
	}
	@Override
	public Payrpf getPayrRecord(String chdrcoy,String chdrnum, int payrseqno){
		 StringBuilder sb = new StringBuilder("SELECT * FROM PAYRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND PAYRSEQNO = ?  ");
         sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, PAYRSEQNO ASC, UNIQUE_NUMBER DESC");
  
		 return jdbcTemplate.queryForObject(sb.toString(), new Object[] {chdrcoy, chdrnum, payrseqno},
					new BeanPropertyRowMapper<Payrpf>(Payrpf.class));
	}
	@Override
	public int updatePayrpfRecord(String chdrcoy,String chdrnum, int payrseqno, String validflag) {
		
		 String sql ="UPDATE PAYRPF SET VALIDFLAG=? WHERE CHDRCOY = ? AND CHDRNUM = ? AND PAYRSEQNO = ?  ";
  
		 return jdbcTemplate.update(sql, new Object[] {validflag, chdrcoy, chdrnum, payrseqno});
		
	}
	
	public void updatePayrPstCde(List<Payrpf> payrpfList){
		List<Payrpf> tempPayrpfList = payrpfList;   
		StringBuilder sb = new StringBuilder(" UPDATE PAYRPF SET PSTCDE= ?, BTDATE=?, BILLCD=?, JOBNM=?,USRPRF=?,DATIME=? ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND PAYRSEQNO=? AND VALIDFLAG='1' ");     
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempPayrpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, tempPayrpfList.get(i).getPstatcode());
				ps.setInt(2, tempPayrpfList.get(i).getBtdate());
                ps.setInt(3, tempPayrpfList.get(i).getBillcd());
                ps.setString(4, tempPayrpfList.get(i).getUsrprf());
                ps.setString(5, tempPayrpfList.get(i).getJobnm());
                ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));	
			    ps.setString(7, tempPayrpfList.get(i).getChdrcoy());
	            ps.setString(8, tempPayrpfList.get(i).getChdrnum());
	            ps.setInt(9, tempPayrpfList.get(i).getPayrseqno());
            }  
      }); 
	}
	@Override
	public int updatePayrpfRecord(Payrpf payrpf) {
		String sql = "UPDATE PAYRPF SET OUTSTAMT=?, BTDATE=?, BILLCD=?, NEXTDATE=?, TRANNO=? WHERE CHDRCOY=? AND CHDRNUM=? AND PAYRSEQNO=?";
		return jdbcTemplate.update(sql, new Object[] {payrpf.getOutstamt(), payrpf.getBtdate(), payrpf.getBillcd(), payrpf.getNextdate(), payrpf.getTranno(),
				payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getPayrseqno()});
	}
	
	@Override
	public List<Payrpf> searchPayrlifRecord(String chdrcoy, String chdrnum) {
		List<Payrpf> payrList = null;
		String sql = "SELECT * FROM PAYRPF WHERE CHDRCOY=? AND CHDRNUM=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, PAYRSEQNO ASC, UNIQUE_NUMBER DESC ";
		payrList = jdbcTemplate.query(sql, new Object[] { chdrcoy, chdrnum },
				new BeanPropertyRowMapper<Payrpf>(Payrpf.class));
		return payrList;
	}
}
