package com.dxc.integral.life.dao.model;

import java.util.Date;

public class Linkagepf {
	
	private long unique_Number;
	private String linkref;
	private String linktype;
	private String linkinfo;
	private String username;
	private Date datime;
	private String updtuser;
	private Date updtime;
	
	
	public long getUnique_Number() {
		return unique_Number;
	}
	public void setUnique_Number(long unique_Number) {
		this.unique_Number = unique_Number;
	}
	public String getLinkref() {
		return linkref;
	}
	public void setLinkref(String linkref) {
		this.linkref = linkref;
	}
	public String getLinktype() {
		return linktype;
	}
	public void setLinktype(String linktype) {
		this.linktype = linktype;
	}
	public String getLinkinfo() {
		return linkinfo;
	}
	public void setLinkinfo(String linkinfo) {
		this.linkinfo = linkinfo;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}
	public String getUpdtuser() {
		return updtuser;
	}
	public void setUpdtuser(String updtuser) {
		this.updtuser = updtuser;
	}
	public Date getUpdtime() {
		return updtime;
	}
	public void setUpdtime(Date updtime) {
		this.updtime = updtime;
	}

}
