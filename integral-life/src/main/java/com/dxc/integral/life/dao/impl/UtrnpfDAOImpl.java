package com.dxc.integral.life.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.UtrnpfDAO;
import com.dxc.integral.life.dao.model.Utrnpf;

/**
 * 
 * @author xma3
 *
 */

@Repository("utrnpfDAO")
@Lazy
public class UtrnpfDAOImpl extends BaseDAOImpl implements UtrnpfDAO{
	@Override
	public int insertUtrnpfRecord(Utrnpf utrnpf) {
		StringBuffer sb = new StringBuffer("INSERT INTO UtrnPF(chdrcoy,chdrnum,life,coverage,rider,plnsfx,tranno,termid,trdt,trtm,user_t,jctljnumpr,vrtfnd,unitsa,strpdate,");
		sb.append("ndfind,nofunt,unityp,batccoy,batcbrn,batcactyr,batcactmn,batctrcde,batcbatch,pricedt,moniesdt,priceused,crtable,cntcurr,nofdunt,fdbkind,covdbtind,ubrepr,");
		sb.append("incinum,inciperd01,inciperd02,inciprm01,inciprm02,ustmno,cntamnt,fndcurr,fundamnt,fundrate,sacscode,sacstyp,genlcde,contyp,triger,trigky,prcseq,svp,");
		sb.append("discfa,persur,crcdte,ciuind,swchind,bschednam,bschednum,JOBNM,USRPRF,DATIME,FUNDPOOL) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		return jdbcTemplate.update(sb.toString(), new Object[]{
				utrnpf.getChdrcoy(),utrnpf.getChdrnum(),utrnpf.getLife(),utrnpf.getCoverage(),utrnpf.getRider(),utrnpf.getPlanSuffix(),utrnpf.getTranno(),utrnpf.getTermid()
				,utrnpf.getTransactionDate(),utrnpf.getTransactionTime(),utrnpf.getUser(),utrnpf.getJobnoPrice(),utrnpf.getUnitVirtualFund(),utrnpf.getUnitSubAccount()
				,utrnpf.getStrpdate(),utrnpf.getNowDeferInd(),utrnpf.getNofUnits(),utrnpf.getUnitType(),utrnpf.getBatccoy(),utrnpf.getBatcbrn(),utrnpf.getBatcactyr(),
				utrnpf.getBatcactmn(),utrnpf.getBatctrcde(),utrnpf.getBatcbatch(),utrnpf.getPriceDateUsed(),utrnpf.getMoniesDate(),utrnpf.getPriceUsed(),utrnpf.getCrtable()
				,utrnpf.getCntcurr(),utrnpf.getNofDunits(),utrnpf.getFeedbackInd(),utrnpf.getCovdbtind(),utrnpf.getUnitBarePrice(),utrnpf.getInciNum(),utrnpf.getInciPerd01()
				,utrnpf.getInciPerd02(),utrnpf.getInciprm01(),utrnpf.getInciprm02(),utrnpf.getUstmno(),utrnpf.getContractAmount(),utrnpf.getFundCurrency().trim()
				,utrnpf.getFundAmount(),utrnpf.getFundRate(),utrnpf.getSacscode(),utrnpf.getSacstyp(),utrnpf.getGenlcde(),utrnpf.getCnttyp(),utrnpf.getTriggerModule()
				,utrnpf.getTriggerKey(),utrnpf.getProcSeqNo(),utrnpf.getSvp(),utrnpf.getDiscountFactor(),utrnpf.getSurrenderPercent(),utrnpf.getCrComDate()
				,utrnpf.getCanInitUnitInd(),utrnpf.getSwitchIndicator(),utrnpf.getScheduleName(),utrnpf.getScheduleNumber(),utrnpf.getJobName(),utrnpf.getUserProfile()				
				,new Timestamp(System.currentTimeMillis()), utrnpf.getFundPool()
			});
	}
	@Override
	public int deleteUtrnpfRecord(Utrnpf utrnpf) {
		
		StringBuffer sb = new StringBuffer("DELETE FROM UTRNPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? AND PLNSFX=? AND COVERAGE=? AND RIDER=? AND LIFE=? AND VRTFND=? ");
		sb.append("AND UNITYP=? AND BATCCOY=? AND BATCBRN=? AND BATCACTYR=? AND BATCACTMN=? AND BATCTRCDE=? AND  BATCBATCH=?");
		
		return jdbcTemplate.update(sb.toString(), new Object[] {utrnpf.getChdrcoy(), utrnpf.getChdrnum(), utrnpf.getTranno(), utrnpf.getPlanSuffix(), utrnpf.getCoverage(), 
				utrnpf.getRider(), utrnpf.getLife(), utrnpf.getUnitVirtualFund(), utrnpf.getUnitType(), utrnpf.getBatccoy(), utrnpf.getBatcbrn(), utrnpf.getBatcactyr(), 
				utrnpf.getBatcactmn(), utrnpf.getBatctrcde(), utrnpf.getBatcbatch()});
	}
	
	@Override
	public List<Utrnpf> getUtrnpfRecords(String chdrcoy, String chdrnum, int tranno, int plnsfx, String coverage, String rider, String life){
		StringBuffer sb = new StringBuffer("SELECT * FROM UTRNPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? AND PLNSFX=? AND COVERAGE=? AND RIDER=? AND LIFE=? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC,TRANNO ASC,PLNSFX ASC,COVERAGE ASC,RIDER ASC,LIFE ASC,UNIQUE_NUMBER DESC");
		
		return jdbcTemplate.query(sb.toString(), new Object[] {chdrcoy, chdrnum, tranno, plnsfx, coverage, rider, life}, new BeanPropertyRowMapper<Utrnpf>(Utrnpf.class) );  
		
	}

}
