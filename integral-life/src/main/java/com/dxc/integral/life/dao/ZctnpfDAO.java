package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Zctnpf;

public interface ZctnpfDAO {
	 public void insertZctnpfRecords(List<Zctnpf> zctnpfList);
}
