package com.dxc.integral.life.utils;

import java.io.IOException;
import java.text.ParseException;

import com.dxc.integral.life.beans.ReverseDTO;

public interface Revodue {
	public ReverseDTO processRevodue(ReverseDTO reverseDTO) throws IOException, ParseException;
}
