package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * 
 * @author fwang3
 *
 *
 */
public class TrmracdDTO {

	private String function;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String cnttype;
	private String crtable;
	private int seqno;
	private String clntcoy;
	private String l1Clntnum;
	private String jlife;
	private String l2Clntnum;
	private int effdate;
	private BigDecimal newRaAmt;
	private BigDecimal recovamt;
	private int crrcd;
	private String lrkcls;
	private String batctrcde;

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public int getSeqno() {
		return seqno;
	}

	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	public String getClntcoy() {
		return clntcoy;
	}

	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}

	public String getL1Clntnum() {
		return l1Clntnum;
	}

	public void setL1Clntnum(String l1Clntnum) {
		this.l1Clntnum = l1Clntnum;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getL2Clntnum() {
		return l2Clntnum;
	}

	public void setL2Clntnum(String l2Clntnum) {
		this.l2Clntnum = l2Clntnum;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public BigDecimal getNewRaAmt() {
		return newRaAmt;
	}

	public void setNewRaAmt(BigDecimal newRaAmt) {
		this.newRaAmt = newRaAmt;
	}

	public BigDecimal getRecovamt() {
		return recovamt;
	}

	public void setRecovamt(BigDecimal recovamt) {
		this.recovamt = recovamt;
	}

	public int getCrrcd() {
		return crrcd;
	}

	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}

	public String getLrkcls() {
		return lrkcls;
	}

	public void setLrkcls(String lrkcls) {
		this.lrkcls = lrkcls;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

}