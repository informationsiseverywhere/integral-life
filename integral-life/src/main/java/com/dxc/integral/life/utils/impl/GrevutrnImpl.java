package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.beans.GreversDTO;
import com.dxc.integral.life.dao.HitdpfDAO;
import com.dxc.integral.life.dao.HitrpfDAO;
import com.dxc.integral.life.dao.UtrnpfDAO;
import com.dxc.integral.life.dao.model.Hitdpf;
import com.dxc.integral.life.dao.model.Hitrpf;
import com.dxc.integral.life.dao.model.Utrnpf;

@Service("grevutrn")
@Lazy
public class GrevutrnImpl {

@Autowired
private HitrpfDAO hitrpfDAO;
@Autowired
private UtrnpfDAO utrnpfDAO;
@Autowired
private HitdpfDAO hitdpfDAO;

	
	public GreversDTO processGrevutrn(GreversDTO greversDTO) {
		
		greversDTO.setStatuz("****");
		greversDTO.setContractAmount(BigDecimal.ZERO);
		greversDTO = processHitrs(greversDTO);
		greversDTO = mainProcessing(greversDTO);
		if(!hitd(greversDTO)) {
			greversDTO.setStatuz("BOMB");
		}
		
		return greversDTO;
		
	}
	
	private GreversDTO processHitrs(GreversDTO greversDTO) {
		List<Hitrpf> list = hitrpfDAO.getHitrpfRecords(greversDTO.getChdrcoy(), greversDTO.getChdrnum(), greversDTO.getLife(), greversDTO.getCoverage(), 
				greversDTO.getRider(), greversDTO.getPlanSuffix(), greversDTO.getTranno());
		if(list == null || list.size() == 0) {
			return greversDTO;
		}
		
		for(Hitrpf hitrpf : list) {
			greversDTO.setContractAmount(greversDTO.getContractAmount().add(hitrpf.getContractAmount()));
            if("".equals(hitrpf.getFeedbackInd().trim())) {
            	hitrpfDAO.deleteHitrpfRecord(hitrpf);
            }else {
            	hitrpf.setFeedbackInd(" ");
            	hitrpf.setTriggerModule(" ");
            	hitrpf.setTriggerKey(" ");   
            	hitrpf.setSurrenderPercent(BigDecimal.ZERO);
            	hitrpf.setProcSeqNo(hitrpf.getProcSeqNo()*-1);
    			hitrpf.setContractAmount(hitrpf.getContractAmount().multiply(new BigDecimal(-1)));
    			hitrpf.setFundAmount(hitrpf.getFundAmount().multiply(new BigDecimal(-1)));
    			hitrpf.setTranno(greversDTO.getNewTranno());
    			hitrpf.setBatctrcde(greversDTO.getBatckey().substring(10, 14));
    			hitrpf.setBatccoy(greversDTO.getBatckey().substring(2, 3));   	
    			hitrpf.setBatcbrn(greversDTO.getBatckey().substring(3, 5));
    			hitrpf.setBatcactyr(Integer.parseInt(greversDTO.getBatckey().substring(5, 9)));
    			hitrpf.setBatcactmn(Integer.parseInt(greversDTO.getBatckey().substring(8, 10)));
    			hitrpf.setBatcbatch(greversDTO.getBatckey().substring(14, 19));
    			
    			hitrpfDAO.insertHitrpfRecord(hitrpf);
            }
		}
		return greversDTO;
	}
	
	private GreversDTO mainProcessing(GreversDTO greversDTO) {
		List<Utrnpf> list = utrnpfDAO.getUtrnpfRecords(greversDTO.getChdrcoy(), greversDTO.getChdrnum(), greversDTO.getTranno(), greversDTO.getPlanSuffix(), 
				greversDTO.getCoverage(), greversDTO.getRider(), greversDTO.getLife());
		if(list == null || list.size() == 0) {
			return greversDTO;
		}
		
		Utrnpf tmpUtrnpf;
		for(Utrnpf utrnpf : list) {
			greversDTO.setContractAmount(greversDTO.getContractAmount().add(utrnpf.getContractAmount()));
			if("".equals(utrnpf.getFeedbackInd().trim())) {
				utrnpfDAO.deleteUtrnpfRecord(utrnpf);
			
			}else {
				tmpUtrnpf = new Utrnpf();
				tmpUtrnpf = utrnpf;
				tmpUtrnpf.setFeedbackInd(" ");
				tmpUtrnpf.setTriggerModule(" ");
				tmpUtrnpf.setTriggerKey(" ");
				tmpUtrnpf.setSurrenderPercent(BigDecimal.ZERO);
				tmpUtrnpf.setProcSeqNo(tmpUtrnpf.getProcSeqNo()*-1);	
				tmpUtrnpf.setContractAmount(tmpUtrnpf.getContractAmount().multiply(new BigDecimal(-1)));				
				tmpUtrnpf.setFundAmount(tmpUtrnpf.getFundAmount().multiply(new BigDecimal(-1)));
				tmpUtrnpf.setNofDunits(tmpUtrnpf.getNofDunits().multiply(new BigDecimal(-1)));
				tmpUtrnpf.setNofUnits(tmpUtrnpf.getNofUnits().multiply(new BigDecimal(-1)));
				tmpUtrnpf.setTransactionDate(greversDTO.getTransDate());
				tmpUtrnpf.setTransactionTime(greversDTO.getTransTime());
				tmpUtrnpf.setTranno(greversDTO.getNewTranno());
				tmpUtrnpf.setTermid(greversDTO.getTermid());
				tmpUtrnpf.setUser(greversDTO.getUser());
			
				tmpUtrnpf.setBatctrcde(greversDTO.getBatckey().substring(10, 14));
				tmpUtrnpf.setBatccoy(greversDTO.getBatckey().substring(2, 3));
				tmpUtrnpf.setBatcbrn(greversDTO.getBatckey().substring(3, 5));
				tmpUtrnpf.setBatcactyr(Integer.parseInt(greversDTO.getBatckey().substring(5, 9)));
				tmpUtrnpf.setBatcactmn(Integer.parseInt(greversDTO.getBatckey().substring(8, 10)));
				tmpUtrnpf.setBatcbatch(greversDTO.getBatckey().substring(14, 19));
				
				utrnpfDAO.insertUtrnpfRecord(tmpUtrnpf);
			}
		}
		
		
		return greversDTO;	
	
	}
	
	private boolean hitd(GreversDTO greversDTO){
		
		List<Hitdpf> list = hitdpfDAO.getHitdpfRecords(greversDTO.getChdrcoy(), greversDTO.getChdrnum(), greversDTO.getLife(), greversDTO.getCoverage(), 
				greversDTO.getRider(), greversDTO.getPlanSuffix(), greversDTO.getTranno());
		if(list == null || list.size() == 0) {
			return true;
		}
		for(Hitdpf hitdpf : list) {
			hitdpfDAO.deleteHitdpfRecord(hitdpf.getChdrcoy(), hitdpf.getChdrnum(), hitdpf.getLife(), hitdpf.getCoverage(), hitdpf.getRider(), hitdpf.getPlanSuffix(), 
					hitdpf.getZintbfnd());
		    hitdpf = hitdpfDAO.getHitdRecord(hitdpf.getChdrcoy(), hitdpf.getChdrnum(), hitdpf.getLife(), hitdpf.getCoverage(), hitdpf.getRider(), hitdpf.getPlanSuffix(), 
		    		hitdpf.getZintbfnd());
		    if(hitdpf == null) {
		    	continue;
		    }
		    if(!"2".equals(hitdpf.getValidflag())) {
		    	return false;
		    }
		    
		    if(hitdpfDAO.updateHitdpfRecord(hitdpf.getChdrcoy(), hitdpf.getChdrnum(), hitdpf.getLife(), hitdpf.getCoverage(), hitdpf.getRider(), hitdpf.getPlanSuffix(), 
		    		hitdpf.getZintbfnd())<=0) {
		    	return false;
		    }
			
		}
		
		return true;
		
	}

}
