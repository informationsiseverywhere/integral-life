package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class T5445 {
	
	private String currcode = "";
	private String frequency = "";
	private BigDecimal refundfe;
	private String rndgrqd = "";
	
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public BigDecimal getRefundfe() {
		return refundfe;
	}
	public void setRefundfe(BigDecimal refundfe) {
		this.refundfe = refundfe;
	}
	public String getRndgrqd() {
		return rndgrqd;
	}
	public void setRndgrqd(String rndgrqd) {
		this.rndgrqd = rndgrqd;
	}
    
	
}
