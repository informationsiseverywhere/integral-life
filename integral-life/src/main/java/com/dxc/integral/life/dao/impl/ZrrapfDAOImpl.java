package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.ZrrapfDAO;
import com.dxc.integral.life.dao.model.Zrrapf;

/**
 * 
 * @author xma3
 *
 */
@Repository("zrrapfDAO")
@Lazy
public class ZrrapfDAOImpl  extends BaseDAOImpl implements ZrrapfDAO {
	@Override	
	public Zrrapf getZrrapfRecord(Zrrapf zrrapf){
		StringBuilder sb = new StringBuilder("SELECT * FROM ZRRAPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ?  AND COVERAGE = ? AND RIDER=? AND PLNSFX=? AND  INSTFROM=?");
		sb.append(" AND BILLFREQ=?  ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, BILLFREQ ASC, INSTFROM ASC, UNIQUE_NUMBER DESC");	
		 List<Zrrapf> zrrapfs = jdbcTemplate.query(sb.toString(), new Object[] {zrrapf.getChdrcoy(), zrrapf.getChdrnum(), zrrapf.getLife(), zrrapf.getCoverage(), zrrapf.getRider(), zrrapf.getPlnsfx(), zrrapf.getInstfrom(), zrrapf.getBillfreq()},
					new BeanPropertyRowMapper<Zrrapf>(Zrrapf.class));
		 if (zrrapfs.size() == 0) {
			 return null;
		 }
		 return zrrapfs.get(0);
	}
	@Override
	public int deleteZrrapfRecord(long uniqueNumber){
		String sql = "delete from zrrapf where unique_number = ?";
		return jdbcTemplate.update(sql, new Object[]{uniqueNumber});
		
	}

}
