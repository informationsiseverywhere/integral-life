package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Lextpf;

/**
 * LextpfDAO for database table <b>Lextpf</b> DB operations.
 * 
 *
 */

public interface LextpfDAO {

	/**
	 * Reads Lextpf records.
	 * 
	 */
	public List<Lextpf> checkLextpfRecord(Lextpf lextpf);
	public List<Lextpf> getLextpfListRecord(String chdrcoy,String chdrnum, String life, String coverage,String rider);
}
