package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.ConlinkInputDTO;
import com.dxc.integral.fsu.beans.ConlinkOutputDTO;
import com.dxc.integral.fsu.beans.ZrdecplcDTO;
import com.dxc.integral.fsu.utils.Xcvrt;
import com.dxc.integral.fsu.utils.Zrdecplc;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.life.beans.RexpupdDTO;
import com.dxc.integral.life.dao.LirrpfDAO;
import com.dxc.integral.life.dao.LrrhpfDAO;
import com.dxc.integral.life.dao.model.Lirrpf;
import com.dxc.integral.life.dao.model.Lrrhpf;
import com.dxc.integral.life.utils.Rexpupd;

/**
 * Life Experience Update.
 * 
 * @author fwang3
 *
 */
@Service("rexpupd")
@Lazy
public class RexpupdImpl implements Rexpupd {
	private RexpupdDTO rexpupdDTO;
	private String wsaaClntnum = "";
	private Lrrhpf lrrhconIO = new Lrrhpf();
	private Lirrpf lirrconIO = new Lirrpf();

	@Autowired
	private LrrhpfDAO lrrhpfDAO;
	@Autowired
	private LirrpfDAO lirrpfDAO;
	@Autowired
	private Xcvrt xcvrt;
	@Autowired
	private Zrdecplc zrdecplc;

	@Override
	public void incrFunction(RexpupdDTO rexpupdDTO) throws IOException {
		this.rexpupdDTO = rexpupdDTO;
		if (StringUtils.isBlank(rexpupdDTO.getReassurer())) {
			wsaaClntnum = rexpupdDTO.getL1Clntnum();
			updateLifeCoOnlyForIncrease();
			if (StringUtils.isNotBlank(rexpupdDTO.getL2Clntnum())) {
				wsaaClntnum = rexpupdDTO.getL2Clntnum();
				updateLifeCoOnlyForIncrease();
			}
		} else {
			wsaaClntnum = rexpupdDTO.getL1Clntnum();
			updateAllForIncrease();
			if (StringUtils.isNotBlank(rexpupdDTO.getL2Clntnum())) {
				wsaaClntnum = rexpupdDTO.getL2Clntnum();
				updateAllForIncrease();
			}
		}
	}

	private void updateLifeCoOnlyForIncrease() throws IOException {
		searchLrrhpf();
		if (lrrhconIO == null) {
			writeLrrh();
		} else {
			if (lrrhconIO.getTranno() != rexpupdDTO.getTranno()) {
				lrrhconIO.setCurrto(rexpupdDTO.getEffdate());
				lrrhconIO.setValidflag("2");
				this.lrrhpfDAO.update(lrrhconIO);
			}
			if (!rexpupdDTO.getCurrency().equalsIgnoreCase(lrrhconIO.getCurrency())) {
				ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
				conlinkInput.setToCurrency(lrrhconIO.getCurrency());
				rexpupdDTO.setSumins(callXcvrt(conlinkInput).getCalculatedAmount());
			}
			lrrhconIO.setSsretn(lrrhconIO.getSsretn().add(rexpupdDTO.getSumins()));
			if (lrrhconIO.getTranno() == rexpupdDTO.getTranno()) {
				this.lrrhpfDAO.update(lrrhconIO);
			} else {
				writeValidflag1Lrrh();
			}
		}
	}

	private void updateAllForIncrease() throws IOException {
		searchLirrpf();
		if (lirrconIO == null) {
			writeLirr();
		} else {
			if (lirrconIO.getTranno() != rexpupdDTO.getTranno()) {
				lirrconIO.setCurrto(rexpupdDTO.getEffdate());
				lirrconIO.setValidflag("2");
				this.lirrpfDAO.update(lirrconIO);
			}
			if (!rexpupdDTO.getCurrency().equalsIgnoreCase(lirrconIO.getCurrency())) {
				ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
				conlinkInput.setToCurrency(lirrconIO.getCurrency());
				rexpupdDTO.setSumins(callXcvrt(conlinkInput).getCalculatedAmount());
			}
			lirrconIO.setRaAmount(lirrconIO.getRaAmount().add(rexpupdDTO.getSumins()));
			if (lirrconIO.getTranno() == rexpupdDTO.getTranno()) {
				this.lirrpfDAO.update(lirrconIO);
			} else {
				writeValidflag1Lirr();
			}
		}
	}

	private void writeLrrh() {
		lrrhconIO = new Lrrhpf();
		lrrhconIO.setClntpfx(CommonConstants.CLNTPFX_CN);
		lrrhconIO.setClntcoy(rexpupdDTO.getClntcoy());
		lrrhconIO.setClntnum(wsaaClntnum);
		lrrhconIO.setCompany(rexpupdDTO.getChdrcoy());
		lrrhconIO.setChdrnum(rexpupdDTO.getChdrnum());
		lrrhconIO.setLife(rexpupdDTO.getLife());
		lrrhconIO.setCoverage(rexpupdDTO.getCoverage());
		lrrhconIO.setRider(rexpupdDTO.getRider());
		lrrhconIO.setPlanSuffix(rexpupdDTO.getPlanSuffix());
		lrrhconIO.setCurrfrom(rexpupdDTO.getEffdate());
		lrrhconIO.setCurrto(CommonConstants.MAXDATE);
		lrrhconIO.setValidflag(CommonConstants.ONE);
		lrrhconIO.setLrkcls(rexpupdDTO.getRiskClass());
		lrrhconIO.setCurrency(rexpupdDTO.getCurrency());
		lrrhconIO.setSsretn(rexpupdDTO.getSumins());
		lrrhconIO.setSsreast(BigDecimal.ZERO);
		lrrhconIO.setSsreasf(BigDecimal.ZERO);
		lrrhconIO.setTranno(rexpupdDTO.getTranno());
		this.lrrhpfDAO.insert(lrrhconIO);
	}

	private void writeLirr() {
		lirrconIO = new Lirrpf();
		lirrconIO.setClntpfx(CommonConstants.CLNTPFX_CN);
		lirrconIO.setClntcoy(rexpupdDTO.getClntcoy());
		lirrconIO.setClntnum(wsaaClntnum);
		lirrconIO.setCompany(rexpupdDTO.getChdrcoy());
		lirrconIO.setChdrnum(rexpupdDTO.getChdrnum());
		lirrconIO.setLife(rexpupdDTO.getLife());
		lirrconIO.setCoverage(rexpupdDTO.getCoverage());
		lirrconIO.setRider(rexpupdDTO.getRider());
		lirrconIO.setPlanSuffix(rexpupdDTO.getPlanSuffix());
		lirrconIO.setCurrfrom(rexpupdDTO.getEffdate());
		lirrconIO.setCurrto(CommonConstants.MAXDATE);
		lirrconIO.setValidflag(CommonConstants.ONE);
		lirrconIO.setRasnum(rexpupdDTO.getReassurer());
		lirrconIO.setRngmnt(rexpupdDTO.getArrangement());
		lirrconIO.setCurrency(rexpupdDTO.getCurrency());
		lirrconIO.setRaAmount(rexpupdDTO.getSumins());
		lirrconIO.setTranno(rexpupdDTO.getTranno());
		this.lirrpfDAO.insert(lirrconIO);
	}

	@Override
	public void decrFunction(RexpupdDTO rexpupdDTO) throws IOException {
		this.rexpupdDTO = rexpupdDTO;
		if (StringUtils.isBlank(rexpupdDTO.getReassurer())) {
			wsaaClntnum = rexpupdDTO.getL1Clntnum();
			updateLifeCoOnly();
			if (StringUtils.isNotBlank(rexpupdDTO.getL2Clntnum())) {
				wsaaClntnum = rexpupdDTO.getL2Clntnum();
				updateLifeCoOnly();
			}
		} else {
			wsaaClntnum = rexpupdDTO.getL1Clntnum();
			updateAll();
			if (StringUtils.isNotBlank(rexpupdDTO.getL2Clntnum())) {
				wsaaClntnum = rexpupdDTO.getL2Clntnum();
				updateAll();
			}
		}
	}

	private void searchLrrhpf() {
		lrrhconIO.setCompany(rexpupdDTO.getChdrcoy());
		lrrhconIO.setChdrnum(rexpupdDTO.getChdrnum());
		lrrhconIO.setLife(rexpupdDTO.getLife());
		lrrhconIO.setCoverage(rexpupdDTO.getCoverage());
		lrrhconIO.setRider(rexpupdDTO.getRider());
		lrrhconIO.setPlanSuffix(rexpupdDTO.getPlanSuffix());
		lrrhconIO.setClntpfx(CommonConstants.CLNTPFX_CN);
		lrrhconIO.setClntcoy(rexpupdDTO.getClntcoy());
		lrrhconIO.setClntnum(wsaaClntnum);
		lrrhconIO.setLrkcls(rexpupdDTO.getRiskClass());
		lrrhconIO = lrrhpfDAO.search(lrrhconIO);
	}

	private void searchLirrpf() {
		lirrconIO.setCompany(rexpupdDTO.getChdrcoy());
		lirrconIO.setChdrnum(rexpupdDTO.getChdrnum());
		lirrconIO.setLife(rexpupdDTO.getLife());
		lirrconIO.setCoverage(rexpupdDTO.getCoverage());
		lirrconIO.setRider(rexpupdDTO.getRider());
		lirrconIO.setPlanSuffix(rexpupdDTO.getPlanSuffix());
		lirrconIO.setRasnum(rexpupdDTO.getReassurer());
		lirrconIO.setRngmnt(rexpupdDTO.getArrangement());
		lirrconIO.setClntpfx(CommonConstants.CLNTPFX_CN);
		lirrconIO.setClntcoy(rexpupdDTO.getClntcoy());
		lirrconIO.setClntnum(wsaaClntnum);
		lirrconIO = lirrpfDAO.search(lirrconIO);
	}

	/**
	 * @throws IOException
	 * 
	 */
	private void updateLifeCoOnly() throws IOException {
		searchLrrhpf();
		if (lrrhconIO == null) {
			return;
		}
		if (lrrhconIO.getTranno() != rexpupdDTO.getTranno()) {
			lrrhconIO.setCurrto(rexpupdDTO.getEffdate());
			lrrhconIO.setValidflag("2");
			this.lrrhpfDAO.update(lrrhconIO);
		}
		if (!rexpupdDTO.getCurrency().equalsIgnoreCase(lrrhconIO.getCurrency())) {
			ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
			conlinkInput.setToCurrency(lrrhconIO.getCurrency());
			rexpupdDTO.setSumins(callXcvrt(conlinkInput).getCalculatedAmount());
		}
		lrrhconIO.setSsretn(lrrhconIO.getSsretn().subtract(rexpupdDTO.getSumins()));
		if (lrrhconIO.getSsretn().intValue() < 0) {
			lrrhconIO.setSsretn(BigDecimal.ZERO);
		}
		if (lrrhconIO.getTranno() == rexpupdDTO.getTranno()) {
			this.lrrhpfDAO.update(lrrhconIO);
		} else {
			writeValidflag1Lrrh();
		}
	}

	private ConlinkOutputDTO callXcvrt(ConlinkInputDTO inputDTO) throws IOException {
		inputDTO.setAmount(rexpupdDTO.getSumins());
		inputDTO.setFromCurrency(rexpupdDTO.getCurrency());
		inputDTO.setCashdate(rexpupdDTO.getEffdate());
		inputDTO.setCompany(rexpupdDTO.getChdrcoy());
		ConlinkOutputDTO outputDTO = xcvrt.executeRealFunction(inputDTO);
		ZrdecplcDTO zrdecplcDTO = new ZrdecplcDTO();
		zrdecplcDTO.setCurrency(lrrhconIO.getCurrency());
		outputDTO.setCalculatedAmount(callRounding(zrdecplcDTO, outputDTO.getCalculatedAmount()));
		return outputDTO;
	}

	private BigDecimal callRounding(ZrdecplcDTO dto, BigDecimal calculatedAmount) throws IOException {
		dto.setCompany(rexpupdDTO.getChdrcoy());
		dto.setBatctrcde(rexpupdDTO.getBatctrcde());
		dto.setAmountIn(calculatedAmount);
		return zrdecplc.convertAmount(dto);
	}

	private void writeValidflag1Lrrh() {
		lrrhconIO.setCurrfrom(rexpupdDTO.getEffdate());
		lrrhconIO.setCurrto(CommonConstants.MAXDATE);
		lrrhconIO.setValidflag(CommonConstants.ONE);
		lrrhconIO.setTranno(rexpupdDTO.getTranno());
		this.lrrhpfDAO.insert(lrrhconIO);
	}

	/**
	 * @throws IOException
	 * 
	 */
	private void updateAll() throws IOException {
		searchLirrpf();
		if (lirrconIO == null) {
			this.next();
		} else {
			if (lirrconIO.getTranno() != rexpupdDTO.getTranno()) {
				lirrconIO.setCurrto(rexpupdDTO.getEffdate());
				lirrconIO.setValidflag("2");
				this.lirrpfDAO.update(lirrconIO);
			}
			if (!rexpupdDTO.getCurrency().equalsIgnoreCase(lirrconIO.getCurrency())) {
				ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
				conlinkInput.setToCurrency(lirrconIO.getCurrency());
				rexpupdDTO.setSumins(callXcvrt(conlinkInput).getCalculatedAmount());
			}
			lirrconIO.setRaAmount(lirrconIO.getRaAmount().subtract(rexpupdDTO.getSumins()));
			if (lirrconIO.getRaAmount().intValue() < 0) {
				lirrconIO.setRaAmount(BigDecimal.ZERO);
			}
			if (lirrconIO.getTranno() == rexpupdDTO.getTranno()) {
				this.lirrpfDAO.update(lirrconIO);
			} else {
				writeValidflag1Lirr();
			}
		}
	}

	private void writeValidflag1Lirr() {
		lirrconIO.setCurrfrom(rexpupdDTO.getEffdate());
		lirrconIO.setCurrto(CommonConstants.MAXDATE);
		lirrconIO.setValidflag(CommonConstants.ONE);
		lirrconIO.setTranno(rexpupdDTO.getTranno());
		this.lirrpfDAO.insert(lirrconIO);

	}

	protected void next() throws IOException {
		searchLrrhpf();
		if (lrrhconIO == null) {
			return;
		}
		if (lrrhconIO.getTranno() != rexpupdDTO.getTranno()) {
			lrrhconIO.setCurrto(rexpupdDTO.getEffdate());
			lrrhconIO.setValidflag("2");
			this.lrrhpfDAO.update(lrrhconIO);
		}
		if (!rexpupdDTO.getCurrency().equalsIgnoreCase(lrrhconIO.getCurrency())) {
			ConlinkInputDTO conlinkInput = new ConlinkInputDTO();
			conlinkInput.setToCurrency(lrrhconIO.getCurrency());
			rexpupdDTO.setSumins(callXcvrt(conlinkInput).getCalculatedAmount());
		}
		if (CommonConstants.ONE.equals(rexpupdDTO.getCestyp().trim())) {
			lrrhconIO.setSsreast(lrrhconIO.getSsreast().subtract(rexpupdDTO.getSumins()));
			if (lrrhconIO.getSsreast().intValue() < 0) {
				lrrhconIO.setSsreast(BigDecimal.ZERO);
			}
		} else {
			lrrhconIO.setSsreasf(lrrhconIO.getSsreasf().subtract(rexpupdDTO.getSumins()));
			if (lrrhconIO.getSsreasf().intValue() < 0) {
				lrrhconIO.setSsreasf(BigDecimal.ZERO);
			}
		}
		if (lrrhconIO.getTranno() == rexpupdDTO.getTranno()) {
			this.lrrhpfDAO.update(lrrhconIO);
		} else {
			writeValidflag1Lrrh();
		}
	}

}
