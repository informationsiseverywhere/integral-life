package com.dxc.integral.life.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.RcvdpfDAO;
import com.dxc.integral.life.dao.model.Rcvdpf;

@Repository("rcvdpfDAO")
@Lazy
public class RcvdpfDAOImpl extends BaseDAOImpl implements RcvdpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RcvdpfDAOImpl.class);

	@Override
	public Rcvdpf readRcvdpf(Rcvdpf rcvdpf) {
		StringBuilder sqlRcvdpfSelect = new StringBuilder("SELECT *  FROM RCVDPF ");
		sqlRcvdpfSelect.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlRcvdpfSelect.append(" AND LIFE = ? AND COVERAGE= ? ");
		sqlRcvdpfSelect.append(" AND RIDER= ? AND CRTABLE= ? ");

		List<Rcvdpf> rcvdpfList = jdbcTemplate.query(
				sqlRcvdpfSelect.toString(), new Object[] { rcvdpf.getChdrcoy(), rcvdpf.getChdrnum(), rcvdpf.getLife(),
						rcvdpf.getCoverage(), rcvdpf.getRider(), rcvdpf.getCrtable() },
				new BeanPropertyRowMapper<Rcvdpf>(Rcvdpf.class));

		if (rcvdpfList != null && !rcvdpfList.isEmpty()) {
			return rcvdpfList.get(0);
		}
		return null;
	}
}
