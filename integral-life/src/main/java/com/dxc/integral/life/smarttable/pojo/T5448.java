package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;
/**
 * @author wli31
 */
public class T5448 {
  	private String lrkclsdsc;
  	private String proppr;
  	private String rcedsar;
  	private String rclmbas;
  	private String rcstfrq;
  	private String rcstsar;
  	private BigDecimal rprior;
  	private String rresmeth;
  	private String rrevfrq;
  	private List<String> rrngmnts;
  	private String rrsktyp;
  	private String rtaxbas;
  	private String filler1;
	public String getLrkclsdsc() {
		return lrkclsdsc;
	}
	public void setLrkclsdsc(String lrkclsdsc) {
		this.lrkclsdsc = lrkclsdsc;
	}
	public String getProppr() {
		return proppr;
	}
	public void setProppr(String proppr) {
		this.proppr = proppr;
	}
	public String getRcedsar() {
		return rcedsar;
	}
	public void setRcedsar(String rcedsar) {
		this.rcedsar = rcedsar;
	}
	public String getRclmbas() {
		return rclmbas;
	}
	public void setRclmbas(String rclmbas) {
		this.rclmbas = rclmbas;
	}
	public String getRcstfrq() {
		return rcstfrq;
	}
	public void setRcstfrq(String rcstfrq) {
		this.rcstfrq = rcstfrq;
	}
	public String getRcstsar() {
		return rcstsar;
	}
	public void setRcstsar(String rcstsar) {
		this.rcstsar = rcstsar;
	}
	public BigDecimal getRprior() {
		return rprior;
	}
	public void setRprior(BigDecimal rprior) {
		this.rprior = rprior;
	}
	public String getRresmeth() {
		return rresmeth;
	}
	public void setRresmeth(String rresmeth) {
		this.rresmeth = rresmeth;
	}
	public String getRrevfrq() {
		return rrevfrq;
	}
	public void setRrevfrq(String rrevfrq) {
		this.rrevfrq = rrevfrq;
	}
	public List<String> getRrngmnts() {
		return rrngmnts;
	}
	public void setRrngmnts(List<String> rrngmnts) {
		this.rrngmnts = rrngmnts;
	}
	public String getRrsktyp() {
		return rrsktyp;
	}
	public void setRrsktyp(String rrsktyp) {
		this.rrsktyp = rrsktyp;
	}
	public String getRtaxbas() {
		return rtaxbas;
	}
	public void setRtaxbas(String rtaxbas) {
		this.rtaxbas = rtaxbas;
	}
	public String getFiller1() {
		return filler1;
	}
	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}
  	

	
}
