package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Mbnspf;

public interface MbnspfDAO {
	public List<Mbnspf> getMbnspfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int yrsinf);
}