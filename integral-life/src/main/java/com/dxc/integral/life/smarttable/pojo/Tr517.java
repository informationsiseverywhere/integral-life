package com.dxc.integral.life.smarttable.pojo;

import java.util.List;

public class Tr517 {
  
  	private List<String> ctables;
  	private List<String> zrwvflgs;
  	private String contitem;
	public List<String> getCtables() {
		return ctables;
	}
	public void setCtables(List<String> ctables) {
		this.ctables = ctables;
	}
	public List<String> getZrwvflgs() {
		return zrwvflgs;
	}
	public void setZrwvflgs(List<String> zrwvflgs) {
		this.zrwvflgs = zrwvflgs;
	}
	public String getContitem() {
		return contitem;
	}
	public void setContitem(String contitem) {
		this.contitem = contitem;
	}
}