package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Ddsupf {

	private long uniqueNumber;
	private String payrcoy;
	private String payrnum;
	private String mandref;
	private String bankkey;
	private String bankacckey;
	private Integer billcd;
	private BigDecimal debitamt;
	private String ddtrancode;
	private String currcode;
	private String bankaccdsc;
	private Integer jobno;
	private String termid;
	private Integer userT;
	private Integer trdt;
	private Integer trtm;
	private String dhnflag;
	private String company;
	private Integer subdat;
	private String mandstat;
	private Integer lapday;
	private String origmanst;
	private String usrprf;
	private String jobnm;
	private Date datime;

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public String getPayrcoy() {
		return this.payrcoy;
	}

	public String getPayrnum() {
		return this.payrnum;
	}

	public String getMandref() {
		return this.mandref;
	}

	public String getBankkey() {
		return this.bankkey;
	}

	public String getBankacckey() {
		return this.bankacckey;
	}

	public Integer getBillcd() {
		return this.billcd;
	}

	public BigDecimal getDebitamt() {
		return this.debitamt;
	}

	public String getDdtrancode() {
		return this.ddtrancode;
	}

	public String getCurrcode() {
		return this.currcode;
	}

	public String getBankaccdsc() {
		return this.bankaccdsc;
	}

	public Integer getJobno() {
		return this.jobno;
	}

	public String getTermid() {
		return this.termid;
	}

	public Integer getUserT() {
		return this.userT;
	}

	public Integer getTrdt() {
		return this.trdt;
	}

	public Integer getTrtm() {
		return this.trtm;
	}

	public String getDhnflag() {
		return this.dhnflag;
	}

	public String getCompany() {
		return this.company;
	}

	public Integer getSubdat() {
		return this.subdat;
	}

	public String getMandstat() {
		return this.mandstat;
	}

	public Integer getLapday() {
		return this.lapday;
	}

	public String getOrigmanst() {
		return this.origmanst;
	}

	public String getUsrprf() {
		return this.usrprf;
	}

	public String getJobnm() {
		return this.jobnm;
	}

	public Date getDatime() {
		return this.datime;
	}

	// Set Methods
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public void setPayrcoy(String payrcoy) {
		this.payrcoy = payrcoy;
	}

	public void setPayrnum(String payrnum) {
		this.payrnum = payrnum;
	}

	public void setMandref(String mandref) {
		this.mandref = mandref;
	}

	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}

	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}

	public void setBillcd(Integer billcd) {
		this.billcd = billcd;
	}

	public void setDebitamt(BigDecimal debitamt) {
		this.debitamt = debitamt;
	}

	public void setDdtrancode(String ddtrancode) {
		this.ddtrancode = ddtrancode;
	}

	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}

	public void setBankaccdsc(String bankaccdsc) {
		this.bankaccdsc = bankaccdsc;
	}

	public void setJobno(Integer jobno) {
		this.jobno = jobno;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public void setUserT(Integer userT) {
		this.userT = userT;
	}

	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}

	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	public void setDhnflag(String dhnflag) {
		this.dhnflag = dhnflag;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setSubdat(Integer subdat) {
		this.subdat = subdat;
	}

	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}

	public void setLapday(Integer lapday) {
		this.lapday = lapday;
	}

	public void setOrigmanst(String origmanst) {
		this.origmanst = origmanst;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}
}
