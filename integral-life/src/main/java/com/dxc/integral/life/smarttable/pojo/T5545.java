package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;
import java.util.List;

public class T5545 {

	public List<String> billfreqs;
	public List<BigDecimal> enhanaPcs;
	public List<BigDecimal> enhanaPrms;
	public List<BigDecimal> enhanbPcs;
	public List<BigDecimal> enhanbPrms;
	public List<BigDecimal> enhancPcs;
	public List<BigDecimal> enhancPrms;
	public List<BigDecimal> enhandPcs;
	public List<BigDecimal> enhandPrms;
	public List<BigDecimal> enhanePcs;
	public List<BigDecimal> enhanePrms;
	public List<BigDecimal> enhanfPcs;
	public List<BigDecimal> enhanfPrms;
	public List<String> getBillfreqs() {
		return billfreqs;
	}
	public void setBillfreqs(List<String> billfreqs) {
		this.billfreqs = billfreqs;
	}
	public List<BigDecimal> getEnhanaPcs() {
		return enhanaPcs;
	}
	public void setEnhanaPcs(List<BigDecimal> enhanaPcs) {
		this.enhanaPcs = enhanaPcs;
	}
	public List<BigDecimal> getEnhanaPrms() {
		return enhanaPrms;
	}
	public void setEnhanaPrms(List<BigDecimal> enhanaPrms) {
		this.enhanaPrms = enhanaPrms;
	}
	public List<BigDecimal> getEnhanbPcs() {
		return enhanbPcs;
	}
	public void setEnhanbPcs(List<BigDecimal> enhanbPcs) {
		this.enhanbPcs = enhanbPcs;
	}
	public List<BigDecimal> getEnhanbPrms() {
		return enhanbPrms;
	}
	public void setEnhanbPrms(List<BigDecimal> enhanbPrms) {
		this.enhanbPrms = enhanbPrms;
	}
	public List<BigDecimal> getEnhancPcs() {
		return enhancPcs;
	}
	public void setEnhancPcs(List<BigDecimal> enhancPcs) {
		this.enhancPcs = enhancPcs;
	}
	public List<BigDecimal> getEnhancPrms() {
		return enhancPrms;
	}
	public void setEnhancPrms(List<BigDecimal> enhancPrms) {
		this.enhancPrms = enhancPrms;
	}
	public List<BigDecimal> getEnhandPcs() {
		return enhandPcs;
	}
	public void setEnhandPcs(List<BigDecimal> enhandPcs) {
		this.enhandPcs = enhandPcs;
	}
	public List<BigDecimal> getEnhandPrms() {
		return enhandPrms;
	}
	public void setEnhandPrms(List<BigDecimal> enhandPrms) {
		this.enhandPrms = enhandPrms;
	}
	public List<BigDecimal> getEnhanePcs() {
		return enhanePcs;
	}
	public void setEnhanePcs(List<BigDecimal> enhanePcs) {
		this.enhanePcs = enhanePcs;
	}
	public List<BigDecimal> getEnhanePrms() {
		return enhanePrms;
	}
	public void setEnhanePrms(List<BigDecimal> enhanePrms) {
		this.enhanePrms = enhanePrms;
	}
	public List<BigDecimal> getEnhanfPcs() {
		return enhanfPcs;
	}
	public void setEnhanfPcs(List<BigDecimal> enhanfPcs) {
		this.enhanfPcs = enhanfPcs;
	}
	public List<BigDecimal> getEnhanfPrms() {
		return enhanfPrms;
	}
	public void setEnhanfPrms(List<BigDecimal> enhanfPrms) {
		this.enhanfPrms = enhanfPrms;
	}
	
	
}
