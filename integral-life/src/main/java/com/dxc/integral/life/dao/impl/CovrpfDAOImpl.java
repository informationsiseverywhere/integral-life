package com.dxc.integral.life.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.CovrpfDAO;
import com.dxc.integral.life.dao.model.Covrpf;

/**
 * 
 * CovrpfDAO implementation for database table <b>COVRPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@Repository("covrpfDAO")
public class CovrpfDAOImpl extends BaseDAOImpl implements CovrpfDAO {

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.CovrpfDAO#readCoverageRecordsByCompanyAndContractNumber(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Covrpf> readCoverageRecordsByCompanyAndContractNumber(String company, String contractNumber) {
		StringBuilder sql = new StringBuilder("select * from COVRPF where ");
		sql.append("chdrcoy=? and chdrnum=? and validflag=? ");
		sql.append(
				"order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc, plnsfx asc, unique_number desc");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber, "1" },
				new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
	}
	public Covrpf readCovrenqData(Covrpf covrpf) {
		StringBuilder sql = new StringBuilder("select * from covrenq ");
		sql.append("where chdrcoy = ? and chdrnum = ? and  life = ? and plnsfx = ? and coverage = ? and rider= ? ");

		return jdbcTemplate.queryForObject(sql.toString(), new Object[] { covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),covrpf.getPlnsfx() == null ? covrpf.getPlanSuffix() : covrpf.getPlnsfx(),
			covrpf.getCoverage(),covrpf.getRider()},new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
	}
	
	public List<Covrpf> searchValidCovrpfRecords(String chdrcoy,String chdrnum, String life, String coverage, String rider) {
		StringBuilder sql = new StringBuilder("select * from covrpf where ");
		sql.append("chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and validflag ='1' ");

		return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy,chdrnum,life,coverage,rider },new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
	}
	public List<Covrpf> searchCovrstaRecords(String chdrcoy,String chdrnum, String life, String coverage, String rider,int currfrom,int plnsfx,int tranno) {
		StringBuilder sql = new StringBuilder("select * from covrsta where ");
		sql.append("chdrcoy = ? and chdrnum = ? and life = ? and coverage = ? and rider = ? and plnsfx=? and validflag != '1' and currfrom =? and ((chdrcoy=? and tranno<=? ))  ");
		return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy,chdrnum,life,coverage,rider, plnsfx,currfrom,chdrnum,tranno},
				new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
	}
	@Override
	public int updateCovrpfCDebt(BigDecimal crdebt, long uniqueNumber) {
		String sql = "UPDATE COVRPF SET CRDEBT=? WHERE UNIQUE_NUMBER=? ";
		return jdbcTemplate.update(sql, new Object[] {crdebt, uniqueNumber});//IJTI-1415
	}
	@Override
	public int insertCovrpf(Covrpf covrpf) {
		StringBuilder sql = new StringBuilder("INSERT INTO COVRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,");
		sql.append("PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,");
		sql.append("TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,");
		sql.append("LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,");
		sql.append("PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,");
		sql.append("CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,USRPRF,JOBNM,DATIME) ");
		sql.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?) ");
		return jdbcTemplate.update(sql.toString(), new Object[] {
		   covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getJlife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),covrpf.getValidflag(),
		   covrpf.getTranno(),covrpf.getCurrfrom(),covrpf.getCurrto(),covrpf.getStatcode(),covrpf.getPstatcode(),covrpf.getStatreasn(),covrpf.getCrrcd(),covrpf.getAnbAtCcd(),
		   covrpf.getSex(),covrpf.getReptcd01(),covrpf.getReptcd02(),covrpf.getReptcd03(),covrpf.getReptcd04(),covrpf.getReptcd05(),covrpf.getReptcd06(),
		   covrpf.getCrInstamt01(),covrpf.getCrInstamt02(),covrpf.getCrInstamt03(),covrpf.getCrInstamt04(),covrpf.getCrInstamt05(),covrpf.getPremCurrency(),
		   covrpf.getTermid(),covrpf.getTransactionDate(),covrpf.getTransactionTime(),covrpf.getUser(),covrpf.getStatFund(),covrpf.getStatSect(),covrpf.getStatSubsect(),
		   covrpf.getCrtable(),covrpf.getRcesDte(),covrpf.getPcesDte(),covrpf.getBcesDte(),covrpf.getNxDte(),covrpf.getRiskCessAge(),
		   covrpf.getPremCessAge(),covrpf.getBenCessAge(),covrpf.getRcesTrm(),covrpf.getPcesTrm(),covrpf.getBcesTrm(),covrpf.getSumins(),covrpf.getSicurr(),
		   covrpf.getVarSumInsured(),covrpf.getMortcls(),covrpf.getLiencd(),covrpf.getRatingClass(),covrpf.getIndexationInd(),covrpf.getBonusInd(),covrpf.getDeferPerdCode(),
		   covrpf.getDeferPerdAmt(),covrpf.getDeferPerdInd(),covrpf.getTotMthlyBenefit(),covrpf.getEstMatValue01(),covrpf.getEstMatValue02(),covrpf.getEstMatDate01(),
		   covrpf.getEstMatDate02(),covrpf.getEstMatInt01(),covrpf.getEstMatInt02(),covrpf.getCampaign(),covrpf.getStatSumins(),covrpf.getRtrnyrs(),
		   covrpf.getReserveUnitsInd(),covrpf.getReserveUnitsDate(),covrpf.getChargeOptionsInd(),covrpf.getFundSplitPlan(),covrpf.getPremCessAgeMth(),
		   covrpf.getPremCessAgeDay(),covrpf.getPremCessTermMth(),covrpf.getPremCessTermDay(),covrpf.getRiskCessAgeMth(),covrpf.getRiskCessAgeDay(),
		   covrpf.getRiskCessTermMth(),covrpf.getRiskCessTermDay(),covrpf.getJlLsInd(),covrpf.getInstprem(),covrpf.getSingp(),covrpf.getRerateDate(),
		   covrpf.getRerateFromDate(),covrpf.getBenBillDate(),covrpf.getAnnivProcDate(),covrpf.getConvertInitialUnits(),covrpf.getReviewProcessing(),
		   covrpf.getUnitStatementDate(),covrpf.getCpiDate(),covrpf.getInitUnitCancDate(),covrpf.getExtraAllocDate(), covrpf.getInitUnitIncrsDate(),covrpf.getCoverageDebt(),
		   covrpf.getPayrseqno(),covrpf.getBappmeth(),covrpf.getZbinstprem(),covrpf.getZlinstprem(),covrpf.getUserProfile(), covrpf.getJobName(),new Timestamp(System.currentTimeMillis()) 
		});
	}
	public void updateCovrpfRecords(List<Covrpf> covrList){
		List<Covrpf> tempCovrpfList = covrList;   
	
		StringBuilder sb = new StringBuilder("update covrpf set currto=?,varsi=?,");
        sb.append("validflag=?,usrprf=?,jobnm=?,datime=? where unique_number=?");
     
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempCovrpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setInt(1, tempCovrpfList.get(i).getCurrto());
				ps.setBigDecimal(2, tempCovrpfList.get(i).getVarsi());
                ps.setString(3, tempCovrpfList.get(i).getValidflag());
                ps.setString(4, tempCovrpfList.get(i).getUsrprf());
                ps.setString(5, tempCovrpfList.get(i).getJobnm());
                ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(7, tempCovrpfList.get(i).getUniqueNumber());
            }  
      }); 
	}
	@Override
	public List<Covrpf> fetchRecords(String company, String cpiDte ,String indxin , String chdrnumTo, String chdrnumFrom, String validFlag) {
		
		StringBuilder sql = new StringBuilder("select COVR.* from COVRPF COVR where "
				+ " COVR.CHDRCOY = ? AND COVR.CPIDTE <> 0 AND COVR.CPIDTE <= ? AND COVR.INDXIN <> ? AND COVR.CHDRNUM BETWEEN ? AND ? AND "
				+ " COVR.VALIDFLAG =? ");
		
		return jdbcTemplate.query(sql.toString(), new Object[] {company,cpiDte,indxin,chdrnumFrom,chdrnumTo,validFlag},
				new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
	
	}
@Override
	public int deleteCovrpfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
		
		String sql = "DELETE FROM COVRPF WHERE CHDRCOY = ? AND  CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, life, coverage, rider, plnsfx});
		
	}
	@Override
   public int updateFlagAndCurtoForCovrpf(Covrpf covrpf) {
		
		String sql = "UPDATE COVRPF SET VALIDFLAG = ? AND CURRTO=?  WHERE CHDRCOY = ? AND  CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ?";
		return jdbcTemplate.update(sql, new Object[] {covrpf.getValidflag(), covrpf.getCurrto(), covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), 
				covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlnsfx()});
		
	}
	public void insertCovrpfRecords(List<Covrpf> covrpfList) {
		final List<Covrpf> tempCovrpfList = covrpfList;   
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO COVRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,STATCODE,PSTATCODE,STATREASN,CRRCD,ANBCCD,SEX,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,CRINST01,CRINST02,CRINST03,CRINST04,CRINST05,PRMCUR,TERMID,TRDT,TRTM,USER_T,STFUND,STSECT,STSSECT,CRTABLE,RCESDTE,PCESDTE,BCESDTE,NXTDTE,RCESAGE,PCESAGE,BCESAGE,RCESTRM,PCESTRM,BCESTRM,SUMINS,SICURR,VARSI,MORTCLS,LIENCD,RATCLS,INDXIN,BNUSIN,DPCD,DPAMT,DPIND,TMBEN,EMV01,EMV02,EMVDTE01,EMVDTE02,EMVINT01,EMVINT02,CAMPAIGN,STSMIN,RTRNYRS,RSUNIN,RUNDTE,CHGOPT,FUNDSP,PCAMTH,PCADAY,PCTMTH,PCTDAY,RCAMTH,RCADAY,RCTMTH,RCTDAY,JLLSID,INSTPREM,SINGP,RRTDAT,RRTFRM,BBLDAT,CBANPR,CBCVIN,CBRVPR,CBUNST,CPIDTE,ICANDT,EXALDT,IINCDT,CRDEBT,PAYRSEQNO,BAPPMETH,ZBINSTPREM,ZLINSTPREM,ZSTPDUTY01,ZCLSTATE,USRPRF,JOBNM,DATIME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?) ");
	       jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {  
            @Override
            public int getBatchSize() {  
                 return tempCovrpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i)  
                    throws SQLException { 
            	ps.setString(1,covrpfList.get(i).getChdrcoy());
                ps.setString(2,covrpfList.get(i).getChdrnum());
                ps.setString(3,covrpfList.get(i).getLife());
                ps.setString(4,covrpfList.get(i).getJlife());
                ps.setString(5,covrpfList.get(i).getCoverage());
                ps.setString(6,covrpfList.get(i).getRider());
                ps.setInt(7,covrpfList.get(i).getPlanSuffix());
                ps.setString(8,covrpfList.get(i).getValidflag());
                ps.setInt(9,covrpfList.get(i).getTranno());
                ps.setInt(10,covrpfList.get(i).getCurrfrom());
                ps.setInt(11,covrpfList.get(i).getCurrto());
                ps.setString(12,covrpfList.get(i).getStatcode());
                ps.setString(13,covrpfList.get(i).getPstatcode());
                ps.setString(14,covrpfList.get(i).getStatreasn());
                ps.setInt(15,covrpfList.get(i).getCrrcd());
                ps.setInt(16,covrpfList.get(i).getAnbccd());
                ps.setString(17,covrpfList.get(i).getSex());
                ps.setString(18,covrpfList.get(i).getReptcd01());
                ps.setString(19,covrpfList.get(i).getReptcd02());
                ps.setString(20,covrpfList.get(i).getReptcd03());
                ps.setString(21,covrpfList.get(i).getReptcd04());
                ps.setString(22,covrpfList.get(i).getReptcd05());
                ps.setString(23,covrpfList.get(i).getReptcd06());
                ps.setBigDecimal(24,covrpfList.get(i).getCrinst01());
                ps.setBigDecimal(25,covrpfList.get(i).getCrinst02());
                ps.setBigDecimal(26,covrpfList.get(i).getCrinst03());
                ps.setBigDecimal(27,covrpfList.get(i).getCrinst04());
                ps.setBigDecimal(28,covrpfList.get(i).getCrinst05());
                ps.setString(29,covrpfList.get(i).getPrmcur());
                ps.setString(30,covrpfList.get(i).getTermid());
                ps.setInt(31,covrpfList.get(i).getTrdt());
                ps.setInt(32,covrpfList.get(i).getTrtm());
                ps.setInt(33,covrpfList.get(i).getUserT());
                ps.setString(34,covrpfList.get(i).getStfund());
                ps.setString(35,covrpfList.get(i).getStsect());
                ps.setString(36,covrpfList.get(i).getStssect());
                ps.setString(37,covrpfList.get(i).getCrtable());
                ps.setInt(38,covrpfList.get(i).getRcesdte());
                ps.setInt(39,covrpfList.get(i).getPcesdte());
                ps.setInt(40,covrpfList.get(i).getBcesdte());
                ps.setInt(41,covrpfList.get(i).getNxtdte());
                ps.setInt(42,covrpfList.get(i).getRcesage());
                ps.setInt(43,covrpfList.get(i).getPcesage());
                ps.setInt(44,covrpfList.get(i).getBcesage());
                ps.setInt(45,covrpfList.get(i).getRcestrm());
                ps.setInt(46,covrpfList.get(i).getPcestrm());
                ps.setInt(47,covrpfList.get(i).getBcestrm());
                ps.setBigDecimal(48,covrpfList.get(i).getSumins());
                ps.setString(49,covrpfList.get(i).getSicurr());
                ps.setBigDecimal(50,covrpfList.get(i).getVarSumInsured());
                ps.setString(51,covrpfList.get(i).getMortcls());
                ps.setString(52,covrpfList.get(i).getLiencd());
                ps.setString(53,covrpfList.get(i).getRatcls());
                ps.setString(54,covrpfList.get(i).getIndxin());
                ps.setString(55,covrpfList.get(i).getBnusin());
                ps.setString(56,covrpfList.get(i).getDpcd());
                ps.setBigDecimal(57,covrpfList.get(i).getDpamt());
                ps.setString(58,covrpfList.get(i).getDpind());
                ps.setBigDecimal(59,covrpfList.get(i).getTmben());
                ps.setBigDecimal(60,covrpfList.get(i).getEmv01());
                ps.setBigDecimal(61,covrpfList.get(i).getEmv02());
                ps.setInt(62,covrpfList.get(i).getEmvdte01());
                ps.setInt(63,covrpfList.get(i).getEmvdte02());
                ps.setBigDecimal(64,covrpfList.get(i).getEmvint01());
                ps.setBigDecimal(65,covrpfList.get(i).getEmvint02());
                ps.setString(66,covrpfList.get(i).getCampaign());
                ps.setBigDecimal(67,covrpfList.get(i).getStsmin());
                ps.setInt(68,covrpfList.get(i).getRtrnyrs());
                ps.setString(69,covrpfList.get(i).getRsunin());
                ps.setInt(70,covrpfList.get(i).getRundte());
                ps.setString(71,covrpfList.get(i).getChgopt());
                ps.setString(72,covrpfList.get(i).getFundsp());
                ps.setInt(73,covrpfList.get(i).getPcamth());
                ps.setInt(74,covrpfList.get(i).getPcaday());
                ps.setInt(75,covrpfList.get(i).getPctmth());
                ps.setInt(76,covrpfList.get(i).getPctday());
                ps.setInt(77,covrpfList.get(i).getRcamth());
                ps.setInt(78,covrpfList.get(i).getRcaday());
                ps.setInt(79,covrpfList.get(i).getRctmth());
                ps.setInt(80,covrpfList.get(i).getRctmth());
                ps.setString(81,covrpfList.get(i).getJllsid());
                ps.setBigDecimal(82,covrpfList.get(i).getInstprem());
                ps.setBigDecimal(83,covrpfList.get(i).getSingp());
                ps.setInt(84,covrpfList.get(i).getRrtdat());
                ps.setInt(85,covrpfList.get(i).getRrtfrm());
                ps.setInt(86,covrpfList.get(i).getBbldat());
                ps.setInt(87,covrpfList.get(i).getCbanpr() == null ? 0 : covrpfList.get(i).getCbanpr());
                ps.setInt(88,covrpfList.get(i).getCbcvin() == null ? 0 : covrpfList.get(i).getCbcvin());
                ps.setInt(89,covrpfList.get(i).getCbrvpr() == null ? 0 : covrpfList.get(i).getCbrvpr());
                ps.setInt(90,covrpfList.get(i).getCbunst());
                ps.setInt(91,covrpfList.get(i).getCpidte());
                ps.setInt(92,covrpfList.get(i).getIcandt() == null ? 0 : covrpfList.get(i).getIcandt());
                ps.setInt(93,covrpfList.get(i).getExaldt() == null ? 0 : covrpfList.get(i).getExaldt());			
                ps.setInt(94,covrpfList.get(i).getIincdt() == null ? 0 : covrpfList.get(i).getIincdt());
                ps.setBigDecimal(95,covrpfList.get(i).getCrdebt());
                ps.setLong(96,covrpfList.get(i).getPayrseqno());
                ps.setString(97,covrpfList.get(i).getBappmeth());
                ps.setBigDecimal(98,covrpfList.get(i).getZbinstprem());
                ps.setBigDecimal(99,covrpfList.get(i).getZlinstprem());
                ps.setBigDecimal(100,covrpfList.get(i).getZstpduty01());
                ps.setString(101,covrpfList.get(i).getZclstate());
                ps.setString(102,covrpfList.get(i).getUsrprf());
				ps.setString(103, covrpfList.get(i).getJobnm());
				ps.setTimestamp(104, new Timestamp(System.currentTimeMillis())); 
            }   
      });

}
	
	public Map<String, List<Covrpf>> searchCovrpf(String chdrcoy, List<String> chdrnum){
		Map<String, List<Covrpf>> covrListMap = new HashMap<>();
		List<Covrpf> covrList;
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("chdrcoy", chdrcoy);
		parameters.addValue("chdrnum", chdrnum);
		StringBuilder sql = new StringBuilder("SELECT * FROM COVRPF WHERE CHDRCOY=:chdrcoy ");
				sql.append("AND CHDRNUM IN (:chdrnum) AND VALIDFLAG='1'");
		covrList=namedParameterJdbcTemplate.query(sql.toString(),parameters,new BeanPropertyRowMapper<Covrpf>(Covrpf.class));
		for(Covrpf covr: covrList) {
			if(!covrListMap.isEmpty() && covrListMap.containsKey(covr.getChdrnum()))
				covrListMap.get(covr.getChdrnum()).add(covr);
			else {
				List<Covrpf> covrpfList = new ArrayList<Covrpf>();
				covrpfList.add(covr);
				covrListMap.put(covr.getChdrnum(), covrpfList);
			}
		}
		return covrListMap;
	}
}
									