package com.dxc.integral.life.smarttable.pojo;

/**
 * T3700 pojo for T3700 smart table.
 * 
 * @author wli31
 *
 */
public class T3700 {

	private Integer agntsort;
  	private Integer chdrsort;
  	private Integer duedsort;
  	private Integer membsort;
  	private Integer pnamsort;
  	private Integer pnumsort;
	public Integer getAgntsort() {
		return agntsort;
	}
	public void setAgntsort(Integer agntsort) {
		this.agntsort = agntsort;
	}
	public Integer getChdrsort() {
		return chdrsort;
	}
	public void setChdrsort(Integer chdrsort) {
		this.chdrsort = chdrsort;
	}
	public Integer getDuedsort() {
		return duedsort;
	}
	public void setDuedsort(Integer duedsort) {
		this.duedsort = duedsort;
	}
	public Integer getMembsort() {
		return membsort;
	}
	public void setMembsort(Integer membsort) {
		this.membsort = membsort;
	}
	public Integer getPnamsort() {
		return pnamsort;
	}
	public void setPnamsort(Integer pnamsort) {
		this.pnamsort = pnamsort;
	}
	public Integer getPnumsort() {
		return pnumsort;
	}
	public void setPnumsort(Integer pnumsort) {
		this.pnumsort = pnumsort;
	}
	
	
}
