package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Grpspf;
/**
 * @author wli31
 */
public interface GrpspfDAO {
	Grpspf readGrpspf(String grupcoy,String grupnum);
}
