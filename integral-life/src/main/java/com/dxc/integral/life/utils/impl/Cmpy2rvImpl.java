package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.dao.ChdrpfDAO;
import com.dxc.integral.fsu.dao.model.Chdrpf;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.smarttable.ItempfService;
import com.dxc.integral.iaf.smarttable.pojo.TableItem;
import com.dxc.integral.iaf.utils.Datcon3;
import com.dxc.integral.life.beans.ComlinkDTO;
import com.dxc.integral.life.services.CalcRnwlCommissionService;
import com.dxc.integral.life.smarttable.pojo.T5694;
import com.dxc.integral.life.utils.Cmpy2rv;

/**
 * 
 * @author xma3
 *
 */
@Service("cmpy2rv")
@Lazy
public class Cmpy2rvImpl implements Cmpy2rv {
	
	@Autowired
	private ItempfService itempfService;
    @Autowired
    private CalcRnwlCommissionService calcRnwlCommissionService;
	@Autowired
	private ChdrpfDAO chdrpfDAO;
	@Autowired
	private Datcon3 datcon3;



	public ComlinkDTO process(ComlinkDTO comlinkDTO) throws Exception{
			
		comlinkDTO.setStatuz("****");
		comlinkDTO.setPayamnt(BigDecimal.ZERO);
		comlinkDTO.setErndamt(BigDecimal.ZERO);
		Chdrpf chdrpf = chdrpfDAO.getLPContractInfo(comlinkDTO.getChdrcoy(), comlinkDTO.getChdrnum());
		if(chdrpf == null) {
			return null;
		}
		T5694 t5694IO=readT5694(comlinkDTO, chdrpf.getSrcebus());
		if(t5694IO==null) {
			return null;
		}
		BigDecimal wsaaT5694 = obtainPaymntRules(comlinkDTO, t5694IO);
		
		if (wsaaT5694==BigDecimal.ZERO) {
			return comlinkDTO;
		}
		
		return calcRnwlCommissionService.calcRnwlCommission(comlinkDTO, wsaaT5694, chdrpf.getCnttype(), false);
		
	}
	
	protected T5694 readT5694(ComlinkDTO comlinkDTO, String srcebus) throws Exception{

		TableItem<T5694> t5694=itempfService.readSmartTableByTableNameAndItem(comlinkDTO.getChdrcoy(), "T5694", comlinkDTO.getMethod().concat(srcebus), CommonConstants.IT_ITEMPFX, T5694.class);
		T5694 t5694IO = new T5694();
		if(t5694 != null) {
			t5694IO = t5694.getItemDetail();
		}else {
			t5694 = itempfService.readSmartTableByTableNameAndItem(comlinkDTO.getChdrcoy(), "T5694", comlinkDTO.getMethod().concat("**"), CommonConstants.IT_ITEMPFX, T5694.class);
            if(t5694 != null) {
            	t5694IO = t5694.getItemDetail();
            }
		}
		return t5694IO;
	}
	
	protected BigDecimal obtainPaymntRules(ComlinkDTO comlinkDTO, T5694 t5694IO) throws Exception{
		long wsaaTerm;
		if("00".equals(comlinkDTO.getBillfreq())) {
			wsaaTerm=0;
			
		}else {
			wsaaTerm=datcon3.getYearsDifference(comlinkDTO.getEffdate(), comlinkDTO.getPtdate())+1;
		}
	  return bypassDatcon(t5694IO, wsaaTerm);
	}
	
	protected BigDecimal bypassDatcon(T5694 t5694IO, long wsaaTerm) {
		BigDecimal wsaaT5694=BigDecimal.ZERO;
		for (int i=0; i<=10; i++){
			if (wsaaTerm<=t5694IO.getToYears().get(i).intValue()) {
				wsaaT5694=t5694IO.getInstalpcs().get(i);
				i=13;
			}
		}
		return wsaaT5694;
	}
	



}
