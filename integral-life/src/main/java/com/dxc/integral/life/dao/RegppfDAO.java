package com.dxc.integral.life.dao;

import java.util.List;

import com.dxc.integral.life.dao.model.Regppf;
/* 
 * 
 * @author wli31
 *
 */
public interface RegppfDAO  {
    public Regppf readRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider,int rgpynum);
    public List<Regppf> readRegpByRgpytype(String chdrnum,String crtable);
}