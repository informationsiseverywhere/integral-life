package com.dxc.integral.life.smarttable.pojo;

import java.math.BigDecimal;

public class T6633 {

	/** The loanAnnivInterest */
	private String loanAnnivInterest;
	/** The policyAnnivInterest */
	private String policyAnnivInterest;
	/** The annloan */
	private String annloan;
	/** The annpoly */
	private String annpoly;
	/** The compfreq */
	private String compfreq;
	/** The day */
	private Integer day;
	/** The interestDay */
	private Integer interestDay;
	/** The interestFrequency */
	private String interestFrequency;
	/** The intRate */
	private BigDecimal intRate;
	/** The inttype */
	private String inttype;
	/** The mperiod */
	private Integer mperiod;
	/** The nofDay */
	private Integer nofDay;//ILIFE-8688

	/**
	 * @return the loanAnnivInterest
	 */
	public String getLoanAnnivInterest() {
		return loanAnnivInterest;
	}

	/**
	 * @param loanAnnivInterest
	 *            the loanAnnivInterest to set
	 */
	public void setLoanAnnivInterest(String loanAnnivInterest) {
		this.loanAnnivInterest = loanAnnivInterest;
	}

	/**
	 * @return the policyAnnivInterest
	 */
	public String getPolicyAnnivInterest() {
		return policyAnnivInterest;
	}

	/**
	 * @param policyAnnivInterest
	 *            the policyAnnivInterest to set
	 */
	public void setPolicyAnnivInterest(String policyAnnivInterest) {
		this.policyAnnivInterest = policyAnnivInterest;
	}

	/**
	 * @return the annloan
	 */
	public String getAnnloan() {
		return annloan;
	}

	/**
	 * @param annloan
	 *            the annloan to set
	 */
	public void setAnnloan(String annloan) {
		this.annloan = annloan;
	}

	/**
	 * @return the annpoly
	 */
	public String getAnnpoly() {
		return annpoly;
	}

	/**
	 * @param annpoly
	 *            the annpoly to set
	 */
	public void setAnnpoly(String annpoly) {
		this.annpoly = annpoly;
	}

	/**
	 * @return the compfreq
	 */
	public String getCompfreq() {
		return compfreq;
	}

	/**
	 * @param compfreq
	 *            the compfreq to set
	 */
	public void setCompfreq(String compfreq) {
		this.compfreq = compfreq;
	}

	/**
	 * @return the day
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * @param day
	 *            the day to set
	 */
	public void setDay(Integer day) {
		this.day = day;
	}

	/**
	 * @return the interestDay
	 */
	public Integer getInterestDay() {
		return interestDay;
	}

	/**
	 * @param interestDay
	 *            the interestDay to set
	 */
	public void setInterestDay(Integer interestDay) {
		this.interestDay = interestDay;
	}

	/**
	 * @return the interestFrequency
	 */
	public String getInterestFrequency() {
		return interestFrequency;
	}

	/**
	 * @param interestFrequency
	 *            the interestFrequency to set
	 */
	public void setInterestFrequency(String interestFrequency) {
		this.interestFrequency = interestFrequency;
	}

	/**
	 * @return the intRate
	 */
	public BigDecimal getIntRate() {
		return intRate;
	}

	/**
	 * @param intRate
	 *            the intRate to set
	 */
	public void setIntRate(BigDecimal intRate) {
		this.intRate = intRate;
	}

	/**
	 * @return the inttype
	 */
	public String getInttype() {
		return inttype;
	}

	/**
	 * @param inttype
	 *            the inttype to set
	 */
	public void setInttype(String inttype) {
		this.inttype = inttype;
	}

	/**
	 * @return the mperiod
	 */
	public Integer getMperiod() {
		return mperiod;
	}

	/**
	 * @param mperiod
	 *            the mperiod to set
	 */
	public void setMperiod(Integer mperiod) {
		this.mperiod = mperiod;
	}

	public Integer getNofDay() {
		return nofDay;
	}

	public void setNofDay(Integer nofDay) {
		this.nofDay = nofDay;
	}
	

}
