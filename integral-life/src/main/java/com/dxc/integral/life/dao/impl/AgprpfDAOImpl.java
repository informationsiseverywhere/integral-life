package com.dxc.integral.life.dao.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.AgprpfDAO;
import com.dxc.integral.life.dao.model.Agprpf;

/**
 * AgprpfDAO implementation for database table <b>AGPRPF</b> DB operations.
 * 
 * @author vhukumagrawa
 *
 */
@Repository("agprpfDAO")
@Lazy
public class AgprpfDAOImpl extends BaseDAOImpl implements AgprpfDAO {

	/** Constant to pass placeholder '?' to prepared statement. */
	private static final String CONSTANT_PLACEHOLDER_20 = "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,";

	/*
	 * (non-Javadoc)
	 * @see com.dxc.integral.life.dao.AgprpfDAO#writeAgprpf(com.dxc.integral.life.dao.model.Agprpf)
	 */
	@Override
	public int writeAgprpf(Agprpf agprpf) {
		StringBuilder sql = new StringBuilder("insert into AGPRPF (");
		sql.append("AGNTCOY, AGNTNUM, ACCTYR, MNTH, CHDRNUM, CNTTYPE, EFFDATE, ");
		sql.append(
				"MLPERPP01, MLPERPP02, MLPERPP03, MLPERPP04, MLPERPP05, MLPERPP06, MLPERPP07, MLPERPP08, MLPERPP09, MLPERPP10, ");
		sql.append(
				"MLPERPC01, MLPERPC02, MLPERPC03, MLPERPC04, MLPERPC05, MLPERPC06, MLPERPC07, MLPERPC08, MLPERPC09, MLPERPC10, ");
		sql.append(
				"MLDIRPP01, MLDIRPP02, MLDIRPP03, MLDIRPP04, MLDIRPP05, MLDIRPP06, MLDIRPP07, MLDIRPP08, MLDIRPP09, MLDIRPP10,");
		sql.append(
				"MLDIRPC01, MLDIRPC02, MLDIRPC03, MLDIRPC04, MLDIRPC05, MLDIRPC06, MLDIRPC07, MLDIRPC08, MLDIRPC09, MLDIRPC10,");
		sql.append(
				"MLGRPPP01, MLGRPPP02, MLGRPPP03, MLGRPPP04, MLGRPPP05, MLGRPPP06, MLGRPPP07, MLGRPPP08, MLGRPPP09, MLGRPPP10,");
		sql.append(
				"MLGRPPC01, MLGRPPC02, MLGRPPC03, MLGRPPC04, MLGRPPC05, MLGRPPC06, MLGRPPC07, MLGRPPC08, MLGRPPC09, MLGRPPC10,");
		sql.append("CNTCOUNT, SUMINS, USRPRF, JOBNM, DATIME) VALUES (");
		sql.append("?, ?, ?, ?, ?, ?, ?,");
		sql.append(CONSTANT_PLACEHOLDER_20);
		sql.append(CONSTANT_PLACEHOLDER_20);
		sql.append(CONSTANT_PLACEHOLDER_20);
		sql.append("?, ?, ?, ?, ?)");

		return jdbcTemplate.update(sql.toString(),
				new Object[] { agprpf.getAgntcoy(), agprpf.getAgntnum(), agprpf.getAcctyr(), agprpf.getMnth(),
						agprpf.getChdrnum(), agprpf.getCnttype(), agprpf.getEffdate(), 
						agprpf.getMlperpp().get(0), agprpf.getMlperpp().get(1), agprpf.getMlperpp().get(2), agprpf.getMlperpp().get(3), agprpf.getMlperpp().get(4),
						agprpf.getMlperpp().get(5), agprpf.getMlperpp().get(6), agprpf.getMlperpp().get(7), agprpf.getMlperpp().get(8), agprpf.getMlperpp().get(9),
						agprpf.getMlperpc().get(0), agprpf.getMlperpc().get(1), agprpf.getMlperpc().get(2), agprpf.getMlperpc().get(3), agprpf.getMlperpc().get(4),
						agprpf.getMlperpc().get(5), agprpf.getMlperpc().get(6), agprpf.getMlperpc().get(7), agprpf.getMlperpc().get(8), agprpf.getMlperpc().get(9),
						agprpf.getMldirpp().get(0), agprpf.getMldirpp().get(1), agprpf.getMldirpp().get(2), agprpf.getMldirpp().get(3), agprpf.getMldirpp().get(4),
						agprpf.getMldirpp().get(5), agprpf.getMldirpp().get(6), agprpf.getMldirpp().get(7), agprpf.getMldirpp().get(8), agprpf.getMldirpp().get(9),
						agprpf.getMldirpc().get(0), agprpf.getMldirpc().get(1), agprpf.getMldirpc().get(2), agprpf.getMldirpc().get(3), agprpf.getMldirpc().get(4),
						agprpf.getMldirpc().get(5), agprpf.getMldirpc().get(6), agprpf.getMldirpc().get(7), agprpf.getMldirpc().get(8), agprpf.getMldirpc().get(9),
						agprpf.getMlgrppp().get(0), agprpf.getMlgrppp().get(1), agprpf.getMlgrppp().get(2), agprpf.getMlgrppp().get(3), agprpf.getMlgrppp().get(4),
						agprpf.getMlgrppp().get(5), agprpf.getMlgrppp().get(6), agprpf.getMlgrppp().get(7), agprpf.getMlgrppp().get(8), agprpf.getMlgrppp().get(9),
						agprpf.getMlgrppc().get(0), agprpf.getMlgrppc().get(1), agprpf.getMlgrppc().get(2), agprpf.getMlgrppc().get(3), agprpf.getMlgrppc().get(4),
						agprpf.getMlgrppc().get(5), agprpf.getMlgrppc().get(6), agprpf.getMlgrppc().get(7), agprpf.getMlgrppc().get(8), agprpf.getMlgrppc().get(9),
						agprpf.getCntcount(), agprpf.getSumins(), agprpf.getUsrprf(), agprpf.getJobnm(), agprpf.getDatime() });
	}

}
