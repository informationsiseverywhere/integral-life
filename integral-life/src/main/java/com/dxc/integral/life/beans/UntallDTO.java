package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * 
 * @author xma3
 *
 */
public class UntallDTO {
	private String untaStatuz;
	private String untaFunction;
	private String untaCompany;
	private int untaEffdate;
	private String untaAalbas;
	private String untaBillfreq;
	private String untaSinglePremInd;

	private int untaMaxPeriod01;
	private int untaMaxPeriod02;
	private int untaMaxPeriod03;
	private int untaMaxPeriod04;
	private int untaMaxPeriod05;
	private int untaMaxPeriod06;
	private int untaMaxPeriod07;

	private BigDecimal untaPcUnit01;
	private BigDecimal untaPcUnit02;
	private BigDecimal untaPcUnit03;
	private BigDecimal untaPcUnit04;
	private BigDecimal untaPcUnit05;
	private BigDecimal untaPcUnit06;
	private BigDecimal untaPcUnit07;
	       
	private BigDecimal untaPcInitUnit01;
	private BigDecimal untaPcInitUnit02;
	private BigDecimal untaPcInitUnit03;
	private BigDecimal untaPcInitUnit04;
	private BigDecimal untaPcInitUnit05;
	private BigDecimal untaPcInitUnit06;
	private BigDecimal untaPcInitUnit07;
	private String untaBatctrcde;
	private String untaEnhall;
	private String untaCntcurr;
	private String untaCalcType;
	private BigDecimal untaInstprem;
	private BigDecimal untaAllocInit; 
	private BigDecimal untaAllocAccum;

	private BigDecimal untaEnhancePerc;
	private BigDecimal untaEnhanceAlloc;
	private String fundPool;
	private String statuz;
	
	public String getUntaStatuz() {
		return untaStatuz;
	}
	public void setUntaStatuz(String untaStatuz) {
		this.untaStatuz = untaStatuz;
	}
	public String getUntaFunction() {
		return untaFunction;
	}
	public void setUntaFunction(String untaFunction) {
		this.untaFunction = untaFunction;
	}
	public String getUntaCompany() {
		return untaCompany;
	}
	public void setUntaCompany(String untaCompany) {
		this.untaCompany = untaCompany;
	}
	public int getUntaEffdate() {
		return untaEffdate;
	}
	public void setUntaEffdate(int untaEffdate) {
		this.untaEffdate = untaEffdate;
	}
	public String getUntaAalbas() {
		return untaAalbas;
	}
	public void setUntaAalbas(String untaAalbas) {
		this.untaAalbas = untaAalbas;
	}
	public String getUntaBillfreq() {
		return untaBillfreq;
	}
	public void setUntaBillfreq(String untaBillfreq) {
		this.untaBillfreq = untaBillfreq;
	}
	public String getUntaSinglePremInd() {
		return untaSinglePremInd;
	}
	public void setUntaSinglePremInd(String untaSinglePremInd) {
		this.untaSinglePremInd = untaSinglePremInd;
	}
	public int getUntaMaxPeriod01() {
		return untaMaxPeriod01;
	}
	public void setUntaMaxPeriod01(int untaMaxPeriod01) {
		this.untaMaxPeriod01 = untaMaxPeriod01;
	}
	public int getUntaMaxPeriod02() {
		return untaMaxPeriod02;
	}
	public void setUntaMaxPeriod02(int untaMaxPeriod02) {
		this.untaMaxPeriod02 = untaMaxPeriod02;
	}
	public int getUntaMaxPeriod03() {
		return untaMaxPeriod03;
	}
	public void setUntaMaxPeriod03(int untaMaxPeriod03) {
		this.untaMaxPeriod03 = untaMaxPeriod03;
	}
	public int getUntaMaxPeriod04() {
		return untaMaxPeriod04;
	}
	public void setUntaMaxPeriod04(int untaMaxPeriod04) {
		this.untaMaxPeriod04 = untaMaxPeriod04;
	}
	public int getUntaMaxPeriod05() {
		return untaMaxPeriod05;
	}
	public void setUntaMaxPeriod05(int untaMaxPeriod05) {
		this.untaMaxPeriod05 = untaMaxPeriod05;
	}
	public int getUntaMaxPeriod06() {
		return untaMaxPeriod06;
	}
	public void setUntaMaxPeriod06(int untaMaxPeriod06) {
		this.untaMaxPeriod06 = untaMaxPeriod06;
	}
	public int getUntaMaxPeriod07() {
		return untaMaxPeriod07;
	}
	public void setUntaMaxPeriod07(int untaMaxPeriod07) {
		this.untaMaxPeriod07 = untaMaxPeriod07;
	}
	public BigDecimal getUntaPcUnit01() {
		return untaPcUnit01;
	}
	public void setUntaPcUnit01(BigDecimal untaPcUnit01) {
		this.untaPcUnit01 = untaPcUnit01;
	}
	public BigDecimal getUntaPcUnit02() {
		return untaPcUnit02;
	}
	public void setUntaPcUnit02(BigDecimal untaPcUnit02) {
		this.untaPcUnit02 = untaPcUnit02;
	}
	public BigDecimal getUntaPcUnit03() {
		return untaPcUnit03;
	}
	public void setUntaPcUnit03(BigDecimal untaPcUnit03) {
		this.untaPcUnit03 = untaPcUnit03;
	}
	public BigDecimal getUntaPcUnit04() {
		return untaPcUnit04;
	}
	public void setUntaPcUnit04(BigDecimal untaPcUnit04) {
		this.untaPcUnit04 = untaPcUnit04;
	}
	public BigDecimal getUntaPcUnit05() {
		return untaPcUnit05;
	}
	public void setUntaPcUnit05(BigDecimal untaPcUnit05) {
		this.untaPcUnit05 = untaPcUnit05;
	}
	public BigDecimal getUntaPcUnit06() {
		return untaPcUnit06;
	}
	public void setUntaPcUnit06(BigDecimal untaPcUnit06) {
		this.untaPcUnit06 = untaPcUnit06;
	}
	public BigDecimal getUntaPcUnit07() {
		return untaPcUnit07;
	}
	public void setUntaPcUnit07(BigDecimal untaPcUnit07) {
		this.untaPcUnit07 = untaPcUnit07;
	}
	public BigDecimal getUntaPcInitUnit01() {
		return untaPcInitUnit01;
	}
	public void setUntaPcInitUnit01(BigDecimal untaPcInitUnit01) {
		this.untaPcInitUnit01 = untaPcInitUnit01;
	}
	public BigDecimal getUntaPcInitUnit02() {
		return untaPcInitUnit02;
	}
	public void setUntaPcInitUnit02(BigDecimal untaPcInitUnit02) {
		this.untaPcInitUnit02 = untaPcInitUnit02;
	}
	public BigDecimal getUntaPcInitUnit03() {
		return untaPcInitUnit03;
	}
	public void setUntaPcInitUnit03(BigDecimal untaPcInitUnit03) {
		this.untaPcInitUnit03 = untaPcInitUnit03;
	}
	public BigDecimal getUntaPcInitUnit04() {
		return untaPcInitUnit04;
	}
	public void setUntaPcInitUnit04(BigDecimal untaPcInitUnit04) {
		this.untaPcInitUnit04 = untaPcInitUnit04;
	}
	public BigDecimal getUntaPcInitUnit05() {
		return untaPcInitUnit05;
	}
	public void setUntaPcInitUnit05(BigDecimal untaPcInitUnit05) {
		this.untaPcInitUnit05 = untaPcInitUnit05;
	}
	public BigDecimal getUntaPcInitUnit06() {
		return untaPcInitUnit06;
	}
	public void setUntaPcInitUnit06(BigDecimal untaPcInitUnit06) {
		this.untaPcInitUnit06 = untaPcInitUnit06;
	}
	public BigDecimal getUntaPcInitUnit07() {
		return untaPcInitUnit07;
	}
	public void setUntaPcInitUnit07(BigDecimal untaPcInitUnit07) {
		this.untaPcInitUnit07 = untaPcInitUnit07;
	}
	public String getUntaBatctrcde() {
		return untaBatctrcde;
	}
	public void setUntaBatctrcde(String untaBatctrcde) {
		this.untaBatctrcde = untaBatctrcde;
	}
	public String getUntaEnhall() {
		return untaEnhall;
	}
	public void setUntaEnhall(String untaEnhall) {
		this.untaEnhall = untaEnhall;
	}
	public String getUntaCntcurr() {
		return untaCntcurr;
	}
	public void setUntaCntcurr(String untaCntcurr) {
		this.untaCntcurr = untaCntcurr;
	}
	public String getUntaCalcType() {
		return untaCalcType;
	}
	public void setUntaCalcType(String untaCalcType) {
		this.untaCalcType = untaCalcType;
	}
	public BigDecimal getUntaInstprem() {
		return untaInstprem;
	}
	public void setUntaInstprem(BigDecimal untaInstprem) {
		this.untaInstprem = untaInstprem;
	}
	public BigDecimal getUntaAllocInit() {
		return untaAllocInit;
	}
	public void setUntaAllocInit(BigDecimal untaAllocInit) {
		this.untaAllocInit = untaAllocInit;
	}
	public BigDecimal getUntaAllocAccum() {
		return untaAllocAccum;
	}
	public void setUntaAllocAccum(BigDecimal untaAllocAccum) {
		this.untaAllocAccum = untaAllocAccum;
	}
	public BigDecimal getUntaEnhancePerc() {
		return untaEnhancePerc;
	}
	public void setUntaEnhancePerc(BigDecimal untaEnhancePerc) {
		this.untaEnhancePerc = untaEnhancePerc;
	}
	public BigDecimal getUntaEnhanceAlloc() {
		return untaEnhanceAlloc;
	}
	public void setUntaEnhanceAlloc(BigDecimal untaEnhanceAlloc) {
		this.untaEnhanceAlloc = untaEnhanceAlloc;
	}
	public String getFundPool() {
		return fundPool;
	}
	public void setFundPool(String fundPool) {
		this.fundPool = fundPool;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	
	
	
}
