package com.dxc.integral.life.dao.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

public class Sttrpf {
	@Id
	@Column(name = "UNIQUE_NUMBER")
    private long uniqueNumber;
    private String chdrpfx;
    private String chdrcoy;
    private String chdrnum;
    private int tranno;
    private String batccoy;
    private String batcbrn;
    private int batcactyr;
    private int batcactmn;
    private String batctrcde;
    private String batcbatch;
    private String revtrcde;
    private String billfreq;
    private String statgov;
    private String statagt;
    private String agntnum;
    private String aracde;
    private String cntbranch;
    private String bandage;
    private String bandsa;
    private String bandprm;
    private String bandtrm;
    private String cnttype;
    private String crtable;
    private String parind;
    private int commyr;
    private String cnPremStat;
    private String pstatcode;
    private String cntcurr;
    private String register;
    private String statFund;
    private String statSect;
    private String statSubsect;
    private String bandageg;
    private String bandsag;
    private String bandprmg;
    private String bandtrmg;
    private String srcebus;
    private String sex;
    private String mortcls;
    private String reptcd01;
    private String reptcd02;
    private String reptcd03;
    private String reptcd04;
    private String reptcd05;
    private String reptcd06;
    private String chdrstcda;
    private String chdrstcdb;
    private String chdrstcdc;
    private String chdrstcdd;
    private String chdrstcde;
    private String ovrdcat;
    private String statcat;
    private BigDecimal stcmth;
    private BigDecimal stvmth;
    private BigDecimal stpmth;
    private BigDecimal stsmth;
    private BigDecimal stmmth;
    private BigDecimal stimth;
    private BigDecimal stcmthg;
    private BigDecimal stpmthg;
    private BigDecimal stsmthg;
    private BigDecimal stumth;
    private BigDecimal stnmth;
    @Column(name = "TRDT")
    private int transactionDate;
    private int trannor;
    private String life;
    private String coverage;
    private String rider;
    private int planSuffix;
    private String acctccy;
    private BigDecimal stlmth;
    private BigDecimal stbmthg;
    private BigDecimal stldmthg;
    private BigDecimal straamt;
	private String usrprf;
	private String jobnm;
	private Date datime;
    public Sttrpf() {
    }

    public Sttrpf(Sttrpf oldSttrpf) {
        super();
        this.uniqueNumber = oldSttrpf.uniqueNumber;
        this.chdrpfx = oldSttrpf.chdrpfx;
        this.chdrcoy = oldSttrpf.chdrcoy;
        this.chdrnum = oldSttrpf.chdrnum;
        this.tranno = oldSttrpf.tranno;
        this.batccoy = oldSttrpf.batccoy;
        this.batcbrn = oldSttrpf.batcbrn;
        this.batcactyr = oldSttrpf.batcactyr;
        this.batcactmn = oldSttrpf.batcactmn;
        this.batctrcde = oldSttrpf.batctrcde;
        this.batcbatch = oldSttrpf.batcbatch;
        this.revtrcde = oldSttrpf.revtrcde;
        this.billfreq = oldSttrpf.billfreq;
        this.statgov = oldSttrpf.statgov;
        this.statagt = oldSttrpf.statagt;
        this.agntnum = oldSttrpf.agntnum;
        this.aracde = oldSttrpf.aracde;
        this.cntbranch = oldSttrpf.cntbranch;
        this.bandage = oldSttrpf.bandage;
        this.bandsa = oldSttrpf.bandsa;
        this.bandprm = oldSttrpf.bandprm;
        this.bandtrm = oldSttrpf.bandtrm;
        this.cnttype = oldSttrpf.cnttype;
        this.crtable = oldSttrpf.crtable;
        this.parind = oldSttrpf.parind;
        this.commyr = oldSttrpf.commyr;
        this.cnPremStat = oldSttrpf.cnPremStat;
        this.pstatcode = oldSttrpf.pstatcode;
        this.cntcurr = oldSttrpf.cntcurr;
        this.register = oldSttrpf.register;
        this.statFund = oldSttrpf.statFund;
        this.statSect = oldSttrpf.statSect;
        this.statSubsect = oldSttrpf.statSubsect;
        this.bandageg = oldSttrpf.bandageg;
        this.bandsag = oldSttrpf.bandsag;
        this.bandprmg = oldSttrpf.bandprmg;
        this.bandtrmg = oldSttrpf.bandtrmg;
        this.srcebus = oldSttrpf.srcebus;
        this.sex = oldSttrpf.sex;
        this.mortcls = oldSttrpf.mortcls;
        this.reptcd01 = oldSttrpf.reptcd01;
        this.reptcd02 = oldSttrpf.reptcd02;
        this.reptcd03 = oldSttrpf.reptcd03;
        this.reptcd04 = oldSttrpf.reptcd04;
        this.reptcd05 = oldSttrpf.reptcd05;
        this.reptcd06 = oldSttrpf.reptcd06;
        this.chdrstcda = oldSttrpf.chdrstcda;
        this.chdrstcdb = oldSttrpf.chdrstcdb;
        this.chdrstcdc = oldSttrpf.chdrstcdc;
        this.chdrstcdd = oldSttrpf.chdrstcdd;
        this.chdrstcde = oldSttrpf.chdrstcde;
        this.ovrdcat = oldSttrpf.ovrdcat;
        this.statcat = oldSttrpf.statcat;
        this.stcmth = oldSttrpf.stcmth;
        this.stvmth = oldSttrpf.stvmth;
        this.stpmth = oldSttrpf.stpmth;
        this.stsmth = oldSttrpf.stsmth;
        this.stmmth = oldSttrpf.stmmth;
        this.stimth = oldSttrpf.stimth;
        this.stcmthg = oldSttrpf.stcmthg;
        this.stpmthg = oldSttrpf.stpmthg;
        this.stsmthg = oldSttrpf.stsmthg;
        this.stumth = oldSttrpf.stumth;
        this.stnmth = oldSttrpf.stnmth;
        this.transactionDate = oldSttrpf.transactionDate;
        this.trannor = oldSttrpf.trannor;
        this.life = oldSttrpf.life;
        this.coverage = oldSttrpf.coverage;
        this.rider = oldSttrpf.rider;
        this.planSuffix = oldSttrpf.planSuffix;
        this.acctccy = oldSttrpf.acctccy;
        this.stlmth = oldSttrpf.stlmth;
        this.stbmthg = oldSttrpf.stbmthg;
        this.stldmthg = oldSttrpf.stldmthg;
        this.straamt = oldSttrpf.straamt;
    }

    public long getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public String getChdrpfx() {
        return chdrpfx;
    }

    public void setChdrpfx(String chdrpfx) {
        this.chdrpfx = chdrpfx;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public int getTranno() {
        return tranno;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public String getBatccoy() {
        return batccoy;
    }

    public void setBatccoy(String batccoy) {
        this.batccoy = batccoy;
    }

    public String getBatcbrn() {
        return batcbrn;
    }

    public void setBatcbrn(String batcbrn) {
        this.batcbrn = batcbrn;
    }

    public int getBatcactyr() {
        return batcactyr;
    }

    public void setBatcactyr(int batcactyr) {
        this.batcactyr = batcactyr;
    }

    public int getBatcactmn() {
        return batcactmn;
    }

    public void setBatcactmn(int batcactmn) {
        this.batcactmn = batcactmn;
    }

    public String getBatctrcde() {
        return batctrcde;
    }

    public void setBatctrcde(String batctrcde) {
        this.batctrcde = batctrcde;
    }

    public String getBatcbatch() {
        return batcbatch;
    }

    public void setBatcbatch(String batcbatch) {
        this.batcbatch = batcbatch;
    }

    public String getRevtrcde() {
        return revtrcde;
    }

    public void setRevtrcde(String revtrcde) {
        this.revtrcde = revtrcde;
    }

    public String getBillfreq() {
        return billfreq;
    }

    public void setBillfreq(String billfreq) {
        this.billfreq = billfreq;
    }

    public String getStatgov() {
        return statgov;
    }

    public void setStatgov(String statgov) {
        this.statgov = statgov;
    }

    public String getStatagt() {
        return statagt;
    }

    public void setStatagt(String statagt) {
        this.statagt = statagt;
    }

    public String getAgntnum() {
        return agntnum;
    }

    public void setAgntnum(String agntnum) {
        this.agntnum = agntnum;
    }

    public String getAracde() {
        return aracde;
    }

    public void setAracde(String aracde) {
        this.aracde = aracde;
    }

    public String getCntbranch() {
        return cntbranch;
    }

    public void setCntbranch(String cntbranch) {
        this.cntbranch = cntbranch;
    }

    public String getBandage() {
        return bandage;
    }

    public void setBandage(String bandage) {
        this.bandage = bandage;
    }

    public String getBandsa() {
        return bandsa;
    }

    public void setBandsa(String bandsa) {
        this.bandsa = bandsa;
    }

    public String getBandprm() {
        return bandprm;
    }

    public void setBandprm(String bandprm) {
        this.bandprm = bandprm;
    }

    public String getBandtrm() {
        return bandtrm;
    }

    public void setBandtrm(String bandtrm) {
        this.bandtrm = bandtrm;
    }

    public String getCnttype() {
        return cnttype;
    }

    public void setCnttype(String cnttype) {
        this.cnttype = cnttype;
    }

    public String getCrtable() {
        return crtable;
    }

    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }

    public String getParind() {
        return parind;
    }

    public void setParind(String parind) {
        this.parind = parind;
    }

    public int getCommyr() {
        return commyr;
    }

    public void setCommyr(int commyr) {
        this.commyr = commyr;
    }

    public String getCnPremStat() {
        return cnPremStat;
    }

    public void setCnPremStat(String cnPremStat) {
        this.cnPremStat = cnPremStat;
    }

    public String getPstatcode() {
        return pstatcode;
    }

    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }

    public String getCntcurr() {
        return cntcurr;
    }

    public void setCntcurr(String cntcurr) {
        this.cntcurr = cntcurr;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getStatFund() {
        return statFund;
    }

    public void setStatFund(String statFund) {
        this.statFund = statFund;
    }

    public String getStatSect() {
        return statSect;
    }

    public void setStatSect(String statSect) {
        this.statSect = statSect;
    }

    public String getStatSubsect() {
        return statSubsect;
    }

    public void setStatSubsect(String statSubsect) {
        this.statSubsect = statSubsect;
    }

    public String getBandageg() {
        return bandageg;
    }

    public void setBandageg(String bandageg) {
        this.bandageg = bandageg;
    }

    public String getBandsag() {
        return bandsag;
    }

    public void setBandsag(String bandsag) {
        this.bandsag = bandsag;
    }

    public String getBandprmg() {
        return bandprmg;
    }

    public void setBandprmg(String bandprmg) {
        this.bandprmg = bandprmg;
    }

    public String getBandtrmg() {
        return bandtrmg;
    }

    public void setBandtrmg(String bandtrmg) {
        this.bandtrmg = bandtrmg;
    }

    public String getSrcebus() {
        return srcebus;
    }

    public void setSrcebus(String srcebus) {
        this.srcebus = srcebus;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMortcls() {
        return mortcls;
    }

    public void setMortcls(String mortcls) {
        this.mortcls = mortcls;
    }

    public String getReptcd01() {
        return reptcd01;
    }

    public void setReptcd01(String reptcd01) {
        this.reptcd01 = reptcd01;
    }

    public String getReptcd02() {
        return reptcd02;
    }

    public void setReptcd02(String reptcd02) {
        this.reptcd02 = reptcd02;
    }

    public String getReptcd03() {
        return reptcd03;
    }

    public void setReptcd03(String reptcd03) {
        this.reptcd03 = reptcd03;
    }

    public String getReptcd04() {
        return reptcd04;
    }

    public void setReptcd04(String reptcd04) {
        this.reptcd04 = reptcd04;
    }

    public String getReptcd05() {
        return reptcd05;
    }

    public void setReptcd05(String reptcd05) {
        this.reptcd05 = reptcd05;
    }

    public String getReptcd06() {
        return reptcd06;
    }

    public void setReptcd06(String reptcd06) {
        this.reptcd06 = reptcd06;
    }

    public String getChdrstcda() {
        return chdrstcda;
    }

    public void setChdrstcda(String chdrstcda) {
        this.chdrstcda = chdrstcda;
    }

    public String getChdrstcdb() {
        return chdrstcdb;
    }

    public void setChdrstcdb(String chdrstcdb) {
        this.chdrstcdb = chdrstcdb;
    }

    public String getChdrstcdc() {
        return chdrstcdc;
    }

    public void setChdrstcdc(String chdrstcdc) {
        this.chdrstcdc = chdrstcdc;
    }

    public String getChdrstcdd() {
        return chdrstcdd;
    }

    public void setChdrstcdd(String chdrstcdd) {
        this.chdrstcdd = chdrstcdd;
    }

    public String getChdrstcde() {
        return chdrstcde;
    }

    public void setChdrstcde(String chdrstcde) {
        this.chdrstcde = chdrstcde;
    }

    public String getOvrdcat() {
        return ovrdcat;
    }

    public void setOvrdcat(String ovrdcat) {
        this.ovrdcat = ovrdcat;
    }

    public String getStatcat() {
        return statcat;
    }

    public void setStatcat(String statcat) {
        this.statcat = statcat;
    }

    public BigDecimal getStcmth() {
        return stcmth;
    }

    public void setStcmth(BigDecimal stcmth) {
        this.stcmth = stcmth;
    }

    public BigDecimal getStvmth() {
        return stvmth;
    }

    public void setStvmth(BigDecimal stvmth) {
        this.stvmth = stvmth;
    }

    public BigDecimal getStpmth() {
        return stpmth;
    }

    public void setStpmth(BigDecimal stpmth) {
        this.stpmth = stpmth;
    }

    public BigDecimal getStsmth() {
        return stsmth;
    }

    public void setStsmth(BigDecimal stsmth) {
        this.stsmth = stsmth;
    }

    public BigDecimal getStmmth() {
        return stmmth;
    }

    public void setStmmth(BigDecimal stmmth) {
        this.stmmth = stmmth;
    }

    public BigDecimal getStimth() {
        return stimth;
    }

    public void setStimth(BigDecimal stimth) {
        this.stimth = stimth;
    }

    public BigDecimal getStcmthg() {
        return stcmthg;
    }

    public void setStcmthg(BigDecimal stcmthg) {
        this.stcmthg = stcmthg;
    }

    public BigDecimal getStpmthg() {
        return stpmthg;
    }

    public void setStpmthg(BigDecimal stpmthg) {
        this.stpmthg = stpmthg;
    }

    public BigDecimal getStsmthg() {
        return stsmthg;
    }

    public void setStsmthg(BigDecimal stsmthg) {
        this.stsmthg = stsmthg;
    }

    public BigDecimal getStumth() {
        return stumth;
    }

    public void setStumth(BigDecimal stumth) {
        this.stumth = stumth;
    }

    public BigDecimal getStnmth() {
        return stnmth;
    }

    public void setStnmth(BigDecimal stnmth) {
        this.stnmth = stnmth;
    }

    public int getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getTrannor() {
        return trannor;
    }

    public void setTrannor(int trannor) {
        this.trannor = trannor;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getRider() {
        return rider;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public int getPlanSuffix() {
        return planSuffix;
    }

    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }

    public String getAcctccy() {
        return acctccy;
    }

    public void setAcctccy(String acctccy) {
        this.acctccy = acctccy;
    }

    public BigDecimal getStlmth() {
        return stlmth;
    }

    public void setStlmth(BigDecimal stlmth) {
        this.stlmth = stlmth;
    }

    public BigDecimal getStbmthg() {
        return stbmthg;
    }

    public void setStbmthg(BigDecimal stbmthg) {
        this.stbmthg = stbmthg;
    }

    public BigDecimal getStldmthg() {
        return stldmthg;
    }

    public void setStldmthg(BigDecimal stldmthg) {
        this.stldmthg = stldmthg;
    }

    public BigDecimal getStraamt() {
        return straamt;
    }

    public void setStraamt(BigDecimal straamt) {
        this.straamt = straamt;
    }

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Date getDatime() {
		return datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}
    
}