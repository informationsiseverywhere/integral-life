package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.TaxdpfDAO;
import com.dxc.integral.life.dao.model.Taxdpf;
/**
 * @author wli31
 */
@Repository("taxdpfDAO")
@Lazy
public class TaxdpfDAOImpl extends BaseDAOImpl implements TaxdpfDAO {

	@Override
	public void insertTaxdpfRecords(List<Taxdpf> taxdpfList) {
		final List<Taxdpf> tempTaxdpfList = taxdpfList;   
	       String sql = "insert into taxdpf(chdrcoy, chdrnum, life, coverage, rider, plansfx, effdate, tranref, instfrom, instto, billcd, tranno, baseamt, trantype,"
		    		+ "taxamt01, taxamt02, taxamt03, txabsind01, txabsind02, txabsind03, txtype01, txtype02, txtype03, postflg, usrprf, jobnm, datime)"
		    		+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	       jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
	            @Override
	            public int getBatchSize() {  
	                 return tempTaxdpfList.size();   
	            }  
	            @Override  
	            public void setValues(PreparedStatement ps, int i)  
	                    throws SQLException { 
	            	ps.setString(1, taxdpfList.get(i).getChdrcoy());
					ps.setString(2, taxdpfList.get(i).getChdrnum());
				    ps.setString(3, taxdpfList.get(i).getLife());
				    ps.setString(4, taxdpfList.get(i).getCoverage());
				    ps.setString(5, taxdpfList.get(i).getRider());	
				    ps.setInt(6, taxdpfList.get(i).getPlansfx());	
				    ps.setInt(7, taxdpfList.get(i).getEffdate());
				    ps.setString(8, taxdpfList.get(i).getTranref());	
					ps.setInt(9, taxdpfList.get(i).getInstfrom());
					ps.setInt(10, taxdpfList.get(i).getInstto());
					ps.setInt(11, taxdpfList.get(i).getBillcd());
					ps.setInt(12, taxdpfList.get(i).getTranno());
				    ps.setBigDecimal(13, taxdpfList.get(i).getBaseamt()); 	
				    ps.setString(14, taxdpfList.get(i).getTrantype());	
				    ps.setBigDecimal(15, taxdpfList.get(i).getTaxamt01()); 
				    ps.setBigDecimal(16, taxdpfList.get(i).getTaxamt02());
				    ps.setBigDecimal(17, taxdpfList.get(i).getTaxamt03());
					ps.setString(18, taxdpfList.get(i).getTxabsind01());
					ps.setString(19, taxdpfList.get(i).getTxabsind02());
					ps.setString(20, taxdpfList.get(i).getTxabsind03());		
					ps.setString(21, taxdpfList.get(i).getTxtype01());
					ps.setString(22, taxdpfList.get(i).getTxtype02());
					ps.setString(23, taxdpfList.get(i).getTxtype03());	
					ps.setString(24, taxdpfList.get(i).getPostflg());
				    ps.setString(25, taxdpfList.get(i).getUsrprf());
				    ps.setString(26, taxdpfList.get(i).getJobnm());
				    ps.setTimestamp(27, new Timestamp(System.currentTimeMillis()));	
	            }   
	      });  

	}
	
	@Override
	public void updateTaxdpfRecords(List<Taxdpf> taxdpfList) {
		final List<Taxdpf> tempTaxdpfList = taxdpfList;   
	       String sql = "UPDATE TAXDPF SET POSTFLG='P',JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
	       jdbcTemplate.batchUpdate(sql,new BatchPreparedStatementSetter() {  
	            @Override
	            public int getBatchSize() {  
	                 return tempTaxdpfList.size();   
	            }  
	            @Override  
	            public void setValues(PreparedStatement ps, int i)  
	                    throws SQLException { 
	            	ps.setString(1, taxdpfList.get(i).getJobnm());
					ps.setString(2, taxdpfList.get(i).getUsrprf());
				    ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
				    ps.setLong(4, taxdpfList.get(i).getUnique_number());
	            }   
	      });  

	}
	
	@Override
	public int deleteTaxdpfRecords(String chdrcoy, String chdrnum, int instfrom, int instto) {
		String sql = "DELETE FROM TAXDPF WHERE CHDRCOY=? AND CHDRNUM=? AND INSTFROM=? AND INSTTO=?";
		return jdbcTemplate.update(sql, new Object[] {chdrcoy, chdrnum, instfrom, instto});
	}

}
