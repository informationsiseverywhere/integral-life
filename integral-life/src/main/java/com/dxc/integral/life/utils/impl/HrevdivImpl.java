package com.dxc.integral.life.utils.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.life.beans.GreversDTO;
import com.dxc.integral.life.dao.HdispfDAO;
import com.dxc.integral.life.dao.HdivpfDAO;
import com.dxc.integral.life.dao.model.Hdispf;
import com.dxc.integral.life.dao.model.Hdivpf;

@Service("hrevdiv")
@Lazy
public class HrevdivImpl {

   @Autowired
   private HdivpfDAO hdivpfDAO;
   @Autowired
   private HdispfDAO hdispfDAO;

   private int wsaaStmtNo = 0;
   private int wsaaStmtDate = 0;
   private BigDecimal wsaaBalance;
	
	public GreversDTO processHrevdiv(GreversDTO greversDTO) {
		greversDTO.setStatuz("****");
		List<Hdivpf> list = hdivpfDAO.readHdivpfRecords(greversDTO.getChdrcoy(), greversDTO.getChdrnum(), greversDTO.getTranno());
		if(list != null && list.size() > 0) {
			for(Hdivpf hdivpf : list) {
				maintainHdiv(hdivpf, greversDTO);
			}
			
		}else {
			greversDTO.setStatuz("BOMB");
			return greversDTO;
		}
		
		if(!maintainHtis(greversDTO)) {
			greversDTO.setStatuz("BOMB");
		}
		return greversDTO;
		
	}
	
	public void maintainHdiv(Hdivpf hdivpf, GreversDTO greversDTO) {
		if("00000".equals(hdivpf.getBatcbatch())){
			if(hdivpf.getDivdStmtNo().intValue() == 0) {
				hdivpfDAO.deleteHdivpfRecord(hdivpf);
			}else {
				Hdivpf insthdivpf = new Hdivpf();
				insthdivpf.setChdrcoy(hdivpf.getChdrcoy());
				insthdivpf.setChdrnum(hdivpf.getChdrnum());
				insthdivpf.setLife(hdivpf.getLife());
				insthdivpf.setCoverage(hdivpf.getCoverage());
				insthdivpf.setRider(hdivpf.getRider());
				insthdivpf.setPlnsfx(hdivpf.getPlnsfx());
				insthdivpf.setHpuanbr(hdivpf.getHpuanbr());
				insthdivpf.setEffdate(hdivpf.getEffdate());
				insthdivpf.setJlife(hdivpf.getJlife());
				insthdivpf.setCntcurr(hdivpf.getCntcurr());
				insthdivpf.setHdvtyp(hdivpf.getHdvtyp());
				insthdivpf.setHdvrate(hdivpf.getHdvrate());
				insthdivpf.setHdveffdt(hdivpf.getHdveffdt());
				insthdivpf.setZdivopt(hdivpf.getZdivopt());
				insthdivpf.setZcshdivmth(hdivpf.getZcshdivmth());
				insthdivpf.setHdvopttx(hdivpf.getHdvopttx());
				insthdivpf.setHdvcaptx(hdivpf.getHdvcaptx());
				insthdivpf.setHincapdt(hdivpf.getHincapdt());
				insthdivpf.setTranno(greversDTO.getNewTranno());
				insthdivpf.setDivdStmtNo(0);
				insthdivpf.setHdvamt(hdivpf.getHdvamt().multiply(new BigDecimal(-1)).setScale(2));
				insthdivpf.setHdvaldt(greversDTO.getEffdate());
				insthdivpf.setBatccoy(greversDTO.getBatckey().substring(2, 3));
				insthdivpf.setBatcbrn(greversDTO.getBatckey().substring(3, 5));
				insthdivpf.setBatcactyr(Integer.parseInt(greversDTO.getBatckey().substring(5, 9)));
				insthdivpf.setBatcactmn(Integer.parseInt(greversDTO.getBatckey().substring(8, 10)));
				insthdivpf.setBatctrcde(greversDTO.getBatckey().substring(10, 14));
				insthdivpf.setBatcbatch(greversDTO.getBatckey().substring(14, 19));
				
				hdivpfDAO.insertHdivpfRecords(insthdivpf);
			
			}
		}
		
	}
	
	private boolean maintainHtis(GreversDTO greversDTO) {
		List<Hdispf> list = hdispfDAO.readHdispfRecords(greversDTO.getChdrcoy(), greversDTO.getChdrnum(), greversDTO.getTranno());
		Hdispf prehdispf;
		if(list != null && list.size() >0) {
			for(Hdispf hdispf : list) {
				wsaaStmtNo = hdispf.getHdvsmtno().intValue();
				wsaaStmtDate = hdispf.getHdvsmtdt().intValue();
				wsaaBalance = hdispf.getHdvbalst();
				hdispfDAO.deleteHdispfRecord(hdispf.getChdrcoy(), hdispf.getChdrnum(), hdispf.getLife(), hdispf.getCoverage(), hdispf.getRider(), hdispf.getPlnsfx().intValue());
				prehdispf = hdispfDAO.readHdispf(hdispf.getChdrcoy(), hdispf.getChdrnum(), hdispf.getLife(), hdispf.getCoverage(), hdispf.getRider(), hdispf.getPlnsfx().intValue());
				if(prehdispf == null) {
					break;
				}else {
					if(!"2".equals(prehdispf.getValidflag())) {
						greversDTO.setStatuz("BOMB");
						return false;
					}
					
					prehdispf.setValidflag("1");
					prehdispf.setHdvsmtno(wsaaStmtNo);
					prehdispf.setHdvsmtdt(wsaaStmtDate);
					prehdispf.setHdvbalst(wsaaBalance);
					hdispfDAO.updateHdispf(prehdispf);
				}
				
			}



		}
		return true;
		
	}
	

	
}
