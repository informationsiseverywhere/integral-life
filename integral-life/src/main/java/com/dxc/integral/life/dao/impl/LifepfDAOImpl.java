package com.dxc.integral.life.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.LifepfDAO;
import com.dxc.integral.life.dao.model.Lifepf;
/**
 * @author wli31
 */
@Repository("lifepfDAO")
@Lazy
public class LifepfDAOImpl extends BaseDAOImpl implements LifepfDAO {
	public List<Lifepf> getLifeRecord(String chdrcoy, String chdrnum, String life, String jlife){
		StringBuilder sql = new StringBuilder("select * from lifepf");
	  	sql.append(" where chdrcoy=? and chdrnum=? and life=? and jlife=?");
	  	return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum,life,jlife},
				new BeanPropertyRowMapper<Lifepf>(Lifepf.class));
	}
	
	public Lifepf getValidLifeRecord(String chdrcoy, String chdrnum, String life, String jlife){
		StringBuilder sql = new StringBuilder(" SELECT * FROM LIFEPF ");
	  	sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? AND VALIDFLAG='1' ");
	  	return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum,life,jlife},
				new BeanPropertyRowMapper<Lifepf>(Lifepf.class));
	}
	public Lifepf getLifeRecordByCurrfrom(String chdrcoy, String chdrnum, String life, String jlife,int currfrom){
		StringBuilder sql = new StringBuilder("select * from lifepf");
	  	sql.append(" where chdrcoy=? and chdrnum=? and life=? and jlife=? and currfrom=?");
	  	return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum,life,jlife,currfrom},
				new BeanPropertyRowMapper<Lifepf>(Lifepf.class));
	}
	public List<Lifepf> searchLifeRecordByChdrNum(String chdrcoy, String chdrnum){
		StringBuilder sql = new StringBuilder("select * from lifepf");
	  	sql.append(" where chdrcoy=? and chdrnum=? ");
	  	return jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum},
				new BeanPropertyRowMapper<Lifepf>(Lifepf.class));
	}

	@Override
	public Lifepf getLifecmcRecord(String chdrcoy, String chdrnum, String life, String jlife) {
		StringBuilder sql = new StringBuilder("select * from lifepf");
	  	sql.append(" where chdrcoy=? and chdrnum=? and life=? and jlife=? order by chdrnum asc, life asc, jlife asc, unique_number desc");
	  	return jdbcTemplate.queryForObject(sql.toString(), new Object[] {chdrcoy, chdrnum,life,jlife},
				new BeanPropertyRowMapper<Lifepf>(Lifepf.class));
	}

	@Override
	public Lifepf getRcntChngRecord(String chdrcoy, String chdrnum, String life, String jlife) {
		List<Lifepf> lifeList = new ArrayList<>();
		Lifepf rcntChangLife = null;
		StringBuilder sql = new StringBuilder("SELECT * FROM LIFEPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? AND VALIDFLAG='2' ");
	  	sql.append("AND TRANNO = (SELECT MAX(TRANNO) FROM LIFEPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? AND VALIDFLAG='2') ");
	  	lifeList = jdbcTemplate.query(sql.toString(), new Object[] {chdrcoy, chdrnum, life, jlife, chdrcoy, chdrnum, life, jlife},
				new BeanPropertyRowMapper<Lifepf>(Lifepf.class));
	  	if(!lifeList.isEmpty()) 
	  		rcntChangLife = lifeList.get(0);
		return rcntChangLife;
	}
}