package com.dxc.integral.life.smarttable.pojo;


public class T5688 {

  	public String anbrqd;
  	public String bankcode;
  	public int cfiLim;
  	public String comlvlacc;
  	public String defFupMeth;
  	public String feemeth;
  	public int jlifemax;
  	public int jlifemin;
  	public int lifemax;
  	public int lifemin;
  	public String minDepMth;
  	public String nbusallw;
  	public String occrqd;
  	public int poldef;
  	public int polmax;
  	public int polmin;
  	public String revacc;
  	public String smkrqd;
  	public String taxrelmth;
  	public String zcertall;
  	public String zcfidtall;
  	public String znfopt;
  	public String dflexmeth;
  	public String bfreqallw;
	public String getAnbrqd() {
		return anbrqd;
	}
	public void setAnbrqd(String anbrqd) {
		this.anbrqd = anbrqd;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public int getCfiLim() {
		return cfiLim;
	}
	public void setCfiLim(int cfiLim) {
		this.cfiLim = cfiLim;
	}
	public String getComlvlacc() {
		return comlvlacc;
	}
	public void setComlvlacc(String comlvlacc) {
		this.comlvlacc = comlvlacc;
	}
	public String getDefFupMeth() {
		return defFupMeth;
	}
	public void setDefFupMeth(String defFupMeth) {
		this.defFupMeth = defFupMeth;
	}
	public String getFeemeth() {
		return feemeth;
	}
	public void setFeemeth(String feemeth) {
		this.feemeth = feemeth;
	}
	public int getJlifemax() {
		return jlifemax;
	}
	public void setJlifemax(int jlifemax) {
		this.jlifemax = jlifemax;
	}
	public int getJlifemin() {
		return jlifemin;
	}
	public void setJlifemin(int jlifemin) {
		this.jlifemin = jlifemin;
	}
	public int getLifemax() {
		return lifemax;
	}
	public void setLifemax(int lifemax) {
		this.lifemax = lifemax;
	}
	public int getLifemin() {
		return lifemin;
	}
	public void setLifemin(int lifemin) {
		this.lifemin = lifemin;
	}
	public String getMinDepMth() {
		return minDepMth;
	}
	public void setMinDepMth(String minDepMth) {
		this.minDepMth = minDepMth;
	}
	public String getNbusallw() {
		return nbusallw;
	}
	public void setNbusallw(String nbusallw) {
		this.nbusallw = nbusallw;
	}
	public String getOccrqd() {
		return occrqd;
	}
	public void setOccrqd(String occrqd) {
		this.occrqd = occrqd;
	}
	public int getPoldef() {
		return poldef;
	}
	public void setPoldef(int poldef) {
		this.poldef = poldef;
	}
	public int getPolmax() {
		return polmax;
	}
	public void setPolmax(int polmax) {
		this.polmax = polmax;
	}
	public int getPolmin() {
		return polmin;
	}
	public void setPolmin(int polmin) {
		this.polmin = polmin;
	}
	public String getRevacc() {
		return revacc;
	}
	public void setRevacc(String revacc) {
		this.revacc = revacc;
	}
	public String getSmkrqd() {
		return smkrqd;
	}
	public void setSmkrqd(String smkrqd) {
		this.smkrqd = smkrqd;
	}
	public String getTaxrelmth() {
		return taxrelmth;
	}
	public void setTaxrelmth(String taxrelmth) {
		this.taxrelmth = taxrelmth;
	}
	public String getZcertall() {
		return zcertall;
	}
	public void setZcertall(String zcertall) {
		this.zcertall = zcertall;
	}
	public String getZcfidtall() {
		return zcfidtall;
	}
	public void setZcfidtall(String zcfidtall) {
		this.zcfidtall = zcfidtall;
	}
	public String getZnfopt() {
		return znfopt;
	}
	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}
	public String getDflexmeth() {
		return dflexmeth;
	}
	public void setDflexmeth(String dflexmeth) {
		this.dflexmeth = dflexmeth;
	}
	public String getBfreqallw() {
		return bfreqallw;
	}
	public void setBfreqallw(String bfreqallw) {
		this.bfreqallw = bfreqallw;
	}
  	
}
