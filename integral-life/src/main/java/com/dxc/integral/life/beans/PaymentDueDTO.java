package com.dxc.integral.life.beans;

import java.math.BigDecimal;

/**
 * DTO for utility.
 * 
 * @author mmalik25
 */
public class PaymentDueDTO {

	/** The nextPaydate */
	private Integer nextPaydate;
	/** The paymmeth */
	private String paymmeth;
	/** The payOpt */
	private String payOpt;
	/** The paymentDue */
	private BigDecimal paymentDue;
	/** The longDesc */
	private String longDesc;
	/** The userNum */
	private Integer userNum;
	/** The contractCurr */
	private String contractCurr;

	/**
	 * @return the nextPaydate
	 */
	public Integer getNextPaydate() {
		return nextPaydate;
	}

	/**
	 * @param nextPaydate
	 *            the nextPaydate to set
	 */
	public void setNextPaydate(Integer nextPaydate) {
		this.nextPaydate = nextPaydate;
	}

	/**
	 * @return the paymmeth
	 */
	public String getPaymmeth() {
		return paymmeth;
	}

	/**
	 * @param paymmeth
	 *            the paymmeth to set
	 */
	public void setPaymmeth(String paymmeth) {
		this.paymmeth = paymmeth;
	}

	/**
	 * @return the payOpt
	 */
	public String getPayOpt() {
		return payOpt;
	}

	/**
	 * @param payOpt
	 *            the payOpt to set
	 */
	public void setPayOpt(String payOpt) {
		this.payOpt = payOpt;
	}

	/**
	 * @return the paymentDue
	 */
	public BigDecimal getPaymentDue() {
		return paymentDue;
	}

	/**
	 * @param paymentDue
	 *            the paymentDue to set
	 */
	public void setPaymentDue(BigDecimal paymentDue) {
		this.paymentDue = paymentDue;
	}

	/**
	 * @return the longDesc
	 */
	public String getLongDesc() {
		return longDesc;
	}

	/**
	 * @param longDesc
	 *            the longDesc to set
	 */
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	/**
	 * @return the userNum
	 */
	public Integer getUserNum() {
		return userNum;
	}

	/**
	 * @param userNum
	 *            the userNum to set
	 */
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}

	/**
	 * @return the contractCurr
	 */
	public String getContractCurr() {
		return contractCurr;
	}

	/**
	 * @param contractCurr
	 *            the contractCurr to set
	 */
	public void setContractCurr(String contractCurr) {
		this.contractCurr = contractCurr;
	}

}
