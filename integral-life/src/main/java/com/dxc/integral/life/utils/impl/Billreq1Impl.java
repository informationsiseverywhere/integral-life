package com.dxc.integral.life.utils.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.dxc.integral.fsu.beans.AddacmvDTO;
import com.dxc.integral.fsu.dao.ClntpfDAO;
import com.dxc.integral.fsu.dao.model.Clntpf;
import com.dxc.integral.fsu.smarttable.pojo.T3616;
import com.dxc.integral.fsu.smarttable.pojo.T3698;
import com.dxc.integral.fsu.utils.Addacmv;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.life.beans.BillreqDTO;
import com.dxc.integral.life.dao.AcagpfDAO;
import com.dxc.integral.life.dao.BextpfDAO;
import com.dxc.integral.life.dao.GrpspfDAO;
import com.dxc.integral.life.dao.model.Acagpf;
import com.dxc.integral.life.dao.model.Bextpf;
import com.dxc.integral.life.dao.model.Grpspf;
import com.dxc.integral.life.exceptions.DataNoFoundException;
import com.dxc.integral.life.smarttable.pojo.T3620;
import com.dxc.integral.life.smarttable.pojo.T3684;
import com.dxc.integral.life.smarttable.pojo.T3700;
import com.dxc.integral.life.smarttable.pojo.T3701;
import com.dxc.integral.life.utils.Billreq1;

/**
 * @author wli31
 */
@Service("billreq1")
@Lazy
public class Billreq1Impl implements Billreq1 {
	@Autowired
	private ItempfDAO itempfDAO;
	@Autowired
	private GrpspfDAO grpspfDAO;
	@Autowired
	private ClntpfDAO clntpfDAO;
	@Autowired
	private AcagpfDAO acagpfDAO;
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;
	@Autowired
	private Addacmv addacmv;
	@Autowired
	private BextpfDAO bextpfDAO;
	@Autowired
	private DescpfDAO descpfDAO;
	
	private WsaaT3620ArrayInner wsaaT3620ArrayInner = null;
	private WsaaT3700ArrayInner wsaaT3700ArrayInner = null;
	private String[] t3616Sacscodes;
	private String[] t3616SubjPrefs;
	private String[] t3701Reconsbr;
	private String[] t3684Key;
	private String[] t3684AssumePaid;
	private Integer[] t3700Sorts = null;
	private String[] wsaaBlrqField = null;
	private Integer[] wsaaField = null;
	private int wsaaT3616Ix;
	private int wsaaT3620Ix;
	private int wsaaT3700Ix;
	private int wsaaT3701Ix;
	private int wsaaT3684Ix;
	private int wsaaT3698Sub;
	private T3616 t3616IO = null;
	private T3620 t3620IO = null;
	private T3700 t3700IO = null;
	private T3701 t3701IO = null;
	private T3684 t3684IO = null;
	private T3698 t3698IO = null;
	private String lastTrancode = "";
	private String t3698Trancode = "";
	private String t3698Sacscode = "";
	private String longdesc = "";
	private AddacmvDTO addacmvDTO = null;
	private Bextpf bextpf = null;
	private Grpspf grpspf = null;
	@Override
	public BillreqDTO processBillreq1(BillreqDTO billreqDTO)throws Exception {
		billreqDTO.setStatuz("****");
		wsaaT3616Ix = 0;
		wsaaT3620Ix = 0;
		wsaaT3700Ix = 0;
		wsaaT3701Ix = 0;
		wsaaT3684Ix = 0;
		wsaaT3698Sub = 0;
		t3700Sorts = new Integer[6];
		wsaaBlrqField = new String[6];
		wsaaField = new Integer[6];
		wsaaT3620ArrayInner = new WsaaT3620ArrayInner();
		wsaaT3700ArrayInner = new WsaaT3700ArrayInner();
		if ("BATCH".equals(billreqDTO.getModeInd())) {
			setupT36161100(billreqDTO.getCompany(),billreqDTO.getSacscode01());
			setupT36201200(billreqDTO.getCompany(),billreqDTO.getBillchnl());
			setupT37001300(billreqDTO.getCompany());
			setupT37011400(billreqDTO.getCompany(),billreqDTO.getTrancode());
			setupT36841500(billreqDTO.getCompany(),billreqDTO.getFacthous());
		}
		getT16882000(billreqDTO);
		if (!"".equals(t3620IO.getAccInd())) {
			setupCommonAcmvFields4100(billreqDTO);
			writeCreditAcmv4200(billreqDTO);
			writeDebitAcmv4300(billreqDTO);
		}
		if (!"".equals(t3620IO.getMediaRequired())) {
			produceBextRecords5000(billreqDTO);
			billreqDTO.setContot01(1);
			billreqDTO.setContot02(bextpf.getInstamt06());
		}
		return billreqDTO;
	}
	protected void getT37014370(BillreqDTO billreqDTO) throws IOException{
		String itemitem = billreqDTO.getTrancode().concat(billreqDTO.getCnttype());
		List<Itempf> t3701List = itempfDAO.readSmartTableByTrimItemList(billreqDTO.getCompany(), "T3701", itemitem,CommonConstants.IT_ITEMPFX);
		if(t3701List != null && t3701List.size() > 0){
			t3701IO = smartTableDataUtils.convertGenareaToPojo(t3701List.get(0).getGenareaj(),T3701.class);
		}else{
			itemitem = billreqDTO.getTrancode().concat("***");
			t3701List = itempfDAO.readSmartTableByTrimItemList(billreqDTO.getCompany(), "T3701", itemitem,CommonConstants.IT_ITEMPFX);
			if(t3701List != null && t3701List.size() > 0){
				t3701IO = smartTableDataUtils.convertGenareaToPojo(t3701List.get(0).getGenareaj(),T3701.class);
			}else{
				throw new DataNoFoundException("B5349: Item "+itemitem+"not found in Table T3701 while processing contract ");
			}
		}
	}
	protected void writeDebitAcmv4300(BillreqDTO billreqDTO) throws Exception{
		addacmvDTO = new AddacmvDTO();
		if ("BATCH".equals(billreqDTO.getModeInd())) {
			addacmvDTO.setMode("BATCH");
		}
		else {
			addacmvDTO.setMode("");
		}
		addacmvDTO.setTranseq(0002);
		addacmvDTO.setRldgpfx(t3616IO.getSubjPref());
		readT36984350(billreqDTO);
		if (!"".equals(t3620IO.getTriggerRequired())) {
			getT37014370(billreqDTO);
		}
		addacmvDTO.setRldgacct("");
		addacmvDTO.setRldgcoy("");
		if (!"".equals(t3620IO.getAgntsdets())) {
			addacmvDTO.setRldgacct(billreqDTO.getAgntnum());
			addacmvDTO.setRldgcoy(billreqDTO.getAgntcoy());
		}
		if (!"".equals(t3620IO.getDdind()) || !"".equals(t3620IO.getPayind()) || !"".equals(t3620IO.getCrcind())) {
			addacmvDTO.setRldgacct(billreqDTO.getAgntnum());
			addacmvDTO.setRldgcoy(billreqDTO.getAgntcoy());
		}
		if (!"".equals(t3620IO.getGrpind()) ) {
			addacmvDTO.setRldgacct(billreqDTO.getGrpsnum());
			addacmvDTO.setRldgacct(billreqDTO.getGrpscoy());
		}
		addacmvDTO.setContot(t3698IO.getCnttots().get(wsaaT3698Sub));
		addacmvDTO.setGlcode(t3698IO.getGlmaps().get(wsaaT3698Sub));
		addacmvDTO.setGlsign(t3698IO.getSigns().get(wsaaT3698Sub));
		addacmvDTO.setSacscode(t3620IO.getSacscode());
		addacmvDTO.setSacstyp(t3620IO.getSacstyp());
		addacmvDTO.setReconsbr(t3701IO.getReconsbr());
		addacmv.processAcmvpf(addacmvDTO);
		
		if (!"".equals(t3684IO.getAssumePaid())) {
			return;
		}
		Acagpf acgapf = new Acagpf();
	    acgapf.setRldgcoy(billreqDTO.getChdrcoy());
	    acgapf.setSacscode(billreqDTO.getSacscode02());
	    acgapf.setRldgacct(billreqDTO.getChdrnum());
	    acgapf.setOrigcurr(billreqDTO.getBillcurr());
	    acgapf.setSacstyp(billreqDTO.getSacstype02());
	    acgapf.setSacscurbal(billreqDTO.getInstamts().get(5));
	    acgapf.setRdocnum(billreqDTO.getChdrnum());
	    acgapf.setGlsign(billreqDTO.getGlsign02());
	    acagpfDAO.insertAcagpf(acgapf);
		
	}
	protected void writeCreditAcmv4200(BillreqDTO billreqDTO) throws IOException{
		if ("BATCH".equals(billreqDTO.getModeInd())) {
			addacmvDTO.setMode("BATCH");
		}
		else {
			addacmvDTO.setMode("");
		}
		addacmvDTO.setTranseq(0001);
		addacmvDTO.setRldgpfx(t3616IO.getSubjPref());
		addacmvDTO.setRldgcoy(billreqDTO.getChdrcoy());
		addacmvDTO.setRldgacct(billreqDTO.getChdrnum());
		/* IF T3620-ASSUME-PAID        NOT = SPACES                     */
		if (!"".equals(t3684IO.getAssumePaid())) {
			addacmvDTO.setContot((int)billreqDTO.getContot01());
			addacmvDTO.setGlcode(billreqDTO.getGlmap01());
			addacmvDTO.setGlsign(billreqDTO.getGlsign01());
			addacmvDTO.setSacscode(billreqDTO.getSacscode01());
			addacmvDTO.setSacstyp(billreqDTO.getSacstype01());
		}
		else {
			addacmvDTO.setContot((int)billreqDTO.getContot02());
			addacmvDTO.setGlcode(billreqDTO.getGlmap02());
			addacmvDTO.setGlsign(billreqDTO.getGlsign02());
			addacmvDTO.setSacscode(billreqDTO.getSacscode02());
			addacmvDTO.setSacstyp(billreqDTO.getSacstype02());
		}
		addacmv.processAcmvpf(addacmvDTO);
		if (!"".equals(t3684IO.getAssumePaid())) {
			return;
		}
		 Acagpf acgapf = new Acagpf();
	     acgapf.setRldgcoy(billreqDTO.getChdrcoy());
	     acgapf.setSacscode(billreqDTO.getSacscode02());
	     acgapf.setRldgacct(billreqDTO.getChdrnum());
	     acgapf.setOrigcurr(billreqDTO.getBillcurr());
	     acgapf.setSacstyp(billreqDTO.getSacstype02());
	     acgapf.setSacscurbal(billreqDTO.getInstamts().get(5));
	     acgapf.setRdocnum(billreqDTO.getChdrnum());
	     acgapf.setGlsign(billreqDTO.getGlsign02());
	     acagpfDAO.insertAcagpf(acgapf);
	}

	
	protected void statementSort4150(BillreqDTO billreqDTO) throws IOException{
		grpspf = grpspfDAO.readGrpspf(billreqDTO.getGrpscoy(), billreqDTO.getGrpsnum());
		if (grpspf == null) {
			return;
		}
		List<Itempf> t3700List = itempfDAO.readSmartTableByTrimItemList(billreqDTO.getCompany(), "T3700", grpspf.getBillseq(),CommonConstants.IT_ITEMPFX);
		if (null != t3700List && t3700List.size() > 0) {
			throw new ItemNotfoundException("Item "+grpspf.getBillseq()+" not found in Table T3700");
		}
		t3700IO = smartTableDataUtils.convertGenareaToPojo(t3700List.get(0).getGenareaj(),T3700.class);
		t3700Sorts[0] = t3700IO.getAgntsort() instanceof Integer ?  t3700IO.getAgntsort() : 0;
		t3700Sorts[1] = t3700IO.getChdrsort() instanceof Integer ?  t3700IO.getAgntsort() : 0;
		t3700Sorts[2] = t3700IO.getDuedsort() instanceof Integer ?  t3700IO.getAgntsort() : 0;
		t3700Sorts[3] = t3700IO.getMembsort() instanceof Integer ?  t3700IO.getAgntsort() : 0;
		t3700Sorts[4] = t3700IO.getPnamsort() instanceof Integer ?  t3700IO.getAgntsort() : 0;
		t3700Sorts[5] = t3700IO.getPnumsort() instanceof Integer ?  t3700IO.getAgntsort() : 0;
		if(t3700Sorts[4] != 0){
			Clntpf clntpf = clntpfDAO.getClntpf(billreqDTO.getPayrpfx(),billreqDTO.getPayrcoy(),billreqDTO.getPayrnum());
			if (clntpf == null) {
				return;
			}
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(clntpf.getSurname());
			stringVariable1.append(clntpf.getInitials());
			billreqDTO.setPayername(stringVariable1.toString());
		}
		wsaaBlrqField[1] = billreqDTO.getAgntnum();
		wsaaBlrqField[2] = billreqDTO.getChdrnum();
		wsaaBlrqField[3] = String.valueOf(billreqDTO.getDuedate());
		wsaaBlrqField[4] = billreqDTO.getMembsel();
		wsaaBlrqField[5] = billreqDTO.getPayername();
		wsaaBlrqField[6] = billreqDTO.getPayrnum();
		int wsaaTop = 5;
		for (int i = 0;i<=5;i++){
			int wsaaSortIx =  t3700Sorts[i];
			if (wsaaSortIx == 0) {
				wsaaField[wsaaTop] = 0;
			    wsaaTop--;
			}
			else {
				wsaaField[wsaaSortIx] = Integer.parseInt(wsaaBlrqField[i]);
			}
		}
		addacmvDTO.setStmtsort("0");
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(wsaaField[1]);
		stringVariable2.append(wsaaField[2]);
		stringVariable2.append(wsaaField[3]);
		stringVariable2.append(wsaaField[4]);
		stringVariable2.append(wsaaField[5]);
		stringVariable2.append(wsaaField[6]);
		addacmvDTO.setStmtsort(stringVariable2.toString());
	}

	protected void readT36984350(BillreqDTO billreqDTO) throws Exception{
		if (t3698Trancode.equals(billreqDTO.getTrancode()) && t3698Sacscode.equals(t3620IO.getSacscode())) {
			return ;
		}
		t3698Trancode = billreqDTO.getTrancode();
		t3698Sacscode = t3620IO.getSacscode();
		String itemitem = t3698Trancode.concat(t3698Sacscode);
		List<Itempf> t3698List = itempfDAO.readSmartTableByTrimItemList(billreqDTO.getCompany(), "T3698", itemitem,CommonConstants.IT_ITEMPFX);
		if(t3698List == null || t3698List.size() <= 0){
			throw new ItemNotfoundException("Item "+itemitem+" not found in Table T3698");
		}
		t3698IO = smartTableDataUtils.convertGenareaToPojo(t3698List.get(0).getGenareaj(),T3698.class);
		if (t3698IO.getSacstypes().get(wsaaT3698Sub).equals(t3620IO.getSacstyp())) {
			return ;
		}
		boolean wsaaMapFound = false;
		for (wsaaT3698Sub = 0; wsaaT3698Sub< 20|| wsaaMapFound; wsaaT3698Sub++){
			if (t3698IO.getSacstypes().get(wsaaT3698Sub).equals(t3620IO.getSacstyp())) {
				wsaaMapFound = true;
			}
		}
		if(!wsaaMapFound){
			throw new Exception();
		}
	}
	protected void getT16882000(BillreqDTO billreqDTO){
		if (billreqDTO.getTrancode() != null && lastTrancode.equals(billreqDTO.getTrancode())) {
			return ;
		}
		lastTrancode = billreqDTO.getTrancode();
		Descpf descpf = descpfDAO.getDescInfo(billreqDTO.getCompany(), "T1688", billreqDTO.getTrancode());
		if (descpf != null) {
			longdesc = descpf.getLongdesc();
		}
	}
	
	protected void setupT36841500(String coy,String itemitem) throws IOException{
		List<Itempf> t3684List = itempfDAO.readSmartTableByTrimItemList(coy, "T3684", itemitem,CommonConstants.IT_ITEMPFX);
		if (null != t3684List && t3684List.size() > 0) {
			t3684Key = new String[t3684List.size()];
			t3684AssumePaid = new String[t3684List.size()];
			for(Itempf itempf : t3684List){
				if(wsaaT3684Ix > 1000){
					throw new DataNoFoundException("B5349: Item "+itempf.getItemitem()+"not found in Table T3684 while processing contract ");
				}
				t3684IO = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(),T3684.class);
				t3684Key[wsaaT3684Ix] = itempf.getItemitem();
				t3684AssumePaid[wsaaT3684Ix] = t3684IO.getAssumePaid();
				wsaaT3684Ix++;
			}
		}
	}
	
	protected void setupT37011400(String coy,String itemitem) throws IOException{
		List<Itempf> t3701List = itempfDAO.readSmartTableByTrimItemList(coy, "T3701", itemitem,CommonConstants.IT_ITEMPFX);
		if (null != t3701List && t3701List.size() > 0) {
			t3701Reconsbr = new String[t3701List.size()];
			for(Itempf itempf : t3701List){
				if(wsaaT3701Ix > 1000){
					throw new DataNoFoundException("B5349: Item "+itempf.getItemitem()+"not found in Table T3701 while processing contract ");
				}
				t3701IO = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(),T3701.class);
				t3701Reconsbr[wsaaT3701Ix] = t3701IO.getReconsbr();
				wsaaT3701Ix++;
			}
		}
	}
	protected void setupT36161100(String coy,String itemitem) throws IOException{
		List<Itempf> t3616List = itempfDAO.readSmartTableByTrimItemList(coy, "T3616",itemitem,  CommonConstants.IT_ITEMPFX);
		if (null != t3616List && t3616List.size() > 0) {
			t3616Sacscodes = new String[t3616List.size()];
			t3616SubjPrefs = new String[t3616List.size()];
			for(Itempf itempf : t3616List){
				if(wsaaT3616Ix > 1000){
					throw new DataNoFoundException("B5349: Item "+itempf.getItemitem()+"not found in Table T3616 while processing contract ");
				}
				t3616IO = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(),T3616.class); 
				t3616Sacscodes[wsaaT3616Ix] = itempf.getItemitem();
				t3616SubjPrefs[wsaaT3616Ix] = t3616IO.getSubjPref();
				wsaaT3616Ix++;
			}
		}else {
			throw new DataNoFoundException("B5349: Item "+itemitem+"not found in Table T3616 while processing contract ");
		}
	}
	protected void setupT36201200(String coy,String itemitem) throws IOException{
		List<Itempf> t3620List = itempfDAO.readSmartTableByTrimItemList(coy, "T3620",itemitem,CommonConstants.IT_ITEMPFX);
		if(null != t3620List && t3620List.size() > 0) {
			for(Itempf itempf : t3620List){
				if(wsaaT3620Ix > 1000){
					throw new DataNoFoundException("B5349: Item "+itempf.getItemitem()+"not found in Table T3620 while processing contract ");
				}
				t3620IO = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(),T3620.class); 
				wsaaT3620ArrayInner.wsaaT3620Billchnl[wsaaT3620Ix] = itempf.getItemitem();
				wsaaT3620ArrayInner.wsaaT3620Agntsdets[wsaaT3620Ix] = t3620IO.getAgntsdets();
				wsaaT3620ArrayInner.wsaaT3620Ddind[wsaaT3620Ix] = t3620IO.getDdind();
				wsaaT3620ArrayInner.wsaaT3620Crcind[wsaaT3620Ix] = t3620IO.getCrcind();
				wsaaT3620ArrayInner.wsaaT3620Grpind[wsaaT3620Ix] = t3620IO.getGrpind();
				wsaaT3620ArrayInner.wsaaT3620MediaRequired[wsaaT3620Ix] = t3620IO.getMediaRequired();
				wsaaT3620ArrayInner.wsaaT3620Payind[wsaaT3620Ix] = t3620IO.getPayind();
				wsaaT3620ArrayInner.wsaaT3620Sacscode[wsaaT3620Ix] = t3620IO.getSacscode();
				wsaaT3620ArrayInner.wsaaT3620Sacstyp[wsaaT3620Ix] = t3620IO.getSacstyp();
				wsaaT3620ArrayInner.wsaaT3620TriggerRequired[wsaaT3620Ix] = t3620IO.getTriggerRequired();
				wsaaT3620ArrayInner.wsaaT3620AccInd[wsaaT3620Ix] = t3620IO.getAccInd();
				wsaaT3620Ix ++;
			}
		}else {
			throw new DataNoFoundException("B5349: Item "+itemitem+"not found in Table T3620 while processing contract ");
		}
	}

	protected void setupT37001300(String coy) throws IOException{

		List<Itempf> t3700List = itempfDAO.readSmartTableListByTableName(coy, "T3700",CommonConstants.IT_ITEMPFX);
		if(null != t3700List && t3700List.size() > 0) {
			for(Itempf itempf : t3700List){
				if(wsaaT3700Ix > 1000){
					throw new DataNoFoundException("B5349: Item "+itempf.getItemitem()+"not found in Table T3700 while processing contract ");
				}
				t3700IO = smartTableDataUtils.convertGenareaToPojo(itempf.getGenareaj(),T3700.class); 
				wsaaT3700ArrayInner.wsaaT3700Agntsort[wsaaT3700Ix] = t3700IO.getAgntsort();
				wsaaT3700ArrayInner.wsaaT3700Chdrsort[wsaaT3700Ix] = t3700IO.getChdrsort();
				wsaaT3700ArrayInner.wsaaT3700Duedsort[wsaaT3700Ix] = t3700IO.getDuedsort();
				wsaaT3700ArrayInner.wsaaT3700Membsort[wsaaT3700Ix] = t3700IO.getMembsort();
				wsaaT3700ArrayInner.wsaaT3700Pnamsort[wsaaT3700Ix] = t3700IO.getPnamsort();
				wsaaT3700ArrayInner.wsaaT3700Pnumsort[wsaaT3700Ix] = t3700IO.getPnumsort();
				wsaaT3700Ix++;
			}
		}else {
			throw new DataNoFoundException("B5349: Item not found in Table T3700 while processing contract ");
		}
	}
	protected void setupCommonAcmvFields4100(BillreqDTO billreqDTO) throws IOException{
		addacmvDTO.setStmtsort("");
		addacmvDTO.setTransactionDate(0);
		addacmvDTO.setTransactionTime(0);
		addacmvDTO.setBatcpfx("BA");
		addacmvDTO.setGenlcoy(billreqDTO.getCompany());
		addacmvDTO.setBatccoy(billreqDTO.getCompany());
		addacmvDTO.setBatcbrn(billreqDTO.getBranch());
		addacmvDTO.setBatcactyr(billreqDTO.getAcctyear());
		addacmvDTO.setBatcactmn(billreqDTO.getAcctmonth());
		addacmvDTO.setBatctrcde(billreqDTO.getTrancode());
		addacmvDTO.setBatcbatch(billreqDTO.getBatch());
		addacmvDTO.setRdoccoy(billreqDTO.getChdrcoy());
		addacmvDTO.setRdocpfx(billreqDTO.getChdrpfx());
		addacmvDTO.setRdocnum(billreqDTO.getChdrnum());
		addacmvDTO.setTnrefsec(billreqDTO.getChdrnum());
		addacmvDTO.setTrandesc(longdesc);
		addacmvDTO.setOrigccy(billreqDTO.getBillcurr());
		addacmvDTO.setGenlcur(billreqDTO.getBillcurr());
		addacmvDTO.setAcctccy(billreqDTO.getBillcurr());
		addacmvDTO.setCrate(new BigDecimal(1));
		addacmvDTO.setOrigamt(billreqDTO.getInstamts().get(5));
		addacmvDTO.setAcctamt(billreqDTO.getInstamts().get(5));
		addacmvDTO.setEffdate(billreqDTO.getInstfrom());
		addacmvDTO.setTranno(billreqDTO.getTranno());
		addacmvDTO.setTermid(billreqDTO.getTermid());
		addacmvDTO.setUser(billreqDTO.getUser());
		addacmvDTO.setCreddte(99999999);
		if (!"".equals(billreqDTO.getGrpsnum())) {
			statementSort4150(billreqDTO);
		}
	}
	protected void produceBextRecords5000(BillreqDTO billreqDTO){
		bextpf = new Bextpf();
		for (int i = 1 ; i <= 15; i++){
			bextpf.setInstamt(i, Double.parseDouble(billreqDTO.getInstamts().get(i-1).toString()));
		}
		bextpf.setChdrpfx(billreqDTO.getChdrpfx());
		bextpf.setChdrcoy(billreqDTO.getChdrcoy());
		bextpf.setChdrnum(billreqDTO.getChdrnum());
		bextpf.setServunit(billreqDTO.getServunit());
		bextpf.setCnttype(billreqDTO.getCnttype());
		bextpf.setCntcurr(billreqDTO.getBillcurr());
		bextpf.setOccdate(billreqDTO.getOccdate());
		bextpf.setCcdate(billreqDTO.getCcdate());
		bextpf.setPtdate(billreqDTO.getPtdate());
		bextpf.setBtdate(billreqDTO.getBtdate());
		bextpf.setBilldate(billreqDTO.getBilldate());
		bextpf.setBillchnl(billreqDTO.getBillchnl());
		bextpf.setBankcode(billreqDTO.getBankcode());
		bextpf.setInstfrom(billreqDTO.getInstfrom());
		bextpf.setInstto(billreqDTO.getInstto());
		bextpf.setInstbchnl(billreqDTO.getInstbchnl());
		bextpf.setInstcchnl(billreqDTO.getInstcchnl());
		bextpf.setInstfreq(billreqDTO.getInstfreq());
		bextpf.setInstjctl(billreqDTO.getInstjctl());
		bextpf.setGrupkey(billreqDTO.getGrpscoy().concat(billreqDTO.getGrpsnum()));
		bextpf.setMembsel(billreqDTO.getMembsel());
		bextpf.setFacthous(billreqDTO.getFacthous());
		bextpf.setBankkey(billreqDTO.getBankkey());
		bextpf.setBankacckey(billreqDTO.getBankacckey());
		bextpf.setCownpfx(billreqDTO.getCownpfx());
		bextpf.setCowncoy(billreqDTO.getCowncoy());
		bextpf.setCownnum(billreqDTO.getCownnum());
		bextpf.setPayrpfx(billreqDTO.getPayrpfx());
		bextpf.setPayrcoy(billreqDTO.getPayrcoy());
		bextpf.setPayrnum(billreqDTO.getPayrnum());
		bextpf.setCntbranch(billreqDTO.getCntbranch());
		bextpf.setAgntpfx(billreqDTO.getAgntpfx());
		bextpf.setAgntcoy(billreqDTO.getAgntcoy());
		bextpf.setAgntnum(billreqDTO.getAgntnum());
		bextpf.setPayflag(billreqDTO.getPayflag());
		bextpf.setBilflag(billreqDTO.getBilflag());
		bextpf.setOutflag(billreqDTO.getOutflag());
		bextpf.setSupflag(billreqDTO.getSupflag());
		bextpf.setBillcd(billreqDTO.getBillcd());
		bextpf.setMandref(billreqDTO.getMandref());
		bextpf.setNextdate(billreqDTO.getNextdate());
		bextpf.setSacscode(billreqDTO.getSacscode01());
		bextpf.setSacstyp(billreqDTO.getSacstype01());
		bextpf.setGlmap(billreqDTO.getGlmap01());
		bextpf.setMandstat(billreqDTO.getMandstat());
		bextpf.setDdderef(0);
		bextpf.setEffdatex(0);
		bextpf.setJobno(0);
		bextpf.setCompany(billreqDTO.getCompany());
		bextpfDAO.insertBextPF(bextpf);
}
	private static final class WsaaT3620ArrayInner { 
		private String[] wsaaT3620Billchnl = new String[30];
		private String[] wsaaT3620Agntsdets = new String[30];
		private String[] wsaaT3620Ddind = new String[30];
		private String[] wsaaT3620Crcind = new String[30];
		private String[] wsaaT3620Grpind = new String[30];
		private String[] wsaaT3620MediaRequired = new String[30];
		private String[] wsaaT3620Payind = new String[30];
		private String[] wsaaT3620Sacscode = new String[30];
		private String[] wsaaT3620Sacstyp = new String[30];
		private String[] wsaaT3620TriggerRequired = new String[30];
		private String[] wsaaT3620AccInd = new String[30];
	}
	private static final class WsaaT3700ArrayInner { 
		private int[] wsaaT3700Agntsort = new int[30];
		private int[] wsaaT3700Chdrsort = new int[30];
		private int[] wsaaT3700Duedsort = new int[30];
		private int[] wsaaT3700Membsort = new int[30];
		private int[] wsaaT3700Pnamsort = new int[30];
		private int[] wsaaT3700Pnumsort = new int[30];
	}
}
