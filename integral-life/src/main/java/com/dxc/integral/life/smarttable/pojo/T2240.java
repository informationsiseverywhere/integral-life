package com.dxc.integral.life.smarttable.pojo;

import java.util.List;
/**
 * @author wli31
 */
public class T2240 {
	private List<String> agecodes = null;
	private List<String> zagelits = null;
	public List<String> getAgecodes() {
		return agecodes;
	}
	public void setAgecodes(List<String> agecodes) {
		this.agecodes = agecodes;
	}
	public List<String> getZagelits() {
		return zagelits;
	}
	public void setZagelits(List<String> zagelits) {
		this.zagelits = zagelits;
	}
	
}
