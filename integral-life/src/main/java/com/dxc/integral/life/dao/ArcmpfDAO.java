package com.dxc.integral.life.dao;

import com.dxc.integral.life.dao.model.Arcmpf;

public interface ArcmpfDAO {
	public Arcmpf getArcmpfRecords(String filn);
}
