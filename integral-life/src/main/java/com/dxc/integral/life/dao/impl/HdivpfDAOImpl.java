package com.dxc.integral.life.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.dxc.integral.iaf.dao.impl.BaseDAOImpl;
import com.dxc.integral.life.dao.HdivpfDAO;
import com.dxc.integral.life.dao.model.Hdivpf;

/*tempHdivpfList.get(i)* 
 * HdivpfDAO implementation for database table <b>Hdivpf</b> DB operations.
 * 
 * @author yyang21
 *
 */
@Repository("hdivpfDAO")
public class HdivpfDAOImpl extends BaseDAOImpl implements HdivpfDAO {

	@Override
	public List<Hdivpf> readHdivpfRecords(String company, String contractNumber){
		StringBuilder sql = new StringBuilder("SELECT * FROM HDIVPF WHERE HDVTYP = 'C' AND CHDRCOY=? AND CHDRNUM=? ");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, HINCAPDT, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, contractNumber },
				new BeanPropertyRowMapper<Hdivpf>(Hdivpf.class));
	}
	
	@Override
	public List<Hdivpf> readHdivpfRecords(String company, String chdrnum, int tranno){
		StringBuilder sql = new StringBuilder("SELECT * FROM HDIVPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?");
		sql.append(" ORDER BY CHDRCOY, CHDRNUM, TRANNO, UNIQUE_NUMBER DESC ");

		return jdbcTemplate.query(sql.toString(), new Object[] { company, chdrnum, tranno },
				new BeanPropertyRowMapper<Hdivpf>(Hdivpf.class));
	}
	
	@Override
	public int deleteHdivpfRecord(Hdivpf hdivpf) {
		StringBuilder sql = new StringBuilder("DELETE FROM HDIVPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE AND JLIFE=? ");
		sql.append("AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND HPUANBR=? AND TRANNO=?");
		
		return jdbcTemplate.update(sql.toString(), 
				new Object[] { hdivpf.getChdrcoy(), hdivpf.getChdrnum(), hdivpf.getLife(), hdivpf.getJlife(), hdivpf.getCoverage(), hdivpf.getRider(), hdivpf.getPlnsfx(), 
						hdivpf.getHpuanbr(), hdivpf.getTranno() });
	}
	
	@Override
	public void insertHdivpfRecords(Hdivpf hdivpfList){
		List<Hdivpf> tempHdivpfList = new ArrayList();
		tempHdivpfList.add(hdivpfList);
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.HDIVPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE,");
		sb.append( " BATCBATCH, CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sb.append( "DATIME)  "); 
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempHdivpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, tempHdivpfList.get(i).getChdrcoy());
			    ps.setString(2, tempHdivpfList.get(i).getChdrnum());
			    ps.setString(3, tempHdivpfList.get(i).getLife());
			    ps.setString(4, tempHdivpfList.get(i).getJlife());			    
				ps.setString(5, tempHdivpfList.get(i).getCoverage());
				ps.setString(6, tempHdivpfList.get(i).getRider());				
			    ps.setInt(7, tempHdivpfList.get(i).getPlnsfx());
			    ps.setInt(8, tempHdivpfList.get(i).getHpuanbr()); 
				ps.setInt(9, tempHdivpfList.get(i).getTranno());				
			    ps.setString(10, tempHdivpfList.get(i).getBatccoy());		
				ps.setString(11, tempHdivpfList.get(i).getBatcbrn());		
			    ps.setInt(12, tempHdivpfList.get(i).getBatcactyr());
			    ps.setInt(13, tempHdivpfList.get(i).getBatcactmn());			    
			    ps.setString(14, tempHdivpfList.get(i).getBatctrcde());		
			    ps.setString(15, tempHdivpfList.get(i).getBatcbatch());
			    ps.setString(16, tempHdivpfList.get(i).getCntcurr());
			    ps.setInt(17, tempHdivpfList.get(i).getEffdate());
			    ps.setInt(18, tempHdivpfList.get(i).getHdvaldt());
			    ps.setString(19, tempHdivpfList.get(i).getHdvtyp());
			    ps.setBigDecimal(20, tempHdivpfList.get(i).getHdvamt());
			    ps.setBigDecimal(21, tempHdivpfList.get(i).getHdvrate());
			    ps.setInt(22, tempHdivpfList.get(i).getHdveffdt());
			    ps.setString(23, tempHdivpfList.get(i).getZdivopt());
			    ps.setString(24, tempHdivpfList.get(i).getZcshdivmth());
			    ps.setInt(25, tempHdivpfList.get(i).getHdvopttx());
			    ps.setInt(26, tempHdivpfList.get(i).getHdvcaptx());
			    ps.setInt(27, tempHdivpfList.get(i).getHincapdt());
			    ps.setInt(28, tempHdivpfList.get(i).getHdvsmtno()); 
			    ps.setString(29, tempHdivpfList.get(i).getUsrprf());
			    ps.setString(30, tempHdivpfList.get(i).getJobnm());
			    ps.setTimestamp(31, new Timestamp(System.currentTimeMillis()));		
            }  
      }); 
	}
	
	@Override
	public void insertHdivpfRecords(List<Hdivpf> hdivpfList){
		List<Hdivpf> tempHdivpfList = hdivpfList;   
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.HDIVPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, HPUANBR, TRANNO, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE,");
		sb.append( " BATCBATCH, CNTCURR, EFFDATE, HDVALDT, HDVTYP, HDVAMT, HDVRATE, HDVEFFDT, ZDIVOPT, ZCSHDIVMTH, HDVOPTTX, HDVCAPTX, HINCAPDT, HDVSMTNO, USRPRF, JOBNM, ");
		sb.append( "DATIME)  "); 
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
	    jdbcTemplate.batchUpdate(sb.toString(),new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {  
                 return tempHdivpfList.size();   
            }  
            @Override  
            public void setValues(PreparedStatement ps, int i) throws SQLException { 
            	ps.setString(1, hdivpfList.get(i).getChdrcoy());
			    ps.setString(2, hdivpfList.get(i).getChdrnum());
			    ps.setString(3, hdivpfList.get(i).getLife());
			    ps.setString(4, hdivpfList.get(i).getJlife());			    
				ps.setString(5, hdivpfList.get(i).getCoverage());
				ps.setString(6, hdivpfList.get(i).getRider());				
			    ps.setInt(7, hdivpfList.get(i).getPlnsfx());
			    ps.setInt(8, hdivpfList.get(i).getHpuanbr()); 
				ps.setInt(9, hdivpfList.get(i).getTranno());				
			    ps.setString(10, hdivpfList.get(i).getBatccoy());		
				ps.setString(11, hdivpfList.get(i).getBatcbrn());		
			    ps.setInt(12, hdivpfList.get(i).getBatcactyr());
			    ps.setInt(13, hdivpfList.get(i).getBatcactmn());			    
			    ps.setString(14, hdivpfList.get(i).getBatctrcde());		
			    ps.setString(15, hdivpfList.get(i).getBatcbatch());
			    ps.setString(16, hdivpfList.get(i).getCntcurr());
			    ps.setInt(17, hdivpfList.get(i).getEffdate());
			    ps.setInt(18, hdivpfList.get(i).getHdvaldt());
			    ps.setString(19, hdivpfList.get(i).getHdvtyp());
			    ps.setBigDecimal(20, hdivpfList.get(i).getHdvamt());
			    ps.setBigDecimal(21, hdivpfList.get(i).getHdvrate());
			    ps.setInt(22, hdivpfList.get(i).getHdveffdt());
			    ps.setString(23, hdivpfList.get(i).getZdivopt());
			    ps.setString(24, hdivpfList.get(i).getZcshdivmth());
			    ps.setInt(25, hdivpfList.get(i).getHdvopttx());
			    ps.setInt(26, hdivpfList.get(i).getHdvcaptx());
			    ps.setInt(27, hdivpfList.get(i).getHincapdt());
			    ps.setInt(28, hdivpfList.get(i).getHdvsmtno()); 
			    ps.setString(29, hdivpfList.get(i).getUsrprf());
			    ps.setString(30, hdivpfList.get(i).getJobnm());
			    ps.setTimestamp(31, new Timestamp(System.currentTimeMillis()));		
            }  
      }); 
	}
}
