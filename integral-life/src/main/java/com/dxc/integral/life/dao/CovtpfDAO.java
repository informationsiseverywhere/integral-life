/**
 * 
 */
package com.dxc.integral.life.dao;

import java.util.List;
import java.util.Map;

import com.dxc.integral.life.dao.model.Covtpf;

/**
 * @author gsaluja2
 *
 */
public interface CovtpfDAO {
	public Map<String, List<Covtpf>> searchCovtpf(int transactionDate);
}
