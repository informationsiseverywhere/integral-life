package com.csc.integral.aop.workflow.utils.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppVarsReflectUtils {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AppVarsReflectUtils.class);
	
	public static final String LOGGER_ON_USER = "loggedOnUser";
	

	private static final String APPVARS_CLASS = "com.quipoz.framework.util.AppVars";

	public static Object getAppVarsInstance() {
		Object av = null;
		try {
			Class avClass = ClassUtils.getClass(APPVARS_CLASS);
			Method mt = avClass.getMethod("getInstance");
			av = mt.invoke(null);
		} catch (Exception ex) {
			LOGGER.error("get appvars instance exception", ex);
		}
		return av;
	}

	public static int getAppVarHashCode() {
		Object av = getAppVarsInstance();
		if (av != null) {
			return av.hashCode();
		} else {
			return -1;
		}
	}

	public static Field getField(String fieldName) {
		try {
			Class avClass = ClassUtils
					.getClass("com.quipoz.framework.util.AppVars");
			return FieldUtils.getField(avClass, "loggedOnUser", true);
		} catch (Exception ex) {
			LOGGER.error("get field " + fieldName + " exception", ex);
		}
		return null;
	}

	public static Object getFieldValue(String fieldName) {
		try {
			Field f = getField(fieldName);
			Object av = getAppVarsInstance();
			if (f != null && av != null) {
				return f.get(av);
			}
		} catch (Exception ex) {
			LOGGER.error("get value of field " + fieldName + " exception", ex);
		}
		return null;
	}
}
