package com.csc.integral.aop.cache;

/**
 * A special interface for a cleaner of cache regions.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface CacheRegionCleaner {

    /**
     * Dispose this region. Flushes objects to and closes auxiliary caches. This is a shutdown
     * command for the given region.
     * 
     * @param declaredRegion
     */
    void disposeRegion(String declaredRegion);
}
