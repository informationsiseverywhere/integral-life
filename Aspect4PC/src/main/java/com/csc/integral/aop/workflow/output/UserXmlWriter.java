package com.csc.integral.aop.workflow.output;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Stack;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;


public class UserXmlWriter extends UserWorkflowWritter {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserXmlWriter.class);

	private static final String FIELD = "Field";
	private static final String PARAM = "Param";
	private static final String METHOD = "Method";
	private static final String IO_ACESS = "IOAcess";
	private static final String CLASS = "Class";
	private static final String CALL_PREF = "callPref";
	private static final String NOTE = "note";
	private static final String VALUE = "value";
	private static final String NAME = "name";
	private static final String PROGRAM = "Program";
	private static final String WORKFLOW = "Workflow";
	private static final String EXCEPTION = "Exception";
	private static final String MESSAGE = "messge";
	private static final String STACKTRACE = "StackTrace";

	private OutputStream out;
	private XMLStreamWriter writer;
	private boolean bStartProgram;
	private int nProgramClassCalled;
	private Path path;

	// number of start element
	private int nStartElement = 0;
	private int indexSequenceFile = 0;
	// method stack, keep method stack trace to format the output
	private Stack<String> methodStack = new Stack<String>();

	protected UserXmlWriter(String userId) {
		super(userId);
		init();
	}

	private void init() {
		newNextOutFile();
	}

	@Override
	public void newNextOutFile() {

		// create dir
		File outDir = null;
		String xmlDir = null;
		
		try {
			xmlDir = WorkflowCaptureCfg.getInstance().getOutputDir()
					+ File.separator + userId;
			String filePath1 = FilenameUtils.getFullPath(xmlDir); //IJTI-851
			String fileName1 = FilenameUtils.getName(xmlDir); //IJTI-851
			outDir = new File(filePath1,fileName1);
			if (!outDir.exists()) {
					outDir.mkdirs();
			}
		} catch (Exception ex) {
			LOGGER.error("create out put directory exception. ", ex);
		}
		// create XML out put file
		try {
			// close all open elements
			while (nStartElement > 0) {
				writer.writeEndElement();
				nStartElement--;
				writer.flush();
			}
			if (writer != null) {
				// close the current xml file
				cleanUp();
			}
			// create next file
			nProgramClassCalled = 0;
			// new file name
			indexSequenceFile++;
			//String xmlFile = xmlDir + File.separator + userId + "-workflow"
				//	+ indexSequenceFile + ".xml";
			path=Paths.get(xmlDir + File.separator+userId+"-workflow"+indexSequenceFile+".xml");
			// create xml writer
			out=Files.newOutputStream(path);
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			writer = factory.createXMLStreamWriter(out);
			writer = new IndentingXMLStreamWriter(writer);
			writer.writeStartDocument();
			// root element
			writer.writeStartElement(WORKFLOW);
			nStartElement++;
		} catch (IOException e) {
			LOGGER.error("init UserXmlWriter exception. {}",
					ExceptionUtils.getStackTrace(e));
			throw new RuntimeException(e);
		} catch (XMLStreamException e) {
			LOGGER.error("init UserXmlWriter exception. {}",
					ExceptionUtils.getStackTrace(e));
			throw new RuntimeException(e);
		} 
	}

	@Override
	public void writeComment(String info) {
		writeXMLComment(info);
	}

	@Override
	public void writeField(String fieldName, String value) {
		writeField("", fieldName, value);
	}

	@Override
	public void writeField(String note, String fieldName, String value) {
		synchronized (this) {
			writeXMLStartElement(FIELD);
			writeXMLAttribute(NAME, fieldName);
			if (note.length() > 0) {
				writeXMLAttribute(NOTE, note);
			}
			writeXMLAttribute(VALUE, value);
			writeXMLEndElement();
		}
	}

	@Override
	public void writeParam(String paramName, String value) {
		writeParam("", paramName, value);
	}

	@Override
	public void writeParam(String note, String paramName, String value) {
		synchronized (this) {
			writeXMLStartElement(PARAM);
			writeXMLAttribute(NAME, paramName);
			if (note.length() > 0) {
				writeXMLAttribute(NOTE, note);
			}
			writeXMLAttribute(VALUE, value);
			writeXMLEndElement();
		}
	}

	@Override
	public void startMethod(Object target, Method method, Object[] args) {
		synchronized (this) {
			// add method to stack trace to close it in end method
			methodStack.push(getMethodAsString(target, method, args));
			// start method log
			writeXMLStartElement(METHOD);
			writeXMLAttribute(NAME, getMethodAsString(target, method, args));
			/*
			 * if (target != null) { writer.writeAttribute(CLASS,
			 * target.getClass().getName()); } else {
			 * writer.writeAttribute(CLASS,
			 * method.getDeclaringClass().getName()); }
			 * writer.writeAttribute(CALL_PREF, getCallPref());
			 */
		}
	}

	@Override
	public void preEndMethod(Object target, Method method, Object[] args) {
		formatPreEndMethod(getMethodAsString(target, method, args));
	}

	@Override
	public void endMethod(Object target, Method method, Object[] args) {
		writeXMLEndElement();

	}

	@Override
	public void startProgram(String programName) {
		// separate out put file
		if (nProgramClassCalled >= config.getMaxProgramClassPerFile()) {
			this.newNextOutFile();
		}

		synchronized (this) {
			nProgramClassCalled++;
			bStartProgram = true;
			methodStack.push(programName);
			writeXMLStartElement(PROGRAM);
			writeXMLAttribute(NAME, programName);
			if (config.isPrintCallReference()) {
				writeXMLAttribute(CALL_PREF, getCallPref());
			}
		}
	}

	@Override
	public void endProgram(String programName) {
		synchronized (this) {
			writeXMLEndElement();
			bStartProgram = false;
		}
	}

	public void captureException(Throwable t, Object target, Method method,
			Object[] args) {
		synchronized (this) {
			writeXMLStartElement(EXCEPTION);
			writeXMLAttribute(NAME, t.getClass().getSimpleName());
			String className = "";
			String msg = (t.getMessage() != null) ? t.getMessage() : "";
			if (target != null) {
				className = target.getClass().getSimpleName();
			} else {
				className = method.getDeclaringClass().getSimpleName();
			}
			writeXMLAttribute(CLASS, className);
			writeXMLAttribute(MESSAGE, msg);
			// get stack trace
			StringBuilder strBuilder = new StringBuilder();
			StackTraceElement[] stacks = t.getStackTrace();
			for (int i = 0; i < stacks.length && i < MAX_STACK_TRACE_LENGHT; i++) {
				strBuilder.append(stacks[i].toString() + ", ");
			}
			writeXMLAttribute(STACKTRACE, strBuilder.toString());

			writeXMLEndElement();
		}
	}

	public void cleanUp() {
		try {
			// close others element & root element
			while (nStartElement > 0) {
				writeXMLEndElement();
			}
			writer.flush();
			writer.close();
			out.close();
		}
		//IJTI-851-Overly Broad Catch
		catch (IOException |XMLStreamException ex) {
			LOGGER.error("finalize Method Exception:",ex);
			
		} 
	}

	/**
	 * @return String with content <time> <threadName> <userId>
	 */
	private String getCallPref() {
		Date logTime = new Date();
		String threadName = Thread.currentThread().getName();
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(dateFormat.format(logTime));
		strBuilder.append(" " + threadName);
		strBuilder.append(" " + userId);
		return strBuilder.toString();
	}

	@Override
	public void preEndProgram(String programName) {
		formatPreEndMethod(programName);
	}

	/**
	 * @param mt
	 *            method
	 */
	private void formatPreEndMethod(String mt) {
		// close method which open but not be closed before
		// endMethod must associate with startMethod
		synchronized (this) {
			while (!methodStack.isEmpty()) {
				String mtInStack = methodStack.pop();
				if (mtInStack != null && mtInStack.equals(mt)) {
					// endMethod associate with startMethod
					break;
				} else {
					// method which open but not be closed
					writeXMLEndElement();
				}
			}
		}
	}

	@Override
	public void startIOExecute(Object target, Method method, Object[] args) {
		synchronized (this) {
			// add method to stack trace to close it in end method
			methodStack.push(getMethodAsString(target, method, args));
			// start method log
			writeXMLStartElement(IO_ACESS);
			writeXMLAttribute(NAME, getMethodAsString(target, method, args));
			if (target != null) {
				writeXMLAttribute(CLASS, target.getClass().getName());
			}
		}
	}

	@Override
	public void endIOExecute(Object target, Method method, Object[] args) {
		endMethod(target, method, args);
	}
	
	@Override
	protected boolean isFilter() {
		if (config.isStackTraceForProgramOnly()) {
			if (bStartProgram) {
				return false;
			}
			return super.isFilter();
		} else {
			return false;
		}
		
	}

	private void writeXMLStartElement(String localName) {
		if (isFilter()) {
			return;
		}
		try {
			synchronized (this) {
				writer.writeStartElement(localName);
				nStartElement++;
				writer.flush();
			}
		} catch (XMLStreamException e) {
			LOGGER.error("endProgram exception. {}",
					ExceptionUtils.getStackTrace(e));
		}
	}

	private void writeXMLEndElement() {
		if (isFilter()) {
			return;
		}

		try {
			synchronized (this) {
				if (nStartElement > 0) {
					writer.writeEndElement();
					nStartElement--;
				}
				writer.flush();
			}
		} catch (XMLStreamException e) {
			LOGGER.error("endProgram exception. {}",
					ExceptionUtils.getStackTrace(e));
		}
	}

	private void writeXMLAttribute(String name, String value) {
		if (isFilter()) {
			return;
		}

		try {
			writer.writeAttribute(name, value);
		} catch (XMLStreamException e) {
			LOGGER.error("writeAttribute exception. {}",
					ExceptionUtils.getStackTrace(e));
		}
	}

	private void writeXMLComment(String comment) {
		if (isFilter()) {
			return;
		}

		try {
			writer.writeComment(comment);
		} catch (XMLStreamException e) {
			LOGGER.error("writXMLComment exception. {}",
					ExceptionUtils.getStackTrace(e));
		}
	}

}
