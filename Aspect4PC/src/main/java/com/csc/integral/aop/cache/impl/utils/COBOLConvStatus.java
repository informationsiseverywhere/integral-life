package com.csc.integral.aop.cache.impl.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum COBOLConvStatus {

    /**
     * Status: OK.
     */
    OK("****");

    private String statusValue;

    private COBOLConvStatus(String status) {
        this.statusValue = status;
    }

    /**
     * Get the current value for statusValue
     * 
     * @return Returns the statusValue.
     */
    public String getStatusValue() {
        return statusValue;
    }

    /**
     * Returns a {@link COBOLConvStatus} matching a given status text.
     * 
     * @param status
     * @return
     */
    public static COBOLConvStatus valueOfStatus(String status) {
        COBOLConvStatus[] vs = values();
        for (COBOLConvStatus statuz : vs) {
            if (StringUtils.equals(statuz.statusValue, status)) {
                return statuz;
            }
        }
        return null;
    }
}
