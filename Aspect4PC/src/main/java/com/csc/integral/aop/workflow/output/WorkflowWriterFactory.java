package com.csc.integral.aop.workflow.output;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;

public abstract class WorkflowWriterFactory {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WorkflowWriterFactory.class);

	private static WorkflowWriterFactory instance = null;
	
	protected WorkflowWriterFactory() {
	}

	public static WorkflowWriterFactory getInstance() {
		
			synchronized (WorkflowWriterFactory.class) {
				if (instance == null) {
					WorkflowCaptureCfg cfg = null;
					try {
						cfg = WorkflowCaptureCfg.getInstance();
						
						Class<?> factoryClass = Class.forName(cfg
								.getOutputFactory());
						instance = (WorkflowWriterFactory)factoryClass.newInstance();
					} catch (ClassNotFoundException e) {
						LOGGER.error("Cannot load class '{}'. Error: {}",
								cfg.getOutputFactory(),
								ExceptionUtils.getStackTrace(e));
						throw new RuntimeException(e);
					} catch (Exception e2) {
						LOGGER.error("Cannot not get factory instance. {}",
								ExceptionUtils.getStackTrace(e2));
						throw new RuntimeException(e2);
					}
				}

			}
		
		return instance;
	}

	abstract public UserWorkflowWritter getWriter(String userId);
	
	abstract public void removeWriter(String userId);
}
