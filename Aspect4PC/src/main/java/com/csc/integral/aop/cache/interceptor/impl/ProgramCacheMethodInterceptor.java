package com.csc.integral.aop.cache.interceptor.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.impl.finder.FieldFinderFactory;
import com.csc.integral.aop.cache.impl.utils.COBOLConvStatus;
import com.csc.integral.utils.InvokableField;
import com.csc.integral.utils.assertion.Assert;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class ProgramCacheMethodInterceptor extends AbstractCacheableMethodInterceptor {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ProgramCacheMethodInterceptor.class);

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object findResultField(Object interceptingObject) {
        InvokableField field = FieldFinderFactory.getInstance().getCacheProgramFieldFinder()
                .findReturnField(interceptingObject);
        Assert.STATE.notNull(field, "No return field was found for program '{}' which is strange!",
            interceptingObject.getClass().getSimpleName());
        return field;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object getCacheableDataFromOriginalCall(Object interceptedObject, Object resultField,
            Object invocationResult) {
        Assert.ARGUMENTS.instanceOf(resultField, InvokableField.class);
        return InvokableField.class.cast(resultField).get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isOriginalCallCompleted(Object interceptingObject, Object invocationResult) {
        InvokableField statusField = getStatusField(interceptingObject);
        if (statusField == null) {
            // Some programs doesn't have status field.
            LOGGER.info("No status field was found for program '{}'", interceptingObject.getClass()
                    .getSimpleName());
            return true;
        }

        String statuzObject = statusField.get().toString();
        return COBOLConvStatus.valueOfStatus(statuzObject) == COBOLConvStatus.OK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object produceReturnValue(boolean cacheValid, Object interceptingObject,
            Object[] args, Object cacheObject, CacheKey cacheKey, Object resultField) {
        if (cacheValid) {

            // cache is valid
            // invoke setParams method to link parameter to object field.
            invokeMethod(interceptingObject, "setParams", new Class<?>[] { args.getClass() },
                new Object[] { args });

            setValidStatus(interceptingObject);

            // set result field
            Assert.ARGUMENTS.instanceOf(resultField, InvokableField.class);
            setFixedLengthStringData(InvokableField.class.cast(resultField).get(), cacheObject);
        }

        return null;
    }

    private InvokableField getStatusField(Object interceptingObject) {
        InvokableField statusField = FieldFinderFactory.getInstance().getCacheProgramFieldFinder()
                .findStatusField(interceptingObject);
        return statusField;
    }

    private void setValidStatus(Object interceptingObject) {
        InvokableField statusField = getStatusField(interceptingObject);
        if (statusField == null) {
            // Some programs doesn't have status field.
            return;
        }

        Object statuzObject = statusField.get();
        setFixedLengthStringData(statuzObject, COBOLConvStatus.OK.getStatusValue());
    }
}
