package com.csc.integral.aop.cache.interceptor.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.CacheManager;
import com.csc.integral.aop.cache.exception.CacheManagementException;
import com.csc.integral.aop.cache.impl.CacheConfiguration;
import com.csc.integral.aop.cache.impl.CacheManagerFactory;
import com.csc.integral.aop.cache.impl.ManualEvictionPolicyHandler;
import com.csc.integral.aop.cache.impl.aspect.ManualEvictionPolicyHandlerRegistrar;
import com.csc.integral.aop.cache.impl.keybuilder.CacheKeyBuilderFactory;
import com.csc.integral.aop.cache.impl.utils.ExternalClassConstant;
import com.csc.integral.aop.cache.interceptor.CacheableInvocation;
import com.csc.integral.aop.cache.interceptor.CacheableMethodInterceptor;
import com.csc.integral.utils.ObjectCloneUtils;
import com.csc.integral.utils.assertion.Assert;

/**
 * Base implementation of all interceptors handling object caching. This base class is responsible
 * for:
 * <ul>
 * <li>Defining the whole processing workflow for object caching. Template pattern is used to allow
 * subclassing.
 * <li>Logging method entry and exit from the system property
 * "<i>csc.integral.aop.methodInOutLogged</i>".
 * </ul>
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public abstract class AbstractCacheableMethodInterceptor implements CacheableMethodInterceptor {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AbstractCacheableMethodInterceptor.class);

    protected static final String NOTIF_OUTPUT_REGION = "output.region";
    protected static final String NOTIF_OUTPUT_CONTINUED = "output.continued";

    private static final String NOTIF_INPUT_DECLARING_TYPE_NAME = "input.declaringTypeName";
    private static final String NOTIF_INPUT_METHOD_NAME = "input.methodName";
    private static final String NOTIF_INPUT_INTERCEPTING_OBJECT = "input.interceptingObject";
    private static final String NOTIF_INPUT_INTERCEPTED_ARGS = "input.interceptedArgs";
    private static final String NOTIF_INPUT_CACHE_KEY = "input.cacheKey";

    private static boolean s_methodInOutLogged = BooleanUtils.toBoolean(System
            .getProperty("csc.integral.aop.methodInOutLogged"));

    /**
     * {@inheritDoc}
     */
    @Override
    public Object intercept(Object target, String methodName, int modifiers, Object[] args,
            Callable<Object> proceedingCallable) throws Throwable {
        CacheableInvocation invocation = new CacheableInvocation(target.getClass(), modifiers,
                methodName, target, proceedingCallable, args);
        return intercept(invocation);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object intercept(CacheableInvocation invocation) throws Throwable {
        Object interceptingObject = invocation.getTarget();
        Object[] args = invocation.getArgs();
        if (ArrayUtils.isEmpty(args)) {
            args = invocation.getArgs();
            if (args.length == 1 && args[0] instanceof Object[]) {
                args = (Object[]) args[0];
            }
        }

        logMethodEntry(invocation, args);

        // Register ManualEvictionPolicy with intercepting object
        registerManualEvictionPolicy(interceptingObject);

        try {
            Observable observerable = new Observable() {
                @Override
                public void notifyObservers(Object data) {
                    setChanged();
                    super.notifyObservers(data);
                }
            };

            // Register intercepting object as the observer of the aspect
            registerAspectObservers(observerable, interceptingObject);

            // notify observers (intercepted objects) to allow them to decide if the cache should be
            // applied. Some more information may be necessary and thus, they must be specified by
            // the observers.
            Map<String, Object> notificationMap = acquireInterceptedObjectInfo(observerable,
                interceptingObject, invocation, args);
            Boolean continued = (Boolean) notificationMap.get(NOTIF_OUTPUT_CONTINUED);
            String region = (String) notificationMap.get(NOTIF_OUTPUT_REGION);

            // proceed the original call if no caching is allowed
            if (BooleanUtils.isFalse(continued)) {
                LOGGER.debug("Caching is disabled by the intercepting object");
                return proceedOriginalCall(invocation);
            }

            // find result field
            Object resultField = findResultField(interceptingObject);

            // build cache key
            CacheKey cacheKey = buildCacheKey(interceptingObject, args, invocation);

            // if cache key cannot be built, this means caching cannot be applied to this program
            // ==> retain the original call.
            if (cacheKey == null) {
                LOGGER.warn(
                    "No cache key can be built for the object ({}) from the invocation ({})",
                    interceptingObject, invocation);
                return proceedOriginalCall(invocation);
            }

            // synchronize cache process for multiple process
            CacheManager cacheManager = CacheManagerFactory.getInstance().getCacheManager();
            // get cache object
            Object cacheObject = cacheManager.get(region, cacheKey);

            // check cache expiration or invalid
            boolean cacheValid = isCacheValid(cacheObject, cacheKey);

            // cache expired or invalid
                // Synchronize and re-read cache to avoid overload execute original call in multiple
                // thread.
                synchronized (cacheManager) {
                    cacheObject = cacheManager.get(region, cacheKey);
                    cacheValid = isCacheValid(cacheObject, cacheKey);
                    if (!cacheValid) {
                        cacheObject = executeOriginalCallAndCache(invocation, interceptingObject,
                            region, resultField, cacheKey, cacheObject);
                    }
                    else { // cache is still valid
                        cacheObject = cloneCacheObject(cacheObject);
                    }
                }
             

            Object finalResult = produceReturnValue(cacheValid, interceptingObject, args,
                cacheObject, cacheKey, resultField);
            LOGGER.info("Final return value is produced. Value={}", finalResult);

            // Notify the intercepting object about the key of the cache object. This data can
            // be
            // stored inside the object to support the removal of cache objects.
            announceCacheKey(observerable, cacheKey);

            return finalResult;

        } finally {
            logMethodExit(invocation);
        }
    }

    private Object executeOriginalCallAndCache(CacheableInvocation invocation,
            Object interceptingObject, String region, Object resultField, CacheKey cacheKey,
            Object cacheObject) throws Throwable {
        Object invocationResult = proceedOriginalCall(invocation);

        // extract the actual return value which is cacheable. This step is necessary
        // for
        // some types of caching but may not for all.
        Object cacheableData = getCacheableDataFromOriginalCall(interceptingObject, resultField,
            invocationResult);

        boolean invocationSucceeded = isOriginalCallCompleted(interceptingObject, invocationResult);

        // put cache data back to store if the original call completes successfully
        if (invocationSucceeded) {
            LOGGER.info("Invocation '{}.{}' completes SUCCESSFULLY",
                invocation.getDeclaringTypeName(), invocation.getName());
            cacheObject = putToCacheStore(region, cacheKey, cacheableData);
        } else {
            LOGGER.warn("Invocation '{}.{}' FAILS to complete", invocation.getDeclaringTypeName(),
                invocation.getName());
        }
        return cacheObject;
    }

    /**
     * Announces to all observers of this aspect about the computed key of a given cache object.
     * <p>
     * <b>Rationale</b>: There are some cases that the method where an intercepting object evicts
     * its cached data is not the same as the intecepted method. For example, business method
     * O.getData() is intercepted and some cache data can be returned. The cache key, hence, is
     * built from the method name of this method (i.e. "getData"). However, method O.clearData() is
     * the actual place to evict cache data. In this case, the intercepted method is "clearData"
     * (not "getData") which makes the computation of the cache key is wrong.
     * <p>
     * <b>Solution</b>: One solution is that the object O stores the cache key and uses that key
     * when it needs to evict cache data. This method is for this purpose. Notice that
     * {@link CacheKey} object is immutable and serializable.
     */
    protected void announceCacheKey(Observable observable, CacheKey key) {
        try {
            Map<String, Object> notifMap = new HashMap<String, Object>(1);
            notifMap.put(NOTIF_INPUT_CACHE_KEY, key);
            observable.notifyObservers(notifMap);
        } catch (Exception e) {
            LOGGER.warn("Error while sending cache key (" + key + ") to observers", e);
        }
    }

    private Object putToCacheStore(String region, CacheKey cacheKey, Object cacheableData) {
        Object realCachingData = cloneCacheObject(cacheableData);
        CacheManagerFactory.getInstance().getCacheManager().put(region, cacheKey, realCachingData);
        return realCachingData;
    }

    /**
     * Clones (deeply) a given cache object.
     * <p>
     * This method is used before putting the a cache object into the cache store and before
     * returning a VALID cache object to a client call.
     * 
     * @param cacheObj
     * @return
     */
    protected Object cloneCacheObject(Object cacheObj) {
        return ObjectCloneUtils.clone(cacheObj);
    }

    /**
     * Checks if a given original invocation completes successfully. Basically this method returns
     * <code>true</code>. However, in some cases such as when COBOL-transformed program is invoked,
     * there is no explicit completion state. Instead, there is a special field in that program
     * storing the current execution status. In those situations, this method needs overriding.
     * 
     * @param interceptingObject
     * @param invocationResult
     * @return
     */
    protected boolean isOriginalCallCompleted(Object interceptingObject, Object invocationResult) {
        return true;
    }

    protected void logMethodEntry(CacheableInvocation invocation, Object[] args) {
        if (s_methodInOutLogged && LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                "Enter method '{}.{}'. Arguments: {}",
                new Object[] {
                    invocation.getName(), invocation.getDeclaringTypeName(),
                    ArrayUtils.toString(args, "(null)") });
        }
    }

    protected void logMethodExit(CacheableInvocation invocation) {
        if (s_methodInOutLogged && LOGGER.isDebugEnabled()) {
            LOGGER.debug("Leave method '{}.{}'.", invocation.getName(),
                invocation.getDeclaringTypeName());
        }
    }

    private Object proceedOriginalCall(CacheableInvocation invocation) throws Throwable {
        LOGGER.debug("Proceeding the original call '{}.{}'", invocation.getDeclaringTypeName(),
            invocation.getName());

        Object res = null;
        Throwable error = null;
        try {
            res = invocation.proceed();
        } catch (Throwable t) {
            if (!handleOriginalCallError(invocation, t)) {
                error = t;
                throw t;
            }
        } finally {
            if (error != null) {
                LOGGER.error(
                    "The original call '{}.{}' fails to complete. Error: {}",
                    new Object[] {
                        invocation.getDeclaringTypeName(), invocation.getName(),
                        ExceptionUtils.getStackTrace(error) });
            } else {
                LOGGER.info("The original call '{}.{}' completes. Return value: {}", new Object[] {
                    invocation.getDeclaringTypeName(), invocation.getName(), res });
            }
           
        }

        return res;
    }

    protected boolean handleOriginalCallError(CacheableInvocation invocation, Throwable t) {
        return false; // no special handling is done, proceed with default handling code
    }

    /**
     * Produces a correct return value. Depending on the type of cache objects, return value can be
     * stored in different ways.
     * <p>
     * <b>Note</b>: Basically, this method is useful only for COBOL-transformed programs and
     * procedures because they store return values in their private fields. Modern code (e.g.
     * Hibernate "find" methods) no longer uses this style.
     * 
     * @param cacheValid
     *            The "VALID" status of the cache object in the store before the original invocation
     *            is proceeded.
     * @param interceptingObject
     *            The intercepting object
     * @param cacheObject
     *            Current cache object retrieved from the store.
     * @param cacheKey
     *            Cache key.
     * @param resultField
     *            The return field object.
     * @return
     */
    protected Object produceReturnValue(boolean cacheValid, Object interceptingObject,
            Object[] args, Object cacheObject, CacheKey cacheKey, Object resultField) {
        return cacheObject;
    }

    /**
     * Extracts the actual cacheable data from the current invocation return value.
     * <p>
     * In some cache object types, especially COBOL-transformed program cache, the actual return
     * value is stored in an instance variable of the program. In some other cases, however, the
     * return value is enough for caching.
     * 
     * @param interceptedObject
     * @param resultField
     * @param invocationResult
     * @return An extracted result value or the given invocation result in the argument list.
     */
    protected Object getCacheableDataFromOriginalCall(Object interceptedObject, Object resultField,
            Object invocationResult) {
        return invocationResult;
    }

    /**
     * Checks whether a cache object is still valid for use or not.
     * 
     * @param cacheObject
     * @param cacheKey
     * @return
     */
    protected boolean isCacheValid(Object cacheObject, CacheKey cacheKey) {
        // [qpham4] Default we use cache provider max life time, so general is return true.
        // If there are any special logics for cache valid, it will be implemented in subclass.
        return cacheObject != null;
    }

    /**
     * Builds a unique cache key base on given intercepted object, invocation arguments and the
     * current signature of the invocation.
     * 
     * @param interceptedObject
     * @param args
     * @param invocation
     * @return A cache key or <code>null</code> if it's not enough information to build any key.
     */
    protected CacheKey buildCacheKey(Object interceptedObject, Object[] args,
            CacheableInvocation invocation) {
        return CacheKeyBuilderFactory.getInstance().getBuilder()
                .build(interceptedObject.getClass(), invocation.getName(), args);
    }

    /**
     * Finds and stores the result field from a given invocation. This method basically is useful
     * for program cache aspect.
     * 
     * @param interceptingObject
     * @return A result field object (subclass dependent) or <code>null</code> if an aspect doesn't
     *         require any special result field (i.e. return value of the invocation is enough).
     */
    protected Object findResultField(Object interceptingObject) {
        return null;
    }

    private void registerAspectObservers(Observable observerable, Object interceptingObject) {
        if (interceptingObject instanceof Observer) {
            LOGGER.debug("Registering '{}' as an observer of the aspect", interceptingObject
                    .getClass().getName());
            observerable.addObserver((Observer) interceptingObject);
            LOGGER.info("'{}' has been registered as an observer of the aspect", interceptingObject
                    .getClass().getName());
        } else {
            LOGGER.warn("'{}' is NOT an Observer. Notifications will not be sent.",
                interceptingObject.getClass().getName());
        }
    }

    private void registerManualEvictionPolicy(Object interceptingObject) {
        LOGGER.debug("Registering '{}' as the observer of the intercepting object",
            ManualEvictionPolicyHandler.class.getSimpleName());

        ManualEvictionPolicyHandlerRegistrar.registerAsObserver(interceptingObject);

        LOGGER.info("'{}' is registered to the intercepting object",
            ManualEvictionPolicyHandler.class.getSimpleName());
    }

    protected Map<String, Object> acquireInterceptedObjectInfo(Observable observable,
            Object interceptingObject, CacheableInvocation invocation, Object[] args) {
        LOGGER.debug("Notifying intercepting object to get more information ..");

        Map<String, Object> notificationMap = new HashMap<String, Object>();

        notificationMap.put(NOTIF_INPUT_DECLARING_TYPE_NAME, invocation.getDeclaringTypeName());
        notificationMap.put(NOTIF_INPUT_METHOD_NAME, invocation.getName());
        notificationMap.put(NOTIF_INPUT_INTERCEPTING_OBJECT, interceptingObject);
        notificationMap.put(NOTIF_INPUT_INTERCEPTED_ARGS, args);

        notificationMap.put(NOTIF_OUTPUT_CONTINUED, Boolean.TRUE);
        notificationMap.put(NOTIF_OUTPUT_REGION, CacheConfiguration.DEFAULT_REGION);

        observable.notifyObservers(notificationMap);

        LOGGER.info("Information from the intercepting object: {}", notificationMap);
        return notificationMap;
    }

    protected static void setFixedLengthStringData(Object owner, Object arg) {
        Assert.ARGUMENTS.notNull(owner, "Owner object is missing");
        Assert.ARGUMENTS.isTrue(
            ExternalClassConstant.FixedLengthStringData.isAssignableFrom(owner.getClass()),
            "Owner object is not a FixedLengthStringData (class: {})", owner.getClass().getName());
        invokeMethod(owner, "set", new Class<?>[] { arg.getClass() }, new Object[] { arg });
    }

    protected static Object invokeMethod(Object owner, String methodName, Class<?>[] paramTypes,
            Object[] args) {
        try {
            Method m = MethodUtils.getAccessibleMethod(owner.getClass(), methodName, paramTypes);
            if (m != null) {
                return m.invoke(owner, args);
            }
            return null;
        } catch (IllegalAccessException e) {
            LOGGER.error("Error while accessing method '{}.{}()'", owner.getClass().getName(),
                methodName);
            throw new CacheManagementException(e);
        } catch (InvocationTargetException e) {
            LOGGER.error("Error while executing method '{}.{}()'", owner.getClass().getName(),
                methodName);
            throw new CacheManagementException(e);
        }
    }
}
