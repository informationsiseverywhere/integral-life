package com.csc.integral.aop.cache.exception;

import com.csc.integral.utils.exception.BaseRTException;

/**
 * An exception when the cache initialization process fails to complete.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheInitializationException extends BaseRTException {
    private static final long serialVersionUID = 1L;

    /**
     * @param message
     * @param messageParams
     */
    public CacheInitializationException(String message, Object... messageParams) {
        super(message, messageParams);
    }

    /**
     * @param message
     * @param cause
     * @param messageParams
     */
    public CacheInitializationException(String message, Throwable cause, Object... messageParams) {
        super(message, cause, messageParams);
    }

    /**
     * @param cause
     */
    public CacheInitializationException(Throwable cause) {
        super(cause);
    }
}
