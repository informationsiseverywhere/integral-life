package com.csc.integral.aop.cache.impl.keybuilder;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.impl.finder.FieldFinderFactory;
import com.csc.integral.aop.cache.impl.utils.ExternalClassConstant;
import com.csc.integral.utils.InvokableField;
import com.csc.integral.utils.ReflectionUtils;
import com.csc.integral.utils.assertion.Assert;

/**
 * The default implementation of {@link CacheKeyBuilder} interface.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class DefaultCacheKeyBuilder implements CacheKeyBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCacheKeyBuilder.class);

    private static final String IDENTIFIER_SEPARATOR = "_";
    private static final String METHOD_GET_IDENTIFIER = "getIdentifier";
    private static final String METHOD_GET_BASE_STRING = "getBaseString";

    /**
     * {@inheritDoc}
     */
    @Override
    public CacheKey build(Class<?> interceptingObjectType, String methodName,
            Object[] invocationArgs) {
        String identifier = computeArgsIdentifier(invocationArgs);
        String typeName = interceptingObjectType == null ? "" : interceptingObjectType.getName();
        CacheKey key = new CacheKey(typeName, methodName, StringUtils.trimToEmpty(identifier));

        LOGGER.debug("Cache key for invocation {}.{} is built. Value: {}", new Object[] {
            typeName, methodName, key });

        return key;
    }

    private String computeArgsIdentifier(Object[] invocationArgs) {
        String identifier = null;

        // The idea is:
        // If there is 1 arg & the argument is probably an ExternalData, build ID from that data
        // If not, simply build a string from arguments's hash (coz there is no glue)

        if (ArrayUtils.isEmpty(invocationArgs)) {
            identifier = "";
        } else if (invocationArgs.length == 1 && isExternalData(invocationArgs[0])) {
            identifier = computeExternalDataIdentifier(invocationArgs[0]);
        } else {
            identifier = computeGenericArgsIdentifier(invocationArgs);
        }
        return identifier;
    }

    private String computeGenericArgsIdentifier(Object[] invocationArgs) {
        String identifier;
        StringBuilder identifierBuilder = new StringBuilder();
        for (Object arg : invocationArgs) {
            if (arg == null) {
                identifierBuilder.append("(null)");
            } else if (arg.getClass().isArray()) {
                identifierBuilder.append(computeObjectArrayIdentifier((Object[]) arg));
            } else if (arg instanceof Collection<?>) {
                identifierBuilder.append(computeCollectionIdentifier((Collection<?>) arg));
            } else if (arg instanceof Map<?, ?>) {
                identifierBuilder.append(computeMapIdentifier((Map<?, ?>) arg));
            } else {
                identifierBuilder.append(computeObjectIdentifier(arg));
            }
            identifierBuilder.append(IDENTIFIER_SEPARATOR);
        }
        identifier = identifierBuilder.deleteCharAt(
            identifierBuilder.length() - IDENTIFIER_SEPARATOR.length()).toString();
        return identifier;
    }

    private String computeCollectionIdentifier(Collection<?> args) {
        StringBuilder b = new StringBuilder("C[");
        iterateToComputeIdentifier(args, b);
        return b.toString();
    }

    private String computeMapIdentifier(Map<?, ?> args) {
        StringBuilder b = new StringBuilder("M[");
        iterateToComputeIdentifier(args.values(), b);
        return b.toString();
    }

    private String computeObjectArrayIdentifier(Object[] args) {
        StringBuilder b = new StringBuilder("A[");
        for (Object arg : args) {
            // [Ngoc Truong] Jan 19, 2012: This method only handles 1 level. This means, arrays or
            // collection/array/map will not be supported.
            b.append(computeObjectIdentifier(arg)).append(IDENTIFIER_SEPARATOR);
        }
        b.append("]");
        return b.toString();
    }

    private void iterateToComputeIdentifier(Collection<?> args, StringBuilder b) {
        for (Object arg : args) {
            // [Ngoc Truong] Jan 19, 2012: This method only handles 1 level. This means, arrays or
            // collection/array/map will not be supported.
            b.append(computeObjectIdentifier(arg)).append(IDENTIFIER_SEPARATOR);
        }
        b.append("]");
    }

    /**
     * Computes the identitifer of a given argument object. This method can be overriden to allow
     * specific extensions.
     * 
     * @param arg
     * @return A not-<code>null</code> String object.
     */
    protected String computeObjectIdentifier(Object arg) {
        if (arg == null) {
            return "(null)";
        }

        int hashCode;
        if (isFixedLengthStringData(arg)) {
            hashCode = StringUtils.trimToEmpty(arg.toString()).hashCode();
        } else {
            hashCode = arg.hashCode();
        }
        return Integer.toHexString(hashCode);
    }

    private String computeExternalDataIdentifier(Object extDataArg) {
        String identifier = null;
        InvokableField field = null;

        if (extDataArg != null) {

            // if the ExternalData object contains a public method getIdentifier(), use it first
            identifier = getUniqueIdentifier(extDataArg);

            // if not, then read from the main configuration about the field to get external data to
            // build identifier
            if (identifier == null) {
                field = FieldFinderFactory.getInstance().getCacheProgramFieldFinder()
                        .findIdentifierField(extDataArg);
                identifier = invokeIdentifierGetterField(field);
            }
            
            // if not, then get from pulbic method getBaseString()
            if (identifier == null) {
                identifier = getIdentifierFromBaseString(extDataArg);
            }

            // if not, then "guess" the field
            if (identifier == null) {
                field = guessExternalDataIdentifierField(extDataArg);
                identifier = invokeIdentifierGetterField(field);
            }
        }

        return identifier;
    }

    private String invokeIdentifierGetterField(InvokableField field) {
        String identifier = null;
        if (field != null) {
            identifier = field.makeAccessible().get(String.class);
        }
        return identifier;
    }

    private String getIdentifierFromBaseString(Object object) {
        Assert.ARGUMENTS.notNull(object, "Object to get ID is missing");

        try {
            Object id = MethodUtils.invokeMethod(object, METHOD_GET_BASE_STRING, null);
            return StringUtils.deleteWhitespace(ObjectUtils.toString(id, null));
        } catch (NoSuchMethodException e) {
            LOGGER.warn("Method '{}.{}' was not found", object.getClass().getName(),
                METHOD_GET_BASE_STRING);
        } catch (IllegalAccessException e) {
            LOGGER.warn("Method '{}.{}' is not accessible", object.getClass().getName(),
                METHOD_GET_BASE_STRING);
        } catch (InvocationTargetException e) {
            LOGGER.warn("Error while running method '{}.{}'", object.getClass().getName(),
                METHOD_GET_BASE_STRING);
        }

        return null;
    }

    private InvokableField guessExternalDataIdentifierField(Object extDataArg) {
        InvokableField field = null;

        // "guess" that the field is "{externalDataClassName}Rec" and look for this
        // field to get identifier
        String fieldPath = StringUtils.replaceOnce(
            StringUtils.uncapitalize(extDataArg.getClass().getSimpleName()), "rec", "Rec");
        field = ReflectionUtils.lookupField(extDataArg, fieldPath);

        // if not, "guess" that the field is "{externalDataClassName}rec" and look for this
        // field to get identifier
        if (field == null) {
            fieldPath = StringUtils.uncapitalize(extDataArg.getClass().getSimpleName());
            field = ReflectionUtils.lookupField(extDataArg, fieldPath);
        }

        // if not, "guess" that the field is "rec" and look for this field to get identifier
        if (field == null) {
            field = ReflectionUtils.lookupField(extDataArg, "rec");
        }
        return field;
    }

    private boolean isExternalData(Object input) {
        return ExternalClassConstant.ExternalData.isAssignableFrom(input.getClass());
    }

    private boolean isFixedLengthStringData(Object input) {
        return ExternalClassConstant.FixedLengthStringData.isAssignableFrom(input.getClass());
    }

    private String getUniqueIdentifier(Object object) {
        Assert.ARGUMENTS.notNull(object, "Object to get ID is missing");

        try {
            Object id = MethodUtils.invokeMethod(object, METHOD_GET_IDENTIFIER, null);
            return ObjectUtils.toString(id, null);
        } catch (NoSuchMethodException e) {
            LOGGER.warn("Method '{}.{}' was not found", object.getClass().getName(),
                METHOD_GET_IDENTIFIER);
        } catch (IllegalAccessException e) {
            LOGGER.warn("Method '{}.{}' is not accessible", object.getClass().getName(),
                METHOD_GET_IDENTIFIER);
        } catch (InvocationTargetException e) {
            LOGGER.warn("Error while running method '{}.{}'", object.getClass().getName(),
                METHOD_GET_IDENTIFIER);
        }

        return null;
    }
}
