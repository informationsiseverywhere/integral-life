package com.csc.integral.aop.workflow.processor;

import java.lang.reflect.Field;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;
import com.csc.integral.aop.workflow.output.UserWorkflowWritter;
import com.csc.integral.utils.ReflectionUtils;

public class OnlineProgramCaptureProcessor extends AbstractWorkflowProcessor{

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OnlineProgramCaptureProcessor.class);
	
	private static final String CODEMODEL_CLASS = "com.quipoz.framework.util.CodeModel";
	private static final String SCREENMODEL_CLASS = "com.quipoz.framework.screenmodel.ScreenModel";
	private static final String SMARTVARMODEL_CLASS = "com.csc.smart400framework.SmartVarModel";
	private static final String BASEMODEL_CLASS = "com.quipoz.framework.util.BaseModel";
	
	@Override
	public void processBeforeMethod(ProceedingJoinPoint pjp) {
		
	
		ProcessorUtils.getUserWorkflowWritter().startProgram(pjp.getTarget().getClass().getName());
		//wr.startMethod(pjp.getTarget(), getJoinPointMethod(pjp), pjp.getArgs());
		WorkflowCaptureCfg cfg = WorkflowCaptureCfg.getInstance();
		if (cfg.isPrintScreenVar()) {
			String note = "Before " + ProcessorUtils.getTargetSimpleClassName(pjp) 
					+ "." + ProcessorUtils.getJoinPointMethod(pjp).getName();
			exploreScreenModel(pjp, note);
			exploreScreenvars(pjp, note);
		}
	}

	@Override
	public void processAfterMethod(ProceedingJoinPoint pjp) {
		

		 ProcessorUtils.getUserWorkflowWritter().preEndProgram(pjp.getTarget().getClass().getName());
		
		//swr.endMethod(pjp.getTarget(), getJoinPointMethod(pjp), pjp.getArgs());
		WorkflowCaptureCfg cfg = WorkflowCaptureCfg.getInstance();
		if (cfg.isPrintScreenVar()) {
			String note = "After " + ProcessorUtils.getTargetSimpleClassName(pjp) 
					+ "." + ProcessorUtils.getJoinPointMethod(pjp).getName();
			exploreScreenModel(pjp, note);
			exploreScreenvars(pjp, note);
		}
		
		 ProcessorUtils.getUserWorkflowWritter().endProgram(pjp.getTarget().getClass().getName());
	}
	
	private void exploreScreenModel(ProceedingJoinPoint pjp, String note) {
	
		//Class<?> cmClass = ReflectionUtils.resolveClass(CODEMODEL_CLASS);
		Object screenModel = getScreenModelInBaseModel(pjp.getTarget());
		if (screenModel != null) {
			Class<?> smClass = ReflectionUtils.resolveClass(SCREENMODEL_CLASS);
			Object pageTitle = ReflectionUtils.getFieldValue(smClass, screenModel, "pageTitle");
			Object screenName = ReflectionUtils.getFieldValue(smClass, screenModel, "screenName");
			Object action = ReflectionUtils.getFieldValue(smClass, screenModel, "action");
			if (pageTitle != null) {
				String title = pageTitle.toString(); //eng/Welcome to INTEGRAL Admin;chi/???? INTEGRAL Admin;				
				// get title in english
				String[] strs = title.split(";");
				if(strs.length > 0) {
					title = strs[0]; //eng/Welcome to INTEGRAL Admin
					strs = title.split("/");
					if(strs.length > 1) {
						title = strs[1]; //Welcome to INTEGRAL Admin
					}
				}
				 ProcessorUtils.getUserWorkflowWritter().writeParam(note, "pageTitle", title);
			}
			if (screenName != null) {
				 ProcessorUtils.getUserWorkflowWritter().writeParam(note, "screenName", screenName.toString());
			}
			if (action != null) {
				 ProcessorUtils.getUserWorkflowWritter().writeParam(note, "action", action.toString());
			}
		}
	}
	
	private void exploreScreenvars(ProceedingJoinPoint pjp, String note) {
		UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();
		// get screen model from base model
		Object screenModel = getScreenModelInBaseModel(pjp.getTarget());		
		if (screenModel != null) {
			Class<?> smClass = ReflectionUtils.resolveClass(SCREENMODEL_CLASS);
			// get varModel from screenModel, varModel is SXXXXcreenVars.java
			Object varModel = ReflectionUtils.getFieldValue(smClass, screenModel, "programVars");
			if(varModel != null) {
				//explore screen fields and print it
				// first, get screenFileds from ScreenVar
				Class<?> svClass = ReflectionUtils
						.resolveClass(SMARTVARMODEL_CLASS);
				Object screenFieldsValue = ReflectionUtils.getFieldValue(svClass,
						varModel, "screenFields");
				//second, get all fields in ScreenVar
				Field[] svFields = ReflectionUtils.resolveClass(
						varModel.getClass().getName()).getDeclaredFields();
				Object[] svFiledValues = getAllFieldValueInScreenVar(varModel);
				//finally, print all fields in array screenFileds with field's name
				if (screenFieldsValue != null && svFiledValues!= null) {
					Object[] focusObjs = (Object[]) screenFieldsValue;
					StringBuilder strBuilder = new StringBuilder();
					for (int i = 0; i < focusObjs.length; i++) {
						String focusObjName = "screenFields[" + i + "]";
						// find screen Field name
						for (int j = 0; j < svFiledValues.length; j ++) {
							if (focusObjs[i] == svFiledValues[j]) {
								// the same pointer, so focusObjs[i] is associated with svFields[j]
								focusObjName = svFields[j].getName();
							}
						}
						// print screenFields[i]
						strBuilder.append(focusObjName);
						strBuilder.append("='" + focusObjs[i].toString() + "', ");
					}
					wr.writeField(note, "screenFields", strBuilder.toString());
				}
			}
		}
	}
	
	/**
	 * get all field value in ScreenVar. 
	 * @param screenVar
	 * @return
	 */
	private Object[] getAllFieldValueInScreenVar(Object screenVar) {
		try {
			Class<?> svClass = ReflectionUtils
					.resolveClass(screenVar.getClass().getName());
			Field[] svFields = svClass.getDeclaredFields();
			Object[] returnObjs = new Object[svFields.length];
			for (int i = 0; i < svFields.length; i++) {
				returnObjs[i] = svFields[i].get(screenVar);
			}
			return returnObjs;
		} catch ( IllegalAccessException iex) {
			LOGGER.error("getAllFieldValueInScreenVar exception. {}"
					+ ExceptionUtils.getStackTrace(iex));
		} 
		return null;
	}
	
	private Object getBaseModel(Object target) {
		//get BaseModel file in CodeModel
		Class<?> cmClass = ReflectionUtils.resolveClass(CODEMODEL_CLASS);
		return ReflectionUtils.getFieldValue(cmClass, target, "bM");
	}
		
	
	private Object getScreenModelInBaseModel(Object target) {
		Object screenModel = null;
		Object bm = getBaseModel(target);		
		if (bm != null) {
			Class<?> bmClass = ReflectionUtils.resolveClass(BASEMODEL_CLASS);
			screenModel = ReflectionUtils.getFieldValue(bmClass, bm, "screenModel");
		}
		return screenModel;
	}
}
