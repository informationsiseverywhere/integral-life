/**
 * 
 */
package com.csc.integral.aop.cache.impl.aspect;

import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log all input and output Parameter of COBOL program.
 * 
 * @author qpham4
 */
@Aspect
public class MethodInOutLogggingAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodInOutLogggingAspect.class);

    /**
     * Intercepts around any invocation from a method <code>mainline</code> and intercepting object
     * is an instance of <code>COBOLConvCodeModel</code>.
     * 
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* mainline(..))", argNames = "pjp")
    @Deprecated
    public Object interceptMainlineMethod(final ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs();
        if (args != null && args.length >= 1) {
            if (args.length == 1 && args[0] instanceof Object[]) {
                args = (Object[]) args[0];
            }
        }

        logMethodEntry(pjp, args, true, null);
        Object returnValue = null;

        try {
            returnValue = pjp.proceed();
            return returnValue;
        } finally {
            logMethodEntry(pjp, args, false, returnValue);
        }
    }

    /**
     * Logging
     * "Method is executed:\t{Method}\t{Input/Output} arguments:\t'[...]'\treturn value: [...]"
     * 
     * @param invocation
     * @param args
     * @param inputOrOutput
     */
    private void logMethodEntry(ProceedingJoinPoint invocation, Object[] args, boolean isInput,
            Object returnValue) {
        String inputOrOutput = isInput ? "Input" : "Output";

        LOGGER.debug(
            "Method is executed:\t{}\t{} arguments:\t{}\treturn value:\t[{}]",
            new Object[] {
                invocation.getSignature().toLongString(), inputOrOutput,
                ArrayUtils.toString(args, "(null)"), returnValue });
    }
}
