package com.csc.integral.aop.cache.impl.utils;

import org.apache.commons.lang3.StringUtils;

import com.csc.integral.aop.cache.CacheManager;
import com.csc.integral.aop.cache.impl.AbstractCacheManager;
import com.csc.integral.aop.cache.impl.CacheContext;
import com.csc.integral.aop.cache.impl.CacheManagerFactory;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class CacheManagerUtils {

    private CacheManagerUtils() {
        // to hide
    }

    /**
     * Returns the {@link CacheContext} of the current {@link CacheManager} (received from the
     * {@link CacheManagerFactory}).
     * 
     * @return A {@link CacheContext} instance or <code>null</code> if current {@link CacheManager}
     *         is not an {@link AbstractCacheManager}.
     */
    public static CacheContext getCurrentCacheContext() {
        CacheManager cm = CacheManagerFactory.getInstance().getCacheManager();
        if (cm instanceof AbstractCacheManager) {
            return ((AbstractCacheManager) cm).getContext();
        }
        return null;
    }

    /**
     * Returns the actual session-region from a given declared region. This method assumes that the
     * given region is a session-scoped one.
     * 
     * @param declaredSessionRegion
     * @return
     */
    public static String getActualSessionRegion(String declaredSessionRegion) {
        return CacheContext.getCurrentHttpSessionId() + "$" + declaredSessionRegion;
    }

    /**
     * Returns the declared session-region from a given actual region.
     * 
     * @param actualSessionRegion
     * @return
     */
    public static String getDeclaredSessionRegion(String actualSessionRegion) {
        return StringUtils.substringAfter(actualSessionRegion, "$");
    }
}
