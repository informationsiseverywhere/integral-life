package com.csc.integral.aop.cache.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.csc.integral.aop.cache.CacheManager;

/**
 * A POJO containing all the configurations of a {@link CacheManager}. This object is loaded once
 * when its owner cache manager is initialized.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheConfiguration {

    /**
     * Default region name.
     */
    public static final String DEFAULT_REGION = "default";

    private static final CacheRegionConfiguration DEFAULT_REGION_CONFIG = new CacheRegionConfiguration(
            DEFAULT_REGION);

    private CacheProvider cacheProvider;
    private Set<CacheRegionConfiguration> regionConfigs;

    CacheConfiguration() {
        cacheProvider = CacheProvider.JCS;
        regionConfigs = new HashSet<CacheRegionConfiguration>();
        regionConfigs.add(DEFAULT_REGION_CONFIG);
    }

    void setCacheProvider(CacheProvider cacheProvider) {
        this.cacheProvider = cacheProvider;
    }

    /**
     * Returns current cache provider.
     * 
     * @return
     */
    public CacheProvider getCacheProvider() {
        return cacheProvider;
    }

    /**
     * Returns a region configuration by a declared region name.
     * 
     * @param declaredRegion
     * @return
     */
    public CacheRegionConfiguration getRegionConfiguration(String declaredRegion) {
        if (StringUtils.isEmpty(declaredRegion)) {
            return null;
        } else if (DEFAULT_REGION.equalsIgnoreCase(declaredRegion)) {
            return DEFAULT_REGION_CONFIG;
        } else {
            for (CacheRegionConfiguration conf : regionConfigs) {
                if (StringUtils.equalsIgnoreCase(declaredRegion, conf.getDeclaredRegion())) {
                    return conf;
                }
            }
            return null;
        }
    }

    /**
     * Checks if a region exists.
     * 
     * @param declaredRegion
     * @return <code>true</code> if a region is the default one or exists in current registered
     *         list. Otherwise, return <code>false</code>.
     */
    public boolean regionExists(String declaredRegion) {
        return getRegionConfiguration(declaredRegion) != null;
    }

    /**
     * Returns current configurations for all regions.
     * 
     * @return
     */
    public Set<CacheRegionConfiguration> getRegionConfigs() {
        return regionConfigs;
    }

    /**
     * Returns all regions' names.
     * <p>
     * <b>Note</b>: Each invocation of this method returns a new array.
     * 
     * @return
     */
    public final String[] getDeclaredRegionNames() {
        List<String> results = new ArrayList<String>(regionConfigs.size());
        for (CacheRegionConfiguration conf : regionConfigs) {
            results.add(conf.getDeclaredRegion());
        }
        return results.toArray(new String[regionConfigs.size()]);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
