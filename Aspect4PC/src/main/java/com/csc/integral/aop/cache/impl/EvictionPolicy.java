package com.csc.integral.aop.cache.impl;

/**
 * All supported eviction policies for a cache region.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum EvictionPolicy {

    /**
     * LRU (Least Recently Used).
     */
    LRU,

    /**
     * MRU (Most Recently Used).
     */
    MRU,

    /**
     * FIFO (First In First Out).
     */
    FIFO
}
