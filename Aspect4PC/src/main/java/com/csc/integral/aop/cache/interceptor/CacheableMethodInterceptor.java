package com.csc.integral.aop.cache.interceptor;

import java.util.concurrent.Callable;

/**
 * Interface for an interceptor which handles a cacheable method invocation.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface CacheableMethodInterceptor {

    /**
     * Intercepts a given invocation to add caching functionality.
     * 
     * @param target
     * @param methodName
     * @param modifiers
     * @param args
     * @param proceedingCallable
     * @return
     * @throws Throwable
     *             if there is any throwable error.
     */
    Object intercept(Object target, String methodName, int modifiers, Object[] args,
            Callable<Object> proceedingCallable) throws Throwable;

    /**
     * Intercepts a given invocation to add caching functionality.
     * 
     * @param invocation
     *            Invocation information. Required.
     * @return
     * @throws Throwable
     *             if there is any throwable error.
     */
    Object intercept(CacheableInvocation invocation) throws Throwable;
}
