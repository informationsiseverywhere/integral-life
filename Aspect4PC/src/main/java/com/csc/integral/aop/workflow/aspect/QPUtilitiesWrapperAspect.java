package com.csc.integral.aop.workflow.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author tphan25
 */
@Aspect
public class QPUtilitiesWrapperAspect {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(QPUtilitiesWrapperAspect.class);
	
}
