package com.csc.integral.aop.performance.perf4j;

import org.apache.commons.lang3.BooleanUtils;
import org.aspectj.lang.annotation.Aspect;
import org.perf4j.LoggingStopWatch;
import org.perf4j.aop.AbstractJoinPoint;
import org.perf4j.aop.AbstractTimingAspect;
import org.perf4j.aop.Profiled;
import org.perf4j.slf4j.Slf4JStopWatch;
import org.slf4j.LoggerFactory;

import com.csc.integral.utils.assertion.Assert;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
@Aspect
public abstract class AbstractIntegralTimingAspect extends AbstractTimingAspect {
    static final String ASPECT_EXPR_ALL_EXEC = "execution(* *(..)) ";
    static final String ASPECT_EXPR_ANNOTATED = "@annotation(org.perf4j.aop.Profiled) ";
    static final String ASPECT_EXPR_CALLABLEP = "target(com.quipoz.framework.util.CallableProgram) ";

    static final String ASPECT_EXPR_NOT_ANNOTATED = ASPECT_EXPR_ALL_EXEC + "&& !("
            + ASPECT_EXPR_ANNOTATED + ") " + "&& " + ASPECT_EXPR_CALLABLEP;

    private static volatile String s_profilingPattern;
    private static volatile Boolean s_profilingMethodArgsIncluded;

    /**
     * {@inheritDoc}
     */
    @Override
    protected LoggingStopWatch newStopWatch(String loggerName, String levelName) {
        return new Slf4JStopWatch(LoggerFactory.getLogger(loggerName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getStopWatchTag(Profiled profiled, AbstractJoinPoint joinPoint,
            Object returnValue, Throwable exceptionThrown) {
        String tag = null;

        if (Profiled.DEFAULT_TAG_NAME.equals(profiled.tag())) {
            Assert.ARGUMENTS.notNull(joinPoint, "[joinPoint] is missing in thread {0} ({1})",
                Thread.currentThread().getId(), Thread.currentThread().getName());
            tag = getCurrentPattern()
                    .createTag(joinPoint.getExecutingObject(), joinPoint.getMethodName(),
                        joinPoint.getParameters(), s_profilingMethodArgsIncluded);
        }

        if (tag == null) {
            tag = super.getStopWatchTag(profiled, joinPoint, returnValue, exceptionThrown);
        }

        return tag == null ? "(null)" : tag;
    }

    static ProfiledTagPattern getCurrentPattern() {
        // [Ngoc Truong] Feb 11, 2012: System property [csc.integral.profiling.pattern] can be used
        // to customize the profiling pattern
        if (s_profilingPattern == null) {
            s_profilingPattern = System.getProperty("csc.integral.profiling.pattern");
        }

        // [Ngoc Truong] Feb 11, 2012: System property [csc.integral.profiling.argsIncl] can be used
        // to include/exclude method argument types in the log lines
        if (s_profilingMethodArgsIncluded == null) {
            s_profilingMethodArgsIncluded = BooleanUtils.toBoolean(System
                    .getProperty("csc.integral.profiling.argsIncl"));
        }

        ProfiledTagPattern pattern = ProfiledTagPattern.valueOfOrNull(s_profilingPattern);
        return pattern == null ? ProfiledTagPattern.DEFAULT : pattern;
    }
}
