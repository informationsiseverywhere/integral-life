package com.csc.integral.aop.cache.interceptor;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;

import com.csc.integral.utils.assertion.Assert;

/**
 * An invocation for a cacheable method interceptor. This class is immutable for better
 * serialization.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheableInvocation implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Class<?> declaringType;
    private final String declaringTypeName;
    private final int modifiers;
    private final String name;

    private final WeakReference<Object> target;
    private final WeakReference<Object[]> args;

    private Callable<Object> proceedingCallable;

    /**
     * Constructs an invocation object with all necessary information.
     * 
     * @param declaringType
     * @param modifiers
     * @param name
     * @param target
     * @param proceedingCallable
     *            A callable command which proceeds the current intercepting method. Required.
     * @param args
     */
    public CacheableInvocation(Class<?> declaringType, int modifiers, String name, Object target,
                               Callable<Object> proceedingCallable, Object[] args) {
        Assert.ARGUMENTS.notNull(declaringType, "'declaringType' is missing");
        Assert.ARGUMENTS.notNull(name, "'name' is missing");
        Assert.ARGUMENTS.notNull(target, "'target' is missing");
        Assert.ARGUMENTS.notNull(proceedingCallable, "'proceedingCallable' is missing");

        // mandatory fields

        this.declaringType = declaringType;
        this.modifiers = modifiers;
        this.name = name;
        this.proceedingCallable = proceedingCallable;

        // optional fields

        this.target = target != null ? new WeakReference<Object>(target) : null;
        this.args = args != null ? new WeakReference<Object[]>(args) : null;
        if (declaringType != null) {
            this.declaringTypeName = declaringType.getName();
        } else {
            this.declaringTypeName = null;
        }
    }

    /**
     * Get the current value for declaringType
     * 
     * @return Returns the declaringType.
     */
    public Class<?> getDeclaringType() {
        return declaringType;
    }

    /**
     * Get the current value for declaringTypeName
     * 
     * @return Returns the declaringTypeName.
     */
    public String getDeclaringTypeName() {
        return declaringTypeName;
    }

    /**
     * Get the current value for modifiers
     * 
     * @return Returns the modifiers.
     */
    public int getModifiers() {
        return modifiers;
    }

    /**
     * Get the current value for name
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the current value for target
     * 
     * @return Returns the target.
     */
    public Object getTarget() {
        return (target != null && target.get() != null) ? target.get() : null;
    }

    /**
     * Get the current value for args
     * 
     * @return Returns the args.
     */
    public Object[] getArgs() {
        return (args != null && args.get() != null) ? args.get() : null;
    }

    /**
     * Proceeds the current invocation.
     * 
     * @return
     * @throws Exception 
     * @throws Throwable
     */
 
    public Object proceed() throws Exception   {   //IJTI-851
        return this.proceedingCallable.call();
    }
}
