package com.csc.integral.aop.cache.impl;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.impl.keybuilder.CacheKeyBuilderFactory;
import com.csc.integral.utils.assertion.Assert;

/**
 * A special, singleton handler for "manual eviction policy" feature in the caching system. This
 * handler is an observer which listens to notifications from client applications to trigger an
 * explicit clearance of cache objects.
 * <p>
 * The scope of this handler is same as that of the {@link CacheManagerFactory}.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class ManualEvictionPolicyHandler implements Observer {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManualEvictionPolicyHandler.class);

    private static final String NOTIFIED_ARG_AFFECTED_ARGS = "affectedArgs";
    private static final String NOTIFIED_ARG_AFFECTED_METHOD_NAME = "affectedMethodName";
    private static final String NOTIFIED_ARG_AFFECTED_CLASS = "affectedClass";
    private static final String NOTIFIED_ARG_AFFECTED_REGION = "affectedRegion";
    private static final String NOTIFIED_ARG_CACHE_KEY = "cacheKey";

    private static ManualEvictionPolicyHandler s_instance = new ManualEvictionPolicyHandler();

    private ManualEvictionPolicyHandler() {
        // to hide
    }

    /**
     * Returns current instance of this handler. This instance shares the same scope as that of the
     * {@link CacheManagerFactory}.
     * 
     * @return the current instance.
     */
    public static ManualEvictionPolicyHandler getInstance() {
        return s_instance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void update(Observable o, Object arg) {
        Assert.ARGUMENTS.notNull(arg, "No notification argument is provided");
        Assert.ARGUMENTS.instanceOf(arg, Map.class, "The argument is not a Map");

        @SuppressWarnings("unchecked") Map<String, Object> argMap = (Map<String, Object>) arg;
        evictCache(argMap);
    }

    private void evictCache(Map<String, Object> argMap) {
        String region = (String) argMap.get(NOTIFIED_ARG_AFFECTED_REGION);
        CacheKey key = buildCacheKey(argMap);
        if (key != null) {
            if (key.isCompleted()) {
                CacheManagerFactory.getInstance().getCacheManager().remove(region, key);
                LOGGER.info("Cache object under key ({}) is removed from region '{}'", key, region);
            } else {
                int removedCount = CacheManagerFactory.getInstance().getCacheManager()
                        .removeAll(region, key);
                LOGGER.info("{} cache objects with partial key ({}) are removed from region '{}'",
                    new Object[] { removedCount, key, region });
            }
        } else {
            LOGGER.warn("Cache key is NULL. The key cannot be built from notification map ({}).",
                argMap);
        }
    }

    /**
     * Builds a cache key from a given notification map. This map must contain the following
     * information:
     * <ul>
     * <li>(Optional) The affected class name. Key {@link #NOTIFIED_ARG_AFFECTED_CLASS} is used
     * here.
     * <li>(Optional) The affected method name. Key {@link #NOTIFIED_ARG_AFFECTED_METHOD_NAME} is
     * used here.
     * <li>(Optional) The affected method arguments. Key {@link #NOTIFIED_ARG_AFFECTED_ARGS} is used
     * here.
     * <li>(Optional) The removing cache key (notified by the cache aspect when a intercepted method
     * is invoked). Key {@link #NOTIFIED_ARG_CACHE_KEY} is used here.
     * </ul>
     * 
     * @param argMap
     * @return
     */
    protected CacheKey buildCacheKey(Map<String, Object> argMap) {
        CacheKey key = (CacheKey) argMap.get(NOTIFIED_ARG_CACHE_KEY);
        if (key == null) {
            Class<?> affectedClass = (Class<?>) argMap.get(NOTIFIED_ARG_AFFECTED_CLASS);
            String methodName = (String) argMap.get(NOTIFIED_ARG_AFFECTED_METHOD_NAME);
            Object[] cacheObjectArgs = (Object[]) argMap.get(NOTIFIED_ARG_AFFECTED_ARGS);

            key = CacheKeyBuilderFactory.getInstance().getBuilder()
                    .build(affectedClass, methodName, cacheObjectArgs);
        }
        return key;
    }
}
