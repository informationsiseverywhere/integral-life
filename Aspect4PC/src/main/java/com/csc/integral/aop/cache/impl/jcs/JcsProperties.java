/**
 * 
 */
package com.csc.integral.aop.cache.impl.jcs;

/**
 * @author qpham4
 *
 */
public class JcsProperties {
    public static final String DEFAULT_REGION = "jcs.default";
    public static final String LRU = "org.apache.jcs.engine.memory.lru.LRUMemoryCache";
    public static final String MRU = "org.apache.jcs.engine.memory.mru.MRUMemoryCache";

}
