package com.csc.integral.aop.performance.perf4j;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.perf4j.aop.Profiled;

import com.csc.integral.aop.performance.perf4j.annotation.ProfiledLiteral;

/**
 * An extended version of Perf4j's timing aspect. This version supports:
 * <ul>
 * <li>The current Perf4j's way (i.e. use annotation {@link Profiled}).
 * <li>All methods belonging to <i>callable programs</i> (i.e. implement
 * {@link com.quipoz.framework.util.CallableProgram} interface) but not being annotated by
 * {@link Profiled} annotation.
 * </ul>
 * 
 * @author ntruong5
 */
@Aspect
public class ExtendedTimingAspect extends AbstractIntegralTimingAspect {
    static final String ASPECT_EXPR_HIBERNATE_DAO = "execution(* find*(..)) " +
    		"&& target(com.csc.smart.dataaccess.dao.GenericDAO)";
		private static final String ASPECT_EXPR_ALL_EXEC = "execution(* *(..)) ";
		private static final String ASPECT_EXPR_ANNOTATED = "@annotation(org.perf4j.aop.Profiled) ";
		private static final String ASPECT_EXPR_CALLABLEP = "target(com.quipoz.framework.util.CallableProgram) ";			
	    private static final String ASPECT_EXPR_NOT_ANNOTATED = ASPECT_EXPR_ALL_EXEC + "&& !("
            + ASPECT_EXPR_ANNOTATED + ") " + "&& " + ASPECT_EXPR_CALLABLEP;


    /**
     * Performs profiling logging code for methods belonging to <i>callable programs</i> (i.e.
     * implement {@link com.quipoz.framework.util.CallableProgram} interface) but not being
     * annotated by {@link Profiled} annotation.
     * 
     * @param pjp
     * @return  
     * @throws Throwable
     * @deprecated Used only by AspectJ.
     */
	 
    @Around(value = "execution(* *(..)) && !( @annotation(org.perf4j.aop.Profiled)) && target(com.quipoz.framework.util.CallableProgram)", argNames = "pjp")
    @Deprecated
    public Object doPerfLoggingForNonAnnotatedMethods(final ProceedingJoinPoint pjp)
            throws Throwable {
        return doPerfLogging(pjp, ProfiledLiteral.getDefaultLiteral());
    }
	

    /**
     * Performs profiling logging code for methods find*() belonging to <i>Hibernate DAO</i> (i.e.
     * implement {@link com.csc.smart.dataaccess.dao.GenericDAO} interface) annotation.
     * 
     * @param pjp
     * @return
     * @throws Throwable
     * @deprecated Used only by AspectJ.
     */
	 
    @Around(value = "execution(* find*(..)) && target(com.csc.smart.dataaccess.dao.GenericDAO) ", argNames = "pjp")
    @Deprecated
    public Object doPerfLoggingForHibernateDAOMethods(final ProceedingJoinPoint pjp)
            throws Throwable {
        return doPerfLogging(pjp, ProfiledLiteral.getDefaultLiteral());
    }
	
}
