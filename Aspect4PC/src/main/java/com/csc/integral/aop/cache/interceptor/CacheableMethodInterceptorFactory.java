package com.csc.integral.aop.cache.interceptor;

import com.csc.integral.aop.cache.impl.utils.ExternalClassConstant;
import com.csc.integral.aop.cache.interceptor.impl.HibernateCacheMethodInterceptor;
import com.csc.integral.aop.cache.interceptor.impl.ProgramCacheMethodInterceptor;
import com.csc.integral.utils.assertion.Assert;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheableMethodInterceptorFactory {

    private static final CacheableMethodInterceptorFactory s_instance = new CacheableMethodInterceptorFactory();

    private CacheableMethodInterceptorFactory() {
        // to hide
    }

    /**
     * Get the current value for instance
     * 
     * @return Returns the instance.
     */
    public static CacheableMethodInterceptorFactory getInstance() {
        return s_instance;
    }

    /**
     * Creates a new interceptor which bases on a given intercepting object.
     * 
     * @param interceptingObject
     * @return
     */
    public CacheableMethodInterceptor newInterceptor(Object interceptingObject) {
        Assert.ARGUMENTS.notNull(interceptingObject, "'interceptingObject' is missing");

        Class<?> cls = interceptingObject.getClass();
        if (ExternalClassConstant.COBOLConvCodeModel.isAssignableFrom(cls)) {
            return new ProgramCacheMethodInterceptor();
        } else if (ExternalClassConstant.GenericDAO.isAssignableFrom(cls)) {
            return new HibernateCacheMethodInterceptor();
        } else if (ExternalClassConstant.BaseDAO.isAssignableFrom(cls)) {
        	return new HibernateCacheMethodInterceptor();      	
        } else {
        	if (ExternalClassConstant.ItemTableDAM.getClassName().equals(cls.getName())){
        		return new HibernateCacheMethodInterceptor(); 
        	} else {
	            Assert.STATE.fail("Unsupported intercepting object type ''{}''", cls.getName());
	            return null;
        	}
        }
    }
}
