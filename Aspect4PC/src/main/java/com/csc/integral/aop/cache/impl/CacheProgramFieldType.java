package com.csc.integral.aop.cache.impl;

/**
 * All supported field types applicable for cache programs.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum CacheProgramFieldType {

    /**
     * Return value.
     * <p>
     * Example: csc.integral.aop.cache.<b>resultField</b>...Getlogo=lsaaLogo
     */
    resultField,

    /**
     * Example: csc.integral.aop.cache.<b>statusField</b>...Getusrdsc=getusrdrec.statuz
     */
    statusField,

    /**
     * Example: csc.integral.aop.cache.<b>idField</b>...Busdate=busdaterec.busdateRec
     */
    idField;

    /**
     * Returns the prefix computed from the current {@link #indicator} value.
     * 
     * @return
     */
    public String getPrefix() {
        return ConfigurationLoader.CONFIG_PREFIX_WITH_DOT + name() + ".";
    }

    /**
     * Get the current value for indicator
     * 
     * @return Returns the indicator.
     */
    public String getIndicator() {
        return name();
    }
}
