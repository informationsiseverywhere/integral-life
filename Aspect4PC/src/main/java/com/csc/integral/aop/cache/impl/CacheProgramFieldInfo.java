package com.csc.integral.aop.cache.impl;

import java.lang.reflect.Field;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Container for a field of a cacheable program. This field can be a return field, a status field or
 * another field.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheProgramFieldInfo {
    private String className;
    private String fieldPath;
    private transient Field field;

    /**
     * Get the current value for field
     * 
     * @return Returns the field.
     */
    public Field getField() {
        return field;
    }

    /**
     * Sets new value for field
     * 
     * @param field
     *            The new value to set.
     */
    public void setField(Field field) {
        this.field = field;
    }

    /**
     * Returns current value of {@link #className}.
     * 
     * @return the {@link #className}.
     */
    public String getClassName() {
        return className;
    }

    /**
     * Sets new value for {@link #className}.
     * 
     * @param className
     *            the new className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * Returns current value of {@link #fieldPath}.
     * 
     * @return the {@link #fieldPath}.
     */
    public String getFieldPath() {
        return fieldPath;
    }

    /**
     * Sets new value for {@link #fieldPath}.
     * 
     * @param fieldPath
     *            the new fieldPath to set
     */
    public void setFieldPath(String fieldPath) {
        this.fieldPath = fieldPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, false);
    }
}
