package com.csc.integral.aop.cache.impl.aspect;

import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An extension for program cache object type.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
@Aspect
public class ProgramCacheObjectAspect extends AbstractCacheObjectAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProgramCacheObjectAspect.class);

    /**
     * Intercepts around any invocation from a method <code>mainline</code> and intercepting object
     * is an instance of <code>COBOLConvCodeModel</code>.
     * 
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* mainline(..))", argNames = "pjp")
    @Deprecated
    public Object interceptMainlineMethod(final ProceedingJoinPoint pjp) throws Throwable {
        if (ArrayUtils.isEmpty(pjp.getArgs()) || ArrayUtils.isEmpty((Object[]) pjp.getArgs()[0])) {
            LOGGER.debug("Method mainline() is not intercepted -> Proceed the original invocation");
            return pjp.proceed();
        }
        return doOnReceiveInvocation(pjp, null);
    }
}
