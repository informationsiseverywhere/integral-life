package com.csc.integral.aop.workflow.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.processor.ProgramStacktraceProcessor;

@Aspect
public class ProgramStackTraceAspect extends AbstractWorkflowAspect {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProgramStackTraceAspect.class);

	private static final String IO_PROGRAM_METHOD_FILTER = "!target(com.csc.smart400framework.dataaccess.SmartFileCode) "
			+ "&& !execution(* com.csc.smart400framework.dataaccess.SmartFileCode.*(..)) ";
	// + "&& !target(com.csc.smart.dataaccess.dao.GenericDAO)"
	// + "&& !execution(* com.quipoz.COBOLFramework.datatype.*.*(..))";

	//private static final String FILTER = "!target(com.quipoz.framework.util.AppVars) ";

	private static final String ASPECT_METHOD_INVOKE = "(execution(* com.csc.*(..)) || execution(* com.quipoz.*(..))) ";
	private static final String ASPECT_PROGRAM_METHOD_INVOKE = ASPECT_METHOD_INVOKE
			+ "&& " + IO_PROGRAM_METHOD_FILTER;

	// private static final String ASPECT_LOG_IN =
	// "execution(public void com.csc.PolisyAsia.FirstServlet.doGet(..))";
	// private static final String ASPECT_LOG_OUT =
	// "execution(public void com.csc.PolisyAsia.LogoutServlet.doPost(..))";

	public ProgramStackTraceAspect() {
		processor = new ProgramStacktraceProcessor();
	}

	@Around(value = " (execution(* com.csc.*(..)) || execution(* com.quipoz.*(..)))      && !target(com.csc.smart400framework.dataaccess.SmartFileCode) ", argNames = "pjp")
	public Object aroundMethod(final ProceedingJoinPoint pjp) throws Throwable {
		return super.aroundMethod(pjp);
	}

	@Override
	protected void beforeMethod(ProceedingJoinPoint pjp) {
		processor.beforeMethod(pjp);
	}

	@Override
	protected void afterMethod(ProceedingJoinPoint pjp, Object returnObj) {
		processor.afterMethod(pjp);
	}
}
