package com.csc.integral.aop.workflow.processor;

import org.aspectj.lang.ProceedingJoinPoint;

public interface WorkflowProcessor {

	public void beforeMethod(final ProceedingJoinPoint pjp);
	
	public void afterMethod(final ProceedingJoinPoint pjp);
	
	public void captureException(Throwable t, final ProceedingJoinPoint pjp);
	
}
