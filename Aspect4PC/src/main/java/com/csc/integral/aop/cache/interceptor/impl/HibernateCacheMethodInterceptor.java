package com.csc.integral.aop.cache.interceptor.impl;

import java.util.Map;
import java.util.Observable;

import org.apache.commons.lang3.StringUtils;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.impl.keybuilder.CacheKeyBuilderFactory;
import com.csc.integral.aop.cache.interceptor.CacheableInvocation;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class HibernateCacheMethodInterceptor extends AbstractCacheableMethodInterceptor {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> acquireInterceptedObjectInfo(Observable observable,
            Object interceptingObject, CacheableInvocation invocation, Object[] args) {
        Map<String, Object> map = super.acquireInterceptedObjectInfo(observable,
            interceptingObject, invocation, args);

        String region = (String) map.get(NOTIF_OUTPUT_REGION);
        if (StringUtils.isEmpty(region)) {
            region = "REF";
            map.put(NOTIF_OUTPUT_REGION, region);
        }
        return map;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CacheKey buildCacheKey(Object interceptedObject, Object[] args,
            CacheableInvocation invocation) {
        return CacheKeyBuilderFactory.getInstance().getBuilder(this)
                .build(interceptedObject.getClass(), invocation.getName(), args);
    }
}
