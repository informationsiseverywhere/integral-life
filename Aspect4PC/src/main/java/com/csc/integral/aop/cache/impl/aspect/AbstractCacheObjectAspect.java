package com.csc.integral.aop.cache.impl.aspect;

import java.util.concurrent.Callable;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;

import com.csc.integral.aop.cache.exception.CacheManagementException;
import com.csc.integral.aop.cache.interceptor.CacheableInvocation;
import com.csc.integral.aop.cache.interceptor.CacheableMethodInterceptor;
import com.csc.integral.aop.cache.interceptor.CacheableMethodInterceptorFactory;

/**
 * Base implementation of all aspects handling object caching. This base class is responsible for:
 * <ul>
 * <li>Defining the whole processing workflow for object caching. Template pattern is used to allow
 * subclassing.
 * <li>Logging method entry and exit from the system property
 * "<i>csc.integral.aop.methodInOutLogged</i>".
 * </ul>
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
@Aspect
public abstract class AbstractCacheObjectAspect {
    protected static final String NOTIF_OUTPUT_REGION = "output.region";
    protected static final String NOTIF_OUTPUT_CONTINUED = "output.continued";

    private AspectJCacheableInvocationCreator cacheableInvocationCreator;

    protected AbstractCacheObjectAspect() {
        cacheableInvocationCreator = new AspectJCacheableInvocationCreator();
    }

    protected Object doOnReceiveInvocation(final ProceedingJoinPoint pjp, Object[] args)
            throws Throwable {
        CacheableMethodInterceptor interceptor = CacheableMethodInterceptorFactory.getInstance()
                .newInterceptor(pjp.getTarget());
        CacheableInvocation inv = cacheableInvocationCreator.create(pjp, args,
            new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    try {
                        return pjp.proceed();
                    } catch (Throwable e) {
                        throw new CacheManagementException("Error while proceeding join-point: {}",
                                e, pjp);
                    }
                }
            });
        return interceptor.intercept(inv);
    }
}
