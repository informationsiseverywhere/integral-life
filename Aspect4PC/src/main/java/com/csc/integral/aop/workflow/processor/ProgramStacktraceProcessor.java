package com.csc.integral.aop.workflow.processor;

import org.aspectj.lang.ProceedingJoinPoint;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;
import com.csc.integral.aop.workflow.output.UserWorkflowWritter;

public class ProgramStacktraceProcessor extends AbstractWorkflowProcessor {

	private static final String MAINLINE = "mainline";
	private static final String SET_PARAMS = "setParams";
	

	private boolean isPrintMainlineParam = WorkflowCaptureCfg.getInstance()
			.isPrintMainlineParam();

	@Override
	public void processBeforeMethod(ProceedingJoinPoint pjp) {
		 
		 ProcessorUtils.getUserWorkflowWritter().startMethod(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp),
				pjp.getArgs());

		printParamValueBeforeMethod(pjp, pjp.getArgs());
	
	}

	@Override
	public void processAfterMethod(ProceedingJoinPoint pjp) {
		 
		 ProcessorUtils.getUserWorkflowWritter().preEndMethod(pjp.getTarget(),
				ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs());
		printParamValueAfterMethod(pjp, pjp.getArgs());
		ProcessorUtils.getUserWorkflowWritter().endMethod(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp),
				pjp.getArgs());
	}

	public void afterLogin(final ProceedingJoinPoint pjp){
		// still not have UserId to creat UserWorkflowWritter
		// UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();
		// wr.startWorkflow("Logged in");
	}

	public void afterLogout(final ProceedingJoinPoint pjp) {
		/*
		 * UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();
		 * wr.finalize(); ProcessorUtils.closeUserWorkflowWritter(wr);
		 */
	}

	private void printParamValueBeforeMethod(ProceedingJoinPoint pjp,
			Object[] params) {

		String className = ProcessorUtils.getTargetSimpleClassName(pjp);
		String mtName = ProcessorUtils.getJoinPointMethod(pjp).getName();
		// Main line params
		if (isPrintMainlineParam && MAINLINE.equals(mtName)) {
			printParamValue(pjp, params, "Before " + className + "." + mtName);
		} else if (isPrintMainlineParam && SET_PARAMS.equals(mtName)) {
			// setParams params
			printParamValue(pjp, params, "Before " + className + "." + mtName);
		}
	}

	private void printParamValueAfterMethod(ProceedingJoinPoint pjp,
			Object[] params) {

		String className = ProcessorUtils.getTargetSimpleClassName(pjp);
		String mtName = ProcessorUtils.getJoinPointMethod(pjp).getName();
		// Main line params
		if (isPrintMainlineParam && MAINLINE.equals(mtName)) {
			printParamValue(pjp, params, "After " + className + "." + mtName);
		}
	}

	private void printParamValue(ProceedingJoinPoint pjp, Object[] params,
			String note) {
		UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();

		if (params != null && params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				if(params[i] != null) {
					if (params[i].getClass().isArray()) {
						Object[] objs = (Object[]) params[i];
						for (int j = 0; j < objs.length; j++) {
							wr.writeParam(note, "params[" + (i + j) + "]",
									objs[j].toString());
						}
					} else {
						wr.writeParam(note, "params[" + i + "]",
								params[i].toString());
					}
				}
			}
		}
	}	
}
