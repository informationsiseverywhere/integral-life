package com.csc.integral.aop.workflow;

import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorkflowCaptureCfg {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WorkflowCaptureCfg.class);

	private Properties cfgProperties;

	private String outputFactory;
	private String outputDir;
	private int maxProgramClassPerFile;
	private boolean isPrintMainlineParam;
	private boolean isPrintScreenVar;
	private boolean isPrintIOAcessDetail;
	private boolean isStackTraceForProgramOnly;
	private boolean isBackgroundThreadOnly;
	private boolean isPrintCallReference;
	private String[] threadNamesFilter;
	private String[] exceptionsFilter;

	private static WorkflowCaptureCfg instance = new WorkflowCaptureCfg();

	public static WorkflowCaptureCfg getInstance() {
		return instance;
	}

	private WorkflowCaptureCfg() {
		loadConfig();
	}

	private void loadConfig() {
		try {
			// load config from workflow-capture.properties
			URL url = WorkflowCaptureCfg.class.getClassLoader().getResource(
					"workflow-capture.properties");
			cfgProperties = new Properties();
			cfgProperties.load(url.openStream());

			outputFactory = cfgProperties
					.getProperty("csc.integral.aop.workflow.output.factory");
			outputDir = cfgProperties
					.getProperty("csc.integral.aop.workflow.output.dir");
			isPrintMainlineParam = Boolean
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.filter.printMainlineParam"));
			isPrintScreenVar = Boolean
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.filter.printScreenVar"));
			isPrintIOAcessDetail = Boolean
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.filter.printIOAcessDetail"));
			isStackTraceForProgramOnly = Boolean
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.filter.stackTraceForProgramOnly"));
			isBackgroundThreadOnly = Boolean
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.filter.captureBackgroundThreadOnly"));
			threadNamesFilter = cfgProperties.getProperty(
					"csc.integral.aop.workflow.filter.threadNamesFilter",
					"http").split(",");
			maxProgramClassPerFile = Integer
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.output.maxProgramClassPerFile"));
			isPrintCallReference = Boolean
					.valueOf(cfgProperties
							.getProperty("csc.integral.aop.workflow.filter.printCallReference"));

			// slip space ' '
			for (int i = 0; i < threadNamesFilter.length; i++) {
				threadNamesFilter[i] = threadNamesFilter[i].trim();
			}
			exceptionsFilter = cfgProperties.getProperty(
					"csc.integral.aop.workflow.filter.ExceptionsFilter", "")
					.split(",");
			// slip space ' '
			for (int i = 0; i < exceptionsFilter.length; i++) {
				exceptionsFilter[i] = exceptionsFilter[i].trim();
			}
		} catch (Exception ex) {
			LOGGER.error("load workflow config exception", ex);
			throw new RuntimeException(ex);
		}
	}

	public Properties getCfgProperties() {
		return cfgProperties;
	}

	public String getOutputFactory() {
		return outputFactory;
	}

	public String getOutputDir() {
		return outputDir;
	}

	public boolean isPrintMainlineParam() {
		return isPrintMainlineParam;
	}

	public boolean isPrintIOAcessDetail() {
		return isPrintIOAcessDetail;
	}

	public void setPrintIOAcessDetail(boolean isPrintIOAcessDetail) {
		this.isPrintIOAcessDetail = isPrintIOAcessDetail;
	}

	public boolean isStackTraceForProgramOnly() {
		return isStackTraceForProgramOnly;
	}

	public void setStackTraceForProgramOnly(boolean isStackTraceForProgramOnly) {
		this.isStackTraceForProgramOnly = isStackTraceForProgramOnly;
	}

	public boolean isBackgroundThreadOnly() {
		return isBackgroundThreadOnly;
	}

	public void setBackgroundThreadOnly(boolean isBackgroundThreadOnly) {
		this.isBackgroundThreadOnly = isBackgroundThreadOnly;
	}

	public String[] getThreadNamesFilter() {
		return threadNamesFilter;
	}

	public void setThreadNamesFilter(String[] threadNamesFilter) {
		this.threadNamesFilter = threadNamesFilter;
	}

	public void setPrintMainlineParam(boolean isPrintMainlineParam) {
		this.isPrintMainlineParam = isPrintMainlineParam;
	}

	public boolean isPrintScreenVar() {
		return isPrintScreenVar;
	}

	public void setPrintScreenVar(boolean isPrintScreenVar) {
		this.isPrintScreenVar = isPrintScreenVar;
	}

	public String[] getExceptionsFilter() {
		return exceptionsFilter;
	}

	public void setExceptionsFilter(String[] exceptionsFilter) {
		this.exceptionsFilter = exceptionsFilter;
	}

	public int getMaxProgramClassPerFile() {
		return maxProgramClassPerFile;
	}

	public void setMaxProgramClassPerFile(int maxProgramClassPerFile) {
		this.maxProgramClassPerFile = maxProgramClassPerFile;
	}

	public boolean isPrintCallReference() {
		return isPrintCallReference;
	}

	public void setPrintCallReference(boolean isPrintCallReference) {
		this.isPrintCallReference = isPrintCallReference;
	}
	
}
