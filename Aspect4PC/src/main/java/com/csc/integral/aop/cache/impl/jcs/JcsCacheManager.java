package com.csc.integral.aop.cache.impl.jcs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.access.exception.ObjectExistsException;
import org.apache.jcs.engine.behavior.ICompositeCacheAttributes;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.apache.jcs.engine.memory.lru.LRUMemoryCache;
import org.apache.jcs.engine.memory.mru.MRUMemoryCache;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.CacheManager;
import com.csc.integral.aop.cache.exception.CacheInitializationException;
import com.csc.integral.aop.cache.exception.CacheManagementException;
import com.csc.integral.aop.cache.impl.AbstractCacheManager;
import com.csc.integral.aop.cache.impl.CacheConfiguration;
import com.csc.integral.aop.cache.impl.CacheRegionConfiguration;
import com.csc.integral.aop.cache.impl.CacheScope;
import com.csc.integral.aop.cache.impl.utils.CacheManagerUtils;
import com.csc.integral.utils.DoubleCheckLockingUtils;
import com.csc.integral.utils.assertion.Assert;

/**
 * An implementation of {@link CacheManager} which uses Apache JCS as the cache provider. This
 * implementation class is used when the configured cache provider is 'JCS'.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class JcsCacheManager extends AbstractCacheManager {
    private Map<String, JCS> cacheRegionMap;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initializeProviderConfigurations() {
        CacheConfiguration cc = getContext().getCacheConfiguration();
        Properties properties = new Properties();

        // fill default region configurations
        CacheRegionConfiguration defaultRegionConf = cc
                .getRegionConfiguration(CacheConfiguration.DEFAULT_REGION);
        fillJcsPropertiesFromRegionConfigs(defaultRegionConf, properties);

        // fill other regions' configurations
        Set<CacheRegionConfiguration> regionConfigs = cc.getRegionConfigs();
        for (CacheRegionConfiguration regionConf : regionConfigs) {
            if (!regionConf.isDefault()) {
                fillJcsPropertiesFromRegionConfigs(regionConf, properties);
            }
        }

        CompositeCacheManager cm = CompositeCacheManager.getUnconfiguredInstance();
        cm.configure(properties);

        // build all application-wide regions.
        cacheRegionMap = new HashMap<String, JCS>();
        createApplicationCacheRegions(cm);
    }

    /**
     * Builds all application-scoped regions as well as all declared regions for session-scoped
     * regions. Those declared regions will be used to build actual session-scoped regions later.
     * 
     * @param cm
     *            A configured instance of {@link CompositeCacheManager}.
     */
    protected void createApplicationCacheRegions(CompositeCacheManager cm) {
        String[] regionNames = cm.getCacheNames();
        for (String regionName : regionNames) {
            try {
                JCS cache = JCS.getInstance(regionName);
                cacheRegionMap.put(regionName, cache);
            } catch (CacheException e) {
                throw new CacheInitializationException(
                        "Error while creating JCS instance for region ''{0}''", e, regionName);
            }
        }
    }

    private void fillJcsPropertiesFromRegionConfigs(CacheRegionConfiguration regionConf,
            Properties properties) {
        fillEvictionPolicy(regionConf, properties);
        fillPersistenceType(regionConf, properties);
        fillMaxObjects(regionConf, properties);
        fillMaxLifeSeconds(regionConf, properties);
    }

    private void fillMaxObjects(CacheRegionConfiguration regionConf, Properties properties) {
        String key = JcsConfigProperty.MaxObjects.getKey(regionConf);
        String value = String.valueOf(regionConf.getMaxObjects());
        properties.put(key, value);
    }

    private void fillMaxLifeSeconds(CacheRegionConfiguration regionConf, Properties properties) {
        if (regionConf.getMaxTimeToLiveSeconds() != null) {
            String key = JcsConfigProperty.MaxLifeSeconds.getKey(regionConf);
            String value = ObjectUtils.toString(regionConf.getMaxTimeToLiveSeconds());
            properties.put(key, value);

            key = JcsConfigProperty.IsEternal.getKey(regionConf);
            value = Boolean.FALSE.toString();
            properties.put(key, value);
        }
    }

    private void fillPersistenceType(CacheRegionConfiguration regionConf, Properties properties) {
        String key, value;
        switch (regionConf.getPersistenceType()) {
            case MEM_ONLY:
                key = JcsConfigProperty.Auxiliaries.getKey(regionConf);
                value = ""; // no auxiliary --> mem only
                properties.put(key, value);

                key = JcsConfigProperty.IsSpool.getKey(regionConf);
                value = Boolean.FALSE.toString();
                properties.put(key, value);

                break;

            case DISK_CACHE:
                key = JcsConfigProperty.Auxiliaries.getKey(regionConf);
                value = "DC"; // 'DC' --> one auxiliary exists: DISK CACHE
                properties.put(key, value);

                key = JcsConfigProperty.IsSpool.getKey(regionConf);
                value = Boolean.TRUE.toString();
                properties.put(key, value);

                break;
        }
    }

    private void fillEvictionPolicy(CacheRegionConfiguration regionConf, Properties properties) {
        String key, value;
        switch (regionConf.getEvictionPolicy()) {
            case LRU:
                key = JcsConfigProperty.MemoryCacheName.getKey(regionConf);
                value = LRUMemoryCache.class.getName();
                properties.put(key, value);
                break;
            case MRU:
                key = JcsConfigProperty.MemoryCacheName.getKey(regionConf);
                value = MRUMemoryCache.class.getName();
                properties.put(key, value);
                break;
        }
    }

    /**
     * Get cache region. If cache region does not exist (session region), initialize it.
     * 
     * @param actualRegion
     * @return
     */
    private JCS getCacheAccess(final String actualRegion) {
        JCS cache = DoubleCheckLockingUtils.getMapBasedSingleton(cacheRegionMap, actualRegion,
            this, new Callable<JCS>() {
                @Override
                public JCS call() throws Exception {
                    return createSessionCacheRegion(actualRegion);
                }
            });
        return cache;
    }

    /**
     * Initializes a session-scoped region.
     * 
     * @param actualRegion
     * @return
     */
    protected JCS createSessionCacheRegion(String actualRegion) {
        String declaredRegion = CacheManagerUtils.getDeclaredSessionRegion(actualRegion);

        CacheRegionConfiguration declaredRegionCfg = getContext().getCacheConfiguration()
                .getRegionConfiguration(declaredRegion);
        Assert.STATE.notNull(declaredRegionCfg,
            "Declared region ''{0}'' does not exist for the actual region ''{1}''", declaredRegion,
            actualRegion);
        Assert.STATE.isTrue(declaredRegionCfg.getScope() == CacheScope.SESSION,
            "Region ''{0}'' is not a session-scoped region. Scope is ''{1}''", declaredRegion,
            declaredRegionCfg.getScope());

        JCS declaredCache = getDeclaredSessionRegion(declaredRegion);
        ICompositeCacheAttributes newCacheAttributes = createNewRegionCacheAttributes(actualRegion,
            declaredCache);
        JCS newCache = createNewSessionRegion(actualRegion, declaredRegion, declaredCache,
            newCacheAttributes);

        return newCache;
    }

    private JCS createNewSessionRegion(String actualRegion, String declaredRegion,
            JCS declaredCache, ICompositeCacheAttributes newCacheAttributes) {
        JCS newCache;
        try {
            newCache = JCS.getInstance(actualRegion, newCacheAttributes);
            newCache.setDefaultElementAttributes(declaredCache.getDefaultElementAttributes());
        } catch (CacheException e) {
            throw new CacheManagementException(
                    "Error while creating actual region ''{0}'' from declared region ''{1}''", e,
                    actualRegion, declaredRegion);
        }
        return newCache;
    }

    private JCS getDeclaredSessionRegion(String declaredRegion) {
        JCS declaredCache;
        try {
            declaredCache = JCS.getInstance(declaredRegion);
        } catch (CacheException e) {
            throw new CacheManagementException("Error while getting declared region ''{0}''", e,
                    declaredRegion);
        }
        return declaredCache;
    }

    private ICompositeCacheAttributes createNewRegionCacheAttributes(String actualRegion,
            JCS declaredCache) {
        ICompositeCacheAttributes newCacheAttributes = declaredCache.getCacheAttributes().copy();
        newCacheAttributes.setCacheName(actualRegion);
        return newCacheAttributes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doPut(String region, CacheKey key, Object cacheObject) {
        try {
            getCacheAccess(region).putInGroup(key, key.getClassName(), cacheObject);
        } catch (ObjectExistsException e) {
            throw new CacheManagementException(
                    "Cache object (key: {0}) already exists in region ''{1}''", e, key, region);
        } catch (CacheException e) {
            throw new CacheManagementException(
                    "Error while putting cache object (key: {0}) into region ''{1}''", e, key,
                    region);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object doGet(String region, CacheKey key) {
        return getCacheAccess(region).getFromGroup(key, key.getClassName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doRemove(String region, CacheKey key) {
        getCacheAccess(region).remove(key, key.getClassName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int doRemoveAll(String actualRegion, CacheKey partialKey) {
        Set<CacheKey> keys = filterCacheKeyClassLevel(actualRegion, partialKey);
        filterCacheKeyMethodLevel(partialKey, keys);
        for (CacheKey key : keys) {
            doRemove(actualRegion, key);
        }
        return keys.size();
    }

    @SuppressWarnings("unchecked")
    private Set<CacheKey> filterCacheKeyClassLevel(String actualRegion, CacheKey partialKey) {
        return new HashSet<CacheKey>(getCacheAccess(actualRegion).getGroupKeys(
            partialKey.getClassName()));
    }

    private void filterCacheKeyMethodLevel(CacheKey partialKey, Set<CacheKey> keys) {
        if (StringUtils.isNotEmpty(partialKey.getMethodName())) {
            List<CacheKey> removingKeys = new LinkedList<CacheKey>();
            for (CacheKey cacheKey : keys) {
                if (!StringUtils.equals(cacheKey.getMethodName(), partialKey.getMethodName())) {
                    removingKeys.add(cacheKey);
                }
            }
            keys.removeAll(removingKeys);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doDisposeRegion(String actualRegion) {
        synchronized (cacheRegionMap) {
            JCS cache = cacheRegionMap.get(actualRegion);
            Assert.STATE.notNull(cache, "No cache region under name ''{0}'' exists", actualRegion);

            CompositeCacheManager.getInstance().freeCache(actualRegion, true);
            cacheRegionMap.remove(actualRegion);
        }
    }
}
