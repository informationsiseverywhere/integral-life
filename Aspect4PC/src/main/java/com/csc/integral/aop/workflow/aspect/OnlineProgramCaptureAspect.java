package com.csc.integral.aop.workflow.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.processor.OnlineProgramCaptureProcessor;

/**
 * @author tphan25
 */
@Aspect
public class OnlineProgramCaptureAspect extends AbstractWorkflowAspect {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OnlineProgramCaptureAspect.class);

	//private static final String ASPECT_ONLINE_PROGRAM_MAINLINE = "execution(public void com.csc..*.programs.P*.mainline(..)) ";
	private static final String ASPECT_ONLINE_PROGRAM_MAINLINE = "execution(* com.csc.*.programs.P*.mainline(..)) ";

	public OnlineProgramCaptureAspect() {
		processor = new OnlineProgramCaptureProcessor();
	}
	@Around(value = "execution(* com.csc.*.programs.P*.mainline(..)) ", argNames = "pjp")
	public Object aroundMainline(final ProceedingJoinPoint pjp)
			throws Throwable {
		return super.aroundMethod(pjp);
	}

	@Override
	protected void beforeMethod(ProceedingJoinPoint pjp) {
		processor.beforeMethod(pjp);
	}

	@Override
	protected void afterMethod(ProceedingJoinPoint pjp, Object returnObj) {
		processor.afterMethod(pjp);
	}
}
