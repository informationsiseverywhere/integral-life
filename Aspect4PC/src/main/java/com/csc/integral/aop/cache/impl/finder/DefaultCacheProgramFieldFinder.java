package com.csc.integral.aop.cache.impl.finder;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.csc.integral.aop.cache.exception.CacheManagementException;
import com.csc.integral.aop.cache.impl.CacheContext;
import com.csc.integral.aop.cache.impl.CacheProgramFieldInfo;
import com.csc.integral.aop.cache.impl.CacheProgramFieldType;
import com.csc.integral.aop.cache.impl.utils.CacheManagerUtils;
import com.csc.integral.utils.InvokableField;
import com.csc.integral.utils.ReflectionUtils;
import com.csc.integral.utils.assertion.Assert;
import com.csc.integral.utils.functor.Predicate;
import com.csc.integral.utils.text.TextPatternMatcher;
import com.csc.integral.utils.text.TextPatternMatcherFactory;

/**
 * Default implementation of {@link CacheProgramFieldFinder} which finds the field according to both
 * explicit and implicit algorithms.
 * <p>
 * <b>For return field lookup:</b>
 * <ul>
 * <li>Explicit lookup: The main configuration is looked up to get the explicit configuration
 * related to return fields. The configuration is stored in the
 * {@link CacheContext#getResultFieldMap()}.
 * <li>Implicit lookup: The return field whose name derives from a given intercepting object class
 * is chosen.
 * </ul>
 * <p>
 * <b>For status field lookup:</b> The algorithm is similar except that the
 * {@link CacheContext#getStatusFieldMap()} is queried for explicit lookup and pattern "*statuz" is
 * used for implicit matching.
 * <p>
 * <b>For identifier field lookup</b>: Only explicit lookup is applicable.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class DefaultCacheProgramFieldFinder implements CacheProgramFieldFinder {

    /**
     * {@inheritDoc}
     */
    @Override
    public InvokableField findReturnField(Object interceptingObject) {

        // explicit return field lookup
        InvokableField result = findFieldExplicitly(interceptingObject,
            CacheProgramFieldType.resultField);

        // implicit return field lookup
        if (result == null) {
            result = findReturnFieldImplicitly(interceptingObject);
        }

        return result != null ? result.makeAccessible() : null;
    }

    /**
     * {@inheritDoc}
     */
    public InvokableField findStatusField(Object interceptingObject) {

        // explicit status field lookup
        InvokableField result = findFieldExplicitly(interceptingObject,
            CacheProgramFieldType.statusField);

        // implicit status field lookup
        if (result == null) {
            result = findStatusFieldImplicitly(interceptingObject);
        }

        return result != null ? result.makeAccessible() : null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvokableField findIdentifierField(Object interceptingObject) {

        // explicit identifier field lookup
        InvokableField result = findFieldExplicitly(interceptingObject,
            CacheProgramFieldType.idField);
        return result != null ? result.makeAccessible() : null;
    }

    private InvokableField findFieldExplicitly(Object interceptingObject,
            CacheProgramFieldType fieldType) {
        Assert.ARGUMENTS.notNull(interceptingObject, "Intercepting object is missing");

        CacheContext cacheContext = CacheManagerUtils.getCurrentCacheContext();
        Assert.STATE.notNull(cacheContext, "Cache context is missing");

        CacheProgramFieldInfo returnField = cacheContext.getFieldMap(fieldType).get(
            interceptingObject.getClass().getName());
        if (returnField != null) {
            return ReflectionUtils.lookupField(interceptingObject, returnField.getFieldPath());
        }
        return null;
    }

    // Conditions:
    // 1) Field name is "statuz" OR
    // 2) There is a record (name: "{programSimpleClassName}rec") having a field named "statuz" OR
    // 3) field name matches "*statuz" pattern
    private InvokableField findStatusFieldImplicitly(Object interceptingObject) {
        InvokableField f = ReflectionUtils.lookupField(interceptingObject, "statuz");

        if (f == null) {
            String fieldPath = StringUtils.uncapitalize(interceptingObject.getClass()
                    .getSimpleName()) + "rec.statuz";
            f = ReflectionUtils.lookupField(interceptingObject, fieldPath);
        }

        if (f == null) {
            Field resultField = findFieldByMatchingName(interceptingObject, "*statuz");
            if (resultField != null) {
                f = new InvokableField(resultField, interceptingObject);
            }
        }

        return f;
    }

    // Conditions:
    // 1) is not a static field.
    // 2) EITHER field name OR field type simple class name matches the pattern
    // "{programSimpleClassName}rec". The order is preserved.
    private InvokableField findReturnFieldImplicitly(Object interceptingObject) {
        final String matchingName = StringUtils.uncapitalize(interceptingObject.getClass()
                .getSimpleName()) + "Rec";
        Field resultField = findFieldByMatchingName(interceptingObject, matchingName);
        if (resultField != null) {
            return new InvokableField(resultField, interceptingObject);
        }
        return null;
    }

    private Field findFieldByMatchingName(Object interceptingObject, final String matchingName) {
        final TextPatternMatcher matcher = TextPatternMatcherFactory.getsInstance().createMatcher()
                .usePatterns(matchingName).caseSensitive(false);

        List<Field> fields = ReflectionUtils.collectFields(interceptingObject.getClass(), false,
            true, new Predicate<Field>() {
                @Override
                public boolean evaluate(Field f) {
                    if (!Modifier.isStatic(f.getModifiers())) {
                        if (matcher.matches(f.getName())) {
                            return true;
                        }
                        if (matcher.matches(f.getType().getSimpleName())) {
                            return true;
                        }
                    }
                    return false;
                }
            });

        if (fields.size() > 1) {
            throw new CacheManagementException(
                    "There are more than 1 (actual: {}) fields matching with name ''{}''",
                    fields.size(), matchingName);
        }
        return fields.size() == 1 ? fields.get(0) : null;
    }
}
