package com.csc.integral.aop.workflow.processor;

import java.lang.reflect.Method;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;

public class ProcessorFilter {

	private static WorkflowCaptureCfg config = WorkflowCaptureCfg.getInstance();
	
	public boolean isFileter(Object target, Method method, Object[] args) {
		boolean bFilter = false;
		
		// filter thread
		if (isFilterThread()) {
			return true;
		}
		
		return bFilter;
	}
	
	public boolean isFileter(Throwable t) {
		boolean bFilter = false;
		
		String[] exceptionsFilter = config.getExceptionsFilter();
		for (String exName : exceptionsFilter) {
			if(t.getClass().getSimpleName().equals(exName)) {
				return true;
			}
		}		
		return bFilter;
	}
	
	private boolean isFilterThread() {
		String threadName = Thread.currentThread().getName();
		String[] threadNamesFilter = config.getThreadNamesFilter();
		for (String tn: threadNamesFilter) {
			if(threadName.contains(tn)) {
				return true;
			}
		}
		return false;
	}
}
