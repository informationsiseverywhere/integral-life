package com.csc.integral.aop.performance.perf4j;

import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;

import com.csc.integral.utils.functor.Command;
import com.csc.integral.utils.functor.ExceptionAwareCommand;
import com.csc.integral.utils.functor.VoidCommand;
import com.csc.integral.utils.functor.VoidExceptionAwareCommand;

/**
 * A Utils class make using {@link StopWatch} class more easy.
 * 
 * @author ntruong5
 */
public final class StopWatchUtils {

    private StopWatchUtils() {
        // Hide it
    }

    /**
     * Executes a given non-returned command with performance monitoring enabled.
     * 
     * @param command
     *            Actual void-returned command.
     * @param tag
     *            Tag name.
     */
    public static void withStopwatch(VoidCommand command, String tag) {
        withStopwatch((Command<Void>) command, tag);
    }

    /**
     * Executes a given command with performance monitoring enabled.
     * 
     * @param <R>
     *            Return type.
     * @param command
     *            Actual command.
     * @param tag
     *            Tag name.
     * @return Command result value.
     */
    public static <R> R withStopwatch(Command<R> command, String tag) {
       
        R result;
        try {
            result = command.execute();
        } finally {
            prepareStopWatch(tag).stop(tag);
        }
        return result;
    }

    /**
     * Executes a given command with performance monitoring enabled. This method is different from
     * {@link #withStopwatch(Command, String)} in that this version allows exceptions thrown from
     * the given command to the caller of this method.
     * 
     * @param <R>
     *            Return type.
     * @param <E>
     *            actually exception type.
     * @param command
     *            Actual command.
     * @param tag
     *            Tag name.
     * @return Command result value.
     * @throws Exception
     *             Exception thrown from the command.
     */
    public static <R, E extends Exception> R withStopwatch(ExceptionAwareCommand<R, E> command,
            String tag) throws E {
       
        R result;
        try {
            result = command.execute();
        } finally {
        	prepareStopWatch(tag).stop(tag);
        }
        return result;
    }

    /**
     * Executes a given command with performance monitoring enabled. This method is different from
     * {@link #withStopwatch(Command, String)} in that this version allows exceptions thrown from
     * the given command to the caller of this method.
     * 
     * @param <E>
     *            Actually exception type.
     * @param command
     *            Actual command.
     * @param tag
     *            Tag name.
     * @throws Exception
     *             Exception thrown from the command.
     */
    public static <E extends Exception> void withStopwatch(VoidExceptionAwareCommand<E> command,
            String tag) throws E {
         
        try {
            command.execute();
        } finally {
        	prepareStopWatch(tag).stop(tag);
        }
    }

    private static StopWatch prepareStopWatch(String tag) {
        return new Log4JStopWatch();
    }
}
