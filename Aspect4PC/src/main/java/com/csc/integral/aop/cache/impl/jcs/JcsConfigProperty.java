package com.csc.integral.aop.cache.impl.jcs;

import java.text.MessageFormat;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.csc.integral.aop.cache.impl.CacheRegionConfiguration;
import com.csc.integral.utils.assertion.Assert;

/**
 * All supported configuration attributes (cache-attributes and element-attributes) in JCS.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum JcsConfigProperty {
    Auxiliaries("jcs${region}", 0), // tweak Eclipse to preserve enum values in separated lines
    MemoryCacheName("jcs${region}.cacheattributes.MemoryCacheName", 0), //
    MaxLifeSeconds("jcs${region}.elementattributes.MaxLifeSeconds", 0), //
    MaxObjects("jcs${region}.cacheattributes.MaxObjects", 0), //
    IsEternal("jcs${region}.elementattributes.IsEternal", 0), //
    IsRemote("jcs${region}.elementattributes.IsRemote", 0), //
    IdleTime("jcs${region}.elementattributes.IdleTime", 0), //
    IsSpool("jcs${region}.elementattributes.IsSpool", 0);

    private String pattern;
    private int numberOfParams;

    private JcsConfigProperty(String pattern, int numberOfParams) {
        this.pattern = pattern;
        this.numberOfParams = numberOfParams;
    }

    /**
     * Returns the current JCS property value with optional replacement arguments <b>for the default
     * region</b>. The number of arguments must be the same as configured parameter size (i.e.
     * {@link #numberOfParams}).
     * 
     * @param args
     * @return
     */
    public String getDefaultKey(Object... args) {
        Assert.ARGUMENTS.notNull(args, "Replacement arguments is null");
        Assert.ARGUMENTS.isTrue(args.length == numberOfParams,
            "Declared number of parameters (value={0}) "
                    + "does not match with actual arguments (length={1})", numberOfParams,
            args.length);

        String actualPattern = StringUtils.replaceOnce(pattern, "${region}", ".default");
        return formatPropertyKey(actualPattern, args);
    }

    /**
     * Returns the current JCS property value with optional replacement arguments <b>for a given
     * region</b> OR <b>for the default region</b>. The number of arguments must be the same as
     * configured parameter size (i.e. {@link #numberOfParams}).
     * 
     * @param region
     * @param args
     * @return
     */
    String getKey(CacheRegionConfiguration region, Object... args) {
        Assert.ARGUMENTS.notNull(region, "No region is given");
        if (region.isDefault()) {
            return getDefaultKey(args);
        } else {
            return getRegionKey(region.getDeclaredRegion(), args);
        }
    }

    /**
     * Returns the current JCS property value with optional replacement arguments <b>for a given
     * region</b>. The number of arguments must be the same as configured parameter size (i.e.
     * {@link #numberOfParams}).
     * 
     * @param region
     * @param args
     * @return
     */
    String getRegionKey(String region, Object... args) {
        Assert.ARGUMENTS.notEmpty(region, "No region is given");
        Assert.ARGUMENTS.notNull(args, "Replacement arguments is null");
        Assert.ARGUMENTS.isTrue(args.length == numberOfParams,
            "Declared number of parameters (value={0}) "
                    + "does not match with actual arguments (length={1})", numberOfParams,
            args.length);

        String actualPattern = StringUtils.replaceOnce(pattern, "${region}", ".region." + region);
        return formatPropertyKey(actualPattern, args);
    }

    private String formatPropertyKey(String actualPattern, Object... args) {
        if (ArrayUtils.isEmpty(args)) {
            return actualPattern;
        }
        return MessageFormat.format(actualPattern, args);
    }
}
