package com.csc.integral.aop.workflow.processor;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import com.csc.integral.aop.workflow.output.UserWorkflowWritter;
import com.csc.integral.aop.workflow.output.WorkflowWriterFactory;
import com.csc.integral.aop.workflow.utils.reflect.AppVarsReflectUtils;

public class ProcessorUtils {
	/**
	 * get the join point method
	 * 
	 * @param pjp
	 * @return
	 */
	public static Method getJoinPointMethod(final ProceedingJoinPoint pjp) {
		
		return ((MethodSignature) pjp.getSignature()).getMethod();
	}
	

	public static String getTargetClassName(final ProceedingJoinPoint pjp) {
		String className = "";
		if (pjp.getTarget() != null) {
			className = pjp.getTarget().getClass().getName();
		} else {
			Method mt = getJoinPointMethod(pjp);
			if(mt != null) {
				className = mt.getClass().getName();
			} 
		}
		return className;
	}

	public static String getTargetSimpleClassName(final ProceedingJoinPoint pjp) {
		String sinpleClassName = "";
		if (pjp.getTarget() != null) {
			sinpleClassName = pjp.getTarget().getClass().getSimpleName();
		} else {
			Method mt = getJoinPointMethod(pjp);
			if(mt != null) {
				sinpleClassName = mt.getClass().getSimpleName();
			} 
		}
		return sinpleClassName;
	}

	public static Object getTargetProperty(String propertyName,
			final ProceedingJoinPoint pjp) {
		// TODO: get a property of target obj
		return null;
	}

	public static UserWorkflowWritter getUserWorkflowWritter() {
		Object userId = AppVarsReflectUtils
				.getFieldValue(AppVarsReflectUtils.LOGGER_ON_USER);
		if (userId == null) {
			userId = new String("Unknow");
		}
		UserWorkflowWritter wt = WorkflowWriterFactory.getInstance().getWriter(
				userId.toString());
		return wt;
	}
	
	public static void closeUserWorkflowWritter(UserWorkflowWritter wr) {
		WorkflowWriterFactory.getInstance().removeWriter(wr.getUserId());
		
	}
}
