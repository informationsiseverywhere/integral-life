package com.csc.integral.aop.monitoring;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.utils.functor.VoidCommand;

/**
 * A simple aspect which registers all AspectJ-woven classes. Client applications then can query
 * woven classes by using {@link #isClassWoven(Class)} or {@link #isWoven(Object)} method. Clients
 * can also register system-wide listeners to be notified when a woven method is being executed.
 * Registration is done via {@link #addListeners(VoidCommand...)}.
 * 
 * @author ntruong5
 */
@Aspect
public final class WovenClassMonitor {
    private static final Logger LOGGER = LoggerFactory.getLogger(WovenClassMonitor.class);

    private static volatile Set<String> s_wovenClassNames = new HashSet<String>(89);

    /**
     * All listeners for the event when woven method is executed.
     */
    private static List<VoidCommand> s_monitoringListeners = new LinkedList<VoidCommand>();

    /**
     * An aspect method to register woven class into current monitored classes.
     * 
     * @param pjp
     * @return
     * @throws Throwable
     * @deprecated Used only by AspectJ.
     */
    @Deprecated
    @Around(value = "execution(* *(..))", argNames = "pjp")
    public Object registerWovenClass(ProceedingJoinPoint pjp) throws Throwable {
        try {
            doRegisterWovenClass(pjp);
        } catch (Throwable t) {
            LOGGER.warn("Error while registering woven class", t);
        }

        return pjp.proceed();
    }

    private void doRegisterWovenClass(ProceedingJoinPoint pjp) {
        if (pjp.getTarget() != null) {
            String classname = pjp.getTarget().getClass().getName();
            if (s_wovenClassNames.add(classname)) {
                LOGGER.debug("Class '{}' has been woven by AspectJ Weaver (LTW).", classname);
            }
            notifyListeners(classname, pjp.getSignature().getName());
        }
    }

    private static void notifyListeners(String classname, String methodName) {
        if (!s_monitoringListeners.isEmpty()) {
            List<VoidCommand> cmds;
            synchronized (s_monitoringListeners) {
                cmds = new ArrayList<VoidCommand>(s_monitoringListeners);
            }
            for (VoidCommand cmd : cmds) {
                cmd.execute(classname, methodName);
            }
        }
    }

    /**
     * Removes all listeners.
     */
    public static void clearListeners() {
        if (!s_monitoringListeners.isEmpty()) {
            synchronized (s_monitoringListeners) {
                s_monitoringListeners.clear();
            }
        }
    }

    /**
     * Add given listeners into the current monitor.
     * <p>
     * <b>Note</b>: Each listener added into the {@link WovenClassMonitor} would listen every method
     * invocations. Be careful about the performance!
     * 
     * @param listeners
     */
    public static void addListeners(VoidCommand... listeners) {
        if (listeners != null && listeners.length > 0) {
            synchronized (s_monitoringListeners) {
                for (VoidCommand l : listeners) {
                    s_monitoringListeners.add(l);
                }
            }
        }
    }

    /**
     * Checks if a given class has been woven by LTW AspectJ.
     * 
     * @param cls
     * @return
     */
    public static boolean isClassWoven(Class<?> cls) {
        return cls != null && s_wovenClassNames.contains(cls.getName());
    }

    /**
     * Checks if a given object has been woven by LTW AspectJ.
     * 
     * @param source
     *            Source object. If this object is a {@link Class}, the overload
     *            {@link #isClassWoven(Class)} is used instead.
     * @return
     */
    public static boolean isWoven(Object source) {
        if (source instanceof Class<?>) {
            return isClassWoven((Class<?>) source);
        }
        return source != null && isClassWoven(source.getClass());
    }
}
