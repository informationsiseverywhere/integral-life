package com.csc.integral.aop.cache.impl.keybuilder;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.csc.integral.aop.cache.impl.aspect.HibernateCacheObjectAspect;
import com.csc.integral.aop.cache.interceptor.impl.HibernateCacheMethodInterceptor;
import com.csc.integral.utils.ReflectionUtils;

/**
 * An extension of {@link DefaultCacheKeyBuilder} which adds some specifics for the "hash-code"
 * algorithm. This builder would be used inside {@link HibernateCacheObjectAspect} or
 * {@link HibernateCacheMethodInterceptor}.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class HibernateEntityCacheKeyBuilder extends DefaultCacheKeyBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    protected String computeObjectIdentifier(Object arg) {
        if (ReflectionUtils.mayBeHibernateEntity(arg)) {
            int hashCode = HashCodeBuilder.reflectionHashCode(arg, false);
            return Integer.toHexString(hashCode);
        }
        return super.computeObjectIdentifier(arg);
    }
}
