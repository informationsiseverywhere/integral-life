package com.csc.integral.aop.cache.impl.keybuilder;

import java.util.concurrent.Callable;

import com.csc.integral.aop.cache.impl.aspect.HibernateCacheObjectAspect;
import com.csc.integral.aop.cache.interceptor.impl.HibernateCacheMethodInterceptor;
import com.csc.integral.utils.DoubleCheckLockingUtils;

/**
 * A factory class for {@link CacheKeyBuilder} implementation lookup.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheKeyBuilderFactory {
    private static CacheKeyBuilderFactory s_instance = new CacheKeyBuilderFactory();

    // the single instance in the scope of this factory
    private volatile CacheKeyBuilder cacheKeyBuilder;
    private volatile CacheKeyBuilder entityCacheKeyBuilder;

    private CacheKeyBuilderFactory() {
        // to hide
    }

    /**
     * Returns the single instance of this class for use.
     * 
     * @return
     */
    public static CacheKeyBuilderFactory getInstance() {
        return s_instance;
    }

    /**
     * Returns a single {@link CacheKeyBuilder} implementation. Invoking this method several times
     * doesn't impact on the performance since it's a singleton instance.
     * 
     * @return
     */
    public CacheKeyBuilder getBuilder() {
        return cacheKeyBuilder = DoubleCheckLockingUtils.getSingleton(cacheKeyBuilder, this,
            new Callable<CacheKeyBuilder>() {
                @Override
                public CacheKeyBuilder call() throws Exception {
                    return new DefaultCacheKeyBuilder();
                }
            });
    }

    /**
     * Returns a single {@link CacheKeyBuilder} implementation. Invoking this method several times
     * doesn't impact on the performance since it's a singleton instance.
     * 
     * @param callerObject
     *            An owner object which calls this factory method.
     * @return
     */
    public CacheKeyBuilder getBuilder(Object callerObject) {
        if (callerObject instanceof HibernateCacheObjectAspect
                || callerObject instanceof HibernateCacheMethodInterceptor) {
            return entityCacheKeyBuilder = DoubleCheckLockingUtils.getSingleton(
                entityCacheKeyBuilder, this, new Callable<CacheKeyBuilder>() {
                    @Override
                    public CacheKeyBuilder call() throws Exception {
                        return new HibernateEntityCacheKeyBuilder();
                    }
                });
        }

        return getBuilder();
    }
}
