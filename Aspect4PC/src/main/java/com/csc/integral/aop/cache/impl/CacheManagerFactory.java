package com.csc.integral.aop.cache.impl;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.CacheManager;
import com.csc.integral.aop.cache.exception.CacheInitializationException;
import com.csc.integral.aop.cache.impl.jcs.JcsCacheManager;
import com.csc.integral.utils.DoubleCheckLockingUtils;
import com.csc.integral.utils.EnumUtils;
import com.csc.integral.utils.ReflectionUtils;

/**
 * A factory class for {@link CacheManager} implementation selection.
 * <p>
 * To get an instance of this factory, call {@link #getInstance()}. After that, method
 * {@link #getCacheManager()} can be used to return a single instance of the {@link CacheManager}
 * under the scope of this factory.
 * <p>
 * <b>Remark</b>: Invoking {@link #getCacheManager()} several times would not impact on the
 * performance of the application.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class CacheManagerFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheManagerFactory.class);

    /*
     * set the default style for toString(). This style affects all ToStringBuilder.reflectToString
     * methods
     */
    static {
        ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /*
     * A static map of (cache provider enum value, cache provider implementation class) pairs.
     */
    private static final Map<CacheProvider, Class<?>> CACHE_PROVIDER_IMPL_CLASS_MAP;
    static {
        CACHE_PROVIDER_IMPL_CLASS_MAP = new EnumMap<CacheProvider, Class<?>>(CacheProvider.class);

        // put all mappings here
        CACHE_PROVIDER_IMPL_CLASS_MAP.put(CacheProvider.JCS, JcsCacheManager.class);
    }

    private static CacheManagerFactory s_instance = new CacheManagerFactory();

    // the single cache manager in the scope of this factory
    private volatile CacheManager cacheManager;

    private CacheManagerFactory() {
        // to hide
    }

    /**
     * Returns the single instance of this class for use.
     * 
     * @return
     */
    public static CacheManagerFactory getInstance() {
        return s_instance;
    }

    /**
     * Returns a single {@link CacheManager} implementation base on the cache configuration.
     * Invoking this method several times doesn't impact on the performance since it's a singleton
     * instance.
     * 
     * @return
     */
    public CacheManager getCacheManager() {
        return cacheManager = DoubleCheckLockingUtils.getSingleton(cacheManager, this,
            new Callable<CacheManager>() {
                @Override
                public CacheManager call() throws Exception {
                    return initCacheManager();
                }
            });
    }

    private CacheManager initCacheManager() {
        LOGGER.debug("Initializing the cache manager ..");

        Class<?> implClass = loadImplementationClass();
        LOGGER.debug("Cache manager implementation class: {}", implClass);

        Object cacheManager = createCacheManager(implClass); // all configs should be loaded here
        LOGGER.debug("Created cache manager instance: {}", cacheManager);
        
        if (LOGGER.isDebugEnabled() && cacheManager instanceof AbstractCacheManager) {
            AbstractCacheManager cm = (AbstractCacheManager) cacheManager;
            LOGGER.debug("Cache manager initial context: {}", cm.getContext());
        }

        LOGGER.info("Cache manager is initialized successfully.");
        return CacheManager.class.cast(cacheManager);
    }

    private Object createCacheManager(Class<?> implClass) {
        Object cacheManager = null;
        try {
            cacheManager = implClass.newInstance();
        } catch (InstantiationException e) {
            throw new CacheInitializationException(
                    "Cannot instantiate an instance of class ''{0}''", e, implClass.getName());
        } catch (IllegalAccessException e) {
            throw new CacheInitializationException(
                    "Class ''{0}'' does not have any default constructor", e, implClass.getName());
        }
        return cacheManager;
    }

    private Class<?> loadImplementationClass() {
        String providerConfig = ConfigurationLoader.getInstance().getCacheConfig("provider");

        CacheProvider cacheProvider = EnumUtils.valueOf(CacheProvider.class, providerConfig);
        if (cacheProvider == null) {
            cacheProvider = CacheProvider.CUSTOM;
        }

        Class<?> implClass = null;
        switch (cacheProvider) {
            case CUSTOM:
                implClass = ReflectionUtils.loadClass(providerConfig, getClass());
                break;
            case JCS:
            default:
                implClass = JcsCacheManager.class;
                break;
        }

        return implClass;
    }
}
