package com.csc.integral.aop.cache.impl.utils;

import com.csc.integral.utils.ReflectionUtils;

/**
 * Constants for all classes which belong to the Quipoz COBOL Framework and are useful in the
 * Aspect4Pc module. The intention is that the Aspect4Pc should NOT depend at compile-time on any
 * POLISY module.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum ExternalClassConstant {

    /**
     * Full classname for <code>ExternalData</code> class.
     */
    ExternalData("com.quipoz.COBOLFramework.datatype.ExternalData"),

    /**
     * Full classname for <code>FixedLengthStringData</code> class.
     */
    FixedLengthStringData("com.quipoz.framework.datatype.FixedLengthStringData"),

    /**
     * Full classname for <code>COBOLExitProgramException</code> class.
     */
    COBOLExitProgramException(
            "com.quipoz.COBOLFramework.util.COBOLConvCodeModel$COBOLExitProgramException"),

    /**
     * Full classname for <code>COBOLConvCodeModel</code> class.
     */
    COBOLConvCodeModel("com.quipoz.COBOLFramework.util.COBOLConvCodeModelv"),

    /**
     * Full classname for <code>GenericDAO</code> class.
     */
    GenericDAO("com.csc.smart400framework.dataaccess.dao.GenericDAO"),
	
    /**
     * Full classname for <code>BaseDAO</code> class.
     */
    BaseDAO("com.csc.smart400framework.dataaccess.dao.BaseDAO"),
    
    /**
     * Full classname for <code>ItemTableDAM</code> class.
     */
    ItemTableDAM("com.csc.smart.dataaccess.ItemTableDAM");
	
	

    private final String className;

    private ExternalClassConstant(String fullClassName) {
        this.className = fullClassName;
    }

    /**
     * Get the actual class name in the Quipoz COBOL framework.
     * 
     * @return Returns the className.
     */
    public String getClassName() {
        return className;
    }

    /**
     * Checks if this external class constant represents a superclass or a superinterface of a given
     * checking class.
     * 
     * @param anotherClass
     * @return
     */
    public boolean isAssignableFrom(Class<?> anotherClass) {
        return ReflectionUtils.mayBeAssignableFrom(anotherClass, className);
    }
}
