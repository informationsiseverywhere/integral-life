package com.csc.integral.aop.workflow.output;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Stack;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.core.FileAppender;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;

/**
 * UserLoggerWriter. Using log4j to write the output. SiftingAppender is used to
 * separate (or sift) logging according to userId
 * http://logback.qos.ch/manual/appenders.html#SiftingAppender
 * 
 * @author tphan25
 * 
 */
public class UserLoggerWriter extends UserWorkflowWritter {
	
	private static final String COMMENT_PREFIX = "	//";
	private static final String FIELD_PREFIX = "	-";
	private static final String PARAM_PREFIX = "	+";
	private static final String METHOD_PREFIX = "=>";
	private static final String IO_PREFIX = "	IO:";
	private static final String EXCEPTION_PREFIX = "	Exception:";

	private Logger logger;
	
	private String msgPrefix = "";
	private int methodTab = 1;
	private boolean bStartProgram;
	private int indexSequenceFile = 0;
	private int nProgramClassCalled;
	private Stack<String> methodStack = new Stack<String>();

	protected UserLoggerWriter(String userId) {
		super(userId);
		init();
		}
	
	private void init(){
		newNextOutFile();
	}
	
	@Override
	public void writeComment(String comment) {
		comment = "	//" + comment;
		writeLog(comment);
	}

	@Override
	public void startMethod(Object target, Method method, Object[] args) {
		synchronized (this) {
			methodStack.push(getMethodAsString(target, method, args));
			if (target != null && method != null) {
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.append("[" + methodTab + "]" + METHOD_PREFIX);
				strBuilder.append(getMethodAsString(target, method, args));
				// write msg
				writeLog(strBuilder.toString());
			}
			// format stack trace. tab
			methodTab++;
			msgPrefix += " ";
		}
	}

	@Override
	public void endMethod(Object target, Method method, Object[] args) {
		// format stack trace. shift tab
		synchronized (this) {
			if (msgPrefix.length() > 0) {
				methodTab--;
				msgPrefix = msgPrefix.substring(0, msgPrefix.length() - 1);
			}
		}
	}
	
	@Override
	public void startProgram(String programName) {
		synchronized (this) {
			if (nProgramClassCalled >= config.getMaxProgramClassPerFile()) {
				this.newNextOutFile();
			}
			bStartProgram = true;
			nProgramClassCalled++;
			methodStack.push(programName);
			writeLog("Start Program:" + programName);
		}
		
	}

	@Override
	public void endProgram(String programName) {
		synchronized (this) {
			bStartProgram = false;
			writeLog("End Program:" + programName);
		}
	}
	

	@Override
	public void writeField(String fieldName, String value) {
		synchronized (this) {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(FIELD_PREFIX);
			strBuilder.append(fieldName + "=");
			strBuilder.append(value);
			writeLog(strBuilder.toString());
		}
	}

	@Override
	public void writeField(String note, String fieldName, String value) {
		synchronized (this) {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(FIELD_PREFIX + note + ":");
			strBuilder.append(fieldName + "=");
			strBuilder.append(value);
			writeLog(strBuilder.toString());
		}
	}

	@Override
	public void writeParam(String paramName, String value) {
		synchronized (this) {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(PARAM_PREFIX);
			strBuilder.append(paramName + "=");
			strBuilder.append(value);
			writeLog(strBuilder.toString());
		}
	}

	@Override
	public void writeParam(String note, String paramName, String value) {
		synchronized (this) {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(PARAM_PREFIX + note + ":");
			strBuilder.append(paramName + "=");
			strBuilder.append(value);
			writeLog(strBuilder.toString());
		}
	}
	
	public void cleanUp() {
		try {
            logger.getAppender(userId).stop();
            logger.detachAppender(userId);
		}
		catch ( Exception ex) {
			logger.error("finalize Method Exception :",ex);
		}
		
	}	
	
	private void writeLog(String msg) {
		Date logTime = new Date();
		String threadName = Thread.currentThread().getName();
		writeLog(dateFormat.format(logTime), threadName, userId,
				msg);
	}
	
	private void writeLog(String time, String threadName, String userId,
			String line) {
		if(isFilter()) {
			return;
		}
		line = msgPrefix + line;
		StringBuilder strBuilder = new StringBuilder();
		if (config.isPrintCallReference()) {
			strBuilder.append("[" + time + "] ");
			strBuilder.append("[" + threadName + "] ");
			strBuilder.append("[" + userId + "] ");
		}
		strBuilder.append(line);
		logger.debug(strBuilder.toString());
	}

	@Override
	public void preEndMethod(Object target, Method method, Object[] args) {
		formatPreEndMethod(getMethodAsString(target, method, args));
	}

	@Override
	public void preEndProgram(String programName) {
		formatPreEndMethod(programName);
	}
	
	@Override
	public void startIOExecute(Object target, Method method, Object[] args) {
		synchronized (this) {
			methodStack.push(getMethodAsString(target, method, args));
			if (target != null && method != null) {
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.append("[" + methodTab + "]" + IO_PREFIX);
				strBuilder.append(getMethodAsString(target, method, args));
				// write msg
				writeLog(strBuilder.toString());
			}
		// format stack trace. tab
			methodTab++;
			msgPrefix += " ";
		}
	}

	@Override
	public void endIOExecute(Object target, Method method, Object[] args) {
		// format stack trace. shift tab
		synchronized (this) {
			if (msgPrefix.length() > 0) {
				methodTab--;
				msgPrefix = msgPrefix.substring(0, msgPrefix.length() - 1);
			}
		}
	}
	
	public void captureException(Throwable t, Object target, Method method, Object[] args) {
		synchronized (this) {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(EXCEPTION_PREFIX);
			strBuilder.append(t.getClass().getSimpleName());
			String msg = (t.getMessage()!=null)?t.getMessage(): "";
			strBuilder.append(". " + msg);
			strBuilder.append(". stacktrace='");
			StackTraceElement[] stacks = t.getStackTrace();
			for (int i = 0; i < stacks.length && i < MAX_STACK_TRACE_LENGHT; i++) {
				strBuilder.append(stacks[i].toString() + ", ");
			}
			strBuilder.append("'");
			writeLog(strBuilder.toString());
		}
	}
	
	
	
	@Override
	public void newNextOutFile() {
		synchronized (this) {
			if(logger != null) {
                logger.getAppender(userId).stop();
                logger.detachAppender(userId);
			}
			// init logger for the user
            this.logger = (Logger) LoggerFactory.getLogger(UserLoggerWriter.class.getName() + "." + userId);
            FileAppender appender = new FileAppender();
            indexSequenceFile++;
            nProgramClassCalled = 0;
            
            LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            appender.setContext(loggerContext);
            // outDir/userId/userId-workflow<indexSequenceFile>.xml
            String logFile = WorkflowCaptureCfg.getInstance().getOutputDir() + File.separator + userId
                    + File.separator + userId + "-workflow" + indexSequenceFile
                    + ".log";
            appender.setFile(logFile);
            appender.setAppend(false);
            PatternLayoutEncoder encoder = new PatternLayoutEncoder();
            encoder.setPattern("%message%n");
            encoder.setContext(loggerContext);
            appender.setEncoder(encoder);
            appender.setName(userId);
            ThresholdFilter thresholdFilter = new ThresholdFilter();
            thresholdFilter.setLevel(Level.DEBUG.toString());
            appender.addFilter(thresholdFilter);
            encoder.start();
            appender.start();
//          appender.activateOptions();
			logger.setLevel(Level.DEBUG);
			logger.addAppender(appender);
			methodTab = 1;
			msgPrefix = "";
		} 
	}
	
	/**
	 * @param mt
	 *            method
	 */
	private void formatPreEndMethod(String mt) {
		// close method which open but not be closed before
		// endMethod must associate with startMethod
		synchronized (this) {
			while (!methodStack.isEmpty()) {
				String mtInStack = methodStack.pop();
				if (mtInStack != null && mtInStack.equals(mt)) {
					// endMethod associate with startMethod
					break;
				} else {
					// method which open but not be closed
					// format stack trace. shift tab
					if (msgPrefix.length() > 0) {
						methodTab--;
						msgPrefix = msgPrefix.substring(0, msgPrefix.length() - 1);
					}
				}
			}
		}
	}
	
	@Override
	protected boolean isFilter() {
		if (config.isStackTraceForProgramOnly()) {
			if (bStartProgram) {
				return false;
			}
			return super.isFilter();
		} else {
			return false;
		}
		
	}
}
