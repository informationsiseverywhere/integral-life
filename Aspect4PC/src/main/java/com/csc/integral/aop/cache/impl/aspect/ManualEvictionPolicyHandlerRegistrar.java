package com.csc.integral.aop.cache.impl.aspect;

import java.lang.reflect.InvocationTargetException;
import java.util.Observable;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.impl.ManualEvictionPolicyHandler;
import com.csc.integral.utils.assertion.Assert;

/**
 * An internal utility class which registers manual eviction policy as the observer of an
 * intercepting object.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class ManualEvictionPolicyHandlerRegistrar {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ManualEvictionPolicyHandlerRegistrar.class);
    private static final String OBSERVABLE_GET_METHOD = "getObservable";

    /**
     * Registers the singleton {@link ManualEvictionPolicyHandler} as an observer for notifications
     * sent from a given intercepting observable object either DIRECTLY or INDIRECTLY.
     * <p>
     * <b>Note</b>: There is at most 1 instance of {@link ManualEvictionPolicyHandler} can be
     * registered as an observer for the given intercepting object.
     * 
     * @param interceptingObject
     *            Object being intercepted. If this object is not an observable object (DIRECTLY or
     *            INDIRECTLY), this method does nothing.
     */
    public static void registerAsObserver(Object interceptingObject) {
        Assert.ARGUMENTS.notNull(interceptingObject, "Intercepting object is NULL");
        Observable observable = getActualObservable(interceptingObject);
        if (observable != null) {
            observable.addObserver(ManualEvictionPolicyHandler.getInstance());
        }
    }

    /**
     * Unregisters the singleton {@link ManualEvictionPolicyHandler} from a given intercepting
     * observable object either DIRECTLY or INDIRECTLY.
     * 
     * @param interceptingObject
     *            Object being intercepted. If this object is not an observable object (DIRECTLY or
     *            INDIRECTLY), this method does nothing.
     */
    public static void unregisterObserver(Object interceptingObject) {
        Assert.ARGUMENTS.notNull(interceptingObject, "Intercepting object is NULL");
        Observable observable = getActualObservable(interceptingObject);
        if (observable != null) {
            observable.deleteObserver(ManualEvictionPolicyHandler.getInstance());
        }
    }

    static Observable getActualObservable(Object interceptingObject) {
        Observable observable = null;

        if (interceptingObject instanceof Observable) {
            observable = (Observable) interceptingObject;
        } else {
            try {
                Object result = MethodUtils.invokeMethod(interceptingObject, OBSERVABLE_GET_METHOD,
                    null);
                if (result instanceof Observable) {
                    observable = (Observable) result;
                }
            } catch (NoSuchMethodException e) {
                LOGGER.debug("Method '{}.{}' was not found", interceptingObject.getClass()
                        .getName(), OBSERVABLE_GET_METHOD);
            } catch (IllegalAccessException e) {
                LOGGER.debug("Method '{}.{}' is not accessible", interceptingObject.getClass()
                        .getName(), OBSERVABLE_GET_METHOD);
            } catch (InvocationTargetException e) {
                LOGGER.debug("Error while running method ''{1}.{0}''", interceptingObject
                        .getClass().getName(), OBSERVABLE_GET_METHOD);
            }
        }

        if (observable == null) {
            LOGGER.debug(
                "Intercepting object (class={}) is not an observable object (directly/indirectly)",
                interceptingObject.getClass().getName());
        }

        return observable;
    }
}
