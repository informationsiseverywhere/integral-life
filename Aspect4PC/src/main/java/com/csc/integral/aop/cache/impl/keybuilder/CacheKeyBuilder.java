package com.csc.integral.aop.cache.impl.keybuilder;

import com.csc.integral.aop.cache.CacheKey;

/**
 * Interface for the construction of {@link CacheKey} objects from an invocation.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface CacheKeyBuilder {

    /**
     * Builds a unique {@link CacheKey} object from any of the given information including:
     * <ul>
     * <li>The intercepting object's type.
     * <li>The invoking method name.
     * <li>The invoking method arguments.
     * </ul>
     * 
     * @param interceptingObjectType
     *            Type of the current intercepting object.
     * @param methodName
     *            Intercepted method name.
     * @param invocationArgs
     *            All method's invocation arguments.
     * @return A constructed cache key.
     */
    CacheKey build(Class<?> interceptingObjectType, String methodName, Object[] invocationArgs);
}
