package com.csc.integral.aop.cache.impl.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.interceptor.impl.AbstractCacheableMethodInterceptor;

/**
 * An extension for Hibernate cache object type.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
@Aspect
public class HibernateCacheObjectAspect extends AbstractCacheObjectAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(HibernateCacheObjectAspect.class);

    /**
     * Intercepts around any invocation from a method <code>find</code> and intercepting object is
     * an instance of <code>com.csc.smart.dataaccess.dao.GenericDAO</code>.
     * 
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* find*(..)) && target(com.csc.smart400framework.dataaccess.dao.ItempfDAO)", argNames = "pjp")
    public Object interceptFindMethod(final ProceedingJoinPoint pjp) throws Throwable {
    	LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }
	
	@Around(value = "execution(* load*(..)) && target(com.csc.smart400framework.dataaccess.dao.ItemDAO)", argNames = "pjp")
    public Object interceptLoadMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }	
	
	@Around(value = "execution(* get*(..)) && target(com.csc.smart400framework.dataaccess.dao.ItemDAO)", argNames = "pjp")
    public Object interceptGetAllMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }	
	
	@Around(value = "execution(* find*(..)) && target(com.csc.smart400framework.dataaccess.dao.ItemDAO)", argNames = "pjp")
    public Object interceptFindAllMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }	
	
	@Around(value = "execution(* read*(..)) && target(com.csc.smart400framework.dataaccess.dao.ItemDAO)", argNames = "pjp")
    public Object interceptReadAllMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }		
	
	@Around(value = "execution(* get*(..)) && target(com.csc.smart400framework.dataaccess.dao.DescDAO)", argNames = "pjp")
    public Object interceptGetMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }
	
	@Around(value = "execution(* searc*(..)) && target(com.csc.smart400framework.dataaccess.dao.DescDAO)", argNames = "pjp")
    public Object interceptSearcMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }	
	
	@Around(value = "execution(* rewrt*(..)) && target(com.csc.smart400framework.dataaccess.SmartFileCode)", argNames = "pjp")
    @Deprecated
    public Object interceptSmartFileCodeExecuteMethod(final ProceedingJoinPoint pjp) throws Throwable {
		LOGGER.info("<<<<<<<<<< Caching Initiated >>>>>>>>>>");
        return doOnReceiveInvocation(pjp, null);
    }	
}
