package com.csc.integral.aop.cache.impl;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.csc.integral.aop.cache.impl.utils.CacheManagerUtils;

/**
 * Configurations for a cache region.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheRegionConfiguration {
    private String declaredRegion;
    private int maxObjects = 10;
    private Integer maxTimeToLiveSeconds = 300;
    private EvictionPolicy evictionPolicy = EvictionPolicy.LRU;
    private PersistenceType persistenceType = PersistenceType.MEM_ONLY;
    private CacheScope scope = CacheScope.APPLICATION;

    CacheRegionConfiguration(String declaredRegion) {
        this.declaredRegion = StringUtils.trimToEmpty(declaredRegion);
    }

    /**
     * Checks if this region configuration is for the default region.
     * 
     * @return
     */
    public boolean isDefault() {
        return CacheConfiguration.DEFAULT_REGION.equalsIgnoreCase(declaredRegion);
    }

    /**
     * Returns the declared region name. A declared region name doesn't include the scope.
     * 
     * @return
     */
    public String getDeclaredRegion() {
        return declaredRegion;
    }

    /**
     * Returns the actual region name of the region stored in the cache store.
     * 
     * @return
     */
    public String getActualRegion() {
        if (scope == CacheScope.SESSION) {
            return CacheManagerUtils.getActualSessionRegion(declaredRegion);
        } else {
            return declaredRegion;
        }
    }

    /**
     * Returns current value of {@link #evictionPolicy}.
     * 
     * @return the {@link #evictionPolicy}.
     */
    public EvictionPolicy getEvictionPolicy() {
        return evictionPolicy;
    }

    /**
     * Returns current value of {@link #persistenceType}.
     * 
     * @return the {@link #persistenceType}.
     */
    public PersistenceType getPersistenceType() {
        return persistenceType;
    }

    /**
     * Returns current value of {@link #scope}.
     * 
     * @return the {@link #scope}.
     */
    public CacheScope getScope() {
        return scope;
    }

    void setDeclaredRegion(String declaredRegion) {
        this.declaredRegion = declaredRegion;
    }

    void setEvictionPolicy(EvictionPolicy val) {
        if (val != null) {
            this.evictionPolicy = val;
        }
    }

    void setPersistenceType(PersistenceType val) {
        if (val != null) {
            this.persistenceType = val;
        }
    }

    void setScope(CacheScope val) {
        if (val != null) {
            this.scope = val;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CacheRegionConfiguration) {
            return ObjectUtils.equals(declaredRegion,
                ((CacheRegionConfiguration) obj).getDeclaredRegion());
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return declaredRegion.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * @return the maxObjects
     */
    public int getMaxObjects() {
        return maxObjects;
    }

    /**
     * @param maxObjects
     *            the maxObjects to set
     */
    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    /**
     * @return the maxTimeToLiveSeconds
     */
    public Integer getMaxTimeToLiveSeconds() {
        return maxTimeToLiveSeconds;
    }

    /**
     * @param maxTimeToLiveSeconds
     *            the maxTimeToLiveSeconds to set
     */
    public void setMaxTimeToLiveSeconds(Integer maxTimeToLiveSeconds) {
        this.maxTimeToLiveSeconds = maxTimeToLiveSeconds;
    }
}
