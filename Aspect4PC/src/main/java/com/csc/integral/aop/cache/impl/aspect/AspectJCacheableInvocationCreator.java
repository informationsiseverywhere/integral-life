package com.csc.integral.aop.cache.impl.aspect;

import java.util.concurrent.Callable;

import org.aspectj.lang.ProceedingJoinPoint;

import com.csc.integral.aop.cache.interceptor.CacheableInvocation;

/**
 * A simple class which creates and fills up a new {@link CacheableInvocation} object from a given
 * AspectJ's {@link ProceedingJoinPoint} object.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class AspectJCacheableInvocationCreator {

    /**
     * Creates a new {@link CacheableInvocation} object from a given AspectJ's
     * {@link ProceedingJoinPoint} object.
     * 
     * @param pjp
     * @param args
     *            All intercepting invocation arguments. This argument is preferred.
     * @param proceedingCallable
     * @return
     */
    public CacheableInvocation create(ProceedingJoinPoint pjp, Object[] args,
            Callable<Object> proceedingCallable) {
        if (args == null) {
            args = pjp.getArgs();
        }

        return new CacheableInvocation(pjp.getSignature().getDeclaringType(), pjp.getSignature()
                .getModifiers(), pjp.getSignature().getName(), pjp.getTarget(), proceedingCallable,
                args);
    }
}
