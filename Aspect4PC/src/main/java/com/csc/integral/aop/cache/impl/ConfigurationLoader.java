package com.csc.integral.aop.cache.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.csc.integral.aop.cache.exception.CacheInitializationException;
import com.csc.integral.utils.DoubleCheckLockingUtils;

/**
 * A simple, utility loader class for configurations from properties files.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class ConfigurationLoader {
    private static final String CONFIG_PREFIX = "csc.integral.aop.cache";
    private static final String CACHE_PROPERTIES_FILE = "cache.properties";
    private static final String MAIN_PROPERTIES_FILE = "aspect4pc-cache-main.properties";
    
    /**
     * A constant for the prefix of all configurations in the module.
     */
    public static final String CONFIG_PREFIX_WITH_DOT = CONFIG_PREFIX + ".";

    private static ConfigurationLoader s_instance = new ConfigurationLoader();
    private static String s_cachePropertiesFile;
    private static String s_mainPropertiesFile;

    private Properties cacheProperties;
    private Properties mainProperties;

    /**
     * Sets a custom cache properties file. This method can be called once. Subsequent calls are
     * ignored.
     * 
     * @param file
     *            The new value to set.
     */
    public static void setCachePropertiesFile(String file) {
        if (!StringUtils.isEmpty(file) && StringUtils.isEmpty(s_cachePropertiesFile)) {
            s_cachePropertiesFile = file;
        }
    }

    /**
     * Sets a custom main properties file. This method can be called once. Subsequent calls are
     * ignored.
     * 
     * @param file
     *            The new value to set.
     */
    public static void setMainPropertiesFile(String file) {
        if (!StringUtils.isEmpty(file) && StringUtils.isEmpty(s_mainPropertiesFile)) {
            s_mainPropertiesFile = file;
        }
    }

    private ConfigurationLoader() {
        // to hide
    }

    /**
     * Get the current value for instance
     * 
     * @return Returns the instance.
     */
    public static ConfigurationLoader getInstance() {
        return s_instance;
    }

    /**
     * Returns all main configurations starting with a prefix key.
     * 
     * @param prefixKey
     * @return
     */
    public Map<String, String> getMainConfigsStartingWith(String prefixKey) {
        prefixKey = prependConfigPrefix(prefixKey);
        Properties properties = getMainProperties();
        Map<String, String> resultMap = new HashMap<String, String>();
        for (Entry<Object, Object> e : properties.entrySet()) {
            String key = (String) e.getKey();
            if (StringUtils.startsWith(key, prefixKey)) {
                resultMap.put(key, (String) e.getValue());
            }
        }
        return resultMap;
    }

    /**
     * Returns a cache configuration by its key.
     * 
     * @param key
     *            Configuration key. This key can be either full (e.g.
     *            "csc.integral.aop.cache.regions") or compact (e.g. "regions"). This 'compact'
     *            mode, the prefix {@link #CONFIG_PREFIX} is prepended automatically.
     * @return
     */
    public String getCacheConfig(String key) {
        key = prependConfigPrefix(key);
        return getCacheProperties().getProperty(key);
    }

    /**
     * Returns a main configuration by its key.
     * 
     * @param key
     *            Configuration key. This key can be either full or compact. This 'compact' mode,
     *            the prefix {@link #CONFIG_PREFIX} is prepended automatically.
     * @return
     */
    public String getMainConfig(String key) {
        key = prependConfigPrefix(key);
        return getMainProperties().getProperty(key);
    }

    private Properties getCacheProperties() {
        return cacheProperties = DoubleCheckLockingUtils.getSingleton(cacheProperties, this,
            new Callable<Properties>() {
                @Override
                public Properties call() throws Exception {
                    return loadCacheConfigurations();
                }
            });
    }

    private Properties getMainProperties() {
        return mainProperties = DoubleCheckLockingUtils.getSingleton(mainProperties, this,
            new Callable<Properties>() {
                @Override
                public Properties call() throws Exception {
                    return loadMainConfigurations();
                }
            });
    }

    private String prependConfigPrefix(String key) {
        if (!StringUtils.startsWith(key, CONFIG_PREFIX)) {
            key = CONFIG_PREFIX_WITH_DOT + key;
        }
        return key;
    }

    Properties loadCacheConfigurations() {
        String file = StringUtils.isEmpty(s_cachePropertiesFile) ? CACHE_PROPERTIES_FILE : s_cachePropertiesFile;
        return loadConfigurationsFromClasspathResource(file);
    }

    Properties loadMainConfigurations() {
        String file = StringUtils.isEmpty(s_mainPropertiesFile) ? MAIN_PROPERTIES_FILE : s_mainPropertiesFile;
        return loadConfigurationsFromClasspathResource(file);
    }

    private Properties loadConfigurationsFromClasspathResource(String classpathResource) {
        Properties properties = new Properties();
        InputStream is = null;
        try {
            is = this.getClass().getClassLoader().getResourceAsStream(classpathResource);
            properties.load(is);
        } catch (IOException e) {
            throw new CacheInitializationException(
                    "Error while loading cache properties file ''{0}''", e, classpathResource);
        } finally {
            IOUtils.closeQuietly(is);
        }
        return properties;
    }
}
