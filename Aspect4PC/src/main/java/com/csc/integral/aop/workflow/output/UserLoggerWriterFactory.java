package com.csc.integral.aop.workflow.output;

import java.util.HashMap;

public class UserLoggerWriterFactory extends WorkflowWriterFactory {
	
	
	HashMap<String, UserWorkflowWritter> writterMap = new HashMap<String, UserWorkflowWritter>();
	
	protected UserLoggerWriterFactory() {
	}
	
	@Override
	public UserWorkflowWritter getWriter(String userId) {
		UserWorkflowWritter writer = writterMap.get(userId);
			synchronized (UserLoggerWriterFactory.class) {
				writer = writterMap.get(userId);
				if (writer == null) {
					writer = new UserLoggerWriter(userId);
					writterMap.put(userId, writer);
				}
			}
		return writer;
	}

	@Override
	public void removeWriter(String userId) {
		writterMap.remove(userId);
	}

}
