package com.csc.integral.aop.workflow.aspect;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.processor.ProcessorUtils;
import com.csc.integral.aop.workflow.processor.WorkflowProcessor;

/**
 * @author tphan25
 * 
 */
public abstract class AbstractWorkflowAspect {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractWorkflowAspect.class);

	protected WorkflowProcessor processor;
	
	/**
	 * around method
	 * 
	 * @param pjp
	 * @return pjp.proceed object
	 * @throws Throwable
	 */
	public Object aroundMethod(final ProceedingJoinPoint pjp) throws Throwable {

		Object returnObj;
		// capture before call target
		try {
			beforeMethod(pjp);
		} catch (Exception ex) {
			LOGGER.error("beforeMethod Exception. Method: {}. {}",
					ProcessorUtils.getJoinPointMethod(pjp).getName(),
					ExceptionUtils.getStackTrace(ex));
		}

		// process target
		try {
			returnObj = pjp.proceed();
		} catch ( Throwable t) {
			try {
				processor.captureException(t, pjp);
			} catch ( Exception processEx) {
				LOGGER.error("captureException Exception. Method: {}. {}",
						ProcessorUtils.getJoinPointMethod(pjp).getName(),
						ExceptionUtils.getStackTrace(processEx));
			}
			throw t;
		}

		// capture after call target
		try {
			afterMethod(pjp, returnObj);
		} catch (Exception ex) {
			LOGGER.error("afterMethod Exception. Method: {}. {}",
					ProcessorUtils.getJoinPointMethod(pjp).getName(),
					ExceptionUtils.getStackTrace(ex));
		}

		return returnObj;
	}

	/**
	 * before join point process
	 * 
	 * @param pjp
	 */
	protected abstract void beforeMethod(final ProceedingJoinPoint pjp);

	/**
	 * after join point process
	 * 
	 * @param pjp
	 * @param returnObj
	 *            : return object when call pjp.process()
	 */
	protected abstract void afterMethod(final ProceedingJoinPoint pjp,
			Object returnObj);
}
