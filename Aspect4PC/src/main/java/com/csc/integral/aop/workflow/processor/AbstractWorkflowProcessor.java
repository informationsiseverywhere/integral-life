package com.csc.integral.aop.workflow.processor;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;
import com.csc.integral.aop.workflow.output.UserWorkflowWritter;
import com.csc.integral.aop.workflow.output.WorkflowWriterFactory;

public abstract class AbstractWorkflowProcessor implements WorkflowProcessor{

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractWorkflowProcessor.class);
	protected ProcessorFilter filter = new ProcessorFilter();
	protected WorkflowCaptureCfg config = WorkflowCaptureCfg.getInstance();
	
	@Override
	public void beforeMethod(final ProceedingJoinPoint pjp) {
		if(filter.isFileter(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs())) {
			return; // filtered
		}
		processBeforeMethod(pjp);
	}
	
	@Override
	public void afterMethod(final ProceedingJoinPoint pjp) {
		if(filter.isFileter(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs())) {
			return; // filtered
		}
		processAfterMethod(pjp);
	}
	
	@Override
	public void captureException(Throwable t, final ProceedingJoinPoint pjp) {
		if(filter.isFileter(t)) {
			return; // filtered
		}		
		UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();		
		wr.captureException(t, pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs());
		LOGGER.warn("Captured an exception. userID= {}. exception={}",
				wr.getUserId(), t.getClass().getName());
		if("ThreadDeath".equals(t.getClass().getSimpleName())) {
			// thread death exception, usually when user log out 
			//CodeRunner.run()
			if("CodeRunner".equals(ProcessorUtils.getTargetSimpleClassName(pjp))) {
				Method mt = ProcessorUtils.getJoinPointMethod(pjp);
				if ("run".equals(mt.getName())) {
					WorkflowWriterFactory factory = WorkflowWriterFactory.getInstance();
					factory.removeWriter(wr.getUserId());
				}
			}
		}
	}
	
	public abstract void processBeforeMethod(final ProceedingJoinPoint pjp);
	
	public abstract void processAfterMethod(final ProceedingJoinPoint pjp);
	
	
}
