package com.csc.integral.aop.cache.impl;

/**
 * An interface for any object supporting the retrieval and management of {@link CacheContext}s.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface CacheContextSupport {

    /**
     * Returns a {@link CacheContext} supported by this class.
     * 
     * @return
     */
    CacheContext getContext();
}
