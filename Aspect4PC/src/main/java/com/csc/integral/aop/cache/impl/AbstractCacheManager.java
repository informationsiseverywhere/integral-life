package com.csc.integral.aop.cache.impl;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.cache.CacheKey;
import com.csc.integral.aop.cache.CacheManager;
import com.csc.integral.aop.cache.CacheRegionCleaner;
import com.csc.integral.utils.EnumUtils;
import com.csc.integral.utils.assertion.Assert;

/**
 * Base implementation for a cache manager.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public abstract class AbstractCacheManager implements CacheManager, CacheContextSupport,
        CacheRegionCleaner {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCacheManager.class);

    protected CacheContext context;

    /**
     * Constructs this manager by:
     * <ul>
     * <li>Initialize the cache context.
     * <li>Load main configurations.
     * <li>Load cache configurations.
     * <li>Initializes cache provider configurations from current context's configurations.
     * </ul>
     */
    protected AbstractCacheManager() {
        this.context = new CacheContext();

        loadMainConfigurations();
        loadCacheConfigurations();

        initializeProviderConfigurations();
    }

    /**
     * Initializes configured provider configurations by converting Aspect4Pc's configs to
     * provider-specific configs.
     */
    protected abstract void initializeProviderConfigurations();

    private void loadFieldInfos(String fieldConfigPrefix, String removedPrefix,
            Map<String, CacheProgramFieldInfo> destFieldMap) {

        Map<String, String> fieldMap = ConfigurationLoader.getInstance()
                .getMainConfigsStartingWith(fieldConfigPrefix);
        for (Entry<String, String> e : fieldMap.entrySet()) {
            CacheProgramFieldInfo field = new CacheProgramFieldInfo();
            field.setFieldPath(e.getValue());
            field.setClassName(StringUtils.removeStart(e.getKey(), removedPrefix));
            destFieldMap.put(field.getClassName(), field);
        }
    }

    /**
     * Loads main configurations and fills into the {@link #context} variable.
     */
    private void loadMainConfigurations() {

        // load all main configurations related to cache program fields
        CacheProgramFieldType[] fieldTypes = CacheProgramFieldType.values();
        for (CacheProgramFieldType fieldType : fieldTypes) {
            loadFieldInfos(fieldType.getIndicator(), fieldType.getPrefix(),
                context.getFieldMap(fieldType));
        }

        LOGGER.debug("Main configurations have been loaded");
    }

    private void loadCacheConfigurations() {
        loadCacheProvider();
        loadCacheRegionConfigurations();
        LOGGER.debug("Cache configurations have been loaded");
    }

    private void loadCacheRegionConfigurations() {
        Set<CacheRegionConfiguration> regionConfigs = this.context.getCacheConfiguration()
                .getRegionConfigs();
        String regionAsString = ConfigurationLoader.getInstance().getCacheConfig("regions");

        // add custom regions into current config
        if (!StringUtils.isEmpty(regionAsString)) {
            String[] regions = StringUtils.split(regionAsString, ',');
            for (String region : regions) {
                CacheRegionConfiguration regionConfig = new CacheRegionConfiguration(region);
                regionConfigs.add(regionConfig);
            }
        }

        initializeCacheRegionConfigs(regionConfigs);
    }

    // fill all region-specific info
    private void initializeCacheRegionConfigs(Set<CacheRegionConfiguration> regionConfigs) {
        String key = null, val = null;
        for (CacheRegionConfiguration regionConf : regionConfigs) {

            // load max objects cached
            key = regionConf.getDeclaredRegion() + ".maxObjects";
            val = ConfigurationLoader.getInstance().getCacheConfig(key);
            regionConf.setMaxObjects(NumberUtils.toInt(val, regionConf.getMaxObjects()));

            // load max life seconds for object cache
            key = regionConf.getDeclaredRegion() + ".maxTimeToLiveSeconds";
            val = ConfigurationLoader.getInstance().getCacheConfig(key);
            regionConf.setMaxTimeToLiveSeconds(NumberUtils.toInt(val,
                regionConf.getMaxTimeToLiveSeconds()));

            // load eviction policy
            key = regionConf.getDeclaredRegion() + ".evictPolicy";
            val = ConfigurationLoader.getInstance().getCacheConfig(key);
            regionConf.setEvictionPolicy(EnumUtils.valueOf(EvictionPolicy.class, val));

            // load persistence type
            key = regionConf.getDeclaredRegion() + ".persistenceType";
            val = ConfigurationLoader.getInstance().getCacheConfig(key);
            regionConf.setPersistenceType(EnumUtils.valueOf(PersistenceType.class, val));

            // load scope
            key = regionConf.getDeclaredRegion() + ".scope";
            val = ConfigurationLoader.getInstance().getCacheConfig(key);
            regionConf.setScope(EnumUtils.valueOf(CacheScope.class, val));
        }
    }

    private void loadCacheProvider() {
        String provider = ConfigurationLoader.getInstance().getCacheConfig("provider");

        CacheProvider cacheProvider = EnumUtils.valueOf(CacheProvider.class, provider);
        if (cacheProvider == null) {
            cacheProvider = CacheProvider.CUSTOM;
        }
        this.context.getCacheConfiguration().setCacheProvider(cacheProvider);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CacheContext getContext() {
        return context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disposeRegion(String declaredRegion) {
        ensureRegion(declaredRegion);

        String actualRegion = context.getCacheConfiguration()
                .getRegionConfiguration(declaredRegion).getActualRegion();
        doDisposeRegion(actualRegion);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(String declaredRegion, CacheKey key, Object cacheObject) {
        ensureRegion(declaredRegion);
        ensureCacheKey(key);

        if (cacheObject != null) {
            CacheRegionConfiguration crc = context.getCacheConfiguration().getRegionConfiguration(
                declaredRegion);
            if (!crc.isDefault()) {
                String actualRegion = crc.getActualRegion();
                doPut(actualRegion, key, cacheObject);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(String declaredRegion, CacheKey key) {
        ensureRegion(declaredRegion);
        ensureCacheKey(key);

        String actualRegion = context.getCacheConfiguration()
                .getRegionConfiguration(declaredRegion).getActualRegion();
        return doGet(actualRegion, key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final <V> V get(String declaredRegion, CacheKey key, Class<V> valueClass) {
        Validate.notNull(valueClass, "Cache value target class is missing");
        Object value = get(declaredRegion, key);
        if (value != null) {
            return valueClass.cast(value);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(String declaredRegion, CacheKey key) {
        ensureRegion(declaredRegion);
        ensureCacheKey(key);

        String actualRegion = context.getCacheConfiguration()
                .getRegionConfiguration(declaredRegion).getActualRegion();
        doRemove(actualRegion, key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int removeAll(String declaredRegion, CacheKey partialKey) {
        ensureRegion(declaredRegion);

        ensureCacheKey(partialKey);
        Assert.ARGUMENTS.notNull(partialKey.getClassName(),
            "Attribute className is missing. Given key is: {}", partialKey);

        String actualRegion = context.getCacheConfiguration()
                .getRegionConfiguration(declaredRegion).getActualRegion();

        return doRemoveAll(actualRegion, partialKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasValidEvictPolicy(String region) {
        ensureRegion(region);
        CacheRegionConfiguration regionConfig = this.context.getCacheConfiguration()
                .getRegionConfiguration(region);

        EvictionPolicy configuredEvictPolicy = regionConfig.getEvictionPolicy();
        EvictionPolicy[] supportedEvictPolicies = this.context.getCacheConfiguration()
                .getCacheProvider().getSupportedEvictionPolicies();

        return ArrayUtils.contains(supportedEvictPolicies, configuredEvictPolicy);
    }

    /**
     * Does the REGION DISPOSE operation internally.
     * 
     * @param actualRegion
     * @see #disposeRegion(String)
     */
    protected abstract void doDisposeRegion(String actualRegion);

    /**
     * Does the PUT operation internally. See {@link #put(String, CacheKey, Object)} for more
     * information about the PUT operation.
     * 
     * @param actualRegion
     *            The actual region which composes of declared region and current user ID.
     * @param key
     * @param cacheObject
     * @see #put(String, CacheKey, Object)
     */
    protected abstract void doPut(String actualRegion, CacheKey key, Object cacheObject);

    /**
     * Does the GET operation. See {@link #get(String, CacheKey)} for more information about the GET
     * operation.
     * 
     * @param actualRegion
     *            The actual region which composes of declared region and current user ID.
     * @param key
     * @return
     */
    protected abstract Object doGet(String actualRegion, CacheKey key);

    /**
     * Does the EVICT operation. See {@link #remove(String, CacheKey)} for more information about
     * the EVICT operation.
     * 
     * @param actualRegion
     *            The actual region which composes of declared region and current user ID.
     * @param key
     */
    protected abstract void doRemove(String actualRegion, CacheKey key);

    /**
     * Does the EVICT operation for several cache objects sharing the same given partial key.
     * 
     * @param actualRegion
     *            The actual region which composes of declared region and current user ID.
     * @param partialKey
     * @return The number of cache objects removed.
     */
    protected abstract int doRemoveAll(String actualRegion, CacheKey partialKey);

    void ensureRegion(String region) {
        Assert.ARGUMENTS.notEmpty(region, "Region is required (actual value: \"{0}\"", region);
        Assert.ARGUMENTS.isTrue(this.context.getCacheConfiguration().regionExists(region),
            "Region ''{0}'' does not exist", region);
    }

    void ensureCacheKey(CacheKey key) {
        Assert.ARGUMENTS.notNull(key, "Cache key is null");
    }
}
