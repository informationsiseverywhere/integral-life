package com.csc.integral.aop.performance.perf4j;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * An enum for all supported pattern which is used to build Perf4J profiled tag.
 * 
 * @author ntruong5
 */
public enum ProfiledTagPattern {

    /**
     * Default pattern for Perf4J profiled tag in statistic log file (CSV, LOG).
     * <p>
     * The pattern is:
     * "T<i>threadId</i>&lt;<i>threadName</i>&gt;.<i>executingClass</i>.<i>executingMethod</i>".
     */
    DEFAULT("T{0}<{1}>.{2}.{3}({4})", true),

    /**
     * Default pattern for Perf4J profiled tag in statistic CSV log file.
     * <p>
     * The pattern is:
     * "T<i>threadId</i>&lt;<i>threadName</i>&gt;,<i>executingClass</i>,<i>executingMethod</i>".
     */
    CSV_ORIENTED("T{0}<{1}>,{2},{3}({4})", true),

    /**
     * A simple pattern for Perf4J profiled tag in statistic log file (CSV, LOG) which composes only
     * executing class and method name.
     * <p>
     * The pattern is: "<i>executingClass</i>.<i>executingMethod</i>".
     */
    SIMPLE("{0}.{1}({2})", false),

    /**
     * The pattern which is the same as specified by Perf4J. Actually, for this pattern, no custom
     * formatting operation is performed.
     */
    COMPATIBLE(null, false);

    private static final String UNKNOWN_TAG_FRAGMENT = "(unknown)";

    private String pattern;
    private boolean groupedByThread;

    private ProfiledTagPattern(String pattern, boolean groupedByThread) {
        this.pattern = pattern;
        this.groupedByThread = groupedByThread;
    }

    /**
     * Creates a tag name for profiled line in a Perf4J log file according to given executing
     * object, method name and current pre-defined pattern.
     * 
     * @param executingObject
     *            The profiled object. If it's <code>null</code>, "(unknown)" is logged.
     * @param methodName
     *            The profiled/executing method name. If it's <code>null</code>, "(unknown)" is
     *            logged.
     * @param executingArgs
     *            All arguments being executed.
     * @param paramTypeIncluded <code>true</code> if execution argument types must be 
     * @return Tag text formatted from given data or <code>null</code> if this pattern is
     *         {@link #COMPATIBLE}.
     */
    public String createTag(Object executingObject, String methodName, Object[] executingArgs,
            boolean paramTypeIncluded) {
        String tag = null;
        if (this != COMPATIBLE) {
            String objectClass = executingObject == null ? UNKNOWN_TAG_FRAGMENT : executingObject
                    .getClass().getSimpleName();
            methodName = methodName == null ? UNKNOWN_TAG_FRAGMENT : methodName;
            List<String> argTypeNames = getTypeSimpleNames(executingArgs);
            tag = doCreateTag(methodName, objectClass, argTypeNames, paramTypeIncluded);
        } else {
            // it's not necessary to format since Perf4J already does this
        }

        return tag;
    }

    private List<String> getTypeSimpleNames(Object[] executingArgs) {
        if (ArrayUtils.isEmpty(executingArgs)) {
            return Collections.emptyList();
        } else {
            LinkedList<String> results = new LinkedList<String>();
            for (Object arg : executingArgs) {
                results.add(arg != null ? arg.getClass().getSimpleName() : "na");
            }
            return results;
        }
    }

    private String doCreateTag(String methodName, String objectClass,
            List<String> argTypeSimpleNames, boolean paramTypeIncluded) {
        String tag;
        List<Object> params = new ArrayList<Object>();

        params.add(objectClass);
        params.add(methodName);

        if (groupedByThread) {
            params.add(0, Thread.currentThread().getId());
            params.add(1, Thread.currentThread().getName());
        }

        if (paramTypeIncluded && !argTypeSimpleNames.isEmpty()) {
            String argText = StringUtils.join(argTypeSimpleNames, ';');
            argText = StringUtils.replace(argText, "[]", "*");
            params.add(argText);
        } else {
            params.add("");
        }

        tag = MessageFormat.format(pattern, params.toArray(new Object[params.size()]));
        return tag;
    }

    /**
     * Returns an enum value corresponding to a given pattern value in String presentation.
     * 
     * @param patternValue
     *            Pattern enum value in text format.
     * @param caseInsensitive
     *            <code>true</code> for case-ignored matching, <code>false</code> for exact
     *            matching.
     * @return Matching {@link ProfiledTagPattern} enum value or <code>null</code> if nothing is
     *         matched.
     */
    public static ProfiledTagPattern valueOfOrNull(String patternValue, boolean caseInsensitive) {
        if (StringUtils.isBlank(patternValue)) {
            return null;
        }
        for (ProfiledTagPattern e : values()) {
            if ((caseInsensitive && e.name().equalsIgnoreCase(patternValue))
                    || (!caseInsensitive && e.name().equals(patternValue))) {
                return e;
            }
        }
        return null;
    }

    /**
     * Returns an enum value corresponding to a given pattern value in String presentation. The
     * matching process is done <i>case-insensitive</i>.
     * 
     * @param patternValue
     *            Pattern enum value in text format.
     * @return Matching {@link ProfiledTagPattern} enum value or <code>null</code> if nothing is
     *         matched.
     */
    public static ProfiledTagPattern valueOfOrNull(String patternValue) {
        return valueOfOrNull(patternValue, true);
    }
}
