package com.csc.integral.aop.workflow.output;

import java.lang.reflect.Method;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.WorkflowCaptureCfg;

public abstract class UserWorkflowWritter {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserWorkflowWritter.class);
	protected static int MAX_STACK_TRACE_LENGHT = 10;
	
	protected FastDateFormat dateFormat = FastDateFormat.getInstance("yyyy/MM/dd HH:mm:ss");
	
	protected static WorkflowCaptureCfg config = WorkflowCaptureCfg.getInstance();
	
	protected String userId = "unknow";
	
	/**
	 * The writer should get from it's factory
	 * @param userId
	 */
	protected UserWorkflowWritter(String userId) {
		this.userId = userId;
	}

	public abstract void writeComment(String info);
	
	public abstract void writeField(String fieldName, String value);
	
	public abstract void writeField(String note, String fieldName, String value);
	
	public abstract void writeParam(String paramName, String value);
	
	public abstract void writeParam(String note, String paramName, String value);
	
	public abstract void startMethod(Object target, Method method, Object[] args);
	
	public abstract void preEndMethod(Object target, Method method, Object[] args);

	public abstract void endMethod(Object target, Method method, Object[] args);
	
	public abstract void startProgram(String programName);
	
	public abstract void preEndProgram(String programName);
	
	public abstract void endProgram(String programName);
	
	public abstract void startIOExecute(Object target, Method method, Object[] args);
	
	public abstract void endIOExecute(Object target, Method method, Object[] args);
	
	public abstract void captureException(Throwable t, Object target, Method method, Object[] args);
	
	/**
	 * close current out file and create a new one
	 */
	public abstract void newNextOutFile();
	
	//public abstract void startWorkflow(String msg);
	
	
	/**
	 * get method as string. Include parameter type
	 * 
	 * @param pjp
	 * @return
	 */
	public static String getMethodAsString(Object target, Method method, Object[] args) {
		
		StringBuilder methodBuilder = new StringBuilder();
		if(target != null) {
			methodBuilder.append(target.getClass().getSimpleName() + ".");
		} else {
			methodBuilder.append(method.getDeclaringClass().getSimpleName() + ".");
		}
		if (method != null) {
			methodBuilder.append(method.getName() + "(");
			if (args != null) {
				for (int i = 0; i < args.length; i++) {
					if (args[i] != null) {
						if (i > 0) {
							methodBuilder.append(", ");
						}
						methodBuilder
								.append(args[i].getClass().getSimpleName());
					}
				}
			}
			methodBuilder.append(")");
		}
		return methodBuilder.toString();
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	protected boolean isFilter() {
		return true;
	}
}
