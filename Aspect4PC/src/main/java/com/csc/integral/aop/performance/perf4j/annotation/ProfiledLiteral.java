package com.csc.integral.aop.performance.perf4j.annotation;

import org.perf4j.StopWatch;
import org.perf4j.aop.AbstractTimingAspect;
import org.perf4j.aop.Profiled;

import com.csc.integral.utils.annotation.AnnotationLiteral;

/**
 * A literal implementation of {@link Profiled} annotation. This implementation is useful when an
 * instance of {@link Profiled} is necessary for client code to use {@link AbstractTimingAspect}'s
 * API.
 * 
 * @author ntruong5
 */
public final class ProfiledLiteral extends AnnotationLiteral<Profiled> implements Profiled {
    private static final long serialVersionUID = 1L;

    /**
     * A default instance in 'singleton' scope. The reason is: this default, literal instance is
     * reused.
     */
    private static final Profiled s_defaultInstance = new ProfiledLiteral();

    private final String tag;
    private final String message;
    private String logger;
    private final String level;
    private final boolean el;
    private final boolean logFailuresSeparately;
    private final long timeThreshold;
    private final boolean normalAndSlowSuffixesEnabled;

    /**
     * Returns the default instance in 'singleton' scope.
     * 
     * @return
     */
    public static Profiled getDefaultLiteral() {
        return s_defaultInstance;
    }
    
    public static Profiled newLiteral(String loggerSuffix) {
        ProfiledLiteral ins = new ProfiledLiteral();
        ins.logger = StopWatch.DEFAULT_LOGGER_NAME + "." + loggerSuffix;
        return ins;
    }

    /**
     * Constructs this literal annotation object with default settings. These settings are the same
     * as what are specified in {@link Profiled} annotation.
     */
    ProfiledLiteral() {
        this.tag = Profiled.DEFAULT_TAG_NAME;
        this.el = true;
        this.level = "INFO";
        this.logFailuresSeparately = false;
        this.logger = StopWatch.DEFAULT_LOGGER_NAME;
        this.message = "";
        this.normalAndSlowSuffixesEnabled = false;
        this.timeThreshold = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String tag() {
        return this.tag;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String message() {
        return this.message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String logger() {
        return this.logger;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String level() {
        return this.level;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean el() {
        return this.el;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean logFailuresSeparately() {
        return this.logFailuresSeparately;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long timeThreshold() {
        return this.timeThreshold;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean normalAndSlowSuffixesEnabled() {
        return this.normalAndSlowSuffixesEnabled;
    }
}
