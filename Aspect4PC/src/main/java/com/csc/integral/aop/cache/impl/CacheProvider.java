package com.csc.integral.aop.cache.impl;

/**
 * All possible cache providers supported by the cache system.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum CacheProvider {

    /**
     * Provider: Apache JCS.
     */
    JCS(EvictionPolicy.LRU, EvictionPolicy.MRU),
    
    /**
     * Custom provider.
     */
    CUSTOM(EvictionPolicy.LRU, EvictionPolicy.MRU, EvictionPolicy.FIFO);

    private EvictionPolicy[] evictionPolicies;

    private CacheProvider(EvictionPolicy... evictPolicies) {
        this.evictionPolicies = evictPolicies;
    }

    /**
     * Returns all eviction policies supported by current provider.
     * 
     * @return
     */
    public EvictionPolicy[] getSupportedEvictionPolicies() {
        return evictionPolicies;
    }
}
