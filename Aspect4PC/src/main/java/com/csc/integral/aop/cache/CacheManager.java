package com.csc.integral.aop.cache;

import com.csc.integral.aop.cache.impl.PersistenceType;
import com.csc.integral.utils.exception.NotSerializableRTException;

/**
 * The central interface for a cache manager. This manager is responsible for storing, getting and
 * evicting cache objects.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface CacheManager {

    /**
     * Puts a new cache object into current cache store under a given region. The cache object is
     * identified (inside the given region) by a unique key.
     * 
     * @param declaredRegion
     *            An existing cache region. The given region must be already defined or a runtime
     *            exception is raised.
     * @param key
     *            A unique key (required).
     * @param cacheObject
     *            Cached value. If it's <code>null</code>, nothing is put. If there is already a
     *            cache object, the new object overwrites the existing one.
     * @throws NotSerializableRTException
     *             if persistent type is not {@link PersistenceType#MEM_ONLY} but cache object is
     *             not serializable.
     */
    void put(String declaredRegion, CacheKey key, Object cacheObject);

    /**
     * Retrieves a cache object under a given region using a given unique key.
     * 
     * @param declaredRegion
     *            An existing cache region. The given region must be already defined or a runtime
     *            exception is raised.
     * @param key
     *            A unique key (required).
     * @return Cached value or <code>null</code> if there is no cache object under given input
     *         arguments.
     * @throws NotSerializableRTException
     *             if persistent type is not {@link PersistenceType#MEM_ONLY} but cache object is
     *             not serializable.
     */
    Object get(String declaredRegion, CacheKey key);

    /**
     * A convenience method to retrieves a cache object under a given region using a given unique
     * key. It's the same as {@link #get(String, Comparable)} except that the cached object will be
     * casted to a given generic type.
     * 
     * @param <V>
     * @param declaredRegion
     *            An existing cache region. The given region must be already defined or a runtime
     *            exception is raised.
     * @param key
     *            A unique key (required).
     * @param targetClass
     *            An expected class of the cached object (required).
     * @return Cached value or <code>null</code> if there is no cache object under given input
     *         arguments.
     * @throws NotSerializableRTException
     *             if persistent type is not {@link PersistenceType#MEM_ONLY} but cache object is
     *             not serializable.
     */
    <V> V get(String declaredRegion, CacheKey key, Class<V> targetClass);

    /**
     * Removes (evicts) a cache object from the cache store.
     * 
     * @param region
     *            An existing cache region. The given region must be already defined or a runtime
     *            exception is raised.
     * @param key
     *            A unique key (required).
     */
    void remove(String region, CacheKey key);

    /**
     * Removes (evicts) all cache objects from the cache store matching a given partial cache key.
     * 
     * @param region
     *            An existing cache region. The given region must be already defined or a runtime
     *            exception is raised.
     * @param partialKey
     *            An example key for the search. All cache objects whose keys are similar to this
     *            key will be removed.
     * @return The number of cache object removed.
     */
    int removeAll(String region, CacheKey partialKey);

    /**
     * Checks if a given region has a valid eviction policy.
     * <p>
     * A region can define an eviction policy (e.g. LRU). However, whether or not a policy is
     * supported depends on the configured cache provider. If an eviction policy is not supported by
     * a cache provider, a runtime exception would be raised. To prevent this from happening, this
     * method can be used to check this status.
     * 
     * @param declaredRegion
     *            An existing cache region. The given region must be already defined or a runtime
     *            exception is raised.
     * @return <code>true</code> if the cache provider under this cache manager supports the
     *         configured eviction policy. Otherwise, return <code>false</code>.
     */
    boolean hasValidEvictPolicy(String declaredRegion);
}
