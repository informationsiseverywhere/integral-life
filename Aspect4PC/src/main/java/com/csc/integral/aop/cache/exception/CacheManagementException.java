package com.csc.integral.aop.cache.exception;

import com.csc.integral.utils.exception.BaseRTException;

/**
 * An exception when there is an error while the cache manager performs its operations.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheManagementException extends BaseRTException {
    private static final long serialVersionUID = 1L;

    /**
     * @param message
     * @param messageParams
     */
    public CacheManagementException(String message, Object... messageParams) {
        super(message, messageParams);
    }

    /**
     * @param message
     * @param cause
     * @param messageParams
     */
    public CacheManagementException(String message, Throwable cause, Object... messageParams) {
        super(message, cause, messageParams);
    }

    /**
     * @param cause
     */
    public CacheManagementException(Throwable cause) {
        super(cause);
    }
}
