package com.csc.integral.aop.workflow.processor;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.output.UserWorkflowWritter;
import com.csc.integral.utils.ReflectionUtils;

public class IOCaptureProcessor extends AbstractWorkflowProcessor{
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IOCaptureProcessor.class);
	@Override
	public void processBeforeMethod(ProceedingJoinPoint pjp) {
		UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();
		wr.startIOExecute(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs());
		if(config.isPrintIOAcessDetail()) {
			wr.writeComment("Function=" + getIOFunction(pjp.getTarget())
					+ ", Condition:" + getIOCondition(pjp.getTarget()));
		}
	}

	@Override
	public void processAfterMethod(ProceedingJoinPoint pjp) {
		UserWorkflowWritter wr = ProcessorUtils.getUserWorkflowWritter();
		wr.preEndMethod(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs());
		if(config.isPrintIOAcessDetail()) {
			wr.writeComment("Status=" + getIOStatus(pjp.getTarget()) + ", ReturnObject:"
					+ getReturnObject(pjp.getTarget()));
		}
		wr.endIOExecute(pjp.getTarget(), ProcessorUtils.getJoinPointMethod(pjp), pjp.getArgs());
	}
	
	private String getIOFunction(Object targetObj) {
		try {
			Object value = ReflectionUtils.getFieldValue(targetObj, "function");
			if (value != null) {
				return value.toString();
			} else {
				return "";
			}
		} catch (Exception ex) {
			LOGGER.error("getIOFunction exception", ex);
		}
		return "";
	}
	
	private String getIOStatus(Object targetObj) {
		try {
			Object value = ReflectionUtils.getFieldValue(targetObj, "statuz");
			if (value != null) {
				return value.toString();
			} else {
				return "";
			}
		} catch (Exception ex) {
			LOGGER.error("getIOFunction exception", ex);
		}
		return "";
	}

	
	private String getIOCondition(Object target) {
		return getReturnObject(target);
	}

	private String getReturnObject(Object target) {
		StringBuilder strBuilder = new StringBuilder();

		try {
			Class<?> smartFileCodeClass = ReflectionUtils
					.resolveClass("com.csc.smart400framework.dataaccess.SmartFileCode");
			Object unique_number = ReflectionUtils.getFieldValue(
					smartFileCodeClass, target, "unique_number");
			if (unique_number != null && unique_number.toString().trim().length() > 0) {
				strBuilder.append("UNIQUE_NUMBER=" + unique_number.toString() + ", ");
			}
			Object keyColumns = ReflectionUtils.getFieldValue(
					smartFileCodeClass, target, "KEYCOLUMNS");
			if (keyColumns != null) {
				String[] keys = ((String) keyColumns).trim().split(",");
				for (String key : keys) {
					Object field = ReflectionUtils.invokeMethod(
							smartFileCodeClass, target, "getFieldByColumnName",
							new Class<?>[] { String.class }, key.toLowerCase().trim());
					if (field != null) {
						strBuilder.append(key + "='" + field + "', ");
					}
				}
			} else {
				// don't have keys, try to get datakey
				Object dataKey = ReflectionUtils.invokeMethod(
						smartFileCodeClass, target, "getDataKey",
						new Class<?>[] {});
				strBuilder.append("DataKey='" + dataKey + "' ");
			}
		} catch (Exception ex) {
			LOGGER.error("getReturnObject exception", ex);
		}
		return strBuilder.toString();
	}
}
