package com.csc.integral.aop.workflow.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.processor.BatchProgramCaptureProcessor;

@Aspect
public class BatchProgramCaptureAspect extends AbstractWorkflowAspect {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BatchProgramCaptureAspect.class);

	private static final String ASPECT_BATCH_PROGRAM_MAINLINE = "execution(* com.csc.*.batchprograms.B*.mainline(..)) ";
	
	public BatchProgramCaptureAspect() {
		processor = new BatchProgramCaptureProcessor();
	}

	@Around(value = "execution(* com.csc.*.batchprograms.B*.mainline(..)) ", argNames = "pjp")
	public Object aroundMainline(final ProceedingJoinPoint pjp)
			throws Throwable {
		return super.aroundMethod(pjp);
	}

	@Override
	protected void beforeMethod(ProceedingJoinPoint pjp) {
		processor.beforeMethod(pjp);
	}

	@Override
	protected void afterMethod(ProceedingJoinPoint pjp, Object returnObj) {
		processor.afterMethod(pjp);
	}
}
