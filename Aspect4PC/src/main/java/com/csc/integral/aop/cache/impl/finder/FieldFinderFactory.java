package com.csc.integral.aop.cache.impl.finder;

import java.util.concurrent.Callable;

import com.csc.integral.utils.DoubleCheckLockingUtils;

/**
 * This is the factory class which builds/chooses a correct implementation of finders according to
 * configurations. Finders include: {@link CacheProgramFieldFinder}.
 * <p>
 * This class is used only for program cache object type.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class FieldFinderFactory {
    private static FieldFinderFactory s_instance = new FieldFinderFactory();

    private volatile CacheProgramFieldFinder returnFieldFinder;

    private FieldFinderFactory() {
        // to hide
    }

    /**
     * Get the current value for instance
     * 
     * @return Returns the instance.
     */
    public static FieldFinderFactory getInstance() {
        return s_instance;
    }

    /**
     * Returns the single instance of the {@link CacheProgramFieldFinder} in the scope of this
     * factory.
     * 
     * @return Returns the returnFieldFinder.
     */
    public CacheProgramFieldFinder getCacheProgramFieldFinder() {
        return returnFieldFinder = DoubleCheckLockingUtils.getSingleton(returnFieldFinder, this,
            new Callable<CacheProgramFieldFinder>() {
                @Override
                public CacheProgramFieldFinder call() throws Exception {
                    return new DefaultCacheProgramFieldFinder();
                }
            });
    }
}
