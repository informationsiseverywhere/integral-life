package com.csc.integral.aop.cache.impl;

/**
 * All possible scopes for a cache region.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum CacheScope {
    
    /**
     * Application-wide scope.
     */
    APPLICATION,
    
    /**
     * User-session scope.
     */
    SESSION
}
