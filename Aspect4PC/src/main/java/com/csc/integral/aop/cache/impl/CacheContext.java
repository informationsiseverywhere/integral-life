package com.csc.integral.aop.cache.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.MDC;

import com.csc.integral.utils.assertion.Assert;

/**
 * A context object for a cache manager. This context contains cache configurations, main
 * configurations and other information.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class CacheContext {
    private static final String CURRENT_USER_ID = "currentUser";
    public static final String CURRENT_HTTP_SESSION_ID = "currentHttpSession";

    // non-region-based configurations
    private CacheConfiguration cacheConfiguration;

    private Map<CacheProgramFieldType, Map<String, CacheProgramFieldInfo>> fieldMap;

    CacheContext() {
        cacheConfiguration = new CacheConfiguration();
        fieldMap = new HashMap<CacheProgramFieldType, Map<String, CacheProgramFieldInfo>>();

        // Init all configuration maps:
        // - A map of (configured class name, return field) pairs.
        // - A map of (configured class name, status field) pairs.
        // - A map of (configured class name, identifier field) pairs.
        for (CacheProgramFieldType fieldType : CacheProgramFieldType.values()) {
            fieldMap.put(fieldType, new HashMap<String, CacheProgramFieldInfo>());
        }
    }

    /**
     * Get the current value for cacheConfiguration
     * 
     * @return Returns the cacheConfiguration.
     */
    public CacheConfiguration getCacheConfiguration() {
        return cacheConfiguration;
    }

    /**
     * Returns a field map by its type.
     * 
     * @param type
     *            Field type, required.
     * @return
     */
    public Map<String, CacheProgramFieldInfo> getFieldMap(CacheProgramFieldType type) {
        Assert.ARGUMENTS.notNull(type, "No field type is given");
        return fieldMap.get(type);
    }

    /**
     * Returns the current user ID from the SLF4J's {@link MDC} (thread-safe).
     * 
     * @param notificationMap
     * @return Current user ID (login session).
     * @throws IllegalStateException
     *             if the current user was not found in MDC.
     */
    public static String getCurrentUserId() {
        return getCurrentUserId(null);
    }

    /**
     * Returns the current user ID from EITHER the given notification map (returned after registered
     * observers had been notified when an aspect handles an invocation) OR the SLF4J's {@link MDC}
     * (thread-safe).
     * 
     * @param notificationMap
     * @return Current user ID (login session).
     * @throws IllegalStateException
     *             if the current user was not passed from client application or was not found in
     *             MDC.
     */
    public static String getCurrentUserId(Map<String, Object> notificationMap) {
        String id = null;
        if (notificationMap != null) {
            id = (String) notificationMap.get(CURRENT_USER_ID);
        }
        if (StringUtils.isEmpty(id)) {
            id = MDC.get(CURRENT_USER_ID);
        }

        if (StringUtils.isEmpty(id)) {
            Assert.STATE.notEmpty(id,
                "The current user was not passed from client application or MDC");
        }
        return id;
    }

    /**
     * Returns the current HTTP SESSION ID from EITHER the given notification map (returned after
     * registered observers had been notified when an aspect handles an invocation) OR the SLF4J's
     * {@link MDC} (thread-safe).
     * 
     * @param notificationMap
     * @return Current HTTP SESSION ID (login session).
     * @throws IllegalStateException
     *             if the current SESSION info was not passed from client application or was not
     *             found in MDC.
     */
    public static String getCurrentHttpSessionId(Map<String, Object> notificationMap) {
        String id = null;
        if (notificationMap != null) {
            id = (String) notificationMap.get(CURRENT_HTTP_SESSION_ID);
        }
        if (StringUtils.isEmpty(id)) {
            id = MDC.get(CURRENT_HTTP_SESSION_ID);
        }

        Assert.STATE.notEmpty(id,
            "The current HTTP Session was not passed from client application or MDC");
        return id;
    }

    /**
     * Returns the current HTTP SESSION ID from the SLF4J's {@link MDC} (thread-safe).
     * 
     * @return Current HTTP SESSION ID (login session).
     * @throws IllegalStateException
     *             if the current SESSION info was not passed from client application or was not
     *             found in MDC.
     */
    public static String getCurrentHttpSessionId() {
        return getCurrentHttpSessionId(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
