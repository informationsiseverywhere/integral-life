package com.csc.integral.aop.cache.impl;

/**
 * All supported persistence types for a cache region.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public enum PersistenceType {

    /**
     * Type: Disk cache. This means: data only stores in both memory and disk files. Invalid cache
     * objects will be moved from the cache to disk files.
     */
    DISK_CACHE,

    /**
     * Type: Memory only. This means: data only stores in memory. Invalid cache objects will be
     * removed from the cache directly.
     */
    MEM_ONLY
}
