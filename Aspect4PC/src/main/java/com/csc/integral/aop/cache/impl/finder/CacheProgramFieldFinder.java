package com.csc.integral.aop.cache.impl.finder;

import com.csc.integral.utils.InvokableField;

/**
 * This interface class is for the logic of searching for the correct value field of a program
 * class. This class is used only for program cache object type.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface CacheProgramFieldFinder {

    /**
     * Searches for the return field from a given intercepting object.
     * 
     * @param interceptingObject
     * @return An {@link InvokableField} object which can be invoked. If no field was found, return
     *         <code>null</code> .
     */
    InvokableField findReturnField(Object interceptingObject);

    /**
     * Searches for the status field from a given intercepting object.
     * 
     * @param interceptingObject
     * @return An {@link InvokableField} object which can be invoked. If no field was found, return
     *         <code>null</code> .
     */
    InvokableField findStatusField(Object interceptingObject);

    /**
     * Searches for the identifier field from a given intercepting object.
     * 
     * @param interceptingObject
     * @return An {@link InvokableField} object which can be invoked. If no field was found, return
     *         <code>null</code> .
     */
    InvokableField findIdentifierField(Object interceptingObject);
}
