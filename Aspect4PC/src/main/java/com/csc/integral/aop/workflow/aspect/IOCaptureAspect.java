package com.csc.integral.aop.workflow.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.aop.workflow.processor.IOCaptureProcessor;

/**
 * @author tphan25
 */
@Aspect
public class IOCaptureAspect extends AbstractWorkflowAspect {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(IOCaptureAspect.class);

	private static final String ASPECT_IO_CAPTURE = "execution(* com.csc.smart400framework.dataaccess.SmartFileCode.execute())"
			+ "|| execution(* com.csc.smart400framework.dataaccess.MultiViewSmartFileCode.execute())";

	public IOCaptureAspect() {
		processor = new IOCaptureProcessor();
	}
	
	@Around(value = "execution(* com.csc.smart400framework.dataaccess.SmartFileCode.execute()) || execution(* com.csc.smart400framework.dataaccess.MultiViewSmartFileCode.execute()) ", argNames = "pjp")
	public Object aroundIOExecute(final ProceedingJoinPoint pjp)
			throws Throwable {
		return super.aroundMethod(pjp);
	}

	@Override
	protected void beforeMethod(ProceedingJoinPoint pjp) {
		processor.beforeMethod(pjp);
	}

	@Override
	protected void afterMethod(ProceedingJoinPoint pjp, Object returnObj) {
		processor.afterMethod(pjp);
	}
	
}
