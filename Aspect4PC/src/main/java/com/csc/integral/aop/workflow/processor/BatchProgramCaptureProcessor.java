package com.csc.integral.aop.workflow.processor;

import org.aspectj.lang.ProceedingJoinPoint;

public class BatchProgramCaptureProcessor extends AbstractWorkflowProcessor {

	@Override
	public void processBeforeMethod(ProceedingJoinPoint pjp) {
		
		ProcessorUtils.getUserWorkflowWritter().startProgram(pjp.getTarget().getClass().getName());
		//wr.startMethod(pjp.getTarget(), getJoinPointMethod(pjp), pjp.getArgs());
	}

	@Override
	public void processAfterMethod(ProceedingJoinPoint pjp) {
	
		ProcessorUtils.getUserWorkflowWritter().preEndProgram(pjp.getTarget().getClass().getName());
		
		//wr.endMethod(pjp.getTarget(), getJoinPointMethod(pjp), pjp.getArgs());
		ProcessorUtils.getUserWorkflowWritter().endProgram(pjp.getTarget().getClass().getName());
	}

}
