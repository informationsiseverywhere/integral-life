package com.csc.integral.aop.cache;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * A POJO containing all information which are necessary to build a unique cache object key in the
 * cache store.
 * <p>
 * <b>Remark</b>: This object is immutable.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class CacheKey implements Comparable<CacheKey>, Serializable {
    private static final long serialVersionUID = 1L;

    private String className;
    private String methodName;
    private String argsIdentifier;

    /**
     * Constructs this key with all necessary information.
     * 
     * @param className
     * @param methodName
     * @param argsIdentifier
     */
    public CacheKey(String className, String methodName, String argsIdentifier) {
        this.className = className;
        this.methodName = methodName;
        this.argsIdentifier = argsIdentifier;
    }

    private CacheKey() {
        // for object cloning only
    }

    /**
     * Returns current value of {@link #className}.
     * 
     * @return the {@link #className}.
     */
    public String getClassName() {
        return className;
    }

    /**
     * Returns current value of {@link #methodName}.
     * 
     * @return the {@link #methodName}.
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Returns current value of {@link #argsIdentifier}.
     * 
     * @return the {@link #argsIdentifier}.
     */
    public String getArgsIdentifier() {
        return argsIdentifier;
    }

    /**
     * Creates a new {@link CacheKey} from current key using all information either from the input
     * arguments or from <b>this</b> object.
     * 
     * @param className
     *            Optional class name. "Optional" means that if this information is missing, the
     *            information from <b>this</b> object is used.
     * @param methodName
     *            Optional method name.
     * @param argsId
     *            Optional arguments' identifier.
     * @return
     */
    public CacheKey deriveWith(String className, String methodName, String argsId) {
        CacheKey clone = new CacheKey();
        clone.className = className != null ? className : this.className;
        clone.methodName = methodName != null ? methodName : this.methodName;
        clone.argsIdentifier = argsId != null ? argsId : this.argsIdentifier;
        return clone;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(CacheKey o) {
        if (o == null) {
            return 1; // null is lower than non-null
        }
        return CompareToBuilder.reflectionCompare(this, o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("cls=\"").append(className).append("\";");
        b.append("method=\"").append(methodName).append("\";");
        b.append("argsId=\"").append(argsIdentifier).append("\"");
        return b.toString();
    }

    /**
     * Checks if this key can be considered as "completed".
     * 
     * @return <code>true</code> if all information are provided.
     */
    public final boolean isCompleted() {
        return StringUtils.isNotEmpty(this.argsIdentifier)
                && StringUtils.isNotEmpty(this.className)
                && StringUtils.isNotEmpty(this.methodName);
    }
}
