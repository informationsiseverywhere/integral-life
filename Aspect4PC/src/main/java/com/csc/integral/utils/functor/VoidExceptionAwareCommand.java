package com.csc.integral.utils.functor;


/**
 * A specific {@link ExceptionAwareCommand} which neglects the return value.
 * 
 * @author ntruong5
 * @param <E>
 *            Actual exception type.
 */
public abstract class VoidExceptionAwareCommand<E extends Exception> implements
        ExceptionAwareCommand<Void, E> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Void execute(Object... args) throws E {
        doExecute(args);
        return null;
    }

    /**
     * Actually executes this command without bothering about the return value.
     * 
     * @param args
     */
    protected abstract void doExecute(Object... args) throws E;
}
