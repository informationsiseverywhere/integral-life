package com.csc.integral.utils.functor;


/**
 * Interface for a general-purpose 'command' object. This functor is similar to {@link Command} with
 * just one difference: it accepts a checked exception.
 * 
 * @param <R>
 *            Return value's type.
 * @param <E>
 *            Actual exception type.
 * @author ntruong5
 */
public interface ExceptionAwareCommand<R, E extends Exception> {

    /**
     * Executes current command.
     * 
     * @param args
     * @return
     * @throws E
     *             If there is an error while executing this command.
     */
    R execute(Object... args) throws E;
}
