package com.csc.integral.utils.text;

import com.csc.integral.utils.text.impl.SimpleTextPatternMatcher;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class TextPatternMatcherFactory {

    /**
     * All supported matching styles for the {@link TextPatternMatcher}.
     * 
     * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
     */
    public enum MatchingStyle {

        /**
         * Simple matching style. This style allows to use a (*) to match a portion of a text (i.e.
         * a String). For examples: "xxx*", "*xxx", "*xxx*", "xxx*yyy"...
         * <p>
         * Notes:
         * <ul>
         * <li>an arbitrary number of (*) characters can be used to separate portions.
         * <li>Direct equality check is also performed.
         * </ul>
         */
        SIMPLE
    }

    private static final TextPatternMatcherFactory INSTANCE = new TextPatternMatcherFactory();

    private TextPatternMatcherFactory() {
        // to hide
    }

    /**
     * Returns the current instance of this factory.
     * 
     * @return Returns the sInstance.
     */
    public static TextPatternMatcherFactory getsInstance() {
        return INSTANCE;
    }

    /**
     * Creates a new {@link TextPatternMatcher} implementation for a given matching style.
     * 
     * @param style
     *            A matching style to get correct implementation. <code>null</code> means "default"
     *            style.
     * @return A new text matcher.
     */
    public TextPatternMatcher createMatcher(MatchingStyle style) {
        style = style == null ? MatchingStyle.SIMPLE : style;
        TextPatternMatcher m = null;
        switch (style) {
            case SIMPLE:
            default:
                m = new SimpleTextPatternMatcher();
        }
        return m;
    }

    /**
     * Creates a new default implementation insntace of the {@link TextPatternMatcher}.
     * 
     * @return A new text matcher.
     */
    public TextPatternMatcher createMatcher() {
        return createMatcher(null);
    }
}
