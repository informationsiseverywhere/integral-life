package com.csc.integral.utils.functor;

/**
 * A specific {@link Command} which neglects the return value.
 * 
 * @author ntruong5
 */
public abstract class VoidCommand implements Command<Void> {

    /**
     * {@inheritDoc}
     */
    @Override
    public final Void execute(Object... args) {
        doExecute(args);
        return null;
    }

    /**
     * Actually executes this command without bothering about the return value.
     * 
     * @param args
     */
    protected abstract void doExecute(Object... args);
}
