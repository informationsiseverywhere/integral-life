package com.csc.integral.utils.exception;

import java.io.NotSerializableException;

/**
 * An "unchecked" version of the exception {@link NotSerializableException}.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class NotSerializableRTException extends BaseRTException {
    private static final long serialVersionUID = 1L;

    /**
     * @param message
     * @param cause
     * @param messageParams
     */
    public NotSerializableRTException(String message, Throwable cause, Object... messageParams) {
        super(message, cause, messageParams);
    }

    /**
     * @param cause
     */
    public NotSerializableRTException(NotSerializableException cause) {
        super(cause);
    }
}
