package com.csc.integral.utils;

import java.util.Map;
import java.util.concurrent.Callable;

import com.csc.integral.utils.assertion.Assert;
import com.csc.integral.utils.exception.BaseRTException;

/**
 * A utility class which solves the "double-check locking" problem by relying on the
 * <code>volatile</code> keyword and synchronization block. For more information about the solution,
 * see <a href="http://en.wikipedia.org/wiki/Double-checked_locking#Usage_in_Java">Double-checked
 * locking</a> (From Wikipedia, the free encyclopedia).
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class DoubleCheckLockingUtils {

    private DoubleCheckLockingUtils() {
        // to hide
    }

    /**
     * Returns the existing instance if it's not <code>null</code> or invokes a given
     * {@link Callable} object to create a new singleton instance.
     * 
     * @param <T>
     * @param existingInstance
     *            An existing instance. <br>
     *            <b><u>Remember</u></b>: The variable passed to this parameter must be declared
     *            with the <code>volatile</code> keyword.
     * @param lockOwner
     *            Owner of the lock when synchronization block has to be used.
     * @param instanceCreator
     *            A {@link Callable} object which creates a new instance when there is no instance.
     * @return
     */
    public static <T> T getSingleton(T existingInstance, Object lockOwner,
            Callable<T> instanceCreator) {
        T instance = existingInstance;
            Assert.STATE.notNull(lockOwner, "Owner object is required for synchronization");
            synchronized (lockOwner) {
                instance = existingInstance;
                if (instance == null) {
                    Assert.STATE.notNull(instanceCreator,
                        "Instance creator is required to create a new singleton instance");
                    try {
                        instance = instanceCreator.call();
                    } catch (Exception e) {
                        throw new BaseRTException("Error while creating a singleton instance", e);
                    }
                    Assert.STATE.notNull(instance, "An instance must be created");
                }
            }
        
        return instance;
    }

    /**
     * Returns the existing instance from a given instance map under given instance map key if it's
     * not <code>null</code> or invokes a given {@link Callable} object to create a new singleton
     * instance.
     * <p>
     * <b>Caution</b>: Since new instances will be put into the given instance map, this map should
     * support putting data.
     * 
     * @param <T>
     * @param <K>
     * @param instanceMap
     *            An instance map which contains all map-based-singleton instances.
     * @param instanceMapKey
     *            A unique key to get instances from the instance map.
     * @param lockOwner
     *            Owner of the lock. Usually this is the caller of this method.
     * @param instanceCreator
     *            A {@link Callable} object which creates a new instance when there is no instance.
     * @return
     */
    public static <T, K> T getMapBasedSingleton(Map<K, T> instanceMap, K instanceMapKey,
            Object lockOwner, Callable<T> instanceCreator) {
        Assert.ARGUMENTS.notNull(instanceMap, "Instance map is missing");
        T instance = instanceMap.get(instanceMapKey);
            Assert.STATE.notNull(lockOwner, "Owner object is required for synchronization");
            synchronized (lockOwner) {
                instance = instanceMap.get(instanceMapKey);
                if (instance == null) {
                    Assert.STATE.notNull(instanceCreator,
                        "Instance creator is required to create a new singleton instance");
                    try {
                        instance = instanceCreator.call();
                        instanceMap.put(instanceMapKey, instance);
                    } catch (Exception e) {
                        throw new BaseRTException("Error while creating a singleton instance", e);
                    }
                    Assert.STATE.notNull(instance, "An instance must be created");
                }
            }
        return instance;
    }
}
