package com.csc.integral.utils.text.impl;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.csc.integral.utils.text.TextPatternMatcher;
import com.csc.integral.utils.text.TextPatternMatcherFactory.MatchingStyle;

/**
 * An implementation of {@link TextPatternMatcher} for matching style {@link MatchingStyle#SIMPLE}.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 * @see MatchingStyle#SIMPLE
 */
public class SimpleTextPatternMatcher implements TextPatternMatcher {
    private String[] patterns;
    private boolean caseSensitive = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public TextPatternMatcher usePatterns(String... patterns) {
        if (ArrayUtils.isEmpty(patterns)) {
            this.patterns = null;
        } else {
            this.patterns = patterns;
        }
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TextPatternMatcher caseSensitive(boolean state) {
        this.caseSensitive = state;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean matches(String text) {
        if (!ArrayUtils.isEmpty(patterns)) {
            for (String pattern : patterns) {
                if (isMatched(pattern, text)) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
     * Match a String against the given pattern, supporting the following simple pattern styles:
     * "xxx*", "*xxx", "*xxx*" and "xxx*yyy" matches (with an arbitrary number of pattern parts), as
     * well as direct equality.
     */
    private boolean isMatched(String pattern, String str) {
        if (pattern == null || str == null) {
            return false;
        }
        int firstIndex = pattern.indexOf('*');
        if (firstIndex == -1) {
            return equalsWithCaseCheck(pattern, str);
        }
        if (firstIndex == 0) {
            if (pattern.length() == 1) {
                return true;
            }
            int nextIndex = pattern.indexOf('*', firstIndex + 1);
            if (nextIndex == -1) {
                return endsWithCaseCheck(pattern, str);
            }
            String part = pattern.substring(1, nextIndex);
            int partIndex = str.indexOf(part);
            while (partIndex != -1) {
                if (isMatched(pattern.substring(nextIndex),
                    str.substring(partIndex + part.length()))) {
                    return true;
                }
                partIndex = str.indexOf(part, partIndex + 1);
            }
            return false;
        }

        return (str.length() >= firstIndex
                && equalsWithCaseCheck(pattern.substring(0, firstIndex),
                    str.substring(0, firstIndex)) && isMatched(pattern.substring(firstIndex),
            str.substring(firstIndex)));
    }

    private boolean endsWithCaseCheck(String pattern, String str) {
        String subPattern = pattern.substring(1);
        return caseSensitive ? str.endsWith(subPattern) : StringUtils.endsWithIgnoreCase(str,
            subPattern);
    }

    private boolean equalsWithCaseCheck(String pattern, String str) {
        return caseSensitive ? pattern.equals(str) : pattern.equalsIgnoreCase(str);
    }
}
