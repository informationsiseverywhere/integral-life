package com.csc.integral.utils.text;

/**
 * Interface for a text matcher.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface TextPatternMatcher {

    /**
     * Sets all applicable patterns to perform matching operation.
     * 
     * @param patterns
     * @return This instance.
     */
    TextPatternMatcher usePatterns(String... patterns);

    /**
     * Sets 'case-sensitive' state for the matching.
     * 
     * @param state
     *            <code>true</code> to indicate that the comparison is perform case-sensitively.
     *            Otherwise, the comparison is case-INsensitively.
     * @return This instance.
     */
    TextPatternMatcher caseSensitive(boolean state);

    /**
     * Matches a given text with the configured patterns.
     * 
     * @param text
     *            Text to match with patterns.
     * @return <code>true</code> if given text matches with one of the configured patterns.
     * @see #usePatterns(String...)
     */
    boolean matches(String text);
}
