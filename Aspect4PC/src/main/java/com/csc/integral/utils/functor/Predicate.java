package com.csc.integral.utils.functor;

/**
 * A functor interface implemented by classes that perform a predicate test on an object.
 * 
 * @param <E> Evaluating object type.
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public interface Predicate<E> {

    /**
     * Evaluates a given object.
     * 
     * @param object
     * @return Evaluation result.
     */
    boolean evaluate(E object);
}
