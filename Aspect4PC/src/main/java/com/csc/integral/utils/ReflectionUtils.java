package com.csc.integral.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.utils.assertion.Assert;
import com.csc.integral.utils.exception.BaseRTException;
import com.csc.integral.utils.functor.Predicate;

/**
 * A utility class for Java Reflection APIs.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class ReflectionUtils {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ReflectionUtils.class);

	private ReflectionUtils() {
		// to hide
	}

	/**
	 * Looks up a Java {@link Field} from a given root class and a given field
	 * path. All fields in the path are separated by dots ("."). For example,
	 * "progVars.foo.foo2" or "progVars.foo[2].foo2[3].foo3".
	 * <p>
	 * <b>Note</b>: This method works for all field visibility settings. This
	 * means, all private, protected, default and public fields are considered
	 * candidates for lookup.
	 * 
	 * @param fieldPath
	 * @param target
	 * @return A {@link Field} object matching with given field path or
	 *         <code>null</code>. Value <code>null</code> is returned in one of
	 *         the following cases:
	 *         <ul>
	 *         <li>Field path is empty. <li>One of the object under the path is
	 *         <code>null</code>. <li>One of the field under the path doesn't
	 *         exist.
	 *         </ul>
	 */
	public static InvokableField lookupField(Object target, String fieldPath) {
		Assert.ARGUMENTS.notNull(target,
				"Target object to lookup field is missing");
		Assert.ARGUMENTS.notEmpty(fieldPath,
				"Field path is not provided (empty)");

		Object current = target;
		Object previous = null;
		Field currentField = null;

		String[] paths = StringUtils.split(fieldPath, ".");
		if (ArrayUtils.isEmpty(paths)) {
			return null;
		}

		int depth = 0;
		for (String path : paths) {
			depth++;
			boolean isArray = false;
			int index = 0;

			if (path.endsWith("]")) {
				isArray = true;

				// get the index
				String indexAsString = path.substring(path.indexOf('[') + 1,
						path.indexOf(']'));

				try {
					index = Integer.parseInt(indexAsString);
				} catch (NumberFormatException e) {
					throw new BaseRTException(
							"Cannot get array-field index from path \"{0}\"",
							e, fieldPath);
				}

				// split off the index
				path = path.substring(0, path.indexOf('['));
			}

			currentField = FieldUtils.getField(current.getClass(), path, true);

			// if there is no field => cannot get next-level field => return
			// immediately
			if (currentField == null) {
				break;
			}

			Object o;
			try {
				o = FieldUtils.readField(currentField, current, true);
			} catch (IllegalAccessException e) {
				throw new BaseRTException("Cannot access field {0}", e,
						currentField.getName());
			}

			if (isArray) {
				Object[] oArray = (Object[]) o;
				o = oArray[index];
			}

			previous = current;
			current = o;

			// if 'current' object is unreachable => cannot get next-level field
			// => return now
			if (current == null) {
				break;
			}
		}

		if (depth == paths.length && currentField != null) {
			previous = previous != null ? previous : target;
			return new InvokableField(currentField, previous);
		}
		return null;
	}

	/**
	 * Resolves a {@link Class} instance from the full-name of that class.
	 * 
	 * @param classFullName
	 * @return Resolved class.
	 * @throws BaseRTException
	 *             if given class was not found.
	 */
	public static Class<?> resolveClass(String classFullName) {
		Class<?> resolvedClass;
		try {
			resolvedClass = ClassUtils.getClass(classFullName);
		} catch (ClassNotFoundException e) {
			throw new BaseRTException(
					"Error while getting Class object for class ''{0}''", e,
					classFullName);
		}
		return resolvedClass;
	}

	/**
	 * Loads a class from given fullname. The execution flow is:
	 * <ul>
	 * <li>If base class is available, try to load target class using this base
	 * {@link ClassLoader}.
	 * <li>If base class is <code>null</code> OR the first trial fails, try to
	 * load target class using system {@link ClassLoader}.
	 * </ul>
	 * 
	 * @param className
	 *            Classname to load class.
	 * @param baseClass
	 *            Base class to load this class. If base class is
	 *            <code>null</code>, system {@link ClassLoader} will be used.
	 * @return Loaded {@link Class}.
	 */
	public static Class<?> loadClass(String className, Class<?> baseClass) {
		Class<?> result = null;

		if (baseClass != null) {
			try {
				result = baseClass.getClassLoader().loadClass(className);
			} catch (Exception e) {
				result = null;
			}
		}

		// continue loading class from system class loader
		if (result == null) {
			try {
				result = ClassLoader.getSystemClassLoader()
						.loadClass(className);
			} catch (Exception e) {
				throw new BaseRTException("Could not load class '{0}'", e,
						className);
			}
		}

		return result;
	}

	/**
	 * Collects all fields from a given root class, either in one or multiple
	 * hierarchical depths, matching with a given predicate.
	 * 
	 * @param rootClass
	 *            The root class which is used to start the search.
	 * @param recursive
	 *            <code>true</code> to allow recursive search (i.e. search in
	 *            all superclasses of the given root class). Otherwise, only
	 *            fields in the root class are considered.
	 * @param onlyFirstField
	 *            <code>true</code> if this method returns when the first field
	 *            is found. Otherwise, all matching fields will be returned.
	 * @param predicate
	 * @return Collected fields or an empty collection.
	 */
	public static List<Field> collectFields(Class<?> rootClass,
			boolean recursive, boolean onlyFirstField,
			Predicate<Field> predicate) {
		Assert.ARGUMENTS.notNull(predicate, "Predicate is missing");

		Set<Field> results = new LinkedHashSet<Field>();
		Field[] fields = null;
		Class<?> clz = rootClass;
		outer: do {
			fields = clz.getDeclaredFields();
			for (Field field : fields) {
				if (predicate.evaluate(field)) {
					results.add(field);
					if (onlyFirstField) {
						break outer;
					}
				}
			}
			fields = clz.getFields();
			for (Field field : fields) {
				if (predicate.evaluate(field)) {
					results.add(field);
					if (onlyFirstField) {
						break outer;
					}
				}
			}
		} while (recursive && (clz = clz.getSuperclass()) != null);

		return new ArrayList<Field>(results);
	}

	/**
	 * Checks if a given checked class name represents a superclass or a
	 * superinterface of a given checking class.
	 * <p>
	 * The "mayBe" means: the verification is performed by matching the class
	 * name of all interfaces and superclasses as well as given checking class
	 * with the given checked class name. As a result, the result is not as
	 * guarrantied as {@link Class#isAssignableFrom(Class)} in Java.
	 * 
	 * @param checkingClass
	 * @param checkedClassName
	 * @return <code>true</code> if <code>checkingClass</code> is a subclass or
	 *         subinterface of the <code>checkedClassName</code>. Otherwise,
	 *         return <code>false</code>.
	 */
	@SuppressWarnings("unchecked")
	public static boolean mayBeAssignableFrom(Class<?> checkingClass,
			String checkedClassName) {
		boolean maybe = false;

		if (checkingClass != null) {
			Class<?> inputCls = checkingClass;

			// check current class first
			maybe = StringUtils.equals(checkedClassName, inputCls.getName());

			// if it fails, try with all interfaces
			if (!maybe) {
				List<Class<?>> classes = ClassUtils.getAllInterfaces(inputCls);
				maybe = mayBeInstanceOf(classes, checkedClassName);
			}

			// eventually, try with all superclasses
			if (!maybe) {
				List<Class<?>> classes = ClassUtils
						.getAllSuperclasses(inputCls);
				maybe = mayBeInstanceOf(classes, checkedClassName);
			}
		}

		return maybe;
	}

	/**
	 * Checks if a given object "may be" a Hibernate annotation-based entity
	 * object. The verification is done by checking with either
	 * <code>javax.persistence.Entity</code> or
	 * <code>org.hibernate.annotations.Entity</code>.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean mayBeHibernateEntity(Object obj) {
		if (obj != null) {
			Annotation[] annotations = obj.getClass().getAnnotations();
			for (Annotation ann : annotations) {
				String type = ann.annotationType().getName();
				if ("javax.persistence.Entity".equals(type)
						|| "org.hibernate.annotations.Entity".equals(type)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Checks if a given object may be an instance of a given checked class full
	 * name.
	 * <p>
	 * The "mayBe" means: the verification is performed by matching the class
	 * name of all interfaces and superclasses as well as given object class
	 * with the given checked name. As a result, the result is not as
	 * guarrantied as <code>instanceof</code> keyword in Java.
	 * <p>
	 * <b>Use-case</b>: client code cannot access the checked class.
	 * 
	 * @param obj
	 *            Input object.
	 * @param checkedClassName
	 *            Checked class name.
	 * @return
	 * @see #mayBeAssignableFrom(Class, String)
	 */
	public static boolean mayBeInstanceOf(Object obj, String checkedClassName) {
		if (obj != null) {
			return mayBeAssignableFrom(obj.getClass(), checkedClassName);
		}
		return false;
	}

	private static boolean mayBeInstanceOf(List<Class<?>> inputClasses,
			String className) {
		for (Class<?> cls : inputClasses) {
			if (StringUtils.equals(className, cls.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get value of field in interceptingObject
	 * 
	 * @param interceptingObject
	 * @param field
	 * @return
	 */
	public static Object getFieldValue(Object interceptingObject, Field field) {
		try {
			return field.get(interceptingObject);
		} catch (IllegalArgumentException e) {
			LOGGER.error(
					"Specified object=[{}] is not an instance of the class or interface "
							+ "declaring the underlying field=[{}]",
					new Object[] { interceptingObject, field }, e);
		} catch (IllegalAccessException e) {
			LOGGER.error("Field=[{}] is not accessable in object=[{}]",
					new Object[] { field, interceptingObject }, e);
		}
		return null;
	}

	/**
	 * Set value to field in interceptingObject
	 * 
	 * @param interceptingObject
	 * @param field
	 * @param value
	 */
	public static void setFieldValue(Object interceptingObject, Field field,
			Object value) {
		try {
			field.set(interceptingObject, value);
		} catch (IllegalArgumentException e) {
			LOGGER.error(
					"Specified object=[{}] is not an instance of the class or interface "
							+ "declaring the underlying field=[{}]",
					new Object[] { interceptingObject, field }, e);
		} catch (IllegalAccessException e) {
			LOGGER.error("Field=[{}] is not accessable in object=[{}]",
					new Object[] { field, interceptingObject }, e);
		}
	}

	/**
	 * Get value of field in obj
	 * 
	 * @param kclass
	 *            class defines field
	 * @param obj
	 *            object class to get field
	 * @param fieldName
	 *            field name
	 * @return value of the field or null
	 */
	public static Object getFieldValue(Class<?> kclass, Object obj,
			String fileName) {
		try {
			Field field = FieldUtils.getField(kclass, fileName, true);
			if (field != null) {
				return field.get(obj);
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("Cannnot get value of field {} in object {}",
					fileName, obj.getClass().getSimpleName());
		} catch (IllegalAccessException e) {
			LOGGER.error("Cannnot get value of field {} in object {}",
					fileName, obj.getClass().getSimpleName());
		}
		return null;
	}

	/**
	 * Get value of field in obj
	 * 
	 * @param obj
	 * @param fieldName
	 *            field name
	 * @return value of the field or null
	 */
	public static Object getFieldValue(Object obj, String fileName) {
		try {
			Field field = FieldUtils.getField(obj.getClass(), fileName, true);
			if (field != null) {
				return field.get(obj);
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("Cannnot get value of field {} in object {}",
					fileName, obj.getClass().getSimpleName());
		} catch (IllegalAccessException e) {
			LOGGER.error("Cannnot get value of field {} in object {}",
					fileName, obj.getClass().getSimpleName());
		}
		return null;
	}

	/**
	 * invoke method and get return object
	 * 
	 * @param kclass
	 *            class defines method
	 * @param obj
	 *            object will be invoked
	 * @param methodName
	 *            method name
	 * @param paramClasses
	 *            param type of the method
	 * @param methodParam
	 *            param of the method
	 * @return return object of the method or null
	 */
	public static Object invokeMethod(Class<?> kclass, Object obj,
			String methodName, Class<?>[] paramClasses, Object... methodParam) {
		try {
			Method method;
			if (paramClasses == null || paramClasses.length == 0) {
				method = MethodUtils.getAccessibleMethod(kclass, methodName);
			} else {
				method = MethodUtils.getAccessibleMethod(kclass, methodName,
						paramClasses);
			}
			return method.invoke(obj, methodParam);
		} catch (IllegalArgumentException e) {
			LOGGER.error("invokeMethod {}.{} exception", obj.getClass()
					.getSimpleName(), methodName);
		} catch (IllegalAccessException e) {
			LOGGER.error("invokeMethod {}.{} exception", obj.getClass()
					.getSimpleName(), methodName);
		} catch (InvocationTargetException e) {
			LOGGER.error("invokeMethod {}.{} exception", obj.getClass()
					.getSimpleName(), methodName);
		}
		return null;
	}

}
