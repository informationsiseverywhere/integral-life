package com.csc.integral.utils;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

import com.csc.integral.utils.assertion.Assert;
import com.csc.integral.utils.exception.BaseRTException;

/**
 * A simple class which holds the information for a {@link Field} and its owner object (which can be
 * used to invoke this field).
 * <p>
 * <b>Remark</b>: This object is immutable.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class InvokableField {

    private Field field;
    private WeakReference<Object> owner;

    /**
     * @param field
     * @param owner
     */
    public InvokableField(Field field, Object owner) {
        Assert.ARGUMENTS.notNull(field, "Field is missing");
        Assert.ARGUMENTS.notNull(owner, "Owner object is missing");

        this.field = field;
        this.owner = new WeakReference<Object>(owner);
    }

    /**
     * Makes this field accessible (invokable). Use this method if this field is not
     * <code>public</code>.
     * 
     * @return
     */
    public InvokableField makeAccessible() {
        field.setAccessible(true);
        return this;
    }

    /**
     * Get the current value for owner
     * 
     * @return Returns the owner.
     */
    public Object getOwner() {
        return owner.get();
    }

    /**
     * Get the current value for field
     * 
     * @return Returns the field.
     */
    public Field getField() {
        return field;
    }

    /**
     * Returns the current value of this field.
     * 
     * @return
     */
    public Object get() {
        if (owner.get() != null) {
            try {
                return field.get(owner.get());
            } catch (Exception e) {
                throw new BaseRTException("Error while invoking field \"{0}\"", e, this);
            }
        }
        return null;
    }

    /**
     * Returns the current value of this field according to a given target class.
     * 
     * @param <T>
     * @param targetClass
     * @return Field value in given type. The following rules are applied:
     *         <ol>
     *         <li>If value is <code>null</code>, return <code>null</code>.
     *         <li>If targetClass is {@link String}, return value string presentation (via
     *         <code>toString</code> method). If the value is a {@link String} as well, a
     *         <code>toString</code> invocation is not necessary.
     *         <li>Otherwise, cast the value to the targetClass. A {@link ClassCastException} may be
     *         raised if the casting fails to complete.
     *         </ol>
     * @see #get()
     */
    public final <T> T get(Class<T> targetClass) {
        Object v = get();
        if (v != null && !(v instanceof String) && targetClass == String.class) {
            v = v.toString();
        }
        return v != null ? targetClass.cast(v) : null;
    }

    /**
     * Assigns a new value to this field.
     * 
     * @param value
     * @return
     */
    public InvokableField set(Object value) {
        if (owner.get() != null) {
            try {
                field.set(owner.get(), value);
            } catch (Exception e) {
                throw new BaseRTException("Error while invoking field \"{0}\"", e, this);
            }
        }
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return field.toString();
    }
}
