package com.csc.integral.utils;

import java.lang.ref.WeakReference;
import java.util.Observable;
import java.util.Observer;

/**
 * A weak-referenced observer which delegates the actual work to a real, referent observer object.
 * It combines the power of weak-reference and Observer-pattern notification.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class WeakObserverReference extends WeakReference<Observer> implements Observer {

    /**
     * Constructs this reference from a referent observer object.
     * 
     * @param referent
     */
    public WeakObserverReference(Observer referent) {
        super(referent);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Observable observable, Object data) {
        Observer observer = get();
        if (observer != null) {
            observer.update(observable, data);
        }
    }
}
