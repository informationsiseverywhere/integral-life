package com.csc.integral.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.istack.Nullable;

/**
 * Utility class for enums.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class EnumUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnumUtils.class);

    private EnumUtils() {
        // to hide
    }

    /**
     * Returns the enum value corresponding to a given name within given enum type.
     * 
     * @param <E>
     * @param enumType
     * @param name
     * @return Enum value or <code>null</code> if no enum value exists.
     */
    @Nullable
    public static <E extends Enum<E>> E valueOf(Class<E> enumType, String name) {
    	//=========IJTI-851=====START==
    	if(name==null)
    	{
    		return null;
    	}
    	 try {
             return Enum.valueOf(enumType, name);
         } catch (IllegalArgumentException e) {
             LOGGER.debug("No enum value '{}' exists in enum type '{}'", name, enumType);
         } 
         return null;   //=========IJTI-851=====END==
    }
}
