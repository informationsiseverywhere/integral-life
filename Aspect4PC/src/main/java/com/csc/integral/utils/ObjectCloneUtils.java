package com.csc.integral.utils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.utils.assertion.Assert;

/**
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public final class ObjectCloneUtils {
    private static final String CLONE_METHOD_NAME = "clone";
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectCloneUtils.class);

    private ObjectCloneUtils() {
        // to hide
    }

    /**
     * Convenience method derived from {@link #clone(Object)} which uses generics for better return
     * value type.
     * 
     * @param <T>
     * @param obj
     * @param objectClass
     * @return
     */
    public static <T> T clone(Object obj, Class<T> objectClass) {
        Object clone = clone(obj);
        if (clone != null) {
            return objectClass.cast(clone);
        }
        return null;
    }

    /**
     * Clones a given object.
     * <p>
     * The processing flow is as below:
     * <ol>
     * <li>This method firstly checks if there is method "clone" declared publicly. If it's found,
     * this method is invoked.
     * <li>Otherwise, a general-purpose cloning mechanism is executed. In this case, the object in
     * argument must be a {@link Serializable} object.
     * </ol>
     * 
     * @param obj
     *            Source object. If it's <code>null</code>, return value is <code>null</code>.
     * @return clone object.
     */
    public static Object clone(Object obj) {
        if (obj == null) {
            return null;
        }

        Object res = null;
        if (obj instanceof List<?>) {
            res = cloneList(obj);
        } else if (obj instanceof Set<?>) {
            res = cloneSet(obj);
        } else if (obj instanceof Map<?, ?>) {
            res = cloneMap(obj);
        } else {
            res = cloneSingleObject(obj);
        }

        return res;
    }

    private static Object cloneMap(Object obj) {
        Object res;
        Map<?, ?> itemMap = (Map<?, ?>) obj;

        Map<Object, Object> resultMap;
        if (itemMap instanceof LinkedHashMap<?, ?>) {
            resultMap = new LinkedHashMap<Object, Object>(itemMap.size());
        } else {
            resultMap = new HashMap<Object, Object>(itemMap.size());
        }

        for (Entry<?, ?> e : itemMap.entrySet()) {
            resultMap.put((Object) e.getKey(), cloneSingleObject((Object) e.getValue()));
        }
        
        res = resultMap;
        return res;
    }

    private static Object cloneSet(Object obj) {
        Object res;
        Set<?> items = (Set<?>) obj;

        Set<Object> results;
        if (items instanceof LinkedHashSet<?>) {
            results = new LinkedHashSet<Object>(items.size());
        } else {
            results = new HashSet<Object>(items.size());
        }

        for (Object item : items) {
            results.add(cloneSingleObject(item));
        }

        res = results;
        return res;
    }

    private static Object cloneList(Object obj) {
        Object res;
        List<?> items = (List<?>) obj;
        List<Object> results = new ArrayList<Object>(items.size());

        for (Object item : items) {
            results.add(cloneSingleObject(item));
        }
        
        res = results;
        return res;
    }

    static Object cloneSingleObject(Object obj) {
        if (obj == null) {
            return null;
        }

        Object res = null;
        boolean cloned = false;

        try {
            res = MethodUtils.invokeMethod(obj, CLONE_METHOD_NAME, null);
            cloned = true;
        } catch (NoSuchMethodException e1) {
            LOGGER.info("Cannot find method '{}.{}()'", obj.getClass().getName(), CLONE_METHOD_NAME);
        } catch (IllegalAccessException e1) {
            LOGGER.info("Error while accessing method '{}.{}()'", obj.getClass().getName(),
                CLONE_METHOD_NAME);
        } catch (InvocationTargetException e1) {
            LOGGER.info("Error while executing method '{}.{}()'", obj.getClass().getName(),
                CLONE_METHOD_NAME);
        }

        if (!cloned) {
            Assert.STATE.instanceOf(obj, Serializable.class);
            res = SerializationUtils.clone((Serializable) obj);
        }

        return res;
    }

}
