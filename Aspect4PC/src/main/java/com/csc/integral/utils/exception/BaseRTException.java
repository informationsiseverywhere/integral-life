package com.csc.integral.utils.exception;

import java.text.MessageFormat;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * A base, generic-purpose runtime exception. This extension uses varargs to allow cause message to
 * be parameterized.
 * 
 * @author Truong Chau Lien Ngoc (ntruong5@csc.com)
 */
public class BaseRTException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs this exception without any message.
     */
    public BaseRTException() {
        super();
    }

    /**
     * @param message
     * @param cause
     * @param messageParams
     */
    public BaseRTException(String message, Throwable cause, Object... messageParams) {
        super(formatMessage(message, messageParams), cause);
    }

    /**
     * @param message
     * @param messageParams
     */
    public BaseRTException(String message, Object... messageParams) {
        super(formatMessage(message, messageParams));
    }

    /**
     * @param cause
     */
    public BaseRTException(Throwable cause) {
        super(cause);
    }

    private static String formatMessage(String message, Object... params) {
        if (StringUtils.isEmpty(message) || ArrayUtils.isEmpty(params)) {
            return message;
        }
        return MessageFormat.format(message, params);
    }
}
