package com.csc.integral.utils.functor;

/**
 * A command which can be executed later. This class inspires the idea of {@link Runnable} in JDK
 * with some improvements in the API including: return value can be specified AND input parameters
 * can be passed freely.
 * 
 * @author ntruong5
 * @param <R>
 *            Return type of a command execution.
 */
public interface Command<R> {

    /**
     * Runs this command with given arguments.
     * 
     * @param args
     * @return
     */
    R execute(Object... args);
}
