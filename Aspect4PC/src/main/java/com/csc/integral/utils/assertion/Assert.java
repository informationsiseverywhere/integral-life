package com.csc.integral.utils.assertion;

import java.text.MessageFormat;
import java.util.Collection;

/**
 * An assert class for assertion.
 * 
 * @author ntruong5
 */
public final class Assert {

    /**
     * Assert executor class for arguments. If one of the assertion methods of this object is not
     * succeeded, {@link IllegalArgumentException} will be thrown.
     */
    public static final AssertExecutor ARGUMENTS = new AbstractAssertExecutor() {
        @Override
        void throwRuntimeException(String message, Object... params) {
            throw new IllegalArgumentException(MessageFormat.format(message, params));
        }
    };

    /**
     * Assert executor class for states inside methods. If one of the assertion methods of this
     * object is not succeeded, {@link IllegalStateException} will be thrown.
     */
    public static final AssertExecutor STATE = new AbstractAssertExecutor() {
        @Override
        void throwRuntimeException(String message, Object... params) {
            throw new IllegalStateException(MessageFormat.format(message, params));
        }
    };

    private Assert() {
        // to hide
    }

    /**
     * Default implementation for all assert executors. The only method subclasses have to implement
     * is {@link #throwRuntimeException(String, Object...)} which throws correct runtime exception.
     * 
     * @author ntruong5
     */
    private static abstract class AbstractAssertExecutor implements AssertExecutor {
        abstract void throwRuntimeException(String message, Object... params);

        /**
         * {@inheritDoc}
         */
        @Override
        public void notNull(Object object, String message, Object... params) {
            if (object == null) {
                throwRuntimeException(message, params);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void isTrue(boolean expression, String message, Object... params) {
            if (!expression) {
                throwRuntimeException(message, params);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void isFalse(boolean expression, String message, Object... params) {
            isTrue(!expression, message, params);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void notEmpty(String object, String message, Object... params) {
            if (object == null || object.isEmpty()) {
                throwRuntimeException(message, params);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void notEmpty(Collection<?> object, String message, Object... params) {
            if (object == null || object.isEmpty()) {
                throwRuntimeException(message, params);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void fail(String message, Object... params) {
            throwRuntimeException(message, params);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void instanceOf(Object object, Class<?> expectedCls) {
            instanceOf(object, expectedCls, "Object ''{0}'' is not an instance of class ''{1}''",
                object, expectedCls);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void instanceOf(Object object, Class<?> expectedCls, String message,
                Object... params) {
            if (object != null && expectedCls != null
                    && !expectedCls.isAssignableFrom(object.getClass())) {
                throwRuntimeException(message, params);
            }
        }
    }

    /**
     * @author ntruong5
     */
    public interface AssertExecutor {

        /**
         * Checks if given expression is <code>true</code> or not.
         * 
         * @param expression
         *            Expression to check.
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void isTrue(boolean expression, String message, Object... params);

        /**
         * Checks if given expression is <code>false</code> or not.
         * 
         * @param expression
         *            Expression to check.
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void isFalse(boolean expression, String message, Object... params);

        /**
         * Checks if given object is NOT <code>null</code>.
         * 
         * @param object
         *            Object to check.
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void notNull(Object object, String message, Object... params);

        /**
         * Checks if given string is NOT empty (blanks are considered NOT empty).
         * 
         * @param object
         *            Object to check.
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void notEmpty(String object, String message, Object... params);

        /**
         * Checks if given collection is NOT empty. <code>null</code> is considered empty here.
         * 
         * @param object
         *            Collection to check.
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void notEmpty(Collection<?> object, String message, Object... params);

        /**
         * Surely throws a runtime exception. This method is useful for the cases when the code
         * below this line would be sure a mistake.
         * 
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void fail(String message, Object... params);

        /**
         * Checks if a given object is an instance of given expected class. A default error message
         * "Object ZZZ is not an instance of YYY" is thrown if a violation occurs.
         * 
         * @param object
         *            Object to check. If it's <code>null</code>, this method is ignored.
         * @param expectedClass
         *            Expected class to be checked. If it's <code>null</code>, this method is
         *            ignored.
         */
        void instanceOf(Object object, Class<?> expectedClass);

        /**
         * Checks if a given object is an instance of given expected class. A default error message
         * "Object ZZZ is not an instance of YYY" is thrown if a violation occurs.
         * 
         * @param object
         *            Object to check. If it's <code>null</code>, this method is ignored.
         * @param expectedClass
         *            Expected class to be checked. If it's <code>null</code>, this method is
         *            ignored.
         * @param message
         *            Error message if the assertion fails.
         * @param params
         *            Message parameters.
         */
        void instanceOf(Object object, Class<?> expectedClass, String message, Object... params);
    }
}
