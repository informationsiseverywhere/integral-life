HOW TO ENABLE AOP
-----------
Set VM argument for the AspectJ compiler and AspectJ weaver load time configuration:  
	-javaagent:<path>\aspectjweaver-1.6.1.jar
	
Set VM argument for the AspectJ configuration:
    -Dorg.aspectj.weaver.loadtime.configuration=\org\aspectj\aop.xml

Change to use log4jAop.xml instead of log4jConfig.xml, set system property as below
	aspect4pc=true
	Changes in main module
	