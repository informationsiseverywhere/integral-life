@rem Weblogic only
@rem The tool automatically compile JSP files to Java, 
@rem split them to ensure that compiled Java files compatible with Weblogic.

@rem Command line: JspSplitter <source_file>
@rem JspSplitter D:\FSS\Weblogic\CSCPolisyAsiaEAR_v3.12_32884.ear

@echo Set WL environment
@rem set WL_HOME=%MW_HOME%\wlserver
@rem call "%WL_HOME%\server\bin\setWLSEnv.cmd"
@echo -----------------
@echo Compile sources and keep the generated .java files
set SOURCE=%1
md %SOURCE%_target
set TARGET=%SOURCE%_target
call java weblogic.appc -verbose -keepgenerated -output %TARGET% %SOURCE%
@echo -----------------
@echo Extract war file
cd %TARGET%
md PolisyAsiaWeb
cd PolisyAsiaWeb
call jar xf %TARGET%\PolisyAsiaWeb.war
@echo -----------------
@echo Split the big Jsp files
set JSP_JAVA_SOURCE=%TARGET%\PolisyAsiaWeb\WEB-INF\classes\jsp_servlet\_polisy\_underwriting\_eng
cd ..
cd ..
call java -jar uber-JspSplitter-1.0.jar %JSP_JAVA_SOURCE%
@echo -----------------
@echo Package PolisyAsiaWeb.war
cd %TARGET%\PolisyAsiaWeb
call jar cf PolisyAsiaWeb.war .
copy PolisyAsiaWeb.war %TARGET%
cd %TARGET%
rd /s/q PolisyAsiaWeb