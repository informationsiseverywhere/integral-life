package com.csc.polisy.util;

public class JspSplitterConstant {
	
	public static final String PREFIX_GENERATEDTEXT = "generatedText";
	public static final String STRINGDATA = "StringData";
	
	public static final String BREAKPOINT_START_1 = "if ((new Byte("; 
	public static final String BREAKPOINT_START_2 = ").getInvisible())).compareTo(new Byte(";
	public static final String BREAKPOINT_END = "catch (java.lang.Throwable __ee)";
	
	public static final String INSERT_PARAMDEFINATION_HERE = "<<insert_paramDefinition_here>>";
	public static final String INSERT_PARAM_HERE = "<<insert_param_here>>";
	public static final String INSERT_CODE_HERE = "<<insert_code_here>>";
	public static final String INSERT_SCREENNAME_HERE = "<<insert_screen_name_here>>";
	
	public static final String KEYWORD_SCREENNAME_S = "S";
	public static final String KEYWORD_SCREENNAME_BEGIN = "__s";	
	public static final String KEYWORD_SCREENNAME_END = "form.java";

}
