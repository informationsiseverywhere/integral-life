package com.csc.polisy.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;

public class FileUtils {

	public static String readFile(String filePath) throws IOException {
		if (!fileExist(filePath)) {
			throw new IOException("File not found.");
		}
		byte[] buffer = new byte[(int) new File(filePath).length()];
		BufferedInputStream f = null;
		try {
			f = new BufferedInputStream(new FileInputStream(filePath));
			f.read(buffer);
		} finally {
			if (f != null)
				try {
					f.close();
				} catch (IOException e) {
					throw new IOException("Could not close BufferedInputStream ", e);
				}
		}
		return new String(buffer);
	}
	
	public static String readFile(InputStream file) throws IOException {
		byte[] buffer = new byte[file.available()];
		file.read(buffer);
		return new String(buffer);
	}
	
	public static String readFile(String filePath, int startLine, int endLine) throws IOException {
		if (!fileExist(filePath)) {
			throw new IOException("File not found.");
		}
		StringBuilder content = new StringBuilder();
		int currentLineNo = 0;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filePath));
			// read to startLine
			while (currentLineNo < startLine) {
				if (in.readLine() == null) {
					throw new IOException("File too small");
				}
				currentLineNo++;
			}

			// read until endLine
			while (currentLineNo <= endLine) {
				String line = in.readLine();
				if (line == null) {
					// here, we'll forgive a short file
					// note finally still cleans up
					throw new IOException("File too small");
				}
				content.append(line);
				content.append("\n");
				currentLineNo++;
			}
		} catch (IOException ex) {
			throw new IOException("Problem reading file.\n" + ex.getMessage());
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException ignore) {
			}
		}
		
		return new String(content);
	}
	
	/**
	 * readBlockandPickupParams
	 * 
	 * @param filePath
	 * @param startLine
	 * @param endLine
	 * @return String[content, params (e.g: generatedText19, generatedText20), paramDefinition (e.g: StringData generatedText19, StringData generatedText20)]
	 * @throws IOException
	 */
	public static String[] readBlockandPickupParams(String filePath, int startLine, int endLine) throws IOException {
		if (!fileExist(filePath)) {
			throw new IOException("File not found.");
		}
		String[] sResult = new String[3];
		StringBuilder content = new StringBuilder();
		StringBuilder params = new StringBuilder();
		StringBuilder paramDefinition = new StringBuilder();
		int currentLineNo = 0;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filePath));
			// read to startLine
			while (currentLineNo < startLine) {
				if (in.readLine() == null) {
					throw new IOException("File too small");
				}
				currentLineNo++;
			}

			// read until endLine
			while (currentLineNo <= endLine) {
				String line = in.readLine();
				if (line == null) {
					sResult[0] = new String(content);
					sResult[1] = new String(params);
					sResult[2] = new String(paramDefinition);
					return sResult;
				}
				if (line.contains(JspSplitterConstant.PREFIX_GENERATEDTEXT)) {
					String param = pickupParamName(line);
					params.append(param + ", ");
					paramDefinition.append(JspSplitterConstant.STRINGDATA + " " + param + ", ");
				}
				content.append(line);
				content.append("\n");
				currentLineNo++;
			}
		} catch (IOException ex) {
			throw new IOException("Problem reading file.\n" + ex.getMessage());
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException ignore) {
			}
		}
		sResult[0] = new String(content);
		sResult[1] = new String(((params != null && params.length() > 0)? params.substring(0, params.length() - 2) : ""));
		sResult[2] = new String(((paramDefinition != null && paramDefinition.length() > 0)? paramDefinition.substring(0, paramDefinition.length() - 2) : ""));		
		return sResult;
	}
	
	public static int identifyReadLineNumber(String filePath, String keyWords) throws IOException {
		LineNumberReader lnreader = new LineNumberReader(new FileReader(filePath));
		try {			
			String line = "";			
			while ((line = lnreader.readLine()) != null) {
				if (line.contains(keyWords)) {
					return lnreader.getLineNumber();
				}
			}
		} catch (IOException ex) {
			throw new IOException("Problem reading file.\n" + ex.getMessage());
		} finally {
			lnreader.close();
		}
		return -1;
	}
	
	public static int identifyReadLineNumber(String filePath, String keyWords1, String keyWords2, int start) throws IOException {
		LineNumberReader lnreader = new LineNumberReader(new FileReader(filePath));
		try {
			int count = 0;
			while ((lnreader.readLine() != null) && (count < start)) {
				count++;
			}
			
			String line = "";			
			while ((line = lnreader.readLine()) != null) {
				if (line.contains(keyWords1) && line.contains(keyWords2)) {
					return lnreader.getLineNumber();
				}
			}
		} catch (IOException ex) {
			throw new IOException("Problem reading file.\n" + ex.getMessage());
		} finally {
			lnreader.close();
		}
		return -1;
	}
	
	public static int countLineNumber(String filePath) throws IOException {
		LineNumberReader lnreader = new LineNumberReader(new FileReader(filePath));
		int count = 0;
		try {			
			while (lnreader.readLine() != null) {
				count++;
			}
		} catch (IOException ex) {
			throw new IOException("Problem reading file.\n" + ex.getMessage());
		} finally {
			lnreader.close();
		}
		return count;
	}
	
	public static void writeFile(String filePath, String content) throws IOException {
		try {
			File file = new File(filePath);
			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(filePath);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.flush();
			bw.close();

		} catch (IOException e) {
			throw new IOException("Could not close BufferedStream ", e);
		}
	}
	
	public static void writeFile(String filePath, String content, boolean append) throws IOException {
		try {
			File file = new File(filePath);
			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(filePath, append);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.flush();
			bw.close();

		} catch (IOException e) {
			throw new IOException("Could not close BufferedStream ", e);
		}
	}
	
	public static File[] getFiles(String dirPath) throws IOException {
		File file = new File(dirPath);
		FileFilter fileFilter = new FileFilter() {
		    public boolean accept(File file) {
		    	return ((file.isFile()) && (file.getName().endsWith(".java")));
		    }
		};
		return file.listFiles(fileFilter);
	}
	
	public static boolean rename(String oldName, String newName) throws IOException {
		File file = new File(oldName);
		File file2 = new File(newName);
		file2.delete();
		return file.renameTo(file2);
	}
	
	public static boolean fileExist(String filePath) throws IOException {
		File file = new File(filePath);
		return file.exists();
	}
	
	/**
	 * pickupParamName
	 * input: if ((new Byte((generatedText19).getInvisible())).compareTo(new Byte(
	 * output: generatedText19
	 * @param s
	 * @return
	 */
	private static String pickupParamName(String s) {
		//TODO
		String param = "";
		if (s.contains(JspSplitterConstant.PREFIX_GENERATEDTEXT)) {
			int begin = s.indexOf(JspSplitterConstant.PREFIX_GENERATEDTEXT);
			param = s.substring(begin, begin + JspSplitterConstant.PREFIX_GENERATEDTEXT.length() + 2);
			if (param.indexOf(")") > 0) {
				param = param.substring(0, param.indexOf(")"));
			}
		}
		return param;
	}
	
	public static String getScreenName(String fileName) {
		String sResult = "";
		int begin = fileName.indexOf(JspSplitterConstant.KEYWORD_SCREENNAME_BEGIN) + 3;
		int end = fileName.indexOf(JspSplitterConstant.KEYWORD_SCREENNAME_END);
		sResult = JspSplitterConstant.KEYWORD_SCREENNAME_S + fileName.substring(begin, end);
		return sResult;
	}
}
