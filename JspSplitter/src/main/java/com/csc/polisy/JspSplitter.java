package com.csc.polisy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.polisy.util.FileUtils;
import com.csc.polisy.util.JspSplitterConstant;

/**
 * Bugzilla #4115
 * Currently, the POLISY (any probably any similar application in the INTEGRAL system) 
 * cannot run successfully in WebLogic Server due to "Big JSP file" problem. 
 * 
 * A temporary solution had been applied:
 * 	- statically translate JSP file to JAVA file, 
 * 	- manually split the _jspService of this JAVA file to several smaller methods.
 * 	- recompile this JAVA file and put into the correct package in the WAR file.
 * 
 * While this solution works, it's not convenient and requires human-power.
 * 
 * Expectation: introduce automation to the above steps. Ideally, every steps above can be automated.
 *
 */
public class JspSplitter {

	private static final Logger LOGGER = LoggerFactory.getLogger(JspSplitter.class);
	
	/**
	 * The command line is JspSplitter <java_file or directory_patch>
	 * @param args
	 */
	public static void main(String[] args) {		
		LOGGER.info("===== JspSplitter =====");
		if (args.length == 0) {
			LOGGER.info("The command line is JspSplitter <java_file or directory_patch>");
			LOGGER.info("E.g: JspSplitter D:/FSS/java/__s8791form.java or JspSplitter D:/FSS/java");
			return;
		}
		if (args == null || "/h".equalsIgnoreCase(args[0]) || "-h".equalsIgnoreCase(args[0])) {
			LOGGER.info("The command line is JspSplitter <java_file or directory_patch>");
			LOGGER.info("E.g: JspSplitter D:/FSS/java/__s8791form.java or JspSplitter D:/FSS/java");
			return;
		}
		LOGGER.info("Starting...");	
		JspSplitter jspSplitter = new JspSplitter();
		boolean result = true;
		try {
			File file = new File(args[0]);
			if (file.isDirectory()) {
				File[] files = FileUtils.getFiles(args[0]);
				for (File f : files) {
					String filePath = f.getPath();				
					LOGGER.info("Processing the file " + filePath + "...");
					jspSplitter.process(filePath);
					LOGGER.info("---------------");
				}
			} else if (file.isFile()) {
				LOGGER.info("Processing the file " + args[0] + "...");
				jspSplitter.process(args[0]);				
			} else {
				LOGGER.info("The command line is JspSplitter <java_file or directory_patch>");
				LOGGER.info("E.g: JspSplitter D:/FSS/java/__s8791form.java or JspSplitter D:/FSS/java");
				return;
			}
		} catch (IOException e) {
			result = false;
			LOGGER.error("main(String[])", e);
		}
		
		if (result) {
			LOGGER.info("Done. The output is " + args[0]);
		}
	}
	
	/**
	 * This is the main process to split the _jspService of JAVA file to several smaller methods. 
	 * The output file can build successful in Eclipse and WebLogic as well.
	 *  
	 * @param filePath: The Java file to be cut
	 * @throws IOException
	 */
	private void process(String filePath) throws IOException {
		String classFilePath = filePath.substring(filePath.length() - 3) + ".class";
		if (FileUtils.fileExist(classFilePath)) {
			return;
		}
		
		int startLine = identifyStartReadLineNumber(filePath);
		if (startLine < 4500) {
			LOGGER.info("The size of file is acceptable, ignore...");
			return;
		}
		
		int endLine = identifyEndReadLineNumber(filePath);
		
		// create a temp file
		String fileTemp =  filePath + "_";
		LOGGER.info("Creating a temporary file..." + fileTemp);
		
		String[] subMethodData = FileUtils.readBlockandPickupParams(filePath, startLine - 1, endLine - 2);
		
		// get main method
		String mainMethod = FileUtils.readFile(filePath, 0, startLine - 2);		
		String screenName = FileUtils.getScreenName(filePath);
		
		InputStream _SCallMethodTemplate = JspSplitter.class.getClassLoader().getResourceAsStream("_SCallMethodTemplate.tmpl");
		if (_SCallMethodTemplate == null) {
			LOGGER.error("Could not load _SCallMethodTemplate.tmpl");
		}
		String _SCallMethodTemplateData = FileUtils.readFile(_SCallMethodTemplate);
		// replace param 
		_SCallMethodTemplateData = _SCallMethodTemplateData.replaceAll(JspSplitterConstant.INSERT_PARAM_HERE, subMethodData[1]);
		FileUtils.writeFile(fileTemp, mainMethod + "\n" + _SCallMethodTemplateData);
		
		// splitting to a sub method
		LOGGER.info("Splitting to sub methods...");
		// read template 
		InputStream _STemplate = JspSplitter.class.getClassLoader().getResourceAsStream("_STemplate.tmpl");
		if (_STemplate == null) {
			LOGGER.error("Could not load _STemplate.tmpl");
		}
		String subMethodTemp = FileUtils.readFile(_STemplate);
		// replace param definition and code content
		subMethodTemp = subMethodTemp.replaceAll(JspSplitterConstant.INSERT_SCREENNAME_HERE, screenName);
		subMethodTemp = subMethodTemp.replaceAll(JspSplitterConstant.INSERT_PARAMDEFINATION_HERE, subMethodData[2]);
		subMethodTemp = subMethodTemp.replaceAll(JspSplitterConstant.INSERT_CODE_HERE, subMethodData[0]);
		
		// write to a temp file
		LOGGER.info("Writing content to a temporary file...");
		FileUtils.writeFile(fileTemp, subMethodTemp, true);
		
		// rename to org name
		LOGGER.info("Renaming..." + filePath);
		FileUtils.rename(fileTemp, filePath);
	}
	
	private int identifyStartReadLineNumber(String filePath) throws IOException {
		int size = FileUtils.countLineNumber(filePath);
		int start = Math.round(size/2);
		return FileUtils.identifyReadLineNumber(filePath, JspSplitterConstant.BREAKPOINT_START_1, JspSplitterConstant.BREAKPOINT_START_2, start);
	}
	
	private int identifyEndReadLineNumber(String filePath) throws IOException {
		return FileUtils.identifyReadLineNumber(filePath, JspSplitterConstant.BREAKPOINT_END);
	}
}
