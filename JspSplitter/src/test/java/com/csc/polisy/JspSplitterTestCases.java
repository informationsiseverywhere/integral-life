package com.csc.polisy;

import java.io.File;
import java.io.IOException;

import com.csc.polisy.util.FileUtils;

public class JspSplitterTestCases {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//testReadWriteFile();
		//testReadBlockandPickupParams();
		testGetFiles();
	}
	
	public static void testReadWriteFile() {
		try {
			String s = FileUtils.readFile("D:/DungLe/Projects/FSS/Temp/aaa.java");
			FileUtils.writeFile("D:/DungLe/Projects/FSS/Temp/bbb.java", s);
			System.out.println(FileUtils.readFile("D:/DungLe/Projects/FSS/Temp/bbb.java", 5, 10));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void testReadBlockandPickupParams() {
		try {
			String[] s = FileUtils.readBlockandPickupParams("D:/DungLe/Projects/FSS/Temp/aaa.java", 0, 5000);
			System.out.println("params = " + s[1]);
			System.out.println("paramDefinition = " + s[2]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void testGetFiles() {
		try {
			File[] files = FileUtils.getFiles("D:/DungLe/Projects/FSS/Temp/java");
			for(int i = 0; i < files.length; i++) {
				System.out.println(files[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
