JspSplitter

A. Description:
The tool automatically compile JSP files to Java, split them to ensure that compiled Java files compatible with Weblogic.
Compatible with Weblogic only.

B. Steps to do:
OPTION 1: Run JspSplitter.bat <ear_source_file>

1. Copy uber-JspSplitter-1.0.jar (from JspSplitter project) and JspSplitter.bat to the same CSCPolisyAsiaEAR.ear's folder.
	Structure of folder:
	|--FSS
		|--Weblogic
			|--CSCPolisyAsiaEAR_v3.12_32884.ear
			|--uber-JspSplitter-1.0.jar
			|--JspSplitter.bat
2. Run command line as JspSplitter <ear_source_file>
	JspSplitter D:\FSS\Weblogic\CSCPolisyAsiaEAR_v3.12_32884.ear
3. The output is <source_file>_target folder
	|--FSS
		|--Weblogic
			|--CSCPolisyAsiaEAR_v3.12_32884.ear_target	(folder)
			|--CSCPolisyAsiaEAR_v3.12_32884.ear			(.ear file)
			|--uber-JspSplitter-1.0.jar					(.jar file)
			|--JspSplitter.bat							(.bat file)
			
OPTION 2: Run JspSplitter.class <java_source_file>

1. Run JspSplitter.class <java_source_file>
2. The output is the same as java_source_file