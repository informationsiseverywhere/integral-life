package com.dxc.integral.listeners;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.dxc.integral.iaf.beans.ControlTotalDTO;
import com.dxc.integral.iaf.dao.ElogpfDAO;
import com.dxc.integral.iaf.dao.SbcontotpfDAO;
import com.dxc.integral.iaf.dao.model.Elogpf;
import com.dxc.integral.iaf.dao.model.Sbcontotpf;
import com.dxc.integral.iaf.reports.beans.r0005report.R0005Record;
import com.dxc.integral.iaf.reports.beans.r0005report.R0005Records;

/**
 * IntegralCommonStepListener
 * 
 * @author ajamodkar
 *
 */
@Component
@Scope(value = "step", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class IntegralStepListener implements StepExecutionListener {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(IntegralStepListener.class);

	@Autowired
	private ControlTotalDTO controlTotalDTO;

	@Autowired
	private SbcontotpfDAO sbcontotpfDAO;

	@Autowired
	private R0005Records r0005Records;

	@Value("${userid}")
	private String userName;

	@Value("${cntBranch}")
	private String branch;

	@Value("${chdrcoy}")
	private String company;

	@Value("${trandate}")
	private String effDate;

	@Value("${batchName}")
	private String batchName;

	@Autowired
	private ElogpfDAO elogpfDAO;
	
	/**
	 * Constructor 
	 */
	public IntegralStepListener() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dxc.integral.iaf.listener.IntegralStepListener#afterStep(org.
	 * springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.debug("IntegralCommonStepListener::afterStep::Entry");
		String stepName = stepExecution.getStepName();
		List<Throwable> exceptionList = stepExecution.getFailureExceptions();
		if (exceptionList.isEmpty()) {
			LOGGER.info("Control total update for batch starts");
			Sbcontotpf sbcontotpf = afterStepContinue(stepExecution);
			sbcontotpf.setContot40(controlTotalDTO.getControlTotal40());
			sbcontotpf.setContot41(controlTotalDTO.getControlTotal41());
			sbcontotpf.setContot42(controlTotalDTO.getControlTotal42());
			sbcontotpf.setContot43(controlTotalDTO.getControlTotal43());
			sbcontotpf.setContot44(controlTotalDTO.getControlTotal44());
			sbcontotpf.setContot45(controlTotalDTO.getControlTotal45());
			sbcontotpf.setContot46(controlTotalDTO.getControlTotal46());
			sbcontotpf.setContot47(controlTotalDTO.getControlTotal47());
			sbcontotpf.setContot48(controlTotalDTO.getControlTotal48());
			sbcontotpf.setContot49(controlTotalDTO.getControlTotal49());
			sbcontotpf.setContot50(controlTotalDTO.getControlTotal50());
			sbcontotpfDAO.insertSbcontotpf(sbcontotpf);

			R0005Record r0005Record = new R0005Record();
			r0005Record.setSbcontotpf(sbcontotpf);
			r0005Record.setStartDateTime(stepExecution.getStartTime());
			r0005Record.setEndDateTime(new Date());
			r0005Records.getR0005RecordList().add(r0005Record);
			LOGGER.info("Control total update for batch ends");
		} else {// ILIFE-5763 Starts
			String exception = (Arrays.toString(exceptionList.get(0).getStackTrace())).replaceAll("[\r\n]", "");
			LOGGER.error("Some exception occurred,logging it in Elogpf. {} ", exception);
			if (String.valueOf(exceptionList.get(0).getClass()).contains("ItemNotfoundException")) {
				Elogpf elogpf = populateElogpf("", exceptionList.get(0).getLocalizedMessage() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (String.valueOf(exceptionList.get(0).getClass()).contains("NullPointerException")) {
				Elogpf elogpf = populateElogpf("", stepName + ": Null Pointer Exception." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else if (String.valueOf(exceptionList.get(0).getClass()).contains("ParseException")) {
				Elogpf elogpf = populateElogpf("", stepName + ": Error occured while parsing the data." + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			} else {
				Elogpf elogpf = populateElogpf("", stepName + ":" + exceptionList.get(0).getClass() + ":"
						+ Arrays.toString(exceptionList.get(0).getStackTrace()).substring(0, 400), "", "");
				elogpfDAO.writeError(elogpf);
			}
		}
		LOGGER.debug("IntegralCommonStepListener::afterStep::Exit");
		return stepExecution.getExitStatus();
	}

	public Sbcontotpf afterStepContinue(StepExecution stepExecution) {	
		Sbcontotpf sbcontotpf = new Sbcontotpf();
		sbcontotpf.setStepname(stepExecution.getStepName());
		sbcontotpf.setUsrprf(userName);
		sbcontotpf.setBranch(branch);
		sbcontotpf.setCompany(company);
		sbcontotpf.setEffdate(Integer.parseInt(effDate));
		sbcontotpf.setJob_execution_id(stepExecution.getJobExecutionId());
		sbcontotpf.setJobnm(batchName);
		sbcontotpf.setDatime(new Timestamp(System.currentTimeMillis()));
		sbcontotpf.setContot1(new BigDecimal(stepExecution.getReadCount()));
		sbcontotpf.setContot2(controlTotalDTO.getControlTotal02());
		sbcontotpf.setContot3(controlTotalDTO.getControlTotal03());
		sbcontotpf.setContot4(new BigDecimal(stepExecution.getWriteCount()));
		sbcontotpf.setContot5(controlTotalDTO.getControlTotal05());
		sbcontotpf.setContot6(controlTotalDTO.getControlTotal06());
		sbcontotpf.setContot7(controlTotalDTO.getControlTotal07());
		sbcontotpf.setContot8(controlTotalDTO.getControlTotal08());
		sbcontotpf.setContot9(controlTotalDTO.getControlTotal09());
		sbcontotpf.setContot10(controlTotalDTO.getControlTotal10());
		sbcontotpf.setContot11(controlTotalDTO.getControlTotal11());
		sbcontotpf.setContot12(controlTotalDTO.getControlTotal12());
		sbcontotpf.setContot13(controlTotalDTO.getControlTotal13());
		sbcontotpf.setContot14(controlTotalDTO.getControlTotal14());
		sbcontotpf.setContot15(controlTotalDTO.getControlTotal15());
		sbcontotpf.setContot16(controlTotalDTO.getControlTotal16());
		sbcontotpf.setContot17(controlTotalDTO.getControlTotal17());
		sbcontotpf.setContot18(controlTotalDTO.getControlTotal18());
		sbcontotpf.setContot19(controlTotalDTO.getControlTotal19());
		sbcontotpf.setContot20(controlTotalDTO.getControlTotal20());
		sbcontotpf.setContot21(controlTotalDTO.getControlTotal21());
		sbcontotpf.setContot22(controlTotalDTO.getControlTotal22());
		sbcontotpf.setContot23(controlTotalDTO.getControlTotal23());
		sbcontotpf.setContot24(controlTotalDTO.getControlTotal24());
		sbcontotpf.setContot25(controlTotalDTO.getControlTotal25());
		sbcontotpf.setContot26(controlTotalDTO.getControlTotal26());
		sbcontotpf.setContot27(controlTotalDTO.getControlTotal27());
		sbcontotpf.setContot28(controlTotalDTO.getControlTotal28());
		sbcontotpf.setContot29(controlTotalDTO.getControlTotal29());
		sbcontotpf.setContot30(controlTotalDTO.getControlTotal30());
		sbcontotpf.setContot31(controlTotalDTO.getControlTotal31());
		sbcontotpf.setContot32(controlTotalDTO.getControlTotal32());
		sbcontotpf.setContot33(controlTotalDTO.getControlTotal33());
		sbcontotpf.setContot34(controlTotalDTO.getControlTotal34());
		sbcontotpf.setContot35(controlTotalDTO.getControlTotal35());
		sbcontotpf.setContot36(controlTotalDTO.getControlTotal36());
		sbcontotpf.setContot37(controlTotalDTO.getControlTotal37());
		sbcontotpf.setContot38(controlTotalDTO.getControlTotal38());
		sbcontotpf.setContot39(controlTotalDTO.getControlTotal39());
		return sbcontotpf;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.dxc.integral.iaf.listener.IntegralStepListener#populateElogpf(String,
	 * String,String,String)
	 */
	public Elogpf populateElogpf(String terminalId, String flogdata, String wsspcomn, String wsspuser) {
		Elogpf elogpf = new Elogpf();
		elogpf.setTerminalid(terminalId);
		elogpf.setFlogdata(flogdata);
		elogpf.setWsspcomn(wsspcomn);
		elogpf.setWsspuser(wsspuser);
		elogpf.setUserpf(userName);
		elogpf.setJobnm(batchName);
		elogpf.setDatime(new Timestamp(System.currentTimeMillis()));
		return elogpf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dxc.integral.iaf.listener.IntegralStepListener#beforeStep(org.
	 * springframework.batch.core.StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("IntegralCommonStepListener::beforeStep::Entry");
		controlTotalDTO.setControlTotal01(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal02(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal03(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal04(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal05(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal06(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal07(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal08(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal09(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal10(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal11(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal12(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal13(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal14(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal15(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal16(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal17(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal18(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal19(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal20(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal21(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal22(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal23(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal24(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal25(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal26(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal27(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal28(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal29(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal30(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal31(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal32(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal33(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal34(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal35(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal36(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal37(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal38(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal39(BigDecimal.ZERO);
		beforeStepContinue();
		LOGGER.debug("IntegralCommonStepListener::beforeStep::Exit");
	}
	
	public void beforeStepContinue(){
		controlTotalDTO.setControlTotal40(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal41(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal42(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal43(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal44(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal45(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal46(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal47(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal48(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal49(BigDecimal.ZERO);
		controlTotalDTO.setControlTotal50(BigDecimal.ZERO);
	}
	
}
