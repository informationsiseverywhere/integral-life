package com.dxc.integral.components;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import com.dxc.integral.beans.L2anendrlxProcessorDTO;
import com.dxc.integral.beans.L2anendrlxReaderDTO;
import com.dxc.integral.iaf.beans.ControlTotalDTO;
import com.dxc.integral.iaf.constants.CommonConstants;
import com.dxc.integral.iaf.dao.DescpfDAO;
import com.dxc.integral.iaf.dao.ItempfDAO;
import com.dxc.integral.iaf.dao.UsrdpfDAO;
import com.dxc.integral.iaf.dao.model.Descpf;
import com.dxc.integral.iaf.dao.model.Itempf;
import com.dxc.integral.iaf.dao.model.Slckpf;
import com.dxc.integral.iaf.dao.model.Usrdpf;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.iaf.smarttable.utils.SmartTableDataUtils;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.Datcon2;
import com.dxc.integral.iaf.utils.DatimeUtil;
import com.dxc.integral.iaf.utils.Sftlock;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;
import com.dxc.integral.life.utils.Crtloan;
import com.dxc.integral.life.utils.Paydebts;
import com.dxc.integral.utils.L2anendrlxProcessorUtil;

/**
 * ItemProcessor for L2ANENDRLX batch step.
 * 
 * @author vhukumagrawa
 *
 */

@Component
@Scope(value = "step", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class L2anendrlxItemProcessor implements ItemProcessor<L2anendrlxReaderDTO, L2anendrlxReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2anendrlxItemProcessor.class);

	/** The batch job name for L2ANENDRLX */
	public static final String L2ANENDRLX_JOB_NAME = "L2ANENDRLX";

	/** The batch transaction code for L2ANENDRLX */

	@Autowired
	private Datcon2 datcon2;

	@Autowired
	private UsrdpfDAO usrdpfDAO;

	@Autowired
	private L2anendrlxProcessorUtil l2anendrlxProcessorUtil;

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private Crtloan crtloan;

	@Autowired
	private Batcup batcup;

	@Autowired
	private DescpfDAO descpfDAO;

	@Autowired
	private Sftlock sftlock;

	@Autowired
	private ControlTotalDTO controlTotalDTO;

	private String programName = "BR622";

	@Autowired
	private ItempfDAO itempfDAO;

	/** The SmartTableDataUtils */
	@Autowired
	private SmartTableDataUtils smartTableDataUtils;/* IJTI-398 */

	@Value("${userid}")
	private String userName;

	@Value("${cntBranch}")
	private String branch;

	@Value("${transactionDate}")
	private String transactionDate;

	@Value("${businessDate}")
	private String businessDate;

	@Value("${batchName}")
	private String batchName;

	@Value("${chdrcoy}")
	private String company;

	@Value("${l2anendrlx.trancode}")
	private String batchTranCode;

	private T5645 t5645IO;
	private Descpf t5645Descpf;
	private Descpf t3695Descpf;

	/**
	 * Constructor 
	 */
	public L2anendrlxItemProcessor() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	@Nullable
	public L2anendrlxReaderDTO process(L2anendrlxReaderDTO readerDTOInput) throws IOException, ParseException {
		L2anendrlxReaderDTO readerDTOOutput = new L2anendrlxReaderDTO();
		readerDTOOutput = readerDTOOutput.cloneReaderDTO(readerDTOInput);
		String contractNo = readerDTOOutput.getChdrnum().replaceAll("[\r\n]", "");
		LOGGER.info("Processing starts for contract : {}", contractNo);
		/* ILIFE-5977 */
		readerDTOOutput.setBatctrcde(batchTranCode);
		boolean validCoverage = l2anendrlxProcessorUtil
				.checkValidCoverage(readerDTOOutput);/* ILIFE-5977 */

		if (!validCoverage) {
			LOGGER.info("Invalid contract : {}", contractNo);
			controlTotalDTO.setControlTotal02(controlTotalDTO.getControlTotal02().add(BigDecimal.ONE));
			return null;
		}

		Slckpf slckpf = getSlckpf(readerDTOOutput);
		if (!sftlock.lockEntity(slckpf)) {
			LOGGER.info("Contract [{}] is already locked.", contractNo);
			controlTotalDTO.setControlTotal03(controlTotalDTO.getControlTotal03().add(BigDecimal.ONE));
			readerDTOOutput = null;
		} else {
			
			Usrdpf usrdpf = usrdpfDAO.getUserInfo(userName);

			readReqSmartTableInfo(readerDTOOutput);// ILIFE-5763

			populateReaderDTO(readerDTOInput, readerDTOOutput, usrdpf, transactionDate);

			Integer tranDate = DatimeUtil.covertToyyyyMMddDate(transactionDate);
			L2anendrlxProcessorDTO processorDTO = l2anendrlxProcessorUtil.getPaymentDueInfo(readerDTOOutput,
					tranDate);/* ILIFE-5977 */

			Integer newDate = datcon2.plusDays(processorDTO.getEffectiveDate(), -1);
			// Old Zraepf record
			readerDTOOutput.setOldCurrto(newDate);
			readerDTOOutput.setDatime(new Timestamp(System.currentTimeMillis()));

			// New Zraepf record
			readerDTOOutput.setNewNpaydate(processorDTO.getNextPaydate());
			readerDTOOutput.setPaydte(processorDTO.getIndex(), processorDTO.getEffectiveDate());
			readerDTOOutput.setPaid(processorDTO.getIndex(), processorDTO.getPaymentDue());
			readerDTOOutput.setNewTotalamnt(readerDTOOutput.getTotamnt().add(processorDTO.getPaymentDue()));
			readerDTOOutput.setNewCurrfrom(processorDTO.getEffectiveDate());
			readerDTOOutput.setNewCurrto(99999999);
			readerDTOOutput.setNewPaymentOpt(processorDTO.getPayOpt());

			// ptrnpf required
			readerDTOOutput.setTrdt(Integer.valueOf(DatimeUtil.getCurrentDate()));
			readerDTOOutput.setTrtm(Integer.valueOf(DatimeUtil.getCurrentTime()));
			readerDTOOutput.setEffectiveDate(processorDTO.getEffectiveDate());
			readerDTOOutput.setPaymentDue(processorDTO.getPaymentDue());
			readerDTOOutput.setPaymmeth(processorDTO.getPaymmeth());

			CrtloanDTO crtloanDTO = l2anendrlxProcessorUtil.populateCrtloanDTO(readerDTOOutput, t5645IO,
					t5645Descpf.getLongdesc());
			crtloan.createLoan(crtloanDTO);/* ILIFE-5977 */

			if (null != readerDTOOutput.getNewPaymentOpt() && !readerDTOOutput.getNewPaymentOpt().trim().isEmpty()) {
				if ("paydebts".equalsIgnoreCase(readerDTOOutput.getNewPaymentOpt())) {
					ZrcshopDTO zrcshopDTO = l2anendrlxProcessorUtil.populateZrcshopDTO(readerDTOOutput, t5645IO,
							t3695Descpf.getLongdesc());/* ILIFE-5977 */
					((Paydebts) appContext.getBean(readerDTOOutput.getNewPaymentOpt().toLowerCase(Locale.getDefault())))
							.payOffDebt(zrcshopDTO);
					controlTotalDTO.setControlTotal06(controlTotalDTO.getControlTotal06().add(BigDecimal.ONE));
				}
			} else {
				controlTotalDTO.setControlTotal05(controlTotalDTO.getControlTotal05().add(BigDecimal.ONE));
			}
			LOGGER.info("Processing ends for contract : {}", contractNo);
	
		}
		return readerDTOOutput;	
	}

	public void populateReaderDTO(L2anendrlxReaderDTO readerDTOInput, L2anendrlxReaderDTO readerDTOOutput, Usrdpf usrdpf, String transactionDate) throws ParseException {

		String[] transactionDateParts = String.valueOf(transactionDate).split("-");

		String acctYear = transactionDateParts[2];
		String acctMonth = transactionDateParts[1];

		String	batchBacth = batcup.getBatchBatch(acctYear, acctMonth, readerDTOOutput.getChdrcoy(), batchTranCode,
					branch, String.valueOf(usrdpf.getUsernum()));
	
		Integer accountingMonth = Integer.valueOf(acctMonth);
		Integer accountingYear = Integer.valueOf(acctYear);

		readerDTOOutput.setUsrprf(usrdpf.getUserid());
		readerDTOOutput.setJobnm(batchName);
		readerDTOOutput.setPolicyTranno(readerDTOInput.getPolicyTranno() + 1);
		readerDTOOutput.setUserNum(usrdpf.getUsernum().intValue());
		readerDTOOutput.setBatcactmn(accountingMonth);
		readerDTOOutput.setBatcactyr(accountingYear);
		readerDTOOutput.setBatcbatch(batchBacth);
		readerDTOOutput.setBatcbrn(branch);
		readerDTOOutput.setBatccoy(company);
		readerDTOOutput.setBatcpfx(CommonConstants.BATCH_PREFIX);
		readerDTOOutput.setBatctrcde(batchTranCode);
		readerDTOOutput.setTermid(" ");
	}
	
	public Slckpf getSlckpf(L2anendrlxReaderDTO readerDTOOutput) {
		Slckpf slckpf = new Slckpf();
		slckpf.setEntity(readerDTOOutput.getChdrnum());
		slckpf.setEnttyp("CH");
		slckpf.setCompany(readerDTOOutput.getChdrcoy());
		slckpf.setProctrancd(batchTranCode);
		slckpf.setUsrprf(userName);
		slckpf.setJobnm(batchName);
		return slckpf;
	}
	
	private void readReqSmartTableInfo(L2anendrlxReaderDTO readerDTO) throws IOException {// ILIFE-5763
		getT5645Info(readerDTO);// ILIFE-5763 /*ILIFE-5977*/
		getT5645Descpf();
		getT3695Descpf(readerDTO);// ILIFE-5763
	}

	private void getT5645Info(L2anendrlxReaderDTO readerDTO) throws IOException {// ILIFE-5763
																					// /*ILIFE-5977*/

		Map<String, List<Itempf>> t5645Map = itempfDAO.readSmartTableByTableName(company, "T5645",
				CommonConstants.IT_ITEMPFX);/* ILIFE-5977 */
		if (null == t5645IO) {
			List<Itempf> t5645ItemList = t5645Map.get(programName);
			if (t5645ItemList != null) {
				t5645IO = smartTableDataUtils.convertGenareaToPojo(t5645ItemList.get(0).getGenareaj(),
						T5645.class); /* IJTI-398 */
			} else if (!t5645Map.containsKey(programName)) {// ILIFE-5763 Starts
				throw new ItemNotfoundException("Br622: Item " + programName
						+ " not found in Table T5645 while processing contract " + readerDTO.getChdrnum());
			} // ILIFE-5763 Ends
		}
	}

	private void getT3695Descpf(L2anendrlxReaderDTO readerDTO) {// ILIFE-5763

		if (t3695Descpf == null) {
			t3695Descpf = descpfDAO.getDescInfo(company, "T3695", t5645IO.getSacstypes().get(0));
		}
		if (null == t3695Descpf) {// ILIFE-5763 Starts
			throw new ItemNotfoundException("Br622: Description for item " + t5645IO.getSacstypes().get(0)
					+ " not found in Table T3695 while processing contract " + readerDTO.getChdrnum());
		} // ILIFE-5763 Ends
	}

	private void getT5645Descpf() {

		if (t5645Descpf == null) {
			t5645Descpf = descpfDAO.getDescInfo(company, "T5645", programName);
		}
	}/* ILIFE-5977 */

	/**
	 * Getter for batchTranCode
	 * 
	 * @return - the batchTranCode
	 */
	public String getBatchTranCode() {
		return batchTranCode;
	}

	@Override
	public String toString() {
		return "L2anendrlxItemProcessor [usrdpfDAO=" + usrdpfDAO + ", l2anendrlxProcessorUtil="
				+ l2anendrlxProcessorUtil + ", appContext=" + appContext + ", crtloan=" + crtloan + ", batcup=" + batcup
				+ ", descpfDAO=" + descpfDAO + ", sftlock=" + sftlock + ", controlTotalDTO=" + controlTotalDTO
				+ ", programName=" + programName + ", itempfDAO=" + itempfDAO + ", smartTableDataUtils="
				+ smartTableDataUtils + ", userName=" + userName + ", branch=" + branch + ", transactionDate="
				+ transactionDate + ", businessDate=" + businessDate + ", batchName=" + batchName + ", company="
				+ company + ", batchTranCode=" + batchTranCode + ", t5645IO=" + t5645IO + ", t5645Descpf=" + t5645Descpf
				+ ", t3695Descpf=" + t3695Descpf + "]";
	}

}
