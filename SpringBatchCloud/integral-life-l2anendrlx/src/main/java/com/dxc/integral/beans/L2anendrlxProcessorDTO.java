package com.dxc.integral.beans;

import java.math.BigDecimal;

/**
 * DTO for L2ANENDRLX processor.
 * 
 * @author mmalik25
 *
 */
public class L2anendrlxProcessorDTO {

	/** The nextPaydate */
	private Integer nextPaydate;
	/** The paymmeth */
	private String paymmeth;
	/** The payOpt */
	private String payOpt;
	/** The paymentDue */
	private BigDecimal paymentDue;
	/** The userNum */
	private Integer userNum;
	/** The contractCurr */
	private String contractCurr;
	/** The index */
	private Integer index;
	/** The effectiveDate */
	private Integer effectiveDate;
	/** The batchCode */
	private String batchCode;
	/** The batchName */
	private String batchName;
	/** The userProfile */
	private String userProfile;

	/**
	 * Constructor 
	 */
	public L2anendrlxProcessorDTO() {
		// Calling default constructor
	}

	/**
	 * @return the nextPaydate
	 */
	public Integer getNextPaydate() {
		return nextPaydate;
	}

	/**
	 * @param nextPaydate
	 *            the nextPaydate to set
	 */
	public void setNextPaydate(Integer nextPaydate) {
		this.nextPaydate = nextPaydate;
	}

	/**
	 * @return the paymmeth
	 */
	public String getPaymmeth() {
		return paymmeth;
	}

	/**
	 * @param paymmeth
	 *            the paymmeth to set
	 */
	public void setPaymmeth(String paymmeth) {
		this.paymmeth = paymmeth;
	}

	/**
	 * @return the payOpt
	 */
	public String getPayOpt() {
		return payOpt;
	}

	/**
	 * @param payOpt
	 *            the payOpt to set
	 */
	public void setPayOpt(String payOpt) {
		this.payOpt = payOpt;
	}

	/**
	 * @return the paymentDue
	 */
	public BigDecimal getPaymentDue() {
		return paymentDue;
	}

	/**
	 * @param paymentDue
	 *            the paymentDue to set
	 */
	public void setPaymentDue(BigDecimal paymentDue) {
		this.paymentDue = paymentDue;
	}

	/**
	 * @return the userNum
	 */
	public Integer getUserNum() {
		return userNum;
	}

	/**
	 * @param userNum
	 *            the userNum to set
	 */
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}

	/**
	 * @return the contractCurr
	 */
	public String getContractCurr() {
		return contractCurr;
	}

	/**
	 * @param contractCurr
	 *            the contractCurr to set
	 */
	public void setContractCurr(String contractCurr) {
		this.contractCurr = contractCurr;
	}

	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * @return the effectiveDate
	 */
	public Integer getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(Integer effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the batchCode
	 */
	public String getBatchCode() {
		return batchCode;
	}

	/**
	 * @param batchCode
	 *            the batchCode to set
	 */
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	/**
	 * @return the batchName
	 */
	public String getBatchName() {
		return batchName;
	}

	/**
	 * @param batchName
	 *            the batchName to set
	 */
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	/**
	 * @return the userProfile
	 */
	public String getUserProfile() {
		return userProfile;
	}

	/**
	 * @param userProfile
	 *            the userProfile to set
	 */
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	@Override
	public String toString() {
		return "L2anendrlxProcessorDTO [nextPaydate=" + nextPaydate + ", paymmeth=" + paymmeth + ", payOpt=" + payOpt
				+ ", paymentDue=" + paymentDue + ", userNum=" + userNum + ", contractCurr=" + contractCurr + ", index="
				+ index + ", effectiveDate=" + effectiveDate + ", batchCode=" + batchCode + ", batchName=" + batchName
				+ ", userProfile=" + userProfile + "]";
	}

}
