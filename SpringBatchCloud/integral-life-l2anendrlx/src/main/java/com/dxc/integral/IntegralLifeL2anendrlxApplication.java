package com.dxc.integral;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;

@EnableTask
@EnableBatchProcessing
@SpringBootApplication
public class IntegralLifeL2anendrlxApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegralLifeL2anendrlxApplication.class);
	
	/**
	 * Constructor 
	 */
	public IntegralLifeL2anendrlxApplication() {
		// Calling default constructor
	}
	
	public static void main(String[] args) {

		// ensure that job params are provided
		if (args == null || args.length == 0) {
			LOGGER.error("Job Parameters are missing. Kindly provide Job Parameters as program arguments.");
			return;
		}

		// set job params as system properties to be read by configuration
		for (String arg : args) {
			String[] splitted = arg.split("=");
			System.setProperty(splitted[0], splitted[1]);

		}
		SpringApplication.run(IntegralLifeL2anendrlxApplication.class, args);
	}

}
