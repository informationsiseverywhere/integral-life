package com.dxc.integral.components;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.dxc.integral.beans.L2anendrlxReaderDTO;
import com.dxc.integral.iaf.utils.Batcup;
import com.dxc.integral.iaf.utils.Sftlock;

/**
 * ItemWriter for L2ANENDRLX batch step.
 * 
 * @author vhukumagrawa
 *
 */
@Component
@Scope(value = "step", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class L2anendrlxItemWriter implements ItemWriter<L2anendrlxReaderDTO> {

	/** The LOGGER Object */
	private static final Logger LOGGER = LoggerFactory.getLogger(L2anendrlxItemWriter.class);

	@Autowired
	private Sftlock sftlock;

	@Autowired
	private Batcup batcup;

	@Value("${businessDate}")
	private String businessDate;

	@Value("${batchName}")
	private String batchName;

	private int tranCount;

	@Autowired
	@Qualifier("l2anendrlxChdrpfWriter")
	private ItemWriter<L2anendrlxReaderDTO> l2anendrlxChdrpfWriter;

	@Autowired
	@Qualifier("l2anendrlxPtrnpfWriter")
	private ItemWriter<L2anendrlxReaderDTO> l2anendrlxPtrnpfWriter;

	@Autowired
	@Qualifier("l2anendrlxZraepfInsertWriter")
	private ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfInsertWriter;

	@Autowired
	@Qualifier("l2anendrlxZraepfUpdateWriter")
	private ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfUpdateWriter;

	/**
	 * Constructor 
	 */
	public L2anendrlxItemWriter() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	@Override
	public void write(List<? extends L2anendrlxReaderDTO> readerDTOList) throws Exception {

		LOGGER.info("L2anendrlxItemWriter starts.");
		l2anendrlxZraepfUpdateWriter.write(readerDTOList);
		l2anendrlxChdrpfWriter.write(readerDTOList);
		l2anendrlxZraepfInsertWriter.write(readerDTOList);
		l2anendrlxPtrnpfWriter.write(readerDTOList);
		synchronized (L2anendrlxItemWriter.class) {
			if (!readerDTOList.isEmpty()) {
				L2anendrlxReaderDTO readerDTO = readerDTOList.get(0);
				tranCount = batcup.processBatcpf(String.valueOf(readerDTO.getBatcactyr()),
						String.valueOf(readerDTO.getBatcactmn()), businessDate, readerDTO.getChdrcoy(),
						readerDTO.getBatctrcde(), readerDTO.getBatcbrn(), batchName, readerDTO.getBatcbatch(),
						readerDTO.getUsrprf(), String.valueOf(readerDTO.getUserNum()), readerDTOList.size(), tranCount);
				LOGGER.info("tranCount:{}", tranCount);// IJTI-1498
			}
		}
		if (!readerDTOList.isEmpty()) {
			if (0 >= readerDTOList.get(0).getChdrnum()
					.compareToIgnoreCase(readerDTOList.get(readerDTOList.size() - 1).getChdrnum())) {
				sftlock.unlockByRange("CH", readerDTOList.get(0).getChdrcoy(), readerDTOList.get(0).getChdrnum(),
						readerDTOList.get(readerDTOList.size() - 1).getChdrnum());
			} else {
				sftlock.unlockByRange("CH", readerDTOList.get(0).getChdrcoy(),
						readerDTOList.get(readerDTOList.size() - 1).getChdrnum(), readerDTOList.get(0).getChdrnum());

			}
		}

		LOGGER.info("L2anendrlxItemWriter ends.");
	}

	@Override
	public String toString() {
		return "L2anendrlxItemWriter [sftlock=" + sftlock + ", batcup=" + batcup + ", businessDate=" + businessDate
				+ ", batchName=" + batchName + ", tranCount=" + tranCount + ", l2anendrlxChdrpfWriter="
				+ l2anendrlxChdrpfWriter + ", l2anendrlxPtrnpfWriter=" + l2anendrlxPtrnpfWriter
				+ ", l2anendrlxZraepfInsertWriter=" + l2anendrlxZraepfInsertWriter + ", l2anendrlxZraepfUpdateWriter="
				+ l2anendrlxZraepfUpdateWriter + "]";
	}

}
