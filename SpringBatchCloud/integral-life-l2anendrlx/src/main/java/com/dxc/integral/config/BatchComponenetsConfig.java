package com.dxc.integral.config;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;
import com.dxc.integral.beans.L2anendrlxReaderDTO;

/**
 * Configuration of reader and writer for L2ANENDRLX.
 * 
 * @author vhukumagrawa
 *
 */
@Configuration
public class BatchComponenetsConfig {
	
	public BatchComponenetsConfig() {
		//default no-arg constructor
	}
	
	@Value("${chdrcoy}")
	private String company;

	@Value("${chdrnumFrom}")
	private String chdrNoFrom;

	@Value("${chdrnumTo}")
	private String chdrNoTo;

	@Value("${trandate}")
	private String tranDate;
	
	@Bean
	@StepScope
	public ItemReader<L2anendrlxReaderDTO> jdbcPagingItemReader(DataSource dataSource,
			PagingQueryProvider queryProvider, RowMapper<L2anendrlxReaderDTO> l2anendrlxReaderRowMapper) {

		Map<String, Object> parameterValues = new HashMap<>();
		parameterValues.put("chdrcoy", company);
		parameterValues.put("chdrnumTo", chdrNoTo);
		parameterValues.put("chdrnumFrom", chdrNoFrom);
		parameterValues.put("trandate", tranDate);
		parameterValues.put("_npaydate", "");
		return new JdbcPagingItemReaderBuilder<L2anendrlxReaderDTO>().dataSource(dataSource).name("l2anendrlxReader")
				.queryProvider(queryProvider).parameterValues(parameterValues).rowMapper(l2anendrlxReaderRowMapper)
				.pageSize(10).build();
	}

	@Bean
	public SqlPagingQueryProviderFactoryBean queryProvider(DataSource dataSource) {
		SqlPagingQueryProviderFactoryBean provider = new SqlPagingQueryProviderFactoryBean();
		provider.setDataSource(dataSource);
		provider.setSelectClause(
				"select a.unique_number as uniqueNumber, c.statcode, c.pstatcode, c.sumins, c.plnsfx as plansuffix,"
						+ " b.tranno as policyTranno, b.cntcurr as policyCurr, b.cnttype as policyType ,b.cowncoy  as cownCoy, b.billcurr as policyBillCurr,"
						+ " b.occdate as policyOccdate,  a.*");
		provider.setFromClause("from zraepf a, chdrpf b , covrpf c");
		provider.setWhereClause(
				"where a.CHDRCOY = :chdrcoy AND a.VALIDFLAG = b.VALIDFLAG  AND a.CHDRNUM BETWEEN :chdrnumFrom"
						+ " AND :chdrnumTo AND a.NPAYDATE != 0  AND a.NPAYDATE <= :trandate"
						+ " and a.chdrnum=b.chdrnum and a.chdrcoy=b.chdrcoy and c.validflag=1"
						+ " and a.chdrnum=c.chdrnum and a.chdrcoy=c.chdrcoy and a.life=c.life and b.validflag=1"
						+ " and a.coverage=c.coverage and a.rider=c.rider and c.plnsfx=0");
		provider.setSortKey("npaydate");
		return provider;
	}
	
	@Bean
	public BeanPropertyItemSqlParameterSourceProvider<L2anendrlxReaderDTO> itemSqlParameterSourceProvider() {
		return new BeanPropertyItemSqlParameterSourceProvider<>();
	}

	@Bean
	public ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfUpdateWriter(DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<L2anendrlxReaderDTO>()
				.itemSqlParameterSourceProvider(itemSqlParameterSourceProvider())
				.sql("update zraepf set validflag=2  ,currto =:oldCurrto, "
						+ "                 usrprf=:usrprf , jobnm=:jobnm , datime=CURRENT_TIMESTAMP"
						+ "                 where chdrnum=:chdrnum and chdrcoy=:chdrcoy and life=:life "
						+ "                 and coverage=:coverage"
						+ "                 and rider=:rider and plnsfx=:plnsfx and currto=:currto")
				.dataSource(dataSource).build();
	}

	@Bean
	public ItemWriter<L2anendrlxReaderDTO> l2anendrlxChdrpfWriter(DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<L2anendrlxReaderDTO>()
				.itemSqlParameterSourceProvider(itemSqlParameterSourceProvider())
				.sql("update chdrpf set tranno=:policyTranno, "
						+ " usrprf=:usrprf, jobnm=:jobnm , datime=CURRENT_TIMESTAMP"
						+ " where chdrnum=:chdrnum and chdrcoy=:chdrcoy")
				.dataSource(dataSource).build();
	}

	@Bean
	public ItemWriter<L2anendrlxReaderDTO> l2anendrlxZraepfInsertWriter(DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<L2anendrlxReaderDTO>()
				.itemSqlParameterSourceProvider(itemSqlParameterSourceProvider())
				.sql("insert into zraepf"
						+ " (chdrcoy,chdrnum,life,coverage,rider,plnsfx,validflag,tranno,currfrom,"
						+ " currto,zrduedte01,zrduedte02,zrduedte03,zrduedte04,zrduedte05,zrduedte06,zrduedte07,"
						+ " zrduedte08,prcnt01,prcnt02,prcnt03,prcnt04,prcnt05,prcnt06,prcnt07,prcnt08,paydte01,"
						+ " paydte02,paydte03,paydte04,paydte05,paydte06,paydte07,paydte08,paid01,paid02,paid03,"
						+ " paid04,paid05,paid06,paid07,paid08,paymmeth01,paymmeth02,paymmeth03,paymmeth04,"
						+ " paymmeth05,paymmeth06,paymmeth07,paymmeth08,zrpayopt01,zrpayopt02,zrpayopt03,zrpayopt04,"
						+ " zrpayopt05,zrpayopt06,zrpayopt07,zrpayopt08,totamnt,npaydate,payclt,bankkey,bankacckey,"
						+ " paycurr,usrprf,jobnm,datime) values"
						+ " (:chdrcoy,:chdrnum,:life,:coverage,:rider,:plnsfx,1,:policyTranno,:newCurrfrom,"
						+ " :newCurrto,:zrduedte01,:zrduedte02,:zrduedte03,:zrduedte04,:zrduedte05,:zrduedte06,"
						+ " :zrduedte07,:zrduedte08,:prcnt01,:prcnt02,:prcnt03,:prcnt04,:prcnt05,:prcnt06,"
						+ " :prcnt07,:prcnt08,:paydte01,:paydte02,:paydte03,:paydte04,:paydte05,:paydte06,"
						+ " :paydte07,:paydte08,:paid01,:paid02,:paid03,:paid04,:paid05,:paid06,:paid07,"
						+ " :paid08,:paymmeth01,:paymmeth02,:paymmeth03,:paymmeth04,:paymmeth05,:paymmeth06,"
						+ " :paymmeth07,:paymmeth08,:zrpayopt01,:zrpayopt02,:zrpayopt03,:zrpayopt04,:zrpayopt05,"
						+ " :zrpayopt06,:zrpayopt07,:zrpayopt08,:newTotalamnt,:newNpaydate,:payclt,:bankkey,:bankacckey,"
						+ " :paycurr,:usrprf,:jobnm,CURRENT_TIMESTAMP)")
				.dataSource(dataSource).build();
	}

	@Bean
	public ItemWriter<L2anendrlxReaderDTO> l2anendrlxPtrnpfWriter(DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<L2anendrlxReaderDTO>()
				.itemSqlParameterSourceProvider(itemSqlParameterSourceProvider())
				.sql("insert into PTRNPF (CHDRPFX,CHDRCOY,CHDRNUM,RECODE,TRANNO,TRDT,TRTM,"
						+ " PTRNEFF,TERMID,USER_T,BATCPFX,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,"
						+ " BATCTRCDE,BATCBATCH,PRTFLG,VALIDFLAG,DATESUB,CRTUSER,"
						+ " USRPRF,JOBNM,DATIME) "
						+ " values ('  ',:chdrcoy,:chdrnum,'  ',:policyTranno,:trdt,:trtm,"
						+ " :effectiveDate,:termid,:userNum,:batcpfx,:batccoy,:batcbrn,:batcactyr,:Batcactmn,"
						+ " :batctrcde,:batcbatch,'  ',' ',:effectiveDate,' ',"
						+ " :usrprf,:jobnm,CURRENT_TIMESTAMP)")
				.dataSource(dataSource).build();
	}
}
