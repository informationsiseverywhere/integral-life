package com.dxc.integral.components;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.dxc.integral.beans.L2anendrlxReaderDTO;

/**
 * RowMapper for L2anendrlxReaderDTO.
 * 
 * @author mmalik25
 */
@Component
public class L2anendrlxReaderRowMapper implements RowMapper<L2anendrlxReaderDTO> {
	
	/**
	 * Constructor
	 */
	public L2anendrlxReaderRowMapper() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */
	@Override
	public L2anendrlxReaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		L2anendrlxReaderDTO output = new L2anendrlxReaderDTO();
		return output.mapResultSetToDTO(rs);

	}

}