package com.dxc.integral.beans;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.lang.Nullable;

/**
 * L2ANENDRLX batch reader DTO.
 * 
 * @author mmalik25
 *
 */
public class L2anendrlxReaderDTO {

	/** The uniqueNumber */
	private int uniqueNumber;
	/** The chdrcoy */
	private String chdrcoy;
	/** The life */
	private String life;
	/** The chdrnum */
	private String chdrnum;
	/** The coverage */
	private String coverage;
	/** The rider */
	private String rider;
	/** The plnsfx */
	private Integer plnsfx;
	/** The validflag */
	private String validflag;
	/** The tranno */
	private Integer tranno;
	/** The currfrom */
	private Integer currfrom;
	/** The currto */
	private Integer currto;
	/** The zrduedte01 */
	private Integer zrduedte01;
	/** The zrduedte02 */
	private Integer zrduedte02;
	/** The zrduedte03 */
	private Integer zrduedte03;
	/** The zrduedte04 */
	private Integer zrduedte04;
	/** The zrduedte05 */
	private Integer zrduedte05;
	/** The zrduedte06 */
	private Integer zrduedte06;
	/** The zrduedte07 */
	private Integer zrduedte07;
	/** The zrduedte08 */
	private Integer zrduedte08;
	/** The prcnt01 */
	private BigDecimal prcnt01;
	/** The prcnt02 */
	private BigDecimal prcnt02;
	/** The prcnt03 */
	private BigDecimal prcnt03;
	/** The prcnt04 */
	private BigDecimal prcnt04;
	/** The prcnt05 */
	private BigDecimal prcnt05;
	/** The prcnt06 */
	private BigDecimal prcnt06;
	/** The prcnt07 */
	private BigDecimal prcnt07;
	/** The prcnt08 */
	private BigDecimal prcnt08;
	/** The paydte01 */
	private Integer paydte01;
	/** The paydte02 */
	private Integer paydte02;
	/** The paydte03 */
	private Integer paydte03;
	/** The paydte04 */
	private Integer paydte04;
	/** The paydte05 */
	private Integer paydte05;
	/** The paydte06 */
	private Integer paydte06;
	/** The paydte07 */
	private Integer paydte07;
	/** The paydte08 */
	private Integer paydte08;
	/** The paid01 */
	private BigDecimal paid01;
	/** The paid02 */
	private BigDecimal paid02;
	/** The paid03 */
	private BigDecimal paid03;
	/** The paid04 */
	private BigDecimal paid04;
	/** The paid05 */
	private BigDecimal paid05;
	/** The paid06 */
	private BigDecimal paid06;
	/** The paid07 */
	private BigDecimal paid07;
	/** The paid08 */
	private BigDecimal paid08;
	/** The paymmeth01 */
	private String paymmeth01;
	/** The paymmeth02 */
	private String paymmeth02;
	/** The paymmeth03 */
	private String paymmeth03;
	/** The paymmeth04 */
	private String paymmeth04;
	/** The paymmeth05 */
	private String paymmeth05;
	/** The paymmeth06 */
	private String paymmeth06;
	/** The paymmeth07 */
	private String paymmeth07;
	/** The paymmeth08 */
	private String paymmeth08;
	/** The zrpayopt01 */
	private String zrpayopt01;
	/** The zrpayopt02 */
	private String zrpayopt02;
	/** The zrpayopt03 */
	private String zrpayopt03;
	/** The zrpayopt04 */
	private String zrpayopt04;
	/** The zrpayopt05 */
	private String zrpayopt05;
	/** The zrpayopt06 */
	private String zrpayopt06;
	/** The zrpayopt07 */
	private String zrpayopt07;
	/** The zrpayopt08 */
	private String zrpayopt08;
	/** The totamnt */
	private BigDecimal totamnt;
	/** The npaydate */
	private Integer npaydate;
	/** The payclt */
	private String payclt;
	/** The bankkey */
	private String bankkey;
	/** The bankacckey */
	private String bankacckey;
	/** The paycurr */
	private String paycurr;
	/** The usrprf */
	private String usrprf;
	/** The jobnm */
	private String jobnm;
	/** The datime */
	private Timestamp datime;
	/** The paymentDue */
	private BigDecimal paymentDue;
	/** The policyTranno */
	private Integer policyTranno;
	/** The policyCurr */
	private String policyCurr;
	/** The policyType */
	private String policyType;
	/** The policyBillCurr */
	private String policyBillCurr;
	/** The policyOccdate */
	private Integer policyOccdate;

	/** The sumins */
	private BigDecimal sumins;
	/** The statcode */
	private String statcode;
	/** The pstatcode */
	private String pstatcode;
	/** The plansuffix */
	private int plansuffix;

	/** The oldCurrto */
	private Integer oldCurrto;
	/** The newNpaydate */
	private Integer newNpaydate;
	/** The newTotalamnt */
	private BigDecimal newTotalamnt;
	/** The newCurrto */
	private Integer newCurrto;
	/** The newCurrfrom */
	private Integer newCurrfrom;
	/** The newPaymentOpt */
	private String newPaymentOpt;
	/** The paymmeth */
	private String paymmeth;

	/** The batcbrn */
	private String batcbrn;
	/** The termid */
	private String termid;
	/** The batcactyr */
	private Integer batcactyr;
	/** The batcactmn */
	private Integer batcactmn;
	/** The batctrcde */
	private String batctrcde;
	/** The batcbatch */
	private String batcbatch;
	/** The batcpfx */
	private String batcpfx;
	/** The batccoy */
	private String batccoy;
	/** The effectiveDate */
	private Integer effectiveDate;
	/** The trdt */
	private Integer trdt;
	/** The trtm */
	private Integer trtm;
	/** The userNum */
	private Integer userNum;
	/** The tranCount */
	private int tranCount;
	/** The cownCoy */
	private String cownCoy;/* IJTI-379 */

	/**
	 * @return the uniqueNumber
	 */
	public int getUniqueNumber() {
		return uniqueNumber;
	}

	/**
	 * @param uniqueNumber
	 *            the uniqueNumber to set
	 */
	public void setUniqueNumber(int uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * @param chdrcoy
	 *            the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * @return the life
	 */
	public String getLife() {
		return life;
	}

	/**
	 * @param life
	 *            the life to set
	 */
	public void setLife(String life) {
		this.life = life;
	}

	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * @param chdrnum
	 *            the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * @param coverage
	 *            the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * @return the rider
	 */
	public String getRider() {
		return rider;
	}

	/**
	 * @param rider
	 *            the rider to set
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}

	/**
	 * @return the plnsfx
	 */
	public Integer getPlnsfx() {
		return plnsfx;
	}

	/**
	 * @param plnsfx
	 *            the plnsfx to set
	 */
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	/**
	 * @return the validflag
	 */
	public String getValidflag() {
		return validflag;
	}

	/**
	 * @param validflag
	 *            the validflag to set
	 */
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	/**
	 * @return the tranno
	 */
	public Integer getTranno() {
		return tranno;
	}

	/**
	 * @param tranno
	 *            the tranno to set
	 */
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	/**
	 * @return the currfrom
	 */
	public Integer getCurrfrom() {
		return currfrom;
	}

	/**
	 * @param currfrom
	 *            the currfrom to set
	 */
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}

	/**
	 * @return the currto
	 */
	public Integer getCurrto() {
		return currto;
	}

	/**
	 * @param currto
	 *            the currto to set
	 */
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}

	/**
	 * @return the zrduedte01
	 */
	public Integer getZrduedte01() {
		return zrduedte01;
	}

	/**
	 * @param zrduedte01
	 *            the zrduedte01 to set
	 */
	public void setZrduedte01(Integer zrduedte01) {
		this.zrduedte01 = zrduedte01;
	}

	/**
	 * @return the zrduedte02
	 */
	public Integer getZrduedte02() {
		return zrduedte02;
	}

	/**
	 * @param zrduedte02
	 *            the zrduedte02 to set
	 */
	public void setZrduedte02(Integer zrduedte02) {
		this.zrduedte02 = zrduedte02;
	}

	/**
	 * @return the zrduedte03
	 */
	public Integer getZrduedte03() {
		return zrduedte03;
	}

	/**
	 * @param zrduedte03
	 *            the zrduedte03 to set
	 */
	public void setZrduedte03(Integer zrduedte03) {
		this.zrduedte03 = zrduedte03;
	}

	/**
	 * @return the zrduedte04
	 */
	public Integer getZrduedte04() {
		return zrduedte04;
	}

	/**
	 * @param zrduedte04
	 *            the zrduedte04 to set
	 */
	public void setZrduedte04(Integer zrduedte04) {
		this.zrduedte04 = zrduedte04;
	}

	/**
	 * @return the zrduedte05
	 */
	public Integer getZrduedte05() {
		return zrduedte05;
	}

	/**
	 * @param zrduedte05
	 *            the zrduedte05 to set
	 */
	public void setZrduedte05(Integer zrduedte05) {
		this.zrduedte05 = zrduedte05;
	}

	/**
	 * @return the zrduedte06
	 */
	public Integer getZrduedte06() {
		return zrduedte06;
	}

	/**
	 * @param zrduedte06
	 *            the zrduedte06 to set
	 */
	public void setZrduedte06(Integer zrduedte06) {
		this.zrduedte06 = zrduedte06;
	}

	/**
	 * @return the zrduedte07
	 */
	public Integer getZrduedte07() {
		return zrduedte07;
	}

	/**
	 * @param zrduedte07
	 *            the zrduedte07 to set
	 */
	public void setZrduedte07(Integer zrduedte07) {
		this.zrduedte07 = zrduedte07;
	}

	/**
	 * @return the zrduedte08
	 */
	public Integer getZrduedte08() {
		return zrduedte08;
	}

	/**
	 * @param zrduedte08
	 *            the zrduedte08 to set
	 */
	public void setZrduedte08(Integer zrduedte08) {
		this.zrduedte08 = zrduedte08;
	}

	/**
	 * @return the prcnt01
	 */
	public BigDecimal getPrcnt01() {
		return prcnt01;
	}

	/**
	 * @param prcnt01
	 *            the prcnt01 to set
	 */
	public void setPrcnt01(BigDecimal prcnt01) {
		this.prcnt01 = prcnt01;
	}

	/**
	 * @return the prcnt02
	 */
	public BigDecimal getPrcnt02() {
		return prcnt02;
	}

	/**
	 * @param prcnt02
	 *            the prcnt02 to set
	 */
	public void setPrcnt02(BigDecimal prcnt02) {
		this.prcnt02 = prcnt02;
	}

	/**
	 * @return the prcnt03
	 */
	public BigDecimal getPrcnt03() {
		return prcnt03;
	}

	/**
	 * @param prcnt03
	 *            the prcnt03 to set
	 */
	public void setPrcnt03(BigDecimal prcnt03) {
		this.prcnt03 = prcnt03;
	}

	/**
	 * @return the prcnt04
	 */
	public BigDecimal getPrcnt04() {
		return prcnt04;
	}

	/**
	 * @param prcnt04
	 *            the prcnt04 to set
	 */
	public void setPrcnt04(BigDecimal prcnt04) {
		this.prcnt04 = prcnt04;
	}

	/**
	 * @return the prcnt05
	 */
	public BigDecimal getPrcnt05() {
		return prcnt05;
	}

	/**
	 * @param prcnt05
	 *            the prcnt05 to set
	 */
	public void setPrcnt05(BigDecimal prcnt05) {
		this.prcnt05 = prcnt05;
	}

	/**
	 * @return the prcnt06
	 */
	public BigDecimal getPrcnt06() {
		return prcnt06;
	}

	/**
	 * @param prcnt06
	 *            the prcnt06 to set
	 */
	public void setPrcnt06(BigDecimal prcnt06) {
		this.prcnt06 = prcnt06;
	}

	/**
	 * @return the prcnt07
	 */
	public BigDecimal getPrcnt07() {
		return prcnt07;
	}

	/**
	 * @param prcnt07
	 *            the prcnt07 to set
	 */
	public void setPrcnt07(BigDecimal prcnt07) {
		this.prcnt07 = prcnt07;
	}

	/**
	 * @return the prcnt08
	 */
	public BigDecimal getPrcnt08() {
		return prcnt08;
	}

	/**
	 * @param prcnt08
	 *            the prcnt08 to set
	 */
	public void setPrcnt08(BigDecimal prcnt08) {
		this.prcnt08 = prcnt08;
	}

	/**
	 * @return the paydte01
	 */
	public Integer getPaydte01() {
		return paydte01;
	}

	/**
	 * @param paydte01
	 *            the paydte01 to set
	 */
	public void setPaydte01(Integer paydte01) {
		this.paydte01 = paydte01;
	}

	/**
	 * @return the paydte02
	 */
	public Integer getPaydte02() {
		return paydte02;
	}

	/**
	 * @param paydte02
	 *            the paydte02 to set
	 */
	public void setPaydte02(Integer paydte02) {
		this.paydte02 = paydte02;
	}

	/**
	 * @return the paydte03
	 */
	public Integer getPaydte03() {
		return paydte03;
	}

	/**
	 * @param paydte03
	 *            the paydte03 to set
	 */
	public void setPaydte03(Integer paydte03) {
		this.paydte03 = paydte03;
	}

	/**
	 * @return the paydte04
	 */
	public Integer getPaydte04() {
		return paydte04;
	}

	/**
	 * @param paydte04
	 *            the paydte04 to set
	 */
	public void setPaydte04(Integer paydte04) {
		this.paydte04 = paydte04;
	}

	/**
	 * @return the paydte05
	 */
	public Integer getPaydte05() {
		return paydte05;
	}

	/**
	 * @param paydte05
	 *            the paydte05 to set
	 */
	public void setPaydte05(Integer paydte05) {
		this.paydte05 = paydte05;
	}

	/**
	 * @return the paydte06
	 */
	public Integer getPaydte06() {
		return paydte06;
	}

	/**
	 * @param paydte06
	 *            the paydte06 to set
	 */
	public void setPaydte06(Integer paydte06) {
		this.paydte06 = paydte06;
	}

	/**
	 * @return the paydte07
	 */
	public Integer getPaydte07() {
		return paydte07;
	}

	/**
	 * @param paydte07
	 *            the paydte07 to set
	 */
	public void setPaydte07(Integer paydte07) {
		this.paydte07 = paydte07;
	}

	/**
	 * @return the paydte08
	 */
	public Integer getPaydte08() {
		return paydte08;
	}

	/**
	 * @param paydte08
	 *            the paydte08 to set
	 */
	public void setPaydte08(Integer paydte08) {
		this.paydte08 = paydte08;
	}

	/**
	 * @return the paid01
	 */
	public BigDecimal getPaid01() {
		return paid01;
	}

	/**
	 * @param paid01
	 *            the paid01 to set
	 */
	public void setPaid01(BigDecimal paid01) {
		this.paid01 = paid01;
	}

	/**
	 * @return the paid02
	 */
	public BigDecimal getPaid02() {
		return paid02;
	}

	/**
	 * @param paid02
	 *            the paid02 to set
	 */
	public void setPaid02(BigDecimal paid02) {
		this.paid02 = paid02;
	}

	/**
	 * @return the paid03
	 */
	public BigDecimal getPaid03() {
		return paid03;
	}

	/**
	 * @param paid03
	 *            the paid03 to set
	 */
	public void setPaid03(BigDecimal paid03) {
		this.paid03 = paid03;
	}

	/**
	 * @return the paid04
	 */
	public BigDecimal getPaid04() {
		return paid04;
	}

	/**
	 * @param paid04
	 *            the paid04 to set
	 */
	public void setPaid04(BigDecimal paid04) {
		this.paid04 = paid04;
	}

	/**
	 * @return the paid05
	 */
	public BigDecimal getPaid05() {
		return paid05;
	}

	/**
	 * @param paid05
	 *            the paid05 to set
	 */
	public void setPaid05(BigDecimal paid05) {
		this.paid05 = paid05;
	}

	/**
	 * @return the paid06
	 */
	public BigDecimal getPaid06() {
		return paid06;
	}

	/**
	 * @param paid06
	 *            the paid06 to set
	 */
	public void setPaid06(BigDecimal paid06) {
		this.paid06 = paid06;
	}

	/**
	 * @return the paid07
	 */
	public BigDecimal getPaid07() {
		return paid07;
	}

	/**
	 * @param paid07
	 *            the paid07 to set
	 */
	public void setPaid07(BigDecimal paid07) {
		this.paid07 = paid07;
	}

	/**
	 * @return the paid08
	 */
	public BigDecimal getPaid08() {
		return paid08;
	}

	/**
	 * @param paid08
	 *            the paid08 to set
	 */
	public void setPaid08(BigDecimal paid08) {
		this.paid08 = paid08;
	}

	/**
	 * @return the paymmeth01
	 */
	public String getPaymmeth01() {
		return paymmeth01;
	}

	/**
	 * @param paymmeth01
	 *            the paymmeth01 to set
	 */
	public void setPaymmeth01(String paymmeth01) {
		this.paymmeth01 = paymmeth01;
	}

	/**
	 * @return the paymmeth02
	 */
	public String getPaymmeth02() {
		return paymmeth02;
	}

	/**
	 * @param paymmeth02
	 *            the paymmeth02 to set
	 */
	public void setPaymmeth02(String paymmeth02) {
		this.paymmeth02 = paymmeth02;
	}

	/**
	 * @return the paymmeth03
	 */
	public String getPaymmeth03() {
		return paymmeth03;
	}

	/**
	 * @param paymmeth03
	 *            the paymmeth03 to set
	 */
	public void setPaymmeth03(String paymmeth03) {
		this.paymmeth03 = paymmeth03;
	}

	/**
	 * @return the paymmeth04
	 */
	public String getPaymmeth04() {
		return paymmeth04;
	}

	/**
	 * @param paymmeth04
	 *            the paymmeth04 to set
	 */
	public void setPaymmeth04(String paymmeth04) {
		this.paymmeth04 = paymmeth04;
	}

	/**
	 * @return the paymmeth05
	 */
	public String getPaymmeth05() {
		return paymmeth05;
	}

	/**
	 * @param paymmeth05
	 *            the paymmeth05 to set
	 */
	public void setPaymmeth05(String paymmeth05) {
		this.paymmeth05 = paymmeth05;
	}

	/**
	 * @return the paymmeth06
	 */
	public String getPaymmeth06() {
		return paymmeth06;
	}

	/**
	 * @param paymmeth06
	 *            the paymmeth06 to set
	 */
	public void setPaymmeth06(String paymmeth06) {
		this.paymmeth06 = paymmeth06;
	}

	/**
	 * @return the paymmeth07
	 */
	public String getPaymmeth07() {
		return paymmeth07;
	}

	/**
	 * @param paymmeth07
	 *            the paymmeth07 to set
	 */
	public void setPaymmeth07(String paymmeth07) {
		this.paymmeth07 = paymmeth07;
	}

	/**
	 * @return the paymmeth08
	 */
	public String getPaymmeth08() {
		return paymmeth08;
	}

	/**
	 * @param paymmeth08
	 *            the paymmeth08 to set
	 */
	public void setPaymmeth08(String paymmeth08) {
		this.paymmeth08 = paymmeth08;
	}

	/**
	 * @return the zrpayopt01
	 */
	public String getZrpayopt01() {
		return zrpayopt01;
	}

	/**
	 * @param zrpayopt01
	 *            the zrpayopt01 to set
	 */
	public void setZrpayopt01(String zrpayopt01) {
		this.zrpayopt01 = zrpayopt01;
	}

	/**
	 * @return the zrpayopt02
	 */
	public String getZrpayopt02() {
		return zrpayopt02;
	}

	/**
	 * @param zrpayopt02
	 *            the zrpayopt02 to set
	 */
	public void setZrpayopt02(String zrpayopt02) {
		this.zrpayopt02 = zrpayopt02;
	}

	/**
	 * @return the zrpayopt03
	 */
	public String getZrpayopt03() {
		return zrpayopt03;
	}

	/**
	 * @param zrpayopt03
	 *            the zrpayopt03 to set
	 */
	public void setZrpayopt03(String zrpayopt03) {
		this.zrpayopt03 = zrpayopt03;
	}

	/**
	 * @return the zrpayopt04
	 */
	public String getZrpayopt04() {
		return zrpayopt04;
	}

	/**
	 * @param zrpayopt04
	 *            the zrpayopt04 to set
	 */
	public void setZrpayopt04(String zrpayopt04) {
		this.zrpayopt04 = zrpayopt04;
	}

	/**
	 * @return the zrpayopt05
	 */
	public String getZrpayopt05() {
		return zrpayopt05;
	}

	/**
	 * @param zrpayopt05
	 *            the zrpayopt05 to set
	 */
	public void setZrpayopt05(String zrpayopt05) {
		this.zrpayopt05 = zrpayopt05;
	}

	/**
	 * @return the zrpayopt06
	 */
	public String getZrpayopt06() {
		return zrpayopt06;
	}

	/**
	 * @param zrpayopt06
	 *            the zrpayopt06 to set
	 */
	public void setZrpayopt06(String zrpayopt06) {
		this.zrpayopt06 = zrpayopt06;
	}

	/**
	 * @return the zrpayopt07
	 */
	public String getZrpayopt07() {
		return zrpayopt07;
	}

	/**
	 * @param zrpayopt07
	 *            the zrpayopt07 to set
	 */
	public void setZrpayopt07(String zrpayopt07) {
		this.zrpayopt07 = zrpayopt07;
	}

	/**
	 * @return the zrpayopt08
	 */
	public String getZrpayopt08() {
		return zrpayopt08;
	}

	/**
	 * @param zrpayopt08
	 *            the zrpayopt08 to set
	 */
	public void setZrpayopt08(String zrpayopt08) {
		this.zrpayopt08 = zrpayopt08;
	}

	/**
	 * @return the totamnt
	 */
	public BigDecimal getTotamnt() {
		return totamnt;
	}

	/**
	 * @param totamnt
	 *            the totamnt to set
	 */
	public void setTotamnt(BigDecimal totamnt) {
		this.totamnt = totamnt;
	}

	/**
	 * @return the npaydate
	 */
	public Integer getNpaydate() {
		return npaydate;
	}

	/**
	 * @param npaydate
	 *            the npaydate to set
	 */
	public void setNpaydate(Integer npaydate) {
		this.npaydate = npaydate;
	}

	/**
	 * @return the payclt
	 */
	public String getPayclt() {
		return payclt;
	}

	/**
	 * @param payclt
	 *            the payclt to set
	 */
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}

	/**
	 * @return the bankkey
	 */
	public String getBankkey() {
		return bankkey;
	}

	/**
	 * @param bankkey
	 *            the bankkey to set
	 */
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}

	/**
	 * @return the bankacckey
	 */
	public String getBankacckey() {
		return bankacckey;
	}

	/**
	 * @param bankacckey
	 *            the bankacckey to set
	 */
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}

	/**
	 * @return the paycurr
	 */
	public String getPaycurr() {
		return paycurr;
	}

	/**
	 * @param paycurr
	 *            the paycurr to set
	 */
	public void setPaycurr(String paycurr) {
		this.paycurr = paycurr;
	}

	/**
	 * @return the usrprf
	 */
	public String getUsrprf() {
		return usrprf;
	}

	/**
	 * @param usrprf
	 *            the usrprf to set
	 */
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	/**
	 * @return the jobnm
	 */
	public String getJobnm() {
		return jobnm;
	}

	/**
	 * @param jobnm
	 *            the jobnm to set
	 */
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	/**
	 * @return the datime
	 */
	public Timestamp getDatime() {
		return datime == null ? null : (Timestamp) datime.clone();
	}

	/**
	 * @param datime
	 *            the datime to set
	 */
	public void setDatime(Timestamp datime) {
		this.datime = datime == null ? null : (Timestamp) datime.clone();
	}

	/**
	 * @return the paymentDue
	 */
	public BigDecimal getPaymentDue() {
		return paymentDue;
	}

	/**
	 * @param paymentDue
	 *            the paymentDue to set
	 */
	public void setPaymentDue(BigDecimal paymentDue) {
		this.paymentDue = paymentDue;
	}

	/**
	 * @return the policyTranno
	 */
	public Integer getPolicyTranno() {
		return policyTranno;
	}

	/**
	 * @param policyTranno
	 *            the policyTranno to set
	 */
	public void setPolicyTranno(Integer policyTranno) {
		this.policyTranno = policyTranno;
	}

	/**
	 * @return the policyCurr
	 */
	public String getPolicyCurr() {
		return policyCurr;
	}

	/**
	 * @param policyCurr
	 *            the policyCurr to set
	 */
	public void setPolicyCurr(String policyCurr) {
		this.policyCurr = policyCurr;
	}

	/**
	 * @return the policyType
	 */
	public String getPolicyType() {
		return policyType;
	}

	/**
	 * @param policyType
	 *            the policyType to set
	 */
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	/**
	 * @return the policyBillCurr
	 */
	public String getPolicyBillCurr() {
		return policyBillCurr;
	}

	/**
	 * @param policyBillCurr
	 *            the policyBillCurr to set
	 */
	public void setPolicyBillCurr(String policyBillCurr) {
		this.policyBillCurr = policyBillCurr;
	}

	/**
	 * @return the policyOccdate
	 */
	public Integer getPolicyOccdate() {
		return policyOccdate;
	}

	/**
	 * @param policyOccdate
	 *            the policyOccdate to set
	 */
	public void setPolicyOccdate(Integer policyOccdate) {
		this.policyOccdate = policyOccdate;
	}

	/**
	 * @return the sumins
	 */
	public BigDecimal getSumins() {
		return sumins;
	}

	/**
	 * @param sumins
	 *            the sumins to set
	 */
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	/**
	 * @return the statcode
	 */
	public String getStatcode() {
		return statcode;
	}

	/**
	 * @param statcode
	 *            the statcode to set
	 */
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	/**
	 * @return the pstatcode
	 */
	public String getPstatcode() {
		return pstatcode;
	}

	/**
	 * @param pstatcode
	 *            the pstatcode to set
	 */
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	/**
	 * @return the plansuffix
	 */
	public int getPlansuffix() {
		return plansuffix;
	}

	/**
	 * @param plansuffix
	 *            the plansuffix to set
	 */
	public void setPlansuffix(int plansuffix) {
		this.plansuffix = plansuffix;
	}

	/**
	 * @return the oldCurrto
	 */
	public Integer getOldCurrto() {
		return oldCurrto;
	}

	/**
	 * @param oldCurrto
	 *            the oldCurrto to set
	 */
	public void setOldCurrto(Integer oldCurrto) {
		this.oldCurrto = oldCurrto;
	}

	/**
	 * @return the newNpaydate
	 */
	public Integer getNewNpaydate() {
		return newNpaydate;
	}

	/**
	 * @param newNpaydate
	 *            the newNpaydate to set
	 */
	public void setNewNpaydate(Integer newNpaydate) {
		this.newNpaydate = newNpaydate;
	}

	/**
	 * @return the newTotalamnt
	 */
	public BigDecimal getNewTotalamnt() {
		return newTotalamnt;
	}

	/**
	 * @param newTotalamnt
	 *            the newTotalamnt to set
	 */
	public void setNewTotalamnt(BigDecimal newTotalamnt) {
		this.newTotalamnt = newTotalamnt;
	}

	/**
	 * @return the newCurrto
	 */
	public Integer getNewCurrto() {
		return newCurrto;
	}

	/**
	 * @param newCurrto
	 *            the newCurrto to set
	 */
	public void setNewCurrto(Integer newCurrto) {
		this.newCurrto = newCurrto;
	}

	/**
	 * @return the newCurrfrom
	 */
	public Integer getNewCurrfrom() {
		return newCurrfrom;
	}

	/**
	 * @param newCurrfrom
	 *            the newCurrfrom to set
	 */
	public void setNewCurrfrom(Integer newCurrfrom) {
		this.newCurrfrom = newCurrfrom;
	}

	/**
	 * @return the newPaymentOpt
	 */
	public String getNewPaymentOpt() {
		return newPaymentOpt;
	}

	/**
	 * @param newPaymentOpt
	 *            the newPaymentOpt to set
	 */
	public void setNewPaymentOpt(String newPaymentOpt) {
		this.newPaymentOpt = newPaymentOpt;
	}

	/**
	 * @return the paymmeth
	 */
	public String getPaymmeth() {
		return paymmeth;
	}

	@Nullable
	public String getPaymmeth(int indx) {

		String paymmethLocal = null;

		switch (indx) {
		case 1:
			paymmethLocal = paymmeth01;
			break;
		case 2:
			paymmethLocal = paymmeth02;
			break;
		case 3:
			paymmethLocal = paymmeth03;
			break;
		case 4:
			paymmethLocal = paymmeth04;
			break;
		case 5:
			paymmethLocal = paymmeth05;
			break;
		case 6:
			paymmethLocal = paymmeth06;
			break;
		case 7:
			paymmethLocal = paymmeth07;
			break;
		case 8:
			paymmethLocal = paymmeth08;
			break;
		default:
			break;
		}

		return paymmethLocal;

	}
	
	/**
	 * @param paymmeth
	 *            the paymmeth to set
	 */
	public void setPaymmeth(String paymmeth) {
		this.paymmeth = paymmeth;
	}

	public void setPaymmeth(int indx, String payMtd) {

		switch (indx) {
		case 1:
			setPaymmeth01(payMtd);
			break;
		case 2:
			setPaymmeth02(payMtd);
			break;
		case 3:
			setPaymmeth03(payMtd);
			break;
		case 4:
			setPaymmeth04(payMtd);
			break;
		case 5:
			setPaymmeth05(payMtd);
			break;
		case 6:
			setPaymmeth06(payMtd);
			break;
		case 7:
			setPaymmeth07(payMtd);
			break;
		case 8:
			setPaymmeth08(payMtd);
			break;
		default:
			return; // Throw error instead?
		}

	}
	
	/**
	 * @return the batcbrn
	 */
	public String getBatcbrn() {
		return batcbrn;
	}

	/**
	 * @param batcbrn
	 *            the batcbrn to set
	 */
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	/**
	 * @return the termid
	 */
	public String getTermid() {
		return termid;
	}

	/**
	 * @param termid
	 *            the termid to set
	 */
	public void setTermid(String termid) {
		this.termid = termid;
	}

	/**
	 * @return the batcactyr
	 */
	public Integer getBatcactyr() {
		return batcactyr;
	}

	/**
	 * @param batcactyr
	 *            the batcactyr to set
	 */
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}

	/**
	 * @return the batcactmn
	 */
	public Integer getBatcactmn() {
		return batcactmn;
	}

	/**
	 * @param batcactmn
	 *            the batcactmn to set
	 */
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}

	/**
	 * @return the batctrcde
	 */
	public String getBatctrcde() {
		return batctrcde;
	}

	/**
	 * @param batctrcde
	 *            the batctrcde to set
	 */
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	/**
	 * @return the batcbatch
	 */
	public String getBatcbatch() {
		return batcbatch;
	}

	/**
	 * @param batcbatch
	 *            the batcbatch to set
	 */
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	/**
	 * @return the batcpfx
	 */
	public String getBatcpfx() {
		return batcpfx;
	}

	/**
	 * @param batcpfx
	 *            the batcpfx to set
	 */
	public void setBatcpfx(String batcpfx) {
		this.batcpfx = batcpfx;
	}

	/**
	 * @return the batccoy
	 */
	public String getBatccoy() {
		return batccoy;
	}

	/**
	 * @param batccoy
	 *            the batccoy to set
	 */
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	/**
	 * @return the effectiveDate
	 */
	public Integer getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(Integer effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the trdt
	 */
	public Integer getTrdt() {
		return trdt;
	}

	/**
	 * @param trdt
	 *            the trdt to set
	 */
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}

	/**
	 * @return the trtm
	 */
	public Integer getTrtm() {
		return trtm;
	}

	/**
	 * @param trtm
	 *            the trtm to set
	 */
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	/**
	 * @return the userNum
	 */
	public Integer getUserNum() {
		return userNum;
	}

	/**
	 * @param userNum
	 *            the userNum to set
	 */
	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}

	/**
	 * @return the tranCount
	 */
	public int getTranCount() {
		return tranCount;
	}

	/**
	 * @param tranCount
	 *            the tranCount to set
	 */
	public void setTranCount(int tranCount) {
		this.tranCount = tranCount;
	}

	@Nullable
	public Integer getZrduedte(int indx) {

		Integer zrduedte = null;

		switch (indx) {
		case 1:
			zrduedte = zrduedte01;
			break;
		case 2:
			zrduedte = zrduedte02;
			break;
		case 3:
			zrduedte = zrduedte03;
			break;
		case 4:
			zrduedte = zrduedte04;
			break;
		case 5:
			zrduedte = zrduedte05;
			break;
		case 6:
			zrduedte = zrduedte06;
			break;
		case 7:
			zrduedte = zrduedte07;
			break;
		case 8:
			zrduedte = zrduedte08;
			break;
		default:
			break;
		}
		return zrduedte;
	}

	public void setZrduedte(int indx, Integer date) {

		switch (indx) {
		case 1:
			setZrduedte01(date);
			break;
		case 2:
			setZrduedte02(date);
			break;
		case 3:
			setZrduedte03(date);
			break;
		case 4:
			setZrduedte04(date);
			break;
		case 5:
			setZrduedte05(date);
			break;
		case 6:
			setZrduedte06(date);
			break;
		case 7:
			setZrduedte07(date);
			break;
		case 8:
			setZrduedte08(date);
			break;
		default:
			return; // Throw error instead?
		}

	}

	@Nullable
	public String getZrpayopt(int indx) {

		String zrpayopt = null;

		switch (indx) {
		case 1:
			zrpayopt = zrpayopt01;
			break;
		case 2:
			zrpayopt = zrpayopt02;
			break;
		case 3:
			zrpayopt = zrpayopt03;
			break;
		case 4:
			zrpayopt = zrpayopt04;
			break;
		case 5:
			zrpayopt = zrpayopt05;
			break;
		case 6:
			zrpayopt = zrpayopt06;
			break;
		case 7:
			zrpayopt = zrpayopt07;
			break;
		case 8:
			zrpayopt = zrpayopt08;
			break;
		default:
			break;
		}

		return zrpayopt;

	}

	public void setZrpayopt(int indx, String payOptn) {

		switch (indx) {
		case 1:
			setZrpayopt01(payOptn);
			break;
		case 2:
			setZrpayopt02(payOptn);
			break;
		case 3:
			setZrpayopt03(payOptn);
			break;
		case 4:
			setZrpayopt04(payOptn);
			break;
		case 5:
			setZrpayopt05(payOptn);
			break;
		case 6:
			setZrpayopt06(payOptn);
			break;
		case 7:
			setZrpayopt07(payOptn);
			break;
		case 8:
			setZrpayopt08(payOptn);
			break;
		default:
			return; // Throw error instead?
		}

	}

	

	@Nullable
	public BigDecimal getPrcnt(int indx) {

		BigDecimal prcnt = null;

		switch (indx) {
		case 1:
			prcnt = prcnt01;
			break;
		case 2:
			prcnt = prcnt02;
			break;
		case 3:
			prcnt = prcnt03;
			break;
		case 4:
			prcnt = prcnt04;
			break;
		case 5:
			prcnt = prcnt05;
			break;
		case 6:
			prcnt = prcnt06;
			break;
		case 7:
			prcnt = prcnt07;
			break;
		case 8:
			prcnt = prcnt08;
			break;
		default:
			break;
		}

		return prcnt;

	}

	public void setPaid(int indx, BigDecimal amount) {

		switch (indx) {
		case 1:
			setPaid01(amount);
			break;
		case 2:
			setPaid02(amount);
			break;
		case 3:
			setPaid03(amount);
			break;
		case 4:
			setPaid04(amount);
			break;
		case 5:
			setPaid05(amount);
			break;
		case 6:
			setPaid06(amount);
			break;
		case 7:
			setPaid07(amount);
			break;
		case 8:
			setPaid08(amount);
			break;
		default:
			return; // Throw error instead?
		}

	}

	public void setPaydte(int indx, Integer date) {

		switch (indx) {
		case 1:
			setPaydte01(date);
			break;
		case 2:
			setPaydte02(date);
			break;
		case 3:
			setPaydte03(date);
			break;
		case 4:
			setPaydte04(date);
			break;
		case 5:
			setPaydte05(date);
			break;
		case 6:
			setPaydte06(date);
			break;
		case 7:
			setPaydte07(date);
			break;
		case 8:
			setPaydte08(date);
			break;
		default:
			return; // Throw error instead?
		}

	}

	/* IJTI-379 Start */
	/**
	 * 
	 * @return cownCoy the cownCoy to set
	 */
	public String getCownCoy() {
		return cownCoy;
	}

	/**
	 * 
	 * @param the
	 *            cownCoy
	 */
	public void setCownCoy(String cownCoy) {
		this.cownCoy = cownCoy;
	}
	/* IJTI-379 End */

	@Override
	public String toString() {
		return "L2anendrlxReaderDTO [uniqueNumber=" + uniqueNumber + ", chdrcoy=" + chdrcoy + ", life=" + life
				+ ", chdrnum=" + chdrnum + ", coverage=" + coverage + ", rider=" + rider + ", plnsfx=" + plnsfx
				+ ", validflag=" + validflag + ", tranno=" + tranno + ", currfrom=" + currfrom + ", currto=" + currto
				+ ", zrduedte01=" + zrduedte01 + ", zrduedte02=" + zrduedte02 + ", zrduedte03=" + zrduedte03
				+ ", zrduedte04=" + zrduedte04 + ", zrduedte05=" + zrduedte05 + ", zrduedte06=" + zrduedte06
				+ ", zrduedte07=" + zrduedte07 + ", zrduedte08=" + zrduedte08 + ", prcnt01=" + prcnt01 + ", prcnt02="
				+ prcnt02 + ", prcnt03=" + prcnt03 + ", prcnt04=" + prcnt04 + ", prcnt05=" + prcnt05 + ", prcnt06="
				+ prcnt06 + ", prcnt07=" + prcnt07 + ", prcnt08=" + prcnt08 + ", paydte01=" + paydte01 + ", paydte02="
				+ paydte02 + ", paydte03=" + paydte03 + ", paydte04=" + paydte04 + ", paydte05=" + paydte05
				+ ", paydte06=" + paydte06 + ", paydte07=" + paydte07 + ", paydte08=" + paydte08 + ", paid01=" + paid01
				+ ", paid02=" + paid02 + ", paid03=" + paid03 + ", paid04=" + paid04 + ", paid05=" + paid05
				+ ", paid06=" + paid06 + ", paid07=" + paid07 + ", paid08=" + paid08 + ", paymmeth01=" + paymmeth01
				+ ", paymmeth02=" + paymmeth02 + ", paymmeth03=" + paymmeth03 + ", paymmeth04=" + paymmeth04
				+ ", paymmeth05=" + paymmeth05 + ", paymmeth06=" + paymmeth06 + ", paymmeth07=" + paymmeth07
				+ ", paymmeth08=" + paymmeth08 + ", zrpayopt01=" + zrpayopt01 + ", zrpayopt02=" + zrpayopt02
				+ ", zrpayopt03=" + zrpayopt03 + ", zrpayopt04=" + zrpayopt04 + ", zrpayopt05=" + zrpayopt05
				+ ", zrpayopt06=" + zrpayopt06 + ", zrpayopt07=" + zrpayopt07 + ", zrpayopt08=" + zrpayopt08
				+ ", totamnt=" + totamnt + ", npaydate=" + npaydate + ", payclt=" + payclt + ", bankkey=" + bankkey
				+ ", bankacckey=" + bankacckey + ", paycurr=" + paycurr + ", usrprf=" + usrprf + ", jobnm=" + jobnm
				+ ", datime=" + datime + ", paymentDue=" + paymentDue + ", policyTranno=" + policyTranno
				+ ", policyCurr=" + policyCurr + ", policyType=" + policyType + ", policyBillCurr=" + policyBillCurr
				+ ", policyOccdate=" + policyOccdate + ", sumins=" + sumins + ", statcode=" + statcode + ", pstatcode="
				+ pstatcode + ", plansuffix=" + plansuffix + ", oldCurrto=" + oldCurrto + ", newNpaydate=" + newNpaydate
				+ ", newTotalamnt=" + newTotalamnt + ", newCurrto=" + newCurrto + ", newCurrfrom=" + newCurrfrom
				+ ", newPaymentOpt=" + newPaymentOpt + ", paymmeth=" + paymmeth + ", batcbrn=" + batcbrn + ", termid="
				+ termid + ", batcactyr=" + batcactyr + ", batcactmn=" + batcactmn + ", batctrcde=" + batctrcde
				+ ", batcbatch=" + batcbatch + ", batcpfx=" + batcpfx + ", batccoy=" + batccoy + ", effectiveDate="
				+ effectiveDate + ", trdt=" + trdt + ", trtm=" + trtm + ", userNum=" + userNum + ", tranCount="
				+ tranCount + ", cownCoy=" + cownCoy + "]";
	}

	public L2anendrlxReaderDTO() {
		super();
	}

	public L2anendrlxReaderDTO mapResultSetToDTO(ResultSet rs) throws SQLException {
		L2anendrlxReaderDTO l2anendrlxReaderDTOOutput = new L2anendrlxReaderDTO();
		l2anendrlxReaderDTOOutput.setPolicyTranno(rs.getInt("policyTranno"));
		l2anendrlxReaderDTOOutput.setPolicyCurr(rs.getString("policyCurr"));
		l2anendrlxReaderDTOOutput.setPolicyType(rs.getString("policyType"));
		l2anendrlxReaderDTOOutput.setPolicyBillCurr(rs.getString("policyBillCurr"));
		l2anendrlxReaderDTOOutput.setPolicyOccdate(rs.getInt("policyOccdate"));
		l2anendrlxReaderDTOOutput.setStatcode(rs.getString("statcode"));
		l2anendrlxReaderDTOOutput.setPstatcode(rs.getString("pstatcode"));
		l2anendrlxReaderDTOOutput.setSumins(rs.getBigDecimal("sumins"));
		l2anendrlxReaderDTOOutput.setUniqueNumber(rs.getInt("unique_number"));
		l2anendrlxReaderDTOOutput.setChdrcoy(rs.getString("chdrcoy"));
		l2anendrlxReaderDTOOutput.setLife(rs.getString("life"));
		l2anendrlxReaderDTOOutput.setChdrnum(rs.getString("chdrnum"));
		l2anendrlxReaderDTOOutput.setCoverage(rs.getString("coverage"));
		l2anendrlxReaderDTOOutput.setNpaydate(rs.getInt("npaydate"));
		l2anendrlxReaderDTOOutput.setPlnsfx(rs.getInt("plnsfx"));
		l2anendrlxReaderDTOOutput.setRider(rs.getString("rider"));
		l2anendrlxReaderDTOOutput.setValidflag(rs.getString("validflag"));
		l2anendrlxReaderDTOOutput.setTranno(rs.getInt("tranno"));
		l2anendrlxReaderDTOOutput.setCurrfrom(rs.getInt("currfrom"));
		l2anendrlxReaderDTOOutput.setCurrto(rs.getInt("currto"));
		l2anendrlxReaderDTOOutput.setZrduedte01(rs.getInt("zrduedte01"));
		l2anendrlxReaderDTOOutput.setZrduedte02(rs.getInt("zrduedte02"));
		l2anendrlxReaderDTOOutput.setZrduedte03(rs.getInt("zrduedte03"));
		l2anendrlxReaderDTOOutput.setZrduedte04(rs.getInt("zrduedte04"));
		l2anendrlxReaderDTOOutput.setZrduedte05(rs.getInt("zrduedte05"));
		l2anendrlxReaderDTOOutput.setZrduedte06(rs.getInt("zrduedte06"));
		l2anendrlxReaderDTOOutput.setZrduedte07(rs.getInt("zrduedte07"));
		l2anendrlxReaderDTOOutput.setZrduedte08(rs.getInt("zrduedte08"));
		l2anendrlxReaderDTOOutput.setPrcnt01(rs.getBigDecimal("prcnt01"));
		l2anendrlxReaderDTOOutput.setPrcnt02(rs.getBigDecimal("prcnt02"));
		l2anendrlxReaderDTOOutput.setPrcnt03(rs.getBigDecimal("prcnt03"));
		l2anendrlxReaderDTOOutput.setPrcnt04(rs.getBigDecimal("prcnt04"));
		l2anendrlxReaderDTOOutput.setPrcnt05(rs.getBigDecimal("prcnt05"));
		l2anendrlxReaderDTOOutput.setPrcnt06(rs.getBigDecimal("prcnt06"));
		l2anendrlxReaderDTOOutput.setPrcnt07(rs.getBigDecimal("prcnt07"));
		l2anendrlxReaderDTOOutput.setPrcnt08(rs.getBigDecimal("prcnt08"));
		l2anendrlxReaderDTOOutput.setPaydte01(rs.getInt("paydte01"));
		l2anendrlxReaderDTOOutput.setPaydte02(rs.getInt("paydte02"));
		l2anendrlxReaderDTOOutput.setPaydte03(rs.getInt("paydte03"));
		l2anendrlxReaderDTOOutput.setPaydte04(rs.getInt("paydte04"));
		l2anendrlxReaderDTOOutput.setPaydte05(rs.getInt("paydte05"));
		l2anendrlxReaderDTOOutput.setPaydte06(rs.getInt("paydte06"));
		l2anendrlxReaderDTOOutput.setPaydte07(rs.getInt("paydte07"));
		l2anendrlxReaderDTOOutput.setPaydte08(rs.getInt("paydte08"));
		mapResultSetToDTO2(l2anendrlxReaderDTOOutput, rs);
		return l2anendrlxReaderDTOOutput;
	}

	private void mapResultSetToDTO2(L2anendrlxReaderDTO l2anendrlxReaderDTOOutput, ResultSet rs) throws SQLException {

		l2anendrlxReaderDTOOutput.setPaid01(rs.getBigDecimal("paid01"));
		l2anendrlxReaderDTOOutput.setPaid02(rs.getBigDecimal("paid02"));
		l2anendrlxReaderDTOOutput.setPaid03(rs.getBigDecimal("paid03"));
		l2anendrlxReaderDTOOutput.setPaid04(rs.getBigDecimal("paid04"));
		l2anendrlxReaderDTOOutput.setPaid05(rs.getBigDecimal("paid05"));
		l2anendrlxReaderDTOOutput.setPaid06(rs.getBigDecimal("paid06"));
		l2anendrlxReaderDTOOutput.setPaid07(rs.getBigDecimal("paid07"));
		l2anendrlxReaderDTOOutput.setPaid08(rs.getBigDecimal("paid08"));
		l2anendrlxReaderDTOOutput.setPaymmeth01(rs.getString("paymmeth01"));
		l2anendrlxReaderDTOOutput.setPaymmeth02(rs.getString("paymmeth02"));
		l2anendrlxReaderDTOOutput.setPaymmeth03(rs.getString("paymmeth03"));
		l2anendrlxReaderDTOOutput.setPaymmeth04(rs.getString("paymmeth04"));
		l2anendrlxReaderDTOOutput.setPaymmeth05(rs.getString("paymmeth05"));
		l2anendrlxReaderDTOOutput.setPaymmeth06(rs.getString("paymmeth06"));
		l2anendrlxReaderDTOOutput.setPaymmeth07(rs.getString("paymmeth07"));
		l2anendrlxReaderDTOOutput.setPaymmeth08(rs.getString("paymmeth08"));
		l2anendrlxReaderDTOOutput.setZrpayopt01(rs.getString("zrpayopt01"));
		l2anendrlxReaderDTOOutput.setZrpayopt02(rs.getString("zrpayopt02"));
		l2anendrlxReaderDTOOutput.setZrpayopt03(rs.getString("zrpayopt03"));
		l2anendrlxReaderDTOOutput.setZrpayopt04(rs.getString("zrpayopt04"));
		l2anendrlxReaderDTOOutput.setZrpayopt05(rs.getString("zrpayopt05"));
		l2anendrlxReaderDTOOutput.setZrpayopt06(rs.getString("zrpayopt06"));
		l2anendrlxReaderDTOOutput.setZrpayopt07(rs.getString("zrpayopt07"));
		l2anendrlxReaderDTOOutput.setZrpayopt08(rs.getString("zrpayopt08"));
		l2anendrlxReaderDTOOutput.setTotamnt(rs.getBigDecimal("totamnt"));
		l2anendrlxReaderDTOOutput.setNpaydate(rs.getInt("npaydate"));
		l2anendrlxReaderDTOOutput.setPayclt(rs.getString("payclt"));
		l2anendrlxReaderDTOOutput.setBankkey(rs.getString("bankkey"));
		l2anendrlxReaderDTOOutput.setBankacckey(rs.getString("bankacckey"));
		l2anendrlxReaderDTOOutput.setPaycurr(rs.getString("paycurr"));
		l2anendrlxReaderDTOOutput.setUsrprf(rs.getString("usrprf"));
		l2anendrlxReaderDTOOutput.setJobnm(rs.getString("jobnm"));
		l2anendrlxReaderDTOOutput.setDatime(rs.getTimestamp("datime"));
		l2anendrlxReaderDTOOutput.setPlansuffix(rs.getInt("plansuffix"));
		l2anendrlxReaderDTOOutput.setCownCoy(rs.getString("cownCoy"));
	}

	public L2anendrlxReaderDTO cloneReaderDTO(L2anendrlxReaderDTO input) {
		L2anendrlxReaderDTO l2anendrlxReaderDTOOutput = new L2anendrlxReaderDTO();
		l2anendrlxReaderDTOOutput.setBankacckey(input.bankacckey);
		l2anendrlxReaderDTOOutput.setBankkey(input.bankkey);
		l2anendrlxReaderDTOOutput.setBatcactmn(input.batcactmn);
		l2anendrlxReaderDTOOutput.setBatcactyr(input.batcactyr);
		l2anendrlxReaderDTOOutput.setBatcbatch(input.batcbatch);
		l2anendrlxReaderDTOOutput.setBatcbrn(input.batcbrn);
		l2anendrlxReaderDTOOutput.setBatccoy(input.batccoy);
		l2anendrlxReaderDTOOutput.setBatcpfx(input.batcpfx);
		l2anendrlxReaderDTOOutput.setBatctrcde(input.batctrcde);
		l2anendrlxReaderDTOOutput.setChdrcoy(input.chdrcoy);
		l2anendrlxReaderDTOOutput.setChdrnum(input.chdrnum);
		l2anendrlxReaderDTOOutput.setCoverage(input.coverage);
		l2anendrlxReaderDTOOutput.setCownCoy(input.cownCoy);
		l2anendrlxReaderDTOOutput.setCurrfrom(input.currfrom);
		l2anendrlxReaderDTOOutput.setCurrto(input.currto);
		l2anendrlxReaderDTOOutput.setDatime(input.datime);
		l2anendrlxReaderDTOOutput.setEffectiveDate(input.effectiveDate);
		l2anendrlxReaderDTOOutput.setJobnm(input.jobnm);
		l2anendrlxReaderDTOOutput.setLife(input.life);
		l2anendrlxReaderDTOOutput.setNewCurrfrom(input.newCurrfrom);
		l2anendrlxReaderDTOOutput.setNewCurrto(input.newCurrto);
		l2anendrlxReaderDTOOutput.setNewNpaydate(input.newNpaydate);
		l2anendrlxReaderDTOOutput.setNewPaymentOpt(input.newPaymentOpt);
		l2anendrlxReaderDTOOutput.setNewTotalamnt(input.newTotalamnt);
		l2anendrlxReaderDTOOutput.setNpaydate(input.npaydate);
		l2anendrlxReaderDTOOutput.setOldCurrto(input.oldCurrto);
		l2anendrlxReaderDTOOutput.setPaid01(input.paid01);
		l2anendrlxReaderDTOOutput.setPaid02(input.paid02);
		l2anendrlxReaderDTOOutput.setPaid03(input.paid03);
		l2anendrlxReaderDTOOutput.setPaid04(input.paid04);
		l2anendrlxReaderDTOOutput.setPaid05(input.paid05);
		l2anendrlxReaderDTOOutput.setPaid06(input.paid06);
		l2anendrlxReaderDTOOutput.setPaid07(input.paid07);
		l2anendrlxReaderDTOOutput.setPaid08(input.paid08);
		l2anendrlxReaderDTOOutput.setPayclt(input.payclt);
		l2anendrlxReaderDTOOutput.setPaycurr(input.paycurr);
		l2anendrlxReaderDTOOutput.setPaydte01(input.paydte01);
		l2anendrlxReaderDTOOutput.setPaydte02(input.paydte02);
		l2anendrlxReaderDTOOutput.setPaydte03(input.paydte03);
		l2anendrlxReaderDTOOutput.setPaydte04(input.paydte04);
		l2anendrlxReaderDTOOutput.setPaydte05(input.paydte05);
		l2anendrlxReaderDTOOutput.setPaydte06(input.paydte06);
		l2anendrlxReaderDTOOutput.setPaydte07(input.paydte07);
		l2anendrlxReaderDTOOutput.setPaydte08(input.paydte08);
		cloneReaderDTO2(input, l2anendrlxReaderDTOOutput);
		cloneReaderDTO3(input, l2anendrlxReaderDTOOutput);
		return l2anendrlxReaderDTOOutput;
	}

	private void cloneReaderDTO2(L2anendrlxReaderDTO input, L2anendrlxReaderDTO l2anendrlxReaderDTOOutput) {
		l2anendrlxReaderDTOOutput.setPaymentDue(input.paymentDue);
		l2anendrlxReaderDTOOutput.setPaymmeth(input.paymmeth);
		l2anendrlxReaderDTOOutput.setPaymmeth01(input.paymmeth01);
		l2anendrlxReaderDTOOutput.setPaymmeth02(input.paymmeth02);
		l2anendrlxReaderDTOOutput.setPaymmeth03(input.paymmeth04);
		l2anendrlxReaderDTOOutput.setPaymmeth04(input.paymmeth04);
		l2anendrlxReaderDTOOutput.setPaymmeth05(input.paymmeth05);
		l2anendrlxReaderDTOOutput.setPaymmeth06(input.paymmeth06);
		l2anendrlxReaderDTOOutput.setPaymmeth07(input.paymmeth07);
		l2anendrlxReaderDTOOutput.setPaymmeth08(input.paymmeth08);
		l2anendrlxReaderDTOOutput.setPlansuffix(input.plansuffix);
		l2anendrlxReaderDTOOutput.setPlnsfx(input.plnsfx);
		l2anendrlxReaderDTOOutput.setPolicyBillCurr(input.policyBillCurr);
		l2anendrlxReaderDTOOutput.setPolicyCurr(input.policyCurr);
		l2anendrlxReaderDTOOutput.setPolicyOccdate(input.policyOccdate);
		l2anendrlxReaderDTOOutput.setPolicyTranno(input.policyTranno);
		l2anendrlxReaderDTOOutput.setPolicyType(input.policyType);
		l2anendrlxReaderDTOOutput.setPrcnt01(input.prcnt01);
		l2anendrlxReaderDTOOutput.setPrcnt02(input.prcnt02);
		l2anendrlxReaderDTOOutput.setPrcnt03(input.prcnt03);
		l2anendrlxReaderDTOOutput.setPrcnt04(input.prcnt04);
		l2anendrlxReaderDTOOutput.setPrcnt05(input.prcnt05);
		l2anendrlxReaderDTOOutput.setPrcnt06(input.prcnt06);
		l2anendrlxReaderDTOOutput.setPrcnt07(input.prcnt07);
		l2anendrlxReaderDTOOutput.setPrcnt08(input.prcnt08);
		l2anendrlxReaderDTOOutput.setPstatcode(input.pstatcode);
		l2anendrlxReaderDTOOutput.setRider(input.rider);
		l2anendrlxReaderDTOOutput.setStatcode(input.statcode);
		l2anendrlxReaderDTOOutput.setSumins(input.sumins);
		l2anendrlxReaderDTOOutput.setTermid(input.termid);
		l2anendrlxReaderDTOOutput.setTotamnt(input.totamnt);
		l2anendrlxReaderDTOOutput.setTranCount(input.tranCount);
		l2anendrlxReaderDTOOutput.setTranno(input.tranno);
		l2anendrlxReaderDTOOutput.setTrdt(input.trdt);
		l2anendrlxReaderDTOOutput.setTrtm(input.trtm);
		l2anendrlxReaderDTOOutput.setUniqueNumber(input.uniqueNumber);
		l2anendrlxReaderDTOOutput.setUserNum(input.userNum);
		l2anendrlxReaderDTOOutput.setUsrprf(input.usrprf);
		l2anendrlxReaderDTOOutput.setValidflag(input.validflag);
	}

	private void cloneReaderDTO3(L2anendrlxReaderDTO input, L2anendrlxReaderDTO l2anendrlxReaderDTOOutput) {

		l2anendrlxReaderDTOOutput.setZrduedte01(input.zrduedte01);
		l2anendrlxReaderDTOOutput.setZrduedte02(input.zrduedte02);
		l2anendrlxReaderDTOOutput.setZrduedte03(input.zrduedte03);
		l2anendrlxReaderDTOOutput.setZrduedte04(input.zrduedte04);
		l2anendrlxReaderDTOOutput.setZrduedte05(input.zrduedte05);
		l2anendrlxReaderDTOOutput.setZrduedte06(input.zrduedte06);
		l2anendrlxReaderDTOOutput.setZrduedte07(input.zrduedte07);
		l2anendrlxReaderDTOOutput.setZrduedte08(input.zrduedte08);
		l2anendrlxReaderDTOOutput.setZrpayopt01(input.zrpayopt01);
		l2anendrlxReaderDTOOutput.setZrpayopt02(input.zrpayopt02);
		l2anendrlxReaderDTOOutput.setZrpayopt03(input.zrpayopt03);
		l2anendrlxReaderDTOOutput.setZrpayopt04(input.zrpayopt04);
		l2anendrlxReaderDTOOutput.setZrpayopt05(input.zrpayopt05);
		l2anendrlxReaderDTOOutput.setZrpayopt06(input.zrpayopt06);
		l2anendrlxReaderDTOOutput.setZrpayopt07(input.zrpayopt07);
		l2anendrlxReaderDTOOutput.setZrpayopt08(input.zrpayopt08);
	}

}