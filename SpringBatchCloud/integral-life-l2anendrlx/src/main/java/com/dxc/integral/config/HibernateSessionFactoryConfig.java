package com.dxc.integral.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

/**
 * Hibernate Session Factory Configuration.
 * 
 * @author vhukumagrawa
 *
 */
@Configuration
public class HibernateSessionFactoryConfig {

	@Value("${hibernate.dialect}")
	private String hibernateDialect;

	@Value("${hibernate.show_sql}")
	private String hibernateShowSql;

	@Value("${hibernate.generate_statistics}")
	private String hibernateGenerateStatistics;

	@Value("${hibernate.cache.use_query_cache}")
	private String hibernateCacheUseQueryCache;

	@Value("${hibernate.cache.use_second_level_cache}")
	private String hibernateCacheUseSecondLevelCache;

	@Value("${hibernate.flushMode}")
	private String hibernateFlushMode;

	@Value("${javax.persistence.validation.mode}")
	private String javaxPersistenceValidationMode;

	@Value("${hibernate.cache.region.factory_class}")
	private String hibernateCacheRegionFactoryClass;

	@Value("${net.sf.ehcache.configurationResourceName}")
	private String netSfEhcacheConfigurationResourceName;

	@Value("${connection.pool_size}")
	private String connectionPoolSize;

	@Value("${hibernate.default_schema}")
	private String hibernateDefaultSchema;
	
	/**
	 * Constructor
	 */
	public HibernateSessionFactoryConfig() {
		// Calling default constructor
	}

	@Bean
	@Primary
	public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {

		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(dataSource);
		localSessionFactoryBean.setPackagesToScan("com.dxc.integral");
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.dialect", hibernateDialect);
		hibernateProperties.setProperty("hibernate.show_sql", hibernateShowSql);
		hibernateProperties.setProperty("hibernate.generate_statistics", hibernateGenerateStatistics);
		hibernateProperties.setProperty("hibernate.cache.use_query_cache", hibernateCacheUseQueryCache);
		hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", hibernateCacheUseSecondLevelCache);
		hibernateProperties.setProperty("hibernate.flushMode", hibernateFlushMode);
		hibernateProperties.setProperty("javax.persistence.validation.mode", javaxPersistenceValidationMode);
		hibernateProperties.setProperty("hibernate.cache.region.factory_class", hibernateCacheRegionFactoryClass);
		hibernateProperties.setProperty("net.sf.ehcache.configurationResourceName",
				netSfEhcacheConfigurationResourceName);
		hibernateProperties.setProperty("connection.pool_size", connectionPoolSize);
		localSessionFactoryBean.setHibernateProperties(hibernateProperties);

		return localSessionFactoryBean;

	}

	@Bean
	public LocalSessionFactoryBean sessionFactorySess(DataSource dataSource) {

		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(dataSource);
		localSessionFactoryBean.setPackagesToScan("com.dxc.integral");
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.dialect", hibernateDialect);
		hibernateProperties.setProperty("hibernate.show_sql", hibernateShowSql);
		hibernateProperties.setProperty("hibernate.generate_statistics", hibernateGenerateStatistics);
		hibernateProperties.setProperty("hibernate.cache.use_query_cache", hibernateCacheUseQueryCache);
		hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", hibernateCacheUseSecondLevelCache);
		hibernateProperties.setProperty("hibernate.flushMode", hibernateFlushMode);
		hibernateProperties.setProperty("javax.persistence.validation.mode", javaxPersistenceValidationMode);
		hibernateProperties.setProperty("hibernate.cache.region.factory_class", hibernateCacheRegionFactoryClass);
		hibernateProperties.setProperty("net.sf.ehcache.configurationResourceName",
				netSfEhcacheConfigurationResourceName);
		hibernateProperties.setProperty("connection.pool_size", connectionPoolSize);
		hibernateProperties.setProperty("hibernate.default_schema", hibernateDefaultSchema);
		localSessionFactoryBean.setHibernateProperties(hibernateProperties);

		return localSessionFactoryBean;

	}

}
