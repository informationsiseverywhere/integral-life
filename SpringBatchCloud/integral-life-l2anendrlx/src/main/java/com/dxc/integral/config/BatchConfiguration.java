package com.dxc.integral.config;

import javax.annotation.PostConstruct;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import com.dxc.integral.beans.L2anendrlxReaderDTO;

/**
 * L2ANENDRLX job configuration
 * 
 * @author vhukumagrawa
 *
 */
@Configuration
@Import({ HibernateSessionFactoryConfig.class, BatchComponenetsConfig.class })
public class BatchConfiguration {

	@Value("${job.report-config}")
	private String reportConfig; 
	
	@Value("${l2anendrlx.chunk-size}")
	private Integer chunkSize;

	@Value("${l2anendrlx.throttle-limit}")
	private Integer throttleLimit;

	@Value("${l2anendrlx.concurrency-limit}")
	private Integer concurrencyLimit;

	/**
	 * Constructor
	 */
	public BatchConfiguration() {
		// Calling default constructor
	}

	@Bean
	public TaskExecutor taskExecutor() {
		SimpleAsyncTaskExecutor executer = new SimpleAsyncTaskExecutor("l2anendrlx_");
		executer.setConcurrencyLimit(concurrencyLimit);
		return executer;
	}
	
	@PostConstruct
	public void setSystemProperties() {
		//set report configuration
		System.setProperty("integralsbconfig", reportConfig);
	} 

	@Bean
	public Job job(StepExecutionListener integralStepListener, JobExecutionListener integralJobListener,
			ItemReader<L2anendrlxReaderDTO> reader,
			@Qualifier("l2anendrlxItemProcessor") ItemProcessor<L2anendrlxReaderDTO, L2anendrlxReaderDTO> processor,
			@Qualifier("l2anendrlxItemWriter") ItemWriter<L2anendrlxReaderDTO> writer,
			StepBuilderFactory stepBuilderFactory, JobBuilderFactory jobBuilderFactory) {

		Step step = stepBuilderFactory.get("BR622").<L2anendrlxReaderDTO, L2anendrlxReaderDTO> chunk(chunkSize)
				.reader(reader).processor(processor).writer(writer).taskExecutor(taskExecutor())
				.throttleLimit(throttleLimit).listener(integralStepListener).build();

		return jobBuilderFactory.get("l2anendrlx").incrementer(new RunIdIncrementer()).listener(integralJobListener)
				.start(step).build();
	}
	
}
