package com.dxc.integral.utils;

import java.io.IOException;
import com.dxc.integral.beans.L2anendrlxProcessorDTO;
import com.dxc.integral.beans.L2anendrlxReaderDTO;
import com.dxc.integral.iaf.exceptions.ItemNotfoundException;
import com.dxc.integral.life.beans.CrtloanDTO;
import com.dxc.integral.life.beans.ZrcshopDTO;
import com.dxc.integral.life.smarttable.pojo.T5645;

/**
 * Processor Utility for L2ANENDRLX batch.
 * 
 * @author mmalik25
 *
 */

public interface L2anendrlxProcessorUtil {

	/**
	 * @param readerDTO
	 * @param tranDate
	 * @throws IOException
	 * @return
	 */
	public L2anendrlxProcessorDTO getPaymentDueInfo(L2anendrlxReaderDTO readerDTO, Integer tranDate)
			throws IOException;/* ILIFE-5977 */

	/**
	 * @param readerDTO
	 * @param t5645IO
	 * @param processorDTO
	 * @param longdesc
	 * @throws IOException
	 * @return
	 */
	public ZrcshopDTO populateZrcshopDTO(L2anendrlxReaderDTO readerDTO, T5645 t5645IO, String longdesc)
			throws IOException;/* ILIFE-5977 */

	/**
	 * @param readerDTO
	 * @throws IOException
	 * @throws ItemNotfoundException
	 * @return
	 */
	public boolean checkValidCoverage(L2anendrlxReaderDTO readerDTO) throws IOException;// ILIFE-5763
																						// /*ILIFE-5977*/

	/**
	 * @param readerDTO
	 * @param t5645IO
	 * @param longdesc
	 * @return
	 */
	public CrtloanDTO populateCrtloanDTO(L2anendrlxReaderDTO readerDTO, T5645 t5645IO, String longdesc);
}
