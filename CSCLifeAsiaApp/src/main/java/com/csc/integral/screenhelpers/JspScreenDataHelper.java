package com.csc.integral.screenhelpers;

import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.screendef.QPScreenField;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.HTMLFormatter;

/**
 * Helps consolidate how a {@link BaseScreenData} is rendered in HTML.
 *
 * @author pnguyen58
 */
public class JspScreenDataHelper {

    /** Hidden, to create instances, use {@link Builder} instead */
    private JspScreenDataHelper() {
        super();
    }

    public String formattedValue;
    public HtmlInputTagAttributes tagInput;

    public static class Builder {

        private final ScreenModel screenModel;
        private final HTMLFormatter formatter;

        private BaseScreenData screenData;
        private QPScreenField screenField;

        private String picInHtml;

        public Builder(ScreenModel screenModel, HTMLFormatter formatter) {
            this.screenModel = screenModel;
            this.formatter = formatter;
        }

        public Builder setScreenData(BaseScreenData screenData) {
            this.screenData = screenData;
            return this;
        }

        public Builder setPicInHtml(String picInHtml) {
            this.picInHtml = picInHtml;
            return this;
        }

        public JspScreenDataHelper build() {
            validateRequiredProperties();
            initProperties();
            return createScreenDataHelper();
        }

        private void validateRequiredProperties() {
            if (screenData == null || formatter == null || screenModel == null)
                throw new AssertionError("Null initialization propertie(s).");
        }

        private void initProperties() {
            screenField = screenModel.getFieldXMLDef(screenData.getFieldName());
            if (picInHtml != null) screenField.setPicinHTML(picInHtml);
        }

        private JspScreenDataHelper createScreenDataHelper() {
            JspScreenDataHelper data = new JspScreenDataHelper();
            init(data);
            return data;
        }

        private void init(JspScreenDataHelper data) {
            data.formattedValue = getFormattedValue();
            data.tagInput = new HtmlInputTagAttributes(screenData, screenField, formatter);
        }

        private String getFormattedValue() {
            if (picInHtml != null) return formatter.getPicFormatted(screenField, screenData);
            String formData = screenData.getFormData();
            return (formData == null) ? "" : formData;
        }
    }
}
