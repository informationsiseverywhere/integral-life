package com.csc.integral.screenhelpers;

import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.screendef.QPScreenField;
import com.quipoz.framework.util.HTMLFormatter;

/**
 * Helps consolidate how a {@link BaseScreenData} is rendered in HTML
 * <input> element.
 *
 * @author pnguyen58
 */
public class HtmlInputTagAttributes {

    private final HTMLFormatter formatter;
    private final BaseScreenData screenData;
    private final QPScreenField screenField;

    public final String value;
    public final String title;
    public final int size;
    public final int maxLength;
    public final int decimal;
    public final String readonly;
    public final String classAttr;

    /** Only meant to be created by {@link JspScreenDataHelper} */
    HtmlInputTagAttributes(BaseScreenData screenData, QPScreenField screenField, HTMLFormatter formatter) {
        super();
        this.formatter = formatter;
        this.screenData = screenData;
        this.screenField = screenField;

        value = getPicFormatted();
        title = getTitle(value);
        size = screenData.getLength();
        maxLength = screenData.getLength();
        decimal = screenField.getDecimals();
        readonly = getReadOnly();
        classAttr = getClassAttr();
    }

    private String getPicFormatted() {
        String value = formatter.getPicFormatted(screenField, screenData);
        return value == null ? "" : value;
    }

    private static String getTitle(String value) {
        return isBlank(value) ? "" : value;
    }

    private String getReadOnly() {
        return isDisabled(screenData) ? "true" : "false";
    }

    private String getClassAttr() {
        if (isDisabled(screenData)) return "output_cell";
        else if (isBold(screenData)) return "bold_cell";
        else if (isRed(screenData)) return "input_cell red reverse";
        else return "input_cell";
    }

    private static boolean isBlank(String str) {
        return str == null || str.length() == 0;
    }

    private static boolean isDisabled(BaseScreenData screenData) {
        return screenData.getEnabled() == BaseScreenData.DISABLED;
    }

    private static boolean isBold(BaseScreenData screenData) {
        return screenData.getHighLight() == BaseScreenData.BOLD;
    }

    private static boolean isRed(BaseScreenData screenData) {
        return BaseScreenData.RED.equals(screenData.getColor());
    }
}
