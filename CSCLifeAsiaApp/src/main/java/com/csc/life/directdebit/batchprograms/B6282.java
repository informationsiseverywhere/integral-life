/*
 * File: B6282.java
 * Date: 29 August 2009 21:21:33
 * Author: Quipoz Limited
 *
 * Class transformed from B6282.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.directdebit.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.BbalTableDAM;
import com.csc.fsu.general.dataaccess.BextTableDAM;
import com.csc.fsu.general.dataaccess.DdsuTableDAM;
import com.csc.fsu.general.dataaccess.DshnTableDAM;
import com.csc.fsu.general.dataaccess.DshnpfTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CoprpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.enquiries.tablestructures.Th5llrec;
import com.csc.life.general.dataaccess.dao.DshnpfDAO;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.recordstructures.P6283par;
import com.csc.life.regularprocessing.reports.R6282Report;
import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
* B6282 - Dishonour Batch Processing
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Overview
* ~~~~~~~~
*
* Dishonour processing subsystem has two parts, the first  part  i
* the  register  of  dishonour,  the  second  part  is  the  actua
* processing required which takes place in batch. This  program  i
* associated  with  the batch processing of the subsystem. The mai
* processing  of  this  program  is  to  back  out  the  collectio
* processing  and  to  re-submit the billing according to DSHN. Th
* program is driven by the dishonour file  (DSHNPF)  and  based  o
* the  selection  criteria  captured  from  S6283. OPNQRYF is to b
* used in the CL program to perform a pre-select on the DSHNPF.
*
*
* R6282
* ~~~~~
*
* Create a printer file to show  all  the  dishonour  transactions
* One  line  per  DSHN RECORD. The report should show the followin
* fields:-
*
*     PAYRCOY
*     PAYRNUM
*     Payor name
*     MANDREF
*     BILLCD
*     FACTHOUS
*     BANKCODE
*     BANKKEY
*     BANKACCKEY
*     CHDRCOY
*     CHDRNUM
*     CNTTYPE
*     OCCDATE
*     INSTAMT06
*     JOBNO
*     MANDSTAT
*     PROCACTION
*
*
* C6282
* ~~~~~
*
* Create a standard CL program and add the following coding:-
*
*     Call 'PASSPARM' to retrieve the parameters for
*     selection (OPNQRYF) purpose. Use substring to put the
*     parameters into different fields. These fields need to
*     be declared at the beginning of the program.
*
*     Use OPNQRYF on DSHNPF to select specific records and
*     the selection criteria is as follows:-
*
*         Processed Flag not = '3'            and
*         Company = PARM-COMPANY              and
*         Job number not > Job number to      and
*         Job number not < Job number from    and
*         Service Unit = 'LP'
*
*     With the following key order:-
*
*         Company
*         Job number
*         PAYRCOY
*         PAYRNUM
*         MANDREF
*         CHDRCOY
*         CHDRNUM
*         INSTFROM  Descending order
*
*     After calling B6282, we need to delete some of the
*     old records by using CPYF and the parameter1 from the
*     jobstream. Parameter1 is a job number and we will
*     delete any records whose job number is less than
*     parameter1.
*
*
* DSHN
* ~~~~
*
* Create a logical file DSHN based on the physical file DSHNPF  an
* include   all  the  physical  file  fields  and  specify  arriva
* sequence as LIFO. Also keyed on the following:-
*
*     COMPANY
*     JOBNO
*     PAYRCOY
*     PAYRNUM
*     MANDREF
*     BILLCD
*     INSTFROM
*
*
* B6282
* ~~~~~
*
* Create a standard batch program.
*
* Select DSHNPF under INPUT-OUTPUT SECTION.
*
*
* Tables: T6661 - Reversal Subroutines
*
* Files: DSHNPF - Dishonour Register Details
*        DSHN - Dishonour Details(logical)
* </pre>
*/
public class B6282 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private DshnpfTableDAM dshnpf = new DshnpfTableDAM();
	private R6282Report printerFile = new R6282Report();
	private DshnpfTableDAM dshnpfRec = new DshnpfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6282");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaEndOfFile = "N";
	private String wsaaEmptyReport = "N";

	private FixedLengthStringData wsaaReversalPossible = new FixedLengthStringData(1).init("Y");
	private Validator wsaaNoReversal = new Validator(wsaaReversalPossible, "N");

		/* N.B. Please amend this program name to suit if the
		      Transaction Code of the processing or the Program
		      which handles the processing changes. This also
		      stands for the Item Name.                                  */
	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTtime = new FixedLengthStringData(6).isAPartOf(wsaaTime, 0);
	private ZonedDecimalData wsaaHours = new ZonedDecimalData(2, 0).isAPartOf(wsaaTtime, 0).setUnsigned();
	private ZonedDecimalData wsaaMinutes = new ZonedDecimalData(2, 0).isAPartOf(wsaaTtime, 2).setUnsigned();
	private ZonedDecimalData wsaaSeconds = new ZonedDecimalData(2, 0).isAPartOf(wsaaTtime, 4).setUnsigned();
	private ZonedDecimalData wsaaTimeR = new ZonedDecimalData(6, 0);
		/* additional date format WSAA-TODAY and WSAA-BTIME
		 used to update the bbalpf.*/
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private static final int wsaaBtime = 0;

	private FixedLengthStringData wsaaDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTdate = new FixedLengthStringData(6).isAPartOf(wsaaDate, 2);
	private ZonedDecimalData wsaaYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaTdate, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaTdate, 2).setUnsigned();
	private ZonedDecimalData wsaaDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaTdate, 4).setUnsigned();
	private ZonedDecimalData wsaaDateR = new ZonedDecimalData(6, 0);

		/* VALUE 'No Dishonour Transactions'.
		 VALUE 'No reversal is possible  '.                      <012>
		                                                            <012>*/
	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRunparm1TranCode = new FixedLengthStringData(4).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaRunparm1Rest = new FixedLengthStringData(6).isAPartOf(wsaaRunparm1, 4);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaPtrnTranno = new ZonedDecimalData(5, 0).setUnsigned(); //Ticket #PINNACLE-1831 : 2nd Dishonor processing having incorrect transaction posting
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaFoundCollection = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaBillingReversed = new FixedLengthStringData(1).init(SPACES);
	private String wsaaPtrnFlag = "";
	private ZonedDecimalData wsaaReversalOutstanding = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaChdrValid = "";
		/* TABLES */
	private static final String t6661 = "T6661";
	private static final String t3678 = "T3678";
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t6626 = "T6626";
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaPageSize = 60;
	private static final String MAND_STATUS_LIVE = "10";
	//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy
		private DshnpfDAO dshnpfDAO = getApplicationContext().getBean("dshnpfDAO", DshnpfDAO.class);
		private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class);
		//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy ends
	private FixedLengthStringData r6282h01Record = new FixedLengthStringData(99);
	private FixedLengthStringData r6282h01O = new FixedLengthStringData(99).isAPartOf(r6282h01Record, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(r6282h01O, 0);
	private ZonedDecimalData jobnofrom = new ZonedDecimalData(8, 0).isAPartOf(r6282h01O, 10);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r6282h01O, 18);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r6282h01O, 19);
	private ZonedDecimalData jobnoto = new ZonedDecimalData(8, 0).isAPartOf(r6282h01O, 49);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r6282h01O, 57);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(r6282h01O, 67);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(r6282h01O, 69);

	private FixedLengthStringData r6282d02Record = new FixedLengthStringData(1);

	private FixedLengthStringData r6282d03Record = new FixedLengthStringData(1);
	private BbalTableDAM bbalIO = new BbalTableDAM();
	private BextTableDAM bextIO = new BextTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DdsuTableDAM ddsuIO = new DdsuTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private DshnTableDAM dshnIO = new DshnTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MandTableDAM mandIO = new MandTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Varcom varcom = new Varcom();
	private T6661rec t6661rec = new T6661rec();
	private T3678rec t3678rec = new T3678rec();
	private T6626rec t6626rec = new T6626rec();
	private T5679rec t5679rec = new T5679rec();
	private Reverserec reverserec = new Reverserec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Syserrrec syserrrec = new Syserrrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private P6283par p6283par = new P6283par();
	private FormatsInner formatsInner = new FormatsInner();
	private R6282d01RecordInner r6282d01RecordInner = new R6282d01RecordInner();
	private boolean isAiaAusDirectDebit ;
	private boolean isAiaAusPaymentGateway;
	private boolean isProrate;
	private LinspfDAO linspfDAO =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private CoprpfDAO coprpfDAO = getApplicationContext().getBean("coprpfDAO", CoprpfDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		updateChdr1050,
		exit1090,
		readNextPtrn2080,
		exit2090
	}

	public B6282() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
    isAiaAusDirectDebit = FeaConfg.isFeatureExist("2", "BTPRO029", appVars, "IT");
		open110();
		openBatch120();
		mainProcess130();
		closeBatch170();
		out185();
	}

protected void open110()
	{
		dshnpf.openInput();
		printerFile.openOutput();
		initialise200();
	}

protected void openBatch120()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void mainProcess130()
	{
		while ( !(isEQ(wsaaEndOfFile, "Y"))) {
			mainProcessing1000();
		}

		/* Was any information written to the report?*/
		if (isEQ(wsaaEmptyReport, "Y")) {
			/*    MOVE WSAA-EMPTY-REPORT-MESSAGE                            */
			/*         TO LONGSTR OF R6282D02-RECORD                        */
			pageOverflow5000();
			printerRec.set(SPACES);
			printerFile.printR6282d02(r6282d02Record);
			wsaaLineCount.add(1);
		}
		if (wsaaNoReversal.isTrue()) {
			/*    MOVE WSAA-NO-REVERSAL-MESSAGE                        <012>*/
			/*         TO LONGSTR OF R6282D02-RECORD                   <012>*/
			pageOverflow5000();
			printerRec.set(SPACES);
			/* WRITE PRINTER-REC FROM R6282D02-RECORD FORMAT 'R6282D02'<012>*/
			printerFile.printR6282d03(r6282d03Record);
			wsaaLineCount.add(1);
		}
	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void out185()
	{
		dshnpf.close();
		printerFile.close();
		/*EXIT*/
	}

protected void initialise200()
	{
    isAiaAusPaymentGateway = FeaConfg.isFeatureExist("2", "BTPRO030", appVars, "IT");
		initialise210();
		setUpHeadingCompany220();
		setUpHeadingBranch230();
		setUpRunDate250();
		effdate260();
		jobnofrJobnoto270();
	}

protected void initialise210()
	{
		wsaaEndOfFile = "N";
		wsaaEmptyReport = "Y";
		wsaaReversalPossible.set("Y");
		p6283par.parmRecord.set(conjobrec.params);
		wsaaRunparm1.set(runparmrec.runparm1);
	}

protected void setUpHeadingCompany220()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(runparmrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		company.set(runparmrec.company);
		companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch230()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(runparmrec.branch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		branch.set(runparmrec.branch);
		branchnm.set(descIO.getLongdesc());
	}

protected void setUpRunDate250()
	{
		/*               AND RUN TIME.......                               */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon1rec.datcon1Rec);
			conerrrec.statuz.set(datcon1rec.statuz);
			systemError005();
		}
		sdate.set(datcon1rec.extDate);
		/* MOVE DTC1-INT-DATE          TO WSAA-DATE.            <A07125>*/
		wsaaToday.set(datcon1rec.intDate);
		wsaaDate.set(wsaaToday);
		wsaaTime.set(getCobolTime());
	}

protected void effdate260()
	{
		repdate.set(p6283par.effdates);
	}

protected void jobnofrJobnoto270()
	{
		jobnofrom.set(p6283par.jobnofrom);
		jobnoto.set(p6283par.jobnoto);
		/* Initialise line count to cause page overflow.*/
		wsaaLineCount.set(99);
		/* Read T6626 once so that if we have to do any reversals, we      */
		/*   don't need to keep reading T6626 every time around the 2000   */
		/*   loop                                                          */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6626);
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemitem(wsaaRunparm1TranCode);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t6626rec.t6626Rec.set(itemIO.getGenarea());
		/* Read T5679 for valid status codes.                           */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(runparmrec.transcode);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					process1100();
				case updateChdr1050:
					updateChdr1050();
					getClientName1060();
					writeRecordToPrinter1080();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void process1100()
	{
		/* Check Re-start by performing 004-UPDATE-REQD*/
		updateReqd004();
		if (isEQ(controlrec.flag, "N")) {
			goTo(GotoLabel.exit1090);
		}
		/* Read the input file sequentially until end of file.*/
		dshnpf.read(dshnpfRec);
		if (dshnpf.isAtEnd()) {
			wsaaEndOfFile = "Y";
			goTo(GotoLabel.exit1090);
		}
		/* Call 'CONTOT' to update Control Total No.1*/
		/* (number of records read)*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/* Read Contract Header (moved from below DSHN read for         */
		/* validation purposes.                                         */
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(dshnpfRec.chdrcoy);
		chdrlifIO.setChdrnum(dshnpfRec.chdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		/* Validate contract header status codes.  If contract status   */
		/* codes are not valid, do not process the dishonour.           */
		wsaaChdrValid = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			validateChdrStatus1100();
		}
		if (isNE(wsaaChdrValid, "Y")) {
			goTo(GotoLabel.exit1090);
		}
		//Ticket #PINNACLE-98
	        if ((isAiaAusDirectDebit || isAiaAusPaymentGateway)  && isNE(dshnpfRec.dhnflag, "2")) {
	            goTo(GotoLabel.exit1090);
	        }
		/* Read and hold DSHN with function READH*/
		dshnIO.setParams(SPACES);
		dshnIO.setCompany(dshnpfRec.company);
		dshnIO.setChdrnum(dshnpfRec.chdrnum);
		dshnIO.setJobno(dshnpfRec.jobno);
		dshnIO.setPayrcoy(dshnpfRec.payrcoy);
		dshnIO.setPayrnum(dshnpfRec.payrnum);
		//Ticket #PINNACLE-98
	        if ((isAiaAusDirectDebit || isAiaAusPaymentGateway) && isNE(dshnpfRec.dhnflag, "2")) {
	            goTo(GotoLabel.exit1090);
	        }
		if(((isAiaAusDirectDebit || isAiaAusPaymentGateway) && StringUtils.isNotEmpty(dshnpfRec.mandref.trim())) || !(isAiaAusDirectDebit && isAiaAusPaymentGateway)) {
		    dshnIO.setMandref(dshnpfRec.mandref);
		}
		dshnIO.setBillcd(dshnpfRec.billcd);
		dshnIO.setInstfrom(dshnpfRec.instfrom);
		dshnIO.setFunction(varcom.readh);
		dshnIO.setFormat(formatsInner.dshnrec1);
		SmartFileCode.execute(appVars, dshnIO);
		if (isNE(dshnIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(dshnIO.getParams());
			databaseError006();
		}
		
		/**
		 * fetch lins where chdrnum = dshn.chdrnum, insfrm = dshn.instfrm, instto = dshn.insto,
		 * if found, check if copr exits for same input
		 * if not found, set isProrate ON
		 */
		Linspf linsObj = linspfDAO.getProratedRecordDishonored(dshnIO.getChdrcoy().toString(),dshnIO.getChdrnum().toString(),"Y",dshnIO.getInstfrom().toInt(),dshnIO.getInstto().toInt());
		isProrate = false; //reset foreach dshn
		if(null != linsObj) {
			Coprpf coprpf = new Coprpf();
			coprpf.setChdrcoy(dshnIO.getChdrcoy().toString());
			coprpf.setChdrnum(dshnIO.getChdrnum().toString());
			coprpf.setInstfrom(dshnIO.getInstfrom().toInt());
			coprpf.setInstto(dshnIO.getInstto().toInt());
			coprpf.setValidflag("2");
			List<Coprpf> coprList = coprpfDAO.getCoprpfRecord(coprpf);
			if(coprList.isEmpty()) {
				isProrate = true;
			}
		}
		/* Read CHDRLIF with function READR*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(dshnpfRec.chdrcoy);
		chdrlifIO.setChdrnum(dshnpfRec.chdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		/* Softlock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(dshnpfRec.chdrnum);
		sftlockrec.transaction.set(runparmrec.transcode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
		/* Update transaction number and store                             */
		/*ADD 1                       TO CHDRLIF-TRANNO.               */
		compute(wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));
		if(isAiaAusDirectDebit) {
			compute(wsaaPtrnTranno, 0).set(add(1, chdrlifIO.getTranno()));//Ticket #PINNACLE-1831 : 2nd Dishonor processing having incorrect transaction posting
		}
		/* Temporary fix to ensure that there is a value other than*/
		/* zeroes in the field DSHN-INSTFROM*/
		if (isEQ(dshnIO.getInstfrom(), ZERO)) {
			datcon2rec.statuz.set(SPACES);
			datcon2rec.intDate1.set(dshnIO.getBtdate());
			datcon2rec.freqFactor.set(-1);
			datcon2rec.frequency.set(dshnIO.getInstfreq());
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				databaseError006();
			}
			dshnIO.setInstfrom(datcon2rec.intDate2);
			dshnIO.setInstto(dshnIO.getBtdate());
		}
		/* If DSHN-PROCACTION = 1 reversal required*/
		/*Ticket #:Pinnacle-98 for austrailia localization reversal is always required */
		if (isAiaAusDirectDebit || isAiaAusPaymentGateway ||  (isEQ(dshnIO.getProcaction(), "1"))) {
			ptrnrevIO.setParams(SPACES);
			ptrnrevIO.setChdrcoy(chdrlifIO.getChdrcoy());
			ptrnrevIO.setChdrnum(chdrlifIO.getChdrnum());
			ptrnrevIO.setTranno(99999);
			ptrnrevIO.setFunction(varcom.begn);
			ptrnrevIO.setFormat(formatsInner.ptrnrevrec);
			SmartFileCode.execute(appVars, ptrnrevIO);
			if ((isNE(ptrnrevIO.getStatuz(), varcom.oK))
			&& (isNE(ptrnrevIO.getStatuz(), varcom.endp))) {
				conerrrec.params.set(ptrnrevIO.getParams());
				databaseError006();
			}
			/* Contract break*/
			if ((isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy()))
			|| (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum()))
			|| (isEQ(ptrnrevIO.getStatuz(), varcom.endp))) {
				ptrnrevIO.setStatuz(varcom.endp);
			}
			wsaaFoundCollection.set(SPACES);
			wsaaBillingReversed.set(SPACES);
			wsaaReversalOutstanding.set(ZERO);
			while ( !(isEQ(ptrnrevIO.getStatuz(), varcom.endp))) {
				reversalProcessing2000();
				/*if(isAiaAusDirectDebit) { //PINNACLE-1905
					wsaaPtrnTranno.set(add(1,wsaaPtrnTranno));//Ticket #PINNACLE-1831 : 2nd Dishonor processing having incorrect transaction posting
					
				}*/
				
			}

		}
		//Ticket #PINNACLE-979: AIA doesn't want re billing
		/* If DSHN-PROCACTION = 2 representation required*/
		if ((isAiaAusDirectDebit || isAiaAusPaymentGateway) && (isEQ(dshnIO.getProcaction(), "2")) && !isProrate) { // do not create bext for billing change copr != null
			rebillProcessing3000();
		}
		/* Deduct the amount of the dishonoured debit from the total       */
		/* stored on the corresponding record of the Bank Balance file     */
		/* (if not found, add a new record).                               */
		bbalIO.setBbalpfx(smtpfxcpy.bbal);
		bbalIO.setBbalcoy(runparmrec.company);
		bbalIO.setBbalcode(dshnIO.getBankcode());
		bbalIO.setFormat(formatsInner.bbalrec);
		bbalIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bbalIO);
		if (isNE(bbalIO.getStatuz(), varcom.oK)
		&& isNE(bbalIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(bbalIO.getParams());
			databaseError006();
		}
		if (isEQ(bbalIO.getStatuz(), varcom.mrnf)) {
			bbalIO.setBbalbal(ZERO);
			bbalIO.setFunction(varcom.writr);
		}
		else {
			bbalIO.setFunction(varcom.rewrt);
		}
		bbalIO.setBbaltdte(wsaaToday);
		bbalIO.setBbaltime(wsaaBtime);
		setPrecision(bbalIO.getBbalbal(), 2);
		bbalIO.setBbalbal(sub(bbalIO.getBbalbal(), dshnIO.getInstamt06()));
		SmartFileCode.execute(appVars, bbalIO);
		if (isNE(bbalIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bbalIO.getParams());
			databaseError006();
		}
		/* Read and hold the corresponding MAND record with a*/
		/* function READH (key from DSHN)*/
		if(((isAiaAusDirectDebit || isAiaAusPaymentGateway) && StringUtils.isNotEmpty(dshnpfRec.mandref.trim())) || !(isAiaAusDirectDebit && isAiaAusPaymentGateway)) {
		    
		    mandIO.setParams(SPACES);
		    mandIO.setPayrcoy(dshnIO.getPayrcoy());
		    mandIO.setPayrnum(dshnIO.getPayrnum());
		    mandIO.setMandref(dshnIO.getMandref());
		    mandIO.setFunction(varcom.readh);
		    mandIO.setFormat(formatsInner.mandrec);
		    SmartFileCode.execute(appVars, mandIO);
		    if (isNE(mandIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(mandIO.getParams());
			databaseError006();
		    }
		}
		/*                                                      <V76F10>*/
		/* MOVE '2'                    TO MAND-VALIDFLAG.       <V76F10>*/
		/* MOVE REWRT                  TO MAND-FUNCTION.        <V76F10>*/
		/* MOVE MANDREC                TO MAND-FORMAT.          <V76F10>*/
		/*                                                      <V76F10>*/
		/* CALL 'MANDIO'               USING MAND-PARAMS.       <V76F10>*/
		/*                                                      <V76F10>*/
		/* IF MAND-STATUZ          NOT = O-K                    <V76F10>*/
		/*    MOVE MAND-PARAMS         TO CONR-PARAMS           <V76F10>*/
		/*    PERFORM 006-DATABASE-ERROR.                       <V76F10>*/
		/*                                                      <V76F10>*/
		/* Read T3678 using MAND-MANDSTAT as the item*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		/*MOVE PARM-COMPANY           TO WSKY-ITEM-ITEMCOY.            */
		wsaaItemkey.itemItemcoy.set(runparmrec.fsuco);
		wsaaItemkey.itemItemtabl.set(t3678);
		if(StringUtils.isNotEmpty(mandIO.getMandstat().trim())) {
		    wsaaItemkey.itemItemitem.set(mandIO.getMandstat());
		}else {
		    wsaaItemkey.itemItemitem.set(MAND_STATUS_LIVE);
		}
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t3678rec.t3678Rec.set(itemIO.getGenarea());
		/* Check the GO/NOGO Flag*/
		/* If GO/NOGO Flag not equal to 'N'*/
		/*    Read T3678 using DSHN-MANDSTAT as the item*/
		/*    If GO/NOGO = 'N'*/
		/*       Set MAND-MANDSTAT to DSHN-MANDSTAT*/
		if (isEQ(t3678rec.gonogoflg, "N")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaItemkey.set(SPACES);
			wsaaItemkey.itemItempfx.set("IT");
			/*MOVE PARM-COMPANY        TO WSKY-ITEM-ITEMCOY             */
			wsaaItemkey.itemItemcoy.set(runparmrec.fsuco);
			wsaaItemkey.itemItemtabl.set(t3678);
			if(((isAiaAusDirectDebit || isAiaAusPaymentGateway) && StringUtils.isNotEmpty(dshnpfRec.mandref.trim())) || !(isAiaAusDirectDebit && isAiaAusPaymentGateway)) {
			    wsaaItemkey.itemItemitem.set(dshnIO.getMandstat());
			}else {
			    wsaaItemkey.itemItemitem.set("10");
			}
			itemIO.setDataKey(wsaaItemkey);
			itemIO.setFunction(varcom.readr);
			itemIO.setFormat(formatsInner.itemrec);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				conerrrec.params.set(itemIO.getParams());
				databaseError006();
			}
			t3678rec.t3678Rec.set(itemIO.getGenarea());
			if (isEQ(t3678rec.gonogoflg, "N")) {
				mandIO.setMandstat(dshnIO.getMandstat());
			}
		}
		/* Rewrite MAND with a function 'REWRT'*/
		/* MOVE REWRT                  TO MAND-FUNCTION.                */
		/* MOVE '1'                    TO MAND-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO MAND-FUNCTION.        <V76F10>*/
		mandIO.setFunction(varcom.rewrt);
		mandIO.setFormat(formatsInner.mandrec);
		if(((isAiaAusDirectDebit || isAiaAusPaymentGateway) && StringUtils.isNotEmpty(dshnpfRec.mandref.trim())) || !(isAiaAusDirectDebit && isAiaAusPaymentGateway)) {
		    SmartFileCode.execute(appVars, mandIO);
		    if (isNE(mandIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(mandIO.getParams());
			databaseError006();
		    }
		}
		/* Set DSHN-PROCESSING to '3' (to denote record has been*/
		/* processed)*/
		dshnIO.setDhnflag("3");
		/* Rewrite DSHN with a function 'REWRT'*/
		dshnIO.setFunction(varcom.rewrt);
		dshnIO.setFormat(formatsInner.dshnrec1);
		SmartFileCode.execute(appVars, dshnIO);
		if (isNE(dshnIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(dshnIO.getParams());
			databaseError006();
		}
		/* Read and hold the corresponding DDSU record with a*/
		/* function READH*/
		ddsuIO.setParams(SPACES);
		ddsuIO.setCompany(dshnIO.getCompany());
		ddsuIO.setJobno(dshnIO.getJobno());
		ddsuIO.setPayrcoy(dshnIO.getPayrcoy());
		ddsuIO.setPayrnum(dshnIO.getPayrnum());
		ddsuIO.setMandref(dshnIO.getMandref());
		ddsuIO.setBillcd(dshnIO.getBillcd());
		ddsuIO.setFunction(varcom.readh);
		ddsuIO.setFormat(formatsInner.ddsurec);
		SmartFileCode.execute(appVars, ddsuIO);
		if ((isNE(ddsuIO.getStatuz(), varcom.oK))
		&& (isNE(ddsuIO.getStatuz(), varcom.mrnf))) {
			conerrrec.params.set(ddsuIO.getParams());
			databaseError006();
		}
		if (isEQ(ddsuIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.updateChdr1050);
		}
		/* Set DDSU-DHNFLAG to '3'*/
		/* (To denote dishonour processing completed/started)*/
		ddsuIO.setDhnflag("3");
		/* Rewrite the DDSU record with function 'REWRT'*/
		ddsuIO.setFunction(varcom.rewrt);
		ddsuIO.setFormat(formatsInner.ddsurec);
		SmartFileCode.execute(appVars, ddsuIO);
		if (isNE(ddsuIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(ddsuIO.getParams());
			databaseError006();
		}
	}

protected void updateChdr1050()
	{
		/* Read and hold the updated CHDR record with a*/
		/* function READH*/
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		/* Update the Contract Header TRANNO, which may or may not have    */
		/* already been updated.                                           */
		/* If it has been updated by REVBILL, it doesnt matter now 'cos    */
		/* we're just moving in the same value that it would have been     */
		/* updated with in REVBILL anyway.                                 */
		chdrlifIO.setTranno(wsaaTranno);
		/* Update CHDRLIF with function 'REWRT'*/
		/* ADD 1                       TO CHDRLIF-TRANNO.*/
		/*    As REVBILL is being called and it adds 1 to TRANNO*/
		/*    we are not doing it here.*/
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		/* Unlock the softlock*/
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(dshnpfRec.chdrnum);
		sftlockrec.transaction.set(runparmrec.transcode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
		/* Write a PTRN record to denote the transaction*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(runparmrec.company);
		ptrnIO.setBatcbrn(runparmrec.batcbranch);
		ptrnIO.setBatcactyr(runparmrec.acctyear);
		ptrnIO.setBatcactmn(runparmrec.acctmonth);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		/*MOVE CHDRLIF-TRANNO         TO PTRN-TRANNO.                  */
		ptrnIO.setTranno(wsaaTranno);
		/* MOVE PARM-EFFDATE           TO PTRN-PTRNEFF.                 */
		ptrnIO.setPtrneff(dshnIO.getInstfrom());
		ptrnIO.setDatesub(runparmrec.effdate);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setBatctrcde(runparmrec.transcode);
		ptrnIO.setValidflag("1");
		varcom.vrcmTranid.set(runparmrec.tranid);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(varcom.vrcmUser);
		String userid = ((SMARTAppVars)SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(userid);//PINNACLE-2954
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			databaseError006();
		}
		/* Call 'BATCUP' to update the batch header with function 'WRITS'*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batctrcde.set(runparmrec.transcode);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
	}

protected void getClientName1060()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(dshnIO.getPayrcoy());
		cltsIO.setClntnum(dshnIO.getPayrnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(cltsIO.getParams());
			databaseError006();
		}
	}

protected void writeRecordToPrinter1080()
	{
		/* Convert the format of Original Commencement Date for display*/
		/* on the report*/
		datcon1rec.intDate.set(dshnIO.getOccdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon1rec.datcon1Rec);
			conerrrec.statuz.set(datcon1rec.statuz);
			systemError005();
		}
		r6282d01RecordInner.occdate.set(datcon1rec.extDate);
		/* Convert the format of BILLCD for display on the report*/
		datcon1rec.intDate.set(dshnIO.getBillcd());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon1rec.datcon1Rec);
			conerrrec.statuz.set(datcon1rec.statuz);
			systemError005();
		}
		r6282d01RecordInner.billcd.set(datcon1rec.extDate);
		/* Move the information to the report fields*/
		r6282d01RecordInner.payrcoy.set(dshnIO.getPayrcoy());
		r6282d01RecordInner.payrnum.set(dshnIO.getPayrnum());
		r6282d01RecordInner.surnameprt.set(cltsIO.getSurname());
		r6282d01RecordInner.mandref.set(dshnIO.getMandref());
		r6282d01RecordInner.facthous.set(dshnIO.getFacthous());
		r6282d01RecordInner.bankcode.set(dshnIO.getBankcode());
		r6282d01RecordInner.bankkey.set(dshnIO.getBankkey());
		r6282d01RecordInner.bankacckey.set(dshnIO.getBankacckey());
		r6282d01RecordInner.chdrcoy.set(dshnIO.getChdrcoy());
		r6282d01RecordInner.chdrnum.set(dshnIO.getChdrnum());
		r6282d01RecordInner.cnttype.set(dshnIO.getCnttype());
		r6282d01RecordInner.instamtprt.set(dshnIO.getInstamt06());
		r6282d01RecordInner.jobnum.set(dshnIO.getJobno());
		r6282d01RecordInner.mandstat.set(dshnIO.getMandstat());
		r6282d01RecordInner.procaction.set(dshnIO.getProcaction());
		/* Consider page overflow*/
		pageOverflow5000();
		printerRec.set(SPACES);
		printerFile.printR6282d01(r6282d01RecordInner.r6282d01Record);
		/* ADD 1                       TO WSAA-LINE-COUNT.              */
		wsaaLineCount.add(2);
		wsaaEmptyReport = "N";
		/* Call 'CONTOT' to update Control Total No.3*/
		/* (number of records printed)*/
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void validateChdrStatus1100()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], chdrlifIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				validateChdrPremStatus1150();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus1150()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], chdrlifIO.getPstatcode())) {
			wsaaSub.set(13);
			wsaaChdrValid = "Y";
		}
		/*EXIT*/
	}

protected void reversalProcessing2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2000();
				case readNextPtrn2080:
					readNextPtrn2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2000()
	{
		/* If PTRNREV-BATCTRCDE = PARM-TRANSCODE, read the next*/
		/* PTRNREV record*/
		if (isEQ(ptrnrevIO.getBatctrcde(), runparmrec.transcode)) {
			goTo(GotoLabel.readNextPtrn2080);
		}
		/* MOVE SPACES                 TO WSAA-ITEMKEY.            <012>*/
		/* MOVE 'IT'                   TO WSKY-ITEM-ITEMPFX.       <012>*/
		/* MOVE PARM-COMPANY           TO WSKY-ITEM-ITEMCOY.       <012>*/
		/* MOVE T1670                  TO WSKY-ITEM-ITEMTABL.      <012>*/
		/* MOVE WSAA-RENEWALS          TO WSKY-ITEM-ITEMITEM.      <012>*/
		/* MOVE WSAA-ITEMKEY           TO ITEM-DATA-KEY.           <012>*/
		/*                                                         <012>*/
		/* MOVE READR                  TO ITEM-FUNCTION.           <012>*/
		/* MOVE ITEMREC                TO ITEM-FORMAT.             <012>*/
		/*                                                         <012>*/
		/* CALL 'ITEMIO'               USING ITEM-PARAMS.          <012>*/
		/*                                                         <012>*/
		/* IF (ITEM-STATUZ         NOT = O-K)                      <012>*/
		/*    MOVE ITEM-PARAMS         TO CONR-PARAMS              <012>*/
		/*    PERFORM 006-DATABASE-ERROR.                          <012>*/
		/*                                                         <012>*/
		/* MOVE ITEM-GENAREA           TO T1670-T1670-REC.         <012>*/
		/* PERFORM VARYING WSAA-COUNT                              <012>*/
		/*    FROM 1 BY 1                                          <012>*/
		/*   UNTIL WSAA-COUNT          > 10                        <012>*/
		/*    IF T1670-PROGRAM(WSAA-COUNT) = WSAA-COLL-PROG        <012>*/
		/*       MOVE 11              TO WSAA-COUNT                <012>*/
		/*       IF PTRNREV-PTRNEFF    = DSHN-INSTFROM AND         <012>*/
		/*          PTRNREV-BATCTRCDE  = T1670-RUNPARMA(WSAA-COUNT)<012>*/
		/*          MOVE '1'          TO WSAA-PTRN-FLAG            <012>*/
		/*       END-IF                                            <012>*/
		/*       IF WSAA-PTRN-FLAG     = '1' AND                   <012>*/
		/*          PTRNREV-BATCTRCDE NOT = T1670-RUNPARMA(WSAA-COUNT)12*/
		/*          GO TO 2080-READ-NEXT-PTRN                      <012>*/
		/*       END-IF                                            <012>*/
		/*    END-IF                                               <012>*/
		/* END-PERFORM.                                            <012>*/
		/*IF PTRNREV-PTRNEFF          = DSHN-INSTFROM AND         <010>*/
		/*PTRNREV-BATCTRCDE        = 'B216'                    <010>*/
		/*MOVE '1'                 TO WSAA-PTRN-FLAG.          <010>*/
		/*IF WSAA-PTRN-FLAG           = '1' AND                   <010>*/
		/*PTRNREV-BATCTRCDE        NOT = 'B216'                <010>*/
		/*GO TO 2080-READ-NEXT-PTRN.                           <010>*/
		/* PTRNREV-PTRNEFF < DSHN-INSTFROM*/
		/* IF PTRNREV-PTRNEFF          < DSHN-INSTFROM                  */
		/*    GO TO 2080-READ-NEXT-PTRN.                           <008>*/
		/*    MOVE ENDP TO PTRNREV-STATUZ                               */
		/*    GO TO 2090-EXIT.                                          */
		/* Test PTRN about to be processed against T6626 entries           */
		/*  Note that T6626 is split into 3 thirds, 5 slots for            */
		/*   'Collection-type' transaction codes,  5 slots for             */
		/*   'Issue-type' transaction codes and 5 slots for 'other'        */
		/*    transaction codes.                                           */
		/*  THE following rules are going to apply to our BATCTRCDE test:  */
		/*    1. IF PTRNREV-BATCTRCDE = an 'Issue-type' on T6626,          */
		/*           STOP Reversal processing immediately.                 */
		/*    2. IF PTRNREV-BATCTRCDE = a 'Collection-type' on T6626,      */
		/*           Do some date checking....                             */
		/*           IF PTRNREV-PTRNEFF < DSHN-INSTFROM                    */
		/*              STOP Reversal processing immediately               */
		/*           IF PTRNREV-PTRNEFF >= DSHN-INSTFROM                   */
		/*              REVERSE this PTRN and then keep reversing          */
		/*                  until a Billing PTRN is found and then STOP    */
		/*                  reversing.                                     */
		/*    3. IF PTRNREV-BATCTRCDE <> any transaction code on T6626     */
		/*          Perform whatever reversal processing is required,      */
		/*              UNLESS the transaction in question is deemed to    */
		/*                be a 'Show-stopper'. ie. the table entry on      */
		/*                T6661 has the field T6661-CONT-REV-FLAG = ' '.   */
		/*                In this case we STOP reversal processing.        */
		/*  Check for Issue                                                */
		for (wsaaSub.set(6); !(isGT(wsaaSub, 10)); wsaaSub.add(1)){
			if (isEQ(ptrnrevIO.getBatctrcde(), t6626rec.transcd[wsaaSub.toInt()])) {
				ptrnrevIO.setStatuz(varcom.endp);
				wsaaSub.set(11);
				goTo(GotoLabel.exit2090);
			}
		}
		/* Check if we have found a Billing PTRN - only do this after      */
		/*      we have found and reversed a Collection PTRN               */
		if (isEQ(wsaaFoundCollection, "Y")) {
			/*     IF PTRNREV-BATCTRCDE    = T6626-TRNCD               <013>*/
			if (isEQ(ptrnrevIO.getBatctrcde(), t6626rec.trcode)) {
				if (isLT(ptrnrevIO.getPtrneff(), dshnIO.getInstfrom())) {
					goTo(GotoLabel.readNextPtrn2080);
				}
				wsaaBillingReversed.set("Y");
				wsaaReversalOutstanding.subtract(1);
			}
		}
		/*  If we get to here, then we need to see whether the transaction */
		/*    we are going to reverse is a 'Collection' type or not.       */
		/*  If the PTRN was written by a 'Collection' type transaction,    */
		/*    then we need to do a date check                              */
		for (wsaaSub.set(1); !(isGT(wsaaSub, 5)); wsaaSub.add(1)){
			if (isEQ(ptrnrevIO.getBatctrcde(), t6626rec.transcd[wsaaSub.toInt()])) {
				if (isLT(ptrnrevIO.getPtrneff(), dshnIO.getInstfrom())) {
					goTo(GotoLabel.readNextPtrn2080);
					/*****             MOVE ENDP       TO PTRNREV-STATUZ        <CAS1.0>*/
					/*****             GO TO 2090-EXIT                          <CAS1.0>*/
				}
				wsaaFoundCollection.set("Y");
				wsaaSub.set(6);
				wsaaReversalOutstanding.add(1);
			}
		}
		/*  Make sure the transaction to be reversed is one that           */
		/*  is expected to be reversed.                                    */
		if (isLT(ptrnrevIO.getPtrneff(), dshnIO.getInstfrom())
		&& isEQ(wsaaReversalOutstanding, ZERO)) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		/* Set up REVE-REVERSE-REC*/
		reverserec.reverseRec.set(SPACES);
		reverserec.language.set(runparmrec.language);
		reverserec.statuz.set("****");
		reverserec.company.set(chdrlifIO.getChdrcoy());
		reverserec.chdrnum.set(dshnIO.getChdrnum());
		reverserec.tranno.set(ptrnrevIO.getTranno());
		/*MOVE CHDRLIF-TRANNO         TO REVE-NEW-TRANNO.              */
		if(isAiaAusDirectDebit) {
			reverserec.newTranno.set(wsaaPtrnTranno); //Ticket #PINNACLE-1831 : 2nd Dishonor processing having incorrect transaction posting
		}else {
			reverserec.newTranno.set(wsaaTranno);
		}
		reverserec.effdate1.set(dshnIO.getInstfrom());
		reverserec.effdate2.set(varcom.vrcmMaxDate);
		reverserec.batcpfx.set(batcdorrec.prefix);
		reverserec.batccoy.set(batcdorrec.company);
		reverserec.batcbrn.set(batcdorrec.branch);
		reverserec.batcactyr.set(batcdorrec.actyear);
		reverserec.batcactmn.set(batcdorrec.actmonth);
		reverserec.batcbatch.set(batcdorrec.batch);
		reverserec.batctrcde.set(batcdorrec.trcde);
		reverserec.planSuffix.set(ZERO);
		reverserec.oldBatctrcde.set(ptrnrevIO.getBatctrcde());
		/* MOVE ZEROES                 TO REVE-TRANS-DATE.         <005>*/
		/* MOVE ZEROES                 TO REVE-TRANS-TIME.         <005>*/
		wsaaDateR.set(wsaaTdate);
		wsaaTimeR.set(wsaaTtime);
		reverserec.transDate.set(wsaaDateR);
		reverserec.transTime.set(wsaaTimeR);
		reverserec.user.set(ZERO);
		reverserec.termid.set(SPACES);
		reverserec.ptrneff.set(ptrnrevIO.getPtrneff());
		reverserec.ptrnBatcpfx.set(ptrnrevIO.getBatcpfx());
		reverserec.ptrnBatccoy.set(ptrnrevIO.getBatccoy());
		reverserec.ptrnBatcbrn.set(ptrnrevIO.getBatcbrn());
		reverserec.ptrnBatcactyr.set(ptrnrevIO.getBatcactyr());
		reverserec.ptrnBatcactmn.set(ptrnrevIO.getBatcactmn());
		reverserec.ptrnBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.ptrnBatcbatch.set(ptrnrevIO.getBatcbatch());
		/* Read T6661 using PTRNREV-BATCTRCDE as the item*/
		/* T6661 is now read as an ordinary table, it is no longer dated   */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t6661);
		wsaaItemkey.itemItemitem.set(ptrnrevIO.getBatctrcde());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		/* If we can not find a record on T6661 we want to exit this loop  */
		/*    as no reversal is possible.                                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*GO TO 2080-READ-NEXT-PTRN.                           <003>*/
			ptrnrevIO.setStatuz(varcom.endp);
			wsaaReversalPossible.set("N");
			goTo(GotoLabel.exit2090);
		}
		/*MOVE WSAA-T6661-MSG      TO CONR-PARAMS              <003>*/
		/*PERFORM 006-DATABASE-ERROR.                          <003>*/
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		/*    MOVE SPACES                 TO ITDM-DATA-KEY.                */
		/*    MOVE PARM-COMPANY           TO ITDM-ITEMCOY.                 */
		/*    MOVE T6661                  TO ITDM-ITEMTABL.                */
		/*    MOVE PTRNREV-BATCTRCDE      TO ITDM-ITEMITEM.                */
		/*    MOVE PTRNREV-PTRNEFF        TO ITDM-ITMFRM.                  */
		/*    MOVE BEGN                   TO ITDM-FUNCTION.                */
		/*    CALL 'ITDMIO'               USING ITDM-PARAMS.               */
		/*    IF (ITDM-STATUZ         NOT = O-K) AND                       */
		/*       (ITDM-STATUZ         NOT = ENDP)                          */
		/*       MOVE ITDM-PARAMS         TO CONR-PARAMS                   */
		/*       PERFORM 006-DATABASE-ERROR.                               */
		/*    IF (ITDM-ITEMCOY        NOT = PARM-COMPANY)                  */
		/*    OR (ITDM-ITEMTABL       NOT = T6661)                         */
		/*    OR (ITDM-ITEMITEM       NOT = PTRNREV-BATCTRCDE)             */
		/*       MOVE SPACES              TO T6661-T6661-REC               */
		/*    ELSE                                                         */
		/*       MOVE ITDM-GENAREA        TO T6661-T6661-REC.              */
		if (isEQ(t6661rec.contRevFlag, " ")) {
			wsaaReversalPossible.set("N");
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(t6661rec.contRevFlag, "Y")) {
			if (isNE(t6661rec.subprog01, SPACES)) {
				callProgram(t6661rec.subprog01, reverserec.reverseRec);
				if (isNE(reverserec.statuz, varcom.oK)) {
					conerrrec.params.set(reverserec.reverseRec);
					conerrrec.statuz.set(reverserec.statuz);
					systemError005();
				}
			}
		}
		if (isEQ(t6661rec.contRevFlag, "Y")) {
			if (isNE(t6661rec.subprog02, SPACES)) {
				callProgram(t6661rec.subprog02, reverserec.reverseRec);
				if (isNE(reverserec.statuz, varcom.oK)) {
					conerrrec.params.set(reverserec.reverseRec);
					conerrrec.statuz.set(reverserec.statuz);
					systemError005();
				}
			}
		}
		if(isAiaAusDirectDebit && ptrnrevIO.batctrcde.trim().equalsIgnoreCase("B522")) { //PINNACLE-1905
			wsaaPtrnTranno.set(add(1,wsaaPtrnTranno));//Ticket #PINNACLE-1831 : 2nd Dishonor processing having incorrect transaction posting
		}
		/* If the flag T661-Cont-Rev-Flag is 'N' then do NOT set the valid */
		/* flag to '2'                                                     */
		if (isEQ(t6661rec.contRevFlag, "N")
		&& isNE(wsaaFoundCollection, "Y")) {
			goTo(GotoLabel.readNextPtrn2080);
		}
		/* Read and hold the corresponding PTRNREV record with a*/
		/* function READH*/
		ptrnrevIO.setFunction(varcom.readh);
		ptrnrevIO.setFormat(formatsInner.ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(ptrnrevIO.getParams());
			databaseError006();
		}
		/* Update the PTRNREV record with PTRNREV-VALIDFLAG = '2'*/
		/* using function 'REWRT'*/
		ptrnrevIO.setCrtuser(ptrnrevIO.getUserProfile());
		ptrnrevIO.setValidflag("2");
		ptrnrevIO.setFunction(varcom.rewrt);
		ptrnrevIO.setFormat(formatsInner.ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(ptrnrevIO.getParams());
			databaseError006();
		}
	}

protected void readNextPtrn2080()
	{
    		//PINNACLE-2412
		if (isEQ(wsaaFoundCollection, "Y")) {
		    ptrnrevIO.setStatuz(varcom.endp);
		    return ;
		}
	
		if (isEQ(wsaaFoundCollection, "Y") 
			&& isEQ(wsaaBillingReversed, "Y")
			&& isEQ(wsaaReversalOutstanding, ZERO)) {

			ptrnrevIO.setStatuz(varcom.endp);
			return ;
		}
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if ((isNE(ptrnrevIO.getStatuz(), varcom.oK))
		&& (isNE(ptrnrevIO.getStatuz(), varcom.endp))) {
			conerrrec.statuz.set(ptrnrevIO.getStatuz());
			databaseError006();
		}
		/* Contract break*/
		if ((isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy()))
		|| (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum()))
		|| (isEQ(ptrnrevIO.getStatuz(), varcom.endp))) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
	}

protected void rebillProcessing3000()
	{
		rebill3000();
	}

protected void rebill3000()
	{
		/* Call 'DATCON2' to increment the DSHN-EFFDATE by DSHN-LAPDAY*/
		datcon2rec.freqFactor.set(dshnIO.getLapday());
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(dshnIO.getEffdate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			databaseError006();
		}
		dshnIO.setSubdat(datcon2rec.intDate2);
		/* Write a BEXT record for the re-billing with this new date*/
		bextIO.setParams(SPACES);
		bextIO.setMandstat(dshnIO.getMandstat());
		bextIO.setChdrpfx(chdrlifIO.getChdrpfx());
		bextIO.setChdrcoy(dshnIO.getChdrcoy());
		bextIO.setChdrnum(dshnIO.getChdrnum());
		bextIO.setServunit(dshnIO.getServunit());
		bextIO.setCnttype(dshnIO.getCnttype());
		bextIO.setCntcurr(dshnIO.getCntcurr());
		bextIO.setOccdate(dshnIO.getOccdate());
		bextIO.setCcdate(dshnIO.getCcdate());
		bextIO.setPtdate(dshnIO.getPtdate());
		bextIO.setBtdate(dshnIO.getBtdate());
		if(isAiaAusDirectDebit) {
			//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy
			List<Itempf> itemList = itemDAO.findByMenuTable("TH5LL", chdrlifIO.getBillfreq().trim());
			if(CollectionUtils.isEmpty(itemList)) {
				systemError005();
			}
			Th5llrec rec = new Th5llrec();
			rec.th5llRec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
			if(rec.day.trim().equalsIgnoreCase("0") || rec.day.trim().length()==0 || rec.day.trim().equalsIgnoreCase("000")) {
				bextIO.setBilldate(chdrlifIO.billcd);
			}else {
				Integer dishonorCount = dshnpfDAO.getDishonouredRecordCountInPeriodOfDate(chdrlifIO.instfrom.toInt(),dshnIO.chdrnum.trim()); //PINNACLE-2988
//				datcon2rec.intDate1.set(dshnIO.getInstfrom());
//				datcon2rec.frequency.set("DY");
//				datcon2rec.freqFactor.set(Integer.parseInt(rec.day.trim())*dishonorCount);
//				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
//				if (isNE(datcon2rec.statuz, varcom.oK)) {
//					systemError005();
//				}
				
				bextIO.setBilldate(addDaysToDate(dshnIO.getInstfrom().toString().trim(),Integer.parseInt(rec.day.trim())*dishonorCount));	
			}
			//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy ends
		}else {
			bextIO.setBilldate(dshnIO.getSubdat());
		}
		bextIO.setBillchnl(dshnIO.getBillchnl());
		bextIO.setBankcode(dshnIO.getBankcode());
		bextIO.setInstfrom(dshnIO.getInstfrom());
		bextIO.setInstto(dshnIO.getInstto());
		bextIO.setInstbchnl(dshnIO.getInstbchnl());
		bextIO.setInstcchnl(dshnIO.getInstcchnl());
		bextIO.setInstfreq(dshnIO.getInstfreq());
		bextIO.setInstamt01(dshnIO.getInstamt01());
		bextIO.setInstamt02(dshnIO.getInstamt02());
		bextIO.setInstamt03(dshnIO.getInstamt03());
		bextIO.setInstamt04(dshnIO.getInstamt04());
		bextIO.setInstamt05(dshnIO.getInstamt05());
		bextIO.setInstamt06(dshnIO.getInstamt06());
		bextIO.setInstamt07(ZERO);
		bextIO.setInstamt08(ZERO);
		bextIO.setInstamt09(ZERO);
		bextIO.setInstamt10(ZERO);
		bextIO.setInstamt11(ZERO);
		bextIO.setInstamt12(ZERO);
		bextIO.setInstamt13(ZERO);
		bextIO.setInstamt14(ZERO);
		bextIO.setInstamt15(ZERO);
		bextIO.setInstjctl(dshnIO.getInstjctl());
		bextIO.setGrupkey(dshnIO.getGrupkey());
		bextIO.setMembsel(dshnIO.getMembsel());
		bextIO.setFacthous(dshnIO.getFacthous());
		bextIO.setBankkey(dshnIO.getBankkey());
		bextIO.setBankacckey(dshnIO.getBankacckey());
		bextIO.setCowncoy(dshnIO.getCowncoy());
		bextIO.setCownnum(dshnIO.getCownnum());
		bextIO.setPayrcoy(dshnIO.getPayrcoy());
		bextIO.setPayrnum(dshnIO.getPayrnum());
		bextIO.setCntbranch(dshnIO.getCntbranch());
		bextIO.setAgntpfx(dshnIO.getAgntpfx());
		bextIO.setAgntcoy(dshnIO.getAgntcoy());
		bextIO.setAgntnum(dshnIO.getAgntnum());
		bextIO.setPayflag(dshnIO.getPayflag());
		bextIO.setBilflag(dshnIO.getBilflag());
		bextIO.setOutflag(dshnIO.getOutflag());
		bextIO.setSupflag(dshnIO.getSupflag());
		//PINNACLE-2044  start
		if(isAiaAusDirectDebit) {
		bextIO.setBillcd(bextIO.getBilldate());
		}
		else
		{
		bextIO.setBillcd(dshnIO.getBillcd());
		}
		//PINNACLE-2044  end
		bextIO.setMandref(dshnIO.getMandref());
		bextIO.setSacscode(dshnIO.getSacscode());
		bextIO.setSacstyp(dshnIO.getSacstyp());
		bextIO.setGlmap(dshnIO.getGlmap());
		bextIO.setEffdatex(ZERO);
		bextIO.setDdderef(ZERO);
		bextIO.setJobno(ZERO);
		bextIO.setNextdate(dshnIO.getNextdate());
		bextIO.setFormat(formatsInner.bextrec);
		bextIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bextIO);
		if (isNE(bextIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bextIO.getParams());
			syserrrec.statuz.set(bextIO.getStatuz());
			databaseError006();
		}
		/* Call 'CONTOT' to update Control Total No.2*/
		/* (number of BEXT records written)*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}
//Ticket #PINNACLE-2044
private  String addDaysToDate(String datime,Integer dayCount) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datime.substring(6)));
	cal.set(Calendar.MONTH, Integer.parseInt(datime.substring(4,6)) - 1);
	cal.set(Calendar.YEAR, Integer.parseInt(datime.substring(0, 4)));
	cal.add(Calendar.DATE, dayCount); // Adding  days
	return sdf.format(cal.getTime());
}

protected void pageOverflow5000()
	{
		/*OVERFLOW*/
		if (isLT(wsaaLineCount, wsaaPageSize)) {
			return ;
		}
		printerRec.set(SPACES);
		printerFile.printR6282h01(r6282h01Record);
		wsaaLineCount.set(12);
		/* Call 'CONTOT' to update Control Total No.4*/
		/* (number of pages printed)*/
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData bbalrec = new FixedLengthStringData(10).init("BBALREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData dshnrec1 = new FixedLengthStringData(10).init("DSHNREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData mandrec = new FixedLengthStringData(10).init("MANDREC");
	private FixedLengthStringData ptrnrevrec = new FixedLengthStringData(10).init("PTRNREVREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData ddsurec = new FixedLengthStringData(10).init("DDSUREC");
	private FixedLengthStringData bextrec = new FixedLengthStringData(10).init("BEXTREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
}
/*
 * Class transformed  from Data Structure R6282D01-RECORD--INNER
 */
private static final class R6282d01RecordInner {

	private FixedLengthStringData r6282d01Record = new FixedLengthStringData(123);
	private FixedLengthStringData r6282d01O = new FixedLengthStringData(123).isAPartOf(r6282d01Record, 0);
	private FixedLengthStringData payrcoy = new FixedLengthStringData(1).isAPartOf(r6282d01O, 0);
	private FixedLengthStringData payrnum = new FixedLengthStringData(8).isAPartOf(r6282d01O, 1);
	private FixedLengthStringData surnameprt = new FixedLengthStringData(15).isAPartOf(r6282d01O, 9);
	private FixedLengthStringData mandref = new FixedLengthStringData(5).isAPartOf(r6282d01O, 24);
	private FixedLengthStringData billcd = new FixedLengthStringData(10).isAPartOf(r6282d01O, 29);
	private FixedLengthStringData facthous = new FixedLengthStringData(2).isAPartOf(r6282d01O, 39);
	private FixedLengthStringData bankcode = new FixedLengthStringData(2).isAPartOf(r6282d01O, 41);
	private FixedLengthStringData bankkey = new FixedLengthStringData(10).isAPartOf(r6282d01O, 43);
	private FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(r6282d01O, 53);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r6282d01O, 54);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(r6282d01O, 62);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(r6282d01O, 65);
	private ZonedDecimalData instamtprt = new ZonedDecimalData(17, 2).isAPartOf(r6282d01O, 75);
	private ZonedDecimalData jobnum = new ZonedDecimalData(8, 0).isAPartOf(r6282d01O, 92);
	private FixedLengthStringData mandstat = new FixedLengthStringData(2).isAPartOf(r6282d01O, 100);
	private FixedLengthStringData procaction = new FixedLengthStringData(1).isAPartOf(r6282d01O, 102);
	private FixedLengthStringData bankacckey = new FixedLengthStringData(20).isAPartOf(r6282d01O, 103);
}
}
