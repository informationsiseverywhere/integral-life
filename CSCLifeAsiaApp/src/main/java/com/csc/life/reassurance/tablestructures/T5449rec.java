package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:15
 * Description:
 * Copybook name: T5449REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5449rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5449Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(t5449Rec, 0);
  	public FixedLengthStringData facsh = new FixedLengthStringData(1).isAPartOf(t5449Rec, 3);
  	public FixedLengthStringData letterType = new FixedLengthStringData(8).isAPartOf(t5449Rec, 4);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(t5449Rec, 12);
  	public FixedLengthStringData prefbas = new FixedLengthStringData(4).isAPartOf(t5449Rec, 16);
  	public ZonedDecimalData qcoshr = new ZonedDecimalData(5, 2).isAPartOf(t5449Rec, 20);
  	public FixedLengthStringData qreshrs = new FixedLengthStringData(50).isAPartOf(t5449Rec, 25);
  	public ZonedDecimalData[] qreshr = ZDArrayPartOfStructure(10, 5, 2, qreshrs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(qreshrs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData qreshr01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData qreshr02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData qreshr03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData qreshr04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData qreshr05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public ZonedDecimalData qreshr06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
  	public ZonedDecimalData qreshr07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
  	public ZonedDecimalData qreshr08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData qreshr09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
  	public ZonedDecimalData qreshr10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
  	public FixedLengthStringData rasnums = new FixedLengthStringData(80).isAPartOf(t5449Rec, 75);
  	public FixedLengthStringData[] rasnum = FLSArrayPartOfStructure(10, 8, rasnums, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(rasnums, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rasnum01 = new FixedLengthStringData(8).isAPartOf(filler1, 0);
  	public FixedLengthStringData rasnum02 = new FixedLengthStringData(8).isAPartOf(filler1, 8);
  	public FixedLengthStringData rasnum03 = new FixedLengthStringData(8).isAPartOf(filler1, 16);
  	public FixedLengthStringData rasnum04 = new FixedLengthStringData(8).isAPartOf(filler1, 24);
  	public FixedLengthStringData rasnum05 = new FixedLengthStringData(8).isAPartOf(filler1, 32);
  	public FixedLengthStringData rasnum06 = new FixedLengthStringData(8).isAPartOf(filler1, 40);
  	public FixedLengthStringData rasnum07 = new FixedLengthStringData(8).isAPartOf(filler1, 48);
  	public FixedLengthStringData rasnum08 = new FixedLengthStringData(8).isAPartOf(filler1, 56);
  	public FixedLengthStringData rasnum09 = new FixedLengthStringData(8).isAPartOf(filler1, 64);
  	public FixedLengthStringData rasnum10 = new FixedLengthStringData(8).isAPartOf(filler1, 72);
  	public FixedLengthStringData relimits = new FixedLengthStringData(170).isAPartOf(t5449Rec, 155);
  	public ZonedDecimalData[] relimit = ZDArrayPartOfStructure(10, 17, 2, relimits, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(170).isAPartOf(relimits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData relimit01 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData relimit02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17);
  	public ZonedDecimalData relimit03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34);
  	public ZonedDecimalData relimit04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51);
  	public ZonedDecimalData relimit05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68);
  	public ZonedDecimalData relimit06 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 85);
  	public ZonedDecimalData relimit07 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 102);
  	public ZonedDecimalData relimit08 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 119);
  	public ZonedDecimalData relimit09 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 136);
  	public ZonedDecimalData relimit10 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 153);
  	public FixedLengthStringData retype = new FixedLengthStringData(1).isAPartOf(t5449Rec, 325);
  	public FixedLengthStringData rprmmths = new FixedLengthStringData(40).isAPartOf(t5449Rec, 326);
  	public FixedLengthStringData[] rprmmth = FLSArrayPartOfStructure(10, 4, rprmmths, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(rprmmths, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rprmmth01 = new FixedLengthStringData(4).isAPartOf(filler3, 0);
  	public FixedLengthStringData rprmmth02 = new FixedLengthStringData(4).isAPartOf(filler3, 4);
  	public FixedLengthStringData rprmmth03 = new FixedLengthStringData(4).isAPartOf(filler3, 8);
  	public FixedLengthStringData rprmmth04 = new FixedLengthStringData(4).isAPartOf(filler3, 12);
  	public FixedLengthStringData rprmmth05 = new FixedLengthStringData(4).isAPartOf(filler3, 16);
  	public FixedLengthStringData rprmmth06 = new FixedLengthStringData(4).isAPartOf(filler3, 20);
  	public FixedLengthStringData rprmmth07 = new FixedLengthStringData(4).isAPartOf(filler3, 24);
  	public FixedLengthStringData rprmmth08 = new FixedLengthStringData(4).isAPartOf(filler3, 28);
  	public FixedLengthStringData rprmmth09 = new FixedLengthStringData(4).isAPartOf(filler3, 32);
  	public FixedLengthStringData rprmmth10 = new FixedLengthStringData(4).isAPartOf(filler3, 36);
  	public FixedLengthStringData rtytyp = new FixedLengthStringData(1).isAPartOf(t5449Rec, 366);
  	public FixedLengthStringData sslivb = new FixedLengthStringData(4).isAPartOf(t5449Rec, 367);
  	public FixedLengthStringData subliv = new FixedLengthStringData(1).isAPartOf(t5449Rec, 371);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(128).isAPartOf(t5449Rec, 372, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5449Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5449Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}