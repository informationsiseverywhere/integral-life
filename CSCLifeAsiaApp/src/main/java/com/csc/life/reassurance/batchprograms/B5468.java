/*
 * File: B5468.java
 * Date: 29 August 2009 21:18:57
 * Author: Quipoz Limited
 * 
 * Class transformed from B5468.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.reports.R5468Report;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Facultative Reassurance Schedule.
*
* Overview
* ========
*
* This program is to browse the LETC files to select records
* that are of Letter Type = 'REASSFAC'.
* The Letter Type is read from T6634 with '***' +
* BPRD-SYSTEM-PARAM01 as the key.
*
* The required records will be selected by using SQL and the
* LETC records will be deleted after processing.
*
* The SQL selection criteria are as follows :
*
*        SELECT    . LETCPF.REQCOY = BSPR-COMPANY
*                  . LETTYPE = 'REASSFAC'
*
* Processing
* ==========
*
* Initialise
*     - Define the SQL query by declaring a cursor. The SQL will
*       extract all LETC records that meet the criteria specified
*       above.
*
* Edit
*     - Read the CHDRLNB logical file for the contract details.
*     - Check T5679 to check the contract status using
*       BPRD-SYSTEM-PARAM01 as the key (2700-VALIDATE-CONTR).
*
*       If the contract is valid, lookup T5688, T3623 and T3588
*       for the contract type desc, risk status desc and premium
*       status desc respectively.
*
*     - Read LIFE and CLTS files for the Life assured details.
*       For Life assured details :   LIFELNB-LIFE  = '01' and
*                                    LIFELNB-JLIFE = '00'
*
*       For Joint Life details   :   LIFELNB-LIFE  = '01' and
*                                    LIFELNB-JLIFE = '01'
*
*     - Read COVR file for the Cover details
*
*****************************************************************
* </pre>
*/
public class B5468 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlletcpf1rs = null;
	private java.sql.PreparedStatement sqlletcpf1ps = null;
	private java.sql.Connection sqlletcpf1conn = null;
	private String sqlletcpf1 = "";
	private R5468Report printerFile = new R5468Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5468");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaSqlReqcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSqlLettype = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaFirstRacd = new FixedLengthStringData(1).init("Y");
	private Validator firstRacd = new Validator(wsaaFirstRacd, "Y");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String cltsrec = "CLTSREC";
	private String letcrec = "LETCREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String lifelnbrec = "LIFELNBREC";
	private String covrlnbrec = "COVRLNBREC";
	private String rasarec = "RASAREC";
	private String racdrec = "RACDREC";
		/* ERRORS */
	private String esql = "ESQL";
	private String t5679 = "T5679";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String tr384 = "TR384";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-LETCPF */
	private FixedLengthStringData sqlLetcrec = new FixedLengthStringData(132);
	private FixedLengthStringData sqlReqcoy = new FixedLengthStringData(1).isAPartOf(sqlLetcrec, 0);
	private FixedLengthStringData sqlLettype = new FixedLengthStringData(8).isAPartOf(sqlLetcrec, 1);
	private FixedLengthStringData sqlClntcoy = new FixedLengthStringData(1).isAPartOf(sqlLetcrec, 9);
	private FixedLengthStringData sqlClntnum = new FixedLengthStringData(8).isAPartOf(sqlLetcrec, 10);
	private PackedDecimalData sqlLetseqno = new PackedDecimalData(7, 0).isAPartOf(sqlLetcrec, 18);
	private FixedLengthStringData sqlLetokey = new FixedLengthStringData(100).isAPartOf(sqlLetcrec, 22);
	private FixedLengthStringData sqlRdoccoy = new FixedLengthStringData(1).isAPartOf(sqlLetcrec, 122);
	private FixedLengthStringData sqlRdocnum = new FixedLengthStringData(9).isAPartOf(sqlLetcrec, 123);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
		/* CONTROL-TOTALS */
	private int ct01 = 1;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaJointLife = new FixedLengthStringData(1).init("Y");
	private Validator jointLife = new Validator(wsaaJointLife, "Y");

	private FixedLengthStringData wsaaRacdKey = new FixedLengthStringData(74);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaRacdKey, 0);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).isAPartOf(wsaaRacdKey, 2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRacdKey, 4);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaRacdKey, 6);
	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(4).isAPartOf(wsaaRacdKey, 8);
	private FixedLengthStringData wsaaSeqno = new FixedLengthStringData(2).isAPartOf(wsaaRacdKey, 12);

	private FixedLengthStringData r5468H01 = new FixedLengthStringData(116);
	private FixedLengthStringData r5468h01O = new FixedLengthStringData(116).isAPartOf(r5468H01, 0);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5468h01O, 0);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37).isAPartOf(r5468h01O, 8);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5468h01O, 45);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(r5468h01O, 53);
	private FixedLengthStringData chdrdesc = new FixedLengthStringData(30).isAPartOf(r5468h01O, 56);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(r5468h01O, 86);
	private FixedLengthStringData rstatdsc = new FixedLengthStringData(10).isAPartOf(r5468h01O, 96);
	private FixedLengthStringData pstatdsc = new FixedLengthStringData(10).isAPartOf(r5468h01O, 106);

	private FixedLengthStringData r5468D01 = new FixedLengthStringData(59);
	private FixedLengthStringData r5468d01O = new FixedLengthStringData(59).isAPartOf(r5468D01, 0);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(r5468d01O, 0);
	private FixedLengthStringData clntnm1 = new FixedLengthStringData(37).isAPartOf(r5468d01O, 8);
	private FixedLengthStringData cltsex = new FixedLengthStringData(1).isAPartOf(r5468d01O, 45);
	private FixedLengthStringData cltdob = new FixedLengthStringData(10).isAPartOf(r5468d01O, 46);
	private ZonedDecimalData anbccd = new ZonedDecimalData(3, 0).isAPartOf(r5468d01O, 56);

	private FixedLengthStringData r5468D02 = new FixedLengthStringData(59);
	private FixedLengthStringData r5468d02O = new FixedLengthStringData(59).isAPartOf(r5468D02, 0);
	private FixedLengthStringData clntnum1 = new FixedLengthStringData(8).isAPartOf(r5468d02O, 0);
	private FixedLengthStringData clntnm2 = new FixedLengthStringData(37).isAPartOf(r5468d02O, 8);
	private FixedLengthStringData cltsex1 = new FixedLengthStringData(1).isAPartOf(r5468d02O, 45);
	private FixedLengthStringData cltdob1 = new FixedLengthStringData(10).isAPartOf(r5468d02O, 46);
	private ZonedDecimalData anbccd1 = new ZonedDecimalData(3, 0).isAPartOf(r5468d02O, 56);

	private FixedLengthStringData r5468D03 = new FixedLengthStringData(4);
	private FixedLengthStringData r5468d03O = new FixedLengthStringData(4).isAPartOf(r5468D03, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5468d03O, 0);

	private FixedLengthStringData r5468D04 = new FixedLengthStringData(87);
	private FixedLengthStringData r5468d04O = new FixedLengthStringData(87).isAPartOf(r5468D04, 0);
	private FixedLengthStringData crtable1 = new FixedLengthStringData(4).isAPartOf(r5468d04O, 0);
	private FixedLengthStringData crtabdesc = new FixedLengthStringData(30).isAPartOf(r5468d04O, 4);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0).isAPartOf(r5468d04O, 34);
	private FixedLengthStringData sicurr = new FixedLengthStringData(3).isAPartOf(r5468d04O, 37);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(r5468d04O, 40);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(r5468d04O, 57);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5468d04O, 60);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5468d04O, 77);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Schedule Definition - Multi Thread Batch*/
	private BscdTableDAM bscdIO = new BscdTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Reassurance Cession Details Logical*/
	private RacdTableDAM racdIO = new RacdTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Tr384rec tr384rec = new Tr384rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		eof2060, 
		exit2090, 
		delete2580, 
		contractHeader2620, 
		lifeAssured2630, 
		jointLife2670, 
		exit2690
	}

	public B5468() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaOverflow.set("N");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("2");
		itemIO.setItemtabl(tr384);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("***");
		stringVariable1.append(bprdIO.getSystemParam01().toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		wsaaSqlLettype.set(tr384rec.letterType);
		wsaaSqlReqcoy.set(bsprIO.getCompany());
		sqlletcpf1 = " SELECT  LETCPF.REQCOY, LETCPF.LETTYPE, LETCPF.CLNTCOY, LETCPF.CLNTNUM, LETCPF.LETSEQNO, LETCPF.LETOKEY, LETCPF.RDOCCOY, LETCPF.RDOCNUM" +
" FROM   " + appVars.getTableNameOverriden("LETCPF") + " " +
" WHERE LETCPF.REQCOY = ?" +
" AND LETCPF.LETTYPE = ?" +
" ORDER BY LETCPF.REQCOY, LETCPF.LETTYPE, LETCPF.CLNTCOY, LETCPF.CLNTNUM, LETCPF.LETSEQNO";
		sqlerrorflag = false;
		try {
			sqlletcpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.fsu.general.dataaccess.LetcpfTableDAM());
			sqlletcpf1ps = appVars.prepareStatementEmbeded(sqlletcpf1conn, sqlletcpf1, "LETCPF");
			appVars.setDBString(sqlletcpf1ps, 1, wsaaSqlReqcoy.toString());
			appVars.setDBString(sqlletcpf1ps, 2, wsaaSqlLettype.toString());
			sqlletcpf1rs = appVars.executeQuery(sqlletcpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		wsaaRasnum.set(SPACES);
		wsaaPrevChdrnum.set(SPACES);
		wsaaClntnum.set(SPACES);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2060: {
					eof2060();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlletcpf1rs.next()) {
				appVars.getDBObject(sqlletcpf1rs, 1, sqlReqcoy);
				appVars.getDBObject(sqlletcpf1rs, 2, sqlLettype);
				appVars.getDBObject(sqlletcpf1rs, 3, sqlClntcoy);
				appVars.getDBObject(sqlletcpf1rs, 4, sqlClntnum);
				appVars.getDBObject(sqlletcpf1rs, 5, sqlLetseqno);
				appVars.getDBObject(sqlletcpf1rs, 6, sqlLetokey);
				appVars.getDBObject(sqlletcpf1rs, 7, sqlRdoccoy);
				appVars.getDBObject(sqlletcpf1rs, 8, sqlRdocnum);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					edit2510();
				}
				case delete2580: {
					delete2580();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		wsaaJointLife.set("Y");
		wsaaRacdKey.set(sqlLetokey);
		racdIO.setParams(SPACES);
		racdIO.setChdrcoy(sqlRdoccoy);
		racdIO.setChdrnum(sqlRdocnum);
		racdIO.setLife(wsaaLife);
		racdIO.setCoverage(wsaaCoverage);
		racdIO.setRider(wsaaRider);
		racdIO.setPlanSuffix(wsaaPlanSuffix);
		racdIO.setSeqno(wsaaSeqno);
		racdIO.setFunction(varcom.readr);
		racdIO.setFormat(racdrec);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(),varcom.oK)
		&& isNE(racdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(racdIO.getParams());
			fatalError600();
		}
		if (isNE(racdIO.getStatuz(),varcom.oK)) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.delete2580);
		}
		if (isNE(racdIO.getRasnum(),wsaaRasnum)
		|| isNE(racdIO.getChdrnum(),wsaaPrevChdrnum)
		|| isNE(sqlClntnum,wsaaClntnum)) {
			printHeader2600();
			wsaaRasnum.set(racdIO.getRasnum());
			wsaaPrevChdrnum.set(racdIO.getChdrnum());
			wsaaClntnum.set(sqlClntnum);
		}
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.delete2580);
		}
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrlnbIO.setLife(racdIO.getLife());
		covrlnbIO.setCoverage(racdIO.getCoverage());
		covrlnbIO.setRider(racdIO.getRider());
		covrlnbIO.setPlanSuffix(racdIO.getPlanSuffix());
		covrlnbIO.setFunction(varcom.readr);
		covrlnbIO.setFormat(covrlnbrec);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		crtable1.set(covrlnbIO.getCrtable());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrlnbIO.getCrtable());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		crtabdesc.set(descIO.getLongdesc());
		rcestrm.set(covrlnbIO.getRiskCessTerm());
		sicurr.set(chdrlnbIO.getCntcurr());
		sumins.set(covrlnbIO.getSumins());
		currcode.set(racdIO.getCurrcode());
		raamount.set(racdIO.getRaAmount());
		datcon1rec.intDate.set(racdIO.getCmdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		cmdate.set(datcon1rec.extDate);
		printerFile.printR5468d04(r5468D04);
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void delete2580()
	{
		deleteLetc2900();
		/*EXIT*/
	}

protected void printHeader2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					printHeader2610();
				}
				case contractHeader2620: {
					contractHeader2620();
				}
				case lifeAssured2630: {
					lifeAssured2630();
				}
				case jointLife2670: {
					jointLife2670();
					print2680();
				}
				case exit2690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void printHeader2610()
	{
		if (isEQ(racdIO.getRasnum(),wsaaRasnum)) {
			goTo(GotoLabel.contractHeader2620);
		}
		rasnum.set(racdIO.getRasnum());
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(bsprIO.getCompany());
		rasaIO.setRasnum(racdIO.getRasnum());
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		clntnm.set(wsspLongconfname);
	}

protected void contractHeader2620()
	{
		if (isEQ(racdIO.getChdrnum(),wsaaPrevChdrnum)) {
			goTo(GotoLabel.lifeAssured2630);
		}
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(racdIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		validateContr2700();
		if (validContract.isTrue()) {
			getChdrDesc2800();
		}
		else {
			goTo(GotoLabel.exit2690);
		}
	}

protected void lifeAssured2630()
	{
		if (isEQ(sqlClntnum,wsaaClntnum)) {
			goTo(GotoLabel.jointLife2670);
		}
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(bsprIO.getCompany());
		lifelnbIO.setChdrnum(racdIO.getChdrnum());
		lifelnbIO.setLife(racdIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		clntnum.set(lifelnbIO.getLifcnum());
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		cltsex.set(cltsIO.getCltsex());
		datcon1rec.intDate.set(cltsIO.getCltdob());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		cltdob.set(datcon1rec.extDate);
		anbccd.set(lifelnbIO.getAnbAtCcd());
		plainname();
		clntnm1.set(wsspLongconfname);
	}

protected void jointLife2670()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(bsprIO.getCompany());
		lifelnbIO.setChdrnum(racdIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.mrnf)
		&& isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			wsaaJointLife.set("N");
		}
		if (jointLife.isTrue()) {
			clntnum1.set(lifelnbIO.getLifcnum());
			cltsIO.setParams(SPACES);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(bsprIO.getFsuco());
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			cltsIO.setFormat(cltsrec);
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			cltsex1.set(cltsIO.getCltsex());
			datcon1rec.intDate.set(cltsIO.getCltdob());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			cltdob1.set(datcon1rec.extDate);
			anbccd1.set(lifelnbIO.getAnbAtCcd());
			plainname();
			clntnm2.set(wsspLongconfname);
		}
	}

protected void print2680()
	{
		printerFile.printR5468h01(r5468H01);
		printerFile.printR5468d01(r5468D01);
		if (jointLife.isTrue()) {
			printerFile.printR5468d02(r5468D02);
		}
		printerFile.printR5468d03(r5468D03);
	}

protected void validateContr2700()
	{
		startValidation2710();
	}

protected void startValidation2710()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()],chdrlnbIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()],chdrlnbIO.getPstatcode())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}

protected void getChdrDesc2800()
	{
		getChdrDesc2810();
	}

protected void getChdrDesc2810()
	{
		cnttype.set(chdrlnbIO.getCnttype());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		chdrdesc.set(descIO.getLongdesc());
		datcon1rec.intDate.set(chdrlnbIO.getOccdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		occdate.set(datcon1rec.extDate);
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rstatdsc.set(descIO.getShortdesc());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		pstatdsc.set(descIO.getShortdesc());
		chdrnum.set(racdIO.getChdrnum());
	}

protected void deleteLetc2900()
	{
		deleteLetcRec2910();
	}

protected void deleteLetcRec2910()
	{
		letcIO.setParams(SPACES);
		letcIO.setRequestCompany(sqlReqcoy);
		letcIO.setLetterType(wsaaSqlLettype);
		letcIO.setClntcoy(sqlClntcoy);
		letcIO.setClntnum(sqlClntnum);
		letcIO.setLetterSeqno(sqlLetseqno);
		letcIO.setFunction(varcom.readh);
		letcIO.setFormat(letcrec);
		SmartFileCode.execute(appVars, letcIO);
		if (isNE(letcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(letcIO.getParams());
			fatalError600();
		}
		letcIO.setFunction(varcom.delet);
		letcIO.setFormat(letcrec);
		SmartFileCode.execute(appVars, letcIO);
		if (isNE(letcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(letcIO.getParams());
			fatalError600();
		}
	}

protected void update3000()
	{
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlletcpf1conn, sqlletcpf1ps, sqlletcpf1rs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
