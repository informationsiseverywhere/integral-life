/*
 * File: Trmracd.java
 * Date: 30 August 2009 2:45:51
 * Author: Quipoz Limited
 * 
 * Class transformed from TRMRACD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdrskTableDAM;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Termination of Reassurance Cessions Subroutine.
*
* Overview
* ========
*
* This subroutine will carry out the termination of existing
* reassurance cession (RACD) records. It performs the following
* functions:
*
* 1. Validflag '2' the RACD record in question.
*
* 2. Call REXPUPD to update the life reassurance experience
*    history details.
*
* 3. For a function of 'CHGR', write a new validflag '3' record
*    with the new reassurance amount.
*
* 4. For a function of 'TRMR', write a new validflag '4' record
*    to indicate a status of terminated for this record.
*
* Linkage Area
* ============
*
* FUNCTION       PIC X(05)
* STATUZ         PIC X(04)
* CHDRCOY        PIC X(01).
* CHDRNUM        PIC X(08).
* LIFE           PIC X(02).
* COVERAGE       PIC X(02).
* RIDER          PIC X(02).
* PLAN-SUFFIX    PIC S9(04) COMP-3.
* TRANNO         PIC S9(05) COMP-3.
* CNTTYPE        PIC X(03).
* CRTABLE        PIC X(04).
* SEQNO          PIC S9(02) COMP-3.
* CLNTCOY        PIC X(01).
* L1-CLNTNUM     PIC X(08).
* JLIFE          PIC X(02).
* L2-CLNTNUM     PIC X(08).
* EFFDATE        PIC S9(08) COMP-3.
* NEW-RA-AMT     PIC S9(11)V9(02) COMP-3.
* RECOVAMT       PIC S9(11)V9(02) COMP-3.
*
* These fields are all contained within TRMRACDREC.
*
*****************************************************************
* </pre>
*/
public class Trmracd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "TRMRACD";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
		/* FORMATS */
	private static final String itdmrec = "ITEMREC";
	private static final String racdrec = "RACDREC";
	private static final String racdrskrec = "RACDRSKREC";
		/* TABLES */
	private static final String t5446 = "T5446";
	private static final String t5448 = "T5448";
		/* ERRORS */
	private static final String e049 = "E049";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private RacdTableDAM racdIO = new RacdTableDAM();
	private RacdrskTableDAM racdrskIO = new RacdrskTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Trmracdrec trmracdrec = new Trmracdrec();

	public Trmracd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		trmracdrec.trmracdRec = convertAndSetParam(trmracdrec.trmracdRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		trmracdrec.statuz.set(varcom.oK);
		if (isNE(trmracdrec.function, "TRMR")
		&& isNE(trmracdrec.function, "CHGR")) {
			syserrrec.statuz.set(e049);
			syserr570();
		}
		readT54481000();
		/* Use RACDRSK logical file (LRKCLS as key).               <V4L027>*/
		/*                                                         <V4L027>*/
		/*    PERFORM 2000-TERMINATE-RACD.                                 */
		terminateRacdrsk2000x();
		updateReassurerExposure3000();
		if (isGT(trmracdrec.newRaAmt, 0)
		&& isEQ(trmracdrec.function, "CHGR")) {
			/*        PERFORM 4000-WRITE-VALIDFLAG3-RACD                       */
			writeValidflag3Racdrsk4000x();
		}
		if (isEQ(trmracdrec.function, "TRMR")) {
			/*        PERFORM 5000-WRITE-VALIDFLAG4-RACD                       */
			writeValidflag4Racdrsk5000x();
		}
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void readT54481000()
	{
		t54481010();
	}

protected void t54481010()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(trmracdrec.chdrcoy);
		itdmIO.setItemtabl(t5448);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(trmracdrec.cnttype);
		stringVariable1.addExpression(trmracdrec.crtable);
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setItemitem(wsaaT5448Item);
		/* MOVE TRMR-EFFDATE           TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(trmracdrec.crrcd);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(itdmIO.getItemitem(), wsaaT5448Item)
		|| isNE(itdmIO.getItemcoy(), trmracdrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5448)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(trmracdrec.chdrcoy);
			itdmIO.setItemtabl(t5448);
			itdmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
	}

protected void terminateRacd2000()
	{
		terminate2010();
	}

protected void terminate2010()
	{
		racdIO.setParams(SPACES);
		racdIO.setChdrcoy(trmracdrec.chdrcoy);
		racdIO.setChdrnum(trmracdrec.chdrnum);
		racdIO.setLife(trmracdrec.life);
		racdIO.setCoverage(trmracdrec.coverage);
		racdIO.setRider(trmracdrec.rider);
		racdIO.setPlanSuffix(trmracdrec.planSuffix);
		racdIO.setSeqno(trmracdrec.seqno);
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
		racdIO.setValidflag("2");
		racdIO.setCurrto(trmracdrec.effdate);
		racdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
	}

protected void terminateRacdrsk2000x()
	{
		terminate2010x();
	}

	/**
	* <pre>
	*********************************                         <V4L027>
	* </pre>
	*/
protected void terminate2010x()
	{
		racdrskIO.setParams(SPACES);
		racdrskIO.setChdrcoy(trmracdrec.chdrcoy);
		racdrskIO.setChdrnum(trmracdrec.chdrnum);
		racdrskIO.setLife(trmracdrec.life);
		racdrskIO.setCoverage(trmracdrec.coverage);
		racdrskIO.setRider(trmracdrec.rider);
		racdrskIO.setPlanSuffix(trmracdrec.planSuffix);
		racdrskIO.setSeqno(trmracdrec.seqno);
		racdrskIO.setLrkcls(trmracdrec.lrkcls);
		racdrskIO.setFormat(racdrskrec);
		racdrskIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, racdrskIO);
		if (isNE(racdrskIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdrskIO.getParams());
			dbError580();
		}
		racdrskIO.setValidflag("2");
		racdrskIO.setCurrto(trmracdrec.effdate);
		racdrskIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, racdrskIO);
		if (isNE(racdrskIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdrskIO.getParams());
			dbError580();
		}
	}

protected void updateReassurerExposure3000()
	{
		update3010();
	}

protected void update3010()
	{
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(trmracdrec.chdrcoy);
		rexpupdrec.chdrnum.set(trmracdrec.chdrnum);
		rexpupdrec.life.set(trmracdrec.life);
		rexpupdrec.coverage.set(trmracdrec.coverage);
		rexpupdrec.rider.set(trmracdrec.rider);
		rexpupdrec.planSuffix.set(trmracdrec.planSuffix);
		rexpupdrec.tranno.set(trmracdrec.tranno);
		rexpupdrec.clntcoy.set(trmracdrec.clntcoy);
		rexpupdrec.l1Clntnum.set(trmracdrec.l1Clntnum);
		if (isNE(trmracdrec.jlife, "00")) {
			rexpupdrec.l2Clntnum.set(trmracdrec.l2Clntnum);
		}
		/*    MOVE RACD-RASNUM            TO RXUP-REASSURER.               */
		/*    MOVE RACD-RNGMNT            TO RXUP-ARRANGEMENT.             */
		/*    MOVE RACD-RA-AMOUNT         TO RXUP-SUMINS.                  */
		/*    MOVE RACD-CESTYPE           TO RXUP-CESTYPE.                 */
		rexpupdrec.reassurer.set(racdrskIO.getRasnum());
		rexpupdrec.arrangement.set(racdrskIO.getRngmnt());
		rexpupdrec.sumins.set(racdrskIO.getRaAmount());
		rexpupdrec.cestype.set(racdrskIO.getCestype());
		/* MOVE RACD-CURRCODE          TO RXUP-CURRENCY.                */
		rexpupdrec.effdate.set(trmracdrec.effdate);
		/*    MOVE T5448-LRKCLS           TO RXUP-RISK-CLASS.              */
		rexpupdrec.riskClass.set(racdrskIO.getLrkcls());
		readT54463100();
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.batctrcde.set(trmracdrec.batctrcde);
		/*    IF RXUP-CURRENCY            NOT = RACD-CURRCODE      <V4L027>*/
		if (isNE(rexpupdrec.currency, racdrskIO.getCurrcode())) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			/*     MOVE RACD-CURRCODE      TO CLNK-CURR-IN             <001>*/
			conlinkrec.currIn.set(racdrskIO.getCurrcode());
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt3200();
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		rexpupdrec.function.set("DECR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz, varcom.oK)) {
			syserrrec.params.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			syserr570();
		}
	}

protected void readT54463100()
	{
		t54463110();
	}

protected void t54463110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(trmracdrec.chdrcoy);
		itdmIO.setItemtabl(t5446);
		/*    MOVE T5448-LRKCLS           TO ITDM-ITEMITEM.        <V4L027>*/
		itdmIO.setItemitem(racdrskIO.getLrkcls());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		/*    IF ITDM-ITEMITEM            NOT = T5448-LRKCLS       <V4L027>*/
		if (isNE(itdmIO.getItemitem(), racdrskIO.getLrkcls())
		|| isNE(itdmIO.getItemcoy(), trmracdrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5446)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*        MOVE T5448-LRKCLS       TO ITDM-ITEMITEM         <V4L027>*/
			itdmIO.setItemitem(racdrskIO.getLrkcls());
			itdmIO.setItemcoy(trmracdrec.chdrcoy);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callXcvrt3200()
	{
		call3210();
	}

protected void call3210()
	{
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(trmracdrec.effdate);
		conlinkrec.company.set(trmracdrec.chdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void writeValidflag3Racd4000()
	{
		racd4010();
	}

protected void racd4010()
	{
		racdIO.setRaAmount(trmracdrec.newRaAmt);
		racdIO.setValidflag("3");
		racdIO.setCurrfrom(trmracdrec.effdate);
		racdIO.setCurrto(varcom.vrcmMaxDate);
		racdIO.setTranno(trmracdrec.tranno);
		racdIO.setReasper(ZERO);
		racdIO.setOvrdind(SPACES);
		racdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
	}

protected void writeValidflag3Racdrsk4000x()
	{
		racdrsk4010x();
	}

	/**
	* <pre>
	****************************************                  <V4L027>
	* </pre>
	*/
protected void racdrsk4010x()
	{
		racdrskIO.setRaAmount(trmracdrec.newRaAmt);
		racdrskIO.setValidflag("3");
		racdrskIO.setCurrfrom(trmracdrec.effdate);
		racdrskIO.setCurrto(varcom.vrcmMaxDate);
		racdrskIO.setTranno(trmracdrec.tranno);
		racdrskIO.setReasper(ZERO);
		racdrskIO.setOvrdind(SPACES);
		racdrskIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdrskIO);
		if (isNE(racdrskIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdrskIO.getParams());
			dbError580();
		}
	}

protected void writeValidflag4Racd5000()
	{
		/*RACD*/
		racdIO.setRecovamt(trmracdrec.recovamt);
		racdIO.setValidflag("4");
		racdIO.setCurrfrom(trmracdrec.effdate);
		racdIO.setCurrto(varcom.vrcmMaxDate);
		racdIO.setTranno(trmracdrec.tranno);
		racdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void writeValidflag4Racdrsk5000x()
	{
		/*X-RACDRSK*/
		racdrskIO.setRecovamt(trmracdrec.recovamt);
		racdrskIO.setValidflag("4");
		racdrskIO.setCurrfrom(trmracdrec.effdate);
		racdrskIO.setCurrto(varcom.vrcmMaxDate);
		racdrskIO.setTranno(trmracdrec.tranno);
		racdrskIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdrskIO);
		if (isNE(racdrskIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdrskIO.getParams());
			dbError580();
		}
		/*EXIT1*/
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		trmracdrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		trmracdrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(trmracdrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rexpupdrec.currency);
		zrdecplrec.batctrcde.set(trmracdrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A000-EXIT*/
	}
}
