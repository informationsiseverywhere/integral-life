package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:26
 * Description:
 * Copybook name: RASALTRREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rasaltrrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rasaltrRec = new FixedLengthStringData(110);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rasaltrRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rasaltrRec, 5);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rasaltrRec, 6);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rasaltrRec, 14);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(rasaltrRec, 16);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rasaltrRec, 18);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rasaltrRec, 20);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(rasaltrRec, 22);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(rasaltrRec, 25);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rasaltrRec, 28);
  	public FixedLengthStringData batckey = new FixedLengthStringData(22).isAPartOf(rasaltrRec, 33);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rasaltrRec, 55);
  	public FixedLengthStringData l1Clntnum = new FixedLengthStringData(8).isAPartOf(rasaltrRec, 58);
  	public FixedLengthStringData l2Clntnum = new FixedLengthStringData(8).isAPartOf(rasaltrRec, 66);
  	public FixedLengthStringData fsuco = new FixedLengthStringData(1).isAPartOf(rasaltrRec, 74);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rasaltrRec, 75);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rasaltrRec, 78);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rasaltrRec, 81);
  	public PackedDecimalData oldSumins = new PackedDecimalData(13, 2).isAPartOf(rasaltrRec, 85);
  	public PackedDecimalData newSumins = new PackedDecimalData(13, 2).isAPartOf(rasaltrRec, 92);
  	public FixedLengthStringData addNewCovr = new FixedLengthStringData(1).isAPartOf(rasaltrRec, 99);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rasaltrRec, 100);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(rasaltrRec, 101);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rasaltrRec, 106);


	public void initialize() {
		COBOLFunctions.initialize(rasaltrRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rasaltrRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}