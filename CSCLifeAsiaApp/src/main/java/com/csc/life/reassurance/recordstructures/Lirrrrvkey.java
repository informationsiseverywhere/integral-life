package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:14
 * Description:
 * Copybook name: LIRRRRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lirrrrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lirrrrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lirrrrvKey = new FixedLengthStringData(64).isAPartOf(lirrrrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData lirrrrvCompany = new FixedLengthStringData(1).isAPartOf(lirrrrvKey, 0);
  	public FixedLengthStringData lirrrrvChdrnum = new FixedLengthStringData(8).isAPartOf(lirrrrvKey, 1);
  	public FixedLengthStringData lirrrrvLife = new FixedLengthStringData(2).isAPartOf(lirrrrvKey, 9);
  	public FixedLengthStringData lirrrrvCoverage = new FixedLengthStringData(2).isAPartOf(lirrrrvKey, 11);
  	public FixedLengthStringData lirrrrvRider = new FixedLengthStringData(2).isAPartOf(lirrrrvKey, 13);
  	public PackedDecimalData lirrrrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lirrrrvKey, 15);
  	public FixedLengthStringData lirrrrvRasnum = new FixedLengthStringData(8).isAPartOf(lirrrrvKey, 18);
  	public FixedLengthStringData lirrrrvRngmnt = new FixedLengthStringData(4).isAPartOf(lirrrrvKey, 26);
  	public FixedLengthStringData lirrrrvClntpfx = new FixedLengthStringData(2).isAPartOf(lirrrrvKey, 30);
  	public FixedLengthStringData lirrrrvClntcoy = new FixedLengthStringData(1).isAPartOf(lirrrrvKey, 32);
  	public FixedLengthStringData lirrrrvClntnum = new FixedLengthStringData(8).isAPartOf(lirrrrvKey, 33);
  	public PackedDecimalData lirrrrvTranno = new PackedDecimalData(5, 0).isAPartOf(lirrrrvKey, 41);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(lirrrrvKey, 44, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lirrrrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lirrrrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}