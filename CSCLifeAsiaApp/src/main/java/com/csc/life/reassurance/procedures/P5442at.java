/*
 * File: P5442at.java
 * Date: 30 August 2009 0:26:51
 * Author: Quipoz Limited
 *
 * Class transformed from P5442AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Cession Maintenance AT Update.
*
* Overview
* ========
*
* This program will be run under AT and will perform the
* general functions for the finalisation of Cession Maintenance.
*
* The general processing will carry out the following
* functions for all RACDs attached to each Coverage on
* the contract.
*
* 1. Increment the TRANNO on the Contract Header.
*
* 2. Invalid current contract header & write a new one
*    effective date , transaction number.
*
* 3. Write a PTRN record to mark the transaction.
*
* 4. Unlock the Soft-Locked Contract.
*
*****************************************************************
* </pre>
*/
public class P5442at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("P5442AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 17);
	private FixedLengthStringData filler1 = new FixedLengthStringData(178).isAPartOf(wsaaTransArea, 22, FILLER).init(SPACES);
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String ptrnrec = "PTRNREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Atmodrec atmodrec = new Atmodrec();

	public P5442at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		wsaaTransArea.set(atmodrec.transArea);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaBatckey.set(atmodrec.batchKey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		contractHeader1000();
		writePtrn2000();
		updateBatchHeader3000();
		releaseSftlck4000();
		/*EXIT*/
		exitProgram();
	}

protected void contractHeader1000()
	{
		chdr1010();
	}

protected void chdr1010()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(wsaaEffdate);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrfrom(wsaaEffdate);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
	}

protected void writePtrn2000()
	{
		ptrn2010();
	}

protected void ptrn2010()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaEffdate);
		/* MOVE WSAA-EFFDATE           TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(atmodrec.company);
		ptrnIO.setChdrnum(wsaaPrimaryChdrnum);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

protected void updateBatchHeader3000()
	{
		/*BATCH*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void releaseSftlck4000()
	{
		rlse4010();
	}

protected void rlse4010()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(wsaaPrimaryChdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
