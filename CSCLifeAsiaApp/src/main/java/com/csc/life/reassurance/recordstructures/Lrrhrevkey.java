package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:28
 * Description:
 * Copybook name: LRRHREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lrrhrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lrrhrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lrrhrevKey = new FixedLengthStringData(64).isAPartOf(lrrhrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData lrrhrevClntpfx = new FixedLengthStringData(2).isAPartOf(lrrhrevKey, 0);
  	public FixedLengthStringData lrrhrevClntcoy = new FixedLengthStringData(1).isAPartOf(lrrhrevKey, 2);
  	public FixedLengthStringData lrrhrevClntnum = new FixedLengthStringData(8).isAPartOf(lrrhrevKey, 3);
  	public FixedLengthStringData lrrhrevCompany = new FixedLengthStringData(1).isAPartOf(lrrhrevKey, 11);
  	public FixedLengthStringData lrrhrevChdrnum = new FixedLengthStringData(8).isAPartOf(lrrhrevKey, 12);
  	public FixedLengthStringData lrrhrevLife = new FixedLengthStringData(2).isAPartOf(lrrhrevKey, 20);
  	public FixedLengthStringData lrrhrevCoverage = new FixedLengthStringData(2).isAPartOf(lrrhrevKey, 22);
  	public FixedLengthStringData lrrhrevRider = new FixedLengthStringData(2).isAPartOf(lrrhrevKey, 24);
  	public PackedDecimalData lrrhrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lrrhrevKey, 26);
  	public FixedLengthStringData lrrhrevLrkcls = new FixedLengthStringData(4).isAPartOf(lrrhrevKey, 29);
  	public PackedDecimalData lrrhrevTranno = new PackedDecimalData(5, 0).isAPartOf(lrrhrevKey, 33);
  	public FixedLengthStringData filler = new FixedLengthStringData(28).isAPartOf(lrrhrevKey, 36, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lrrhrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lrrhrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}