/*
 * File: P5433.java
 * Date: 30 August 2009 0:25:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P5433.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.screens.S5433ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5433 - Reassurance Costing Submenu.
*
* This program forms part of the Reassurance Development.
* It is the Sub-menu for the Reassurance Costing Enquiry
* Suite.
*
* 1000-INITIALISE.
*
* Initialise screen.
*
* 2000-SCREEN-EDIT.
*
* Display the screen.
* Validate all fields including batch request if required.
* If errors redisplay screen.
*
* 3000-UPDATE.
*
* Update the WSSP storage area.
* If  a valid contract header record found then perform a KEEPS
* on  the  record  in  order  to store it for use in the select
* programs.
*
* 4000-WHERE-NEXT.
*
* Store the next 4 programs.
* Move 1 to the program pointer.
*
*****************************************************************
* </pre>
*/
public class P5433 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5433");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaStatFlag = new FixedLengthStringData(1);
	private Validator wsaaExitStat = new Validator(wsaaStatFlag, "Y");
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e455 = "E455";
	private static final String e544 = "E544";
	private static final String e767 = "E767";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String itemrec = "ITEMREC   ";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S5433ScreenVars sv = ScreenProgram.getScreenVars( S5433ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2590
	}

	public P5433() {
		super();
		screenVars = sv;
		new ScreenModel("S5433", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		wsaaStatFlag.set("N");
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2500();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
			validateKey12210();
		}

protected void validateKey12210()
	{
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrenqIO);
		}
		else {
			chdrenqIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(e544);
			wsspcomn.edterror.set("Y");
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		/* Release any previously kept COVRENQ record.*/
		covrenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)) {
			checkStatus2300();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)
		&& isNE(wsspcomn.branch,chdrenqIO.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkStatus2300()
	{
		readStatusTable2310();
	}

protected void readStatusTable2310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		wsaaStatFlag.set("N");
		while ( !(wsaaExitStat.isTrue())) {
			lookForStat2400();
		}
		
	}

protected void lookForStat2400()
	{
		/*SEARCH*/
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.chdrselErr.set(e767);
			wsaaStatFlag.set("Y");
		}
		else {
			if (isNE(t5679rec.cnRiskStat[wsaaSub.toInt()],chdrenqIO.getStatcode())) {
				wsaaStatFlag.set("N");
			}
			else {
				wsaaStatFlag.set("Y");
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2500()
	{
		try {
			validateRequest2510();
			retrieveBatchProgs2520();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2510()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit2590);
		}
	}

protected void retrieveBatchProgs2520()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		updateWssp3010();
		keeps3070();
	}

protected void updateWssp3010()
	{
		wsspcomn.flag.set("I");
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
	}

protected void keeps3070()
	{
		chdrenqIO.setFunction("KEEPS");
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}
}
