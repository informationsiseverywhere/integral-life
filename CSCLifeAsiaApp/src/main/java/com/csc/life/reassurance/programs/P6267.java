/*
 * File: P6267.java
 * Date: 30 August 2009 0:41:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P6267.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.AnntmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.RactTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S6267ScreenVars;
import com.csc.life.reassurance.tablestructures.T6662rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*
*  P6267 - Component Change Reassurance.
*  -------------------------------------
*
*
*  Overview.
*  ---------
*
*  Reassurance  Coverage details will be defined using coverage
*  codes similar to the ordinary Coverage/Rider codes  used  to
*  define  LIFE  contracts.  Each one of these will be assigned
*  to Coverages or Riders that already exist  on  the  contract
*  and  will  relate  to  the selected component on a plan-wide
*  basis, i.e. if a Reassurance Code is assigned to Coverage #1
*  on  a  plan of 10 policies it will apply to each Coverage #1
*  on  each  of  the  10  policies.  Therefore  as  Reassurance
*  operates  on  a Sum Assured basis the amount to be reassured
*  will be a fraction of the total of all the Sums  Assured  on
*  all 10 of the occurrences of Coverage #1.
*
*  When  reassurance  is  specified  for  any  component  on  a
*  contract the  system  will  create  one  reassurance  detail
*  record  on RACT for each component for which reassurance has
*  been specified.
*
*  No extra processing is required at the issue  stage  as  the
*  Reassurance  records,  unlike the coverage records, will not
*  be written to transaction records for later  conversion  but
*  will be written directly to the correct data-sets.
*
*  In  Component  Change  the  system  must  allow for existing
*  Reassurance details to be modified or for reassurance to  be
*  added to a contract that previously had none.
*
*  During  Component  Change  the  user  may  have  amended the
*  existing Coverage/Rider record, or added  a  new  component.
*  In  either  case  the  Component  Change  screen  will  have
*  created a COVT  record.  However  the  user  may  only  have
*  selected  Reassurance  which means that Reassurance is to be
*  added to the component without any  changes  being  made  to
*  that  component. This process may have been repeated for any
*  component on the plan and for any policy  within  the  plan.
*  For  this  reason  there  may be a component that has one or
*  more  COVT  records  created  each   with   different   Plan
*  Suffixes.  This  fact  will  be  used  in  the processing to
*  calculate the total Sum Assured for the component.
*
*
*  Initialise.
*  -----------
*
*
*  The details of the contract being processed will  have  been
*  stored  in the CHDRMJA I/O module. Use the RETRV function to
*  obtain the contract header details.
*
*  The key of the Coverage/Rider  with  which  the  reassurance
*  details  will  be associated will be held in the COVRMJA I/O
*  module. However this will not necessarily hold  the  details
*  of  the  newly created COVT record which may have been added
*  to the COVT physical  file  by  the  previous  function  via
*  COVTMJA  if  a  component  change  was  actually made. If no
*  component change was made but only Reassurance was  selected
*  then no COVT record will have been created.
*
*  Perform  a  RETRV  on  COVRMJA.  This  will provide a key of
*  Company, Contract, Life, Coverage, Rider and Plan Suffix.
*
*  Attempt to read the corresponding  COVTMJA  record  for  the
*  component.  If  one  is found store the Sex, Mortality Class
*  and Risk Cessation Date  from  there  otherwise  store  them
*  from  the  COVRMJA record. They will be used later on in the
*  Premium Calculation and to be stored on the RACT.
*
*  Obtain the main Life details by using the Company,  Contract
*  and  Life returned from COVTMJA plus a Joint Life of '00' to
*  perform a READR on LIFEMJA. Store the main  Life's  age  and
*  sex.
*
*  Use  the  same  LIFEMJA  key except with a Joint Life set to
*  '01' to READR on LIFEMJA again  in  order  to  retrieve  the
*  Joint Life details if they exist.
*
*  If found store the Joint Life's age and sex.
*
*  Use  the  Contract  Company, Contract Number, Life, Coverage
*  and Rider from the first COVRMJA with RASNUM and RATYPE  set
*  to  spaces  to perform a BEGN on the Reassurance Type Detail
*  record RACT. If no record is found then  this  is  the  very
*  first  time  that  Reassurance  has  been requested for this
*  component so all the reassurance details will  be  displayed
*  blank for input.
*
*  If a matching record is found then proceed as follows:
*
*       Display  the  Reassurance  Account  Number, RASNUM, and
*       use this to read RASA, the Reassurance  Account  master
*       file.  Use  the  client  number from there to read CLTS
*       and then use CONFNAME to format the client's  name  for
*       display.
*
*       Display  all  the other details looking up descriptions
*       as required.
*
*  The total sum assured for the  selected  component  must  be
*  calculated  and  displayed.  The selected component may have
*  had changes requested for some of the  policies  within  the
*  plan   and   therefore   some   COVRMJA   records  may  have
*  corresponding COVTMJA  records  associated  with  them.  The
*  summation is carried out as follows:
*
*       Use  the key obtained from COVRMJA and a Plan Suffix of
*       zero to perform a BEGN on COVRMJA. If end  of  file  or
*       no  matching  record  is  found  then report a database
*       error and cease processing.
*
*       Starting with this record process all  COVRMJA  records
*       that  match on Company, Contract Number, Life, Coverage
*       and Rider - that is for all values of Plan Suffix.  For
*       each  record  read use the full key to read COVTMJA. If
*       a matching record is found then  use  the  Sum  Assured
*       from  here  otherwise  use  the  Sum  Assured  from the
*       COVRMJA record. This final sum is output as 'Total  S/A
*       for Component'.
*
*
*  Store  the  screen  details  as they have been displayed for
*  later comparison to see if any details  have  actually  been
*  changed.
*
*  Check  WSSP-FLAG.  If 'I' (Enquiry  mode), protect all input
*  capable fields.
*
*
*  Validation.
*  -----------
*
*
*  Process the screen using 'S6267IO'.
*
*  If 'KILL' has been requested or the program  is  in  enquiry
*  mode skip all validation.
*
*  Validate  the  screen  according to the rules defined by the
*  field help. For all fields  which  have  descriptions,  look
*  them  up again. For the calculation of instalment premium to
*  work all the fields must first be validated and be correct.
*
*  The user  may  remove  Reassurance  from  the  component  by
*  blanking  out  the  Reassurance  Number  and the Reassurance
*  Type. Therefore allow spaces in both fields not just in  one
*  of  them.  If  the  user  has opted to blank out both fields
*  skip the remainder of the validation.
*
*  The Premium Calculation subroutine to be  called  will  vary
*  depending  on  the  appropriate Premium Calculation field on
*  T5687 calculated in the following way:
*
*       If this is a Single Life only, (the Joint  Life  record
*       was  not  found above), then use the Single Life Method
*       from T5687 - T5687-PREMMETH.
*
*       If this is a Joint Life case, (the  Joint  Life  record
*       was   found   above),  and  the  'Joint  Life  Allowed'
*       indicator, (S5687-JLIFE-PRESENT),  is  space  then  use
*       the Joint Life method from T5687 - T5687-JL-PREM-METH.
*
*       If  this  is  a Joint Life case, (the Joint Life record
*       was  found  above),  and  the  'Joint   Life   Allowed'
*       indicator,  (S5687-JLIFE-PRESENT),  is  not  space then
*       use the Single Life method from T5687 - T5687-PREMMETH.
*
*  If the appropriate one of these is  blank  then  no  premium
*  calculation  is  required  so do not call any subroutine. If
*  there is an entry in this field then use this in the key  to
*  access  T5675.  Use  the Premium Calculation Subroutine name
*  from the extra data screen on T5675  as  the  subroutine  to
*  call when calculating the premium.
*
*  Obtain  the general Coverage/Rider details including Premium
*  Calculation method from T5687 using ITDMIO with  a  function
*  of  READS.  The  key  is SMTP-ITEM, Company, 'T5687' and the
*  Reassurance Type - RATYPE.
*
*  Obtain the long description from DESC using  DESCIO  with  a
*  function of READS.
*
*  Obtain  the  Term  Component  Edit  rules  from  T5608 using
*  ITDMIO. This table will be accessed as follows:
*
*       Use  a  key  of   SMTP-ITEM,   Company,   'T5671'   and
*       Transaction  Code  concatenated  with  RATYPE  to  read
*       T5671 - Generic Component Control table.
*
*       This  contains  a  list  of  up  to  4  programs   with
*       associated  AT  module names and Validation Item Codes.
*       Match the current program name  against  the  list  and
*       select the associated Validation Item Code.
*
*       Use  this  to  construct  a  key of SMTP-ITEM, Company,
*       'T5608' and the Validation Item Code concatenated  with
*       the  Contract  Currency  Code  to  read  T5608  through
*       ITDMIO.
*
*  Use the RATYPE to read  T6662.  If  no  matching  record  is
*  found   then   this  coverage  code  may  not  be  used  for
*  Reassurance. If the record is  found  then  check  that  the
*  coverage  code  to  which it is being applied is held on the
*  table of associated coverage codes. If it is not inform  the
*  user  that  the  Reassurance  Type  is  not  valid  for  the
*  component.
*
*
*  Check that the Check Sum Assured entered  is  less  than  or
*  equal  to  the  total sum assured for the component. It must
*  also be within the maximum and minimum values on T5608.
*
*
*  Premium Calculation.
*  --------------------
*
*  The premium amount is  required  on  all  products  and  all
*  validating  must  be  successfully  completed  before  it is
*  calculated. If there is no premium method defined  (i.e. the
*  relevant  code  was  blank),  the  premium  amount  must  be
*  entered. Otherwise entry by the user  is  optional  but  the
*  system  will  always  calculate  it  and  override it if the
*  entered value falls outside an acceptable variation.
*
*  To calculate it call  the  relevant  calculation  subroutine
*  from  T5675  worked  out above with the following parameters
*  set:
*
*       CPRM-FUNCTION       - 'CALC'
*       CPRM-CHDR-CHDRCOY   - Contract Company
*       CPRM-CHDR-CHDRNUM   - Contract Header Number
*       CPRM-LIFE-LIFE      - COVRMJA-LIFE
*       CPRM-COVR-COVERAGE  - COVRMJA-COVERAGE
*       CPRM-COVR-RIDER     - COVRMJA-RIDER
*       CPRM-PLNSFX         - zero
*       CPRM-BILLFREQ       - CHDRMJA-BILLFREQ
*       CPRM-MOP            - CHDRMJA-BILLCHNL
*       CPRM-LIFE-JLIFE     - '01' if COVTRAS-JLIFE = '01'
*                             else set it to '00'
*       CPRM-CRTABLE        - RATYPE
*       CPRM-MORTCLS        - COVRMJA-MORTCLS
*       CPRM-CURRCODE       - CHDRMJA-CNTCURR
*       CPRM-EFFECTDT       - Contract Original Commencement
*                             Date - CHDRLNB-OCCDATE
*       CPRM-TERMDATE       - COVRMJA-PREM-CESS-DATE
*       CPRM-SUMIN          - S6262-SUMIN
*       CPRM-CALC-PREM      - S6262-INST-PREM
*       CPRM-RATINGDATE     - Contract Original Commencement
*                             Date - CHDRMJA-OCCDATE
*       CPRM-RE-RATE-DATE   - Contract Original Commencement
*                             Date - CHDR,JA-OCCDATE
*       CPRM-LAGE           - age of main Life
*       CPRM-JLAGE          - age of Joint Life
*       CPRM-LSEX           - sex of main Life
*       CPRM-JLSEX          - sex of Joint Life
*       CPRM-DURATION       - calculated using DATCON3 with
*                             the following parameters:
*
*            DTC3-INT-DATE-1     -    CHDRMJA-OCCDATE
*            DTC3-INT-DATE-2     -    COVTRAS-PREM-CESS-DATE
*            DTC3-FREQUENCY      -    FRQ-YRLY.
*
*            After the call  to  DATCON3  add  0.99999  to  the
*            result:  DTC3-FREQ-FACTOR, and place this value in
*            CPRM-DURATION.
*
*  On return from the premium calculation subroutine there  may
*  be  an  error message associated with the premium itself. If
*  so use this error message and highlight the  premium  field.
*  Otherwise place the returned premium on the screen.
*
*  If 'CALC' was requested re-display the screen.
*
*
*  Updating.
*  ---------
*
*
*  If  the  'KILL'  function  key  was pressed or if in enquiry
*  mode or if no changes have been made skip the updating.
*
*  Records may be added, modified or deleted. If a RACT  record
*  existed  and  the  user  has  changed  any  details then the
*  existing RACT record must be logically  deleted  and  a  new
*  one created.
*
*  Updating  will  be  as  follows,  for the RACT record if the
*  frequency is '00' then place the premium in  RACT-SINGP  and
*  set  RACT-INSTPREM  to  zero,  otherwise  place the value in
*  RACT-INSTPREM and set RACT-SINGP to sero.
*
*
*  Add Reassurance.
*  ----------------
*
*  If there was  no  RACT  record  originally  and  Reassurance
*  details have been entered write a RACT record. In fact, we
*  will write 2 RACT records, a validflag '1' and a validflag '2'
*  - The purpose of the validflag '2' is to provide Reversals
*     with a reference point i.e. Reversals can identify when
*     the Reassurance was added and so it can be removed at the
*     correct time in the reversals processing.
*
*  Set   the   RACT-CURRFROM   to   the   CHDRMJA-BTDATE,   the
*  RACT-RCESDTE to the greatest Risk Cessation Date found  from
*  the COVR and COVT records and the VALIDFLAG to '2'. Add 1 to
*  the existing contract header TRANNO and put on RACT record.
*  Change the VALIDFLAG to '1' and write another otherwise
*  identical record.
*
*  Modify Reassurance.
*  -------------------
*
*  If  there  was  a  RACT  record originally but any field has
*  been altered then  rewrite  the  original  RACT  record  and
*  write the new RACT record.
*
*   Write a validflag '2' RACT record, updating the RACT
*   RCESDTE with the BTDATE on the Contract Header record, add
*   1 to the TRANNO on the RACT record.
*
*   Then, write a validflag '4' RACT record, updating the RACT
*   RCESDTE with the BTDATE on the Contract Header record, add
*   1 to the TRANNO on the RACT record.
*
*   Then write the validflag '1' RACT record, again with an
*   updated TRANNO, set the CURRFROM to the BTDATE from the
*   Contract Header, the RCESDTE to the greater of the 2 values
*   found on the COVR and COVT records.
*
*   The purpose of the above Validflag '2' & '4' records is to
*    enable correct Reassurance Component Change reversals.
*
*  Delete Reassurance.
*  -------------------
*
*  If  there  was  a  RACT record originally and the RASNUM and
*  RATYPE code have been blanked out then rewrite the  original
*  RACT  record  with the RCESDTE set to the CHDRMJA-BTDATE and
*  the VALIDFLAG set to '2' and the TRANNO set to the value of
*  the Contract Header tranno + 1.
*
*  Also write a validflag '4' copy of the above RACT record.
*   This is to enable Reassurance premium collections &
*   Reassurance Component Change reversals to process correctly.
*
*  Next Program.
*  -------------
*
*  Add 1 to the program pointer and exit.
*
*
**************************************************************** *
* </pre>
*/
public class P6267 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6267");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaStoreFields = new FixedLengthStringData(28);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaStoreFields, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaStoreFields, 1);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaStoreFields, 9);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaStoreFields, 11);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaStoreFields, 13);
	private FixedLengthStringData wsaaRactSex = new FixedLengthStringData(1).isAPartOf(wsaaStoreFields, 15);
	private PackedDecimalData wsaaRactCrrcd = new PackedDecimalData(8, 0).isAPartOf(wsaaStoreFields, 16);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaStoreFields, 21);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaStoreFields, 25);

	private FixedLengthStringData wsaaPremiumFields = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaPremMortcls = new FixedLengthStringData(1).isAPartOf(wsaaPremiumFields, 0);
	private FixedLengthStringData wsaaPremJlife = new FixedLengthStringData(2).isAPartOf(wsaaPremiumFields, 1);
		/* WSAA-LIFE-DETAILS */
	private PackedDecimalData wsaaAge = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-LIFE-DETAILS */
	private PackedDecimalData wsbbAge = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaT5608Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5608Item1 = new FixedLengthStringData(5).isAPartOf(wsaaT5608Item, 0);
	private FixedLengthStringData wsaaT5608Item2 = new FixedLengthStringData(3).isAPartOf(wsaaT5608Item, 5);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trcode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Rastype = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);

	private FixedLengthStringData screenDataArea = new FixedLengthStringData(51);
		/* SCREEN-DATA-FIELDS */
	private FixedLengthStringData screenRasnum = new FixedLengthStringData(8).isAPartOf(screenDataArea, 0);
	private FixedLengthStringData screenRatype = new FixedLengthStringData(4).isAPartOf(screenDataArea, 8);
	private FixedLengthStringData screenRapayfrq = new FixedLengthStringData(2).isAPartOf(screenDataArea, 12);
	private ZonedDecimalData screenInstPrem = new ZonedDecimalData(17, 2).isAPartOf(screenDataArea, 14);
	private ZonedDecimalData screenRaAmount = new ZonedDecimalData(17, 2).isAPartOf(screenDataArea, 31);
	private FixedLengthStringData screenCurrcode = new FixedLengthStringData(3).isAPartOf(screenDataArea, 48);
		/* WSAA-MISC */
	private ZonedDecimalData wsaaSumAssuredAccum = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaRiskCessDate = new PackedDecimalData(8, 0);
	private String wsaaNoPremCalc = "";
	private FixedLengthStringData wsaaSubroutineName = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaValItemCode = new FixedLengthStringData(5);

	private FixedLengthStringData wsaaPremMethInd = new FixedLengthStringData(1);
	private Validator useSingleLife = new Validator(wsaaPremMethInd, "S");
	private Validator useJointLife = new Validator(wsaaPremMethInd, "J");
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5608 = "T5608";
	private static final String t5633 = "T5633";
	private static final String t5671 = "T5671";
	private static final String t5675 = "T5675";
	private static final String t5687 = "T5687";
	private static final String t6662 = "T6662";
	private AnntmjaTableDAM anntmjaIO = new AnntmjaTableDAM();
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private RactTableDAM ractIO = new RactTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5608rec t5608rec = new T5608rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T6662rec t6662rec = new T6662rec();
	private Premiumrec premiumrec = new Premiumrec();
	private Wssplife wssplife = new Wssplife();
	private S6267ScreenVars sv = ScreenProgram.getScreenVars( S6267ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		chdrFields1020, 
		addSoNoCovr1040, 
		exit2090
	}

	public P6267() {
		super();
		screenVars = sv;
		new ScreenModel("S6267", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
				case chdrFields1020: 
					chdrFields1020();
					accumSumAssured1030();
				case addSoNoCovr1040: 
					addSoNoCovr1040();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		/*    Screen initialisation*/
		sv.dataArea.set(SPACES);
		sv.instPrem.set(ZERO);
		sv.raAmount.set(ZERO);
		sv.totsuma.set(ZERO);
		sv.btdate.set(varcom.vrcmMaxDate);
		/*    Field Initialisation.*/
		screenDataArea.set(SPACES);
		wsaaStoreFields.set(SPACES);
		wsaaPremiumFields.set(SPACES);
		wsaaT5608Item.set(SPACES);
		wsaaT5671Item.set(SPACES);
		wsaaSubroutineName.set(SPACES);
		wsaaValItemCode.set(SPACES);
		wsaaPremMethInd.set(SPACES);
		datcon3rec.freqFactor.set(ZERO);
		screenInstPrem.set(ZERO);
		screenRaAmount.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaRiskCessDate.set(ZERO);
		wsaaSumAssuredAccum.set(ZERO);
		datcon3rec.intDate1.set(varcom.vrcmMaxDate);
		datcon3rec.intDate2.set(varcom.vrcmMaxDate);
		wsaaBatckey.set(wsspcomn.batchkey);
		/*    Set screen fields*/
		/*    Read CHDRMJA (RETRV) to obtain contract header information.*/
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*    Retrieve the COVRMJA record; coverage/rider record.*/
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		/*    Set up COVTMJA key details from COVRMJA record read.*/
		/*    Attempt to read the corresponding COVTMJA record for the comp*/
		covtmjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(covrmjaIO.getChdrnum());
		covtmjaIO.setLife(covrmjaIO.getLife());
		covtmjaIO.setCoverage(covrmjaIO.getCoverage());
		covtmjaIO.setRider(covrmjaIO.getRider());
		covtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		covtmjaIO.setFunction(varcom.readr);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		/*    If a COVTMJA record is found, store the Sex, Mortality Class*/
		/*    and CRRCD date, otherwise store them from the COVRMJA record.*/
		/*    These will be used later on in the Premium Calculation and wi*/
		/*    be stored on the RACT.*/
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)) {
			wsaaRactSex.set(covrmjaIO.getSex());
			wsaaPremMortcls.set(covrmjaIO.getMortcls());
		}
		else {
			wsaaPremMortcls.set(covtmjaIO.getMortcls());
			if (isEQ(covtmjaIO.getSex01(),SPACES)) {
				wsaaRactSex.set(covtmjaIO.getSex02());
			}
			else {
				wsaaRactSex.set(covtmjaIO.getSex01());
			}
		}
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the life assured and joint-life details (if any) do*/
		/* the following;-*/
		/*  - read  the life details using LIFEMJA (life number from*/
		/*       COVRMJA, joint life number '00').*/
		/*  - store the main life's age and sex.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		wsaaAge.set(lifemjaIO.getAnbAtCcd());
		wsaaSex.set(lifemjaIO.getCltsex());
		/*  - read the joint life details using LIFEMJA (life number*/
		/*       from COVRMJA, joint  life number '01').*/
		/*  - store the Joint Life's details, if they exist.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			wsaaPremMethInd.set("S");
			wsbbSex.set(SPACES);
			wsbbAge.set(ZERO);
		}
		else {
			wsaaPremMethInd.set("J");
			wsbbAge.set(lifemjaIO.getAnbAtCcd());
			wsbbSex.set(lifemjaIO.getCltsex());
		}
		/*    Use the Contract Company, Contract Number, Life, Coverage and*/
		/*    Rider from the COVRMJA record with RASNUM and RATYPE set to*/
		/*    spaces, to perform a BEGN on the Reassurance Type Detail reco*/
		/*    RACT.  If no record is found, then this is the very first tim*/
		/*    that Reassurance has been requested for this component so all*/
		/*    the reassurance details will be displayed blank for input.*/
		ractIO.setChdrcoy(covrmjaIO.getChdrcoy());
		ractIO.setChdrnum(covrmjaIO.getChdrnum());
		ractIO.setLife(covrmjaIO.getLife());
		ractIO.setCoverage(covrmjaIO.getCoverage());
		ractIO.setRider(covrmjaIO.getRider());
		ractIO.setRasnum(SPACES);
		ractIO.setRatype(SPACES);
		ractIO.setValidflag(SPACES);
		ractIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ractIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ractIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		ractIO.setFormat(formatsInner.ractrec);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)
		&& isNE(ractIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		if ((isEQ(ractIO.getStatuz(),varcom.oK)
		&& isEQ(ractIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		&& isEQ(ractIO.getChdrnum(),covrmjaIO.getChdrnum())
		&& isEQ(ractIO.getLife(),covrmjaIO.getLife())
		&& isEQ(ractIO.getCoverage(),covrmjaIO.getCoverage())
		&& isEQ(ractIO.getRider(),covrmjaIO.getRider())
		&& isEQ(ractIO.getValidflag(),"1"))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.chdrFields1020);
		}
		sv.rasnum.set(ractIO.getRasnum());
		rasaIO.setRasnum(ractIO.getRasnum());
		rasaIO.setRascoy(ractIO.getChdrcoy());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(formatsInner.rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		readClient2310();
		sv.raAmount.set(ractIO.getRaAmount());
		if (isEQ(ractIO.getInstprem(),ZERO)) {
			sv.instPrem.set(ractIO.getSingp());
		}
		else {
			sv.instPrem.set(ractIO.getInstprem());
		}
		sv.rapayfrq.set(ractIO.getRapayfrq());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5633);
		descIO.setDescitem(sv.rapayfrq);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.payfrqd.set(descIO.getShortdesc());
		sv.ratype.set(ractIO.getRatype());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sv.ratype);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.ratypeDesc.set(descIO.getLongdesc());
	}

protected void chdrFields1020()
	{
		sv.currcode.set(chdrmjaIO.getCntcurr());
		sv.btdate.set(chdrmjaIO.getBtdate());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sv.currcode);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.currds.set(descIO.getShortdesc());
		if (isEQ(wsspcomn.flag,"A")) {
			wsaaSumAssuredAccum.set(covtmjaIO.getSumins());
			wsaaRiskCessDate.set(covtmjaIO.getRiskCessDate());
			wsaaChdrcoy.set(covtmjaIO.getChdrcoy());
			wsaaChdrnum.set(covtmjaIO.getChdrnum());
			wsaaCoverage.set(covtmjaIO.getCoverage());
			wsaaLife.set(covtmjaIO.getLife());
			wsaaRider.set(covtmjaIO.getRider());
			wsaaCrtable.set(covtmjaIO.getCrtable());
			goTo(GotoLabel.addSoNoCovr1040);
		}
	}

protected void accumSumAssured1030()
	{
		wsaaChdrcoy.set(covrmjaIO.getChdrcoy());
		wsaaChdrnum.set(covrmjaIO.getChdrnum());
		wsaaCoverage.set(covrmjaIO.getCoverage());
		wsaaRider.set(covrmjaIO.getRider());
		wsaaLife.set(covrmjaIO.getLife());
		wsaaPremJlife.set(covrmjaIO.getJlife());
		wsaaCrtable.set(covrmjaIO.getCrtable());
		wsaaPlanSuffix.set(covrmjaIO.getPlanSuffix());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getChdrcoy(),wsaaChdrcoy)
		&& isEQ(covrmjaIO.getChdrnum(),wsaaChdrnum)
		&& isEQ(covrmjaIO.getLife(),wsaaLife)
		&& isEQ(covrmjaIO.getCoverage(),wsaaCoverage)
		&& isEQ(covrmjaIO.getRider(),wsaaRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrmjaIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrmjaIO.getLife(),wsaaLife)
		|| isNE(covrmjaIO.getCoverage(),wsaaCoverage)
		|| isNE(covrmjaIO.getRider(),wsaaRider))) {
			calculateComponent1500();
		}
		
	}

protected void addSoNoCovr1040()
	{
		sv.totsuma.set(wsaaSumAssuredAccum);
		screenRasnum.set(sv.rasnum);
		screenRapayfrq.set(sv.rapayfrq);
		screenRatype.set(sv.ratype);
		screenRaAmount.set(sv.raAmount);
		screenInstPrem.set(sv.instPrem);
		screenCurrcode.set(ractIO.getCurrcode());
		covrmjaIO.setChdrcoy(wsaaChdrcoy);
		covrmjaIO.setChdrnum(wsaaChdrnum);
		covrmjaIO.setLife(wsaaLife);
		covrmjaIO.setCoverage(wsaaCoverage);
		covrmjaIO.setRider(wsaaRider);
		covrmjaIO.setCrtable(wsaaCrtable);
		covrmjaIO.setPlanSuffix(wsaaPlanSuffix);
		if (isEQ(wsspcomn.flag,"I")) {
			sv.cltnameOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.rapayfrqOut[varcom.pr.toInt()].set("Y");
			sv.rasnumOut[varcom.pr.toInt()].set("Y");
			sv.ratypeOut[varcom.pr.toInt()].set("Y");
			sv.ratypedescOut[varcom.pr.toInt()].set("Y");
			sv.raamountOut[varcom.pr.toInt()].set("Y");
			sv.totsumaOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void calculateComponent1500()
	{
			calculateComponent1501();
		}

protected void calculateComponent1501()
	{
		covtmjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(covrmjaIO.getChdrnum());
		covtmjaIO.setLife(covrmjaIO.getLife());
		covtmjaIO.setCoverage(covrmjaIO.getCoverage());
		covtmjaIO.setRider(covrmjaIO.getRider());
		covtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		covtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)) {
			wsaaSumAssuredAccum.add(covrmjaIO.getSumins());
		}
		else {
			wsaaSumAssuredAccum.add(covtmjaIO.getSumins());
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
		&& isGT(covrmjaIO.getRiskCessDate(),wsaaRiskCessDate)) {
			wsaaRiskCessDate.set(covrmjaIO.getRiskCessDate());
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)
		&& isGT(covtmjaIO.getRiskCessDate(),wsaaRiskCessDate)) {
			wsaaRiskCessDate.set(covtmjaIO.getRiskCessDate());
		}
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsaaNoPremCalc = "N";
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
			try {
					screenIo2010();
					validate2020();
					calc2030();
				}
			catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6267IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S6267-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    If F11:-KILL or Enquiry then do not do proceed with Validatio*/
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO 2990-EXIT.                                             */
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isEQ(sv.rasnum,SPACES)
		&& isEQ(sv.ratype,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.rasnum,SPACES)
		&& isNE(sv.ratype,SPACES)) {
			/*    MOVE U999                TO S6267-RATYPE-ERR              */
			sv.ratypeErr.set(errorsInner.e065);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.ratype,SPACES)
		&& isNE(sv.rasnum,SPACES)) {
			sv.ratypeErr.set(errorsInner.e763);
			/*    MOVE U999                TO S6267-RATYPE-ERR              */
			sv.payfrqd.set(SPACES);
			wsspcomn.edterror.set("Y");
		}
		checkRasnum2100();
		if (isEQ(sv.rapayfrq,SPACES)) {
			/*     MOVE U991                TO S6267-RAPAYFRQ-ERR            */
			sv.rapayfrqErr.set(errorsInner.f021);
			sv.payfrqd.set(SPACES);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			goTo(GotoLabel.exit2090);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5633);
		descIO.setDescitem(sv.rapayfrq);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.payfrqd.set(SPACES);
		}
		else {
			sv.payfrqd.set(descIO.getShortdesc());
		}
		checkRatype2200();
		if (isGT(sv.raAmount,sv.totsuma)) {
			/*     MOVE U996                TO S6267-RAAMOUNT-ERR            */
			sv.raamountErr.set(errorsInner.h090);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaNoPremCalc,"Y")) {
			if (isLT(sv.instPrem,ZERO)
			|| isEQ(sv.instPrem,ZERO)) {
				/*        MOVE U992             TO S6267-INSTPRM-ERR             */
				sv.instprmErr.set(errorsInner.h086);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			else {
				goTo(GotoLabel.exit2090);
			}
		}
		if (isLT(sv.raAmount,t5608rec.sumInsMin)
		|| isGT(sv.raAmount,t5608rec.sumInsMax)) {
			/*     MOVE U995                TO S6267-RAAMOUNT-ERR            */
			sv.raamountErr.set(errorsInner.h089);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void calc2030()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(sv.ratype);
		premiumrec.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(chdrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(wsaaLife);
		if (isEQ(wsaaPremJlife,"01")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(wsaaCoverage);
		premiumrec.covrRider.set(wsaaRider);
		premiumrec.effectdt.set(chdrmjaIO.getBtdate());
		premiumrec.termdate.set(wsaaRiskCessDate);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAge);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAge);
		datcon3rec.intDate1.set(chdrmjaIO.getBtdate());
		datcon3rec.intDate2.set(wsaaRiskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(chdrmjaIO.getCntcurr());
		premiumrec.sumin.set(sv.raAmount);
		premiumrec.mortcls.set(wsaaPremMortcls);
		premiumrec.billfreq.set(chdrmjaIO.getBillfreq());
		premiumrec.mop.set(chdrmjaIO.getBillchnl());
		premiumrec.ratingdate.set(chdrmjaIO.getOccdate());
		premiumrec.reRateDate.set(chdrmjaIO.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.plnsfx.set(ZERO);
		premiumrec.reasind.set("2");
		if (isNE(wsaaPlanSuffix,0)) {
			annyIO.setPlanSuffix(wsaaPlanSuffix);
			getAnny2400();
		}
		else {
			anntmjaIO.setPlanSuffix(wsaaPlanSuffix);
			getAnntmja2500();
			if (isEQ(anntmjaIO.getStatuz(),varcom.mrnf)) {
				annyIO.setPlanSuffix(ZERO);
				getAnny2400();
			}
		}
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(wsaaSubroutineName, premiumrec.premiumRec);
		}
		else
		{			
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
		Vpxlextrec vpxlextrec = new Vpxlextrec();
		vpxlextrec.function.set("INIT");
		callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
		
		Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
		vpxchdrrec.function.set("INIT");
		callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
		premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		Vpxacblrec vpxacblrec=new Vpxacblrec();
		callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		//premiumrec.premMethod.set(wsaaSubroutineName.toString().substring(3));
		callProgram(wsaaSubroutineName, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/	
		//****Ticket #ILIFE-2005 end		
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO 2900-EXIT.                                             */
			return ;
		}
		sv.instPrem.set(premiumrec.calcPrem);
		/*    All the goto's shall NOW be directed through this Para.*/
		/*    This Para had to be created becuse F9 was not working for all*/
		/*    cases ie: When Blank screen displayed, F9 used to exit,*/
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    But now all Go To will fall through this Para (2900-EXIT)    */
		/*    But now all Go To will fall through this Para (2090-EXIT)    */
		/*    so F9 can Be Caught on ALL Cases. The Exception is Kill*/
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    which will go through Directly 2990-EXIT.                    */
		/*    which will go through Directly 2090-EXIT.                    */
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*2900-EXIT.                                                       */
		/*2090-EXIT.                                               <S19FIX>*/
		/*    If F9 is Pressed then Re-Display The Screen.*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkRasnum2100()
	{
		checkRasnum2110();
	}

protected void checkRasnum2110()
	{
		rasaIO.setRascoy(chdrmjaIO.getChdrcoy());
		rasaIO.setRasnum(sv.rasnum);
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(formatsInner.rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)
		&& isNE(rasaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		if (isEQ(rasaIO.getStatuz(),varcom.mrnf)) {
			/*      MOVE U998               TO S6267-RASNUM-ERR              */
			sv.rasnumErr.set(errorsInner.h092);
			wsspcomn.edterror.set("Y");
		}
		else {
			readClient2310();
		}
	}

protected void checkRatype2200()
	{
			checkRastype2210();
		}

protected void checkRastype2210()
	{
		/*     Get ready for Premium Calculations.*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(sv.ratype);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),sv.ratype)) {
			/*      MOVE U997               TO S6267-RATYPE-ERR              */
			sv.ratypeErr.set(errorsInner.h091);
			wsspcomn.edterror.set("Y");
			return ;
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		itemIO.setParams(SPACES);
		if (useSingleLife.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		else {
			if (useJointLife.isTrue()) {
				if (isEQ(t5687rec.jlifePresent,SPACES)) {
					itemIO.setItemitem(t5687rec.jlPremMeth);
				}
				else {
					itemIO.setItemitem(t5687rec.premmeth);
				}
			}
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sv.ratype);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.ratypeDesc.set(descIO.getLongdesc());
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			wsaaNoPremCalc = "Y";
			return ;
		}
		/*     Read T5675 to obtain the subroutine to call when calculating*/
		/*     the premium*/
		/*    Item Moved Above*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemseq(SPACES);
		itemIO.setItemtabl(t5675);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/* ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		wsaaSubroutineName.set(t5675rec.premsubr);
		/*     Read T5671 - Generic Component Control Table*/
		/*     This contains a list of up to 4 programs with associated AT*/
		/*     module names and Validation Item Codes.  Match the current*/
		/*     program name against the list and select the asociated*/
		/*     Validation Item Code*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Trcode.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Rastype.set(sv.ratype);
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		if (isEQ(wsaaProg,t5671rec.pgm01)) {
			wsaaValItemCode.set(t5671rec.edtitm01);
		}
		else {
			if (isEQ(wsaaProg,t5671rec.pgm02)) {
				wsaaValItemCode.set(t5671rec.edtitm02);
			}
			else {
				if (isEQ(wsaaProg,t5671rec.pgm03)) {
					wsaaValItemCode.set(t5671rec.edtitm03);
				}
				else {
					if (isEQ(wsaaProg,t5671rec.pgm04)) {
						wsaaValItemCode.set(t5671rec.edtitm04);
					}
					else {
						wsaaValItemCode.set(SPACES);
					}
				}
			}
		}
		/*     Obtain the UNIT Edit Rules from T5608 using ITDMIO*/
		itdmIO.setParams(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5608);
		wsaaT5608Item1.set(wsaaValItemCode);
		wsaaT5608Item2.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5608Item);
		/*  MOVE CHDRMJA-OCCDATE       TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(chdrmjaIO.getPtdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)
		|| (isNE(wsspcomn.company,itdmIO.getItemcoy())
		&& isNE(t5608,itdmIO.getItemtabl())
		&& isNE(wsaaT5608Item,itdmIO.getItemitem()))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5608rec.t5608Rec.set(itdmIO.getGenarea());
		/*     Use the RATYPE to read T6662.*/
		/*     If no matching record is found then this coverage code may n*/
		/*     be used for Reassurance. If record is found then check that*/
		/*     coverage code to which it is being applied is held on the*/
		/*     table of associated coverage codes. If not inform the User*/
		/*     that the Reassurance type is not valid for the component.*/
		itdmIO.setParams(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6662);
		itdmIO.setItemitem(sv.ratype);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/*     If the Statuz is not OK or the Key has changed then*/
		/*     display a message saying 'Not A Reassurance Type.'*/
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t6662,itdmIO.getItemtabl())
		|| isNE(sv.ratype,itdmIO.getItemitem())) {
			/*      MOVE U994               TO S6267-RATYPE-ERR              */
			sv.ratypeErr.set(errorsInner.h088);
			wsspcomn.edterror.set("Y");
		}
		t6662rec.t6662Rec.set(itdmIO.getGenarea());
		/*     If No match for the reassurance code can be found then*/
		/*     display a message saying 'RATYPE not Valid for Component.'*/
		if (isEQ(covrmjaIO.getCrtable(),t6662rec.reascode01)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode02)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode03)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode04)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode05)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode06)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode07)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode08)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode09)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode10)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode11)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode12)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode13)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode14)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode15)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode16)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode17)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode18)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode19)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode20)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode21)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode22)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode23)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode24)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode25)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode26)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode27)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode28)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode29)
		|| isEQ(covrmjaIO.getCrtable(),t6662rec.reascode30)) {
			/*NEXT_SENTENCE*/
		}
		else {
			/*      MOVE U993               TO S6267-RATYPE-ERR              */
			sv.ratypeErr.set(errorsInner.h087);
			wsspcomn.edterror.set("Y");
		}
	}

protected void readClient2310()
	{
		readkClient2311();
	}

protected void readkClient2311()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setClntcoy(rasaIO.getClntcoy());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    Formating the name for output to the screen.*/
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void getAnny2400()
	{
		begins2410();
	}

protected void begins2410()
	{
		annyIO.setChdrcoy(covrmjaIO.getChdrcoy());
		annyIO.setChdrnum(covrmjaIO.getChdrnum());
		annyIO.setLife(covrmjaIO.getLife());
		annyIO.setCoverage(covrmjaIO.getCoverage());
		annyIO.setRider(covrmjaIO.getRider());
		/*    MOVE COVRMJA-PLAN-SUFFIX    TO  ANNY-PLAN-SUFFIX.            */
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

protected void getAnntmja2500()
	{
		begins2510();
	}

protected void begins2510()
	{
		anntmjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		anntmjaIO.setChdrnum(covrmjaIO.getChdrnum());
		anntmjaIO.setLife(covrmjaIO.getLife());
		anntmjaIO.setCoverage(covrmjaIO.getCoverage());
		anntmjaIO.setRider(covrmjaIO.getRider());
		/*    MOVE COVRMJA-PLAN-SUFFIX    TO  ANNTMJA-PLAN-SUFFIX.         */
		anntmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(),varcom.oK)
		&& isNE(anntmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anntmjaIO.getParams());
			syserrrec.statuz.set(anntmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntmjaIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(anntmjaIO.getFreqann());
			premiumrec.advance.set(anntmjaIO.getAdvance());
			premiumrec.arrears.set(anntmjaIO.getArrears());
			premiumrec.guarperd.set(anntmjaIO.getGuarperd());
			premiumrec.intanny.set(anntmjaIO.getIntanny());
			premiumrec.capcont.set(anntmjaIO.getCapcont());
			premiumrec.withprop.set(anntmjaIO.getWithprop());
			premiumrec.withoprop.set(anntmjaIO.getWithoprop());
			premiumrec.ppind.set(anntmjaIO.getPpind());
			premiumrec.nomlife.set(anntmjaIO.getNomlife());
			premiumrec.dthpercn.set(anntmjaIO.getDthpercn());
			premiumrec.dthperco.set(anntmjaIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3100();
		}

protected void updateDatabase3100()
	{
		/*    Update database files as required.*/
		/*    If Kill or In Enquiry or No Changes Made then skip Up-Dating*/
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")
		|| (isEQ(sv.instPrem,screenInstPrem)
		&& isEQ(sv.rapayfrq,screenRapayfrq)
		&& isEQ(sv.rasnum,screenRasnum)
		&& isEQ(sv.ratype,screenRatype)
		&& isEQ(sv.raAmount,screenRaAmount)
		&& isEQ(sv.currcode,screenCurrcode))) {
			return ;
		}
		/*  If no reassurance details entered on the screen bypass update*/
		/*    and no RACT details originally, bypass update.*/
		if (isEQ(sv.rasnum,SPACES)
		&& isEQ(screenRasnum,SPACES)
		&& isEQ(sv.ratype,SPACES)
		&& isEQ(screenRatype,SPACES)) {
			return ;
		}
		/*    If there was no RACT record originally and Reassurance detail*/
		/*    have been entered, write a RACT record.*/
		if (isEQ(screenRasnum,SPACES)
		&& isNE(sv.rasnum,SPACES)
		&& isEQ(screenRatype,SPACES)
		&& isNE(sv.ratype,SPACES)) {
			addRact3140();
			return ;
		}
		/*    If there was a RACT record originally and the RASNUM and RATY*/
		/*    code have been blanked out then rewrite the original RACT rec*/
		/*    with the RCESDTE set to the CHRDMJA-BTDATE and the VALIDFLAG */
		/*    to '3'.*/
		if ((isNE(screenRatype,SPACES)
		&& isEQ(sv.ratype,SPACES))
		&& (isNE(screenRasnum,SPACES)
		&& isEQ(sv.rasnum,SPACES))) {
			deleteRact3160();
			return ;
		}
		/*    If there was a RACT record originally but either the         */
		/*    Reassurer ( RASNUM ) or Reassurance type ( RATYPE ) has      */
		/*    changed, then we want to do a 'DELETE' on the old RACT       */
		/*    record and an 'ADD' with the new Reassurance details         */
		if ((isNE(screenRasnum,SPACES)
		&& isNE(sv.rasnum,SPACES)
		&& isNE(sv.rasnum,screenRasnum))
		|| (isNE(screenRatype,SPACES)
		&& isNE(sv.ratype,SPACES)
		&& isNE(sv.ratype,screenRatype))) {
			deleteRact3160();
			addRact3140();
			return ;
		}
		/* If any of the other fields on the RACT record have changed,     */
		/*   then we will modify the existing RACT record.                 */
		if (isNE(sv.rapayfrq,screenRapayfrq)
		|| isNE(sv.raAmount,screenRaAmount)
		|| isNE(sv.instPrem,screenInstPrem)
		|| isNE(sv.currcode,screenCurrcode)) {
			modifyRact3150();
		}
	}

protected void addRact3140()
	{
		addRact3141();
	}

protected void addRact3141()
	{
		/* Write a validflag '2' first to provide a reference point*/
		/*  for Reversals in case a reversal is required. Then write*/
		/*  the validflag '1'.*/
		ractIO.setDataArea(SPACES);
		ractIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ractIO.setChdrnum(chdrmjaIO.getChdrnum());
		ractIO.setLife(wsaaLife);
		ractIO.setCoverage(wsaaCoverage);
		ractIO.setRider(wsaaRider);
		ractIO.setRasnum(sv.rasnum);
		ractIO.setRatype(sv.ratype);
		/* MOVE '1'                    TO RACT-VALIDFLAG.               */
		ractIO.setValidflag("2");
		if (isEQ(sv.rapayfrq,"00")) {
			ractIO.setSingp(sv.instPrem);
			ractIO.setInstprem(ZERO);
		}
		else {
			ractIO.setSingp(ZERO);
			ractIO.setInstprem(sv.instPrem);
		}
		ractIO.setCurrcode(chdrmjaIO.getCntcurr());
		ractIO.setSex(wsaaRactSex);
		ractIO.setRaAmount(sv.raAmount);
		ractIO.setRapayfrq(sv.rapayfrq);
		ractIO.setCurrfrom(chdrmjaIO.getBtdate());
		/*    MOVE WSAA-RACT-CRRCD        TO RACT-CRRCD.                   */
		ractIO.setCrrcd(chdrmjaIO.getBtdate());
		ractIO.setRiskCessDate(wsaaRiskCessDate);
		ractIO.setBtdate(sv.btdate);
		ractIO.setTransactionDate(varcom.vrcmDate);
		ractIO.setTransactionTime(varcom.vrcmTime);
		ractIO.setUser(varcom.vrcmUser);
		ractIO.setTermid(varcom.vrcmTermid);
		setPrecision(ractIO.getTranno(), 0);
		ractIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)
		&& isNE(ractIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		/*  Now write validflag '1' RACT record                            */
		ractIO.setValidflag("1");
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)
		&& isNE(ractIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
	}

protected void modifyRact3150()
	{
		modifyRact3151();
	}

protected void modifyRact3151()
	{
		/*    If there was a RACT record originally but any field has      */
		/*    been altered then rewrite the Original RACT record and       */
		/*    write a new RACT record.                                     */
		/*    Attempt to write a '4' RACT record - if one already exists,  */
		/*    leave this record as it is, else one will be written.        */
		/*    Also attempt to write a V/F '2' record which matches the     */
		/*    Validflag '4' record excepting the tranno, which is updated  */
		/*    on the validflag '4' but not the validflag '2'. This is to   */
		/*    enable reversals to work correctly.                          */
		/*    Rewrite the '1' record.                                      */
		ractIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ractIO.setChdrnum(chdrmjaIO.getChdrnum());
		ractIO.setLife(wsaaLife);
		ractIO.setCoverage(wsaaCoverage);
		ractIO.setRider(wsaaRider);
		ractIO.setRasnum(screenRasnum);
		ractIO.setRatype(screenRatype);
		setPrecision(ractIO.getTranno(), 0);
		ractIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ractIO.setValidflag("2");
		ractIO.setTransactionDate(varcom.vrcmDate);
		ractIO.setTransactionTime(varcom.vrcmTime);
		ractIO.setUser(varcom.vrcmUser);
		ractIO.setTermid(varcom.vrcmTermid);
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)
		&& isNE(ractIO.getStatuz(),varcom.dupr)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		ractIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ractIO.setChdrnum(chdrmjaIO.getChdrnum());
		ractIO.setLife(wsaaLife);
		ractIO.setCoverage(wsaaCoverage);
		ractIO.setRider(wsaaRider);
		ractIO.setRasnum(screenRasnum);
		ractIO.setRatype(screenRatype);
		/* MOVE '3'                    TO RACT-VALIDFLAG.          <002>*/
		ractIO.setValidflag("4");
		setPrecision(ractIO.getTranno(), 0);
		ractIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ractIO.setTransactionDate(varcom.vrcmDate);
		ractIO.setTransactionTime(varcom.vrcmTime);
		ractIO.setUser(varcom.vrcmUser);
		ractIO.setTermid(varcom.vrcmTermid);
		ractIO.setRiskCessDate(chdrmjaIO.getBtdate());
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)
		&& isNE(ractIO.getStatuz(),varcom.dupr)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		ractIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ractIO.setChdrnum(chdrmjaIO.getChdrnum());
		ractIO.setLife(wsaaLife);
		ractIO.setCoverage(wsaaCoverage);
		ractIO.setRider(wsaaRider);
		ractIO.setRasnum(screenRasnum);
		ractIO.setRatype(screenRatype);
		ractIO.setValidflag("1");
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.rapayfrq,"00")) {
			ractIO.setSingp(sv.instPrem);
			ractIO.setInstprem(ZERO);
		}
		else {
			ractIO.setSingp(ZERO);
			ractIO.setInstprem(sv.instPrem);
		}
		ractIO.setCurrcode(chdrmjaIO.getCntcurr());
		ractIO.setSex(wsaaRactSex);
		ractIO.setRaAmount(sv.raAmount);
		ractIO.setRapayfrq(sv.rapayfrq);
		ractIO.setRatype(sv.ratype);
		ractIO.setCurrfrom(chdrmjaIO.getBtdate());
		ractIO.setCrrcd(chdrmjaIO.getBtdate());
		ractIO.setRiskCessDate(wsaaRiskCessDate);
		ractIO.setBtdate(sv.btdate);
		ractIO.setTransactionDate(varcom.vrcmDate);
		ractIO.setTransactionTime(varcom.vrcmTime);
		ractIO.setUser(varcom.vrcmUser);
		ractIO.setTermid(varcom.vrcmTermid);
		setPrecision(ractIO.getTranno(), 0);
		ractIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
	}

protected void deleteRact3160()
	{
		deleteRact3161();
	}

protected void deleteRact3161()
	{
		/*    Rewrite the RACT record, flagged as deleted.*/
		/*  We now write a V/F '4' with the New tranno AND a V/F '2'       */
		/*   with the Old tranno to enable us to do Reversals correctly.   */
		ractIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ractIO.setChdrnum(chdrmjaIO.getChdrnum());
		ractIO.setLife(wsaaLife);
		ractIO.setCoverage(wsaaCoverage);
		ractIO.setRider(wsaaRider);
		ractIO.setRasnum(screenRasnum);
		ractIO.setRatype(screenRatype);
		ractIO.setValidflag("1");
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		/* MOVE CHDRMJA-BTDATE         TO RACT-RISK-CESS-DATE.          */
		/* MOVE '3'                    TO RACT-VALIDFLAG.               */
		ractIO.setValidflag("2");
		setPrecision(ractIO.getTranno(), 0);
		ractIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ractIO.setTransactionDate(varcom.vrcmDate);
		ractIO.setTransactionTime(varcom.vrcmTime);
		ractIO.setUser(varcom.vrcmUser);
		ractIO.setTermid(varcom.vrcmTermid);
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.rewrt);
		/*    MOVE UPDAT                  TO RACT-FUNCTION.                */
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
		ractIO.setRiskCessDate(chdrmjaIO.getBtdate());
		ractIO.setValidflag("4");
		setPrecision(ractIO.getTranno(), 0);
		ractIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ractIO.setFormat(formatsInner.ractrec);
		ractIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ractIO);
		if (isNE(ractIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e065 = new FixedLengthStringData(4).init("E065");
	private FixedLengthStringData e763 = new FixedLengthStringData(4).init("E763");
	private FixedLengthStringData f021 = new FixedLengthStringData(4).init("F021");
	private FixedLengthStringData h086 = new FixedLengthStringData(4).init("H086");
	private FixedLengthStringData h087 = new FixedLengthStringData(4).init("H087");
	private FixedLengthStringData h088 = new FixedLengthStringData(4).init("H088");
	private FixedLengthStringData h089 = new FixedLengthStringData(4).init("H089");
	private FixedLengthStringData h090 = new FixedLengthStringData(4).init("H090");
	private FixedLengthStringData h091 = new FixedLengthStringData(4).init("H091");
	private FixedLengthStringData h092 = new FixedLengthStringData(4).init("H092");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData rasarec = new FixedLengthStringData(10).init("RASAREC");
	private FixedLengthStringData ractrec = new FixedLengthStringData(10).init("RACTREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData lifemjarec = new FixedLengthStringData(10).init("LIFEMJAREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
}
}
