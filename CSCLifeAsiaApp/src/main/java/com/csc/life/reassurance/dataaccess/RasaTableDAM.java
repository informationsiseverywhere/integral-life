package com.csc.life.reassurance.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RasaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:45:33
 * Class transformed from RASA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RasaTableDAM extends RasapfTableDAM {

	public RasaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("RASA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RASCOY"
		             + ", RASNUM";
		
		QUALIFIEDCOLUMNS = 
		            "RASCOY, " +
		            "RASNUM, " +
		            "VALIDFLAG, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "PAYCLT, " +
		            "RAPAYMTH, " +
		            "RAPAYFRQ, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "CURRCODE, " +
		            "DTEPAY, " +
		            "AGNTNUM, " +
		            "MINSTA, " +
		            "COMMDT, " +
		            "TERMDT, " +
		            "REASST, " +
		            "STMTDT, " +
		            "NXTPAY, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RASCOY ASC, " +
		            "RASNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "RASCOY DESC, " +
		            "RASNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               rascoy,
                               rasnum,
                               validflag,
                               clntcoy,
                               clntnum,
                               payclt,
                               rapaymth,
                               rapayfrq,
                               facthous,
                               bankkey,
                               bankacckey,
                               currcode,
                               dtepay,
                               agntnum,
                               minsta,
                               commdt,
                               termdt,
                               reasst,
                               stmtdt,
                               nxtpay,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getRascoy().toInternal()
					+ getRasnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rascoy);
			what = ExternalData.chop(what, rasnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(rascoy.toInternal());
	nonKeyFiller20.setInternal(rasnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(154);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getValidflag().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getPayclt().toInternal()
					+ getRapaymth().toInternal()
					+ getRapayfrq().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getCurrcode().toInternal()
					+ getDtepay().toInternal()
					+ getAgntnum().toInternal()
					+ getMinsta().toInternal()
					+ getCommdt().toInternal()
					+ getTermdt().toInternal()
					+ getReasst().toInternal()
					+ getStmtdt().toInternal()
					+ getNxtpay().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, rapaymth);
			what = ExternalData.chop(what, rapayfrq);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, dtepay);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, minsta);
			what = ExternalData.chop(what, commdt);
			what = ExternalData.chop(what, termdt);
			what = ExternalData.chop(what, reasst);
			what = ExternalData.chop(what, stmtdt);
			what = ExternalData.chop(what, nxtpay);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRascoy() {
		return rascoy;
	}
	public void setRascoy(Object what) {
		rascoy.set(what);
	}
	public FixedLengthStringData getRasnum() {
		return rasnum;
	}
	public void setRasnum(Object what) {
		rasnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}	
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	public FixedLengthStringData getRapaymth() {
		return rapaymth;
	}
	public void setRapaymth(Object what) {
		rapaymth.set(what);
	}	
	public FixedLengthStringData getRapayfrq() {
		return rapayfrq;
	}
	public void setRapayfrq(Object what) {
		rapayfrq.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}	
	public PackedDecimalData getDtepay() {
		return dtepay;
	}
	public void setDtepay(Object what) {
		setDtepay(what, false);
	}
	public void setDtepay(Object what, boolean rounded) {
		if (rounded)
			dtepay.setRounded(what);
		else
			dtepay.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public PackedDecimalData getMinsta() {
		return minsta;
	}
	public void setMinsta(Object what) {
		setMinsta(what, false);
	}
	public void setMinsta(Object what, boolean rounded) {
		if (rounded)
			minsta.setRounded(what);
		else
			minsta.set(what);
	}	
	public PackedDecimalData getCommdt() {
		return commdt;
	}
	public void setCommdt(Object what) {
		setCommdt(what, false);
	}
	public void setCommdt(Object what, boolean rounded) {
		if (rounded)
			commdt.setRounded(what);
		else
			commdt.set(what);
	}	
	public PackedDecimalData getTermdt() {
		return termdt;
	}
	public void setTermdt(Object what) {
		setTermdt(what, false);
	}
	public void setTermdt(Object what, boolean rounded) {
		if (rounded)
			termdt.setRounded(what);
		else
			termdt.set(what);
	}	
	public FixedLengthStringData getReasst() {
		return reasst;
	}
	public void setReasst(Object what) {
		reasst.set(what);
	}	
	public PackedDecimalData getStmtdt() {
		return stmtdt;
	}
	public void setStmtdt(Object what) {
		setStmtdt(what, false);
	}
	public void setStmtdt(Object what, boolean rounded) {
		if (rounded)
			stmtdt.setRounded(what);
		else
			stmtdt.set(what);
	}	
	public PackedDecimalData getNxtpay() {
		return nxtpay;
	}
	public void setNxtpay(Object what) {
		setNxtpay(what, false);
	}
	public void setNxtpay(Object what, boolean rounded) {
		if (rounded)
			nxtpay.setRounded(what);
		else
			nxtpay.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		rascoy.clear();
		rasnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		validflag.clear();
		clntcoy.clear();
		clntnum.clear();
		payclt.clear();
		rapaymth.clear();
		rapayfrq.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		currcode.clear();
		dtepay.clear();
		agntnum.clear();
		minsta.clear();
		commdt.clear();
		termdt.clear();
		reasst.clear();
		stmtdt.clear();
		nxtpay.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}