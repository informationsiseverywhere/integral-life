package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.B5464TempDAO;
import com.csc.life.reassurance.dataaccess.model.B5464DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class B5464TempDAOImpl extends BaseDAOImpl<B5464DTO> implements	B5464TempDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(B5464TempDAOImpl.class);
    
	private final static String TABLE_NAME = "B5464TEMP";
	
	private void deleteTempData() {
		String sqlStr = "DELETE FROM " + TABLE_NAME;
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	private void insertTempData(String wsaaChdrnumfrm, String wsaaChdrnumto, int wsaaSqlEffdate) {
        StringBuilder sqlStr = new StringBuilder("INSERT INTO ");
        sqlStr.append(TABLE_NAME);
        sqlStr.append(" (CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, SEQNO, RASNUM, RNGMNT, CURRFROM, TRANNO, VALIDFLAG, CURRCODE, CURRTO, RETYPE, RAAMOUNT, CTDATE, CMDATE, REASPER, RECOVAMT, CESTYPE, LRKCLS, RREVDT,");
        sqlStr.append("CHTRANNO, CHCNTTYPE, CHSTATCODE, CHPSTCDE, CHBILLFREQ, CHCNTCURR, CHPOLSUM, CHBILLCHNL, CHOCCDATE, CHPOLINC, CHPTDATE,");
        sqlStr.append("COCRRCD, COCRTABLE, COSUMINS, COJLIFE, COINSTPREM, COCHDRCOY, COCHDRNUM, COLIFE, COCOVERAGE, CORIDER, COPLNSFX,"); 
        sqlStr.append("JLIFE, LIFCNUM, JLIFE2, LIFCNUM2, RAUQNUM,CHUQNUM)");
        sqlStr.append(" SELECT ");
        sqlStr.append("RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO, RA.RASNUM, RA.RNGMNT, RA.CURRFROM, RA.TRANNO, RA.VALIDFLAG, RA.CURRCODE, RA.CURRTO, RA.RETYPE, RA.RAAMOUNT, RA.CTDATE, RA.CMDATE, RA.REASPER, RA.RECOVAMT, RA.CESTYPE, RA.LRKCLS, RA.RREVDT,");
        sqlStr.append("CH.TRANNO CHTRANNO,CH.CNTTYPE CHCNTTYPE,CH.STATCODE CHSTATCODE,CH.PSTCDE CHPSTCDE,CH.BILLFREQ CHBILLFREQ,CH.CNTCURR CHCNTCURR,CH.POLSUM CHPOLSUM,CH.BILLCHNL CHBILLCHNL,CH.OCCDATE CHOCCDATE,CH.POLINC CHPOLINC, CH.PTDATE CHPTDATE,");
        sqlStr.append("CO.CRRCD COCRRCD,CO.CRTABLE COCRTABLE,CO.SUMINS COSUMINS,CO.JLIFE COJLIFE,CO.INSTPREM COINSTPREM,CO.CHDRCOY COCHDRCOY,CO.CHDRNUM COCHDRNUM,CO.LIFE COLIFE,CO.COVERAGE COCOVERAGE,CO.RIDER CORIDER,CO.PLNSFX COPLNSFX,");
        sqlStr.append("L1.JLIFE, L1.LIFCNUM, L2.JLIFE, L2.LIFCNUM, RA.UNIQUE_NUMBER, CH.UNIQUE_NUMBER");
        sqlStr.append(" FROM RACDPF RA");
        sqlStr.append(" JOIN CHDRPF CH ON RA.CHDRCOY = CH.CHDRCOY AND RA.CHDRNUM = CH.CHDRNUM AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG = '1'"); 
        sqlStr.append(" JOIN COVRPF CO ON RA.CHDRCOY = CO.CHDRCOY AND RA.CHDRNUM = CO.CHDRNUM AND RA.LIFE = CO.LIFE AND RA.COVERAGE = CO.COVERAGE AND RA.RIDER = CO.RIDER AND RA.PLNSFX = CO.PLNSFX AND CO.VALIDFLAG <> '2'");
        sqlStr.append(" LEFT JOIN LIFEPF L1 ON RA.CHDRCOY = L1.CHDRCOY AND RA.CHDRNUM = L1.CHDRNUM AND RA.LIFE = L1.LIFE AND L1.VALIDFLAG = '1' AND L1.JLIFE = '00'");
        sqlStr.append(" LEFT JOIN LIFEPF L2 ON RA.CHDRCOY = L2.CHDRCOY AND RA.CHDRNUM = L2.CHDRNUM AND RA.LIFE = L2.LIFE AND L2.VALIDFLAG = '1' AND L2.JLIFE = '01'");
        sqlStr.append(" WHERE RA.CHDRNUM BETWEEN ? AND ? AND (RA.RREVDT < ? OR RA.RREVDT = ?) AND (RA.RREVDT < RA.CTDATE OR RA.RREVDT = RA.CTDATE) AND RA.VALIDFLAG IN ('1','3')");
        sqlStr.append(" GROUP BY RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO, RA.RASNUM, RA.RNGMNT, RA.CURRFROM, RA.TRANNO, RA.VALIDFLAG, RA.CURRCODE, RA.CURRTO, RA.RETYPE, RA.RAAMOUNT, RA.CTDATE, RA.CMDATE, RA.REASPER, RA.RECOVAMT, RA.CESTYPE, RA.LRKCLS, RA.RREVDT,"); 
        sqlStr.append(" CH.TRANNO, CH.CNTTYPE, CH.STATCODE, CH.PSTCDE, CH.BILLFREQ, CH.CNTCURR, CH.POLSUM, CH.BILLCHNL, CH.CNTCURR, CH.OCCDATE, CH.POLINC, CH.PTDATE,");
        sqlStr.append(" CO.CRRCD, CO.CRTABLE, CO.SUMINS, CO.JLIFE, CO.INSTPREM, CO.CHDRCOY, CO.CHDRNUM, CO.LIFE, CO.COVERAGE, CO.RIDER, CO.PLNSFX,");
        sqlStr.append(" L1.JLIFE, L1.LIFCNUM, L2.JLIFE, L2.LIFCNUM, RA.UNIQUE_NUMBER, CH.UNIQUE_NUMBER ");
        LOGGER.info("sql insert dto B5464 {}", sqlStr.toString());//IJTI-1561

		PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		try {
			ps.setString(1, wsaaChdrnumfrm);
			ps.setString(2, wsaaChdrnumto);
			ps.setInt(3, wsaaSqlEffdate);
			ps.setInt(4, wsaaSqlEffdate);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertTempData()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

	}
	
	@Override
	public void buildTempData(String wsaaChdrnumfrm, String wsaaChdrnumto,
			int wsaaSqlEffdate) {
		deleteTempData();
		insertTempData(wsaaChdrnumfrm, wsaaChdrnumto, wsaaSqlEffdate);
	}

	@Override
	public List<B5464DTO> findTempDataResult(int minRecord, int maxRecord) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM, TE.LIFE, TE.COVERAGE, TE.RIDER, TE.PLNSFX, TE.SEQNO) ROWNM, TE.*");
        sqlStr.append(" FROM ");
        sqlStr.append(TABLE_NAME);
        sqlStr.append(" TE) RS WHERE ROWNM>=? AND ROWNM<? ORDER BY ROWNM");
		PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		List<B5464DTO> dtoList = new LinkedList<B5464DTO>();
		ResultSet rs = null;
        try {
        	ps.setInt(1, minRecord);
        	ps.setInt(2, maxRecord);

        	rs = ps.executeQuery();

            while (rs.next()) {
                B5464DTO dto = new B5464DTO();
                dto.setSqlChdrcoy(rs.getString("CHDRCOY"));
                dto.setSqlChdrnum(rs.getString("CHDRNUM"));
                dto.setSqlLife(rs.getString("LIFE"));
                dto.setSqlCoverage(rs.getString("COVERAGE"));
                dto.setSqlRider(rs.getString("RIDER"));
                dto.setSqlPlnsfx(rs.getInt("PLNSFX"));
                dto.setSqlSeqno(rs.getInt("SEQNO"));
                dto.setSqlTranno(rs.getInt("TRANNO"));
                dto.setSqlRasnum(rs.getString("RASNUM"));
                dto.setSqlRngmnt(rs.getString("RNGMNT"));
                dto.setSqlValidflag(rs.getString("VALIDFLAG"));
                dto.setSqlCurrcode(rs.getString("CURRCODE"));
                dto.setSqlCurrfrom(rs.getInt("CURRFROM"));
                dto.setSqlCurrto(rs.getInt("CURRTO"));
                dto.setSqlRetype(rs.getString("RETYPE"));
                dto.setSqlRaamount(rs.getBigDecimal("RAAMOUNT"));
                dto.setSqlCtdate(rs.getInt("CTDATE"));
                dto.setSqlCmdate(rs.getInt("CMDATE"));
                dto.setSqlReasper(rs.getBigDecimal("REASPER"));
                dto.setSqlRrevdt(rs.getInt("RREVDT"));
                dto.setSqlRecovamt(rs.getBigDecimal("RECOVAMT"));
                dto.setSqlCestype(rs.getString("CESTYPE"));
                dto.setSqlLrkcls(rs.getString("LRKCLS"));
                
                dto.setSqlChTranno(rs.getInt("CHTRANNO"));
                dto.setSqlChCnttype(rs.getString("CHCNTTYPE"));
                dto.setSqlChStatcode(rs.getString("CHSTATCODE"));
                dto.setSqlChPstatcode(rs.getString("CHPSTCDE"));
                dto.setSqlChBillfreq(rs.getString("CHBILLFREQ"));
                dto.setSqlChCntcurr(rs.getString("CHCNTCURR"));
                dto.setSqlChPolsum(rs.getInt("CHPOLSUM"));
                dto.setSqlChBillchnl(rs.getString("CHBILLCHNL"));
                dto.setSqlChOccdate(rs.getInt("CHOCCDATE"));
                dto.setSqlChPolinc(rs.getInt("CHPOLINC"));
                dto.setSqlChPtdate(rs.getInt("CHPTDATE"));
                
                dto.setSqlCoCrrcd(rs.getInt("COCRRCD"));
                dto.setSqlCoCrtable(rs.getString("COCRTABLE"));
                dto.setSqlCoSumins(rs.getBigDecimal("COSUMINS"));
                dto.setSqlCoJlife(rs.getString("COJLIFE"));
                dto.setSqlCoInstprem(rs.getBigDecimal("COINSTPREM"));
                dto.setSqlCoChdrcoy(rs.getString("COCHDRCOY"));
                dto.setSqlCoChdrnum(rs.getString("COCHDRNUM"));
                dto.setSqlCoLife(rs.getString("COLIFE"));
                dto.setSqlCoCoverage(rs.getString("COCOVERAGE"));
                dto.setSqlCoRider(rs.getString("CORIDER"));
                dto.setSqlCoPlnsfx(rs.getInt("COPLNSFX"));
                
                dto.setSqlJlife1(rs.getString("JLIFE"));
                dto.setSqlLifcnum1(rs.getString("LIFCNUM"));
                dto.setSqlJlife2(rs.getString("JLIFE2"));
                dto.setSqlLifcnum2(rs.getString("LIFCNUM2"));
                
                dto.setSqlRaUniqueNum(rs.getLong("RAUQNUM"));
                dto.setSqlChUniqueNum(rs.getLong("CHUQNUM"));
                
                dtoList.add(dto);
            }

        } catch (SQLException e) {
            LOGGER.error("findTempDataResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return dtoList;
	}


}