/*
 * File: P5459.java
 * Date: 30 August 2009 0:27:42
 * Author: Quipoz Limited
 * 
 * Class transformed from P5459.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RacdcsnTableDAM;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.screens.S5459ScreenVars;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Facultative Cession Maintenance.
*
* This is a subfile selection program controlled by OPTSWCH.
* It is used to create, modify, delete or enquire upon inactive
* facultative RACD records (i.e. VALIDFLAG = '3' and CESTYPE =
* '2'). This program will never allow the user to access treaty
* RACD records.
*
* This program is used in Alterations and so does a retrieve
* on COVTs.
*
* This program is accessed from P5466,the Facultative Reassurance
* Confirmation Screen, if additional Facultative Reassurance is
* necessary.
*
* The user has three options:
*
* 1 - Modify
*
* 2 - Delete
*
* 3 - Create (function key F10 will enable the user to add
*     a new RACD record).
*
*****************************************************************
* </pre>
*/
public class P5459 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5459");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaOpt = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);

	private FixedLengthStringData wsaaCvChdrkey = new FixedLengthStringData(11);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaCvChdrkey, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCvChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCvChdrkey, 2);
	private FixedLengthStringData wsaaCvChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCvChdrkey, 3);
	private FixedLengthStringData wsaaCvLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCvCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCvRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCvPlnsfx = new PackedDecimalData(4, 0);
	private String wsaaOptionSelected = "";

	private FixedLengthStringData wsaaValidFkey = new FixedLengthStringData(1);
	private Validator validFkey = new Validator(wsaaValidFkey, "Y");

	private FixedLengthStringData wsaaCompSelected = new FixedLengthStringData(1);
	private Validator compSelected = new Validator(wsaaCompSelected, "Y");
	private String r060 = "R060";
		/* TABLES */
	private String t5448 = "T5448";
	private String t5688 = "T5688";
	private String racdcsnrec = "RACDCSNREC";
	private String racdlnbrec = "RACDLNBREC";
	private String itdmrec = "ITEMREC";
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/Rider details - Major Alts*/
	//private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Cession Details For Cession Calculation*/
	private RacdcsnTableDAM racdcsnIO = new RacdcsnTableDAM();
		/*Reassurance Cession Details New Busines*/
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private T5448rec t5448rec = new T5448rec();
	private Wssplife wssplife = new Wssplife();
	private S5459ScreenVars sv = ScreenProgram.getScreenVars( S5459ScreenVars.class);
	
	//ILB-456 start 
	private Covrpf covrpf = new Covrpf();
	private int covrpfCount = 0;
	private List<Covrpf> covrpfList;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		next1380, 
		exit1390, 
		preExit, 
		exit2090, 
		updateErrorIndicators2120, 
		optswch4080, 
		exit4090
	}

	public P5459() {
		super();
		screenVars = sv;
		new ScreenModel("S5459", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			subfileLoad1080();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5459", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		wsaaValidFkey.set("N");
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		for (wsaaOpt.set(1); !(isGT(wsaaOpt,3)); wsaaOpt.add(1)){
			sv.optdsc[wsaaOpt.toInt()].set(SPACES);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			sv.optdsc[1].set(optswchrec.optsDsc[1]);
			sv.optdscx.set(SPACES);
		}
		if (isEQ(wsspcomn.flag,"M")
		|| isEQ(wsspcomn.flag,"A")) {
			sv.optdsc01.set(optswchrec.optsDsc[1]);
			sv.optdsc02.set(optswchrec.optsDsc[2]);
			sv.optdsc03.set(optswchrec.optsDsc[3]);
			sv.optdscx.set(optswchrec.optsDsc[4]);
		}
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		covtmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		wsaaCvChdrcoy.set(covtmjaIO.getChdrcoy());
		wsaaCvChdrnum.set(covtmjaIO.getChdrnum());
		wsaaCvLife.set(covtmjaIO.getLife());
		wsaaCvCoverage.set(covtmjaIO.getCoverage());
		wsaaCvRider.set(covtmjaIO.getRider());
		wsaaCvPlnsfx.set(ZERO);
		wssplife.bigAmt.set(covtmjaIO.getSumins());
		sv.sumins01.set(covtmjaIO.getSumins());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(chdrpf.getCnttype());/* IJTI-1523 */
		stringVariable1.append(covtmjaIO.getCrtable().toString());
		wsaaT5448Item.setLeft(stringVariable1.toString());
		sv.sumins02.set(ZERO);
		wssplife.chdrky.set(wsaaCvChdrkey);
		wssplife.life.set(wsaaCvLife);
		sv.life.set(wsaaCvLife);
		wssplife.coverage.set(wsaaCvCoverage);
		sv.coverage.set(wsaaCvCoverage);
		wssplife.rider.set(wsaaCvRider);
		sv.rider.set(wsaaCvRider);
		wssplife.planSuffix.set(wsaaCvPlnsfx);
		wssplife.cntcurr.set(chdrpf.getCntcurr());
		wsspcomn.tranno.set(chdrpf.getTranno());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		wsspcomn.currfrom.set(chdrpf.getPtdate());
		wssplife.occdate.set(chdrpf.getOccdate());
		readT54481100();
		readT56881200();
		wssplife.lrkcls.set(t5448rec.rrsktyp);
		wssplife.rrevfrq.set(t5448rec.rrevfrq);
	}

protected void subfileLoad1080()
	{
		racdcsnIO.setParams(SPACES);
		racdcsnIO.setChdrcoy(wsaaCvChdrcoy);
		racdcsnIO.setChdrnum(wsaaCvChdrnum);
		racdcsnIO.setLife(wsaaCvLife);
		racdcsnIO.setCoverage(wsaaCvCoverage);
		racdcsnIO.setRider(wsaaCvRider);
		racdcsnIO.setPlanSuffix(wsaaCvPlnsfx);
		racdcsnIO.setCestype(SPACES);
		racdcsnIO.setSeqno(ZERO);
		racdcsnIO.setFormat(racdcsnrec);
		racdcsnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdcsnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdcsnIO.getStatuz(),varcom.endp))) {
			listRacd1300();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void readT54481100()
	{
		t54481110();
	}

protected void t54481110()
	{
		readCovr1101();
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5448);
		itdmIO.setItemitem(wsaaT5448Item);
		/*if (isEQ(covrmjaIO.getStatuz(),varcom.mrnf)) {
			itdmIO.setItmfrm(covtmjaIO.getEffdate());
		}*/
		if (covrpf == null) {
			itdmIO.setItmfrm(covtmjaIO.getEffdate());
		}
		else {
			itdmIO.setItmfrm(covrpf.getCrrcd());
		}
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaT5448Item,itdmIO.getItemitem())
		|| isNE(chdrpf.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(chdrpf.getChdrcoy());
			itdmIO.setItemtabl(t5448);
			itdmIO.setStatuz(r060);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
	}

protected void readCovr1101()
	{
		para1101();
	}

protected void para1101()
	{
		covrpf.setChdrcoy(covtmjaIO.getChdrcoy().toString());
		covrpf.setChdrnum(covtmjaIO.getChdrnum().toString());
		covrpf.setLife(covtmjaIO.getLife().toString());
		covrpf.setCoverage(covtmjaIO.getCoverage().toString());
		covrpf.setRider(covtmjaIO.getRider().toString());
		covrpf.setPlanSuffix(covtmjaIO.getPlanSuffix().toInt());
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),"1");
		if (covrpf == null) {
			fatalError600();
		}
		/*covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void readT56881200()
	{
		tableRead1201();
	}

protected void tableRead1201()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
	}

protected void listRacd1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFirstRacd1310();
				}
				case next1380: {
					next1380();
				}
				case exit1390: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFirstRacd1310()
	{
		SmartFileCode.execute(appVars, racdcsnIO);
		if (isNE(racdcsnIO.getStatuz(),varcom.oK)
		&& isNE(racdcsnIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdcsnIO.getParams());
			fatalError600();
		}
		if (isNE(racdcsnIO.getChdrcoy(),wsaaCvChdrcoy)
		|| isNE(racdcsnIO.getChdrnum(),wsaaCvChdrnum)
		|| isNE(racdcsnIO.getLife(),wsaaCvLife)
		|| isNE(racdcsnIO.getCoverage(),wsaaCvCoverage)
		|| isNE(racdcsnIO.getRider(),wsaaCvRider)
		|| isNE(racdcsnIO.getPlanSuffix(),wsaaCvPlnsfx)
		|| isEQ(racdcsnIO.getStatuz(),varcom.endp)) {
			racdcsnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1390);
		}
		sv.sumins02.add(racdcsnIO.getRaAmount());
		if (isNE(racdcsnIO.getValidflag(),"3")
		|| isNE(racdcsnIO.getCestype(),"2")) {
			goTo(GotoLabel.next1380);
		}
		sv.cmdate.set(racdcsnIO.getCmdate());
		sv.rasnum.set(racdcsnIO.getRasnum());
		sv.rngmnt.set(racdcsnIO.getRngmnt());
		sv.raAmount.set(racdcsnIO.getRaAmount());
		sv.ovrdind.set(racdcsnIO.getOvrdind());
		sv.hrrn.set(racdcsnIO.getRrn());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5459", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void next1380()
	{
		racdcsnIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isEQ(wsaaScrnStatuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaScrnStatuz,"SWCH")) {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("F");
			optswchrec.optsSelCode.set(wsaaScrnStatuz);
			optswchrec.optsSelOptno.set(ZERO);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				scrnparams.errorCode.set(optswchrec.optsStatuz);
				wsspcomn.edterror.set("Y");
			}
			else {
				wsaaValidFkey.set("Y");
			}
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5459", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		wsaaCompSelected.set("N");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNextRecord2130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		wsaaCompSelected.set("Y");
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5459", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextRecord2130()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5459", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case optswch4080: {
					optswch4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsaaOptionSelected = "N";
		if (validFkey.isTrue()) {
			optswchrec.optsSelType.set("F");
			optswchrec.optsSelCode.set(wsaaScrnStatuz);
			optswchrec.optsSelOptno.set(ZERO);
			wsaaValidFkey.set("N");
			wsaaOptionSelected = "Y";
			goTo(GotoLabel.optswch4080);
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			goTo(GotoLabel.optswch4080);
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			lineSelect4200();
			wsaaOptionSelected = "Y";
		}
		if (isEQ(sv.select,"2")) {
			optswchrec.optsSelOptno.set(2);
			lineSelect4200();
			wsaaOptionSelected = "Y";
		}
		if (isEQ(sv.select,"3")) {
			optswchrec.optsSelOptno.set(3);
			lineSelect4200();
			wsaaOptionSelected = "Y";
		}
	}

protected void optswch4080()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		&& isEQ(wsaaOptionSelected,"N")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			programStack4300();
			goTo(GotoLabel.exit4090);
		}
		programStack4300();
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5459", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,scrnparams.statuz)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void lineSelect4200()
	{
		line4210();
	}

protected void line4210()
	{
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		sv.select.set(SPACES);
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setRrn(sv.hrrn);
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		racdlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		scrnparams.function.set(varcom.supd);
		screenio4100();
	}

protected void programStack4300()
	{
		/*STCK*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*EXIT*/
	}
}
