package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST560
 * @version 1.0 generated on 30/08/09 07:27
 * @author Quipoz
 */
public class St560ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(221);
	public FixedLengthStringData dataFields = new FixedLengthStringData(61).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,19);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,27);
	public ZonedDecimalData fromdate = DD.fromdate.copyToZonedDecimal().isAPartOf(dataFields,35);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,43);
	public ZonedDecimalData todate = DD.todate.copyToZonedDecimal().isAPartOf(dataFields,53);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 61);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData fromdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 101);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] fromdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fromdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);

	public LongData St560screenWritten = new LongData(0);
	public LongData St560protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St560ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(todateOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fromdateOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, jobq, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, todate, fromdate};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut, todateOut, fromdateOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr, todateErr, fromdateErr};
		screenDateFields = new BaseData[] {effdate, todate, fromdate};
		screenDateErrFields = new BaseData[] {effdateErr, todateErr, fromdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp, todateDisp, fromdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = St560screen.class;
		protectRecord = St560protect.class;
	}

}
