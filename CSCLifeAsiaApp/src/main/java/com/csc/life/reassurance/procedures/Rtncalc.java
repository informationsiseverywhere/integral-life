/*
 * File: Rtncalc.java
 * Date: 30 August 2009 2:13:50
 * Author: Quipoz Limited
 * 
 * Class transformed from RTNCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.RacrTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.life.reassurance.dataaccess.LirrTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhTableDAM;
import com.csc.life.reassurance.recordstructures.Rtncalcrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Reassurance Retention Calculation Subroutine.
*
* Overview
* ========
*
* This subroutine is called from CSNCALC (cession calculation
* subroutine) to calculate the amount of retention that is
* available for either the life company (BASE function) or a
* reassurer (RESR function) under a specific arrangement for a
* given life. This will be used to determine maximum retentions
* and cessions that may be applied to a sum reassured.
*
* Linkage Area
* ============
*
* FUNCTION       PIC X(05)
* STATUZ         PIC X(04)
* COMPANY        PIC X(01).
* CLNTCOY        PIC X(01).
* CLNTNUM        PIC X(08).
* RISK-CLASS     PIC X(04).
* REASSURER      PIC X(08).
* ARRANGEMENT    PIC X(04).
* EFFDATE        PIC S9(08) COMP-3.
* CURRENCY       PIC X(03).
* AVAILABLE      PIC S9(11)V9(02) COMP-3.
* DISC-RETENTION PIC S9(11)V9(02) COMP-3.
*
* These fields are all contained within RTNCALCREC.
*
* Processing
* ==========
*
* BASE function:
*
* Read RACR for sub-standard retentions.
* If no active RACR records, read T5446 for retention defaults.
* Read LRRH records to determine how much has already been
* retained for this life.
* If there are no LRRH records, then the amount retained is zero.
* Otherwise, convert the LRRH amounts into the required currency
* (if necessary) and subtract these from the available retention.
*
* RESR function:
*
* Read T5449 to get arrangement details.
* Search through the array of reassurers from T5449 for a match
* on reassurer number.
* Read the LIRR records to determine cessions already made for
* this life to this reassurer and arrangement.
* If no LIRR found, then there are no current cessions so set
* the amount available to the maximum cession amount from T5449.
* Otherwise, convert the reassurer amounts from the LIRR records
* into the required currency (if necessary) and subtract these
* amounts from the T5449 maximum cession amount to determine the
* available retention.
*
*****************************************************************
* </pre>
*/
public class Rtncalc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "RTNCALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaT5449Ix = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPosition = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaReassurerFound = new FixedLengthStringData(1).init("N");
	private Validator reassurerFound = new Validator(wsaaReassurerFound, "Y");
		/* FORMATS */
	private String itdmrec = "ITEMREC";
	private String lirrrec = "LIRRREC";
	private String lrrhrec = "LRRHREC";
	private String racrrec = "RACRREC";
		/* TABLES */
	private String t5446 = "T5446";
	private String t5449 = "T5449";
		/* ERRORS */
	private String e049 = "E049";
	private String r051 = "R051";
	private String r057 = "R057";
	private String r059 = "R059";
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Reassurer Experience History*/
	private LirrTableDAM lirrIO = new LirrTableDAM();
		/*Life Retention & Reassurance History*/
	private LrrhTableDAM lrrhIO = new LrrhTableDAM();
		/*Sub Standard Life Retention Logical*/
	private RacrTableDAM racrIO = new RacrTableDAM();
	private Rtncalcrec rtncalcrec = new Rtncalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5446rec t5446rec = new T5446rec();
	private T5449rec t5449rec = new T5449rec();
	private Varcom varcom = new Varcom();

	public Rtncalc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		rtncalcrec.rtncalcRec = convertAndSetParam(rtncalcrec.rtncalcRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		/*MAINLINE-START*/
		rtncalcrec.statuz.set(varcom.oK);
		rtncalcrec.available.set(ZERO);
		if (isNE(rtncalcrec.function,"BASE")
		&& isNE(rtncalcrec.function,"RESR")) {
			syserrrec.statuz.set(e049);
			syserr570();
		}
		if (isEQ(rtncalcrec.function,"BASE")) {
			baseFunction1000();
		}
		else {
			if (isEQ(rtncalcrec.function,"RESR")) {
				resrFunction2000();
			}
		}
		/*MAINLINE-EXIT*/
		exitProgram();
	}

protected void baseFunction1000()
	{
		base1010();
	}

protected void base1010()
	{
		racrIO.setParams(SPACES);
		racrIO.setClntpfx(fsupfxcpy.clnt);
		racrIO.setClntcoy(rtncalcrec.clntcoy);
		racrIO.setClntnum(rtncalcrec.clntnum);
		racrIO.setLrkcls(rtncalcrec.riskClass);
		racrIO.setCurrfrom(ZERO);
		racrIO.setFormat(racrrec);
		racrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racrIO.setFitKeysSearch("CLNTPFX", "CLNTCOY", "CLNTNUM", "LRKCLS");
		SmartFileCode.execute(appVars, racrIO);
		if (isNE(racrIO.getStatuz(),varcom.oK)
		&& isNE(racrIO.getStatuz(),varcom.endp)) {
			dbError580();
		}
		if (isNE(racrIO.getClntpfx(),fsupfxcpy.clnt)
		|| isNE(racrIO.getClntcoy(),rtncalcrec.clntcoy)
		|| isNE(racrIO.getClntnum(),rtncalcrec.clntnum)
		|| isNE(racrIO.getLrkcls(),rtncalcrec.riskClass)
		|| isEQ(racrIO.getStatuz(),varcom.endp)) {
			racrIO.setStatuz(varcom.endp);
		}
		if (isEQ(racrIO.getStatuz(),varcom.oK)) {
			rtncalcrec.currency.set(racrIO.getCurrcode());
			rtncalcrec.available.set(racrIO.getRetn());
			rtncalcrec.discRetention.set(racrIO.getDiscretn());
		}
		else {
			readT54461100();
			rtncalcrec.currency.set(t5446rec.currcode);
			rtncalcrec.available.set(t5446rec.retn);
			rtncalcrec.discRetention.set(t5446rec.discretn);
		}
		lrrhIO.setParams(SPACES);
		lrrhIO.setClntpfx(fsupfxcpy.clnt);
		lrrhIO.setClntcoy(rtncalcrec.clntcoy);
		lrrhIO.setClntnum(rtncalcrec.clntnum);
		lrrhIO.setCompany(rtncalcrec.company);
		lrrhIO.setLrkcls(rtncalcrec.riskClass);
		lrrhIO.setFormat(lrrhrec);
		lrrhIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhIO.setFitKeysSearch("CLNTCOY", "CLNTNUM", "COMPANY", "LRKCLS");
		SmartFileCode.execute(appVars, lrrhIO);
		if (isNE(lrrhIO.getStatuz(),varcom.oK)
		&& isNE(lrrhIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lrrhIO.getParams());
			dbError580();
		}
		while ( !(isEQ(lrrhIO.getStatuz(),varcom.endp)
		|| isNE(lrrhIO.getClntcoy(),rtncalcrec.clntcoy)
		|| isNE(lrrhIO.getClntnum(),rtncalcrec.clntnum)
		|| isNE(lrrhIO.getCompany(),rtncalcrec.company)
		|| isNE(lrrhIO.getLrkcls(),rtncalcrec.riskClass))) {
			if (isNE(lrrhIO.getCurrency(),rtncalcrec.currency)) {
				conlinkrec.amountIn.set(lrrhIO.getSsretn());
				conlinkrec.currIn.set(lrrhIO.getCurrency());
				conlinkrec.currOut.set(rtncalcrec.currency);
				callXcvrt1200();
				rtncalcrec.available.subtract(conlinkrec.amountOut);
			}
			else {
				rtncalcrec.available.subtract(lrrhIO.getSsretn());
			}
			lrrhIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lrrhIO);
			if (isNE(lrrhIO.getStatuz(),varcom.oK)
			&& isNE(lrrhIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lrrhIO.getParams());
				dbError580();
			}
		}
		
		if (isLT(rtncalcrec.available,0)) {
			rtncalcrec.available.set(0);
		}
	}

protected void readT54461100()
	{
		t54461110();
	}

protected void t54461110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rtncalcrec.company);
		itdmIO.setItemtabl(t5446);
		itdmIO.setItemitem(rtncalcrec.riskClass);
		itdmIO.setItmfrm(rtncalcrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(rtncalcrec.riskClass,itdmIO.getItemitem())
		|| isNE(rtncalcrec.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5446)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(rtncalcrec.riskClass);
			itdmIO.setItemcoy(rtncalcrec.company);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(r051);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callXcvrt1200()
	{
		/*CALL*/
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(rtncalcrec.effdate);
		conlinkrec.company.set(rtncalcrec.company);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr570();
		}
		/*EXIT*/
	}

protected void resrFunction2000()
	{
		resr2010();
	}

protected void resr2010()
	{
		readT54492100();
		wsaaReassurerFound.set("N");
		for (wsaaT5449Ix.set(1); !(isGT(wsaaT5449Ix,10)
		|| reassurerFound.isTrue()); wsaaT5449Ix.add(1)){
			if (isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()],rtncalcrec.reassurer)) {
				wsaaReassurerFound.set("Y");
				wsaaPosition.set(wsaaT5449Ix);
			}
		}
		if (!reassurerFound.isTrue()) {
			syserrrec.statuz.set(r059);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(rtncalcrec.arrangement.toString());
			stringVariable1.append(SPACES);
			stringVariable1.append(rtncalcrec.reassurer.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserr570();
		}
		lirrIO.setParams(SPACES);
		lirrIO.setCompany(rtncalcrec.company);
		lirrIO.setRasnum(rtncalcrec.reassurer);
		lirrIO.setRngmnt(rtncalcrec.arrangement);
		lirrIO.setClntpfx(fsupfxcpy.clnt);
		lirrIO.setClntcoy(rtncalcrec.clntcoy);
		lirrIO.setClntnum(rtncalcrec.clntnum);
		lirrIO.setFormat(lirrrec);
		lirrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lirrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lirrIO.setFitKeysSearch("RASNUM", "RNGMNT", "CLNTCOY", "CLNTNUM");
		SmartFileCode.execute(appVars, lirrIO);
		if (isNE(lirrIO.getStatuz(),varcom.oK)
		&& isNE(lirrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lirrIO.getParams());
			dbError580();
		}
		rtncalcrec.available.set(t5449rec.relimit[wsaaPosition.toInt()]);
		while ( !(isEQ(lirrIO.getStatuz(),varcom.endp)
		|| isNE(lirrIO.getRasnum(),rtncalcrec.reassurer)
		|| isNE(lirrIO.getRngmnt(),rtncalcrec.arrangement)
		|| isNE(lirrIO.getClntcoy(),rtncalcrec.clntcoy)
		|| isNE(lirrIO.getClntnum(),rtncalcrec.clntnum))) {
			if (isNE(lirrIO.getCurrency(),t5449rec.currcode)) {
				conlinkrec.amountIn.set(lirrIO.getRaAmount());
				conlinkrec.currIn.set(lirrIO.getCurrency());
				conlinkrec.currOut.set(t5449rec.currcode);
				callXcvrt1200();
				rtncalcrec.available.subtract(conlinkrec.amountOut);
			}
			else {
				rtncalcrec.available.subtract(lirrIO.getRaAmount());
			}
			lirrIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lirrIO);
			if (isNE(lirrIO.getStatuz(),varcom.oK)
			&& isNE(lirrIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lirrIO.getParams());
				dbError580();
			}
		}
		
		if (isLT(rtncalcrec.available,0)) {
			rtncalcrec.available.set(0);
		}
		rtncalcrec.currency.set(t5449rec.currcode);
		rtncalcrec.discRetention.set(0);
	}

protected void readT54492100()
	{
		t54492110();
	}

protected void t54492110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rtncalcrec.company);
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(rtncalcrec.arrangement);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError580();
		}
		if (isNE(itdmIO.getItemcoy(),rtncalcrec.company)
		|| isNE(itdmIO.getItemtabl(),t5449)
		|| isNE(itdmIO.getItemitem(),rtncalcrec.arrangement)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(r057);
			itdmIO.setItemcoy(rtncalcrec.company);
			itdmIO.setItemtabl(t5449);
			itdmIO.setItemitem(rtncalcrec.arrangement);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5449rec.t5449Rec.set(itdmIO.getGenarea());
		}
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rtncalcrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rtncalcrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
