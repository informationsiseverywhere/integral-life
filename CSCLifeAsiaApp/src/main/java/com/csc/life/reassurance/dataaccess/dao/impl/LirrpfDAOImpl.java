package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.LirrpfDAO;
import com.csc.life.reassurance.dataaccess.model.Lirrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LirrpfDAOImpl extends BaseDAOImpl<Lirrpf> implements LirrpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LirrpfDAOImpl.class);
    
    public List<Lirrpf> searchlLirrconRecord(String coy, String chdrnum) {
		String sql = "SELECT UNIQUE_NUMBER FROM LIRRCON WHERE COMPANY=? AND CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		List<Lirrpf> searchResult = new LinkedList<Lirrpf>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Lirrpf t = new Lirrpf();
				t.setUniqueNumber(rs.getLong(1));
				searchResult.add(t);
			}

		} catch (SQLException e) {
			LOGGER.error("searchlLirrconRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return searchResult;
	}   
	
	public void deleteLirrpfRecord(List<Lirrpf> dataList){
		String sql = "DELETE FROM LIRRPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Lirrpf l:dataList){
				ps.setLong(1, l.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteLirrpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public void updateLirrpfBulk(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx, int busDate) {
			String sqlLirrUpdate = "UPDATE LIRRPF SET VALIDFLAG='2',CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE  COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG='1' ";
			PreparedStatement psLirrUpdate = getPrepareStatement(sqlLirrUpdate);
			try {
					psLirrUpdate.setInt(1, busDate);
					psLirrUpdate.setString(2, getJobnm());
					psLirrUpdate.setString(3, getUsrprf());
					psLirrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
					psLirrUpdate.setString(5, coy);
					psLirrUpdate.setString(6, chdrnum);
					psLirrUpdate.setString(7, life);
					psLirrUpdate.setString(8, coverage);
					psLirrUpdate.setString(9, rider);
					psLirrUpdate.setString(10, plnsfx);
					psLirrUpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateLirrpfBulk()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psLirrUpdate, null);
			}		
	}
	
	@Override
	public void updateLirrpfByRngmnt(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx, int busDate, String rngmnt) {
			String sqlLirrUpdate = "UPDATE LIRRPF SET VALIDFLAG='2', CURRTO=?, JOBNM=?, USRPRF=?, DATIME=? "
					+ "WHERE COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? "
					+ "AND VALIDFLAG='1' AND RNGMNT = ? ";
			PreparedStatement psLirrUpdate = getPrepareStatement(sqlLirrUpdate);
			try {
					psLirrUpdate.setInt(1, busDate);
					psLirrUpdate.setString(2, getJobnm());
					psLirrUpdate.setString(3, getUsrprf());
					psLirrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
					psLirrUpdate.setString(5, coy);
					psLirrUpdate.setString(6, chdrnum);
					psLirrUpdate.setString(7, life);
					psLirrUpdate.setString(8, coverage);
					psLirrUpdate.setString(9, rider);
					psLirrUpdate.setString(10, plnsfx);
					psLirrUpdate.setString(11, rngmnt);
					psLirrUpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateLirrpfByRngmnt()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psLirrUpdate, null);
			}		
	}
}