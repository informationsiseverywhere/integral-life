package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:41
 * Description:
 * Copybook name: RECOCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Recoclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData recoclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData recoclmKey = new FixedLengthStringData(64).isAPartOf(recoclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData recoclmChdrcoy = new FixedLengthStringData(1).isAPartOf(recoclmKey, 0);
  	public FixedLengthStringData recoclmChdrnum = new FixedLengthStringData(8).isAPartOf(recoclmKey, 1);
  	public FixedLengthStringData recoclmLife = new FixedLengthStringData(2).isAPartOf(recoclmKey, 9);
  	public FixedLengthStringData recoclmCoverage = new FixedLengthStringData(2).isAPartOf(recoclmKey, 11);
  	public FixedLengthStringData recoclmRider = new FixedLengthStringData(2).isAPartOf(recoclmKey, 13);
  	public PackedDecimalData recoclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(recoclmKey, 15);
  	public PackedDecimalData recoclmSeqno = new PackedDecimalData(2, 0).isAPartOf(recoclmKey, 18);
  	public PackedDecimalData recoclmCostdate = new PackedDecimalData(8, 0).isAPartOf(recoclmKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(recoclmKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(recoclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		recoclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}