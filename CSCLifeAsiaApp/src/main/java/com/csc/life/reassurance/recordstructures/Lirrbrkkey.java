package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:12
 * Description:
 * Copybook name: LIRRBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lirrbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lirrbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lirrbrkKey = new FixedLengthStringData(64).isAPartOf(lirrbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData lirrbrkCompany = new FixedLengthStringData(1).isAPartOf(lirrbrkKey, 0);
  	public FixedLengthStringData lirrbrkChdrnum = new FixedLengthStringData(8).isAPartOf(lirrbrkKey, 1);
  	public PackedDecimalData lirrbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lirrbrkKey, 9);
  	public FixedLengthStringData lirrbrkLife = new FixedLengthStringData(2).isAPartOf(lirrbrkKey, 12);
  	public FixedLengthStringData lirrbrkCoverage = new FixedLengthStringData(2).isAPartOf(lirrbrkKey, 14);
  	public FixedLengthStringData lirrbrkRider = new FixedLengthStringData(2).isAPartOf(lirrbrkKey, 16);
  	public FixedLengthStringData lirrbrkRasnum = new FixedLengthStringData(8).isAPartOf(lirrbrkKey, 18);
  	public FixedLengthStringData lirrbrkRngmnt = new FixedLengthStringData(4).isAPartOf(lirrbrkKey, 26);
  	public FixedLengthStringData lirrbrkClntpfx = new FixedLengthStringData(2).isAPartOf(lirrbrkKey, 30);
  	public FixedLengthStringData lirrbrkClntcoy = new FixedLengthStringData(1).isAPartOf(lirrbrkKey, 32);
  	public FixedLengthStringData lirrbrkClntnum = new FixedLengthStringData(8).isAPartOf(lirrbrkKey, 33);
  	public FixedLengthStringData filler = new FixedLengthStringData(23).isAPartOf(lirrbrkKey, 41, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lirrbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lirrbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}