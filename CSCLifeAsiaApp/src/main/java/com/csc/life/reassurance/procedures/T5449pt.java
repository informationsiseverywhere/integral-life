/*
 * File: T5449pt.java
 * Date: 30 August 2009 2:20:55
 * Author: Quipoz Limited
 * 
 * Class transformed from T5449PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5449.
*
*
*****************************************************************
* </pre>
*/
public class T5449pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Reassurance Arrangement                    S5449");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(69);
	private FixedLengthStringData filler9 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Arrangement Type:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 19);
	private FixedLengthStringData filler10 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 20, FILLER).init("   Risk Class:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 35);
	private FixedLengthStringData filler11 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine004, 39, FILLER).init("   Arrangement Cession Type:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 68);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(68);
	private FixedLengthStringData filler12 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Sub Standard Lives Allowed?:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 30);
	private FixedLengthStringData filler13 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 42, FILLER).init("Sub Standard Limits :");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 64);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(80);
	private FixedLengthStringData filler15 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Premium Refund Basis:");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler16 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 27, FILLER).init(" Currency:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 38);
	private FixedLengthStringData filler17 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine006, 41, FILLER).init(" Company % Share if Quota Based:");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(79);
	private FixedLengthStringData filler18 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Facultative Schedule Required?:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine007, 42, FILLER).init("Facultative Schedule Letter:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 71);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(65);
	private FixedLengthStringData filler21 = new FixedLengthStringData(65).isAPartOf(wsaaPrtLine008, 0, FILLER).init("    Reasurer  Quota %       Retention           Premium Basis");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(60);
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" 1.");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 5);
	private FixedLengthStringData filler23 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 56);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(60);
	private FixedLengthStringData filler26 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" 2.");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 5);
	private FixedLengthStringData filler27 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 56);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(60);
	private FixedLengthStringData filler30 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" 3.");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 5);
	private FixedLengthStringData filler31 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 56);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(60);
	private FixedLengthStringData filler34 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" 4.");
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 5);
	private FixedLengthStringData filler35 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 56);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(60);
	private FixedLengthStringData filler38 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" 5.");
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 5);
	private FixedLengthStringData filler39 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 56);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(60);
	private FixedLengthStringData filler42 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" 6.");
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 5);
	private FixedLengthStringData filler43 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 56);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(60);
	private FixedLengthStringData filler46 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" 7.");
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 5);
	private FixedLengthStringData filler47 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 56);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(60);
	private FixedLengthStringData filler50 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" 8.");
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 5);
	private FixedLengthStringData filler51 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine016, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 56);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(60);
	private FixedLengthStringData filler54 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" 9.");
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 5);
	private FixedLengthStringData filler55 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine017, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine017, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 56);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(60);
	private FixedLengthStringData filler58 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" 10.");
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine018, 5);
	private FixedLengthStringData filler59 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine018, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine018, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine018, 32).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 56);

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(28);
	private FixedLengthStringData filler62 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine019, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5449rec t5449rec = new T5449rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5449pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5449rec.t5449Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5449rec.retype);
		fieldNo015.set(t5449rec.facsh);
		fieldNo008.set(t5449rec.lrkcls);
		fieldNo010.set(t5449rec.subliv);
		fieldNo011.set(t5449rec.sslivb);
		fieldNo014.set(t5449rec.qcoshr);
		fieldNo017.set(t5449rec.rasnum01);
		fieldNo021.set(t5449rec.rasnum02);
		fieldNo025.set(t5449rec.rasnum03);
		fieldNo029.set(t5449rec.rasnum04);
		fieldNo033.set(t5449rec.rasnum05);
		fieldNo037.set(t5449rec.rasnum06);
		fieldNo041.set(t5449rec.rasnum07);
		fieldNo045.set(t5449rec.rasnum08);
		fieldNo049.set(t5449rec.rasnum09);
		fieldNo053.set(t5449rec.rasnum10);
		fieldNo018.set(t5449rec.qreshr01);
		fieldNo022.set(t5449rec.qreshr02);
		fieldNo026.set(t5449rec.qreshr03);
		fieldNo030.set(t5449rec.qreshr04);
		fieldNo034.set(t5449rec.qreshr05);
		fieldNo038.set(t5449rec.qreshr06);
		fieldNo042.set(t5449rec.qreshr07);
		fieldNo046.set(t5449rec.qreshr08);
		fieldNo050.set(t5449rec.qreshr09);
		fieldNo054.set(t5449rec.qreshr10);
		fieldNo019.set(t5449rec.relimit01);
		fieldNo023.set(t5449rec.relimit02);
		fieldNo027.set(t5449rec.relimit03);
		fieldNo031.set(t5449rec.relimit04);
		fieldNo035.set(t5449rec.relimit05);
		fieldNo039.set(t5449rec.relimit06);
		fieldNo043.set(t5449rec.relimit07);
		fieldNo047.set(t5449rec.relimit08);
		fieldNo051.set(t5449rec.relimit09);
		fieldNo055.set(t5449rec.relimit10);
		fieldNo020.set(t5449rec.rprmmth01);
		fieldNo028.set(t5449rec.rprmmth03);
		fieldNo024.set(t5449rec.rprmmth02);
		fieldNo032.set(t5449rec.rprmmth04);
		fieldNo036.set(t5449rec.rprmmth05);
		fieldNo040.set(t5449rec.rprmmth06);
		fieldNo044.set(t5449rec.rprmmth07);
		fieldNo048.set(t5449rec.rprmmth08);
		fieldNo052.set(t5449rec.rprmmth09);
		fieldNo056.set(t5449rec.rprmmth10);
		fieldNo012.set(t5449rec.prefbas);
		fieldNo009.set(t5449rec.rtytyp);
		fieldNo013.set(t5449rec.currcode);
		fieldNo016.set(t5449rec.letterType);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine019);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
