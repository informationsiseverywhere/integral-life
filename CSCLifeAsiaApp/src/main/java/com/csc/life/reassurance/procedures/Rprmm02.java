/*
 * File: Rprmm02.java
 * Date: 30 August 2009 2:13:29
 * Author: Quipoz Limited
 * 
 * Class transformed from RPRMM02.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.tablestructures.T5658rec;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.reassurance.recordstructures.Rprmiumrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*   Reassurance Premium Calculation Method 02
*
* This calculation will be used for joint-life cases.
*
* PROCESSING.
* ----------
*
* - Read the LIFE file in order to access the J/L ages and sex:
*
*       - read the LIFE details using 'LIFELNB' (life number from
*         COVTTRM, joint-life number '00') store the life age AND
*         sex. Read the 'LIFELNB' (joint-life number '01') and
*         store the joint-life's age and sex.
*
*
* - Access table T5585 (keyed on coverage/rider)
* - This table may be dated, if so, use the version:
*
*        1) Rating date.
*
* Calculate an equivalent equal age as follows:
*
*         - One of the lives, both or none of the lives may be
*           adjusted.
*
*  An example follows:
*
*         - If the sex of both parties is female and the sex
*           adjustment indicator on T5585 is 'F', then both ages
*           must be adjusted . This adjustment value will be
*           found on T5585.
*
*         - If only one of the parties is female, then only her
*           age will be adjusted.
*
*         - If both parties are male and the sex adjustment
*           value on T5585 is 'F', then the ages will not be
*           adjusted.
*
*         End of example.
*
*         - Adjust the age of the life by the adjustment amount,
*           if an adjustment applies (i.e. if the sex of the life
*           equals the sex of the life to be adjusted (T5585).
*
*         - The reverse will also be true for males.
*
* Calculate the highest / lowest adjusted age.
*
*         - if the life-age is greater than the joint-life age,
*              - subtract J/L age from the life-age
*              otherwise
*              - subtract the life-age from the J/L age.
*
* Translate this difference in ages.
*
*         - This difference will be used to look up T5585 and
*           the 'addition to age' obtained.
*         - The age to be applied indicator will also be
*           obtained.
*
* Apply adjustment.
*
*         - Apply the adjustment age (addition to age) to the
*           indicated age be it the higher or the lower age.
*
*         - This age will now become the ANB @ RCD.
*
* The remainder of this program is as for single life, so PRMPM01
* is cloned in order to reduce programming time.
*
* CALCULATE-BASIC-ANNNUAL-PREMIUM (and apply age rates)
* (Age, Sex & Duration taken from linkage)
*
* Obtain the age rates from the (LEXT) record.
*
*  - read all the LEXT records for this contract, life and
*  coverage into the working-storage table. Compute the
*  adjusted age as being the summation of the LEXT age
*  rates plus the ANB @ RCD.
*
* Use the age calculated above to access the table T5664 and
* check the following:
*
*  - that the basic annual premium (indexed by year) from
*  the T5664 table is not zero. If it is zero, then display
*  an error message and skip the additional procedures.
*  Otherwise store the premium as the (BAP).
*
* - we should now have an age rated BAP.
*
* APPLY-RATE-PER-MILLE-LOADINGS
*
* - sum the rates per mille from the LEXT W/S table.
*
*  - add rates per mille to the BAP
*
* - we should now have a BAP with rates / mille applied.
*
* APPLY-DISCOUNT.
*
* Access the discount table T5659 using the key:-
*
* - Discount method from T5664 concatenated with currency.
*
*  - check the sum insured against the volume band ranges
*  and when within a range store the discount amount.
*
*  - compute the BAP as the BAP - discount
*
* - we should now have a BAP with discount applied.
*
* REASRATFAC :
*  - The calling program passes in this value which is from
*    T5450 with item setted up by T5449-RPRMMTH
*
*  - compute the BAP = BAP * reassurance-rate-factor,
*    when it is not = 0.
*
*  - we should now have a BAP with reassurance rate factor
*    applied.
*
*
*****************************************************************
* </pre>
*/
public class Rprmm02 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RPRMM02";
	private ZonedDecimalData wsaaAge00Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAge01Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String f272 = "F272";
	private static final String h043 = "H043";
	private static final String f262 = "F262";
	private static final String f110 = "F110";
		/* TABLES */
	private static final String t5585 = "T5585";
	private static final String t5658 = "T5658";
	private static final String t5659 = "T5659";
		/* FORMATS */
	private static final String itdmrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);
	private PackedDecimalData wsaaSub00 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub01 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaModalFactor = new PackedDecimalData(5, 4);
		/*01  FILLER REDEFINES WSAA-ROUND-NUM.                             
		01  FILLER REDEFINES WSAA-ROUND-NUM.                             
		01  FILLER REDEFINES WSAA-ROUND-NUM.                             
		01  FILLER REDEFINES WSAA-ROUND-NUM.                             */
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler, 12).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 13).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler4, 14).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler6, 15);

	private FixedLengthStringData wsaaT5658Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5658Rrate = new FixedLengthStringData(4).isAPartOf(wsaaT5658Key, 0);
	private FixedLengthStringData wsaaT5658Ageterm = new FixedLengthStringData(2).isAPartOf(wsaaT5658Key, 4);
	private FixedLengthStringData wsaaT5658Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5658Key, 6);
	private FixedLengthStringData wsaaT5658Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5658Key, 7);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5585rec t5585rec = new T5585rec();
	private T5658rec t5658rec = new T5658rec();
	private T5659rec t5659rec = new T5659rec();
	private Varcom varcom = new Varcom();
	private Rprmiumrec rprmiumrec = new Rprmiumrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		adjust192, 
		adjust196, 
		go198, 
		readLext210, 
		checkT5658Insprm230, 
		checkSumInsuredRange430, 
		calculateLoadings610
	}

	public Rprmm02() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rprmiumrec.rprmiumRec = convertAndSetParam(rprmiumrec.rprmiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		init020();
		exit090();
	}

protected void init020()
	{
		rprmiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialize100();
		basicAnnualPremium200();
		ratesPerMillieLoadings300();
		volumeDiscountBap1400();
		applyRateFactor450();
		premiumUnit500();
		percentageLoadings600();
		instalmentPremium700();
		rounding800();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		para110();
		calculateTerm130();
		readT5658140();
	}

protected void para110()
	{
		wsaaRatesPerMillieTot.set(ZERO);
		wsaaBap.set(ZERO);
		wsaaBip.set(ZERO);
		wsaaAdjustedAge.set(ZERO);
		wsaaDiscountAmt.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaT5659Key.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 8)); wsaaSub.add(1)){
			wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
		}
		readT5585135();
		wsaaSub00.set(ZERO);
		wsaaSub01.set(ZERO);
		wsaaAge00Adjusted.set(rprmiumrec.lage);
		wsaaAge01Adjusted.set(rprmiumrec.jlage);
		if (isEQ(rprmiumrec.lsex, t5585rec.sexageadj)) {
			adjustAge00191();
		}
		if (isEQ(rprmiumrec.jlsex, t5585rec.sexageadj)) {
			adjustAge01195();
		}
		compute(wsaaAgeDifference, 0).set(sub(wsaaAge00Adjusted, wsaaAge01Adjusted));
		wsaaSub.set(0);
		ageDifferance198();
	}

protected void calculateTerm130()
	{
		datcon3rec.frequency.set("01");
		datcon3rec.intDate1.set(rprmiumrec.effectdt);
		datcon3rec.intDate2.set(rprmiumrec.termdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			syserr570();
		}
	}

	/**
	* <pre>
	* Build a key, dependent on term.
	* This key (see below)
	* will read table T5658. This table contains the parameters to be
	* used in the calculation of the Basic Annual Premium for
	* Coverage/Rider components.
	* The key is a concatenation of the following fields:-
	*  1) Premium rate setted by calling program from T5450. It is
	*     T5450-SLCTRATE or T5450-ULTMRATE.
	*     In order to decide whether to use which rate, take the
	*     Cession Commencement date on RACD record, and the Costing
	*     Commencement date  and find the differance between them.
	*     If the difference is < Discount/Commission Period specified
	*     on T5450 then the record is within the Discount period
	*     and as such the Select Rate Tables must be used, otherwisw
	*     the Ultimate Rate Tables must be used.
	*  2) Term                     (calculated)
	*  3) Mortality Class
	*  4) Sex
	* Access the required table by reading the table directly (ITDM).
	* The contents of the table are then stored. This table is dated
	* use:
	*  1) Rating Date
	* </pre>
	*/
protected void readT5658140()
	{
		/*    READ TABLE T5658. THIS TABLE CONTAINS THE PARAMETERS TO BE*/
		/*    USED IN THE CALCULATION OF THE BASIC ANNUAL PREMIUM FOR*/
		/*    COVERAGE/RIDER COMPONENTS.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rprmiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5658);
		wsaaT5658Rrate.set(rprmiumrec.rrate);
		wsaaT5658Ageterm.set(rprmiumrec.duration);
		wsaaT5658Mortcls.set(rprmiumrec.mortcls);
		/* MOVE RPRM-LSEX              TO WSAA-T5658-SEX.               */
		wsaaT5658Sex.set(SPACES);
		itdmIO.setItemitem(wsaaT5658Key);
		itdmIO.setFormat(itdmrec);
		itdmIO.setItmfrm(rprmiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(wsaaT5658Key, itdmIO.getItemitem())
		|| isNE(rprmiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5658)) {
			itdmIO.setItemcoy(rprmiumrec.chdrChdrcoy);
			itdmIO.setItemitem(wsaaT5658Key);
			itdmIO.setItemtabl(t5658);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			dbError580();
		}
		else {
			t5658rec.t5658Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT5585135()
	{
		readTable136();
	}

protected void readTable136()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rprmiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5585);
		itdmIO.setItemitem(rprmiumrec.crtable);
		itdmIO.setItmfrm(rprmiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(rprmiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(rprmiumrec.crtable, itdmIO.getItemitem())
		|| isNE(itdmIO.getItemtabl(), t5585)) {
			itdmIO.setItemcoy(rprmiumrec.chdrChdrcoy);
			itdmIO.setItemitem(rprmiumrec.crtable);
			itdmIO.setItemtabl(t5585);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			dbError580();
		}
		t5585rec.t5585Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
	}

protected void adjustAge00191()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust192: 
					adjust192();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust192()
	{
		wsaaSub00.add(1);
		if (isGT(wsaaSub00, 9)) {
			syserrrec.statuz.set(h043);
			syserr570();
		}
		if (isLTE(rprmiumrec.lage, t5585rec.agelimit[wsaaSub00.toInt()])) {
			compute(wsaaAge00Adjusted, 0).set(add(rprmiumrec.lage, t5585rec.ageadj[wsaaSub00.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust192);
		}
		/*EXIT*/
	}

protected void adjustAge01195()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust196: 
					adjust196();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust196()
	{
		wsaaSub01.add(1);
		if (isGT(wsaaSub01, 9)) {
			syserrrec.statuz.set(h043);
			syserr570();
		}
		if (isLTE(rprmiumrec.jlage, t5585rec.agelimit[wsaaSub01.toInt()])) {
			compute(wsaaAge01Adjusted, 0).set(add(rprmiumrec.jlage, t5585rec.ageadj[wsaaSub01.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust196);
		}
		/*EXIT*/
	}

protected void ageDifferance198()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case go198: 
					go198();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go198()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 18)) {
			syserrrec.statuz.set(f262);
			syserr570();
		}
		if (isGT(wsaaAgeDifference, t5585rec.agedif[wsaaSub.toInt()])) {
			goTo(GotoLabel.go198);
		}
		if (isEQ(t5585rec.hghlowage, "H")) {
			if (isGT(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
		if (isEQ(t5585rec.hghlowage, "L")) {
			if (isLTE(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
	}

protected void basicAnnualPremium200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey200();
				case readLext210: 
					readLext210();
				case checkT5658Insprm230: 
					checkT5658Insprm230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupLextKey200()
	{
		/*  - read all the LEXT records for this contract, life and*/
		/*  coverage into the working-storage table. Compute the*/
		/*  adjusted age as being the summation of the LEXT age*/
		/*  rates plus the ANB @ RCD.*/
		lextIO.setChdrcoy(rprmiumrec.chdrChdrcoy);
		lextIO.setChdrnum(rprmiumrec.chdrChdrnum);
		lextIO.setLife(rprmiumrec.lifeLife);
		lextIO.setCoverage(rprmiumrec.covrCoverage);
		lextIO.setRider(rprmiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		wsaaSub.set(ZERO);
	}

protected void readLext210()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			dbError580();
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.checkT5658Insprm230);
		}
		if (isEQ(lextIO.getChdrcoy(), rprmiumrec.chdrChdrcoy)
		&& isEQ(lextIO.getChdrnum(), rprmiumrec.chdrChdrnum)
		&& isEQ(lextIO.getLife(), rprmiumrec.lifeLife)
		&& isEQ(lextIO.getCoverage(), rprmiumrec.covrCoverage)
		&& isEQ(lextIO.getRider(), rprmiumrec.covrRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.checkT5658Insprm230);
		}
		/*  Skip any expired special terms.*/
		lextIO.setFunction(varcom.nextr);
		if (isLTE(lextIO.getExtCessDate(), rprmiumrec.reRateDate)) {
			goTo(GotoLabel.readLext210);
		}
		if (isEQ(lextIO.getStatuz(), varcom.oK)) {
			if (isEQ(rprmiumrec.reasind, "2")
			&& isEQ(lextIO.getReasind(), "1")) {
				goTo(GotoLabel.readLext210);
			}
			else {
				if (isNE(rprmiumrec.reasind, "2")
				&& isEQ(lextIO.getReasind(), "2")) {
					goTo(GotoLabel.readLext210);
				}
			}
		}
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(lextIO.getOppc());
		wsaaRatesPerMillieTot.add(lextIO.getInsprm());
		wsaaAgerateTot.add(lextIO.getAgerate());
		goTo(GotoLabel.readLext210);
	}

protected void checkT5658Insprm230()
	{
		/* Use the age calculated above to access the table T5658 and*/
		/* check the following:*/
		/*  - that the basic annual premium (indexed by year) from*/
		/*  the T5658 table is not zero. If it is zero, then display*/
		/*  an error message and skip the additional procedures.*/
		/*  Otherwise store the premium as the (BAP).*/
		wsaaAdjustedAge.add(wsaaAgerateTot);
		/*    IF WSAA-ADJUSTED-AGE         < 1                             */
		if (isLT(wsaaAdjustedAge, 0)
		|| isGT(wsaaAdjustedAge, 110)) {
			syserrrec.statuz.set(e107);
			syserr570();
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge, 0)) {
			if (isEQ(t5658rec.insprem, ZERO)) {
				syserrrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5658rec.insprem);
			}
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE        = 100                            */
		/*       IF T5658-INSTPR          = ZERO                           */
		/*          MOVE E107             TO SYSR-STATUZ                   */
		/*          PERFORM 570-SYSERR                                     */
		/*       ELSE                                                      */
		/*          MOVE T5658-INSTPR     TO WSAA-BAP                      */
		/*       END-IF                                                    */
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			if ((setPrecision(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], 0)
			&& isEQ(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], ZERO))) {
				syserrrec.statuz.set(e107);
				syserr570();
			}
			else {
				compute(wsaaBap, 2).set(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()]);
			}
		}
		else {
			if (isEQ(t5658rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				syserrrec.statuz.set(e107);
				syserr570();
			}
			else {
				wsaaBap.set(t5658rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void ratesPerMillieLoadings300()
	{
		/*PARA*/
		/* APPLY-RATE-PER-MILLE-LOADINGS*/
		/* - sum the rates per mille from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(wsaaBap, 2).set(add(wsaaRatesPerMillieTot, wsaaBap));
		/*EXIT*/
	}

protected void volumeDiscountBap1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readT5659410();
				case checkSumInsuredRange430: 
					checkSumInsuredRange430();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT5659410()
	{
		/* APPLY-DISCOUNT.*/
		/* Access the discount table T5659 using the key:-*/
		/* - Discount method from T5658 concatenated with currency.*/
		/*  - check the sum insurred against the volume band ranges*/
		/*  and when within a range store the discount amount.*/
		/*  - compute the BAP as the BAP - discount*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rprmiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5659);
		wsaaDisccntmeth.set(t5658rec.disccntmeth);
		wsaaCurrcode.set(rprmiumrec.currcode);
		itdmIO.setItemitem(wsaaT5659Key);
		itdmIO.setItmfrm(rprmiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(rprmiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5659)
		|| isNE(wsaaT5659Key, itdmIO.getItemitem())
		|| isNE(itdmIO.getStatuz(), varcom.oK)) {
			itdmIO.setItemcoy(rprmiumrec.chdrChdrcoy);
			itdmIO.setItemitem(wsaaT5659Key);
			itdmIO.setItemtabl(t5659);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			dbError580();
		}
		t5659rec.t5659Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
	}

protected void checkSumInsuredRange430()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 4)) {
			return ;
		}
		if (isLT(rprmiumrec.sumin, t5659rec.volbanfr[wsaaSub.toInt()])
		|| isGT(rprmiumrec.sumin, t5659rec.volbanto[wsaaSub.toInt()])) {
			goTo(GotoLabel.checkSumInsuredRange430);
		}
		else {
			wsaaDiscountAmt.set(t5659rec.volbanle[wsaaSub.toInt()]);
		}
		compute(wsaaBap, 2).set(sub(wsaaBap, wsaaDiscountAmt));
		/*EXIT*/
	}

protected void applyRateFactor450()
	{
		/*RATES*/
		/*  The RPRM-REASRATFAC is setted up in calling program by*/
		/*  reading T5450 with item from T5449-RPRMMTH*/
		if (isNE(rprmiumrec.reasratfac, ZERO)) {
			compute(wsaaBap, 2).set(mult(wsaaBap, rprmiumrec.reasratfac));
		}
		/*EXIT*/
	}

protected void premiumUnit500()
	{
		/*PARA*/
		/* APPLY-PREMIUM-UNIT*/
		/* - Obtain the risk-unit from T5658*/
		/* - multiply BAP by the sum-insured and divide*/
		/*   by the risk-unit*/
		if (isEQ(t5658rec.unit, ZERO)) {
			syserrrec.statuz.set(f110);
			syserr570();
		}
		else {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, rprmiumrec.sumin)), t5658rec.unit)));
		}
		/*EXIT*/
	}

protected void percentageLoadings600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para600();
				case calculateLoadings610: 
					calculateLoadings610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para600()
	{
		/* APPLY-PERCENTAGE-LOADINGS*/
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the BAP as follows:*/
		/*  BAP = BAP * loading percentage / 100.*/
		wsaaSub.set(0);
	}

protected void calculateLoadings610()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			return ;
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()], 0)) {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, wsaaLextOppc[wsaaSub.toInt()])), 100)));
		}
		goTo(GotoLabel.calculateLoadings610);
	}

protected void instalmentPremium700()
	{
		para700();
		instalmentPrem710();
	}

protected void para700()
	{
		/* CALCULATE-INSTALMENT-PREMIUM.*/
		/* Determine which billing frequency to use.*/
		/* compute the basic-instalment-premium (BIP) as:-*/
		/* basic-premium * factor (FACTOR is from T5658).*/
		wsaaBip.set(0);
	}

protected void instalmentPrem710()
	{
		wsaaModalFactor.set(0);
		if (isEQ(rprmiumrec.billfreq, "01")
		|| isEQ(rprmiumrec.billfreq, "00")) {
			wsaaModalFactor.set(t5658rec.mfacty);
		}
		if (isEQ(rprmiumrec.billfreq, "02")) {
			wsaaModalFactor.set(t5658rec.mfacthy);
		}
		if (isEQ(rprmiumrec.billfreq, "12")) {
			wsaaModalFactor.set(t5658rec.mfactm);
		}
		if (isEQ(rprmiumrec.billfreq, "04")) {
			wsaaModalFactor.set(t5658rec.mfactq);
		}
		if (isEQ(rprmiumrec.billfreq, "13")) {
			wsaaModalFactor.set(t5658rec.mfact4w);
		}
		if (isEQ(rprmiumrec.billfreq, "24")) {
			wsaaModalFactor.set(t5658rec.mfacthm);
		}
		if (isEQ(rprmiumrec.billfreq, "26")) {
			wsaaModalFactor.set(t5658rec.mfact2w);
		}
		if (isEQ(rprmiumrec.billfreq, "52")) {
			wsaaModalFactor.set(t5658rec.mfactw);
		}
		if (isEQ(wsaaModalFactor, 0)) {
			syserrrec.statuz.set(f272);
			syserr570();
		}
		else {
			compute(wsaaBip, 4).set(mult(wsaaBap, wsaaModalFactor));
		}
	}

protected void rounding800()
	{
		para801();
		rounded820();
	}

protected void para801()
	{
		/* - Round up depending on the rounding factor (obtained from*/
		/* the T5659 table).*/
		/* - if the prem-unit from T5658 is greater than zero, then*/
		/* compute the BIP as the rounded number / the premium-unit*/
		/* (from T5658). The premium unit is the quantity in which the*/
		/* currency is dominated in.*/
		wsaaRoundNum.set(wsaaBip);
		if (isEQ(t5659rec.rndfact, 1)
		|| isEQ(t5659rec.rndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 10)) {
			if (isLT(wsaaRound1, 5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound100.set(0);
			}
		}
	}

protected void rounded820()
	{
		wsaaBip.set(wsaaRoundNum);
		if (isEQ(t5658rec.premUnit, 0)) {
			rprmiumrec.calcPrem.set(wsaaBip);
		}
		else {
			compute(rprmiumrec.calcPrem, 2).set((div(wsaaBip, t5658rec.premUnit)));
		}
		/*EXIT*/
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rprmiumrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rprmiumrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
