package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5431screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 22, 4, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5431ScreenVars sv = (S5431ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5431screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5431screensfl, 
			sv.S5431screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5431ScreenVars sv = (S5431ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5431screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5431ScreenVars sv = (S5431ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5431screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5431screensflWritten.gt(0))
		{
			sv.s5431screensfl.setCurrentIndex(0);
			sv.S5431screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5431ScreenVars sv = (S5431ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5431screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5431ScreenVars screenVars = (S5431ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hlifeno.setFieldName("hlifeno");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.select.setFieldName("select");
				screenVars.component.setFieldName("component");
				screenVars.cmpntnum.setFieldName("cmpntnum");
				screenVars.deit.setFieldName("deit");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hlifeno.set(dm.getField("hlifeno"));
			screenVars.hsuffix.set(dm.getField("hsuffix"));
			screenVars.hcoverage.set(dm.getField("hcoverage"));
			screenVars.hrider.set(dm.getField("hrider"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.select.set(dm.getField("select"));
			screenVars.component.set(dm.getField("component"));
			screenVars.cmpntnum.set(dm.getField("cmpntnum"));
			screenVars.deit.set(dm.getField("deit"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5431ScreenVars screenVars = (S5431ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hlifeno.setFieldName("hlifeno");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.select.setFieldName("select");
				screenVars.component.setFieldName("component");
				screenVars.cmpntnum.setFieldName("cmpntnum");
				screenVars.deit.setFieldName("deit");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hlifeno").set(screenVars.hlifeno);
			dm.getField("hsuffix").set(screenVars.hsuffix);
			dm.getField("hcoverage").set(screenVars.hcoverage);
			dm.getField("hrider").set(screenVars.hrider);
			dm.getField("statcode").set(screenVars.statcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("select").set(screenVars.select);
			dm.getField("component").set(screenVars.component);
			dm.getField("cmpntnum").set(screenVars.cmpntnum);
			dm.getField("deit").set(screenVars.deit);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5431screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5431ScreenVars screenVars = (S5431ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hlifeno.clearFormatting();
		screenVars.hsuffix.clearFormatting();
		screenVars.hcoverage.clearFormatting();
		screenVars.hrider.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.component.clearFormatting();
		screenVars.cmpntnum.clearFormatting();
		screenVars.deit.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5431ScreenVars screenVars = (S5431ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hlifeno.setClassString("");
		screenVars.hsuffix.setClassString("");
		screenVars.hcoverage.setClassString("");
		screenVars.hrider.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.select.setClassString("");
		screenVars.component.setClassString("");
		screenVars.cmpntnum.setClassString("");
		screenVars.deit.setClassString("");
	}

/**
 * Clear all the variables in S5431screensfl
 */
	public static void clear(VarModel pv) {
		S5431ScreenVars screenVars = (S5431ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hlifeno.clear();
		screenVars.hsuffix.clear();
		screenVars.hcoverage.clear();
		screenVars.hrider.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.select.clear();
		screenVars.component.clear();
		screenVars.cmpntnum.clear();
		screenVars.deit.clear();
	}
}
