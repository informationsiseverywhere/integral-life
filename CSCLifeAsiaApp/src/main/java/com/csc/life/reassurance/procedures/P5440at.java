/*
 * File: P5440at.java
 * Date: 30 August 2009 0:26:31
 * Author: Quipoz Limited
 *
 * Class transformed from P5440AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Cession AT Update.
*
* This program will be run under AT and will perform the
* general functions for the finalisation of Contract Auto
* Cession.
*
* The general processing will carry out the following
* functions for all components attached to each life on
* the contract.
*
* 1. Increment the TRANNO on the Contract Header.
*
* 2. Call TRMRACD to terminate any existing cessions.
*    (i.e. validflag '1' RACD records).
*
* 3. Call REXPUPD to update the life reassurance experience
*    history as required.
*
* 4. Call ACTVRES to activate the new RACD records (i.e.
*    change to validflag '1' any validflag '3' records).
*
* 5. Write a PTRN record to mark the transaction.
*
*****************************************************************
* </pre>
*/
public class P5440at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("P5440AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 17);
	private FixedLengthStringData filler1 = new FixedLengthStringData(178).isAPartOf(wsaaTransArea, 22, FILLER).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaJlifenum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaJlifeExists = new FixedLengthStringData(1);
	private Validator jlifeExists = new Validator(wsaaJlifeExists, "Y");
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String lifeenqrec = "LIFEENQREC";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String ptrnrec = "PTRNREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Actvresrec actvresrec = new Actvresrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Atmodrec atmodrec = new Atmodrec();

	public P5440at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		wsaaTransArea.set(atmodrec.transArea);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaLife.set(SPACES);
		wsaaLifenum.set(SPACES);
		wsaaJlifenum.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		contractHeader1000();
		lifemjaIO.setParams(SPACES);
		lifemjaIO.setChdrcoy(atmodrec.company);
		lifemjaIO.setChdrnum(wsaaPrimaryChdrnum);
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.begn);
		while ( !(isEQ(lifemjaIO.getStatuz(),varcom.endp))) {
			processLife2000();
		}

		writePtrn6000();
		updateBatchHeader7000();
		releaseSftlck8000();
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void contractHeader1000()
	{
		chdr1010();
	}

protected void chdr1010()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(wsaaEffdate);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrfrom(wsaaEffdate);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
	}

protected void processLife2000()
	{
			life2010();
		}

protected void life2010()
	{
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			xxxxFatalError();
		}
		if (isNE(lifemjaIO.getChdrcoy(),atmodrec.company)
		|| isNE(lifemjaIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(lifemjaIO.getLife(),wsaaLife)) {
			wsaaLife.set(lifemjaIO.getLife());
			wsaaLifenum.set(lifemjaIO.getLifcnum());
			wsaaJlifeExists.set("N");
			readCovr3000();
		}
		lifemjaIO.setFunction(varcom.nextr);
	}

protected void readCovr3000()
	{
		covr3010();
	}

protected void covr3010()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(atmodrec.company);
		lifeenqIO.setChdrnum(wsaaPrimaryChdrnum);
		lifeenqIO.setLife(wsaaLife);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFormat(lifeenqrec);
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			wsaaJlifenum.set(lifeenqIO.getLifcnum());
			wsaaJlifeExists.set("Y");
		}
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(atmodrec.company);
		covrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		covrmjaIO.setLife(wsaaLife);
		covrmjaIO.setPlanSuffix(varcom.vrcmMaxDate);
		covrmjaIO.setFunction(varcom.begn);
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			processCovr4000();
		}

	}

protected void processCovr4000()
	{
		/*COVR*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(),atmodrec.company)
		|| isNE(covrmjaIO.getChdrnum(),wsaaPrimaryChdrnum)
		|| isNE(covrmjaIO.getLife(),wsaaLife)) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		activateRacd5000();
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void activateRacd5000()
	{
		racd5010();
	}

protected void racd5010()
	{
		/* Call ACTVRES to activate new RACD records. This will update*/
		/* them from validflag '3' to validflag '1'.*/
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(covrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(covrmjaIO.getChdrnum());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		actvresrec.tranno.set(chdrmjaIO.getTranno());
		actvresrec.life.set(covrmjaIO.getLife());
		actvresrec.coverage.set(covrmjaIO.getCoverage());
		actvresrec.rider.set(covrmjaIO.getRider());
		actvresrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		actvresrec.crtable.set(covrmjaIO.getCrtable());
		actvresrec.effdate.set(wsaaEffdate);
		actvresrec.clntcoy.set(wsaaFsuco);
		actvresrec.l1Clntnum.set(wsaaLifenum);
		actvresrec.jlife.set(covrmjaIO.getJlife());
		actvresrec.crrcd.set(covrmjaIO.getCrrcd());
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		if (jlifeExists.isTrue()) {
			actvresrec.l2Clntnum.set(wsaaJlifenum);
		}
		actvresrec.oldSumins.set(ZERO);
		actvresrec.newSumins.set(covrmjaIO.getSumins());
		actvresrec.function.set("ACT8");
		actvresrec.language.set(atmodrec.language);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			xxxxFatalError();
		}
	}

protected void writePtrn6000()
	{
		ptrn6010();
	}

protected void ptrn6010()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaEffdate);
		/* MOVE WSAA-EFFDATE           TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(atmodrec.company);
		ptrnIO.setChdrnum(wsaaPrimaryChdrnum);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

protected void updateBatchHeader7000()
	{
		/*BATCH*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void releaseSftlck8000()
	{
		rlse8010();
	}

protected void rlse8010()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(wsaaPrimaryChdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
