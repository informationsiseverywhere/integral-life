package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:20
 * Description:
 * Copybook name: RACDRCLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdrclkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdrclFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData racdrclKey = new FixedLengthStringData(256).isAPartOf(racdrclFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdrclChdrcoy = new FixedLengthStringData(1).isAPartOf(racdrclKey, 0);
  	public FixedLengthStringData racdrclChdrnum = new FixedLengthStringData(8).isAPartOf(racdrclKey, 1);
  	public FixedLengthStringData racdrclLife = new FixedLengthStringData(2).isAPartOf(racdrclKey, 9);
  	public FixedLengthStringData racdrclCoverage = new FixedLengthStringData(2).isAPartOf(racdrclKey, 11);
  	public FixedLengthStringData racdrclRider = new FixedLengthStringData(2).isAPartOf(racdrclKey, 13);
  	public PackedDecimalData racdrclPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdrclKey, 15);
  	public PackedDecimalData racdrclSeqno = new PackedDecimalData(2, 0).isAPartOf(racdrclKey, 18);
  	public FixedLengthStringData racdrclLrkcls = new FixedLengthStringData(4).isAPartOf(racdrclKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(racdrclKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdrclFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdrclFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}