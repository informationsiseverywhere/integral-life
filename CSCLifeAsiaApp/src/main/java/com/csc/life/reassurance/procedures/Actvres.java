/*
 * File: Actvres.java
 * Date: 29 August 2009 20:11:58
 * Author: Quipoz Limited
 * 
 * Class transformed from ACTVRES.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.dataaccess.RacdrclTableDAM;
import com.csc.life.reassurance.dataaccess.RacdseqTableDAM;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Reassurance Cession Activation Subroutine.
*
* Overview
* ========
*
* This subroutine will activate the reassurance cessions
* (i.e. RACD records) for the component and then update
* </pre>
*/
public class Actvres extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ACTVRES";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaRetained = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSeqNumber = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private PackedDecimalData wsaaReasper = new PackedDecimalData(7, 4);

	private FixedLengthStringData wsaaValidComp = new FixedLengthStringData(1);
	private Validator validComp = new Validator(wsaaValidComp, "Y");
	private ZonedDecimalData wsxxSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsxxLrkcls = new FixedLengthStringData(4);
	private ZonedDecimalData wsyySub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsyySub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsyyLrkcls = new FixedLengthStringData(4);

	private FixedLengthStringData wsyyReassuranceLrkcls = new FixedLengthStringData(320);
	private FixedLengthStringData[] wsyyReaLrkclss = FLSArrayPartOfStructure(80, 4, wsyyReassuranceLrkcls, 0);
	private FixedLengthStringData[] wsyyReaLrkcls = FLSDArrayPartOfArrayStructure(4, wsyyReaLrkclss, 0);
		/* FORMATS */
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
	private static final String racdseqrec = "RACDSEQREC";
	private static final String racdrclrec = "RACDRCLREC";
	private static final String racdmjarec = "RACDMJAREC";
	private static final String chdrlifrec = "CHDRLIFREC";

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLetokeys, 1);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaLetokeys, 9);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaLetokeys, 11);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaLetokeys, 13);
	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(4).isAPartOf(wsaaLetokeys, 15);
	private FixedLengthStringData wsaaSeqno = new FixedLengthStringData(2).isAPartOf(wsaaLetokeys, 19);
		/* ERRORS */
	private static final String e049 = "E049";
	private static final String r051 = "R051";
	private static final String r057 = "R057";
		/* TABLES */
	private static final String t5446 = "T5446";
	private static final String t5447 = "T5447";
	private static final String t5448 = "T5448";
	private static final String t5449 = "T5449";
	private static final String th618 = "TH618";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
	private RacdrclTableDAM racdrclIO = new RacdrclTableDAM();
	private RacdseqTableDAM racdseqIO = new RacdseqTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private T5449rec t5449rec = new T5449rec();
	private Th618rec th618rec = new Th618rec();
	private Varcom varcom = new Varcom();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Actvresrec actvresrec = new Actvresrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkCompany010, 
		mainlineExit010
	}

	public Actvres() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		actvresrec.actvresRec = convertAndSetParam(actvresrec.actvresRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					mainlineStart010();
				case checkCompany010: 
					checkCompany010();
				case mainlineExit010: 
					mainlineExit010();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void mainlineStart010()
	{
		actvresrec.statuz.set(varcom.oK);
		if (isNE(actvresrec.function, "ACT8") && isNE(actvresrec.function, "ADJU")) {
			syserrrec.statuz.set(e049);
			syserr570();
		}
		/*  INITIALIZE                     WSYY-REA-LRKCLSS.     <S19FIX>*/
		initialize(wsyyReassuranceLrkcls);
		/* Calculate the sum retained by the company. This is equal*/
		/* to the original sum assured minus the total amount that*/
		/* has been ceded to the reassurers.*/
		compute(wsaaRetained, 2).set(sub(actvresrec.newSumins, actvresrec.oldSumins));
		wsaaValidComp.set("Y");
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(actvresrec.chdrcoy);
		itdmIO.setItemtabl(t5448);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(actvresrec.cnttype);
		stringVariable1.addExpression(actvresrec.crtable);
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setItemitem(wsaaT5448Item);
		/*  MOVE ACVR-EFFDATE           TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(actvresrec.crrcd);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		/* If no item is found, do not produce an error yet. Check*/
		/* for RACD records first.*/
		if (isNE(itdmIO.getItemitem(), wsaaT5448Item)
		|| isNE(itdmIO.getItemcoy(), actvresrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5448)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaValidComp.set("N");
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
			/*        PERFORM 2100-READ-T5446                          <V4L027>*/
			readTh6183100();
		}
		racdseqIO.setParams(SPACES);
		racdseqIO.setChdrcoy(actvresrec.chdrcoy);
		racdseqIO.setChdrnum(actvresrec.chdrnum);
		racdseqIO.setLife(actvresrec.life);
		racdseqIO.setCoverage(actvresrec.coverage);
		racdseqIO.setRider(actvresrec.rider);
		racdseqIO.setPlanSuffix(actvresrec.planSuffix);
		racdseqIO.setSeqno(99);
		racdseqIO.setFormat(racdseqrec);
		racdseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, racdseqIO);
		/* If there are no RACD records to be activated, we must*/
		/* still check if the life company has retained any of*/
		/* the sum assured and if so we must update the life*/
		/* experience.*/
		if (isNE(racdseqIO.getStatuz(), varcom.oK)
		|| isNE(racdseqIO.getChdrcoy(), actvresrec.chdrcoy)
		|| isNE(racdseqIO.getChdrnum(), actvresrec.chdrnum)
		|| isNE(racdseqIO.getLife(), actvresrec.life)
		|| isNE(racdseqIO.getCoverage(), actvresrec.coverage)
		|| isNE(racdseqIO.getRider(), actvresrec.rider)
		|| isNE(racdseqIO.getPlanSuffix(), actvresrec.planSuffix)) {
			goTo(GotoLabel.checkCompany010);
		}
		/* Once here, we may have RACD records that need to be*/
		/* activated. Thus the component must be a valid item on*/
		/* T5448. If not, produce an error and exit program.*/
		if (!validComp.isTrue()) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(actvresrec.chdrcoy);
			itdmIO.setItemtabl(t5448);
			itdmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		wsaaSeqNumber.set(racdseqIO.getSeqno());
		/* Update the reassurer exposure with the sum reassured.*/
		updateReassurerExposure1000();
		goTo(GotoLabel.mainlineExit010);
	}

protected void checkCompany010()
	{
		/* Check the product reassurance bypass table, T5447. If the item*/
		/* exists, reassurance is not required for this product so bypass*/
		/* the life company update.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(actvresrec.chdrcoy);
		itemIO.setItemtabl(t5447);
		itemIO.setItemitem(actvresrec.cnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			wsaaValidComp.set("N");
		}
		readTh6183100();
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaValidComp.set("N");
		}
		/* The sum retained by the company is equal to the original*/
		/* sum assured minus the total amount that has been ceded to*/
		/* the reassurers. If this amount is positive and an item exists*/
		/* on T5448 for the component, update the company exposure.*/
		if (isGT(wsaaRetained, 0)
		&& validComp.isTrue()) {
			/*        PERFORM 2000-UPDATE-COMPANY-EXPOSURE                     */
			for (wsxxSub.set(1); !(isGT(wsxxSub, 5)); wsxxSub.add(1)){
				if (isNE(th618rec.lrkcls[wsxxSub.toInt()], SPACES)) {
					wsxxLrkcls.set(SPACES);
					wsxxLrkcls.set(th618rec.lrkcls[wsxxSub.toInt()]);
					readT54462100();
					rexpupdrec.rexpupdRec.set(SPACES);
					rexpupdrec.riskClass.set(th618rec.lrkcls[wsxxSub.toInt()]);
					updateCompanyExposure2000();
				}
			}
		}
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void updateReassurerExposure1000()
	{
		update1010();
	}

protected void update1010()
	{
		/* Update existing RACD records with the new REASPER (proportion*/
		/* of total sum assured).*/
		/* RACD with validflag = '1'                                       */
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setChdrcoy(actvresrec.chdrcoy);
		racdmjaIO.setChdrnum(actvresrec.chdrnum);
		racdmjaIO.setLife(actvresrec.life);
		racdmjaIO.setCoverage(actvresrec.coverage);
		racdmjaIO.setRider(actvresrec.rider);
		racdmjaIO.setPlanSuffix(actvresrec.planSuffix);
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(racdmjarec);
		racdmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			updateRacd1100();
		}
		
		/* Process new RACD records.*/
		/* RACD with Validflag = '3'.                              <V4L027>*/
		/* MOVE 0                      TO WSYY-SUB.             <LA3429>*/
		wsyySub.set(0);
		/* MOVE SPACES                 TO RACDLNB-PARAMS.               */
		/* MOVE ACVR-CHDRCOY           TO RACDLNB-CHDRCOY.              */
		/* MOVE ACVR-CHDRNUM           TO RACDLNB-CHDRNUM.              */
		/* MOVE ACVR-LIFE              TO RACDLNB-LIFE.                 */
		/* MOVE ACVR-COVERAGE          TO RACDLNB-COVERAGE.             */
		/* MOVE ACVR-RIDER             TO RACDLNB-RIDER.                */
		/* MOVE ACVR-PLAN-SUFFIX       TO RACDLNB-PLAN-SUFFIX.          */
		/* MOVE ZEROES                 TO RACDLNB-SEQNO.                */
		/* MOVE RACDLNBREC             TO RACDLNB-FORMAT.               */
		/* MOVE BEGN                   TO RACDLNB-FUNCTION.             */
		/* PERFORM 1200-PROCESS-RACD                                    */
		/*     UNTIL RACDLNB-STATUZ    = ENDP.                          */
		racdrclIO.setParams(SPACES);
		racdrclIO.setChdrcoy(actvresrec.chdrcoy);
		racdrclIO.setChdrnum(actvresrec.chdrnum);
		racdrclIO.setLife(actvresrec.life);
		racdrclIO.setCoverage(actvresrec.coverage);
		racdrclIO.setRider(actvresrec.rider);
		racdrclIO.setPlanSuffix(actvresrec.planSuffix);
		racdrclIO.setSeqno(ZERO);
		racdrclIO.setFormat(racdrclrec);
		racdrclIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdrclIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdrclIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdrclIO.getStatuz(),varcom.endp))) {
			processRacd1200();
		}
		
	}

protected void updateRacd1100()
	{
		racd1110();
	}

protected void racd1110()
	{
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(), varcom.oK)
		|| isNE(racdmjaIO.getChdrcoy(), actvresrec.chdrcoy)
		|| isNE(racdmjaIO.getChdrnum(), actvresrec.chdrnum)
		|| isNE(racdmjaIO.getLife(), actvresrec.life)
		|| isNE(racdmjaIO.getCoverage(), actvresrec.coverage)
		|| isNE(racdmjaIO.getRider(), actvresrec.rider)
		|| isNE(racdmjaIO.getPlanSuffix(), actvresrec.planSuffix)) {
			racdmjaIO.setStatuz(varcom.endp);
			return ;
		}
		if(isEQ(actvresrec.function,"ADJU")) {
			compute(wsaaRetained, 2).set(sub(wsaaRetained, racdmjaIO.getRaAmount()));
		}
		/* Calculate the proportion of the total sum assured that is*/
		/* reassured for this cession, based on the new sum assured.*/
		compute(wsaaReasper, 4).set(mult(div(racdmjaIO.getRaAmount(), actvresrec.newSumins), 100));
		/* If the proportion reassured for an existing cession has*/
		/* changed as a result of new cessions created, the record*/
		/* must be rewritten as validflag '2' for historical purposes*/
		/* and a new validflag '1' record must be written with the*/
		/* new proportion reassured.*/
		if (isNE(wsaaReasper, racdmjaIO.getReasper())) {
			racdseqIO.setParams(SPACES);
			racdseqIO.setRrn(racdmjaIO.getRrn());
			racdseqIO.setFormat(racdseqrec);
			racdseqIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, racdseqIO);
			if (isNE(racdseqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(racdseqIO.getParams());
				dbError580();
			}
			racdseqIO.setValidflag("2");
			racdseqIO.setCurrto(actvresrec.effdate);
			racdseqIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, racdseqIO);
			if (isNE(racdseqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(racdseqIO.getParams());
				dbError580();
			}
			racdseqIO.setValidflag("1");
			racdseqIO.setCurrfrom(actvresrec.effdate);
			racdseqIO.setCurrto(varcom.vrcmMaxDate);
			racdseqIO.setTranno(actvresrec.tranno);
			racdseqIO.setReasper(wsaaReasper, true);
			racdseqIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, racdseqIO);
			if (isNE(racdseqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(racdseqIO.getParams());
				dbError580();
			}
		}
		racdmjaIO.setFunction(varcom.nextr);
	}

protected void processRacd1200()
	{
		racd1210();
	}

protected void racd1210()
	{
		/* CALL 'RACDLNBIO'            USING RACDLNB-PARAMS.            */
		/* IF RACDLNB-STATUZ           NOT = O-K                        */
		/* OR RACDLNB-CHDRCOY          NOT = ACVR-CHDRCOY               */
		/* OR RACDLNB-CHDRNUM          NOT = ACVR-CHDRNUM               */
		/* OR RACDLNB-LIFE             NOT = ACVR-LIFE                  */
		/* OR RACDLNB-COVERAGE         NOT = ACVR-COVERAGE              */
		/* OR RACDLNB-RIDER            NOT = ACVR-RIDER                 */
		/* OR RACDLNB-PLAN-SUFFIX      NOT = ACVR-PLAN-SUFFIX           */
		/*     PERFORM 1800-UPD-COMPANY-LRKCLS                  <LA3429>*/
		/*     PERFORM 1700-CHECK-NON-REASS                     <LA3429>*/
		/*     MOVE ENDP               TO RACDLNB-STATUZ                */
		/*     GO TO 1290-EXIT                                          */
		/* END-IF.                                                      */
		SmartFileCode.execute(appVars, racdrclIO);
		if (isNE(racdrclIO.getStatuz(), varcom.oK)
		&& isNE(racdrclIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdrclIO.getParams());
			syserrrec.statuz.set(racdrclIO.getStatuz());
			syserr570();
		}
		if (isNE(racdrclIO.getStatuz(), varcom.oK)
		|| isNE(racdrclIO.getChdrcoy(), actvresrec.chdrcoy)
		|| isNE(racdrclIO.getChdrnum(), actvresrec.chdrnum)
		|| isNE(racdrclIO.getLife(), actvresrec.life)
		|| isNE(racdrclIO.getCoverage(), actvresrec.coverage)
		|| isNE(racdrclIO.getRider(), actvresrec.rider)
		|| isNE(racdrclIO.getPlanSuffix(), actvresrec.planSuffix)) {
			updCompanyLrkcls1800();
			if(isGT(wsaaRetained,ZERO) && isNE(actvresrec.function, "ADJU")) {
				checkNonReass1700();
			}
			racdrclIO.setStatuz(varcom.endp);
			return ;
		}
		/*                                                         <V4L027>*/
		/* Check risk class. To calculate sum retained (WSAA-RETAINED)     */
		/* for each risk class.                                    <V4L027>*/
		/*                                                         <V4L027>*/
		wsyySub.add(1);
		/* IF WSYY-SUB                 =  1                     <LA3429>*/
		/*    MOVE RACDLNB-LRKCLS      TO WSYY-LRKCLS           <LA3429>*/
		/* END-IF.                                              <LA3429>*/
		/* IF RACDLNB-LRKCLS           NOT = WSYY-LRKCLS        <LA3429>*/
		/*    PERFORM 1800-UPD-COMPANY-LRKCLS                   <LA3429>*/
		/* END-IF.                                              <LA3429>*/
		if (isEQ(wsyySub, 1)) {
			wsyyLrkcls.set(racdrclIO.getLrkcls());
		}
		if (isNE(racdrclIO.getLrkcls(), wsyyLrkcls)) {
			updCompanyLrkcls1800();
		}
		/*                                                         <V4L027>*/
		/* Keep component risk class. It is used to determine which<V4L027>*/
		/* risk class is not reassured (section 1700-CHECK-NON-REASS)      */
		/* and need to record for this one as well.                <V4L027>*/
		/*                                                         <V4L027>*/
		/* MOVE RACDLNB-LRKCLS         TO WSYY-REA-LRKCLS (WSYY-SUB).   */
		wsyyReaLrkcls[wsyySub.toInt()].set(racdrclIO.getLrkcls());
		/* Read T5449 at this point, so the RATECODE can be obtained       */
		/* <II0108>.                                               <V4L027>*/
		readT54491400();
		/* Increase reassurer's retention for the life and joint life,*/
		/* as required.*/
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(actvresrec.chdrcoy);
		rexpupdrec.chdrnum.set(actvresrec.chdrnum);
		rexpupdrec.life.set(actvresrec.life);
		rexpupdrec.coverage.set(actvresrec.coverage);
		rexpupdrec.rider.set(actvresrec.rider);
		rexpupdrec.planSuffix.set(actvresrec.planSuffix);
		rexpupdrec.tranno.set(actvresrec.tranno);
		rexpupdrec.clntcoy.set(actvresrec.clntcoy);
		rexpupdrec.l1Clntnum.set(actvresrec.l1Clntnum);
		rexpupdrec.batctrcde.set(actvresrec.batctrcde);
		if (isNE(actvresrec.jlife, "00")) {
			rexpupdrec.l2Clntnum.set(actvresrec.l2Clntnum);
		}
		/* MOVE RACDLNB-RASNUM         TO RXUP-REASSURER.               */
		/* MOVE RACDLNB-RNGMNT         TO RXUP-ARRANGEMENT.             */
		/* MOVE RACDLNB-RA-AMOUNT      TO RXUP-SUMINS.                  */
		/* MOVE RACDLNB-CESTYPE        TO RXUP-CESTYPE.                 */
		/* MOVE SPACES                 TO WSXX-LRKCLS           <LA3429>*/
		/* MOVE RACDLNB-LRKCLS         TO WSXX-LRKCLS           <LA3429>*/
		rexpupdrec.reassurer.set(racdrclIO.getRasnum());
		rexpupdrec.arrangement.set(racdrclIO.getRngmnt());
		rexpupdrec.sumins.set(racdrclIO.getRaAmount());
		rexpupdrec.cestype.set(racdrclIO.getCestype());
		wsxxLrkcls.set(SPACES);
		wsxxLrkcls.set(racdrclIO.getLrkcls());
		readT54462100();
		/* MOVE RACDLNB-CURRCODE       TO RXUP-CURRENCY.                */
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(actvresrec.effdate);
		/*    MOVE T5448-LRKCLS           TO RXUP-RISK-CLASS.              */
		/* MOVE RACDLNB-LRKCLS         TO RXUP-RISK-CLASS.      <LA3429>*/
		rexpupdrec.riskClass.set(racdrclIO.getLrkcls());
		if (isNE(rexpupdrec.currency, actvresrec.currency)) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(actvresrec.currency);
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt3000();
			rexpupdrec.sumins.set(conlinkrec.amountOut);
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		rexpupdrec.function.set("INCR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz, varcom.oK)) {
			syserrrec.params.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			syserr570();
		}
		/*    SUBTRACT RACDLNB-RA-AMOUNT  FROM WSAA-RETAINED.              */
		/* IF WSYY-LRKCLS              = RACDLNB-LRKCLS         <LA3429>*/
		if (isEQ(wsyyLrkcls, racdrclIO.getLrkcls())) {
			compute(wsaaRetained, 2).set(sub(wsaaRetained, racdrclIO.getRaAmount()));
			/*****                             - RACDLNB-RA-AMOUNT      <LA3429>*/
		}
		else {
			compute(wsaaRetained, 2).set(sub(sub(actvresrec.newSumins, actvresrec.oldSumins), racdrclIO.getRaAmount()));
			/*                             - RACDLNB-RA-AMOUNT      <LA3429>*/
			wsyyLrkcls.set(racdrclIO.getLrkcls());
			/*****    MOVE RACDLNB-LRKCLS      TO WSYY-LRKCLS           <LA3429>*/
		}
		/* Activate RACD by setting validflag to '1'.*/
		rewriteRacd1300();
		/*    PERFORM 1400-READ-T5449.                                     */
		/*    IF  RACDLNB-CESTYPE         = '2'                            */
		/*    AND T5449-FACSH             NOT = SPACE                      */
		/*    AND T5449-LETTER-TYPE       NOT = SPACES                     */
		/*        PERFORM 1500-WRITE-FAC-SCHD                              */
		/*    END-IF.                                                      */
		/*1250-CHECK-COMPANY.                                              */
		/*                                                         <V4L027>*/
		/*  Cloned from 010-Check-Company above.....               <V4L027>*/
		/*                                                         <V4L027>*/
		/*    MOVE SPACES                 TO ITEM-PARAMS.                  */
		/*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*    MOVE ACVR-CHDRCOY           TO ITEM-ITEMCOY.                 */
		/*    MOVE T5447                  TO ITEM-ITEMTABL.                */
		/*    MOVE ACVR-CNTTYPE           TO ITEM-ITEMITEM.                */
		/*    MOVE ITEMREC                TO ITEM-FORMAT.                  */
		/*    MOVE READR                  TO ITEM-FUNCTION.                */
		/*    CALL 'ITEMIO'               USING ITEM-PARAMS.               */
		/*    IF  ITEM-STATUZ             NOT = O-K                        */
		/*    AND ITEM-STATUZ             NOT = MRNF                       */
		/*        MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
		/*    END-IF.                                                      */
		/*    IF ITEM-STATUZ              = O-K                            */
		/*        MOVE 'N'                TO WSAA-VALID-COMP               */
		/*    END-IF.                                                      */
		/*    IF  WSAA-RETAINED           > 0                              */
		/*    AND VALID-COMP                                               */
		/*        MOVE SPACES             TO RXUP-REXPUPD-REC              */
		/*        MOVE RACDLNB-LRKCLS     TO RXUP-RISK-CLASS               */
		/*        PERFORM 2000-UPDATE-COMPANY-EXPOSURE                     */
		/*    END-IF.                                                      */
		/*                                                        <V4L027>*/
		/*    MOVE NEXTR                  TO RACDLNB-STATUZ.               */
		/* MOVE NEXTR                  TO RACDLNB-FUNCTION.     <LA3429>*/
		racdrclIO.setFunction(varcom.nextr);
	}

protected void rewriteRacd1300()
	{
		racd1310();
	}

protected void racd1310()
	{
		racdseqIO.setParams(SPACES);
		/* MOVE RACDLNB-RRN            TO RACDSEQ-RRN.                  */
		racdseqIO.setRrn(racdrclIO.getRrn());
		racdseqIO.setFormat(racdseqrec);
		racdseqIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdseqIO);
		if (isNE(racdseqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdseqIO.getParams());
			dbError580();
		}
		/* IF RACDLNB-SEQNO            = ZEROES                         */
		if (isEQ(racdrclIO.getSeqno(), ZERO)) {
			wsaaSeqNumber.add(1);
			racdseqIO.setSeqno(wsaaSeqNumber);
		}
		racdseqIO.setValidflag("1");
		racdseqIO.setTranno(actvresrec.tranno);
		compute(wsaaReasper, 4).set(mult(div(racdseqIO.getRaAmount(), actvresrec.newSumins), 100));
		racdseqIO.setReasper(wsaaReasper, true);
		racdseqIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, racdseqIO);
		if (isNE(racdseqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdseqIO.getParams());
			dbError580();
		}
	}

protected void readT54491400()
	{
		t54491410();
	}

protected void t54491410()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(actvresrec.chdrcoy);
		itdmIO.setItemtabl(t5449);
		/* MOVE RACDLNB-RNGMNT         TO ITDM-ITEMITEM.                */
		itdmIO.setItemitem(racdrclIO.getRngmnt());
		itdmIO.setItmfrm(actvresrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		/*  IF ITDM-ITEMITEM            NOT = RACDLNB-RNGMNT             */
		if (isNE(itdmIO.getItemitem(), racdrclIO.getRngmnt())
		|| isNE(itdmIO.getItemcoy(), actvresrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5449)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*     MOVE RACDLNB-RNGMNT     TO ITDM-ITEMITEM                 */
			itdmIO.setItemitem(racdrclIO.getRngmnt());
			itdmIO.setItemcoy(actvresrec.chdrcoy);
			itdmIO.setItemtabl(t5449);
			itdmIO.setStatuz(r057);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5449rec.t5449Rec.set(itdmIO.getGenarea());
		}
	}

protected void writeFacSchd1500()
	{
		facSchd1510();
	}

protected void facSchd1510()
	{
		/* Write LETC via LETRQST.*/
		getCntbranch1600();
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(actvresrec.chdrcoy);
		letrqstrec.letterType.set(t5449rec.letterType);
		letrqstrec.letterRequestDate.set(actvresrec.effdate);
		letrqstrec.clntcoy.set(actvresrec.clntcoy);
		letrqstrec.clntnum.set(actvresrec.l1Clntnum);
		letrqstrec.otherKeys.set(SPACES);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(racdseqIO.getChdrcoy());
		letrqstrec.rdocnum.set(racdseqIO.getChdrnum());
		letrqstrec.chdrnum.set(racdseqIO.getChdrnum());
		/*  MOVE RACDSEQ-CHDRCOY        TO WSAA-CHDRCOY.                 */
		/*  MOVE RACDSEQ-CHDRNUM        TO WSAA-CHDRNUM.                 */
		/*  MOVE RACDSEQ-LIFE           TO WSAA-LIFE.                    */
		/*  MOVE RACDSEQ-COVERAGE       TO WSAA-COVERAGE.                */
		/*  MOVE RACDSEQ-RIDER          TO WSAA-RIDER.                   */
		/*  MOVE RACDSEQ-PLAN-SUFFIX    TO WSAA-PLAN-SUFFIX.             */
		letcokcpy.luLife.set(racdseqIO.getLife());
		letcokcpy.luCoverage.set(racdseqIO.getCoverage());
		letcokcpy.luRider.set(racdseqIO.getRider());
		letcokcpy.luPlanSuffix.set(racdseqIO.getPlanSuffix());
		letrqstrec.otherKeys.set(letcokcpy.lu);
		/*  MOVE WSAA-LETOKEYS          TO LETRQST-OTHER-KEYS.           */
		letrqstrec.tranno.set(actvresrec.tranno);
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			syserr570();
		}
	}

protected void getCntbranch1600()
	{
		/*CNTBRANCH*/
		chdrlifIO.setChdrcoy(racdseqIO.getChdrcoy());
		chdrlifIO.setChdrnum(racdseqIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void checkNonReass1700()
	{
		start1710();
	}

	/**
	* <pre>
	******************************                            <V4L027>
	* </pre>
	*/
protected void start1710()
	{
		for (wsyySub.set(1); !(isGT(wsyySub, 5)); wsyySub.add(1)){
			if (isNE(th618rec.lrkcls[wsyySub.toInt()], SPACES)) {
				for (wsyySub1.set(1); !(isGT(wsyySub1, 5)); wsyySub1.add(1)){
					if (isEQ(wsyyReaLrkcls[wsyySub1.toInt()], th618rec.lrkcls[wsyySub.toInt()])) {
						wsyySub1.set(7);
					}
				}
				if (isLT(wsyySub1, 7)) {
					rexpupdrec.rexpupdRec.set(SPACES);
					wsaaRetained.set(actvresrec.newSumins);
					rexpupdrec.riskClass.set(th618rec.lrkcls[wsyySub.toInt()]);
					wsxxLrkcls.set(SPACES);
					wsxxLrkcls.set(th618rec.lrkcls[wsyySub.toInt()]);
					readT54462100();
					updateCompanyExposure2000();
				}
			}
		}
	}

protected void updCompanyLrkcls1800()
	{
		start1810();
	}

protected void start1810()
	{
		/*                                                         <V4L027>*/
		/*  Cloned from 010-Check-Company above.....               <V4L027>*/
		/*                                                         <V4L027>*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(actvresrec.chdrcoy);
		itemIO.setItemtabl(t5447);
		itemIO.setItemitem(actvresrec.cnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			wsaaValidComp.set("N");
		}
		if (isGT(wsaaRetained, 0)
		&& validComp.isTrue()) {
			for (wsxxSub.set(1); !(isGT(wsxxSub, 5)); wsxxSub.add(1)){
				if (isNE(th618rec.lrkcls[wsxxSub.toInt()], SPACES)) {
					wsxxLrkcls.set(SPACES);
					wsxxLrkcls.set(th618rec.lrkcls[wsxxSub.toInt()]);
					readT54462100();
					rexpupdrec.rexpupdRec.set(SPACES);
					rexpupdrec.riskClass.set(wsyyLrkcls);
					updateCompanyExposure2000();
				}
			}
		}
	}

protected void updateCompanyExposure2000()
	{
		update2010();
	}

protected void update2010()
	{
		/*    MOVE SPACES                 TO RXUP-REXPUPD-REC.             */
		rexpupdrec.chdrcoy.set(actvresrec.chdrcoy);
		rexpupdrec.chdrnum.set(actvresrec.chdrnum);
		rexpupdrec.life.set(actvresrec.life);
		rexpupdrec.coverage.set(actvresrec.coverage);
		rexpupdrec.rider.set(actvresrec.rider);
		rexpupdrec.planSuffix.set(actvresrec.planSuffix);
		rexpupdrec.tranno.set(actvresrec.tranno);
		rexpupdrec.clntcoy.set(actvresrec.clntcoy);
		rexpupdrec.l1Clntnum.set(actvresrec.l1Clntnum);
		rexpupdrec.batctrcde.set(actvresrec.batctrcde);
		if (isNE(actvresrec.jlife, "00")) {
			rexpupdrec.l2Clntnum.set(actvresrec.l2Clntnum);
		}
		rexpupdrec.sumins.set(wsaaRetained);
		/* PERFORM 2100-READ-T5446.                                     */
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(actvresrec.effdate);
		/*    MOVE T5448-LRKCLS           TO RXUP-RISK-CLASS.              */
		if (isNE(rexpupdrec.currency, actvresrec.currency)) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(actvresrec.currency);
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt3000();
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		rexpupdrec.function.set("INCR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz, varcom.oK)) {
			syserrrec.params.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			syserr570();
		}
	}

protected void readT54462100()
	{
		t54462110();
	}

protected void t54462110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(actvresrec.chdrcoy);
		itdmIO.setItemtabl(t5446);
		/*    MOVE T5448-LRKCLS           TO ITDM-ITEMITEM.                */
		itdmIO.setItemitem(wsxxLrkcls);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		/*    IF ITDM-ITEMITEM            NOT = T5448-LRKCLS               */
		if (isNE(itdmIO.getItemitem(), wsxxLrkcls)
		|| isNE(itdmIO.getItemcoy(), actvresrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5446)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*        MOVE T5448-LRKCLS       TO ITDM-ITEMITEM                 */
			itdmIO.setItemitem(wsxxLrkcls);
			itdmIO.setItemcoy(actvresrec.chdrcoy);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(r051);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callXcvrt3000()
	{
		/*CALL*/
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(actvresrec.effdate);
		conlinkrec.company.set(actvresrec.chdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr570();
		}
		/*EXIT*/
	}

protected void readTh6183100()
	{
		th6183110();
	}

	/**
	* <pre>
	*************************                                 <V4L027>
	* </pre>
	*/
protected void th6183110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(actvresrec.chdrcoy);
		itemIO.setItemtabl(th618);
		itemIO.setItemitem(t5448rec.rrsktyp);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		th618rec.th618Rec.set(itemIO.getGenarea());
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(actvresrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rexpupdrec.currency);
		zrdecplrec.batctrcde.set(actvresrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A090-EXIT*/
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		actvresrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		actvresrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
