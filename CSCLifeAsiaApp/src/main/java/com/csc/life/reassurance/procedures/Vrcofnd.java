package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.VpxReinsureRec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.dao.RasapfDAO;
import com.csc.life.reassurance.dataaccess.dao.RecopfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.dataaccess.model.Rasapf;
import com.csc.life.reassurance.dataaccess.model.Recopf;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Recopstrec;
import com.csc.life.reassurance.recordstructures.VrcorfndRec;
import com.csc.life.reassurance.tablestructures.T5445rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.life.reassurance.tablestructures.T5450rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Vrcofnd extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VRCOFND";
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private VrcorfndRec vrcorfndRec = new VrcorfndRec();
	private Map<String, List<List<String>>> inPutParam = new HashMap<>();
	private Map<String, List<List<List<String>>>> inPutParam2 = new HashMap<>();
	private Map<String, List<String>> inPutParam3 = new HashMap<>();
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private static final String T5449 = "T5449";
	private static final String T5445 = "T5445";
	private static final String T5450 = "T5450";
	private static final String T5687 = "T5687";
	private T5449rec t5449rec = new T5449rec();
	private T5445rec t5445rec = new T5445rec();
	private T5687rec t5687rec = new T5687rec();
	private T5450rec t5450rec = new T5450rec();
	private static final String E049 = "E049";
	private static final String R057 = "R057";
	private List<Lextpf> lextList = new ArrayList<>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private RasapfDAO rasapfDAO = getApplicationContext().getBean("rasapfDAO", RasapfDAO.class);
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private RecopfDAO recopfDAO = getApplicationContext().getBean("recopfDAO", RecopfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private RcvdpfDAO rcvdpfDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Recopstrec recopstrec = new Recopstrec();
	private VpxReinsureRec reinsurerec = new VpxReinsureRec();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private ZonedDecimalData wsaaCovrCount = new ZonedDecimalData(3, 0).setUnsigned();
	private boolean isTransEffDate = false;
	private PackedDecimalData wsaaRefundPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRefundCompay = new PackedDecimalData(17, 2);

	public Vrcofnd() {
		super();
	}

	@Override
	public void mainline(Object... parmArray) {
		rcorfndrec.rcorfndRec = convertAndSetParam(rcorfndrec.rcorfndRec, parmArray, 0);
		try {
			mainline1000();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void mainline1000() {
		initMap2();
		initMap3();
		initialise2000();
	}

	protected void initialise2000() {
		rcorfndrec.statuz.set(Varcom.oK);
		rcorfndrec.refundFee.set(ZERO);
		if (isNE(rcorfndrec.function, "UPDT")) {
			rcorfndrec.statuz.set(E049);
		}
		chdrpf = chdrpfDAO.getchdrRecord(rcorfndrec.chdrcoy.toString(), rcorfndrec.chdrnum.toString());
		List<Racdpf> racdpfList = racdpfDAO.searchRacdstRecord(rcorfndrec.chdrcoy.toString(),
				rcorfndrec.chdrnum.toString(), rcorfndrec.life.toString(), rcorfndrec.coverage.toString(),
				rcorfndrec.rider.toString(), rcorfndrec.planSuffix.toInt(), "1");
		if (null != racdpfList && !racdpfList.isEmpty()) {
			for (Racdpf racdpf : racdpfList) {
				if (isEQ(racdpf.getSeqno(), rcorfndrec.seqno) && isEQ(racdpf.getCurrfrom(), rcorfndrec.currfrom)) {
					Itempf itempf = itemDAO.findItemByDate("IT", rcorfndrec.chdrcoy.toString(), T5449,
							racdpf.getRngmnt(), rcorfndrec.effdate.toString());
					if (itempf != null) {
						t5449rec.t5449Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					} else {
						rcorfndrec.statuz.set(R057);
						return;
					}
					if (isLTE(racdpf.getCtdate(), rcorfndrec.effdate) || isEQ(t5449rec.prefbas, SPACES)) {
						return;
					}
					itempf = itemDAO.findItemByDate("IT", rcorfndrec.chdrcoy.toString(), T5445,
							t5449rec.prefbas.toString(), rcorfndrec.effdate.toString());
					if (itempf != null) {
						t5445rec.t5445Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					} else {
						rcorfndrec.statuz.set(R057);
						return;
					}
					Recopf recopf = recopfDAO.searchRecopfRecord(rcorfndrec.chdrcoy.toString(),
							rcorfndrec.chdrnum.toString(), rcorfndrec.life.toString(), rcorfndrec.coverage.toString(),
							rcorfndrec.rider.toString(), rcorfndrec.planSuffix.toInt(), rcorfndrec.seqno.toString(),
							Integer.toString(racdpf.getCtdate())); //IJS-414, earlier incorrect date was picked
			
					loadRequestData(racdpf, recopf);
					callRecopost400(recopf);
				}
			}
		}
	}

	protected void loadRequestData(Racdpf racdpf, Recopf recopf) {
		List<Agcmpf> agcmpfList = agcmpfDAO.searchAgcmstRecord(rcorfndrec.chdrcoy.toString(),
				rcorfndrec.chdrnum.toString(), rcorfndrec.life.toString(), rcorfndrec.coverage.toString(),
				rcorfndrec.rider.toString(), rcorfndrec.planSuffix.toInt(), "1");
		Covrpf mainCovr = covrpfDAO.getCovrRecord(rcorfndrec.chdrcoy.toString(), rcorfndrec.chdrnum.toString(),
				rcorfndrec.life.toString(), rcorfndrec.coverage.toString(), rcorfndrec.rider.toString(),
				rcorfndrec.planSuffix.toInt(), "1");
		vrcorfndRec.setChdrnum(rcorfndrec.chdrnum.toString());
		vrcorfndRec.setCnttype(rcorfndrec.cnttype.toString());
		vrcorfndRec.setCnttype(rcorfndrec.cnttype.toString());
		vrcorfndRec.setRiskComDate(chdrpf.getOccdate().toString());
		vrcorfndRec.setCovrrcesdate(rcorfndrec.effdate.toString());
		vrcorfndRec.setPtdate(chdrpf.getPtdate().toString());
		vrcorfndRec.setBillfreq(chdrpf.getBillfreq());
		vrcorfndRec.setPaidPrem("0");
		vrcorfndRec.setCoverage(rcorfndrec.coverage.toString());
		vrcorfndRec.setCrtable(rcorfndrec.crtable.toString());
		vrcorfndRec.setCovrcd(String.valueOf(mainCovr.getCrrcd()));
		vrcorfndRec.setCovrPaidPrem(String.valueOf(recopf.getPrem()));
		reinsurerec.setReinsCode(racdpf.getRasnum());

		if (null != agcmpfList && !agcmpfList.isEmpty()) {
			vrcorfndRec.setAgentClass(agcmpfList.get(0).getAgcls());
		}
		vrcorfndRec.setAnnprem("0");
		vrcorfndRec.setEffdate(rcorfndrec.effdate.toString());
		Recopf recopf2 = recopfDAO.getInitialCompay(rcorfndrec.chdrcoy.toString(),
				rcorfndrec.chdrnum.toString(), rcorfndrec.life.toString(), rcorfndrec.coverage.toString(),
				rcorfndrec.rider.toString(), rcorfndrec.planSuffix.toInt(), racdpf.getRasnum().trim(), racdpf.getRngmnt());
		if(null != recopf2){
			vrcorfndRec.setRcommtot(String.valueOf(recopf2.getCompay()));
		}
		setLifepfData();
		vrcorfndRec.setPremCessDate(String.valueOf(mainCovr.getPremCessDate()));
		vrcorfndRec.setSrcebus(chdrpf.getSrcebus());
		Itempf itempf = itemDAO.findItemByDate("IT", rcorfndrec.chdrcoy.toString(), T5687, mainCovr.getCrtable(),
				rcorfndrec.effdate.toString());
		if (itempf != null) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			vrcorfndRec.setPremMethod(t5687rec.premmeth.toString());
		}
		vrcorfndRec.setCurrcode(chdrpf.getCntcurr());
		vrcorfndRec.setBillfreq(chdrpf.getBillfreq()); // doubt
		if (isEQ(mainCovr.getRerateFromDate(), varcom.vrcmMaxDate) || isEQ(mainCovr.getRerateFromDate(), ZERO)) {
			vrcorfndRec.setRatingdate(chdrpf.getOccdate().toString());
		} else {
			vrcorfndRec.setRatingdate(String.valueOf(mainCovr.getRerateFromDate()));
		}
		vrcorfndRec.setTpdtype(mainCovr.getTpdtype());
		readCovrpf();
		inPutParam3.get("reinsCostfreq").add(recopf.getRcstfrq());
		inPutParam3.get("lifRiskcls").add(racdpf.getLrkcls());
		inPutParam3.get("reinsSumins").add(rcorfndrec.newSumins.toString());
		inPutParam3.get("arngCode").add(racdpf.getRngmnt());
		Rasapf rasapf = rasapfDAO.getRasapf(rcorfndrec.chdrcoy.toString(), racdpf.getRasnum().trim());
		if (null != rasapf)
			inPutParam3.get("reinsAccno").add(rasapf.getBankacckey().trim());
		else
			inPutParam3.get("reinsAccno").add("");
		setupT5449Params(racdpf.getRngmnt());
		vrcorfndRec.setNoOfCoverages(wsaaCovrCount.toString());
		vrcorfndRec.setCostfreq(recopf.getRcstfrq());
		transferDataToMap();
		setupRequestRec();
		if (AppVars.getInstance().getAppConfig().isVpmsEnable() && ExternalisedRules.isCallExternal("CALCVRCOFND")) {
			setupBusinessDate();
			callProgram("CALCVRCOFND", vrcorfndRec, reinsurerec);
			if (isTransEffDate) {
				AppVars.getInstance().setBusinessdate("");
			}
		}
		if (vrcorfndRec.statuz.toString().equals(Varcom.oK.toString())) {
			compute(wsaaRefundPrem, 5).set(vrcorfndRec.getReassCovr());
			compute(wsaaRefundCompay, 5).set(vrcorfndRec.getReassCAmount());
		} else {
			syserrrec.subrname.set(wsaaSubr);
		}
	}

	private void setupBusinessDate() {
		if (AppVars.getInstance().getBusinessdate() == null || isEQ(AppVars.getInstance().getBusinessdate(), "")) {
			String effDate = rcorfndrec.effdate.toString();
			String formatDate = effDate.substring(6, 8) + "/" + effDate.substring(4, 6) + "/" + effDate.substring(0, 4);
			AppVars.getInstance().setBusinessdate(formatDate);
			isTransEffDate = true;
		}
	}

	private void initMap2() {

		inPutParam.put("reinsSuminsList", new ArrayList<List<String>>());
		inPutParam.put("suminList", new ArrayList<List<String>>());
		inPutParam.put("covrCoverageList", new ArrayList<List<String>>());
		inPutParam.put("covrRiderIdList", new ArrayList<List<String>>());
		inPutParam.put("dobList", new ArrayList<List<String>>());
		inPutParam.put("mortclsList", new ArrayList<List<String>>());
		inPutParam.put("lsexList", new ArrayList<List<String>>());
		inPutParam.put("rstate01List", new ArrayList<List<String>>());
		inPutParam.put("durationList", new ArrayList<List<String>>());
		inPutParam.put("prmbasisList", new ArrayList<List<String>>());
		inPutParam.put("linkcovList", new ArrayList<List<String>>());
		inPutParam.put("covrsumList", new ArrayList<List<String>>());
		inPutParam.put("countList", new ArrayList<List<String>>());
		inPutParam.put("reinsArngtypList", new ArrayList<List<String>>());
		inPutParam.put("reinsTrtqtshrprcntList", new ArrayList<List<String>>());
		inPutParam.put("reinsTrtrtnlmtList", new ArrayList<List<String>>());
		inPutParam.put("reinsRtncurrList", new ArrayList<List<String>>());
		inPutParam.put("reinsCostfreqList", new ArrayList<List<String>>());
		inPutParam.put("reinsRatfactList", new ArrayList<List<String>>());
		inPutParam.put("reinsPremratList", new ArrayList<List<String>>());
		inPutParam.put("lifRiskclsList", new ArrayList<List<String>>());
		inPutParam.put("arngCodeList", new ArrayList<List<String>>());
		inPutParam.put("reinsAccnoList", new ArrayList<List<String>>());

		inPutParam.put("agerateList", new ArrayList<List<String>>());
		inPutParam.put("insprmList", new ArrayList<List<String>>());
		inPutParam.put("oppcList", new ArrayList<List<String>>());
		inPutParam.put("opcdaList", new ArrayList<List<String>>());
		inPutParam.put("zmortpctList", new ArrayList<List<String>>());

		inPutParam2.put("agerateList2", new ArrayList<List<List<String>>>());
		inPutParam2.put("insprmList2", new ArrayList<List<List<String>>>());
		inPutParam2.put("oppcList2", new ArrayList<List<List<String>>>());
		inPutParam2.put("opcdaList2", new ArrayList<List<List<String>>>());
		inPutParam2.put("zmortpctList2", new ArrayList<List<List<String>>>());
	}

	private void initMap3() {

		inPutParam3.put("reinsSumins", new ArrayList<String>());
		inPutParam3.put("sumin", new ArrayList<String>());
		inPutParam3.put("covrCoverage", new ArrayList<String>());
		inPutParam3.put("covrRiderId", new ArrayList<String>());
		inPutParam3.put("dob", new ArrayList<String>());
		inPutParam3.put("mortcls", new ArrayList<String>());
		inPutParam3.put("lsex", new ArrayList<String>());
		inPutParam3.put("rstate01", new ArrayList<String>());
		inPutParam3.put("duration", new ArrayList<String>());
		inPutParam3.put("prmbasis", new ArrayList<String>());
		inPutParam3.put("linkcov", new ArrayList<String>());
		inPutParam3.put("covrsum", new ArrayList<String>());
		inPutParam3.put("count", new ArrayList<String>());
		inPutParam3.put("reinsArngtyp", new ArrayList<String>());
		inPutParam3.put("reinsTrtqtshrprcnt", new ArrayList<String>());
		inPutParam3.put("reinsTrtrtnlmt", new ArrayList<String>());
		inPutParam3.put("reinsRtncurr", new ArrayList<String>());
		inPutParam3.put("reinsCostfreq", new ArrayList<String>());
		inPutParam3.put("reinsRatfact", new ArrayList<String>());
		inPutParam3.put("reinsPremrat", new ArrayList<String>());
		inPutParam3.put("lifRiskcls", new ArrayList<String>());
		inPutParam3.put("arngCode", new ArrayList<String>());
		inPutParam3.put("reinsAccno", new ArrayList<String>());

		inPutParam3.put("agerate", new ArrayList<String>());
		inPutParam3.put("insprm", new ArrayList<String>());
		inPutParam3.put("oppc", new ArrayList<String>());
		inPutParam3.put("opcda", new ArrayList<String>());
		inPutParam3.put("zmortpct", new ArrayList<String>());

	}

	protected void setupT5449Params(String rngmnt) {
		Itempf itempf = itemDAO.findItemByDate("IT", rcorfndrec.chdrcoy.toString(), T5449, rngmnt,
				rcorfndrec.effdate.toString());
		if (itempf != null) {
			t5449rec.t5449Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			inPutParam3.get("reinsArngtyp").add(t5449rec.retype.toString());
			inPutParam3.get("reinsTrtqtshrprcnt").add(t5449rec.qreshr01.toString()); // doubt
			inPutParam3.get("reinsTrtrtnlmt").add(String.valueOf(t5449rec.relimit01.toInt()));
			inPutParam3.get("reinsRtncurr").add(t5449rec.currcode.toString());

			itempf = itemDAO.findItemByDate("IT", rcorfndrec.chdrcoy.toString(), T5450, t5449rec.rprmmth01.toString(),
					rcorfndrec.effdate.toString());
			if (itempf != null) {
				t5450rec.t5450Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				inPutParam3.get("reinsRatfact").add(t5450rec.rratfac01.toString());
				inPutParam3.get("reinsPremrat").add(t5450rec.slctrate01.toString());
			}
		}
	}

	private void readCovrpf() {
		Covrpf covrpf = covrpfDAO.getCovrRecord(rcorfndrec.chdrcoy.toString(), rcorfndrec.chdrnum.toString(),
				rcorfndrec.life.toString(), rcorfndrec.coverage.toString(), rcorfndrec.rider.toString(),
				rcorfndrec.planSuffix.toInt(), "1");
		wsaaCovrCount.set(0);
		Lextpf lextpf = new Lextpf();
		lextpf.setChdrcoy(rcorfndrec.chdrcoy.toString());
		lextpf.setChdrnum(rcorfndrec.chdrnum.toString());
		lextList = lextpfDAO.getLextpfData(lextpf);

		wsaaCovrCount.add(1);
		inPutParam3.get("sumin").add(covrpf.getSumins().toString());
		inPutParam3.get("covrCoverage").add(covrpf.getCoverage());
		inPutParam3.get("covrRiderId").add(covrpf.getRider());
		inPutParam3.get("dob").add(vrcorfndRec.getCltdob());
		inPutParam3.get("mortcls").add(covrpf.getMortcls());
		inPutParam3.get("lsex").add(covrpf.getSex());
		inPutParam3.get("duration").add(calcDuration(covrpf.getPremCessDate()));
		inPutParam3.get("rstate01").add(covrpf.getZclstate());
		setRcvdpfData(covrpf);
		inPutParam3.get("linkcov").add("");
		inPutParam3.get("covrsum").add("");
		setLoadData(covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider());
	}

	protected void setRcvdpfData(Covrpf covrpf) {
		Rcvdpf rcvdpf = new Rcvdpf();
		rcvdpf.setChdrcoy(covrpf.getChdrcoy());
		rcvdpf.setChdrnum(covrpf.getChdrnum());
		rcvdpf.setLife(covrpf.getLife());
		rcvdpf.setCoverage(covrpf.getCoverage());
		rcvdpf.setRider(covrpf.getRider());
		rcvdpf.setCrtable(covrpf.getCrtable());
		rcvdpf = rcvdpfDAO.readRcvdpf(rcvdpf);
		if (null != rcvdpf && rcvdpf.getPrmbasis().equals("S"))
			inPutParam3.get("prmbasis").add("Y");
		else
			inPutParam3.get("prmbasis").add("");
	}

	protected void setLoadData(String life, String coverage, String rider) {
		int countLext = 0;
		if (null != lextList && !lextList.isEmpty()) {
			for (Lextpf lext : lextList) {
				if (isEQ(life, lext.getLife()) && isEQ(coverage, lext.getCoverage()) && isEQ(rider, lext.getRider())) {
					inPutParam3.get("agerate").add(String.valueOf(lext.getAgerate()));
					inPutParam3.get("insprm").add(String.valueOf(lext.getInsprm()));
					inPutParam3.get("oppc").add(String.valueOf(lext.getOppc()));
					inPutParam3.get("opcda").add(lext.getOpcda());
					inPutParam3.get("zmortpct").add(String.valueOf(lext.getZmortpct()));
					countLext++;
				}
			}
		} else {
			inPutParam3.get("agerate").add("0");
			inPutParam3.get("insprm").add("0");
			inPutParam3.get("oppc").add("0");
			inPutParam3.get("opcda").add("");
			inPutParam3.get("zmortpct").add("0");
		}
		inPutParam3.get("count").add(String.valueOf(countLext));
	}

	protected void setLifepfData() {
		Lifepf lifepf = lifepfDAO.getLifeRecord(rcorfndrec.chdrcoy.toString(), rcorfndrec.chdrnum.toString(), rcorfndrec.life.toString(),
				"00");
		if (null != lifepf) {
			vrcorfndRec.setCltdob(String.valueOf(new BigDecimal(lifepf.getCltdob())));
			vrcorfndRec.setLage(String.valueOf(lifepf.getAnbAtCcd()));
			Clexpf clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", lifepf.getLifcnum());
			if (clexpf != null)
				vrcorfndRec.setRstaflag(clexpf.getRstaflag());
			else
				vrcorfndRec.setRstaflag("");
		}
	}

	protected void callRecopost400(Recopf recopf) {

		recopstrec.function.set("RFND");
		recopstrec.language.set(rcorfndrec.language);
		recopstrec.chdrcoy.set(rcorfndrec.chdrcoy);
		recopstrec.chdrnum.set(recopf.getChdrnum());
		recopstrec.life.set(recopf.getLife());
		recopstrec.coverage.set(recopf.getCoverage());
		recopstrec.rider.set(recopf.getRider());
		recopstrec.planSuffix.set(recopf.getPlnsfx());
		recopstrec.rasnum.set(recopf.getRasnum());
		recopstrec.seqno.set(rcorfndrec.seqno);
		recopstrec.cnttype.set(rcorfndrec.cnttype);
		recopstrec.effdate.set(recopf.getCostdate());
		recopstrec.ctdate.set(recopf.getCtdate());
		recopstrec.costdate.set(recopf.getCostdate());
		recopstrec.validflag.set(recopf.getValidflag());
		recopstrec.retype.set(recopf.getRetype());
		recopstrec.rngmnt.set(recopf.getRngmnt());
		recopstrec.batccoy.set(rcorfndrec.batccoy);
		recopstrec.batcbrn.set(rcorfndrec.batcbrn);
		recopstrec.batcactyr.set(rcorfndrec.batcactyr);
		recopstrec.batcactmn.set(rcorfndrec.batcactmn);
		recopstrec.batctrcde.set(rcorfndrec.batctrcde);
		recopstrec.batcbatch.set(rcorfndrec.batcbatch);
		recopstrec.tranno.set(rcorfndrec.tranno);
		recopstrec.ctdate.set(recopf.getCtdate());
		recopstrec.sraramt.set(recopf.getSraramt());
		recopstrec.raAmount.set(recopf.getRaAmount());
		recopstrec.origcurr.set(recopf.getOrigcurr());
		recopstrec.prem.set(wsaaRefundPrem);
		recopstrec.compay.set(wsaaRefundCompay);
		recopstrec.rcstfrq.set(recopf.getRcstfrq());
		recopstrec.taxamt.set(ZERO);
		recopstrec.recovAmt.set(ZERO);
		recopstrec.crtable.set(rcorfndrec.crtable);
		recopstrec.vpmsflag.set(1);
		callProgram(Rfndpst.class, recopstrec.recopstRec);
		if (isNE(recopstrec.statuz, Varcom.oK)) {
			rcorfndrec.statuz.set(recopstrec.statuz);
		}
	}

	protected String calcDuration(int pcesdte) {
		datcon3rec.intDate1.set(rcorfndrec.effdate);
		datcon3rec.intDate2.set(pcesdte);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			rcorfndrec.statuz.set(datcon3rec.statuz);
		}
		return String.valueOf(datcon3rec.freqFactor.toInt());
	}

	protected void transferDataToMap() {

		inPutParam.get("reinsSuminsList").add(inPutParam3.get("reinsSumins"));
		inPutParam.get("suminList").add(inPutParam3.get("sumin"));
		inPutParam.get("covrCoverageList").add(inPutParam3.get("covrCoverage"));
		inPutParam.get("covrRiderIdList").add(inPutParam3.get("covrRiderId"));
		inPutParam.get("dobList").add(inPutParam3.get("dob"));
		inPutParam.get("mortclsList").add(inPutParam3.get("mortcls"));
		inPutParam.get("lsexList").add(inPutParam3.get("lsex"));
		inPutParam.get("rstate01List").add(inPutParam3.get("rstate01"));
		inPutParam.get("durationList").add(inPutParam3.get("duration"));
		inPutParam.get("prmbasisList").add(inPutParam3.get("prmbasis"));
		inPutParam.get("linkcovList").add(inPutParam3.get("linkcov"));
		inPutParam.get("covrsumList").add(inPutParam3.get("covrsum"));
		inPutParam.get("countList").add(inPutParam3.get("count"));
		inPutParam.get("reinsArngtypList").add(inPutParam3.get("reinsArngtyp"));
		inPutParam.get("reinsTrtqtshrprcntList").add(inPutParam3.get("reinsTrtqtshrprcnt"));
		inPutParam.get("reinsTrtrtnlmtList").add(inPutParam3.get("reinsTrtrtnlmt"));
		inPutParam.get("reinsRtncurrList").add(inPutParam3.get("reinsRtncurr"));
		inPutParam.get("reinsCostfreqList").add(inPutParam3.get("reinsCostfreq"));
		inPutParam.get("reinsRatfactList").add(inPutParam3.get("reinsRatfact"));
		inPutParam.get("reinsPremratList").add(inPutParam3.get("reinsPremrat"));
		inPutParam.get("lifRiskclsList").add(inPutParam3.get("lifRiskcls"));
		inPutParam.get("arngCodeList").add(inPutParam3.get("arngCode"));
		inPutParam.get("reinsAccnoList").add(inPutParam3.get("reinsAccno"));

		inPutParam.get("agerateList").add(inPutParam3.get("agerate"));
		inPutParam2.get("agerateList2").add(inPutParam.get("agerateList"));

		inPutParam.get("insprmList").add(inPutParam3.get("insprm"));
		inPutParam2.get("insprmList2").add(inPutParam.get("insprmList"));

		inPutParam.get("oppcList").add(inPutParam3.get("oppc"));
		inPutParam2.get("oppcList2").add(inPutParam.get("oppcList"));

		inPutParam.get("opcdaList").add(inPutParam3.get("opcda"));
		inPutParam2.get("opcdaList2").add(inPutParam.get("opcdaList"));

		inPutParam.get("zmortpctList").add(inPutParam3.get("zmortpct"));
		inPutParam2.get("zmortpctList2").add(inPutParam.get("zmortpctList"));

	}

	protected void setupRequestRec() {

		vrcorfndRec.setSumin(inPutParam.get("suminList"));
		vrcorfndRec.setCovrCoverage(inPutParam.get("covrCoverageList"));
		vrcorfndRec.setCovrRiderId(inPutParam.get("covrRiderIdList"));
		vrcorfndRec.setDob(inPutParam.get("dobList"));
		vrcorfndRec.setMortcls(inPutParam.get("mortclsList"));
		vrcorfndRec.setLsex(inPutParam.get("lsexList"));
		vrcorfndRec.setRstate01(inPutParam.get("rstate01List"));
		vrcorfndRec.setDuration(inPutParam.get("durationList"));
		vrcorfndRec.setPrmbasis(inPutParam.get("prmbasisList"));
		vrcorfndRec.setLinkcov(inPutParam.get("linkcovList"));
		vrcorfndRec.setCovrsum(inPutParam.get("covrsumList"));
		vrcorfndRec.setCount(inPutParam.get("countList"));
		vrcorfndRec.setAgerate(inPutParam2.get("agerateList2"));
		vrcorfndRec.setInsprm(inPutParam2.get("insprmList2"));
		vrcorfndRec.setOppc(inPutParam2.get("oppcList2"));
		vrcorfndRec.setOpcda(inPutParam2.get("opcdaList2"));
		vrcorfndRec.setZmortpct(inPutParam2.get("zmortpctList2"));

		reinsurerec.setReinsSumins(inPutParam.get("reinsSuminsList"));
		reinsurerec.setReinsArngtyp(inPutParam.get("reinsArngtypList"));
		reinsurerec.setReinsTrtqtshrprcnt(inPutParam.get("reinsTrtqtshrprcntList"));
		reinsurerec.setReinsTrtrtnlmt(inPutParam.get("reinsTrtrtnlmtList"));
		reinsurerec.setReinsRtncurr(inPutParam.get("reinsRtncurrList"));
		reinsurerec.setReinsCostfreq(inPutParam.get("reinsCostfreqList"));
		reinsurerec.setReinsRatfact(inPutParam.get("reinsRatfactList"));
		reinsurerec.setReinsPremrat(inPutParam.get("reinsPremratList"));
		reinsurerec.setLifRiskcls(inPutParam.get("lifRiskclsList"));
		reinsurerec.setArngCode(inPutParam.get("arngCodeList"));
		reinsurerec.setReinsAccno(inPutParam.get("reinsAccnoList"));

	}
}
