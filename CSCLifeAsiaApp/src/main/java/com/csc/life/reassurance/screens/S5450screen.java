package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5450screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5450ScreenVars sv = (S5450ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5450screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5450ScreenVars screenVars = (S5450ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.premsubr01.setClassString("");
		screenVars.rjlprem01.setClassString("");
		screenVars.slctrate01.setClassString("");
		screenVars.ultmrate01.setClassString("");
		screenVars.basicCommMeth01.setClassString("");
		screenVars.inlprd01.setClassString("");
		screenVars.rratfac01.setClassString("");
		screenVars.commrate01.setClassString("");
		screenVars.rprmcls01.setClassString("");
		screenVars.premsubr02.setClassString("");
		screenVars.rjlprem02.setClassString("");
		screenVars.slctrate02.setClassString("");
		screenVars.ultmrate02.setClassString("");
		screenVars.inlprd02.setClassString("");
		screenVars.basicCommMeth02.setClassString("");
		screenVars.commrate02.setClassString("");
		screenVars.rprmcls02.setClassString("");
		screenVars.rratfac02.setClassString("");
		screenVars.rprmcls03.setClassString("");
		screenVars.premsubr03.setClassString("");
		screenVars.rjlprem03.setClassString("");
		screenVars.slctrate03.setClassString("");
		screenVars.ultmrate03.setClassString("");
		screenVars.inlprd03.setClassString("");
		screenVars.basicCommMeth03.setClassString("");
		screenVars.commrate03.setClassString("");
		screenVars.rratfac03.setClassString("");
		screenVars.rprmcls04.setClassString("");
		screenVars.premsubr04.setClassString("");
		screenVars.rjlprem04.setClassString("");
		screenVars.slctrate04.setClassString("");
		screenVars.ultmrate04.setClassString("");
		screenVars.inlprd04.setClassString("");
		screenVars.basicCommMeth04.setClassString("");
		screenVars.commrate04.setClassString("");
		screenVars.rratfac04.setClassString("");
		screenVars.premsubr05.setClassString("");
		screenVars.rjlprem05.setClassString("");
		screenVars.slctrate05.setClassString("");
		screenVars.ultmrate05.setClassString("");
		screenVars.inlprd05.setClassString("");
		screenVars.basicCommMeth05.setClassString("");
		screenVars.commrate05.setClassString("");
		screenVars.rratfac05.setClassString("");
		screenVars.rprmcls05.setClassString("");
	}

/**
 * Clear all the variables in S5450screen
 */
	public static void clear(VarModel pv) {
		S5450ScreenVars screenVars = (S5450ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.premsubr01.clear();
		screenVars.rjlprem01.clear();
		screenVars.slctrate01.clear();
		screenVars.ultmrate01.clear();
		screenVars.basicCommMeth01.clear();
		screenVars.inlprd01.clear();
		screenVars.rratfac01.clear();
		screenVars.commrate01.clear();
		screenVars.rprmcls01.clear();
		screenVars.premsubr02.clear();
		screenVars.rjlprem02.clear();
		screenVars.slctrate02.clear();
		screenVars.ultmrate02.clear();
		screenVars.inlprd02.clear();
		screenVars.basicCommMeth02.clear();
		screenVars.commrate02.clear();
		screenVars.rprmcls02.clear();
		screenVars.rratfac02.clear();
		screenVars.rprmcls03.clear();
		screenVars.premsubr03.clear();
		screenVars.rjlprem03.clear();
		screenVars.slctrate03.clear();
		screenVars.ultmrate03.clear();
		screenVars.inlprd03.clear();
		screenVars.basicCommMeth03.clear();
		screenVars.commrate03.clear();
		screenVars.rratfac03.clear();
		screenVars.rprmcls04.clear();
		screenVars.premsubr04.clear();
		screenVars.rjlprem04.clear();
		screenVars.slctrate04.clear();
		screenVars.ultmrate04.clear();
		screenVars.inlprd04.clear();
		screenVars.basicCommMeth04.clear();
		screenVars.commrate04.clear();
		screenVars.rratfac04.clear();
		screenVars.premsubr05.clear();
		screenVars.rjlprem05.clear();
		screenVars.slctrate05.clear();
		screenVars.ultmrate05.clear();
		screenVars.inlprd05.clear();
		screenVars.basicCommMeth05.clear();
		screenVars.commrate05.clear();
		screenVars.rratfac05.clear();
		screenVars.rprmcls05.clear();
	}
}
