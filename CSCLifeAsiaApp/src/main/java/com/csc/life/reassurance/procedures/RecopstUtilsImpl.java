package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.productdefinition.procedures.LifacmvPojo;
import com.csc.life.productdefinition.procedures.LifacmvUtils;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.dao.RecopfDAO;
import com.csc.life.reassurance.dataaccess.model.Recopf;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Reassurance Costing Posting Subroutine.
*
*
*  This subroutine will be called from the main module of the
*  Reassurance Costing History - B5456.
*
*  It will write costing history records and carry out the
*  necessary ledger posting for each costing event.
*
*  It will perform the following functions:
*
*  1) Creation of RECO record to hold all the costing
*     information.
*
*  2) Conversion of Premium Currency where necessary.
*
*  3) Posting of all relevant ledger entriesd via 'LIFACMV',
*     as per the T5645 item of 'RECOPST'.
*
*  Processing.
*  -----------
*
*  Set the return STATUZ in linkage to O-K.
*
*  Process only if a function of 'RCST' has been passed.
*
*  Read table T5645 for sub account details of the Contract.
*
*  Read table T5688 to find out if the contract type wants
*  Component level accounting.
*
*  Create a RECO record to hold all the costing information.
*
*  Posting of all relevant Ledger entries, via LIFACMV, as
*  per the T5645 item of the same name as this subroutine.
*
*
*****************************************************************
* </pre>
*/

public class RecopstUtilsImpl implements RecopstUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(RecopstUtilsImpl.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaSubr = "RECOPST";
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private Syserrrec syserrrec = new Syserrrec();
	private Syserr syserr = new Syserr();
	private Varcom varcom = new Varcom();
	private String e049 = "E049";
	
	private FixedLengthStringData wsaaWorkAreas = new FixedLengthStringData(34);
	private ZonedDecimalData wsaaNetPrem = new ZonedDecimalData(13, 2).isAPartOf(wsaaWorkAreas, 0);
	private PackedDecimalData wsaaRecovAmt = new PackedDecimalData(17, 2).isAPartOf(wsaaWorkAreas, 13);
	private PackedDecimalData wsaaTotClaim = new PackedDecimalData(18, 2).isAPartOf(wsaaWorkAreas, 22);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaWorkAreas, 32);
	
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5688 = "T5688";
	
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RecopfDAO recopfDAO = getApplicationContext().getBean("recopfDAO", RecopfDAO.class);
	private DescpfDAO descpfDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private LifacmvUtils lifacmvUtils = getApplicationContext().getBean("lifacmvUtils", LifacmvUtils.class);
	private LifacmvPojo lifacmvPojo;
	
	private Itempf itempf;

	
	
	public RecopstPojo processRecopst(RecopstPojo recopstPojo) {
		
		syserrrec.subrname.set(wsaaSubr);
		wsaaTransTime.set(getCobolTime());
		if (!"RCST".equals(recopstPojo.getFunction())) {
			syserrrec.statuz.set(e049);
			syserrrec.syserrType.set("2");
			recopstPojo.setStatuz("BOMB");
			return recopstPojo;
		}
		
		initial(recopstPojo);
		preparations(recopstPojo);
		createReco(recopstPojo);
		postingProcessing(recopstPojo);
		return recopstPojo;
	}
	
	protected void postingProcessing(RecopstPojo recopstPojo)
	{
		startPostingProcessing(recopstPojo);
		posting(recopstPojo);
	}
	
	protected void posting(RecopstPojo recopstPojo)
	{
		if ("RCST".equals(recopstPojo.getFunction())) {
			costingPosting(recopstPojo);
		}
		/*EXIT*/
	}
	
	protected void costingPosting(RecopstPojo recopstPojo)
	{
		/*COST-POST*/
		postPremium(recopstPojo);
		postCommission(recopstPojo);
		postTax(recopstPojo);
		postNetPrem(recopstPojo);
		/*EXIT*/
	}
	
	protected void postNetPrem(RecopstPojo recopstPojo){
		/*NPREM-POST*/
		lifacmvPojo.setOrigamt(wsaaNetPrem.getbigdata());
		lifacmvPojo.setRldgacct(recopstPojo.getRasnum());
		wsaaSub1.set(3);
		callLifacmv(recopstPojo);
		/*EXIT*/
	}
	
	protected void postTax(RecopstPojo recopstPojo){
		/*TAX-POST*/
		lifacmvPojo.setOrigamt(recopstPojo.getTaxamt());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub1.set(7);
			lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
		}
		else {
			wsaaSub1.set(6);
			lifacmvPojo.setRldgacct(recopstPojo.getChdrnum());
		}
		callLifacmv(recopstPojo);
		/*EXIT*/
	}
	
	protected void postCommission(RecopstPojo recopstPojo)
	{
		/*COMM-POST*/
		lifacmvPojo.setOrigamt(recopstPojo.getCompay());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub1.set(5);
			lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
		}
		else {
			wsaaSub1.set(2);
			lifacmvPojo.setRldgacct(recopstPojo.getChdrnum());
		}
		callLifacmv(recopstPojo);
		/*EXIT*/
	}
	
	
	protected void postPremium(RecopstPojo recopstPojo)
	{
		/*PREM-POST*/
		lifacmvPojo.setOrigamt(recopstPojo.getPrem());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub1.set(4);
			lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
		}
		else {
			wsaaSub1.set(1);
			lifacmvPojo.setRldgacct(recopstPojo.getChdrnum());
		}
		callLifacmv(recopstPojo);
		/*EXIT*/
	}
	
	protected void callLifacmv(RecopstPojo recopstPojo)
	{	
		if (lifacmvPojo.getOrigamt().compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		lifacmvPojo.setSacscode(t5645rec.sacscode[wsaaSub1.toInt()].toString());
		lifacmvPojo.setSacstyp(t5645rec.sacstype[wsaaSub1.toInt()].toString());
		lifacmvPojo.setGlcode(t5645rec.glmap[wsaaSub1.toInt()].toString());
		lifacmvPojo.setGlsign(t5645rec.sign[wsaaSub1.toInt()].toString());
		lifacmvPojo.setContot(t5645rec.cnttot[wsaaSub1.toInt()].toInt());
		lifacmvUtils.calcLifacmv(lifacmvPojo);
		if (!"****".equals(lifacmvPojo.getStatuz())) {
			syserrrec.params.set(lifacmvPojo);
			syserrrec.statuz.set(lifacmvPojo.getStatuz());
			syserr(recopstPojo);
		}

	}
	
	protected void startPostingProcessing(RecopstPojo recopstPojo)
	{
		String longdesc = descpfDAO.getLongDesc("IT", recopstPojo.getChdrcoy(), 
				t1688, recopstPojo.getBatctrcde(), recopstPojo.getLanguage());
		if (longdesc==null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.syserrType.set("1");
			syserrrec.params.set(recopstPojo.getChdrcoy().concat(t1688)
					.concat(recopstPojo.getBatctrcde()));
			syserr(recopstPojo);
		}
		
		lifacmvPojo = new LifacmvPojo();
		lifacmvPojo.setJrnseq(recopstPojo.getSeqno());
		lifacmvPojo.setFunction("PSTW");
		lifacmvPojo.setTrandesc(longdesc);
		lifacmvPojo.setBatccoy(recopstPojo.getBatccoy());
		lifacmvPojo.setBatcbrn(recopstPojo.getBatcbrn());
		lifacmvPojo.setBatcactyr(recopstPojo.getBatcactyr());
		lifacmvPojo.setBatcactmn(recopstPojo.getBatcactmn());
		lifacmvPojo.setBatctrcde(recopstPojo.getBatctrcde());
		lifacmvPojo.setBatcbatch(recopstPojo.getBatcbatch());
		lifacmvPojo.setRdocnum(recopstPojo.getChdrnum());
		lifacmvPojo.setTranno(recopstPojo.getTranno());
		lifacmvPojo.setRldgcoy(recopstPojo.getChdrcoy());
		lifacmvPojo.setOrigcurr(recopstPojo.getOrigcurr());
		lifacmvPojo.setGenlcoy(recopstPojo.getChdrcoy());
		lifacmvPojo.setEffdate(recopstPojo.getCostdate());
		lifacmvPojo.setRcamt(0);
		lifacmvPojo.setFrcdate(varcom.vrcmMaxDate.toInt());
		lifacmvPojo.setTransactionDate(recopstPojo.getEffdate());
		lifacmvPojo.setTransactionTime(wsaaTransTime.toInt());
		lifacmvPojo.setUser(0);
		lifacmvPojo.setTermid(SPACES.toString());
		String[] substituteCodes = new String[6];
	    substituteCodes[0] = recopstPojo.getCnttype();
	    lifacmvPojo.setSubstituteCodes(substituteCodes);   	
		lifacmvPojo.setTranref(recopstPojo.getChdrnum());
		wsaaRldgChdrnum.set(recopstPojo.getChdrnum());
		wsaaRldgLife.set(recopstPojo.getLife());
		wsaaRldgCoverage.set(recopstPojo.getCoverage());
		wsaaRldgRider.set(recopstPojo.getRider());
		wsaaPlan.set(recopstPojo.getPlanSuffix());
		wsaaRldgPlanSuff.set(wsaaPlansuff);
		lifacmvPojo.setAcctamt(BigDecimal.ZERO);
		lifacmvPojo.setCrate(BigDecimal.ZERO);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			substituteCodes[5] = recopstPojo.getCrtable();
		}
		else {
			substituteCodes[5] = "";
		}
	}

	
	protected void createReco(RecopstPojo recopstPojo){
		Recopf recopf = new Recopf();
		recopf.setChdrcoy(recopstPojo.getChdrcoy());
		recopf.setChdrnum(recopstPojo.getChdrnum());
		recopf.setLife(recopstPojo.getLife());
		recopf.setCoverage(recopstPojo.getCoverage());
		recopf.setRider(recopstPojo.getRider());
		recopf.setPlnsfx(recopstPojo.getPlanSuffix());
		recopf.setRasnum(recopstPojo.getRasnum());
		recopf.setSeqno(recopstPojo.getSeqno());
		recopf.setCostdate(recopstPojo.getCostdate());
		recopf.setValidflag(recopstPojo.getValidflag());
		recopf.setRetype(recopstPojo.getRetype());
		recopf.setRngmnt(recopstPojo.getRngmnt());
		recopf.setSraramt(recopstPojo.getSraramt());
		recopf.setRaAmount(recopstPojo.getRaAmount());
		recopf.setCtdate(recopstPojo.getCtdate());
		recopf.setOrigcurr(recopstPojo.getOrigcurr());
		recopf.setPrem(recopstPojo.getPrem());
		recopf.setCompay(recopstPojo.getCompay());
		recopf.setTaxamt(recopstPojo.getTaxamt());
		recopf.setRefundfe(BigDecimal.ZERO);
		recopf.setBatccoy(recopstPojo.getBatccoy());
		recopf.setBatcbrn(recopstPojo.getBatcbrn());
		recopf.setBatcactyr(recopstPojo.getBatcactyr());
		recopf.setBatcactmn(recopstPojo.getBatcactmn());
		recopf.setBatctrcde(recopstPojo.getBatctrcde());
		recopf.setBatcbatch(recopstPojo.getBatcbatch());
		recopf.setTranno(recopstPojo.getTranno());
		recopf.setRcstfrq(recopstPojo.getRcstfrq());
		recopf.setRPRate(recopstPojo.getRPRate());
		recopf.setAnnprem(recopstPojo.getAnnprem()); 
		if(recopfDAO.insertRecopf(recopf)<=0) {
			syserrrec.params.set(recopf.getChdrcoy().concat(recopf.getChdrnum()));
			syserrrec.syserrType.set("1");
			syserr(recopstPojo);
		}
	}
	
	protected void initial(RecopstPojo recopstPojo){
		recopstPojo.setStatuz("****");		
		initializeWsaaWorkAreas();
		readT5645(recopstPojo);
		readT5688(recopstPojo);
	
	}
	
	protected void preparations(RecopstPojo recopstPojo){
		/*CURRENCY-CHECK*/
		wsaaRecovAmt.set(recopstPojo.getRecovAmt());
		compute(wsaaNetPrem, 3).setRounded(sub(add(recopstPojo.getPrem(),recopstPojo.getTaxamt()),recopstPojo.getCompay()));
		/*EXIT*/
	}
	
	private void readT5645(RecopstPojo recopstPojo) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(recopstPojo.getChdrcoy());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaSubr);
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf!=null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
		}
		else {		
			syserrrec.params.set(recopstPojo.getChdrcoy().concat(t5645).concat(wsaaSubr));
			syserrrec.statuz.set("MRNF");
			syserr(recopstPojo);
		}
		
	}
	
	private void readT5688(RecopstPojo recopstPojo) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(recopstPojo.getChdrcoy());
		itempf.setItemtabl(t5688);
		itempf.setItemitem(wsaaSubr);
		itempf = itemDao.getItemRecordByItemkey(itempf);
		
	
		List<Itempf> list = itemDao.getItdmByFrmdate(recopstPojo.getChdrcoy(), t5688, recopstPojo.getCnttype(), recopstPojo.getEffdate());
		
		if(!list.isEmpty()) {
			t5688rec.t5688Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		}
				
	}
	
	private void initializeWsaaWorkAreas() {
		wsaaNetPrem.set(ZERO);
		wsaaRecovAmt.set(ZERO);
		wsaaTotClaim.set(ZERO);
		wsaaSub1.set(ZERO);
	}
	
	public void syserr(RecopstPojo recopstPojo){
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(syserrrec.syserrType);
		syserr.mainline(new Object[] { syserrrec.syserrRec });
		recopstPojo.setStatuz("BOMB");
		LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
		throw new RuntimeException(new COBOLExitProgramException());
	}
	
	protected ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
	
	
}
