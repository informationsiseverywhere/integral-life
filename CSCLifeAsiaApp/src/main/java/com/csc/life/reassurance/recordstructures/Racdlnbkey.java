package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:20
 * Description:
 * Copybook name: RACDLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdlnbKey = new FixedLengthStringData(64).isAPartOf(racdlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(racdlnbKey, 0);
  	public FixedLengthStringData racdlnbChdrnum = new FixedLengthStringData(8).isAPartOf(racdlnbKey, 1);
  	public FixedLengthStringData racdlnbLife = new FixedLengthStringData(2).isAPartOf(racdlnbKey, 9);
  	public FixedLengthStringData racdlnbCoverage = new FixedLengthStringData(2).isAPartOf(racdlnbKey, 11);
  	public FixedLengthStringData racdlnbRider = new FixedLengthStringData(2).isAPartOf(racdlnbKey, 13);
  	public PackedDecimalData racdlnbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdlnbKey, 15);
  	public PackedDecimalData racdlnbSeqno = new PackedDecimalData(2, 0).isAPartOf(racdlnbKey, 18);
  	public FixedLengthStringData racdlnbCestype = new FixedLengthStringData(1).isAPartOf(racdlnbKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(racdlnbKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}