package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.B5464DTO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RacdpfDAOImpl extends BaseDAOImpl<Racdpf> implements RacdpfDAO {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(RacdpfDAOImpl.class);
    
	@Override
	public List<B5464DTO> findRacdResult(String wsaaChdrnumfrm, String wsaaChdrnumto, int wsaaSqlEffdate,
			int minRecord, int maxRecord) {
		
        StringBuilder sqlRacdrskSelect = new StringBuilder();
        sqlRacdrskSelect.append("SELECT * FROM (");
        sqlRacdrskSelect.append("SELECT ROW_NUMBER() OVER (ORDER BY RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO) AS ROWNM,");
        sqlRacdrskSelect.append("RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO, RA.RASNUM, RA.RNGMNT, RA.CURRFROM, RA.TRANNO, RA.VALIDFLAG, RA.CURRCODE, RA.CURRTO, RA.RETYPE, RA.RAAMOUNT, RA.CTDATE, RA.CMDATE, RA.REASPER, RA.RECOVAMT, RA.CESTYPE, RA.LRKCLS, RA.RREVDT,");
        sqlRacdrskSelect.append("CH.TRANNO CHTRANNO,CH.CNTTYPE CHCNTTYPE,CH.STATCODE CHSTATCODE,CH.PSTCDE CHPSTCDE,CH.BILLFREQ CHBILLFREQ,CH.CNTCURR CHCNTCURR,CH.POLSUM CHPOLSUM,CH.BILLCHNL CHBILLCHNL,CH.OCCDATE CHOCCDATE,CH.POLINC CHPOLINC, CH.PTDATE CHPTDATE,");
        sqlRacdrskSelect.append("CO.CRRCD COCRRCD,CO.CRTABLE COCRTABLE,CO.SUMINS COSUMINS,CO.JLIFE COJLIFE,CO.INSTPREM COINSTPREM,CO.CHDRCOY COCHDRCOY,CO.CHDRNUM COCHDRNUM,CO.LIFE COLIFE,CO.COVERAGE COCOVERAGE,CO.RIDER CORIDER,CO.MORTCLS COMORTCLS,CO.RCESDTE CORCESDTE,CO.VALIDFLAG COVALIDFLAG,");
        sqlRacdrskSelect.append("LI.JLIFE, LI.LIFCNUM, RA.UNIQUE_NUMBER RAUQ, CH.UNIQUE_NUMBER CHUQ");
        sqlRacdrskSelect.append(" FROM RACDPF RA");
        sqlRacdrskSelect.append(" JOIN CHDRPF CH ON RA.CHDRCOY = CH.CHDRCOY AND RA.CHDRNUM = CH.CHDRNUM");
        sqlRacdrskSelect.append(" JOIN COVRPF CO ON RA.CHDRCOY = CO.CHDRCOY AND RA.CHDRNUM = CO.CHDRNUM AND RA.LIFE = CO.LIFE AND RA.COVERAGE = CO.COVERAGE AND RA.RIDER = CO.RIDER AND RA.PLNSFX = CO.PLNSFX");
        sqlRacdrskSelect.append(" JOIN LIFEPF LI ON RA.CHDRCOY = LI.CHDRCOY AND RA.CHDRNUM = LI.CHDRNUM AND RA.LIFE = LI.LIFE");
        sqlRacdrskSelect.append(" WHERE RA.CHDRNUM BETWEEN ? AND ? AND (RA.RREVDT < ? OR RA.RREVDT = ?) AND (RA.RREVDT < RA.CTDATE OR RA.RREVDT = RA.CTDATE) AND RA.VALIDFLAG = '1'");
        sqlRacdrskSelect.append(" GROUP BY RA.UNIQUE_NUMBER, RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO, RA.RASNUM, RA.RNGMNT, RA.CURRFROM, RA.TRANNO, RA.VALIDFLAG, RA.CURRCODE, RA.CURRTO, RA.RETYPE, RA.RAAMOUNT, RA.CTDATE, RA.CMDATE, RA.REASPER, RA.RECOVAMT, RA.CESTYPE, RA.LRKCLS, RA.RREVDT,"); 
        sqlRacdrskSelect.append(" CH.TRANNO, CH.CNTTYPE, CH.STATCODE, CH.PSTCDE, CH.BILLFREQ, CH.CNTCURR, CH.POLSUM, CH.BILLCHNL, CH.CNTCURR, CH.OCCDATE, CH.POLINC, CH.PTDATE, CH.UNIQUE_NUMBER,");
        sqlRacdrskSelect.append(" CO.CRRCD, CO.CRTABLE, CO.SUMINS, CO.JLIFE, CO.INSTPREM, CO.CHDRCOY, CO.CHDRNUM, CO.LIFE, CO.COVERAGE, CO.RIDER, CO.MORTCLS, CO.RCESDTE, CO.VALIDFLAG,");
        sqlRacdrskSelect.append(" LI.JLIFE, LI.LIFCNUM)");
        sqlRacdrskSelect.append(" RACDRS WHERE ROWNM>=? AND ROWNM<? ORDER BY ROWNM;");
        
		PreparedStatement psRacdrskSelect = getPrepareStatement(sqlRacdrskSelect.toString());
		List<B5464DTO> racdSearchResultDtoList = new LinkedList<B5464DTO>();
		ResultSet sqlracdpf1rs = null;
        try {
			psRacdrskSelect.setString(1, wsaaChdrnumfrm);
			psRacdrskSelect.setString(2, wsaaChdrnumto);
			psRacdrskSelect.setInt(3, wsaaSqlEffdate);
			psRacdrskSelect.setInt(4, wsaaSqlEffdate);
			psRacdrskSelect.setInt(5, minRecord);
			psRacdrskSelect.setInt(6, maxRecord);

			sqlracdpf1rs = executeQuery(psRacdrskSelect);

            while (sqlracdpf1rs.next()) {
                B5464DTO racdSearchResultDto = new B5464DTO();
                racdSearchResultDto.setSqlChdrcoy(sqlracdpf1rs.getString(2));
                racdSearchResultDto.setSqlChdrnum(sqlracdpf1rs.getString(3));
                racdSearchResultDto.setSqlLife(sqlracdpf1rs.getString(4));
                racdSearchResultDto.setSqlCoverage(sqlracdpf1rs.getString(5));
                racdSearchResultDto.setSqlRider(sqlracdpf1rs.getString(6));
                racdSearchResultDto.setSqlPlnsfx(sqlracdpf1rs.getInt(7));
                racdSearchResultDto.setSqlSeqno(sqlracdpf1rs.getInt(8));
                racdSearchResultDto.setSqlRasnum(sqlracdpf1rs.getString(9));
                racdSearchResultDto.setSqlRngmnt(sqlracdpf1rs.getString(10));
                racdSearchResultDto.setSqlCurrfrom(sqlracdpf1rs.getInt(11));
                racdSearchResultDto.setSqlTranno(sqlracdpf1rs.getInt(12));
                racdSearchResultDto.setSqlValidflag(sqlracdpf1rs.getString(13));
                racdSearchResultDto.setSqlCurrcode(sqlracdpf1rs.getString(14));
                racdSearchResultDto.setSqlCurrto(sqlracdpf1rs.getInt(15));
                racdSearchResultDto.setSqlRetype(sqlracdpf1rs.getString(16));
                racdSearchResultDto.setSqlRaamount(sqlracdpf1rs.getBigDecimal(17));
                racdSearchResultDto.setSqlCtdate(sqlracdpf1rs.getInt(18));
                racdSearchResultDto.setSqlCmdate(sqlracdpf1rs.getInt(19));
                racdSearchResultDto.setSqlReasper(sqlracdpf1rs.getBigDecimal(20));
                racdSearchResultDto.setSqlRecovamt(sqlracdpf1rs.getBigDecimal(21));
                racdSearchResultDto.setSqlCestype(sqlracdpf1rs.getString(22));
                racdSearchResultDto.setSqlLrkcls(sqlracdpf1rs.getString(23));
                racdSearchResultDto.setSqlRrevdt(sqlracdpf1rs.getInt(24));
                
                racdSearchResultDto.setSqlChTranno(sqlracdpf1rs.getInt(24));
                racdSearchResultDto.setSqlChCnttype(sqlracdpf1rs.getString(25));
                racdSearchResultDto.setSqlChStatcode(sqlracdpf1rs.getString(26));
                racdSearchResultDto.setSqlChPstatcode(sqlracdpf1rs.getString(27));
                racdSearchResultDto.setSqlChBillfreq(sqlracdpf1rs.getString(28));
                racdSearchResultDto.setSqlChCntcurr(sqlracdpf1rs.getString(29));
                racdSearchResultDto.setSqlChPolsum(sqlracdpf1rs.getInt(30));
                racdSearchResultDto.setSqlChBillchnl(sqlracdpf1rs.getString(31));
                racdSearchResultDto.setSqlChOccdate(sqlracdpf1rs.getInt(32));
                racdSearchResultDto.setSqlChPolinc(sqlracdpf1rs.getInt(33));
                racdSearchResultDto.setSqlChPtdate(sqlracdpf1rs.getInt(34));
                
                
                racdSearchResultDto.setSqlCoCrrcd(sqlracdpf1rs.getInt(34));
                racdSearchResultDto.setSqlCoCrtable(sqlracdpf1rs.getString(35));
                racdSearchResultDto.setSqlCoSumins(sqlracdpf1rs.getBigDecimal(36));
                racdSearchResultDto.setSqlCoJlife(sqlracdpf1rs.getString(37));
                racdSearchResultDto.setSqlCoInstprem(sqlracdpf1rs.getBigDecimal(38));
                racdSearchResultDto.setSqlCoChdrcoy(sqlracdpf1rs.getString(39));
                racdSearchResultDto.setSqlCoChdrnum(sqlracdpf1rs.getString(40));
                racdSearchResultDto.setSqlCoLife(sqlracdpf1rs.getString(41));
                racdSearchResultDto.setSqlCoCoverage(sqlracdpf1rs.getString(42));
                racdSearchResultDto.setSqlCoRider(sqlracdpf1rs.getString(43));
                racdSearchResultDto.setSqlJlife1(sqlracdpf1rs.getString(44));
                racdSearchResultDto.setSqlLifcnum1(sqlracdpf1rs.getString(45));
                racdSearchResultDto.setSqlJlife2(sqlracdpf1rs.getString(46));
                racdSearchResultDto.setSqlLifcnum2(sqlracdpf1rs.getString(47));
                
                racdSearchResultDto.setSqlRaUniqueNum(sqlracdpf1rs.getLong(49));
                
                racdSearchResultDtoList.add(racdSearchResultDto);
            }

        } catch (SQLException e) {
			LOGGER.error("findRacdResult()", e); /* IJTI-1479 */
        } finally {
            close(psRacdrskSelect, sqlracdpf1rs);
        }
        return racdSearchResultDtoList;
	}
	
	
	@Override
	public void deleteRacdRcds(List<Racdpf> list) {
		if (list == null || list.size() == 0) {
			return;
		}
		String stmt = "DELETE FROM RACDPF WHERE UNIQUE_NUMBER=?";
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
			for (Racdpf r : list) {
				ps.setLong(1, r.getUnique_number());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteRacdRcds()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	 public void insertRacdBulk(List<Racdpf> racdrskBulkOpList) {
        // racdrskIO bulk insert
        if (racdrskBulkOpList != null && racdrskBulkOpList.size() > 0) {
            
            StringBuilder sqlRacdrskInsert = new StringBuilder();
            sqlRacdrskInsert.append("INSERT INTO RACDPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,TRANNO,SEQNO,VALIDFLAG,CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,LRKCLS,JOBNM,USRPRF,DATIME,RRTFRM ,RRTDAT,PENDCSTTO,PRORATEFLAG )");
            sqlRacdrskInsert.append(" SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,?,SEQNO,?,CURRCODE,?,?,RETYPE,RNGMNT,RAAMOUNT,?,CMDATE,REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,LRKCLS,?,?,?,?,?,?,?");
            sqlRacdrskInsert.append(" FROM RACDPF WHERE UNIQUE_NUMBER=?");
            
            PreparedStatement psRacdrskInsert = getPrepareStatement(sqlRacdrskInsert.toString());
            try {
                for (Racdpf p : racdrskBulkOpList) {

                    psRacdrskInsert.setInt(1, p.getTranno());
                    psRacdrskInsert.setString(2, p.getValidflag());
                    psRacdrskInsert.setInt(3, (p.getCurrfrom()));
                    psRacdrskInsert.setInt(4, (p.getCurrto()));
                    psRacdrskInsert.setInt(5, (p.getCtdate()));
                    // JOBNM,USRPRF,DATIME
                    psRacdrskInsert.setString(6, getJobnm());
                    psRacdrskInsert.setString(7, getUsrprf());
                    psRacdrskInsert.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                	psRacdrskInsert.setLong(9, p.getRrtfrm());
                	psRacdrskInsert.setLong(10, p.getRrtdat());
                	psRacdrskInsert.setInt(11, p.getPendcstto());
                	psRacdrskInsert.setString(12, p.getProrateflag());
                    psRacdrskInsert.setLong(13, p.getUnique_number());
                    
                    psRacdrskInsert.addBatch();
                }
                psRacdrskInsert.executeBatch();
            } catch (SQLException e) {
				LOGGER.error("insertRacdBulk()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psRacdrskInsert, null);
            }
        }
    }

    public void updateRacdBulk(List<Racdpf> racdrskBulkOpList, int busDate) {
        // racdrskIO bulk update
        if (racdrskBulkOpList != null && racdrskBulkOpList.size() > 0) {
            String sqlRacdrskUpdate = "UPDATE RACDPF SET VALIDFLAG='2',CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=?";
            
            PreparedStatement psRacdrskUpdate = getPrepareStatement(sqlRacdrskUpdate);
            try {
                for (Racdpf p : racdrskBulkOpList) {
                    psRacdrskUpdate.setInt(1, busDate);
                    psRacdrskUpdate.setString(2, getJobnm());
                    psRacdrskUpdate.setString(3, getUsrprf());
                    psRacdrskUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                    psRacdrskUpdate.setLong(5, p.getUnique_number());
                    psRacdrskUpdate.addBatch();
                }
                psRacdrskUpdate.executeBatch();
            } catch (SQLException e) {
				LOGGER.error("updateRacdBulk()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psRacdrskUpdate, null);
            }
        }
    }
    @Override
    public List<Racdpf> searchRacdResult(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx) {
        
        StringBuilder sqlRacdrskSelect = new StringBuilder();
        sqlRacdrskSelect.append("SELECT CTDATE");
        sqlRacdrskSelect.append(" FROM RACDPF");
        sqlRacdrskSelect.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG='1'");
        sqlRacdrskSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO DESC, UNIQUE_NUMBER DESC ");
        
        PreparedStatement psRacdrskSelect = getPrepareStatement(sqlRacdrskSelect.toString());
        List<Racdpf> racdSearchResultDtoList = new LinkedList<Racdpf>();
        ResultSet sqlracdpf1rs = null;
        try {
            psRacdrskSelect.setString(1, coy);
            psRacdrskSelect.setString(2, chdrnum);
            psRacdrskSelect.setString(3, life);
            psRacdrskSelect.setString(4, coverage);
            psRacdrskSelect.setString(5, rider);
            psRacdrskSelect.setString(6, plnsfx);




            sqlracdpf1rs = executeQuery(psRacdrskSelect);

            while (sqlracdpf1rs.next()) {
                Racdpf racdpf = new Racdpf();
                racdpf.setCtdate(sqlracdpf1rs.getInt(1));
                racdSearchResultDtoList.add(racdpf);
            }


        } catch (SQLException e) {
			LOGGER.error("searchRacdResult()", e); /* IJTI-1479 */
        } finally {
            close(psRacdrskSelect, sqlracdpf1rs);
        }
        return racdSearchResultDtoList;
    }
    public List<Racdpf> readRacdRecord(Racdpf racdpf){
    	 StringBuilder sqlRacdrskSelect = new StringBuilder();
         sqlRacdrskSelect.append("SELECT CTDATE");
         sqlRacdrskSelect.append(" FROM RACDPF");
         sqlRacdrskSelect.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND SEQNO=? AND CESTYPE=?");
         sqlRacdrskSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO DESC, UNIQUE_NUMBER DESC ");
         
         PreparedStatement psRacdrskSelect = getPrepareStatement(sqlRacdrskSelect.toString());
         List<Racdpf> racdSearchResultDtoList = new LinkedList<Racdpf>();
         ResultSet sqlracdpf1rs = null;
         try {
             psRacdrskSelect.setString(1, racdpf.getChdrcoy());
             psRacdrskSelect.setString(2, racdpf.getChdrnum());
             psRacdrskSelect.setString(3, racdpf.getLife());
             psRacdrskSelect.setString(4, racdpf.getCoverage());
             psRacdrskSelect.setString(5, racdpf.getRider());
             psRacdrskSelect.setInt(6, racdpf.getPlanSuffix());
             psRacdrskSelect.setInt(7, racdpf.getSeqno());
             psRacdrskSelect.setString(8, racdpf.getCestype());

             sqlracdpf1rs = executeQuery(psRacdrskSelect);

             while (sqlracdpf1rs.next()) {
                 Racdpf racdpf1 = new Racdpf();
                 racdpf1.setCtdate(sqlracdpf1rs.getInt("CTDATE"));
                 racdSearchResultDtoList.add(racdpf1);
             }


         } catch (SQLException e) {
			LOGGER.error("readRacdRecord()", e); /* IJTI-1479 */
         } finally {
             close(psRacdrskSelect, sqlracdpf1rs);
         }
         return racdSearchResultDtoList;

    }    
    public List<Racdpf> searchRacdRecord(String coy, String chdrnum) {
		String sql = "SELECT UNIQUE_NUMBER,CESTYPE,LIFE,COVERAGE,RIDER,CHDRCOY,CHDRNUM,PLNSFX ,RRTDAT,RRTFRM FROM RACD WHERE CHDRCOY=? AND CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		List<Racdpf> searchResult = new LinkedList<Racdpf>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Racdpf t = new Racdpf();
				t.setUnique_number(rs.getLong(1));
				t.setCestype(rs.getString(2));
				t.setLife(rs.getString(3));
				t.setCoverage(rs.getString(4));
				t.setRider(rs.getString(5));
				t.setChdrcoy(rs.getString(6));
				t.setChdrnum(rs.getString(7));
				t.setPlanSuffix(rs.getInt(8));
				t.setRrtdat(rs.getInt(9));
				t.setRrtfrm(rs.getInt(10));
				searchResult.add(t);
			}

		} catch (SQLException e) {
			LOGGER.error("searchRacdRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return searchResult;
	}   
    
    public Map<String, List<Racdpf>> searchRacdlnbRecord(List<String> chdrnumList) {
        StringBuilder sqlRacdSelect1 = new StringBuilder("SELECT * FROM RACDLNB WHERE ");
        sqlRacdSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlRacdSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO ASC, CESTYPE ASC, UNIQUE_NUMBER ASC ");
        PreparedStatement psRacdSelect = getPrepareStatement(sqlRacdSelect1.toString());
        ResultSet sqlracdpf1rs = null;
        Map<String, List<Racdpf>> racdpfSearchResult = new HashMap<String, List<Racdpf>>();
        try {
            sqlracdpf1rs = executeQuery(psRacdSelect);
            while (sqlracdpf1rs.next()) {
                Racdpf racdpf = new Racdpf();
                racdpf.setUnique_number(sqlracdpf1rs.getLong("Unique_number"));
                racdpf.setChdrcoy(sqlracdpf1rs.getString("Chdrcoy"));
                racdpf.setChdrnum(sqlracdpf1rs.getString("Chdrnum"));
                racdpf.setLife(sqlracdpf1rs.getString("Life"));
                racdpf.setCoverage(sqlracdpf1rs.getString("Coverage"));
                racdpf.setRider(sqlracdpf1rs.getString("Rider"));
                racdpf.setPlanSuffix(sqlracdpf1rs.getInt("Plnsfx"));
                racdpf.setRasnum(sqlracdpf1rs.getString("Rasnum"));
                racdpf.setTranno(sqlracdpf1rs.getInt("Tranno"));
                racdpf.setSeqno(sqlracdpf1rs.getInt("Seqno"));
                racdpf.setValidflag(sqlracdpf1rs.getString("Validflag"));
                racdpf.setCurrcode(sqlracdpf1rs.getString("Currcode"));
                racdpf.setCurrfrom(sqlracdpf1rs.getInt("Currfrom"));
                racdpf.setCurrto(sqlracdpf1rs.getInt("Currto"));
                racdpf.setRetype(sqlracdpf1rs.getString("Retype"));
                racdpf.setRngmnt(sqlracdpf1rs.getString("Rngmnt"));
                racdpf.setRaAmount(sqlracdpf1rs.getBigDecimal("Raamount"));
                racdpf.setCtdate(sqlracdpf1rs.getInt("Ctdate"));
                racdpf.setCmdate(sqlracdpf1rs.getInt("Cmdate"));
                racdpf.setReasper(sqlracdpf1rs.getBigDecimal("Reasper"));
                racdpf.setRrevdt(sqlracdpf1rs.getInt("Rrevdt"));
                racdpf.setRecovamt(sqlracdpf1rs.getBigDecimal("Recovamt"));
                racdpf.setCestype(sqlracdpf1rs.getString("Cestype"));
                racdpf.setOvrdind(sqlracdpf1rs.getString("Ovrdind"));
                racdpf.setLrkcls(sqlracdpf1rs.getString("Lrkcls"));
                racdpf.setRrtdat(sqlracdpf1rs.getInt("RRTDAT"));
                racdpf.setRrtfrm(sqlracdpf1rs.getInt("RRTFRM"));

                if (racdpfSearchResult.containsKey(racdpf.getChdrnum())) {
                    racdpfSearchResult.get(racdpf.getChdrnum()).add(racdpf);
                } else {
                    List<Racdpf> racdpfList = new ArrayList<Racdpf>();
                    racdpfList.add(racdpf);
                    racdpfSearchResult.put(racdpf.getChdrnum(), racdpfList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("searchRacdlnbRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psRacdSelect, sqlracdpf1rs);
        }
        return racdpfSearchResult;

    }
 public List<Racdpf> searchRacdmjaResult(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx) {
        
        StringBuilder sqlRacdrskSelect = new StringBuilder();
        sqlRacdrskSelect.append("SELECT CTDATE");
        sqlRacdrskSelect.append(" FROM RACDMJA");
        sqlRacdrskSelect.append(" WHERE CHDRCOY = ? AND CHDRNUM= ? and LIFE=? and COVERAGE=?  AND PLNSFX=? ");
        sqlRacdrskSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO DESC, UNIQUE_NUMBER DESC ");
        
        PreparedStatement psRacdrskSelect = getPrepareStatement(sqlRacdrskSelect.toString());
        List<Racdpf> racdSearchResultDtoList = new LinkedList<Racdpf>();
        ResultSet sqlracdpf1rs = null;
        try {
          /*  psRacdrskSelect.setString(1, coy);
            psRacdrskSelect.setString(2, chdrnum);
            psRacdrskSelect.setString(3, life);
            psRacdrskSelect.setString(4, coverage);*/
        	psRacdrskSelect.setString(1, StringUtil.fillSpace(coy.trim(), DD.coy.length)); 
        	psRacdrskSelect.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length)); 
        	psRacdrskSelect.setString(3, StringUtil.fillSpace(life.trim(), DD.life.length)); 
        	psRacdrskSelect.setString(4, StringUtil.fillSpace(coverage.trim(), DD.coverage.length)); 
            psRacdrskSelect.setString(5, rider);
            psRacdrskSelect.setString(6, plnsfx);
            sqlracdpf1rs = executeQuery(psRacdrskSelect);
            while (sqlracdpf1rs.next()) {
                Racdpf racdpf = new Racdpf();
                racdpf.setCtdate(sqlracdpf1rs.getInt(1));
                racdSearchResultDtoList.add(racdpf);
            }
        } catch (SQLException e) {
			LOGGER.error("searchRacdResult()", e); /* IJTI-1479 */
        } finally {
            close(psRacdrskSelect, sqlracdpf1rs);
        }
        return racdSearchResultDtoList;
    }

    public List<Racdpf> searchRacdstRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
            int plnsfx, String validflag) {
        StringBuilder sqlRacdSelect1 = new StringBuilder(
                "SELECT * FROM RACDPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX=? AND VALIDFLAG =? ");
        sqlRacdSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, RASNUM ASC, TRANNO DESC,  UNIQUE_NUMBER DESC ");

        PreparedStatement psRacdSelect = getPrepareStatement(sqlRacdSelect1.toString());
        ResultSet sqlracdpf1rs = null;
        List<Racdpf> racdpfSearchResult = new ArrayList<Racdpf>();
        try {
            psRacdSelect.setString(1, chdrcoy);
            psRacdSelect.setString(2, chdrnum);
            psRacdSelect.setString(3, life);
            psRacdSelect.setString(4, coverage);
            psRacdSelect.setString(5, rider);
            psRacdSelect.setInt(6, plnsfx);
            psRacdSelect.setString(7, validflag);
            
            sqlracdpf1rs = executeQuery(psRacdSelect);
            while (sqlracdpf1rs.next()) {
                Racdpf racdpf = new Racdpf();
                racdpf.setUnique_number(sqlracdpf1rs.getLong("Unique_number"));
                racdpf.setChdrcoy(sqlracdpf1rs.getString("Chdrcoy"));
                racdpf.setChdrnum(sqlracdpf1rs.getString("Chdrnum"));
                racdpf.setLife(sqlracdpf1rs.getString("Life"));
                racdpf.setCoverage(sqlracdpf1rs.getString("Coverage"));
                racdpf.setRider(sqlracdpf1rs.getString("Rider"));
                racdpf.setPlanSuffix(sqlracdpf1rs.getInt("Plnsfx"));
                racdpf.setRasnum(sqlracdpf1rs.getString("Rasnum"));
                racdpf.setTranno(sqlracdpf1rs.getInt("Tranno"));
                racdpf.setSeqno(sqlracdpf1rs.getInt("Seqno"));
                racdpf.setValidflag(sqlracdpf1rs.getString("Validflag"));
                racdpf.setCurrcode(sqlracdpf1rs.getString("Currcode"));
                racdpf.setCurrfrom(sqlracdpf1rs.getInt("Currfrom"));
                racdpf.setCurrto(sqlracdpf1rs.getInt("Currto"));
                racdpf.setRetype(sqlracdpf1rs.getString("Retype"));
                racdpf.setRngmnt(sqlracdpf1rs.getString("Rngmnt"));
                racdpf.setRaAmount(sqlracdpf1rs.getBigDecimal("Raamount"));
                racdpf.setCtdate(sqlracdpf1rs.getInt("Ctdate"));
                racdpf.setCmdate(sqlracdpf1rs.getInt("Cmdate"));
                racdpf.setReasper(sqlracdpf1rs.getBigDecimal("Reasper"));
                racdpf.setRrevdt(sqlracdpf1rs.getInt("Rrevdt"));
                racdpf.setRecovamt(sqlracdpf1rs.getBigDecimal("Recovamt"));
                racdpf.setCestype(sqlracdpf1rs.getString("Cestype"));
                racdpf.setOvrdind(sqlracdpf1rs.getString("Ovrdind"));
                racdpf.setLrkcls(sqlracdpf1rs.getString("Lrkcls"));
                racdpf.setRrtdat(sqlracdpf1rs.getInt("Rrtdat"));
                racdpf.setRrtfrm(sqlracdpf1rs.getInt("Rrtfrm"));

                racdpfSearchResult.add(racdpf);
            }

        } catch (SQLException e) {
			LOGGER.error("searchRacdstRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psRacdSelect, sqlracdpf1rs);
        }
        return racdpfSearchResult;

    }

	@Override
	public Racdpf getRacdbySeqNo(String chdrcoy, String chdrnum, int seqNo) {
		StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,");
		sql.append("TRANNO,SEQNO,VALIDFLAG,CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,REASPER,RREVDT,");
		sql.append("RECOVAMT,CESTYPE,OVRDIND,LRKCLS, RRTDAT,RRTFRM FROM RACDPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND SEQNO = ? AND VALIDFLAG='1'");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		Racdpf racdpf = null;
        ResultSet rs = null;
        try {
        	ps.setString(1, chdrcoy);
        	ps.setString(2, chdrnum);
            ps.setInt(3, seqNo);
            
            rs = executeQuery(ps);
            if(rs.next()) {
            	racdpf = new Racdpf();
            	racdpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
                racdpf.setChdrcoy(rs.getString("CHDRCOY"));
                racdpf.setChdrnum(rs.getString("CHDRNUM"));
                racdpf.setLife(rs.getString("LIFE"));
                racdpf.setCoverage(rs.getString("COVERAGE"));
                racdpf.setRider(rs.getString("RIDER"));
                racdpf.setPlanSuffix(rs.getInt("PLNSFX"));
                racdpf.setRasnum(rs.getString("RASNUM"));
                racdpf.setTranno(rs.getInt("TRANNO"));
                racdpf.setSeqno(rs.getInt("SEQNO"));
                racdpf.setValidflag(rs.getString("VALIDFLAG"));
                racdpf.setCurrcode(rs.getString("CURRCODE"));
                racdpf.setCurrfrom(rs.getInt("CURRFROM"));
                racdpf.setCurrto(rs.getInt("CURRTO"));
                racdpf.setRetype(rs.getString("RETYPE"));
                racdpf.setRngmnt(rs.getString("RNGMNT"));
                racdpf.setRaAmount(rs.getBigDecimal("RAAMOUNT"));
                racdpf.setCtdate(rs.getInt("CTDATE"));
                racdpf.setCmdate(rs.getInt("CMDATE"));
                racdpf.setReasper(rs.getBigDecimal("REASPER"));
                racdpf.setRrevdt(rs.getInt("RREVDT"));
                racdpf.setRecovamt(rs.getBigDecimal("RECOVAMT"));
                racdpf.setCestype(rs.getString("CESTYPE"));
                racdpf.setOvrdind(rs.getString("OVRDIND"));
                racdpf.setLrkcls(rs.getString("LRKCLS"));
                racdpf.setRrtdat(rs.getInt("RRTDAT"));
                racdpf.setRrtfrm(rs.getInt("RRTFRM"));
            }
        } catch (SQLException e) {
			LOGGER.error("getRacdbySeqNo()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return racdpf;
	}
	
	@Override
	public Racdpf getRacdbySeqNo(String chdrcoy, String chdrnum, int seqNo, String life, String coverage, String rider, int plnsfx) {
		StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,");
		sql.append("TRANNO,SEQNO,VALIDFLAG,CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,");
		sql.append("LRKCLS,RRTDAT,RRTFRM FROM RACDPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND SEQNO = ? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG='1'");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		Racdpf racdpf = null;
        ResultSet rs = null;
        try {
        	ps.setString(1, chdrcoy);
        	ps.setString(2, chdrnum);
            ps.setInt(3, seqNo);
            ps.setString(4, life);
            ps.setString(5, coverage);
            ps.setString(6, rider);
            ps.setInt(7, plnsfx);
            
            rs = executeQuery(ps);
            
            if(rs.next()) {
            	racdpf = new Racdpf();
            	racdpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
                racdpf.setChdrcoy(rs.getString("CHDRCOY"));
                racdpf.setChdrnum(rs.getString("CHDRNUM"));
                racdpf.setLife(rs.getString("LIFE"));
                racdpf.setCoverage(rs.getString("COVERAGE"));
                racdpf.setRider(rs.getString("RIDER"));
                racdpf.setPlanSuffix(rs.getInt("PLNSFX"));
                racdpf.setRasnum(rs.getString("RASNUM"));
                racdpf.setTranno(rs.getInt("TRANNO"));
                racdpf.setSeqno(rs.getInt("SEQNO"));
                racdpf.setValidflag(rs.getString("VALIDFLAG"));
                racdpf.setCurrcode(rs.getString("CURRCODE"));
                racdpf.setCurrfrom(rs.getInt("CURRFROM"));
                racdpf.setCurrto(rs.getInt("CURRTO"));
                racdpf.setRetype(rs.getString("RETYPE"));
                racdpf.setRngmnt(rs.getString("RNGMNT"));
                racdpf.setRaAmount(rs.getBigDecimal("RAAMOUNT"));
                racdpf.setCtdate(rs.getInt("CTDATE"));
                racdpf.setCmdate(rs.getInt("CMDATE"));
                racdpf.setReasper(rs.getBigDecimal("REASPER"));
                racdpf.setRrevdt(rs.getInt("RREVDT"));
                racdpf.setRecovamt(rs.getBigDecimal("RECOVAMT"));
                racdpf.setCestype(rs.getString("CESTYPE"));
                racdpf.setOvrdind(rs.getString("OVRDIND"));
                racdpf.setLrkcls(rs.getString("LRKCLS"));
                racdpf.setRrtdat(rs.getInt("RRTDAT"));
                racdpf.setRrtfrm(rs.getInt("RRTFRM"));
            }
        } catch (SQLException e) {
			LOGGER.error("getRacdbySeqNo()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return racdpf;
	}

	@Override
	public void insertBulkRacd(List<Racdpf> racdList) {
		StringBuilder sqlRacdInsert = new StringBuilder();
		sqlRacdInsert.append("INSERT INTO RACDPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,TRANNO,SEQNO,VALIDFLAG,CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,LRKCLS,JOBNM,USRPRF,DATIME,RRTDAT,RRTFRM,PENDCSTTO,PRORATEFLAG) ");
		sqlRacdInsert.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
        
        PreparedStatement psRacdInsert = getPrepareStatement(sqlRacdInsert.toString());
        try {
            for (Racdpf racd : racdList) {
            	psRacdInsert.setString(1, racd.getChdrcoy());
            	psRacdInsert.setString(2, racd.getChdrnum());
            	psRacdInsert.setString(3, racd.getLife());
            	psRacdInsert.setString(4, racd.getCoverage());
            	psRacdInsert.setString(5, racd.getRider());
            	psRacdInsert.setInt(6, racd.getPlanSuffix());
            	psRacdInsert.setString(7, racd.getRasnum());
            	psRacdInsert.setInt(8, racd.getTranno());
            	psRacdInsert.setInt(9, racd.getSeqno());
            	psRacdInsert.setString(10, racd.getValidflag());
            	psRacdInsert.setString(11, racd.getCurrcode());
            	psRacdInsert.setInt(12, racd.getCurrfrom());
            	psRacdInsert.setInt(13, racd.getCurrto());
            	psRacdInsert.setString(14, racd.getRetype());
            	psRacdInsert.setString(15, racd.getRngmnt());
            	psRacdInsert.setBigDecimal(16, racd.getRaAmount());
            	psRacdInsert.setInt(17, racd.getCtdate());
            	psRacdInsert.setInt(18, racd.getCmdate());
            	psRacdInsert.setBigDecimal(19, racd.getReasper());
            	psRacdInsert.setInt(20, racd.getRrevdt());
            	psRacdInsert.setBigDecimal(21, racd.getRecovamt());
            	psRacdInsert.setString(22, racd.getCestype());
            	psRacdInsert.setString(23, racd.getOvrdind());
            	psRacdInsert.setString(24, racd.getLrkcls());
            	psRacdInsert.setString(25, getJobnm());
            	psRacdInsert.setString(26, getUsrprf());
            	psRacdInsert.setTimestamp(27, new Timestamp(System.currentTimeMillis()));
            	if (racd.getRrtdat() != 0) {
            		psRacdInsert.setInt(28,racd.getRrtdat());
				} else {
					psRacdInsert.setInt(28, java.sql.Types.INTEGER);
				}
            	
            	if (racd.getRrtdat() != 0) {
            		psRacdInsert.setInt(29,racd.getRrtfrm());
				} else {
					psRacdInsert.setInt(29, java.sql.Types.INTEGER);
				}
            	psRacdInsert.setInt(30,racd.getPendcstto());
            	psRacdInsert.setString(31, racd.getProrateflag()!=null?racd.getProrateflag():"");
                psRacdInsert.addBatch();
            }
            psRacdInsert.executeBatch();
        } catch (SQLException e) {
			LOGGER.error("insertBulkRacd()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psRacdInsert, null);
        }
	}

	@Override
	public List<Racdpf> getRacdRecord(Racdpf racd) {
		StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,TRANNO,SEQNO,VALIDFLAG,");
		sql.append("CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,LRKCLS ,RRTDAT,RRTFRM FROM RACDPF ");
		sql.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND RETYPE=? AND RNGMNT=? ");
		sql.append("AND RASNUM=? AND VALIDFLAG='1' ORDER BY SEQNO DESC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		List<Racdpf> racdpfList = new LinkedList<>();
		ResultSet rs = null;
		try {
			ps.setString(1, racd.getChdrcoy());
			ps.setString(2, racd.getChdrnum());
			ps.setString(3, racd.getLife());
			ps.setString(4, racd.getCoverage());
			ps.setString(5, racd.getRider());
			ps.setInt(6, racd.getPlanSuffix());
			ps.setString(7, racd.getRetype());
			ps.setString(8, racd.getRngmnt());
			ps.setString(9, racd.getRasnum());
			rs = executeQuery(ps);
			while(rs.next()) {
				Racdpf racdpf = new Racdpf();
				racdpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				racdpf.setChdrcoy(rs.getString("CHDRCOY"));
				racdpf.setChdrnum(rs.getString("CHDRNUM"));
				racdpf.setLife(rs.getString("LIFE"));
				racdpf.setCoverage(rs.getString("COVERAGE"));
				racdpf.setRider(rs.getString("RIDER"));
				racdpf.setPlanSuffix(rs.getInt("PLNSFX"));
				racdpf.setRasnum(rs.getString("RASNUM"));
				racdpf.setTranno(rs.getInt("TRANNO"));
				racdpf.setSeqno(rs.getInt("SEQNO"));
				racdpf.setValidflag(rs.getString("VALIDFLAG"));
				racdpf.setCurrcode(rs.getString("CURRCODE"));
				racdpf.setCurrfrom(rs.getInt("CURRFROM"));
				racdpf.setCurrto(rs.getInt("CURRTO"));
				racdpf.setRetype(rs.getString("RETYPE"));
				racdpf.setRngmnt(rs.getString("RNGMNT"));
				racdpf.setRaAmount(rs.getBigDecimal("RAAMOUNT"));
				racdpf.setCtdate(rs.getInt("CTDATE"));
				racdpf.setCmdate(rs.getInt("CMDATE"));
				racdpf.setReasper(rs.getBigDecimal("REASPER"));
				racdpf.setRrevdt(rs.getInt("RREVDT"));
				racdpf.setRecovamt(rs.getBigDecimal("RECOVAMT"));
				racdpf.setCestype(rs.getString("CESTYPE"));
				racdpf.setOvrdind(rs.getString("OVRDIND"));
				racdpf.setLrkcls(rs.getString("LRKCLS"));
				racdpf.setRrtdat(rs.getInt("RRTDAT"));
		        racdpf.setRrtfrm(rs.getInt("RRTFRM"));
				racdpfList.add(racdpf);
			}
		} catch (SQLException e) {
			LOGGER.error("getRacdRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return racdpfList;
	}

	@Override
	public int getRacdCntByType(Racdpf racdpf) {
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM RACDPF ");
		sql.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG='1' ");
		if(racdpf.getRetype()!=null) {
			sql.append("AND RETYPE=? ");
		}
		PreparedStatement ps = getPrepareStatement(sql.toString());
		int count=0;
		ResultSet rs = null;
		try {
			ps.setString(1, racdpf.getChdrcoy());
			ps.setString(2, racdpf.getChdrnum());
			ps.setString(3, racdpf.getLife());
			ps.setString(4, racdpf.getCoverage());
			ps.setString(5, racdpf.getRider());
			ps.setInt(6, racdpf.getPlanSuffix());
			if(racdpf.getRetype()!=null) {
				ps.setString(7, racdpf.getRetype());
			}
			rs = executeQuery(ps);
			if(rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error("getFaculRacdCnt()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return count;
	}

	@Override
	public List<Racdpf> getRacdFaclRecord(Racdpf racd) {
		StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,RASNUM,TRANNO,SEQNO,VALIDFLAG,");
		sql.append("CURRCODE,CURRFROM,CURRTO,RETYPE,RNGMNT,RAAMOUNT,CTDATE,CMDATE,REASPER,RREVDT,RECOVAMT,CESTYPE,OVRDIND,LRKCLS,RRTDAT,RRTFRM  FROM RACDPF ");
		sql.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND RETYPE=? AND VALIDFLAG='1' ORDER BY SEQNO ASC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		List<Racdpf> racdList = new LinkedList<Racdpf>();
		ResultSet rs = null;
		try {
			ps.setString(1, racd.getChdrcoy());
			ps.setString(2, racd.getChdrnum());
			ps.setString(3, racd.getLife());
			ps.setString(4, racd.getCoverage());
			ps.setString(5, racd.getRider());
			ps.setInt(6, racd.getPlanSuffix());
			ps.setString(7, racd.getRetype());
			rs = executeQuery(ps);
			while(rs.next()) {
				Racdpf racdpf = new Racdpf();
				racdpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				racdpf.setChdrcoy(rs.getString("CHDRCOY"));
				racdpf.setChdrnum(rs.getString("CHDRNUM"));
				racdpf.setLife(rs.getString("LIFE"));
				racdpf.setCoverage(rs.getString("COVERAGE"));
				racdpf.setRider(rs.getString("RIDER"));
				racdpf.setPlanSuffix(rs.getInt("PLNSFX"));
				racdpf.setRasnum(rs.getString("RASNUM"));
				racdpf.setTranno(rs.getInt("TRANNO"));
				racdpf.setSeqno(rs.getInt("SEQNO"));
				racdpf.setValidflag(rs.getString("VALIDFLAG"));
				racdpf.setCurrcode(rs.getString("CURRCODE"));
				racdpf.setCurrfrom(rs.getInt("CURRFROM"));
				racdpf.setCurrto(rs.getInt("CURRTO"));
				racdpf.setRetype(rs.getString("RETYPE"));
				racdpf.setRngmnt(rs.getString("RNGMNT"));
				racdpf.setRaAmount(rs.getBigDecimal("RAAMOUNT"));
				racdpf.setCtdate(rs.getInt("CTDATE"));
				racdpf.setCmdate(rs.getInt("CMDATE"));
				racdpf.setReasper(rs.getBigDecimal("REASPER"));
				racdpf.setRrevdt(rs.getInt("RREVDT"));
				racdpf.setRecovamt(rs.getBigDecimal("RECOVAMT"));
				racdpf.setCestype(rs.getString("CESTYPE"));
				racdpf.setOvrdind(rs.getString("OVRDIND"));
				racdpf.setLrkcls(rs.getString("LRKCLS"));
				racdpf.setRrtdat(rs.getInt("RRTDAT"));
		        racdpf.setRrtfrm(rs.getInt("RRTFRM"));
				racdList.add(racdpf);
			}
		} catch (SQLException e) {
			LOGGER.error("getRacdFaclRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return racdList;
	}
	@Override
	public int updateRacd(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx,int rrtdat,int rrtfrm,String validflag) {
		// TODO Auto-generated method stub
		
		int result = 0;
	            String sqlRacdrskUpdate = "UPDATE RACDPF SET  RRTFRM= RRTDAT, RRTDAT= ?, USRPRF= ?, DATIME= ? WHERE CHDRCOY=? and CHDRNUM=? and  LIFE=? and COVERAGE=? and RIDER=? and PLNSFX=? and validflag =? ";
	            
	           
	            PreparedStatement psRacdrskUpdate = getPrepareStatement(sqlRacdrskUpdate.toString());
	         
	            try {
	                
	            	    psRacdrskUpdate.setInt(1, rrtdat);
	                    psRacdrskUpdate.setString(2, getUsrprf());
	                    psRacdrskUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
	                    psRacdrskUpdate.setString(4, coy);
	                    psRacdrskUpdate.setString(5, chdrnum);
	                    psRacdrskUpdate.setString(6,life);
	                    psRacdrskUpdate.setString(7, coverage);
	                    psRacdrskUpdate.setString(8, rider);
	                    psRacdrskUpdate.setString(9, plnsfx);
	                   	psRacdrskUpdate.setString(10, validflag);
	                    result = psRacdrskUpdate.executeUpdate();
	                
	                psRacdrskUpdate.executeBatch();
	            } catch (SQLException e) {
					LOGGER.error("updateRacd()", e); 
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psRacdrskUpdate, null);
	            }
	        
		return result;
	}
	
	public void updateRacdPendcsttoProrateflagBulk(List<Racdpf> racdrskBulkOpList) {
        if (racdrskBulkOpList != null && !racdrskBulkOpList.isEmpty()) {
            String sqlRacdrskUpdate = "UPDATE RACDPF SET TRANNO = ?, PENDCSTTO = ?, PRORATEFLAG = ?, JOBNM = ?, USRPRF = ?, DATIME = ? WHERE UNIQUE_NUMBER = ? ";
            
            PreparedStatement psRacdrskUpdate = getPrepareStatement(sqlRacdrskUpdate);
            try {
                for (Racdpf r : racdrskBulkOpList) {
                    psRacdrskUpdate.setInt(1, r.getTranno());
                    psRacdrskUpdate.setInt(2, r.getPendcstto());
                    psRacdrskUpdate.setString(3, r.getProrateflag());
                    psRacdrskUpdate.setString(4, getJobnm());
                    psRacdrskUpdate.setString(5, getUsrprf());
                    psRacdrskUpdate.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                    psRacdrskUpdate.setLong(7, r.getUnique_number());
                    psRacdrskUpdate.addBatch();
                }
                psRacdrskUpdate.executeBatch();
            } catch (SQLException e) {
				LOGGER.error("updateRacdPendcsttoProrateflagBulk()", e);
                throw new SQLRuntimeException(e);
            } finally {
                close(psRacdrskUpdate, null);
            }
        }
    }
}
