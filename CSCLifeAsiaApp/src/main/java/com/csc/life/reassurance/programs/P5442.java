/*
 * File: P5442.java
 * Date: 30 August 2009 0:26:41
 * Author: Quipoz Limited
 * 
 * Class transformed from P5442.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.screens.S5442ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Cession Maintenance Subfile Screen.
*
* This program display inforce RACD records of the contract
* number selected in S5474. User can create, modify, enquire,
* and delete cessions by calling P5444.
*
* This Screen uses the work with approach to display Reassurance
* (RACDMJA) details. The contract number from the Sub-Menu
* appears at the top of the Screen and the associated Lives,
* components and RACDMJA Details are displayed. These will be
* interspersed   between  the  components  so  that  they  are
* visually  associated  with  the  components  to  which  they
* relate.  The  usual  indentation  method will be employed by
* the  subfile  portion  of  the  screen  to   represent   the
* structure  of  the  contract  and the interdependance of the
* coverages and riders within the lives assured.
*
* The user will control the work to be carried out by  placing
* certain  actions  against  the  components  or  the existing
* payment details.
*
* The Program uses OPTION Switching to switch from this to the
* next transaction screen.
*
* Although part of Major Alterations, this Program uses the
* COVRLNB Logical. This is because both the COVR and the
* COVRMJA Logicals originated in the days where the Relative
* Record number appeared in the Copybook primarily for show, as
* a PIC X(04) field. As this Program does a READD, it needs to
* use a Logical which has the Relative Record Number set up
* properly, as a PIC 9(09) COMP-4 field.
*
****************************************************************
* </pre>
*/
public class P5442 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5442");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e005 = "E005";
		/* TABLES */
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private String cltsrec = "CLTSREC";
	private String covrlnbrec = "COVRLNBREC";
	private String liferec = "LIFEREC";
	private String racdmjarec = "RACDMJAREC";
	//ILIFE-1539 STARTS
	private FixedLengthStringData wsaaReassuranceDetails = new FixedLengthStringData(46);
	//ILIFE-1539 ENDS
	private FixedLengthStringData wsaaCtdate = new FixedLengthStringData(10).isAPartOf(wsaaReassuranceDetails, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaReassuranceDetails, 10, FILLER).init(SPACES);
	private FixedLengthStringData wsaaRetype = new FixedLengthStringData(11).isAPartOf(wsaaReassuranceDetails, 11);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaReassuranceDetails, 22, FILLER).init(SPACES);
	private FixedLengthStringData wsaaRngmnt = new FixedLengthStringData(4).isAPartOf(wsaaReassuranceDetails, 23);
	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaReassuranceDetails, 27, FILLER).init(SPACES);
	private ZonedDecimalData wsaaRaAmount = new ZonedDecimalData(13, 2).isAPartOf(wsaaReassuranceDetails, 29).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 17);
	private FixedLengthStringData filler3 = new FixedLengthStringData(178).isAPartOf(wsaaTransactionRec, 22, FILLER).init(SPACES);

	private FixedLengthStringData wsaaAtSubmitFlag = new FixedLengthStringData(1).init("N");
	private Validator atSubmissionReqd = new Validator(wsaaAtSubmitFlag, "Y");

		/* WSAA-MESSAGE-AREA */
	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("AT request submitted for ");
	private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private String wsaaFirstTime = "Y";
	private String wsaaEnquireOnly = "Y";
	private FixedLengthStringData wsaaAction = new FixedLengthStringData(1).init(SPACES);
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaFirstLifeRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstCovrRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstRacdRead = new FixedLengthStringData(1);
	private String wsaaFirstRecord = "";

	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).isAPartOf(wsaaContractStatuzCheck, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Reassurance Cession Details Alterations*/
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatchkey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5442ScreenVars sv = ScreenProgram.getScreenVars( S5442ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1190, 
		exit1290, 
		exit1390, 
		preExit, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		updateErrorIndicators2170, 
		exit3090, 
		optswch4080, 
		exit4090
	}

	public P5442() {
		super();
		screenVars = sv;
		new ScreenModel("S5442", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsaaAction.set(SPACES);
			goTo(GotoLabel.exit1090);
		}
		wsaaAction.set(SPACES);
		scrnparams.subfileRrn.set(1);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.subfileMore.set("N");
		scrnparams.function.set(varcom.sclr);
		processScreen("S5442", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		sv.chdrsel.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		wsspcomn.currfrom.set(chdrmjaIO.getOccdate());
		wsaaReassuranceDetails.set(SPACES);
		wsaaFirstLifeRead.set(SPACES);
		wsaaFirstCovrRead.set(SPACES);
		wsaaFirstRacdRead.set(SPACES);
		wsaaEffdate.set(ZERO);
		wsaaEffdate.set(wssplife.effdate);
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(wsspcomn.company);
		lifeIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeIO.setCurrfrom(ZERO);
		lifeIO.setFormat(liferec);
		wsaaFirstLifeRead.set("Y");
		while ( !(isEQ(lifeIO.getStatuz(),varcom.endp))) {
			getPolicyDetails1100();
		}
		
	}

protected void getPolicyDetails1100()
	{
		try {
			getClientNumber1110();
			getClientName1120();
			getCompPayDetails1130();
		}
		catch (GOTOException e){
		}
	}

protected void getClientNumber1110()
	{
		if (isEQ(wsaaFirstLifeRead,"Y")) {
			lifeIO.setFunction(varcom.begn);
			wsaaFirstLifeRead.set("N");
		}
		else {
			lifeIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(),varcom.oK))
		&& (isNE(lifeIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if ((isEQ(lifeIO.getStatuz(),varcom.endp))
		|| (isNE(lifeIO.getChdrcoy(),wsspcomn.company))
		|| (isNE(lifeIO.getChdrnum(),chdrmjaIO.getChdrnum()))
		|| (isNE(lifeIO.getJlife(),"00"))
		|| (isNE(lifeIO.getValidflag(),"1"))) {
			lifeIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
	}

protected void getClientName1120()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifeIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.subfileArea.set(SPACES);
		sv.life.set(lifeIO.getLife());
		sv.shortdesc.set(lifeIO.getLifcnum());
		sv.elemdesc.set(wsspcomn.longconfname);
		sv.actionOut[varcom.pr.toInt()].set("Y");
		sv.actionOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.hrrn01.set(ZERO);
		sv.hrrn02.set(ZERO);
		sv.hrrn03.set(ZERO);
		if(isEQ(sv.bascovr,SPACES))
			sv.bascovr.set(3);
		addToSubfile1400();
	}

protected void getCompPayDetails1130()
	{
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(lifeIO.getChdrcoy());
		covrlnbIO.setChdrnum(lifeIO.getChdrnum());
		covrlnbIO.setLife(lifeIO.getLife());
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFormat(covrlnbrec);
		wsaaFirstCovrRead.set("Y");
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			getComponentDetails1200();
		}
		
	}

protected void getComponentDetails1200()
	{
		try {
			getCovrDetails1210();
		}
		catch (GOTOException e){
		}
	}

protected void getCovrDetails1210()
	{
		if (isEQ(wsaaFirstCovrRead,"Y")) {
			covrlnbIO.setFunction(varcom.begn);
			wsaaFirstCovrRead.set("N");
		}
		else {
			covrlnbIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(),varcom.oK))
		&& (isNE(covrlnbIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if ((isEQ(covrlnbIO.getStatuz(),varcom.endp))
		|| (isNE(covrlnbIO.getChdrcoy(),lifeIO.getChdrcoy()))
		|| (isNE(covrlnbIO.getChdrnum(),lifeIO.getChdrnum()))
		|| (isNE(covrlnbIO.getLife(),lifeIO.getLife()))) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.exit1290);
		}
		if ((isEQ(covrlnbIO.getLife(),sv.life))
		&& (isEQ(covrlnbIO.getCoverage(),sv.coverage))
		&& (isEQ(covrlnbIO.getRider(),sv.rider))) {
			goTo(GotoLabel.exit1290);
		}
		else {
			sv.subfileArea.set(SPACES);
			sv.life.set(covrlnbIO.getLife());
			sv.rider.set(covrlnbIO.getRider());
			sv.coverage.set(covrlnbIO.getCoverage());
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			if ((isEQ(covrlnbIO.getRider(),ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
			}
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(t5687);
			descIO.setDescitem(covrlnbIO.getCrtable());
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if ((isNE(descIO.getStatuz(),varcom.oK))
			&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				sv.elemdesc.set(SPACES);
			}
			else {
				sv.elemdesc.set(descIO.getLongdesc());
			}
			sv.shortdesc.set(covrlnbIO.getCrtable());
			sv.hcrtable.set(covrlnbIO.getCrtable());
			sv.hrrn01.set(ZERO);
			sv.hrrn02.set(ZERO);
			sv.hrrn03.set(covrlnbIO.getRrn());
		    if ((isEQ(covrlnbIO.getRider(),ZERO))) {
		    	sv.bascovr.set(1);
		    }
		    else
		       sv.bascovr.set(2);
			addToSubfile1400();
		}
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setChdrcoy(covrlnbIO.getChdrcoy());
		racdmjaIO.setChdrnum(covrlnbIO.getChdrnum());
		racdmjaIO.setLife(covrlnbIO.getLife());
		racdmjaIO.setCoverage(covrlnbIO.getCoverage());
		racdmjaIO.setRider(covrlnbIO.getRider());
		racdmjaIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(racdmjarec);
		wsaaFirstRacdRead.set("Y");
		sv.hrrn02.set(covrlnbIO.getRrn());
		sv.hrrn03.set(covrlnbIO.getRrn());
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			getReassuranceDetails1300();
		}
		
	}

protected void getReassuranceDetails1300()
	{
		try {
			readRacdmjaDetails1310();
		}
		catch (GOTOException e){
		}
	}

protected void readRacdmjaDetails1310()
	{
		if (isEQ(wsaaFirstRacdRead,"Y")) {
			racdmjaIO.setFunction(varcom.begn);
			wsaaFirstRacdRead.set("N");
		}
		else {
			racdmjaIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, racdmjaIO);
		if ((isNE(racdmjaIO.getStatuz(),varcom.oK))
		&& (isNE(racdmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(racdmjaIO.getParams());
			fatalError600();
		}
		if ((isEQ(racdmjaIO.getStatuz(),varcom.endp))
		|| (isNE(racdmjaIO.getChdrcoy(),covrlnbIO.getChdrcoy()))
		|| (isNE(racdmjaIO.getChdrnum(),covrlnbIO.getChdrnum()))
		|| (isNE(racdmjaIO.getLife(),covrlnbIO.getLife()))
		|| (isNE(racdmjaIO.getCoverage(),covrlnbIO.getCoverage()))
		|| (isNE(racdmjaIO.getRider(),covrlnbIO.getRider()))
		|| (isNE(racdmjaIO.getPlanSuffix(),covrlnbIO.getPlanSuffix()))) {
			racdmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1390);
		}
		datcon1rec.intDate.set(racdmjaIO.getCtdate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaCtdate.set(datcon1rec.extDate);
		if (isEQ(racdmjaIO.getRetype(),"F")) {
			wsaaRetype.set("Facultative");
		}
		else {
			wsaaRetype.set("Treaty");
		}
		wsaaRngmnt.set(racdmjaIO.getRngmnt());
		wsaaRaAmount.set(racdmjaIO.getRaAmount());
		sv.subfileArea.set(SPACES);
		sv.life.set(racdmjaIO.getLife());
		sv.coverage.set(racdmjaIO.getCoverage());
		sv.rider.set(racdmjaIO.getRider());
		sv.lifeOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.hcrtable.set(covrlnbIO.getCrtable());
		sv.hrrn01.set(racdmjaIO.getRrn());
		sv.hrrn02.set(covrlnbIO.getRrn());
		sv.hrrn03.set(ZERO);
		sv.elemdesc.set(wsaaReassuranceDetails);
		if(isEQ(sv.bascovr,SPACES))
			sv.bascovr.set(3);
		addToSubfile1400();
	}

protected void addToSubfile1400()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5442", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*DISPLAY-SCREEN*/
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2060();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateScreen2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5442", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		

		/*EXIT*/

	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2110();
				}
				case updateErrorIndicators2170: {
					updateErrorIndicators2170();
					readNextModifiedRecord2180();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2110()
	{
		if (isEQ(sv.action,SPACES)) {
			sv.actionErr.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2170);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.action);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.actionErr.set(optswchrec.optsStatuz);
		}
		wsaaAction.set(sv.action);
		if (isNE(sv.action,"2")) {
			wsaaEnquireOnly = "N";
		}
		if(isEQ(sv.indxflg,"R") && isEQ(sv.action,1))
		{
			sv.actionErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2170);
		}
		
		if(isEQ(sv.indxflg,"Q") || (isEQ(sv.indxflg,"N") && isNE(sv.action,SPACES)) )
		{
			sv.actionErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2170);
		}
		
		if(isEQ(sv.indxflg,"P") && (isEQ(sv.action,2) || isEQ(sv.action,3) || isEQ(sv.action,4)))
		{
			sv.actionErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2170);
		}

		if ((isNE(sv.hrrn01,ZERO))
		&& (isEQ(sv.action,"1"))) {
			sv.actionErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2170);
		}
	}

protected void updateErrorIndicators2170()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.hrrn01,ZERO)) {
			if ((isEQ(sv.rider,"00"))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(SPACES);
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(SPACES);
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5442", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2180()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5442", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			processSelections3010();
		}
		catch (GOTOException e){
		}
	}

protected void processSelections3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsaaAtSubmitFlag.set("Y");
			goTo(GotoLabel.exit3090);
		}
		
		if(isEQ(sv.indxflg,"P")){
			wsspcomn.flag.set("C");
		}
		else if(isEQ(sv.indxflg,"R")){
			if(isEQ(sv.action,2))
				wsspcomn.flag.set("M");
			if(isEQ(sv.action,3))
				wsspcomn.flag.set("I");
			if(isEQ(sv.action,4))
				wsspcomn.flag.set("D");
		
		}
		if(isEQ(wsspcomn.flag,"I"))
			wsaaEnquireOnly = "Y";
		
		if (atSubmissionReqd.isTrue()
		&& isEQ(wsaaAction,SPACES)
		&& isEQ(wsaaEnquireOnly,"N")) {
			callAt3100();
		}
	}

protected void callAt3100()
	{
		at3001();
	}

protected void at3001()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5442AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wssplife.effdate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		atreqrec.primaryKey.set(chdrmjaIO.getChdrnum());
		wsaaEffdate.set(wssplife.effdate);
		wsaaFsuco.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
		wsaaMsgnum.set(chdrmjaIO.getChdrnum());
		wsspcomn.msgarea.set(wsaaMsgarea);
		wsaaAtSubmitFlag.set("N");
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case optswch4080: {
					optswch4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.action,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			goTo(GotoLabel.optswch4080);
		}
		if (isEQ(sv.action,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
		if (isEQ(sv.action,"2")) {
			optswchrec.optsSelOptno.set(2);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
		if (isEQ(sv.action,"3")) {
			optswchrec.optsSelOptno.set(3);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
		if (isEQ(sv.action,"4")) {
			optswchrec.optsSelOptno.set(4);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
	}

protected void optswch4080()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			programStack4200();
			goTo(GotoLabel.exit4090);
		}
		programStack4200();
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5442", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void programStack4200()
	{
		/*STCK*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*EXIT*/
	}

protected void keepRecord4300()
	{
		keep4310();
	}

protected void keep4310()
	{
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		racdmjaIO.setNonKey(SPACES);
		racdmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		racdmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		racdmjaIO.setLife(sv.life);
		racdmjaIO.setCoverage(sv.coverage);
		racdmjaIO.setRider(sv.rider);
		racdmjaIO.setCurrcode(chdrmjaIO.getCntcurr());
		racdmjaIO.setFormat(racdmjarec);
		if (isEQ(sv.hrrn01,ZERO)) {
			racdmjaIO.setTranno(ZERO);
			racdmjaIO.setSeqno(ZERO);
			racdmjaIO.setCurrfrom(ZERO);
			racdmjaIO.setCurrto(ZERO);
			racdmjaIO.setRaAmount(ZERO);
			racdmjaIO.setCtdate(ZERO);
			racdmjaIO.setCmdate(ZERO);
			racdmjaIO.setReasper(ZERO);
			racdmjaIO.setRecovamt(ZERO);
			racdmjaIO.setRrevdt(ZERO);
			racdmjaIO.setValidflag("1");
			racdmjaIO.setFunction(varcom.keeps);
		}
		else {
			racdmjaIO.setRrn(sv.hrrn01);
			racdmjaIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, racdmjaIO);
			if (isNE(racdmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(racdmjaIO.getParams());
				fatalError600();
			}
			racdmjaIO.setFunction(varcom.keeps);
		}
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdmjaIO.getParams());
			fatalError600();
		}
		covrlnbIO.setParams(SPACES);
		if (isEQ(wsaaAction,"1")) {
			covrlnbIO.setRrn(sv.hrrn03);
		}
		else {
			covrlnbIO.setRrn(sv.hrrn02);
		}
		covrlnbIO.setFormat("COVRLNBREC");
		covrlnbIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			fatalError600();
		}
		covrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			fatalError600();
		}
	}
}
