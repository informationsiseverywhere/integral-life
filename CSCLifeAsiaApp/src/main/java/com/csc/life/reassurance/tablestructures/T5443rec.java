package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:12
 * Description:
 * Copybook name: T5443REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5443rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5443Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData gstrates = new FixedLengthStringData(20).isAPartOf(t5443Rec, 0);
  	public ZonedDecimalData[] gstrate = ZDArrayPartOfStructure(5, 4, 2, gstrates, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(gstrates, 0, FILLER_REDEFINE);
  	public ZonedDecimalData gstrate01 = new ZonedDecimalData(4, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData gstrate02 = new ZonedDecimalData(4, 2).isAPartOf(filler, 4);
  	public ZonedDecimalData gstrate03 = new ZonedDecimalData(4, 2).isAPartOf(filler, 8);
  	public ZonedDecimalData gstrate04 = new ZonedDecimalData(4, 2).isAPartOf(filler, 12);
  	public ZonedDecimalData gstrate05 = new ZonedDecimalData(4, 2).isAPartOf(filler, 16);
  	public FixedLengthStringData reprmfrms = new FixedLengthStringData(85).isAPartOf(t5443Rec, 20);
  	public ZonedDecimalData[] reprmfrm = ZDArrayPartOfStructure(5, 17, 2, reprmfrms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(85).isAPartOf(reprmfrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData reprmfrm01 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData reprmfrm02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData reprmfrm03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData reprmfrm04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData reprmfrm05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public FixedLengthStringData reprmtos = new FixedLengthStringData(85).isAPartOf(t5443Rec, 105);
  	public ZonedDecimalData[] reprmto = ZDArrayPartOfStructure(5, 17, 2, reprmtos, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(85).isAPartOf(reprmtos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData reprmto01 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData reprmto02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17);
  	public ZonedDecimalData reprmto03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34);
  	public ZonedDecimalData reprmto04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51);
  	public ZonedDecimalData reprmto05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(310).isAPartOf(t5443Rec, 190, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5443Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5443Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}