package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5462
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5462ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(91);
	public FixedLengthStringData dataFields = new FixedLengthStringData(43).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 43);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 55);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(274);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(96).isAPartOf(subfileArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData elemdesc = DD.elemdesc.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData hcallind = DD.hcallind.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,51);
	public FixedLengthStringData hrrns = new FixedLengthStringData(27).isAPartOf(subfileFields, 55);
	public ZonedDecimalData[] hrrn = ZDArrayPartOfStructure(3, 9, 0, hrrns, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(27).isAPartOf(hrrns, 0, FILLER_REDEFINE);
	public ZonedDecimalData hrrn01 = DD.hrrn.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData hrrn02 = DD.hrrn.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData hrrn03 = DD.hrrn.copyToZonedDecimal().isAPartOf(filler,18);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,82);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,84);
	public FixedLengthStringData shortdesc = DD.shrtdesc.copy().isAPartOf(subfileFields,86);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 96);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData elemdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcallindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hrrnsErr = new FixedLengthStringData(12).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData[] hrrnErr = FLSArrayPartOfStructure(3, 4, hrrnsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(hrrnsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hrrn01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData hrrn02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData hrrn03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData shrtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 140);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] elemdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcallindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData hrrnsOut = new FixedLengthStringData(36).isAPartOf(outputSubfile, 60);
	public FixedLengthStringData[] hrrnOut = FLSArrayPartOfStructure(3, 12, hrrnsOut, 0);
	public FixedLengthStringData[][] hrrnO = FLSDArrayPartOfArrayStructure(12, 1, hrrnOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(hrrnsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hrrn01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] hrrn02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] hrrn03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] shrtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 272);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5462screensflWritten = new LongData(0);
	public LongData S5462screenctlWritten = new LongData(0);
	public LongData S5462screenWritten = new LongData(0);
	public LongData S5462protectWritten = new LongData(0);
	public GeneralTable s5462screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5462screensfl;
	}

	public S5462ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"02","04","-02","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hrrn01, hrrn02, hrrn03, hcrtable, hcallind, action, life, coverage, rider, shortdesc, elemdesc};
		screenSflOutFields = new BaseData[][] {hrrn01Out, hrrn02Out, hrrn03Out, hcrtableOut, hcallindOut, actionOut, lifeOut, coverageOut, riderOut, shrtdescOut, elemdescOut};
		screenSflErrFields = new BaseData[] {hrrn01Err, hrrn02Err, hrrn03Err, hcrtableErr, hcallindErr, actionErr, lifeErr, coverageErr, riderErr, shrtdescErr, elemdescErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {cnttype, ctypedes, chdrsel};
		screenOutFields = new BaseData[][] {cnttypeOut, ctypedesOut, chdrselOut};
		screenErrFields = new BaseData[] {cnttypeErr, ctypedesErr, chdrselErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenActionVar = action;
		screenRecord = S5462screen.class;
		screenSflRecord = S5462screensfl.class;
		screenCtlRecord = S5462screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5462protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5462screenctl.lrec.pageSubfile);
	}
}
