package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:42
 * Description:
 * Copybook name: RECORCOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Recorcokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData recorcoFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData recorcoKey = new FixedLengthStringData(64).isAPartOf(recorcoFileKey, 0, REDEFINE);
  	public FixedLengthStringData recorcoChdrcoy = new FixedLengthStringData(1).isAPartOf(recorcoKey, 0);
  	public FixedLengthStringData recorcoChdrnum = new FixedLengthStringData(8).isAPartOf(recorcoKey, 1);
  	public FixedLengthStringData recorcoLife = new FixedLengthStringData(2).isAPartOf(recorcoKey, 9);
  	public FixedLengthStringData recorcoCoverage = new FixedLengthStringData(2).isAPartOf(recorcoKey, 11);
  	public FixedLengthStringData recorcoRider = new FixedLengthStringData(2).isAPartOf(recorcoKey, 13);
  	public PackedDecimalData recorcoPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(recorcoKey, 15);
  	public PackedDecimalData recorcoCostdate = new PackedDecimalData(8, 0).isAPartOf(recorcoKey, 18);
  	public PackedDecimalData recorcoSeqno = new PackedDecimalData(2, 0).isAPartOf(recorcoKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(recorcoKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(recorcoFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		recorcoFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}