package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH619
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh619ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(308);
	public FixedLengthStringData dataFields = new FixedLengthStringData(84).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData lrkclss = new FixedLengthStringData(20).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] lrkcls = FLSArrayPartOfStructure(5, 4, lrkclss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(lrkclss, 0, FILLER_REDEFINE);
	public FixedLengthStringData lrkcls01 = DD.lrkcls.copy().isAPartOf(filler,0);
	public FixedLengthStringData lrkcls02 = DD.lrkcls.copy().isAPartOf(filler,4);
	public FixedLengthStringData lrkcls03 = DD.lrkcls.copy().isAPartOf(filler,8);
	public FixedLengthStringData lrkcls04 = DD.lrkcls.copy().isAPartOf(filler,12);
	public FixedLengthStringData lrkcls05 = DD.lrkcls.copy().isAPartOf(filler,16);
	public FixedLengthStringData rngmnts = new FixedLengthStringData(20).isAPartOf(dataFields, 59);
	public FixedLengthStringData[] rngmnt = FLSArrayPartOfStructure(5, 4, rngmnts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(rngmnts, 0, FILLER_REDEFINE);
	public FixedLengthStringData rngmnt01 = DD.rngmnt.copy().isAPartOf(filler1,0);
	public FixedLengthStringData rngmnt02 = DD.rngmnt.copy().isAPartOf(filler1,4);
	public FixedLengthStringData rngmnt03 = DD.rngmnt.copy().isAPartOf(filler1,8);
	public FixedLengthStringData rngmnt04 = DD.rngmnt.copy().isAPartOf(filler1,12);
	public FixedLengthStringData rngmnt05 = DD.rngmnt.copy().isAPartOf(filler1,16);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 84);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lrkclssErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] lrkclsErr = FLSArrayPartOfStructure(5, 4, lrkclssErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(lrkclssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData lrkcls01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData lrkcls02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData lrkcls03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData lrkcls04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData lrkcls05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData rngmntsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] rngmntErr = FLSArrayPartOfStructure(5, 4, rngmntsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(rngmntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rngmnt01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData rngmnt02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData rngmnt03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData rngmnt04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData rngmnt05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 140);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData lrkclssOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(5, 12, lrkclssOut, 0);
	public FixedLengthStringData[][] lrkclsO = FLSDArrayPartOfArrayStructure(12, 1, lrkclsOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(lrkclssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] lrkcls01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] lrkcls02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] lrkcls03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] lrkcls04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] lrkcls05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData rngmntsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(5, 12, rngmntsOut, 0);
	public FixedLengthStringData[][] rngmntO = FLSDArrayPartOfArrayStructure(12, 1, rngmntOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(rngmntsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rngmnt01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] rngmnt02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] rngmnt03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] rngmnt04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] rngmnt05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh619screenWritten = new LongData(0);
	public LongData Sh619protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh619ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, lrkcls01, rngmnt01, lrkcls02, rngmnt02, lrkcls03, rngmnt03, lrkcls04, rngmnt04, lrkcls05, rngmnt05};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, lrkcls01Out, rngmnt01Out, lrkcls02Out, rngmnt02Out, lrkcls03Out, rngmnt03Out, lrkcls04Out, rngmnt04Out, lrkcls05Out, rngmnt05Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, lrkcls01Err, rngmnt01Err, lrkcls02Err, rngmnt02Err, lrkcls03Err, rngmnt03Err, lrkcls04Err, rngmnt04Err, lrkcls05Err, rngmnt05Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh619screen.class;
		protectRecord = Sh619protect.class;
	}

}
