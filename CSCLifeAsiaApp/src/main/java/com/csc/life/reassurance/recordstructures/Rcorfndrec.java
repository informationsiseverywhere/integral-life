package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:29
 * Description:
 * Copybook name: RCORFNDREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rcorfndrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rcorfndRec = new FixedLengthStringData(176);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rcorfndRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rcorfndRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rcorfndRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rcorfndRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rcorfndRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rcorfndRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rcorfndRec, 22);
  	public PackedDecimalData seqno = new PackedDecimalData(2, 0).isAPartOf(rcorfndRec, 24);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(rcorfndRec, 26);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rcorfndRec, 29);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rcorfndRec, 33);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(rcorfndRec, 36);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rcorfndRec, 39);
  	public PackedDecimalData currfrom = new PackedDecimalData(8, 0).isAPartOf(rcorfndRec, 44);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(rcorfndRec, 49);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 0, FILLER);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rcorfndRec, 68);
  	public PackedDecimalData newRaAmt = new PackedDecimalData(17, 2).isAPartOf(rcorfndRec, 73);
  	public PackedDecimalData refund = new PackedDecimalData(17, 2).isAPartOf(rcorfndRec, 90);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rcorfndRec, 107);
  	public PackedDecimalData refundFee = new PackedDecimalData(17, 2).isAPartOf(rcorfndRec, 108);
  	public PackedDecimalData refundPrem = new PackedDecimalData(17, 2).isAPartOf(rcorfndRec, 125);
  	public PackedDecimalData refundComm = new PackedDecimalData(17, 2).isAPartOf(rcorfndRec, 142);
  	public PackedDecimalData newSumins = new PackedDecimalData(17, 2).isAPartOf(rcorfndRec, 159);

	public void initialize() {
		COBOLFunctions.initialize(rcorfndRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rcorfndRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}