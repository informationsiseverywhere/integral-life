package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.recordstructures.VpxReinsureRec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.dao.RasapfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.dataaccess.model.Rasapf;
import com.csc.life.reassurance.recordstructures.Rprmiumrec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;

public class VpxReinsure extends SMARTCodeModel {
	
	private Rprmiumrec rprmiumrec = new Rprmiumrec();
	private VpxReinsureRec reinsurerec = new VpxReinsureRec();
	
	private Map<String,List<String>> inPutParam = new HashMap<>();
	private Map<String,List<List<String>>> inPutParam2 = new HashMap<>();
	private Racdpf racdpf = null;
	private RacdpfDAO racdDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private RasapfDAO rasapfDAO = getApplicationContext().getBean("rasapfDAO", RasapfDAO.class);
    private T5449rec t5449rec = new T5449rec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	@Override
	public void mainline(Object... parmArray)
	{
		rprmiumrec.rprmiumRec = convertAndSetParam(rprmiumrec.rprmiumRec, parmArray, 0);
		reinsurerec = (VpxReinsureRec) parmArray[1];
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	protected void startSubr010() {	
		initMap();
		initialise();
		setParams();
		setReinsureRec();
		exitProgram();
	}
	
	private void initMap() {
		inPutParam.put("reinsSumins", new ArrayList<String>());
		inPutParam.put("reinsArngtyp", new ArrayList<String>());
		inPutParam.put("reinsTrtqtshrprcnt", new ArrayList<String>());
		inPutParam.put("reinsTrtrtnlmt", new ArrayList<String>());
		inPutParam.put("reinsRtncurr", new ArrayList<String>());
		inPutParam.put("reinsCostfreq", new ArrayList<String>());
		inPutParam.put("reinsRatfact", new ArrayList<String>());
		inPutParam.put("reinsPremrat", new ArrayList<String>());
		inPutParam.put("lifRiskcls", new ArrayList<String>());
		inPutParam.put("arngCode", new ArrayList<String>());
		inPutParam.put("reinsAccno", new ArrayList<String>());
		inPutParam.put("racdRCD", new ArrayList<String>());
		
		inPutParam2.put("reinsSuminsParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsArngtypParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsTrtqtshrprcntParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsTrtrtnlmtParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsRtncurrParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsCostfreqParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsRatfactParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsPremratParamList", new ArrayList<List<String>>());
		inPutParam2.put("lifRiskclsParamList", new ArrayList<List<String>>());
		inPutParam2.put("arngCodeParamList", new ArrayList<List<String>>());
		inPutParam2.put("reinsAccnoParamList", new ArrayList<List<String>>());
		inPutParam2.put("racdRCDParamList", new ArrayList<List<String>>());
	}
	
	protected void initialise() {

		racdpf = racdDAO.getRacdbySeqNo(rprmiumrec.chdrChdrcoy.toString(), rprmiumrec.chdrChdrnum.toString(), rprmiumrec.seqNo.toInt(), 
				rprmiumrec.lifeLife.toString(), rprmiumrec.covrCoverage.toString(), rprmiumrec.covrRider.toString(), rprmiumrec.plnsfx.toInt());
		
		if(racdpf!=null) {
			reinsurerec.setReinsCode(racdpf.getRasnum());
	    	inPutParam.get("reinsSumins").add(racdpf.getRaAmount().toString());
	    	inPutParam.get("reinsArngtyp").add(racdpf.getRetype());
	    	readT5449();
	    	for (int i=1; i <= 10; i++) {
	            if (isEQ(t5449rec.rasnum[i], racdpf.getRasnum())) {
	            	inPutParam.get("reinsTrtqtshrprcnt").add(t5449rec.qreshr[i].toString());
	    	    	inPutParam.get("reinsTrtrtnlmt").add(t5449rec.relimit[i].toString());
	    	    	inPutParam.get("reinsRtncurr").add(t5449rec.currcode.toString());
	                break;
	            }
	        }
	    	
	    	inPutParam.get("reinsCostfreq").add(rprmiumrec.billfreq.toString());
	    	inPutParam.get("reinsRatfact").add(rprmiumrec.reasratfac.toString());
	    	inPutParam.get("reinsPremrat").add(rprmiumrec.rrate.toString());
	    	inPutParam.get("lifRiskcls").add(racdpf.getLrkcls());
	    	inPutParam.get("arngCode").add(racdpf.getRngmnt());
	    	getBankAcckey(rprmiumrec.chdrChdrcoy.toString(),racdpf.getRasnum());
	    	inPutParam.get("racdRCD").add(String.valueOf(racdpf.getCmdate()));
		}
    }
	
	protected void readT5449() {
		List<Itempf> itdmpfList = itemDAO.getItdmByFrmdate(rprmiumrec.chdrChdrcoy.toString(), "T5449", racdpf.getRngmnt(), racdpf.getCurrfrom());
		if(!itdmpfList.isEmpty()) {
			t5449rec.t5449Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
	}
	
	protected void getBankAcckey(String rascoy, String rasnum) {
    	Rasapf rasapf = rasapfDAO.getRasapf(rascoy, rasnum);
    	if(rasapf!=null) {
    		inPutParam.get("reinsAccno").add(rasapf.getBankacckey().trim());
    	}
    }
	
	protected void setParams() {
		inPutParam2.get("reinsSuminsParamList").add(inPutParam.get("reinsSumins"));
		inPutParam2.get("reinsArngtypParamList").add(inPutParam.get("reinsArngtyp"));
		inPutParam2.get("reinsTrtqtshrprcntParamList").add(inPutParam.get("reinsTrtqtshrprcnt"));
		inPutParam2.get("reinsTrtrtnlmtParamList").add(inPutParam.get("reinsTrtrtnlmt"));
		inPutParam2.get("reinsRtncurrParamList").add(inPutParam.get("reinsRtncurr"));
		inPutParam2.get("reinsCostfreqParamList").add(inPutParam.get("reinsCostfreq"));
		inPutParam2.get("reinsRatfactParamList").add(inPutParam.get("reinsRatfact"));
		inPutParam2.get("reinsPremratParamList").add(inPutParam.get("reinsPremrat"));
		inPutParam2.get("lifRiskclsParamList").add(inPutParam.get("lifRiskcls"));
		inPutParam2.get("arngCodeParamList").add(inPutParam.get("arngCode"));
		inPutParam2.get("reinsAccnoParamList").add(inPutParam.get("reinsAccno"));
		inPutParam2.get("racdRCDParamList").add(inPutParam.get("racdRCD"));
	}
	
	protected void setReinsureRec() {
		reinsurerec.statuz.set(Varcom.oK);
		reinsurerec.setReinsSumins(inPutParam2.get("reinsSuminsParamList"));
		reinsurerec.setReinsArngtyp(inPutParam2.get("reinsArngtypParamList"));
		reinsurerec.setReinsTrtqtshrprcnt(inPutParam2.get("reinsTrtqtshrprcntParamList"));
		reinsurerec.setReinsTrtrtnlmt(inPutParam2.get("reinsTrtrtnlmtParamList"));
		reinsurerec.setReinsRtncurr(inPutParam2.get("reinsRtncurrParamList"));
		reinsurerec.setReinsCostfreq(inPutParam2.get("reinsCostfreqParamList"));
		reinsurerec.setReinsRatfact(inPutParam2.get("reinsRatfactParamList"));
		reinsurerec.setReinsPremrat(inPutParam2.get("reinsPremratParamList"));
		reinsurerec.setLifRiskcls(inPutParam2.get("lifRiskclsParamList"));
		reinsurerec.setArngCode(inPutParam2.get("arngCodeParamList"));
		reinsurerec.setReinsAccno(inPutParam2.get("reinsAccnoParamList"));
		reinsurerec.setRacdRCD(inPutParam2.get("racdRCDParamList"));
	}
}