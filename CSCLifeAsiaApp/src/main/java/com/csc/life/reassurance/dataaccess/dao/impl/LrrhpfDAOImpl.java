package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.LrrhpfDAO;
import com.csc.life.reassurance.dataaccess.model.Lrrhpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LrrhpfDAOImpl extends BaseDAOImpl<Lrrhpf> implements LrrhpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LrrhpfDAOImpl.class);
	public List<Lrrhpf> searchlLrrhconRecord(String coy, String chdrnum) {
		String sql = "SELECT UNIQUE_NUMBER FROM LRRHCON WHERE COMPANY=? AND CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		List<Lrrhpf> searchResult = new LinkedList<Lrrhpf>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Lrrhpf t = new Lrrhpf();
				t.setUniqueNumber(rs.getLong(1));
				searchResult.add(t);
			}

		} catch (SQLException e) {
			LOGGER.error("searchlLrrhconRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return searchResult;
	}   
	
	public void deleteLrrhpfRecord(List<Lrrhpf> dataList){
		String sql = "DELETE FROM LRRHPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Lrrhpf l:dataList){
				ps.setLong(1, l.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteLrrhpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	@Override
	public void updateLrrhpfBulk(String coy, String chdrnum,  String life, String coverage, String rider, String plnsfx,
			 int busDate) {
		
			String sqlLrrhUpdate = "UPDATE LRRHPF SET VALIDFLAG='2',CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE  COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG='1' ";
			PreparedStatement psLrrhUpdate = getPrepareStatement(sqlLrrhUpdate);
			try {
				
					psLrrhUpdate.setInt(1, busDate);
					psLrrhUpdate.setString(2, getJobnm());
					psLrrhUpdate.setString(3, getUsrprf());
					psLrrhUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
					psLrrhUpdate.setString(5, coy);
					psLrrhUpdate.setString(6, chdrnum);
					psLrrhUpdate.setString(7, life);
					psLrrhUpdate.setString(8, coverage);
					psLrrhUpdate.setString(9, rider);
					psLrrhUpdate.setString(10, plnsfx);
					psLrrhUpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateLrrhpfBulk()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psLrrhUpdate, null);
			}
		
	}
	
	@Override
	public void updateLrrhpfSsretnAndSsreast(String coy, String chdrnum,  String life, String coverage, String rider, 
			String plnsfx, int busDate) {
		
			String sqlLrrhUpdate = "UPDATE LRRHPF SET SSRETN=0, SSREAST=0, JOBNM=?, USRPRF=?, DATIME=?"
					+ " WHERE COMPANY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? "
					+ "AND VALIDFLAG='1' ";
			PreparedStatement psLrrhUpdate = getPrepareStatement(sqlLrrhUpdate);
			try {
				
					psLrrhUpdate.setString(1, getJobnm());
					psLrrhUpdate.setString(2, getUsrprf());
					psLrrhUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
					psLrrhUpdate.setString(4, coy);
					psLrrhUpdate.setString(5, chdrnum);
					psLrrhUpdate.setString(6, life);
					psLrrhUpdate.setString(7, coverage);
					psLrrhUpdate.setString(8, rider);
					psLrrhUpdate.setString(9, plnsfx);
					psLrrhUpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateLrrhpfSsretnAndSsreast()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psLrrhUpdate, null);
			}
	}
	
	 public void insertLrrhpfRecord(Lrrhpf lrrhpf) {
	    	StringBuilder insertSql = new StringBuilder();
	    	insertSql.append(" INSERT INTO VM1DTA.LRRHPF (CLNTPFX, CLNTCOY, CLNTNUM, COMPANY, CHDRNUM, LIFE, "
	    			+ "COVERAGE, RIDER, PLNSFX, LRKCLS, CURRFROM, CURRTO, VALIDFLAG, TRANNO, CURRENCY, SSRETN, "
	    			+ "SSREAST, SSREASF, USRPRF, JOBNM, DATIME ) ");
	    	insertSql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
	    	PreparedStatement ps = getPrepareStatement(insertSql.toString());
	    		
	        try {
	        	ps.setString(1, lrrhpf.getClntpfx());
				ps.setString(2, lrrhpf.getClntcoy());
				ps.setString(3, lrrhpf.getClntnum());
				ps.setString(4, lrrhpf.getCompany());
				ps.setString(5, lrrhpf.getChdrnum());
				ps.setString(6, lrrhpf.getLife());
				ps.setString(7, lrrhpf.getCoverage());
				ps.setString(8, lrrhpf.getRider());
				ps.setInt(9, lrrhpf.getPlanSuffix());
				ps.setString(10, lrrhpf.getLrkcls());
				ps.setInt(11, lrrhpf.getCurrfrom());
				ps.setInt(12, lrrhpf.getCurrto());
				ps.setString(13, lrrhpf.getValidflag());
				ps.setInt(14, lrrhpf.getTranno());
				ps.setString(15, lrrhpf.getCurrency());
				ps.setBigDecimal(16, lrrhpf.getSsretn());
				ps.setBigDecimal(17, lrrhpf.getSsreast());
				ps.setBigDecimal(18, lrrhpf.getSsreasf());
	            ps.setString(19, getUsrprf());
	            ps.setString(20, getJobnm());
	            ps.setTimestamp(21, new Timestamp(System.currentTimeMillis()));
	            ps.executeUpdate();
	        } 
	        catch (SQLException e) {
	        	LOGGER.error("insertLrrhpfRecord()", e);
	            throw new SQLRuntimeException(e);
	        } 
	        finally {
	            close(ps, null);
	        }
	 	}
	 
	 public Lrrhpf getLrrhpfRecord(Lrrhpf lrrhpf) {
			StringBuilder sql = new StringBuilder("SELECT ");
			sql.append(" UNIQUE_NUMBER, CLNTPFX, CLNTCOY, CLNTNUM, COMPANY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, "
					+ " LRKCLS, CURRFROM, CURRTO, VALIDFLAG, TRANNO, CURRENCY, SSRETN, SSREAST, SSREASF, USRPRF, "
					+ " JOBNM, DATIME, CREATED_AT ");
			sql.append(" FROM LRRHPF ");
			sql.append(" WHERE COMPANY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? "
					+ "AND PLNSFX = ? AND VALIDFLAG = '1' ");

			PreparedStatement ps = null;
			ResultSet rs = null;
			Lrrhpf lrrh = new Lrrhpf();
			try {
				ps = getPrepareStatement(sql.toString());
				ps.setString(1, lrrhpf.getCompany());
	            ps.setString(2, lrrhpf.getChdrnum());
	            ps.setString(3, lrrhpf.getLife());
	            ps.setString(4, lrrhpf.getCoverage());
	            ps.setString(5, lrrhpf.getRider());
	            ps.setInt(6, lrrhpf.getPlanSuffix());
				rs = ps.executeQuery();
				while (rs.next()) {
					lrrh.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					lrrh.setClntpfx(rs.getString("CLNTPFX"));
					lrrh.setClntcoy(rs.getString("CLNTCOY"));
					lrrh.setClntnum(rs.getString("CLNTNUM"));
					lrrh.setCompany(rs.getString("COMPANY"));
					lrrh.setChdrnum(rs.getString("CHDRNUM"));
					lrrh.setLife(rs.getString("LIFE"));
					lrrh.setCoverage(rs.getString("COVERAGE"));
					lrrh.setRider(rs.getString("RIDER"));
					lrrh.setPlanSuffix(rs.getInt("PLNSFX"));
					lrrh.setLrkcls(rs.getString("LRKCLS"));
					lrrh.setCurrfrom(rs.getInt("CURRFROM"));
					lrrh.setCurrto(rs.getInt("CURRTO"));
					lrrh.setValidflag(rs.getString("VALIDFLAG"));
					lrrh.setTranno(rs.getInt("TRANNO"));
					lrrh.setCurrency(rs.getString("CURRENCY"));
					lrrh.setSsretn(rs.getBigDecimal("SSRETN"));
					lrrh.setSsreast(rs.getBigDecimal("SSREAST"));
					lrrh.setSsreasf(rs.getBigDecimal("SSREASF"));
				}
			} catch (SQLException e) {
				LOGGER.error("getLrrhpfRecord()" + e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return lrrh;
		}
}