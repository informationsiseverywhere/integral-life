package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5468.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5468Report extends SMARTReportLayout { 

	private ZonedDecimalData anbccd = new ZonedDecimalData(3, 0);
	private FixedLengthStringData chdrdesc = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData cltdob = new FixedLengthStringData(10);
	private FixedLengthStringData cltsex = new FixedLengthStringData(1);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData crtabdesc = new FixedLengthStringData(30);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private FixedLengthStringData pstatdsc = new FixedLengthStringData(10);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0);
	private FixedLengthStringData rstatdsc = new FixedLengthStringData(10);
	private FixedLengthStringData sicurr = new FixedLengthStringData(3);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5468Report() {
		super();
	}


	/**
	 * Print the XML for R5468d01
	 */
	public void printR5468d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 1, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 9, 37));
		cltsex.setFieldName("cltsex");
		cltsex.setInternal(subString(recordData, 46, 1));
		cltdob.setFieldName("cltdob");
		cltdob.setInternal(subString(recordData, 47, 10));
		anbccd.setFieldName("anbccd");
		anbccd.setInternal(subString(recordData, 57, 3));
		printLayout("R5468d01",			// Record name
			new BaseData[]{			// Fields:
				clntnum,
				clntnm,
				cltsex,
				cltdob,
				anbccd
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5468d02
	 */
	public void printR5468d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 1, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 9, 37));
		cltsex.setFieldName("cltsex");
		cltsex.setInternal(subString(recordData, 46, 1));
		cltdob.setFieldName("cltdob");
		cltdob.setInternal(subString(recordData, 47, 10));
		anbccd.setFieldName("anbccd");
		anbccd.setInternal(subString(recordData, 57, 3));
		printLayout("R5468d02",			// Record name
			new BaseData[]{			// Fields:
				clntnum,
				clntnm,
				cltsex,
				cltdob,
				anbccd
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5468d03
	 */
	public void printR5468d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 1, 4));
		printLayout("R5468d03",			// Record name
			new BaseData[]{			// Fields:
				crtable
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for R5468d04
	 */
	public void printR5468d04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 1, 4));
		crtabdesc.setFieldName("crtabdesc");
		crtabdesc.setInternal(subString(recordData, 5, 30));
		rcestrm.setFieldName("rcestrm");
		rcestrm.setInternal(subString(recordData, 35, 3));
		sicurr.setFieldName("sicurr");
		sicurr.setInternal(subString(recordData, 38, 3));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 41, 17));
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 58, 3));
		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 61, 17));
		cmdate.setFieldName("cmdate");
		cmdate.setInternal(subString(recordData, 78, 10));
		printLayout("R5468d04",			// Record name
			new BaseData[]{			// Fields:
				crtable,
				crtabdesc,
				rcestrm,
				sicurr,
				sumins,
				currcode,
				raamount,
				cmdate
			}
		);

	}

	/**
	 * Print the XML for R5468h01
	 */
	public void printR5468h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 1, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 9, 37));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 46, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 54, 3));
		chdrdesc.setFieldName("chdrdesc");
		chdrdesc.setInternal(subString(recordData, 57, 30));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 87, 10));
		rstatdsc.setFieldName("rstatdsc");
		rstatdsc.setInternal(subString(recordData, 97, 10));
		pstatdsc.setFieldName("pstatdsc");
		pstatdsc.setInternal(subString(recordData, 107, 10));
		printLayout("R5468h01",			// Record name
			new BaseData[]{			// Fields:
				rasnum,
				clntnm,
				chdrnum,
				cnttype,
				chdrdesc,
				occdate,
				rstatdsc,
				pstatdsc
			}
		);

		currentPrintLine.set(9);
	}


}
