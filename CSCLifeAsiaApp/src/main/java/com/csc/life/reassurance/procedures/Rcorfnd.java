/*
 * File: Rcorfnd.java
 * Date: 30 August 2009 2:03:46
 * Author: Quipoz Limited
 *
 * Class transformed from RCORFND.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.reassurance.dataaccess.RacdrcoTableDAM;
import com.csc.life.reassurance.dataaccess.RecoclmTableDAM;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Recopstrec;
import com.csc.life.reassurance.tablestructures.T5445rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Reassurance Refund Calculation Subroutine.
*
* Overview
* ========
*
* When  Alterations  are done to  a policy, it  may  result  in
* Terminations  of  existing  Cessions  with effect  from  the
* alteration  effective  date. As  Premiums  are  Costed  on  a
* regular  basis , there  may  be need  to calculate  and  claim
* Premium Refund for the period between the alteration effective
* date  and  the costed  to date of the particular cession. This
* Subroutine  Calculates  the Refund applicable from the cession
* detail,  effective date of the change  and the new Sum Assured
* for the Tranche (i.e reduced Sum Assured or Zero).
*
* Linkage
* =======
*
* FUNCTION           PIC X(05).
* STATUZ             PIC X(04).
* CHDRCOY            PIC X(01).
* CHDRNUM            PIC X(08).
* LIFE               PIC X(02).
* COVERAGE           PIC X(02).
* RIDER              PIC X(02).
* SEQNO              PIC S9(02) COMP-3.
* PLAN-SUFFIX        PIC S9(04) COMP-3.
* CRTABLE            PIC X(04).
* POLSUM             PIC S9(04) COMP-3.
* EFFDATE            PIC S9(08) COMP-3.
* CURRFROM           PIC S9(08) COMP-3.
* BATCKEY.
*     FILLER         PIC X(02).
*     BATCCOY        PIC X(01).
*     BATCBRN        PIC X(02).
*     BATCACTYR      PIC S9(02) COMP-3.
*     BATCACTMN      PIC S9(02) COMP-3.
*     BATCTRCDE      PIC X(04).
*     BATCBATCH      PIC X(05).
* TRANNO             PIC S9(05) COMP-3.
* NEW-RA-AMT         PIC S9(11)V99 COMP-3.
* REFUND             PIC S9(11)V99 COMP-3.
* LANGUAGE           PIC X(01).
* REFUND-FEE         PIC S9(09)V9(02) COMP-3.
* REFUND-PREM        PIC S9(09)V9(02) COMP-3.
* REFUND-COMM        PIC S9(09)V9(02) COMP-3.
*
* These fields are all contained within RCORFNDREC.
*
*****************************************************************
* </pre>
*/
public class Rcorfnd extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "RCORFND";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCtdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRaAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRefundPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRefundCompay = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRefundBalance = new PackedDecimalData(17, 2);
	private String wsaaReallyNotFound = "Y";
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5).setUnsigned();

	private FixedLengthStringData wsaaFreqFactorX = new FixedLengthStringData(11).isAPartOf(wsaaFreqFactor, 0, REDEFINE);
	private ZonedDecimalData wsaaFrequencyFactor = new ZonedDecimalData(6, 0).isAPartOf(wsaaFreqFactorX, 0).setUnsigned();
	private PackedDecimalData wsaaFirstFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaFirstFactorRnd = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaSecondFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaSecondFactorRnd = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaFirstFrequencyFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaSecondFrequencyFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaMasterFrequencyFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaRefundFactor = new PackedDecimalData(16, 5);
	private String itdmrec = "ITEMREC";
	private String recoclmrec = "RECOCLMREC";
	private String racdrcorec = "RACDRCOREC";
		/* TABLES */
	private String t5449 = "T5449";
	private String t5445 = "T5445";
		/* ERRORS */
	private String e049 = "E049";
	private String r057 = "R057";
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Reassurance Cession Details Descending*/
	private RacdrcoTableDAM racdrcoIO = new RacdrcoTableDAM();
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
		/*Costing Logical for Claims & Termination*/
	private RecoclmTableDAM recoclmIO = new RecoclmTableDAM();
	private Recopstrec recopstrec = new Recopstrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5445rec t5445rec = new T5445rec();
	private T5449rec t5449rec = new T5449rec();
	private Varcom varcom = new Varcom();
	//PINNACLE-2457
	private boolean isAiaAusDirectDebit ;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit199,
		exit299,
		nextr358,
		exit359
	}

	public Rcorfnd() {
		super();
	}

public void mainline(Object... parmArray)
	{
		rcorfndrec.rcorfndRec = convertAndSetParam(rcorfndrec.rcorfndRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		try {
			mainlineStart101();
		}
		catch (GOTOException e){
		}
		finally{
			exit199();
		}
	}

protected void mainlineStart101()
	{
		isAiaAusDirectDebit = FeaConfg.isFeatureExist("2", "BTPRO029", appVars, "IT");		 		

		rcorfndrec.statuz.set(varcom.oK);
		rcorfndrec.refundFee.set(ZERO);
		rcorfndrec.refundPrem.set(ZERO);
		rcorfndrec.refundComm.set(ZERO);
		if (isNE(rcorfndrec.function,"UPDT")) {
			rcorfndrec.statuz.set(e049);
			systemError500();
		}
		initialTasks200();
		if (isLTE(racdrcoIO.getCtdate(),rcorfndrec.effdate)
		|| isEQ(t5449rec.prefbas,SPACES)) {
			goTo(GotoLabel.exit199);
		}
		calculateRefund300();
		if (isEQ(recoclmIO.getStatuz(),varcom.endp)
		&& isEQ(wsaaReallyNotFound,"Y")) {
			goTo(GotoLabel.exit199);
		}
		callRecopost400();
	}

protected void exit199()
	{
		exitProgram();
	}

protected void initialTasks200()
	{
		try {
			start201();
		}
		catch (GOTOException e){
		}
	}

protected void start201()
	{
		racdrcoIO.setParams(SPACES);
		racdrcoIO.setChdrcoy(rcorfndrec.chdrcoy);
		racdrcoIO.setChdrnum(rcorfndrec.chdrnum);
		racdrcoIO.setLife(rcorfndrec.life);
		racdrcoIO.setCoverage(rcorfndrec.coverage);
		racdrcoIO.setRider(rcorfndrec.rider);
		racdrcoIO.setPlanSuffix(rcorfndrec.planSuffix);
		racdrcoIO.setSeqno(rcorfndrec.seqno);
		racdrcoIO.setCurrfrom(rcorfndrec.currfrom);
		racdrcoIO.setFormat(racdrcorec);
		racdrcoIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, racdrcoIO);
		if (isNE(racdrcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrcoIO.getParams());
			dbError600();
		}
		wsaaCtdate.set(racdrcoIO.getCtdate());
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rcorfndrec.chdrcoy);
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(racdrcoIO.getRngmnt());
		itdmIO.setItmfrm(rcorfndrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError600();
		}
		if (isNE(racdrcoIO.getRngmnt(),itdmIO.getItemitem())
		|| isNE(rcorfndrec.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5449)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(racdrcoIO.getRngmnt());
			itdmIO.setItemcoy(rcorfndrec.chdrcoy);
			itdmIO.setItemtabl(t5449);
			syserrrec.statuz.set(r057);
			syserrrec.params.set(itdmIO.getParams());
			systemError500();
		}
		else {
			t5449rec.t5449Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5449rec.prefbas,SPACES)) {
			rcorfndrec.refundFee.set(ZERO);
			rcorfndrec.refundPrem.set(ZERO);
			rcorfndrec.refundComm.set(ZERO);
			goTo(GotoLabel.exit299);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rcorfndrec.chdrcoy);
		itdmIO.setItemtabl(t5445);
		itdmIO.setItemitem(t5449rec.prefbas);
		itdmIO.setItmfrm(rcorfndrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError600();
		}
		if (isNE(t5449rec.prefbas,itdmIO.getItemitem())
		|| isNE(rcorfndrec.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5445)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(t5449rec.prefbas);
			itdmIO.setItemcoy(rcorfndrec.chdrcoy);
			itdmIO.setItemtabl(t5445);
			syserrrec.statuz.set(r057);
			syserrrec.params.set(itdmIO.getParams());
			systemError500();
		}
		else {
			t5445rec.t5445Rec.set(itdmIO.getGenarea());
		}
	}

protected void calculateRefund300()
	{
		start301();
	}

protected void start301()
	{
		recoclmIO.setParams(SPACES);
		recoclmIO.setChdrcoy(racdrcoIO.getChdrcoy());
		recoclmIO.setChdrnum(racdrcoIO.getChdrnum());
		recoclmIO.setLife(racdrcoIO.getLife());
		recoclmIO.setCoverage(racdrcoIO.getCoverage());
		recoclmIO.setRider(racdrcoIO.getRider());
		recoclmIO.setPlanSuffix(racdrcoIO.getPlanSuffix());
		recoclmIO.setSeqno(racdrcoIO.getSeqno());
		recoclmIO.setCostdate(varcom.vrcmMaxDate);
		recoclmIO.setFormat(recoclmrec);
		recoclmIO.setFunction(varcom.begn);
		wsaaReallyNotFound = "Y";
		while ( !(isEQ(recoclmIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaReallyNotFound,"N"))) {
			loopThruRecos350();
		}

	}

protected void loopThruRecos350()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					recoLoop351();
				}
				case nextr358: {
					nextr358();
				}
				case exit359: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void recoLoop351()
	{
		SmartFileCode.execute(appVars, recoclmIO);
		if (isNE(recoclmIO.getStatuz(),varcom.oK)
		&& isNE(recoclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(recoclmIO.getParams());
			dbError600();
		}
		if (isNE(recoclmIO.getChdrcoy(),racdrcoIO.getChdrcoy())
		|| isNE(recoclmIO.getChdrnum(),racdrcoIO.getChdrnum())
		|| isNE(recoclmIO.getLife(),racdrcoIO.getLife())
		|| isNE(recoclmIO.getCoverage(),racdrcoIO.getCoverage())
		|| isNE(recoclmIO.getRider(),racdrcoIO.getRider())
		|| isNE(recoclmIO.getPlanSuffix(),racdrcoIO.getPlanSuffix())
		|| isNE(recoclmIO.getSeqno(),racdrcoIO.getSeqno())
		|| isEQ(recoclmIO.getStatuz(),varcom.endp)) {
			recoclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit359);
		}
		if (isLT(recoclmIO.getPrem(),0)
		|| isLT(recoclmIO.getCtdate(),rcorfndrec.effdate)) {
			goTo(GotoLabel.nextr358);
		}
		wsaaReallyNotFound = "N";
		datcon3rec.intDate1.set(rcorfndrec.effdate);
		datcon3rec.intDate2.set(wsaaCtdate);
		if(!isAiaAusDirectDebit) {
		datcon3rec.frequency.set(t5445rec.frequency);
		}
		else {
		datcon3rec.frequency.set(recoclmIO.getRcstfrq()); //PINNACLE-2457
		}
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError500();
		}
		wsaaFirstFactor.set(datcon3rec.freqFactor);
		if (isNE(t5445rec.rndgrqd,SPACES)) {
			wsaaFirstFactorRnd.setRounded(wsaaFirstFactor);
			wsaaFirstFrequencyFactor.set(wsaaFirstFactorRnd);
		}
		else {
			wsaaFreqFactor.set(wsaaFirstFactor);
			wsaaFirstFrequencyFactor.set(wsaaFrequencyFactor);
		}
		datcon3rec.intDate1.set(recoclmIO.getCostdate());
		datcon3rec.intDate2.set(wsaaCtdate);
		if(!isAiaAusDirectDebit) {
		datcon3rec.frequency.set(t5445rec.frequency); 
		}
		else {
		datcon3rec.frequency.set(recoclmIO.getRcstfrq()); //PINNACLE-2457
		}

		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError500();
		}
		wsaaSecondFactor.set(datcon3rec.freqFactor);
		if (isNE(t5445rec.rndgrqd,SPACES)) {
			wsaaSecondFactorRnd.setRounded(wsaaSecondFactor);
			wsaaSecondFrequencyFactor.set(wsaaSecondFactorRnd);
		}
		else {
			wsaaFreqFactor.set(wsaaSecondFactor);
			wsaaSecondFrequencyFactor.set(wsaaFrequencyFactor);
		}
		compute(wsaaMasterFrequencyFactor, 5).set(div(wsaaFirstFrequencyFactor,wsaaSecondFrequencyFactor));
		compute(wsaaRefundFactor, 5).set(mult(rcorfndrec.refund,wsaaMasterFrequencyFactor));
		compute(wsaaRefundPrem, 5).set(mult(recoclmIO.getPrem(),wsaaRefundFactor));
		compute(wsaaRefundCompay, 5).set(mult(recoclmIO.getCompay(),wsaaRefundFactor));
		compute(wsaaRefundBalance, 2).set(sub(wsaaRefundPrem,wsaaRefundCompay));
		if (isGTE(t5445rec.refundfe,wsaaRefundBalance)) {
			recopstrec.refundfe.set(ZERO);
		}
		else {
			recopstrec.refundfe.set(t5445rec.refundfe);
		}
	}

protected void nextr358()
	{
		recoclmIO.setFunction(varcom.nextr);
	}

protected void callRecopost400()
	{
		start401();
	}

protected void start401()
	{
		recopstrec.function.set("RFND");
		recopstrec.language.set(rcorfndrec.language);
		recopstrec.chdrcoy.set(recoclmIO.getChdrcoy());
		recopstrec.chdrnum.set(recoclmIO.getChdrnum());
		recopstrec.life.set(recoclmIO.getLife());
		recopstrec.coverage.set(recoclmIO.getCoverage());
		recopstrec.rider.set(recoclmIO.getRider());
		recopstrec.planSuffix.set(recoclmIO.getPlanSuffix());
		recopstrec.rasnum.set(recoclmIO.getRasnum());
		recopstrec.seqno.set(rcorfndrec.seqno);
		recopstrec.cnttype.set(rcorfndrec.cnttype);
		recopstrec.effdate.set(recoclmIO.getCostdate());
		recopstrec.ctdate.set(recoclmIO.getCtdate());
		recopstrec.costdate.set(recoclmIO.getCostdate());
		recopstrec.validflag.set(recoclmIO.getValidflag());
		recopstrec.retype.set(recoclmIO.getRetype());
		recopstrec.rngmnt.set(recoclmIO.getRngmnt());
		recopstrec.batccoy.set(rcorfndrec.batccoy);
		recopstrec.batcbrn.set(rcorfndrec.batcbrn);
		recopstrec.batcactyr.set(rcorfndrec.batcactyr);
		recopstrec.batcactmn.set(rcorfndrec.batcactmn);
		recopstrec.batctrcde.set(rcorfndrec.batctrcde);
		recopstrec.batcbatch.set(rcorfndrec.batcbatch);
		recopstrec.tranno.set(rcorfndrec.tranno);
		recopstrec.ctdate.set(recoclmIO.getCtdate());
		recopstrec.sraramt.set(recoclmIO.getSraramt());
		recopstrec.raAmount.set(recoclmIO.getRaAmount());
		recopstrec.origcurr.set(recoclmIO.getOrigcurr());
		recopstrec.prem.set(wsaaRefundPrem);
		recopstrec.compay.set(wsaaRefundCompay);
		recopstrec.rcstfrq.set(recoclmIO.getRcstfrq());
		recopstrec.taxamt.set(ZERO);
		recopstrec.recovAmt.set(ZERO);
		recopstrec.crtable.set(rcorfndrec.crtable);
		callProgram(Rfndpst.class, recopstrec.recopstRec);
		if (isNE(recopstrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(recopstrec.statuz);
			syserrrec.params.set(recopstrec.recopstRec);
			systemError500();
		}
	}

protected void systemError500()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		exit199();
	}

protected void dbError600()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		exit199();
	}
}
