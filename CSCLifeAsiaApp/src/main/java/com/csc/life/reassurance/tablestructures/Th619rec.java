package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:43
 * Description:
 * Copybook name: TH619REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th619rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th619Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData lrkclss = new FixedLengthStringData(20).isAPartOf(th619Rec, 0);
  	public FixedLengthStringData[] lrkcls = FLSArrayPartOfStructure(5, 4, lrkclss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(lrkclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData lrkcls01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData lrkcls02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData lrkcls03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData lrkcls04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData lrkcls05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData rngmnts = new FixedLengthStringData(20).isAPartOf(th619Rec, 20);
  	public FixedLengthStringData[] rngmnt = FLSArrayPartOfStructure(5, 4, rngmnts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(rngmnts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rngmnt01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData rngmnt02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData rngmnt03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData rngmnt04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData rngmnt05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(460).isAPartOf(th619Rec, 40, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th619Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th619Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}