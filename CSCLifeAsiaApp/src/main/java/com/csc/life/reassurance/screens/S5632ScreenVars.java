package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5632
 * @version 1.0 generated on 30/08/09 06:46
 * @author Quipoz
 */
public class S5632ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(142);
	public FixedLengthStringData dataFields = new FixedLengthStringData(46).isAPartOf(dataArea, 0);
	public FixedLengthStringData bankreq = DD.bankreq.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData paymentMethod = DD.paymmeth.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 46);
	public FixedLengthStringData bankreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData paymmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 70);
	public FixedLengthStringData[] bankreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] paymmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5632screenWritten = new LongData(0);
	public LongData S5632protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5632ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bankreqOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymmethOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, bankreq, paymentMethod};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, bankreqOut, paymmethOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, bankreqErr, paymmethErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5632screen.class;
		protectRecord = S5632protect.class;
	}

}
