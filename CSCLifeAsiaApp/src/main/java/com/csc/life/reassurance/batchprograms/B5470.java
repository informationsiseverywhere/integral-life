/*
 * File: B5470.java
 * Date: 29 August 2009 21:19:49
 * Author: Quipoz Limited
 * 
 * Class transformed from B5470.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.common.DD;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.reassurance.dataaccess.dao.impl.AcmvpfDAOImpl;
import com.csc.life.reassurance.dataaccess.model.B5470DTO;
import com.csc.life.reassurance.reports.R5470Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reassurer Statement Print.
*
* Overview
* ========
*
*  This program produces the Reassurer statement print for
*  those Reassurers who have earned their minimum or more -
*  RASA-MINSTA.  If they have earned sufficient to be reported
*  on then each ACMV which has been extracted via SQL using the
*  logical ACMVRAS (KEY = BATCCOY, SACSCODE, EFFDATE selecting
*  FRCDATE = 99999999 and INTEXTIND = 'E') is reported on the
*  statement print and written to the ACMX reporting file for
*  use in the subsequent program, B5471.
*
***********************************************************************
* </pre>
*/
public class B5470 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private java.sql.ResultSet sqlacmvstmCursorrs = null;
	private java.sql.PreparedStatement sqlacmvstmCursorps = null;
	private java.sql.Connection sqlacmvstmCursorconn = null;
	private R5470Report printerFile = new R5470Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(300);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5470");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private PackedDecimalData wsaaReportTotal = new PackedDecimalData(10, 2);
	private PackedDecimalData wsaaCheckTotal = new PackedDecimalData(10, 2);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private String wsaaEndAcmvras = "";
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private PackedDecimalData wsaaOrigamt = new PackedDecimalData(13, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEnough = new FixedLengthStringData(1).init("N");
	private Validator enoughMoneyEarned = new Validator(wsaaEnough, "Y");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);

	private FixedLengthStringData r5470D01 = new FixedLengthStringData(85);
	private FixedLengthStringData r5470d01O = new FixedLengthStringData(85).isAPartOf(r5470D01, 0);
	private FixedLengthStringData effcdte = new FixedLengthStringData(10).isAPartOf(r5470d01O, 0);
	private FixedLengthStringData dtldesc = new FixedLengthStringData(30).isAPartOf(r5470d01O, 10);
	private FixedLengthStringData chdrno = new FixedLengthStringData(8).isAPartOf(r5470d01O, 40);
	private ZonedDecimalData orgamnt = new ZonedDecimalData(17, 2).isAPartOf(r5470d01O, 48);
	private FixedLengthStringData origcur = new FixedLengthStringData(3).isAPartOf(r5470d01O, 65);
	private ZonedDecimalData newamnt = new ZonedDecimalData(17, 2).isAPartOf(r5470d01O, 68);

	private FixedLengthStringData r5470T01 = new FixedLengthStringData(18);
	private FixedLengthStringData r5470t01O = new FixedLengthStringData(18).isAPartOf(r5470T01, 0);
	private ZonedDecimalData totcbal = new ZonedDecimalData(18, 2).isAPartOf(r5470t01O, 0);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler3, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler3, 8);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private R5470H01Inner r5470H01Inner = new R5470H01Inner();
	
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAOImpl.class);
	private AcmvpfDAO acmvDAO = getApplicationContext().getBean("acmvDAO", AcmvpfDAOImpl.class);
	private Map<String,Descpf> t3629Map = null;
	private Iterator<B5470DTO> iterator;
	private B5470DTO sqlacmvpfInner = null;
	private List<B5470DTO> acmvDtoList = null;
	private List<B5470DTO> acmvDtoTotalList = new ArrayList<B5470DTO>();
    private int minRecord = 1;
    private int incrRange;
    private int bulkCount = 0;
    private boolean isEnd = false;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfCursor2080, 
		exit2090
	}

	public B5470() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		wsaaRldgacct.set(SPACES);
		wsaaReportTotal.set(ZERO);
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaSacscode.set(bprdIO.getSystemParam02());
		reportHeaderInit1100();
		
		
		//ILIFE-2525 Life Batch Performance Improvement by liwei
        if (bprdIO.systemParam01.isNumeric()) {
            if (bprdIO.systemParam01.toInt() > 0) {
                incrRange = bprdIO.systemParam01.toInt();
            } else {
                incrRange = bprdIO.cyclesPerCommit.toInt();
            }
        } else {
            incrRange = bprdIO.cyclesPerCommit.toInt();
        } 
		acmvDtoList = acmvDAO.searchAcmvByRasNumResult(wsaaCompany.toString(), wsaaSacscode.toString(),wsaaEffdate.toString(),minRecord,minRecord + incrRange);
		t3629Map = descDAO.getItems("IT", bsprIO.getCompany().toString(), "T3629", bsscIO.getLanguage().toString());
        if (acmvDtoList != null && acmvDtoList.size() > 0) {
            iterator = acmvDtoList.iterator();
        } else {
            wsspEdterror.set(varcom.endp);
        }
	}

protected void reportHeaderInit1100()
	{
		initialise1100();
	}

protected void initialise1100()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			r5470H01Inner.companynm.set(SPACES);
		}
		else {
			r5470H01Inner.companynm.set(descIO.getLongdesc());
		}
		r5470H01Inner.company.set(bsprIO.getCompany());
		/* Get statement date.*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		callDatcon16000();
		r5470H01Inner.stdat.set(datcon1rec.extDate);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfCursor2080: 
					endOfCursor2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
	//ILIFE-2525 Life Batch Performance Improvement by liwei
	    if (iterator.hasNext()) {
	        sqlacmvpfInner = iterator.next();
	        acmvDtoTotalList.add(sqlacmvpfInner);
	    } else {
	        minRecord += incrRange;
	        acmvDtoList = acmvDAO.searchAcmvByRasNumResult(wsaaCompany.toString(), wsaaSacscode.toString(),wsaaEffdate.toString(),minRecord,minRecord + incrRange);
	        if (acmvDtoList != null && acmvDtoList.size() > 0) {
	            iterator = acmvDtoList.iterator();
	            if (iterator.hasNext()) {
	    	        sqlacmvpfInner = iterator.next();
	    	        acmvDtoTotalList.add(sqlacmvpfInner);
	    	    }
	            else{
	            	isEnd = true;
	            	wsspEdterror.set(varcom.endp);
	            }
	        } else {
	        	isEnd = true;
	        	wsspEdterror.set(varcom.endp);
	        }
	    }
	  //ILIFE-2525 Life Batch Performance Improvement by liwei

		if (isEQ(sqlacmvpfInner.getSqlRldgacct(), wsaaRldgacct) && !isEnd) {
			goTo(GotoLabel.exit2090);
		}
		else {
			if (isNE(wsaaRldgacct,SPACES)
			&& enoughMoneyEarned.isTrue()) {
				printTotals2600();
			}
		}
		wsaaRldgacct.set(sqlacmvpfInner.getSqlRldgacct());
		checkEnoughEarned2200();
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		if (enoughMoneyEarned.isTrue()) {
			printTotals2600();
		}
		wsspEdterror.set(varcom.endp);
	}


protected void checkEnoughEarned2200()
	{
		edit2200();
	}

protected void edit2200()
	{
		wsaaEnough.set("N");
		wsaaEndAcmvras = "N";
		readAcmvras2201();
		if (isLTE(wsaaCheckTotal,ZERO)) {
			wsaaEnough.set("Y");
		}
	}

protected void readAcmvras2201()
	{
			para2201();
		}

protected void para2201()
	{

		if (isEQ(sqlacmvpfInner.getSqlGlsign(),"+")) {
			compute(wsaaOrigamt, 2).set(mult(sqlacmvpfInner.getSqlOrigamt(),-1));
		}
		else {
			wsaaOrigamt.set(sqlacmvpfInner.getSqlOrigamt());
		}
		if (isEQ(sqlacmvpfInner.getSqlOrigcurr(),sqlacmvpfInner.getSqlCurrcode())) {
			wsaaCheckTotal.add(wsaaOrigamt);
		}
		else {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(wsaaOrigamt);
			callXcvrt5000();
			wsaaCheckTotal.add(conlinkrec.amountOut);
		}
		/* Check the accumulated amount against the minimum statement*/
		/* amount.*/
		if (isGT(wsaaCheckTotal,0)) {
			if (isGT(sqlacmvpfInner.getSqlMinsta(),wsaaCheckTotal)) {
				wsaaEnough.set("N");
			}
			else {
				wsaaEnough.set("Y");
				wsaaEndAcmvras = "Y";
				return ;
			}
		}
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		if (!enoughMoneyEarned.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
		wsspEdterror.set(varcom.oK);
		if (isEQ(sqlacmvpfInner.getSqlGlsign(), "+")) {
			compute(wsaaOrigamt, 2).set(mult(sqlacmvpfInner.getSqlOrigamt(), -1));
		}
		else {
			wsaaOrigamt.set(sqlacmvpfInner.getSqlOrigamt());
		}
		if (isEQ(sqlacmvpfInner.getSqlOrigcurr(), sqlacmvpfInner.getSqlCurrcode())) {
			wsaaReportTotal.add(wsaaOrigamt);
		}
		else {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(wsaaOrigamt);
			callXcvrt5000();
			wsaaReportTotal.add(conlinkrec.amountOut);
		}
	}

protected void printTotals2600()
	{
		/*START*/
		printerRec.set(SPACES);
		totcbal.set(wsaaReportTotal);
		printerFile.printR5470t01(r5470T01);
		wsaaOverflow.set("Y");
		wsaaReportTotal.set(ZERO);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(sqlacmvpfInner.getSqlEffdate());
		callDatcon16000();
		effcdte.set(datcon1rec.extDate);
		dtldesc.set(sqlacmvpfInner.getSqlTrandesc());
		chdrno.set(sqlacmvpfInner.getSqlRdocnum());
		orgamnt.set(wsaaOrigamt);
		origcur.set(sqlacmvpfInner.getSqlOrigcurr());
		if (isEQ(sqlacmvpfInner.getSqlOrigcurr(), sqlacmvpfInner.getSqlCurrcode())) {
			newamnt.set(wsaaOrigamt);
		}
		else {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(wsaaOrigamt);
			callXcvrt5000();
			newamnt.set(conlinkrec.amountOut);
		}
		if (newPageReq.isTrue()) {
			pageOverflow3100();
		}
		printerRec.set(SPACES);
		printerFile.printR5470d01(r5470D01);

	}

protected void pageOverflow3100()
	{
		para3100();
	}

protected void para3100()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		printerRec.set(SPACES);
		r5470H01Inner.rasnum.set(sqlacmvpfInner.getSqlRasnum());
	//// ILIFE-2525 Life Batch Performance Improvement by liwei start

		plainname();
		r5470H01Inner.cltname.set(wsspLongconfname);
//		r5470H01Inner.forattn.set(sqlacmvpfInner.getForAttOf());
		r5470H01Inner.addr1.set(sqlacmvpfInner.getSqlCltaddr01());
		r5470H01Inner.addr2.set(sqlacmvpfInner.getSqlCltaddr02());
		r5470H01Inner.addr3.set(sqlacmvpfInner.getSqlCltaddr03());
		r5470H01Inner.addr4.set(sqlacmvpfInner.getSqlCltaddr04());
		r5470H01Inner.pcode.set(sqlacmvpfInner.getSqlCltpcode());
		/* Get currency description*/
		r5470H01Inner.statcurr.set(sqlacmvpfInner.getSqlCurrcode());
		
		if(t3629Map != null){
			if(t3629Map.get(sqlacmvpfInner.getSqlCurrcode()) != null){/* IJTI-1523 */
				r5470H01Inner.currdesc.set(t3629Map.get(sqlacmvpfInner.getSqlCurrcode()).getLongdesc());/* IJTI-1523 */
			}else{
				r5470H01Inner.currdesc.set(SPACES);
			}
		}else{
			r5470H01Inner.currdesc.set(SPACES);
		}
		//// ILIFE-2525 Life Batch Performance Improvement by liwei end
		/* Print heading line*/
		printerFile.printR5470h01(r5470H01Inner.r5470H01);
		wsaaOverflow.set("N");
	}

protected void writeAcmx3200()
	{

	}
protected void commit3500()
	{
		/*COMMIT*/
		acmvDAO.insertAcmxBulk(acmvDtoTotalList);
		if(acmvDtoTotalList != null){
			contotrec.totno.set(ct02);
			contotrec.totval.set(acmvDtoTotalList.size());
			callContot001();
			contotrec.totno.set(ct03);
			contotrec.totval.set(acmvDtoTotalList.size());
			callContot001();
			acmvDtoTotalList.clear();//IJTI-320
		}
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
	    if (acmvDtoTotalList != null) {
	    	acmvDtoTotalList.clear();
	    }
	    if (acmvDtoList != null) {
	    	acmvDtoList.clear();
	    }
	    t3629Map.clear();
		printerFile.close();
		getAppVars().freeDBConnectionIgnoreErr(sqlacmvstmCursorconn, sqlacmvstmCursorps, sqlacmvstmCursorrs);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void callXcvrt5000()
	{
		para5000();
	}

protected void para5000()
	{
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.currIn.set(sqlacmvpfInner.getSqlOrigcurr());
		conlinkrec.currOut.set(sqlacmvpfInner.getSqlCurrcode());
		conlinkrec.amountIn.set(wsaaOrigamt);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/*    MOVE CLNK-AMOUNT-OUT    TO ZRDP-AMOUNT-IN.                   */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT    TO CLNK-AMOUNT-OUT.                  */
		if (isNE(conlinkrec.amountOut, ZERO)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void callDatcon16000()
	{
		/*PARA*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(sqlacmvpfInner.getSqlClttype(),"C")) {
			corpname();
			return ;
		}
		wsspLongconfname.set(sqlacmvpfInner.getSqlSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(sqlacmvpfInner.getSqlClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(sqlacmvpfInner.getSqlGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(sqlacmvpfInner.getSqlSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(sqlacmvpfInner.getSqlGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
		}
		else {
			wsspLongconfname.set(sqlacmvpfInner.getSqlSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(sqlacmvpfInner.getSqlClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(sqlacmvpfInner.getSqlEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(sqlacmvpfInner.getSqlSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(sqlacmvpfInner.getSqlSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(sqlacmvpfInner.getSqlGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(sqlacmvpfInner.getSqlSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(sqlacmvpfInner.getSqlGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(sqlacmvpfInner.getSqlSurname(), "  ");
		stringVariable2.setStringInto(wsspLongconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlacmvpfInner.getSqlLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(sqlacmvpfInner.getSqlLgivname(), "  ");
		stringVariable1.setStringInto(wsspLongconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sqlacmvpfInner.getSqlCurrcode());
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrasrec = new FixedLengthStringData(10).init("ACMVRASREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData rasarec = new FixedLengthStringData(10).init("RASAREC");
	private FixedLengthStringData acmxrec = new FixedLengthStringData(10).init("ACMXREC");
}
/*
 * Class transformed  from Data Structure R5470-H01--INNER
 */
private static final class R5470H01Inner { 

	private FixedLengthStringData r5470H01 = new FixedLengthStringData(169+DD.cltaddr.length*4); //Starts ILIFE-3212
	private FixedLengthStringData r5470h01O = new FixedLengthStringData(169+DD.cltaddr.length*4).isAPartOf(r5470H01, 0);
	private FixedLengthStringData stdat = new FixedLengthStringData(10).isAPartOf(r5470h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5470h01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5470h01O, 11);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5470h01O, 41);
	private FixedLengthStringData forattn = new FixedLengthStringData(30).isAPartOf(r5470h01O, 49);
	private FixedLengthStringData statcurr = new FixedLengthStringData(3).isAPartOf(r5470h01O, 79);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(r5470h01O, 82);
	private FixedLengthStringData cltname = new FixedLengthStringData(47).isAPartOf(r5470h01O, 112);
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5470h01O, 159);
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5470h01O, 159+DD.cltaddr.length*1);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5470h01O, 159+DD.cltaddr.length*2);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5470h01O, 159+DD.cltaddr.length*3);
	private FixedLengthStringData pcode = new FixedLengthStringData(10).isAPartOf(r5470h01O, 159+DD.cltaddr.length*4); //End ILIFE-3212
}

}
