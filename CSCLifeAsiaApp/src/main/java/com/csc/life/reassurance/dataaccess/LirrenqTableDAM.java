package com.csc.life.reassurance.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LirrenqTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:51
 * Class transformed from LIRRENQ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LirrenqTableDAM extends LirrpfTableDAM {

	public LirrenqTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LIRRENQ");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", CLNTPFX"
		             + ", CLNTCOY"
		             + ", CLNTNUM"
		             + ", RASNUM"
		             + ", RNGMNT"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CLNTPFX, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "COMPANY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "RASNUM, " +
		            "RNGMNT, " +
		            "CURRENCY, " +
		            "RAAMOUNT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "CLNTPFX ASC, " +
		            "CLNTCOY ASC, " +
		            "CLNTNUM ASC, " +
		            "RASNUM ASC, " +
		            "RNGMNT ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "CLNTPFX DESC, " +
		            "CLNTCOY DESC, " +
		            "CLNTNUM DESC, " +
		            "RASNUM DESC, " +
		            "RNGMNT DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               clntpfx,
                               clntcoy,
                               clntnum,
                               company,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               currfrom,
                               currto,
                               validflag,
                               tranno,
                               rasnum,
                               rngmnt,
                               currency,
                               raAmount,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(32);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getCompany().toInternal()
					+ getClntpfx().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getRasnum().toInternal()
					+ getRngmnt().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, clntpfx);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, rasnum);
			what = ExternalData.chop(what, rngmnt);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller14 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller15 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(clntpfx.toInternal());
	nonKeyFiller2.setInternal(clntcoy.toInternal());
	nonKeyFiller3.setInternal(clntnum.toInternal());
	nonKeyFiller4.setInternal(company.toInternal());
	nonKeyFiller5.setInternal(chdrnum.toInternal());
	nonKeyFiller14.setInternal(rasnum.toInternal());
	nonKeyFiller15.setInternal(rngmnt.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(113);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ nonKeyFiller14.toInternal()
					+ nonKeyFiller15.toInternal()
					+ getCurrency().toInternal()
					+ getRaAmount().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nonKeyFiller14);
			what = ExternalData.chop(what, nonKeyFiller15);
			what = ExternalData.chop(what, currency);
			what = ExternalData.chop(what, raAmount);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(Object what) {
		clntpfx.set(what);
	}
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}
	public FixedLengthStringData getRasnum() {
		return rasnum;
	}
	public void setRasnum(Object what) {
		rasnum.set(what);
	}
	public FixedLengthStringData getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(Object what) {
		rngmnt.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getCurrency() {
		return currency;
	}
	public void setCurrency(Object what) {
		currency.set(what);
	}	
	public PackedDecimalData getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(Object what) {
		setRaAmount(what, false);
	}
	public void setRaAmount(Object what, boolean rounded) {
		if (rounded)
			raAmount.setRounded(what);
		else
			raAmount.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		clntpfx.clear();
		clntcoy.clear();
		clntnum.clear();
		rasnum.clear();
		rngmnt.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		currfrom.clear();
		currto.clear();
		validflag.clear();
		tranno.clear();
		nonKeyFiller14.clear();
		nonKeyFiller15.clear();
		currency.clear();
		raAmount.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}