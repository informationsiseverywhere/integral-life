package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5460
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5460ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(65).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,12);
	public ZonedDecimalData ssreasf = DD.ssreasf.copyToZonedDecimal().isAPartOf(dataFields,14);
	public ZonedDecimalData ssreast = DD.ssreast.copyToZonedDecimal().isAPartOf(dataFields,31);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,48);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 65);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ssreasfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ssreastErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 93);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ssreasfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ssreastOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(161);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(47).isAPartOf(subfileArea, 0);
	public ZonedDecimalData cmdate = DD.cmdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData ctdate = DD.ctdate.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData ovrdind = DD.ovrdind.copy().isAPartOf(subfileFields,16);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(subfileFields,17);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(subfileFields,34);
	public FixedLengthStringData retype = DD.retype.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(subfileFields,43);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 47);
	public FixedLengthStringData cmdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData ctdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData ovrdindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData retypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 75);
	public FixedLengthStringData[] cmdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] ctdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] ovrdindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] retypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 159);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData cmdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ctdateDisp = new FixedLengthStringData(10);

	public LongData S5460screensflWritten = new LongData(0);
	public LongData S5460screenctlWritten = new LongData(0);
	public LongData S5460screenWritten = new LongData(0);
	public LongData S5460protectWritten = new LongData(0);
	public GeneralTable s5460screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5460screensfl;
	}

	public S5460ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {cmdate, rasnum, rngmnt, raAmount, ovrdind, retype, ctdate};
		screenSflOutFields = new BaseData[][] {cmdateOut, rasnumOut, rngmntOut, raamountOut, ovrdindOut, retypeOut, ctdateOut};
		screenSflErrFields = new BaseData[] {cmdateErr, rasnumErr, rngmntErr, raamountErr, ovrdindErr, retypeErr, ctdateErr};
		screenSflDateFields = new BaseData[] {cmdate, ctdate};
		screenSflDateErrFields = new BaseData[] {cmdateErr, ctdateErr};
		screenSflDateDispFields = new BaseData[] {cmdateDisp, ctdateDisp};

		screenFields = new BaseData[] {chdrnum, sumins, life, coverage, rider, ssreast, ssreasf};
		screenOutFields = new BaseData[][] {chdrnumOut, suminsOut, lifeOut, coverageOut, riderOut, ssreastOut, ssreasfOut};
		screenErrFields = new BaseData[] {chdrnumErr, suminsErr, lifeErr, coverageErr, riderErr, ssreastErr, ssreasfErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5460screen.class;
		screenSflRecord = S5460screensfl.class;
		screenCtlRecord = S5460screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5460protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5460screenctl.lrec.pageSubfile);
	}
}
