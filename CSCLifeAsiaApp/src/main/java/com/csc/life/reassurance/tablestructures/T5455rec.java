package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:19
 * Description:
 * Copybook name: T5455REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5455rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5455Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cesnind = new FixedLengthStringData(1).isAPartOf(t5455Rec, 0);
  	public FixedLengthStringData stmtind = new FixedLengthStringData(1).isAPartOf(t5455Rec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(t5455Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5455Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5455Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}