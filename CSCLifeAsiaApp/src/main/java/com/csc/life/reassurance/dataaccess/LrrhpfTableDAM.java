package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LrrhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:46
 * Class transformed from LRRHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LrrhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 123;
	public FixedLengthStringData lrrhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData lrrhpfRecord = lrrhrec;
	
	public FixedLengthStringData clntpfx = DD.clntpfx.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData clntcoy = DD.clntcoy.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(lrrhrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(lrrhrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(lrrhrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(lrrhrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(lrrhrec);
	public PackedDecimalData ssretn = DD.ssretn.copy().isAPartOf(lrrhrec);
	public PackedDecimalData ssreast = DD.ssreast.copy().isAPartOf(lrrhrec);
	public PackedDecimalData ssreasf = DD.ssreasf.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(lrrhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(lrrhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LrrhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LrrhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LrrhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LrrhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LrrhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LrrhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LrrhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LRRHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CLNTPFX, " +
							"CLNTCOY, " +
							"CLNTNUM, " +
							"COMPANY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"LRKCLS, " +
							"CURRFROM, " +
							"CURRTO, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"CURRENCY, " +
							"SSRETN, " +
							"SSREAST, " +
							"SSREASF, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     clntpfx,
                                     clntcoy,
                                     clntnum,
                                     company,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     lrkcls,
                                     currfrom,
                                     currto,
                                     validflag,
                                     tranno,
                                     currency,
                                     ssretn,
                                     ssreast,
                                     ssreasf,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		clntpfx.clear();
  		clntcoy.clear();
  		clntnum.clear();
  		company.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		lrkcls.clear();
  		currfrom.clear();
  		currto.clear();
  		validflag.clear();
  		tranno.clear();
  		currency.clear();
  		ssretn.clear();
  		ssreast.clear();
  		ssreasf.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLrrhrec() {
  		return lrrhrec;
	}

	public FixedLengthStringData getLrrhpfRecord() {
  		return lrrhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLrrhrec(what);
	}

	public void setLrrhrec(Object what) {
  		this.lrrhrec.set(what);
	}

	public void setLrrhpfRecord(Object what) {
  		this.lrrhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(lrrhrec.getLength());
		result.set(lrrhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}