/*
 * File: P6262.java
 * Date: 30 August 2009 0:41:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P6262.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.RactlnbTableDAM;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.CovtrasTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S6262ScreenVars;
import com.csc.life.reassurance.tablestructures.T6662rec;
import com.csc.life.terminationclaims.dataaccess.AnntTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*              " REASSURANCE "
*               ~~~~~~~~~~~~~
*
*      P6262 - New Business Reassurance.
*
*     Overview.
*     ~~~~~~~~~
*
*  This program handles Reassurance processing from within
*  New Business.
*  Reassurance Coverage details will be defined using coverage
*  codes similar to the ordinary Coverage/Rider codes used to
*  define LIFE contracts. Each one of these will be assigned to
*  Coverages or Riders that already exist on the contract and
*  will relate to the selected component on a plan-wide basis,
*  i.e. if a Reassurance Code is assigned to Coverage #1 on a
*  plan of 10 policies it will apply to each Coverage #1 on each
*  of the 10 policies. Therefore as Reassurance operates on a
*  Sum Assured basis the amount to be reassured will be a
*  fraction of the total of all the Sums Assured on all 10 of
*  the occurrences of Coverage #1.
*
*  When reassurance is specified for any component on a
*  contract the system will create one Reassurance detail
*  record on RACT for each component for which reassurance
*  has been specified.
*
*  No extra processing is required at the issue stage as the
*  Reassurance records, unlike the coverage records, will not
*  be written to transaction records for later conversion but
*  will be written directly to the correct data-sets.
*
*  Initialise.
*
*  The details of the contract being processed will have been
*  stored in the CHDRLNB I/O module. Use the RETRV
*  function to obtain the contract header details.
*  The key of the Coverage/Rider with which the reassurance
*  details will be associated will be held in the COVTLNB I/O
*  module. However this will not hold the details of the newly
*  created COVT record which will have been added to the
*  COVT physical file by the previous function via another
*  I/O module depending on which generic type it was, ie.
*  COVTUNL for Unit Linked components or COVTTRM for
*  term based components. Therefore the COVT file will have
*  to be read and a different logical view - COVTRAS - will
*  be used for this purpose.
*
*  Perform a RETRV on COVTLNB. This will provide a key of
*  Company, Contract, Life, Coverage and Rider.
*  Obtain the main Life details by using the Company,
*  Contract and Life returned from COVTLNB plus a Joint Life
*  of '00' to perform a READR on LIFELNB. Store the main
*  Life's age and sex and the Mortality Class and Effective
*  date.
*
*  Use the Client Number from LIFELNB,
*  (LIFELNB-LIFCNUM), to do a READR on CLTS to obtain
*  the main life details. Format the name for display using the
*  routine CONFNAME.
*  Use the same LIFELNB key except with a Joint Life set to
*  '01' to READR on LIFELNB again in order to retrieve the
*  Joint Life details if they exist.
*  If found then use the Client number on that record to
*  obtain the Joint Life details for display as for the main lif
*  above. Also store the Joint Life's age and sex.
*  The total sum assured for the selected component must be
*  calculated and displayed. This is carried out as follows:
*       Use the key obtained from COVTLNB to perform a
*       BEGN on COVTRAS. If end of file or no matching
*       record is found then report a database error and
*       cease processing. Otherwise take the value of the sum
*       assured and store it in Working Storage. Then
*       continue to perform NEXTR functions on COVTRAS
*       until end of file or the key changes. For each
*       matching COVTRAS record accumulate the sum assured
*       in the Working Storage field. This final sum is output
*       as 'Total S/A for Component'.
*        
*  Use the Contract Company, Contract Number, Life,
*  Coverage and Rider with RASNUM and RATYPE set to
*  spaces to perform a BEGN on the Reassurance Type file
*  RACT. If no record is found then this is the very first
*  time that Reassurance has been requested for this
*  component so all the reassurance details will be displayed
*  blank for input.
*  If a matching record is found then proceed as follows:
*       Display the Reassurance Account Number, RASNUM,
*       and use this to read RASA, the Reassurance Account
*       master file. Use the client number from there to read
*       CLTS and then use CONFNAME to format the client's
*       name for display.
*        
*       Display the other RACT details looking up descriptions
*       where necessary.
*        
*  Store the screen details as they have been displayed for
*  later comparison to see if any details have actually been
*  changed.
*  Check WSSP-FLAG. If 'I' (Enquiry  mode), protect all input
*  capable fields.
*        
*  Validation.
*        
*  Process the screen using 'S6262IO'.
*  If 'KILL' has been requested or the program is in enquiry
*  mode skip all validation.
*  Validate the screen according to the rules defined by the
*  field help. For all fields which have descriptions, look them
*  up again. For the calculation of instalment premium to work
*  all the fields must first be validated and be correct.
*  The user may remove Reassurance from the component by
*  blanking out both the Reassurance Account Number and
*  Reassurance Type. Therefore allow spaces in both fields or
*  in neither of them. If the user has opted to blank out both
*  fields skip the remainder of the validation.
*  The Premium Calculation subroutine to be called will vary
*  depending on the appropriate Premium Calculation field on
*  T5687 calculated in the following way:
*       If this is a Single Life only, (the Joint Life record
*       was not found above), then use the Single Life Method
*       from T5687 - T5687-PREMMETH.
*        
*       If this is a Joint Life case, (the Joint Life record was
*       found above), and the 'Joint Life Allowed' indicator,
*       (S5687-JLIFE-PRESENT), is space then use the Joint
*       Life method from T5687 - T5687-JL-PREM-METH.
*        
*       If this is a Joint Life case, (the Joint Life record was
*       found above), and the 'Joint Life Allowed' indicator,
*       (S5687-JLIFE-PRESENT), is not space then use the
*       Single Life method from T5687 - T5687-PREMEMTH.
*        
*  If the appropriate one of these is blank then no premium
*  calculation is required so do not call any subroutine. If
*  there is an entry in this field then use this in the key to
*  access T5675. Use the Premium Calculation Subroutine name
*  from the extra data screen on T5675 as the subroutine to
*  call when calculating the premium.
*  Obtain the general Coverage/Rider details including
*  Premium Calculation method from T5687 using ITDMIO with a
*  function of READS. The key is SMTP-ITEM, Company,
*  'T5687' and the Reassurance Type - RASTYPE.
*  Obtain the long description from DESC using DESCIO with a
*  function of READS.
*  Obtain the Term Component Edit rules from T5608 using
*  ITDMIO. This table will be accessed as follows:
*       Use a key of SMTP-ITEM, Company, 'T5671' and
*       Transaction Code concatenated with RASTYPE to read
*       T5671 - Generic Component Control table.
*        
*       This contains a list of up to 4 programs with
*       associated AT module names and Validation Item Codes.
*       Match the current program name against the list and
*       select the associated Validation Item Code.
*        
*       Use this to construct a key of SMTP-ITEM, Company,
*       'T5608' and the Validation Item Code concatenated with
*       the Contract Currency Code to read T5608 through
*       ITDMIO.
*  Use the RATYPE to read T6662. If no matching record is
*  found then this coverage code may not be used for
*  Reassurance. If the record is found then check that the
*  coverage code to which it is being applied is held on the
*  table of associated coverage codes. If it is not inform the
*  user that the Reassurance Type is not valid for the
*  component.
*  Check that the Check Sum Assured entered is less than or
*  equal to the total sum assured for the component. It must
*  also be within the maximum and minimum values on T5608.
*  Premium Calculation.
*  The premium amount is required on all products and all
*  validating must be successfully completed before it is
*  calculated. If there is no premium method defined (i.e. the
*  relevant code was blank), the premium amount must be
*  entered. Otherwise entry by the user is optional but the
*  system will always calculate it and overide it if the entered
*  value falls outside an aceptable variation.
*  To calculate it call the relevant calculation subroutine from
*  T5675 worked out above with the following parameters set:
*
*       CPRM-FUNCTION       - 'CALC'
*       CPRM-CHDR-CHDRCOY   - Contract Company
*       CPRM-CHDR-CHDRNUM   - Contract Header Number
*       CPRM-LIFE-LIFE      - COVTRAS-LIFE
*       CPRM-COVR-COVERAGE  - COVTRAS-COVERAGE
*       CPRM-COVR-RIDER     - COVTRAS-RIDER
*       CPRM-PLNSFX         - zero
*       CPRM-BILLFREQ       - CHDRLNB-BILLFREQ
*       CPRM-MOP            - CHDRLNB-BILLCHNL
*       CPRM-LIFE-JLIFE     - '01' if COVTRAS-JLIFE = '01'
*                             else set it to '00'
*       CPRM-CRTABLE        - RASTYPE
*       CPRM-MORTCLS        - COVTRAS-MORTCLS
*       CPRM-CURRCODE       - CHDRLNB-CNTCURR
*       CPRM-EFFECTDT       - Contract Original Commencement
*                             Date - CHDRLNB-OCCDATE
*       CPRM-TERMDATE       - COVTRAS-PREM-CESS-DATE
*       CPRM-SUMIN          - S6262-RA-AMOUNT
*       CPRM-CALC-PREM      - S6262-INST-PREM
*       CPRM-RATINGDATE     - Contract Original Commencement
*                             Date - CHDRLNB-OCCDATE
*       CPRM-RE-RATE-DATE   - Contract Original Commencement
*                             Date - CHDRLNB-OCCDATE
*       CPRM-LAGE           - age of main Life
*       CPRM-JLAGE          - age of Joint Life
*       CPRM-LSEX           - sex of main Life
*       CPRM-JLSEX          - sex of Joint Life
*       CPRM-DURATION       - calculated using DATCON3 with the
*                             following parameters:
*
*            DTC3-INT-DATE-1     -    CHDRLNB-OCCDATE
*            DTC3-INT-DATE-2     -    COVTRAS-PREM-CESS-DATE
*            DTC3-FREQUENCY      -    FRQ-YRLY.
*             
*            After the call to DATCON3 add 0.99999 to the
*            result: DTC3-FREQ-FACTOR, and place this value in
*            CPRM-DURATION.
*             
*  On return from the premium calculation subroutine there
*  may be an error message associated with the premium itself.
*  If so use this error message and highlight the premium
*  field. Otherwise place the returned premium on the screen.
*  If 'CALC' was requested re-display the screen.
*
*  Updating.
*
*  If the 'KILL' function key was pressed or if in enquiry
*  mode or if no changes have been made skip the updating.
*  Records may be added, modified or deleted. If a RACT
*  record existed and the user has changed the RASNUM or
*  RATYPE then the existing RACT record must be deleted
*  and a new one created.
*  Updating will be as follows, for the RACT record if the
*  frequency is '00' then place the premium in RACTLNB-SINGP
*  and set RACTLNB-INSTPREM to zero, otherwise place the value
*  in RACTLNB-INSTPREM and set RACTLNB-SINGP to sero.
*
*  Add Reassurance.
*
*  If there was no RACT record originally and an RATYPE
*  code has been entered write a RACT record.
*
*  Modify Reassurance.
*
*  If there was a RACT record originally but any of the
*  details have been altered then delete the original RACT
*  record and write the new RACT record.
*  If there was a RACT record originally and the RASNUM and
*  RATYPE are still the same but other RACT details have
*  been altered then rewrite the RACT record using a function
*  of UPDAT.
*
*  Delete Reassurance.
*
*  If there was a RACT record originally and the RASNUM and
*  RATYPE have been blanked out then delete the original
*  RACT record.
*
*  Next Program.
*
*  Add 1 to the program pointer and exit.
*
**************************************************************** *
* </pre>
*/
public class P6262 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6262");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* WSAA-MAIN-LIFE-DETAILS */
	private PackedDecimalData wsaaAge = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaT5608Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5608Item1 = new FixedLengthStringData(5).isAPartOf(wsaaT5608Item, 0);
	private FixedLengthStringData wsaaT5608Item2 = new FixedLengthStringData(3).isAPartOf(wsaaT5608Item, 5);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trcode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Rastype = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
		/* WSBB-MAIN-LIFE-DETAILS */
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData screenDataArea = new FixedLengthStringData(46);
		/* SCREEN-DATA-FIELDS */
	private ZonedDecimalData screenInstPrem = new ZonedDecimalData(17, 2).isAPartOf(screenDataArea, 0);
	private FixedLengthStringData screenRapayfrq = new FixedLengthStringData(2).isAPartOf(screenDataArea, 17);
	private FixedLengthStringData screenRasnum = new FixedLengthStringData(8).isAPartOf(screenDataArea, 19);
	private FixedLengthStringData screenRatype = new FixedLengthStringData(4).isAPartOf(screenDataArea, 27);
	private ZonedDecimalData screenRaAmount = new ZonedDecimalData(15, 0).isAPartOf(screenDataArea, 31);
		/* WSAA-THIS-AND-THAT */
	private PackedDecimalData wsaaSumAssuredAccum = new PackedDecimalData(17, 0);
	private FixedLengthStringData wsaaSubroutineName = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaValItemCode = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaPremMethInd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaRcesdte = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaStoredCovtras = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCovtrasSex = new FixedLengthStringData(1).isAPartOf(wsaaStoredCovtras, 0);
	private PackedDecimalData wsaaCovtrasCrrcd = new PackedDecimalData(8, 0).isAPartOf(wsaaStoredCovtras, 1);
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5608 = "T5608";
	private static final String t5671 = "T5671";
	private static final String t5675 = "T5675";
	private static final String t5687 = "T5687";
	private static final String t6662 = "T6662";
		/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String cltsrec = "CLTSREC";
	private static final String covtlnbrec = "COVTLNBREC";
	private static final String covtrasrec = "COVTRASREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String ractlnbrec = "RACTLNBREC";
	private static final String rasarec = "RASAREC";
	private AnntTableDAM anntIO = new AnntTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovtrasTableDAM covtrasIO = new CovtrasTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private RactlnbTableDAM ractlnbIO = new RactlnbTableDAM();
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Premiumrec premiumrec = new Premiumrec();
	private T5608rec t5608rec = new T5608rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T6662rec t6662rec = new T6662rec();
	private Wssplife wssplife = new Wssplife();
	private S6262ScreenVars sv = ScreenProgram.getScreenVars( S6262ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		afterJlife1010, 
		afterRact1030, 
		exit2090, 
		noPremCalcSubr2220, 
		exit2299, 
		writeOrRewriteRact3155
	}

	public P6262() {
		super();
		screenVars = sv;
		new ScreenModel("S6262", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
				}
				case afterJlife1010: {
					afterJlife1010();
					displayRact1020();
				}
				case afterRact1030: {
					afterRact1030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		sv.dataArea.set(SPACES);
		datcon3rec.freqFactor.set(ZERO);
		datcon3rec.intDate1.set(ZERO);
		datcon3rec.intDate2.set(ZERO);
		itdmIO.setItmfrm(ZERO);
		t5608rec.sumInsMax.set(ZERO);
		t5608rec.sumInsMin.set(ZERO);
		wsbbAnbAtCcd.set(ZERO);
		wsaaSumAssuredAccum.set(ZERO);
		wsaaPremMethInd.set(ZERO);
		sv.raAmount.set(ZERO);
		sv.instPrem.set(ZERO);
		screenDataArea.set(SPACES);
		sv.ratype.set(SPACES);
		screenInstPrem.set(ZERO);
		screenRaAmount.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		covtrasIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrasIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrasIO.setLife(covtlnbIO.getLife());
		covtrasIO.setCoverage(covtlnbIO.getCoverage());
		covtrasIO.setRider(covtlnbIO.getRider());
		wsaaSumAssuredAccum.set(0);
		covtrasIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtrasIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		covtrasIO.setFormat(covtrasrec);
		SmartFileCode.execute(appVars, covtrasIO);
		if (isNE(covtrasIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtrasIO.getParams());
			fatalError600();
		}
		wsaaCovtrasSex.set(covtrasIO.getSex01());
		wsaaCovtrasCrrcd.set(covtrasIO.getEffdate());
		wsaaRcesdte.set(ZERO);
		while ( !(isEQ(covtrasIO.getStatuz(),varcom.endp)
		|| isNE(covtrasIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(covtrasIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(covtrasIO.getLife(),covtlnbIO.getLife())
		|| isNE(covtrasIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(covtrasIO.getRider(),covtlnbIO.getRider()))) {
			accumulateAssured1120();
		}
		
		sv.totsuma.set(wsaaSumAssuredAccum);
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		wsaaAge.set(lifelnbIO.getAnbAtCcd());
		wsaaSex.set(lifelnbIO.getCltsex());
		wsaaPremMethInd.set(ZERO);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			wsaaPremMethInd.set("1");
		}
		else {
			if (isEQ(t5687rec.jlifePresent,SPACES)) {
				wsaaPremMethInd.set("2");
			}
			else {
				wsaaPremMethInd.set("1");
			}
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			wsbbSex.set(SPACES);
			wsbbAnbAtCcd.set(0);
			goTo(GotoLabel.afterJlife1010);
		}
		wsbbAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsbbSex.set(lifelnbIO.getCltsex());
	}

protected void afterJlife1010()
	{
		ractlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		ractlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		ractlnbIO.setLife(covtlnbIO.getLife());
		ractlnbIO.setCoverage(covtlnbIO.getCoverage());
		ractlnbIO.setRider(covtlnbIO.getRider());
		ractlnbIO.setRasnum(SPACES);
		ractlnbIO.setRatype(SPACES);
		ractlnbIO.setValidflag(SPACES);
		ractlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ractlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ractlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		ractlnbIO.setFormat(ractlnbrec);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)
		&& isNE(ractlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(ractlnbIO.getStatuz(),varcom.oK)
		&& isEQ(ractlnbIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		&& isEQ(ractlnbIO.getChdrnum(),covtlnbIO.getChdrnum())
		&& isEQ(ractlnbIO.getLife(),covtlnbIO.getLife())
		&& isEQ(ractlnbIO.getCoverage(),covtlnbIO.getCoverage())
		&& isEQ(ractlnbIO.getRider(),covtlnbIO.getRider())
		&& isEQ(ractlnbIO.getValidflag(),"3")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.currcode.set(chdrlnbIO.getCntcurr());
			goTo(GotoLabel.afterRact1030);
		}
		sv.currcode.set(chdrlnbIO.getCntcurr());
		sv.rasnum.set(ractlnbIO.getRasnum());
		rasaIO.setRasnum(ractlnbIO.getRasnum());
		rasaIO.setRascoy(ractlnbIO.getChdrcoy());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setClntcoy(rasaIO.getClntcoy());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void displayRact1020()
	{
		sv.raAmount.set(ractlnbIO.getRaAmount());
		sv.rapayfrq.set(ractlnbIO.getRapayfrq());
		sv.ratype.set(ractlnbIO.getRatype());
		if (isEQ(ractlnbIO.getInstprem(),ZERO)) {
			sv.instPrem.set(ractlnbIO.getSingp());
			screenInstPrem.set(ractlnbIO.getSingp());
		}
		else {
			sv.instPrem.set(ractlnbIO.getInstprem());
			screenInstPrem.set(ractlnbIO.getInstprem());
		}
		descIO.setParams(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sv.ratype);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.ratypeDesc.set(descIO.getLongdesc());
	}

protected void afterRact1030()
	{
		descIO.setDataKey(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sv.currcode);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.currds.fill("?");
		}
		else {
			sv.currds.set(descIO.getShortdesc());
		}
		screenRasnum.set(sv.rasnum);
		screenRapayfrq.set(sv.rapayfrq);
		screenRatype.set(sv.ratype);
		screenRaAmount.set(sv.raAmount);
		screenInstPrem.set(sv.instPrem);
		if (isEQ(wsspcomn.flag,"I")) {
			sv.cltnameOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.rapayfrqOut[varcom.pr.toInt()].set("Y");
			sv.rasnumOut[varcom.pr.toInt()].set("Y");
			sv.ratypeOut[varcom.pr.toInt()].set("Y");
			sv.ratypedescOut[varcom.pr.toInt()].set("Y");
			sv.raamountOut[varcom.pr.toInt()].set("Y");
			sv.totsumaOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void accumulateAssured1120()
	{
		/*ACCUMULATE-ASSURED*/
		compute(wsaaSumAssuredAccum, 2).set(add(wsaaSumAssuredAccum,covtrasIO.getSumins()));
		if (isGT(covtrasIO.getRiskCessDate(),wsaaRcesdte)) {
			wsaaRcesdte.set(covtrasIO.getRiskCessDate());
		}
		covtrasIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covtrasIO);
		if (isNE(covtrasIO.getStatuz(),varcom.oK)
		&& isNE(covtrasIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtrasIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
			try {
					screenIo2010();
					validate2020();
					calc2030();
				}
			catch (GOTOException e){
			/* Expected exception for control flow purposes. */
			}
		}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isEQ(sv.rasnum,SPACES)
		&& isNE(sv.ratype,SPACES)) {
			sv.ratypeErr.set(errorsInner.e065);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.rasnum,SPACES)) {
			checkRasnum2100();
		}
		if (isNE(sv.ratype,SPACES)) {
			checkRatype2200();
		}
		else {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.rasnum,SPACES)
		&& isEQ(sv.ratype,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.ratype,SPACES)
		&& isNE(screenRatype,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.raAmount,sv.totsuma)) {
			sv.raamountErr.set(errorsInner.h090);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			goTo(GotoLabel.exit2090);
		}
		if (isLT(sv.raAmount,t5608rec.sumInsMin)
		|| isGT(sv.raAmount,t5608rec.sumInsMax)) {
			sv.raamountErr.set(errorsInner.h089);
			wsspcomn.edterror.set("Y");
		}
		descIO.setDataKey(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sv.currcode);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			sv.currcodeErr.set(errorsInner.f982);
			wsspcomn.edterror.set("Y");
		}
		sv.currds.set(descIO.getShortdesc());
	}

protected void calc2030()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(sv.ratype);
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		premiumrec.lifeLife.set(covtlnbIO.getLife());
		if (isEQ(covtrasIO.getLife(),"01")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(covtrasIO.getPremCessDate());
		premiumrec.covrCoverage.set(covtrasIO.getCoverage());
		premiumrec.covrRider.set(covtrasIO.getRider());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAge);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon3rec.intDate2.set(covtrasIO.getPremCessDate());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(chdrlnbIO.getCntcurr());
		premiumrec.sumin.set(sv.raAmount);
		premiumrec.mortcls.set(covtrasIO.getMortcls());
		premiumrec.billfreq.set(chdrlnbIO.getBillfreq());
		premiumrec.mop.set(chdrlnbIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.reasind.set("2");
		if (isEQ(wsaaSubroutineName,SPACES)) {
			return ;
		}
		getAnnt2400();
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(wsaaSubroutineName, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrlnbIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(wsaaSubroutineName.toString().substring(3));
			callProgram(wsaaSubroutineName, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/	
		/*Ticket #ILIFE-2005 - End	*/
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		sv.instPrem.set(premiumrec.calcPrem);
		ractlnbIO.setInstprem(premiumrec.calcPrem);
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*2900-EXIT.                                                       */
		/*2090-EXIT.                                               <S19FIX>*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkRasnum2100()
	{
		checkRasnum2110();
	}

protected void checkRasnum2110()
	{
		/* If the Re-Assurance Number is not Blanks Then*/
		/* Validate this number Against the RASA File*/
		/* and at the same time get The Client Number & Name.*/
		/* Setting up the Key to validate the input RASANUM*/
		rasaIO.setRascoy(covtlnbIO.getChdrcoy());
		rasaIO.setRasnum(sv.rasnum);
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)
		&& isNE(rasaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		if (isEQ(rasaIO.getStatuz(),varcom.mrnf)) {
			sv.rasnumErr.set(errorsInner.h092);
			wsspcomn.edterror.set("Y");
		}
		else {
			checkClient2310();
		}
	}

protected void checkRatype2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkRastype2210();
				case noPremCalcSubr2220: 
					noPremCalcSubr2220();
				case exit2299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkRastype2210()
	{
		/* Get ready for Premium Calculations.*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(sv.ratype);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),sv.ratype)) {
			sv.ratypeErr.set(errorsInner.h091);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2299);
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		itemIO.setParams(SPACES);
		if (isEQ(wsaaPremMethInd,"1")) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremMethInd,"2")) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			wsaaSubroutineName.set(SPACES);
			goTo(GotoLabel.noPremCalcSubr2220);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setItemseq(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			scrnparams.errorCode.set(errorsInner.f026);
			wsspcomn.edterror.set("Y");
		}
		else {
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/* ILIFE-3142 End*/
			t5675rec.t5675Rec.set(itemIO.getGenarea());
			wsaaSubroutineName.set(SPACES);
			wsaaSubroutineName.set(t5675rec.premsubr);
		}
	}

protected void noPremCalcSubr2220()
	{
		descIO.setParams(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sv.ratype);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.ratypeDesc.set(descIO.getLongdesc());
		itemIO.setParams(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Trcode.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Rastype.set(sv.ratype);
		itemIO.setItemitem(wsaaT5671Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
			return ;
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		if (isEQ(wsaaProg,t5671rec.pgm01)) {
			wsaaValItemCode.set(t5671rec.edtitm01);
		}
		else {
			if (isEQ(wsaaProg,t5671rec.pgm02)) {
				wsaaValItemCode.set(t5671rec.edtitm02);
			}
			else {
				if (isEQ(wsaaProg,t5671rec.pgm03)) {
					wsaaValItemCode.set(t5671rec.edtitm03);
				}
				else {
					if (isEQ(wsaaProg,t5671rec.pgm04)) {
						wsaaValItemCode.set(t5671rec.edtitm04);
					}
				}
			}
		}
		itdmIO.setParams(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5608);
		wsaaT5608Item1.set(wsaaValItemCode);
		wsaaT5608Item2.set(chdrlnbIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5608Item);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t5608,itdmIO.getItemtabl())
		|| isNE(wsaaT5608Item,itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.f024);
			wsspcomn.edterror.set("Y");
			return ;
		}
		else {
			t5608rec.t5608Rec.set(itdmIO.getGenarea());
		}
		itdmIO.setParams(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6662);
		itdmIO.setItemitem(sv.ratype);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t6662,itdmIO.getItemtabl())
		|| isNE(sv.ratype,itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.ratypeErr.set(errorsInner.h088);
			wsspcomn.edterror.set("Y");
			return ;
		}
		else {
			t6662rec.t6662Rec.set(itdmIO.getGenarea());
		}
		covtrasIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrasIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrasIO.setLife(covtlnbIO.getLife());
		covtrasIO.setCoverage(covtlnbIO.getCoverage());
		covtrasIO.setRider(covtlnbIO.getRider());
		wsaaSumAssuredAccum.set(0);
		covtrasIO.setFunction(varcom.begn);
		covtrasIO.setFormat(covtrasrec);
		SmartFileCode.execute(appVars, covtrasIO);
		if (isNE(covtrasIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtrasIO.getParams());
			fatalError600();
		}
		if (isEQ(covtrasIO.getCrtable(),t6662rec.reascode01)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode02)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode03)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode04)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode05)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode06)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode07)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode08)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode09)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode10)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode11)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode12)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode13)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode14)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode15)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode16)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode17)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode18)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode19)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode20)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode21)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode22)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode23)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode24)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode25)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode26)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode27)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode28)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode29)
		|| isEQ(covtrasIO.getCrtable(),t6662rec.reascode30)) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.ratypeErr.set(errorsInner.h087);
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkClient2310()
	{
		checkClient2311();
	}

protected void checkClient2311()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setClntcoy(rasaIO.getClntcoy());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* Formating the name for output to the screen.*/
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void getAnnt2400()
	{
		begins2410();
	}

protected void begins2410()
	{
		anntIO.setChdrcoy(covtrasIO.getChdrcoy());
		anntIO.setChdrnum(covtrasIO.getChdrnum());
		anntIO.setLife(covtrasIO.getLife());
		anntIO.setCoverage(covtrasIO.getCoverage());
		anntIO.setRider(covtrasIO.getRider());
		anntIO.setSeqnbr(covtrasIO.getSeqnbr());
		anntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anntIO);
		if (isNE(anntIO.getStatuz(),varcom.oK)
		&& isNE(anntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anntIO.getParams());
			syserrrec.statuz.set(anntIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(anntIO.getFreqann());
			premiumrec.advance.set(anntIO.getAdvance());
			premiumrec.arrears.set(anntIO.getArrears());
			premiumrec.guarperd.set(anntIO.getGuarperd());
			premiumrec.intanny.set(anntIO.getIntanny());
			premiumrec.capcont.set(anntIO.getCapcont());
			premiumrec.withprop.set(anntIO.getWithprop());
			premiumrec.withoprop.set(anntIO.getWithoprop());
			premiumrec.ppind.set(anntIO.getPpind());
			premiumrec.nomlife.set(anntIO.getNomlife());
			premiumrec.dthpercn.set(anntIO.getDthpercn());
			premiumrec.dthperco.set(anntIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

protected void update3000()
	{
			updateDatabase3100();
		}

protected void updateDatabase3100()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")
		|| (isEQ(sv.instPrem,screenInstPrem)
		&& isEQ(sv.rapayfrq,screenRapayfrq)
		&& isEQ(sv.rasnum,screenRasnum)
		&& isEQ(sv.ratype,screenRatype)
		&& isEQ(sv.raAmount,screenRaAmount))) {
			return ;
		}
		ractlnbIO.setSex(wsaaCovtrasSex);
		ractlnbIO.setCrrcd(wsaaCovtrasCrrcd);
		ractlnbIO.setCurrfrom(wsaaCovtrasCrrcd);
		ractlnbIO.setBtdate(wsaaCovtrasCrrcd);
		ractlnbIO.setRiskCessDate(wsaaRcesdte);
		ractlnbIO.setRasnum(sv.rasnum);
		ractlnbIO.setValidflag("3");
		if ((isNE(screenRasnum,SPACES)
		&& isNE(sv.rasnum,SPACES)
		&& isNE(screenRasnum,sv.rasnum))
		|| (isNE(screenRatype,SPACES)
		&& isNE(sv.ratype,SPACES)
		&& isNE(sv.ratype,screenRatype))
		|| isNE(screenRatype,SPACES)
		&& isNE(sv.ratype,SPACES)
		&& (isNE(sv.rapayfrq,screenRapayfrq)
		|| isNE(sv.raAmount,screenRaAmount)
		|| isNE(sv.instPrem,screenInstPrem))) {
			modifyRact3160();
		}
		if (isEQ(screenRatype,SPACES)
		&& isNE(sv.ratype,SPACES)) {
			addRact3170();
		}
		if (isNE(screenRatype,SPACES)
		&& isEQ(sv.ratype,SPACES)) {
			deleteRact3180();
		}
	}

protected void addRact3170()
	{
		addRact3171();
	}

protected void addRact3171()
	{
		if (isEQ(sv.rapayfrq,"00")) {
			ractlnbIO.setSingp(sv.instPrem);
			ractlnbIO.setInstprem(ZERO);
		}
		else {
			ractlnbIO.setSingp(ZERO);
			ractlnbIO.setInstprem(sv.instPrem);
		}
		ractlnbIO.setRaAmount(sv.raAmount);
		ractlnbIO.setCurrcode(sv.currcode);
		ractlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ractlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		ractlnbIO.setLife(covtlnbIO.getLife());
		ractlnbIO.setCoverage(covtlnbIO.getCoverage());
		ractlnbIO.setRider(covtlnbIO.getRider());
		ractlnbIO.setRatype(sv.ratype);
		ractlnbIO.setRapayfrq(sv.rapayfrq);
		ractlnbIO.setTransactionDate(varcom.vrcmDate);
		ractlnbIO.setTransactionTime(varcom.vrcmTime);
		ractlnbIO.setUser(varcom.vrcmUser);
		ractlnbIO.setTermid(varcom.vrcmTermid);
		ractlnbIO.setTranno(ZERO);
		ractlnbIO.setFormat(ractlnbrec);
		ractlnbIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
	}

protected void modifyRact3160()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					modifyRact3161();
				case writeOrRewriteRact3155: 
					writeOrRewriteRact3155();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void modifyRact3161()
	{
		if (isEQ(sv.rasnum,screenRasnum)
		&& isEQ(sv.ratype,screenRatype)) {
			ractlnbIO.setFunction(varcom.updat);
			goTo(GotoLabel.writeOrRewriteRact3155);
		}
		ractlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ractlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		ractlnbIO.setRasnum(screenRasnum);
		ractlnbIO.setLife(covtlnbIO.getLife());
		ractlnbIO.setCoverage(covtlnbIO.getCoverage());
		ractlnbIO.setRider(covtlnbIO.getRider());
		ractlnbIO.setRatype(screenRatype);
		ractlnbIO.setFunction(varcom.readh);
		ractlnbIO.setFormat(ractlnbrec);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
		ractlnbIO.setFunction(varcom.delet);
		ractlnbIO.setFormat(ractlnbrec);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
		ractlnbIO.setFunction(varcom.writr);
	}

protected void writeOrRewriteRact3155()
	{
		if (isEQ(sv.rapayfrq,"00")) {
			ractlnbIO.setSingp(sv.instPrem);
			ractlnbIO.setInstprem(ZERO);
		}
		else {
			ractlnbIO.setSingp(ZERO);
			ractlnbIO.setInstprem(sv.instPrem);
		}
		ractlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ractlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		ractlnbIO.setRasnum(sv.rasnum);
		ractlnbIO.setLife(covtlnbIO.getLife());
		ractlnbIO.setCoverage(covtlnbIO.getCoverage());
		ractlnbIO.setRider(covtlnbIO.getRider());
		ractlnbIO.setRatype(sv.ratype);
		ractlnbIO.setRapayfrq(sv.rapayfrq);
		ractlnbIO.setRaAmount(sv.raAmount);
		ractlnbIO.setCurrcode(sv.currcode);
		ractlnbIO.setTransactionDate(varcom.vrcmDate);
		ractlnbIO.setTransactionTime(varcom.vrcmTime);
		ractlnbIO.setUser(varcom.vrcmUser);
		ractlnbIO.setTermid(varcom.vrcmTermid);
		ractlnbIO.setTranno(ZERO);
		ractlnbIO.setFormat(ractlnbrec);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
	}

protected void deleteRact3180()
	{
		deleteRact3181();
	}

protected void deleteRact3181()
	{
		ractlnbIO.setFunction(varcom.readh);
		ractlnbIO.setFormat(ractlnbrec);
		ractlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ractlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		ractlnbIO.setRasnum(screenRasnum);
		ractlnbIO.setLife(covtlnbIO.getLife());
		ractlnbIO.setCoverage(covtlnbIO.getCoverage());
		ractlnbIO.setRider(covtlnbIO.getRider());
		ractlnbIO.setRatype(screenRatype);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
		ractlnbIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ractlnbIO);
		if (isNE(ractlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ractlnbIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData f982 = new FixedLengthStringData(4).init("F982");
	private FixedLengthStringData f024 = new FixedLengthStringData(4).init("F024");
	private FixedLengthStringData f025 = new FixedLengthStringData(4).init("F025");
	private FixedLengthStringData f026 = new FixedLengthStringData(4).init("F026");
	private FixedLengthStringData e065 = new FixedLengthStringData(4).init("E065");
	private FixedLengthStringData h087 = new FixedLengthStringData(4).init("H087");
	private FixedLengthStringData h088 = new FixedLengthStringData(4).init("H088");
	private FixedLengthStringData h089 = new FixedLengthStringData(4).init("H089");
	private FixedLengthStringData h090 = new FixedLengthStringData(4).init("H090");
	private FixedLengthStringData h091 = new FixedLengthStringData(4).init("H091");
	private FixedLengthStringData h092 = new FixedLengthStringData(4).init("H092");
}
}
