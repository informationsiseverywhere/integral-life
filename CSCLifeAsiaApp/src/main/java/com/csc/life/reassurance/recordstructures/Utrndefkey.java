package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:48
 * Description:
 * Copybook name: UTRNDEFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrndefkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrndefFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrndefKey = new FixedLengthStringData(64).isAPartOf(utrndefFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrndefChdrcoy = new FixedLengthStringData(1).isAPartOf(utrndefKey, 0);
  	public FixedLengthStringData utrndefChdrnum = new FixedLengthStringData(8).isAPartOf(utrndefKey, 1);
  	public FixedLengthStringData utrndefLife = new FixedLengthStringData(2).isAPartOf(utrndefKey, 9);
  	public FixedLengthStringData utrndefCoverage = new FixedLengthStringData(2).isAPartOf(utrndefKey, 11);
  	public FixedLengthStringData utrndefRider = new FixedLengthStringData(2).isAPartOf(utrndefKey, 13);
  	public PackedDecimalData utrndefPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrndefKey, 15);
  	public FixedLengthStringData utrndefUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrndefKey, 18);
  	public FixedLengthStringData utrndefUnitType = new FixedLengthStringData(1).isAPartOf(utrndefKey, 22);
  	public PackedDecimalData utrndefMoniesDate = new PackedDecimalData(8, 0).isAPartOf(utrndefKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(utrndefKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrndefFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrndefFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}