/*
 * File: Bt560.java
 * Date: 29 August 2009 22:37:35
 * Author: Quipoz Limited
 * 
 * Class transformed from BT560.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ZlifelcTableDAM;
import com.csc.life.productdefinition.tablestructures.Tt579rec;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.recordstructures.Pt560par;
import com.csc.life.reassurance.reports.Rt560Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   The Accident Riders Aggregate Limits Report.
*
*   This program will read LIFE file, and table TT579 to get
*   accident riders and the aggregate limit of the sum assured.
*
*   The report will be printed only if the life assured has
*   the total sum assured of accident riders exceed the limit
*   on TT579.
*
***********************************************************************
* </pre>
*/
public class Bt560 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqllifepf1rs = null;
	private java.sql.PreparedStatement sqllifepf1ps = null;
	private java.sql.Connection sqllifepf1conn = null;
	private String sqllifepf1 = "";
	private Rt560Report printerFile = new Rt560Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BT560");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaPayeeGivn = "PYNMN";
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRngmnt = new FixedLengthStringData(4).init(SPACES);
		/* WSAA-TOTALS */
	private ZonedDecimalData wsaaArrtot = new ZonedDecimalData(16, 2).init(ZERO);
	private ZonedDecimalData wsaaRidtot = new ZonedDecimalData(18, 2).init(ZERO);
	private ZonedDecimalData wsaaContot = new ZonedDecimalData(18, 2).init(ZERO);
	private ZonedDecimalData wsaaClntot = new ZonedDecimalData(18, 2).init(ZERO);
	private ZonedDecimalData wsaaGrandtot = new ZonedDecimalData(18, 2).init(ZERO);
		/* WSAA-COUNTERS */
	private ZonedDecimalData wsaaLctr = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSw = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNew = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaLifcnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTotalSa = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotalSa2 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private ZonedDecimalData wsaaFromdate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String racdrec = "RACDREC";
	private String covrlnbrec = "COVRLNBREC";
	private String zlifelcrec = "ZLIFELCREC";
		/* ERRORS */
	private String g875 = "G875";
	private String esql = "ESQL";
	private String t1693 = "T1693";
	private String tt579 = "TT579";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-LIFEPF */
	private FixedLengthStringData sqlLiferec = new FixedLengthStringData(36);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlLiferec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlLiferec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlLiferec, 9);
	private FixedLengthStringData sqlJlife = new FixedLengthStringData(2).isAPartOf(sqlLiferec, 11);
	private FixedLengthStringData sqlLifcnum = new FixedLengthStringData(8).isAPartOf(sqlLiferec, 13);
	private PackedDecimalData sqlCurrfrom = new PackedDecimalData(8, 0).isAPartOf(sqlLiferec, 21);
	private PackedDecimalData sqlCurrto = new PackedDecimalData(8, 0).isAPartOf(sqlLiferec, 26);
	private PackedDecimalData sqlLcdte = new PackedDecimalData(8, 0).isAPartOf(sqlLiferec, 31);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* WSAA-BT560 */
	private FixedLengthStringData wsaaBt560H01 = new FixedLengthStringData(20);
	private FixedLengthStringData rt560h01O = new FixedLengthStringData(20).isAPartOf(wsaaBt560H01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rt560h01O, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rt560h01O, 10);

	private FixedLengthStringData wsaaBt560H02 = new FixedLengthStringData(88);
	private FixedLengthStringData rt560h02O = new FixedLengthStringData(88).isAPartOf(wsaaBt560H02, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rt560h02O, 0);
	private FixedLengthStringData rh01Clntnum = new FixedLengthStringData(8).isAPartOf(rt560h02O, 30);
	private FixedLengthStringData rh01Clntname = new FixedLengthStringData(50).isAPartOf(rt560h02O, 38);

	private FixedLengthStringData wsaaBt560D01 = new FixedLengthStringData(69);
	private FixedLengthStringData rt560d01O = new FixedLengthStringData(69).isAPartOf(wsaaBt560D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rt560d01O, 0);
	private FixedLengthStringData rd01Crtable = new FixedLengthStringData(4).isAPartOf(rt560d01O, 8);
	private ZonedDecimalData rd01Sumins = new ZonedDecimalData(17, 2).isAPartOf(rt560d01O, 12);
	private FixedLengthStringData rd01Rngmnt = new FixedLengthStringData(4).isAPartOf(rt560d01O, 29);
	private FixedLengthStringData rd01Retype = new FixedLengthStringData(1).isAPartOf(rt560d01O, 33);
	private FixedLengthStringData rd01Cmdate = new FixedLengthStringData(10).isAPartOf(rt560d01O, 34);
	private FixedLengthStringData rd01Rasnum = new FixedLengthStringData(8).isAPartOf(rt560d01O, 44);
	private ZonedDecimalData rdRaamount = new ZonedDecimalData(17, 2).isAPartOf(rt560d01O, 52);

	private FixedLengthStringData wsaaBt560D1 = new FixedLengthStringData(17);
	private FixedLengthStringData rd560d1O = new FixedLengthStringData(17).isAPartOf(wsaaBt560D1, 0);
	private ZonedDecimalData amta = new ZonedDecimalData(17, 2).isAPartOf(rd560d1O, 0);

	private FixedLengthStringData wsaaBt560D2 = new FixedLengthStringData(17);
	private FixedLengthStringData rd560d2O = new FixedLengthStringData(17).isAPartOf(wsaaBt560D2, 0);
	private ZonedDecimalData amtb = new ZonedDecimalData(17, 2).isAPartOf(rd560d2O, 0);

	private FixedLengthStringData wsaaBt560D3 = new FixedLengthStringData(18);
	private FixedLengthStringData rd560d3O = new FixedLengthStringData(18).isAPartOf(wsaaBt560D3, 0);
	private ZonedDecimalData amtma = new ZonedDecimalData(18, 2).isAPartOf(rd560d3O, 0);

	private FixedLengthStringData wsaaBt560D4 = new FixedLengthStringData(18);
	private FixedLengthStringData rd560d4O = new FixedLengthStringData(18).isAPartOf(wsaaBt560D4, 0);
	private ZonedDecimalData amtmb = new ZonedDecimalData(18, 2).isAPartOf(rd560d4O, 0);

	private FixedLengthStringData wsaaBt560D5 = new FixedLengthStringData(18);
	private FixedLengthStringData rd560d5O = new FixedLengthStringData(18).isAPartOf(wsaaBt560D5, 0);
	private ZonedDecimalData amtmc = new ZonedDecimalData(18, 2).isAPartOf(rd560d5O, 0);

	private FixedLengthStringData wsaaBt560D6 = new FixedLengthStringData(18);
	private FixedLengthStringData rd560d6O = new FixedLengthStringData(18).isAPartOf(wsaaBt560D6, 0);
	private ZonedDecimalData amtmd = new ZonedDecimalData(18, 2).isAPartOf(rd560d6O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pt560par pt560par = new Pt560par();
		/*Reassurance Cession Details Logical*/
	private RacdTableDAM racdIO = new RacdTableDAM();
	private Tt579rec tt579rec = new Tt579rec();
		/*LIFE Logical keyed on LIFCNUM*/
	private ZlifelcTableDAM zlifelcIO = new ZlifelcTableDAM();
	private static final int wsaaMaxLines = 100; //TICKET #ILIFE-1148

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2080, 
		exit2090, 
		exit2590, 
		nextCovrlnb2680, 
		exit2690, 
		nextZlifelc5180, 
		exit5190, 
		nextCovrlnb5280, 
		exit5290, 
		exit5590, 
		exit5710
	}

	public Bt560() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		start1010();
		initSetUp1020();
		setUpHeadingDates1040();
		readTt5791050();
		defineCursor1060();
	}

protected void start1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaArrtot.set(ZERO);
		wsaaRidtot.set(ZERO);
		wsaaContot.set(ZERO);
		wsaaClntot.set(ZERO);
		wsaaTotalSa.set(ZERO);
		wsaaTotalSa2.set(ZERO);
		wsaaFirstTime.set("Y");
		pt560par.parmRecord.set(bupaIO.getParmarea());
		wsaaFromdate.set(pt560par.fromdate);
		if (isEQ(wsaaFromdate,0)
		|| isEQ(wsaaFromdate,99999999)) {
			wsaaFromdate.set(0);
		}
		wsaaTodate.set(pt560par.todate);
		if (isEQ(wsaaTodate,0)
		|| isEQ(wsaaTodate,99999999)) {
			wsaaTodate.set(99999999);
		}
	}

protected void initSetUp1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(g875);
			syserrrec.params.set(descIO.getParams());
			fatalError9100();
		}
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		rh01Repdate.set(datcon1rec.extDate);
	}

protected void readTt5791050()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tt579);
		itdmIO.setItemitem("**");
		itdmIO.setItmfrm(bsscIO.getEffectiveDate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9100();
		}
		if (isNE(itdmIO.getItemitem(),"**")
		|| isNE(bsprIO.getCompany(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),tt579)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			fatalError9100();
		}
		else {
			tt579rec.tt579Rec.set(itdmIO.getGenarea());
		}
	}

protected void defineCursor1060()
	{
		sqllifepf1 = " SELECT  CHDRCOY, CHDRNUM, LIFE, JLIFE, LIFCNUM, CURRFROM, CURRTO, LCDTE" +
" FROM   " + appVars.getTableNameOverriden("LIFEPF") + " " +
" WHERE VALIDFLAG = '1'" +
" AND STATCODE = 'IF'" +
" AND (LCDTE >= ?" +
" AND LCDTE <= ?)" +
" ORDER BY LIFCNUM, CHDRNUM, LIFE, JLIFE";
		sqlerrorflag = false;
		try {
			sqllifepf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.LifepfTableDAM());
			sqllifepf1ps = appVars.prepareStatementEmbeded(sqllifepf1conn, sqllifepf1, "LIFEPF");
			appVars.setDBDouble(sqllifepf1ps, 1, wsaaFromdate.toDouble());
			appVars.setDBDouble(sqllifepf1ps, 2, wsaaTodate.toDouble());
			sqllifepf1rs = appVars.executeQuery(sqllifepf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqllifepf1rs.next()) {
				appVars.getDBObject(sqllifepf1rs, 1, sqlChdrcoy);
				appVars.getDBObject(sqllifepf1rs, 2, sqlChdrnum);
				appVars.getDBObject(sqllifepf1rs, 3, sqlLife);
				appVars.getDBObject(sqllifepf1rs, 4, sqlJlife);
				appVars.getDBObject(sqllifepf1rs, 5, sqlLifcnum);
				appVars.getDBObject(sqllifepf1rs, 6, sqlCurrfrom);
				appVars.getDBObject(sqllifepf1rs, 7, sqlCurrto);
				appVars.getDBObject(sqllifepf1rs, 8, sqlLcdte);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		if (firstTime.isTrue()) {
			wsaaLifcnum.set(sqlLifcnum);
			wsaaFirstTime.set("N");
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		try {
			start2510();
		}
		catch (GOTOException e){
		}
	}

protected void start2510()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(sqlLifcnum,wsaaLifcnum)
		&& isGT(wsaaTotalSa,tt579rec.amta)) {
			printReport5000();
			wsaaLifcnum.set(sqlLifcnum);
			wsaaTotalSa.set(0);
			goTo(GotoLabel.exit2590);
		}
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(sqlChdrcoy);
		covrlnbIO.setChdrnum(sqlChdrnum);
		covrlnbIO.setLife(sqlLife);
		covrlnbIO.setPlanSuffix(0);
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			readCovr2600();
		}
		
	}

protected void readCovr2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2610();
				}
				case nextCovrlnb2680: {
					nextCovrlnb2680();
				}
				case exit2690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2610()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError9100();
		}
		if (isNE(sqlChdrcoy,covrlnbIO.getChdrcoy())
		|| isNE(sqlChdrnum,covrlnbIO.getChdrnum())
		|| isEQ(covrlnbIO.getStatuz(),varcom.endp)) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.nextCovrlnb2680);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,15)
		|| isEQ(tt579rec.crtable[wsaaSub.toInt()],SPACES)); wsaaSub.add(1)){
			if (isEQ(tt579rec.crtable[wsaaSub.toInt()],covrlnbIO.getCrtable())) {
				wsaaTotalSa.add(covrlnbIO.getSumins());
				wsaaSub.set(16);
			}
		}
	}

protected void nextCovrlnb2680()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void update3000()
	{
		/*START*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		start4010();
	}

protected void start4010()
	{
		pageOverflow5700();
		amta.set(wsaaArrtot);
		printerFile.printRd560d1(wsaaBt560D1);
		amtb.set(wsaaRidtot);
		printerFile.printRd560d2(wsaaBt560D2);
		amtma.set(wsaaContot);
		printerFile.printRd560d3(wsaaBt560D3);
		amtmb.set(wsaaClntot);
		printerFile.printRd560d4(wsaaBt560D4);
		amtmc.set(wsaaGrandtot);
		printerFile.printRd560d5(wsaaBt560D5);
		amtmd.set(wsaaTotalSa2);
		printerFile.printRd560d6(wsaaBt560D6);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
	}

protected void printReport5000()
	{
		/*START*/
		zlifelcIO.setParams(SPACES);
		zlifelcIO.setChdrcoy(bsprIO.getCompany());
		zlifelcIO.setLifcnum(wsaaLifcnum);
		zlifelcIO.setFormat(zlifelcrec);
		zlifelcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zlifelcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(zlifelcIO.getStatuz(),varcom.endp))) {
			readZlifelc5100();
		}
		
		/*EXIT*/
	}

protected void readZlifelc5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5110();
				}
				case nextZlifelc5180: {
					nextZlifelc5180();
				}
				case exit5190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5110()
	{
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)
		&& isNE(zlifelcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zlifelcIO.getStatuz());
			syserrrec.params.set(zlifelcIO.getParams());
			fatalError9100();
		}
		if (isEQ(zlifelcIO.getStatuz(),varcom.endp)
		|| isNE(zlifelcIO.getChdrcoy(),bsprIO.getCompany())
		|| isNE(zlifelcIO.getLifcnum(),wsaaLifcnum)) {
			zlifelcIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		if (isNE(zlifelcIO.getValidflag(),"1")) {
			goTo(GotoLabel.nextZlifelc5180);
		}
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(bsprIO.getCompany());
		covrlnbIO.setChdrnum(zlifelcIO.getChdrnum());
		covrlnbIO.setPlanSuffix(0);
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			readCovrlnb5200();
		}
		
	}

protected void nextZlifelc5180()
	{
		zlifelcIO.setFunction(varcom.nextr);
	}

protected void readCovrlnb5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5210();
				}
				case nextCovrlnb5280: {
					nextCovrlnb5280();
				}
				case exit5290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5210()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError9100();
		}
		if (isNE(bsprIO.getCompany(),covrlnbIO.getChdrcoy())
		|| isNE(zlifelcIO.getChdrnum(),covrlnbIO.getChdrnum())
		|| isEQ(covrlnbIO.getStatuz(),varcom.endp)) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5290);
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.nextCovrlnb5280);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,15)
		|| isEQ(tt579rec.crtable[wsaaSub.toInt()],SPACES)); wsaaSub.add(1)){
			if (isEQ(tt579rec.crtable[wsaaSub.toInt()],covrlnbIO.getCrtable())) {
				readRacd5400();
				wsaaTotalSa2.add(covrlnbIO.getSumins());
				wsaaSub.set(16);
			}
		}
	}

protected void nextCovrlnb5280()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void readRacd5400()
	{
		start5410();
	}

protected void start5410()
	{
		racdIO.setParams(SPACES);
		racdIO.setChdrcoy(covrlnbIO.getChdrcoy());
		racdIO.setChdrnum(covrlnbIO.getChdrnum());
		racdIO.setLife(covrlnbIO.getLife());
		racdIO.setCoverage(covrlnbIO.getCoverage());
		racdIO.setRider(covrlnbIO.getRider());
		racdIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		racdIO.setSeqno(ZERO);
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdIO.getStatuz(),varcom.endp))) {
			readNext5420();
		}
		
	}

protected void readNext5420()
	{
		start5420();
	}

protected void start5420()
	{
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(),varcom.oK)
		&& isNE(racdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(racdIO.getStatuz());
			syserrrec.params.set(racdIO.getParams());
			fatalError9100();
		}
		if (isNE(covrlnbIO.getChdrcoy(),racdIO.getChdrcoy())
		|| isNE(covrlnbIO.getChdrnum(),racdIO.getChdrnum())
		|| isNE(covrlnbIO.getLife(),racdIO.getLife())
		|| isNE(covrlnbIO.getCoverage(),racdIO.getCoverage())
		|| isNE(covrlnbIO.getRider(),racdIO.getRider())
		|| isNE(covrlnbIO.getPlanSuffix(),racdIO.getPlanSuffix())
		|| isEQ(racdIO.getStatuz(),varcom.endp)) {
			racdIO.setStatuz(varcom.endp);
		}
		if (isEQ(racdIO.getValidflag(),"1") && isNE(racdIO.getStatuz(),varcom.endp)) { //TICKET#ILIFE-1148
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot5900();
			moveData5500();
		}
		racdIO.setFunction(varcom.nextr);
	}

protected void moveData5500()
	{
		try {
			start5510();
		}
		catch (GOTOException e){
		}
	}

protected void start5510()
	{
		if (isEQ(racdIO.getStatuz(),varcom.endp)) {
			noRacd5511();
			goTo(GotoLabel.exit5590);
		}
		if (isEQ(wsaaSw,0)) {
			wsaaOverflow.set("N");
			printerFile.printRt560h01(wsaaBt560H01, indicArea);
			wsaaLctr.set(6);
			rh01Clntnum.set(zlifelcIO.getLifcnum());
			callNamadrs6000();
			writeH25750();
			wsaaClntnum.set(zlifelcIO.getLifcnum());
			wsaaChdrnum.set(racdIO.getChdrnum());
			wsaaRider.set(racdIO.getRider());
			wsaaRngmnt.set(racdIO.getRngmnt());
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot5900();
			pageOverflow5700();
			check5600();
		}
		else {
			check5600();
		}
	}

protected void noRacd5511()
	{
		start5511();
	}

protected void start5511()
	{
		if (isEQ(wsaaSw,0)
		|| isGT(wsaaLctr,wsaaMaxLines)) { //TICKET #ILIFE-1148
			wsaaOverflow.set("N");
			printerFile.printRt560h01(wsaaBt560H01, indicArea);
			wsaaLctr.set(6);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot5900();
			rh01Clntnum.set(zlifelcIO.getLifcnum());
			wsaaClntnum.set(zlifelcIO.getLifcnum());
			callNamadrs6000();
			writeH25750();
			wsaaSw.set(1);
		}
		else {
			if (isNE(zlifelcIO.getLifcnum(),wsaaClntnum)) {
				rh01Clntnum.set(zlifelcIO.getLifcnum());
				wsaaClntnum.set(zlifelcIO.getLifcnum());
				callNamadrs6000();
				writeH25750();
				contotrec.totno.set(ct02);
				contotrec.totval.set(1);
				callContot5900();
			}
		}
		rd01Chdrnum.set(covrlnbIO.getChdrnum());
		rd01Crtable.set(covrlnbIO.getCrtable());
		rd01Sumins.set(covrlnbIO.getSumins());
		rd01Rngmnt.set(SPACES);
		rd01Retype.set(SPACES);
		rd01Rasnum.set(SPACES);
		rdRaamount.set(ZERO);
		rd01Cmdate.set(ZERO);
		writeD015760();
		wsaaLctr.add(1);
	}

protected void check5600()
	{
		check5610();
	}

protected void check5610()
	{
		wsspEdterror.set(varcom.oK);
		if (isEQ(zlifelcIO.getLifcnum(),wsaaClntnum)) {
			if (isEQ(racdIO.getChdrnum(),wsaaChdrnum)) {
				if (isEQ(racdIO.getRider(),wsaaRider)) {
					if (isEQ(racdIO.getRngmnt(),wsaaRngmnt)) {
						/*NEXT_SENTENCE*/
					}
					else {
						amta.set(wsaaArrtot);
						pageOverflow5700();
						writeD15770();
						wsaaLctr.add(2);
						wsaaArrtot.set(ZERO);
						wsaaRngmnt.set(racdIO.getRngmnt());
					}
				}
				else {
					amta.set(wsaaArrtot);
					amtb.set(wsaaRidtot);
					pageOverflow5700();
					printerFile.printRd560d1(wsaaBt560D1);
					printerFile.printRd560d2(wsaaBt560D2);
					wsaaLctr.add(4);
					wsaaRidtot.set(ZERO);
					wsaaArrtot.set(ZERO);
				}
			}
			else {
				accCt025780();
				amta.set(wsaaArrtot);
				amtb.set(wsaaRidtot);
				amtma.set(wsaaContot);
				pageOverflow5700();
				printerFile.printRd560d1(wsaaBt560D1);
				printerFile.printRd560d2(wsaaBt560D2);
				printerFile.printRd560d3(wsaaBt560D3);
				wsaaLctr.add(6);
				wsaaContot.set(ZERO);
				wsaaArrtot.set(ZERO);
				wsaaRidtot.set(ZERO);
			}
		}
		else {
			amta.set(wsaaArrtot);
			amtb.set(wsaaRidtot);
			amtma.set(wsaaContot);
			amtmb.set(wsaaClntot);
			pageOverflow5700();
			printerFile.printRd560d1(wsaaBt560D1);
			printerFile.printRd560d2(wsaaBt560D2);
			printerFile.printRd560d3(wsaaBt560D3);
			printerFile.printRd560d4(wsaaBt560D4);
			wsaaLctr.add(8);
			wsaaContot.set(ZERO);
			wsaaArrtot.set(ZERO);
			wsaaRidtot.set(ZERO);
			wsaaClntot.set(ZERO);
			accCt025780();
		}
		pageOverflow5700();
		printReportDetail5730();
	}

protected void pageOverflow5700()
	{
		try {
			pageOverflow5710();
		}
		catch (GOTOException e){
		}
	}

protected void pageOverflow5710()
	{
	//TICKET #ILIFE-1148
			if (isLT(wsaaLctr,wsaaMaxLines)) {
			goTo(GotoLabel.exit5710);
		}
		else {
			newPage5800();
			wsaaOverflow.set("Y");
		}
		if (isEQ(wsaaOverflow,"Y")) {
			rh01Clntnum.set(zlifelcIO.getLifcnum());
			wsaaNew.set(1);
			callNamadrs6000();
			printerFile.printRt560h02(wsaaBt560H02);
			wsaaOverflow.set("N");
			wsaaLctr.add(6);
		}
	}

protected void checkClient5720()
	{
		/*CHECK-CLIENT*/
		if (isEQ(wsaaSw,1)) {
			//TICKET #ILIFE-1148
			if (isLT(wsaaLctr,wsaaMaxLines) &&  (isNE(zlifelcIO.getLifcnum(),wsaaClntnum))) {
				rh01Clntnum.set(zlifelcIO.getLifcnum());
				callNamadrs6000();
				writeH25750();
				rd01Chdrnum.set(racdIO.getChdrnum());
				rd01Crtable.set(covrlnbIO.getCrtable());
				rd01Sumins.set(covrlnbIO.getSumins());
			}
		}
		/*EXIT*/
	}

protected void printReportDetail5730()
	{
		printReport5731();
	}

protected void printReport5731()
	{
		if (isEQ(zlifelcIO.getLifcnum(),wsaaClntnum)
		&& isEQ(wsaaSw,1)) {
			rh01Clntnum.set(SPACES);
			if (isEQ(racdIO.getChdrnum(),wsaaChdrnum)) {
				rd01Chdrnum.set(SPACES);
				if (isEQ(racdIO.getRider(),wsaaRider)) {
					rd01Crtable.set(SPACES);
					rd01Sumins.set(ZERO);
					rd01Rngmnt.set(racdIO.getRngmnt());
					rd01Retype.set(racdIO.getRetype());
					wsaaRider.set(racdIO.getRider());
					wsaaRngmnt.set(racdIO.getRngmnt());
				}
				else {
					rd01Crtable.set(covrlnbIO.getCrtable());
					rd01Sumins.set(covrlnbIO.getSumins());
					rd01Rngmnt.set(racdIO.getRngmnt());
					rd01Retype.set(racdIO.getRetype());
					wsaaRider.set(racdIO.getRider());
					wsaaRngmnt.set(racdIO.getRngmnt());
				}
			}
			else {
				rd01Chdrnum.set(racdIO.getChdrnum());
				rd01Crtable.set(covrlnbIO.getCrtable());
				rd01Rngmnt.set(racdIO.getRngmnt());
				rd01Retype.set(racdIO.getRetype());
				wsaaRider.set(racdIO.getRider());
				wsaaRngmnt.set(racdIO.getRngmnt());
				wsaaChdrnum.set(racdIO.getChdrnum());
			}
		}
		else {
			checkClient5720();
			wsaaOverflow.set("Y");
			pageOverflow5700();
			rh01Clntnum.set(zlifelcIO.getLifcnum());
			rd01Chdrnum.set(racdIO.getChdrnum());
			rd01Crtable.set(covrlnbIO.getCrtable());
			rd01Sumins.set(covrlnbIO.getSumins());
			rd01Rngmnt.set(racdIO.getRngmnt());
			rd01Retype.set(racdIO.getRetype());
			wsaaRider.set(racdIO.getRider());
			wsaaRngmnt.set(racdIO.getRngmnt());
			wsaaChdrnum.set(racdIO.getChdrnum());
			wsaaClntnum.set(zlifelcIO.getLifcnum());
			wsaaSw.set(1);
		}
		pageOverflow5700();
		printDetail5740();
	}

protected void printDetail5740()
	{
		printDetail5741();
	}

protected void printDetail5741()
	{
		rd01Cmdate.set(racdIO.getCmdate());
		rd01Rasnum.set(racdIO.getRasnum());
		rdRaamount.set(racdIO.getRaAmount());
		wsaaArrtot.add(racdIO.getRaAmount());
		wsaaRidtot.add(racdIO.getRaAmount());
		wsaaContot.add(racdIO.getRaAmount());
		wsaaClntot.add(racdIO.getRaAmount());
		wsaaGrandtot.add(racdIO.getRaAmount());
		pageOverflow5700();
		if (isEQ(wsaaNew,1)) {
			rd01Chdrnum.set(racdIO.getChdrnum());
			rd01Crtable.set(covrlnbIO.getCrtable());
			rd01Sumins.set(covrlnbIO.getSumins());
			wsaaNew.set(0);
		}
		writeD015760();
		wsaaLctr.add(1);
	}

protected void writeH25750()
	{
		/*WRITE-H2*/
		printerFile.printRt560h02(wsaaBt560H02, indicArea);
		wsaaLctr.add(6);
		/*EXIT*/
	}

protected void writeD015760()
	{
		/*WRITE-D01*/
		printerFile.printRt560d01(wsaaBt560D01);
		/*EXIT*/
	}

protected void writeD15770()
	{
		/*WRITE-D1*/
		printerFile.printRd560d1(wsaaBt560D1);
		/*EXIT*/
	}

protected void accCt025780()
	{
		/*ACC-CT02*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot5900();
		/*EXIT*/
	}

protected void newPage5800()
	{
		/*NEW-PAGE*/
		printerFile.printRt560h01(wsaaBt560H01);
		wsaaLctr.set(6);
		/*EXIT*/
	}

protected void callContot5900()
	{
		/*CONTOT*/
		callProgram(Contot.class, contotrec.contotRec);
		/*EXIT*/
	}

protected void callNamadrs6000()
	{
		callNamadrs6010();
	}

protected void callNamadrs6010()
	{
		if (isEQ(zlifelcIO.getLifcnum(),NUMERIC)) {
			namadrsrec.namadrsRec.set(SPACES);
			namadrsrec.clntPrefix.set("CN");
			namadrsrec.clntCompany.set(bsprIO.getFsuco());
			namadrsrec.clntNumber.set(zlifelcIO.getLifcnum());
			namadrsrec.function.set(wsaaPayeeGivn);
			namadrsrec.language.set(bsscIO.getLanguage());
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(namadrsrec.statuz);
				appVars.addDiagnostic("NMAD-STATUZ :"+namadrsrec.statuz, 0);
				fatalError9100();
			}
			else {
				StringBuilder stringVariable1 = new StringBuilder();
				stringVariable1.append(SPACES);
				stringVariable1.append(namadrsrec.name.toString());
				rh01Clntname.setLeft(stringVariable1.toString());
			}
		}
		else {
			rh01Clntname.set(SPACES);
		}
	}

protected void sqlError9000()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError9100();
	}

protected void fatalError9100()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("4");
		}
		else {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		wsspEdterror.set(varcom.bomb);
		lsaaStatuz.set(varcom.bomb);
		/*EXIT*/
		stopRun();
	}
}
