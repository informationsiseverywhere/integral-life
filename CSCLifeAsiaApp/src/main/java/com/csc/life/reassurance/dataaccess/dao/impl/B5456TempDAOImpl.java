package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.B5456TempDAO;
import com.csc.life.reassurance.dataaccess.model.B5456DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public abstract class B5456TempDAOImpl extends BaseDAOImpl<B5456DTO> implements B5456TempDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(B5456TempDAOImpl.class);
    
    protected final static String TABLE_NAME = "B5456DATA"; //ILB-475
    public void initTempTable(String wsaaCompany, String wsaaChdrnumfrm, String wsaaChdrnumto, int wsaaSqlEffdate){
        deleteTempTable();
        createTempTable(wsaaCompany, wsaaChdrnumfrm, wsaaChdrnumto,  wsaaSqlEffdate);
    }
    private void deleteTempTable(){
        String dropSql = "DELETE FROM "+TABLE_NAME;
        PreparedStatement psDrop = getPrepareStatement(dropSql);
        try {
            psDrop.execute();
        } catch (SQLException e) {
            LOGGER.error("deleteTempTable()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psDrop, null);
        }
    }
    protected abstract void createTempTable(String wsaaCompany, String wsaaChdrnumfrm, String wsaaChdrnumto, int wsaaSqlEffdate);
}