/*
 * File: P5444.java
 * Date: 30 August 2009 0:26:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P5444.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.procedures.Rexpupd;
import com.csc.life.reassurance.procedures.Trmracd;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.screens.S5444ScreenVars;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.life.reassurance.tablestructures.T5455rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
* Reassurance Cession Maintenance Transaction Screen.
*
* Overview
* ========
*
* This Transaction Program is called from the Reassurance
* Cession Maintenance Program, P5442. Depending on the Action
* chosen, this Program creates a new RACD Record, modifies
* terminates or can enquire into an existing RACD Record.
*
* PLEASE NOTE: This transaction was created for EXCEPTIONAL
* CASES ONLY and should only be used when ABSOLUTELY NECESSARY.
*
* Processing
* ==========
*
* 1000-Initialise
*
* If  returning  from  a  program  further  down  the   stack,
* WSSP-SEC-ACTN = '*', then skip this section.
*
* Initialise  all  relevant  variables  and prepare the screen
* for display.
*
* Set a 'First Time' flag for use in the update section.
*
* Perform a RETRV on the CHDRMJA and RACDMJA files.
*
* Use CHDRMJA to display the heading  details  on  the  screen
* using  T5688  for  the  contract  type  description  and the
* Client  file  for  the  relevant  client  names.  The  short
* descriptions  for  the  contracts  risk  and  premium status
* codes should be  obtained  from  T3623,  (Risk  Status)  and
* T3588, (Premium Status).
*
* Set  the  Contract Currency in the heading from the Contract
* Header.
*
* For Registration set the Payment Currency  to  the  Contract
* Currency.
*
* Display the RACDMJA details on the screen looking up  all  of
* the descriptions from DESC where appropriate.
*
* The total Sum Assured must be Calculated and displayed. This
* is done by a BEGN on the COVRLNB Records with Plan Suffix = 0
* and all the COVRs which match the Company, Life, Coverage and
* Rider are Accumulated.
*
* 2000-Screen-Edit
*
* If   returning  from  a  program  further  down  the  stack,
* WSSP-SEC-ACTN = '*', then skip this section.
*
* If WSSP-FLAG = 'I' protect the screen.
*
* If 'KILL' has been pressed skip this section.
*
* If 'CALC' has been pressed set WSSP-EDTERROR to 'Y'.
*
* The Sequence Number, and all of the Fields below the second
* dotted line are display only at all times.
*
* The Reassurer Account Number must be Validated against the
* RASA File. The description of the Reassurer and other table
* descriptions must be updated.
*
* Sum Reassured cannot be greater than the component Sums
* Assured.
*
* 3000-Update
*
* If  returning  from  a  program  further  down  the   stack,
* WSSP-SEC-ACTN = '*', then skip this section.
*
* If 'KILL' has been pressed skip this section.
*
* If WSSP-FLAG = 'I' then skip this section.
*
* If WSSP-FLAG = 'C' Write a new RACD with Validflag = '3',
* and call Subroutine ACTVRES to change Validflag and update
* experience.
*
* If WSSP-FLAG = 'D' Delete the RACD by calling TRMRACD with a
* function of TRMR.
*
* If WSSP-FLAG = 'M' Delete the RACD by calling TRMRACD with a
* function of TRMR, Write a new RACD with Validflag = '3',
* and call Subroutine ACTVRES to change Validflag and update
* experience.
*
* 4000- Where Next
*
* Add 1 to Program pointer.
*
****************************************************************
* </pre>
*/
public class P5444 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5444");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5449Ix = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTh618Ix = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaReasper = new ZonedDecimalData(8, 5);

	private FixedLengthStringData wsaaReassurerFound = new FixedLengthStringData(1);
	private Validator reassurerFound = new Validator(wsaaReassurerFound, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOn = new Validator(indicTable, "1");
	private Validator indOff = new Validator(indicTable, "0");
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t3629 = "T3629";
	private static final String t5446 = "T5446";
	private static final String t5448 = "T5448";
	private static final String t5449 = "T5449";
	private static final String t5454 = "T5454";
	private static final String t5455 = "T5455";
	private static final String t5688 = "T5688";
	private static final String th618 = "TH618";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String cltsrec = "CLTSREC   ";
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String descrec = "DESCREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String itdmrec = "ITEMREC   ";
	private static final String racdmjarec = "RACDMJAREC";
	private static final String racdrec = "RACDREC   ";
	private static final String rasarec = "RASAREC   ";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Reassurance Cession Details Logical*/
	private RacdTableDAM racdIO = new RacdTableDAM();
		/*Reassurance Cession Details Alterations*/
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private T5449rec t5449rec = new T5449rec();
	private T5455rec t5455rec = new T5455rec();
	private Th618rec th618rec = new Th618rec();
	private Actvresrec actvresrec = new Actvresrec();
	private Trmracdrec trmracdrec = new Trmracdrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Wssplife wssplife = new Wssplife();
	private S5444ScreenVars sv = ScreenProgram.getScreenVars( S5444ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		racdmja1060, 
		exit1090, 
		computePercentage2070, 
		checkForErrors2080, 
		validLrkcls2640, 
		exit2690
	}

	public P5444() {
		super();
		screenVars = sv;
		new ScreenModel("S5444", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
@Override
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case racdmja1060: 
					racdmja1060();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.cmdate.set(varcom.vrcmMaxDate);
		sv.ctdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.sumins.set(ZERO);
		sv.reasper.set(ZERO);
		sv.recovamt.set(ZERO);
		sv.recovamt.set(ZERO);
		indicArea.set(ZERO);
		/*  Retrieve the Contract Header.*/
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		sv.crtable.set(covrlnbIO.getCrtable());
		sv.sumins.set(covrlnbIO.getSumins());
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDescitem(chdrmjaIO.getCntcurr());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currds.set(descIO.getLongdesc());
		}
		else {
			sv.currds.fill("?");
		}
		
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currdesc.set(descIO.getLongdesc());
		}
		else {
			sv.currdesc.fill("?");
		}
		
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setDesctabl(t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.tabledesc.set(descIO.getLongdesc());
		}
		else {
			sv.tabledesc.fill("?");
		}
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setDesctabl(t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.longdesc.set(descIO.getLongdesc());
		}
		else {
			sv.longdesc.fill("?");
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.currcd.set(chdrmjaIO.getCntcurr());
		sv.currcode.set(chdrmjaIO.getCntcurr());
		lifemjaIO.setParams(SPACES);
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrlnbIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		wsaaL1Clntnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),"1")) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsaaL2Clntnum.set(SPACES);
			goTo(GotoLabel.racdmja1060);
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		wsaaL2Clntnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),"1")) {
			sv.jlinsnameErr.set(errorsInner.e350);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void racdmja1060()
	{
		racdmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdmjaIO.getParams());
			fatalError600();
		}
		sv.cmdate.set(racdmjaIO.getCmdate());
		sv.ctdate.set(racdmjaIO.getCtdate());
		sv.rrevdt.set(racdmjaIO.getRrevdt());
		sv.lrkcls.set(racdmjaIO.getLrkcls());
		readT54481300();
		if (isEQ(wsspcomn.flag,"C")) {
			sv.rasnum.set(SPACES);
			sv.rngmnt.set(SPACES);
			sv.retype.set(SPACES);
			sv.sumins.set(covrlnbIO.getSumins());
			sv.raAmount.set(ZERO);
			sv.reasper.set(ZERO);
			sv.cmdate.set(wssplife.effdate);
			sv.ctdate.set(wssplife.effdate);
			if (isNE(t5448rec.rrevfrq,SPACES)) {
				getReviewDate2300();
				sv.rrevdt.set(datcon2rec.intDate2);
			}
			else {
				sv.rrevdt.set(varcom.vrcmMaxDate);
			}
			return ;
		}
		sv.rasnum.set(racdmjaIO.getRasnum());
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(wsspcomn.company);
		rasaIO.setRasnum(sv.rasnum);
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isEQ(rasaIO.getStatuz(),varcom.oK)) {
			cltsIO.setClntnum(rasaIO.getClntnum());
			getClientDetails1200();
			if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
			|| isNE(cltsIO.getValidflag(),"1")) {
				sv.clntnameErr.set(errorsInner.e946);
			}
			plainname();
			sv.clntname.set(wsspcomn.longconfname);
		}
		else {
			sv.rasnumErr.set(errorsInner.e946);
		}
		sv.rngmnt.set(racdmjaIO.getRngmnt());
		sv.retype.set(racdmjaIO.getRetype());
		sv.retypeOut[varcom.pr.toInt()].set("Y");
		descIO.setDescitem(racdmjaIO.getRngmnt());
		descIO.setDesctabl(t5449);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rngmntdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rngmntdesc.fill("?");
		}
		descIO.setDescitem(racdmjaIO.getRetype());
		descIO.setDesctabl(t5454);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.retypedesc.set(descIO.getLongdesc());
		}
		else {
			sv.retypedesc.set(SPACES);
		}
		sv.reasper.set(racdmjaIO.getReasper());
		sv.currcode.set(racdmjaIO.getCurrcode());
		descIO.setDescitem(racdmjaIO.getCurrcode());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currdesc.set(descIO.getLongdesc());
		}
		else {
			sv.currdesc.fill("?");
		}
		sv.raAmount.set(racdmjaIO.getRaAmount());
		sv.recovamt.set(racdmjaIO.getRecovamt());
		if (isEQ(wsspcomn.flag,"D")) {
			indOn.setTrue(25);
		}
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readT54481300()
	{
		t54481310();
	}

protected void t54481310()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5448);
		itdmIO.setItmfrm(covrlnbIO.getCrrcd());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(covrlnbIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaT5448Item,itdmIO.getItemitem())
		|| isNE(chdrmjaIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
			itdmIO.setItemtabl(t5448);
			itdmIO.setStatuz(errorsInner.r060);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"D")) {
			protectScr2400();
		}
		if (isNE(wsspcomn.flag,"C")) {
			sv.retypeOut[varcom.pr.toInt()].set("Y");
		}
		/*SCREENIO*/
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case computePercentage2070: 
					computePercentage2070();
				case checkForErrors2080: 
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		descIO.setDescitem(sv.retype);
		descIO.setDesctabl(t5454);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.retypedesc.set(descIO.getLongdesc());
		}
		else {
			sv.retypedesc.set(SPACES);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"D")) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.raAmount,ZERO)) {
			sv.raamountErr.set(errorsInner.e186);
		}
		if (isGT(sv.raAmount,sv.sumins)) {
			sv.raamountErr.set(errorsInner.h090);
		}
		if (isNE(sv.lrkcls,SPACES)) {
			readTh6182600();
		}
		if (isEQ(sv.rngmnt,SPACES)) {
			sv.rngmntErr.set(errorsInner.e186);
		}
		if (isEQ(sv.rasnum,SPACES)) {
			sv.rasnumErr.set(errorsInner.e186);
		}
		else {
			rasaIO.setParams(SPACES);
			rasaIO.setRascoy(wsspcomn.company);
			rasaIO.setRasnum(sv.rasnum);
			rasaIO.setFormat(rasarec);
			rasaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, rasaIO);
			if (isEQ(rasaIO.getStatuz(),varcom.oK)) {
				readT54552100();
			}
			else {
				sv.rasnumErr.set(errorsInner.e946);
			}
		}
		if (isNE(sv.rngmnt,SPACES)) {
			readT54492200();
		}
		if (isNE(wsspcomn.flag,"C")) {
			goTo(GotoLabel.computePercentage2070);
		}
		if (isEQ(sv.retype,SPACES)) {
			sv.retypeErr.set(errorsInner.f399);
		}
		if (isEQ(sv.rasnum,SPACES)) {
			sv.rasnumErr.set(errorsInner.f399);
		}
		if (isEQ(sv.rngmnt,SPACES)) {
			sv.rngmntErr.set(errorsInner.f399);
		}
		if (isEQ(sv.raAmount,ZERO)) {
			sv.raamountErr.set(errorsInner.f399);
		}
	}

protected void computePercentage2070()
	{
		compute(wsaaReasper, 5).set(mult(div(sv.raAmount,sv.sumins),100));
		sv.reasper.setRounded(wsaaReasper);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readT54552100()
	{
		t54552110();
	}

protected void t54552110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5455);
		itemIO.setItemitem(rasaIO.getReasst());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.rasnumErr.set(errorsInner.r050);
		}
		else {
			t5455rec.t5455Rec.set(itemIO.getGenarea());
			if (isEQ(t5455rec.cesnind,SPACES)) {
				sv.rasnumErr.set(errorsInner.r061);
			}
		}
		cltsIO.setClntnum(rasaIO.getClntnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),"1")) {
			sv.clntnameErr.set(errorsInner.e946);
		}
		plainname();
		sv.clntname.set(wsspcomn.longconfname);
	}

protected void readT54492200()
	{
			t54492210();
		}

protected void t54492210()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(sv.rngmnt);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5449)
		|| isNE(itdmIO.getItemitem(),sv.rngmnt)) {
			sv.rngmntErr.set(errorsInner.r057);
			return ;
		}
		t5449rec.t5449Rec.set(itdmIO.getGenarea());
		if (isNE(t5449rec.lrkcls,sv.lrkcls)) {
			sv.rngmntErr.set(errorsInner.r063);
			return ;
		}
		if (isNE(sv.retype,t5449rec.retype)) {
			sv.retypeErr.set(errorsInner.r071);
			return ;
			/****        GO TO 2080-CHECK-FOR-ERRORS                            */
		}
		if (isNE(sv.rasnum,SPACES)
		&& isEQ(sv.rasnumErr,SPACES)) {
			for (wsaaT5449Ix.set(1); !(isGT(wsaaT5449Ix,10)
			|| reassurerFound.isTrue()); wsaaT5449Ix.add(1)){
				if (isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()],sv.rasnum)) {
					wsaaReassurerFound.set("Y");
				}
			}
			if (isGT(wsaaT5449Ix,10)) {
				sv.rasnumErr.set(errorsInner.r059);
			}
		}
	}

protected void getReviewDate2300()
	{
		reviewDate2310();
	}

protected void reviewDate2310()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate2.set(sv.cmdate);
		datcon3rec.frequency.set(t5448rec.rrevfrq);
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(t5448rec.rrevfrq);
		compute(datcon2rec.freqFactor, 5).set(add(1,datcon3rec.freqFactor));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
	}

protected void protectScr2400()
	{
		/*PROTECT*/
		sv.rasnumOut[varcom.pr.toInt()].set("Y");
		sv.retypeOut[varcom.pr.toInt()].set("Y");
		sv.raamountOut[varcom.pr.toInt()].set("Y");
		sv.reasperOut[varcom.pr.toInt()].set("Y");
		sv.lrkclsOut[varcom.pr.toInt()].set("Y");
		sv.rngmntOut[varcom.pr.toInt()].set("Y");
		
		/*EXIT*/
	}

protected void readTh6182600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					th6182610();
					validateLrkcls2630();
				case validLrkcls2640: 
					validLrkcls2640();
				case exit2690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void th6182610()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(th618);
		itdmIO.setItemitem(t5448rec.rrsktyp);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),th618)
		|| isNE(itdmIO.getItemitem(),t5448rec.rrsktyp)) {
			sv.lrkclsErr.set(errorsInner.rl30);
			goTo(GotoLabel.exit2690);
			/****      GO TO 2290-EXIT                                  <S19FIX>*/
		}
		th618rec.th618Rec.set(itdmIO.getGenarea());
	}

protected void validateLrkcls2630()
	{
		for (wsaaTh618Ix.set(1); !(isGT(wsaaTh618Ix,5)); wsaaTh618Ix.add(1)){
			if (isEQ(th618rec.lrkcls[wsaaTh618Ix.toInt()],sv.lrkcls)) {
				goTo(GotoLabel.validLrkcls2640);
			}
		}
	}

protected void validLrkcls2640()
	{
		if (isGT(wsaaTh618Ix,5)) {
			sv.lrkclsErr.set(errorsInner.rl30);
		}
	}

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"C")) {
			writeNewRacd3200();
			callActvres3300();
			experienceUpdate3500();
		}
		if (isEQ(wsspcomn.flag,"M")) {
			rlseRacdmja3100();
			deleteRacd3400();
			experienceUpdate3500();
			writeNewRacd3200();
			callActvres3300();
		}
		if (isEQ(wsspcomn.flag,"D")) {
			rlseRacdmja3100();
			deleteRacd3400();
			experienceUpdate3500();
		}
	}

protected void rlseRacdmja3100()
	{
		/*RELEASED*/
		racdmjaIO.setFormat(racdmjarec);
		racdmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writeNewRacd3200()
	{
		starts3210();
	}

protected void starts3210()
	{
		racdIO.setCurrfrom(wssplife.effdate);
		racdIO.setCurrto(varcom.vrcmMaxDate);
		racdIO.setRasnum(sv.rasnum);
		setPrecision(racdIO.getTranno(), 0);
		racdIO.setTranno(add(1,chdrmjaIO.getTranno()));
		racdIO.setRngmnt(sv.rngmnt);
		racdIO.setCurrcode(sv.currcode);
		racdIO.setRetype(sv.retype);
		racdIO.setRaAmount(sv.raAmount);
		racdIO.setCtdate(sv.ctdate);
		racdIO.setCmdate(sv.cmdate);
		racdIO.setReasper(sv.reasper);
		racdIO.setRrevdt(sv.rrevdt);
		racdIO.setRecovamt(sv.recovamt);
		racdIO.setOvrdind("Y");
		racdIO.setValidflag("3");
		racdIO.setChdrcoy(racdmjaIO.getChdrcoy());
		racdIO.setChdrnum(racdmjaIO.getChdrnum());
		racdIO.setLife(racdmjaIO.getLife());
		racdIO.setCoverage(racdmjaIO.getCoverage());
		racdIO.setRider(racdmjaIO.getRider());
		racdIO.setPlanSuffix(racdmjaIO.getPlanSuffix());
		racdIO.setSeqno(ZERO);
		racdIO.setLrkcls(sv.lrkcls);
		if (isEQ(racdIO.getRetype(),"F")) {
			racdIO.setCestype("2");
		}
		else {
			racdIO.setCestype("1");
		}
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			fatalError600();
		}
	}

protected void callActvres3300()
	{
		activate3310();
	}

protected void activate3310()
	{
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(chdrmjaIO.getChdrnum());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		compute(actvresrec.tranno, 0).set(add(1,chdrmjaIO.getTranno()));
		actvresrec.life.set(covrlnbIO.getLife());
		actvresrec.coverage.set(covrlnbIO.getCoverage());
		actvresrec.rider.set(covrlnbIO.getRider());
		actvresrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		actvresrec.crtable.set(covrlnbIO.getCrtable());
		actvresrec.effdate.set(wssplife.effdate);
		actvresrec.clntcoy.set(wsspcomn.fsuco);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.jlife.set(covrlnbIO.getJlife());
		actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		actvresrec.oldSumins.set(covrlnbIO.getSumins());
		actvresrec.newSumins.set(covrlnbIO.getSumins());
		actvresrec.function.set("ACT8");
		actvresrec.crrcd.set(covrlnbIO.getCrrcd());
		actvresrec.language.set(wsspcomn.language);
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			fatalError600();
		}
	}

protected void deleteRacd3400()
	{
		delete3410();
	}

protected void delete3410()
	{
		trmracdrec.trmracdRec.set(SPACES);
		trmracdrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		trmracdrec.chdrnum.set(racdmjaIO.getChdrnum());
		trmracdrec.life.set(racdmjaIO.getLife());
		trmracdrec.coverage.set(racdmjaIO.getCoverage());
		trmracdrec.rider.set(racdmjaIO.getRider());
		trmracdrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		trmracdrec.crtable.set(covrlnbIO.getCrtable());
		trmracdrec.cnttype.set(chdrmjaIO.getCnttype());
		trmracdrec.effdate.set(wssplife.effdate);
		trmracdrec.newRaAmt.set(ZERO);
		trmracdrec.recovamt.set(ZERO);
		trmracdrec.lrkcls.set(racdmjaIO.getLrkcls());
		compute(trmracdrec.tranno, 0).set(add(1,chdrmjaIO.getTranno()));
		trmracdrec.seqno.set(racdmjaIO.getSeqno());
		trmracdrec.clntcoy.set(wsspcomn.fsuco);
		trmracdrec.jlife.set(covrlnbIO.getJlife());
		trmracdrec.l1Clntnum.set(wsaaL1Clntnum);
		trmracdrec.l2Clntnum.set(wsaaL2Clntnum);
		trmracdrec.crrcd.set(covrlnbIO.getCrrcd());
		trmracdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		trmracdrec.function.set("TRMR");
		callProgram(Trmracd.class, trmracdrec.trmracdRec);
		if (isNE(trmracdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(trmracdrec.statuz);
			syserrrec.params.set(trmracdrec.trmracdRec);
			fatalError600();
		}
	}

protected void experienceUpdate3500()
	{
			update3510();
		}

protected void update3510()
	{
		if (isEQ(wsspcomn.flag,"M")
		&& isEQ(racdmjaIO.getRaAmount(),sv.raAmount)) {
			return ;
		}
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(covrlnbIO.getChdrcoy());
		rexpupdrec.chdrnum.set(covrlnbIO.getChdrnum());
		rexpupdrec.life.set(covrlnbIO.getLife());
		rexpupdrec.coverage.set(covrlnbIO.getCoverage());
		rexpupdrec.rider.set(covrlnbIO.getRider());
		rexpupdrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		compute(rexpupdrec.tranno, 0).set(add(1,chdrmjaIO.getTranno()));
		rexpupdrec.clntcoy.set(wsspcomn.fsuco);
		rexpupdrec.l1Clntnum.set(wsaaL1Clntnum);
		if (isNE(covrlnbIO.getJlife(),"00")) {
			rexpupdrec.l2Clntnum.set(wsaaL2Clntnum);
		}
		rexpupdrec.sumins.set(racdmjaIO.getRaAmount());
		readT54463600();
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(wssplife.effdate);
		rexpupdrec.riskClass.set(t5448rec.rrsktyp);
		rexpupdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		if (isEQ(wsspcomn.flag,"M")) {
			if (isGT(racdmjaIO.getRaAmount(),sv.raAmount)) {
				rexpupdrec.function.set("INCR");
				compute(rexpupdrec.sumins, 2).set(sub(racdmjaIO.getRaAmount(),sv.raAmount));
			}
			else {
				rexpupdrec.function.set("DECR");
				compute(rexpupdrec.sumins, 2).set(sub(sv.raAmount,racdmjaIO.getRaAmount()));
			}
		}
		else {
			if (isEQ(wsspcomn.flag,"C")) {
				rexpupdrec.function.set("DECR");
				rexpupdrec.sumins.set(sv.raAmount);
			}
			else {
				if (isEQ(wsspcomn.flag,"D")) {
					rexpupdrec.function.set("INCR");
					rexpupdrec.sumins.set(racdmjaIO.getRaAmount());
				}
			}
		}
		if (isNE(rexpupdrec.currency,racdmjaIO.getCurrcode())) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(racdmjaIO.getCurrcode());
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt3700();
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			fatalError600();
		}
	}

protected void readT54463600()
	{
		t54463610();
	}

protected void t54463610()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5446);
		// #MIBT-225, t5448rec.rrsktyp is risk type but t5446 contain risk class
		// itdmIO.setItemitem(t5448rec.rrsktyp);
		itdmIO.setItemitem(sv.lrkcls);		
		// #MIBT-225, t5448rec.rrsktyp is risk type but t5446 contain risk class ENDS
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		// #MIBT-225, t5448rec.rrsktyp is risk type but t5446 contain risk class
		//if (isNE(itdmIO.getItemitem(),t5448rec.rrsktyp)		
		if (isNE(itdmIO.getItemitem(),sv.lrkcls)
		// #MIBT-225, t5448rec.rrsktyp is risk type but t5446 contain risk class ENDS
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5446)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(t5448rec.rrsktyp);
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callXcvrt3700()
	{
		/*CALL*/
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(wssplife.effdate);
		conlinkrec.company.set(chdrmjaIO.getChdrcoy());
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
		/**  END OF CONFNAME ***********************************************/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e350 = new FixedLengthStringData(4).init("E350");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e946 = new FixedLengthStringData(4).init("E946");
	private FixedLengthStringData f399 = new FixedLengthStringData(4).init("F399");
	private FixedLengthStringData h090 = new FixedLengthStringData(4).init("H090");
	private FixedLengthStringData r050 = new FixedLengthStringData(4).init("R050");
	private FixedLengthStringData r057 = new FixedLengthStringData(4).init("R057");
	private FixedLengthStringData r059 = new FixedLengthStringData(4).init("R059");
	private FixedLengthStringData r060 = new FixedLengthStringData(4).init("R060");
	private FixedLengthStringData r061 = new FixedLengthStringData(4).init("R061");
	private FixedLengthStringData r063 = new FixedLengthStringData(4).init("R063");
	private FixedLengthStringData r071 = new FixedLengthStringData(4).init("R071");
	private FixedLengthStringData rl30 = new FixedLengthStringData(4).init("RL30");
	}
}
