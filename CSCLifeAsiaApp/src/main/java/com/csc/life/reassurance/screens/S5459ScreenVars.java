package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5459
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5459ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(333);
	public FixedLengthStringData dataFields = new FixedLengthStringData(141).isAPartOf(dataArea, 0);
	public FixedLengthStringData optdscx = DD.optdscx.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData optdscs = new FixedLengthStringData(45).isAPartOf(dataFields, 60);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(3, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData suminss = new FixedLengthStringData(34).isAPartOf(dataFields, 107);
	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(2, 17, 2, suminss, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(34).isAPartOf(suminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumins01 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData sumins02 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,17);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 141);
	public FixedLengthStringData optdscxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(3, 4, optdscsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData suminssErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] suminsErr = FLSArrayPartOfStructure(2, 4, suminssErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(suminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumins01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData sumins02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 189);
	public FixedLengthStringData[] optdscxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(3, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData suminssOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(2, 12, suminssOut, 0);
	public FixedLengthStringData[][] suminsO = FLSDArrayPartOfArrayStructure(12, 1, suminsOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(suminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumins01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] sumins02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(162);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(48).isAPartOf(subfileArea, 0);
	public ZonedDecimalData cmdate = DD.cmdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData hrrn = DD.hrrn.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData ovrdind = DD.ovrdind.copy().isAPartOf(subfileFields,17);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(subfileFields,35);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(subfileFields,43);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,47);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 48);
	public FixedLengthStringData cmdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hrrnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData ovrdindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 76);
	public FixedLengthStringData[] cmdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hrrnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] ovrdindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 160);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData cmdateDisp = new FixedLengthStringData(10);

	public LongData S5459screensflWritten = new LongData(0);
	public LongData S5459screenctlWritten = new LongData(0);
	public LongData S5459screenWritten = new LongData(0);
	public LongData S5459protectWritten = new LongData(0);
	public GeneralTable s5459screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5459screensfl;
	}

	public S5459ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hrrn, cmdate, rasnum, rngmnt, raAmount, ovrdind, select};
		screenSflOutFields = new BaseData[][] {hrrnOut, cmdateOut, rasnumOut, rngmntOut, raamountOut, ovrdindOut, selectOut};
		screenSflErrFields = new BaseData[] {hrrnErr, cmdateErr, rasnumErr, rngmntErr, raamountErr, ovrdindErr, selectErr};
		screenSflDateFields = new BaseData[] {cmdate};
		screenSflDateErrFields = new BaseData[] {cmdateErr};
		screenSflDateDispFields = new BaseData[] {cmdateDisp};

		screenFields = new BaseData[] {chdrnum, sumins01, life, coverage, rider, sumins02, optdsc01, optdsc02, optdsc03, cnttype, ctypedes, optdscx};
		screenOutFields = new BaseData[][] {chdrnumOut, sumins01Out, lifeOut, coverageOut, riderOut, sumins02Out, optdsc01Out, optdsc02Out, optdsc03Out, cnttypeOut, ctypedesOut, optdscxOut};
		screenErrFields = new BaseData[] {chdrnumErr, sumins01Err, lifeErr, coverageErr, riderErr, sumins02Err, optdsc01Err, optdsc02Err, optdsc03Err, cnttypeErr, ctypedesErr, optdscxErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5459screen.class;
		screenSflRecord = S5459screensfl.class;
		screenCtlRecord = S5459screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5459protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5459screenctl.lrec.pageSubfile);
	}
}
