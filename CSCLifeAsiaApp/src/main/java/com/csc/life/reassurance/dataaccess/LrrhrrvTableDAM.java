package com.csc.life.reassurance.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LrrhrrvTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:01
 * Class transformed from LRRHRRV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LrrhrrvTableDAM extends LrrhpfTableDAM {

	public LrrhrrvTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LRRHRRV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CLNTPFX"
		             + ", CLNTCOY"
		             + ", CLNTNUM"
		             + ", COMPANY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", LRKCLS"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "CLNTPFX, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "COMPANY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "LRKCLS, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "CURRENCY, " +
		            "SSRETN, " +
		            "SSREAST, " +
		            "SSREASF, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CLNTPFX ASC, " +
		            "CLNTCOY ASC, " +
		            "CLNTNUM ASC, " +
		            "COMPANY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "LRKCLS ASC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CLNTPFX DESC, " +
		            "CLNTCOY DESC, " +
		            "CLNTNUM DESC, " +
		            "COMPANY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "LRKCLS DESC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobName,
                               userProfile,
                               datime,
                               clntpfx,
                               clntcoy,
                               clntnum,
                               company,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               lrkcls,
                               currfrom,
                               currto,
                               validflag,
                               tranno,
                               currency,
                               ssretn,
                               ssreast,
                               ssreasf,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(28);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getClntpfx().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getCompany().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLrkcls().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, clntpfx);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, lrkcls);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller170 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller40.setInternal(clntpfx.toInternal());
	nonKeyFiller50.setInternal(clntcoy.toInternal());
	nonKeyFiller60.setInternal(clntnum.toInternal());
	nonKeyFiller70.setInternal(company.toInternal());
	nonKeyFiller80.setInternal(chdrnum.toInternal());
	nonKeyFiller90.setInternal(life.toInternal());
	nonKeyFiller100.setInternal(coverage.toInternal());
	nonKeyFiller110.setInternal(rider.toInternal());
	nonKeyFiller120.setInternal(planSuffix.toInternal());
	nonKeyFiller130.setInternal(lrkcls.toInternal());
	nonKeyFiller170.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(123);
		
		nonKeyData.set(
					getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getValidflag().toInternal()
					+ nonKeyFiller170.toInternal()
					+ getCurrency().toInternal()
					+ getSsretn().toInternal()
					+ getSsreast().toInternal()
					+ getSsreasf().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, nonKeyFiller170);
			what = ExternalData.chop(what, currency);
			what = ExternalData.chop(what, ssretn);
			what = ExternalData.chop(what, ssreast);
			what = ExternalData.chop(what, ssreasf);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(Object what) {
		clntpfx.set(what);
	}
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}
	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLrkcls() {
		return lrkcls;
	}
	public void setLrkcls(Object what) {
		lrkcls.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getCurrency() {
		return currency;
	}
	public void setCurrency(Object what) {
		currency.set(what);
	}	
	public PackedDecimalData getSsretn() {
		return ssretn;
	}
	public void setSsretn(Object what) {
		setSsretn(what, false);
	}
	public void setSsretn(Object what, boolean rounded) {
		if (rounded)
			ssretn.setRounded(what);
		else
			ssretn.set(what);
	}	
	public PackedDecimalData getSsreast() {
		return ssreast;
	}
	public void setSsreast(Object what) {
		setSsreast(what, false);
	}
	public void setSsreast(Object what, boolean rounded) {
		if (rounded)
			ssreast.setRounded(what);
		else
			ssreast.set(what);
	}	
	public PackedDecimalData getSsreasf() {
		return ssreasf;
	}
	public void setSsreasf(Object what) {
		setSsreasf(what, false);
	}
	public void setSsreasf(Object what, boolean rounded) {
		if (rounded)
			ssreasf.setRounded(what);
		else
			ssreasf.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		clntpfx.clear();
		clntcoy.clear();
		clntnum.clear();
		company.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		lrkcls.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		jobName.clear();
		userProfile.clear();
		datime.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		currfrom.clear();
		currto.clear();
		validflag.clear();
		nonKeyFiller170.clear();
		currency.clear();
		ssretn.clear();
		ssreast.clear();
		ssreasf.clear();		
	}


}