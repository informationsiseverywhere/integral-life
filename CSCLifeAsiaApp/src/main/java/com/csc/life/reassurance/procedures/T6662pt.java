/*
 * File: T6662pt.java
 * Date: 30 August 2009 2:29:42
 * Author: Quipoz Limited
 * 
 * Class transformed from T6662PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.reassurance.tablestructures.T6662rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6662.
*
*
*
***********************************************************************
* </pre>
*/
public class T6662pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Valid Reassurance Codes for Component           S6662");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(58);
	private FixedLengthStringData filler9 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 22);
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 27);
	private FixedLengthStringData filler11 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine004, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 43);
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 48);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(58);
	private FixedLengthStringData filler13 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 22);
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 27);
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 43);
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 48);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(58);
	private FixedLengthStringData filler17 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 22);
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 27);
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 43);
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 48);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(58);
	private FixedLengthStringData filler21 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 22);
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 27);
	private FixedLengthStringData filler23 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 43);
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 48);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(58);
	private FixedLengthStringData filler25 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 22);
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 27);
	private FixedLengthStringData filler27 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 43);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 48);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(58);
	private FixedLengthStringData filler29 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 22);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 27);
	private FixedLengthStringData filler31 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 43);
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 48);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(58);
	private FixedLengthStringData filler33 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 22);
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 27);
	private FixedLengthStringData filler35 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 43);
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 48);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(58);
	private FixedLengthStringData filler37 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 22);
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 27);
	private FixedLengthStringData filler39 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 43);
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 48);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(58);
	private FixedLengthStringData filler41 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 22);
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 27);
	private FixedLengthStringData filler43 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 43);
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 48);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(58);
	private FixedLengthStringData filler45 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 22);
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 27);
	private FixedLengthStringData filler47 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 43);
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 48);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(58);
	private FixedLengthStringData filler49 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 22);
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 27);
	private FixedLengthStringData filler51 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 43);
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 48);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(58);
	private FixedLengthStringData filler53 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 22);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 27);
	private FixedLengthStringData filler55 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 43);
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 48);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(58);
	private FixedLengthStringData filler57 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 22);
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 27);
	private FixedLengthStringData filler59 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 43);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 48);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(58);
	private FixedLengthStringData filler61 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 22);
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine017, 27);
	private FixedLengthStringData filler63 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 43);
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine017, 48);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(58);
	private FixedLengthStringData filler65 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 22);
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine018, 27);
	private FixedLengthStringData filler67 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 43);
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine018, 48);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6662rec t6662rec = new T6662rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6662pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6662rec.t6662Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6662rec.reascode01);
		fieldNo008.set(t6662rec.shortdesc01);
		fieldNo009.set(t6662rec.reascode02);
		fieldNo010.set(t6662rec.shortdesc02);
		fieldNo011.set(t6662rec.reascode03);
		fieldNo012.set(t6662rec.shortdesc03);
		fieldNo013.set(t6662rec.reascode04);
		fieldNo014.set(t6662rec.shortdesc04);
		fieldNo015.set(t6662rec.reascode05);
		fieldNo016.set(t6662rec.shortdesc05);
		fieldNo017.set(t6662rec.reascode06);
		fieldNo018.set(t6662rec.shortdesc06);
		fieldNo019.set(t6662rec.reascode07);
		fieldNo020.set(t6662rec.shortdesc07);
		fieldNo021.set(t6662rec.reascode08);
		fieldNo022.set(t6662rec.shortdesc08);
		fieldNo023.set(t6662rec.reascode09);
		fieldNo024.set(t6662rec.shortdesc09);
		fieldNo025.set(t6662rec.reascode10);
		fieldNo026.set(t6662rec.shortdesc10);
		fieldNo027.set(t6662rec.reascode11);
		fieldNo028.set(t6662rec.shortdesc11);
		fieldNo029.set(t6662rec.reascode12);
		fieldNo030.set(t6662rec.shortdesc12);
		fieldNo031.set(t6662rec.reascode13);
		fieldNo032.set(t6662rec.shortdesc13);
		fieldNo033.set(t6662rec.reascode14);
		fieldNo034.set(t6662rec.shortdesc14);
		fieldNo035.set(t6662rec.reascode15);
		fieldNo036.set(t6662rec.shortdesc15);
		fieldNo037.set(t6662rec.reascode16);
		fieldNo038.set(t6662rec.shortdesc16);
		fieldNo039.set(t6662rec.reascode17);
		fieldNo040.set(t6662rec.shortdesc17);
		fieldNo041.set(t6662rec.reascode18);
		fieldNo042.set(t6662rec.shortdesc18);
		fieldNo043.set(t6662rec.reascode19);
		fieldNo044.set(t6662rec.shortdesc19);
		fieldNo045.set(t6662rec.reascode20);
		fieldNo046.set(t6662rec.shortdesc20);
		fieldNo047.set(t6662rec.reascode21);
		fieldNo048.set(t6662rec.shortdesc21);
		fieldNo049.set(t6662rec.reascode22);
		fieldNo050.set(t6662rec.shortdesc22);
		fieldNo051.set(t6662rec.reascode23);
		fieldNo052.set(t6662rec.shortdesc23);
		fieldNo053.set(t6662rec.reascode24);
		fieldNo054.set(t6662rec.shortdesc24);
		fieldNo055.set(t6662rec.reascode25);
		fieldNo056.set(t6662rec.shortdesc25);
		fieldNo057.set(t6662rec.reascode26);
		fieldNo058.set(t6662rec.shortdesc26);
		fieldNo059.set(t6662rec.reascode27);
		fieldNo060.set(t6662rec.shortdesc27);
		fieldNo061.set(t6662rec.reascode28);
		fieldNo062.set(t6662rec.shortdesc28);
		fieldNo063.set(t6662rec.reascode29);
		fieldNo064.set(t6662rec.shortdesc29);
		fieldNo065.set(t6662rec.reascode30);
		fieldNo066.set(t6662rec.shortdesc30);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
