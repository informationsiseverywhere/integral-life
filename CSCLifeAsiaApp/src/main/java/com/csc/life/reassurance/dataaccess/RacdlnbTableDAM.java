package com.csc.life.reassurance.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RacdlnbTableDAM.java
 * Date: Sun, 30 Aug 2009 03:45:12
 * Class transformed from RACDLNB.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RacdlnbTableDAM extends RacdpfTableDAM {

	public RacdlnbTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("RACDLNB");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", SEQNO"
		             + ", CESTYPE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "RASNUM, " +
		            "TRANNO, " +
		            "SEQNO, " +
		            "VALIDFLAG, " +
		            "CURRCODE, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "RETYPE, " +
		            "RNGMNT, " +
		            "RAAMOUNT, " +
		            "CTDATE, " +
		            "CMDATE, " +
		            "REASPER, " +
		            "RREVDT, " +
		            "RECOVAMT, " +
		            "CESTYPE, " +
		            "OVRDIND, " +
		            "LRKCLS, " +
		            "RRTDAT,"+
		            "RRTFRM,"+
		            "PENDCSTTO, " +
		            "PRORATEFLAG, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "SEQNO ASC, " +
		            "CESTYPE ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "SEQNO DESC, " +
		            "CESTYPE DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               rasnum,
                               tranno,
                               seqno,
                               validflag,
                               currcode,
                               currfrom,
                               currto,
                               retype,
                               rngmnt,
                               raAmount,
                               ctdate,
                               cmdate,
                               reasper,
                               rrevdt,
                               recovamt,
                               cestype,
                               ovrdind,
                               lrkcls,
                               rrtdat,
                               rrtfrm,
                               pendcstto,
                               prorateflag,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(43);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getSeqno().toInternal()
					+ getCestype().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, cestype);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller220 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());
	nonKeyFiller90.setInternal(seqno.toInternal());
	nonKeyFiller220.setInternal(cestype.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(163);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getRasnum().toInternal()
					+ getTranno().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getValidflag().toInternal()
					+ getCurrcode().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getRetype().toInternal()
					+ getRngmnt().toInternal()
					+ getRaAmount().toInternal()
					+ getCtdate().toInternal()
					+ getCmdate().toInternal()
					+ getReasper().toInternal()
					+ getRrevdt().toInternal()
					+ getRecovamt().toInternal()
					+ nonKeyFiller220.toInternal()
					+ getOvrdind().toInternal()
					+ getLrkcls().toInternal()
					+ getRrtdat().toInternal()
					+ getRrtfrm().toInternal()
					+ getPendcstto().toInternal()
					+ getProrateflag().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, rasnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, retype);
			what = ExternalData.chop(what, rngmnt);
			what = ExternalData.chop(what, raAmount);
			what = ExternalData.chop(what, ctdate);
			what = ExternalData.chop(what, cmdate);
			what = ExternalData.chop(what, reasper);
			what = ExternalData.chop(what, rrevdt);
			what = ExternalData.chop(what, recovamt);
			what = ExternalData.chop(what, nonKeyFiller220);
			what = ExternalData.chop(what, ovrdind);
			what = ExternalData.chop(what, lrkcls);
			what = ExternalData.chop(what, rrtdat);
			what = ExternalData.chop(what, rrtfrm);
			what = ExternalData.chop(what, pendcstto);
			what = ExternalData.chop(what, prorateflag);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}
	public FixedLengthStringData getCestype() {
		return cestype;
	}
	public void setCestype(Object what) {
		cestype.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getRasnum() {
		return rasnum;
	}
	public void setRasnum(Object what) {
		rasnum.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getRetype() {
		return retype;
	}
	public void setRetype(Object what) {
		retype.set(what);
	}	
	public FixedLengthStringData getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(Object what) {
		rngmnt.set(what);
	}	
	public PackedDecimalData getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(Object what) {
		setRaAmount(what, false);
	}
	public void setRaAmount(Object what, boolean rounded) {
		if (rounded)
			raAmount.setRounded(what);
		else
			raAmount.set(what);
	}	
	public PackedDecimalData getCtdate() {
		return ctdate;
	}
	public void setCtdate(Object what) {
		setCtdate(what, false);
	}
	public void setCtdate(Object what, boolean rounded) {
		if (rounded)
			ctdate.setRounded(what);
		else
			ctdate.set(what);
	}	
	public PackedDecimalData getCmdate() {
		return cmdate;
	}
	public void setCmdate(Object what) {
		setCmdate(what, false);
	}
	public void setCmdate(Object what, boolean rounded) {
		if (rounded)
			cmdate.setRounded(what);
		else
			cmdate.set(what);
	}	
	public PackedDecimalData getReasper() {
		return reasper;
	}
	public void setReasper(Object what) {
		setReasper(what, false);
	}
	public void setReasper(Object what, boolean rounded) {
		if (rounded)
			reasper.setRounded(what);
		else
			reasper.set(what);
	}	
	public PackedDecimalData getRrevdt() {
		return rrevdt;
	}
	public void setRrevdt(Object what) {
		setRrevdt(what, false);
	}
	public void setRrevdt(Object what, boolean rounded) {
		if (rounded)
			rrevdt.setRounded(what);
		else
			rrevdt.set(what);
	}	
	public PackedDecimalData getRecovamt() {
		return recovamt;
	}
	public void setRecovamt(Object what) {
		setRecovamt(what, false);
	}
	public void setRecovamt(Object what, boolean rounded) {
		if (rounded)
			recovamt.setRounded(what);
		else
			recovamt.set(what);
	}	
	public FixedLengthStringData getOvrdind() {
		return ovrdind;
	}
	public void setOvrdind(Object what) {
		ovrdind.set(what);
	}	
	public FixedLengthStringData getLrkcls() {
		return lrkcls;
	}
	public void setLrkcls(Object what) {
		lrkcls.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getRrtdat() {
		return rrtdat;
	}
	public void setRrtdat(Object what) {
		rrtdat.set(what);
	}
	
	public PackedDecimalData getRrtfrm() {
		return rrtfrm;
	}
	public void setRrtfrm(Object what) {
		rrtfrm.set(what);
	}
	public PackedDecimalData getPendcstto() {
		return pendcstto;
	}
	public void setPendcstto(Object what) {
		pendcstto.set(what);
	}
	public FixedLengthStringData getProrateflag() {
		return prorateflag;
	}
	public void setProrateflag(Object what) {
		prorateflag.set(what);
	}
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		seqno.clear();
		cestype.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		rasnum.clear();
		tranno.clear();
		nonKeyFiller90.clear();
		validflag.clear();
		currcode.clear();
		currfrom.clear();
		currto.clear();
		retype.clear();
		rngmnt.clear();
		raAmount.clear();
		ctdate.clear();
		cmdate.clear();
		reasper.clear();
		rrevdt.clear();
		recovamt.clear();
		nonKeyFiller220.clear();
		ovrdind.clear();
		lrkcls.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		rrtdat.clear();
		rrtfrm.clear();
		pendcstto.clear();
		prorateflag.clear();
	}


}