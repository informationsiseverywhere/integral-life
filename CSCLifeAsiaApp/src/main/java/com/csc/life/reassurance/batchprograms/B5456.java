/*
 * File: B5456.java
 * Date: 29 August 2009 21:17:09
 * Author: Quipoz Limited
 *
 * Class transformed from B5456.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.dataaccess.dao.B5456TempDAO;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.dao.RecopfDAO;
import com.csc.life.reassurance.dataaccess.model.B5456DTO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.procedures.RecopstPojo;
import com.csc.life.reassurance.procedures.RecopstUtils;
import com.csc.life.reassurance.recordstructures.Restdtyrec;
import com.csc.life.reassurance.recordstructures.Rprmiumrec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.life.reassurance.tablestructures.T5450rec;
import com.csc.life.reassurance.tablestructures.Th621rec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Jctlkey;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * REMARKS.
 * 
 *   B5456 - Reassurance Costing Batch program
 *   -----------------------------------------
 * 
 * 
 *   Overview.
 *   ---------
 * 
 *   Each tranche of reassurance may be  related to a distinct
 *   reassurance arrangement and thus may be subject to different
 *   premium calculation & rates, commission & Taxation.
 * 
 *   This program will create the  collections  from  reassurance
 *   coverage  type  records.  The  output  from this will be the
 *   appropriate ACMV records that reflect the  monies  due  from
 *   each reassurance component.
 * 
 *   Reassurance  premiums  will  be  billed  in advance and will
 *   cover the period from the current billed to date to  a  date
 *   one Costing frequency in advance.
 * 
 *   The Reassurance Cession Deatils file, (RACD), will be used as
 *   the driver for this file. RACD records that require
 *   processing will be selected by SQL executed from within
 *   this program.
 * 
 *   The premium will be re-calculated depending on the reassurance
 *   arrangement.
 *   There are 2 types of the premiums - Original Terms and
 *   regular.
 * 
 *   The SQL selection criteria for this program are as follows:
 * 
 * 
 *        SELECT:
 *        -------
 * 
 *        . RACD-CHDRCOY   = Batch Run Company
 *        . RACD-VALIDFLAG = '1'
 *        . RACD-CTDATE    is less than or equal to the Batch
 *                         Effective Date
 * 
 *        KEY:
 *        ----
 * 
 *        . RACD-CHDRCOY
 *        . RACD-CHDRNUM
 *        . RACD-LIFE
 *        . RACD-COVERAGE
 *        . RACD-RIDER
 *        . RACD-PLNSFX
 *        . RACD-SEQNO
 * 
 *   The following control totals will be maintained within  this
 *   job:
 * 
 *        1.   RACD Records extracded
 *        2.   Total Premiums Costed
 *        3.   Total Commission
 *        4.   Total Tax Paidon
 *        5.   Total Net Premium
 * 
 *   Control totals 2, 3, 4 and 5 will be automatically maintained
 *   by the LIFACMV subroutine which will be called  to  add  the
 *   ACMV records.
 * 
 *   A PTRn Record will be written and the Contract Header will
 *   be rewritten with an incremented TRANNO.
 * 
 *   The RACD records which have been processed will be rewritten
 *   with validflag = 2, and a new RACD will be created with
 *   the old record detail and validflag = 1 and new trans number
 *   and the CTDATE to be the old CTDATE plus 1 frequency.
 *
 *****************************************************************
 * </pre>
 */
public class B5456 extends Mainb {

    public static final String ROUTINE = QPUtilities.getThisClass();
    public int numberOfParameters = 0;
    private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5456");
    private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
    /*
     * These fields are required by MAINB processing and should not be deleted.
     */
    private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
    private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
    private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
    private String wsaaChdrnumfrm;
    private String wsaaChdrnumto;
    private String wsaaChdrcoy = "";
    private String wsaaChdrnum = "";
    private int wsaaTranno;
    private static final int wsaaT5449Size = 10;
    private BigDecimal wsaaNumPremInsts;
    /* WSAA-FLAGS */
    private String wsaaIgnoreContracd = "";

    private boolean validContractFlag = true;
    private boolean newContractFlag = true;
    /* WSAA-LIFE-DETAILS */
    private int wsaaAge;
    private String wsaaSex;
    /* WSBB-LIFE-DETAILS */
    private int wsbbAge;
    private String wsbbSex;

    private boolean useJointLifeFlag = false;
    private String wsaaCompany;
    private int wsaaSqlEffdate;
    private int wsxxSub;
    private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);

    private String wsaaTh621Items;
    /* ERRORS */
    private static final String h021 = "H021";
    private static final String r055 = "R055";
    private static final String r057 = "R057";
    private static final String r072 = "R072";
    private static final String r074 = "R074";
    private static final String rl29 = "RL29";
    /* TABLES */
    private static final String t1688 = "T1688";
    private static final String t5448 = "T5448";
    private static final String t5449 = "T5449";
    private static final String t5450 = "T5450";
    private static final String t5647 = "T5647";
    private static final String t5679 = "T5679";
    private static final String th621 = "TH621";
    /* CONTROL-TOTALS */
    private static final int ct01 = 1;
    private static final int ct02 = 2;
    private static final int ct03 = 3;
    private static final int ct04 = 4;
    private static final int ct05 = 5;
    private static final int ct06 = 6;	//PINNACLE-2760

    private int ct01Count = 0;
    private BigDecimal ct02Prem = BigDecimal.ZERO;
    private BigDecimal ct03Comm = BigDecimal.ZERO;
    private BigDecimal ct04Tax = BigDecimal.ZERO;
    private BigDecimal ct05Net = BigDecimal.ZERO;
    private int ct06Sftlck = 0;	//PINNACLE-2760

    private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
    private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
    private Itemkey wsaaItemkey = new Itemkey();
    private Jctlkey wsaaJctlkey = new Jctlkey();
    private Datcon2rec datcon2rec = new Datcon2rec();
    private Datcon3rec datcon3rec = new Datcon3rec();
    private Datcon4rec datcon4rec = new Datcon4rec();
    private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
    private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
    private Freqcpy freqcpy = new Freqcpy();
    private Restdtyrec restdtyrec = new Restdtyrec();
    private Rprmiumrec rprmiumrec = new Rprmiumrec();
    private Srcalcpy srcalcpy = new Srcalcpy();
    private Comlinkrec comlinkrec = new Comlinkrec();
    private T5448rec t5448rec = new T5448rec();
    private T5449rec t5449rec = new T5449rec();
    private T5450rec t5450rec = new T5450rec();
    private T5647rec t5647rec = new T5647rec();
    private T5679rec t5679rec = new T5679rec();
    private T6598rec t6598rec = new T6598rec();
    private Th621rec th621rec = new Th621rec();
    private P6671par p6671par = new P6671par();
    private WsaaWorkAreasInner wsaaWorkAreasInner = new WsaaWorkAreasInner();
    private ExternalisedRules er = new ExternalisedRules();

    
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private RacdpfDAO racdDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
    private ChdrpfDAO chdrDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private B5456TempDAO b5456TempDAO = getApplicationContext().getBean("b5456TempDAO", B5456TempDAO.class);
    private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
    private Map<String, List<Itempf>> t5448Map = new HashMap<String, List<Itempf>>();
    private Map<String, List<Itempf>> t5449Map = new HashMap<String, List<Itempf>>();
    private Map<String, List<Itempf>> t5450Map = new HashMap<String, List<Itempf>>();
    private Map<String, List<Itempf>> t5647Map = new HashMap<String, List<Itempf>>();
    private Map<String, List<Itempf>> t6598Map = new HashMap<String, List<Itempf>>();
    private Map<String, List<Itempf>> th621Map = new HashMap<String, List<Itempf>>();
     
    private List<Chdrpf> chdrBulkOpList = null;
    private List<Ptrnpf> ptrnBulkOpList = null;
    private List<Racdpf> racdrskBulkOpList = null;
    private List<B5456DTO> racdSearchResultDtoList = null;
    private B5456DTO sqlRacdpfInner = null;
    private int minRecord = 1;
    private int incrRange;
    private Iterator<B5456DTO> iterator;
    private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
    
    private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
    private RecopstUtils recopstUtils = getApplicationContext().getBean("recopstUtils", RecopstUtils.class);
    private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
    private SftlockUtil sftlockUtil = new SftlockUtil();
    private SftlockRecBean sftlockRecBean;
    private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
    private AgntpfDAO agntpfDAO =  getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private boolean isTransEffDate;
	private String wsaarcstfrq;
	private int sqlCtDate; //PINNACLE-2054
	//PINNACLE-2297 start
	private int wsaaEndDate; 
	private RecopfDAO recopfDAO = getApplicationContext().getBean("recopfDAO", RecopfDAO.class);
	private int wsaaMinDay = 1;
	private int wsaaMaxDay = 13;
	private boolean isProrate;
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaPtdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPtdateYy = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaNextAnndate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaNextAnndate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextAnndateYy = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextAnndateMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaNextAnndateDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private boolean isAiaAusFreqChange;
	private boolean isAiaAusRerate;
	//PINNACLE-2297 end
	//PINNACLE-2684  START
	 private int wsaaPtrnTranno;
	 private String fortnight = "26";
	 private Chdrpf chdrPojo; 
	//PINNACLE-2684  END
	//PINNACLE-2902 START
	 private String origRcstfrq = "";
	 Racdpf racdpf = null;
	//PINNACLE-2902 END
    /**
     * Contains all possible labels used by goTo action.
     */
    private enum GotoLabel implements GOTOInterface {
        DEFAULT, eof2080, exit2090, readhRacd2502, exit2509, origTerms2615, exit2619, calcCommission3040
    }

    public B5456() {
        super();
    }

    protected FixedLengthStringData getWsaaProg() {
        return wsaaProg;
    }

    protected PackedDecimalData getWsaaCommitCnt() {
        return wsaaCommitCnt;
    }

    protected PackedDecimalData getWsaaCycleCnt() {
        return wsaaCycleCnt;
    }

    protected FixedLengthStringData getWsspEdterror() {
        return wsspEdterror;
    }

    protected FixedLengthStringData getLsaaStatuz() {
        return lsaaStatuz;
    }

    protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
        this.lsaaStatuz = lsaaStatuz;
    }

    protected FixedLengthStringData getLsaaBsscrec() {
        return lsaaBsscrec;
    }

    protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
        this.lsaaBsscrec = lsaaBsscrec;
    }

    protected FixedLengthStringData getLsaaBsprrec() {
        return lsaaBsprrec;
    }

    protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
        this.lsaaBsprrec = lsaaBsprrec;
    }

    protected FixedLengthStringData getLsaaBprdrec() {
        return lsaaBprdrec;
    }

    protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
        this.lsaaBprdrec = lsaaBprdrec;
    }

    protected FixedLengthStringData getLsaaBuparec() {
        return lsaaBuparec;
    }

    protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
        this.lsaaBuparec = lsaaBuparec;
    }

    /**
     * The mainline method is the default entry point to the class
     */
    public void mainline(Object... parmArray) {
        lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
        lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
        lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
        lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
        lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
        try {
            super.mainline();
            new Validator(new FixedLengthStringData("J "), "J").isTrue();
        } catch (COBOLExitProgramException e) {
            // Expected exception for control flow purposes
        }
    }

    protected void restart0900() {
        /* RESTART */
        /* EXIT */
    }

    protected void initialise1000() {
    	isAiaAusFreqChange = FeaConfg.isFeatureExist("2", "BTPRO033", appVars, "IT"); //AIA specific
    	isAiaAusRerate = FeaConfg.isFeatureExist("2", "BTPRO036", appVars, "IT"); //AIA specific
        // Add by yy for chunk read 
        if (bprdIO.systemParam01.isNumeric()) {
            if (bprdIO.systemParam01.toInt() > 0) {
                incrRange = bprdIO.systemParam01.toInt();
            } else {
                incrRange = bprdIO.cyclesPerCommit.toInt();
            }
        } else {
            incrRange = bprdIO.cyclesPerCommit.toInt();
        }  
        wsspEdterror.set(varcom.oK);
        wsaaCompany = bsprIO.getCompany().toString();
        wsaaSqlEffdate = bsscIO.getEffectiveDate().toInt();
        p6671par.parmRecord.set(bupaIO.getParmarea());
        wsaaChdrnumfrm = p6671par.chdrnum.toString();
        wsaaChdrnumto = p6671par.chdrnum1.toString();
        if (isEQ(p6671par.chdrnum, SPACES)) {
            wsaaChdrnumfrm = "0";
        }
        if (isEQ(p6671par.chdrnum1, SPACES)) {
            wsaaChdrnumto= "zzzzzzzz";
        }
        initSetUp1030();
    }
    protected void initSetUp1030() {
        wsaaWorkAreasInner.wsaaNewPremium = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaSrar = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaNewCtdate = 0;
        wsaaWorkAreasInner.wsaaPropReserve = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaTotalReserve = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaCommissionDue = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaNetPrem = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaDurationCtCm = 0;
        wsaaIgnoreContracd = "N";
        Descpf descpf = descDAO.getdescData("IT", t1688, bprdIO.getAuthCode().toString(), bsprIO.getCompany().toString(), bsscIO.getLanguage().toString());
       
        if (descpf==null) {
            syserrrec.params.set("IT".concat(t1688).concat(bprdIO.getAuthCode().toString()));
            fatalError600();
        }

        String coy = bsprIO.getCompany().toString();
        t5448Map = itemDAO.loadSmartTable("IT", coy, t5448);
        t5449Map = itemDAO.loadSmartTable("IT", coy, t5449);
        t5450Map = itemDAO.loadSmartTable("IT", coy, t5450);
        t5647Map = itemDAO.loadSmartTable("IT", coy, t5647);
        t6598Map = itemDAO.loadSmartTable("IT", coy, "T6598");
        th621Map = itemDAO.loadSmartTable("IT", coy, th621);
        readT56791100(coy);
        /* Now initialise WSAA-RUN-INSTJCTL with Job JCTL Details */
        wsaaJctlkey.jctlKey.set(SPACES);
        wsaaJctlkey.jctlJobsts.set("A");
        wsaaJctlkey.jctlJctlpfx.set(smtpfxcpy.jctl);
        wsaaJctlkey.jctlJctlcoy.set(bsprIO.getCompany());
        wsaaJctlkey.jctlJctljn.set(bsscIO.getJobName());
        wsaaJctlkey.jctlJctlacyr.set(bsscIO.getAcctYear());
        wsaaJctlkey.jctlJctlacmn.set(bsscIO.getAcctMonth());
        // by yy for ILPI-231
        b5456TempDAO.initTempTable(wsaaCompany, wsaaChdrnumfrm, wsaaChdrnumto, wsaaSqlEffdate);
        racdSearchResultDtoList = b5456TempDAO.searchRacdResult(minRecord,minRecord + incrRange);
        if (racdSearchResultDtoList != null && racdSearchResultDtoList.size() > 0) {
            iterator = racdSearchResultDtoList.iterator();
        } else {
            wsspEdterror.set(varcom.endp);
        }
    }

    protected void readT56791100(String company) {
    
        List<Itempf> itempfList = itemDAO.getAllItemitem("IT",company,t5679,bprdIO.getAuthCode().trim());
        if (null != itempfList && !itempfList.isEmpty()) { 
    		t5679rec.t5679Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
            return;
        }
        fatalError600();
    }

    protected void readFile2000() {
        if (iterator.hasNext()) {
            sqlRacdpfInner = iterator.next();
        } else {
            minRecord += incrRange;
            // for read paging
            racdSearchResultDtoList = b5456TempDAO.searchRacdResult(minRecord, minRecord + incrRange);
            if (racdSearchResultDtoList != null && racdSearchResultDtoList.size() > 0) {
                iterator = racdSearchResultDtoList.iterator();
                sqlRacdpfInner = iterator.next();
            } else {
                wsspEdterror.set(varcom.endp);
            }
        }
    }

    protected void edit2500() {
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
            try {
                switch (nextMethod) {
                case DEFAULT:
                    start2501();
                case readhRacd2502:
                    readhRacd2502();
                    if(isAiaAusRerate) {//PINNACLE-2297 
                    	getNextAnnivDate();  
                    }
                case exit2509:
                }
                break;
            } catch (GOTOException e) {
                nextMethod = (GotoLabel) e.getNextMethod();
            }
        }
    }

    protected void start2501() {
        wsspEdterror.set(varcom.oK);
        if (sqlRacdpfInner.getSqlChdrcoy().equals(wsaaChdrcoy) && sqlRacdpfInner.getSqlChdrnum().equals(wsaaChdrnum)) {
            newContractFlag = false;
            wsaaPtrnTranno = wsaaTranno;
            if ("Y".equals(wsaaIgnoreContracd)) {
                wsspEdterror.set(SPACES);
                goTo(GotoLabel.exit2509);
            } else {
                goTo(GotoLabel.readhRacd2502);
            }
        }
        newContractFlag = true;
        origRcstfrq = "";
        newContract2510();
        if ("Y".equals(wsaaIgnoreContracd)) {
            wsspEdterror.set(SPACES);
            goTo(GotoLabel.exit2509);
        }
    }

    protected void readhRacd2502() {
//        contotrec.totno.set(ct01);
//        contotrec.totval.set(1);
//        callContot001();
        ct01Count++;
        // changed by yy for ILPI-231
        readT54492530();
        wsaaWorkAreasInner.wsaaT5449Rprmmth = "";
        for (int i=1; !((i > wsaaT5449Size) || !StringUtil.isEmpty(wsaaWorkAreasInner.wsaaT5449Rprmmth)); i++) {
            if (isEQ(t5449rec.rasnum[i], sqlRacdpfInner.getSqlRasnum())) {
                wsaaWorkAreasInner.wsaaT5449Rprmmth = t5449rec.rprmmth[i].toString().trim();
            }
        }
        if (StringUtil.isEmpty(wsaaWorkAreasInner.wsaaT5449Rprmmth)) {
            syserrrec.statuz.set(r072);
            syserrrec.params.set("Premium Basis should be set in smart table T5449 and item is ".concat(sqlRacdpfInner.getSqlRngmnt()));
            fatalError600();
        }
        /* PERFORM 2550-READ-COVR. */
        /* Life Assured And Joint Life Details. */
        lifeJlife2570();
        readT54502580();
        /* IF T5450-REASRATFAC = ZEROES */
        /* MOVE 1 TO T5450-REASRATFAC */
        /* END-IF. */
        if (isEQ(t5450rec.rratfac[wsxxSub], ZERO)) {
            t5450rec.rratfac[wsxxSub].set(1);
        }
        /* Reassurance Method */
        readT54482590();
      //PINNACLE-2054
//      calcCtdate2610();
      sqlCtDate = sqlRacdpfInner.getSqlCtdate();//PINNACLE-2054
      wsaaWorkAreasInner.wsaaNewCtdate = sqlRacdpfInner.getSqlCtdate(); //PINNACLE-2297
        if(!newContractFlag){
        	chdrPojo = new Chdrpf();
            return;
        }
        /* Rewrite the old contract header as validflag '2'. */
        // changed by yy for ILPI-231
        /*Chdrpf chdrPojo = new Chdrpf();
        chdrPojo.setUniqueNumber(sqlRacdpfInner.getSqlChUniqueNum());
        chdrPojo.setChdrcoy(sqlRacdpfInner.getSqlChdrcoy().charAt(0));
        chdrPojo.setChdrnum(sqlRacdpfInner.getSqlChdrnum());
        chdrPojo.setTranno(wsaaTranno);// diff
        chdrPojo.setValidflag('1');
        chdrPojo.setCurrfrom(sqlRacdpfInner.getSqlChCurrfrom());
        chdrPojo.setCurrto(99999999);
        if(chdrBulkOpList==null){
            chdrBulkOpList = new LinkedList<Chdrpf>();
        }
        chdrBulkOpList.add(chdrPojo);*/
        updateChdrpf();
        createPtrn2700();
        
        /* Release the softlock. */
        sftlockRecBean = new SftlockRecBean();
    	sftlockRecBean.setFunction("UNLK");
    	sftlockRecBean.setCompany(bsprIO.getCompany().toString());
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(sqlRacdpfInner.getSqlChdrnum());
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
 
        if (!("****").equals(sftlockRecBean.getStatuz())) {
            syserrrec.params.set(bsprIO.getCompany().toString().concat(sqlRacdpfInner.getSqlChdrnum()));
            syserrrec.statuz.set(sftlockRecBean.getStatuz());
            fatalError600();
        }
    }
    
    //PINNACLE-2297
    protected void getNextAnnivDate() {
    	wsaaOccdate.set(sqlRacdpfInner.getSqlChOccdate());
    	wsaaPtdate.set(sqlCtDate);
    	wsaaNextAnndateYy.set(wsaaPtdateYy);
    	wsaaNextAnndateMm.set(wsaaOccdateMm);
    	wsaaNextAnndateDd.set(wsaaOccdateDd);
    	datcon1rec.function.set(varcom.conv);
    	datcon1rec.statuz.set(SPACES);
    	while ( !(isEQ(datcon1rec.statuz, varcom.oK))) {
    	datcon1rec.intDate.set(wsaaNextAnndate);
    	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    	if (isNE(datcon1rec.statuz, varcom.oK)) {
    	wsaaNextAnndateDd.subtract(1);
    	}
    	}
    	if (isLT(wsaaNextAnndate, sqlCtDate)) {
    	datcon4rec.intDate1.set(wsaaNextAnndate);
    	datcon4rec.billday.set(wsaaOccdateDd);
    	datcon4rec.billmonth.set(wsaaOccdateMm);
    	datcon4rec.freqFactor.set(1);
    	datcon4rec.frequency.set("01");
    	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
    	if (isNE(datcon4rec.statuz, varcom.oK)) {
    	syserrrec.params.set(datcon4rec.datcon4Rec);
    	syserrrec.statuz.set(datcon4rec.statuz);
    	fatalError600();
    	}
    	wsaaNextAnndate.set(datcon4rec.intDate2);
    	}
    }

    protected void newContract2510() {
        wsaaIgnoreContracd = "N";
        validateChdr2600();
        if(!validContractFlag){
            wsaaIgnoreContracd = "Y";
            return;
        }
        wsaaTranno = sqlRacdpfInner.getSqlChTranno() + 1;
        wsaaPtrnTranno = wsaaTranno; //PINNACLE-2684
        // changed by yy for ILPI-231
        /* Softlock the contract. */
        sftlockRecBean = new SftlockRecBean();
    	sftlockRecBean.setFunction("LOCK");
    	sftlockRecBean.setCompany(bsprIO.getCompany().toString());
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(sqlRacdpfInner.getSqlChdrnum());
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (!("****").equals(sftlockRecBean.getStatuz())
        		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
    		  syserrrec.params.set(bsprIO.getCompany().toString().concat(sqlRacdpfInner.getSqlChdrnum()));
              syserrrec.statuz.set(sftlockRecBean.getStatuz());
              fatalError600();
    	}
    	//PINNACLE-2760 Starts
    	if ("LOCK".equals(sftlockRecBean.getStatuz())) {
    		conlogrec.error.set("E101");
    		conlogrec.params.set("CH"+bsprIO.getCompany().toString()+sqlRacdpfInner.getSqlChdrnum());
    		callConlog003();
    		ct06Sftlck++;
            wsspEdterror.set(SPACES);
            goTo(GotoLabel.exit2509);
        }
    	wsaaChdrcoy = sqlRacdpfInner.getSqlChdrcoy();
        wsaaChdrnum = sqlRacdpfInner.getSqlChdrnum();
      //PINNACLE-2760 End
    }

    protected void readT54492530() {
        if (t5449Map.containsKey(sqlRacdpfInner.getSqlRngmnt())) {
            List<Itempf> t5449ItemList = t5449Map.get(sqlRacdpfInner.getSqlRngmnt());
            for (Itempf t5449Item : t5449ItemList) {
                if (t5449Item.getItmfrm().compareTo(new BigDecimal(sqlRacdpfInner.getSqlCoCrrcd())) <= 0) {
                    t5449rec.t5449Rec.set(StringUtil.rawToString(t5449Item.getGenarea()));
                    return;
                }
            }
        }
        // if we don't found the record
        syserrrec.params.set(sqlRacdpfInner.getSqlRngmnt() + t5449);
        syserrrec.statuz.set(r057);
        fatalError600();
    }

    protected void lifeJlife2570() {
        wsaaAge = sqlRacdpfInner.getSqlAnbAtCcd00();
        wsaaSex = sqlRacdpfInner.getSqlCltsex00();
        // test the result, when left join failed
        if (sqlRacdpfInner.getSqlAnbAtCcd01() == 0
                && (sqlRacdpfInner.getSqlCltsex01() == null || sqlRacdpfInner.getSqlCltsex01().trim().isEmpty())) {
            useJointLifeFlag = false;
            wsbbSex = "";
            wsbbAge = 0;
        } else {
            useJointLifeFlag = true;
            wsbbAge = sqlRacdpfInner.getSqlAnbAtCcd01();
            wsbbSex = sqlRacdpfInner.getSqlCltsex01();
        }
    }

    protected void readT54502580() {
        // changed by yy for ILPI-231
        boolean foundFlag = false;
        if (t5450Map.containsKey(wsaaWorkAreasInner.wsaaT5449Rprmmth)) {
            List<Itempf> t5450ItemList = t5450Map.get(wsaaWorkAreasInner.wsaaT5449Rprmmth);
            for (Itempf t5450Item : t5450ItemList) {
                if (t5450Item.getItmfrm().compareTo(new BigDecimal(sqlRacdpfInner.getSqlCoCrrcd())) <= 0) {
                    t5450rec.t5450Rec.set(StringUtil.rawToString(t5450Item.getGenarea()));
                    foundFlag = true;
                }
            }
        }
        if(!foundFlag){
            syserrrec.params.set(wsaaWorkAreasInner.wsaaT5449Rprmmth+t5450);
            syserrrec.statuz.set(r055);
            fatalError600();
        }
        readTh6212585();
        for (wsxxSub = 1; !((wsxxSub > 5) || isEQ(t5450rec.rprmcls[wsxxSub], th621rec.rprmcls)); wsxxSub++) {
            /* CONTINUE_STMT */
        }
        if (isGT(wsxxSub, 5)) {
            syserrrec.statuz.set(rl29);
            fatalError600();
        }
        if (isEQ(t5450rec.commrate[wsxxSub], SPACES)
                && isNE(t5450rec.basicCommMeth[wsxxSub], SPACES)) {
            syserrrec.params.set(wsaaTh621Items + th621);
            syserrrec.statuz.set(r074);
            fatalError600();
        }
    }

    protected void readTh6212585() {
        // changed by yy for ILPI-231
        wsaaTh621Items = sqlRacdpfInner.getSqlCoCrtable() + sqlRacdpfInner.getSqlLrkcls();
        if (th621Map.containsKey(wsaaTh621Items)) {
            List<Itempf> th621ItemList = th621Map.get(wsaaTh621Items);
            for (Itempf th621Item : th621ItemList) {
                if (th621Item.getItmfrm().compareTo(new BigDecimal(sqlRacdpfInner.getSqlCoCrrcd())) <= 0) {
                    th621rec.th621Rec.set(StringUtil.rawToString(th621Item.getGenarea()));
                    return;
                }
            }
        }
        syserrrec.params.set(wsaaTh621Items + th621);
        syserrrec.statuz.set(r055);
        fatalError600();
    }

    protected void readT54482590() {
        // by yy for ILPI-231
        String wsaaT5448Item = sqlRacdpfInner.getSqlChCnttype() + sqlRacdpfInner.getSqlCoCrtable();
        if (t5448Map.containsKey(wsaaT5448Item)) {
            List<Itempf> t5448ItemList = t5448Map.get(wsaaT5448Item);
            for (Itempf t5448Item : t5448ItemList) {
                if (t5448Item.getItmfrm().compareTo(new BigDecimal(sqlRacdpfInner.getSqlCoCrrcd())) <= 0) {
                    t5448rec.t5448Rec.set(StringUtil.rawToString(t5448Item.getGenarea()));
                    if(isNE(t5448rec.rcstfrq,SPACES))
                    	wsaarcstfrq = t5448rec.rcstfrq.toString();
                    else 
                    	wsaarcstfrq = sqlRacdpfInner.getSqlChBillfreq();
                    	readRecoFrequency();
                    return;
                }
            }
        }
        // if we don't found the record
        syserrrec.params.set(t5448);
        fatalError600();
    }
    
    //PINNACLE-2297
    protected void readRecoFrequency() {
        if(null != sqlRacdpfInner && sqlRacdpfInner.getSqlPendCstTo() != 0) {
	        String rcstfreq = recopfDAO.getCostFreq(sqlRacdpfInner.getSqlChdrcoy(), sqlRacdpfInner.getSqlChdrnum(), sqlRacdpfInner.getSqlLife(), sqlRacdpfInner.getSqlCoverage(), sqlRacdpfInner.getSqlRider(), sqlRacdpfInner.getSqlPlnsfx());
			if(rcstfreq!=null) {
				origRcstfrq = wsaarcstfrq;
				wsaarcstfrq = rcstfreq;
			}
        }
    }

    protected void validateChdr2600() {
        /* START */
        // by yy for ILPI-231
        validContractFlag = false;
        for (int i = 1; i<=12; i++) {
            if (isEQ(t5679rec.cnRiskStat[i], sqlRacdpfInner.getSqlChStatcode())) {
                for (i = 1; i<=12; i++) {
                    if (isEQ(t5679rec.cnPremStat[i], sqlRacdpfInner.getSqlChPstatcode())) {
                        validContractFlag = true;
                        return;
                    }
                }
            }
        }
        /* EXIT */
    }

    protected void calcCtdate2610() {
    	//PINNACLE- 2297 start
    	if(isNE(wsaaWorkAreasInner.wsaaNewCtdate,0)) {
    		sqlCtDate = wsaaWorkAreasInner.wsaaNewCtdate;  //store previous ctdate
    		}
    	if(!isProrate) {//get next ctdate
	    	wsaaWorkAreasInner.wsaaNewCtdate = getNextCtdate(sqlCtDate);
    	}else { //smothen ctdate if prorate calculation done
    		isProrate = false;
    		//smothen if difference in anniversary date and ctdate is less than 2
    		if(isEQ(wsaaEndDate,0)) {
    			//smothen out in case of last freq of fortnight policy
    			wsaaWorkAreasInner.wsaaNewCtdate = wsaaNextAnndate.toInt();
    		}else { //smothen in frequency change scenario
    			wsaaWorkAreasInner.wsaaNewCtdate = wsaaEndDate;
    		}
    	}
    	if(isAiaAusFreqChange || isAiaAusRerate) {
    		checkProrate(); 
    	}
    	//PINNACLE- 2297 end
    	//PINNACLE-2971 reset enddate
    	if(isNE(wsaaEndDate,0)) {
    		wsaaEndDate = 0; 
    	}
    }
    
    //PINNACLE-2297
    protected void checkProrate() {
    	int diffInDays = 0;
    	if(isEQ(wsaaEndDate,0)) { // if no frequency change
    		// Find the difference in the anniversary date and the new ctdate
        	//if diff >0 and <3, update the ctdate with anniversary date
        	diffInDays = getDiffInDays(wsaaNextAnndate.toInt(),wsaaWorkAreasInner.wsaaNewCtdate);
        	wsaaMinDay = 15;
        	wsaaMaxDay = 16;
    	}/*else {
    		// Find the difference in the pendcstfrm date and the new ctdate
        	//if diff >0 and <14, turn on prorate flag
        	 diffInDays =  getDiffInDays(wsaaEndDate,wsaaWorkAreasInner.wsaaNewCtdate);
        	 wsaaMaxDay = 13;
    	}*/
    	if(diffInDays >= wsaaMinDay && diffInDays <= wsaaMaxDay) {
    		isProrate = true;
    	}
    }
    
    //PINNACLE-2297
    protected int getDiffInDays(int date1, int date2) {
    	datcon3rec.datcon3Rec.set(SPACES);
        datcon3rec.frequency.set(freqcpy.daily);
        datcon3rec.intDate1.set(date2);
        datcon3rec.intDate2.set(date1);
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz, varcom.oK)) {
           syserrrec.statuz.set(datcon3rec.statuz);
           syserrrec.params.set(datcon3rec.datcon3Rec);
           fatalError600();
        }
    	return datcon3rec.freqFactor.toInt();
    }
    
    protected void calcCtDateTerm() {
        /* IF T5450-SLCTRATE = SPACES */
        /* AND T5450-ULTMRATE = SPACES */
        if (isEQ(t5450rec.slctrate[wsxxSub], SPACES) && isEQ(t5450rec.ultmrate[wsxxSub], SPACES)) {
            origTerms2615();
        }
    }


    protected void origTerms2615() {
        // changed by yy for ILPI-231
        if (isEQ(t5448rec.rcstfrq,SPACES) || isEQ(t5448rec.rcstfrq, sqlRacdpfInner.getSqlChBillfreq()) || "00".equals(sqlRacdpfInner.getSqlChBillfreq())) {
            wsaaNumPremInsts = BigDecimal.ONE;
            return;
        }
        initialize(datcon3rec.datcon3Rec);
        // changed by yy for ILPI-231
        datcon3rec.frequency.set(sqlRacdpfInner.getSqlChBillfreq());
        datcon3rec.intDate1.set(sqlCtDate); 
        datcon3rec.intDate2.set(getNextCtdate(sqlCtDate));
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz, varcom.oK)) {
            syserrrec.statuz.set(datcon3rec.statuz);
            syserrrec.params.set(datcon3rec.datcon3Rec);
            fatalError600();
        }
        wsaaNumPremInsts = datcon3rec.freqFactor.getbigdata();
    }
    
    //PINNACLE-2297
    protected int getNextCtdate(int inputDate) {
    	initialize(datcon4rec.datcon4Rec);
    	datcon4rec.freqFactor.set(1);
    	datcon4rec.frequency.set(wsaarcstfrq);
    	datcon4rec.intDate1.set(inputDate);
    	datcon4rec.billday.set(String.valueOf(sqlRacdpfInner.getSqlChOccdate()).substring(6, 8));	//PINNACLE-2708, PINNACLE-3036
    	datcon4rec.billmonth.set(String.valueOf(sqlRacdpfInner.getSqlChOccdate()).substring(4, 6));//PINNACLE-2708,	PINNACLE-3036
    	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
    	if (isNE(datcon4rec.statuz, varcom.oK)) {
    		syserrrec.statuz.set(datcon4rec.statuz);
    		syserrrec.params.set(datcon4rec.datcon4Rec);
    		fatalError600();
    	}
    	return datcon4rec.intDate2.toInt();
    }

    protected void createPtrn2700() {
        wsaaTransactionDate.set(bsscIO.getEffectiveDate());
        Ptrnpf ptrnIO = new Ptrnpf();
        varcom.vrcmDate.set(getCobolDate());
        ptrnIO.setBatcpfx(smtpfxcpy.batc.toString());
        ptrnIO.setBatccoy(bsprIO.getCompany().toString());
        ptrnIO.setBatcbrn(batcdorrec.branch.toString());
        ptrnIO.setBatcactyr(bsscIO.getAcctYear().toInt());
        ptrnIO.setBatcactmn(bsscIO.getAcctMonth().toInt());
        ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
        ptrnIO.setBatcbatch(batcdorrec.batch.toString());
        ptrnIO.setChdrcoy(sqlRacdpfInner.getSqlChdrcoy());
        ptrnIO.setChdrnum(sqlRacdpfInner.getSqlChdrnum());
        ptrnIO.setTranno(wsaaPtrnTranno); //PINNACLE-2684
        ptrnIO.setTrdt(varcom.vrcmDate.toInt());
        ptrnIO.setTrtm(varcom.vrcmTime.toInt());
        
//      ptrnIO.setTrtm(varcom.vrcmTime.toInt());
//      ptrnIO.setTrdt(varcom.vrcmDate.toInt());
        ptrnIO.setPtrneff(wsaaWorkAreasInner.wsaaNewCtdate);
        ptrnIO.setTermid(varcom.vrcmTermid.toString());
        ptrnIO.setUserT(0);
        ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
        if(ptrnBulkOpList==null){
            ptrnBulkOpList = new LinkedList<Ptrnpf>();
        }
        ptrnBulkOpList.add(ptrnIO);
    }
    
    protected void updateChdrpf() {
   	 chdrPojo = new Chdrpf();
        chdrPojo.setUniqueNumber(sqlRacdpfInner.getSqlChUniqueNum());
        chdrPojo.setChdrcoy(sqlRacdpfInner.getSqlChdrcoy().charAt(0));
        chdrPojo.setChdrnum(sqlRacdpfInner.getSqlChdrnum());
        chdrPojo.setTranno(wsaaPtrnTranno);// diff
        /*if(isLTE(wsaaWorkAreasInner.wsaaNewCtdate,bsscIO.getEffectiveDate())) {
       	 chdrPojo.setValidflag('2');
       	 chdrPojo.setCurrto(bsscIO.getEffectiveDate().toInt());
        }else {*/
       	 chdrPojo.setValidflag('1');
       	 chdrPojo.setCurrto(99999999);
        //}
        chdrPojo.setCurrfrom(sqlRacdpfInner.getSqlChCurrfrom());

        if(chdrBulkOpList==null){
            chdrBulkOpList = new LinkedList<Chdrpf>();
        }
        chdrBulkOpList.add(chdrPojo);
   }

    protected void update3000() {
    	if(isAiaAusFreqChange) {
    		checkInitialProrate();
    	}
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
            try {
                switch (nextMethod) {
                case DEFAULT:
                	if(newContractFlag && isNE(wsaaPtrnTranno,wsaaTranno)) {
	                	//PINNACLE-2684 - New PTRN should be created for each RI entry
	                	createPtrn2700();
                	}
                	calcCtDateTerm();
                    update3010();
                    calcPrem3020();
                case calcCommission3040:
                    calcCommission3040();
                    calcCtdate2610(); //PINNACLE-2054
                    // rewriteOldRacd3050();
                    acmvPosting3070();
                  //PINNACLE-2684 - New RACD should be created for each RI entry
                    if(newContractFlag  && isNE(wsaaPtrnTranno,wsaaTranno)) {
	                	//PINNACLE-2684 - CHDR should be updated with latest tranno
	                	updateChdrpf();
                	}
                    createNewRacd3060();   //PINNACLE-2296
                    contotBatchTotal3080();
                    
                }
                break;
            } catch (GOTOException e) {
                nextMethod = (GotoLabel) e.getNextMethod();
            }
        }
        //getWsaaCommitCnt().set(0);// just commit for once
    }

    //PINNACLE- 2297
    protected void checkInitialProrate() {
    	isProrate = false;
    	wsaaEndDate  = 0;
        if(null != sqlRacdpfInner && sqlRacdpfInner.getSqlPendCstTo() != 0) {
        	wsaaEndDate = sqlRacdpfInner.getSqlPendCstTo();
        	
        	//if(isNE(wsaaEndDate,wsaaNextAnndate.toInt())) { Prorate regardless of anniversary date
        	/*int diffInDays = getDiffInDays(wsaaEndDate,sqlCtDate);
        		if(diffInDays>=wsaaMinDay && diffInDays<=wsaaMaxDay) {*/
        			isProrate = true;
        		//}
        	//}
        }else {//PINNACLE-2684
        	int diffInDays = getDiffInDays(wsaaNextAnndate.toInt(),sqlCtDate);
    		if(isEQ(wsaarcstfrq,fortnight) && diffInDays>=15 && diffInDays<=16) { //check if last freq in fortnight policy
    			isProrate = true;
    		}
        }
    }
    
    protected void update3010() {
        /* Calculate the Sum at risk if both the Costing Based on Sum */
        /* Reassured At Risk field and the Reserve Calculation Method */
        /* (both of which exist on T5448) are non-blank. */
        if (isNE(t5448rec.rresmeth, SPACES) && isNE(t5448rec.rcstsar, SPACES)) {
            wsaaWorkAreasInner.wsaaTotalReserve = BigDecimal.ZERO;
            calcReserves3100();
            // changed by yy for ILPI-231
            wsaaWorkAreasInner.wsaaPropReserve = wsaaWorkAreasInner.wsaaTotalReserve.multiply(sqlRacdpfInner.getSqlRaamount()).divide(sqlRacdpfInner.getSqlCoSumins());
            //wsaaWorkAreasInner.wsaaPropReserve.setScale(newScale)
            //compute(wsaaWorkAreasInner.wsaaPropReserve, 3).setRounded(div(mult(wsaaWorkAreasInner.wsaaTotalReserve, sqlRacdpfInner.getSqlRaamount),sqlRacdpfInner.getSqlCoSumins));
            //compute(wsaaWorkAreasInner.wsaaSrar, 1).setRounded(sub(sqlRacdpfInner.getSqlRaamount, wsaaWorkAreasInner.wsaaPropReserve));
            wsaaWorkAreasInner.wsaaSrar = sqlRacdpfInner.getSqlRaamount().subtract(wsaaWorkAreasInner.wsaaPropReserve);
        } else {
            wsaaWorkAreasInner.wsaaSrar = sqlRacdpfInner.getSqlRaamount();
        }
        /* MOVE WSAA-SRAR TO ZRDP-AMOUNT-IN. */
        /* PERFORM A000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO WSAA-SRAR. */
        if (wsaaWorkAreasInner.wsaaSrar.compareTo(BigDecimal.ZERO) !=0 ) {
        	zrdecplcPojo.setAmountIn(wsaaWorkAreasInner.wsaaSrar);
            a000CallRounding();
            wsaaWorkAreasInner.wsaaSrar = zrdecplcPojo.getAmountOut();
        }
    }

    protected void calcPrem3020() {
        /* IF T5450-SLCTRATE = SPACES */
        /* AND T5450-ULTMRATE = SPACES */
        if (isEQ(t5450rec.slctrate[wsxxSub], SPACES) && isEQ(t5450rec.ultmrate[wsxxSub], SPACES)) {
            origTermsPrem3200();
        } else {
            regularPrem3300();
        }
        /* The Stamp duty & GST calculation will be performed either */
        /* for Tax. They use the same copy book for the linkage. If */
        /* there is no entry in T5448-taxation Basis, the taxamt is */
        /* set to zero, and all processing is skipped. */
        if (isEQ(t5448rec.rtaxbas, SPACES)) {
            restdtyrec.tax.set(ZERO);
            goTo(GotoLabel.calcCommission3040);
        }
        
        readT65983400(t5448rec.rtaxbas.toString());
        if (isEQ(t6598rec.calcprog, SPACES)) {
            restdtyrec.tax.set(ZERO);
            goTo(GotoLabel.calcCommission3040);
        }
        initialize(restdtyrec.restdtyRec);
        restdtyrec.statuz.set(varcom.oK);
        restdtyrec.company.set(sqlRacdpfInner.getSqlChdrcoy());
        restdtyrec.chdrnum.set(sqlRacdpfInner.getSqlChdrnum());
        restdtyrec.life.set(sqlRacdpfInner.getSqlLife());
        restdtyrec.coverage.set(sqlRacdpfInner.getSqlCoverage());
        restdtyrec.rider.set(sqlRacdpfInner.getSqlRider());
        restdtyrec.plnsfx.set(sqlRacdpfInner.getSqlPlnsfx());
        // changed by yy for ILPI-231
        // restdtyrec.cntcurr.set(chdrlifIO.getCntcurr());
        // restdtyrec.crtable.set(covrIO.getCrtable());
        // restdtyrec.effdate.set(covrIO.getCrrcd());
        restdtyrec.cntcurr.set(sqlRacdpfInner.getSqlChCntcurr());
        restdtyrec.crtable.set(sqlRacdpfInner.getSqlCoCrtable());
        restdtyrec.effdate.set(sqlRacdpfInner.getSqlCoCrrcd());

        restdtyrec.sumass.set(sqlRacdpfInner.getSqlRaamount());
        restdtyrec.premium.set(wsaaWorkAreasInner.wsaaNewPremium);
        restdtyrec.tax.set(ZERO);
        callProgram(t6598rec.calcprog, restdtyrec.restdtyRec);
        if (isNE(restdtyrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(restdtyrec.statuz);
            syserrrec.params.set(restdtyrec.restdtyRec);
            fatalError600();
        }
        /* MOVE RTAX-TAX TO ZRDP-AMOUNT-IN. */
        /* PERFORM A000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO RTAX-TAX. */
        if (isNE(restdtyrec.tax, 0)) {
        	zrdecplcPojo.setAmountIn(restdtyrec.tax.getbigdata());
            a000CallRounding();
            restdtyrec.tax.set(zrdecplcPojo.getAmountOut());
        }
    }

    protected void calcCommission3040() {
        initialize(datcon2rec.datcon2Rec);
        datcon2rec.function.set(SPACES);
        /* MOVE T5450-INLPRD TO DTC2-FREQ-FACTOR. */
        datcon2rec.freqFactor.set(t5450rec.inlprd[wsxxSub]);
        datcon2rec.frequency.set(freqcpy.mthly);
        // changed by yy for ILPI-231
        // datcon2rec.intDate1.set(covrIO.getCrrcd());
        datcon2rec.intDate1.set(sqlRacdpfInner.getSqlCoCrrcd());
        callProgram(Datcon2.class, datcon2rec.datcon2Rec);
        if (isNE(datcon2rec.statuz, varcom.oK)) {
            syserrrec.statuz.set(datcon2rec.statuz);
            syserrrec.params.set(datcon2rec.datcon2Rec);
            fatalError600();
        }
        if (isLTE(datcon2rec.intDate2, bsscIO.getEffectiveDate())) {
            calcCommission3700();
        }
    }

    protected void createNewRacd3060() {
        /* MOVE WSAA-NEW-CTDATE TO RACD-CTDATE. */
        /* MOVE BSSC-EFFECTIVE-DATE TO RACD-CURRFROM. */
        /* MOVE VRCM-MAX-DATE TO RACD-CURRTO. */
        /* MOVE '1' TO RACD-VALIDFLAG. */
        /* MOVE WSAA-TRANNO TO RACD-TRANNO. */
        /* MOVE WRITR TO RACD-FUNCTION. */
        racdpf = new Racdpf();
        racdpf.setUnique_number(sqlRacdpfInner.getSqlRaUniqueNum());
        if (bsscIO.getEffectiveDate().toInt() == (sqlRacdpfInner.getRrtdat())) {
        	racdpf.setRrtfrm(sqlRacdpfInner.getRrtdat());
        	racdpf.setRrtdat(sqlRacdpfInner.getCorrtdat());
       
        }else {
        	racdpf.setRrtfrm(sqlRacdpfInner.getRrtfrm());
        	racdpf.setRrtdat(sqlRacdpfInner.getRrtdat());
        }
        racdpf.setCtdate(wsaaWorkAreasInner.wsaaNewCtdate);
        racdpf.setTranno(wsaaPtrnTranno); //PINNACLE-2684
        racdpf.setCurrfrom(bsscIO.getEffectiveDate().toInt());
        racdpf.setCurrto(varcom.vrcmMaxDate.toInt());
        racdpf.setPendcstto(0); //PINNACLE- 2297
        racdpf.setProrateflag(SPACE);
        racdpf.setValidflag("1"); //PINNACLE-2902
        
        if(racdrskBulkOpList == null){
            racdrskBulkOpList = new LinkedList<Racdpf>();
        }
        racdrskBulkOpList.add(racdpf);
    }

    protected void acmvPosting3070() {
    	
    	RecopstPojo recopstPojo = new RecopstPojo();
    	recopstPojo.setFunction("RCST");
    	recopstPojo.setStatuz("****");
    	recopstPojo.setBatccoy(bsprIO.getCompany().toString());
    	recopstPojo.setBatcbrn(batcdorrec.branch.toString());
    	recopstPojo.setBatcactyr(bsscIO.getAcctYear().toInt());
    	recopstPojo.setBatcactmn(bsscIO.getAcctMonth().toInt());
    	recopstPojo.setBatctrcde(bprdIO.getAuthCode().toString());
    	recopstPojo.setBatcbatch(batcdorrec.batch.toString());
    	recopstPojo.setTranno(wsaaPtrnTranno); //PINNACLE-2684
    	recopstPojo.setTermid(varcom.vrcmTranid.toString());
    	recopstPojo.setLanguage(bsscIO.getLanguage().toString());
    	recopstPojo.setChdrpfx(fsupfxcpy.chdr.toString());
    	recopstPojo.setChdrcoy(sqlRacdpfInner.getSqlChdrcoy());
    	recopstPojo.setChdrnum(sqlRacdpfInner.getSqlChdrnum());
    	recopstPojo.setLife(sqlRacdpfInner.getSqlLife());
    	recopstPojo.setCoverage(sqlRacdpfInner.getSqlCoverage());
    	recopstPojo.setRider(sqlRacdpfInner.getSqlRider());
    	recopstPojo.setPlanSuffix(sqlRacdpfInner.getSqlPlnsfx());
    	recopstPojo.setRasnum(sqlRacdpfInner.getSqlRasnum());
    	recopstPojo.setSeqno(sqlRacdpfInner.getSqlSeqno());
    	recopstPojo.setCostdate(sqlCtDate); 
    	recopstPojo.setEffdate(bsscIO.getEffectiveDate().toInt());
    	recopstPojo.setValidflag("1");
    	recopstPojo.setRetype(sqlRacdpfInner.getSqlRetype());
    	recopstPojo.setRngmnt(sqlRacdpfInner.getSqlRngmnt());
    	recopstPojo.setSraramt(wsaaWorkAreasInner.wsaaSrar);
    	recopstPojo.setRaAmount(sqlRacdpfInner.getSqlRaamount());
    	recopstPojo.setCtdate(wsaaWorkAreasInner.wsaaNewCtdate);
    	recopstPojo.setOrigcurr(sqlRacdpfInner.getSqlCurrcode());
        /* MOVE RASA-CURRCODE TO RCOP-ACCTCURR. */
        /* MOVE WSAA-NEW-PREMIUM TO RCOP-PREM01. */
        /* MOVE WSAA-COMMISSION-DUE TO RCOP-COMPAY01. */
        /* MOVE RTAX-TAX TO RCOP-TAXAMT01. */
    	recopstPojo.setPrem(wsaaWorkAreasInner.wsaaNewPremium);
    	recopstPojo.setCompay(wsaaWorkAreasInner.wsaaCommissionDue);
    	recopstPojo.setTaxamt(restdtyrec.tax.getbigdata());
        /* MOVE ZEROS TO RCOP-PREM02 */
        /* RCOP-COMPAY02 */
        /* RCOP-TAXAMT02 */
        /* RCOP-CRATE. */
        /* MOVE ZEROES TO RCOP-REFUNDFE01. */
        /* MOVE ZEROES TO RCOP-REFUNDFE02. */
    	recopstPojo.setRefundfe(BigDecimal.ZERO);
        // changed by yy for ILPI-231
    	recopstPojo.setPolsum(new BigDecimal(sqlRacdpfInner.getSqlChPolsum()));
    	recopstPojo.setRecovAmt(BigDecimal.ZERO);
        // changed by yy for ILPI-231
    	recopstPojo.setCrtable(sqlRacdpfInner.getSqlCoCrtable());
    	recopstPojo.setCnttype(sqlRacdpfInner.getSqlChCnttype());
    	recopstPojo.setRcstfrq(wsaarcstfrq);
    	recopstPojo.setRPRate(rprmiumrec.reinsPremBaseRate.getbigdata());
		recopstPojo.setAnnprem(rprmiumrec.reinsAnnPrem.getbigdata()); //PINNACLE-2903
    	recopstPojo = recopstUtils.processRecopst(recopstPojo);
        if (!("****").equals(recopstPojo.getStatuz())) {
            syserrrec.statuz.set(recopstPojo.getStatuz());
            syserrrec.params.set(recopstPojo);
            fatalError600();
        }
 
    }

    protected void contotBatchTotal3080() {
        /* Control total for Premium Costed */
//        contotrec.totno.set(ct02);
//        contotrec.totval.set(wsaaWorkAreasInner.wsaaNewPremium);
//        callContot001();
        ct02Prem = ct02Prem.add(wsaaWorkAreasInner.wsaaNewPremium);
        /* Control total for Commission */
//        contotrec.totno.set(ct03);
//        contotrec.totval.set(wsaaWorkAreasInner.wsaaCommissionDue);
//        callContot001();
        ct03Comm = ct03Comm.add(wsaaWorkAreasInner.wsaaCommissionDue);
        /* Control total for Tax paid */
//        contotrec.totno.set(ct04);
//        contotrec.totval.set(restdtyrec.tax);
//        callContot001();
        ct04Tax = ct04Tax.add(restdtyrec.tax.getbigdata());
        /* Control total for Net Premium */
        wsaaWorkAreasInner.wsaaNetPrem = wsaaWorkAreasInner.wsaaNewPremium.add(restdtyrec.tax.getbigdata()).subtract(wsaaWorkAreasInner.wsaaCommissionDue);
//        compute(wsaaWorkAreasInner.wsaaNetPrem, 3).setRounded(
//                sub(add(wsaaWorkAreasInner.wsaaNewPremium, restdtyrec.tax), wsaaWorkAreasInner.wsaaCommissionDue));
//        contotrec.totno.set(ct05);
//        contotrec.totval.set(wsaaWorkAreasInner.wsaaNetPrem);
//        callContot001();
        ct05Net = ct05Net.add(wsaaWorkAreasInner.wsaaNetPrem);
        
        wsaaWorkAreasInner.wsaaNewPremium = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaCommissionDue = BigDecimal.ZERO;
        wsaaWorkAreasInner.wsaaNetPrem = BigDecimal.ZERO;
        //PINNACLE-2297 if no frequency change, collect until batch effective date
        if( isLTE(wsaaWorkAreasInner.wsaaNewCtdate,bsscIO.getEffectiveDate())) {  //PINNACLE-2054
        	wsaaPtrnTranno++;
			//PINNACLE-2902 START
        	//Invalidate current chdrpf record
        	chdrPojo.setValidflag('2');
        	chdrPojo.setCurrto(bsscIO.getEffectiveDate().toInt());
        	//Invalidate current racdpf record
            racdpf.setValidflag("2");
            racdpf.setCurrto(bsscIO.getEffectiveDate().toInt());
            //PINNACLE-2902 END
            if(isNE(origRcstfrq,"")) {
            	//PINNACLE-2971 set the current frequency from CHDRPF
        		wsaarcstfrq = origRcstfrq;
        	}
        	//PINNACLE-2902 END
        	goTo(GotoLabel.DEFAULT);
        }
    }

    protected void calcReserves3100() {
        /* Caluculate the sum at risk for the component as for the */
        /* effective date. */
        readT65983400(t5448rec.rresmeth.toString());
        if (isEQ(t6598rec.calcprog, SPACES)) {
            syserrrec.params.set(t5448rec.rresmeth + "T6598");
            syserrrec.statuz.set(h021);
            fatalError600();
        }
        srcalcpy.surrenderRec.set(SPACES);
        srcalcpy.tsvtot.set(ZERO);
        srcalcpy.tsv1tot.set(ZERO);
        srcalcpy.chdrChdrcoy.set(sqlRacdpfInner.getSqlChdrcoy());
        srcalcpy.chdrChdrnum.set(sqlRacdpfInner.getSqlChdrnum());
        srcalcpy.planSuffix.set(sqlRacdpfInner.getSqlPlnsfx());
        srcalcpy.polsum.set(sqlRacdpfInner.getSqlChPolsum());
        srcalcpy.lifeLife.set(sqlRacdpfInner.getSqlLife());
        srcalcpy.lifeJlife.set(sqlRacdpfInner.getSqlCoJlife());
        srcalcpy.covrCoverage.set(sqlRacdpfInner.getSqlCoverage());
        srcalcpy.covrRider.set(sqlRacdpfInner.getSqlRider());
        srcalcpy.crtable.set(sqlRacdpfInner.getSqlCoCrtable());
        srcalcpy.crrcd.set(sqlRacdpfInner.getSqlCoCrrcd());
        srcalcpy.ptdate.set(sqlCtDate); 
        srcalcpy.effdate.set(sqlCtDate); 
        srcalcpy.language.set(bsscIO.getLanguage());
        srcalcpy.chdrCurr.set(sqlRacdpfInner.getSqlCurrcode());
        srcalcpy.billfreq.set(sqlRacdpfInner.getSqlChBillfreq());
        /* Now call the Surrender value calculation subroutine. */
        while (!(isEQ(srcalcpy.status, varcom.endp))) {
            getSurrValue3150();
        }
    }


    protected void getSurrValue3150() {
        /* START */
        srcalcpy.estimatedVal.set(ZERO);
        srcalcpy.actualVal.set(ZERO);
        /* IVE-796 RUL Product - Partial Surrender Calculation started */
        // callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);

        if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) {
            callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
        } else {
            Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
            Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
            Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

            vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
            vpxsurcrec.function.set("INIT");
            callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec, vpxsurcrec); // IO
                                                                           // read
            srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
            vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
            vpmfmtrec.initialize();
            vpmfmtrec.amount02.set(wsaaEstimateTot);

            // make a chdr specially for VPMS
            ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
            chdrlifIO.setTranno(sqlRacdpfInner.getSqlChTranno());
            chdrlifIO.setCnttype(sqlRacdpfInner.getSqlChCnttype());
            chdrlifIO.setStatcode(sqlRacdpfInner.getSqlChStatcode());
            chdrlifIO.setPstatcode(sqlRacdpfInner.getSqlChPstatcode());
            chdrlifIO.setBillfreq(sqlRacdpfInner.getSqlChBillfreq());
            chdrlifIO.setCntcurr(sqlRacdpfInner.getSqlChCntcurr());
            chdrlifIO.setPolsum(sqlRacdpfInner.getSqlChPolsum());
            chdrlifIO.setBillchnl(sqlRacdpfInner.getSqlChBillchnl());
            chdrlifIO.setOccdate(sqlRacdpfInner.getSqlChOccdate());
            chdrlifIO.setPolinc(sqlRacdpfInner.getSqlChPolinc());
            chdrlifIO.setChdrcoy(sqlRacdpfInner.getSqlChdrcoy());
            chdrlifIO.setChdrnum(sqlRacdpfInner.getSqlChdrnum());
            chdrlifIO.setValidflag("1");
            callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec, chdrlifIO);// VPMS call

            if (isEQ(srcalcpy.type, "L")) {
                vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
                callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
                srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
                srcalcpy.status.set(varcom.endp);
            } else if (isEQ(srcalcpy.type, "C")) {
                srcalcpy.status.set(varcom.endp);
            } else if (isEQ(srcalcpy.type, "A") && vpxsurcrec.statuz.equals(varcom.endp)) {
                srcalcpy.status.set(varcom.endp);
            } else {
                srcalcpy.status.set(varcom.oK);
            }
        }

        /* IVE-796 RUL Product - Partial Surrender Calculation end */
        if (isNE(srcalcpy.status, varcom.oK) && isNE(srcalcpy.status, varcom.endp)) {
            syserrrec.statuz.set(srcalcpy.status);
            syserrrec.params.set(srcalcpy.surrenderRec);
            fatalError600();
        }
        /* The surrender value of the units is returned in the estimated */
        /* variable for unit-linked products and in the actual variable */
        /* for traditional products. */
        if (isGT(srcalcpy.estimatedVal, ZERO)) {
        	wsaaWorkAreasInner.wsaaTotalReserve=wsaaWorkAreasInner.wsaaTotalReserve.add(srcalcpy.estimatedVal.getbigdata());
        } else {
        	wsaaWorkAreasInner.wsaaTotalReserve=wsaaWorkAreasInner.wsaaTotalReserve.add(srcalcpy.actualVal.getbigdata());
        }
        /* EXIT */
    }

    protected void origTermsPrem3200() {
        wsaaWorkAreasInner.wsaaNewPremium = sqlRacdpfInner.getSqlCoInstprem().multiply(wsaaWorkAreasInner.wsaaSrar).multiply(wsaaNumPremInsts).multiply(t5450rec.rratfac[wsxxSub].getbigdata()).divide(sqlRacdpfInner.getSqlCoSumins(), BigDecimal.ROUND_HALF_UP);
        // compute(wsaaWorkAreasInner.wsaaNewPremium, 6).setRounded(div(mult(mult(mult(sqlRacdpfInner.getSqlCoInstprem, wsaaWorkAreasInner.wsaaSrar), wsaaNumPremInsts),
        // t5450rec.rratfac[wsxxSub]), sqlRacdpfInner.getSqlCoSumins));
        
        if (sqlRacdpfInner.getSqlOppc2() != null && sqlRacdpfInner.getSqlOppc2().compareTo(BigDecimal.ZERO) > 0) {
            wsaaWorkAreasInner.wsaaNewPremium = wsaaWorkAreasInner.wsaaNewPremium.multiply(sqlRacdpfInner.getSqlOppc2());
//            compute(wsaaWorkAreasInner.wsaaNewPremium, 3).setRounded(
//                    mult(wsaaWorkAreasInner.wsaaNewPremium, sqlRacdpfInner.getSqlOppc2));
        }
    }

     protected void regularPrem3300() {
        rprmiumrec.rprmiumRec.set(SPACES);
        rprmiumrec.function.set("CALC");
        rprmiumrec.chdrChdrcoy.set(sqlRacdpfInner.getSqlChdrcoy());
        rprmiumrec.chdrChdrnum.set(sqlRacdpfInner.getSqlChdrnum());
        rprmiumrec.lifeLife.set(sqlRacdpfInner.getSqlLife());
        rprmiumrec.covrCoverage.set(sqlRacdpfInner.getSqlCoverage());
        rprmiumrec.covrRider.set(sqlRacdpfInner.getSqlRider());
        rprmiumrec.plnsfx.set(sqlRacdpfInner.getSqlPlnsfx());
        rprmiumrec.reinsCommprem.set(ZERO); //IBPLIFE-7238
        // changed by yy for ILPI-231
        rprmiumrec.reinsPremBaseRate.set(ZERO);
		rprmiumrec.reinsAnnPrem.set(ZERO);  //PINNACLE-2903
        rprmiumrec.mortcls.set(sqlRacdpfInner.getSqlCoMortcls());
        rprmiumrec.reasind.set("2");
        rprmiumrec.billfreq.set(wsaarcstfrq);
        rprmiumrec.reasratfac.set(t5450rec.rratfac[wsxxSub]);
        // changed by yy for ILPI-231
        rprmiumrec.mop.set(sqlRacdpfInner.getSqlChBillchnl());
        if (useJointLifeFlag) {
            rprmiumrec.lifeJlife.set("01");
        } else {
            rprmiumrec.lifeJlife.set("00");
        }
        // changed by yy for ILPI-231
        rprmiumrec.crtable.set(sqlRacdpfInner.getSqlCoCrtable());
        // changed by yy for ILPI-231
        rprmiumrec.currcode.set(sqlRacdpfInner.getSqlChCntcurr());
        rprmiumrec.effectdt.set(sqlRacdpfInner.getSqlChOccdate());
        rprmiumrec.termdate.set(sqlRacdpfInner.getSqlCoRiskCessDate());
        rprmiumrec.sumin.set(wsaaWorkAreasInner.wsaaSrar);
        rprmiumrec.calcPrem.set(ZERO);
        // changed by yy for ILPI-231
        /*rprmiumrec.ratingdate.set(sqlRacdpfInner.getSqlChOccdate());
        rprmiumrec.reRateDate.set(sqlRacdpfInner.getSqlChOccdate());*/
         if (bsscIO.getEffectiveDate().toInt() == (sqlRacdpfInner.getRrtdat())) {
        rprmiumrec.ratingdate.set(sqlRacdpfInner.getRrtdat());
        rprmiumrec.reRateDate.set(sqlRacdpfInner.getRrtdat());
        }else {
        	 rprmiumrec.ratingdate.set(sqlRacdpfInner.getRrtfrm());
             rprmiumrec.reRateDate.set(sqlRacdpfInner.getRrtfrm());
        }
        rprmiumrec.lsex.set(wsaaSex);
        rprmiumrec.lage.set(wsaaAge);
        rprmiumrec.jlsex.set(wsbbSex);
        rprmiumrec.jlage.set(wsbbAge);
        // changed by yy for ILPI-231
        datcon3rec.intDate1.set(sqlRacdpfInner.getSqlCoCrrcd());
        datcon3rec.intDate2.set(sqlRacdpfInner.getSqlCoRiskCessDate());
        datcon3rec.frequency.set(freqcpy.yrly);
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz, varcom.oK)) {
            syserrrec.statuz.set(datcon3rec.statuz);
            syserrrec.params.set(datcon3rec.datcon3Rec);
            fatalError600();
        }
        datcon3rec.freqFactor.add(0.99999);
        rprmiumrec.duration.set(datcon3rec.freqFactor);
        durationCtCm3350();
        /* IF WSAA-DURATION-CT-CM < T5450-INLPRD */
        if (isLT(wsaaWorkAreasInner.wsaaDurationCtCm, t5450rec.inlprd[wsxxSub])) {
            rprmiumrec.rrate.set(t5450rec.slctrate[wsxxSub]);
        } else {
            rprmiumrec.rrate.set(t5450rec.ultmrate[wsxxSub]);
        }
        rprmiumrec.seqNo.set(sqlRacdpfInner.getSqlSeqno());
        rprmiumrec.cnttype.set(sqlRacdpfInner.getSqlChCnttype());
		checkTransEffDate();
		//PINNACLE-2297
		if(isProrate) {
			//set start and end date for pro rated premium calculation
			rprmiumrec.bilfrmdt.set(wsaaWorkAreasInner.wsaaNewCtdate);
			rprmiumrec.biltodt.set(wsaaNextAnndate);// prorate end date for rerate
			if(isNE(wsaaEndDate,0)) {
				rprmiumrec.biltodt.set(wsaaEndDate);// prorate end date for freq change
			}
		}else {
			rprmiumrec.bilfrmdt.set(0);
			rprmiumrec.biltodt.set(0);
		}
        /* Call the Appropriate Sub-Routine. */
        if (useJointLifeFlag) {
            /* CALL T5450-JLPREMSUBR USING RPRM-RPRMIUM-REC */
            callProgram(t5450rec.rjlprem[wsxxSub], rprmiumrec.rprmiumRec);
        } else {
            /* CALL T5450-PREMSUBR USING RPRM-RPRMIUM-REC */
            callProgram(t5450rec.premsubr[wsxxSub], rprmiumrec.rprmiumRec);
        }
        if (isNE(rprmiumrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(rprmiumrec.statuz);
            syserrrec.params.set(rprmiumrec.rprmiumRec);
            fatalError600();
        }
        if(isTransEffDate) {
        	AppVars.getInstance().setBusinessdate("");
        }
        wsaaWorkAreasInner.wsaaNewPremium = rprmiumrec.calcPrem.getbigdata();
        /* MOVE WSAA-NEW-PREMIUM TO ZRDP-AMOUNT-IN. */
        /* PERFORM A000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO WSAA-NEW-PREMIUM. */
        if (wsaaWorkAreasInner.wsaaNewPremium.compareTo(BigDecimal.ZERO) != 0) {
        	zrdecplcPojo.setAmountIn(wsaaWorkAreasInner.wsaaNewPremium);
            a000CallRounding();
            wsaaWorkAreasInner.wsaaNewPremium = zrdecplcPojo.getAmountOut();
        }
    }
	
	protected void checkTransEffDate() {
		if(AppVars.getInstance().getBusinessdate().equals("")) {
        	String effDate = bsscIO.getEffectiveDate().toString();
            String formatDate = effDate.substring(6,8)+"/"+effDate.substring(4,6)+"/"+effDate.substring(0,4);
        	AppVars.getInstance().setBusinessdate(formatDate);
        	isTransEffDate = true;
        }
	}

    protected void durationCtCm3350() {
        /* START */
        /* Calculate the number of days between the Cession */
        /* Commencement date & the Costing Commencement date on RACD. */
        datcon3rec.datcon3Rec.set(SPACES);
        datcon3rec.frequency.set(freqcpy.mthly);
        // changed by yy for ILPI-231
        // datcon3rec.intDate1.set(covrIO.getCrrcd());
        datcon3rec.intDate1.set(sqlRacdpfInner.getSqlCoCrrcd());
        datcon3rec.intDate2.set(sqlCtDate); 
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz, varcom.oK)) {
            syserrrec.statuz.set(datcon3rec.statuz);
            syserrrec.params.set(datcon3rec.datcon3Rec);
            fatalError600();
        }
        wsaaWorkAreasInner.wsaaDurationCtCm = datcon3rec.freqFactor.toInt();
        /* EXIT */
    }

    protected void readT65983400(String itemitem) {
        // changed by yy for ILPI-231
        if (t6598Map.containsKey(itemitem)) {
            List<Itempf> t6598ItemList = t6598Map.get(itemitem);
            Itempf t6598Item = t6598ItemList.get(0);
            t6598rec.t6598Rec.set(StringUtil.rawToString(t6598Item.getGenarea()));
            return;
        }

        // if we don't found the record
        t6598rec.t6598Rec.set(SPACES);
    }

    protected void calcCommission3700() {
        wsaaWorkAreasInner.wsaaCommissionDue = BigDecimal.ZERO;
        if (wsaaWorkAreasInner.wsaaNewPremium.compareTo(BigDecimal.ZERO) <= 0 || isEQ(t5450rec.basicCommMeth[wsxxSub], SPACES)){
                //isLTE(wsaaWorkAreasInner.wsaaNewPremium, ZERO) || isEQ(t5450rec.basicCommMeth[wsxxSub], SPACES)) {
            return;
        }
        /* MOVE T5450-BASIC-COMM-METH TO WSKY-ITEM-ITEMITEM. */
        wsaaItemkey.itemItemitem.set(t5450rec.basicCommMeth[wsxxSub]);
        readT56473750();
        comlinkrec.clnkallRec.set(SPACES);
        comlinkrec.instprem.set(wsaaWorkAreasInner.wsaaNewPremium);
        comlinkrec.annprem.set(wsaaWorkAreasInner.wsaaNewPremium);
        callCommission3760();
        wsaaWorkAreasInner.wsaaCommissionDue = comlinkrec.rcommtot.getbigdata();
        /* MOVE WSAA-COMMISSION-DUE TO ZRDP-AMOUNT-IN. */
        /* PERFORM A000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO WSAA-COMMISSION-DUE. */
        if (BigDecimal.ZERO.compareTo(wsaaWorkAreasInner.wsaaCommissionDue)!=0) {
        	zrdecplcPojo.setAmountIn(wsaaWorkAreasInner.wsaaCommissionDue);
            a000CallRounding();
            wsaaWorkAreasInner.wsaaCommissionDue = zrdecplcPojo.getAmountOut();
        }
    }

    protected void readT56473750() {
        /* Read T5647 to obtain Commission Payment Subroutine. */
        // changed by yy for ILPI-231
        if (t5647Map.containsKey(wsaaItemkey.itemItemitem.trim())) {
            List<Itempf> t5647ItemList = t5647Map.get(wsaaItemkey.itemItemitem.trim());
            Itempf t5647Item = t5647ItemList.get(0);
            t5647rec.t5647Rec.set(StringUtil.rawToString(t5647Item.getGenarea()));
            return;
        }

        // if we don't found the record
        syserrrec.params.set(sqlRacdpfInner.getSqlRngmnt() + t5647);
        fatalError600();
    }

    protected void callCommission3760() {
        // changed by yy for ILPI-231
        // comlinkrec.agent.set(rasaIO.getAgntnum());
        comlinkrec.agent.set(sqlRacdpfInner.getSqlAgntnum());

        comlinkrec.chdrcoy.set(sqlRacdpfInner.getSqlChdrcoy());
        comlinkrec.chdrnum.set(sqlRacdpfInner.getSqlChdrnum());
        comlinkrec.life.set(sqlRacdpfInner.getSqlLife());
        comlinkrec.coverage.set(sqlRacdpfInner.getSqlCoverage());
        comlinkrec.rider.set(sqlRacdpfInner.getSqlRider());
        comlinkrec.planSuffix.set(sqlRacdpfInner.getSqlPlnsfx());
        /* Before calling the commission calculation subroutine with */
        /* a zero plan suffix, make sure a COVR record exists with the */
        /* plan-suffix - the commission subroutines need to find A */
        /* COVR record because otherwise they abend with a status of */
        /* MRNF - therefore we do the following POLSUM/POLINC check */
        /* to see if all COVRs are broken out/summarised etc... */
        // changed by yy for ILPI-231
        // if (isGT(chdrlifIO.getPolinc(),1)
        // && isEQ(chdrlifIO.getPolsum(),ZERO)) {
        if (sqlRacdpfInner.getSqlChPolinc() > 1 && sqlRacdpfInner.getSqlChPolsum() == 0) {
            comlinkrec.planSuffix.set(1);
        }
        /* MOVE T5450-COMMRATE TO CLNK-CRTABLE. */
        comlinkrec.crtable.set(sqlRacdpfInner.getSqlCoCrtable());
        // if (useJointLife.isTrue()) {
        if (useJointLifeFlag) {
            comlinkrec.jlife.set("01");
        } else {
            comlinkrec.jlife.set("00");
        }
        comlinkrec.method.set(wsaaItemkey.itemItemitem);
        comlinkrec.billfreq.set(wsaarcstfrq);
        // changed by yy for ILPI-231
        // comlinkrec.effdate.set(covrIO.getCrrcd());
        comlinkrec.effdate.set(sqlRacdpfInner.getSqlCoCrrcd());
        comlinkrec.language.set(bsscIO.getLanguage());
        /* Agent Class is spaces, as no Commission Enhancement is required */
        /* for reassurance commission calculations. */
        /* If required, the agent commission file (AGCM) will need to be */
        /* read, using RASA-AGNTNUM, to obtain the necessary details. */
        Aglfpf aglfpf = aglfpfDAO.searchAglflnb(wsaaChdrcoy, sqlRacdpfInner.getSqlAgntnum());
        comlinkrec.agentClass.set(aglfpf.getAgentClass());
        Agntpf agntlag = agntpfDAO.getAgntRec(wsaaChdrcoy, sqlRacdpfInner.getSqlAgntnum());
        if(agntlag!=null && agntlag.getValidflag().equals("1")) {
        	comlinkrec.agentype.set(agntlag.getAgtype().trim());
        }
        comlinkrec.reinsCode.set(sqlRacdpfInner.getSqlRasnum());
        if(isNE(rprmiumrec.reinsCommprem,ZERO)) {	//IBPLIFE-7238
        	comlinkrec.reinsCommprem.set(rprmiumrec.reinsCommprem);
        }else {
        	comlinkrec.reinsCommprem.set(ZERO);//IJS-501
        }
        comlinkrec.rcommtot.set(ZERO);
        comlinkrec.icommpd.set(ZERO);
        comlinkrec.icommernd.set(ZERO);
        comlinkrec.payamnt.set(ZERO);
        comlinkrec.erndamt.set(ZERO);
        comlinkrec.ptdate.set(0);
        comlinkrec.icommtot.set(ZERO);
        comlinkrec.gstAmount.set(ZERO);
        comlinkrec.commprem.set(ZERO);
        checkTransEffDate();
        callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
        if (isNE(comlinkrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(comlinkrec.statuz);
            syserrrec.params.set(comlinkrec.clnkallRec);
            fatalError600();
        }
        if(isTransEffDate) {
        	AppVars.getInstance().setBusinessdate("");
        }
    }

    protected void commit3500() {
        /* COMMIT */
        int busDate = bsscIO.getEffectiveDate().toInt();
        //PINNACLE-2760 Starts
        if(chdrBulkOpList!=null) {
	        chdrDAO.updateChdrRecord(chdrBulkOpList, busDate);
	        chdrDAO.insertChdrRecord(chdrBulkOpList, busDate);
	        chdrBulkOpList.clear();
        }
        if(racdrskBulkOpList!=null) {
	        racdDAO.updateRacdBulk(racdrskBulkOpList, busDate);
	        racdDAO.insertRacdBulk(racdrskBulkOpList);
	        racdrskBulkOpList.clear();
        }
        if(ptrnBulkOpList!=null) {
	        ptrnpfDAO.insertPtrnPF(ptrnBulkOpList);
	        ptrnBulkOpList.clear();
        }
        //PINNACLE-2760 End
        
        contotrec.totno.set(ct01);
        contotrec.totval.set(ct01Count);
        callContot001();
        contotrec.totno.set(ct02);
        contotrec.totval.set(ct02Prem);
        callContot001();
        contotrec.totno.set(ct03);
        contotrec.totval.set(ct03Comm);
        callContot001();
        contotrec.totno.set(ct04);
        contotrec.totval.set(ct04Tax);
        callContot001();
        contotrec.totno.set(ct05);
        contotrec.totval.set(ct05Net);
        callContot001();
        contotrec.totno.set(ct06);
        contotrec.totval.set(ct06Sftlck);	//PINNACLE-2760
        callContot001();
        
        ct01Count = 0;
        ct02Prem = BigDecimal.ZERO;
        ct03Comm = BigDecimal.ZERO;
        ct04Tax = BigDecimal.ZERO;
        ct05Net = BigDecimal.ZERO;
        /* EXIT */
    }

    protected void rollback3600() {
        /* ROLLBACK */
        /* EXIT */
    }

    protected void close4000() {
        /* CLOSE-FILES */
        /* Close the cursor */
        //getAppVars().freeDBConnectionIgnoreErr(sqlracdpf1conn, sqlracdpf1ps, sqlracdpf1rs);
        
        t5448Map.clear();
        t5449Map.clear();
        t5450Map.clear();
        t5647Map.clear();
        t6598Map.clear();
        th621Map.clear();
        
        if (chdrBulkOpList != null) {
            chdrBulkOpList.clear();
        }
        if (ptrnBulkOpList != null) {
            ptrnBulkOpList.clear();
        }
        if (racdrskBulkOpList != null) {
            racdrskBulkOpList.clear();
        }
        if (racdSearchResultDtoList != null) {
            racdSearchResultDtoList.clear();
        }
        t5448Map = null;
        t5449Map = null;
        t5450Map = null;
        t5647Map = null;
        t6598Map = null;
        th621Map = null;
        chdrBulkOpList = null;
        ptrnBulkOpList = null;
        racdrskBulkOpList = null;
        racdSearchResultDtoList = null;
        sqlRacdpfInner = null;
        
        lsaaStatuz.set(varcom.oK);
        /* EXIT */
    }

    protected void a000CallRounding() {     
        zrdecplcPojo.setFunction(SPACES.toString());
    	zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
    	zrdecplcPojo.setStatuz(varcom.oK.toString());
    	zrdecplcPojo.setCurrency(sqlRacdpfInner.getSqlChCntcurr());
    	zrdecplcPojo.setBatctrcde(bprdIO.getAuthCode().toString());
    	zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
    	if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
    		syserrrec.statuz.set(zrdecplcPojo.getStatuz());
    		fatalError600();
    	}
    }


    /*
     * Class transformed from Data Structure WSAA-WORK-AREAS--INNER
     */
    private static final class WsaaWorkAreasInner {

        private String wsaaT5449Rprmmth;
        private BigDecimal wsaaNewPremium;
        private BigDecimal wsaaTotalReserve;
        private BigDecimal wsaaPropReserve;
        private BigDecimal wsaaSrar;
        private BigDecimal wsaaNetPrem;
        private int wsaaDurationCtCm;
        private BigDecimal wsaaCommissionDue;
        private int wsaaNewCtdate;
    }
}
