package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:14
 * Description:
 * Copybook name: T5448REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5448rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5448Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData lrkclsdsc = new FixedLengthStringData(25).isAPartOf(t5448Rec, 0);
  	public FixedLengthStringData proppr = new FixedLengthStringData(1).isAPartOf(t5448Rec, 25);
  	public FixedLengthStringData rcedsar = new FixedLengthStringData(1).isAPartOf(t5448Rec, 26);
  	public FixedLengthStringData rclmbas = new FixedLengthStringData(4).isAPartOf(t5448Rec, 27);
  	public FixedLengthStringData rcstfrq = new FixedLengthStringData(2).isAPartOf(t5448Rec, 31);
  	public FixedLengthStringData rcstsar = new FixedLengthStringData(1).isAPartOf(t5448Rec, 33);
  	public ZonedDecimalData rprior = new ZonedDecimalData(2, 0).isAPartOf(t5448Rec, 34);
  	public FixedLengthStringData rresmeth = new FixedLengthStringData(4).isAPartOf(t5448Rec, 36);
  	public FixedLengthStringData rrevfrq = new FixedLengthStringData(2).isAPartOf(t5448Rec, 40);
  	public FixedLengthStringData rrngmnts = new FixedLengthStringData(40).isAPartOf(t5448Rec, 42);
  	public FixedLengthStringData[] rrngmnt = FLSArrayPartOfStructure(10, 4, rrngmnts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(rrngmnts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rrngmnt01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData rrngmnt02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData rrngmnt03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData rrngmnt04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData rrngmnt05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData rrngmnt06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData rrngmnt07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData rrngmnt08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData rrngmnt09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData rrngmnt10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData rrsktyp = new FixedLengthStringData(4).isAPartOf(t5448Rec, 82);
  	public FixedLengthStringData rtaxbas = new FixedLengthStringData(4).isAPartOf(t5448Rec, 86);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(410).isAPartOf(t5448Rec, 90, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5448Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5448Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}