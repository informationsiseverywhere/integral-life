/*
 * File: P5438.java
 * Date: 30 August 2009 0:25:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P5438.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S5438ScreenVars;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.life.reassurance.tablestructures.T5455rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Facultative Reassurance Details.
*
* This screen runs off the P5437 facultative cession selection
* screen. It is used to add, modify, delete or enquire upon
* facultative RACD records that have not been activated.
*
* Validation:
*
* - For Add or Modify, the following fields must be entered:
*   Reassurer, Araangement, Amount Reassured.
*
* - Reassurer must be on the RASA file.
*
* - Reassurer status must allow cessions (read T5455).
*
* - Arrangement must exist on T5449 and must be the same risk
*   class as the component (from T5448).
*
* - The reassurer must be a participant in the arrangement
*   (T5449).
*
* - The sum reassured for each cession must not exceed the
*   component sum assured.
*
*****************************************************************
* </pre>
*/
public class P5438 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5438");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaChdrkey = new FixedLengthStringData(11);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaChdrkey, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaChdrkey, 2);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaChdrkey, 3);
	private String wsaaDelete = "Press ENTER to confirm Delete";
	private PackedDecimalData wsaaT5449Ix = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTh618Ix = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaReassurerFound = new FixedLengthStringData(1);
	private Validator reassurerFound = new Validator(wsaaReassurerFound, "Y");
		/* ERRORS */
	private String e186 = "E186";
	private String e946 = "E946";
	private String h090 = "H090";
	private String r050 = "R050";
	private String r057 = "R057";
	private String r059 = "R059";
	private String r061 = "R061";
	private String r062 = "R062";
	private String r063 = "R063";
		/* TABLES */
	private String t5449 = "T5449";
	private String t5455 = "T5455";
	private String th618 = "TH618";
		/* FORMATS */
	private String cltsrec = "CLTSREC";
	private String itemrec = "ITEMREC";
	private String itdmrec = "ITEMREC";
	private String racdrec = "RACDREC";
	private String racdlnbrec = "RACDLNBREC";
	private String rasarec = "RASAREC";
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Reassurance Cession Details Logical*/
	private RacdTableDAM racdIO = new RacdTableDAM();
		/*Reassurance Cession Details New Busines*/
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private T5449rec t5449rec = new T5449rec();
	private T5455rec t5455rec = new T5455rec();
	private Th618rec th618rec = new Th618rec();
	private Wssplife wssplife = new Wssplife();
	private S5438ScreenVars sv = ScreenProgram.getScreenVars( S5438ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		cont1020, 
		preExit, 
		exit2090, 
		exit2290, 
		exit3090
	}

	public P5438() {
		super();
		screenVars = sv;
		new ScreenModel("S5438", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case cont1020: {
					cont1020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		sv.dataArea.set(SPACES);
		sv.sumins.set(wssplife.bigAmt);
		if (isEQ(wsspcomn.flag,"C")) {
			sv.raAmount.set(ZERO);
			sv.cmdate.set(varcom.vrcmMaxDate);
			sv.ctdate.set(varcom.vrcmMaxDate);
			goTo(GotoLabel.cont1020);
		}
		racdlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		sv.rasnum.set(racdlnbIO.getRasnum());
		sv.rngmnt.set(racdlnbIO.getRngmnt());
		sv.cmdate.set(racdlnbIO.getCmdate());
		sv.ctdate.set(racdlnbIO.getCtdate());
		sv.raAmount.set(racdlnbIO.getRaAmount());
		if (isEQ(racdlnbIO.getRasnum(),SPACES)) {
			goTo(GotoLabel.cont1020);
		}
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(wsspcomn.company);
		rasaIO.setRasnum(racdlnbIO.getRasnum());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		
		/* ILIFE-6026 cltnme is changed to clntname, refer DDFieldWindowing of rasnum. the code is moved to readReassure whichis called from validate2020 also. */
		readReassurer();
	}

/* START OF ILIFE 6026 */
protected void readReassurer(){
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(rasaIO.getClntcoy());
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.clntname.set(wsspcomn.longconfname);
}
/* END OF ILIFE-6026 */

protected void cont1020()
	{
		if (isEQ(wsspcomn.flag,"D")) {
			sv.comt.set(wsaaDelete);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"D")) {
			scrnparams.function.set("PROT");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"D")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"C")) {
			sv.cmdate.set(wsspcomn.currfrom);
		}
		if (isEQ(sv.raAmount,ZERO)) {
			sv.raamountErr.set(e186);
		}
		if (isGT(sv.raAmount,sv.sumins)) {
			sv.raamountErr.set(h090);
		}
		if (isEQ(sv.rngmnt,SPACES)) {
			sv.rngmntErr.set(e186);
		}
		if (isEQ(sv.rasnum,SPACES)) {
			sv.rasnumErr.set(e186);
		}
		else {
			rasaIO.setParams(SPACES);
			rasaIO.setRascoy(wsspcomn.company);
			rasaIO.setRasnum(sv.rasnum);
			rasaIO.setFormat(rasarec);
			rasaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, rasaIO);
			if (isEQ(rasaIO.getStatuz(),varcom.oK)) {
				readT54552100();
			}
			else {
				sv.rasnumErr.set(e946);
			}
			readReassurer();  //ILIFE-6026
		}
		if (isNE(sv.rngmnt,SPACES)) {
			readT54492200();
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readT54552100()
	{
		t54552110();
	}

protected void t54552110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5455);
		itemIO.setItemitem(rasaIO.getReasst());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.rasnumErr.set(r050);
		}
		else {
			t5455rec.t5455Rec.set(itemIO.getGenarea());
			if (isEQ(t5455rec.cesnind,SPACES)) {
				sv.rasnumErr.set(r061);
			}
		}
	}

protected void readT54492200()
	{
		try {
			t54492210();
		}
		catch (GOTOException e){
		}
	}

protected void t54492210()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(sv.rngmnt);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5449)
		|| isNE(itdmIO.getItemitem(),sv.rngmnt)) {
			sv.rngmntErr.set(r057);
			goTo(GotoLabel.exit2290);
		}
		t5449rec.t5449Rec.set(itdmIO.getGenarea());
		if (isNE(t5449rec.retype,"F")) {
			sv.rngmntErr.set(r062);
			goTo(GotoLabel.exit2290);
		}
		readTh6182250();
		for (wsaaTh618Ix.set(1); !(isGT(wsaaTh618Ix,5)
		|| isEQ(th618rec.lrkcls[wsaaTh618Ix.toInt()],t5449rec.lrkcls)); wsaaTh618Ix.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaTh618Ix,5)) {
			sv.rngmntErr.set(r063);
			goTo(GotoLabel.exit2290);
		}
		if (isNE(sv.rasnum,SPACES)
		&& isEQ(sv.rasnumErr,SPACES)) {
			wsaaReassurerFound.set("N");
			for (wsaaT5449Ix.set(1); !(isGT(wsaaT5449Ix,10)
			|| reassurerFound.isTrue()); wsaaT5449Ix.add(1)){
				if (isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()],sv.rasnum)) {
					wsaaReassurerFound.set("Y");
				}
			}
			if (isGT(wsaaT5449Ix,10)) {
				sv.rasnumErr.set(r059);
			}
		}
	}

protected void readTh6182250()
	{
		start2251();
	}

protected void start2251()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th618);
		itemIO.setItemitem(wssplife.lrkcls);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		th618rec.th618Rec.set(itdmIO.getGenarea());
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"C")) {
			writeNewRacd3300();
		}
		else {
			if (isEQ(wsspcomn.flag,"M")) {
				rlseRacd3100();
				updateRacd3200();
			}
			else {
				if (isEQ(wsspcomn.flag,"D")) {
					rlseRacd3100();
					deleteRacd3400();
				}
			}
		}
	}

protected void rlseRacd3100()
	{
		/*RELEASE*/
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateRacd3200()
	{
		/*UPDATE*/
		racdlnbIO.setRasnum(sv.rasnum);
		racdlnbIO.setRngmnt(sv.rngmnt);
		racdlnbIO.setRaAmount(sv.raAmount);
		racdlnbIO.setOvrdind("Y");
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writeNewRacd3300()
	{
		write3301();
	}

protected void write3301()
	{
		racdIO.setParams(SPACES);
		wsaaChdrkey.set(wssplife.chdrky);
		racdIO.setChdrcoy(wsaaChdrcoy);
		racdIO.setChdrnum(wsaaChdrnum);
		racdIO.setLife(wssplife.life);
		racdIO.setCoverage(wssplife.coverage);
		racdIO.setRider(wssplife.rider);
		racdIO.setPlanSuffix(wssplife.planSuffix);
		racdIO.setCurrcode(wssplife.cntcurr);
		racdIO.setTranno(wsspcomn.tranno);
		racdIO.setRasnum(sv.rasnum);
		racdIO.setRngmnt(sv.rngmnt);
		racdIO.setRaAmount(sv.raAmount);
		racdIO.setCmdate(sv.cmdate);
		racdIO.setCurrfrom(sv.cmdate);
		racdIO.setCtdate(sv.cmdate);
		racdIO.setSeqno(ZERO);
		racdIO.setRecovamt(ZERO);
		racdIO.setCestype("2");
		racdIO.setRetype("F");
		racdIO.setOvrdind("Y");
		if (isNE(wssplife.rrevfrq,SPACES)) {
			calcReviewDate3350();
			racdIO.setRrevdt(datcon2rec.intDate2);
		}
		else {
			racdIO.setRrevdt(varcom.vrcmMaxDate);
		}
		setPrecision(racdIO.getReasper(), 2);
		racdIO.setReasper(mult(div(sv.raAmount,wssplife.bigAmt),100));
		racdIO.setValidflag("3");
		racdIO.setCurrto(varcom.vrcmMaxDate);
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			fatalError600();
		}
	}

protected void calcReviewDate3350()
	{
		calc3351();
	}

protected void calc3351()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wssplife.occdate);
		datcon3rec.intDate2.set(racdIO.getCmdate());
		datcon3rec.frequency.set(wssplife.rrevfrq);
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wssplife.occdate);
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(wssplife.rrevfrq);
		compute(datcon2rec.freqFactor, 5).set(add(1,datcon3rec.freqFactor));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
	}

protected void deleteRacd3400()
	{
		/*DELETE*/
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
