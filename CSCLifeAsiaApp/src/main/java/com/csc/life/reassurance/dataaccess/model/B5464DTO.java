package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;

public class B5464DTO {
	/* SQL-RACDPF */
	private String sqlChdrcoy;
	private String sqlChdrnum;
	private String sqlLife;
	private String sqlCoverage;
	private String sqlRider;
	private int sqlPlnsfx;
	private int sqlSeqno;
	private String sqlRasnum;
	private String sqlRngmnt;
	private int sqlCurrfrom;
	private int sqlTranno;
	private String sqlValidflag;
	private String sqlCurrcode;
	private int sqlCurrto;
	private String sqlRetype;
	private BigDecimal sqlRaamount;
	private int sqlCtdate;
	private int sqlCmdate;
	private BigDecimal sqlReasper;
	private BigDecimal sqlRecovamt;
	private String sqlCestype;
	private String sqlLrkcls;
	private int sqlRrevdt;

	// CHDRPF
	private int sqlChTranno;
	private String sqlChCnttype;
	private String sqlChStatcode;
	private String sqlChPstatcode;
	private String sqlChBillfreq;
	private String sqlChCntcurr;
	private int sqlChPolsum;
	private String sqlChBillchnl;
	private int sqlChOccdate;
	private int sqlChPolinc;
	private int sqlChPtdate;

	// COVRPF
	private int sqlCoCrrcd;
	private String sqlCoCrtable;
	private BigDecimal sqlCoSumins;
	private String sqlCoJlife;
	private BigDecimal sqlCoInstprem;
	private String sqlCoChdrcoy;
	private String sqlCoChdrnum;
	private String sqlCoLife;
	private String sqlCoCoverage;
	private String sqlCoRider;
	private int sqlCoPlnsfx;

	// LIFEPF
	private String sqlJlife1;
	private String sqlLifcnum1;
	private String sqlJlife2;
	private String sqlLifcnum2;

	// add field
	private long sqlRaUniqueNum;
	private long sqlChUniqueNum;

	public String getSqlChdrcoy() {
		return sqlChdrcoy;
	}

	public void setSqlChdrcoy(String sqlChdrcoy) {
		this.sqlChdrcoy = sqlChdrcoy;
	}

	public String getSqlChdrnum() {
		return sqlChdrnum;
	}

	public void setSqlChdrnum(String sqlChdrnum) {
		this.sqlChdrnum = sqlChdrnum;
	}

	public String getSqlLife() {
		return sqlLife;
	}

	public void setSqlLife(String sqlLife) {
		this.sqlLife = sqlLife;
	}

	public String getSqlCoverage() {
		return sqlCoverage;
	}

	public void setSqlCoverage(String sqlCoverage) {
		this.sqlCoverage = sqlCoverage;
	}

	public String getSqlRider() {
		return sqlRider;
	}

	public void setSqlRider(String sqlRider) {
		this.sqlRider = sqlRider;
	}

	public int getSqlPlnsfx() {
		return sqlPlnsfx;
	}

	public void setSqlPlnsfx(int sqlPlnsfx) {
		this.sqlPlnsfx = sqlPlnsfx;
	}

	public int getSqlSeqno() {
		return sqlSeqno;
	}

	public void setSqlSeqno(int sqlSeqno) {
		this.sqlSeqno = sqlSeqno;
	}

	public String getSqlRasnum() {
		return sqlRasnum;
	}

	public void setSqlRasnum(String sqlRasnum) {
		this.sqlRasnum = sqlRasnum;
	}

	public String getSqlRngmnt() {
		return sqlRngmnt;
	}

	public void setSqlRngmnt(String sqlRngmnt) {
		this.sqlRngmnt = sqlRngmnt;
	}

	public int getSqlCurrfrom() {
		return sqlCurrfrom;
	}

	public void setSqlCurrfrom(int sqlCurrfrom) {
		this.sqlCurrfrom = sqlCurrfrom;
	}

	public int getSqlTranno() {
		return sqlTranno;
	}

	public void setSqlTranno(int sqlTranno) {
		this.sqlTranno = sqlTranno;
	}

	public String getSqlValidflag() {
		return sqlValidflag;
	}

	public void setSqlValidflag(String sqlValidflag) {
		this.sqlValidflag = sqlValidflag;
	}

	public String getSqlCurrcode() {
		return sqlCurrcode;
	}

	public void setSqlCurrcode(String sqlCurrcode) {
		this.sqlCurrcode = sqlCurrcode;
	}

	public int getSqlCurrto() {
		return sqlCurrto;
	}

	public void setSqlCurrto(int sqlCurrto) {
		this.sqlCurrto = sqlCurrto;
	}

	public String getSqlRetype() {
		return sqlRetype;
	}

	public void setSqlRetype(String sqlRetype) {
		this.sqlRetype = sqlRetype;
	}

	public BigDecimal getSqlRaamount() {
		return sqlRaamount;
	}

	public void setSqlRaamount(BigDecimal sqlRaamount) {
		this.sqlRaamount = sqlRaamount;
	}

	public int getSqlCtdate() {
		return sqlCtdate;
	}

	public void setSqlCtdate(int sqlCtdate) {
		this.sqlCtdate = sqlCtdate;
	}

	public int getSqlCmdate() {
		return sqlCmdate;
	}

	public void setSqlCmdate(int sqlCmdate) {
		this.sqlCmdate = sqlCmdate;
	}

	public BigDecimal getSqlReasper() {
		return sqlReasper;
	}

	public void setSqlReasper(BigDecimal sqlReasper) {
		this.sqlReasper = sqlReasper;
	}

	public BigDecimal getSqlRecovamt() {
		return sqlRecovamt;
	}

	public void setSqlRecovamt(BigDecimal sqlRecovamt) {
		this.sqlRecovamt = sqlRecovamt;
	}

	public String getSqlCestype() {
		return sqlCestype;
	}

	public void setSqlCestype(String sqlCestype) {
		this.sqlCestype = sqlCestype;
	}

	public String getSqlLrkcls() {
		return sqlLrkcls;
	}

	public void setSqlLrkcls(String sqlLrkcls) {
		this.sqlLrkcls = sqlLrkcls;
	}

	public int getSqlChTranno() {
		return sqlChTranno;
	}

	public void setSqlChTranno(int sqlChTranno) {
		this.sqlChTranno = sqlChTranno;
	}

	public String getSqlChCnttype() {
		return sqlChCnttype;
	}

	public void setSqlChCnttype(String sqlChCnttype) {
		this.sqlChCnttype = sqlChCnttype;
	}

	public String getSqlChStatcode() {
		return sqlChStatcode;
	}

	public void setSqlChStatcode(String sqlChStatcode) {
		this.sqlChStatcode = sqlChStatcode;
	}

	public String getSqlChPstatcode() {
		return sqlChPstatcode;
	}

	public void setSqlChPstatcode(String sqlChPstatcode) {
		this.sqlChPstatcode = sqlChPstatcode;
	}

	public String getSqlChBillfreq() {
		return sqlChBillfreq;
	}

	public void setSqlChBillfreq(String sqlChBillfreq) {
		this.sqlChBillfreq = sqlChBillfreq;
	}

	public String getSqlChCntcurr() {
		return sqlChCntcurr;
	}

	public void setSqlChCntcurr(String sqlChCntcurr) {
		this.sqlChCntcurr = sqlChCntcurr;
	}

	public int getSqlChPolsum() {
		return sqlChPolsum;
	}

	public void setSqlChPolsum(int sqlChPolsum) {
		this.sqlChPolsum = sqlChPolsum;
	}

	public String getSqlChBillchnl() {
		return sqlChBillchnl;
	}

	public void setSqlChBillchnl(String sqlChBillchnl) {
		this.sqlChBillchnl = sqlChBillchnl;
	}

	public int getSqlChOccdate() {
		return sqlChOccdate;
	}

	public void setSqlChOccdate(int sqlChOccdate) {
		this.sqlChOccdate = sqlChOccdate;
	}

	public int getSqlChPolinc() {
		return sqlChPolinc;
	}

	public void setSqlChPolinc(int sqlChPolinc) {
		this.sqlChPolinc = sqlChPolinc;
	}

	public int getSqlCoCrrcd() {
		return sqlCoCrrcd;
	}

	public void setSqlCoCrrcd(int sqlCoCrrcd) {
		this.sqlCoCrrcd = sqlCoCrrcd;
	}

	public String getSqlCoCrtable() {
		return sqlCoCrtable;
	}

	public void setSqlCoCrtable(String sqlCoCrtable) {
		this.sqlCoCrtable = sqlCoCrtable;
	}

	public BigDecimal getSqlCoSumins() {
		return sqlCoSumins;
	}

	public void setSqlCoSumins(BigDecimal sqlCoSumins) {
		this.sqlCoSumins = sqlCoSumins;
	}

	public String getSqlCoJlife() {
		return sqlCoJlife;
	}

	public void setSqlCoJlife(String sqlCoJlife) {
		this.sqlCoJlife = sqlCoJlife;
	}

	public BigDecimal getSqlCoInstprem() {
		return sqlCoInstprem;
	}

	public void setSqlCoInstprem(BigDecimal sqlCoInstprem) {
		this.sqlCoInstprem = sqlCoInstprem;
	}

	public String getSqlCoChdrcoy() {
		return sqlCoChdrcoy;
	}

	public void setSqlCoChdrcoy(String sqlCoChdrcoy) {
		this.sqlCoChdrcoy = sqlCoChdrcoy;
	}

	public String getSqlCoChdrnum() {
		return sqlCoChdrnum;
	}

	public void setSqlCoChdrnum(String sqlCoChdrnum) {
		this.sqlCoChdrnum = sqlCoChdrnum;
	}

	public String getSqlCoLife() {
		return sqlCoLife;
	}

	public void setSqlCoLife(String sqlCoLife) {
		this.sqlCoLife = sqlCoLife;
	}

	public String getSqlCoCoverage() {
		return sqlCoCoverage;
	}

	public void setSqlCoCoverage(String sqlCoCoverage) {
		this.sqlCoCoverage = sqlCoCoverage;
	}

	public String getSqlCoRider() {
		return sqlCoRider;
	}

	public void setSqlCoRider(String sqlCoRider) {
		this.sqlCoRider = sqlCoRider;
	}

	public int getSqlCoPlnsfx() {
		return sqlCoPlnsfx;
	}

	public void setSqlCoPlnsfx(int sqlCoPlnsfx) {
		this.sqlCoPlnsfx = sqlCoPlnsfx;
	}

	public int getSqlRrevdt() {
		return sqlRrevdt;
	}

	public void setSqlRrevdt(int sqlRrevdt) {
		this.sqlRrevdt = sqlRrevdt;
	}

	public int getSqlChPtdate() {
		return sqlChPtdate;
	}

	public void setSqlChPtdate(int sqlChPtdate) {
		this.sqlChPtdate = sqlChPtdate;
	}

	public String getSqlJlife1() {
		return sqlJlife1;
	}

	public void setSqlJlife1(String sqlJlife1) {
		this.sqlJlife1 = sqlJlife1;
	}

	public String getSqlLifcnum1() {
		return sqlLifcnum1;
	}

	public void setSqlLifcnum1(String sqlLifcnum1) {
		this.sqlLifcnum1 = sqlLifcnum1;
	}

	public String getSqlJlife2() {
		return sqlJlife2;
	}

	public void setSqlJlife2(String sqlJlife2) {
		this.sqlJlife2 = sqlJlife2;
	}

	public String getSqlLifcnum2() {
		return sqlLifcnum2;
	}

	public void setSqlLifcnum2(String sqlLifcnum2) {
		this.sqlLifcnum2 = sqlLifcnum2;
	}

	public long getSqlRaUniqueNum() {
		return sqlRaUniqueNum;
	}

	public void setSqlRaUniqueNum(long sqlRaUniqueNum) {
		this.sqlRaUniqueNum = sqlRaUniqueNum;
	}

	public long getSqlChUniqueNum() {
		return sqlChUniqueNum;
	}

	public void setSqlChUniqueNum(long sqlChUniqueNum) {
		this.sqlChUniqueNum = sqlChUniqueNum;
	}

}
