package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:24
 * Description:
 * Copybook name: RACTBTCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ractbtckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ractbtcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ractbtcKey = new FixedLengthStringData(64).isAPartOf(ractbtcFileKey, 0, REDEFINE);
  	public FixedLengthStringData ractbtcChdrcoy = new FixedLengthStringData(1).isAPartOf(ractbtcKey, 0);
  	public FixedLengthStringData ractbtcChdrnum = new FixedLengthStringData(8).isAPartOf(ractbtcKey, 1);
  	public FixedLengthStringData ractbtcLife = new FixedLengthStringData(2).isAPartOf(ractbtcKey, 9);
  	public FixedLengthStringData ractbtcCoverage = new FixedLengthStringData(2).isAPartOf(ractbtcKey, 11);
  	public FixedLengthStringData ractbtcRider = new FixedLengthStringData(2).isAPartOf(ractbtcKey, 13);
  	public FixedLengthStringData ractbtcRasnum = new FixedLengthStringData(8).isAPartOf(ractbtcKey, 15);
  	public FixedLengthStringData ractbtcRatype = new FixedLengthStringData(4).isAPartOf(ractbtcKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(ractbtcKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ractbtcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ractbtcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}