/*
 * File: Grevreas.java
 * Date: 29 August 2009 22:50:51
 * Author: Quipoz Limited
 *
 * Class transformed from GREVREAS.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.reassurance.dataaccess.LirrconTableDAM;
import com.csc.life.reassurance.dataaccess.LirrrevTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhconTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhrevTableDAM;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdrevTableDAM;
import com.csc.life.reassurance.dataaccess.RecoTableDAM;
import com.csc.life.reassurance.dataaccess.RecorcoTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Generic Reassurance Reversal Subroutine.
*
* This is a new subroutine which forms part of Reassurance
* Reversals. It is called upon via T5671 for any reversable
* transactions that create reassurance records (i.e. Lapse,
* Surrender, Maturity, Expiry, Death Claim, Reassurance
* Costing, Reassurance Review, Cession Maintenance, Contract
* Auto Cession, Component Modify, Component Add and Paid-up).
*
* It will be called using the standard generic reversal linkage
* copybook, GREVERSREC. It will delete all RACD, LIRR, LRRH and
* RECO records created by the transaction being reversed and it
* will reinstate the previous validflag '2' records.
*
* This subroutine is performed at component level.
*
*****************************************************************
* </pre>
*/
public class Grevreas extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GREVREAS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String lirrconrec = "LIRRCONREC";
	private String lirrrevrec = "LIRRREVREC";
	private String lrrhconrec = "LRRHCONREC";
	private String lrrhrevrec = "LRRHREVREC";
	private String racdrec = "RACDREC";
	private String racdrevrec = "RACDREVREC";
	private String recorec = "RECOREC";
	private String recorcorec = "RECORCOREC";
	private Greversrec greversrec = new Greversrec();
		/*Reassurer Exp. History Contract Level*/
	private LirrconTableDAM lirrconIO = new LirrconTableDAM();
		/*Reassurer Experience History Reversals*/
	private LirrrevTableDAM lirrrevIO = new LirrrevTableDAM();
		/*Life Retention and Reassurance History*/
	private LrrhconTableDAM lrrhconIO = new LrrhconTableDAM();
		/*Life Retention & Reass. History Reversal*/
	private LrrhrevTableDAM lrrhrevIO = new LrrhrevTableDAM();
		/*Reassurance Cession Details Logical*/
	private RacdTableDAM racdIO = new RacdTableDAM();
		/*Reassurance Cession Details For Reversal*/
	private RacdrevTableDAM racdrevIO = new RacdrevTableDAM();
		/*Reassurance Costing Details Logical*/
	private RecoTableDAM recoIO = new RecoTableDAM();
		/*Reassurance Costing Details By Seqno.*/
	private RecorcoTableDAM recorcoIO = new RecorcoTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr1080,
		exit1090,
		exit1190,
		nextr2080,
		exit2090,
		exit2190,
		nextr3080,
		exit3090,
		exit3190,
		nextr4080,
		exit4090
	}

	public Grevreas() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		greversrec.statuz.set(varcom.oK);
		racdIO.setParams(SPACES);
		racdIO.setChdrcoy(greversrec.chdrcoy);
		racdIO.setChdrnum(greversrec.chdrnum);
		racdIO.setLife(greversrec.life);
		racdIO.setCoverage(greversrec.coverage);
		racdIO.setRider(greversrec.rider);
		racdIO.setPlanSuffix(greversrec.planSuffix);
		racdIO.setSeqno(ZERO);
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdIO.getStatuz(),varcom.endp))) {
			processRacd1000();
		}

		lrrhconIO.setParams(SPACES);
		lrrhconIO.setCompany(greversrec.chdrcoy);
		lrrhconIO.setChdrnum(greversrec.chdrnum);
		lrrhconIO.setLife(greversrec.life);
		lrrhconIO.setCoverage(greversrec.coverage);
		lrrhconIO.setRider(greversrec.rider);
		lrrhconIO.setPlanSuffix(greversrec.planSuffix);
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhconIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhconIO.setFitKeysSearch("COMPANY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(lrrhconIO.getStatuz(),varcom.endp))) {
			processLrrh2000();
		}

		lirrconIO.setParams(SPACES);
		lirrconIO.setCompany(greversrec.chdrcoy);
		lirrconIO.setChdrnum(greversrec.chdrnum);
		lirrconIO.setLife(greversrec.life);
		lirrconIO.setCoverage(greversrec.coverage);
		lirrconIO.setRider(greversrec.rider);
		lirrconIO.setPlanSuffix(greversrec.planSuffix);
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lirrconIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lirrconIO.setFitKeysSearch("COMPANY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(lirrconIO.getStatuz(),varcom.endp))) {
			processLirr3000();
		}

		recoIO.setParams(SPACES);
		recoIO.setChdrcoy(greversrec.chdrcoy);
		recoIO.setChdrnum(greversrec.chdrnum);
		recoIO.setLife(greversrec.life);
		recoIO.setCoverage(greversrec.coverage);
		recoIO.setRider(greversrec.rider);
		recoIO.setPlanSuffix(greversrec.planSuffix);
		recoIO.setFormat(recorec);
		recoIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		recoIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(recoIO.getStatuz(),varcom.endp))) {
			processReco4000();
		}

	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void processRacd1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					racd1010();
				}
				case nextr1080: {
					nextr1080();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void racd1010()
	{
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(),varcom.oK)
		&& isNE(racdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
		if (isNE(racdIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(racdIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(racdIO.getLife(),greversrec.life)
		|| isNE(racdIO.getCoverage(),greversrec.coverage)
		|| isNE(racdIO.getRider(),greversrec.rider)
		|| isNE(racdIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(racdIO.getStatuz(),varcom.endp)) {
			racdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(racdIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr1080);
		}
		//PINNACLE-3048
		if (isNE(racdIO.getValidflag(),"1")
		&& isNE(racdIO.getValidflag(),"4") && isNE(racdIO.getValidflag(),"2")) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
		deleteUpdateRacd1100();
	}

protected void nextr1080()
	{
		racdIO.setFunction(varcom.nextr);
	}

protected void deleteUpdateRacd1100()
	{
		try {
			racd1110();
		}
		catch (GOTOException e){
		}
	}

protected void racd1110()
	{
		racdrevIO.setParams(SPACES);
		racdrevIO.setRrn(racdIO.getRrn());
		racdrevIO.setFormat(racdrevrec);
		racdrevIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdrevIO);
		if (isNE(racdrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrevIO.getParams());
			dbError580();
		}
		racdrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdrevIO);
		if (isNE(racdrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrevIO.getParams());
			dbError580();
		}
		racdrevIO.setParams(SPACES);
		racdrevIO.setChdrcoy(racdIO.getChdrcoy());
		racdrevIO.setChdrnum(racdIO.getChdrnum());
		racdrevIO.setLife(racdIO.getLife());
		racdrevIO.setCoverage(racdIO.getCoverage());
		racdrevIO.setRider(racdIO.getRider());
		racdrevIO.setPlanSuffix(racdIO.getPlanSuffix());
		racdrevIO.setSeqno(racdIO.getSeqno());
		racdrevIO.setTranno(racdIO.getTranno());
		racdrevIO.setFormat(racdrevrec);
		racdrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "SEQNO");
		SmartFileCode.execute(appVars, racdrevIO);
		if (isNE(racdrevIO.getStatuz(),varcom.oK)
		&& isNE(racdrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdrevIO.getParams());
			dbError580();
		}
		if (isNE(racdIO.getChdrcoy(),racdrevIO.getChdrcoy())
		|| isNE(racdIO.getChdrnum(),racdrevIO.getChdrnum())
		|| isNE(racdIO.getLife(),racdrevIO.getLife())
		|| isNE(racdIO.getCoverage(),racdrevIO.getCoverage())
		|| isNE(racdIO.getRider(),racdrevIO.getRider())
		|| isNE(racdIO.getPlanSuffix(),racdrevIO.getPlanSuffix())
		|| isNE(racdIO.getSeqno(),racdrevIO.getSeqno())
		|| isEQ(racdrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
		racdrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, racdrevIO);
		if (isNE(racdrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrevIO.getParams());
			dbError580();
		}
		racdrevIO.setValidflag("1");
		racdrevIO.setCurrto(varcom.vrcmMaxDate);
		racdrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, racdrevIO);
		if (isNE(racdrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrevIO.getParams());
			dbError580();
		}
	}

protected void processLrrh2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					lrrh2010();
				}
				case nextr2080: {
					nextr2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lrrh2010()
	{
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(),varcom.oK)
		&& isNE(lrrhconIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		if (isNE(lrrhconIO.getCompany(),greversrec.chdrcoy)
		|| isNE(lrrhconIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(lrrhconIO.getLife(),greversrec.life)
		|| isNE(lrrhconIO.getCoverage(),greversrec.coverage)
		|| isNE(lrrhconIO.getRider(),greversrec.rider)
		|| isNE(lrrhconIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(lrrhconIO.getStatuz(),varcom.endp)) {
			lrrhconIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(lrrhconIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr2080);
		}
		if (isNE(lrrhconIO.getValidflag(),"1")) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		deleteUpdateLrrh2100();
	}

protected void nextr2080()
	{
		lrrhconIO.setFunction(varcom.nextr);
	}

protected void deleteUpdateLrrh2100()
	{
		try {
			lrrh2110();
		}
		catch (GOTOException e){
		}
	}

protected void lrrh2110()
	{
		lrrhrevIO.setParams(SPACES);
		lrrhrevIO.setRrn(lrrhconIO.getRrn());
		lrrhrevIO.setFormat(lrrhrevrec);
		lrrhrevIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, lrrhrevIO);
		if (isNE(lrrhrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lrrhrevIO.getParams());
			dbError580();
		}
		lrrhrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lrrhrevIO);
		if (isNE(lrrhrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lrrhrevIO.getParams());
			dbError580();
		}
		lrrhrevIO.setParams(SPACES);
		lrrhrevIO.setCompany(lrrhconIO.getCompany());
		lrrhrevIO.setChdrnum(lrrhconIO.getChdrnum());
		lrrhrevIO.setLife(lrrhconIO.getLife());
		lrrhrevIO.setCoverage(lrrhconIO.getCoverage());
		lrrhrevIO.setRider(lrrhconIO.getRider());
		lrrhrevIO.setPlanSuffix(lrrhconIO.getPlanSuffix());
		lrrhrevIO.setClntpfx(lrrhconIO.getClntpfx());
		lrrhrevIO.setClntcoy(lrrhconIO.getClntcoy());
		lrrhrevIO.setClntnum(lrrhconIO.getClntnum());
		lrrhrevIO.setLrkcls(lrrhconIO.getLrkcls());
		lrrhrevIO.setTranno(lrrhconIO.getTranno());
		lrrhrevIO.setFormat(lrrhrevrec);
		lrrhrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhrevIO.setFitKeysSearch("CLNTPFX", "CLNTCOY", "CLNTNUM", "COMPANY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "LRKCLS");
		SmartFileCode.execute(appVars, lrrhrevIO);
		if (isNE(lrrhrevIO.getStatuz(),varcom.oK)
		&& isNE(lrrhrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lrrhrevIO.getParams());
			dbError580();
		}
		if (isNE(lrrhconIO.getCompany(),lrrhrevIO.getCompany())
		|| isNE(lrrhconIO.getChdrnum(),lrrhrevIO.getChdrnum())
		|| isNE(lrrhconIO.getLife(),lrrhrevIO.getLife())
		|| isNE(lrrhconIO.getCoverage(),lrrhrevIO.getCoverage())
		|| isNE(lrrhconIO.getRider(),lrrhrevIO.getRider())
		|| isNE(lrrhconIO.getPlanSuffix(),lrrhrevIO.getPlanSuffix())
		|| isNE(lrrhconIO.getClntpfx(),lrrhrevIO.getClntpfx())
		|| isNE(lrrhconIO.getClntcoy(),lrrhrevIO.getClntcoy())
		|| isNE(lrrhconIO.getClntnum(),lrrhrevIO.getClntnum())
		|| isNE(lrrhconIO.getLrkcls(),lrrhrevIO.getLrkcls())
		|| isEQ(lrrhrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		lrrhrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lrrhrevIO);
		if (isNE(lrrhrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lrrhrevIO.getParams());
			dbError580();
		}
		lrrhrevIO.setValidflag("1");
		lrrhrevIO.setCurrto(varcom.vrcmMaxDate);
		lrrhrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lrrhrevIO);
		if (isNE(lrrhrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lrrhrevIO.getParams());
			dbError580();
		}
	}

protected void processLirr3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					lirr3010();
				}
				case nextr3080: {
					nextr3080();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lirr3010()
	{
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(),varcom.oK)
		&& isNE(lirrconIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
		if (isNE(lirrconIO.getCompany(),greversrec.chdrcoy)
		|| isNE(lirrconIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(lirrconIO.getLife(),greversrec.life)
		|| isNE(lirrconIO.getCoverage(),greversrec.coverage)
		|| isNE(lirrconIO.getRider(),greversrec.rider)
		|| isNE(lirrconIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(lirrconIO.getStatuz(),varcom.endp)) {
			lirrconIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
		if (isNE(lirrconIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr3080);
		}
		if (isNE(lirrconIO.getValidflag(),"1")) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
		deleteUpdateLirr3100();
	}

protected void nextr3080()
	{
		lirrconIO.setFunction(varcom.nextr);
	}

protected void deleteUpdateLirr3100()
	{
		try {
			lirr3110();
		}
		catch (GOTOException e){
		}
	}

protected void lirr3110()
	{
		lirrrevIO.setParams(SPACES);
		lirrrevIO.setRrn(lirrconIO.getRrn());
		lirrrevIO.setFormat(lirrrevrec);
		lirrrevIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, lirrrevIO);
		if (isNE(lirrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lirrrevIO.getParams());
			dbError580();
		}
		lirrrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lirrrevIO);
		if (isNE(lirrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lirrrevIO.getParams());
			dbError580();
		}
		lirrrevIO.setParams(SPACES);
		lirrrevIO.setCompany(lirrconIO.getCompany());
		lirrrevIO.setChdrnum(lirrconIO.getChdrnum());
		lirrrevIO.setLife(lirrconIO.getLife());
		lirrrevIO.setCoverage(lirrconIO.getCoverage());
		lirrrevIO.setRider(lirrconIO.getRider());
		lirrrevIO.setPlanSuffix(lirrconIO.getPlanSuffix());
		lirrrevIO.setRasnum(lirrconIO.getRasnum());
		lirrrevIO.setRngmnt(lirrconIO.getRngmnt());
		lirrrevIO.setClntpfx(lirrconIO.getClntpfx());
		lirrrevIO.setClntcoy(lirrconIO.getClntcoy());
		lirrrevIO.setClntnum(lirrconIO.getClntnum());
		lirrrevIO.setTranno(lirrconIO.getTranno());
		lirrrevIO.setFormat(lirrrevrec);
		lirrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhrevIO.setFitKeysSearch("COMPANY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "RASNUM", "RNGMNT", "CLNTPFX", "CLNTCOY", "CLNTNUM");
		SmartFileCode.execute(appVars, lirrrevIO);
		if (isNE(lirrrevIO.getStatuz(),varcom.oK)
		&& isNE(lirrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lirrrevIO.getParams());
			dbError580();
		}
		if (isNE(lirrconIO.getCompany(),lirrrevIO.getCompany())
		|| isNE(lirrconIO.getChdrnum(),lirrrevIO.getChdrnum())
		|| isNE(lirrconIO.getLife(),lirrrevIO.getLife())
		|| isNE(lirrconIO.getCoverage(),lirrrevIO.getCoverage())
		|| isNE(lirrconIO.getRider(),lirrrevIO.getRider())
		|| isNE(lirrconIO.getPlanSuffix(),lirrrevIO.getPlanSuffix())
		|| isNE(lirrconIO.getRasnum(),lirrrevIO.getRasnum())
		|| isNE(lirrconIO.getRngmnt(),lirrrevIO.getRngmnt())
		|| isNE(lirrconIO.getClntpfx(),lirrrevIO.getClntpfx())
		|| isNE(lirrconIO.getClntcoy(),lirrrevIO.getClntcoy())
		|| isNE(lirrconIO.getClntnum(),lirrrevIO.getClntnum())
		|| isEQ(lirrrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
		lirrrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lirrrevIO);
		if (isNE(lirrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lirrrevIO.getParams());
			dbError580();
		}
		lirrrevIO.setValidflag("1");
		lirrrevIO.setCurrto(varcom.vrcmMaxDate);
		lirrrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lirrrevIO);
		if (isNE(lirrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lirrrevIO.getParams());
			dbError580();
		}
	}

protected void processReco4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					reco4010();
				}
				case nextr4080: {
					nextr4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void reco4010()
	{
		SmartFileCode.execute(appVars, recoIO);
		if (isNE(recoIO.getStatuz(),varcom.oK)
		&& isNE(recoIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(recoIO.getParams());
			dbError580();
		}
		if (isNE(recoIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(recoIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(recoIO.getLife(),greversrec.life)
		|| isNE(recoIO.getCoverage(),greversrec.coverage)
		|| isNE(recoIO.getRider(),greversrec.rider)
		|| isNE(recoIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(recoIO.getStatuz(),varcom.endp)) {
			recoIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4090);
		}
		if (isNE(recoIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr4080);
		}
		deleteReco4100();
	}

protected void nextr4080()
	{
		recoIO.setFunction(varcom.nextr);
	}

protected void deleteReco4100()
	{
		reco4110();
	}

protected void reco4110()
	{
		recorcoIO.setParams(SPACES);
		recorcoIO.setRrn(recoIO.getRrn());
		recorcoIO.setFormat(recorcorec);
		recorcoIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, recorcoIO);
		if (isNE(recorcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(recorcoIO.getParams());
			dbError580();
		}
		recorcoIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, recorcoIO);
		if (isNE(recorcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(recorcoIO.getParams());
			dbError580();
		}
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
