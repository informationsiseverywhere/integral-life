package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5437screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21, 10}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 20, 4, 65}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5437ScreenVars sv = (S5437ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5437screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5437screensfl, 
			sv.S5437screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5437ScreenVars sv = (S5437ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5437screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5437ScreenVars sv = (S5437ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5437screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5437screensflWritten.gt(0))
		{
			sv.s5437screensfl.setCurrentIndex(0);
			sv.S5437screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5437ScreenVars sv = (S5437ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5437screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5437ScreenVars screenVars = (S5437ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hrrn.setFieldName("hrrn");
				screenVars.cmdateDisp.setFieldName("cmdateDisp");
				screenVars.rasnum.setFieldName("rasnum");
				screenVars.rngmnt.setFieldName("rngmnt");
				screenVars.raAmount.setFieldName("raAmount");
				screenVars.ovrdind.setFieldName("ovrdind");
				screenVars.select.setFieldName("select");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hrrn.set(dm.getField("hrrn"));
			screenVars.cmdateDisp.set(dm.getField("cmdateDisp"));
			screenVars.rasnum.set(dm.getField("rasnum"));
			screenVars.rngmnt.set(dm.getField("rngmnt"));
			screenVars.raAmount.set(dm.getField("raAmount"));
			screenVars.ovrdind.set(dm.getField("ovrdind"));
			screenVars.select.set(dm.getField("select"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5437ScreenVars screenVars = (S5437ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hrrn.setFieldName("hrrn");
				screenVars.cmdateDisp.setFieldName("cmdateDisp");
				screenVars.rasnum.setFieldName("rasnum");
				screenVars.rngmnt.setFieldName("rngmnt");
				screenVars.raAmount.setFieldName("raAmount");
				screenVars.ovrdind.setFieldName("ovrdind");
				screenVars.select.setFieldName("select");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hrrn").set(screenVars.hrrn);
			dm.getField("cmdateDisp").set(screenVars.cmdateDisp);
			dm.getField("rasnum").set(screenVars.rasnum);
			dm.getField("rngmnt").set(screenVars.rngmnt);
			dm.getField("raAmount").set(screenVars.raAmount);
			dm.getField("ovrdind").set(screenVars.ovrdind);
			dm.getField("select").set(screenVars.select);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5437screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5437ScreenVars screenVars = (S5437ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hrrn.clearFormatting();
		screenVars.cmdateDisp.clearFormatting();
		screenVars.rasnum.clearFormatting();
		screenVars.rngmnt.clearFormatting();
		screenVars.raAmount.clearFormatting();
		screenVars.ovrdind.clearFormatting();
		screenVars.select.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5437ScreenVars screenVars = (S5437ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hrrn.setClassString("");
		screenVars.cmdateDisp.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.rngmnt.setClassString("");
		screenVars.raAmount.setClassString("");
		screenVars.ovrdind.setClassString("");
		screenVars.select.setClassString("");
	}

/**
 * Clear all the variables in S5437screensfl
 */
	public static void clear(VarModel pv) {
		S5437ScreenVars screenVars = (S5437ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hrrn.clear();
		screenVars.cmdateDisp.clear();
		screenVars.cmdate.clear();
		screenVars.rasnum.clear();
		screenVars.rngmnt.clear();
		screenVars.raAmount.clear();
		screenVars.ovrdind.clear();
		screenVars.select.clear();
	}
}
