/*
 * File: P2610edlr.java
 * Date: 29 August 2009 23:38:56
 * Author: Quipoz Limited
 * 
 * Class transformed from P2610EDLR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Cktrdsc;
import com.csc.fsu.general.procedures.Cktrtyp;
import com.csc.fsu.general.procedures.Glkey;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Cktrdscrec;
import com.csc.fsu.general.recordstructures.Cktrtyprec;
import com.csc.fsu.general.recordstructures.Glkeyrec;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*        CASH RECEIPTSEDITING - TRANSCODE LR - LIFE REASSURER
*
* THIS ROUTINE IS CALLED FROM P2610 - CASH CREATE
* TO VALIDATE THE TRANSACTION KEY FOR WHICH THE
* CASH APPLIES.
*****************************************************
* </pre>
*/
public class P2610edlr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("P2610EDLR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(17).isAPartOf(wsaaTrankey, 3);
		/* ERRORS */
	private static final String e049 = "E049";
	private static final String e126 = "E126";
	private static final String e192 = "E192";
	private static final String e193 = "E193";
	private static final String e194 = "E194";
	private static final String e196 = "E196";
	private static final String e199 = "E199";
	private static final String e289 = "E289";
	private static final String e946 = "E946";
		/* FORMATS */
	private static final String rasarec = "RASAREC";
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Cktrtyprec cktrtyprec = new Cktrtyprec();
	private Cktrdscrec cktrdscrec = new Cktrdscrec();
	private Glkeyrec glkeyrec = new Glkeyrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Cashedrec cashedrec = new Cashedrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkKey1060, 
		endGlkey1070, 
		exit1090
	}

	public P2610edlr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		cashedrec.cashedRec = convertAndSetParam(cashedrec.cashedRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		para011();
		exit090();
	}

protected void para011()
	{
		cashedrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaProg);
		if (isNE(cashedrec.function, "CHECK")) {
			cashedrec.statuz.set(e049);
			return ;
		}
		checkDissection1000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void syserr570()
	{
		para571();
		exit579();
	}

protected void para571()
	{
		if (isEQ(wsaaStatuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(wsaaStatuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		cashedrec.statuz.set(varcom.bomb);
	}

protected void exit579()
	{
		exit090();
	}

protected void dbError580()
	{
		para581();
		exit589();
	}

protected void para581()
	{
		if (isEQ(wsaaStatuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(wsaaStatuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		cashedrec.statuz.set(varcom.bomb);
	}

protected void exit589()
	{
		exit090();
	}

protected void checkDissection1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
					checkTrandesc1050();
				case checkKey1060: 
					checkKey1060();
				case endGlkey1070: 
					endGlkey1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		wsaaTrankey.set(cashedrec.trankey);
		cktrtyprec.function.set("RETN ");
		cktrtyprec.trancode.set(cashedrec.trancode);
		cktrtyprec.sacstyp.set(cashedrec.sacstyp);
		cktrtyprec.sacscode.set(cashedrec.sacscode);
		cktrtyprec.company.set(cashedrec.company);
		callProgram(Cktrtyp.class, cktrtyprec.cktrtypRec);
		if (isEQ(cktrtyprec.statuz, varcom.bomb)) {
			cashedrec.statuz.set(cktrtyprec.statuz);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(cktrtyprec.statuz, varcom.oK)) {
			cashedrec.sacstypErr.set(cktrtyprec.statuz);
		}
		cashedrec.contot.set(cktrtyprec.contot);
		/* IF CSHD-TRANAMT             = 0                              */
		/*     MOVE E199               TO CSHD-TRANAMT-ERR.             */
		if (isEQ(cashedrec.tranamt, ZERO)
		&& isEQ(cashedrec.acctamt, ZERO)) {
			cashedrec.tranamtErr.set(e199);
		}
	}

protected void checkTrandesc1050()
	{
		cktrdscrec.function.set("CHKCS");
		cktrdscrec.trantyp.set(cashedrec.sacstyp);
		cktrdscrec.trancode.set(cashedrec.sacscode);
		cktrdscrec.trandesc.set(cashedrec.trandesc);
		cktrdscrec.company.set(cashedrec.company);
		cktrdscrec.language.set(cashedrec.language);
		cktrdscrec.clntkey.set(cashedrec.clntkey);
		cktrdscrec.doctyp.set(fsupfxcpy.cash);
		callProgram(Cktrdsc.class, cktrdscrec.cktrdscRec);
		if (isEQ(cktrdscrec.statuz, varcom.nogo)) {
			goTo(GotoLabel.checkKey1060);
		}
		/* IF CKDS-STATUZ              NOT = O-K                        */
		if (isNE(cktrdscrec.statuz, varcom.oK)
		&& isNE(cktrdscrec.statuz, varcom.mrnf)) {
			wsaaStatuz.set(cktrdscrec.statuz);
			syserr570();
		}
		cashedrec.trandesc.set(cktrdscrec.trandesc);
	}

protected void checkKey1060()
	{
		if (isEQ(wsaaTranEntity, SPACES)) {
			cashedrec.trankeyErr.set(e194);
			goTo(GotoLabel.exit1090);
		}
		rasaIO.setRascoy(cashedrec.company);
		wsaaTranCompany.set(cashedrec.company);
		rasaIO.setRasnum(wsaaTranEntity);
		cashedrec.trankey.set(wsaaTrankey);
		/*                    RETURN TRANKEY TO CALLING PROGRAM*/
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(), varcom.oK)
		&& isNE(rasaIO.getStatuz(), varcom.mrnf)) {
			wsaaStatuz.set(rasaIO.getStatuz());
			syserrrec.params.set(rasaIO.getParams());
			dbError580();
		}
		if (isEQ(rasaIO.getStatuz(), varcom.mrnf)) {
			cashedrec.trankeyErr.set(e946);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(rasaIO.getReasst(), "A")) {
			cashedrec.trankeyErr.set(e126);
			goTo(GotoLabel.exit1090);
		}
		glkeyrec.function.set("RETN");
		glkeyrec.company.set(cashedrec.company);
		glkeyrec.sacscode.set(cashedrec.sacscode);
		glkeyrec.sacstyp.set(cashedrec.sacstyp);
		glkeyrec.trancode.set(cashedrec.trancode);
		glkeyrec.ledgkey.set(wsaaTrankey);
		callGlkey2000();
		if (isEQ(glkeyrec.statuz, varcom.oK)) {
			goTo(GotoLabel.endGlkey1070);
		}
		if (isEQ(glkeyrec.statuz, "NFND")) {
			cashedrec.sacstypErr.set(e196);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(glkeyrec.statuz, "MRNF")) {
			cashedrec.sacscodeErr.set(e192);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(glkeyrec.statuz, "NCUR")) {
			cashedrec.sacscodeErr.set(e193);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(glkeyrec.statuz, "BCTL")) {
			cashedrec.sacstypErr.set(e289);
			goTo(GotoLabel.exit1090);
		}
	}

protected void endGlkey1070()
	{
		cashedrec.genlPrefix.set(glkeyrec.genlPrefix);
		cashedrec.genlCompany.set(glkeyrec.genlCompany);
		cashedrec.genlCurrency.set(cashedrec.disscurr);
		cashedrec.genlAccount.set(glkeyrec.genlAccount);
		cashedrec.sign.set(glkeyrec.sign);
	}

protected void callGlkey2000()
	{
		/*PARA*/
		callProgram(Glkey.class, glkeyrec.glkeyRec);
		if (isEQ(glkeyrec.statuz, varcom.bomb)) {
			wsaaStatuz.set(glkeyrec.statuz);
			dbError580();
		}
		/*EXIT*/
	}
}
