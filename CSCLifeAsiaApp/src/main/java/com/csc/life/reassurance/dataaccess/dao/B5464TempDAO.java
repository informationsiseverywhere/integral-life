package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;

import com.csc.life.reassurance.dataaccess.model.B5464DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface B5464TempDAO extends BaseDAO<B5464DTO> {
	public void buildTempData(String wsaaChdrnumfrm, String wsaaChdrnumto,
			int wsaaSqlEffdate);

	List<B5464DTO> findTempDataResult(int minRecord, int maxRecord);
}