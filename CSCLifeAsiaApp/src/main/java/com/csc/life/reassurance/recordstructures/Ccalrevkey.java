package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:10
 * Description:
 * Copybook name: CCALREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ccalrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ccalrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ccalrevKey = new FixedLengthStringData(64).isAPartOf(ccalrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData ccalrevChdrcoy = new FixedLengthStringData(1).isAPartOf(ccalrevKey, 0);
  	public FixedLengthStringData ccalrevChdrnum = new FixedLengthStringData(8).isAPartOf(ccalrevKey, 1);
  	public FixedLengthStringData ccalrevLife = new FixedLengthStringData(2).isAPartOf(ccalrevKey, 9);
  	public FixedLengthStringData ccalrevCoverage = new FixedLengthStringData(2).isAPartOf(ccalrevKey, 11);
  	public FixedLengthStringData ccalrevRider = new FixedLengthStringData(2).isAPartOf(ccalrevKey, 13);
  	public PackedDecimalData ccalrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ccalrevKey, 15);
  	public PackedDecimalData ccalrevSeqno = new PackedDecimalData(2, 0).isAPartOf(ccalrevKey, 18);
  	public PackedDecimalData ccalrevTranno = new PackedDecimalData(5, 0).isAPartOf(ccalrevKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(ccalrevKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ccalrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ccalrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}