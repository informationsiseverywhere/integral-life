package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RecopfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:16
 * Class transformed from RECOPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RecopfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 202;
	public FixedLengthStringData recorec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData recopfRecord = recorec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(recorec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(recorec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(recorec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(recorec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(recorec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(recorec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(recorec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(recorec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(recorec);
	public PackedDecimalData costdate = DD.costdate.copy().isAPartOf(recorec);
	public FixedLengthStringData retype = DD.retype.copy().isAPartOf(recorec);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(recorec);
	public PackedDecimalData sraramt = DD.sraramt.copy().isAPartOf(recorec);
	public PackedDecimalData raAmount = DD.raamount.copy().isAPartOf(recorec);
	public PackedDecimalData ctdate = DD.ctdate.copy().isAPartOf(recorec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(recorec);
	public PackedDecimalData prem = DD.prem.copy().isAPartOf(recorec);
	public PackedDecimalData compay = DD.compay.copy().isAPartOf(recorec);
	public PackedDecimalData taxamt = DD.taxamt.copy().isAPartOf(recorec);
	public PackedDecimalData refundfe = DD.refundfe.copy().isAPartOf(recorec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(recorec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(recorec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(recorec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(recorec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(recorec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(recorec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(recorec);
	public FixedLengthStringData rcstfrq = DD.rcstfrq.copy().isAPartOf(recorec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(recorec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(recorec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(recorec);
	public PackedDecimalData RPRate = DD.RPRate.copy().isAPartOf(recorec);
	public PackedDecimalData annprem = DD.annprem.copy().isAPartOf(recorec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RecopfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RecopfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RecopfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RecopfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RecopfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RecopfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RecopfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RECOPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"RASNUM, " +
							"SEQNO, " +
							"VALIDFLAG, " +
							"COSTDATE, " +
							"RETYPE, " +
							"RNGMNT, " +
							"SRARAMT, " +
							"RAAMOUNT, " +
							"CTDATE, " +
							"ORIGCURR, " +
							"PREM, " +
							"COMPAY, " +
							"TAXAMT, " +
							"REFUNDFE, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"TRANNO, " +
							"RCSTFRQ, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"RPRATE," +
							"ANNPREM" +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     rasnum,
                                     seqno,
                                     validflag,
                                     costdate,
                                     retype,
                                     rngmnt,
                                     sraramt,
                                     raAmount,
                                     ctdate,
                                     origcurr,
                                     prem,
                                     compay,
                                     taxamt,
                                     refundfe,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     tranno,
                                     rcstfrq,
                                     userProfile,
                                     jobName,
                                     datime,
                                     RPRate,
									 annprem,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		rasnum.clear();
  		seqno.clear();
  		validflag.clear();
  		costdate.clear();
  		retype.clear();
  		rngmnt.clear();
  		sraramt.clear();
  		raAmount.clear();
  		ctdate.clear();
  		origcurr.clear();
  		prem.clear();
  		compay.clear();
  		taxamt.clear();
  		refundfe.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		tranno.clear();
  		rcstfrq.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		RPRate.clear();
		annprem.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRecorec() {
  		return recorec;
	}

	public FixedLengthStringData getRecopfRecord() {
  		return recopfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRecorec(what);
	}

	public void setRecorec(Object what) {
  		this.recorec.set(what);
	}

	public void setRecopfRecord(Object what) {
  		this.recopfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(recorec.getLength());
		result.set(recorec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}