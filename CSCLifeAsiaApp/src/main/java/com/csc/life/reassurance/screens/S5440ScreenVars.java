package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5440
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5440ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(462);
	public FixedLengthStringData dataFields = new FixedLengthStringData(238).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData comts = new FixedLengthStringData(60).isAPartOf(dataFields, 24);
	public FixedLengthStringData[] comt = FLSArrayPartOfStructure(2, 30, comts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(comts, 0, FILLER_REDEFINE);
	public FixedLengthStringData comt01 = DD.comt.copy().isAPartOf(filler,0);
	public FixedLengthStringData comt02 = DD.comt.copy().isAPartOf(filler,30);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,115);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,162);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,225);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 238);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData comtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] comtErr = FLSArrayPartOfStructure(2, 4, comtsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(comtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData comt01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData comt02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 294);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData comtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] comtOut = FLSArrayPartOfStructure(2, 12, comtsOut, 0);
	public FixedLengthStringData[][] comtO = FLSDArrayPartOfArrayStructure(12, 1, comtOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(comtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] comt01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] comt02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(240);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(78).isAPartOf(subfileArea, 0);
	public FixedLengthStringData cmpntnum = DD.cmpntnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData deit = DD.deit.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData hcoverage = DD.hcoverage.copy().isAPartOf(subfileFields,47);
	public FixedLengthStringData hlifeno = DD.hlifeno.copy().isAPartOf(subfileFields,49);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,51);
	public ZonedDecimalData hsuffix = DD.hsuffix.copyToZonedDecimal().isAPartOf(subfileFields,53);
	public FixedLengthStringData pstatcode = DD.premdesc.copy().isAPartOf(subfileFields,57);
	public FixedLengthStringData statcode = DD.riskdesc.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,77);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 78);
	public FixedLengthStringData cmpntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData deitErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcoverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hlifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hsuffixErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData premdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData riskdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 118);
	public FixedLengthStringData[] cmpntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] deitOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcoverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hlifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hsuffixOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] premdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] riskdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 238);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5440screensflWritten = new LongData(0);
	public LongData S5440screenctlWritten = new LongData(0);
	public LongData S5440screenWritten = new LongData(0);
	public LongData S5440protectWritten = new LongData(0);
	public GeneralTable s5440screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5440screensfl;
	}

	public S5440ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hflagOut,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hlifeno, hsuffix, hcoverage, hrider, statcode, pstatcode, select, component, cmpntnum, deit};
		screenSflOutFields = new BaseData[][] {hlifenoOut, hsuffixOut, hcoverageOut, hriderOut, riskdescOut, premdescOut, selectOut, componentOut, cmpntnumOut, deitOut};
		screenSflErrFields = new BaseData[] {hlifenoErr, hsuffixErr, hcoverageErr, hriderErr, riskdescErr, premdescErr, selectErr, componentErr, cmpntnumErr, deitErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, comt01, comt02, hflag};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, comt01Out, comt02Out, hflagOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, comt01Err, comt02Err, hflagErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5440screen.class;
		screenSflRecord = S5440screensfl.class;
		screenCtlRecord = S5440screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5440protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5440screenctl.lrec.pageSubfile);
	}
}
