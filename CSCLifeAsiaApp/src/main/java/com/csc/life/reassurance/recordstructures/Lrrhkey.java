package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:28
 * Description:
 * Copybook name: LRRHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lrrhkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lrrhFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lrrhKey = new FixedLengthStringData(64).isAPartOf(lrrhFileKey, 0, REDEFINE);
  	public FixedLengthStringData lrrhClntpfx = new FixedLengthStringData(2).isAPartOf(lrrhKey, 0);
  	public FixedLengthStringData lrrhClntcoy = new FixedLengthStringData(1).isAPartOf(lrrhKey, 2);
  	public FixedLengthStringData lrrhClntnum = new FixedLengthStringData(8).isAPartOf(lrrhKey, 3);
  	public FixedLengthStringData lrrhCompany = new FixedLengthStringData(1).isAPartOf(lrrhKey, 11);
  	public FixedLengthStringData lrrhLrkcls = new FixedLengthStringData(4).isAPartOf(lrrhKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(lrrhKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lrrhFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lrrhFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}