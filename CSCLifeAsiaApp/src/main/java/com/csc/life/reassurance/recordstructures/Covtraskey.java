package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:30
 * Description:
 * Copybook name: COVTRASKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtraskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtrasFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtrasKey = new FixedLengthStringData(64).isAPartOf(covtrasFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtrasChdrcoy = new FixedLengthStringData(1).isAPartOf(covtrasKey, 0);
  	public FixedLengthStringData covtrasChdrnum = new FixedLengthStringData(8).isAPartOf(covtrasKey, 1);
  	public FixedLengthStringData covtrasLife = new FixedLengthStringData(2).isAPartOf(covtrasKey, 9);
  	public FixedLengthStringData covtrasCoverage = new FixedLengthStringData(2).isAPartOf(covtrasKey, 11);
  	public FixedLengthStringData covtrasRider = new FixedLengthStringData(2).isAPartOf(covtrasKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(covtrasKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtrasFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtrasFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}