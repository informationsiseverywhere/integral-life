package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5469.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5469Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntsname = new FixedLengthStringData(30);
	private FixedLengthStringData cltaddr01 = new FixedLengthStringData(DD.cltaddr.length);//pmujavadiya//ILIFE-3222
	private FixedLengthStringData cltaddr02 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData cltaddr03 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10);
	private FixedLengthStringData crrcd = new FixedLengthStringData(10);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10);
	private FixedLengthStringData dateto = new FixedLengthStringData(10);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private FixedLengthStringData lrkcls = new FixedLengthStringData(4);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0);
	private FixedLengthStringData shortnam = new FixedLengthStringData(28);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData totamnt01 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totamnt02 = new ZonedDecimalData(18, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5469Report() {
		super();
	}


	/**
	 * Print the XML for R5469d01
	 */
	public void printR5469d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 9, 8));
		clntsname.setFieldName("clntsname");
		clntsname.setInternal(subString(recordData, 17, 30));
		crrcd.setFieldName("crrcd");
		crrcd.setInternal(subString(recordData, 47, 10));
		rcestrm.setFieldName("rcestrm");
		rcestrm.setInternal(subString(recordData, 57, 3));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 60, 17));
		lrkcls.setFieldName("lrkcls");
		lrkcls.setInternal(subString(recordData, 77, 4));
		cmdate.setFieldName("cmdate");
		cmdate.setInternal(subString(recordData, 81, 10));
		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 91, 17));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 108, 30));
		printLayout("R5469d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				clntnum,
				clntsname,
				crrcd,
				rcestrm,
				sumins,
				lrkcls,
				cmdate,
				raamount,
				descrip
			}
		);

	}

	/**
	 * Print the XML for R5469h01
	 */
	public void printR5469h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		shortnam.setFieldName("shortnam");
		shortnam.setInternal(subString(recordData, 1, 28));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 29, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 37, 37));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 74, 30));
		cltaddr01.setFieldName("cltaddr01");
		cltaddr01.setInternal(subString(recordData, 104, DD.cltaddr.length));//pmujavadiya//ILIFE-3222
		cltaddr02.setFieldName("cltaddr02");
		cltaddr02.setInternal(subString(recordData, 104+DD.cltaddr.length, DD.cltaddr.length));
		cltaddr03.setFieldName("cltaddr03");
		cltaddr03.setInternal(subString(recordData, 104+DD.cltaddr.length*2, DD.cltaddr.length));
		datefrm.setFieldName("datefrm");
		datefrm.setInternal(subString(recordData, 104+DD.cltaddr.length*3,10));
		dateto.setFieldName("dateto");
		dateto.setInternal(subString(recordData, 114+DD.cltaddr.length*3, 10));
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 124+DD.cltaddr.length*3, 3));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 127+DD.cltaddr.length*3, 30));
		printLayout("R5469h01",			// Record name
			new BaseData[]{			// Fields:
				shortnam,
				pagnbr,
				rasnum,
				clntnm,
				descrip,
				cltaddr01,
				cltaddr02,
				cltaddr03,
				datefrm,
				dateto,
				currcode,
				longdesc
			}
		);

		currentPrintLine.set(12);
	}

	/**
	 * Print the XML for R5469h02
	 */
	public void printR5469h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 1, 8));
		printLayout("R5469h02",			// Record name
			new BaseData[]{			// Fields:
				clntnum
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5469t01
	 */
	public void printR5469t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 1, 3));
		totamnt01.setFieldName("totamnt01");
		totamnt01.setInternal(subString(recordData, 4, 18));
		totamnt02.setFieldName("totamnt02");
		totamnt02.setInternal(subString(recordData, 22, 18));
		printLayout("R5469t01",			// Record name
			new BaseData[]{			// Fields:
				currcode,
				totamnt01,
				totamnt02
			}
		);

		currentPrintLine.add(2);
	}


}
