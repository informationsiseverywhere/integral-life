package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5442
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5442ScreenVars extends SmartVarModel { 
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(108);
	public FixedLengthStringData dataFields = new FixedLengthStringData(44).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,43);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 44);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 60);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(275);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(96).isAPartOf(subfileArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData elemdesc = DD.elemdesc.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData hrrns = new FixedLengthStringData(27).isAPartOf(subfileFields, 54);
	public ZonedDecimalData[] hrrn = ZDArrayPartOfStructure(3, 9, 0, hrrns, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(27).isAPartOf(hrrns, 0, FILLER_REDEFINE);
	public ZonedDecimalData hrrn01 = DD.hrrn.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData hrrn02 = DD.hrrn.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData hrrn03 = DD.hrrn.copyToZonedDecimal().isAPartOf(filler,18);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,81);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,83);
	public FixedLengthStringData shortdesc = DD.shrtdesc.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData bascovr = DD.action.copy().isAPartOf(subfileFields,95);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 96);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData elemdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hrrnsErr = new FixedLengthStringData(12).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData[] hrrnErr = FLSArrayPartOfStructure(3, 4, hrrnsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(hrrnsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hrrn01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData hrrn02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData hrrn03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData shrtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData bascovrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 140);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] elemdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData hrrnsOut = new FixedLengthStringData(36).isAPartOf(outputSubfile, 48);
	public FixedLengthStringData[] hrrnOut = FLSArrayPartOfStructure(3, 12, hrrnsOut, 0);
	public FixedLengthStringData[][] hrrnO = FLSDArrayPartOfArrayStructure(12, 1, hrrnOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(hrrnsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hrrn01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] hrrn02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] hrrn03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] shrtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] bascovrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 272);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5442screensflWritten = new LongData(0);
	public LongData S5442screenctlWritten = new LongData(0);
	public LongData S5442screenWritten = new LongData(0);
	public LongData S5442protectWritten = new LongData(0);
	public GeneralTable s5442screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5442screensfl;
	}

	public S5442ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"02","04","-02","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(indxflgOut,new String[] {"03","26","-03","04", null, null, null, null, null, null, null, null});
		
		screenSflFields = new BaseData[] {hrrn01, hrrn02, hrrn03, hcrtable, action, life, coverage, rider, shortdesc, elemdesc, indxflg, bascovr};
		screenSflOutFields = new BaseData[][] {hrrn01Out, hrrn02Out, hrrn03Out, hcrtableOut, actionOut, lifeOut, coverageOut, riderOut, shrtdescOut, elemdescOut, indxflgOut, bascovrOut};
		screenSflErrFields = new BaseData[] {hrrn01Err, hrrn02Err, hrrn03Err, hcrtableErr, actionErr, lifeErr, coverageErr, riderErr, shrtdescErr, elemdescErr, indxflgErr, bascovrErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {cnttype, ctypedes, chdrsel};
		screenOutFields = new BaseData[][] {cnttypeOut, ctypedesOut, chdrselOut};
		screenErrFields = new BaseData[] {cnttypeErr, ctypedesErr, chdrselErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenActionVar = action;
		screenRecord = S5442screen.class;
		screenSflRecord = S5442screensfl.class;
		screenCtlRecord = S5442screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5442protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5442screenctl.lrec.pageSubfile);
	}
}
