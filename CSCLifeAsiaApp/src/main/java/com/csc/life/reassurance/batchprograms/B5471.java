/*
 * File: B5471.java
 * Date: 29 August 2009 21:20:10
 * Author: Quipoz Limited
 * 
 * Class transformed from B5471.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AcmvagtTableDAM;
import com.csc.life.agents.tablestructures.T6657rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.reassurance.dataaccess.AcmxTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.reassurance.dataaccess.dao.AcmxpfDAO;
import com.csc.life.reassurance.dataaccess.dao.impl.AcmvpfDAOImpl;
import com.csc.life.reassurance.dataaccess.model.Acmvagt;
import com.csc.life.reassurance.dataaccess.model.Acmxpf;
import com.csc.life.reassurance.dataaccess.model.B5471DTO;
import com.csc.life.reassurance.dataaccess.model.Rasapf;
import com.csc.life.reassurance.tablestructures.T5632rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Concomit;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.ItemDAOImpl;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reassurer Payments.
*
* This program will read the temporary file, B5470TMP, to access
* the ACMV records that need to be processed for payment.
*
* For each record read, update the ACMV file by setting the fully
* reconciled date (FRCDATE) to the effective date and the
* reconciled amount (RCAMT) to the original amount (ORIGAMT).
*
* For each reassurer, post a balancing ACMV record for the
* total amount and call PAYREQ to create payment records.
*
*****************************************************************
* </pre>
*/
public class B5471 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5471");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaTotAmtIn = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotAmtOut = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotCurrIn = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotCurrOut = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaOrigamt = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaStoredRasa = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaStoredRasnum = new FixedLengthStringData(8).isAPartOf(wsaaStoredRasa, 0);
	private FixedLengthStringData wsaaStoredCurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaStoredCode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredType = new FixedLengthStringData(2);
	private boolean btchpr01Permission = false; //BTCHPR01
	private FixedLengthStringData wsaaFirstRead = new FixedLengthStringData(1);
	private Validator firstRead = new Validator(wsaaFirstRead, "Y");

	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);
	private Validator recordFound = new Validator(wsaaRecordFound, "Y");

	private FixedLengthStringData wsaaDatimeInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDatimeDate = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 0);
	private FixedLengthStringData wsaaDatimeTime = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaDatimeInit, 20);
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5632 = "T5632";
	private static final String t5645 = "T5645";
	private static final String t6657 = "T6657";
		/* FORMATS */
	private static final String acmxrec = "ACMXREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String rasarec = "RASAREC";
	private static final String acmvagtrec = "ACMVAGTREC";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AcmvagtTableDAM acmvagtIO = new AcmvagtTableDAM();
	private AcmxTableDAM acmxIO = new AcmxTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Payreqrec payreqrec = new Payreqrec();
	private T5632rec t5632rec = new T5632rec();
	private T5645rec t5645rec = new T5645rec();
	private T6657rec t6657rec = new T6657rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	//ILIFE-2525 Life Batch Performance Improvement by liwei	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAOImpl.class);
	private AcmxpfDAO acmxDao = getApplicationContext().getBean("acmxDAO",AcmxpfDAO.class);
	private AcmvpfDAO acmvDAO = getApplicationContext().getBean("acmvDAO", AcmvpfDAOImpl.class);
	private Map<String, List<Itempf>> t5645ListMap = null;
	private Map<String, List<Itempf>> t5632ListMap = null;
	private Map<String, List<Itempf>> t6657ListMap = null;
	private String strEffDate;
	private String strCompany;
	private static final String h134 = "H134";
	private List<B5471DTO> b5471DtoList = null;
	private List<Acmxpf> acmxList = null;
	private Iterator<B5471DTO> iterator;
	private Iterator<Acmxpf> acmxIterator;
	private B5471DTO b5471DTO = null;
	private Acmxpf acmxpf = null;
    private int minRecord = 1;
    private int incrRange;
    private List<Acmvagt> totalAcmvpfList= new ArrayList<Acmvagt>();
    private Acmvagt acmvagtpf = null;
    private Rasapf rasapf = null;
    private Map<String, Acmxpf> acmxListMap = new IdentityHashMap<String, Acmxpf>();
/**
 * Contains all possible labels used by goTo action.
 */
	
	public B5471() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readSmartTables(strCompany);
		readT5645();
		readB5471DtoResulte();
	}
private void readSmartTables(String company){
	t5645ListMap = itemDAO.loadSmartTable("IT", company, "T5645");
	t5632ListMap = itemDAO.loadSmartTable("IT", company, "T5632");
	t6657ListMap = itemDAO.loadSmartTable("IT", company, "T6657");	
}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaDatimeInit.set(bsscIO.getDatimeInit());
		wsaaStoredRasa.set(SPACES);
		wsaaRecordFound.set("N");
		wsaaFirstRead.set("Y");
		wsaaDate.set(getCobolDate());
		wsaaTime.set(getCobolTime());
		//ILIFE-2525 Life Batch Performance Improvement by liwei
		strEffDate = bsscIO.getEffectiveDate().toString();
		strCompany = bsprIO.getCompany().toString();
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}

	}
protected void readB5471DtoResulte(){
    if (bprdIO.systemParam01.isNumeric()) {
        if (bprdIO.systemParam01.toInt() > 0) {
            incrRange = bprdIO.systemParam01.toInt();
        } else {
            incrRange = bprdIO.cyclesPerCommit.toInt();
        }
    } else {
        incrRange = bprdIO.cyclesPerCommit.toInt();
    } 
	b5471DtoList = acmxDao.searchAcmxResult(strCompany,minRecord,minRecord + incrRange);
    if (b5471DtoList != null && b5471DtoList.size() > 0) {
        iterator = b5471DtoList.iterator();
    } else {
        wsspEdterror.set(varcom.endp);
    }
}
protected void readT5645(){
	String keyItemitem = wsaaProg.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5645rec.t5645Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}

protected void readT5632(String keyItemitem){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5632ListMap.containsKey(keyItemitem)){	
		itempfList = t5632ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5632rec.t5632Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5632rec.t5632Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5632rec.t5632Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}

protected void readT6657(String keyItemitem){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6657ListMap.containsKey(keyItemitem)){	
		itempfList = t6657ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6657rec.t6657Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t6657rec.t6657Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t6657rec.t6657Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}
protected void readFile2000()
	{
					readFile2010();
					end2080();
		
	}

protected void readFile2010()
	{
	//ILIFE-2525 Life Batch Performance Improvement by liwei start
    if (iterator.hasNext()) {
    	b5471DTO = iterator.next();
    	acmxListMap.put(b5471DTO.getSqlRasnum(), b5471DTO.getAcmxpf());
    } else {
        minRecord += incrRange;
        b5471DtoList = acmxDao.searchAcmxResult(strCompany,minRecord,minRecord + incrRange);
        if (b5471DtoList != null && b5471DtoList.size() > 0) {
            iterator = b5471DtoList.iterator();
            if (iterator.hasNext()) {
            	b5471DTO = iterator.next();
            	acmxListMap.put(b5471DTO.getSqlRasnum(), b5471DTO.getAcmxpf());
    	    }
            else{
            	 wsspEdterror.set(varcom.endp);
            }
        } else {
        	 wsspEdterror.set(varcom.endp);
        }
    }
	  
	    
  //ILIFE-2525 Life Batch Performance Improvement by liwei end
//		SmartFileCode.execute(appVars, acmxIO);
//		if (isNE(acmxIO.getStatuz(),varcom.oK)
//		&& isNE(acmxIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(acmxIO.getParams());
//			syserrrec.statuz.set(acmxIO.getStatuz());
//			fatalError600();
//		}
    	
		if (isNE(b5471DTO.getAcmxpf().getBatccoy(),bsprIO.getCompany()) ||b5471DtoList.size()<=0){
			end2080();
		}
		/*  READ TMP-FILE                                                */
		/*     AT END                                                    */
		/*          GO TO 2080-END                                       */
		/*  END-READ.*/
//		acmxIO.setFunction(varcom.nextr);
		return;
	}

protected void end2080()
	{
		if (recordFound.isTrue()) {
			acmxList = new ArrayList<Acmxpf>();
			for( Map.Entry<String, Acmxpf> map :acmxListMap.entrySet()){
				if(map.getKey().trim().equalsIgnoreCase(wsaaStoredRasa.toString().trim())){
					acmxList.add(map.getValue());
				}
			}
			reReadAcmx3100();
			endOfCurr2600();
			endOfRasa2700();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (firstRead.isTrue()) {
			wsaaFirstRead.set("N");
			wsaaTotCurrIn.set(ZERO);
			wsaaTotCurrOut.set(ZERO);
			wsaaTotAmtOut.set(ZERO);
			wsaaStoredCurr.set(b5471DTO.getAcmxpf().getOrigcurr());
			wsaaStoredCode.set(b5471DTO.getAcmxpf().getSacscode());
			wsaaStoredType.set(b5471DTO.getAcmxpf().getSacstyp());
			readRasa2800();
		}
		if (isNE(b5471DTO.getAcmxpf().getRldgacct(),wsaaStoredRasa)) {
			acmxList = new ArrayList<Acmxpf>();
			for( Map.Entry<String, Acmxpf> map :acmxListMap.entrySet()){
				if(map.getKey().trim().equalsIgnoreCase(wsaaStoredRasa.toString().trim())){
					acmxList.add(map.getValue());
				}
			}
			reReadAcmx3100();
			endOfCurr2600();
			endOfRasa2700();
			readRasa2800();
		}
		else {
			if (isNE(b5471DTO.getAcmxpf().getOrigcurr(),wsaaStoredCurr)
			|| isNE(b5471DTO.getAcmxpf().getSacscode(),wsaaStoredCode)
			|| isNE(b5471DTO.getAcmxpf().getSacstyp(),wsaaStoredType)) {
				endOfCurr2600();
			}
		}
	}

protected void endOfCurr2600()
	{
					start2610();
		 
					postCurrency2650();
		 
					initialise2680();
				
	}

protected void start2610()
	{
		if (isLTE(wsaaTotAmtOut,0)) {
			initialise2680();
		}
		/* Currency Conversion will not be performed in all cases. The     */
		/* following is list of entries that should be produced :          */
		/* Reassurer's Pay Currency = Cession Currency                     */
		/* e.g.                                                            */
		/*                         DR            CR                        */
		/* GBP      LR  RA      REASPAY                                    */
		/* GBP      LR  PD                    REASPAID                     */
		/* Reassurer's Pay Currency = GBP                                  */
		/* Cession Currency         = USD                                  */
		/* This is a multi-currency case so currency conversion            */
		/* entries have to be written.                                     */
		/* e.g.                                                            */
		/*                         DR            CR                        */
		/* GBP      LR  IC      REASPAY                                    */
		/* GBP      LR  CY                    CURRCONV                     */
		/* USD      LR  CY      CURRCONV                                   */
		/* USD      LR  PD                    REASPAID                     */
		if (isNE(rasapf.getCurrcode(),wsaaStoredCurr)) {
			postCurrency2650();
		}
		/* Post the Amount to the Reassurer's Account.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rldgcoy.set(rasapf.getRascoy());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.rdocnum.set(rasapf.getRasnum());
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.rldgacct.set(rasapf.getRasnum());
		lifacmvrec.origcurr.set(wsaaStoredCurr);
		lifacmvrec.origamt.set(wsaaTotCurrIn);
		/* MOVE WSAA-TOT-CURR-OUT      TO LIFA-ACCTAMT.                 */
		lifacmvrec.acctamt.set(ZERO);
		/* MOVE RASA-CURRCODE          TO LIFA-GENLCUR.                 */
		lifacmvrec.genlcoy.set(bsprIO.getCompany());
		/* IF RASA-CURRCODE            = WSAA-STORED-CURR               */
		/*    MOVE 1                   TO LIFA-CRATE                    */
		/* ELSE                                                         */
		/*    MOVE CLNK-RATE-USED      TO LIFA-CRATE                    */
		/* END-IF.                                                      */
		lifacmvrec.crate.set(1);
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.postyear.set(bsscIO.getAcctYear());
		lifacmvrec.postmonth.set(bsscIO.getAcctMonth());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		/* MOVE ZEROES                 TO LIFA-USER.                    */
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(bprdIO.getCompany().toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("RL");
			}
		
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		initialise2680();
	}

protected void postCurrency2650()
	{
		/* If the reassurer currency is not the same as the transaction*/
		/* currency, create two currency conversion postings.*/
		/* IF RASA-CURRCODE            = WSAA-STORED-CURR               */
		/*     GO 2690-EXIT                                             */
		/* END-IF.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rldgcoy.set(rasapf.getRascoy());
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.rdocnum.set(rasapf.getRasnum());
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.rldgacct.set(rasapf.getRasnum());
		lifacmvrec.origcurr.set(wsaaStoredCurr);
		lifacmvrec.origamt.set(wsaaTotCurrIn);
		lifacmvrec.acctamt.set(ZERO);
		/*                                LIFA-CRATE.                   */
		lifacmvrec.crate.set(1);
		lifacmvrec.genlcoy.set(bsprIO.getCompany());
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.postyear.set(bsscIO.getAcctYear());
		lifacmvrec.postmonth.set(bsscIO.getAcctMonth());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		/* MOVE ZEROES                 TO LIFA-USER.                    */
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(bprdIO.getCompany().toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("RL");
			}		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		/* The second currency posting.....*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rldgcoy.set(rasapf.getRascoy());
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.rdocnum.set(rasapf.getRasnum());
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.rldgacct.set(rasapf.getRasnum());
		lifacmvrec.origcurr.set(rasapf.getCurrcode());
		lifacmvrec.origamt.set(wsaaTotCurrOut);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.genlcoy.set(bsprIO.getCompany());
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.postyear.set(bsscIO.getAcctYear());
		lifacmvrec.postmonth.set(bsscIO.getAcctMonth());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		/* MOVE ZEROES                 TO LIFA-USER.                    */
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("RL");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("RL");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void initialise2680()
	{
		wsaaStoredCurr.set(b5471DTO.getAcmxpf().getOrigcurr());
		wsaaStoredCode.set(b5471DTO.getAcmxpf().getSacscode());
		wsaaStoredType.set(b5471DTO.getAcmxpf().getSacstyp());
		wsaaTotCurrIn.set(ZERO);
		wsaaTotCurrOut.set(ZERO);
		wsaaStoredCurr.set(b5471DTO.getAcmxpf().getOrigcurr());
		wsaaStoredCode.set(b5471DTO.getAcmxpf().getSacscode());
		wsaaStoredType.set(b5471DTO.getAcmxpf().getSacstyp());
		/*EXIT*/
	}

protected void endOfRasa2700()
	{
			start2710();
		}

protected void start2710()
	{
		/* No need to create a payment records if the reassurer is not*/
		/* owed money.*/
		if (isLTE(wsaaTotAmtOut,0)) {
			return ;
		}
//ILIFE-2525 Life Batch Performance Improvement by liwei start
		readT5632(rasapf.getRapaymth());/* IJTI-1523 */
		readT6657(rasapf.getCurrcode());/* IJTI-1523 */
//ILIFE-2525 Life Batch Performance Improvement by liwei end
		payreqrec.rec.set(SPACES);
		payreqrec.effdate.set(bsscIO.getEffectiveDate());
		payreqrec.paycurr.set(rasapf.getCurrcode());
		payreqrec.pymt.set(wsaaTotAmtOut);
		payreqrec.trandesc.set(descIO.getLongdesc());
		payreqrec.bankkey.set(rasapf.getBankkey());
		payreqrec.bankacckey.set(rasapf.getBankacckey());
		payreqrec.sacscode.set(t5645rec.sacscode02);
		payreqrec.sacstype.set(t5645rec.sacstype02);
		payreqrec.glcode.set(t5645rec.glmap02);
		payreqrec.sign.set(t5645rec.sign02);
		/* MOVE RASA-PAYCLT            TO PAYREQ-FRM-RLDGACCT.          */
		payreqrec.frmRldgacct.set(rasapf.getRasnum());
		payreqrec.cnttot.set(t5645rec.cnttot02);
		payreqrec.termid.set(varcom.vrcmTermid);
		/* MOVE ZEROES                 TO PAYREQ-USER.                  */
		payreqrec.user.set(wsaaUser);
		payreqrec.reqntype.set(t5632rec.paymentMethod);
		payreqrec.clntcoy.set(rasapf.getClntcoy());
		payreqrec.clntnum.set(rasapf.getPayclt());
		payreqrec.bankcode.set(t6657rec.bankcode);
		payreqrec.batckey.set(batcdorrec.batchkey);
		payreqrec.tranref.set(bsscIO.getJobName());
		payreqrec.language.set(bsscIO.getLanguage());
		payreqrec.zbatctrcde.set(bprdIO.getAuthCode()); 
		/* As manual payments cannot be requested through reassurance*/
		/* payments nothing is passed to PAYREQ-CHEQNO.*/
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(payreqrec.rec);
			syserrrec.statuz.set(payreqrec.statuz);
			fatalError600();
		}
		wsaaTotAmtOut.set(ZERO);
	}

protected void readRasa2800()
	{
//		/*RASA*/
		wsaaStoredRasa.set(b5471DTO.getAcmxpf().getRldgacct());
//		rasaIO.setParams(SPACES);
//		rasaIO.setRascoy(bsprIO.getCompany());
//		rasaIO.setRasnum(wsaaStoredRasnum);
//		rasaIO.setFormat(rasarec);
//		rasaIO.setFunction(varcom.readr);
//		SmartFileCode.execute(appVars, rasaIO);
//		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(rasaIO.getParams());
//			fatalError600();
//		}
//		/*EXIT*/
		rasapf = new Rasapf();
		rasapf.setBankacckey(b5471DTO.getSqlBankacckey());
		rasapf.setRapaymth(b5471DTO.getSqlRapaymth());
		rasapf.setCurrcode(b5471DTO.getSqlCurrcode());
		rasapf.setBankkey(b5471DTO.getSqlBankkey());
		rasapf.setRasnum(b5471DTO.getSqlRasnum());
		rasapf.setClntcoy(b5471DTO.getSqlClntcoy());
		rasapf.setPayclt(b5471DTO.getSqlPayclt());
		rasapf.setRascoy(b5471DTO.getSqlRascoy());
	}

protected void update3000()
	{
					update3010();
					callAcmv3060();
				
	}

protected void update3010()
	{
		if (isEQ(b5471DTO.getAcmxpf().getGlsign(),"+")) {
			compute(wsaaOrigamt, 3).setRounded(mult(b5471DTO.getAcmxpf().getOrigamt(),-1));
		}
		else {
			wsaaOrigamt.set(b5471DTO.getAcmxpf().getOrigamt());
		}
		wsaaRecordFound.set("Y");
		if (isEQ(b5471DTO.getAcmxpf().getOrigcurr(),rasapf.getCurrcode())) {
			/*      ADD ACMX-ORIGAMT        TO WSAA-TOT-AMT-IN       <LA3381>*/
			wsaaTotAmtOut.add(wsaaOrigamt);
			wsaaTotCurrIn.add(wsaaOrigamt);
			wsaaTotCurrOut.add(wsaaOrigamt);
			/*     GO TO 3050-ACMV                                          */
			return;
		}
		/* Set up parameters for Currency conversion*/
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.currIn.set(b5471DTO.getAcmxpf().getOrigcurr());
		conlinkrec.currOut.set(rasapf.getCurrcode());
		/*  MOVE ACMX-ORIGAMT           TO CLNK-AMOUNT-IN.       <LA3381>*/
		conlinkrec.amountIn.set(wsaaOrigamt);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			/*     MOVE CLNK-CLNK002-REC   TO SYSR-STATUZ                   */
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO CLNK-AMOUNT-OUT.             */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		wsaaTotCurrIn.add(conlinkrec.amountIn);
		wsaaTotAmtOut.add(conlinkrec.amountOut);
		wsaaTotCurrOut.add(conlinkrec.amountOut);
		return;
	}

protected void acmv3050()
	{
		/* Update the existing ACMV record*/
		acmvagtpf = acmvDAO.searchAcmvByAxmcResult(b5471DTO.getAcmxpf());
		totalAcmvpfList.add(acmvagtpf);
	}

protected void callAcmv3060()
	{

		/* Post the reverse of the original ACMV.                          */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.rldgcoy.set(acmvagtpf.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvagtpf.getGenlcoy());
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(acmvagtpf.getRdocnum());
		lifacmvrec.tranno.set(0);
		lifacmvrec.jrnseq.set(acmvagtpf.getJrnseq());
		lifacmvrec.sacscode.set(acmvagtpf.getSacscode());
		lifacmvrec.sacstyp.set(acmvagtpf.getSacstyp());
		lifacmvrec.glcode.set(acmvagtpf.getGlcode());
		if (isEQ(acmvagtpf.getGlsign(),"+")) {
			lifacmvrec.glsign.set("-");
		}
		else {
			lifacmvrec.glsign.set("+");
		}
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(acmvagtpf.getOrigcurr());
		lifacmvrec.rldgacct.set(acmvagtpf.getRldgacct());
		lifacmvrec.origamt.set(acmvagtpf.getOrigamt());
		lifacmvrec.acctamt.set(acmvagtpf.getAcctamt());
		lifacmvrec.genlcur.set(acmvagtpf.getGenlcur());
		lifacmvrec.crate.set(acmvagtpf.getCrate());
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.postyear.set(bsscIO.getAcctYear());
		lifacmvrec.postmonth.set(bsscIO.getAcctMonth());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.tranref.set(acmvagtpf.getTranref());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("RL");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaRecordFound.set("Y");
	}

protected void reReadAcmx3100()
	{
					update3110();
		
					call3120();
		
	}

protected void update3110() {
	if (isLTE(wsaaTotAmtOut, 0)) {
		return;
	}


//	acmxList = acmxDao.searchAcmxByRldgacct(strCompany,wsaaStoredRasnum.toString());
    if (acmxList != null && acmxList.size() > 0) {
    	acmxIterator = acmxList.iterator();
    } else {
    	fatalError600();
    }
}

protected void call3120() {
	if (acmxIterator.hasNext()) {
		acmxpf = acmxIterator.next();
	} else {
		return;
	}
	updateAcmv3200();
	call3120();
}

protected void updateAcmv3200()
	{
					acmv3210();
		
					callAcmv3220();
		
	}

protected void acmv3210(){
	
	acmvagtpf = acmvDAO.searchAcmvByAxmcResult(acmxpf);
	if(acmvagtpf == null){
		fatalError600();
	}
}

protected void callAcmv3220()
	{
		if (isNE(acmxpf.getBatccoy(),acmvagtpf.getBatccoy())
		|| isNE(acmxpf.getBatcbrn(),acmvagtpf.getBatcbrn())
		|| isNE(acmxpf.getBatcactyr(),acmvagtpf.getBatcactyr())
		|| isNE(acmxpf.getBatcactmn(),acmvagtpf.getBatcactmn())
		|| isNE(acmxpf.getBatctrcde(),acmvagtpf.getBatctrcde())
		|| isNE(acmxpf.getBatcbatch(),acmvagtpf.getBatcbatch())
		|| isNE(acmxpf.getRldgacct(),acmvagtpf.getRldgacct())
		|| isNE(acmxpf.getOrigcurr(),acmvagtpf.getOrigcurr())
		|| isNE(acmxpf.getSacscode(),acmvagtpf.getSacscode())
		|| isNE(acmxpf.getSacstyp(),acmvagtpf.getSacstyp())
		|| isNE(acmxpf.getTranno(),acmvagtpf.getTranno())
		|| isNE(acmxpf.getRdocnum(),acmvagtpf.getRdocnum())
		|| isNE(acmxpf.getJrnseq(),acmvagtpf.getJrnseq())) {
			fatalError600();
		}

		acmvagtpf.setFrcdate(Integer.parseInt(bsscIO.getEffectiveDate().toString()));
		acmvagtpf.setRcamt(acmxpf.getOrigamt());
		totalAcmvpfList.add(acmvagtpf);
		/* Post the reverse of the original ACMV.                          */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.rldgcoy.set(acmvagtpf.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvagtpf.getGenlcoy());
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(acmvagtpf.getRdocnum());
		lifacmvrec.tranno.set(0);
		lifacmvrec.jrnseq.set(acmvagtpf.getJrnseq());
		lifacmvrec.sacscode.set(acmvagtpf.getSacscode());
		lifacmvrec.sacstyp.set(acmvagtpf.getSacstyp());
		lifacmvrec.glcode.set(acmvagtpf.getGlcode());
		if (isEQ(acmvagtpf.getGlsign(),"+")) {
			lifacmvrec.glsign.set("-");
		}
		else {
			lifacmvrec.glsign.set("+");
		}
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(acmvagtpf.getOrigcurr());
		lifacmvrec.rldgacct.set(acmvagtpf.getRldgacct());
		lifacmvrec.origamt.set(acmvagtpf.getOrigamt());
		lifacmvrec.acctamt.set(acmvagtpf.getAcctamt());
		lifacmvrec.genlcur.set(acmvagtpf.getGenlcur());
		lifacmvrec.crate.set(acmvagtpf.getCrate());
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.postyear.set(bsscIO.getAcctYear());
		lifacmvrec.postmonth.set(bsscIO.getAcctMonth());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.tranref.set(acmvagtpf.getTranref());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("RL");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void commit3500()
	{
		acmvDAO.updateAcmvBulk(totalAcmvpfList);
		totalAcmvpfList.clear();
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rasapf.getCurrcode());
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}

}
