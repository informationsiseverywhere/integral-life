/*
 * File: P5462.java
 * Date: 30 August 2009 0:27:54
 * Author: Quipoz Limited
 * 
 * Class transformed from P5462.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.CcalTableDAM;
import com.csc.life.reassurance.dataaccess.RacdrcvTableDAM;
import com.csc.life.reassurance.screens.S5462ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Work With Cash Calls Display Screen.
*
* This Screen uses the work with approach to display Reassurance
* RACD details. The contract number from the previous Sub-Menu
* appears at the top of the Screen and the associated Lives,
* components and RACD Details are displayed. These will be
* interspersed   between  the  components  so  that  they  are
* visually  associated  with  the  components  to  which  they
* relate.
*
* The RACD details shown are those with a Validflag of '4' ie
* those which have been terminated in some way, and thus may
* require a Cash Call transaction.
*
* The  usual  indentation  method is employed by the
* subfile  portion  of  the  screen  to   represent   the
* structure  of  the  contract  and the interdependance of the
* coverages and riders within the lives assured.
*
* The user will control the work to be carried out by  placing
* certain  actions  against  the  components  or  the existing
* payment details. The following validation is contained within
* the Program. Only one Cash Call may be permitted per RACD
* Record, although this may be modified as many times as is
* deemed necessary. The Cash Call Record may only be set up at
* RACD level, and trying to do it at any other level will cause
* an error.
*
* The Program uses OPTION Switching to switch from this to the
* next transaction screen.
*
* Although part of Terminations, this Program uses the
* COVRLNB Logical. This is because both the COVR and the
* COVRMJA Logicals originated in the days where the Relative
* Record number appeared in the Copybook primarily for show, as
* a PIC X(04) field. As this Program does a READD, it needs to
* use a Logical which has the Relative Record Number set up
* properly, as a PIC 9(09) COMP-4 field.
*
****************************************************************
* </pre>
*/
public class P5462 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5462");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e005 = "E005";
	private String r068 = "R068";
	private String r069 = "R069";
	private String r079 = "R079";
		/* TABLES */
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private String cltsrec = "CLTSREC";
	private String covrlnbrec = "COVRLNBREC";
	private String liferec = "LIFEREC";
	private String racdrcvrec = "RACDRCVREC";
	private String ccalrec = "CCALREC";

	private FixedLengthStringData wsaaReassuranceDetails = new FixedLengthStringData(43);
	private FixedLengthStringData wsaaCtdate = new FixedLengthStringData(10).isAPartOf(wsaaReassuranceDetails, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaReassuranceDetails, 10, FILLER).init(SPACES);
	private FixedLengthStringData wsaaRetype = new FixedLengthStringData(11).isAPartOf(wsaaReassuranceDetails, 11);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaReassuranceDetails, 22, FILLER).init(SPACES);
	private FixedLengthStringData wsaaRngmnt = new FixedLengthStringData(4).isAPartOf(wsaaReassuranceDetails, 23);
	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaReassuranceDetails, 27, FILLER).init(SPACES);
	private ZonedDecimalData wsaaRaAmount = new ZonedDecimalData(13, 2).isAPartOf(wsaaReassuranceDetails, 29).setPattern("ZZZZZZZZZZ9.99");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 17);
	private FixedLengthStringData filler3 = new FixedLengthStringData(178).isAPartOf(wsaaTransactionRec, 22, FILLER).init(SPACES);

	private FixedLengthStringData wsaaAtSubmitFlag = new FixedLengthStringData(1).init("N");
	private Validator atSubmissionReqd = new Validator(wsaaAtSubmitFlag, "Y");

		/* WSAA-MESSAGE-AREA */
	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("AT request submitted for ");
	private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
	private FixedLengthStringData wsaaAction = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8);
	private String wsaaFirstTime = "Y";
	private String wsaaEnquireOnly = "Y";
	private FixedLengthStringData wsaaHcallind = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaFirstLifeRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstCovrlnbRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstRacdrcvRead = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCompare = new FixedLengthStringData(2);
	private FixedLengthStringData filler5 = new FixedLengthStringData(1).isAPartOf(wsaaCompare, 0, FILLER).init("0");
	private String wsaaFirstRecord = "";

	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).isAPartOf(wsaaContractStatuzCheck, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
	private Atreqrec atreqrec = new Atreqrec();
		/*Claim Cash Call*/
	private CcalTableDAM ccalIO = new CcalTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Reassurance Cash Calls Logical File*/
	private RacdrcvTableDAM racdrcvIO = new RacdrcvTableDAM();
	private Sftlockrec sftlockrec1 = new Sftlockrec();
	private Batckey wsaaBatchkey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5462ScreenVars sv = ScreenProgram.getScreenVars( S5462ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1199, 
		exit1399, 
		exit1499, 
		exit1599, 
		preExit, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		updateErrorIndicators2670, 
		exit3090, 
		optswch4080, 
		exit4090, 
		exit4390
	}

	public P5462() {
		super();
		screenVars = sv;
		new ScreenModel("S5462", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsaaAction.set(SPACES);
			goTo(GotoLabel.exit1090);
		}
		wsaaAction.set(SPACES);
		scrnparams.subfileRrn.set(1);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.subfileMore.set("N");
		scrnparams.function.set(varcom.sclr);
		processScreen("S5462", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		sv.chdrsel.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		wsspcomn.currfrom.set(chdrmjaIO.getOccdate());
		wsaaReassuranceDetails.set(SPACES);
		wsaaFirstLifeRead.set(SPACES);
		wsaaFirstCovrlnbRead.set(SPACES);
		wsaaFirstRacdrcvRead.set(SPACES);
		wsaaEffdate.set(ZERO);
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(wsspcomn.company);
		lifeIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeIO.setCurrfrom(ZERO);
		lifeIO.setFormat(liferec);
		wsaaFirstLifeRead.set("Y");
		while ( !(isEQ(lifeIO.getStatuz(),varcom.endp))) {
			getPolicyDetails1100();
		}
		
	}

protected void getPolicyDetails1100()
	{
		try {
			getClientNumber1110();
			getClientName1120();
		}
		catch (GOTOException e){
		}
	}

protected void getClientNumber1110()
	{
		if (isEQ(wsaaFirstLifeRead,"Y")) {
			lifeIO.setFunction(varcom.begn);
			wsaaFirstLifeRead.set("N");
		}
		else {
			lifeIO.setFunction(varcom.nextr);
		}
		//performance improvement -- Anjali
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if ((isEQ(lifeIO.getStatuz(),varcom.endp))
		|| (isNE(lifeIO.getChdrcoy(),wsspcomn.company))
		|| (isNE(lifeIO.getChdrnum(),chdrmjaIO.getChdrnum()))
		|| (isNE(lifeIO.getJlife(),"00"))
		|| (isNE(lifeIO.getValidflag(),"1"))) {
			lifeIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1199);
		}
	}

protected void getClientName1120()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifeIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.subfileArea.set(SPACES);
		sv.life.set(lifeIO.getLife());
		sv.shortdesc.set(lifeIO.getLifcnum());
		sv.elemdesc.set(wsspcomn.longconfname);
		sv.actionOut[varcom.pr.toInt()].set("Y");
		sv.actionOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.hrrn01.set(ZERO);
		sv.hrrn02.set(ZERO);
		sv.hrrn03.set(ZERO);
		addToSubfile2500();
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(lifeIO.getChdrcoy());
		covrlnbIO.setChdrnum(lifeIO.getChdrnum());
		covrlnbIO.setLife(lifeIO.getLife());
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFormat(covrlnbrec);
		wsaaFirstCovrlnbRead.set("Y");
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			getComponentDetails1300();
		}
		
	}

protected void getComponentDetails1300()
	{
		try {
			getCovrlnbDetails1301();
		}
		catch (GOTOException e){
		}
	}

protected void getCovrlnbDetails1301()
	{
		if (isEQ(wsaaFirstCovrlnbRead,"Y")) {
			covrlnbIO.setFunction(varcom.begn);
			wsaaFirstCovrlnbRead.set("N");
		}
		else {
			covrlnbIO.setFunction(varcom.nextr);
		}
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if ((isEQ(covrlnbIO.getStatuz(),varcom.endp))
		|| (isNE(covrlnbIO.getChdrcoy(),lifeIO.getChdrcoy()))
		|| (isNE(covrlnbIO.getChdrnum(),lifeIO.getChdrnum()))
		|| (isNE(covrlnbIO.getLife(),lifeIO.getLife()))) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1399);
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.exit1399);
		}
		if (isEQ(covrlnbIO.getLife(),sv.life)
		&& isEQ(covrlnbIO.getCoverage(),sv.coverage)
		&& isEQ(covrlnbIO.getRider(),sv.rider)) {
			goTo(GotoLabel.exit1399);
		}
		else {
			sv.subfileArea.set(SPACES);
			sv.life.set(covrlnbIO.getLife());
			sv.rider.set(covrlnbIO.getRider());
			sv.coverage.set(covrlnbIO.getCoverage());
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			if ((isEQ(covrlnbIO.getRider(),ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
			}
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(t5687);
			descIO.setDescitem(covrlnbIO.getCrtable());
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if ((isNE(descIO.getStatuz(),varcom.oK))
			&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
				sv.elemdesc.set(SPACES);
			}
			else {
				sv.elemdesc.set(descIO.getLongdesc());
			}
			sv.shortdesc.set(covrlnbIO.getCrtable());
			sv.hcrtable.set(covrlnbIO.getCrtable());
			sv.hrrn01.set(ZERO);
			sv.hrrn02.set(ZERO);
			sv.hrrn03.set(covrlnbIO.getRrn());
			addToSubfile2500();
		}
		racdrcvIO.setParams(SPACES);
		racdrcvIO.setChdrcoy(covrlnbIO.getChdrcoy());
		racdrcvIO.setChdrnum(covrlnbIO.getChdrnum());
		racdrcvIO.setLife(covrlnbIO.getLife());
		racdrcvIO.setCoverage(covrlnbIO.getCoverage());
		racdrcvIO.setRider(covrlnbIO.getRider());
		racdrcvIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		racdrcvIO.setSeqno(ZERO);
		racdrcvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdrcvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdrcvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		racdrcvIO.setFormat(racdrcvrec);
		wsaaFirstRacdrcvRead.set("Y");
		sv.hrrn02.set(covrlnbIO.getRrn());
		sv.hrrn03.set(covrlnbIO.getRrn());
		while ( !(isEQ(racdrcvIO.getStatuz(),varcom.endp))) {
			getReassuranceDetails1400();
		}
		
	}

protected void getReassuranceDetails1400()
	{
		try {
			readRacdrcvDetails1401();
		}
		catch (GOTOException e){
		}
	}

protected void readRacdrcvDetails1401()
	{
		if (isEQ(wsaaFirstRacdrcvRead,"Y")) {
			racdrcvIO.setFunction(varcom.begn);
			wsaaFirstRacdrcvRead.set("N");
		}
		else {
			racdrcvIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, racdrcvIO);
		if (isNE(racdrcvIO.getStatuz(),varcom.oK)
		&& isNE(racdrcvIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdrcvIO.getParams());
			fatalError600();
		}
		if ((isEQ(racdrcvIO.getStatuz(),varcom.endp))
		|| (isNE(racdrcvIO.getChdrcoy(),covrlnbIO.getChdrcoy()))
		|| (isNE(racdrcvIO.getChdrnum(),covrlnbIO.getChdrnum()))
		|| (isNE(racdrcvIO.getLife(),covrlnbIO.getLife()))
		|| (isNE(racdrcvIO.getCoverage(),covrlnbIO.getCoverage()))
		|| (isNE(racdrcvIO.getRider(),covrlnbIO.getRider()))
		|| (isNE(racdrcvIO.getPlanSuffix(),covrlnbIO.getPlanSuffix()))) {
			racdrcvIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1499);
		}
		datcon1rec.intDate.set(racdrcvIO.getCtdate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaCtdate.set(datcon1rec.extDate);
		if (isEQ(racdrcvIO.getRetype(),"F")) {
			wsaaRetype.set("Facultative");
		}
		else {
			wsaaRetype.set("Treaty");
		}
		wsaaRngmnt.set(racdrcvIO.getRngmnt());
		wsaaRaAmount.set(racdrcvIO.getRaAmount());
		wsaaHcallind.set(SPACES);
		findCashCall1500();
		sv.subfileArea.set(SPACES);
		sv.hcallind.set(wsaaHcallind);
		sv.life.set(racdrcvIO.getLife());
		sv.coverage.set(racdrcvIO.getCoverage());
		sv.rider.set(racdrcvIO.getRider());
		sv.lifeOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.hcrtable.set(covrlnbIO.getCrtable());
		sv.hrrn01.set(racdrcvIO.getRrn());
		sv.hrrn02.set(covrlnbIO.getRrn());
		sv.hrrn03.set(covrlnbIO.getRrn());
		sv.elemdesc.set(wsaaReassuranceDetails);
		addToSubfile2500();
		racdrcvIO.setFunction(varcom.nextr);
	}

protected void findCashCall1500()
	{
		try {
			processCashCalls1501();
		}
		catch (GOTOException e){
		}
	}

protected void processCashCalls1501()
	{
		sv.hcallind.set(SPACES);
		ccalIO.setChdrcoy(racdrcvIO.getChdrcoy());
		ccalIO.setChdrnum(racdrcvIO.getChdrnum());
		ccalIO.setLife(racdrcvIO.getLife());
		ccalIO.setCoverage(racdrcvIO.getCoverage());
		ccalIO.setRider(racdrcvIO.getRider());
		ccalIO.setPlanSuffix(racdrcvIO.getPlanSuffix());
		ccalIO.setSeqno(racdrcvIO.getSeqno());
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ccalIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, ccalIO);
		if ((isNE(ccalIO.getStatuz(),varcom.oK))
		&& (isNE(ccalIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ccalIO.getParams());
			fatalError600();
		}
		if (isEQ(ccalIO.getStatuz(),varcom.endp)
		|| isNE(ccalIO.getChdrcoy(),racdrcvIO.getChdrcoy())
		|| isNE(ccalIO.getChdrnum(),racdrcvIO.getChdrnum())
		|| isNE(ccalIO.getLife(),racdrcvIO.getLife())
		|| isNE(ccalIO.getCoverage(),racdrcvIO.getCoverage())
		|| isNE(ccalIO.getRider(),racdrcvIO.getRider())
		|| isNE(ccalIO.getPlanSuffix(),racdrcvIO.getPlanSuffix())
		|| isNE(ccalIO.getSeqno(),racdrcvIO.getSeqno())) {
			goTo(GotoLabel.exit1599);
		}
		wsaaHcallind.set("Y");
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*DISPLAY-SCREEN*/
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2060();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5462", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void addToSubfile2500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5462", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.action,SPACES)) {
			sv.actionErr.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if ((isEQ(sv.hrrn01,ZERO))
		&& (isNE(sv.action,SPACES))) {
			sv.actionErr.set(e005);
			sv.action.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.action,"1")
		&& isNE(sv.hcallind,SPACES)) {
			sv.actionErr.set(r069);
			sv.action.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.hcallind,SPACES)
		&& (isEQ(sv.action,"2")
		|| isEQ(sv.action,"3"))) {
			sv.actionErr.set(r068);
			sv.action.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.action,"2")
		&& isLT(wssplife.effdate,ccalIO.getCalldt())) {
			sv.actionErr.set(r079);
			sv.action.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.action);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.actionErr.set(optswchrec.optsStatuz);
		}
		wsaaAction.set(sv.action);
		if (isNE(sv.action,"3")) {
			wsaaEnquireOnly = "N";
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.hrrn01,ZERO)) {
			if ((isEQ(sv.rider,"00"))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(SPACES);
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(SPACES);
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5462", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5462", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			processSelections3010();
		}
		catch (GOTOException e){
		}
	}

protected void processSelections3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsaaAtSubmitFlag.set("Y");
			goTo(GotoLabel.exit3090);
		}
		if (atSubmissionReqd.isTrue()
		&& isEQ(wsaaAction,SPACES)
		&& isEQ(wsaaEnquireOnly,"N")) {
			callAt3100();
		}
		else {
			if (isEQ(wsaaAction,SPACES)) {
				rlseSftlock3200();
			}
		}
	}

protected void callAt3100()
	{
		at3001();
	}

protected void at3001()
	{
		sftlockrec1.sftlockRec.set(SPACES);
		sftlockrec1.company.set(wsspcomn.company);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec1.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec1.user.set(varcom.vrcmUser);
		sftlockrec1.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec1.statuz);
			syserrrec.params.set(sftlockrec1.sftlockRec);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5462AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wssplife.effdate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		atreqrec.primaryKey.set(chdrmjaIO.getChdrnum());
		wsaaFsuco.set(wsspcomn.fsuco);
		wsaaEffdate.set(wssplife.effdate);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
		wsaaMsgnum.set(chdrmjaIO.getChdrnum());
		wsspcomn.msgarea.set(wsaaMsgarea);
		wsaaAtSubmitFlag.set("N");
	}

protected void rlseSftlock3200()
	{
		unlockContract3210();
	}

protected void unlockContract3210()
	{
		sftlockrec1.sftlockRec.set(SPACES);
		sftlockrec1.statuz.set(varcom.oK);
		sftlockrec1.company.set(wsspcomn.company);
		sftlockrec1.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec1.user.set(varcom.vrcmUser);
		sftlockrec1.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec1.sftlockRec);
			syserrrec.statuz.set(sftlockrec1.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case optswch4080: {
					optswch4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.action,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			goTo(GotoLabel.optswch4080);
		}
		if (isEQ(sv.action,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
		if (isEQ(sv.action,"2")) {
			optswchrec.optsSelOptno.set(2);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
		if (isEQ(sv.action,"3")) {
			optswchrec.optsSelOptno.set(3);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.action.set(SPACES);
			keepRecord4300();
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
	}

protected void optswch4080()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			programStack4200();
			goTo(GotoLabel.exit4090);
		}
		programStack4200();
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5462", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void programStack4200()
	{
		/*STCK*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*EXIT*/
	}

protected void keepRecord4300()
	{
		try {
			keep4310();
		}
		catch (GOTOException e){
		}
	}

protected void keep4310()
	{
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		racdrcvIO.setNonKey(SPACES);
		racdrcvIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		racdrcvIO.setChdrnum(chdrmjaIO.getChdrnum());
		racdrcvIO.setLife(sv.life);
		racdrcvIO.setCoverage(sv.coverage);
		racdrcvIO.setRider(sv.rider);
		racdrcvIO.setFormat(racdrcvrec);
		if (isEQ(sv.hrrn01,ZERO)) {
			racdrcvIO.setTranno(ZERO);
			racdrcvIO.setSeqno(ZERO);
			racdrcvIO.setCurrfrom(ZERO);
			racdrcvIO.setCurrto(ZERO);
			racdrcvIO.setRaAmount(ZERO);
			racdrcvIO.setCtdate(ZERO);
			racdrcvIO.setCmdate(ZERO);
			racdrcvIO.setReasper(ZERO);
			racdrcvIO.setRecovamt(ZERO);
			racdrcvIO.setRrevdt(ZERO);
			racdrcvIO.setValidflag("1");
			racdrcvIO.setFunction(varcom.keeps);
		}
		else {
			racdrcvIO.setRrn(sv.hrrn01);
			racdrcvIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, racdrcvIO);
			if (isNE(racdrcvIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(racdrcvIO.getParams());
				fatalError600();
			}
			racdrcvIO.setFunction(varcom.keeps);
		}
		SmartFileCode.execute(appVars, racdrcvIO);
		if (isNE(racdrcvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrcvIO.getParams());
			fatalError600();
		}
		covrlnbIO.setParams(SPACES);
		if (isEQ(wsaaAction,"1")) {
			covrlnbIO.setRrn(sv.hrrn03);
		}
		else {
			covrlnbIO.setRrn(sv.hrrn02);
		}
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		covrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaAction,"1")) {
			goTo(GotoLabel.exit4390);
		}
		ccalIO.setParams(SPACES);
		ccalIO.setChdrcoy(racdrcvIO.getChdrcoy());
		ccalIO.setChdrnum(racdrcvIO.getChdrnum());
		ccalIO.setLife(racdrcvIO.getLife());
		ccalIO.setCoverage(racdrcvIO.getCoverage());
		ccalIO.setRider(racdrcvIO.getRider());
		ccalIO.setPlanSuffix(racdrcvIO.getPlanSuffix());
		ccalIO.setSeqno(racdrcvIO.getSeqno());
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalIO.getParams());
			fatalError600();
		}
	}
}
