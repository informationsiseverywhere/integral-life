/*
 * File: C5470.java
 * Date: 30 August 2009 2:58:50
 * Author: $Id$
 * 
 * Class transformed from C5470.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator;
import java.util.List;

import com.csc.life.reassurance.batchprograms.B5470;
import com.csc.smart400framework.dataaccess.dao.AcmvpfDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.utility.ConversionUtil;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.FileCode;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;
public class C5470 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);
	private FixedLengthStringData bsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData buparec = new FixedLengthStringData(1024);
	private FixedLengthStringData effdt = new FixedLengthStringData(8);
	private FixedLengthStringData sacs = new FixedLengthStringData(2);
	private FixedLengthStringData coy = new FixedLengthStringData(1);
	private FixedLengthStringData maxdate = new FixedLengthStringData(8);
	private FixedLengthStringData lib = new FixedLengthStringData(5);
	private FixedLengthStringData file = new FixedLengthStringData(8);
	private Iterator<Acmvpf> acmvpfIter = null;
	private AcmvpfDAO acmvpfDao = null;
	private List<Acmvpf> acmvpfList = null;
	public C5470() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		buparec = convertAndSetParam(buparec, parmArray, 4);
		bprdrec = convertAndSetParam(bprdrec, parmArray, 3);
		bsprrec = convertAndSetParam(bsprrec, parmArray, 2);
		bsscrec = convertAndSetParam(bsscrec, parmArray, 1);
		statuz = convertAndSetParam(statuz, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					sacs.set(subString(bprdrec, 572, 2));
					effdt.set(subString(bsscrec, 174, 8));
					coy.set(subString(bsprrec, 16, 1));
					maxdate.set("99999999");
					lib.set("QTEMP");
					file.set("B5470TMP");
					FileCode.createPhysicalFile(lib + "." + file, 177, null, null, null, null, null, null, null);
					//acmvpfDao = DAOFactory.getDAO(AcmvpfDAO.class);
					acmvpfDao = DAOFactory.getAcmvpfDAO();
					acmvpfList = acmvpfDao.findByCriteriaAndOrder(ConversionUtil.toString(sacs), effdt.toInt(),
																		maxdate.toInt(), ConversionUtil.toCharacter(coy));
					acmvpfIter = acmvpfList.iterator();
					callProgram(B5470.class, new Object[] {statuz, bsscrec, bsprrec, bprdrec, buparec,acmvpfIter});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					statuz.set("BOMB");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
