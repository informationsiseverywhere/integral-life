package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6262
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6262ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(342);
	public FixedLengthStringData dataFields = new FixedLengthStringData(166).isAPartOf(dataArea, 0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,50);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,60);
	public FixedLengthStringData payfrqd = DD.payfrqd.copy().isAPartOf(dataFields,77);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(dataFields,87);
	public FixedLengthStringData rapayfrq = DD.rapayfrq.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData ratype = DD.ratype.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData ratypeDesc = DD.ratypedesc.copy().isAPartOf(dataFields,118);
	public ZonedDecimalData totsuma = DD.totsuma.copyToZonedDecimal().isAPartOf(dataFields,148);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 166);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData payfrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData rapayfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ratypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ratypedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData totsumaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 210);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] payfrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] rapayfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ratypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ratypedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] totsumaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6262screenWritten = new LongData(0);
	public LongData S6262windowWritten = new LongData(0);
	public LongData S6262protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6262ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rasnumOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rapayfrqOut,new String[] {"02","50","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratypeOut,new String[] {"03","50","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"05","50","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(raamountOut,new String[] {"06","50","-06",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {rasnum, cltname, rapayfrq, ratype, ratypeDesc, totsuma, instPrem, currcode, currds, payfrqd, raAmount};
		screenOutFields = new BaseData[][] {rasnumOut, cltnameOut, rapayfrqOut, ratypeOut, ratypedescOut, totsumaOut, instprmOut, currcodeOut, currdsOut, payfrqdOut, raamountOut};
		screenErrFields = new BaseData[] {rasnumErr, cltnameErr, rapayfrqErr, ratypeErr, ratypedescErr, totsumaErr, instprmErr, currcodeErr, currdsErr, payfrqdErr, raamountErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6262screen.class;
		protectRecord = S6262protect.class;
	}

}
