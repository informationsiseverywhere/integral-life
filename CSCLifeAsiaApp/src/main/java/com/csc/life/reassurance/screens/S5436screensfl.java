package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5436screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 19, 14, 64}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5436ScreenVars sv = (S5436ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5436screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5436screensfl, 
			sv.S5436screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5436ScreenVars sv = (S5436ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5436screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5436ScreenVars sv = (S5436ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5436screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5436screensflWritten.gt(0))
		{
			sv.s5436screensfl.setCurrentIndex(0);
			sv.S5436screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5436ScreenVars sv = (S5436ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5436screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5436ScreenVars screenVars = (S5436ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rngmnt.setFieldName("rngmnt");
				screenVars.retype.setFieldName("retype");
				screenVars.raAmount.setFieldName("raAmount");
				screenVars.currency.setFieldName("currency");
				screenVars.chdrnum.setFieldName("chdrnum");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.rngmnt.set(dm.getField("rngmnt"));
			screenVars.retype.set(dm.getField("retype"));
			screenVars.raAmount.set(dm.getField("raAmount"));
			screenVars.currency.set(dm.getField("currency"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5436ScreenVars screenVars = (S5436ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rngmnt.setFieldName("rngmnt");
				screenVars.retype.setFieldName("retype");
				screenVars.raAmount.setFieldName("raAmount");
				screenVars.currency.setFieldName("currency");
				screenVars.chdrnum.setFieldName("chdrnum");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("rngmnt").set(screenVars.rngmnt);
			dm.getField("retype").set(screenVars.retype);
			dm.getField("raAmount").set(screenVars.raAmount);
			dm.getField("currency").set(screenVars.currency);
			dm.getField("chdrnum").set(screenVars.chdrnum);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5436screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5436ScreenVars screenVars = (S5436ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.rngmnt.clearFormatting();
		screenVars.retype.clearFormatting();
		screenVars.raAmount.clearFormatting();
		screenVars.currency.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5436ScreenVars screenVars = (S5436ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.rngmnt.setClassString("");
		screenVars.retype.setClassString("");
		screenVars.raAmount.setClassString("");
		screenVars.currency.setClassString("");
		screenVars.chdrnum.setClassString("");
	}

/**
 * Clear all the variables in S5436screensfl
 */
	public static void clear(VarModel pv) {
		S5436ScreenVars screenVars = (S5436ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.rngmnt.clear();
		screenVars.retype.clear();
		screenVars.raAmount.clear();
		screenVars.currency.clear();
		screenVars.chdrnum.clear();
	}
}
