package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:53
 * Description:
 * Copybook name: REXPUPDREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rexpupdrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rexpupdRec = new FixedLengthStringData(85);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rexpupdRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rexpupdRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rexpupdRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rexpupdRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rexpupdRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rexpupdRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rexpupdRec, 22);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(rexpupdRec, 24);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rexpupdRec, 27);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(1).isAPartOf(rexpupdRec, 30);
  	public FixedLengthStringData l1Clntnum = new FixedLengthStringData(8).isAPartOf(rexpupdRec, 31);
  	public FixedLengthStringData l2Clntnum = new FixedLengthStringData(8).isAPartOf(rexpupdRec, 39);
  	public FixedLengthStringData riskClass = new FixedLengthStringData(4).isAPartOf(rexpupdRec, 47);
  	public FixedLengthStringData reassurer = new FixedLengthStringData(8).isAPartOf(rexpupdRec, 51);
  	public FixedLengthStringData arrangement = new FixedLengthStringData(4).isAPartOf(rexpupdRec, 59);
  	public FixedLengthStringData cestype = new FixedLengthStringData(1).isAPartOf(rexpupdRec, 63);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(rexpupdRec, 64);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(rexpupdRec, 67);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rexpupdRec, 76);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(rexpupdRec, 81);


	public void initialize() {
		COBOLFunctions.initialize(rexpupdRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rexpupdRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}