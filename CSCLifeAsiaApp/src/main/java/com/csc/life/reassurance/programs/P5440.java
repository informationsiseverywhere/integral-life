/*
 * File: P5440.java
 * Date: 30 August 2009 0:26:13
 * Author: Quipoz Limited
 * 
 * Class transformed from P5440.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.procedures.Csncalc;
import com.csc.life.reassurance.procedures.Rcorfnd;
import com.csc.life.reassurance.procedures.Rexpupd;
import com.csc.life.reassurance.procedures.Trmracd;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.screens.S5440ScreenVars;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Auto Cession Component Selection.
*
* Overview
* ========
*
* This program forms part of the Reassurance Development.
* It is run off the Contract Auto Cession Submenu screen, S5439.
*
* It is a scrolling selection subfile. Switching after component
* selection is controlled by OPTSWCH.
*
* Processing
* ==========
*
* 1000-Initialise
*
* Clear the subfile.
* Retrieve CHDRLNB.
* Set up screen header details.
* Load all associated coverages and riders to the subfile.
* Call CSNCALC to create RACD records. If a statuz of 'FACL'
* is returned, facultative RACD records have been created so
* load these into the subfile. Note that treaty RACD records
* are NOT loaded into the subfile and therefore are never
* displayed. The reason for this is that the user is only
* allowed to modify facultative records. Treaty records are
* created from arrangements which are table driven and should
* not be altered.
* If a status of O-K is returned, facultative records were
* not created and therefore do not need to be reviewed.
* Display a message to the user and skip all other processing.
* Otherwise, intialise call to OPTSWCH and add to the subfile
* all coverages and riders which have facultative reassurance
* associated with them.
*
* 2000-Screen-Edit
*
* Display the screen.
* If no facultative records exist, skip out of this section.
* Otherwise, validate the subfile and redisplay the screen if
* any errors exist.
*
* 3000-Update
*
* No updating required.
*
* 4000-Where-Next
*
* Find selected components, if any. If no facultative records
* were created, there will be no records displayed and therefore
* no selection can be made so return to the submenu.
* If a selection is found, store the COVRLNB for use in
* the next program and call OPTSWCH to load the program stack
* and call the next program.
* If no selection is found, return to the submenu.
*
*****************************************************************
* </pre>
*/
public class P5440 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5440");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaAtSubmitFlag = new FixedLengthStringData(1);
	private Validator atSubmissionReqd = new Validator(wsaaAtSubmitFlag, "Y");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(178).isAPartOf(wsaaTransactionRec, 22, FILLER).init(SPACES);

		/* WSAA-MESSAGE-AREA */
	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("AT request submitted for ");
	private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
	private PackedDecimalData wsaaCessAmt = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaStoreLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaValidStatcode = new FixedLengthStringData(1);
	private Validator validStatcode = new Validator(wsaaValidStatcode, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1);
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaFacl = new FixedLengthStringData(1);
	private Validator noFacl = new Validator(wsaaFacl, "N");
	private Validator yesFacl = new Validator(wsaaFacl, "Y");

	private FixedLengthStringData wsaaReviewInd = new FixedLengthStringData(1);
	private Validator reviewed = new Validator(wsaaReviewInd, "Y");
	private Validator notReviewed = new Validator(wsaaReviewInd, "N");

	private FixedLengthStringData wsaaCompSelected = new FixedLengthStringData(1);
	private Validator compSelected = new Validator(wsaaCompSelected, "Y");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaJlifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(1);
	private Validator statuzOk = new Validator(wsaaStatuz, "Y");
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);

	private FixedLengthStringData wsaaJlifeExists = new FixedLengthStringData(1);
	private Validator jlifeExists = new Validator(wsaaJlifeExists, "Y");

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* ERRORS */
	private static final String r067 = "R067";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5446 = "T5446";
	private static final String t5448 = "T5448";
	private static final String t5679 = "T5679";
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String tr386 = "TR386";
		/* FORMATS */
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String lifeenqrec = "LIFEENQREC";
	private static final String racdlnbrec = "RACDLNBREC";
	private static final String racdmjarec = "RACDMJAREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Atreqrec atreqrec = new Atreqrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Optswchrec optswchrec = new Optswchrec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private Trmracdrec trmracdrec = new Trmracdrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S5440ScreenVars sv = ScreenProgram.getScreenVars( S5440ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		next1180, 
		exit1190, 
		next1980, 
		exit1990, 
		exit2090, 
		next2054, 
		exit2055, 
		updateErrorIndicators2120
	}

	public P5440() {
		super();
		screenVars = sv;
		new ScreenModel("S5440", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			headerDetail1030();
			readLifeDetails1050();
			jointLifeDetails1060();
			contractTypeStatus1070();
			subfileLoad1080();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		wsspcomn.currfrom.set(wssplife.effdate);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5440", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void headerDetail1030()
	{
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.cntcurr.set(chdrlnbIO.getCntcurr());
		sv.register.set(chdrlnbIO.getRegister());
	}

protected void readLifeDetails1050()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1060()
	{
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void contractTypeStatus1070()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
	}

protected void subfileLoad1080()
	{
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			processCovr1800();
		}
		
		sv.hflagOut[varcom.nd.toInt()].set("N");
		wsaaFacl.set("Y");
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifeenqIO.setFormat(lifeenqrec);
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaLife.set(SPACES);
		wsaaStatuz.set("Y");
		while ( !(isEQ(lifeenqIO.getStatuz(),varcom.endp))) {
			callCsncalc1900();
		}
		
		if (statuzOk.isTrue()) {
			sv.comt01.set(tr386rec.progdesc01);
			sv.comt02.set(tr386rec.progdesc02);
			sv.hflagOut[varcom.nd.toInt()].set("Y");
			wsaaFacl.set("N");
			wsaaAtSubmitFlag.set("Y");
			return ;
		}
		else {
			wsaaAtSubmitFlag.set("N");
		}
		scrnparams.subfileRrn.set(1);
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaStoreLife.set(SPACES);
		wsaaStoreCoverage.set(SPACES);
		wsaaStoreRider.set(SPACES);
		while ( !(isEQ(racdlnbIO.getStatuz(),varcom.endp))) {
			listCoverages1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void listCoverages1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFirstCoverage1110();
				}
				case next1180: {
					next1180();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFirstCoverage1110()
	{
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)
		&& isNE(racdlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(racdlnbIO.getStatuz(),varcom.endp)) {
			racdlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		if (isNE(racdlnbIO.getCestype(),"2")) {
			goTo(GotoLabel.next1180);
		}
		if (isEQ(racdlnbIO.getLife(),wsaaStoreLife)
		&& isEQ(racdlnbIO.getCoverage(),wsaaStoreCoverage)
		&& isEQ(racdlnbIO.getRider(),wsaaStoreRider)) {
			goTo(GotoLabel.next1180);
		}
		else {
			wsaaStoreLife.set(racdlnbIO.getLife());
			wsaaStoreCoverage.set(racdlnbIO.getCoverage());
			wsaaStoreRider.set(racdlnbIO.getRider());
		}
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(racdlnbIO.getChdrcoy());
		covrlnbIO.setChdrnum(racdlnbIO.getChdrnum());
		covrlnbIO.setLife(racdlnbIO.getLife());
		covrlnbIO.setCoverage(racdlnbIO.getCoverage());
		covrlnbIO.setRider(racdlnbIO.getRider());
		covrlnbIO.setPlanSuffix(racdlnbIO.getPlanSuffix());
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isEQ(covrlnbIO.getRider(),ZERO)) {
			coverageToScreen1200();
		}
		else {
			riderToScreen1300();
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5440", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void next1180()
	{
		racdlnbIO.setFunction(varcom.nextr);
	}

protected void coverageToScreen1200()
	{
		/*COVRLNB-SUBFILE*/
		sv.cmpntnum.set(covrlnbIO.getCoverage());
		wsaaCoverage.set(covrlnbIO.getCrtable());
		sv.component.set(wsaaCovrComponent);
		getDescription1400();
		covrStatusDescs1500();
		validateComponent1600();
		sv.hlifeno.set(covrlnbIO.getLife());
		sv.hsuffix.set(covrlnbIO.getPlanSuffix());
		sv.hcoverage.set(covrlnbIO.getCoverage());
		sv.hrider.set(covrlnbIO.getRider());
		/*EXIT*/
	}

protected void riderToScreen1300()
	{
		/*RIDER-SUBFILE*/
		sv.cmpntnum.set(covrlnbIO.getRider());
		wsaaRider.set(covrlnbIO.getCrtable());
		sv.component.set(wsaaRidrComponent);
		getDescription1400();
		covrStatusDescs1500();
		validateComponent1600();
		sv.hlifeno.set(covrlnbIO.getLife());
		sv.hsuffix.set(covrlnbIO.getPlanSuffix());
		sv.hcoverage.set(covrlnbIO.getCoverage());
		sv.hrider.set(covrlnbIO.getRider());
		/*EXIT*/
	}

protected void getDescription1400()
	{
		para1410();
	}

protected void para1410()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrlnbIO.getCrtable());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.deit.fill("?");
		}
		else {
			sv.deit.set(descIO.getLongdesc());
		}
	}

protected void covrStatusDescs1500()
	{
		para1510();
	}

protected void para1510()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrlnbIO.getPstatcode());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatcode.fill("?");
		}
		else {
			sv.pstatcode.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrlnbIO.getStatcode());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.statcode.fill("?");
		}
		else {
			sv.statcode.set(descIO.getShortdesc());
		}
	}

protected void validateComponent1600()
	{
		component1610();
	}

protected void component1610()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		headerStatuzCheck1700();
		if (isNE(wsaaValidStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void headerStatuzCheck1700()
	{
		headerStatuzCheck1710();
	}

protected void headerStatuzCheck1710()
	{
		wsaaValidStatcode.set("N");
		wsaaValidPstatcode.set("N");
		if (isEQ(covrlnbIO.getRider(),SPACES)
		|| isEQ(covrlnbIO.getRider(),"00")) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| validStatcode.isTrue()); wsaaIndex.add(1)){
				if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrlnbIO.getStatcode())) {
					wsaaValidStatcode.set("Y");
					for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
					|| validPstatcode.isTrue()); wsaaIndex.add(1)){
						if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrlnbIO.getPstatcode())) {
							wsaaValidPstatcode.set("Y");
						}
					}
				}
			}
		}
		if (isNE(covrlnbIO.getRider(),SPACES)
		&& isNE(covrlnbIO.getRider(),"00")) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrlnbIO.getStatcode())) {
					wsaaValidStatcode.set("Y");
					for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
					|| validPstatcode.isTrue()); wsaaIndex.add(1)){
						if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrlnbIO.getPstatcode())) {
							wsaaValidPstatcode.set("Y");
						}
					}
				}
			}
		}
		if (validStatcode.isTrue()
		&& validPstatcode.isTrue()) {
			wsaaValidStatus = "Y";
		}
	}

protected void processCovr1800()
	{
			covr1801();
		}

protected void covr1801()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(),chdrlnbIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		racdmjaIO.setChdrnum(covrmjaIO.getChdrnum());
		racdmjaIO.setLife(covrmjaIO.getLife());
		racdmjaIO.setCoverage(covrmjaIO.getCoverage());
		racdmjaIO.setRider(covrmjaIO.getRider());
		racdmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(racdmjarec);
		racdmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		wsaaCessAmt.set(ZERO);
		readLife1870();
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			processRacd1810();
		}
		
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void processRacd1810()
	{
			racd1811();
		}

protected void racd1811()
	{
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)
		&& isNE(racdmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdmjaIO.getParams());
			fatalError600();
		}
		if (isNE(racdmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(racdmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(racdmjaIO.getLife(),covrmjaIO.getLife())
		|| isNE(racdmjaIO.getCoverage(),covrmjaIO.getCoverage())
		|| isNE(racdmjaIO.getRider(),covrmjaIO.getRider())
		|| isNE(racdmjaIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())
		|| isEQ(racdmjaIO.getStatuz(),varcom.endp)) {
			racdmjaIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaCessAmt.add(racdmjaIO.getRaAmount());
		if (isGT(racdmjaIO.getCtdate(),wssplife.effdate)) {
			callRcorfnd1850();
		}
		terminateRacd1820();
		if (isLT(wsaaCessAmt,covrmjaIO.getSumins())) {
			retentionUpdate1830();
		}
		racdmjaIO.setFunction(varcom.nextr);
	}

protected void terminateRacd1820()
	{
		racd1821();
	}

protected void racd1821()
	{
		trmracdrec.trmracdRec.set(SPACES);
		trmracdrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		trmracdrec.chdrnum.set(racdmjaIO.getChdrnum());
		trmracdrec.life.set(racdmjaIO.getLife());
		trmracdrec.coverage.set(racdmjaIO.getCoverage());
		trmracdrec.rider.set(racdmjaIO.getRider());
		trmracdrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		trmracdrec.lrkcls.set(racdmjaIO.getLrkcls());
		trmracdrec.crtable.set(covrmjaIO.getCrtable());
		trmracdrec.cnttype.set(chdrlnbIO.getCnttype());
		trmracdrec.effdate.set(wssplife.effdate);
		trmracdrec.newRaAmt.set(ZERO);
		trmracdrec.recovamt.set(ZERO);
		compute(trmracdrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		trmracdrec.seqno.set(racdmjaIO.getSeqno());
		trmracdrec.clntcoy.set(wsspcomn.fsuco);
		trmracdrec.l1Clntnum.set(wsaaLifenum);
		trmracdrec.jlife.set(covrmjaIO.getJlife());
		trmracdrec.crrcd.set(covrmjaIO.getCrrcd());
		trmracdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		if (jlifeExists.isTrue()) {
			trmracdrec.l2Clntnum.set(wsaaJlifenum);
		}
		trmracdrec.function.set("TRMR");
		callProgram(Trmracd.class, trmracdrec.trmracdRec);
		if (isNE(trmracdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(trmracdrec.statuz);
			syserrrec.params.set(trmracdrec.trmracdRec);
			fatalError600();
		}
	}

protected void retentionUpdate1830()
	{
			update1831();
		}

protected void update1831()
	{
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCnttype());
		stringVariable1.addExpression(covrmjaIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5448);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(),wsaaT5448Item)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			return ;
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(covrmjaIO.getChdrcoy());
		rexpupdrec.chdrnum.set(covrmjaIO.getChdrnum());
		rexpupdrec.life.set(covrmjaIO.getLife());
		rexpupdrec.coverage.set(covrmjaIO.getCoverage());
		rexpupdrec.rider.set(covrmjaIO.getRider());
		rexpupdrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		compute(rexpupdrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		rexpupdrec.clntcoy.set(wsspcomn.fsuco);
		rexpupdrec.l1Clntnum.set(wsaaLifenum);
		if (isNE(covrmjaIO.getJlife(),"00")) {
			rexpupdrec.l2Clntnum.set(wsaaJlifenum);
		}
		compute(rexpupdrec.sumins, 2).set(sub(covrmjaIO.getSumins(),wsaaCessAmt));
		readT54461840();
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(wssplife.effdate);
		rexpupdrec.riskClass.set(racdmjaIO.getLrkcls());
		rexpupdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		if (isNE(rexpupdrec.currency,chdrlnbIO.getCntcurr())) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt1860();
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		rexpupdrec.function.set("DECR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			fatalError600();
		}
	}

protected void readT54461840()
	{
		t54461841();
	}

protected void t54461841()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5446);
		itdmIO.setItemitem(racdmjaIO.getLrkcls());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(),racdmjaIO.getLrkcls())
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5446)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(racdmjaIO.getLrkcls());
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callRcorfnd1850()
	{
		callRcorfnd1851();
	}

protected void callRcorfnd1851()
	{
		rcorfndrec.rcorfndRec.set(SPACES);
		rcorfndrec.function.set("UPDT");
		rcorfndrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		rcorfndrec.chdrnum.set(racdmjaIO.getChdrnum());
		rcorfndrec.life.set(racdmjaIO.getLife());
		rcorfndrec.coverage.set(racdmjaIO.getCoverage());
		rcorfndrec.rider.set(racdmjaIO.getRider());
		rcorfndrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		rcorfndrec.currfrom.set(racdmjaIO.getCurrfrom());
		rcorfndrec.seqno.set(racdmjaIO.getSeqno());
		compute(rcorfndrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		rcorfndrec.effdate.set(wssplife.effdate);
		rcorfndrec.polsum.set(chdrlnbIO.getPolsum());
		rcorfndrec.batckey.set(wsspcomn.batchkey);
		rcorfndrec.newRaAmt.set(ZERO);
		rcorfndrec.language.set(wsspcomn.language);
		rcorfndrec.refund.set(1);
		rcorfndrec.crtable.set(covrmjaIO.getCrtable());
		rcorfndrec.cnttype.set(chdrlnbIO.getCnttype());
		callProgram(Rcorfnd.class, rcorfndrec.rcorfndRec);
		if (isNE(rcorfndrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rcorfndrec.statuz);
			syserrrec.params.set(rcorfndrec.rcorfndRec);
			fatalError600();
		}
	}

protected void callXcvrt1860()
	{
		call1861();
	}

protected void call1861()
	{
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(wssplife.effdate);
		conlinkrec.company.set(chdrlnbIO.getChdrcoy());
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void readLife1870()
	{
		life1871();
	}

protected void life1871()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		wsaaLifenum.set(lifeenqIO.getLifcnum());
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			wsaaJlifenum.set(lifeenqIO.getLifcnum());
			wsaaJlifeExists.set("Y");
		}
		else {
			wsaaJlifenum.set(SPACES);
			wsaaJlifeExists.set("N");
		}
	}

protected void callCsncalc1900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					csncalc1910();
				}
				case next1980: {
					next1980();
				}
				case exit1990: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void csncalc1910()
	{
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(lifeenqIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(lifeenqIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(lifeenqIO.getStatuz(),varcom.endp)) {
			lifeenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1990);
		}
		if (isEQ(lifeenqIO.getLife(),wsaaLife)) {
			goTo(GotoLabel.next1980);
		}
		wsaaLife.set(lifeenqIO.getLife());
		csncalcrec.csncalcRec.set(SPACES);
		csncalcrec.function.set("COVR");
		csncalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		csncalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		csncalcrec.life.set(lifeenqIO.getLife());
		csncalcrec.cnttype.set(chdrlnbIO.getCnttype());
		csncalcrec.currency.set(chdrlnbIO.getCntcurr());
		csncalcrec.fsuco.set(wsspcomn.fsuco);
		csncalcrec.language.set(wsspcomn.language);
		csncalcrec.incrAmt.set(ZERO);
		csncalcrec.effdate.set(wssplife.effdate);
		compute(csncalcrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		csncalcrec.planSuffix.set(ZERO);
		csncalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Csncalc.class, csncalcrec.csncalcRec);
		if (isNE(csncalcrec.statuz,varcom.oK)
		&& isNE(csncalcrec.statuz,"FACL")) {
			syserrrec.statuz.set(csncalcrec.statuz);
			syserrrec.params.set(csncalcrec.csncalcRec);
			fatalError600();
		}
		if (isEQ(csncalcrec.statuz,"FACL")) {
			wsaaStatuz.set("N");
		}
	}

protected void next1980()
	{
		lifeenqIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		&& noFacl.isTrue()) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (yesFacl.isTrue()) {
			scrnparams.function.set(varcom.init);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenEditPara2000();
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenEditPara2000()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (noFacl.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
		else {
			if (reviewed.isTrue()) {
				goTo(GotoLabel.exit2090);
			}
		}
		checkFaclReviewed2050();
		if (reviewed.isTrue()) {
			wsaaAtSubmitFlag.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5440", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		wsaaCompSelected.set("N");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		if (!compSelected.isTrue()) {
			scrnparams.errorCode.set(r067);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkFaclReviewed2050()
	{
		/*START*/
		wsaaReviewInd.set("Y");
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(racdlnbIO.getStatuz(),varcom.endp))) {
			checkFacl2050();
		}
		
		/*EXIT*/
	}

protected void checkFacl2050()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkFaclPara2050();
				case next2054: {
					next2054();
				}
				case exit2055: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkFaclPara2050()
	{
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)
		&& isNE(racdlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(racdlnbIO.getStatuz(),varcom.endp)) {
			racdlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2055);
		}
		if (isNE(racdlnbIO.getCestype(),"2")) {
			goTo(GotoLabel.next2054);
		}
		else {
			if (isEQ(racdlnbIO.getOvrdind(),SPACES)) {
				racdlnbIO.setStatuz(varcom.endp);
				wsaaReviewInd.set("N");
			}
		}
	}

protected void next2054()
	{
		racdlnbIO.setFunction(varcom.nextr);
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNext2130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(sv.select,SPACES)) {
				wsaaCompSelected.set("Y");
			}
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5440", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNext2130()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5440", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* Bypass this section if returning from a previous use of this*/
		/* program.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (noFacl.isTrue()) {
			if (atSubmissionReqd.isTrue()) {
				sftlckCallAt3100();
			}
			else {
				rlseSftlock3200();
			}
		}
		else {
			if (reviewed.isTrue()) {
				sftlckCallAt3100();
			}
		}
	}

protected void sftlckCallAt3100()
	{
		softlock3110();
	}

protected void softlock3110()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5440AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		atreqrec.primaryKey.set(chdrlnbIO.getChdrnum());
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaFsuco.set(wsspcomn.fsuco);
		wsaaEffdate.set(wssplife.effdate);
		atreqrec.transArea.set(wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
		wsaaMsgnum.set(chdrlnbIO.getChdrnum());
		wsspcomn.msgarea.set(wsaaMsgarea);
	}

protected void rlseSftlock3200()
	{
		unlockContract3210();
	}

protected void unlockContract3210()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
					nextProgram4010();
					optswch4080();
				}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (noFacl.isTrue()) {
			return ;
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			return ;
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.select.set(SPACES);
			covrlnbIO.setParams(SPACES);
			covrlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			covrlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
			covrlnbIO.setLife(sv.hlifeno);
			covrlnbIO.setPlanSuffix(sv.hsuffix);
			covrlnbIO.setCoverage(sv.hcoverage);
			covrlnbIO.setRider(sv.hrider);
			covrlnbIO.setFunction(varcom.reads);
			covrlnbIO.setFormat(covrlnbrec);
			SmartFileCode.execute(appVars, covrlnbIO);
			if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrlnbIO.getParams());
				fatalError600();
			}
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
	}

protected void optswch4080()
	{
		programStack4200();
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5440", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void programStack4200()
	{
		/*STCK*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rexpupdrec.currency);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
}
