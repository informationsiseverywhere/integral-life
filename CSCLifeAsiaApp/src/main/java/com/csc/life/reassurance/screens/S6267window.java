package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for WINDOW
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6267window extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {17, 4, 22, 18, 5, 23, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 9, 44, 51}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6267ScreenVars sv = (S6267ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6267windowWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6267ScreenVars screenVars = (S6267ScreenVars)pv;
		screenVars.rasnum.setClassString("");
	}

/**
 * Clear all the variables in S6267window
 */
	public static void clear(VarModel pv) {
		S6267ScreenVars screenVars = (S6267ScreenVars) pv;
		screenVars.rasnum.clear();
	}
}
