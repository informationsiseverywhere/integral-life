/*
 * File: Pp008edlr.java
 * Date: 30 August 2009 1:15:38
 * Author: Quipoz Limited
 *
 * Class transformed from PP008EDLR.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.accounting.recordstructures.Jrnledrec;
import com.csc.fsu.general.procedures.Cktrtyp;
import com.csc.fsu.general.procedures.Glkey;
import com.csc.fsu.general.recordstructures.Cktrtyprec;
import com.csc.fsu.general.recordstructures.Glkeyrec;
import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  GL JOURNALS EDITING   - TRANSCODE LR - LIFE REASSURER
*
*
* This routine is called dynamically from PP008 - GL direct
* journals create to validate the transaction key for which
* the journal applies.
*
*
*********************************************************
* </pre>
*/
public class Pp008edlr extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("PP008EDLR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	private String e049 = "E049";
	private String e192 = "E192";
	private String e193 = "E193";
	private String e194 = "E194";
	private String e196 = "E196";
	private String e199 = "E199";
	private String e289 = "E289";
	private String e946 = "E946";
	private String e971 = "E971";
		/* FORMATS */
	private String rasarec = "RASAREC";
	private Cktrtyprec cktrtyprec = new Cktrtyprec();
	private Glkeyrec glkeyrec = new Glkeyrec();
	private Jrnledrec jrnledrec = new Jrnledrec();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Vflagcpy vflagcpy = new Vflagcpy();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit090,
		exit590,
		exit690,
		endGlkey1070,
		exit1090
	}

	public Pp008edlr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		jrnledrec.jrnledRec = convertAndSetParam(jrnledrec.jrnledRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		jrnledrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaProg);
		if (isNE(jrnledrec.function,"CHECK")) {
			jrnledrec.statuz.set(e049);
			goTo(GotoLabel.exit090);
		}
		checkDissection1000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void syserr500()
	{
		try {
			para500();
		}
		catch (GOTOException e){
		}
		finally{
			exit590();
		}
	}

protected void para500()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit590()
	{
		jrnledrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError600()
	{
		try {
			para600();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void para600()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		jrnledrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void checkDissection1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
					checkKey1060();
				}
				case endGlkey1070: {
					endGlkey1070();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		wsaaTrankey.set(jrnledrec.trankey);
		cktrtyprec.function.set("RETN");
		cktrtyprec.sacstyp.set(jrnledrec.sacstyp);
		cktrtyprec.sacscode.set(jrnledrec.sacscode);
		cktrtyprec.trancode.set(jrnledrec.trancode);
		cktrtyprec.company.set(jrnledrec.company);
		callProgram(Cktrtyp.class, cktrtyprec.cktrtypRec);
		if (isEQ(cktrtyprec.statuz,varcom.bomb)) {
			jrnledrec.statuz.set(cktrtyprec.statuz);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(cktrtyprec.statuz,varcom.oK)) {
			jrnledrec.sacstypErr.set(cktrtyprec.statuz);
		}
		jrnledrec.contot.set(cktrtyprec.contot);
		if (isEQ(jrnledrec.tranamt,0)) {
			jrnledrec.tranamtErr.set(e199);
		}
	}

protected void checkKey1060()
	{
		if (isEQ(wsaaTranEntity,SPACES)) {
			jrnledrec.trankeyErr.set(e194);
			goTo(GotoLabel.exit1090);
		}
		wsaaTranCompany.set(jrnledrec.company);
		jrnledrec.trankey.set(wsaaTrankey);
		rasaIO.setRascoy(wsaaTranCompany);
		rasaIO.setRasnum(wsaaTranEntity);
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)
		&& isNE(rasaIO.getStatuz(),varcom.endp)) {
			syserrrec.dbparams.set(rasaIO.getParams());
			dbError600();
		}
		if ((isNE(rasaIO.getRascoy(),wsaaTranCompany))
		|| (isNE(rasaIO.getRasnum(),wsaaTranEntity))) {
			rasaIO.setStatuz(varcom.endp);
		}
		if (isEQ(rasaIO.getStatuz(),varcom.endp)) {
			jrnledrec.trankeyErr.set(e946);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(rasaIO.getValidflag(),vflagcpy.inForceVal)
		&& isNE(rasaIO.getValidflag(),vflagcpy.propVal)) {
			jrnledrec.trankeyErr.set(e971);
			goTo(GotoLabel.exit1090);
		}
		glkeyrec.function.set("RETN");
		glkeyrec.company.set(jrnledrec.company);
		glkeyrec.trancode.set(jrnledrec.trancode);
		glkeyrec.sacscode.set(jrnledrec.sacscode);
		glkeyrec.sacstyp.set(jrnledrec.sacstyp);
		glkeyrec.ledgkey.set(wsaaTrankey);
		callGlkey2000();
		if (isEQ(glkeyrec.statuz,varcom.oK)) {
			goTo(GotoLabel.endGlkey1070);
		}
		if (isEQ(glkeyrec.statuz,"NFND")) {
			jrnledrec.sacstypErr.set(e196);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(glkeyrec.statuz,"MRNF")) {
			jrnledrec.sacscodeErr.set(e192);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(glkeyrec.statuz,"NCUR")) {
			jrnledrec.sacscodeErr.set(e193);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(glkeyrec.statuz,"BCTL")) {
			jrnledrec.sacstypErr.set(e289);
			goTo(GotoLabel.exit1090);
		}
	}

protected void endGlkey1070()
	{
		jrnledrec.genlPrefix.set(glkeyrec.genlPrefix);
		jrnledrec.genlCompany.set(glkeyrec.genlCompany);
		jrnledrec.genlCurrency.set(jrnledrec.disscurr);
		jrnledrec.genlAccount.set(glkeyrec.genlAccount);
		jrnledrec.sign.set(glkeyrec.sign);
	}

protected void callGlkey2000()
	{
		/*PARA*/
		callProgram(Glkey.class, glkeyrec.glkeyRec);
		if (isEQ(glkeyrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(glkeyrec.statuz);
			syserrrec.params.set(glkeyrec.glkeyRec);
			syserr500();
		}
		/*EXIT*/
	}
}
