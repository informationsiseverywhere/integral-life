/*
 * File: Rasaltr.java
 * Date: 30 August 2009 2:03:19
 * Author: Quipoz Limited
 * 
 * Class transformed from RASALTR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.reassurance.recordstructures.Rasaltrrec;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.T5472rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Reassurance Alterations Cession Calculaton Subroutine.
*
* Overview
* ========
*
* This Subroutine  will be called from Component  Changes
* and  also  from the  Automatic  Increases  Batch Job. It
* will support two  different Functions, "ALTR" and "INCR"
* to distinguish  between  the two  different streams.  If
* the Function is ALTR, it will read  COVT Records for new
* coverage information. If it is "INCR", it will read INCR
* Records.
*
* Processing
* ==========
*
* Read COVR/INCR Records to find the Changed Components.
*
* For each COVT Record, read the corresponding COVR Record.
* If no COVR Record is found, the old Sum Insured is zero.
*
* Find the difference in the two Sums Assured.
*
* Read T5448 Reassurance Method to discern whether Proportionate
* processing is applicable.
*
* Check whether the Particular Transaction code is eligable
* for Proportionate Processing by reading the allowable
* transaction codes table.
*
* If there has been an increase in Sums Assured, and it is
* not Proportionate Processing, set up a flag and call
* CSNCALC to handle it later after processing all the COVT
* Records.
*
* If there has been a decrease in Sums Assured, amd non-
* Proportionate Processing, read the existing Cessions
* (RACD) in reverse order and reduce the Reassured amount
* to cater for the decrease in sums assured completely.
* This may result in either reducing the existing Reassurance
* amount to zero or creating a new amount. If for a RACD,
* the new amount is zero, call TRMRACD Subroutine with a
* function of 'TRMR'. If the new amount is not Zero call the
* TRMRACD Subroutine with 'CHGR' as a function.
*
* If proportionate processing is applicable, whether for
* Increases or decreases, Read all RACD Records, amd for
* each of them call TRMRACD with a Function of 'CHGR'.
*
* Linkage Area
* ============
*
* FUNCTION     PIC X(05)
* STATUZ       PIC X(04)
* CHDRCOY      PIC X(01).
* CHDRNUM      PIC X(08).
* LIFE         PIC X(02).
* JLIFE        PIC X(02).
* COVERAGE     PIC X(02).
* RIDER        PIC X(02).
* PLNSFX       PIC S9(04) COMP-3.
* POLSUM       PIC S9(04) COMP-3.
* EFFDATE      PIC S9(08) COMP-3.
* BATCKEY      PIC X(18).
* TRANNO       PIC S9(05) COMP-3.
* L1-CLNTNUM   PIC X(08).
* L2-CLNTNUM   PIC X(08).
* FSUCO        PIC X(01).
* CNTTYPE      PIC X(03).
* CNTCURR      PIC X(03).
* CRTABLE      PIC X(04).
* OLD-SUMINS   PIC S9(11)V99 COMP-3.
* NEW-SUMINS   PIC S9(11)V99 COMP-3.
* ADD-NEW-COVR PIC X(01).
* LANGUAGE     PIC X(01).
*
* These fields are all contained within RASALTRREC.
*
*****************************************************************
* </pre>
*/
public class Rasaltr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RASALTR";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaIncrNonProp = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOldRaAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaReducedSa = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaCurrLrkcls = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTempReducedSa = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaDifference = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPropDifference = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5472Key = new FixedLengthStringData(8);
	private String wsaaAutoIncrease = "N";
	private PackedDecimalData wsaaCessAmt = new PackedDecimalData(13, 2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaNoRacdRec = new FixedLengthStringData(1);
	private Validator noRacdRec = new Validator(wsaaNoRacdRec, "Y");
	private static final String covrmjarec = "COVRMJAREC";
	private static final String racdmjarec = "RACDMJAREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5446 = "T5446";
	private static final String t5447 = "T5447";
	private static final String t5448 = "T5448";
	private static final String t5458 = "T5458";
	private static final String t5472 = "T5472";
	private static final String t6598 = "T6598";
	private static final String th618 = "TH618";
		/* ERRORS */
	private static final String e049 = "E049";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private T5472rec t5472rec = new T5472rec();
	private T6598rec t6598rec = new T6598rec();
	private Th618rec th618rec = new Th618rec();
	private Trmracdrec trmracdrec = new Trmracdrec();
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Rasaltrrec rasaltrrec = new Rasaltrrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		proportionateCheck750, 
		exit799, 
		exit439
	}

	public Rasaltr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rasaltrrec.rasaltrRec = convertAndSetParam(rasaltrrec.rasaltrRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		mainlineStart101();
		exit199();
	}

protected void mainlineStart101()
	{
		rasaltrrec.statuz.set(varcom.oK);
		wsaaBatckey.set(rasaltrrec.batckey);
		if (isNE(rasaltrrec.function,"ALTR")
		&& isNE(rasaltrrec.function,"INCR")) {
			rasaltrrec.statuz.set(e049);
			syserr570();
		}
		/* Check the product reassurance bypass table, T5447. If the       */
		/* product is present on the table then reasssurance processing    */
		/* is not required - exit program.                                 */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rasaltrrec.chdrcoy);
		itemIO.setItemtabl(t5447);
		itemIO.setItemitem(rasaltrrec.cnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		wsaaNoRacdRec.set("N");
		if (isEQ(rasaltrrec.function,"ALTR")) {
			processCoverage200();
		}
		else {
			storeChdr250();
			processAutoIncreases300();
			rlseChdr350();
		}
	}

protected void exit199()
	{
		exitProgram();
	}

protected void processCoverage200()
	{
		covr201();
	}

protected void covr201()
	{
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(rasaltrrec.chdrcoy);
		covrmjaIO.setChdrnum(rasaltrrec.chdrnum);
		covrmjaIO.setLife(rasaltrrec.life);
		covrmjaIO.setCoverage(rasaltrrec.coverage);
		covrmjaIO.setRider(rasaltrrec.rider);
		covrmjaIO.setPlanSuffix(rasaltrrec.planSuffix);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			dbError580();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.mrnf)) {
			wsaaOldSumins.set(ZERO);
		}
		else {
			wsaaOldSumins.set(covrmjaIO.getSumins());
			rasaltrrec.crrcd.set(covrmjaIO.getCrrcd());
		}
		compute(wsaaSaDifference, 2).set(sub(wsaaOldSumins,rasaltrrec.newSumins));
		if (isNE(wsaaOldSumins,rasaltrrec.newSumins)) {
			additionalProcessing700();
		}
	}

protected void storeChdr250()
	{
		/*CHDR*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(rasaltrrec.chdrcoy);
		chdrlnbIO.setChdrnum(rasaltrrec.chdrnum);
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void processAutoIncreases300()
	{
		/*INCR*/
		/* If we are dealing with Automatic Increases where a new*/
		/* Coverage is created, then for Reassurance, it will be*/
		/* necessary to Reassure the difference. This is done by*/
		/* Calling CSNCALC for the difference.*/
		/* If on the other hand, we are dealing with the scenario*/
		/* where the Sums Assured on the existing Coverages are*/
		/* simply increased, we proceed as if for an increase using*/
		/* Component Modify. When Calling CSNCALC as a part of*/
		/* Automatic Increases, a function of 'COVR' must always*/
		/* be used.*/
		/* It must be noted that the following assumption has been*/
		/* made:*/
		/*     If we are dealing with an "Add to existing Coverage",*/
		/*     for Reassurance, the Increase will ALWAYS be Proportional*/
		/*     and if dealing with an "Add new Coverage" Scenario, for*/
		/*     Reassurance, the Increase will ALWAYS be Non-Proportional*/
		if (isEQ(rasaltrrec.addNewCovr,"Y")) {
			csncalcrec.function.set("COVR");
			compute(csncalcrec.incrAmt, 2).set(sub(rasaltrrec.newSumins,rasaltrrec.oldSumins));
			callCsncalc500();
		}
		else {
			wsaaOldSumins.set(rasaltrrec.oldSumins);
			wsaaAutoIncrease = "Y";
			compute(wsaaSaDifference, 2).set(sub(wsaaOldSumins, rasaltrrec.newSumins));
			additionalProcessing700();
		}
		/*EXIT*/
	}

protected void rlseChdr350()
	{
		/*CHDR*/
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void additionalProcessing700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					tableReads701();
				case proportionateCheck750: 
					proportionateCheck750();
				case exit799: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void tableReads701()
	{
		/* Read T5448, the Reassurance Method Table, to find the*/
		/* Reserve Method, if one exists.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rasaltrrec.chdrcoy);
		itdmIO.setItemtabl(t5448);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(rasaltrrec.cnttype);
		stringVariable1.addExpression(rasaltrrec.crtable);
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setItemitem(wsaaT5448Item);
		/* MOVE RSLT-EFFDATE           TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(rasaltrrec.crrcd);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(wsaaT5448Item,itdmIO.getItemitem())
		|| isNE(rasaltrrec.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*     MOVE ENDP               TO ITDM-STATUZ                   */
			goTo(GotoLabel.exit799);
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
		wsaaBatckey.set(rasaltrrec.batckey);
		if (isEQ(t5448rec.rclmbas, SPACES)) {
			goTo(GotoLabel.proportionateCheck750);
		}
		/* Read of T5472, the Reassurance Claim Basis Table,*/
		/* to give various Calculation Routines.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rasaltrrec.chdrcoy);
		itemIO.setItemtabl(t5472);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(t5448rec.rclmbas);
		stringVariable2.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable2.setStringInto(wsaaT5472Key);
		itemIO.setItemitem(wsaaT5472Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		t5472rec.t5472Rec.set(itemIO.getGenarea());
		/* Read of T6598 with Reserve Method from the Reassurance*/
		/* Method Table, to get Refund Routine, if one exists.*/
		if (isEQ(t5448rec.rresmeth,SPACES)) {
			goTo(GotoLabel.proportionateCheck750);
			/*****      GO TO 450-PROPORTIONATE-CHECK                           */
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rasaltrrec.chdrcoy);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5448rec.rresmeth);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*450-PROPORTIONATE-CHECK.                                         
	* </pre>
	*/
protected void proportionateCheck750()
	{
		if (isNE(t5448rec.proppr,SPACES)) {
			readT5458410();
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				t5448rec.proppr.set(SPACES);
			}
		}
		wsaaReducedSa.set(ZERO);
		if ((isNE(t5448rec.proppr,SPACES)
		|| isEQ(wsaaAutoIncrease,"Y"))
		&& isNE(wsaaOldSumins,ZERO)) {
			processProportionate420();
		}
		else {
			if (isGT(wsaaOldSumins,rasaltrrec.newSumins)) {
				compute(wsaaReducedSa, 2).set(sub(wsaaOldSumins,rasaltrrec.newSumins));
				processDecreaseNonProp430();
			}
			else {
				compute(csncalcrec.incrAmt, 2).set(sub(rasaltrrec.newSumins,wsaaOldSumins));
				csncalcrec.function.set("INCR");
				callCsncalc500();
			}
		}
	}

protected void readT5458410()
	{
		tableRead411();
	}

protected void tableRead411()
	{
		/* Check whether the particular transaction is allowed by reading*/
		/* Table T5458. If returned Statuz is MRNF, spaces are moved to*/
		/* T5448-PROPPR.*/
		wsaaBatckey.set(rasaltrrec.batckey);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rasaltrrec.chdrcoy);
		itemIO.setItemtabl(t5458);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
	}

protected void processProportionate420()
	{
		prop421();
	}

protected void prop421()
	{
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setCurrfrom(ZERO);
		racdmjaIO.setChdrcoy(rasaltrrec.chdrcoy);
		racdmjaIO.setChdrnum(rasaltrrec.chdrnum);
		racdmjaIO.setLife(rasaltrrec.life);
		racdmjaIO.setCoverage(rasaltrrec.coverage);
		racdmjaIO.setRider(rasaltrrec.rider);
		racdmjaIO.setPlanSuffix(rasaltrrec.planSuffix);
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(racdmjarec);
		racdmjaIO.setFunction(varcom.begn);
		wsaaCessAmt.set(ZERO);
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			readThroughRacdProp425();
		}
		
		/*    IF  WSAA-CESS-AMT           < WSAA-SA-DIFFERENCE             */
		/*    AND WSAA-SA-DIFFERENCE      NOT = 0                          */
		/*        PERFORM 450-EXPERIENCE-UPDATE                            */
		/*    END-IF.                                                      */
		if (noRacdRec.isTrue()
		&& isNE(wsaaTempReducedSa, 0)) {
			coExperienceUpdate480();
		}
	}

protected void readThroughRacdProp425()
	{
			propIo426();
		}

protected void propIo426()
	{
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)
		&& isNE(racdmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdmjaIO.getParams());
			syserrrec.statuz.set(racdmjaIO.getStatuz());
			dbError580();
		}
		if (isNE(racdmjaIO.getChdrcoy(),rasaltrrec.chdrcoy)
		|| isNE(racdmjaIO.getChdrnum(),rasaltrrec.chdrnum)
		|| isNE(racdmjaIO.getLife(),rasaltrrec.life)
		|| isNE(racdmjaIO.getCoverage(),rasaltrrec.coverage)
		|| isNE(racdmjaIO.getRider(),rasaltrrec.rider)
		|| isNE(racdmjaIO.getPlanSuffix(),rasaltrrec.planSuffix)
		|| isEQ(racdmjaIO.getStatuz(),varcom.endp)) {
			racdmjaIO.setStatuz(varcom.endp);
			if (isEQ(racdmjaIO.getFunction(), varcom.nextr)) {
				wsaaNoRacdRec.set("Y");
		}
			return ;
		}
		/* RSLT-STATUZ is the trigger for the display of the next*/
		/* Screen in the Program Stack. The Screen should only be*/
		/* displayed for Facultative Reassurance where an increase*/
		/* has occured.*/
		if (isEQ(racdmjaIO.getRetype(),"F")
		&& isGT(rasaltrrec.newSumins,wsaaOldSumins)) {
			rasaltrrec.statuz.set("FACL");
		}
		compute(wsaaReducedSa, 2).set(sub(wsaaOldSumins,rasaltrrec.newSumins));
		compute(wsaaPropDifference, 2).set(div(mult(racdmjaIO.getReasper(),wsaaReducedSa),100));
		compute(rcorfndrec.refund, 2).set(div(wsaaPropDifference,racdmjaIO.getRaAmount()));
		if (isNE(t5472rec.addprocess,SPACES)) {
			rcorfndrec.newSumins.set(sub(racdmjaIO.getRaAmount(),wsaaPropDifference));
			compute(rcorfndrec.refund, 2).set(div(wsaaPropDifference,racdmjaIO.getRaAmount()));
			refundSubr600();
		}
		compute(trmracdrec.newRaAmt, 2).set(sub(racdmjaIO.getRaAmount(),wsaaPropDifference));
		wsaaCessAmt.add(wsaaPropDifference);
		trmracdrec.statuz.set(varcom.oK);
		trmracdrec.function.set("CHGR");
		trmracdrec.chdrcoy.set(rasaltrrec.chdrcoy);
		trmracdrec.chdrnum.set(rasaltrrec.chdrnum);
		trmracdrec.life.set(racdmjaIO.getLife());
		trmracdrec.coverage.set(racdmjaIO.getCoverage());
		trmracdrec.rider.set(racdmjaIO.getRider());
		trmracdrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		trmracdrec.seqno.set(racdmjaIO.getSeqno());
		trmracdrec.lrkcls.set(racdmjaIO.getLrkcls());
		trmracdrec.l1Clntnum.set(rasaltrrec.l1Clntnum);
		trmracdrec.l2Clntnum.set(rasaltrrec.l2Clntnum);
		trmracdrec.clntcoy.set(rasaltrrec.fsuco);
		trmracdrec.effdate.set(rasaltrrec.effdate);
		trmracdrec.tranno.set(rasaltrrec.tranno);
		trmracdrec.crtable.set(rasaltrrec.crtable);
		trmracdrec.jlife.set(rasaltrrec.jlife);
		trmracdrec.cnttype.set(rasaltrrec.cnttype);
		trmracdrec.recovamt.set(ZERO);
		trmracdrec.crrcd.set(rasaltrrec.crrcd);
		trmracdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Trmracd.class, trmracdrec.trmracdRec);
		if (isNE(trmracdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(trmracdrec.statuz);
			syserrrec.params.set(trmracdrec.trmracdRec);
			syserr570();
		}
		if (isLT(wsaaCessAmt,wsaaSaDifference)
		&& isNE(wsaaSaDifference,0)) {
			experienceUpdate450();
		}
		racdmjaIO.setFunction(varcom.nextr);
	}

protected void processDecreaseNonProp430()
	{
		nonPropDec431();
	}

protected void nonPropDec431()
	{
		/*    MOVE SPACES                 TO RACDMJA-DATA-AREA.            */
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setSeqno(99);
		racdmjaIO.setChdrcoy(rasaltrrec.chdrcoy);
		racdmjaIO.setChdrnum(rasaltrrec.chdrnum);
		racdmjaIO.setLife(rasaltrrec.life);
		racdmjaIO.setCoverage(rasaltrrec.coverage);
		racdmjaIO.setRider(rasaltrrec.rider);
		racdmjaIO.setPlanSuffix(rasaltrrec.planSuffix);
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(racdmjarec);
		racdmjaIO.setFunction(varcom.begn);
		wsaaCessAmt.set(ZERO);
		wsaaTempReducedSa.set(wsaaReducedSa);
		wsaaCurrLrkcls.set(SPACES);
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			readThroughRacdsNprop435();
		}
		
		/*                             UNTIL RACDMJA-STATUZ = ENDP      */
		/*                             OR WSAA-REDUCED-SA = 0.          */
		/*    IF  WSAA-CESS-AMT           < WSAA-SA-DIFFERENCE             */
		/*    AND WSAA-SA-DIFFERENCE      NOT = 0                          */
		/*        PERFORM 450-EXPERIENCE-UPDATE                            */
		/*    END-IF.                                                      */
		if (noRacdRec.isTrue()
		&& isNE(wsaaTempReducedSa,0)) {
			coExperienceUpdate480();
		}
	}

protected void readThroughRacdsNprop435()
	{
		try {
			nonpropio436();
			trmracd438();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nonpropio436()
	{
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)
		&& isNE(racdmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdmjaIO.getParams());
			dbError580();
		}
		if (isNE(racdmjaIO.getChdrcoy(),rasaltrrec.chdrcoy)
		|| isNE(racdmjaIO.getChdrnum(),rasaltrrec.chdrnum)
		|| isNE(racdmjaIO.getLife(),rasaltrrec.life)
		|| isNE(racdmjaIO.getCoverage(),rasaltrrec.coverage)
		|| isNE(racdmjaIO.getRider(),rasaltrrec.rider)
		|| isNE(racdmjaIO.getPlanSuffix(),rasaltrrec.planSuffix)
		|| isEQ(racdmjaIO.getStatuz(),varcom.endp)) {
			racdmjaIO.setStatuz(varcom.endp);
			/*     MOVE ZEROES             TO WSAA-REDUCED-SA       <LA3429>*/
			if (isEQ(racdmjaIO.getFunction(), varcom.nextr)) {
				wsaaNoRacdRec.set("Y");
			}
			goTo(GotoLabel.exit439);
		}
		/* Risk Class changes. Reset and process the RA amount again.   */
		if (isNE(racdmjaIO.getLrkcls(),wsaaCurrLrkcls)) {
			wsaaTempReducedSa.set(wsaaReducedSa);
			wsaaCurrLrkcls.set(racdmjaIO.getLrkcls());
			wsaaCessAmt.set(ZERO);
		}
		else {
			/* If still same Risk Class and the amount to processed is      */
			/* zero, skip to the next RACD records.                         */
			if (isEQ(wsaaTempReducedSa,ZERO)) {
				goTo(GotoLabel.exit439);
			}
		}
		
		/* IF RACDMJA-RA-AMOUNT        <= WSAA-REDUCED-SA               */
		if (isLTE(racdmjaIO.getRaAmount(),wsaaTempReducedSa)) {
			trmracdrec.newRaAmt.set(ZERO);
			rcorfndrec.refund.set(1);
			trmracdrec.function.set("TRMR");
			/*     COMPUTE WSAA-REDUCED-SA = WSAA-REDUCED-SA -              */
			compute(wsaaTempReducedSa, 2).set(sub(wsaaTempReducedSa,racdmjaIO.getRaAmount()));
			wsaaCessAmt.add(racdmjaIO.getRaAmount());
			if (isNE(t5472rec.addprocess,SPACES)) {
				rcorfndrec.newSumins.set(0);
				refundSubr600();
			}
		}
		else {
			compute(trmracdrec.newRaAmt, 2).set(sub(racdmjaIO.getRaAmount(),wsaaTempReducedSa));
			/*                               WSAA-REDUCED-SA                */
			trmracdrec.function.set("CHGR");
			wsaaCessAmt.add(wsaaTempReducedSa);
			/*     ADD WSAA-REDUCED-SA     TO WSAA-CESS-AMT                 */
			if (isNE(t5472rec.addprocess,SPACES)) {
				/*         COMPUTE RCRF-REFUND  = WSAA-REDUCED-SA /             */
				rcorfndrec.newSumins.set(trmracdrec.newRaAmt);
				compute(rcorfndrec.refund, 2).set(div(wsaaTempReducedSa,racdmjaIO.getRaAmount()));
				refundSubr600();
			}
			wsaaTempReducedSa.set(ZERO);
			/**        MOVE ZEROES             TO WSAA-REDUCED-SA       <LA3429>*/
		}
	}

protected void trmracd438()
	{
		trmracdrec.statuz.set(varcom.oK);
		trmracdrec.chdrcoy.set(rasaltrrec.chdrcoy);
		trmracdrec.chdrnum.set(rasaltrrec.chdrnum);
		trmracdrec.life.set(racdmjaIO.getLife());
		trmracdrec.coverage.set(racdmjaIO.getCoverage());
		trmracdrec.rider.set(racdmjaIO.getRider());
		trmracdrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		trmracdrec.seqno.set(racdmjaIO.getSeqno());
		trmracdrec.lrkcls.set(racdmjaIO.getLrkcls());
		trmracdrec.l1Clntnum.set(rasaltrrec.l1Clntnum);
		trmracdrec.l2Clntnum.set(rasaltrrec.l2Clntnum);
		trmracdrec.clntcoy.set(rasaltrrec.fsuco);
		trmracdrec.effdate.set(rasaltrrec.effdate);
		trmracdrec.tranno.set(rasaltrrec.tranno);
		trmracdrec.crtable.set(rasaltrrec.crtable);
		trmracdrec.jlife.set(rasaltrrec.jlife);
		trmracdrec.cnttype.set(rasaltrrec.cnttype);
		trmracdrec.recovamt.set(ZERO);
		trmracdrec.crrcd.set(rasaltrrec.crrcd);
		trmracdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Trmracd.class, trmracdrec.trmracdRec);
		if (isNE(trmracdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(trmracdrec.statuz);
			syserrrec.params.set(trmracdrec.trmracdRec);
			syserr570();
		}
		if (isLT(wsaaCessAmt,wsaaSaDifference)
		&& isNE(wsaaSaDifference,0)) {
			experienceUpdate450();
		}
		racdmjaIO.setFunction(varcom.nextr);
	}

protected void experienceUpdate450()
	{
		update451();
	}

protected void update451()
	{
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(rasaltrrec.chdrcoy);
		rexpupdrec.chdrnum.set(rasaltrrec.chdrnum);
		rexpupdrec.life.set(rasaltrrec.life);
		rexpupdrec.coverage.set(rasaltrrec.coverage);
		rexpupdrec.rider.set(rasaltrrec.rider);
		rexpupdrec.planSuffix.set(rasaltrrec.planSuffix);
		rexpupdrec.tranno.set(rasaltrrec.tranno);
		rexpupdrec.clntcoy.set(rasaltrrec.fsuco);
		rexpupdrec.l1Clntnum.set(rasaltrrec.l1Clntnum);
		if (isNE(rasaltrrec.jlife,"00")) {
			rexpupdrec.l2Clntnum.set(rasaltrrec.l2Clntnum);
		}
		compute(rexpupdrec.sumins, 2).set(sub(wsaaSaDifference,wsaaCessAmt));
		readT5446460();
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(rasaltrrec.effdate);
		/*    MOVE T5448-LRKCLS           TO RXUP-RISK-CLASS.              */
		rexpupdrec.riskClass.set(racdmjaIO.getLrkcls());
		rexpupdrec.reassurer.set(racdmjaIO.getRasnum());
		rexpupdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		if (isNE(rexpupdrec.currency,rasaltrrec.cntcurr)) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(rasaltrrec.cntcurr);
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt470();
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		rexpupdrec.function.set("DECR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			syserr570();
		}
	}

protected void readT5446460()
	{
		t5446461();
	}

protected void t5446461()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rasaltrrec.chdrcoy);
		itdmIO.setItemtabl(t5446);
		/*    MOVE T5448-LRKCLS           TO ITDM-ITEMITEM.                */
		itdmIO.setItemitem(racdmjaIO.getLrkcls());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		/*    IF ITDM-ITEMITEM            NOT = T5448-LRKCLS               */
		if (isNE(itdmIO.getItemitem(),racdmjaIO.getLrkcls())
		|| isNE(itdmIO.getItemcoy(),rasaltrrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5446)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*        MOVE T5448-LRKCLS       TO ITDM-ITEMITEM                 */
			itdmIO.setItemitem(racdmjaIO.getLrkcls());
			itdmIO.setItemcoy(rasaltrrec.chdrcoy);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callXcvrt470()
	{
		/*CALL*/
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(rasaltrrec.effdate);
		conlinkrec.company.set(rasaltrrec.chdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr570();
		}
		/*EXIT*/
	}

protected void coExperienceUpdate480()
	{
		update481();
	}

protected void update481()
	{
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(rasaltrrec.chdrcoy);
		rexpupdrec.chdrnum.set(rasaltrrec.chdrnum);
		rexpupdrec.life.set(rasaltrrec.life);
		rexpupdrec.coverage.set(rasaltrrec.coverage);
		rexpupdrec.rider.set(rasaltrrec.rider);
		rexpupdrec.planSuffix.set(rasaltrrec.planSuffix);
		rexpupdrec.tranno.set(rasaltrrec.tranno);
		rexpupdrec.clntcoy.set(rasaltrrec.fsuco);
		rexpupdrec.l1Clntnum.set(rasaltrrec.l1Clntnum);
		if (isNE(rasaltrrec.jlife, "00")) {
			rexpupdrec.l2Clntnum.set(rasaltrrec.l2Clntnum);
		}
		rexpupdrec.sumins.set(wsaaTempReducedSa);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rasaltrrec.chdrcoy);
		itemIO.setItemtabl(th618);
		itemIO.setItemitem(t5448rec.rrsktyp);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError580();
		}
		th618rec.th618Rec.set(itemIO.getGenarea());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 5)); wsaaSub.add(1)){
			if (isNE(th618rec.lrkcls[wsaaSub.toInt()], SPACES)) {
				racdmjaIO.setLrkcls(th618rec.lrkcls[wsaaSub.toInt()]);
				readT5446460();
				rexpupdrec.currency.set(t5446rec.currcode);
				rexpupdrec.effdate.set(rasaltrrec.effdate);
				rexpupdrec.riskClass.set(racdmjaIO.getLrkcls());
				if (isNE(rexpupdrec.currency, rasaltrrec.cntcurr)) {
					conlinkrec.amountIn.set(rexpupdrec.sumins);
					conlinkrec.currIn.set(rasaltrrec.cntcurr);
					conlinkrec.currOut.set(rexpupdrec.currency);
					callXcvrt470();
					rexpupdrec.sumins.set(conlinkrec.amountOut);
				}
				rexpupdrec.function.set("DECR");
				callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
				if (isNE(rexpupdrec.statuz, varcom.oK)) {
					syserrrec.statuz.set(rexpupdrec.statuz);
					syserrrec.params.set(rexpupdrec.rexpupdRec);
					syserr570();
				}
			}
		}
	}

protected void callCsncalc500()
	{
		cession501();
	}

protected void cession501()
	{
		csncalcrec.statuz.set(varcom.oK);
		csncalcrec.chdrcoy.set(rasaltrrec.chdrcoy);
		csncalcrec.chdrnum.set(rasaltrrec.chdrnum);
		csncalcrec.life.set(rasaltrrec.life);
		csncalcrec.coverage.set(rasaltrrec.coverage);
		csncalcrec.rider.set(rasaltrrec.rider);
		csncalcrec.planSuffix.set(rasaltrrec.planSuffix);
		csncalcrec.currency.set(rasaltrrec.cntcurr);
		csncalcrec.cnttype.set(rasaltrrec.cnttype);
		csncalcrec.fsuco.set(rasaltrrec.fsuco);
		csncalcrec.effdate.set(rasaltrrec.effdate);
		csncalcrec.tranno.set(rasaltrrec.tranno);
		csncalcrec.language.set(rasaltrrec.language);
		csncalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Csncalc.class, csncalcrec.csncalcRec);
		if (isNE(csncalcrec.statuz,varcom.oK)
		&& isNE(csncalcrec.statuz,"FACL")) {
			syserrrec.statuz.set(csncalcrec.statuz);
			syserrrec.params.set(csncalcrec.csncalcRec);
			syserr570();
		}
		rasaltrrec.statuz.set(csncalcrec.statuz);
	}

protected void refundSubr600()
	{
		refund601();
	}

protected void refund601()
	{
		rcorfndrec.statuz.set(varcom.oK);
		rcorfndrec.function.set("UPDT");
		rcorfndrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		rcorfndrec.chdrnum.set(racdmjaIO.getChdrnum());
		rcorfndrec.life.set(racdmjaIO.getLife());
		rcorfndrec.coverage.set(racdmjaIO.getCoverage());
		rcorfndrec.rider.set(racdmjaIO.getRider());
		rcorfndrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		rcorfndrec.seqno.set(racdmjaIO.getSeqno());
		rcorfndrec.currfrom.set(racdmjaIO.getCurrfrom());
		rcorfndrec.tranno.set(rasaltrrec.tranno);
		rcorfndrec.polsum.set(rasaltrrec.polsum);
		rcorfndrec.batckey.set(rasaltrrec.batckey);
		rcorfndrec.language.set(rasaltrrec.language);
		rcorfndrec.effdate.set(rasaltrrec.effdate);
		rcorfndrec.crtable.set(rasaltrrec.crtable);
		rcorfndrec.cnttype.set(rasaltrrec.cnttype);
		callProgram(t5472rec.addprocess, rcorfndrec.rcorfndRec);
		if (isNE(rcorfndrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rcorfndrec.statuz);
			syserrrec.params.set(rcorfndrec.rcorfndRec);
			syserr570();
		}
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rasaltrrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rasaltrrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
