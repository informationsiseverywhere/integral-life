package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5470.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5470Report extends SMARTReportLayout { 

	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length); //Starts ILIFE-3212
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length); //End ILIFE-3212
	private FixedLengthStringData chdrno = new FixedLengthStringData(8);
	private FixedLengthStringData cltname = new FixedLengthStringData(47);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData dtldesc = new FixedLengthStringData(30);
	private FixedLengthStringData effcdte = new FixedLengthStringData(10);
	private FixedLengthStringData forattn = new FixedLengthStringData(30);
	private ZonedDecimalData newamnt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData orgamnt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData origcur = new FixedLengthStringData(3);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData pcode = new FixedLengthStringData(10);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private FixedLengthStringData statcurr = new FixedLengthStringData(3);
	private FixedLengthStringData stdat = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totcbal = new ZonedDecimalData(18, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5470Report() {
		super();
	}


	/**
	 * Print the XML for R5470d01
	 */
	public void printR5470d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		effcdte.setFieldName("effcdte");
		effcdte.setInternal(subString(recordData, 1, 10));
		dtldesc.setFieldName("dtldesc");
		dtldesc.setInternal(subString(recordData, 11, 30));
		chdrno.setFieldName("chdrno");
		chdrno.setInternal(subString(recordData, 41, 8));
		orgamnt.setFieldName("orgamnt");
		orgamnt.setInternal(subString(recordData, 49, 17));
		origcur.setFieldName("origcur");
		origcur.setInternal(subString(recordData, 66, 3));
		newamnt.setFieldName("newamnt");
		newamnt.setInternal(subString(recordData, 69, 17));
		printLayout("R5470d01",			// Record name
			new BaseData[]{			// Fields:
				effcdte,
				dtldesc,
				chdrno,
				orgamnt,
				origcur,
				newamnt
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5470h01
	 */
	public void printR5470h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stdat.setFieldName("stdat");
		stdat.setInternal(subString(recordData, 1, 10));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 42, 8));
		forattn.setFieldName("forattn");
		forattn.setInternal(subString(recordData, 50, 30));
		statcurr.setFieldName("statcurr");
		statcurr.setInternal(subString(recordData, 80, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 83, 30));
		cltname.setFieldName("cltname");
		cltname.setInternal(subString(recordData, 113, 47));
		addr1.setFieldName("addr1");
		addr1.setInternal(subString(recordData, 160, DD.cltaddr.length)); //Starts ILIFE-3212
		addr2.setFieldName("addr2");
		addr2.setInternal(subString(recordData, 160+DD.cltaddr.length*1, DD.cltaddr.length));
		addr3.setFieldName("addr3");
		addr3.setInternal(subString(recordData, 160+DD.cltaddr.length*2, DD.cltaddr.length));
		addr4.setFieldName("addr4");
		addr4.setInternal(subString(recordData, 160+DD.cltaddr.length*3, DD.cltaddr.length));
		pcode.setFieldName("pcode");
		pcode.setInternal(subString(recordData, 160+DD.cltaddr.length*4, 10)); //End ILIFE-3212
		printLayout("R5470h01",			// Record name
			new BaseData[]{			// Fields:
				stdat,
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr,
				rasnum,
				forattn,
				statcurr,
				currdesc,
				cltname,
				addr1,
				addr2,
				addr3,
				addr4,
				pcode
			}
		);

		currentPrintLine.set(17);
	}

	/**
	 * Print the XML for R5470t01
	 */
	public void printR5470t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		totcbal.setFieldName("totcbal");
		totcbal.setInternal(subString(recordData, 1, 18));
		printLayout("R5470t01",			// Record name
			new BaseData[]{			// Fields:
				totcbal
			}
		);

		currentPrintLine.add(1);
	}


}
