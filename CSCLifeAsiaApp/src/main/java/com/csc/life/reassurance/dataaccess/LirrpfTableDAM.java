package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LirrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:45
 * Class transformed from LIRRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LirrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 113;
	public FixedLengthStringData lirrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData lirrpfRecord = lirrrec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(lirrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(lirrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(lirrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(lirrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(lirrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(lirrrec);
	public FixedLengthStringData clntpfx = DD.clntpfx.copy().isAPartOf(lirrrec);
	public FixedLengthStringData clntcoy = DD.clntcoy.copy().isAPartOf(lirrrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(lirrrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(lirrrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(lirrrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(lirrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(lirrrec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(lirrrec);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(lirrrec);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(lirrrec);
	public PackedDecimalData raAmount = DD.raamount.copy().isAPartOf(lirrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(lirrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(lirrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(lirrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LirrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LirrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LirrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LirrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LirrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LirrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LirrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LIRRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CLNTPFX, " +
							"CLNTCOY, " +
							"CLNTNUM, " +
							"CURRFROM, " +
							"CURRTO, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"RASNUM, " +
							"RNGMNT, " +
							"CURRENCY, " +
							"RAAMOUNT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     clntpfx,
                                     clntcoy,
                                     clntnum,
                                     currfrom,
                                     currto,
                                     validflag,
                                     tranno,
                                     rasnum,
                                     rngmnt,
                                     currency,
                                     raAmount,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		clntpfx.clear();
  		clntcoy.clear();
  		clntnum.clear();
  		currfrom.clear();
  		currto.clear();
  		validflag.clear();
  		tranno.clear();
  		rasnum.clear();
  		rngmnt.clear();
  		currency.clear();
  		raAmount.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLirrrec() {
  		return lirrrec;
	}

	public FixedLengthStringData getLirrpfRecord() {
  		return lirrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLirrrec(what);
	}

	public void setLirrrec(Object what) {
  		this.lirrrec.set(what);
	}

	public void setLirrpfRecord(Object what) {
  		this.lirrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(lirrrec.getLength());
		result.set(lirrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}