package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5460screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {17, 4, 22, 5, 18, 23, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5460screensfl";
		lrec.subfileClass = S5460screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 11;
		lrec.pageSubfile = 10;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 2, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5460ScreenVars sv = (S5460ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5460screenctlWritten, sv.S5460screensflWritten, av, sv.s5460screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5460ScreenVars screenVars = (S5460ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.ssreast.setClassString("");
		screenVars.ssreasf.setClassString("");
	}

/**
 * Clear all the variables in S5460screenctl
 */
	public static void clear(VarModel pv) {
		S5460ScreenVars screenVars = (S5460ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.sumins.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.ssreast.clear();
		screenVars.ssreasf.clear();
	}
}
