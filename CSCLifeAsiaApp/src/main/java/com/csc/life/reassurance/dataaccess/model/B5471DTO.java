package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;

public class B5471DTO {
    /* SQL-acmv*/
    
    private Acmxpf acmxpf;
    private Acmvagt acmvagt;
    // rasapf
    private String sqlRascoy;
    private String sqlRasnum;
    private String sqlCurrcode;
    private String sqlClntcoy;
    private String sqlPayclt;
    private String sqlRapaymth;
    private String sqlBankkey;
    private String sqlBankacckey;
    
    
	public Acmvagt getAcmvagt() {
		return acmvagt;
	}
	public void setAcmvagt(Acmvagt acmvagt) {
		this.acmvagt = acmvagt;
	}
	public Acmxpf getAcmxpf() {
		return acmxpf;
	}
	public void setAcmxpf(Acmxpf acmxpf) {
		this.acmxpf = acmxpf;
	}
	public String getSqlRascoy() {
		return sqlRascoy;
	}
	public void setSqlRascoy(String sqlRascoy) {
		this.sqlRascoy = sqlRascoy;
	}
	public String getSqlRasnum() {
		return sqlRasnum;
	}
	public void setSqlRasnum(String sqlRasnum) {
		this.sqlRasnum = sqlRasnum;
	}
	public String getSqlCurrcode() {
		return sqlCurrcode;
	}
	public void setSqlCurrcode(String sqlCurrcode) {
		this.sqlCurrcode = sqlCurrcode;
	}
	public String getSqlClntcoy() {
		return sqlClntcoy;
	}
	public void setSqlClntcoy(String sqlClntcoy) {
		this.sqlClntcoy = sqlClntcoy;
	}
	public String getSqlPayclt() {
		return sqlPayclt;
	}
	public void setSqlPayclt(String sqlPayclt) {
		this.sqlPayclt = sqlPayclt;
	}
	public String getSqlRapaymth() {
		return sqlRapaymth;
	}
	public void setSqlRapaymth(String sqlRapaymth) {
		this.sqlRapaymth = sqlRapaymth;
	}
	public String getSqlBankkey() {
		return sqlBankkey;
	}
	public void setSqlBankkey(String sqlBankkey) {
		this.sqlBankkey = sqlBankkey;
	}
	public String getSqlBankacckey() {
		return sqlBankacckey;
	}
	public void setSqlBankacckey(String sqlBankacckey) {
		this.sqlBankacckey = sqlBankacckey;
	}
    
   
    
    
}
