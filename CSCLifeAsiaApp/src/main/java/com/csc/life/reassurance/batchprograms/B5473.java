/*
 * File: B5473.java
 * Date: 29 August 2009 21:20:31
 * Author: Quipoz Limited
 * 
 * Class transformed from B5473.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.recordstructures.P5473par;
import com.csc.life.reassurance.reports.R5473Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  B5473 - Claims Paid Report
*  --------------------------
*
*  This program will read the RACD file and select all records
*  that are of validflag = '4', recovamt <> 0 and CURRFROM >=
*  P5473-CURRFRM and CURRFROM <= P5473-CURRTO. SQL is used to
*  retrieve all records that satisfy the selection criteria.
*
*  A Parameter screen is available for users to specify the date
*  range.
*
*  The SQL selection criteria are as follows :
*
*        SELECT :  . RACDPF.RECOVAMT  <> 0              and
*                  . RACDPF.VALIDFLAG = '4'             and
*                  . P5473-DATEFROM  <= RACDPF.CURRFROM and
*                    P5473-DATETO    >= RACDPF.CURRFROM
*
*  Initialise
*     - Define the SQL query by declaring a cursor. The SQL will
*       extract all RACD records that meet the criteria specified
*       above.
*
*  Read
*     - the COVRLNB, LIFELNB, RASA and CLTS file are read
*       to retrieve the required information for printing.
*     - write details to report while not primary file EOF
*     - if new page, write headings
*     - write details
*
*     Read next primary file record
*
*    End Perform
*
*****************************************************************
* </pre>
*/
public class B5473 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlracdpf1rs = null;
	private java.sql.PreparedStatement sqlracdpf1ps = null;
	private java.sql.Connection sqlracdpf1conn = null;
	private String sqlracdpf1 = "";
	private R5473Report printerFile = new R5473Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5473");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private ZonedDecimalData wsaaDatefrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateto = new ZonedDecimalData(8, 0);
	private String descrec = "DESCREC";
	private String ptrnrevrec = "PTRNREVREC";
	private String rasarec = "RASAREC";
	private String cltsrec = "CLTSREC";
	private String covrlnbrec = "COVRLNBREC";
	private String lifelnbrec = "LIFELNBREC";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);
		/* ERRORS */
	private String esql = "ESQL";

		/* SQL-RACDPF */
	private FixedLengthStringData sqlRacdrec = new FixedLengthStringData(47);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlRacdrec, 2);
	private FixedLengthStringData sqlRasnum = new FixedLengthStringData(8).isAPartOf(sqlRacdrec, 10);
	private FixedLengthStringData sqlCurrcode = new FixedLengthStringData(3).isAPartOf(sqlRacdrec, 18);
	private PackedDecimalData sqlRecovamt = new PackedDecimalData(17, 2).isAPartOf(sqlRacdrec, 21);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 30);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 32);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 34);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlRacdrec, 36);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(sqlRacdrec, 39);
	private PackedDecimalData sqlCurrfrom = new PackedDecimalData(8, 0).isAPartOf(sqlRacdrec, 42);
	private String t1688 = "T1688";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8);
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData r5473H01 = new FixedLengthStringData(75);
	private FixedLengthStringData r5473h01O = new FixedLengthStringData(75).isAPartOf(r5473H01, 0);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5473h01O, 0);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37).isAPartOf(r5473h01O, 8);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r5473h01O, 45);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10).isAPartOf(r5473h01O, 55);
	private FixedLengthStringData dateto = new FixedLengthStringData(10).isAPartOf(r5473h01O, 65);

	private FixedLengthStringData r5473D01 = new FixedLengthStringData(127);
	private FixedLengthStringData r5473d01O = new FixedLengthStringData(127).isAPartOf(r5473D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5473d01O, 0);
	private FixedLengthStringData lifcnum = new FixedLengthStringData(8).isAPartOf(r5473d01O, 8);
	private FixedLengthStringData clntnm1 = new FixedLengthStringData(37).isAPartOf(r5473d01O, 16);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5473d01O, 53);
	private FixedLengthStringData crrcd = new FixedLengthStringData(10).isAPartOf(r5473d01O, 57);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(r5473d01O, 67);
	private ZonedDecimalData recovamt = new ZonedDecimalData(17, 2).isAPartOf(r5473d01O, 70);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r5473d01O, 87);
	private FixedLengthStringData ptrneff = new FixedLengthStringData(10).isAPartOf(r5473d01O, 117);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private P5473par p5473par = new P5473par();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		eof2060, 
		exit2090
	}

	public B5473() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1020();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		p5473par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaDatefrm.set(p5473par.datefrm);
		wsaaDateto.set(p5473par.dateto);
		sqlracdpf1 = " SELECT  RACDPF.CHDRCOY, RACDPF.CHDRNUM, RACDPF.RASNUM, RACDPF.CURRCODE, RACDPF.RECOVAMT, RACDPF.LIFE, RACDPF.COVERAGE, RACDPF.RIDER, RACDPF.PLNSFX, RACDPF.TRANNO, RACDPF.CURRFROM" +
" FROM   " + appVars.getTableNameOverriden("RACDPF") + " " +
" WHERE RACDPF.RECOVAMT <> 0" +
" AND RACDPF.VALIDFLAG = '4'" +
" AND RACDPF.CURRFROM >= ?" +
" AND RACDPF.CURRFROM <= ?" +
" ORDER BY RACDPF.RASNUM, RACDPF.CHDRNUM";
		sqlerrorflag = false;
		try {
			sqlracdpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.reassurance.dataaccess.RacdpfTableDAM());
			sqlracdpf1ps = appVars.prepareStatementEmbeded(sqlracdpf1conn, sqlracdpf1, "RACDPF");
			appVars.setDBDouble(sqlracdpf1ps, 1, wsaaDatefrm.toDouble());
			appVars.setDBDouble(sqlracdpf1ps, 2, wsaaDateto.toDouble());
			sqlracdpf1rs = appVars.executeQuery(sqlracdpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		datcon1rec.intDate.set(wsaaDateto);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dateto.set(datcon1rec.extDate);
		datcon1rec.intDate.set(wsaaDatefrm);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datefrm.set(datcon1rec.extDate);
	}

protected void setUpHeadingDates1020()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2060: {
					eof2060();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlracdpf1rs.next()) {
				appVars.getDBObject(sqlracdpf1rs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlracdpf1rs, 2, sqlChdrnum);
				appVars.getDBObject(sqlracdpf1rs, 3, sqlRasnum);
				appVars.getDBObject(sqlracdpf1rs, 4, sqlCurrcode);
				appVars.getDBObject(sqlracdpf1rs, 5, sqlRecovamt);
				appVars.getDBObject(sqlracdpf1rs, 6, sqlLife);
				appVars.getDBObject(sqlracdpf1rs, 7, sqlCoverage);
				appVars.getDBObject(sqlracdpf1rs, 8, sqlRider);
				appVars.getDBObject(sqlracdpf1rs, 9, sqlPlnsfx);
				appVars.getDBObject(sqlracdpf1rs, 10, sqlTranno);
				appVars.getDBObject(sqlracdpf1rs, 11, sqlCurrfrom);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		if (isNE(sqlRasnum,wsaaRasnum)) {
			wsaaOverflow.set("Y");
			reassurerName2600();
			printHeader2700();
		}
		/*EXIT*/
	}

protected void reassurerName2600()
	{
		reassurerName2610();
	}

protected void reassurerName2610()
	{
		rasnum.set(sqlRasnum);
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(bsprIO.getCompany());
		rasaIO.setRasnum(sqlRasnum);
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.endp)
		&& isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		clntnm.set(wsspLongconfname);
	}

protected void printHeader2700()
	{
		/*PRINT-HEADER*/
		printerFile.printR5473h01(r5473H01);
		wsaaOverflow.set("N");
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		chdrnum.set(sqlChdrnum);
		readLife3100();
		readCovr3200();
		currcode.set(sqlCurrcode);
		recovamt.set(sqlRecovamt);
		readTransaction3300();
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ptrnrevIO.getPtrneff());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		ptrneff.set(datcon1rec.extDate);
		printerFile.printR5473d01(r5473D01);
		wsaaRasnum.set(sqlRasnum);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void readLife3100()
	{
		readLife3110();
	}

protected void readLife3110()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(sqlChdrcoy);
		lifelnbIO.setChdrnum(sqlChdrnum);
		lifelnbIO.setLife(sqlLife);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		clntnm1.set(wsspLongconfname);
	}

protected void readCovr3200()
	{
		readCovr3210();
	}

protected void readCovr3210()
	{
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(sqlChdrcoy);
		covrlnbIO.setChdrnum(sqlChdrnum);
		covrlnbIO.setLife(sqlLife);
		covrlnbIO.setCoverage(sqlCoverage);
		covrlnbIO.setRider(sqlRider);
		covrlnbIO.setPlanSuffix(sqlPlnsfx);
		covrlnbIO.setFunction(varcom.readr);
		covrlnbIO.setFormat(covrlnbrec);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		crtable.set(covrlnbIO.getCrtable());
		datcon1rec.intDate.set(covrlnbIO.getCrrcd());
		datcon1rec.function.set(varcom.conv);		//Code added by bkonduru2 ILIFE-3130
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		crrcd.set(datcon1rec.extDate);
	}

protected void readTransaction3300()
	{
		readTransaction3310();
	}

protected void readTransaction3310()
	{
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(sqlChdrcoy);
		ptrnrevIO.setChdrnum(sqlChdrnum);
		ptrnrevIO.setTranno(sqlTranno);
		ptrnrevIO.setFunction(varcom.readr);
		ptrnrevIO.setFormat(ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		longdesc.set(descIO.getLongdesc());
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlracdpf1conn, sqlracdpf1ps, sqlracdpf1rs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
