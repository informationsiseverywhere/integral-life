/*
 * File: Restdty.java
 * Date: 30 August 2009 2:05:12
 * Author: Quipoz Limited
 *
 * Class transformed from RESTDTY.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.productdefinition.dataaccess.ChdrsdcTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrsdcTableDAM;
import com.csc.life.productdefinition.tablestructures.T6650rec;
import com.csc.life.reassurance.recordstructures.Restdtyrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Reassurance Stamp Duty Calculation.
*
* Overview
* ========
*
* This subroutine is called from the reassurance costing batch
* program, B5456, to calculate the amount of stamp duty to be
* paid.
*
* Policy Level Processing
* =======================
*
* Check for the Plan Suffix equal to zero, if it is not equal
* to zero, then process for Stamp Duty as normal.
*
* If the Plan Suffix is equal to zero, then divide the Sum
* Assured by the number of policies summarised (from the
* contract header). The stamp duty calculation is then
* performed and before the Stamp Duty Amount is passed back
* to the calling program, it is multiplied by the number of
* policies in the POLSUM field.
*
* Calculate Stamp Duty
* ====================
*
* Please note that the amount calculated per entry of Sum
* Assured is at the rate per factor or part thereof. That is,
* if the sum assured is for $6500, the amount being calculated
* would regard this as $7000 (i.e. if the factor was 1000).
*
* The Sum Assured may satisfy more than one entry, so all the
* entries are checked until the Sum Assured does not exceed
* one of the limits on the table.
*
* For each entry range that the Sum Assured satisfies, the
* amount in the range must be divided by the factor and
* multiplied by the rate. This amount is stored. The next
* range is checked and the same applies.
*
* It may occur within the range,that the Sum Assured does not    t
* exceed or even match the limit. In this case the factor and
* the rate only apply to the balance of the Sum Assured
* within this range and not to the range limit itself.
*
* For every entry range satisfied, a stamp duty amount is
* calculated. When all the ranges are satisfied the total
* amount accumulated is returned as the stamp duty applicable
* for this currency.
*
* If the Plan Suffix is not equal to zero (see the section on
* policy level above), multiply the Stamp Duty amount by the
* number of policy summerised (POLSUM).
*
* The Stamp Duty is then returned through the linkage.
*
* Linkage
* =======
*
* STATUZ             PIC X(04).
* COMPANY            PIC X(01).
* CHDRNUM            PIC X(08).
* LIFE               PIC X(02).
* COVERAGE           PIC X(02).
* RIDER              PIC X(02).
* PLNSFX             PIC S9(04) COMP-3.
* CNTCURR            PIC X(03).
* CRTABLE            PIC X(04).
* EFFDATE            PIC 9(08).
* SUMASS             PIC S9(11)V9(02).
* PREMIUM            PIC S9(11)V9(02).
* TAX                PIC S9(11)V9(02).
*
* These fields are all contained within RESTDTYREC.
*
*****************************************************************
* </pre>
*/
public class Restdty extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "RESTDTY";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaSumAssured = new PackedDecimalData(11, 0);
	private PackedDecimalData wsaaStampDuty = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRangeDuty = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaRem = new PackedDecimalData(11, 0);
	private PackedDecimalData wsaaFraction = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaRemainder = new PackedDecimalData(2, 0);

	private FixedLengthStringData wsaaEndFlag = new FixedLengthStringData(1);
	private Validator wsaaEndRange = new Validator(wsaaEndFlag, "Y");
		/* TABLES */
	private String t6650 = "T6650";
		/* ERRORS */
	private String r075 = "R075";
		/* FORMATS */
	private String itdmrec = "ITEMREC";
	private String chdrsdcrec = "CHDRSDCREC";
		/*Contract Header - Stamp Duty Calculation*/
	private ChdrsdcTableDAM chdrsdcIO = new ChdrsdcTableDAM();
		/*Coverage/rider details - stamp duty*/
	private CovrsdcTableDAM covrsdcIO = new CovrsdcTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Restdtyrec restdtyrec = new Restdtyrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T6650rec t6650rec = new T6650rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit390
	}

	public Restdty() {
		super();
	}

public void mainline(Object... parmArray)
	{
		restdtyrec.restdtyRec = convertAndSetParam(restdtyrec.restdtyRec, parmArray, 0);
		try {
			subroutineMain000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void subroutineMain000()
	{
		/*MAIN*/
		initialise100();
		process200();
		if (isGT(wsaaSumAssured,ZERO)) {
			planSuffix300();
			stampDuty400();
			termination500();
		}
		else {
			restdtyrec.tax.set(ZERO);
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*INITIALISE*/
		restdtyrec.statuz.set(varcom.oK);
		wsaaSub1.set(ZERO);
		wsaaSumAssured.set(ZERO);
		wsaaStampDuty.set(ZERO);
		wsaaRangeDuty.set(ZERO);
		wsaaSaRem.set(ZERO);
		wsaaFraction.set(ZERO);
		wsaaRemainder.set(ZERO);
		wsaaEndFlag.set(SPACES);
		/*EXIT*/
	}

protected void process200()
	{
		stampDutyRatetable210();
	}

protected void stampDutyRatetable210()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(restdtyrec.company);
		itdmIO.setItemtabl(t6650);
		itdmIO.setItemitem(restdtyrec.cntcurr);
		itdmIO.setItmfrm(restdtyrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),restdtyrec.company)
		|| isNE(itdmIO.getItemtabl(),t6650)
		|| isNE(itdmIO.getItemitem(),restdtyrec.cntcurr)) {
			itdmIO.setItemcoy(restdtyrec.company);
			itdmIO.setItemtabl(t6650);
			itdmIO.setItemitem(restdtyrec.cntcurr);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(r075);
			syserr570();
		}
		t6650rec.t6650Rec.set(itdmIO.getGenarea());
		wsaaSumAssured.set(restdtyrec.sumass);
	}

protected void planSuffix300()
	{
		try {
			checkPlanSuffix310();
		}
		catch (GOTOException e){
		}
	}

protected void checkPlanSuffix310()
	{
		if (isNE(restdtyrec.plnsfx,ZERO)) {
			goTo(GotoLabel.exit390);
		}
		chdrsdcIO.setParams(SPACES);
		chdrsdcIO.setChdrcoy(restdtyrec.company);
		chdrsdcIO.setChdrnum(restdtyrec.chdrnum);
		chdrsdcIO.setFormat(chdrsdcrec);
		chdrsdcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrsdcIO);
		if (isNE(chdrsdcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsdcIO.getParams());
			dbError580();
		}
		if (isNE(chdrsdcIO.getPolsum(),ZERO)) {
			compute(wsaaSumAssured, 0).set(div(wsaaSumAssured,chdrsdcIO.getPolsum()));
		}
	}

protected void stampDuty400()
	{
		/*MAIN-STAMP-DUTY*/
		wsaaSaRem.set(wsaaSumAssured);
		for (wsaaSub1.set(1); !(wsaaEndRange.isTrue()); wsaaSub1.add(1)){
			findRange410();
		}
		/*EXIT*/
	}

protected void findRange410()
	{
		/*RANGE*/
		if (isLTE(wsaaSumAssured,t6650rec.sumin[wsaaSub1.toInt()])) {
			lastRange420();
		}
		else {
			fullRange430();
		}
		wsaaStampDuty.add(wsaaRangeDuty);
		/*EXIT*/
	}

protected void lastRange420()
	{
		/*LAST*/
		compute(wsaaFraction, 0).setDivide(wsaaSaRem, (t6650rec.fact[wsaaSub1.toInt()]));
		wsaaRemainder.setRemainder(wsaaFraction);
		if (isGT(wsaaRemainder,ZERO)) {
			wsaaFraction.add(1);
		}
		compute(wsaaRangeDuty, 5).setRounded((mult(wsaaFraction,t6650rec.rrat[wsaaSub1.toInt()])));
		wsaaEndFlag.set("Y");
		/*EXIT*/
	}

protected void fullRange430()
	{
		/*FULL*/
		compute(wsaaRangeDuty, 5).setRounded(mult((div(t6650rec.sumin[wsaaSub1.toInt()],t6650rec.fact[wsaaSub1.toInt()])),t6650rec.rrat[wsaaSub1.toInt()]));
		compute(wsaaSaRem, 0).set(sub(wsaaSumAssured,t6650rec.sumin[wsaaSub1.toInt()]));
		/*EXIT*/
	}

protected void termination500()
	{
		/*TERMINATION*/
		if (isNE(restdtyrec.plnsfx,ZERO)) {
			compute(restdtyrec.tax, 2).set(mult(wsaaStampDuty,chdrsdcIO.getPolsum()));
		}
		else {
			restdtyrec.tax.set(wsaaStampDuty);
		}
		/*EXIT*/
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		restdtyrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		restdtyrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
