package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.AcmxpfDAO;
import com.csc.life.reassurance.dataaccess.model.Acmxpf;
import com.csc.life.reassurance.dataaccess.model.B5471DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AcmxpfDAOImpl extends BaseDAOImpl<Acmxpf> implements AcmxpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(AcmxpfDAOImpl.class);
 
	@Override
	public int deleteAcmxpfBulk(String Batccoy) {
		String sql = "delete acmxpf where batccoy=?";
		PreparedStatement ps = getPrepareStatement(sql);
		int count = 0;
		try {
			ps.setString(1, Batccoy);
			count = ps.executeUpdate();
		} catch (SQLException e) {
            LOGGER.error("deleteAcmxpfBulk()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
		return count;
	}

	public List<B5471DTO> searchAcmxResult(String batccoy,int min_record, int max_record) {
		List<B5471DTO> b5470DtoList = null;
		B5471DTO b5471Dto = null;
		Acmxpf acmxpf = null;

		StringBuilder sql = new StringBuilder();
		sql.append("select aa.* from (")
		.append("select ROW_NUMBER() over(order by ac.rldgacct, ac.unique_number) rownm,ac.batccoy, ac.sacscode, ac.effdate, ac.rldgacct, ac.origcurr, ac.sacstyp, ac.tranno, ac.batcbrn, ac.batcactyr,ac.batcactmn,")
			.append("ac.batctrcde, ac.batcbatch, ac.rdocnum,ac.jrnseq, ac.origamt, ac.glsign, ac.usrprf,ac.jobnm, ac.datime ,ra.rapaymth,ra.currcode,ra.bankkey,ra.bankacckey,ra.Rasnum,ra.clntcoy,ra.payclt,ra.rascoy from acmxpf ac")
			.append(" left join rasapf ra on ra.rasnum = ac.rldgacct  and ra.rascoy = ac.batccoy and ra.rascoy=? where ac.Batccoy =? )aa")//ILife-3009
			.append(" where aa.rownm>=? and aa.rownm < ?");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			b5470DtoList = new ArrayList<B5471DTO>();
			stmt = getConnection().prepareStatement(sql.toString());
			stmt.setString(1, batccoy);
			stmt.setString(2, batccoy);
			stmt.setInt(3, min_record);
			stmt.setInt(4, max_record);
			rs = stmt.executeQuery();
			while (rs.next()) {
				if (rs.getString(1) != null) {
					acmxpf = new Acmxpf();
					b5471Dto = new B5471DTO();
					acmxpf.setBatccoy(rs.getString(2));
					acmxpf.setSacscode(rs.getString(3));
					acmxpf.setEffdate(rs.getInt(4));
					acmxpf.setRldgacct(rs.getString(5));
					acmxpf.setOrigcurr(rs.getString(6));
					acmxpf.setSacstyp(rs.getString(7));
					acmxpf.setTranno(rs.getInt(8));
					acmxpf.setBatcbrn(rs.getString(9));
					acmxpf.setBatcactyr(rs.getInt(10));
					acmxpf.setBatcactmn(rs.getInt(11));
					acmxpf.setBatctrcde(rs.getString(12));
					acmxpf.setBatcbatch(rs.getString(13));
					acmxpf.setRdocnum(rs.getString(14));
					acmxpf.setJrnseq(rs.getInt(15));
					acmxpf.setOrigamt(rs.getInt(16));
					acmxpf.setGlsign(rs.getString(17));
					acmxpf.setUsrprf(rs.getString(18));
					acmxpf.setJobnm(rs.getString(19));
					acmxpf.setDatime(rs.getString(20));
					b5471Dto.setAcmxpf(acmxpf);
					b5471Dto.setSqlRapaymth(rs.getString(21));
					b5471Dto.setSqlCurrcode(rs.getString(22));
					b5471Dto.setSqlBankkey(rs.getString(23));
					b5471Dto.setSqlBankacckey(rs.getString(24));
					b5471Dto.setSqlRasnum(rs.getString(25));
					b5471Dto.setSqlClntcoy(rs.getString(26));
					b5471Dto.setSqlPayclt(rs.getString(27));
					b5471Dto.setSqlRascoy(rs.getString(28));
					b5470DtoList.add(b5471Dto);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchAcmvResult()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		return b5470DtoList;
	}
	
	public List<Acmxpf> searchAcmxByRldgacct(String batccoy,String rldgacct){
		List<Acmxpf> acmxList = null;
		Acmxpf acmxpf = null;

		StringBuilder sql = new StringBuilder();
		//IJTI-306 START
		sql.append("select unique_number,batccoy, sacscode, effdate, rldgacct, origcurr, sacstyp, tranno, batcbrn, batcactyr,batcactmn,")
			.append("batctrcde, batcbatch, rdocnum,jrnseq, origamt, glsign, usrprf,jobnm, datime from acmxpf ")
			.append("where batccoy= ? and rldgacct = ? order by unique_number,rldgacct");
		
		ResultSet rs = null;
		PreparedStatement statement = null;
		try {
			acmxList = new ArrayList<Acmxpf>();
			statement = getPrepareStatement(sql.toString());
			statement.setString(1, batccoy);
			statement.setString(2, rldgacct);
			rs = statement.executeQuery();
			//IJTI-306 END
			while (rs.next()) {
				if (rs.getString(1) != null) {
					acmxpf = new Acmxpf();
					acmxpf.setBatccoy(rs.getString(2));
					acmxpf.setSacscode(rs.getString(3));
					acmxpf.setEffdate(rs.getInt(4));
					acmxpf.setRldgacct(rs.getString(5));
					acmxpf.setOrigcurr(rs.getString(6));
					acmxpf.setSacstyp(rs.getString(7));	
					acmxpf.setTranno(rs.getInt(8));
					acmxpf.setBatcbrn(rs.getString(9));
					acmxpf.setBatcactyr(rs.getInt(10));
					acmxpf.setBatcactmn(rs.getInt(11));
					acmxpf.setBatctrcde(rs.getString(12));
					acmxpf.setBatcbatch(rs.getString(13));
					acmxpf.setRdocnum(rs.getString(14));
					acmxpf.setJrnseq(rs.getInt(15));
					acmxpf.setOrigamt(rs.getInt(16));
					acmxpf.setGlsign(rs.getString(17));
					acmxpf.setUsrprf(rs.getString(18));
					acmxpf.setJobnm(rs.getString(19));
					acmxpf.setDatime(rs.getString(20));
					acmxList.add(acmxpf);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchAcmxByRldgacct()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			
			close(statement, rs);
		}
		return acmxList;
	}
}