package com.csc.life.reassurance.recordstructures;

import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class VrcorfndRec  extends ExternalData {
	
	private static final long serialVersionUID = 1L;
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	private String chdrnum;
	private String cnttype;
	private String riskComDate;
	private String covrrcesdate;
	private String ptdate;
	private String billfreq;					
	private String paidPrem;
	private String coverage;
	private String crtable;
	private String covrcd;
	private String covrPaidPrem;
	private String agentClass;
	private String rcommtot;
	private String annprem;
	private String effdate;
	private String cltdob;
	private String premCessDate;
	private String srcebus;
	private String premMethod;
	private String lage;
	private String currcode;
	private String ratingdate;
	private String rstaflag;
	private String tpdtype;
	private String noOfCoverages;
	private String costfreq;
	private List<List<String>> sumin;
	private List<List<String>> covrCoverage;
	private List<List<String>> covrRiderId;
	private List<List<String>> dob;
	private List<List<String>> mortcls;
	private List<List<String>> lsex;
	private List<List<String>> rstate01;
	private List<List<String>> duration;
	private List<List<String>> prmbasis;
	private List<List<String>> linkcov; 
	private List<List<String>> covrsum;
	private List<List<String>> count;
	private List<List<List<String>>> agerate;
	private List<List<List<String>>> insprm;
	private List<List<List<String>>> oppc;
	private List<List<List<String>>> opcda;
	private List<List<List<String>>> zmortpct;
	private String reassRfnDys;
	private String reassPol;
	private String reassCovr;
	private String reassCAmount;

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCovrrcesdate() {
		return covrrcesdate;
	}
	public void setCovrrcesdate(String covrrcesdate) {
		this.covrrcesdate = covrrcesdate;
	}
	public String getRiskComDate() {
		return riskComDate;
	}
	public void setRiskComDate(String riskComDate) {
		this.riskComDate = riskComDate;
	}
	public String getPtdate() {
		return ptdate;
	}
	public void setPtdate(String ptdate) {
		this.ptdate = ptdate;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getPaidPrem() {
		return paidPrem;
	}
	public void setPaidPrem(String paidPrem) {
		this.paidPrem = paidPrem;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCovrcd() {
		return covrcd;
	}
	public void setCovrcd(String covrcd) {
		this.covrcd = covrcd;
	}
	public String getCovrPaidPrem() {
		return covrPaidPrem;
	}
	public void setCovrPaidPrem(String covrPaidPrem) {
		this.covrPaidPrem = covrPaidPrem;
	}
	public String getAgentClass() {
		return agentClass;
	}
	public void setAgentClass(String agentClass) {
		this.agentClass = agentClass;
	}
	public String getRcommtot() {
		return rcommtot;
	}
	public void setRcommtot(String rcommtot) {
		this.rcommtot = rcommtot;
	}
	public String getAnnprem() {
		return annprem;
	}
	public void setAnnprem(String annprem) {
		this.annprem = annprem;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public String getCltdob() {
		return cltdob;
	}
	public void setCltdob(String cltdob) {
		this.cltdob = cltdob;
	}
	public String getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(String premCessDate) {
		this.premCessDate = premCessDate;
	}
	public String getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(String srcebus) {
		this.srcebus = srcebus;
	}
	public String getPremMethod() {
		return premMethod;
	}
	public void setPremMethod(String premMethod) {
		this.premMethod = premMethod;
	}
	public String getLage() {
		return lage;
	}
	public void setLage(String lage) {
		this.lage = lage;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getRatingdate() {
		return ratingdate;
	}
	public void setRatingdate(String ratingdate) {
		this.ratingdate = ratingdate;
	}
	public String getRstaflag() {
		return rstaflag;
	}
	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}
	public String getTpdtype() {
		return tpdtype;
	}
	public void setTpdtype(String tpdtype) {
		this.tpdtype = tpdtype;
	}
	public String getNoOfCoverages() {
		return noOfCoverages;
	}
	public void setNoOfCoverages(String noOfCoverages) {
		this.noOfCoverages = noOfCoverages;
	}
	public String getcostfreq() {
		return costfreq;
	}
	public void setCostfreq(String costfreq) {
		this.costfreq = costfreq;
	}
	public List<List<String>> getSumin() {
		return sumin;
	}
	public void setSumin(List<List<String>> sumin) {
		this.sumin = sumin;
	}
	public List<List<String>> getCovrCoverage() {
		return covrCoverage;
	}
	public void setCovrCoverage(List<List<String>> covrCoverage) {
		this.covrCoverage = covrCoverage;
	}
	public List<List<String>> getCovrRiderId() {
		return covrRiderId;
	}
	public void setCovrRiderId(List<List<String>> covrRiderId) {
		this.covrRiderId = covrRiderId;
	}
	public List<List<String>> getDob() {
		return dob;
	}
	public void setDob(List<List<String>> dob) {
		this.dob = dob;
	}
	public List<List<String>> getMortcls() {
		return mortcls;
	}
	public void setMortcls(List<List<String>> mortcls) {
		this.mortcls = mortcls;
	}
	public List<List<String>> getLsex() {
		return lsex;
	}
	public void setLsex(List<List<String>> lsex) {
		this.lsex = lsex;
	}
	public List<List<String>> getRstate01() {
		return rstate01;
	}
	public void setRstate01(List<List<String>> rstate01) {
		this.rstate01 = rstate01;
	}
	public List<List<String>> getDuration() {
		return duration;
	}
	public void setDuration(List<List<String>> duration) {
		this.duration = duration;
	}
	public List<List<String>> getPrmbasis() {
		return prmbasis;
	}
	public void setPrmbasis(List<List<String>> prmbasis) {
		this.prmbasis = prmbasis;
	}
	public List<List<String>> getLinkcov() {
		return linkcov;
	}
	public void setLinkcov(List<List<String>> linkcov) {
		this.linkcov = linkcov;
	}
	public List<List<String>> getCovrsum() {
		return covrsum;
	}
	public void setCovrsum(List<List<String>> covrsum) {
		this.covrsum = covrsum;
	}
	public List<List<String>> getCount() {
		return count;
	}
	public void setCount(List<List<String>> count) {
		this.count = count;
	}
	public String getReassRfnDys() {
		return reassRfnDys;
	}
	public void setReassRfnDys(String reassRfnDys) {
		this.reassRfnDys = reassRfnDys;
	}
	public String getReassPol() {
		return reassPol;
	}
	public void setReassPol(String reassPol) {
		this.reassPol = reassPol;
	}
	public String getReassCovr() {
		return reassCovr;
	}
	public void setReassCovr(String reassCovr) {
		this.reassCovr = reassCovr;
	}
	public String getReassCAmount() {
		return reassCAmount;
	}
	public void setReassCAmount(String reassCAmount) {
		this.reassCAmount = reassCAmount;
	}
	public List<List<List<String>>> getAgerate() {
		return agerate;
	}
	public void setAgerate(List<List<List<String>>> agerate) {
		this.agerate = agerate;
	}
	public List<List<List<String>>> getInsprm() {
		return insprm;
	}
	public void setInsprm(List<List<List<String>>> insprm) {
		this.insprm = insprm;
	}
	public List<List<List<String>>> getOppc() {
		return oppc;
	}
	public void setOppc(List<List<List<String>>> oppc) {
		this.oppc = oppc;
	}
	public List<List<List<String>>> getOpcda() {
		return opcda;
	}
	public void setOpcda(List<List<List<String>>> opcda) {
		this.opcda = opcda;
	}
	public List<List<List<String>>> getZmortpct() {
		return zmortpct;
	}
	public void setZmortpct(List<List<List<String>>> zmortpct) {
		this.zmortpct = zmortpct;
	}
	 
}
