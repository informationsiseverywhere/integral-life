package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5448screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5448ScreenVars sv = (S5448ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5448screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5448ScreenVars screenVars = (S5448ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.rrsktyp.setClassString("");
		screenVars.lrkclsdsc.setClassString("");
		screenVars.rcedsar.setClassString("");
		screenVars.rcstsar.setClassString("");
		screenVars.rresmeth.setClassString("");
		screenVars.rcstfrq.setClassString("");
		screenVars.rrevfrq.setClassString("");
		screenVars.rtaxbas.setClassString("");
		screenVars.rrngmnt01.setClassString("");
		screenVars.rrngmnt02.setClassString("");
		screenVars.rrngmnt03.setClassString("");
		screenVars.rrngmnt04.setClassString("");
		screenVars.rrngmnt05.setClassString("");
		screenVars.rrngmnt06.setClassString("");
		screenVars.rrngmnt07.setClassString("");
		screenVars.rrngmnt08.setClassString("");
		screenVars.rrngmnt09.setClassString("");
		screenVars.rrngmnt10.setClassString("");
		screenVars.rprior.setClassString("");
		screenVars.rclmbas.setClassString("");
		screenVars.proppr.setClassString("");
	}

/**
 * Clear all the variables in S5448screen
 */
	public static void clear(VarModel pv) {
		S5448ScreenVars screenVars = (S5448ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.rrsktyp.clear();
		screenVars.lrkclsdsc.clear();
		screenVars.rcedsar.clear();
		screenVars.rcstsar.clear();
		screenVars.rresmeth.clear();
		screenVars.rcstfrq.clear();
		screenVars.rrevfrq.clear();
		screenVars.rtaxbas.clear();
		screenVars.rrngmnt01.clear();
		screenVars.rrngmnt02.clear();
		screenVars.rrngmnt03.clear();
		screenVars.rrngmnt04.clear();
		screenVars.rrngmnt05.clear();
		screenVars.rrngmnt06.clear();
		screenVars.rrngmnt07.clear();
		screenVars.rrngmnt08.clear();
		screenVars.rrngmnt09.clear();
		screenVars.rrngmnt10.clear();
		screenVars.rprior.clear();
		screenVars.rclmbas.clear();
		screenVars.proppr.clear();
	}
}
