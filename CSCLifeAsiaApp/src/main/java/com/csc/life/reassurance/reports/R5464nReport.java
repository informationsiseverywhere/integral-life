package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5464N.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5464nReport extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private FixedLengthStringData retypedesc = new FixedLengthStringData(30);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5464nReport() {
		super();
	}


	/**
	 * Print the XML for R5464nd03
	 */
	public void printR5464nd03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		printLayout("R5464nd03",			// Record name
			new BaseData[]{			// Fields:
				company
			}
		);

		currentPrintLine.add(6);
	}

	/**
	 * Print the XML for R5464nd04
	 */
	public void printR5464nd04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 9, 4));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 13, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 15, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 17, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 19, 4));
		cmdate.setFieldName("cmdate");
		cmdate.setInternal(subString(recordData, 23, 10));
		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 33, 8));
		rngmnt.setFieldName("rngmnt");
		rngmnt.setInternal(subString(recordData, 41, 4));
		retypedesc.setFieldName("retypedesc");
		retypedesc.setInternal(subString(recordData, 45, 30));
		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 75, 17));
		printLayout("R5464nd04",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				crtable,
				life,
				coverage,
				rider,
				plnsfx,
				cmdate,
				rasnum,
				rngmnt,
				retypedesc,
				raamount
			}
		);

	}

	/**
	 * Print the XML for R5464nh01
	 */
	public void printR5464nh01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 32, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 42, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 44, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		time.setFieldName("time");
		time.set(getTime());
		printLayout("R5464nh01",			// Record name
			new BaseData[]{			// Fields:
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				pagnbr,
				time
			}
		);

		currentPrintLine.set(6);
	}

	/**
	 * Print the XML for R5464nt02
	 */
	public void printR5464nt02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 1, 17));
		printLayout("R5464nt02",			// Record name
			new BaseData[]{			// Fields:
				raamount
			}
		);

		currentPrintLine.add(2);
	}


}
