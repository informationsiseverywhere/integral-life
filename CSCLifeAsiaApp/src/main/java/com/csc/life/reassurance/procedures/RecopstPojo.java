package com.csc.life.reassurance.procedures;

import java.math.BigDecimal;

public class RecopstPojo {
	private String function;
	private String statuz;
	private String batckey;
	private String filler;
	private String batccoy;
	private String batcbrn;
	private int batcactyr;
	private int batcactmn;
	private String batctrcde;
	private String batcbatch;
	private int tranno;
	private String termid;
	private String language;
	private String chdrpfx;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String rasnum;
	private int seqno;
	private int costdate;
	private String validflag;
	private String retype;
	private String rngmnt;
	private BigDecimal sraramt;
	private BigDecimal raAmount;
	private int ctdate;
	private String origcurr;
	private BigDecimal prem;
	private BigDecimal compay;
	private BigDecimal taxamt;
	private BigDecimal refundfe;
	private BigDecimal polsum;
	private int effdate;
	private BigDecimal recovAmt;
	private String crtable;
	private String cnttype;
	private String rcstfrq;
	private BigDecimal RPRate;
	private BigDecimal annprem;

	public BigDecimal getAnnprem() {
		return annprem;
	}
	public void setAnnprem(BigDecimal annprem) {
		this.annprem = annprem;
	}
	public BigDecimal getRPRate() {
		return RPRate;
	}
	public void setRPRate(BigDecimal RPRate) {
		this.RPRate = RPRate;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getBatckey() {
		return batckey;
	}
	public void setBatckey(String batckey) {
		this.batckey = batckey;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getRasnum() {
		return rasnum;
	}
	public void setRasnum(String rasnum) {
		this.rasnum = rasnum;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public int getCostdate() {
		return costdate;
	}
	public void setCostdate(int costdate) {
		this.costdate = costdate;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getRetype() {
		return retype;
	}
	public void setRetype(String retype) {
		this.retype = retype;
	}
	public String getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(String rngmnt) {
		this.rngmnt = rngmnt;
	}
	public BigDecimal getSraramt() {
		return sraramt;
	}
	public void setSraramt(BigDecimal sraramt) {
		this.sraramt = sraramt;
	}
	public BigDecimal getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(BigDecimal raAmount) {
		this.raAmount = raAmount;
	}
	public int getCtdate() {
		return ctdate;
	}
	public void setCtdate(int ctdate) {
		this.ctdate = ctdate;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public BigDecimal getPrem() {
		return prem;
	}
	public void setPrem(BigDecimal prem) {
		this.prem = prem;
	}
	public BigDecimal getCompay() {
		return compay;
	}
	public void setCompay(BigDecimal compay) {
		this.compay = compay;
	}
	public BigDecimal getTaxamt() {
		return taxamt;
	}
	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}
	public BigDecimal getRefundfe() {
		return refundfe;
	}
	public void setRefundfe(BigDecimal refundfe) {
		this.refundfe = refundfe;
	}
	public BigDecimal getPolsum() {
		return polsum;
	}
	public void setPolsum(BigDecimal polsum) {
		this.polsum = polsum;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getRecovAmt() {
		return recovAmt;
	}
	public void setRecovAmt(BigDecimal recovAmt) {
		this.recovAmt = recovAmt;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getRcstfrq() {
		return rcstfrq;
	}
	public void setRcstfrq(String rcstfrq) {
		this.rcstfrq = rcstfrq;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	
	
}
