package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5444
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5444ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(988);
	public FixedLengthStringData dataFields = new FixedLengthStringData(492).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(dataFields,16);
	public ZonedDecimalData cmdate = DD.cmdate.copyToZonedDecimal().isAPartOf(dataFields,66);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,77);
	public ZonedDecimalData ctdate = DD.ctdate.copyToZonedDecimal().isAPartOf(dataFields,81);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData currdesc = DD.currdesc.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,165);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,173);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,228);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(dataFields,305);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,309);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,317);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(dataFields,325);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,342);
	public ZonedDecimalData reasper = DD.reasper.copyToZonedDecimal().isAPartOf(dataFields,350);
	public ZonedDecimalData recovamt = DD.recovamt.copyToZonedDecimal().isAPartOf(dataFields,355);
	public FixedLengthStringData retype = DD.retype.copy().isAPartOf(dataFields,372);
	public FixedLengthStringData retypedesc = DD.retypedesc.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(dataFields,403);
	public FixedLengthStringData rngmntdesc = DD.rngmntdesc.copy().isAPartOf(dataFields,407);
	public ZonedDecimalData rrevdt = DD.rrevdt.copyToZonedDecimal().isAPartOf(dataFields,437);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,445);
	public FixedLengthStringData tabledesc = DD.tabledesc.copy().isAPartOf(dataFields,462);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 492);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cmdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData currdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lrkclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData reasperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData recovamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData retypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData retypedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData rngmntdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rrevdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData tabledescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 616);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cmdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] currdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] reasperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] recovamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] retypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] retypedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] rngmntdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rrevdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] tabledescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cmdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ctdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rrevdtDisp = new FixedLengthStringData(10);

	public LongData S5444screenWritten = new LongData(0);
	public LongData S5444protectWritten = new LongData(0);
	
	public boolean hasSubfile() {
		return false;
	}


	public S5444ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rasnumOut,new String[] {"11","12","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rngmntOut,new String[] {"13","14","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(raamountOut,new String[] {"23","24","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(retypeOut,new String[] {"15","16","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		fieldIndMap.put(lrkclsOut,new String[] {null,"20",null,null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, crtable, lifcnum, linsname, jlifcnum, jlinsname, occdate, ptdate, longdesc, btdate, currcd, currds, rasnum, rngmnt, currdesc, raAmount, clntname, rngmntdesc, reasper, recovamt, sumins, rrevdt, ctdate, cmdate, retypedesc, currcode, retype, tabledesc, lrkcls};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, crtableOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, occdateOut, ptdateOut, longdescOut, btdateOut, currcdOut, currdsOut, rasnumOut, rngmntOut, currdescOut, raamountOut, clntnameOut, rngmntdescOut, reasperOut, recovamtOut, suminsOut, rrevdtOut, ctdateOut, cmdateOut, retypedescOut, currcodeOut, retypeOut, tabledescOut, lrkclsOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, crtableErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, occdateErr, ptdateErr, longdescErr, btdateErr, currcdErr, currdsErr, rasnumErr, rngmntErr, currdescErr, raamountErr, clntnameErr, rngmntdescErr, reasperErr, recovamtErr, suminsErr, rrevdtErr, ctdateErr, cmdateErr, retypedescErr, currcodeErr, retypeErr, tabledescErr, lrkclsErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, rrevdt, ctdate, cmdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, rrevdtErr, ctdateErr, cmdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, rrevdtDisp, ctdateDisp, cmdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5444screen.class;
		protectRecord = S5444protect.class;
	}

}
