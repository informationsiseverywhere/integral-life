package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5434
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5434ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(126);
	public FixedLengthStringData dataFields = new FixedLengthStringData(30).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clttwo = DD.clttwo.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData submnuprog = DD.submnuprog.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 30);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clttwoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lrkclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData submnuprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 54);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clttwoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] submnuprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5434screenWritten = new LongData(0);
	public LongData S5434protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5434ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clttwoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rasnumOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lrkclsOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rngmntOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {submnuprog, action, clttwo, rasnum, lrkcls, rngmnt};
		screenOutFields = new BaseData[][] {submnuprogOut, actionOut, clttwoOut, rasnumOut, lrkclsOut, rngmntOut};
		screenErrFields = new BaseData[] {submnuprogErr, actionErr, clttwoErr, rasnumErr, lrkclsErr, rngmntErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5434screen.class;
		protectRecord = S5434protect.class;
	}

}
