/*
 * File: Trmreas.java
 * Date: 30 August 2009 2:45:57
 * Author: Quipoz Limited
 * 
 * Class transformed from TRMREAS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.recordstructures.Rclmcalrec;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.T5472rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Reassurance Terminations Subroutine.
*
* This subroutine will be called from various Terminations
* AT Modules and will Perform the following Functions:
*
* - It will Read all the RACD records for the Component, and for
*   each RACD record with a Validflag of '1', it will Calculate
*   the amount to be refunded and will post the amounts by
*   calling the Refund Subroutine RCORFND.
*
* - It will call the Claim Recovery subroutine to find out the
*   amount to be returned as part of the Claim.
*
* - It will call Reassurance Termination Subroutine which will
*   write the RACD with a Validflag of '4' and which will also
*   update the Reassurance experience.
*
****************************************************************
* </pre>
*/
public class Trmreas extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "TRMREAS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5472Item = new FixedLengthStringData(8);
		/*                                VALUE ZEROES.                    */
	private PackedDecimalData wsaaReduced = new PackedDecimalData(17, 2).init(ZERO);

	private FixedLengthStringData wsaaFirstRead = new FixedLengthStringData(1);
	private Validator firstRead = new Validator(wsaaFirstRead, "Y");

	private FixedLengthStringData wsaaNoReassurance = new FixedLengthStringData(1);
	private Validator noReassurance = new Validator(wsaaNoReassurance, "Y");
	private ZonedDecimalData wsxxSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsxxLrkcls = new FixedLengthStringData(4);
		/* FORMATS */
	private static final String racdmjarec = "RACDMJAREC";
	private static final String rasarec = "RASAREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t5446 = "T5446";
	private static final String t5447 = "T5447";
	private static final String t5448 = "T5448";
	private static final String t5472 = "T5472";
	private static final String t6598 = "T6598";
	private static final String th618 = "TH618";
		/* ERRORS */
	private static final String e049 = "E049";
	private static final String r051 = "R051";
	private static final String r065 = "R065";
	private static final String r077 = "R077";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Trmracdrec trmracdrec = new Trmracdrec();
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
	private Rclmcalrec rclmcalrec = new Rclmcalrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private T1693rec t1693rec = new T1693rec();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private T5472rec t5472rec = new T5472rec();
	private T6598rec t6598rec = new T6598rec();
	private Th618rec th618rec = new Th618rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Trmreasrec trmreasrec = new Trmreasrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lifeRead250, 
		exit299, 
		callProcessprog430
	}

	public Trmreas() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		trmreasrec.trracalRec = convertAndSetParam(trmreasrec.trracalRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main100()
	{
		mainlineStart101();
		exit199();
	}

protected void mainlineStart101()
	{
		trmreasrec.statuz.set(varcom.oK);
		if (isNE(trmreasrec.function, "TRMR")
		&& isNE(trmreasrec.function, "CHGR")) {
			syserrrec.statuz.set(e049);
			syserr800();
		}
		wsaaBatckey.set(trmreasrec.batckey);
		readT5447700();
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/* PERFORM 200-TABLE-READS.                                     */
		wsaaNoReassurance.set("N");
		tableReads200();
		if (noReassurance.isTrue()) {
			return ;
		}
		compute(wsaaReduced, 2).set(sub(trmreasrec.oldSumins, trmreasrec.newSumins));
		wsaaFirstRead.set("Y");
		wsaaNoReassurance.set("N");
		readRacds300();
		/*    IF NO-REASSURANCE                                    <V4L027>*/
		/*        GO TO 199-EXIT                                   <V4L027>*/
		/*    END-IF.                                              <V4L027>*/
		/*    PERFORM 500-EXPERIENCE-UPDATE.                               */
		rexpupdrec.reassurer.set(SPACES);
		for (wsxxSub.set(1); !(isEQ(th618rec.lrkcls[wsxxSub.toInt()], SPACES)
		|| isGT(wsxxSub, 5)); wsxxSub.add(1)){
			wsxxLrkcls.set(th618rec.lrkcls[wsxxSub.toInt()]);
			experienceUpdate500();
		}
	}

protected void exit199()
	{
		exitProgram();
	}

protected void tableReads200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					tableReads201();
				case lifeRead250: 
					lifeRead250();
				case exit299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void tableReads201()
	{
		/* Read Company Table T1693 to get FSU Company.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(trmreasrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		/* Read the reassurance method table, T5448, to determine Claim*/
		/* Basis.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(trmreasrec.chdrcoy);
		itdmIO.setItemtabl(t5448);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(trmreasrec.cnttype);
		stringVariable1.addExpression(trmreasrec.crtable);
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setItmfrm(trmreasrec.crrcd);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError900();
		}
		if (isNE(wsaaT5448Item, itdmIO.getItemitem())
		|| isNE(trmreasrec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5448)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(trmreasrec.chdrcoy);
			itdmIO.setItemtabl(t5448);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(r065);
			syserr800();
			wsaaNoReassurance.set("Y");
			goTo(GotoLabel.exit299);
		}
		t5448rec.t5448Rec.set(itdmIO.getGenarea());
		if (isEQ(t5448rec.rclmbas, SPACES)) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(trmreasrec.chdrcoy);
			itdmIO.setItemtabl(t5448);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(r077);
			syserr800();
		}
		/* Read of T5472, the Reassurance Claim Basis Table,*/
		/* to give various Calculation Routines.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(trmreasrec.chdrcoy);
		itemIO.setItemtabl(t5472);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(t5448rec.rclmbas);
		stringVariable2.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable2.setStringInto(wsaaT5472Item);
		itemIO.setItemitem(wsaaT5472Item);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
		t5472rec.t5472Rec.set(itemIO.getGenarea());
		/* Read of T6598 with Reserve Method from the Reassurance*/
		/* Method Table, to get Reserve Routine, if one exists.*/
		if (isEQ(t5448rec.rresmeth, SPACES)) {
			t6598rec.calcprog.set(SPACES);
			goTo(GotoLabel.lifeRead250);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(trmreasrec.chdrcoy);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5448rec.rresmeth);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void lifeRead250()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(trmreasrec.chdrcoy);
		lifelnbIO.setChdrnum(trmreasrec.chdrnum);
		lifelnbIO.setLife(trmreasrec.life);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError900();
		}
		wsaaL1Clntnum.set(lifelnbIO.getLifcnum());
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(trmreasrec.chdrcoy);
		lifelnbIO.setChdrnum(trmreasrec.chdrnum);
		lifelnbIO.setLife(trmreasrec.life);
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError900();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
			wsaaL2Clntnum.set(lifelnbIO.getLifcnum());
		}
		else {
			wsaaL2Clntnum.set(SPACES);
		}
		/* Read Risk Type Table, TH618, for multiple risk classes.      */
		readTh6181100();
	}

protected void readRacds300()
	{
		racd301();
	}

protected void racd301()
	{
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setChdrcoy(trmreasrec.chdrcoy);
		racdmjaIO.setChdrnum(trmreasrec.chdrnum);
		racdmjaIO.setLife(trmreasrec.life);
		racdmjaIO.setCoverage(trmreasrec.coverage);
		racdmjaIO.setRider(trmreasrec.rider);
		racdmjaIO.setPlanSuffix(trmreasrec.planSuffix);
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(racdmjarec);
		racdmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			readThruRacd350();
		}
		
	}

protected void readThruRacd350()
	{
		racdLoop351();
	}

protected void racdLoop351()
	{
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(), varcom.oK)
		&& isNE(racdmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdmjaIO.getParams());
			dbError900();
		}
		if (isEQ(racdmjaIO.getStatuz(), varcom.endp)
		|| isNE(racdmjaIO.getChdrcoy(), trmreasrec.chdrcoy)
		|| isNE(racdmjaIO.getChdrnum(), trmreasrec.chdrnum)
		|| isNE(racdmjaIO.getLife(), trmreasrec.life)
		|| isNE(racdmjaIO.getCoverage(), trmreasrec.coverage)
		|| isNE(racdmjaIO.getRider(), trmreasrec.rider)
		|| isNE(racdmjaIO.getPlanSuffix(), trmreasrec.planSuffix)) {
			if (firstRead.isTrue()) {
				wsaaNoReassurance.set("Y");
			}
			racdmjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (firstRead.isTrue()) {
			/*     PERFORM 200-TABLE-READS                         <V4LAQR> */
			wsaaFirstRead.set("N");
		}
		readRasa370();
		callSubroutines400();
		compute(wsaaReduced, 2).set(sub(trmreasrec.oldSumins, trmreasrec.newSumins));
		wsxxLrkcls.set(racdmjaIO.getLrkcls());
		experienceUpdate500();
		racdmjaIO.setFunction(varcom.nextr);
	}

protected void readRasa370()
	{
		/*RASA*/
		/* Read the RASA file with the reassurer.*/
		rasaIO.setDataArea(SPACES);
		rasaIO.setRascoy(racdmjaIO.getChdrcoy());
		rasaIO.setRasnum(racdmjaIO.getRasnum());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			dbError900();
		}
		/*EXIT*/
	}

protected void callSubroutines400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					subrs401();
				case callProcessprog430: 
					callProcessprog430();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void subrs401()
	{
		if (isEQ(t5472rec.calcprog, SPACES)) {
			rclmcalrec.recovAmt.set(ZERO);
			goTo(GotoLabel.callProcessprog430);
		}
		/* Claim Calculation Routine.*/
		rclmcalrec.rclmcalRec.set(SPACES);
		rclmcalrec.recovAmt.set(ZERO);
		rclmcalrec.function.set("POST");
		rclmcalrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		rclmcalrec.chdrnum.set(racdmjaIO.getChdrnum());
		rclmcalrec.life.set(racdmjaIO.getLife());
		rclmcalrec.coverage.set(racdmjaIO.getCoverage());
		rclmcalrec.rider.set(racdmjaIO.getRider());
		rclmcalrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		rclmcalrec.seqno.set(racdmjaIO.getSeqno());
		rclmcalrec.rasnum.set(racdmjaIO.getRasnum());
		rclmcalrec.polsum.set(trmreasrec.polsum);
		rclmcalrec.effdate.set(trmreasrec.effdate);
		rclmcalrec.batckey.set(trmreasrec.batckey);
		rclmcalrec.tranno.set(trmreasrec.tranno);
		rclmcalrec.raAmount.set(racdmjaIO.getRaAmount());
		rclmcalrec.reservprog.set(t6598rec.calcprog);
		rclmcalrec.billfreq.set(trmreasrec.billfreq);
		rclmcalrec.ptdate.set(trmreasrec.ptdate);
		rclmcalrec.origcurr.set(trmreasrec.origcurr);
		rclmcalrec.acctcurr.set(rasaIO.getCurrcode());
		rclmcalrec.crtable.set(trmreasrec.crtable);
		rclmcalrec.cnttype.set(trmreasrec.cnttype);
		rclmcalrec.crrcd.set(trmreasrec.crrcd);
		rclmcalrec.convUnits.set(trmreasrec.convUnits);
		rclmcalrec.jlife.set(trmreasrec.jlife);
		rclmcalrec.singp.set(trmreasrec.singp);
		rclmcalrec.sumins.set(trmreasrec.oldSumins);
		rclmcalrec.clmPercent.set(trmreasrec.clmPercent);
		rclmcalrec.pstatcode.set(trmreasrec.pstatcode);
		rclmcalrec.language.set(trmreasrec.language);
		callProgram(t5472rec.calcprog, rclmcalrec.rclmcalRec);
		if (isNE(rclmcalrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(rclmcalrec.statuz);
			syserrrec.params.set(rclmcalrec.rclmcalRec);
			syserr800();
		}
		zrdecplrec.amountIn.set(rclmcalrec.recovAmt);
		zrdecplrec.currency.set(trmreasrec.origcurr);
		a000CallRounding();
		rclmcalrec.recovAmt.set(zrdecplrec.amountOut);
	}

protected void callProcessprog430()
	{
		if (isEQ(t5472rec.procesprog, SPACES)) {
			return ;
		}
		/* Calculate refund.*/
		if (isEQ(trmreasrec.function, "CHGR")) {
			if (isLTE(wsaaReduced, 0)) {
				return ;
			}
			else {
				if (isLTE(racdmjaIO.getRaAmount(), wsaaReduced)) {
					trmracdrec.function.set("TRMR");
					trmracdrec.newRaAmt.set(ZERO);
					if (isNE(t5472rec.addprocess, SPACES)) {
						rcorfndrec.rcorfndRec.set(SPACES);
						rcorfndrec.refund.set(1);
						calculateRefund600();
					}
				}
				else {
					trmracdrec.function.set("CHGR");
					compute(trmracdrec.newRaAmt, 2).set(sub(racdmjaIO.getRaAmount(), wsaaReduced));
					wsaaReduced.set(racdmjaIO.getRaAmount());
					if (isNE(t5472rec.addprocess, SPACES)) {
						rcorfndrec.rcorfndRec.set(SPACES);
						compute(rcorfndrec.refund, 2).set(div(wsaaReduced, racdmjaIO.getRaAmount()));
						calculateRefund600();
					}
				}
			}
		}
		else {
			trmracdrec.function.set("TRMR");
			trmracdrec.newRaAmt.set(ZERO);
			if (isNE(t5472rec.addprocess, SPACES)) {
				rcorfndrec.rcorfndRec.set(SPACES);
				rcorfndrec.refund.set(1);
				calculateRefund600();
			}
		}
		trmracdrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		trmracdrec.chdrnum.set(racdmjaIO.getChdrnum());
		trmracdrec.life.set(racdmjaIO.getLife());
		trmracdrec.coverage.set(racdmjaIO.getCoverage());
		trmracdrec.rider.set(racdmjaIO.getRider());
		trmracdrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		trmracdrec.seqno.set(racdmjaIO.getSeqno());
		trmracdrec.lrkcls.set(racdmjaIO.getLrkcls());
		trmracdrec.l1Clntnum.set(wsaaL1Clntnum);
		trmracdrec.l2Clntnum.set(wsaaL2Clntnum);
		trmracdrec.crtable.set(trmreasrec.crtable);
		trmracdrec.cnttype.set(trmreasrec.cnttype);
		trmracdrec.clntcoy.set(t1693rec.fsuco);
		trmracdrec.effdate.set(trmreasrec.effdate);
		trmracdrec.tranno.set(trmreasrec.tranno);
		trmracdrec.recovamt.set(rclmcalrec.recovAmt);
		trmracdrec.crrcd.set(trmreasrec.crrcd);
		trmracdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(t5472rec.procesprog, trmracdrec.trmracdRec);
		if (isNE(trmracdrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmracdrec.statuz);
			syserrrec.params.set(trmracdrec.trmracdRec);
			syserr800();
		}
		compute(wsaaReduced, 2).set(sub(wsaaReduced, racdmjaIO.getRaAmount()));
	}

protected void experienceUpdate500()
	{
		update501();
	}

protected void update501()
	{
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(trmreasrec.chdrcoy);
		rexpupdrec.chdrnum.set(trmreasrec.chdrnum);
		rexpupdrec.life.set(trmreasrec.life);
		rexpupdrec.coverage.set(trmreasrec.coverage);
		rexpupdrec.rider.set(trmreasrec.rider);
		rexpupdrec.planSuffix.set(trmreasrec.planSuffix);
		rexpupdrec.tranno.set(trmreasrec.tranno);
		rexpupdrec.clntcoy.set(t1693rec.fsuco);
		rexpupdrec.l1Clntnum.set(wsaaL1Clntnum);
		if (isNE(trmreasrec.jlife, "00")) {
			rexpupdrec.l2Clntnum.set(wsaaL2Clntnum);
		}
		rexpupdrec.sumins.set(wsaaReduced);
		readT5446550();
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(trmreasrec.effdate);
		/*    MOVE T5448-LRKCLS           TO RXUP-RISK-CLASS.              */
		rexpupdrec.riskClass.set(wsxxLrkcls);
		rexpupdrec.arrangement.set(racdmjaIO.getRngmnt());
		rexpupdrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		if (isNE(rexpupdrec.currency, trmreasrec.origcurr)) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(trmreasrec.origcurr);
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt560();
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		rexpupdrec.function.set("DECR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			syserr800();
		}
	}

protected void readT5446550()
	{
		t5446551();
	}

protected void t5446551()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(trmreasrec.chdrcoy);
		itdmIO.setItemtabl(t5446);
		/*    MOVE T5448-LRKCLS           TO ITDM-ITEMITEM.                */
		itdmIO.setItemitem(wsxxLrkcls);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError900();
		}
		/*    IF ITDM-ITEMITEM            NOT = T5448-LRKCLS               */
		if (isNE(itdmIO.getItemitem(), wsxxLrkcls)
		|| isNE(itdmIO.getItemcoy(), trmreasrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5446)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*        MOVE T5448-LRKCLS       TO ITDM-ITEMITEM                 */
			itdmIO.setItemitem(wsxxLrkcls);
			itdmIO.setItemcoy(trmreasrec.chdrcoy);
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(r051);
			syserr800();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callXcvrt560()
	{
		call561();
	}

protected void call561()
	{
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(trmreasrec.effdate);
		conlinkrec.company.set(trmreasrec.chdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr800();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(rexpupdrec.currency);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void calculateRefund600()
	{
		refunds601();
	}

protected void refunds601()
	{
		rcorfndrec.function.set("UPDT");
		rcorfndrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		rcorfndrec.chdrnum.set(racdmjaIO.getChdrnum());
		rcorfndrec.life.set(racdmjaIO.getLife());
		rcorfndrec.coverage.set(racdmjaIO.getCoverage());
		rcorfndrec.rider.set(racdmjaIO.getRider());
		rcorfndrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		rcorfndrec.currfrom.set(racdmjaIO.getCurrfrom());
		/*  MOVE TRRA-PTDATE            TO RCRF-EFFDATE.                 */
		rcorfndrec.effdate.set(trmreasrec.effdate);
		rcorfndrec.seqno.set(racdmjaIO.getSeqno());
		rcorfndrec.polsum.set(trmreasrec.polsum);
		rcorfndrec.batckey.set(trmreasrec.batckey);
		rcorfndrec.tranno.set(trmreasrec.tranno);
		rcorfndrec.newRaAmt.set(ZERO);
		rcorfndrec.language.set(trmreasrec.language);
		rcorfndrec.crtable.set(trmreasrec.crtable);
		rcorfndrec.cnttype.set(trmreasrec.cnttype);
		rcorfndrec.newSumins.set(trmreasrec.newSumins);
		callProgram(t5472rec.addprocess, rcorfndrec.rcorfndRec);
		if (isNE(rcorfndrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(rcorfndrec.statuz);
			syserrrec.params.set(rcorfndrec.rcorfndRec);
			syserr800();
		}
	}

protected void readT5447700()
	{
		start710();
	}

protected void start710()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(trmreasrec.chdrcoy);
		itemIO.setItemtabl(t5447);
		itemIO.setItemitem(trmreasrec.cnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
	}

protected void readTh6181100()
	{
		start1110();
	}

protected void start1110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(trmreasrec.chdrcoy);
		itemIO.setItemtabl(th618);
		itemIO.setItemitem(t5448rec.rrsktyp);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
		th618rec.th618Rec.set(itemIO.getGenarea());
	}

protected void syserr800()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		trmreasrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError900()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		trmreasrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(trmreasrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr800();
		}
		/*A900-EXIT*/
	}
}
