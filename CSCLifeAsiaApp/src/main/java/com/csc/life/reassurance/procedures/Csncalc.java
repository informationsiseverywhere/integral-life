/*
 * File: Csncalc.java
 * Date: 29 August 2009 22:42:45
 * Author: Quipoz Limited
 * 
 * Class transformed from CSNCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.dataaccess.CovtcsnTableDAM;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdcsnTableDAM;
import com.csc.life.reassurance.dataaccess.dao.LirrpfDAO;
import com.csc.life.reassurance.dataaccess.dao.LrrhpfDAO;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Lrrhpf;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.recordstructures.Rtncalcrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.life.reassurance.tablestructures.T5451rec;
import com.csc.life.reassurance.tablestructures.T5472rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.reassurance.tablestructures.Th619rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Reassurance Cession Calculation Subroutine.
*
* Overview
* ========
*
* This is the cession calculation subroutine for reassurance.
* It is called from pre-issue validation, contract auto cession,
* component change and reassurance review to perform the
* following:
*
* 1. Determine if reassurance is required for components under
*    a life.
*
* 2. Calculate how much should be ceded for each component based
*    on the reassurance priority of that component, the company
*    retentions, the reassurance arrangement and the reassurers'
*    experience.
*
* 3. Create RACDs (cession detail records) for both treaty and
*    facultative arrangements.
*
* 4. Advise the calling program whether facultative cessions
*    have been created.
*
* Both quota share and surplus arrangements will be handled
* by CSNCALC. Where no further reassurance arrangements exist
* for a method, but there remains a portion of the sum assured
* to be ceded, a temporary facultative cession with no reassurer
* or arrangement will be created. This will be reported back to
* the calling program by returning a status of 'FACL'.
*
* There are three valid functions, 'COVT', 'COVR' and 'INCR'.
* CSNCALC is used both at pre and post contract issue and
* needs to know whether to read COVT or COVR records. The
* 'INCR' function is used for recalculating cessions from first
* principles when non-proportionate increases are performed.
*
* Cessions
* ========
*
* 1. Quota Share (T5449-RTYTYP = 'Q').
*
* The method used for quota share cessions depends on whether
* the life company has a quota share or not.
*
* If the life company share is greater than zero, i.e.
* T5449-QCOSHR > 0, the life company will cede all risks on
* a percentage share basis. The life company will agree to
* retain a certain protion of the sum assured, up to its
* retention limits, and the reassurers agree to accept the
* remaining proportions of the sum assured up to their
* retention limits.
*
* Example:
*
* Life company retention   : 15% to a maximum of 150,000
* Reassurer 1 retention    : 60% to a maximum of 750,000
* Reassurer 2 retention    : 25% to a maximum of 300,000
*
* Sum Assured              : 1,000,000
* Life company sum retained:   150,000
* Reassurer 1 sum retained :   600,000
* Reassurer 2 sum retained :   250,000
*
* If the life company share is zero, i.e. T5449-QCOSHR = 0,
* the life company will cede the sum assured up to its own
* retention limit for the life. The remaining sum assured
* is split among the reassurers in the arrangement according
* to their share up to their retention limits.
*
* 2. Surplus (T5449-RTYTYP = 'S').
*
* In the case of a surplus treaty, the life company will cede
* all risks above a certain sum assured (i.e. its retention
* limit). The reassurers agree to accept this excess of sum
* assured up to their retention limits. Each reassurer retention
* limit must be exhausted before the next is invoked.
*
* Example:
*
* Life company retention   : 150,000
* Reassurer 1 retention    : 750,000
* Reassurer 2 retention    : 300,000
*
* Sum Assured              : 500,000
* Life company sum retained: 150,000
* Reassurer 1 sum retained : 350,000
* Reassurer 2 sum retained :       0
*
* Reassurer 1 accepts the excess of the sum assured above the
* life company retention. Since this amount is still within its
* acceptable limits, Reassurer 2 does not need to accept any
* risk.
*
* Facultative vs. Treaty
* ======================
*
* Reassurance records (RACDPF) created by CSNCALC will be
* either treaty (RETYPE = 'T') or facultative (RETYPE = 'F').
*
* Most reassurance cessions will be created under a treaty,
* which is simply a standing agreement between the life
* company and a reassurer (known as an arrangement and
* defined on table T5449).
*
* However, if the sum assured to be ceded exceeds the
* retention limits of the reassurer(s), the life company
* will have to negotiate specific terms with a reassurer
* (or several reassurers) to reassure this amount. This is
* known as facultative reassurance. When facultative RACD
* records are created, CSNCALC will return a status of FACL
* and force the user to review the records. This is necessary
* since each facultative agreement must be treated individually.
*
* The RACD records can not be activated (this is done by the
* subroutine ACTVRES) until all the faculative records have
* been reviewed and accepted.
*
* Every time CSNCALC is called, all treaty records and all
* facultative records that have not been reviewed (RACD-
* OVRDIND = spaces) will be deleted and the cessions will be
* recalculated from scratch. If faculative records remain,
* the sums reassured on these records are included in the cession
* calculations (so as to not reassure these amounts again).
* 
* Calling this subroutine from P6674at with ADJU function..
* In the current system if we have more than 1 component and
* if we lapse one/more components then reassurance amount of these
* components are not adjusting into the subsequent components.
* Now it'll work if CSLRI008 feature is ON in Life.
* Calling Actvres subroutine to activate these adjusted reassurance amounts.
*
* Linkage Area
* ============
*
* FUNCTION    PIC X(05)
* STATUZ      PIC X(04)
* CHDRCOY     PIC X(01).
* CHDRNUM     PIC X(08).
* LIFE        PIC X(02).
* COVERAGE    PIC X(02).
* RIDER       PIC X(02).
* PLNSFX      PIC S9(04) COMP-3.
* FSUCO       PIC X(01).
* CNTTYPE     PIC X(03).
* CURRENCY    PIC X(03).
* EFFDATE     PIC S9(08) COMP-3.
* TRANNO      PIC S9(05) COMP-3.
* LANGUAGE    PIC X(01).
* INCR-AMT    PIC S9(11)V9(02) COMP-3.
*
* These fields are all contained within CSNCALCREC.
*
*****************************************************************
* </pre>
*/
public class Csncalc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "CSNCALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaAccumSurrenderValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFaclSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAvailable = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRetention = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCoShare = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaMax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCessAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSurplus = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMaxAgerate = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaMaxOppc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaMaxInsprm = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaComp = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaRisk = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaArr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaRetn = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSort = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaPointer = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNextPointer = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNumSwitches = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSearch = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private PackedDecimalData wsaaT5449Ix = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaJLifenum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaStoreCess = new PackedDecimalData(17, 2);
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaTotalRiskSumins = new PackedDecimalData(17, 2);

		/* The following 2-dimensional array is used to store the
		 available retention and the reassurance arrangement for
		 each reassurer.*/
	private FixedLengthStringData retentionArray = new FixedLengthStringData(2079);
	private FixedLengthStringData[] reassurerRetentions = FLSArrayPartOfStructure(99, 21, retentionArray, 0);
	private FixedLengthStringData[] wsaaReassurer = FLSDArrayPartOfArrayStructure(8, reassurerRetentions, 0);
	private FixedLengthStringData[] wsaaArrangement = FLSDArrayPartOfArrayStructure(4, reassurerRetentions, 8);
	private PackedDecimalData[] wsaaReAvalRetn = PDArrayPartOfArrayStructure(17, 2, reassurerRetentions, 12);

	private FixedLengthStringData wsaaNoMoreSwitches = new FixedLengthStringData(1);
	private Validator noMoreSwitches = new Validator(wsaaNoMoreSwitches, "Y");

	private FixedLengthStringData wsaaRiskClassFound = new FixedLengthStringData(1);
	private Validator riskClassFound = new Validator(wsaaRiskClassFound, "Y");

	private FixedLengthStringData wsaaRetentionFound = new FixedLengthStringData(1);
	private Validator retentionFound = new Validator(wsaaRetentionFound, "Y");

	private FixedLengthStringData wsaaJlifeExists = new FixedLengthStringData(1);
	private Validator jlifeExists = new Validator(wsaaJlifeExists, "Y");
	private ZonedDecimalData wsxxSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsxxCount = new PackedDecimalData(3, 0);
	private String wsxxExceedCoAval = "";
	private ZonedDecimalData wsddSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsddRngmnt = new FixedLengthStringData(4);
	private int wsaaIndex = 0;
	private FixedLengthStringData wsaaRngmntT5449 = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String racdrec = "RACDREC";
	private static final String racdcsnrec = "RACDCSNREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private static final String t5447 = "T5447";
	private static final String t5448 = "T5448";
	private static final String t5449 = "T5449";
	private static final String t5451 = "T5451";
	private static final String t6598 = "T6598";
	private static final String th618 = "TH618";
	private static final String th619 = "TH619";
	private static final String t5472 = "T5472";
	private static final String t5446 = "T5446";
		/* ERRORS */
	private static final String e049 = "E049";
	private static final String e103 = "E103";
	private static final String r054 = "R054";
	private static final String r057 = "R057";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private CovtcsnTableDAM covtcsnIO = new CovtcsnTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private RacdTableDAM racdIO = new RacdTableDAM();
	private RacdcsnTableDAM racdcsnIO = new RacdcsnTableDAM();
	private Rtncalcrec rtncalcrec = new Rtncalcrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5448rec t5448rec = new T5448rec();
	private T5449rec t5449rec = new T5449rec();
	private T5451rec t5451rec = new T5451rec();
	private T6598rec t6598rec = new T6598rec();
	private Th618rec th618rec = new Th618rec();
	private Th619rec th619rec = new Th619rec();
	private T5472rec t5472rec = new T5472rec();
	private T5446rec t5446rec = new T5446rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private Trmracdrec trmracdrec = new Trmracdrec();
	private RiskCompArrayInner riskCompArrayInner = new RiskCompArrayInner();
	private WsaaSortDetailsInner wsaaSortDetailsInner = new WsaaSortDetailsInner();
	private ExternalisedRules er = new ExternalisedRules();
	private List<Racdpf> racdList = new ArrayList<>();
	private RacdpfDAO racdDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private LrrhpfDAO lrrhpfDAO = getApplicationContext().getBean("lrrhpfDAO", LrrhpfDAO.class);
	private LirrpfDAO lirrpfDAO = getApplicationContext().getBean("lirrpfDAO", LirrpfDAO.class);
	private boolean isNewTreatyCreated;
	private FixedLengthStringData wsaaT5472Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsxxLrkcls = new FixedLengthStringData(4);
	private Lrrhpf lrrhpf = new Lrrhpf();
	/*private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;*/
	//PINNACLE-2323 
	protected CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private List<Covtpf> covtpfList = null;
	private boolean newFacl = true;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr1180, 
		exit1190, 
		exit1190x, 
		nextr2180, 
		exit2190, 
		exit2190x
	}

	public Csncalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		csncalcrec.csncalcRec = convertAndSetParam(csncalcrec.csncalcRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString()); //PINNACLE-2323
		csncalcrec.statuz.set(varcom.oK);
		if (isNE(csncalcrec.function, "COVT")
		&& isNE(csncalcrec.function, "COVR")
		&& isNE(csncalcrec.function, "INCR")
		&& isNE(csncalcrec.function, "ADJU")
		&& isNE(csncalcrec.function, "DECR")) {
			syserrrec.statuz.set(e049);
			syserr570();
		}
		/* Check the product reassurance bypass table, T5447. If the*/
		/* product is present on the table then reasssurance processing*/
		/* is not required - exit program.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(csncalcrec.chdrcoy);
		itemIO.setItemtabl(t5447);
		itemIO.setItemitem(csncalcrec.cnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/* Retrieve the contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			//ILIFE-8141
			chdrlnbIO.setChdrcoy(csncalcrec.chdrcoy);
			chdrlnbIO.setChdrnum(csncalcrec.chdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				dbError580();
			}

		}
		/* Initialise working storage fields.*/
		initialize(riskCompArrayInner.riskCompArray);
		initialize(retentionArray);
		wsaaCessAmt.set(ZERO);
		wsaaAvailable.set(ZERO);
		wsaaRetention.set(ZERO);
		wsaaCoShare.set(ZERO);
		wsaaSaMax.set(ZERO);
		wsaaSurplus.set(ZERO);
		if (isEQ(csncalcrec.function, "COVT")
			|| isEQ(csncalcrec.function, "INCR")) {

			covtFunction1000();
		}
		else {
			if (isEQ(csncalcrec.function, "COVR") || isEQ(csncalcrec.function, "DECR")) {
				covrFunction2000();
			}
			if (isEQ(csncalcrec.function, "ADJU")) {
				adjuOldReassuranceData();
				covrFunction2000();
			}
		}
		if(!racdList.isEmpty()) {
			racdDAO.insertBulkRacd(racdList);
			racdList.clear();
		}
	}



protected void adjuOldReassuranceData() {
	lrrhpfDAO.updateLrrhpfBulk(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString()
			,csncalcrec.life.toString(),csncalcrec.coverage.toString(),csncalcrec.rider.toString(),csncalcrec.planSuffix.toString(), csncalcrec.effdate.toInt());
	
	lirrpfDAO.updateLirrpfBulk(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString()
			,csncalcrec.life.toString(),csncalcrec.coverage.toString(),csncalcrec.rider.toString(),csncalcrec.planSuffix.toString(), csncalcrec.effdate.toInt());
}

protected void adjuOldReassuranceExperienceData() {
	
	lrrhpf = new Lrrhpf();
	lrrhpf.setCompany(csncalcrec.chdrcoy.toString());
	lrrhpf.setChdrnum(csncalcrec.chdrnum.toString());
	lrrhpf.setLife(csncalcrec.life.toString());
	lrrhpf.setCoverage(csncalcrec.coverage.toString());
	lrrhpf.setRider(csncalcrec.rider.toString());
	lrrhpf.setPlanSuffix(csncalcrec.planSuffix.toInt());
	lrrhpf.setValidflag("1");
	lrrhpf = lrrhpfDAO.getLrrhpfRecord(lrrhpf);
	if(null == lrrhpf) {
		dbError580();
	}
	else {
		if(csncalcrec.tranno.equals(lrrhpf.getTranno())) {
			lrrhpfDAO.updateLrrhpfSsretnAndSsreast(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString()
				,csncalcrec.life.toString(),csncalcrec.coverage.toString(),csncalcrec.rider.toString(),csncalcrec.planSuffix.toString(), csncalcrec.effdate.toInt());
		}
		else {
			lrrhpfDAO.updateLrrhpfBulk(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString()
					,csncalcrec.life.toString(),csncalcrec.coverage.toString(),csncalcrec.rider.toString(),csncalcrec.planSuffix.toString(), csncalcrec.effdate.toInt());
			
			Lrrhpf lrrhpfnew = new Lrrhpf(lrrhpf);
			lrrhpfnew.setCurrfrom(csncalcrec.effdate.toInt());
			lrrhpfnew.setValidflag("1");
			lrrhpfnew.setSsretn(BigDecimal.ZERO);
			lrrhpfnew.setSsreast(BigDecimal.ZERO);
			lrrhpfnew.setTranno(csncalcrec.tranno.toInt());
			lrrhpfDAO.insertLrrhpfRecord(lrrhpfnew);
		}
	}
	
	List<Racdpf> racdpfList = racdDAO.searchRacdstRecord(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString(), 
			csncalcrec.life.toString(), csncalcrec.coverage.toString(), csncalcrec.rider.toString(), 
			csncalcrec.planSuffix.toInt(), "1");
	
	for(Racdpf racdpf : racdpfList) {
		if(racdpf.getRetype().equals("T")) {
			lirrpfDAO.updateLirrpfByRngmnt(csncalcrec.chdrcoy.toString(), csncalcrec.chdrnum.toString(),
					csncalcrec.life.toString(), csncalcrec.coverage.toString(), csncalcrec.rider.toString(),
					csncalcrec.planSuffix.toString(), csncalcrec.effdate.toInt(), racdpf.getRngmnt());
		}
	}
}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void covtFunction1000()
	{
		covt1010();
	}

protected void covt1010()
	{
		covtcsnIO.setParams(SPACES);
		covtcsnIO.setChdrcoy(csncalcrec.chdrcoy);
		covtcsnIO.setChdrnum(csncalcrec.chdrnum);
		covtcsnIO.setLife(csncalcrec.life);
		covtcsnIO.setCoverage(csncalcrec.coverage);
		covtcsnIO.setRider(csncalcrec.rider);
		covtcsnIO.setSeqnbr(ZERO);
		covtcsnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtcsnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		while ( !(isEQ(covtcsnIO.getStatuz(),varcom.endp))) {
			readThruCovt1100();
		}
		
		for (wsaaRisk.set(1); !(isGT(wsaaRisk, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()], SPACES)); wsaaRisk.add(1)){
			sortArray9000();
		}
		for (wsaaRisk.set(1); !(isGT(wsaaRisk, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()], SPACES)); wsaaRisk.add(1)){
			checkCount9500();
		}
		for (wsaaRisk.set(1); !(isGT(wsaaRisk, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()], SPACES)); wsaaRisk.add(1)){
			processArrangements10000();
		}
	}

protected void readThruCovt1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covt1110();
				case nextr1180: 
					nextr1180();
				case exit1190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covt1110()
	{
		SmartFileCode.execute(appVars, covtcsnIO);
		if (isNE(covtcsnIO.getStatuz(), varcom.oK)
		&& isNE(covtcsnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtcsnIO.getParams());
			dbError580();
		}
		/* CSNCALC can be called at contract or at component level. If*/
		/* it has been called at contract level, the LIFE, COVERAGE,*/
		/* RIDER and PLAN-SUFFIX fields will be blank and therefore*/
		/* don't need to be included in the following status check.*/
		if (isEQ(covtcsnIO.getStatuz(), varcom.endp)
		|| isNE(covtcsnIO.getChdrcoy(), csncalcrec.chdrcoy)
		|| isNE(covtcsnIO.getChdrnum(), csncalcrec.chdrnum)
		|| isNE(covtcsnIO.getLife(), csncalcrec.life)
		|| (isNE(covtcsnIO.getCoverage(), csncalcrec.coverage)
		&& isNE(csncalcrec.coverage, SPACES))
		|| (isNE(covtcsnIO.getRider(), csncalcrec.rider)
		&& isNE(csncalcrec.rider, SPACES))) {
			covtcsnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		/* If the sum assured is not greater than zero, skip this*/
		/* component.*/
		if (isLTE(covtcsnIO.getSumins(), 0)) {
			goTo(GotoLabel.nextr1180);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItmfrm(covtcsnIO.getEffdate());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(csncalcrec.cnttype);
		stringVariable1.addExpression(covtcsnIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		readT54483000();
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextr1180);
		}
		/*<<<<< NEW REASSURANCE SYSTEM >>>>>************top*      */
		/* Using Risk Type in T5448, read new table TH618 to identify      */
		/* the Risk Class(es) for each product.                            */
		readTh6183100();
		if (isEQ(itdmIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.nextr1180);
		}
		/* There are maximum 5 Risk Classes for each Risk Type.            */
		for (wsxxSub.set(1); !(isGT(wsxxSub, 5)); wsxxSub.add(1)){
			thruRiskcls1100x();
		}
	}

	/**
	* <pre>
	**********<<<<< NEW REASSURANCE SYSTEM >>>>>>***********bot*      
	*    PERFORM 4000-SEARCH-RISK-ARRAY.                              
	* If the risk class is not in the array, then the available       
	* and discretionary retentions have not yet been determined.      
	* RTNCALC will be called once for the life and again for the      
	* joint life if one exists. The lower retention returned will     
	* then be stored in the array.                                    
	*    IF NOT RISK-CLASS-FOUND                                      
	*       PERFORM 1200-READ-LIFE                                    
	*    END-IF.                                                      
	* We now have a risk class slot, i.e. WSAA-RISK has a value,      
	* so here we must loop through the array to find a free           
	* component slot, i.e. a value for WSAA-COMP.                     
	*    PERFORM 4500-SEARCH-COMP-ARRAY.                              
	*    MOVE COVTCSN-COVERAGE       TO WSAA-COVERAGE                 
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVTCSN-RIDER          TO WSAA-RIDER                    
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVTCSN-PLAN-SUFFIX    TO WSAA-PLNSFX                   
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVTCSN-CRTABLE        TO WSAA-CRTABLE                  
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVTCSN-EFFDATE        TO WSAA-CRRCD                    
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE T5448-RPRIOR           TO WSAA-RPRIOR                   
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE T5448-RRESMETH         TO WSAA-RRESMETH                 
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE T5448-RREVFRQ          TO WSAA-RREVFRQ                  
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    PERFORM VARYING WSAA-ARR    FROM 1 BY 1                      
	*        UNTIL WSAA-ARR          > 10                             
	*        MOVE T5448-RNGMNT (WSAA-ARR)                             
	*                                TO WSAA-RNGMNT                   
	*                                  (WSAA-RISK WSAA-COMP WSAA-ARR) 
	*    END-PERFORM.                                                 
	* If the function is 'INCR', we only want to reassure the         
	* increase amount since RACDs will already exist for the          
	* original sum assured amount. This increase amount has           
	* been passed over in the linkage as the INCR-AMT.                
	*    IF CSNC-FUNCTION            = 'COVT'                         
	*        MOVE COVTCSN-SUMINS     TO WSAA-SUMINS                   
	*                                   (WSAA-RISK WSAA-COMP)         
	*                                   WSAA-NEWSUMIN                 
	*                                   (WSAA-RISK WSAA-COMP)         
	*    ELSE                                                         
	*        MOVE CSNC-INCR-AMT      TO WSAA-SUMINS                   
	*                                   (WSAA-RISK WSAA-COMP)         
	*                                   WSAA-NEWSUMIN                 
	*                                   (WSAA-RISK WSAA-COMP)         
	*    END-IF.                                                      
	*    MOVE ZEROES                 TO WSAA-MAX-AGERATE              
	*                                   WSAA-MAX-OPPC                 
	*                                   WSAA-MAX-INSPRM.              
	*    MOVE SPACES                 TO LEXT-PARAMS.                  
	*    MOVE COVTCSN-CHDRCOY        TO LEXT-CHDRCOY.                 
	*    MOVE COVTCSN-CHDRNUM        TO LEXT-CHDRNUM.                 
	*    MOVE COVTCSN-LIFE           TO LEXT-LIFE.                    
	*    MOVE COVTCSN-COVERAGE       TO LEXT-COVERAGE.                
	*    MOVE COVTCSN-RIDER          TO LEXT-RIDER.                   
	*    MOVE ZEROES                 TO LEXT-SEQNBR.                  
	*    MOVE LEXTREC                TO LEXT-FORMAT.                  
	*    MOVE BEGN                   TO LEXT-FUNCTION.                
	*    CALL 'LEXTIO'               USING LEXT-PARAMS.               
	*    IF  LEXT-STATUZ             NOT = O-K                        
	*    AND LEXT-STATUZ             NOT = ENDP                       
	*        MOVE LEXT-PARAMS        TO SYSR-PARAMS                   
	*    END-IF.                                                      
	*    PERFORM 7000-READ-LEXT                                       
	*        UNTIL LEXT-STATUZ       = ENDP                           
	*           OR LEXT-CHDRCOY      NOT = COVTCSN-CHDRCOY            
	*           OR LEXT-CHDRNUM      NOT = COVTCSN-CHDRNUM            
	*           OR LEXT-LIFE         NOT = COVTCSN-LIFE               
	*           OR LEXT-COVERAGE     NOT = COVTCSN-COVERAGE           
	*           OR LEXT-RIDER        NOT = COVTCSN-RIDER.             
	*    MOVE WSAA-MAX-AGERATE       TO WSAA-AGERATE                  
	*                                   (WSAA-RISK WSAA-COMP).        
	*    MOVE WSAA-MAX-OPPC          TO WSAA-OPPC                     
	*                                   (WSAA-RISK WSAA-COMP).        
	*    MOVE WSAA-MAX-INSPRM        TO WSAA-INSPRM                   
	*                                   (WSAA-RISK WSAA-COMP).        
	*    MOVE ZEROES                 TO WSAA-FACL-SUMINS.             
	*    MOVE SPACES                 TO RACDCSN-PARAMS.               
	*    MOVE COVTCSN-CHDRCOY        TO RACDCSN-CHDRCOY.              
	*    MOVE COVTCSN-CHDRNUM        TO RACDCSN-CHDRNUM.              
	*    MOVE COVTCSN-LIFE           TO RACDCSN-LIFE.                 
	*    MOVE COVTCSN-COVERAGE       TO RACDCSN-COVERAGE.             
	*    MOVE COVTCSN-RIDER          TO RACDCSN-RIDER.                
	*    MOVE ZEROES                 TO RACDCSN-PLAN-SUFFIX           
	*                                   RACDCSN-SEQNO.                
	*    MOVE RACDCSNREC             TO RACDCSN-FORMAT.               
	*    MOVE BEGN                   TO RACDCSN-FUNCTION.             
	*    CALL 'RACDCSNIO'            USING RACDCSN-PARAMS.            
	*    IF  RACDCSN-STATUZ          NOT = O-K                        
	*    AND RACDCSN-STATUZ          NOT = ENDP                       
	*        MOVE RACDCSN-PARAMS     TO SYSR-PARAMS                   
	*    END-IF.                                                      
	*    PERFORM 8000-READ-RACD                                       
	*        UNTIL RACDCSN-STATUZ    = ENDP                           
	*        OR RACDCSN-CHDRCOY      NOT = COVTCSN-CHDRCOY            
	*        OR RACDCSN-CHDRNUM      NOT = COVTCSN-CHDRNUM            
	*        OR RACDCSN-LIFE         NOT = COVTCSN-LIFE               
	*        OR RACDCSN-COVERAGE     NOT = COVTCSN-COVERAGE           
	*        OR RACDCSN-RIDER        NOT = COVTCSN-RIDER              
	*        OR RACDCSN-SEQNO        NOT = ZEROES                     
	*        OR RACDCSN-PLAN-SUFFIX  NOT = ZEROES.                    
	*    SUBTRACT WSAA-FACL-SUMINS   FROM WSAA-NEWSUMIN               
	*                                     (WSAA-RISK WSAA-COMP).      
	*    IF WSAA-NEWSUMIN (WSAA-RISK WSAA-COMP) < 0                   
	*        MOVE 0                  TO WSAA-NEWSUMIN                 
	*                                   (WSAA-RISK WSAA-COMP)         
	*    END-IF.                                                      
	* </pre>
	*/
protected void nextr1180()
	{
		covtcsnIO.setFunction(varcom.nextr);
	}

protected void thruRiskcls1100x()
	{
		try {
			process1110x();
			baseProgram1120x();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void process1110x()
	{
		if (isEQ(th618rec.lrkcls[wsxxSub.toInt()], SPACES)) {
			goTo(GotoLabel.exit1190x);
		}
	}

protected void baseProgram1120x()
	{
		/* At this point thru 1190X-EXIT is the from Base Program          */
		/* (CSNCALC).                                                      */
		searchRiskArray4000();
		/* If the risk class is not in the array, then the available       */
		/* and discretionary retentions have not yet been determined.      */
		/* RTNCALC will be called once for the life and again for the      */
		/* joint life if one exists. The lower retention returned will     */
		/* then be stored in the array.                                    */
		if (!riskClassFound.isTrue()) {
			readLife1200();
		}
		/* We now have a risk class slot, i.e. WSAA-RISK has a value,      */
		/* so here we must loop through the array to find a free           */
		/* component slot, i.e. a value for WSAA-COMP.                     */
		searchCompArray4500();
		riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getCoverage());
		riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getRider());
		riskCompArrayInner.wsaaPlnsfx[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getPlanSuffix());
		riskCompArrayInner.wsaaCrtable[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getCrtable());
		riskCompArrayInner.wsaaCrrcd[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getEffdate());
		riskCompArrayInner.wsaaRprior[wsaaRisk.toInt()][wsaaComp.toInt()].set(t5448rec.rprior);
		riskCompArrayInner.wsaaRresmeth[wsaaRisk.toInt()][wsaaComp.toInt()].set(t5448rec.rresmeth);
		riskCompArrayInner.wsaaRrevfrq[wsaaRisk.toInt()][wsaaComp.toInt()].set(t5448rec.rrevfrq);
		for (wsaaArr.set(1); !(isGT(wsaaArr, 10)); wsaaArr.add(1)){
			if (isEQ(t5448rec.rrngmnt[wsaaArr.toInt()], SPACES)) {
				riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()].set(t5448rec.rrngmnt[wsaaArr.toInt()]);
			}
			else {
				getRngmntTh6194600();
				if (isNE(wsddRngmnt, SPACES)) {
					riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()].set(wsddRngmnt);
				}
				else {
					syserrrec.statuz.set(varcom.mrnf);
					dbError580();
				}
			}
		}
		/* If the function is 'INCR', we only want to reassure the         */
		/* increase amount since RACDs will already exist for the          */
		/* original sum assured amount. This increase amount has           */
		/* been passed over in the linkage as the INCR-AMT.                */
		if (isEQ(csncalcrec.function, "COVT") || isEQ(csncalcrec.function, "DECR")) {
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getSumins());
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtcsnIO.getSumins());
		}
		else {
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(csncalcrec.incrAmt);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(csncalcrec.incrAmt);
		}
		wsaaMaxAgerate.set(ZERO);
		wsaaMaxOppc.set(ZERO);
		wsaaMaxInsprm.set(ZERO);
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(covtcsnIO.getChdrcoy());
		lextIO.setChdrnum(covtcsnIO.getChdrnum());
		lextIO.setLife(covtcsnIO.getLife());
		lextIO.setCoverage(covtcsnIO.getCoverage());
		lextIO.setRider(covtcsnIO.getRider());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			dbError580();
		}
		while ( !(isEQ(lextIO.getStatuz(), varcom.endp)
		|| isNE(lextIO.getChdrcoy(), covtcsnIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(), covtcsnIO.getChdrnum())
		|| isNE(lextIO.getLife(), covtcsnIO.getLife())
		|| isNE(lextIO.getCoverage(), covtcsnIO.getCoverage())
		|| isNE(lextIO.getRider(), covtcsnIO.getRider()))) {
			readLext7000();
		}
		
		riskCompArrayInner.wsaaAgerate[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaMaxAgerate);
		riskCompArrayInner.wsaaOppc[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaMaxOppc);
		riskCompArrayInner.wsaaInsprm[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaMaxInsprm);
		wsaaFaclSumins.set(ZERO);
		racdcsnIO.setParams(SPACES);
		racdcsnIO.setChdrcoy(covtcsnIO.getChdrcoy());
		racdcsnIO.setChdrnum(covtcsnIO.getChdrnum());
		racdcsnIO.setLife(covtcsnIO.getLife());
		racdcsnIO.setCoverage(covtcsnIO.getCoverage());
		racdcsnIO.setRider(covtcsnIO.getRider());
		racdcsnIO.setPlanSuffix(ZERO);
		racdcsnIO.setSeqno(ZERO);
		racdcsnIO.setFormat(racdcsnrec);
		racdcsnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdcsnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX","SEQNO");
		SmartFileCode.execute(appVars, racdcsnIO);
		if (isNE(racdcsnIO.getStatuz(), varcom.oK)
		&& isNE(racdcsnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdcsnIO.getParams());
			dbError580();
		}
		while ( !(isEQ(racdcsnIO.getStatuz(), varcom.endp)
		|| isNE(racdcsnIO.getChdrcoy(), covtcsnIO.getChdrcoy())
		|| isNE(racdcsnIO.getChdrnum(), covtcsnIO.getChdrnum())
		|| isNE(racdcsnIO.getLife(), covtcsnIO.getLife())
		|| isNE(racdcsnIO.getCoverage(), covtcsnIO.getCoverage())
		|| isNE(racdcsnIO.getRider(), covtcsnIO.getRider())
		|| isNE(racdcsnIO.getSeqno(), ZERO)
		|| isNE(racdcsnIO.getPlanSuffix(), ZERO))) {
			readRacd8000();
		}
		
		riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaFaclSumins);
		if (isLT(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)) {
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
		}
	}

protected void readLife1200()
	{
		life1210();
	}

protected void life1210()
	{
		rtncalcrec.rtncalcRec.set(SPACES);
		rtncalcrec.available.set(ZERO);
		rtncalcrec.discRetention.set(ZERO);
		wsaaJlifeExists.set("N");
		if (isEQ(covtcsnIO.getJlife(), "00")
		|| isEQ(covtcsnIO.getJlife(), SPACES)) {
			lifelnbIO.setParams(SPACES);
			lifelnbIO.setChdrcoy(covtcsnIO.getChdrcoy());
			lifelnbIO.setChdrnum(covtcsnIO.getChdrnum());
			lifelnbIO.setLife(covtcsnIO.getLife());
			lifelnbIO.setJlife("00");
			lifelnbIO.setFormat(lifelnbrec);
			lifelnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifelnbIO.getParams());
				dbError580();
			}
			wsaaLifenum.set(lifelnbIO.getLifcnum());
			rtncalcrec.function.set("BASE");
			rtncalcrec.clntnum.set(lifelnbIO.getLifcnum());
			/*       MOVE T5448-LRKCLS        TO RTNC-RISK-CLASS               */
			rtncalcrec.riskClass.set(th618rec.lrkcls[wsxxSub.toInt()]);
			retention5000();
		}
		if (isEQ(covtcsnIO.getJlife(), "01")
		|| isEQ(covtcsnIO.getJlife(), SPACES)) {
			lifelnbIO.setParams(SPACES);
			lifelnbIO.setChdrcoy(covtcsnIO.getChdrcoy());
			lifelnbIO.setChdrnum(covtcsnIO.getChdrnum());
			lifelnbIO.setLife(covtcsnIO.getLife());
			lifelnbIO.setJlife("01");
			lifelnbIO.setFormat(lifelnbrec);
			lifelnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)
			&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(lifelnbIO.getParams());
				dbError580();
			}
			if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
				wsaaAvailable.set(rtncalcrec.available);
				wsaaRetention.set(rtncalcrec.discRetention);
				wsaaLifenum.set(lifelnbIO.getLifcnum());
				wsaaJLifenum.set(lifelnbIO.getLifcnum());
				wsaaJlifeExists.set("Y");
				rtncalcrec.function.set("BASE");
				rtncalcrec.clntnum.set(lifelnbIO.getLifcnum());
				/*           MOVE T5448-LRKCLS    TO RTNC-RISK-CLASS               */
				rtncalcrec.riskClass.set(th618rec.lrkcls[wsxxSub.toInt()]);
				retention5000();
				if (isLT(wsaaAvailable, rtncalcrec.available)) {
					rtncalcrec.available.set(wsaaAvailable);
					rtncalcrec.discRetention.set(wsaaRetention);
				}
			}
		}
		riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].set(rtncalcrec.available);
		riskCompArrayInner.wsaaCoDiscRetn[wsaaRisk.toInt()].set(rtncalcrec.discRetention);
		/*    MOVE T5448-LRKCLS           TO WSAA-RISK-CLASS (WSAA-RISK).  */
		riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()].set(th618rec.lrkcls[wsxxSub.toInt()]);
	}

protected void readT54491250()
	{
		start1251();
	}

protected void start1251()
	{
		itdmIO.setItemcoy(csncalcrec.chdrcoy);
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(wsaaRngmntT5449);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(itdmIO.getItemitem(), wsaaRngmntT5449)
		|| isNE(itdmIO.getItemcoy(), csncalcrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5449)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaRngmntT5449);
			itdmIO.setItemcoy(csncalcrec.chdrcoy);
			itdmIO.setItemtabl(t5449);
			itdmIO.setStatuz(r057);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5449rec.t5449Rec.set(itdmIO.getGenarea());
		}
	}

protected void covrFunction2000()
	{
		covr2010();
	}

protected void covr2010()
	{
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(csncalcrec.chdrcoy);
		covrlnbIO.setChdrnum(csncalcrec.chdrnum);
		covrlnbIO.setLife(csncalcrec.life);
		covrlnbIO.setCoverage(csncalcrec.coverage);
		covrlnbIO.setRider(csncalcrec.rider);
		covrlnbIO.setPlanSuffix(csncalcrec.planSuffix);
		covrlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			readThruCovr2100();
		}
		
		for (wsaaRisk.set(1); !(isGT(wsaaRisk, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()], SPACES)); wsaaRisk.add(1)){
			sortArray9000();
		}
		for (wsaaRisk.set(1); !(isGT(wsaaRisk, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()], SPACES)); wsaaRisk.add(1)){
			checkCount9500();
		}
		for (wsaaRisk.set(1); !(isGT(wsaaRisk, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()], SPACES)); wsaaRisk.add(1)){
			processArrangements10000();
		}
	}

protected void readThruCovr2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covr2110();
				case nextr2180: 
					nextr2180();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covr2110()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
		&& isNE(covrlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrlnbIO.getParams());
			dbError580();
		}
		
		//PINNACLE-2323 return if covr.coverage < covt.coverage && func is DECR
		if(isEQ(csncalcrec.function,"DECR") && !covtpfList.isEmpty() 
				&& covrlnbIO.getCoverage().toInt() < Integer.valueOf(covtpfList.get(0).getCoverage())) {
			goTo(GotoLabel.nextr2180); 
		}
		
		/* CSNCALC can be called at contract or at component level. If*/
		/* it has been called at contract level, the LIFE, COVERAGE,*/
		/* RIDER and PLAN-SUFFIX fields will be blank and therefore*/
		/* don't need to be included in the following status check.*/
		if (isEQ(covrlnbIO.getStatuz(), varcom.endp)
		|| isNE(covrlnbIO.getChdrcoy(), csncalcrec.chdrcoy)
		|| isNE(covrlnbIO.getChdrnum(), csncalcrec.chdrnum)
		|| isNE(covrlnbIO.getLife(), csncalcrec.life)
		|| (isNE(covrlnbIO.getCoverage(), csncalcrec.coverage)
		&& isNE(csncalcrec.coverage, SPACES))
		|| (isNE(covrlnbIO.getRider(), csncalcrec.rider)
		&& isNE(csncalcrec.rider, SPACES))
		|| (isNE(covrlnbIO.getPlanSuffix(), csncalcrec.planSuffix)
		&& isNE(csncalcrec.planSuffix, ZERO))) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2190);
		}
		/* If the sum assured is not greater than zero, skip this*/
		/* component.*/
		if (isLTE(covrlnbIO.getSumins(), 0)
		|| isNE(covrlnbIO.getValidflag(), "1")) {
			goTo(GotoLabel.nextr2180);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItmfrm(covrlnbIO.getCrrcd());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(csncalcrec.cnttype);
		stringVariable1.addExpression(covrlnbIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		readT54483000();
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextr2180);
		}
		/*<<<<< NEW REASSURANCE SYSTEM >>>>>************top*      */
		/* Using Risk Type in T5448, read new table TH618 to identify      */
		/* the Risk Class(es) for each product.                            */
		readTh6183100();
		if (isEQ(itdmIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.nextr2180);
		}
		/* There are maximum 5 Risk Classes for each Risk Type.            */
		for (wsxxSub.set(1); !(isGT(wsxxSub, 5)); wsxxSub.add(1)){
			thruRiskcls2100x();
		}
	}

	/**
	* <pre>
	**********<<<<< NEW REASSURANCE SYSTEM >>>>>>***********bot*      
	*    PERFORM 4000-SEARCH-RISK-ARRAY.                              
	* If the risk class is not in the array, then the available       
	* and discretionary retentions have not yet been determined.      
	* RTNCALC will be called once for the life and again for the      
	* joint life if one exists. The lower retention returned will     
	* then be stored in the array.                                    
	*    IF NOT RISK-CLASS-FOUND                                      
	*       PERFORM 2200-READ-LIFE                                    
	*    END-IF.                                                      
	* We now have a risk class slot, i.e. WSAA-RISK has a value,      
	* so here we must loop through the array to find a free           
	* component slot, i.e. a value for WSAA-COMP.                     
	*    PERFORM 4500-SEARCH-COMP-ARRAY.                              
	*    MOVE COVRLNB-COVERAGE       TO WSAA-COVERAGE                 
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVRLNB-RIDER          TO WSAA-RIDER                    
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVRLNB-PLAN-SUFFIX    TO WSAA-PLNSFX                   
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVRLNB-CRTABLE        TO WSAA-CRTABLE                  
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE COVRLNB-CRRCD          TO WSAA-CRRCD                    
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE T5448-RPRIOR           TO WSAA-RPRIOR                   
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE T5448-RRESMETH         TO WSAA-RRESMETH                 
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    MOVE T5448-RREVFRQ          TO WSAA-RREVFRQ                  
	*                                   (WSAA-RISK  WSAA-COMP).       
	*    PERFORM VARYING WSAA-ARR    FROM 1 BY 1                      
	*        UNTIL WSAA-ARR          > 10                             
	*        MOVE T5448-RNGMNT (WSAA-ARR)                             
	*                                TO WSAA-RNGMNT                   
	*                                  (WSAA-RISK WSAA-COMP WSAA-ARR) 
	*    END-PERFORM.                                                 
	* If both the Ceding at Sum At Risk field and the Reserve         
	* Calculation Method on T5448 are non-blank, the sum at risk      
	* must be calculated and reassured.                               
	*    IF  T5448-RCEDSAR           NOT = SPACE                      
	*    AND T5448-RRESMETH          NOT = SPACES                     
	*        MOVE 0                  TO WSAA-ACCUM-SURRENDER-VALUE    
	*        PERFORM 6000-CALC-RESERVES                               
	*        COMPUTE WSAA-SUMINS (WSAA-RISK WSAA-COMP)                
	*                                = COVRLNB-SUMINS                 
	*                                - WSAA-ACCUM-SURRENDER-VALUE     
	*    ELSE                                                         
	*        MOVE COVRLNB-SUMINS     TO WSAA-SUMINS                   
	*                                   (WSAA-RISK WSAA-COMP)         
	*    END-IF.                                                      
	*    MOVE WSAA-SUMINS (WSAA-RISK WSAA-COMP)                       
	*                                TO WSAA-NEWSUMIN                 
	*                                   (WSAA-RISK WSAA-COMP)         
	*    MOVE ZEROES                 TO WSAA-MAX-AGERATE              
	*                                   WSAA-MAX-OPPC                 
	*                                   WSAA-MAX-INSPRM.              
	*    MOVE SPACES                 TO LEXT-PARAMS.                  
	*    MOVE COVRLNB-CHDRCOY        TO LEXT-CHDRCOY.                 
	*    MOVE COVRLNB-CHDRNUM        TO LEXT-CHDRNUM.                 
	*    MOVE COVRLNB-LIFE           TO LEXT-LIFE.                    
	*    MOVE COVRLNB-COVERAGE       TO LEXT-COVERAGE.                
	*    MOVE COVRLNB-RIDER          TO LEXT-RIDER.                   
	*    MOVE ZEROES                 TO LEXT-SEQNBR.                  
	*    MOVE LEXTREC                TO LEXT-FORMAT.                  
	*    MOVE BEGN                   TO LEXT-FUNCTION.                
	*    CALL 'LEXTIO'               USING LEXT-PARAMS.               
	*    IF  LEXT-STATUZ             NOT = O-K                        
	*    AND LEXT-STATUZ             NOT = ENDP                       
	*        MOVE LEXT-PARAMS        TO SYSR-PARAMS                   
	*    END-IF.                                                      
	*    PERFORM 7000-READ-LEXT                                       
	*        UNTIL LEXT-STATUZ       = ENDP                           
	*           OR LEXT-CHDRCOY      NOT = COVRLNB-CHDRCOY            
	*           OR LEXT-CHDRNUM      NOT = COVRLNB-CHDRNUM            
	*           OR LEXT-LIFE         NOT = COVRLNB-LIFE               
	*           OR LEXT-COVERAGE     NOT = COVRLNB-COVERAGE           
	*           OR LEXT-RIDER        NOT = COVRLNB-RIDER.             
	*    MOVE WSAA-MAX-AGERATE       TO WSAA-AGERATE                  
	*                                   (WSAA-RISK WSAA-COMP).        
	*    MOVE WSAA-MAX-OPPC          TO WSAA-OPPC                     
	*                                   (WSAA-RISK WSAA-COMP).        
	*    MOVE WSAA-MAX-INSPRM        TO WSAA-INSPRM                   
	*                                   (WSAA-RISK WSAA-COMP).        
	*    MOVE ZEROES                 TO WSAA-FACL-SUMINS.             
	*    MOVE SPACES                 TO RACDCSN-PARAMS.               
	*    MOVE COVRLNB-CHDRCOY        TO RACDCSN-CHDRCOY.              
	*    MOVE COVRLNB-CHDRNUM        TO RACDCSN-CHDRNUM.              
	*    MOVE COVRLNB-LIFE           TO RACDCSN-LIFE.                 
	*    MOVE COVRLNB-COVERAGE       TO RACDCSN-COVERAGE.             
	*    MOVE COVRLNB-RIDER          TO RACDCSN-RIDER.                
	*    MOVE COVRLNB-PLAN-SUFFIX    TO RACDCSN-PLAN-SUFFIX.          
	*    MOVE ZEROES                 TO RACDCSN-SEQNO.                
	*    MOVE RACDCSNREC             TO RACDCSN-FORMAT.               
	*    MOVE BEGN                   TO RACDCSN-FUNCTION.             
	*    CALL 'RACDCSNIO'            USING RACDCSN-PARAMS.            
	*    IF  RACDCSN-STATUZ          NOT = O-K                        
	*    AND RACDCSN-STATUZ          NOT = ENDP                       
	*        MOVE RACDCSN-PARAMS     TO SYSR-PARAMS                   
	*    END-IF.                                                      
	*    PERFORM 8000-READ-RACD                                       
	*        UNTIL RACDCSN-STATUZ    = ENDP                           
	*        OR RACDCSN-CHDRCOY      NOT = COVRLNB-CHDRCOY            
	*        OR RACDCSN-CHDRNUM      NOT = COVRLNB-CHDRNUM            
	*        OR RACDCSN-LIFE         NOT = COVRLNB-LIFE               
	*        OR RACDCSN-COVERAGE     NOT = COVRLNB-COVERAGE           
	*        OR RACDCSN-RIDER        NOT = COVRLNB-RIDER              
	*        OR RACDCSN-PLAN-SUFFIX  NOT = COVRLNB-PLAN-SUFFIX.       
	*    SUBTRACT WSAA-FACL-SUMINS   FROM WSAA-NEWSUMIN               
	*                                     (WSAA-RISK WSAA-COMP).      
	*    IF WSAA-NEWSUMIN (WSAA-RISK WSAA-COMP) < 0                   
	*        MOVE 0                  TO WSAA-NEWSUMIN                 
	*                                   (WSAA-RISK WSAA-COMP)         
	*    END-IF.                                                      
	* </pre>
	*/
protected void nextr2180()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void thruRiskcls2100x()
	{
		try {
			process2110x();
			baseProgram2120x();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void process2110x()
	{
		if (isEQ(th618rec.lrkcls[wsxxSub.toInt()], SPACES)) {
			goTo(GotoLabel.exit2190x);
		}
	}

protected void baseProgram2120x()
	{
		/* At this point thru 1190X-EXIT is the from Base Program          */
		/* (CSNCALC).                                                      */
		searchRiskArray4000();
		/* If the risk class is not in the array, then the available       */
		/* and discretionary retentions have not yet been determined.      */
		/* RTNCALC will be called once for the life and again for the      */
		/* joint life if one exists. The lower retention returned will     */
		/* then be stored in the array.                                    */
		if (!riskClassFound.isTrue()) {
			readLife2200();
		}
		/* We now have a risk class slot, i.e. WSAA-RISK has a value,      */
		/* so here we must loop through the array to find a free           */
		/* component slot, i.e. a value for WSAA-COMP.                     */
		searchCompArray4500();
		riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()].set(covrlnbIO.getCoverage());
		riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()].set(covrlnbIO.getRider());
		riskCompArrayInner.wsaaPlnsfx[wsaaRisk.toInt()][wsaaComp.toInt()].set(covrlnbIO.getPlanSuffix());
		riskCompArrayInner.wsaaCrtable[wsaaRisk.toInt()][wsaaComp.toInt()].set(covrlnbIO.getCrtable());
		riskCompArrayInner.wsaaCrrcd[wsaaRisk.toInt()][wsaaComp.toInt()].set(covrlnbIO.getCrrcd());
		riskCompArrayInner.wsaaRprior[wsaaRisk.toInt()][wsaaComp.toInt()].set(t5448rec.rprior);
		riskCompArrayInner.wsaaRresmeth[wsaaRisk.toInt()][wsaaComp.toInt()].set(t5448rec.rresmeth);
		riskCompArrayInner.wsaaRrevfrq[wsaaRisk.toInt()][wsaaComp.toInt()].set(t5448rec.rrevfrq);
		for (wsaaArr.set(1); !(isGT(wsaaArr, 10)); wsaaArr.add(1)){
			if (isEQ(t5448rec.rrngmnt[wsaaArr.toInt()], SPACES)) {
				riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()].set(t5448rec.rrngmnt[wsaaArr.toInt()]);
			}
			else {
				getRngmntTh6194600();
				if (isNE(wsddRngmnt, SPACES)) {
					riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()].set(wsddRngmnt);
				}
				else {
					syserrrec.statuz.set(varcom.mrnf);
					dbError580();
				}
			}
		}
		/* If both the Ceding at Sum At Risk field and the Reserve         */
		/* Calculation Method on T5448 are non-blank, the sum at risk      */
		/* must be calculated and reassured.                               */
		if (isNE(t5448rec.rcedsar, SPACES)
		&& isNE(t5448rec.rresmeth, SPACES)) {
			wsaaAccumSurrenderValue.set(0);
			calcReserves6000();
			compute(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()], 2).set(sub(covrlnbIO.getSumins(), wsaaAccumSurrenderValue));
		}
		else {
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(covrlnbIO.getSumins());
			//PINNACLE-2323 if function is DECR,  check coverage in covt list, if yes use covt sumins
			if(isEQ(csncalcrec.function,"DECR")) {
				for(Covtpf covtpf : covtpfList) {
					if(isEQ(covrlnbIO.getLife(),covtpf.getLife())
							&& isEQ(covrlnbIO.getCoverage(),covtpf.getCoverage()) 
							&& isEQ(covrlnbIO.getRider(),covtpf.getRider())) {
						riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(covtpf.getSumins());
						break;
					}
				}
			}
		}
		riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()]);
		wsaaMaxAgerate.set(ZERO);
		wsaaMaxOppc.set(ZERO);
		wsaaMaxInsprm.set(ZERO);
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(covrlnbIO.getChdrcoy());
		lextIO.setChdrnum(covrlnbIO.getChdrnum());
		lextIO.setLife(covrlnbIO.getLife());
		lextIO.setCoverage(covrlnbIO.getCoverage());
		lextIO.setRider(covrlnbIO.getRider());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			dbError580();
		}
		while ( !(isEQ(lextIO.getStatuz(), varcom.endp)
		|| isNE(lextIO.getChdrcoy(), covrlnbIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(), covrlnbIO.getChdrnum())
		|| isNE(lextIO.getLife(), covrlnbIO.getLife())
		|| isNE(lextIO.getCoverage(), covrlnbIO.getCoverage())
		|| isNE(lextIO.getRider(), covrlnbIO.getRider()))) {
			readLext7000();
		}
		
		riskCompArrayInner.wsaaAgerate[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaMaxAgerate);
		riskCompArrayInner.wsaaOppc[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaMaxOppc);
		riskCompArrayInner.wsaaInsprm[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaMaxInsprm);
		wsaaFaclSumins.set(ZERO);
		racdcsnIO.setParams(SPACES);
		racdcsnIO.setChdrcoy(covrlnbIO.getChdrcoy());
		racdcsnIO.setChdrnum(covrlnbIO.getChdrnum());
		racdcsnIO.setLife(covrlnbIO.getLife());
		racdcsnIO.setCoverage(covrlnbIO.getCoverage());
		racdcsnIO.setRider(covrlnbIO.getRider());
		racdcsnIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		racdcsnIO.setSeqno(ZERO);
		racdcsnIO.setFormat(racdcsnrec);
		racdcsnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdcsnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, racdcsnIO);
		if (isNE(racdcsnIO.getStatuz(), varcom.oK)
		&& isNE(racdcsnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdcsnIO.getParams());
			dbError580();
		}
		while ( !(isEQ(racdcsnIO.getStatuz(), varcom.endp)
		|| isNE(racdcsnIO.getChdrcoy(), covrlnbIO.getChdrcoy())
		|| isNE(racdcsnIO.getChdrnum(), covrlnbIO.getChdrnum())
		|| isNE(racdcsnIO.getLife(), covrlnbIO.getLife())
		|| isNE(racdcsnIO.getCoverage(), covrlnbIO.getCoverage())
		|| isNE(racdcsnIO.getRider(), covrlnbIO.getRider())
		|| isNE(racdcsnIO.getPlanSuffix(), covrlnbIO.getPlanSuffix()))) {
			readRacd8000();
		}
		
		riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaFaclSumins);
		if (isLT(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)) {
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
		}
	}

protected void readLife2200()
	{
		life2210();
	}

protected void life2210()
	{
		rtncalcrec.rtncalcRec.set(SPACES);
		rtncalcrec.available.set(ZERO);
		rtncalcrec.discRetention.set(ZERO);
		wsaaJlifeExists.set("N");
		if (isEQ(covrlnbIO.getJlife(), "00")
		|| isEQ(covrlnbIO.getJlife(), SPACES)) {
			lifelnbIO.setParams(SPACES);
			lifelnbIO.setChdrcoy(covrlnbIO.getChdrcoy());
			lifelnbIO.setChdrnum(covrlnbIO.getChdrnum());
			lifelnbIO.setLife(covrlnbIO.getLife());
			lifelnbIO.setJlife("00");
			lifelnbIO.setFormat(lifelnbrec);
			lifelnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifelnbIO.getParams());
				dbError580();
			}
			wsaaLifenum.set(lifelnbIO.getLifcnum());
			rtncalcrec.function.set("BASE");
			rtncalcrec.clntnum.set(lifelnbIO.getLifcnum());
			/*       MOVE T5448-LRKCLS        TO RTNC-RISK-CLASS               */
			rtncalcrec.riskClass.set(th618rec.lrkcls[wsxxSub.toInt()]);
			retention5000();
		}
		if (isEQ(covrlnbIO.getJlife(), "01")
		|| isEQ(covrlnbIO.getJlife(), SPACES)) {
			lifelnbIO.setParams(SPACES);
			lifelnbIO.setChdrcoy(covrlnbIO.getChdrcoy());
			lifelnbIO.setChdrnum(covrlnbIO.getChdrnum());
			lifelnbIO.setLife(covrlnbIO.getLife());
			lifelnbIO.setJlife("01");
			lifelnbIO.setFormat(lifelnbrec);
			lifelnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)
			&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(lifelnbIO.getParams());
				dbError580();
			}
			if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
				wsaaAvailable.set(rtncalcrec.available);
				wsaaRetention.set(rtncalcrec.discRetention);
				wsaaLifenum.set(lifelnbIO.getLifcnum());
				wsaaJLifenum.set(lifelnbIO.getLifcnum());
				wsaaJlifeExists.set("Y");
				rtncalcrec.function.set("BASE");
				rtncalcrec.clntnum.set(lifelnbIO.getLifcnum());
				/*           MOVE T5448-LRKCLS    TO RTNC-RISK-CLASS               */
				rtncalcrec.riskClass.set(th618rec.lrkcls[wsxxSub.toInt()]);
				wsaaArr.set(1);
				getRngmntTh6194600();
				if (isNE(wsddRngmnt, SPACES)) {
					rtncalcrec.arrangement.set(wsddRngmnt);
					wsaaRngmntT5449.set(wsddRngmnt);
				}
				else {
					syserrrec.statuz.set(varcom.mrnf);
					dbError580();
				}
				itdmIO.setParams(SPACES);
				itdmIO.setItmfrm(covrlnbIO.getCurrfrom());
				readT54491250();
				retention5000();
				if (isLT(wsaaAvailable, rtncalcrec.available)) {
					rtncalcrec.available.set(wsaaAvailable);
					rtncalcrec.discRetention.set(wsaaRetention);
				}
			}
		}
		riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].set(rtncalcrec.available);
		riskCompArrayInner.wsaaCoDiscRetn[wsaaRisk.toInt()].set(rtncalcrec.discRetention);
		/*    MOVE T5448-LRKCLS           TO WSAA-RISK-CLASS (WSAA-RISK).  */
		riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()].set(th618rec.lrkcls[wsxxSub.toInt()]);
	}

protected void readT54483000()
	{
		t54483010();
	}

protected void t54483010()
	{
		/* Read the reassurance method table, T5448, to determine risk*/
		/* class, priority and cession basis.*/
		itdmIO.setItemcoy(csncalcrec.chdrcoy);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setItemtabl(t5448);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(wsaaT5448Item, itdmIO.getItemitem())
		|| isNE(csncalcrec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5448)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTh6183100()
	{
		th6183110();
	}

protected void th6183110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(csncalcrec.chdrcoy);
		itemIO.setItemtabl(th618);
		itemIO.setItemitem(t5448rec.rrsktyp);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		th618rec.th618Rec.set(itemIO.getGenarea());
	}

protected void searchRiskArray4000()
	{
		/*ARRAY*/
		wsaaRiskClassFound.set("N");
		for (wsaaSearch.set(1); !(isGT(wsaaSearch, 20)
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaSearch.toInt()], th618rec.lrkcls[wsxxSub.toInt()])
		|| isEQ(riskCompArrayInner.wsaaRiskClass[wsaaSearch.toInt()], SPACES)); wsaaSearch.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSearch, 20)) {
			syserrrec.statuz.set(e103);
			syserr570();
		}
		else {
			/*        IF WSAA-RISK-CLASS (WSAA-SEARCH) = T5448-LRKCLS          */
			if (isEQ(riskCompArrayInner.wsaaRiskClass[wsaaSearch.toInt()], th618rec.lrkcls[wsxxSub.toInt()])) {
				wsaaRiskClassFound.set("Y");
			}
			wsaaRisk.set(wsaaSearch);
		}
		/*EXIT*/
	}

protected void searchCompArray4500()
	{
		/*ARRAY*/
		for (wsaaSearch.set(1); !(isGT(wsaaSearch, 99)
		|| isEQ(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaSearch.toInt()], 0)); wsaaSearch.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSearch, 99)) {
			syserrrec.statuz.set(e103);
			syserr570();
		}
		else {
			wsaaComp.set(wsaaSearch);
		}
		/*EXIT*/
	}

protected void getRngmntTh6194600()
	{
		start4610();
	}

	/**
	* <pre>
	*******************************                           <V4L027>
	* </pre>
	*/
protected void start4610()
	{
		wsddRngmnt.set(SPACES);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(csncalcrec.chdrcoy);
		itemIO.setItemtabl(th619);
		itemIO.setItemitem(t5448rec.rrngmnt[wsaaArr.toInt()]);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		else {
			th619rec.th619Rec.set(itemIO.getGenarea());
		}
		for (wsddSub.set(1); !(isGT(wsddSub, 5)); wsddSub.add(1)){
			if (isEQ(th619rec.lrkcls[wsddSub.toInt()], th618rec.lrkcls[wsxxSub.toInt()])) {
				wsddRngmnt.set(th619rec.rngmnt[wsddSub.toInt()]);
				wsddSub.set(6);
			}
		}
	}

protected void retention5000()
	{
		retn5010();
	}

protected void retn5010()
	{
		rtncalcrec.company.set(csncalcrec.chdrcoy);
		rtncalcrec.clntcoy.set(csncalcrec.fsuco);
		rtncalcrec.effdate.set(csncalcrec.effdate);
		rtncalcrec.available.set(ZERO);
		rtncalcrec.discRetention.set(ZERO);
		callProgram(Rtncalc.class, rtncalcrec.rtncalcRec);
		if (isNE(rtncalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(rtncalcrec.statuz);
			syserrrec.params.set(rtncalcrec.rtncalcRec);
			syserr570();
		}
		if (isNE(rtncalcrec.currency, csncalcrec.currency)) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(rtncalcrec.available);
			conlinkrec.currIn.set(rtncalcrec.currency);
			conlinkrec.currOut.set(csncalcrec.currency);
			callXcvrt16000();
			rtncalcrec.available.set(conlinkrec.amountOut);
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(rtncalcrec.discRetention);
			conlinkrec.currIn.set(rtncalcrec.currency);
			conlinkrec.currOut.set(csncalcrec.currency);
			callXcvrt16000();
			rtncalcrec.discRetention.set(conlinkrec.amountOut);
		}
	}

protected void calcReserves6000()
	{
		calc6010();
	}

protected void calc6010()
	{
		/* Calucualte the sum at risk for the component as for the*/
		/* effective date.*/
		readT65986100();
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrlnbIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrlnbIO.getChdrnum());
		srcalcpy.planSuffix.set(covrlnbIO.getPlanSuffix());
		srcalcpy.polsum.set(chdrlnbIO.getPolsum());
		srcalcpy.lifeLife.set(covrlnbIO.getLife());
		srcalcpy.lifeJlife.set(covrlnbIO.getJlife());
		srcalcpy.covrCoverage.set(covrlnbIO.getCoverage());
		srcalcpy.covrRider.set(covrlnbIO.getRider());
		srcalcpy.crtable.set(covrlnbIO.getCrtable());
		srcalcpy.crrcd.set(covrlnbIO.getCrrcd());
		srcalcpy.ptdate.set(chdrlnbIO.getPtdate());
		srcalcpy.effdate.set(csncalcrec.effdate);
		srcalcpy.language.set(csncalcrec.language);
		srcalcpy.chdrCurr.set(chdrlnbIO.getCntcurr());
		srcalcpy.billfreq.set(chdrlnbIO.getBillfreq());
		/* Now call the Surrender value calculation subroutine.*/
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			getSurrValue6200();
		}
		
	}

protected void readT65986100()
	{
		t65986110();
	}

protected void t65986110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(csncalcrec.chdrcoy);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5448rec.rresmeth);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError580();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void getSurrValue6200()
	{
		/*START*/
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.statuz.set(srcalcpy.status);
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserr570();
		}
		/* The surrender value of the units is returned in the estimated*/
		/* variable for unit-linked products and in the actual variable*/
		/* for traditional products.*/
		if (isGT(srcalcpy.estimatedVal, ZERO)) {
			wsaaAccumSurrenderValue.add(srcalcpy.estimatedVal);
		}
		else {
			wsaaAccumSurrenderValue.add(srcalcpy.actualVal);
		}
		/*EXIT*/
	}

protected void readLext7000()
	{
		lext7010();
		next7080();
	}

protected void lext7010()
	{
		/* If the reassurance indicator is '1', then the special term does */
		/* apply to reassurance so skip this record.                       */
		if (isEQ(lextIO.getReasind(), "1")) {
			return ;
		}
		/* Read through special terms and record the highest rating for*/
		/* age addition, load % and rate adjustment.*/
		if (isGT(lextIO.getAgerate(), wsaaMaxAgerate)) {
			wsaaMaxAgerate.set(lextIO.getAgerate());
		}
		if (isGT(lextIO.getOppc(), wsaaMaxOppc)) {
			wsaaMaxOppc.set(lextIO.getOppc());
		}
		if (isGT(lextIO.getInsprm(), wsaaMaxInsprm)) {
			wsaaMaxInsprm.set(lextIO.getInsprm());
		}
	}

protected void next7080()
	{
		lextIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void readRacd8000()
	{
		racd8010();
		nextr8050();
	}

protected void racd8010()
	{
		if (isNE(racdcsnIO.getLrkcls(), th618rec.lrkcls[wsxxSub.toInt()]) || isEQ(csncalcrec.function, "ADJU") || isEQ(csncalcrec.function, "DECR")) {
			return ;
		}
		/* Delete any validflag '3' non-manual override cession records*/
		/* and reduce the sum to be reassured by the total reassured*/
		/* amounts of all validflag '3' confirmed facultative cession*/
		/* records plus any existing in-force cession records.*/
		if (isEQ(racdcsnIO.getOvrdind(), SPACES)
		&& isEQ(racdcsnIO.getValidflag(), "3")) {
			deleteRacd8100();
		}
		else {
			wsaaFaclSumins.add(racdcsnIO.getRaAmount());
			getRacdRetn8200();
		}
	}

protected void nextr8050()
	{
		racdcsnIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, racdcsnIO);
		if (isNE(racdcsnIO.getStatuz(), varcom.oK)
		&& isNE(racdcsnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdcsnIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void deleteRacd8100()
	{
		racd8110();
	}

protected void racd8110()
	{
		racdIO.setParams(SPACES);
		racdIO.setRrn(racdcsnIO.getRrn());
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
		racdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
	}

protected void getRacdRetn8200()
	{
		retn8210();
	}

protected void retn8210()
	{
		if (isNE(racdcsnIO.getValidflag(), "1")
		&& isNE(racdcsnIO.getValidflag(), "2")) {
			setRacdList(racdcsnIO);
			deleteRacd8100();
		}
		/* Search through array for the reassurer arrangement retention.*/
		/* If not found, get the available retention for the life assured*/
		/* by calling RTNCALC. If joint life, calculated the available*/
		/* retention for the joint life by calling RTNCALC. Store the*/
		/* lower of the two in the array. If the retention currency is*/
		/* not equal to the contract currency, use XCVRT to convert the*/
		/* retention amounts.*/
		wsaaRetentionFound.set("Y");
		for (wsaaSearch.set(1); !(isGT(wsaaSearch, 99)
		|| isEQ(wsaaReassurer[wsaaSearch.toInt()], SPACES)
		|| (isEQ(racdcsnIO.getRngmnt(), wsaaArrangement[wsaaSearch.toInt()])
		&& isEQ(racdcsnIO.getRasnum(), wsaaReassurer[wsaaSearch.toInt()]))); wsaaSearch.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSearch, 99)) {
			syserrrec.statuz.set(e103);
			syserr570();
		}
		else {
			wsaaRetn.set(wsaaSearch);
		}
		if (isEQ(wsaaReassurer[wsaaRetn.toInt()], SPACES)) {
			wsaaRetentionFound.set("N");
		}
		rtncalcrec.rtncalcRec.set(SPACES);
		if (!retentionFound.isTrue()) {
			rtncalcrec.function.set("RESR");
			rtncalcrec.clntnum.set(wsaaLifenum);
			rtncalcrec.riskClass.set(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()]);
			rtncalcrec.arrangement.set(racdcsnIO.getRngmnt());
			rtncalcrec.reassurer.set(racdcsnIO.getRasnum());
			retention5000();
			if (jlifeExists.isTrue()) {
				wsaaAvailable.set(rtncalcrec.available);
				rtncalcrec.function.set("RESR");
				rtncalcrec.clntnum.set(wsaaLifenum);
				rtncalcrec.riskClass.set(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()]);
				rtncalcrec.arrangement.set(racdcsnIO.getRngmnt());
				rtncalcrec.reassurer.set(racdcsnIO.getRasnum());
				retention5000();
				if (isLT(wsaaAvailable, rtncalcrec.available)) {
					rtncalcrec.available.set(wsaaAvailable);
				}
			}
			wsaaReAvalRetn[wsaaRetn.toInt()].set(rtncalcrec.available);
			wsaaArrangement[wsaaRetn.toInt()].set(rtncalcrec.arrangement);
			wsaaReassurer[wsaaRetn.toInt()].set(rtncalcrec.reassurer);
		}
		wsaaReAvalRetn[wsaaRetn.toInt()].subtract(racdcsnIO.getRaAmount());
	}

protected void setRacdList(RacdcsnTableDAM racdcsnIO) {
	Racdpf racd = new Racdpf();
	racd.setTranno(csncalcrec.tranno.toInt());
	racd.setCurrfrom(racdcsnIO.getCurrfrom().toInt());
	racd.setCurrto(racdcsnIO.getCurrto().toInt());
	racd.setCtdate(racdcsnIO.getCtdate().toInt());
	racd.setChdrcoy(racdcsnIO.getChdrcoy().toString());
	racd.setChdrnum(racdcsnIO.getChdrnum().toString());
	racd.setLife(racdcsnIO.getLife().toString());
	racd.setCoverage(racdcsnIO.getCoverage().toString());
	racd.setRider(racdcsnIO.getRider().toString());
	racd.setPlanSuffix(racdcsnIO.getPlanSuffix().toInt());
	racd.setRasnum(racdcsnIO.getRasnum().toString());
	racd.setSeqno(racdcsnIO.getSeqno().toInt());
	racd.setValidflag(racdcsnIO.getValidflag().toString());
	racd.setCurrcode(racdcsnIO.getCurrcode().toString());
	racd.setRetype(racdcsnIO.getRetype().toString());
	racd.setRngmnt(racdcsnIO.getRngmnt().toString());
	racd.setRaAmount(racdcsnIO.getRaAmount().getbigdata());
	racd.setCmdate(racdcsnIO.getCmdate().toInt());
	racd.setReasper(racdcsnIO.getReasper().getbigdata());
	racd.setRrevdt(racdcsnIO.getRrevdt().toInt());
	racd.setRecovamt(racdcsnIO.getRecovamt().getbigdata());
	racd.setCestype(racdcsnIO.getCestype().toString());
	racd.setOvrdind(racdcsnIO.getOvrdind().toString());
	racd.setLrkcls(racdcsnIO.getLrkcls().toString());
	racd.setRrtdat(racdcsnIO.getRrtdat().toInt());
	racd.setRrtfrm(racdcsnIO.getRrtfrm().toInt());
	racdList.add(racd);
}

protected void sortArray9000()
	{
		sort9010();
	}

protected void sort9010()
	{
		/* Sort the components by priority within risk class.*/
		riskCompArrayInner.wsaaCount[wsaaRisk.toInt()].set(0);
		for (wsaaSort.set(1); !(isGT(wsaaSort, 99)
		|| isEQ(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaSort.toInt()], SPACES)); wsaaSort.add(1)){
			riskCompArrayInner.wsaaCount[wsaaRisk.toInt()].add(1);
		}
		wsaaNoMoreSwitches.set("N");
		while ( !(noMoreSwitches.isTrue())) {
			wsaaNextPointer.set(ZERO);
			wsaaNumSwitches.set(ZERO);
			for (wsaaPointer.set(1); !(isGT(wsaaPointer, riskCompArrayInner.wsaaCount[wsaaRisk.toInt()])); wsaaPointer.add(1)){
				compute(wsaaNextPointer, 0).set(add(1, wsaaPointer));
				if (isGT(riskCompArrayInner.wsaaRprior[wsaaRisk.toInt()][wsaaNextPointer.toInt()], riskCompArrayInner.wsaaRprior[wsaaRisk.toInt()][wsaaPointer.toInt()])) {
					wsaaNumSwitches.add(1);
					wsaaSortDetailsInner.wsaaSortDetails.set(riskCompArrayInner.wsaaCompDetails[wsaaRisk.toInt()][wsaaNextPointer.toInt()]);
					riskCompArrayInner.wsaaCompDetails[wsaaRisk.toInt()][wsaaNextPointer.toInt()].set(riskCompArrayInner.wsaaCompDetails[wsaaRisk.toInt()][wsaaPointer.toInt()]);
					riskCompArrayInner.wsaaCompDetails[wsaaRisk.toInt()][wsaaPointer.toInt()].set(wsaaSortDetailsInner.wsaaSortDetails);
				}
			}
			if (isEQ(wsaaNumSwitches, 0)) {
				wsaaNoMoreSwitches.set("Y");
			}
		}
		
	}

protected void checkCount9500()
	{
		start9510();
	}

protected void start9510()
	{
		/* Check each components within a Risk Class and total their sum   */
		/* insured for later use.                                          */
		riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()].set(0);
		if (isGT(riskCompArrayInner.wsaaCount[wsaaRisk.toInt()], 1)) {
			wsxxExceedCoAval = "N";
			for (wsxxCount.set(1); !(isGT(wsxxCount, 99)
			|| isEQ(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsxxCount.toInt()], SPACES)); wsxxCount.add(1)){
				if (isGT(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsxxCount.toInt()], riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()])
				|| isEQ(wsxxExceedCoAval, "Y")) {
					wsxxExceedCoAval = "Y";
					riskCompArrayInner.wsaaReaFlag[wsaaRisk.toInt()][wsxxCount.toInt()].set("Y");
				}
				else {
					riskCompArrayInner.wsaaReaFlag[wsaaRisk.toInt()][wsxxCount.toInt()].set("N");
				}
				compute(riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()], 0).set(add(riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()], riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsxxCount.toInt()]));
			}
		}
		else {
			riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()].set(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][1]);
		}
		compute(riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()], 0).set(sub(riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()], riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]));
		if (isLT(riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()], 0)
		|| isLTE(riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()], riskCompArrayInner.wsaaCoDiscRetn[wsaaRisk.toInt()])) {
			riskCompArrayInner.wsaaTotReass[wsaaRisk.toInt()].set(0);
		}
	}

protected void processArrangements10000()
	{
		process10010();
	}

protected void process10010()
	{
		/* For each component in the array, process all arrangements in*/
		/* order of hierarchy until the sum assured is completely ceded*/
		/* or until all arrangements are exhausted.*/
		for (wsaaComp.set(1); !(isGT(wsaaComp, riskCompArrayInner.wsaaCount[wsaaRisk.toInt()])); wsaaComp.add(1)){
			for (wsaaArr.set(1); !(isGT(wsaaArr, 10)
			|| isEQ(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)
			|| isEQ(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()], SPACES)); wsaaArr.add(1)){
				readT544911000();
				t5451rec.agerate.set(ZERO);
				t5451rec.oppc.set(ZERO);
				t5451rec.insprm.set(ZERO);
				if (isNE(t5449rec.sslivb, SPACES)) {
					readT545112000();
				}
				/* If the component has special terms and the arrangement*/
				/* does not allow special terms (i.e. the sub-standard lives*/
				/* allowed field on T5449 is not spaces), or if the component*/
				/* has special terms and these exceed the limits set for the*/
				/* arrangement on T5451, then do not process this arrangement.*/
				/* Otherwise, process the component as either quota share or*/
				/* surplus, depending on the arrangement cession type on T5449.*/
				if (((isNE(riskCompArrayInner.wsaaAgerate[wsaaRisk.toInt()][wsaaComp.toInt()], 0)
				|| isNE(riskCompArrayInner.wsaaOppc[wsaaRisk.toInt()][wsaaComp.toInt()], 0)
				|| isNE(riskCompArrayInner.wsaaInsprm[wsaaRisk.toInt()][wsaaComp.toInt()], 0))
				&& isEQ(t5449rec.subliv, SPACES))
				|| (isGT(riskCompArrayInner.wsaaAgerate[wsaaRisk.toInt()][wsaaComp.toInt()], t5451rec.agerate)
				&& isNE(t5451rec.agerate, 0))
				|| (isGT(riskCompArrayInner.wsaaOppc[wsaaRisk.toInt()][wsaaComp.toInt()], t5451rec.oppc)
				&& isNE(t5451rec.oppc, 0))
				|| (isGT(riskCompArrayInner.wsaaInsprm[wsaaRisk.toInt()][wsaaComp.toInt()], t5451rec.insprm)
				&& isNE(t5451rec.insprm, 0))) {
					/*           MOVE ZEROES       TO WSAA-SUMINS                   */
					/*                               (WSAA-RISK WSAA-COMP)          */
					/*           MOVE ZEROES       TO WSAA-NEWSUMIN                 */
					/*                               (WSAA-RISK WSAA-COMP)          */
					/*NEXT_SENTENCE*/
				}
				else {
					if (isEQ(t5449rec.rtytyp, "Q")){
						quotaShare13000();
					}
					else if (isEQ(t5449rec.rtytyp, "S")){
						surplus14000();
					}
				}
			}
			/* If the sum assured has not been completely ceded, create a*/
			/* facultative RACD record for the remaining amount.*/
			if (isNE(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)) {
				wsaaCessAmt.set(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()]);
				if (isEQ(csncalcrec.function, "ADJU") || isEQ(csncalcrec.function, "DECR")) {
					checkRacdFaclRecord();
				} else {
					racdIO.setParams(SPACES);
					racdIO.setRetype("F");
					writeRacd15000();
				}
			} else if ((isEQ(csncalcrec.function, "ADJU") || isEQ(csncalcrec.function, "DECR")) && isEQ(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)) {
				Racdpf racd = new Racdpf();
				racd.setChdrcoy(csncalcrec.chdrcoy.toString());
				racd.setChdrnum(csncalcrec.chdrnum.toString());
				racd.setLife(csncalcrec.life.toString());
				/*racd.setCoverage(csncalcrec.coverage.toString());
				racd.setRider(csncalcrec.rider.toString());
				racd.setPlanSuffix(csncalcrec.planSuffix.toInt());*/
				racd.setCoverage(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()].toString());
				racd.setRider(riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()].toString());
				racd.setPlanSuffix(riskCompArrayInner.wsaaPlnsfx[wsaaRisk.toInt()][wsaaComp.toInt()].toInt());
				racd.setRetype("F");
				List<Racdpf> racdFaclList = racdDAO.getRacdFaclRecord(racd);
				if(!racdFaclList.isEmpty()) {
					for(Racdpf r : racdFaclList) {
						trmracdrec.newRaAmt.set(ZERO);
						trmracdrec.function.set("TRMR");
						callTrmracd(r);
					}
				}
			}
		}
	}

protected void checkRacdFaclRecord() {
	Racdpf racd = new Racdpf();
	racd.setChdrcoy(csncalcrec.chdrcoy.toString());
	racd.setChdrnum(csncalcrec.chdrnum.toString());
	racd.setLife(csncalcrec.life.toString());
	//PINNACLE-2323 START
	racd.setCoverage(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()].toString());
	racd.setRider(riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()].toString());
	racd.setPlanSuffix(riskCompArrayInner.wsaaPlnsfx[wsaaRisk.toInt()][wsaaComp.toInt()].toInt());
	//PINNACLE-2323 END
	racd.setRetype("F");
	List<Racdpf> racdFaclList = racdDAO.getRacdFaclRecord(racd);
	if(!racdFaclList.isEmpty()) {
		String rngmnt="";
		String rasnum="";
		for(int i=0;i<racdFaclList.size();i++) {
			if(isNewTreatyCreated) {
				if(i==0) {
					rngmnt = racdFaclList.get(i).getRngmnt();
					rasnum = racdFaclList.get(i).getRasnum();
				}
				trmracdrec.newRaAmt.set(ZERO);
				trmracdrec.function.set("TRMR");
				callTrmracd(racdFaclList.get(i));
			} else {
				if(i==0) {
					trmracdrec.newRaAmt.set(wsaaCessAmt);
					trmracdrec.function.set("CHGR");
					callTrmracd(racdFaclList.get(i));
				} else {
					trmracdrec.newRaAmt.set(ZERO);
					trmracdrec.function.set("TRMR");
					callTrmracd(racdFaclList.get(i));
				}
			}
		}
		if(isNewTreatyCreated) {
			racdIO.setParams(SPACES);
			racdIO.setRetype("F");
			racdIO.setOvrdind("Y");
			racdIO.setRngmnt(rngmnt);
			racdIO.setRasnum(rasnum);
			newFacl = false; //PINNACLE-2323
			writeRacd15000();
			newFacl = true; //PINNACLE-2323
		}
	}
}

protected void readT544911000()
	{
		t544911010();
	}

protected void t544911010()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(csncalcrec.chdrcoy);
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
		itdmIO.setItmfrm(riskCompArrayInner.wsaaCrrcd[wsaaRisk.toInt()][wsaaComp.toInt()]);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()], itdmIO.getItemitem())
		|| isNE(csncalcrec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5449)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
			itdmIO.setItemcoy(csncalcrec.chdrcoy);
			itdmIO.setItemtabl(t5449);
			itdmIO.setStatuz(r057);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5449rec.t5449Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT545112000()
	{
		t545112010();
	}

protected void t545112010()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(csncalcrec.chdrcoy);
		itdmIO.setItemtabl(t5451);
		itdmIO.setItemitem(t5449rec.sslivb);
		itdmIO.setItmfrm(riskCompArrayInner.wsaaCrrcd[wsaaRisk.toInt()][wsaaComp.toInt()]);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(t5449rec.sslivb, itdmIO.getItemitem())
		|| isNE(csncalcrec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5451)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t5449rec.sslivb);
			itdmIO.setItemcoy(csncalcrec.chdrcoy);
			itdmIO.setItemtabl(t5451);
			itdmIO.setStatuz(r054);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		else {
			t5451rec.t5451Rec.set(itdmIO.getGenarea());
		}
	}

protected void quotaShare13000()
	{
		/*QUOTA*/
		/* The method used for quota share cessions varies depending*/
		/* on whether the company has a quota share or not. The quota*/
		/* share for the company is found on T5449. If the company has*/
		/* a quota share, then the company will retain that share of*/
		/* the sum assured up to its own retention limit for the life.*/
		if (isGT(t5449rec.qcoshr, 0)) {
			quotaQuota13100();
		}
		else {
			surplusQuota13200();
		}
		/*EXIT*/
	}

protected void quotaQuota13100()
	{
		quota13110();
	}

protected void quota13110()
	{
		/*If sum at risk less than or equal to T5446 retention, no<CAS1.0>*/
		/*reassurance applies.                                    <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/*  IF WSAA-SUMINS(WSAA-RISK WSAA-COMP)                 <CAS1.0>*/
		/*                            <= WSAA-CO-AVAL-RETN      <CAS1.0>*/
		/*                               (WSAA-RISK )           <CAS1.0>*/
		/*      MOVE O-K               TO CSNC-STATUZ           <CAS1.0>*/
		/*      GO TO 13190-EXIT                                <CAS1.0>*/
		/*  END-IF.                                             <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		if (isGT(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()], riskCompArrayInner.wsaaCoDiscRetn[wsaaRisk.toInt()])) {
			/*CONTINUE_STMT*/
		}
		else {
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(ZERO);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(ZERO);
			return ;
		}
		/* Calculate company share.*/
		compute(wsaaCoShare, 2).set(div(mult(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()], t5449rec.qcoshr), 100));
		zrdecplrec.amountIn.set(wsaaCoShare);
		a000CallRounding();
		wsaaCoShare.set(zrdecplrec.amountOut);
		/* Calculate maximum sum assured for this arrangement.*/
		if (isGT(wsaaCoShare, riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()])
		|| isEQ(wsaaCoShare, riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()])) {
			compute(wsaaSaMax, 2).set(div(mult(riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()], riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()]), wsaaCoShare));
			wsaaCoShare.set(riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]);
			riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].set(0);
		}
		else {
			riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].subtract(wsaaCoShare);
			wsaaSaMax.set(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()]);
		}
		/* Reduce sum assured by company share.*/
		riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCoShare);
		riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCoShare);
		/* Calculate cession amount.*/
		for (wsaaT5449Ix.set(1); !(isGT(wsaaT5449Ix, 10)
		|| isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()], SPACES)
		|| isEQ(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)); wsaaT5449Ix.add(1)){
			compute(wsaaCessAmt, 2).set(div(mult(wsaaSaMax, t5449rec.qreshr[wsaaT5449Ix.toInt()]), 100));
			zrdecplrec.amountIn.set(wsaaCessAmt);
			a000CallRounding();
			wsaaCessAmt.set(zrdecplrec.amountOut);
			getReasRetn13300();
			if (isGT(wsaaCessAmt, wsaaReAvalRetn[wsaaRetn.toInt()])) {
				wsaaCessAmt.set(wsaaReAvalRetn[wsaaRetn.toInt()]);
			}
			wsaaReAvalRetn[wsaaRetn.toInt()].subtract(wsaaCessAmt);
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCessAmt);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCessAmt);
			isNewTreatyCreated = false;
			if (isGT(wsaaCessAmt, 0)) {
				if (isEQ(csncalcrec.function, "ADJU")
						|| isEQ(csncalcrec.function, "DECR")) {
					checkRacdTreatyRecord();
				} else {
					racdIO.setParams(SPACES);
					racdIO.setRetype(t5449rec.retype);
					racdIO.setRngmnt(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
					racdIO.setRasnum(t5449rec.rasnum[wsaaT5449Ix.toInt()]);
					writeRacd15000();
				}
			}
		}
	}

protected void checkRacdTreatyRecord() {
	Racdpf racd = new Racdpf();
	racd.setChdrcoy(csncalcrec.chdrcoy.toString());
	racd.setChdrnum(csncalcrec.chdrnum.toString());
	racd.setLife(csncalcrec.life.toString());
	//PINNACLE-2323 START
	racd.setCoverage(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()].toString());
	racd.setRider(riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()].toString());
	racd.setPlanSuffix(riskCompArrayInner.wsaaPlnsfx[wsaaRisk.toInt()][wsaaComp.toInt()].toInt());
	//PINNACLE-2323 END
	racd.setRetype(t5449rec.retype.toString());
	racd.setRngmnt(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()].toString());
	racd.setRasnum(t5449rec.rasnum[wsaaT5449Ix.toInt()].toString());
	List<Racdpf> racdpfList = racdDAO.getRacdRecord(racd);
	PackedDecimalData wsaaTotSumins = new PackedDecimalData(17, 2).init(0);
	PackedDecimalData wsaaSaDiff = new PackedDecimalData(17, 2).init(0);
	if(!racdpfList.isEmpty()) {
		for(Racdpf r:racdpfList) {
			compute(wsaaTotSumins, 2).set(add(wsaaTotSumins,r.getRaAmount()));
		}
		compute(wsaaSaDiff, 2).set(sub(wsaaTotSumins,wsaaCessAmt));
		for(Racdpf r:racdpfList) {
			if(isGT(wsaaSaDiff,ZERO)) {
				if (isLTE(r.getRaAmount(),wsaaSaDiff)) {
					trmracdrec.newRaAmt.set(ZERO);
					trmracdrec.function.set("TRMR");
					rcorfndrec.newSumins.set(0);
					refundSubrCall(r);
					callTrmracd(r);
					compute(wsaaSaDiff, 2).set(sub(wsaaSaDiff,r.getRaAmount()));
				}
				else {
					compute(trmracdrec.newRaAmt, 2).set(sub(r.getRaAmount(),wsaaSaDiff));
					trmracdrec.function.set("CHGR");
					rcorfndrec.newSumins.set(trmracdrec.newRaAmt);
					refundSubrCall(r);
					callTrmracd(r);
					wsaaSaDiff.set(ZERO);
				}
			} else if(isLT(wsaaSaDiff,ZERO)) {
				compute(wsaaSaDiff, 2).set(mult(wsaaSaDiff,-1));
				compute(trmracdrec.newRaAmt, 2).set(add(r.getRaAmount(),wsaaSaDiff));
				trmracdrec.function.set("CHGR");
				callTrmracd(r);
				wsaaSaDiff.set(ZERO);
			} else {
				//PINNACLE-2323 START
				compute(trmracdrec.newRaAmt, 2).set(wsaaCessAmt);
				trmracdrec.function.set("CHGR");
				callTrmracd(r);
				//PINNACLE-2323 END
			}
		}
	} else {
		isNewTreatyCreated = true;
		racdIO.setParams(SPACES);
		racdIO.setRetype(t5449rec.retype);
		racdIO.setRngmnt(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
		racdIO.setRasnum(t5449rec.rasnum[wsaaT5449Ix.toInt()]);
		writeRacd15000();
	}
}

protected void readT54462100() {
	itdmIO.setParams(SPACES);
	itdmIO.setItemcoy(csncalcrec.chdrcoy);
	itdmIO.setItemtabl(t5446);
	/*    MOVE T5448-LRKCLS           TO ITDM-ITEMITEM.                */
	itdmIO.setItemitem(wsxxLrkcls);
	itdmIO.setItmfrm(varcom.vrcmMaxDate);
	itdmIO.setFormat(itdmrec);
	itdmIO.setFunction(varcom.begn);
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		dbError580();
	}
	/*    IF ITDM-ITEMITEM            NOT = T5448-LRKCLS               */
	if (isNE(itdmIO.getItemitem(), wsxxLrkcls)
	|| isNE(itdmIO.getItemcoy(), csncalcrec.chdrcoy)
	|| isNE(itdmIO.getItemtabl(), t5446)
	|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
		/*        MOVE T5448-LRKCLS       TO ITDM-ITEMITEM                 */
		itdmIO.setItemitem(wsxxLrkcls);
		itdmIO.setItemcoy(csncalcrec.chdrcoy);
		itdmIO.setItemtabl(t5446);
		itdmIO.setStatuz("R051");
		syserrrec.params.set(itdmIO.getParams());
		dbError580();
	}
	else {
		t5446rec.t5446Rec.set(itdmIO.getGenarea());
	}
}

protected void callXcvrt3000() {
	conlinkrec.rateUsed.set(ZERO);
	conlinkrec.amountOut.set(ZERO);
	conlinkrec.cashdate.set(csncalcrec.effdate);
	conlinkrec.company.set(csncalcrec.chdrcoy);
	conlinkrec.function.set("REAL");
	callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
	if (isNE(conlinkrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(conlinkrec.statuz);
		syserrrec.params.set(conlinkrec.clnk002Rec);
		syserr570();
	}
}

protected void callTrmracd(Racdpf racdpf) {
	trmracdrec.statuz.set(varcom.oK);
	trmracdrec.chdrcoy.set(csncalcrec.chdrcoy);
	trmracdrec.chdrnum.set(csncalcrec.chdrnum);
	trmracdrec.life.set(racdpf.getLife());
	trmracdrec.coverage.set(racdpf.getCoverage());
	trmracdrec.rider.set(racdpf.getRider());
	trmracdrec.planSuffix.set(racdpf.getPlanSuffix());
	trmracdrec.seqno.set(racdpf.getSeqno());
	trmracdrec.lrkcls.set(racdpf.getLrkcls());
	trmracdrec.l1Clntnum.set(wsaaLifenum);
	trmracdrec.l2Clntnum.set(wsaaJLifenum);
	trmracdrec.clntcoy.set(csncalcrec.fsuco);
	trmracdrec.effdate.set(csncalcrec.effdate);
	trmracdrec.tranno.set(csncalcrec.tranno);
	//PINNACLE-2323
	/*if (isEQ(csncalcrec.function, "DECR")) {
		trmracdrec.crtable.set(covtcsnIO.getCrtable());
		trmracdrec.jlife.set(covtcsnIO.getJlife());
		trmracdrec.crrcd.set(covtcsnIO.getEffdate());
	}*/
	/*else {*/
		trmracdrec.crtable.set(covrlnbIO.getCrtable());
		trmracdrec.jlife.set(covrlnbIO.getJlife());
		trmracdrec.crrcd.set(covrlnbIO.getCrrcd());
	/*}*/
	trmracdrec.cnttype.set(csncalcrec.cnttype);
	trmracdrec.recovamt.set(ZERO);
	trmracdrec.batctrcde.set(csncalcrec.batctrcde);
	callProgram(Trmracd.class, trmracdrec.trmracdRec);
	if (isNE(trmracdrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(trmracdrec.statuz);
		syserrrec.params.set(trmracdrec.trmracdRec);
		syserr570();
	}
}


protected void surplusQuota13200()
	{
		quota13210();
	}

protected void quota13210()
	{
		/* Calculate surplus above company retention.*/
		compute(wsaaSurplus, 2).set(sub(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()], riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]));
		/*    IF WSAA-SURPLUS             = 0                              */
		/*    OR WSAA-SURPLUS             > 0                      <LA3293>*/
		/*        SUBTRACT WSAA-CO-AVAL-RETN (WSAA-RISK) FROM      <LA3293>*/
		/*                 WSAA-SUMINS (WSAA-RISK WSAA-COMP)       <LA3293>*/
		/*        MOVE 0         TO WSAA-CO-AVAL-RETN (WSAA-RISK) <LA3293> */
		/*    ELSE                                                         */
		/*        SUBTRACT WSAA-SUMINS (WSAA-RISK WSAA-COMP)               */
		/*            FROM WSAA-CO-AVAL-RETN (WSAA-RISK)                   */
		/*        MOVE WSAA-SURPLUS       TO WSAA-SUMINS                   */
		/*                                  (WSAA-RISK WSAA-COMP)          */
		/*    END-IF.                                                      */
		if (isGT(wsaaSurplus, 0)) {
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]);
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]);
			riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].set(0);
		}
		else {
			riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].subtract(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()]);
			wsaaSurplus.set(0);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
		}
		wsaaStoreCess.set(ZERO);
		/* Calculate cession amount.*/
		wsaaTotalRiskSumins.set(ZERO);
		for (ix.set(wsaaComp); !(isGT(ix, 99)
		|| isEQ(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][ix.toInt()], ZERO)); ix.add(1)){
			wsaaTotalRiskSumins.add(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][ix.toInt()]);
		}
		/*    IF WSAA-SUMINS (WSAA-RISK WSAA-COMP)                         */
		/*                                > WSAA-CO-DISC-RETN (WSAA-RISK)  */
		if (isGT(wsaaTotalRiskSumins, riskCompArrayInner.wsaaCoDiscRetn[wsaaRisk.toInt()])) {
			for (wsaaT5449Ix.set(1); !(isGT(wsaaT5449Ix, 10)
			|| isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()], SPACES)
			|| isEQ(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)); wsaaT5449Ix.add(1)){
				compute(wsaaCessAmt, 2).set(div(mult(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()], t5449rec.qreshr[wsaaT5449Ix.toInt()]), 100));
				zrdecplrec.amountIn.set(wsaaCessAmt);
				a000CallRounding();
				wsaaCessAmt.set(zrdecplrec.amountOut);
				getReasRetn13300();
				if (isGT(wsaaCessAmt, wsaaReAvalRetn[wsaaRetn.toInt()])) {
					wsaaCessAmt.set(wsaaReAvalRetn[wsaaRetn.toInt()]);
				}
				wsaaReAvalRetn[wsaaRetn.toInt()].subtract(wsaaCessAmt);
				/*       SUBTRACT WSAA-CESS-AMT                                 */
				/*           FROM WSAA-SUMINS (WSAA-RISK WSAA-COMP)             */
				wsaaStoreCess.add(wsaaCessAmt);
				riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCessAmt);
				if (isGT(wsaaCessAmt, 0)) {
					racdIO.setParams(SPACES);
					racdIO.setRetype(t5449rec.retype);
					racdIO.setRngmnt(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
					racdIO.setRasnum(t5449rec.rasnum[wsaaT5449Ix.toInt()]);
					writeRacd15000();
				}
			}
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaStoreCess);
		}
		else {
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(ZERO);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(ZERO);
		}
	}

protected void getReasRetn13300()
	{
		retn13310();
	}

protected void retn13310()
	{
		/* Search through array for the reassurer arrangement retention.*/
		/* If not found, get the available retention for the life assured*/
		/* by calling RTNCALC. If joint life, calculated the available*/
		/* retention for the joint life by calling RTNCALC. Store the*/
		/* lower of the two in the array. If the retention currency is*/
		/* not equal to the contract currency, use XCVRT to convert the*/
		/* retention amounts.*/
		wsaaRetentionFound.set("Y");
		for (wsaaSearch.set(1); !(isGT(wsaaSearch, 99)
		|| isEQ(wsaaReassurer[wsaaSearch.toInt()], SPACES)
		|| (isEQ(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()], wsaaArrangement[wsaaSearch.toInt()])
		&& isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()], wsaaReassurer[wsaaSearch.toInt()]))); wsaaSearch.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSearch, 99)) {
			syserrrec.statuz.set(e103);
			syserr570();
		}
		else {
			wsaaRetn.set(wsaaSearch);
		}
		if (isEQ(wsaaReassurer[wsaaRetn.toInt()], SPACES)) {
			wsaaRetentionFound.set("N");
		}
		if (!retentionFound.isTrue()) {
			rtncalcrec.function.set("RESR");
			rtncalcrec.clntnum.set(wsaaLifenum);
			rtncalcrec.riskClass.set(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()]);
			rtncalcrec.arrangement.set(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
			rtncalcrec.reassurer.set(t5449rec.rasnum[wsaaT5449Ix.toInt()]);
			retention5000();
			if (jlifeExists.isTrue()) {
				wsaaAvailable.set(rtncalcrec.available);
				rtncalcrec.function.set("RESR");
				rtncalcrec.clntnum.set(wsaaLifenum);
				rtncalcrec.riskClass.set(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()]);
				rtncalcrec.arrangement.set(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
				rtncalcrec.reassurer.set(t5449rec.rasnum[wsaaT5449Ix.toInt()]);
				retention5000();
				if (isLT(wsaaAvailable, rtncalcrec.available)) {
					rtncalcrec.available.set(wsaaAvailable);
				}
			}
			wsaaReAvalRetn[wsaaRetn.toInt()].set(rtncalcrec.available);
			wsaaArrangement[wsaaRetn.toInt()].set(rtncalcrec.arrangement);
			wsaaReassurer[wsaaRetn.toInt()].set(rtncalcrec.reassurer);
		}
	}

protected void surplus14000()
	{
		surplus14010();
	}

protected void surplus14010()
	{
		/* Calculate surplus above company retention.*/
		compute(wsaaSurplus, 2).set(sub(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()], riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]));
		if (isGT(wsaaSurplus, 0)) {
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()]);
			riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].set(0);
		}
		else {
			riskCompArrayInner.wsaaCoAvalRetn[wsaaRisk.toInt()].subtract(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()]);
			wsaaSurplus.set(0);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
		}
		/* Reduce sum assured left to reassure for the component.*/
		riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(wsaaSurplus);
		/* Calculate cession amount.*/
		wsaaTotalRiskSumins.set(ZERO);
		for (ix.set(wsaaComp); !(isGT(ix, 99)
		|| isEQ(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][ix.toInt()], ZERO)); ix.add(1)){
			wsaaTotalRiskSumins.add(riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][ix.toInt()]);
		}
		/*    IF WSAA-NEWSUMIN (WSAA-RISK WSAA-COMP)                       */
		/*                                > WSAA-CO-DISC-RETN (WSAA-RISK)  */
		if (isGT(wsaaTotalRiskSumins, riskCompArrayInner.wsaaCoDiscRetn[wsaaRisk.toInt()])) {
			for (wsaaT5449Ix.set(1); !(isGT(wsaaT5449Ix, 10)
			|| isEQ(t5449rec.rasnum[wsaaT5449Ix.toInt()], SPACES)
			|| isEQ(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], 0)); wsaaT5449Ix.add(1)){
				getReasRetn13300();
				if (isGT(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()], wsaaReAvalRetn[wsaaRetn.toInt()])) {
					wsaaCessAmt.set(wsaaReAvalRetn[wsaaRetn.toInt()]);
					wsaaReAvalRetn[wsaaRetn.toInt()].set(0);
					riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCessAmt);
					riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].subtract(wsaaCessAmt);
				}
				else {
					wsaaCessAmt.set(riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()]);
					riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
					riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
					wsaaReAvalRetn[wsaaRetn.toInt()].subtract(wsaaCessAmt);
				}
				if (isGT(wsaaCessAmt, 0)) {
					racdIO.setParams(SPACES);
					racdIO.setRetype(t5449rec.retype);
					racdIO.setRngmnt(riskCompArrayInner.wsaaRngmnt[wsaaRisk.toInt()][wsaaComp.toInt()][wsaaArr.toInt()]);
					racdIO.setRasnum(t5449rec.rasnum[wsaaT5449Ix.toInt()]);
					writeRacd15000();
				}
			}
		}
		else {
			riskCompArrayInner.wsaaSumins[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
			riskCompArrayInner.wsaaNewsumin[wsaaRisk.toInt()][wsaaComp.toInt()].set(0);
		}
	}

protected void writeRacd15000()
	{
		racd15010();
	}

protected void racd15010()
	{
		racdIO.setTranno(csncalcrec.tranno);
		racdIO.setChdrcoy(csncalcrec.chdrcoy);
		racdIO.setChdrnum(csncalcrec.chdrnum);
		racdIO.setLife(csncalcrec.life);
		racdIO.setCoverage(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()]);
		racdIO.setRider(riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()]);
		racdIO.setPlanSuffix(riskCompArrayInner.wsaaPlnsfx[wsaaRisk.toInt()][wsaaComp.toInt()]);
		racdIO.setLrkcls(riskCompArrayInner.wsaaRiskClass[wsaaRisk.toInt()]);
		racdIO.setSeqno(ZERO);
		racdIO.setReasper(ZERO);
		racdIO.setRecovamt(ZERO);
		racdIO.setValidflag("3");
		racdIO.setCurrcode(chdrlnbIO.getCntcurr());
		racdIO.setCurrfrom(csncalcrec.effdate);
		racdIO.setCtdate(csncalcrec.effdate);
		racdIO.setCmdate(csncalcrec.effdate);
		racdIO.setRrtdat(csncalcrec.effdate.toInt());
		racdIO.setRrtfrm(csncalcrec.effdate.toInt());
		racdIO.setCurrto(varcom.vrcmMaxDate);
		if (isEQ(racdIO.getRetype(), "T")) {
			racdIO.setCestype("1");
		}
		else {
			racdIO.setCestype("2");
			//PINNACLE-2323 START
			if(newFacl) {
				if(isEQ(racdIO.getRasnum(),SPACES))  //IBPLIFE-13960
					csncalcrec.statuz.set("FACL");
				csncalcrec.coverage.set(riskCompArrayInner.wsaaCoverage[wsaaRisk.toInt()][wsaaComp.toInt()]);
				csncalcrec.rider.set(riskCompArrayInner.wsaaRider[wsaaRisk.toInt()][wsaaComp.toInt()]);
			}
			//PINNACLE-2323 END
		}
		racdIO.setRaAmount(wsaaCessAmt);
		/* The review date is calculated using the review frequency from*/
		/* T5448. It must be the nearest date after the cession commence*/
		/* date that is a factor of the contract commence date.*/
		/* For example:*/
		/* Contract comennce date:  01/01/1995*/
		/* Cession commence date:   14/06/1995*/
		/* Review frequency:        Quarterly*/
		/* Review date:             01/07/1995*/
		/* After review, the next review date would be set to: 01/10/1995.*/
		/* The review date is set in line with to the contract commence*/
		/* date to facilitate review processing.*/
		if (isNE(riskCompArrayInner.wsaaRrevfrq[wsaaRisk.toInt()][wsaaComp.toInt()], SPACES)) {
			calcReviewDate15500();
			/*        MOVE DTC2-INT-DATE-2    TO RACD-RREVDT                   */
			racdIO.setRrevdt(datcon4rec.intDate2);
		}
		else {
			racdIO.setRrevdt(varcom.vrcmMaxDate);
		}
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
	}

protected void calcReviewDate15500()
	{
		calc15510();
	}

protected void calc15510()
	{
		//ILIFE-5253 start  by dpuhawan
		chdrlnbIO.setFunction(varcom.readr); 
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK) && isNE(chdrlnbIO.getStatuz(),varcom.mrnf))//ILIFE-5324
		{
			syserrrec.params.set(chdrlnbIO.getParams());
			dbError580();
		}	
		//ILIFE-5253 end
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon3rec.intDate2.set(racdIO.getCmdate());
		datcon3rec.frequency.set(riskCompArrayInner.wsaaRrevfrq[wsaaRisk.toInt()][wsaaComp.toInt()]);
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			syserr570();
		}
		/*    MOVE SPACE                  TO DTC2-DATCON2-REC              */
		/*    MOVE CHDRLNB-OCCDATE        TO DTC2-INT-DATE-1               */
		/*    MOVE 0                      TO DTC2-INT-DATE-2               */
		/*    MOVE WSAA-RREVFRQ(WSAA-RISK WSAA-COMP)                       */
		/*                                TO DTC2-FREQUENCY                */
		/*    ADD 1 TO DTC3-FREQ-FACTOR   GIVING DTC2-FREQ-FACTOR          */
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC           */
		/*    IF DTC2-STATUZ              NOT = O-K                        */
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS                   */
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 570-SYSERR                                       */
		/*    END-IF.                                                      */
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(chdrlnbIO.getOccdate());
		wsaaOccdate.set(chdrlnbIO.getOccdate());
		datcon4rec.intDate2.set(0);
		datcon4rec.frequency.set(riskCompArrayInner.wsaaRrevfrq[wsaaRisk.toInt()][wsaaComp.toInt()]);
		compute(datcon4rec.freqFactor, 5).set(add(1, datcon3rec.freqFactor));
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			syserr570();
		}
	}

protected void callXcvrt16000()
	{
		call16010();
	}

protected void call16010()
	{
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(csncalcrec.effdate);
		conlinkrec.company.set(csncalcrec.chdrcoy);
		/* MOVE 'SURR'                 TO CLNK-FUNCTION.                */
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		csncalcrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		csncalcrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(csncalcrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(csncalcrec.currency);
		zrdecplrec.batctrcde.set(csncalcrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A900-EXIT*/
	}

protected void refundSubrCall(Racdpf racdpf) {
	readT5472();
	rcorfndrec.statuz.set(varcom.oK);
	rcorfndrec.function.set("UPDT");
	rcorfndrec.chdrcoy.set(racdpf.getChdrcoy());
	rcorfndrec.chdrnum.set(racdpf.getChdrnum());
	rcorfndrec.life.set(racdpf.getLife());
	rcorfndrec.coverage.set(racdpf.getCoverage());
	rcorfndrec.rider.set(racdpf.getRider());
	rcorfndrec.planSuffix.set(racdpf.getPlanSuffix());
	rcorfndrec.seqno.set(racdpf.getSeqno());
	rcorfndrec.currfrom.set(racdpf.getCurrfrom());
	rcorfndrec.tranno.set(racdpf.getTranno());
	rcorfndrec.language.set(csncalcrec.language);
	rcorfndrec.effdate.set(csncalcrec.effdate);
	rcorfndrec.crtable.set(covtcsnIO.getCrtable());
	rcorfndrec.cnttype.set(csncalcrec.cnttype);
	rcorfndrec.batctrcde.set(csncalcrec.batctrcde);
	rcorfndrec.batckey.set(csncalcrec.batckey);
	callProgram(t5472rec.addprocess, rcorfndrec.rcorfndRec);
	if (isNE(rcorfndrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(rcorfndrec.statuz);
		syserrrec.params.set(rcorfndrec.rcorfndRec);
		syserr570();
	}
}

protected void readT5472()
{
	t5472();
}

protected void t5472()
{
	itemIO.setParams(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(csncalcrec.chdrcoy);
	itemIO.setItemtabl(t5472);
	StringUtil stringVariable2 = new StringUtil();
	stringVariable2.addExpression(t5448rec.rclmbas);
	stringVariable2.addExpression(csncalcrec.batctrcde);
	stringVariable2.setStringInto(wsaaT5472Key);
	itemIO.setItemitem(wsaaT5472Key);
	itemIO.setFormat(itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		dbError580();
	}
	t5472rec.t5472Rec.set(itemIO.getGenarea());
}
/*
 * Class transformed  from Data Structure RISK-COMP-ARRAY--INNER
 */
private static final class RiskCompArrayInner { 

		/* The following 3-dimensional array is used to store component
		 information within each risk class.*/
	private FixedLengthStringData riskCompArray = new FixedLengthStringData(182820);
	private FixedLengthStringData[] riskClassDetails = FLSArrayPartOfStructure(20, 9141, riskCompArray, 0);
	private FixedLengthStringData[] wsaaRiskClass = FLSDArrayPartOfArrayStructure(4, riskClassDetails, 0);
	private PackedDecimalData[] wsaaCount = PDArrayPartOfArrayStructure(3, 0, riskClassDetails, 4);
	private PackedDecimalData[] wsaaCoAvalRetn = PDArrayPartOfArrayStructure(17, 2, riskClassDetails, 6);
	private PackedDecimalData[] wsaaCoDiscRetn = PDArrayPartOfArrayStructure(17, 2, riskClassDetails, 15);
	private PackedDecimalData[] wsaaTotReass = PDArrayPartOfArrayStructure(17, 2, riskClassDetails, 24);
	private FixedLengthStringData[][] wsaaCompDetails = FLSDArrayPartOfArrayStructure(99, 92, riskClassDetails, 33);
	private FixedLengthStringData[][] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCompDetails, 0);
	private FixedLengthStringData[][] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCompDetails, 2);
	private PackedDecimalData[][] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaCompDetails, 4);
	private FixedLengthStringData[][] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCompDetails, 7);
	private PackedDecimalData[][] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaCompDetails, 11);
	private PackedDecimalData[][] wsaaSumins = PDArrayPartOfArrayStructure(17, 2, wsaaCompDetails, 16);
	private PackedDecimalData[][] wsaaNewsumin = PDArrayPartOfArrayStructure(17, 2, wsaaCompDetails, 25);
	private FixedLengthStringData[][] wsaaReaFlag = FLSDArrayPartOfArrayStructure(1, wsaaCompDetails, 34);
	private ZonedDecimalData[][] wsaaRprior = ZDArrayPartOfArrayStructure(2, 0, wsaaCompDetails, 35, UNSIGNED_TRUE);
	private FixedLengthStringData[][] wsaaRresmeth = FLSDArrayPartOfArrayStructure(4, wsaaCompDetails, 37);
	private FixedLengthStringData[][] wsaaRrevfrq = FLSDArrayPartOfArrayStructure(2, wsaaCompDetails, 41);
	private PackedDecimalData[][] wsaaAgerate = PDArrayPartOfArrayStructure(3, 0, wsaaCompDetails, 43);
	private PackedDecimalData[][] wsaaOppc = PDArrayPartOfArrayStructure(5, 2, wsaaCompDetails, 45);
	private PackedDecimalData[][] wsaaInsprm = PDArrayPartOfArrayStructure(6, 0, wsaaCompDetails, 48);
	private FixedLengthStringData[][][] wsaaRngmnts = FLSDArrayPartOfArrayStructure(10, 4, wsaaCompDetails, 52);
	private FixedLengthStringData[][][] wsaaRngmnt = FLSDArrayPartOfArrayStructure(4, wsaaRngmnts, 0);
}
/*
 * Class transformed  from Data Structure WSAA-SORT-DETAILS--INNER
 */
private static final class WsaaSortDetailsInner { 

		/* This is a copy of the WSAA-COMP-DETAILS array (see above), used
		 to sort the component details by priority within risk class.*/
	private FixedLengthStringData wsaaSortDetails = new FixedLengthStringData(94);
	private FixedLengthStringData wsaaSortCovr = new FixedLengthStringData(2).isAPartOf(wsaaSortDetails, 0);
	private FixedLengthStringData wsaaSortRider = new FixedLengthStringData(2).isAPartOf(wsaaSortDetails, 2);
	private PackedDecimalData wsaaSortPlnsfx = new PackedDecimalData(4, 0).isAPartOf(wsaaSortDetails, 4);
	private FixedLengthStringData wsaaSortCrtab = new FixedLengthStringData(4).isAPartOf(wsaaSortDetails, 7);
	private PackedDecimalData wsaaSortCrrcd = new PackedDecimalData(8, 0).isAPartOf(wsaaSortDetails, 11);
	private PackedDecimalData wsaaSortSumin = new PackedDecimalData(17, 2).isAPartOf(wsaaSortDetails, 16);
	private PackedDecimalData wsaaSortNewsumin = new PackedDecimalData(17, 2).isAPartOf(wsaaSortDetails, 25);
	private FixedLengthStringData wsaaSortReaFlag = new FixedLengthStringData(1).isAPartOf(wsaaSortDetails, 34);
	private ZonedDecimalData wsaaSortPrior = new ZonedDecimalData(2, 0).isAPartOf(wsaaSortDetails, 35).setUnsigned();
	private FixedLengthStringData wsaaSortRmeth = new FixedLengthStringData(4).isAPartOf(wsaaSortDetails, 37);
	private FixedLengthStringData wsaaSortRevfrq = new FixedLengthStringData(4).isAPartOf(wsaaSortDetails, 41);
	private PackedDecimalData wsaaSortAge = new PackedDecimalData(3, 0).isAPartOf(wsaaSortDetails, 45);
	private PackedDecimalData wsaaSortOppc = new PackedDecimalData(5, 2).isAPartOf(wsaaSortDetails, 47);
	private PackedDecimalData wsaaSortPrem = new PackedDecimalData(6, 0).isAPartOf(wsaaSortDetails, 50);
	private FixedLengthStringData[] wsaaSortRngmnts = FLSArrayPartOfStructure(10, 4, wsaaSortDetails, 54);
	private FixedLengthStringData[] wsaaSortRngmnt = FLSDArrayPartOfArrayStructure(4, wsaaSortRngmnts, 0);
}
}
