package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:06
 * Description:
 * Copybook name: T6662REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6662rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6662Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData reascodes = new FixedLengthStringData(120).isAPartOf(t6662Rec, 0);
  	public FixedLengthStringData[] reascode = FLSArrayPartOfStructure(30, 4, reascodes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(reascodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData reascode01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData reascode02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData reascode03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData reascode04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData reascode05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData reascode06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData reascode07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData reascode08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData reascode09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData reascode10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData reascode11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData reascode12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData reascode13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData reascode14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData reascode15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData reascode16 = new FixedLengthStringData(4).isAPartOf(filler, 60);
  	public FixedLengthStringData reascode17 = new FixedLengthStringData(4).isAPartOf(filler, 64);
  	public FixedLengthStringData reascode18 = new FixedLengthStringData(4).isAPartOf(filler, 68);
  	public FixedLengthStringData reascode19 = new FixedLengthStringData(4).isAPartOf(filler, 72);
  	public FixedLengthStringData reascode20 = new FixedLengthStringData(4).isAPartOf(filler, 76);
  	public FixedLengthStringData reascode21 = new FixedLengthStringData(4).isAPartOf(filler, 80);
  	public FixedLengthStringData reascode22 = new FixedLengthStringData(4).isAPartOf(filler, 84);
  	public FixedLengthStringData reascode23 = new FixedLengthStringData(4).isAPartOf(filler, 88);
  	public FixedLengthStringData reascode24 = new FixedLengthStringData(4).isAPartOf(filler, 92);
  	public FixedLengthStringData reascode25 = new FixedLengthStringData(4).isAPartOf(filler, 96);
  	public FixedLengthStringData reascode26 = new FixedLengthStringData(4).isAPartOf(filler, 100);
  	public FixedLengthStringData reascode27 = new FixedLengthStringData(4).isAPartOf(filler, 104);
  	public FixedLengthStringData reascode28 = new FixedLengthStringData(4).isAPartOf(filler, 108);
  	public FixedLengthStringData reascode29 = new FixedLengthStringData(4).isAPartOf(filler, 112);
  	public FixedLengthStringData reascode30 = new FixedLengthStringData(4).isAPartOf(filler, 116);
  	public FixedLengthStringData shortdescs = new FixedLengthStringData(300).isAPartOf(t6662Rec, 120);
  	public FixedLengthStringData[] shortdesc = FLSArrayPartOfStructure(30, 10, shortdescs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(300).isAPartOf(shortdescs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData shortdesc01 = new FixedLengthStringData(10).isAPartOf(filler1, 0);
  	public FixedLengthStringData shortdesc02 = new FixedLengthStringData(10).isAPartOf(filler1, 10);
  	public FixedLengthStringData shortdesc03 = new FixedLengthStringData(10).isAPartOf(filler1, 20);
  	public FixedLengthStringData shortdesc04 = new FixedLengthStringData(10).isAPartOf(filler1, 30);
  	public FixedLengthStringData shortdesc05 = new FixedLengthStringData(10).isAPartOf(filler1, 40);
  	public FixedLengthStringData shortdesc06 = new FixedLengthStringData(10).isAPartOf(filler1, 50);
  	public FixedLengthStringData shortdesc07 = new FixedLengthStringData(10).isAPartOf(filler1, 60);
  	public FixedLengthStringData shortdesc08 = new FixedLengthStringData(10).isAPartOf(filler1, 70);
  	public FixedLengthStringData shortdesc09 = new FixedLengthStringData(10).isAPartOf(filler1, 80);
  	public FixedLengthStringData shortdesc10 = new FixedLengthStringData(10).isAPartOf(filler1, 90);
  	public FixedLengthStringData shortdesc11 = new FixedLengthStringData(10).isAPartOf(filler1, 100);
  	public FixedLengthStringData shortdesc12 = new FixedLengthStringData(10).isAPartOf(filler1, 110);
  	public FixedLengthStringData shortdesc13 = new FixedLengthStringData(10).isAPartOf(filler1, 120);
  	public FixedLengthStringData shortdesc14 = new FixedLengthStringData(10).isAPartOf(filler1, 130);
  	public FixedLengthStringData shortdesc15 = new FixedLengthStringData(10).isAPartOf(filler1, 140);
  	public FixedLengthStringData shortdesc16 = new FixedLengthStringData(10).isAPartOf(filler1, 150);
  	public FixedLengthStringData shortdesc17 = new FixedLengthStringData(10).isAPartOf(filler1, 160);
  	public FixedLengthStringData shortdesc18 = new FixedLengthStringData(10).isAPartOf(filler1, 170);
  	public FixedLengthStringData shortdesc19 = new FixedLengthStringData(10).isAPartOf(filler1, 180);
  	public FixedLengthStringData shortdesc20 = new FixedLengthStringData(10).isAPartOf(filler1, 190);
  	public FixedLengthStringData shortdesc21 = new FixedLengthStringData(10).isAPartOf(filler1, 200);
  	public FixedLengthStringData shortdesc22 = new FixedLengthStringData(10).isAPartOf(filler1, 210);
  	public FixedLengthStringData shortdesc23 = new FixedLengthStringData(10).isAPartOf(filler1, 220);
  	public FixedLengthStringData shortdesc24 = new FixedLengthStringData(10).isAPartOf(filler1, 230);
  	public FixedLengthStringData shortdesc25 = new FixedLengthStringData(10).isAPartOf(filler1, 240);
  	public FixedLengthStringData shortdesc26 = new FixedLengthStringData(10).isAPartOf(filler1, 250);
  	public FixedLengthStringData shortdesc27 = new FixedLengthStringData(10).isAPartOf(filler1, 260);
  	public FixedLengthStringData shortdesc28 = new FixedLengthStringData(10).isAPartOf(filler1, 270);
  	public FixedLengthStringData shortdesc29 = new FixedLengthStringData(10).isAPartOf(filler1, 280);
  	public FixedLengthStringData shortdesc30 = new FixedLengthStringData(10).isAPartOf(filler1, 290);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(80).isAPartOf(t6662Rec, 420, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6662Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6662Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}