package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:22
 * Description:
 * Copybook name: RACDRRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdrrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdrrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdrrvKey = new FixedLengthStringData(64).isAPartOf(racdrrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdrrvChdrcoy = new FixedLengthStringData(1).isAPartOf(racdrrvKey, 0);
  	public FixedLengthStringData racdrrvChdrnum = new FixedLengthStringData(8).isAPartOf(racdrrvKey, 1);
  	public FixedLengthStringData racdrrvLife = new FixedLengthStringData(2).isAPartOf(racdrrvKey, 9);
  	public FixedLengthStringData racdrrvCoverage = new FixedLengthStringData(2).isAPartOf(racdrrvKey, 11);
  	public FixedLengthStringData racdrrvRider = new FixedLengthStringData(2).isAPartOf(racdrrvKey, 13);
  	public PackedDecimalData racdrrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdrrvKey, 15);
  	public PackedDecimalData racdrrvSeqno = new PackedDecimalData(2, 0).isAPartOf(racdrrvKey, 18);
  	public PackedDecimalData racdrrvTranno = new PackedDecimalData(5, 0).isAPartOf(racdrrvKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(racdrrvKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdrrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdrrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}