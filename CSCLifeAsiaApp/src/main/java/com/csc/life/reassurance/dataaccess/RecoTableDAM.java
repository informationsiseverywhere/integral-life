package com.csc.life.reassurance.dataaccess;

import java.math.BigDecimal; 
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RecoTableDAM.java
 * Date: Sun, 30 Aug 2009 03:45:44
 * Class transformed from RECO.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RecoTableDAM extends RecopfTableDAM {

	public RecoTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("RECO");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "RASNUM, " +
		            "SEQNO, " +
		            "VALIDFLAG, " +
		            "COSTDATE, " +
		            "RETYPE, " +
		            "RNGMNT, " +
		            "SRARAMT, " +
		            "RAAMOUNT, " +
		            "CTDATE, " +
		            "ORIGCURR, " +
		            "PREM, " +
		            "COMPAY, " +
		            "TAXAMT, " +
		            "REFUNDFE, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "TRANNO, " +
		            "RCSTFRQ, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "RPRATE," +
					"ANNPREM, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               rasnum,
                               seqno,
                               validflag,
                               costdate,
                               retype,
                               rngmnt,
                               sraramt,
                               raAmount,
                               ctdate,
                               origcurr,
                               prem,
                               compay,
                               taxamt,
                               refundfe,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               tranno,
                               rcstfrq,
                               userProfile,
                               jobName,
                               datime,
                               RPRate,
							   annprem,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(202);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getRasnum().toInternal()
					+ getSeqno().toInternal()
					+ getValidflag().toInternal()
					+ getCostdate().toInternal()
					+ getRetype().toInternal()
					+ getRngmnt().toInternal()
					+ getSraramt().toInternal()
					+ getRaAmount().toInternal()
					+ getCtdate().toInternal()
					+ getOrigcurr().toInternal()
					+ getPrem().toInternal()
					+ getCompay().toInternal()
					+ getTaxamt().toInternal()
					+ getRefundfe().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getTranno().toInternal()
					+ getRcstfrq().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getRPRate().toInternal()
					+ getAnnprem().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, rasnum);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, costdate);
			what = ExternalData.chop(what, retype);
			what = ExternalData.chop(what, rngmnt);
			what = ExternalData.chop(what, sraramt);
			what = ExternalData.chop(what, raAmount);
			what = ExternalData.chop(what, ctdate);
			what = ExternalData.chop(what, origcurr);
			what = ExternalData.chop(what, prem);
			what = ExternalData.chop(what, compay);
			what = ExternalData.chop(what, taxamt);
			what = ExternalData.chop(what, refundfe);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, rcstfrq);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);	
			what = ExternalData.chop(what, RPRate);
			what = ExternalData.chop(what, annprem);
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getRasnum() {
		return rasnum;
	}
	public void setRasnum(Object what) {
		rasnum.set(what);
	}	
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCostdate() {
		return costdate;
	}
	public void setCostdate(Object what) {
		setCostdate(what, false);
	}
	public void setCostdate(Object what, boolean rounded) {
		if (rounded)
			costdate.setRounded(what);
		else
			costdate.set(what);
	}	
	public FixedLengthStringData getRetype() {
		return retype;
	}
	public void setRetype(Object what) {
		retype.set(what);
	}	
	public FixedLengthStringData getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(Object what) {
		rngmnt.set(what);
	}	
	public PackedDecimalData getSraramt() {
		return sraramt;
	}
	public void setSraramt(Object what) {
		setSraramt(what, false);
	}
	public void setSraramt(Object what, boolean rounded) {
		if (rounded)
			sraramt.setRounded(what);
		else
			sraramt.set(what);
	}	
	public PackedDecimalData getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(Object what) {
		setRaAmount(what, false);
	}
	public void setRaAmount(Object what, boolean rounded) {
		if (rounded)
			raAmount.setRounded(what);
		else
			raAmount.set(what);
	}	
	public PackedDecimalData getCtdate() {
		return ctdate;
	}
	public void setCtdate(Object what) {
		setCtdate(what, false);
	}
	public void setCtdate(Object what, boolean rounded) {
		if (rounded)
			ctdate.setRounded(what);
		else
			ctdate.set(what);
	}	
	public FixedLengthStringData getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(Object what) {
		origcurr.set(what);
	}	
	public PackedDecimalData getPrem() {
		return prem;
	}
	public void setPrem(Object what) {
		setPrem(what, false);
	}
	public void setPrem(Object what, boolean rounded) {
		if (rounded)
			prem.setRounded(what);
		else
			prem.set(what);
	}	
	public PackedDecimalData getRPRate() {
		return RPRate;
	}
	public void setRPRate(Object what) {
		setRPRate(what, false);
	}
	public void setRPRate(Object what, boolean rounded) {
		if (rounded)
			RPRate.setRounded(what);
		else
			RPRate.set(what);
	}
	public PackedDecimalData getCompay() {
		return compay;
	}
	public void setCompay(Object what) {
		setCompay(what, false);
	}
	public void setCompay(Object what, boolean rounded) {
		if (rounded)
			compay.setRounded(what);
		else
			compay.set(what);
	}	
	public PackedDecimalData getTaxamt() {
		return taxamt;
	}
	public void setTaxamt(Object what) {
		setTaxamt(what, false);
	}
	public void setTaxamt(Object what, boolean rounded) {
		if (rounded)
			taxamt.setRounded(what);
		else
			taxamt.set(what);
	}	
	public PackedDecimalData getRefundfe() {
		return refundfe;
	}
	public void setRefundfe(Object what) {
		setRefundfe(what, false);
	}
	public void setRefundfe(Object what, boolean rounded) {
		if (rounded)
			refundfe.setRounded(what);
		else
			refundfe.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getRcstfrq() {
		return rcstfrq;
	}
	public void setRcstfrq(Object what) {
		rcstfrq.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
		public PackedDecimalData getAnnprem() {
		return annprem;
	}
	public void setAnnprem(PackedDecimalData annprem) {
		this.annprem = annprem;
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		rasnum.clear();
		seqno.clear();
		validflag.clear();
		costdate.clear();
		retype.clear();
		rngmnt.clear();
		sraramt.clear();
		raAmount.clear();
		ctdate.clear();
		origcurr.clear();
		prem.clear();
		compay.clear();
		taxamt.clear();
		refundfe.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		tranno.clear();
		rcstfrq.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();	
		RPRate.clear();
		annprem.clear();
	}


}