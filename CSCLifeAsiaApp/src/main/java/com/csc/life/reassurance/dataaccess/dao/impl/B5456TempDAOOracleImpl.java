package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.B5456TempDAO;
import com.csc.life.reassurance.dataaccess.model.B5456DTO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class B5456TempDAOOracleImpl extends B5456TempDAOImpl implements B5456TempDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(B5456TempDAOOracleImpl.class);

    protected void createTempTable(String wsaaCompany, String wsaaChdrnumfrm, String wsaaChdrnumto, int wsaaSqlEffdate){
        
        StringBuilder sqlRacdrskSelect = new StringBuilder("INSERT INTO ");
        sqlRacdrskSelect.append(TABLE_NAME);
        sqlRacdrskSelect.append(" (CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, SEQNO, RASNUM, RNGMNT, CURRFROM, TRANNO, VALIDFLAG, CURRCODE, CURRTO, RETYPE, RAAMOUNT, CTDATE, CMDATE, REASPER, RECOVAMT, CESTYPE, LRKCLS, CHTRANNO, CHCNTTYPE, CHSTATCODE, CHPSTCDE, CHBILLFREQ, CHCNTCURR, CHPOLSUM, CHBILLCHNL, CHOCCDATE, CHPOLINC, COCRRCD, COCRTABLE, COSUMINS, COJLIFE, COINSTPREM, COCHDRCOY, COCHDRNUM, COLIFE, COCOVERAGE, CORIDER, COMORTCLS, CORCESDTE, COVALIDFLAG, RASAAGNTNUM, ANBCCD01, CLTSEX01, ANBCCD02, CLTSEX02, MAXAGERATE, MAXOPPC, MAXINSPRM, OPPCDIV, RAUQ, CHUQ, CHCURM, RRTDAT,RRTFRM,CORRTDAT,CORRTFRM)");
        sqlRacdrskSelect.append(" SELECT RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO, RA.RASNUM, RA.RNGMNT, RA.CURRFROM, RA.TRANNO, RA.VALIDFLAG, RA.CURRCODE, RA.CURRTO, RA.RETYPE, RA.RAAMOUNT, RA.CTDATE, RA.CMDATE, RA.REASPER, RA.RECOVAMT, RA.CESTYPE, RA.LRKCLS, ");
        sqlRacdrskSelect.append("  CH.TRANNO CHTRANNO,CH.CNTTYPE CHCNTTYPE,CH.STATCODE CHSTATCODE,CH.PSTCDE CHPSTCDE,CH.BILLFREQ CHBILLFREQ,CH.CNTCURR CHCNTCURR,CH.POLSUM CHPOLSUM,CH.BILLCHNL CHBILLCHNL,CH.OCCDATE CHOCCDATE,CH.POLINC CHPOLINC,");
        sqlRacdrskSelect.append("  CO.CRRCD COCRRCD,CO.CRTABLE COCRTABLE,CO.SUMINS COSUMINS,CO.JLIFE COJLIFE,CO.INSTPREM COINSTPREM,CO.CHDRCOY COCHDRCOY,CO.CHDRNUM COCHDRNUM,CO.LIFE COLIFE,CO.COVERAGE COCOVERAGE,CO.RIDER CORIDER,CO.MORTCLS COMORTCLS,CO.RCESDTE CORCESDTE,CO.VALIDFLAG COVALIDFLAG,RASA.AGNTNUM RASAAGNTNUM,L1.ANBCCD ANBCCD01,L1.CLTSEX CLTSEX01,L2.ANBCCD ANBCCD02,L2.CLTSEX CLTSEX02, ");
        sqlRacdrskSelect.append("  MAX(LE1.AGERATE) AS MAXAGERATE, MAX(LE1.OPPC) AS MAXOPPC, MAX(LE1.INSPRM) AS MAXINSPRM, exp(sum(ln(LE2.OPPC / 100))) AS OPPCDIV, RA.UNIQUE_NUMBER RAUQ, CH.UNIQUE_NUMBER CHUQ, CH.CURRFROM CHCURM ,MAX(RA.RRTDAT),MAX(RA.RRTFRM),MAX(CO.RRTDAT) AS CORRTDAT, MAX(CO.RRTFRM) AS CORRTFRM  ");
        sqlRacdrskSelect.append("  FROM RACDPF RA ");
        sqlRacdrskSelect.append("   INNER JOIN CHDRPF CH ON CH.CHDRCOY = RA.CHDRCOY AND CH.CHDRNUM = RA.CHDRNUM AND CH.VALIDFLAG = '1' ");
        sqlRacdrskSelect.append("   INNER JOIN COVRPF CO ON RA.CHDRCOY = CO.CHDRCOY AND RA.CHDRNUM = CO.CHDRNUM AND RA.LIFE = CO.LIFE AND RA.COVERAGE = CO.COVERAGE AND RA.RIDER = CO.RIDER AND RA.PLNSFX = CO.PLNSFX AND CO.VALIDFLAG = '1' ");
        sqlRacdrskSelect.append("   INNER JOIN RASAPF RASA ON RA.CHDRCOY = RASA.RASCOY AND RA.RASNUM = RASA.RASNUM ");
        sqlRacdrskSelect.append("   INNER JOIN LIFEPF L1 ON RA.CHDRCOY = L1.CHDRCOY AND RA.CHDRNUM = L1.CHDRNUM AND RA.LIFE = L1.LIFE AND L1.JLIFE = '00' ");
        sqlRacdrskSelect.append("   LEFT JOIN LIFEPF L2 ON RA.CHDRCOY = L2.CHDRCOY AND RA.CHDRNUM = L2.CHDRNUM AND RA.LIFE = L2.LIFE AND L2.JLIFE = '01' ");
        sqlRacdrskSelect.append("   LEFT JOIN LEXTPF LE1 ON RA.CHDRCOY = LE1.CHDRCOY AND RA.CHDRNUM = LE1.CHDRNUM AND RA.LIFE = LE1.LIFE AND RA.COVERAGE = LE1.COVERAGE AND RA.RIDER = LE1.RIDER  ");
        sqlRacdrskSelect.append("   LEFT JOIN LEXTPF LE2 ON RA.CHDRCOY = LE2.CHDRCOY AND RA.CHDRNUM = LE2.CHDRNUM AND RA.LIFE = LE2.LIFE AND RA.COVERAGE = LE2.COVERAGE AND RA.RIDER = LE2.RIDER AND LE2.REASIND = 2 AND LE2.OPPC>0 ");
        sqlRacdrskSelect.append("  WHERE RA.CHDRCOY = ");
        sqlRacdrskSelect.append(wsaaCompany);
        sqlRacdrskSelect.append("  AND RA.VALIDFLAG = '1' AND RA.CHDRNUM BETWEEN '");
        sqlRacdrskSelect.append(wsaaChdrnumfrm);
        sqlRacdrskSelect.append("' AND '");
        sqlRacdrskSelect.append(wsaaChdrnumto);
        sqlRacdrskSelect.append("' AND (RA.CTDATE < ");
        sqlRacdrskSelect.append(wsaaSqlEffdate);
        sqlRacdrskSelect.append(" OR RA.CTDATE = ");
        sqlRacdrskSelect.append(wsaaSqlEffdate);
        sqlRacdrskSelect.append(") ");
        sqlRacdrskSelect.append("  GROUP BY RA.UNIQUE_NUMBER, RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO, RA.RASNUM, RA.RNGMNT, RA.CURRFROM, RA.TRANNO, RA.VALIDFLAG, RA.CURRCODE, RA.CURRTO, RA.RETYPE, RA.RAAMOUNT, RA.CTDATE, RA.CMDATE, RA.REASPER, RA.RECOVAMT, RA.CESTYPE, RA.LRKCLS, CH.TRANNO, CH.CNTTYPE, CH.STATCODE, CH.PSTCDE, CH.BILLFREQ, CH.CNTCURR, CH.POLSUM, CH.BILLCHNL, CH.CNTCURR, CH.OCCDATE, CH.POLINC, CH.UNIQUE_NUMBER, CO.CRRCD, CO.CRTABLE, CO.SUMINS, CO.JLIFE, CO.INSTPREM, CO.CHDRCOY, CO.CHDRNUM, CO.LIFE, CO.COVERAGE, CO.RIDER, CO.MORTCLS, CO.RCESDTE, CO.VALIDFLAG, RASA.AGNTNUM, L1.ANBCCD, L1.CLTSEX, L2.ANBCCD, L2.CLTSEX,CH.CURRFROM  ");
        PreparedStatement psCreate = getPrepareStatement(sqlRacdrskSelect.toString());
        try {
            int i = psCreate.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("createTempTable()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psCreate, null);
        }
    }
    public List<B5456DTO> searchRacdResult(int min_record, int max_record){
        
        StringBuilder sqlRacdrskSelect = new StringBuilder();
        sqlRacdrskSelect.append(" SELECT * FROM (");
        sqlRacdrskSelect.append(" SELECT ALLR.* , ROWNUM RN FROM (");
        sqlRacdrskSelect.append("  SELECT * FROM ");
        sqlRacdrskSelect.append(TABLE_NAME);
        sqlRacdrskSelect.append(" RA ORDER BY RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.PLNSFX, RA.SEQNO ");
        sqlRacdrskSelect.append(" ) ALLR WHERE ROWNUM < ?");
        sqlRacdrskSelect.append(" ) WHERE RN >= ? ");
        
        PreparedStatement psRacdrskSelect = getPrepareStatement(sqlRacdrskSelect.toString());
        ResultSet sqlracdpf1rs = null;
        List<B5456DTO> racdSearchResultDtoList = null;
        try {
            psRacdrskSelect.setInt(1, max_record);
            psRacdrskSelect.setInt(2, min_record);

            sqlracdpf1rs = executeQuery(psRacdrskSelect);
            racdSearchResultDtoList = new LinkedList<B5456DTO>();

            while (sqlracdpf1rs.next()) {
                B5456DTO racdSearchResultDto = new B5456DTO();
                racdSearchResultDto.setSqlChdrcoy(sqlracdpf1rs.getString(1));
                racdSearchResultDto.setSqlChdrnum(sqlracdpf1rs.getString(2));
                racdSearchResultDto.setSqlLife(sqlracdpf1rs.getString(3));
                racdSearchResultDto.setSqlCoverage(sqlracdpf1rs.getString(4));
                racdSearchResultDto.setSqlRider(sqlracdpf1rs.getString(5));
                racdSearchResultDto.setSqlPlnsfx(sqlracdpf1rs.getInt(6));
                racdSearchResultDto.setSqlSeqno(sqlracdpf1rs.getInt(7));
                racdSearchResultDto.setSqlRasnum(sqlracdpf1rs.getString(8));
                racdSearchResultDto.setSqlRngmnt(sqlracdpf1rs.getString(9));
                racdSearchResultDto.setSqlCurrfrom(sqlracdpf1rs.getInt(10));
                racdSearchResultDto.setSqlTranno(sqlracdpf1rs.getInt(11));
                racdSearchResultDto.setSqlValidflag(sqlracdpf1rs.getString(12));
                racdSearchResultDto.setSqlCurrcode(sqlracdpf1rs.getString(13));
                racdSearchResultDto.setSqlCurrto(sqlracdpf1rs.getInt(14));
                racdSearchResultDto.setSqlRetype(sqlracdpf1rs.getString(15));
                racdSearchResultDto.setSqlRaamount(sqlracdpf1rs.getBigDecimal(16));
                racdSearchResultDto.setSqlCtdate(sqlracdpf1rs.getInt(17));
                racdSearchResultDto.setSqlCmdate(sqlracdpf1rs.getInt(18));
                racdSearchResultDto.setSqlReasper(sqlracdpf1rs.getBigDecimal(19));
                racdSearchResultDto.setSqlRecovamt(sqlracdpf1rs.getBigDecimal(20));
                racdSearchResultDto.setSqlCestype(sqlracdpf1rs.getString(21));
                racdSearchResultDto.setSqlLrkcls(sqlracdpf1rs.getString(22));
                racdSearchResultDto.setSqlChTranno(sqlracdpf1rs.getInt(23));
                racdSearchResultDto.setSqlChCnttype(sqlracdpf1rs.getString(24));
                racdSearchResultDto.setSqlChStatcode(sqlracdpf1rs.getString(25));
                racdSearchResultDto.setSqlChPstatcode(sqlracdpf1rs.getString(26));
                racdSearchResultDto.setSqlChBillfreq(sqlracdpf1rs.getString(27));
                racdSearchResultDto.setSqlChCntcurr(sqlracdpf1rs.getString(28));
                racdSearchResultDto.setSqlChPolsum(sqlracdpf1rs.getInt(29));
                racdSearchResultDto.setSqlChBillchnl(sqlracdpf1rs.getString(30));
                racdSearchResultDto.setSqlChOccdate(sqlracdpf1rs.getInt(31));
                racdSearchResultDto.setSqlChPolinc(sqlracdpf1rs.getInt(32));
                racdSearchResultDto.setSqlCoCrrcd(sqlracdpf1rs.getInt(33));
                racdSearchResultDto.setSqlCoCrtable(sqlracdpf1rs.getString(34));
                racdSearchResultDto.setSqlCoSumins(sqlracdpf1rs.getBigDecimal(35));
                racdSearchResultDto.setSqlCoJlife(sqlracdpf1rs.getString(36));
                racdSearchResultDto.setSqlCoInstprem(sqlracdpf1rs.getBigDecimal(37));
                racdSearchResultDto.setSqlCoChdrcoy(sqlracdpf1rs.getString(38));
                racdSearchResultDto.setSqlCoChdrnum(sqlracdpf1rs.getString(39));
                racdSearchResultDto.setSqlCoLife(sqlracdpf1rs.getString(40));
                racdSearchResultDto.setSqlCoCoverage(sqlracdpf1rs.getString(41));
                racdSearchResultDto.setSqlCoRider(sqlracdpf1rs.getString(42));
                racdSearchResultDto.setSqlCoMortcls(sqlracdpf1rs.getString(43));
                racdSearchResultDto.setSqlCoRiskCessDate(sqlracdpf1rs.getInt(44));
                racdSearchResultDto.setSqlCoValidflag(sqlracdpf1rs.getString(45));
                racdSearchResultDto.setSqlAgntnum(sqlracdpf1rs.getString(46));
                racdSearchResultDto.setSqlAnbAtCcd00(sqlracdpf1rs.getInt(47));
                racdSearchResultDto.setSqlCltsex00(sqlracdpf1rs.getString(48));
                racdSearchResultDto.setSqlAnbAtCcd01(sqlracdpf1rs.getInt(49));
                racdSearchResultDto.setSqlCltsex01(sqlracdpf1rs.getString(50));
                racdSearchResultDto.setSqlMaxAgerate(sqlracdpf1rs.getInt(51));
                racdSearchResultDto.setSqlMaxOppc(sqlracdpf1rs.getBigDecimal(52));
                racdSearchResultDto.setSqlMaxInsprm(sqlracdpf1rs.getInt(53));
                racdSearchResultDto.setSqlOppc2(sqlracdpf1rs.getBigDecimal(54));
                racdSearchResultDto.setSqlRaUniqueNum(sqlracdpf1rs.getLong(55));
                racdSearchResultDto.setSqlChUniqueNum(sqlracdpf1rs.getLong(56));
                racdSearchResultDto.setSqlChCurrfrom(sqlracdpf1rs.getInt(57));
                racdSearchResultDtoList.add(racdSearchResultDto);
            }
            psRacdrskSelect.close();

        } catch (SQLException e) {
            LOGGER.error("searchRacdResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psRacdrskSelect, sqlracdpf1rs);
        }
        return racdSearchResultDtoList;
    }

}