package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5448
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5448ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(582);
	public FixedLengthStringData dataFields = new FixedLengthStringData(150).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData lrkclsdsc = DD.lrkclsdsc.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData proppr = DD.proppr.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData rcedsar = DD.rcedsar.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData rclmbas = DD.rclmbas.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData rcstfrq = DD.rcstfrq.copy().isAPartOf(dataFields,86);
	public FixedLengthStringData rcstsar = DD.rcstsar.copy().isAPartOf(dataFields,88);
	public ZonedDecimalData rprior = DD.rprior.copyToZonedDecimal().isAPartOf(dataFields,89);
	public FixedLengthStringData rresmeth = DD.rresmeth.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData rrevfrq = DD.rrevfrq.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData rrngmnts = new FixedLengthStringData(40).isAPartOf(dataFields, 97);
	public FixedLengthStringData[] rrngmnt = FLSArrayPartOfStructure(10, 4, rrngmnts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(rrngmnts, 0, FILLER_REDEFINE);
	public FixedLengthStringData rrngmnt01 = DD.rrngmnt.copy().isAPartOf(filler,0);
	public FixedLengthStringData rrngmnt02 = DD.rrngmnt.copy().isAPartOf(filler,4);
	public FixedLengthStringData rrngmnt03 = DD.rrngmnt.copy().isAPartOf(filler,8);
	public FixedLengthStringData rrngmnt04 = DD.rrngmnt.copy().isAPartOf(filler,12);
	public FixedLengthStringData rrngmnt05 = DD.rrngmnt.copy().isAPartOf(filler,16);
	public FixedLengthStringData rrngmnt06 = DD.rrngmnt.copy().isAPartOf(filler,20);
	public FixedLengthStringData rrngmnt07 = DD.rrngmnt.copy().isAPartOf(filler,24);
	public FixedLengthStringData rrngmnt08 = DD.rrngmnt.copy().isAPartOf(filler,28);
	public FixedLengthStringData rrngmnt09 = DD.rrngmnt.copy().isAPartOf(filler,32);
	public FixedLengthStringData rrngmnt10 = DD.rrngmnt.copy().isAPartOf(filler,36);
	public FixedLengthStringData rrsktyp = DD.rrsktyp.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData rtaxbas = DD.rtaxbas.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 150);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData lrkclsdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData propprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData rcedsarErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData rclmbasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData rcstfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData rcstsarErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData rpriorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData rresmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData rrevfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData rrngmntsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] rrngmntErr = FLSArrayPartOfStructure(10, 4, rrngmntsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(rrngmntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rrngmnt01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData rrngmnt02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData rrngmnt03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData rrngmnt04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData rrngmnt05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData rrngmnt06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData rrngmnt07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData rrngmnt08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData rrngmnt09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData rrngmnt10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData rrsktypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rtaxbasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 258);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] lrkclsdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] propprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] rcedsarOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] rclmbasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] rcstfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] rcstsarOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] rpriorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] rresmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] rrevfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData rrngmntsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] rrngmntOut = FLSArrayPartOfStructure(10, 12, rrngmntsOut, 0);
	public FixedLengthStringData[][] rrngmntO = FLSDArrayPartOfArrayStructure(12, 1, rrngmntOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(120).isAPartOf(rrngmntsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rrngmnt01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] rrngmnt02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] rrngmnt03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] rrngmnt04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] rrngmnt05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] rrngmnt06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] rrngmnt07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] rrngmnt08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] rrngmnt09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] rrngmnt10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] rrsktypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rtaxbasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5448screenWritten = new LongData(0);
	public LongData S5448protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5448ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rrsktypOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rresmethOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcstfrqOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrevfrqOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rtaxbasOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt03Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt05Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt06Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt07Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt08Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt09Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rrngmnt10Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rclmbasOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, rrsktyp, lrkclsdsc, rcedsar, rcstsar, rresmeth, rcstfrq, rrevfrq, rtaxbas, rrngmnt01, rrngmnt02, rrngmnt03, rrngmnt04, rrngmnt05, rrngmnt06, rrngmnt07, rrngmnt08, rrngmnt09, rrngmnt10, rprior, rclmbas, proppr};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, rrsktypOut, lrkclsdscOut, rcedsarOut, rcstsarOut, rresmethOut, rcstfrqOut, rrevfrqOut, rtaxbasOut, rrngmnt01Out, rrngmnt02Out, rrngmnt03Out, rrngmnt04Out, rrngmnt05Out, rrngmnt06Out, rrngmnt07Out, rrngmnt08Out, rrngmnt09Out, rrngmnt10Out, rpriorOut, rclmbasOut, propprOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, rrsktypErr, lrkclsdscErr, rcedsarErr, rcstsarErr, rresmethErr, rcstfrqErr, rrevfrqErr, rtaxbasErr, rrngmnt01Err, rrngmnt02Err, rrngmnt03Err, rrngmnt04Err, rrngmnt05Err, rrngmnt06Err, rrngmnt07Err, rrngmnt08Err, rrngmnt09Err, rrngmnt10Err, rpriorErr, rclmbasErr, propprErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5448screen.class;
		protectRecord = S5448protect.class;
	}

}
