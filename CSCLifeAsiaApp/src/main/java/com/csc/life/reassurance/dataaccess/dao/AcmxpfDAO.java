package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;

import com.csc.life.reassurance.dataaccess.model.Acmxpf;
import com.csc.life.reassurance.dataaccess.model.B5471DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;


public interface AcmxpfDAO extends BaseDAO<Acmxpf>{

    public int deleteAcmxpfBulk(String batccoy);
    public List<B5471DTO> searchAcmxResult(String batccoy,int min_record, int max_record);
    public List<Acmxpf> searchAcmxByRldgacct(String batccoy,String rldgacct);
}
