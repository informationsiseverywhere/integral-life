package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5443
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5443ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(586);
	public FixedLengthStringData dataFields = new FixedLengthStringData(250).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData gstrates = new FixedLengthStringData(20).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] gstrate = ZDArrayPartOfStructure(5, 4, 2, gstrates, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(gstrates, 0, FILLER_REDEFINE);
	public ZonedDecimalData gstrate01 = DD.gstrate.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData gstrate02 = DD.gstrate.copyToZonedDecimal().isAPartOf(filler,4);
	public ZonedDecimalData gstrate03 = DD.gstrate.copyToZonedDecimal().isAPartOf(filler,8);
	public ZonedDecimalData gstrate04 = DD.gstrate.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData gstrate05 = DD.gstrate.copyToZonedDecimal().isAPartOf(filler,16);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,21);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,29);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,37);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData reprmfrms = new FixedLengthStringData(85).isAPartOf(dataFields, 75);
	public ZonedDecimalData[] reprmfrm = ZDArrayPartOfStructure(5, 17, 2, reprmfrms, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(85).isAPartOf(reprmfrms, 0, FILLER_REDEFINE);
	public ZonedDecimalData reprmfrm01 = DD.reprmfrm.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData reprmfrm02 = DD.reprmfrm.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData reprmfrm03 = DD.reprmfrm.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData reprmfrm04 = DD.reprmfrm.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData reprmfrm05 = DD.reprmfrm.copyToZonedDecimal().isAPartOf(filler1,68);
	public FixedLengthStringData reprmtos = new FixedLengthStringData(85).isAPartOf(dataFields, 160);
	public ZonedDecimalData[] reprmto = ZDArrayPartOfStructure(5, 17, 2, reprmtos, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(85).isAPartOf(reprmtos, 0, FILLER_REDEFINE);
	public ZonedDecimalData reprmto01 = DD.reprmto.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData reprmto02 = DD.reprmto.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData reprmto03 = DD.reprmto.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData reprmto04 = DD.reprmto.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData reprmto05 = DD.reprmto.copyToZonedDecimal().isAPartOf(filler2,68);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 250);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData gstratesErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] gstrateErr = FLSArrayPartOfStructure(5, 4, gstratesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(gstratesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData gstrate01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData gstrate02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData gstrate03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData gstrate04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData gstrate05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData reprmfrmsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] reprmfrmErr = FLSArrayPartOfStructure(5, 4, reprmfrmsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(reprmfrmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData reprmfrm01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData reprmfrm02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData reprmfrm03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData reprmfrm04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData reprmfrm05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData reprmtosErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] reprmtoErr = FLSArrayPartOfStructure(5, 4, reprmtosErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(20).isAPartOf(reprmtosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData reprmto01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData reprmto02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData reprmto03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData reprmto04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData reprmto05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 334);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData gstratesOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] gstrateOut = FLSArrayPartOfStructure(5, 12, gstratesOut, 0);
	public FixedLengthStringData[][] gstrateO = FLSDArrayPartOfArrayStructure(12, 1, gstrateOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(60).isAPartOf(gstratesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] gstrate01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] gstrate02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] gstrate03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] gstrate04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] gstrate05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData reprmfrmsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] reprmfrmOut = FLSArrayPartOfStructure(5, 12, reprmfrmsOut, 0);
	public FixedLengthStringData[][] reprmfrmO = FLSDArrayPartOfArrayStructure(12, 1, reprmfrmOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(60).isAPartOf(reprmfrmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] reprmfrm01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] reprmfrm02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] reprmfrm03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] reprmfrm04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] reprmfrm05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData reprmtosOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] reprmtoOut = FLSArrayPartOfStructure(5, 12, reprmtosOut, 0);
	public FixedLengthStringData[][] reprmtoO = FLSDArrayPartOfArrayStructure(12, 1, reprmtoOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(60).isAPartOf(reprmtosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] reprmto01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] reprmto02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] reprmto03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] reprmto04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] reprmto05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5443screenWritten = new LongData(0);
	public LongData S5443protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5443ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(reprmfrm02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmto02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gstrate02Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmfrm01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmto01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gstrate01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmfrm03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmto03Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gstrate03Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmfrm04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmto04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gstrate04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmfrm05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reprmto05Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gstrate05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, reprmfrm02, reprmto02, gstrate02, reprmfrm01, reprmto01, gstrate01, reprmfrm03, reprmto03, gstrate03, reprmfrm04, reprmto04, gstrate04, reprmfrm05, reprmto05, gstrate05};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, reprmfrm02Out, reprmto02Out, gstrate02Out, reprmfrm01Out, reprmto01Out, gstrate01Out, reprmfrm03Out, reprmto03Out, gstrate03Out, reprmfrm04Out, reprmto04Out, gstrate04Out, reprmfrm05Out, reprmto05Out, gstrate05Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, reprmfrm02Err, reprmto02Err, gstrate02Err, reprmfrm01Err, reprmto01Err, gstrate01Err, reprmfrm03Err, reprmto03Err, gstrate03Err, reprmfrm04Err, reprmto04Err, gstrate04Err, reprmfrm05Err, reprmto05Err, gstrate05Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5443screen.class;
		protectRecord = S5443protect.class;
	}

}
