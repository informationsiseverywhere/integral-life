package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.reassurance.dataaccess.model.Acmvagt;
import com.csc.life.reassurance.dataaccess.model.Acmxpf;
import com.csc.life.reassurance.dataaccess.model.B5470DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.smart400framework.dataaccess.model.Acmvpf;


public interface AcmvpfDAO extends BaseDAO<Acmvpf>{

    public List<B5470DTO> searchAcmvByRasNumResult(String batccoy,String sacscode,String effdate,int min_record, int max_record);
    public void insertAcmxBulk(List<B5470DTO> acmvBulkList);
    public void updateAcmvBulk(List<Acmvagt> acmvBulkList);
    public Acmvagt searchAcmvByAxmcResult(Acmxpf acmxpf);
    public List<Acmvpf> searchAcmvpfRecord(Acmvpf acmvpf);
    public List<Acmvpf> searchAcmvpfRecordbyTranno(Acmvpf acmvpf);
    public List<Acmvpf> ReadSurrender100(Acmvpf acmvpf);
    public List<Acmvpf> ReadAcmvpf(Acmvpf acmvpf);
    
    Map<Integer, Acmvpf> searchAcmvpfMap(String coy, String chdrnum);
}