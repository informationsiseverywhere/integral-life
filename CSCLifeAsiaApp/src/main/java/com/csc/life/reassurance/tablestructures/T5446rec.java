package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;


import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:13
 * Description:
 * Copybook name: T5446REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5446rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5446Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(t5446Rec, 0);
  	public ZonedDecimalData discretn = new ZonedDecimalData(17, 2).isAPartOf(t5446Rec, 3);
  	public ZonedDecimalData retn = new ZonedDecimalData(17, 2).isAPartOf(t5446Rec, 20);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(t5446Rec, 37);
  	public ZonedDecimalData maxAmount = new ZonedDecimalData(17, 2).isAPartOf(t5446Rec, 41);
  	public ZonedDecimalData fieldseq = new ZonedDecimalData(4, 0).isAPartOf(t5446Rec, 58);
	public FixedLengthStringData rskcldisp =  new FixedLengthStringData(1).isAPartOf(t5446Rec, 62);
  	public FixedLengthStringData filler = new FixedLengthStringData(437).isAPartOf(t5446Rec, 63, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5446Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5446Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}