package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:19
 * Description:
 * Copybook name: RACDENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdenqKey = new FixedLengthStringData(64).isAPartOf(racdenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdenqChdrcoy = new FixedLengthStringData(1).isAPartOf(racdenqKey, 0);
  	public FixedLengthStringData racdenqChdrnum = new FixedLengthStringData(8).isAPartOf(racdenqKey, 1);
  	public FixedLengthStringData racdenqLife = new FixedLengthStringData(2).isAPartOf(racdenqKey, 9);
  	public FixedLengthStringData racdenqCoverage = new FixedLengthStringData(2).isAPartOf(racdenqKey, 11);
  	public FixedLengthStringData racdenqRider = new FixedLengthStringData(2).isAPartOf(racdenqKey, 13);
  	public PackedDecimalData racdenqSeqno = new PackedDecimalData(2, 0).isAPartOf(racdenqKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(racdenqKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}