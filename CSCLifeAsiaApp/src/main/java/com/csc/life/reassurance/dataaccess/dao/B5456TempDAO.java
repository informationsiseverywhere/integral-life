package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;

import com.csc.life.reassurance.dataaccess.model.B5456DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface B5456TempDAO extends BaseDAO<B5456DTO> {
    public void initTempTable(String wsaaCompany, String wsaaChdrnumfrm, String wsaaChdrnumto, int wsaaSqlEffdate);
    public List<B5456DTO> searchRacdResult(int min_record, int max_record);
}