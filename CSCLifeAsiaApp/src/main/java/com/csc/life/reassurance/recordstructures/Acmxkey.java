package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:45
 * Description:
 * Copybook name: ACMXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmxkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmxFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData acmxKey = new FixedLengthStringData(256).isAPartOf(acmxFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmxBatccoy = new FixedLengthStringData(1).isAPartOf(acmxKey, 0);
  	public FixedLengthStringData acmxRldgacct = new FixedLengthStringData(16).isAPartOf(acmxKey, 1);
  	public FixedLengthStringData acmxOrigcurr = new FixedLengthStringData(3).isAPartOf(acmxKey, 17);
  	public FixedLengthStringData acmxSacscode = new FixedLengthStringData(2).isAPartOf(acmxKey, 20);
  	public FixedLengthStringData acmxSacstyp = new FixedLengthStringData(2).isAPartOf(acmxKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(acmxKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmxFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmxFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}