package com.csc.life.reassurance.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LirrrrvTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:53
 * Class transformed from LIRRRRV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LirrrrvTableDAM extends LirrpfTableDAM {

	public LirrrrvTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LIRRRRV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", RASNUM"
		             + ", RNGMNT"
		             + ", CLNTPFX"
		             + ", CLNTCOY"
		             + ", CLNTNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "COMPANY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "CLNTPFX, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "RASNUM, " +
		            "RNGMNT, " +
		            "CURRENCY, " +
		            "RAAMOUNT, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "RASNUM ASC, " +
		            "RNGMNT ASC, " +
		            "CLNTPFX ASC, " +
		            "CLNTCOY ASC, " +
		            "CLNTNUM ASC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "RASNUM DESC, " +
		            "RNGMNT DESC, " +
		            "CLNTPFX DESC, " +
		            "CLNTCOY DESC, " +
		            "CLNTNUM DESC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobName,
                               userProfile,
                               datime,
                               company,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               clntpfx,
                               clntcoy,
                               clntnum,
                               currfrom,
                               currto,
                               validflag,
                               tranno,
                               rasnum,
                               rngmnt,
                               currency,
                               raAmount,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(20);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getCompany().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getRasnum().toInternal()
					+ getRngmnt().toInternal()
					+ getClntpfx().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, rasnum);
			what = ExternalData.chop(what, rngmnt);
			what = ExternalData.chop(what, clntpfx);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller160 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller170 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller180 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller40.setInternal(company.toInternal());
	nonKeyFiller50.setInternal(chdrnum.toInternal());
	nonKeyFiller60.setInternal(life.toInternal());
	nonKeyFiller70.setInternal(coverage.toInternal());
	nonKeyFiller80.setInternal(rider.toInternal());
	nonKeyFiller90.setInternal(planSuffix.toInternal());
	nonKeyFiller100.setInternal(clntpfx.toInternal());
	nonKeyFiller110.setInternal(clntcoy.toInternal());
	nonKeyFiller120.setInternal(clntnum.toInternal());
	nonKeyFiller160.setInternal(tranno.toInternal());
	nonKeyFiller170.setInternal(rasnum.toInternal());
	nonKeyFiller180.setInternal(rngmnt.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(113);
		
		nonKeyData.set(
					getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getValidflag().toInternal()
					+ nonKeyFiller160.toInternal()
					+ nonKeyFiller170.toInternal()
					+ nonKeyFiller180.toInternal()
					+ getCurrency().toInternal()
					+ getRaAmount().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, nonKeyFiller160);
			what = ExternalData.chop(what, nonKeyFiller170);
			what = ExternalData.chop(what, nonKeyFiller180);
			what = ExternalData.chop(what, currency);
			what = ExternalData.chop(what, raAmount);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getRasnum() {
		return rasnum;
	}
	public void setRasnum(Object what) {
		rasnum.set(what);
	}
	public FixedLengthStringData getRngmnt() {
		return rngmnt;
	}
	public void setRngmnt(Object what) {
		rngmnt.set(what);
	}
	public FixedLengthStringData getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(Object what) {
		clntpfx.set(what);
	}
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getCurrency() {
		return currency;
	}
	public void setCurrency(Object what) {
		currency.set(what);
	}	
	public PackedDecimalData getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(Object what) {
		setRaAmount(what, false);
	}
	public void setRaAmount(Object what, boolean rounded) {
		if (rounded)
			raAmount.setRounded(what);
		else
			raAmount.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		rasnum.clear();
		rngmnt.clear();
		clntpfx.clear();
		clntcoy.clear();
		clntnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		jobName.clear();
		userProfile.clear();
		datime.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		currfrom.clear();
		currto.clear();
		validflag.clear();
		nonKeyFiller160.clear();
		nonKeyFiller170.clear();
		nonKeyFiller180.clear();
		currency.clear();
		raAmount.clear();		
	}


}