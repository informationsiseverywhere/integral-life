/*
 * File: Reasout.java
 * Date: 30 August 2009 2:04:21
 * Author: Quipoz Limited
 * 
 * Class transformed from REASOUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.CovrbrkTableDAM;
import com.csc.life.contractservicing.recordstructures.Genoutrec;
import com.csc.life.reassurance.dataaccess.LirrbrkTableDAM;
import com.csc.life.reassurance.dataaccess.LirrconTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhbrkTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhconTableDAM;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdbrkTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reassurance Breakout Subroutine.
*
* This is a new subroutine which forms part of the Reassurance
* Development. It will be called from T5671 by BRKOUT, to break
* out reassurance (i.e. RACD, LRRH and LIRR) records.
*
*****************************************************************
* </pre>
*/
public class Reasout extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REASOUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaReasper = new PackedDecimalData(7, 4);

		/*01  WSAA-AMOUNT-TABLE.                                           
		    03 WSAA-AMOUNTS-IN          OCCURS 17.                       
		    03 WSAA-AMOUNTS-OUT         OCCURS 17.                       */
	private FixedLengthStringData wsaaAmountTable = new FixedLengthStringData(578);
	private FixedLengthStringData[] wsaaAmountsIn = FLSArrayPartOfStructure(17, 17, wsaaAmountTable, 0);
	private ZonedDecimalData[] wsaaAmountIn = ZDArrayPartOfArrayStructure(17, 2, wsaaAmountsIn, 0);
	private FixedLengthStringData[] wsaaAmountsOut = FLSArrayPartOfStructure(17, 17, wsaaAmountTable, 289);
	private ZonedDecimalData[] wsaaAmountOut = ZDArrayPartOfArrayStructure(17, 2, wsaaAmountsOut, 0);

	private FixedLengthStringData wsaaTotTable = new FixedLengthStringData(153);
	private PackedDecimalData[] wsaaTotAmount = PDArrayPartOfStructure(17, 17, 2, wsaaTotTable, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0);
		/* FORMATS */
	private static final String racdrec = "RACDREC";
	private static final String racdbrkrec = "RACDBRKREC";
	private static final String lirrconrec = "LIRRCONREC";
	private static final String lirrbrkrec = "LIRRBRKREC";
	private static final String lrrhconrec = "LRRHCONREC";
	private static final String lrrhbrkrec = "LRRHBRKREC";
	private static final String chdrmatrec = "CHDRMATREC";
	private static final String covrbrkrec = "COVRBRKREC";
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private CovrbrkTableDAM covrbrkIO = new CovrbrkTableDAM();
	private LirrbrkTableDAM lirrbrkIO = new LirrbrkTableDAM();
	private LirrconTableDAM lirrconIO = new LirrconTableDAM();
	private LrrhbrkTableDAM lrrhbrkIO = new LrrhbrkTableDAM();
	private LrrhconTableDAM lrrhconIO = new LrrhconTableDAM();
	private RacdTableDAM racdIO = new RacdTableDAM();
	private RacdbrkTableDAM racdbrkIO = new RacdbrkTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Genoutrec genoutrec = new Genoutrec();

	public Reasout() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		genoutrec.outRec = convertAndSetParam(genoutrec.outRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		genoutrec.statuz.set(varcom.oK);
		initialize(wsaaAmountTable);
		initialize(wsaaTotTable);
		/* Read contract header to obtain the current transaction*/
		/* number.*/
		chdrmatIO.setParams(SPACES);
		chdrmatIO.setChdrcoy(genoutrec.chdrcoy);
		chdrmatIO.setChdrnum(genoutrec.chdrnum);
		chdrmatIO.setFormat(chdrmatrec);
		chdrmatIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			dbError580();
		}
		racdbrkIO.setParams(SPACES);
		racdbrkIO.setChdrnum(genoutrec.chdrnum);
		racdbrkIO.setChdrcoy(genoutrec.chdrcoy);
		racdbrkIO.setPlanSuffix(ZERO);
		racdbrkIO.setSeqno(ZERO);
		racdbrkIO.setFormat(racdbrkrec);
		racdbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, racdbrkIO);
		if (isNE(racdbrkIO.getStatuz(), varcom.oK)
		&& isNE(racdbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdbrkIO.getParams());
			dbError580();
		}
		if (isNE(racdbrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(racdbrkIO.getChdrcoy(), genoutrec.chdrcoy)
		|| isNE(racdbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(racdbrkIO.getStatuz(), varcom.endp)) {
			racdbrkIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(racdbrkIO.getStatuz(), varcom.endp))) {
			mainProcessingRacd100();
		}
		
		/* First Read of LIRRBRK Reassurer Experience Details.*/
		lirrbrkIO.setParams(SPACES);
		lirrbrkIO.setChdrnum(genoutrec.chdrnum);
		lirrbrkIO.setCompany(genoutrec.chdrcoy);
		lirrbrkIO.setPlanSuffix(ZERO);
		lirrbrkIO.setFormat(lirrbrkrec);
		lirrbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, lirrbrkIO);
		if (isNE(lirrbrkIO.getStatuz(), varcom.oK)
		&& isNE(lirrbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lirrbrkIO.getParams());
			dbError580();
		}
		if (isNE(lirrbrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(lirrbrkIO.getCompany(), genoutrec.chdrcoy)
		|| isNE(lirrbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(lirrbrkIO.getStatuz(), varcom.endp)) {
			lirrbrkIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(lirrbrkIO.getStatuz(), varcom.endp))) {
			mainProcessingLirr200();
		}
		
		/* First Read of LRRHBRK Life Experience Details.*/
		lrrhbrkIO.setParams(SPACES);
		lrrhbrkIO.setChdrnum(genoutrec.chdrnum);
		lrrhbrkIO.setCompany(genoutrec.chdrcoy);
		lrrhbrkIO.setPlanSuffix(ZERO);
		lrrhbrkIO.setFormat(lrrhbrkrec);
		lrrhbrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, lrrhbrkIO);
		if (isNE(lrrhbrkIO.getStatuz(), varcom.oK)
		&& isNE(lrrhbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lrrhbrkIO.getParams());
			dbError580();
		}
		if (isNE(lrrhbrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(lrrhbrkIO.getCompany(), genoutrec.chdrcoy)
		|| isNE(lrrhbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(lrrhbrkIO.getStatuz(), varcom.endp)) {
			lrrhbrkIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(lrrhbrkIO.getStatuz(), varcom.endp))) {
			mainProcessingLrrh300();
		}
		
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void mainProcessingRacd100()
	{
		para101();
		nextRacd105();
	}

protected void para101()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveRacdValsToTable110();
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix, genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
				calcAmts400();
			}
			moveRacdValsFromTable120();
			writeBrkRacd130();
		}
		calcNewSummaryRecRacd140();
	}

protected void nextRacd105()
	{
		racdbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, racdbrkIO);
		if (isNE(racdbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdbrkIO.getParams());
			dbError580();
		}
		/* In the absence of a proper release function rewrite the*/
		/* record at end of file.  This will enable the calling program*/
		/* to update the record if required*/
		if (isNE(racdbrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(racdbrkIO.getChdrcoy(), genoutrec.chdrcoy)
		|| isNE(racdbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(racdbrkIO.getStatuz(), varcom.endp)) {
			racdbrkIO.setFormat(racdbrkrec);
			racdbrkIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, racdbrkIO);
			if (isNE(racdbrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(racdbrkIO.getParams());
				dbError580();
			}
			else {
				racdbrkIO.setStatuz(varcom.endp);
			}
		}
	}

protected void moveRacdValsToTable110()
	{
		/*ENTRY*/
		wsaaAmountIn[1].set(racdbrkIO.getRaAmount());
		wsaaAmountIn[2].set(racdbrkIO.getRecovamt());
		/*EXIT*/
	}

protected void moveRacdValsFromTable120()
	{
		/*ENTRY*/
		racdbrkIO.setRaAmount(wsaaAmountOut[1]);
		racdbrkIO.setRecovamt(wsaaAmountOut[2]);
		/*EXIT*/
	}

protected void writeBrkRacd130()
	{
		entry131();
	}

protected void entry131()
	{
		racdIO.setChdrcoy(racdbrkIO.getChdrcoy());
		racdIO.setChdrnum(racdbrkIO.getChdrnum());
		racdIO.setLife(racdbrkIO.getLife());
		racdIO.setCoverage(racdbrkIO.getCoverage());
		racdIO.setRider(racdbrkIO.getRider());
		racdIO.setSeqno(racdbrkIO.getSeqno());
		racdIO.setPlanSuffix(wsaaPlanSuffix);
		racdIO.setNonKey(racdbrkIO.getNonKey());
		covrbrkIO.setParams(SPACES);
		covrbrkIO.setChdrcoy(racdIO.getChdrcoy());
		covrbrkIO.setChdrnum(racdIO.getChdrnum());
		covrbrkIO.setPlanSuffix(racdIO.getPlanSuffix());
		covrbrkIO.setLife(racdIO.getLife());
		covrbrkIO.setCoverage(racdIO.getCoverage());
		covrbrkIO.setRider(racdIO.getRider());
		covrbrkIO.setFormat(covrbrkrec);
		covrbrkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrbrkIO);
		if (isNE(covrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrbrkIO.getParams());
			dbError580();
		}
		compute(wsaaReasper, 4).set(mult(div(racdIO.getRaAmount(), covrbrkIO.getSumins()), 100));
		racdIO.setReasper(wsaaReasper, true);
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
	}

protected void calcNewSummaryRecRacd140()
	{
		entry141();
	}

protected void entry141()
	{
		if (isLT(genoutrec.newSummary, 2)) {
			deleteSummaryRacd150();
		}
		/* Calculate the values of the summarised record or the last*/
		/* policy record. Calculate the new values by subtracting the*/
		/* accumulated amounts from the previous summarised values.*/
		setPrecision(racdbrkIO.getRaAmount(), 2);
		racdbrkIO.setRaAmount(sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		setPrecision(racdbrkIO.getRecovamt(), 2);
		racdbrkIO.setRecovamt(sub(wsaaAmountIn[2], wsaaTotAmount[2]));
		/* Write the last policy.*/
		if (isLT(genoutrec.newSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkRacd130();
			return ;
		}
		/* To get here there are still records summarised so rewrite the*/
		/* the record.*/
		racdbrkIO.setFormat(racdbrkrec);
		racdbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, racdbrkIO);
		if (isNE(racdbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdbrkIO.getParams());
			dbError580();
		}
	}

protected void deleteSummaryRacd150()
	{
		/*ENTRY*/
		racdbrkIO.setFunction(varcom.delet);
		racdbrkIO.setFormat(racdbrkrec);
		SmartFileCode.execute(appVars, racdbrkIO);
		if (isNE(racdbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdbrkIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void mainProcessingLirr200()
	{
		para201();
		nextLirrcon205();
	}

protected void para201()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveLirrValsToTable210();
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix, genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
				calcAmts400();
			}
			moveLirrValsFromTable220();
			writeBrkLirr230();
		}
		calcNewSummaryRecLirr240();
	}

protected void nextLirrcon205()
	{
		lirrbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lirrbrkIO);
		if (isNE(lirrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrbrkIO.getParams());
			dbError580();
		}
		/* In the absence of a proper release function rewrite the*/
		/* record at end of file.  This will enable the calling program*/
		/* to update the record if required*/
		if (isNE(lirrbrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(lirrbrkIO.getCompany(), genoutrec.chdrcoy)
		|| isNE(lirrbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(lirrbrkIO.getStatuz(), varcom.endp)) {
			lirrbrkIO.setFormat(lirrbrkrec);
			lirrbrkIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, lirrbrkIO);
			if (isNE(lirrbrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lirrbrkIO.getParams());
				dbError580();
			}
			else {
				lirrbrkIO.setStatuz(varcom.endp);
			}
		}
	}

protected void moveLirrValsToTable210()
	{
		/*ENTRY*/
		wsaaAmountIn[1].set(lirrbrkIO.getRaAmount());
		/*EXIT*/
	}

protected void moveLirrValsFromTable220()
	{
		/*ENTRY*/
		lirrbrkIO.setRaAmount(wsaaAmountOut[1]);
		/*EXIT*/
	}

protected void writeBrkLirr230()
	{
		entry231();
	}

protected void entry231()
	{
		lirrconIO.setCompany(lirrbrkIO.getCompany());
		lirrconIO.setChdrnum(lirrbrkIO.getChdrnum());
		lirrconIO.setLife(lirrbrkIO.getLife());
		lirrconIO.setCoverage(lirrbrkIO.getCoverage());
		lirrconIO.setRider(lirrbrkIO.getRider());
		lirrconIO.setRasnum(lirrbrkIO.getRasnum());
		lirrconIO.setRngmnt(lirrbrkIO.getRngmnt());
		lirrconIO.setClntpfx(lirrbrkIO.getClntpfx());
		lirrconIO.setClntcoy(lirrbrkIO.getClntcoy());
		lirrconIO.setClntnum(lirrbrkIO.getClntnum());
		lirrconIO.setPlanSuffix(wsaaPlanSuffix);
		lirrconIO.setNonKey(lirrbrkIO.getNonKey());
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
	}

protected void calcNewSummaryRecLirr240()
	{
		entry241();
	}

protected void entry241()
	{
		if (isLT(genoutrec.newSummary, 2)) {
			deleteSummaryLirr250();
		}
		/* Calculate the values of the summarised record or the last*/
		/* policy record. Calculate the new values by subtracting the*/
		/* accumulated amounts from the previous summarised values.*/
		setPrecision(lirrbrkIO.getRaAmount(), 2);
		lirrbrkIO.setRaAmount(sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		/* Write the last policy.*/
		if (isLT(genoutrec.newSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkLirr230();
			return ;
		}
		/* To get here there are still records summarised so rewrite the*/
		/* the record.*/
		lirrbrkIO.setFormat(lirrbrkrec);
		lirrbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lirrbrkIO);
		if (isNE(lirrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrbrkIO.getParams());
			dbError580();
		}
	}

protected void deleteSummaryLirr250()
	{
		/*ENTRY*/
		lirrbrkIO.setFunction(varcom.delet);
		lirrbrkIO.setFormat(lirrbrkrec);
		SmartFileCode.execute(appVars, lirrbrkIO);
		if (isNE(lirrbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrbrkIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void mainProcessingLrrh300()
	{
		para301();
		nextLrrh305();
	}

protected void para301()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
			wsaaTotAmount[wsaaSub.toInt()].set(ZERO);
		}
		moveLrrhValsToTable310();
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix, genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			for (wsaaSub.set(1); !(isGT(wsaaSub, 17)); wsaaSub.add(1)){
				calcAmts400();
			}
			moveLrrhValsFromTable320();
			writeBrkLrrh330();
		}
		calcNewSummaryRecLrrh340();
	}

protected void nextLrrh305()
	{
		lrrhbrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lrrhbrkIO);
		if (isNE(lrrhbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhbrkIO.getParams());
			dbError580();
		}
		/* In the absence of a proper release function rewrite the*/
		/* record at end of file.  This will enable the calling program*/
		/* to update the record if required*/
		if (isNE(lrrhbrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(lrrhbrkIO.getCompany(), genoutrec.chdrcoy)
		|| isNE(lrrhbrkIO.getPlanSuffix(), ZERO)
		|| isEQ(lrrhbrkIO.getStatuz(), varcom.endp)) {
			lrrhbrkIO.setFormat(lrrhbrkrec);
			lrrhbrkIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, lrrhbrkIO);
			if (isNE(lrrhbrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lrrhbrkIO.getParams());
				dbError580();
			}
			else {
				lrrhbrkIO.setStatuz(varcom.endp);
			}
		}
	}

protected void moveLrrhValsToTable310()
	{
		/*ENTRY*/
		wsaaAmountIn[1].set(lrrhbrkIO.getSsretn());
		wsaaAmountIn[2].set(lrrhbrkIO.getSsreast());
		wsaaAmountIn[3].set(lrrhbrkIO.getSsreasf());
		/*EXIT*/
	}

protected void moveLrrhValsFromTable320()
	{
		/*ENTRY*/
		lrrhbrkIO.setSsretn(wsaaAmountOut[1]);
		lrrhbrkIO.setSsreast(wsaaAmountOut[2]);
		lrrhbrkIO.setSsreasf(wsaaAmountOut[3]);
		/*EXIT*/
	}

protected void writeBrkLrrh330()
	{
		entry3231();
	}

protected void entry3231()
	{
		lrrhconIO.setCompany(lrrhbrkIO.getCompany());
		lrrhconIO.setChdrnum(lrrhbrkIO.getChdrnum());
		lrrhconIO.setLife(lrrhbrkIO.getLife());
		lrrhconIO.setCoverage(lrrhbrkIO.getCoverage());
		lrrhconIO.setRider(lrrhbrkIO.getRider());
		lrrhconIO.setClntpfx(lrrhbrkIO.getClntpfx());
		lrrhconIO.setClntcoy(lrrhbrkIO.getClntcoy());
		lrrhconIO.setClntnum(lrrhbrkIO.getClntnum());
		lrrhconIO.setLrkcls(lrrhbrkIO.getLrkcls());
		lrrhconIO.setPlanSuffix(wsaaPlanSuffix);
		lrrhconIO.setNonKey(lrrhbrkIO.getNonKey());
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
	}

protected void calcNewSummaryRecLrrh340()
	{
		entry341();
	}

protected void entry341()
	{
		if (isLT(genoutrec.newSummary, 2)) {
			deleteSummaryLrrh350();
		}
		/* Calculate the values of the summarised record or the last*/
		/* policy record. Calculate the new values by subtracting the*/
		/* accumulated amounts from the previous summarised values.*/
		setPrecision(lrrhbrkIO.getSsretn(), 2);
		lrrhbrkIO.setSsretn(sub(wsaaAmountIn[1], wsaaTotAmount[1]));
		setPrecision(lrrhbrkIO.getSsreast(), 2);
		lrrhbrkIO.setSsreast(sub(wsaaAmountIn[2], wsaaTotAmount[2]));
		setPrecision(lrrhbrkIO.getSsreasf(), 2);
		lrrhbrkIO.setSsreasf(sub(wsaaAmountIn[3], wsaaTotAmount[3]));
		/* Write the last policy.*/
		if (isLT(genoutrec.newSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkLrrh330();
			return ;
		}
		/* To get here there are still records summarised so rewrite the*/
		/* the record.*/
		lrrhbrkIO.setFormat(lrrhbrkrec);
		lrrhbrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lrrhbrkIO);
		if (isNE(lrrhbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhbrkIO.getParams());
			dbError580();
		}
	}

protected void deleteSummaryLrrh350()
	{
		/*ENTRY*/
		lrrhbrkIO.setFunction(varcom.delet);
		lrrhbrkIO.setFormat(lrrhbrkrec);
		SmartFileCode.execute(appVars, lrrhbrkIO);
		if (isNE(lrrhbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhbrkIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void calcAmts400()
	{
		/*ENTRY*/
		if (isEQ(wsaaAmountIn[wsaaSub.toInt()], 0)) {
			wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
			return ;
		}
		compute(wsaaAmountOut[wsaaSub.toInt()], 3).setRounded(div(wsaaAmountIn[wsaaSub.toInt()], genoutrec.oldSummary));
		zrdecplrec.amountIn.set(wsaaAmountOut[wsaaSub.toInt()]);
		a000CallRounding();
		wsaaAmountOut[wsaaSub.toInt()].set(zrdecplrec.amountOut);
		compute(wsaaTotAmount[wsaaSub.toInt()], 2).set(add(wsaaTotAmount[wsaaSub.toInt()], wsaaAmountOut[wsaaSub.toInt()]));
		/*EXIT*/
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		genoutrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(genoutrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(genoutrec.cntcurr);
		zrdecplrec.batctrcde.set(genoutrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			dbError580();
		}
		/*A000-EXIT*/
	}
}
