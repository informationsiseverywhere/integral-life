package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:42
 * Description:
 * Copybook name: TH618REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th618rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th618Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData lrkclss = new FixedLengthStringData(20).isAPartOf(th618Rec, 0);
  	public FixedLengthStringData[] lrkcls = FLSArrayPartOfStructure(5, 4, lrkclss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(lrkclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData lrkcls01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData lrkcls02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData lrkcls03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData lrkcls04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData lrkcls05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(480).isAPartOf(th618Rec, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th618Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th618Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}