package com.csc.life.reassurance.dataaccess.model;


public class Acmxpf {
    private long unique_number;
    private String batccoy;
    private String sacscode;
    private int effdate;
    private String rldgacct;
    private String origcurr;
    private String sacstyp;
    private int tranno;
    private String batcbrn;
    private int batcactyr;
    private int batcactmn;
    private String batctrcde;
    private String batcbatch;
    private String rdocnum;
    private int jrnseq;
    private double origamt;
    private String glsign;
    private String usrprf;
    private String jobnm;
    private String datime;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(String rdocnum) {
		this.rdocnum = rdocnum;
	}
	public int getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}
	public double getOrigamt() {
		return origamt;
	}
	public void setOrigamt(double origamt) {
		this.origamt = origamt;
	}
	public String getGlsign() {
		return glsign;
	}
	public void setGlsign(String glsign) {
		this.glsign = glsign;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}

   

}
