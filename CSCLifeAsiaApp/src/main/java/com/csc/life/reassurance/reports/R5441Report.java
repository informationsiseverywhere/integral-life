package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5441.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5441Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10);
	private FixedLengthStringData dateto = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5441Report() {
		super();
	}


	/**
	 * Print the XML for R5441d01
	 */
	public void printR5441d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 1, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 9, 37));
		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 46, 8));
		rngmnt.setFieldName("rngmnt");
		rngmnt.setInternal(subString(recordData, 54, 4));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 58, 8));
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 66, 3));
		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 69, 17));
		printLayout("R5441d01",			// Record name
			new BaseData[]{			// Fields:
				clntnum,
				clntnm,
				rasnum,
				rngmnt,
				chdrnum,
				currcode,
				raamount
			}
		);

	}

	/**
	 * Print the XML for R5441h01
	 */
	public void printR5441h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		datefrm.setFieldName("datefrm");
		datefrm.setInternal(subString(recordData, 1, 10));
		dateto.setFieldName("dateto");
		dateto.setInternal(subString(recordData, 11, 10));
		printLayout("R5441h01",			// Record name
			new BaseData[]{			// Fields:
				pagnbr,
				datefrm,
				dateto
			}
		);

		currentPrintLine.set(8);
	}


}
