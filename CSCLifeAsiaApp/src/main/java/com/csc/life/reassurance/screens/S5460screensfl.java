package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5460screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {17, 4, 22, 5, 18, 23, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 19, 2, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5460ScreenVars sv = (S5460ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5460screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5460screensfl, 
			sv.S5460screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5460ScreenVars sv = (S5460ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5460screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5460ScreenVars sv = (S5460ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5460screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5460screensflWritten.gt(0))
		{
			sv.s5460screensfl.setCurrentIndex(0);
			sv.S5460screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5460ScreenVars sv = (S5460ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5460screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5460ScreenVars screenVars = (S5460ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.cmdateDisp.setFieldName("cmdateDisp");
				screenVars.rasnum.setFieldName("rasnum");
				screenVars.rngmnt.setFieldName("rngmnt");
				screenVars.raAmount.setFieldName("raAmount");
				screenVars.ovrdind.setFieldName("ovrdind");
				screenVars.retype.setFieldName("retype");
				screenVars.ctdateDisp.setFieldName("ctdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.cmdateDisp.set(dm.getField("cmdateDisp"));
			screenVars.rasnum.set(dm.getField("rasnum"));
			screenVars.rngmnt.set(dm.getField("rngmnt"));
			screenVars.raAmount.set(dm.getField("raAmount"));
			screenVars.ovrdind.set(dm.getField("ovrdind"));
			screenVars.retype.set(dm.getField("retype"));
			screenVars.ctdateDisp.set(dm.getField("ctdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5460ScreenVars screenVars = (S5460ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.cmdateDisp.setFieldName("cmdateDisp");
				screenVars.rasnum.setFieldName("rasnum");
				screenVars.rngmnt.setFieldName("rngmnt");
				screenVars.raAmount.setFieldName("raAmount");
				screenVars.ovrdind.setFieldName("ovrdind");
				screenVars.retype.setFieldName("retype");
				screenVars.ctdateDisp.setFieldName("ctdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("cmdateDisp").set(screenVars.cmdateDisp);
			dm.getField("rasnum").set(screenVars.rasnum);
			dm.getField("rngmnt").set(screenVars.rngmnt);
			dm.getField("raAmount").set(screenVars.raAmount);
			dm.getField("ovrdind").set(screenVars.ovrdind);
			dm.getField("retype").set(screenVars.retype);
			dm.getField("ctdateDisp").set(screenVars.ctdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5460screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5460ScreenVars screenVars = (S5460ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.cmdateDisp.clearFormatting();
		screenVars.rasnum.clearFormatting();
		screenVars.rngmnt.clearFormatting();
		screenVars.raAmount.clearFormatting();
		screenVars.ovrdind.clearFormatting();
		screenVars.retype.clearFormatting();
		screenVars.ctdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5460ScreenVars screenVars = (S5460ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.cmdateDisp.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.rngmnt.setClassString("");
		screenVars.raAmount.setClassString("");
		screenVars.ovrdind.setClassString("");
		screenVars.retype.setClassString("");
		screenVars.ctdateDisp.setClassString("");
	}

/**
 * Clear all the variables in S5460screensfl
 */
	public static void clear(VarModel pv) {
		S5460ScreenVars screenVars = (S5460ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.cmdateDisp.clear();
		screenVars.cmdate.clear();
		screenVars.rasnum.clear();
		screenVars.rngmnt.clear();
		screenVars.raAmount.clear();
		screenVars.ovrdind.clear();
		screenVars.retype.clear();
		screenVars.ctdateDisp.clear();
		screenVars.ctdate.clear();
	}
}
