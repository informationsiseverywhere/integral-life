/*
 * File: P5476.java
 * Date: 30 August 2009 0:28:58
 * Author: Quipoz Limited
 * 
 * Class transformed from P5476.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.procedures.Trcdechk;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.CcalrevTableDAM;
import com.csc.life.reassurance.screens.S5476ScreenVars;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Cash Call Reversal Confirmation.
*
* This is a new program which forms part of the Reassurance
* Development. It controls the Cash Call Reversal Confirmation
* Screen, S5476. If the last transaction on this contract was
* not a cash call, an error message will be displayed and the
* reversal will not be possible.  Alternative action by the
* user will be required first.
*
* The Cash Call Details displayed will be from the Cash Call
* file CCALPF and will be displayed for information only.
* Once ENTER is pressed, the policy is softlocked and an AT
* request submitted.
*
*****************************************************************
* </pre>
*/
public class P5476 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5476");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaForwardTranscode = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private String wsaaTranError = "";

	private FixedLengthStringData wsaaProcessingMsg = new FixedLengthStringData(43);
	private ZonedDecimalData wsaaMsgTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaProcessingMsg, 0).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(wsaaProcessingMsg, 5, FILLER).init(" - ");
	private FixedLengthStringData wsaaMsgTrantype = new FixedLengthStringData(4).isAPartOf(wsaaProcessingMsg, 8);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 12, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgTrandesc = new FixedLengthStringData(30).isAPartOf(wsaaProcessingMsg, 13);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
		/* ERRORS */
	private String f910 = "F910";
	private String r080 = "R080";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String cltsrec = "CLTSREC";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String lifemjarec = "LIFEMJAREC";
	private String ptrnrevrec = "PTRNREVREC";
	private String ccalrevrec = "CCALREVREC";
	private Atreqrec atreqrec = new Atreqrec();
		/*Cash Call Logical For Reversals*/
	private CcalrevTableDAM ccalrevIO = new CcalrevTableDAM();
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5476ScreenVars sv = ScreenProgram.getScreenVars( S5476ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1190, 
		exit1390, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P5476() {
		super();
		screenVars = sv;
		new ScreenModel("S5476", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retrieveContractHeader1020();
		readLifeDetails1030();
		jointLifeDetails1040();
		contractDetails1050();
		begnPtrn1060();
	}

protected void initialise1010()
	{
		wsaaTranError = "N";
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		wsaaForwardTranscode.set(SPACES);
		wsaaMsgTrantype.set(SPACES);
		wsaaMsgTrandesc.set(SPACES);
		wsaaTransArea.set(SPACES);
		wsaaToday.set(ZERO);
		wsaaMsgTranno.set(ZERO);
		wsaaPrimaryChdrnum.set(ZERO);
		wsaaTranDate.set(ZERO);
		wsaaTranTime.set(ZERO);
		wsaaUser.set(ZERO);
		wsaaTodate.set(ZERO);
		wsaaSuppressTo.set(ZERO);
		wsaaTranNum.set(ZERO);
		wsaaPlnsfx.set(ZERO);
		wsaaBbldat.set(ZERO);
		wsaaCfiafiTranno.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		sv.callamt.set(ZERO);
		sv.callrecd.set(ZERO);
		sv.calldt.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5476", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaForwardTranscode.set(t6661rec.trcode);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
	}

protected void retrieveContractHeader1020()
	{
		chdrclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrclmIO.getChdrnum());
		sv.cnttype.set(chdrclmIO.getCnttype());
		sv.cntcurr.set(chdrclmIO.getCntcurr());
	}

protected void readLifeDetails1030()
	{
		lifemjaIO.setParams(SPACES);
		lifemjaIO.setChdrcoy(chdrclmIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrclmIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1040()
	{
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void contractDetails1050()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrclmIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrclmIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrclmIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void begnPtrn1060()
	{
		tranchkrec.tcdeStatuz.set(varcom.oK);
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrclmIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrclmIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		ptrnrevIO.setStatuz(varcom.oK);
		ptrnrevIO.setFormat(ptrnrevrec);
		while ( !(isEQ(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)
		|| isEQ(ptrnrevIO.getStatuz(),varcom.endp)
		|| isEQ(tranchkrec.tcdeStatuz,varcom.mrnf))) {
			readPtrn1100();
		}
		
		if (isNE(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)) {
			sv.trandesOut[varcom.nd.toInt()].set("Y");
			sv.chdrnumErr.set(r080);
			wsspcomn.edterror.set("Y");
			wsaaTranError = "Y";
		}
		ccalrevIO.setParams(SPACES);
		ccalrevIO.setChdrcoy(chdrclmIO.getChdrcoy());
		ccalrevIO.setChdrnum(chdrclmIO.getChdrnum());
		ccalrevIO.setPlanSuffix(ZERO);
		ccalrevIO.setSeqno(ZERO);
		ccalrevIO.setTranno(ptrnrevIO.getTranno());
		ccalrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ccalrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ccalrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		ccalrevIO.setFormat(ccalrevrec);
		while ( !(isEQ(ccalrevIO.getStatuz(),varcom.endp))) {
			loadSubfile1300();
		}
		
	}

protected void readPtrn1100()
	{
		try {
			call1110();
		}
		catch (GOTOException e){
		}
	}

protected void call1110()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if (isNE(ptrnrevIO.getChdrcoy(),chdrclmIO.getChdrcoy())
		|| isNE(ptrnrevIO.getChdrnum(),chdrclmIO.getChdrnum())
		|| isEQ(ptrnrevIO.getStatuz(),varcom.endp)) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)) {
			setMessages1200();
			goTo(GotoLabel.exit1190);
		}
		tranchkrec.codeCheckRec.set(SPACES);
		tranchkrec.tcdeTranCode.set(ptrnrevIO.getBatctrcde());
		tranchkrec.tcdeCompany.set(wsspcomn.company);
		callProgram(Trcdechk.class, tranchkrec.codeCheckRec);
		if (isNE(tranchkrec.tcdeStatuz,varcom.oK)
		&& isNE(tranchkrec.tcdeStatuz,varcom.mrnf)) {
			syserrrec.params.set(tranchkrec.codeCheckRec);
			syserrrec.statuz.set(tranchkrec.tcdeStatuz);
			fatalError600();
		}
		ptrnrevIO.setFunction(varcom.nextr);
	}

protected void setMessages1200()
	{
		display1210();
	}

protected void display1210()
	{
		wsaaMsgTranno.set(ptrnrevIO.getTranno());
		wsaaMsgTrantype.set(ptrnrevIO.getBatctrcde());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaMsgTrandesc.set(descIO.getLongdesc());
		sv.trandes.set(wsaaProcessingMsg);
		sv.trandesOut[varcom.nd.toInt()].set(SPACES);
	}

protected void loadSubfile1300()
	{
		try {
			buildScreen1310();
		}
		catch (GOTOException e){
		}
	}

protected void buildScreen1310()
	{
		SmartFileCode.execute(appVars, ccalrevIO);
		if (isNE(ccalrevIO.getStatuz(),varcom.oK)
		&& isNE(ccalrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ccalrevIO.getParams());
			fatalError600();
		}
		if (isNE(ccalrevIO.getChdrcoy(),chdrclmIO.getChdrcoy())
		|| isNE(ccalrevIO.getChdrnum(),chdrclmIO.getChdrnum())
		|| isNE(ccalrevIO.getTranno(),ptrnrevIO.getTranno())
		|| isEQ(ccalrevIO.getStatuz(),varcom.endp)) {
			ccalrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1390);
		}
		sv.planSuffix.set(ccalrevIO.getPlanSuffix());
		sv.life.set(ccalrevIO.getLife());
		sv.coverage.set(ccalrevIO.getCoverage());
		sv.rider.set(ccalrevIO.getRider());
		sv.calldt.set(ccalrevIO.getCalldt());
		sv.currcode.set(ccalrevIO.getCurrcode());
		sv.callamt.set(ccalrevIO.getCallamt());
		sv.callrecd.set(ccalrevIO.getCallrecd());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5476", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		ccalrevIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaTranError,"Y")) {
			sv.chdrnumErr.set(r080);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		|| isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.enttyp.set(chdrclmIO.getChdrpfx());
		sftlockrec.company.set(chdrclmIO.getChdrcoy());
		sftlockrec.entity.set(chdrclmIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrclmIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaPlnsfx.set(ZERO);
		wsaaSupflag.set("N");
		wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaSuppressTo.set(ZERO);
		wsaaCfiafiTranno.set(ZERO);
		wsaaCfiafiTranCode.set(SPACES);
		atreqrec.transArea.set(wsaaTransArea);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
