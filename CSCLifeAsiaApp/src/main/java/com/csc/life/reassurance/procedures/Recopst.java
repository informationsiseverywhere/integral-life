/*
 * File: Recopst.java
 * Date: 30 August 2009 2:04:27
 * Author: Quipoz Limited
 *
 * Class transformed from RECOPST.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.RecoTableDAM;
import com.csc.life.reassurance.recordstructures.Recopstrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Reassurance Costing Posting Subroutine.
*
*
*  This subroutine will be called from the main module of the
*  Reassurance Costing History - B5456.
*
*  It will write costing history records and carry out the
*  necessary ledger posting for each costing event.
*
*  It will perform the following functions:
*
*  1) Creation of RECO record to hold all the costing
*     information.
*
*  2) Conversion of Premium Currency where necessary.
*
*  3) Posting of all relevant ledger entriesd via 'LIFACMV',
*     as per the T5645 item of 'RECOPST'.
*
*  Processing.
*  -----------
*
*  Set the return STATUZ in linkage to O-K.
*
*  Process only if a function of 'RCST' has been passed.
*
*  Read table T5645 for sub account details of the Contract.
*
*  Read table T5688 to find out if the contract type wants
*  Component level accounting.
*
*  Create a RECO record to hold all the costing information.
*
*  Posting of all relevant Ledger entries, via LIFACMV, as
*  per the T5645 item of the same name as this subroutine.
*
*
*****************************************************************
* </pre>
*/
public class Recopst extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "RECOPST";
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);

	private FixedLengthStringData wsaaWorkAreas = new FixedLengthStringData(34);
	private ZonedDecimalData wsaaNetPrem = new ZonedDecimalData(13, 2).isAPartOf(wsaaWorkAreas, 0);
	private PackedDecimalData wsaaRecovAmt = new PackedDecimalData(17, 2).isAPartOf(wsaaWorkAreas, 13);
	private PackedDecimalData wsaaTotClaim = new PackedDecimalData(18, 2).isAPartOf(wsaaWorkAreas, 22);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaWorkAreas, 32);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private String e049 = "E049";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5688 = "T5688";
	private String itdmrec = "ITEMREC";
	private String recorec = "RECOREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
		/*Reassurance Costing Details Logical*/
	private RecoTableDAM recoIO = new RecoTableDAM();
	private Recopstrec recopstrec = new Recopstrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		exit1099
	}

	public Recopst() {
		super();
	}

public void mainline(Object... parmArray)
	{
		recopstrec.recopstRec = convertAndSetParam(recopstrec.recopstRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaTransTime.set(getCobolTime());
		if (isNE(recopstrec.function,"RCST")) {
			syserrrec.statuz.set(e049);
			syserr3000();
		}
		initialise100();
		preparations200();
		createReco300();
		postingProcessing400();
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		try {
			initial101();
		}
		catch (GOTOException e){
		}
	}

protected void initial101()
	{
		recopstrec.statuz.set(varcom.oK);
		initialize(wsaaWorkAreas);
		wsaaNetPrem.set(ZERO);
		wsaaSub1.set(ZERO);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(recopstrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError2000();
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(recopstrec.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItmfrm(recopstrec.effdate);
		itdmIO.setItemitem(recopstrec.cnttype);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError2000();
			goTo(GotoLabel.exit190);
		}
		if (isNE(itdmIO.getItemcoy(),recopstrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),recopstrec.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void preparations200()
	{
		/*CURRENCY-CHECK*/
		wsaaRecovAmt.set(recopstrec.recovAmt);
		compute(wsaaNetPrem, 3).setRounded(sub(add(recopstrec.prem,recopstrec.taxamt),recopstrec.compay));
		/*EXIT*/
	}

protected void createReco300()
	{
		reco301();
	}

protected void reco301()
	{
		recoIO.setParams(SPACES);
		recoIO.setChdrcoy(recopstrec.chdrcoy);
		recoIO.setChdrnum(recopstrec.chdrnum);
		recoIO.setLife(recopstrec.life);
		recoIO.setCoverage(recopstrec.coverage);
		recoIO.setRider(recopstrec.rider);
		recoIO.setPlanSuffix(recopstrec.planSuffix);
		recoIO.setRasnum(recopstrec.rasnum);
		recoIO.setSeqno(recopstrec.seqno);
		recoIO.setCostdate(recopstrec.costdate);
		recoIO.setValidflag(recopstrec.validflag);
		recoIO.setRetype(recopstrec.retype);
		recoIO.setRngmnt(recopstrec.rngmnt);
		recoIO.setSraramt(recopstrec.sraramt);
		recoIO.setRaAmount(recopstrec.raAmount);
		recoIO.setCtdate(recopstrec.ctdate);
		recoIO.setOrigcurr(recopstrec.origcurr);
		recoIO.setPrem(recopstrec.prem);
		recoIO.setCompay(recopstrec.compay);
		recoIO.setTaxamt(recopstrec.taxamt);
		recoIO.setRefundfe(ZERO);
		recoIO.setBatccoy(recopstrec.batccoy);
		recoIO.setBatcbrn(recopstrec.batcbrn);
		recoIO.setBatcactyr(recopstrec.batcactyr);
		recoIO.setBatcactmn(recopstrec.batcactmn);
		recoIO.setBatctrcde(recopstrec.batctrcde);
		recoIO.setBatcbatch(recopstrec.batcbatch);
		recoIO.setTranno(recopstrec.tranno);
		recoIO.setRcstfrq(recopstrec.rcstfrq);
		recoIO.setFormat(recorec);
		recoIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, recoIO);
		if (isNE(recoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(recoIO.getParams());
			dbError2000();
		}
	}

protected void postingProcessing400()
	{
		start401();
		posting402();
	}

protected void start401()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(recopstrec.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(recopstrec.batctrcde);
		descIO.setLanguage(recopstrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			dbError2000();
		}
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.jrnseq.set(recopstrec.seqno);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.batccoy.set(recopstrec.batccoy);
		lifacmvrec1.batcbrn.set(recopstrec.batcbrn);
		lifacmvrec1.batcactyr.set(recopstrec.batcactyr);
		lifacmvrec1.batcactmn.set(recopstrec.batcactmn);
		lifacmvrec1.batctrcde.set(recopstrec.batctrcde);
		lifacmvrec1.batcbatch.set(recopstrec.batcbatch);
		lifacmvrec1.rdocnum.set(recopstrec.chdrnum);
		lifacmvrec1.tranno.set(recopstrec.tranno);
		lifacmvrec1.rldgcoy.set(recopstrec.chdrcoy);
		lifacmvrec1.origcurr.set(recopstrec.origcurr);
		lifacmvrec1.genlcoy.set(recopstrec.chdrcoy);
		lifacmvrec1.effdate.set(recopstrec.costdate);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(recopstrec.effdate);
		lifacmvrec1.transactionTime.set(wsaaTransTime);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.termid.set(SPACES);
		lifacmvrec1.substituteCode[1].set(recopstrec.cnttype);
		lifacmvrec1.tranref.set(recopstrec.chdrnum);
		wsaaRldgChdrnum.set(recopstrec.chdrnum);
		wsaaRldgLife.set(recopstrec.life);
		wsaaRldgCoverage.set(recopstrec.coverage);
		wsaaRldgRider.set(recopstrec.rider);
		wsaaPlan.set(recopstrec.planSuffix);
		wsaaRldgPlanSuff.set(wsaaPlansuff);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec1.substituteCode[6].set(recopstrec.crtable);
		}
		else {
			lifacmvrec1.substituteCode[6].set(SPACES);
		}
	}

protected void posting402()
	{
		if (isEQ(recopstrec.function,"RCST")) {
			costingPosting410();
		}
		/*EXIT*/
	}

protected void costingPosting410()
	{
		/*COST-POST*/
		postPremium420();
		postCommission430();
		postTax440();
		postNetPrem450();
		/*EXIT*/
	}

protected void postPremium420()
	{
		/*PREM-POST*/
		lifacmvrec1.origamt.set(recopstrec.prem);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub1.set(4);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			wsaaSub1.set(1);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(recopstrec.chdrnum);
		}
		callLifacmv1000();
		/*EXIT*/
	}

protected void postCommission430()
	{
		/*COMM-POST*/
		lifacmvrec1.origamt.set(recopstrec.compay);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub1.set(5);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			wsaaSub1.set(2);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(recopstrec.chdrnum);
		}
		callLifacmv1000();
		/*EXIT*/
	}

protected void postTax440()
	{
		/*TAX-POST*/
		lifacmvrec1.origamt.set(recopstrec.taxamt);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub1.set(7);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			wsaaSub1.set(6);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(recopstrec.chdrnum);
		}
		callLifacmv1000();
		/*EXIT*/
	}

protected void postNetPrem450()
	{
		/*NPREM-POST*/
		lifacmvrec1.origamt.set(wsaaNetPrem);
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(recopstrec.rasnum);
		wsaaSub1.set(3);
		callLifacmv1000();
		/*EXIT*/
	}

protected void callLifacmv1000()
	{
		try {
			postings1001();
		}
		catch (GOTOException e){
		}
	}

protected void postings1001()
	{
		if (isEQ(lifacmvrec1.origamt,ZERO)) {
			goTo(GotoLabel.exit1099);
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserr3000();
		}
	}

protected void dbError2000()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		recopstrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void syserr3000()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		recopstrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
