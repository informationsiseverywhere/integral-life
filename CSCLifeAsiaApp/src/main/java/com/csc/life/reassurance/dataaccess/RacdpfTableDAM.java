package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RacdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:13
 * Class transformed from RACDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RacdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 163;
	public FixedLengthStringData racdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData racdpfRecord = racdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(racdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(racdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(racdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(racdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(racdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(racdrec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(racdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(racdrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(racdrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(racdrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(racdrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(racdrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(racdrec);
	public FixedLengthStringData retype = DD.retype.copy().isAPartOf(racdrec);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(racdrec);
	public PackedDecimalData raAmount = DD.raamount.copy().isAPartOf(racdrec);
	public PackedDecimalData ctdate = DD.ctdate.copy().isAPartOf(racdrec);
	public PackedDecimalData cmdate = DD.cmdate.copy().isAPartOf(racdrec);
	public PackedDecimalData reasper = DD.reasper.copy().isAPartOf(racdrec);
	public PackedDecimalData rrevdt = DD.rrevdt.copy().isAPartOf(racdrec);
	public PackedDecimalData recovamt = DD.recovamt.copy().isAPartOf(racdrec);
	public FixedLengthStringData cestype = DD.cestype.copy().isAPartOf(racdrec);
	public FixedLengthStringData ovrdind = DD.ovrdind.copy().isAPartOf(racdrec);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(racdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(racdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(racdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(racdrec);
	public PackedDecimalData rrtdat = DD.rrtdat.copy().isAPartOf(racdrec);//8
	public PackedDecimalData rrtfrm = DD.rrtfrm.copy().isAPartOf(racdrec);//8
	public PackedDecimalData pendcstto = DD.ctdate.copy().isAPartOf(racdrec);
	public FixedLengthStringData prorateflag = DD.validflag.copy().isAPartOf(racdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RacdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RacdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RacdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RacdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RacdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RacdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RacdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RACDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"RASNUM, " +
							"TRANNO, " +
							"SEQNO, " +
							"VALIDFLAG, " +
							"CURRCODE, " +
							"CURRFROM, " +
							"CURRTO, " +
							"RETYPE, " +
							"RNGMNT, " +
							"RAAMOUNT, " +
							"CTDATE, " +
							"CMDATE, " +
							"REASPER, " +
							"RREVDT, " +
							"RECOVAMT, " +
							"CESTYPE, " +
							"OVRDIND, " +
							"LRKCLS, " +
							"RRTDAT,"+
				            "RRTFRM,"+
				            "PENDCSTTO, " +
				            "PRORATEFLAG, "+
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     rasnum,
                                     tranno,
                                     seqno,
                                     validflag,
                                     currcode,
                                     currfrom,
                                     currto,
                                     retype,
                                     rngmnt,
                                     raAmount,
                                     ctdate,
                                     cmdate,
                                     reasper,
                                     rrevdt,
                                     recovamt,
                                     cestype,
                                     ovrdind,
                                     lrkcls,
                                     rrtdat,
                                     rrtfrm,
                                     pendcstto,
                                     prorateflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		rasnum.clear();
  		tranno.clear();
  		seqno.clear();
  		validflag.clear();
  		currcode.clear();
  		currfrom.clear();
  		currto.clear();
  		retype.clear();
  		rngmnt.clear();
  		raAmount.clear();
  		ctdate.clear();
  		cmdate.clear();
  		reasper.clear();
  		rrevdt.clear();
  		recovamt.clear();
  		cestype.clear();
  		ovrdind.clear();
  		lrkcls.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		rrtdat.clear();
		rrtfrm.clear();
        pendcstto.clear();
        prorateflag.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRacdrec() {
  		return racdrec;
	}

	public FixedLengthStringData getRacdpfRecord() {
  		return racdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRacdrec(what);
	}

	public void setRacdrec(Object what) {
  		this.racdrec.set(what);
	}

	public void setRacdpfRecord(Object what) {
  		this.racdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(racdrec.getLength());
		result.set(racdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}