package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:44
 * Description:
 * Copybook name: ACMVRASKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvraskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvrasFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvrasKey = new FixedLengthStringData(64).isAPartOf(acmvrasFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvrasBatccoy = new FixedLengthStringData(1).isAPartOf(acmvrasKey, 0);
  	public FixedLengthStringData acmvrasSacscode = new FixedLengthStringData(2).isAPartOf(acmvrasKey, 1);
  	public FixedLengthStringData acmvrasRldgacct = new FixedLengthStringData(16).isAPartOf(acmvrasKey, 3);
  	public PackedDecimalData acmvrasEffdate = new PackedDecimalData(8, 0).isAPartOf(acmvrasKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(acmvrasKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvrasFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvrasFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}