package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;

public class B5456DTO {
    /* SQL-RACDPF */
    private String sqlChdrcoy;
    private String sqlChdrnum;
    private String sqlLife;
    private String sqlCoverage;
    private String sqlRider;
    private int sqlPlnsfx;
    private int sqlSeqno;
    private String sqlRasnum;
    private String sqlRngmnt;
    private int sqlCurrfrom;
    private int sqlTranno;
    private String sqlValidflag;
    private String sqlCurrcode;
    private int sqlCurrto;
    private String sqlRetype;
    private BigDecimal sqlRaamount;
    private int sqlCtdate;
    private int sqlCmdate;
    private BigDecimal sqlReasper;
    private BigDecimal sqlRecovamt;
    private String sqlCestype;
    private String sqlLrkcls;
    private int sqlPendCstTo; //PINNACLE-1834 Frequency Change
    private String sqlProrateflag;//PINNACLE-1834 Frequency Change
    
    // by yy for ILPI-231
    // CHDRPF
    private int sqlChTranno;
    private String sqlChCnttype;
    private String sqlChStatcode;
    private String sqlChPstatcode;
    private String sqlChBillfreq;
    private String sqlChCntcurr;
    private int sqlChPolsum;
    private String sqlChBillchnl;
    private int sqlChOccdate;
    private int sqlChPolinc;

    // COVRPF
    private int sqlCoCrrcd;
    private String sqlCoCrtable;
    private BigDecimal sqlCoSumins;
    private String sqlCoJlife;
    private BigDecimal sqlCoInstprem;
    private String sqlCoChdrcoy;
    private String sqlCoChdrnum;
    private String sqlCoLife;
    private String sqlCoCoverage;
    private String sqlCoRider;
    private String sqlCoMortcls;
    private int sqlCoRiskCessDate;
    private String sqlCoValidflag;

    // RASAPF
    private String sqlAgntnum;

    // LIFEPF
    private int sqlAnbAtCcd00;
    private String sqlCltsex00;
    private int sqlAnbAtCcd01;
    private String sqlCltsex01;

    // LEXTPF
    private int sqlMaxAgerate;
    private BigDecimal sqlMaxOppc;
    private int sqlMaxInsprm;
    private BigDecimal sqlOppc2;

    private long sqlRaUniqueNum;
    private long sqlChUniqueNum;
    private int sqlChCurrfrom;
    private int rrtdat;
    private int rrtfrm;
    
    private int corrtdat;
    private int corrtfrm;
    
  
	public String getSqlChdrcoy() {
        return sqlChdrcoy;
    }
    public void setSqlChdrcoy(String sqlChdrcoy) {
        this.sqlChdrcoy = sqlChdrcoy;
    }
    public String getSqlChdrnum() {
        return sqlChdrnum;
    }
    public void setSqlChdrnum(String sqlChdrnum) {
        this.sqlChdrnum = sqlChdrnum;
    }
    public String getSqlLife() {
        return sqlLife;
    }
    public void setSqlLife(String sqlLife) {
        this.sqlLife = sqlLife;
    }
    public String getSqlCoverage() {
        return sqlCoverage;
    }
    public void setSqlCoverage(String sqlCoverage) {
        this.sqlCoverage = sqlCoverage;
    }
    public String getSqlRider() {
        return sqlRider;
    }
    public void setSqlRider(String sqlRider) {
        this.sqlRider = sqlRider;
    }
    public int getSqlPlnsfx() {
        return sqlPlnsfx;
    }
    public void setSqlPlnsfx(int sqlPlnsfx) {
        this.sqlPlnsfx = sqlPlnsfx;
    }
    public int getSqlSeqno() {
        return sqlSeqno;
    }
    public void setSqlSeqno(int sqlSeqno) {
        this.sqlSeqno = sqlSeqno;
    }
    public String getSqlRasnum() {
        return sqlRasnum;
    }
    public void setSqlRasnum(String sqlRasnum) {
        this.sqlRasnum = sqlRasnum;
    }
    public String getSqlRngmnt() {
        return sqlRngmnt;
    }
    public void setSqlRngmnt(String sqlRngmnt) {
        this.sqlRngmnt = sqlRngmnt;
    }
    public int getSqlCurrfrom() {
        return sqlCurrfrom;
    }
    public void setSqlCurrfrom(int sqlCurrfrom) {
        this.sqlCurrfrom = sqlCurrfrom;
    }
    public int getSqlTranno() {
        return sqlTranno;
    }
    public void setSqlTranno(int sqlTranno) {
        this.sqlTranno = sqlTranno;
    }
    public String getSqlValidflag() {
        return sqlValidflag;
    }
    public void setSqlValidflag(String sqlValidflag) {
        this.sqlValidflag = sqlValidflag;
    }
    public String getSqlCurrcode() {
        return sqlCurrcode;
    }
    public void setSqlCurrcode(String sqlCurrcode) {
        this.sqlCurrcode = sqlCurrcode;
    }
    public int getSqlCurrto() {
        return sqlCurrto;
    }
    public void setSqlCurrto(int sqlCurrto) {
        this.sqlCurrto = sqlCurrto;
    }
    public String getSqlRetype() {
        return sqlRetype;
    }
    public void setSqlRetype(String sqlRetype) {
        this.sqlRetype = sqlRetype;
    }
    public BigDecimal getSqlRaamount() {
        return sqlRaamount;
    }
    public void setSqlRaamount(BigDecimal sqlRaamount) {
        this.sqlRaamount = sqlRaamount;
    }
    public int getSqlCtdate() {
        return sqlCtdate;
    }
    public void setSqlCtdate(int sqlCtdate) {
        this.sqlCtdate = sqlCtdate;
    }
    public int getSqlCmdate() {
        return sqlCmdate;
    }
    public void setSqlCmdate(int sqlCmdate) {
        this.sqlCmdate = sqlCmdate;
    }
    public BigDecimal getSqlReasper() {
        return sqlReasper;
    }
    public void setSqlReasper(BigDecimal sqlReasper) {
        this.sqlReasper = sqlReasper;
    }
    public BigDecimal getSqlRecovamt() {
        return sqlRecovamt;
    }
    public void setSqlRecovamt(BigDecimal sqlRecovamt) {
        this.sqlRecovamt = sqlRecovamt;
    }
    public String getSqlCestype() {
        return sqlCestype;
    }
    public void setSqlCestype(String sqlCestype) {
        this.sqlCestype = sqlCestype;
    }
    public String getSqlLrkcls() {
        return sqlLrkcls;
    }
    public void setSqlLrkcls(String sqlLrkcls) {
        this.sqlLrkcls = sqlLrkcls;
    }
    public int getSqlChTranno() {
        return sqlChTranno;
    }
    public void setSqlChTranno(int sqlChTranno) {
        this.sqlChTranno = sqlChTranno;
    }
    public String getSqlChCnttype() {
        return sqlChCnttype;
    }
    public void setSqlChCnttype(String sqlChCnttype) {
        this.sqlChCnttype = sqlChCnttype;
    }
    public String getSqlChStatcode() {
        return sqlChStatcode;
    }
    public void setSqlChStatcode(String sqlChStatcode) {
        this.sqlChStatcode = sqlChStatcode;
    }
    public String getSqlChPstatcode() {
        return sqlChPstatcode;
    }
    public void setSqlChPstatcode(String sqlChPstatcode) {
        this.sqlChPstatcode = sqlChPstatcode;
    }
    public String getSqlChBillfreq() {
        return sqlChBillfreq;
    }
    public void setSqlChBillfreq(String sqlChBillfreq) {
        this.sqlChBillfreq = sqlChBillfreq;
    }
    public String getSqlChCntcurr() {
        return sqlChCntcurr;
    }
    public void setSqlChCntcurr(String sqlChCntcurr) {
        this.sqlChCntcurr = sqlChCntcurr;
    }
    public int getSqlChPolsum() {
        return sqlChPolsum;
    }
    public void setSqlChPolsum(int sqlChPolsum) {
        this.sqlChPolsum = sqlChPolsum;
    }
    public String getSqlChBillchnl() {
        return sqlChBillchnl;
    }
    public void setSqlChBillchnl(String sqlChBillchnl) {
        this.sqlChBillchnl = sqlChBillchnl;
    }
    public int getSqlChOccdate() {
        return sqlChOccdate;
    }
    public void setSqlChOccdate(int sqlChOccdate) {
        this.sqlChOccdate = sqlChOccdate;
    }
    public int getSqlChPolinc() {
        return sqlChPolinc;
    }
    public void setSqlChPolinc(int sqlChPolinc) {
        this.sqlChPolinc = sqlChPolinc;
    }
    public int getSqlCoCrrcd() {
        return sqlCoCrrcd;
    }
    public void setSqlCoCrrcd(int sqlCoCrrcd) {
        this.sqlCoCrrcd = sqlCoCrrcd;
    }
    public String getSqlCoCrtable() {
        return sqlCoCrtable;
    }
    public void setSqlCoCrtable(String sqlCoCrtable) {
        this.sqlCoCrtable = sqlCoCrtable;
    }
    public BigDecimal getSqlCoSumins() {
        return sqlCoSumins;
    }
    public void setSqlCoSumins(BigDecimal sqlCoSumins) {
        this.sqlCoSumins = sqlCoSumins;
    }
    public String getSqlCoJlife() {
        return sqlCoJlife;
    }
    public void setSqlCoJlife(String sqlCoJlife) {
        this.sqlCoJlife = sqlCoJlife;
    }
    public BigDecimal getSqlCoInstprem() {
        return sqlCoInstprem;
    }
    public void setSqlCoInstprem(BigDecimal sqlCoInstprem) {
        this.sqlCoInstprem = sqlCoInstprem;
    }
    public String getSqlCoChdrcoy() {
        return sqlCoChdrcoy;
    }
    public void setSqlCoChdrcoy(String sqlCoChdrcoy) {
        this.sqlCoChdrcoy = sqlCoChdrcoy;
    }
    public String getSqlCoChdrnum() {
        return sqlCoChdrnum;
    }
    public void setSqlCoChdrnum(String sqlCoChdrnum) {
        this.sqlCoChdrnum = sqlCoChdrnum;
    }
    public String getSqlCoLife() {
        return sqlCoLife;
    }
    public void setSqlCoLife(String sqlCoLife) {
        this.sqlCoLife = sqlCoLife;
    }
    public String getSqlCoCoverage() {
        return sqlCoCoverage;
    }
    public void setSqlCoCoverage(String sqlCoCoverage) {
        this.sqlCoCoverage = sqlCoCoverage;
    }
    public String getSqlCoRider() {
        return sqlCoRider;
    }
    public void setSqlCoRider(String sqlCoRider) {
        this.sqlCoRider = sqlCoRider;
    }
    public String getSqlCoMortcls() {
        return sqlCoMortcls;
    }
    public void setSqlCoMortcls(String sqlCoMortcls) {
        this.sqlCoMortcls = sqlCoMortcls;
    }
    public int getSqlCoRiskCessDate() {
        return sqlCoRiskCessDate;
    }
    public void setSqlCoRiskCessDate(int sqlCoRiskCessDate) {
        this.sqlCoRiskCessDate = sqlCoRiskCessDate;
    }
    public String getSqlCoValidflag() {
        return sqlCoValidflag;
    }
    public void setSqlCoValidflag(String sqlCoValidflag) {
        this.sqlCoValidflag = sqlCoValidflag;
    }
    public String getSqlAgntnum() {
        return sqlAgntnum;
    }
    public void setSqlAgntnum(String sqlAgntnum) {
        this.sqlAgntnum = sqlAgntnum;
    }
    public int getSqlAnbAtCcd00() {
        return sqlAnbAtCcd00;
    }
    public void setSqlAnbAtCcd00(int sqlAnbAtCcd00) {
        this.sqlAnbAtCcd00 = sqlAnbAtCcd00;
    }
    public String getSqlCltsex00() {
        return sqlCltsex00;
    }
    public void setSqlCltsex00(String sqlCltsex00) {
        this.sqlCltsex00 = sqlCltsex00;
    }
    public int getSqlAnbAtCcd01() {
        return sqlAnbAtCcd01;
    }
    public void setSqlAnbAtCcd01(int sqlAnbAtCcd01) {
        this.sqlAnbAtCcd01 = sqlAnbAtCcd01;
    }
    public String getSqlCltsex01() {
        return sqlCltsex01;
    }
    public void setSqlCltsex01(String sqlCltsex01) {
        this.sqlCltsex01 = sqlCltsex01;
    }
    public int getSqlMaxAgerate() {
        return sqlMaxAgerate;
    }
    public void setSqlMaxAgerate(int sqlMaxAgerate) {
        this.sqlMaxAgerate = sqlMaxAgerate;
    }
    public BigDecimal getSqlMaxOppc() {
        return sqlMaxOppc;
    }
    public void setSqlMaxOppc(BigDecimal sqlMaxOppc) {
        this.sqlMaxOppc = sqlMaxOppc;
    }
    public int getSqlMaxInsprm() {
        return sqlMaxInsprm;
    }
    public void setSqlMaxInsprm(int sqlMaxInsprm) {
        this.sqlMaxInsprm = sqlMaxInsprm;
    }
    public BigDecimal getSqlOppc2() {
        return sqlOppc2;
    }
    public void setSqlOppc2(BigDecimal sqlOppc2) {
        this.sqlOppc2 = sqlOppc2;
    }
    public long getSqlRaUniqueNum() {
        return sqlRaUniqueNum;
    }
    public void setSqlRaUniqueNum(long sqlRaUniqueNum) {
        this.sqlRaUniqueNum = sqlRaUniqueNum;
    }
    public long getSqlChUniqueNum() {
        return sqlChUniqueNum;
    }
    public void setSqlChUniqueNum(long sqlChUniqueNum) {
        this.sqlChUniqueNum = sqlChUniqueNum;
    }
    public int getSqlChCurrfrom() {
        return sqlChCurrfrom;
    }
    public void setSqlChCurrfrom(int sqlChCurrfrom) {
        this.sqlChCurrfrom = sqlChCurrfrom;
    }
    /**
  	 * @return the rrtdat
  	 */
  	public int getRrtdat() {
  		return rrtdat;
  	}
  	/**
  	 * @param rrtdat the rrtdat to set
  	 */
  	public void setRrtdat(int rrtdat) {
  		this.rrtdat = rrtdat;
  	}
  	/**
  	 * @return the rrtfrm
  	 */
  	public int getRrtfrm() {
  		return rrtfrm;
  	}
  	/**
  	 * @param rrtfrm the rrtfrm to set
  	 */
  	public void setRrtfrm(int rrtfrm) {
  		this.rrtfrm = rrtfrm;
  	}

	/**
	 * @return the corrtdat
	 */
	public int getCorrtdat() {
		return corrtdat;
	}
	/**
	 * @param corrtdat the corrtdat to set
	 */
	public void setCorrtdat(int corrtdat) {
		this.corrtdat = corrtdat;
	}
	/**
	 * @return the corrtfrm
	 */
	public int getCorrtfrm() {
		return corrtfrm;
	}
	/**
	 * @param corrtfrm the corrtfrm to set
	 */
	public void setCorrtfrm(int corrtfrm) {
		this.corrtfrm = corrtfrm;
	}
	public int getSqlPendCstTo() {
		return sqlPendCstTo;
	}
	public void setSqlPendCstTo(int sqlPendCstTo) {
		this.sqlPendCstTo = sqlPendCstTo;
	}
	public String getSqlProrateflag() {
		return sqlProrateflag;
	}
	public void setSqlProrateflag(String sqlProrateflag) {
		this.sqlProrateflag = sqlProrateflag;
	}
	
	 
}
