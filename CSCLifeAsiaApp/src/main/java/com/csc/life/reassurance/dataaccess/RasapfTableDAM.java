package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RasapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:14
 * Class transformed from RASAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RasapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 168;
	public FixedLengthStringData rasarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData rasapfRecord = rasarec;
	
	public FixedLengthStringData raspfx = DD.raspfx.copy().isAPartOf(rasarec);
	public FixedLengthStringData rascoy = DD.rascoy.copy().isAPartOf(rasarec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(rasarec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(rasarec);
	public FixedLengthStringData clntcoy = DD.clntcoy.copy().isAPartOf(rasarec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(rasarec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(rasarec);
	public FixedLengthStringData rapaymth = DD.rapaymth.copy().isAPartOf(rasarec);
	public FixedLengthStringData rapayfrq = DD.rapayfrq.copy().isAPartOf(rasarec);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(rasarec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(rasarec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(rasarec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(rasarec);
	public PackedDecimalData dtepay = DD.dtepay.copy().isAPartOf(rasarec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(rasarec);
	public PackedDecimalData minsta = DD.minsta.copy().isAPartOf(rasarec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(rasarec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(rasarec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(rasarec);
	public PackedDecimalData commdt = DD.commdt.copy().isAPartOf(rasarec);
	public PackedDecimalData termdt = DD.termdt.copy().isAPartOf(rasarec);
	public FixedLengthStringData reasst = DD.reasst.copy().isAPartOf(rasarec);
	public PackedDecimalData stmtdt = DD.stmtdt.copy().isAPartOf(rasarec);
	public PackedDecimalData nxtpay = DD.nxtpay.copy().isAPartOf(rasarec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(rasarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(rasarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(rasarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RasapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RasapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RasapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RasapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RasapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RasapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RasapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RASAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"RASPFX, " +
							"RASCOY, " +
							"RASNUM, " +
							"VALIDFLAG, " +
							"CLNTCOY, " +
							"CLNTNUM, " +
							"PAYCLT, " +
							"RAPAYMTH, " +
							"RAPAYFRQ, " +
							"FACTHOUS, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"CURRCODE, " +
							"DTEPAY, " +
							"AGNTNUM, " +
							"MINSTA, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"COMMDT, " +
							"TERMDT, " +
							"REASST, " +
							"STMTDT, " +
							"NXTPAY, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     raspfx,
                                     rascoy,
                                     rasnum,
                                     validflag,
                                     clntcoy,
                                     clntnum,
                                     payclt,
                                     rapaymth,
                                     rapayfrq,
                                     facthous,
                                     bankkey,
                                     bankacckey,
                                     currcode,
                                     dtepay,
                                     agntnum,
                                     minsta,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     commdt,
                                     termdt,
                                     reasst,
                                     stmtdt,
                                     nxtpay,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		raspfx.clear();
  		rascoy.clear();
  		rasnum.clear();
  		validflag.clear();
  		clntcoy.clear();
  		clntnum.clear();
  		payclt.clear();
  		rapaymth.clear();
  		rapayfrq.clear();
  		facthous.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		currcode.clear();
  		dtepay.clear();
  		agntnum.clear();
  		minsta.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		commdt.clear();
  		termdt.clear();
  		reasst.clear();
  		stmtdt.clear();
  		nxtpay.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRasarec() {
  		return rasarec;
	}

	public FixedLengthStringData getRasapfRecord() {
  		return rasapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRasarec(what);
	}

	public void setRasarec(Object what) {
  		this.rasarec.set(what);
	}

	public void setRasapfRecord(Object what) {
  		this.rasapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(rasarec.getLength());
		result.set(rasarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}