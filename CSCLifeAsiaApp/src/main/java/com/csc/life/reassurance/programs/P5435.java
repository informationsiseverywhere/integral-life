/*
 * File: P5435.java
 * Date: 30 August 2009 0:25:43
 * Author: Quipoz Limited
 * 
 * Class transformed from P5435.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.LrrhTableDAM;
import com.csc.life.reassurance.screens.S5435ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Life Reassurance Experience Scroll.
*
* Overview
* ========
*
* This program forms part of the Reassurance Development.
*
* This program is invoked when an action of 'A' is selected
* from the Reassurance Experience Enquiry Submenu, S5434.
*
* This program will display all LRRH records for the client
* selected. If a risk class is also selected, it will only
* display the LRRH records for that particular risk class.
*
* Processing
* ==========
*
* 1000-Initialise.
*
* Clear the screen.
* Read CLTS in order to obtain the client details.
* Load the subfile with the LRRH records.
*
* 2000-Screen-Edit.
*
* Display the screen.
*
* 3000-Update.
*
* No updating required.
*
* 4000-Where-Next.
*
* Add 1 to the program pointer.
*
*****************************************************************
* </pre>
*/
public class P5435 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5435");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String cltsrec = "CLTSREC";
	private String lrrhrec = "LRRHREC";
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Life Retention & Reassurance History*/
	private LrrhTableDAM lrrhIO = new LrrhTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5435ScreenVars sv = ScreenProgram.getScreenVars( S5435ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr1180, 
		exit1190, 
		preExit, 
		lgnmExit, 
		plainExit, 
		payeeExit
	}

	public P5435() {
		super();
		screenVars = sv;
		new ScreenModel("S5435", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.cltdob.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5435", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		cltsIO.setParams(SPACES);
		cltsIO.setDataKey(wsspcomn.clntkey);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
		sv.clntnum.set(cltsIO.getClntnum());
		sv.cltdob.set(cltsIO.getCltdob());
		sv.cltsex.set(cltsIO.getCltsex());
		sv.cltpcode.set(cltsIO.getCltpcode());
		sv.ctrycode.set(cltsIO.getCtrycode());
		sv.cltaddr01.set(cltsIO.getCltaddr01());
		sv.cltaddr02.set(cltsIO.getCltaddr02());
		sv.cltaddr03.set(cltsIO.getCltaddr03());
		sv.cltaddr04.set(cltsIO.getCltaddr04());
		sv.cltaddr05.set(cltsIO.getCltaddr05()); //ILIFE-3212
		lrrhIO.setParams(SPACES);
		lrrhIO.setClntpfx(cltsIO.getClntpfx());
		lrrhIO.setClntcoy(cltsIO.getClntcoy());
		lrrhIO.setClntnum(cltsIO.getClntnum());
		lrrhIO.setCompany(wsspcomn.company);
		lrrhIO.setLrkcls(wssplife.lrkcls);
		lrrhIO.setFormat(lrrhrec);
		lrrhIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhIO.setFitKeysSearch("CLNTPFX", "CLNTCOY", "CLNTNUM", "COMPANY");
		while ( !(isEQ(lrrhIO.getStatuz(),varcom.endp))) {
			processLrrh1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void processLrrh1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1110();
				}
				case nextr1180: {
					nextr1180();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1110()
	{
		SmartFileCode.execute(appVars, lrrhIO);
		if (isNE(lrrhIO.getStatuz(),varcom.oK)
		&& isNE(lrrhIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lrrhIO.getParams());
			fatalError600();
		}
		if (isNE(lrrhIO.getClntpfx(),cltsIO.getClntpfx())
		|| isNE(lrrhIO.getClntcoy(),cltsIO.getClntcoy())
		|| isNE(lrrhIO.getClntnum(),cltsIO.getClntnum())
		|| isNE(lrrhIO.getCompany(),wsspcomn.company)
		|| (isNE(lrrhIO.getLrkcls(),wssplife.lrkcls)
		&& isNE(wssplife.lrkcls,SPACES))
		|| isEQ(lrrhIO.getStatuz(),varcom.endp)) {
			lrrhIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(lrrhIO.getSsretn(),ZERO)
		&& isEQ(lrrhIO.getSsreast(),ZERO)
		&& isEQ(lrrhIO.getSsreasf(),ZERO)) {
			goTo(GotoLabel.nextr1180);
		}
		sv.subfileFields.set(SPACES);
		sv.chdrnum.set(lrrhIO.getChdrnum());
		sv.life.set(lrrhIO.getLife());
		sv.coverage.set(lrrhIO.getCoverage());
		sv.rider.set(lrrhIO.getRider());
		sv.lrkcls.set(lrrhIO.getLrkcls());
		sv.currency.set(lrrhIO.getCurrency());
		sv.ssretn.set(lrrhIO.getSsretn());
		sv.ssreast.set(lrrhIO.getSsreast());
		sv.ssreasf.set(lrrhIO.getSsreasf());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5435", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void nextr1180()
	{
		lrrhIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}
}
