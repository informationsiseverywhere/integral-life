package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5595
 * @version 1.0 generated on 30/08/09 06:44
 * @author Quipoz
 */
public class S5595ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(87);
	public FixedLengthStringData dataFields = new FixedLengthStringData(55).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cname = DD.cname.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 55);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 63);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(171);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(41).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData minsta = DD.minsta.copyToZonedDecimal().isAPartOf(subfileFields,11);
	public FixedLengthStringData rapayfrq = DD.rapayfrq.copy().isAPartOf(subfileFields,28);
	public FixedLengthStringData rapaymth = DD.rapaymth.copy().isAPartOf(subfileFields,30);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(subfileFields,31);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,39);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 41);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData minstaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData rapayfrqErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData rapaymthErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 73);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] minstaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] rapayfrqOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] rapaymthOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 169);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5595screensflWritten = new LongData(0);
	public LongData S5595screenctlWritten = new LongData(0);
	public LongData S5595screenWritten = new LongData(0);
	public LongData S5595protectWritten = new LongData(0);
	public GeneralTable s5595screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5595screensfl;
	}

	public S5595ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {slt, agntnum, validflag, rasnum, rapaymth, rapayfrq, currcode, minsta};
		screenSflOutFields = new BaseData[][] {sltOut, agntnumOut, validflagOut, rasnumOut, rapaymthOut, rapayfrqOut, currcodeOut, minstaOut};
		screenSflErrFields = new BaseData[] {sltErr, agntnumErr, validflagErr, rasnumErr, rapaymthErr, rapayfrqErr, currcodeErr, minstaErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, cname};
		screenOutFields = new BaseData[][] {clntnumOut, cnameOut};
		screenErrFields = new BaseData[] {clntnumErr, cnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5595screen.class;
		screenSflRecord = S5595screensfl.class;
		screenCtlRecord = S5595screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5595protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5595screenctl.lrec.pageSubfile);
	}
}
