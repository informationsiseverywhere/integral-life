package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:19
 * Description:
 * Copybook name: T5472REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5472rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5472Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData addprocess = new FixedLengthStringData(7).isAPartOf(t5472Rec, 0);
  	public FixedLengthStringData calcprog = new FixedLengthStringData(7).isAPartOf(t5472Rec, 7);
  	public FixedLengthStringData procesprog = new FixedLengthStringData(7).isAPartOf(t5472Rec, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(479).isAPartOf(t5472Rec, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5472Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5472Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}