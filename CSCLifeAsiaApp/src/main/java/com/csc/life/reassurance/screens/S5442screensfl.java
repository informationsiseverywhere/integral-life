package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5442screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 5, 6, 7, 2, 3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5442ScreenVars sv = (S5442ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5442screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5442screensfl, 
			sv.S5442screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5442ScreenVars sv = (S5442ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5442screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5442ScreenVars sv = (S5442ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5442screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5442screensflWritten.gt(0))
		{
			sv.s5442screensfl.setCurrentIndex(0);
			sv.S5442screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5442ScreenVars sv = (S5442ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5442screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5442ScreenVars screenVars = (S5442ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hrrn01.setFieldName("hrrn01");
				screenVars.hrrn02.setFieldName("hrrn02");
				screenVars.hrrn03.setFieldName("hrrn03");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.action.setFieldName("action");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.elemdesc.setFieldName("elemdesc");
				screenVars.bascovr.setFieldName("bascovr");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hrrn01.set(dm.getField("hrrn01"));
			screenVars.hrrn02.set(dm.getField("hrrn02"));
			screenVars.hrrn03.set(dm.getField("hrrn03"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.action.set(dm.getField("action"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.shortdesc.set(dm.getField("shortdesc"));
			screenVars.elemdesc.set(dm.getField("elemdesc"));
			screenVars.bascovr.set(dm.getField("bascovr"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5442ScreenVars screenVars = (S5442ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hrrn01.setFieldName("hrrn01");
				screenVars.hrrn02.setFieldName("hrrn02");
				screenVars.hrrn03.setFieldName("hrrn03");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.action.setFieldName("action");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.elemdesc.setFieldName("elemdesc");
				screenVars.bascovr.setFieldName("bascovr");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hrrn01").set(screenVars.hrrn01);
			dm.getField("hrrn02").set(screenVars.hrrn02);
			dm.getField("hrrn03").set(screenVars.hrrn03);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("action").set(screenVars.action);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("shortdesc").set(screenVars.shortdesc);
			dm.getField("elemdesc").set(screenVars.elemdesc);
			dm.getField("bascovr").set(screenVars.bascovr);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5442screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5442ScreenVars screenVars = (S5442ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hrrn01.clearFormatting();
		screenVars.hrrn02.clearFormatting();
		screenVars.hrrn03.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.action.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.shortdesc.clearFormatting();
		screenVars.elemdesc.clearFormatting();
		screenVars.bascovr.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5442ScreenVars screenVars = (S5442ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hrrn01.setClassString("");
		screenVars.hrrn02.setClassString("");
		screenVars.hrrn03.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.action.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.elemdesc.setClassString("");
		screenVars.bascovr.setClassString("");
	}

/**
 * Clear all the variables in S5442screensfl
 */
	public static void clear(VarModel pv) {
		S5442ScreenVars screenVars = (S5442ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hrrn01.clear();
		screenVars.hrrn02.clear();
		screenVars.hrrn03.clear();
		screenVars.hcrtable.clear();
		screenVars.action.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.shortdesc.clear();
		screenVars.elemdesc.clear();
		screenVars.bascovr.clear();
	}
}
