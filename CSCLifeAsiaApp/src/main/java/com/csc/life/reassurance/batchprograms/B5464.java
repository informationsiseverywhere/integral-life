/*
 * File: B5464.java
 * Date: 29 August 2009 21:18:13
 * Author: Quipoz Limited
 *
 * Class transformed from B5464.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
//Reverting this file code to version IFSU-44 as per ILIFE-8287

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.dataaccess.RacdenqTableDAM;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.dataaccess.RacdmjaTableDAM;
import com.csc.life.reassurance.dataaccess.RacdseqTableDAM;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.procedures.Csncalc;
import com.csc.life.reassurance.procedures.Rcorfnd;
import com.csc.life.reassurance.procedures.Rexpupd;
import com.csc.life.reassurance.procedures.Trmracd;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.reassurance.recordstructures.Rcorfndrec;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.life.reassurance.reports.R5464aReport;
import com.csc.life.reassurance.reports.R5464nReport;
import com.csc.life.reassurance.reports.R5464tReport;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.reassurance.tablestructures.Trmracdrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reassurance Reviews Report.
*
* Overview
* ========
*
* Regular processing to determine if reassurance levels need
* adjustment is required. In arrangements where only the sum at
* risk is assessed for reassurance, rather than the entire sum
* assured, the amount reassured must be adjusted at regular
* intervals to reduce sums reassured as reserves build up and
* thus offset the sum at risk.
*
* This process should adjust the current reassurance position of
* the contract with reference to the current retentions for the
* life based on the treaty effective at new business for the
* contract. Thus if terminations or reductions in sums assured
* for related benefits for the same life have occurred, then the
* reassurance for the contract being reviewed may take up any
* slack in retentions that may exist.
*
* The timing of reviews is determined by the reassurance method
* review frequency on T5448. This will be used to calculate the
* next review date for each cession at date of cession.
*
* This batch job will select all cessions which are due for
* on a given date (i.e. where the next review date on the RACD
* file is less than or equal to the effective date of the job).
*
* Logically this process should run before costing to ensure
* the correct cessions are costed. However, if costing should
* be run first, a refund will be calculated for any terminated
* cessions.
*
* Processing
* ==========
*
* This Program will read through RACD records using SQL and
* select contracts with at least one RACD record due for review
* (i.e. where the Next Review Date <= the Effective Date).
*
* It will then process the records at contract level.
*
* A contract will only be processed if it meets the following
* conditions:
*
* - paid-to date greater or equal to the effective date
* - contract status validated against T5679
*
* For each component attached to this contract, any existing
* cessions (RACDs) which have a review date less than or equal
* to the effective date AND less than or equal to the paid-to-
* date will be terminated by calling TRMRACD and printed to a
* report, R5464. If the review date < costed to date, refunds
* will be calculated by calling the subroutine RCORFND.
*
* Once all the cessions records for a component have been
* processed, the life company retention will be updated by
* calling REXPUPD.
*
* Then, it will recalculate the cessions for the contract by
* calling the CSNCALC subroutine. If any default facultative
* cession records were created (i.e. reassurer and arrangement
* blank), these are deleted and printd to a report, R5464A.
* All the other newly created cessions are printed to another
* report, R5464.
*
* Finally, it will call ACTVRES to activate the newly created
* cession records, i.e. change them from validflag '3' to '1'
* and update the experience files.
*
* A transaction history record (PTRN) and a new contract header
* (CHDR) record will be created.
*
* Control Totals
* ==============
*
* 1   Number of RACD records terminated
* 2   Total sum reassured terminated
* 3   Number of new RACDs created
* 4   Total sum reassured new
* 5   Number of facultative RACDs deleted
* 6   Total sum reassured deleted
*
*****************************************************************
* </pre>
*/
public class B5464 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlracdpf1rs = null;
	private java.sql.PreparedStatement sqlracdpf1ps = null;
	private java.sql.Connection sqlracdpf1conn = null;
	private String sqlracdpf1 = "";
	private R5464tReport printerFile1 = new R5464tReport();
	private R5464aReport printerFile2 = new R5464aReport();
	private R5464nReport printerFile3 = new R5464nReport();
	private FixedLengthStringData printerRec1 = new FixedLengthStringData(220);
	private FixedLengthStringData printerRec2 = new FixedLengthStringData(220);
	private FixedLengthStringData printerRec3 = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5464");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaJlifenum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaOldRaamount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaNewRaamount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDelRaamount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnum2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCurrLrkcls = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaCessAmt = new PackedDecimalData(17, 2);
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5446 = "T5446";
	private static final String t5448 = "T5448";
	private static final String t5454 = "T5454";
	private static final String t5679 = "T5679";
	private static final String th618 = "TH618";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-RACDPF */
	private FixedLengthStringData sqlRacdrec = new FixedLengthStringData(8);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlRacdrec, 0);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaOverflowA = new FixedLengthStringData(1).init("Y");
	private Validator newPageReqA = new Validator(wsaaOverflowA, "Y");

	private FixedLengthStringData wsaaOverflowN = new FixedLengthStringData(1).init("Y");
	private Validator newPageReqN = new Validator(wsaaOverflowN, "Y");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaRacdDel = new FixedLengthStringData(1).init("N");
	private Validator racdDel = new Validator(wsaaRacdDel, "Y");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaFirstTimeDel = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeDel = new Validator(wsaaFirstTimeDel, "Y");

	private FixedLengthStringData wsaaSecondDetl = new FixedLengthStringData(1).init("Y");
	private Validator secondDetlFound = new Validator(wsaaSecondDetl, "Y");
	private ZonedDecimalData wsxxSub = new ZonedDecimalData(2, 0).setUnsigned();

		/*01  R5464-H01.
		    COPY DD-R5464H01-O OF R5464.
		01  R5464-D01.
		    COPY DD-R5464D01-O OF R5464.
		01  R5464-D02.
		    COPY DD-R5464D02-O OF R5464.
		01  R5464-D03.
		    COPY DD-R5464D03-O OF R5464.
		01  R5464-D04.
		    COPY DD-R5464D04-O OF R5464.
		01  R5464-T01.
		    COPY DD-R5464T01-O OF R5464.
		01  R5464-T02.
		    COPY DD-R5464T02-O OF R5464.                                 */
	private FixedLengthStringData r5464tH01 = new FixedLengthStringData(73);
	private FixedLengthStringData r5464th01O = new FixedLengthStringData(73).isAPartOf(r5464tH01, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5464th01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5464th01O, 1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r5464th01O, 31);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(r5464th01O, 41);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(r5464th01O, 43);

	private FixedLengthStringData r5464tD01 = new FixedLengthStringData(1);
	private FixedLengthStringData r5464td01O = new FixedLengthStringData(1).isAPartOf(r5464tD01, 0);
	private FixedLengthStringData company1 = new FixedLengthStringData(1).isAPartOf(r5464td01O, 0);

	private FixedLengthStringData r5464tT01 = new FixedLengthStringData(17);
	private FixedLengthStringData r5464tt01O = new FixedLengthStringData(17).isAPartOf(r5464tT01, 0);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5464tt01O, 0);

	private FixedLengthStringData r5464nH01 = new FixedLengthStringData(73);
	private FixedLengthStringData r5464nh01O = new FixedLengthStringData(73).isAPartOf(r5464nH01, 0);
	private FixedLengthStringData company2 = new FixedLengthStringData(1).isAPartOf(r5464nh01O, 0);
	private FixedLengthStringData companynm1 = new FixedLengthStringData(30).isAPartOf(r5464nh01O, 1);
	private FixedLengthStringData sdate1 = new FixedLengthStringData(10).isAPartOf(r5464nh01O, 31);
	private FixedLengthStringData branch1 = new FixedLengthStringData(2).isAPartOf(r5464nh01O, 41);
	private FixedLengthStringData branchnm1 = new FixedLengthStringData(30).isAPartOf(r5464nh01O, 43);

	private FixedLengthStringData r5464nD03 = new FixedLengthStringData(1);
	private FixedLengthStringData r5464nd03O = new FixedLengthStringData(1).isAPartOf(r5464nD03, 0);
	private FixedLengthStringData company3 = new FixedLengthStringData(1).isAPartOf(r5464nd03O, 0);

	private FixedLengthStringData r5464nT02 = new FixedLengthStringData(17);
	private FixedLengthStringData r5464nt02O = new FixedLengthStringData(17).isAPartOf(r5464nT02, 0);
	private ZonedDecimalData raamount1 = new ZonedDecimalData(17, 2).isAPartOf(r5464nt02O, 0);

	private FixedLengthStringData r5464aH01 = new FixedLengthStringData(73);
	private FixedLengthStringData r5464ah01O = new FixedLengthStringData(73).isAPartOf(r5464aH01, 0);
	private FixedLengthStringData company4 = new FixedLengthStringData(1).isAPartOf(r5464ah01O, 0);
	private FixedLengthStringData companynm2 = new FixedLengthStringData(30).isAPartOf(r5464ah01O, 1);
	private FixedLengthStringData sdate2 = new FixedLengthStringData(10).isAPartOf(r5464ah01O, 31);
	private FixedLengthStringData branch2 = new FixedLengthStringData(2).isAPartOf(r5464ah01O, 41);
	private FixedLengthStringData branchnm2 = new FixedLengthStringData(30).isAPartOf(r5464ah01O, 43);

	private FixedLengthStringData r5464aD01 = new FixedLengthStringData(49);
	private FixedLengthStringData r5464ad01O = new FixedLengthStringData(49).isAPartOf(r5464aD01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5464ad01O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5464ad01O, 8);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5464ad01O, 12);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5464ad01O, 14);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5464ad01O, 16);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5464ad01O, 18);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5464ad01O, 22);
	private ZonedDecimalData raamount2 = new ZonedDecimalData(17, 2).isAPartOf(r5464ad01O, 32);

	private FixedLengthStringData r5464aT01 = new FixedLengthStringData(17);
	private FixedLengthStringData r5464at01O = new FixedLengthStringData(17).isAPartOf(r5464aT01, 0);
	private ZonedDecimalData raamount3 = new ZonedDecimalData(17, 2).isAPartOf(r5464at01O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private RacdenqTableDAM racdenqIO = new RacdenqTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private RacdmjaTableDAM racdmjaIO = new RacdmjaTableDAM();
	private RacdseqTableDAM racdseqIO = new RacdseqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Trmracdrec trmracdrec = new Trmracdrec();
	private Rcorfndrec rcorfndrec = new Rcorfndrec();
	private Actvresrec actvresrec = new Actvresrec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private P6671par p6671par = new P6671par();
	private T5446rec t5446rec = new T5446rec();
	private T5448rec t5448rec = new T5448rec();
	private T5679rec t5679rec = new T5679rec();
	private Th618rec th618rec = new Th618rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private R5464nD04Inner r5464nD04Inner = new R5464nD04Inner();
	private R5464tD02Inner r5464tD02Inner = new R5464tD02Inner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		next3280,
		exit3290,
		nextr3408,
		exit3409
	}

	public B5464() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		printerFile1.openOutput();
		printerFile2.openOutput();
		printerFile3.openOutput();
		wsaaSecondDetl.set("N");
		wsaaRacdDel.set("N");
		wsaaFirstTimeDel.set("Y");
		wsaaOverflow.set("Y");
		wsaaOverflowN.set("Y");
		wsaaOverflowA.set("Y");
		wsaaChdrnum.set(SPACES);
		wsaaChdrnum2.set(SPACES);
		wsaaOldRaamount.set(ZERO);
		wsaaNewRaamount.set(ZERO);
		wsaaDelRaamount.set(ZERO);
		wsspEdterror.set(varcom.oK);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		wsaaChdrnumfrm.set(p6671par.chdrnum);
		wsaaChdrnumto.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		company.set(bsprIO.getCompany());
		companynm.set(descIO.getLongdesc());
		company2.set(bsprIO.getCompany());
		companynm1.set(descIO.getLongdesc());
		company4.set(bsprIO.getCompany());
		companynm2.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		/* MOVE BPRD-DEFAULT-BRANCH    TO DESC-DESCITEM.                */
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		branch.set(bsprIO.getDefaultBranch());
		branchnm.set(descIO.getLongdesc());
		branch1.set(bsprIO.getDefaultBranch());
		branchnm1.set(descIO.getLongdesc());
		branch2.set(bsprIO.getDefaultBranch());
		branchnm2.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		sdate.set(datcon1rec.extDate);
		sdate1.set(datcon1rec.extDate);
		sdate2.set(datcon1rec.extDate);
		sqlracdpf1 = " SELECT  RACDPF.CHDRNUM" +
" FROM   " + getAppVars().getTableNameOverriden("RACDPF") + " " +
" WHERE RACDPF.CHDRNUM BETWEEN ? AND ?" +
" AND RACDPF.RREVDT <= ?" +
" AND RACDPF.RREVDT <= RACDPF.CTDATE" +
" AND RACDPF.VALIDFLAG = '1'" +
" GROUP BY RACDPF.CHDRNUM";
		sqlerrorflag = false;
		try {
			sqlracdpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.reassurance.dataaccess.RacdpfTableDAM());
			sqlracdpf1ps = getAppVars().prepareStatementEmbeded(sqlracdpf1conn, sqlracdpf1, "RACDPF");
			getAppVars().setDBString(sqlracdpf1ps, 1, wsaaChdrnumfrm);
			getAppVars().setDBString(sqlracdpf1ps, 2, wsaaChdrnumto);
			getAppVars().setDBNumber(sqlracdpf1ps, 3, wsaaEffectiveDate);
			sqlracdpf1rs = getAppVars().executeQuery(sqlracdpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlracdpf1rs)) {
				getAppVars().getDBObject(sqlracdpf1rs, 1, sqlChdrnum);
			}
//			MIBT-100 STARTS
			else{
				wsspEdterror.set(varcom.endp);
			}
//			MIBT-100 ENDS
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* An SQLCODE = +100 is returned when end-of-file has been*/
		/* reached.*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)) {
			wsspEdterror.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
			edit2501();
		}

protected void edit2501()
	{
		wsspEdterror.set(varcom.oK);
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(sqlChdrnum);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isGT(bsscIO.getEffectiveDate(),chdrlnbIO.getPtdate())) {
			wsspEdterror.set(SPACES);
			return ;
		}
		validateChdr2600();
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
		lockContract2700();
	}

protected void validateChdr2600()
	{
		/*PARA*/
		/* Validate the new contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()],chdrlnbIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()],chdrlnbIO.getPstatcode())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
		/*EXIT*/
	}

protected void lockContract2700()
	{
		lockContract2710();
	}

protected void lockContract2710()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sqlChdrnum);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void readLife2800()
	{
		life2810();
	}

protected void life2810()
	{
		/* Obtain the Life Assured and Joint Life, if one exists.*/
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		/* MOVE '01'                   TO LIFEENQ-LIFE.                 */
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		wsaaLifenum.set(lifeenqIO.getLifcnum());
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			wsaaJlifenum.set(lifeenqIO.getLifcnum());
		}
		else {
			wsaaJlifenum.set(SPACES);
		}
	}

protected void update3000()
	{
		updates3010();
	}

protected void updates3010()
	{
		/* For each component attached to this contract, terminate*/
		/* any existing RACDs and update the life company exposure*/
		/* as required.*/
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaOldRaamount.set(ZERO);
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			processCovr3100();
		}

		raamount.set(wsaaOldRaamount);
		printerFile1.printR5464tt01(r5464tT01);
		/* Call CSNCALC to create new RACD records.*/
		/* PERFORM 3200-CALL-CSNCALC.                                   */
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaLife.set(SPACES);
		while ( !(isEQ(lifeenqIO.getStatuz(),varcom.endp))) {
			callCsncalc3200();
		}

		wsaaSecondDetl.set("N");
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaNewRaamount.set(ZERO);
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			processCovr3300();
		}

		if (secondDetlFound.isTrue()) {
			raamount1.set(wsaaNewRaamount);
			/*      WRITE PRINTER-REC1      FROM R5464-T02*/
			printerFile3.printR5464nt02(r5464nT02);
		}
		writeNewChdr3700();
		writePtrn3800();
		unlockContract3900();
	}

protected void processCovr3100()
	{
			covr3101();
		}

protected void covr3101()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(),chdrlnbIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaCurrLrkcls.set(SPACES);
		racdmjaIO.setParams(SPACES);
		racdmjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		racdmjaIO.setChdrnum(covrmjaIO.getChdrnum());
		racdmjaIO.setLife(covrmjaIO.getLife());
		racdmjaIO.setCoverage(covrmjaIO.getCoverage());
		racdmjaIO.setRider(covrmjaIO.getRider());
		racdmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		racdmjaIO.setSeqno(99);
		racdmjaIO.setFormat(formatsInner.racdmjarec);
		racdmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		wsaaCessAmt.set(ZERO);
		readLife2800();
		while ( !(isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			processRacd3110();
		}

		covrmjaIO.setFunction(varcom.nextr);
	}

protected void processRacd3110()
	{
			racd3111();
		}

protected void racd3111()
	{
		SmartFileCode.execute(appVars, racdmjaIO);
		if (isNE(racdmjaIO.getStatuz(),varcom.oK)
		&& isNE(racdmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdmjaIO.getParams());
			fatalError600();
		}
		/* Check whether there is any RACD for coverage/rider,          */
		/* if no RACD found, there may still be retention by company.   */
		/* Experience Update is also required.                          */
		if (isNE(racdmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(racdmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(racdmjaIO.getLife(),covrmjaIO.getLife())
		|| isNE(racdmjaIO.getCoverage(),covrmjaIO.getCoverage())
		|| isNE(racdmjaIO.getRider(),covrmjaIO.getRider())
		|| isNE(racdmjaIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())
		&& (isEQ(racdmjaIO.getFunction(),varcom.begn)
		&& isEQ(racdmjaIO.getStatuz(),varcom.endp))) {
			processRiskClass3115();
		}
		if (isNE(racdmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(racdmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(racdmjaIO.getLife(),covrmjaIO.getLife())
		|| isNE(racdmjaIO.getCoverage(),covrmjaIO.getCoverage())
		|| isNE(racdmjaIO.getRider(),covrmjaIO.getRider())
		|| isNE(racdmjaIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())
		|| isEQ(racdmjaIO.getStatuz(),varcom.endp)) {
			racdmjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(racdmjaIO.getLrkcls(),wsaaCurrLrkcls)) {
			wsaaCessAmt.set(ZERO);
			wsaaCurrLrkcls.set(racdmjaIO.getLrkcls());
		}
		if (isLTE(racdmjaIO.getRrevdt(),bsscIO.getEffectiveDate())
		&& isLTE(racdmjaIO.getRrevdt(),chdrlnbIO.getPtdate())) {
			wsaaCessAmt.add(racdmjaIO.getRaAmount());
			terminateRacd3120();
			if (isLT(racdmjaIO.getRrevdt(),racdmjaIO.getCtdate())) {
				callRcorfnd3150();
			}
		}
		if (isLT(wsaaCessAmt,covrmjaIO.getSumins())) {
			experienceUpdate3130();
		}
		racdmjaIO.setFunction(varcom.nextr);
	}

protected void processRiskClass3115()
	{
			start3115();
		}

protected void start3115()
	{
		/* Read T5448 to obtain Risk Type                               */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCnttype());
		stringVariable1.addExpression(covrmjaIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5448);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(),wsaaT5448Item)
		|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			return ;
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th618);
		itemIO.setItemitem(t5448rec.rrsktyp);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		th618rec.th618Rec.set(itemIO.getGenarea());
		for (wsxxSub.set(1); !(isGT(wsxxSub,5)); wsxxSub.add(1)){
			if (isNE(th618rec.lrkcls[wsxxSub.toInt()],SPACES)) {
				racdmjaIO.setLrkcls(th618rec.lrkcls[wsxxSub.toInt()]);
				experienceUpdate3130();
			}
		}
	}

protected void terminateRacd3120()
	{
		racd3121();
	}

protected void racd3121()
	{
		trmracdrec.trmracdRec.set(SPACES);
		trmracdrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		trmracdrec.chdrnum.set(racdmjaIO.getChdrnum());
		trmracdrec.life.set(racdmjaIO.getLife());
		trmracdrec.coverage.set(racdmjaIO.getCoverage());
		trmracdrec.rider.set(racdmjaIO.getRider());
		trmracdrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		trmracdrec.lrkcls.set(racdmjaIO.getLrkcls());
		trmracdrec.crtable.set(covrmjaIO.getCrtable());
		trmracdrec.cnttype.set(chdrlnbIO.getCnttype());
		trmracdrec.effdate.set(bsscIO.getEffectiveDate());
		trmracdrec.newRaAmt.set(ZERO);
		trmracdrec.recovamt.set(ZERO);
		compute(trmracdrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		trmracdrec.seqno.set(racdmjaIO.getSeqno());
		trmracdrec.clntcoy.set(bsprIO.getFsuco());
		trmracdrec.l1Clntnum.set(wsaaLifenum);
		trmracdrec.jlife.set(covrmjaIO.getJlife());
		trmracdrec.l2Clntnum.set(wsaaJlifenum);
		trmracdrec.crrcd.set(covrmjaIO.getCrrcd());
		trmracdrec.batctrcde.set(bprdIO.getAuthCode());
		trmracdrec.function.set("TRMR");
		callProgram(Trmracd.class, trmracdrec.trmracdRec);
		if (isNE(trmracdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(trmracdrec.statuz);
			syserrrec.params.set(trmracdrec.trmracdRec);
			fatalError600();
		}
		if (newPageReq.isTrue()) {
			printerFile1.printR5464th01(r5464tH01);
			printerFile1.printR5464td01(r5464tD01);
			wsaaOverflow.set("N");
		}
		if (isNE(racdmjaIO.getChdrnum(),wsaaChdrnum)) {
			r5464tD02Inner.chdrnum.set(racdmjaIO.getChdrnum());
			wsaaChdrnum.set(racdmjaIO.getChdrnum());
		}
		else {
			r5464tD02Inner.chdrnum.set(SPACES);
		}
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(racdmjaIO.getCmdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		r5464tD02Inner.crtable.set(covrmjaIO.getCrtable());
		r5464tD02Inner.life.set(racdmjaIO.getLife());
		r5464tD02Inner.coverage.set(racdmjaIO.getCoverage());
		r5464tD02Inner.rider.set(racdmjaIO.getRider());
		r5464tD02Inner.plnsfx.set(racdmjaIO.getPlanSuffix());
		r5464tD02Inner.cmdate.set(datcon1rec.extDate);
		r5464tD02Inner.rasnum.set(racdmjaIO.getRasnum());
		r5464tD02Inner.rngmnt.set(racdmjaIO.getRngmnt());
		/* Read T5454 to retrieve the RETYPE description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5454);
		descIO.setDescitem(racdmjaIO.getRetype());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		r5464tD02Inner.retypedesc.set(descIO.getLongdesc());
		r5464tD02Inner.raamount.set(racdmjaIO.getRaAmount());
		wsaaOldRaamount.add(racdmjaIO.getRaAmount());
		printerFile1.printR5464td02(r5464tD02Inner.r5464tD02);
		/* Update the Control Totals - No. of RACD records terminated*/
		/* and Total Sum Reassured Terminated.*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct02);
		contotrec.totval.set(racdmjaIO.getRaAmount());
		callContot001();
	}

protected void experienceUpdate3130()
	{
			update3131();
		}

protected void update3131()
	{
		/* Read the reassurance method table, T5448, to determine risk*/
		/* class.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCnttype());
		stringVariable1.addExpression(covrmjaIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5448);
		itdmIO.setItemitem(wsaaT5448Item);
		/*  MOVE BSSC-EFFECTIVE-DATE    TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* If no item found on T5448 for the component in question,        */
		/* skip out of section since processing is not required.           */
		if (isNE(itdmIO.getItemitem(),wsaaT5448Item)
		|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*     MOVE MRNF               TO ITDM-STATUZ                   */
			/*     MOVE BSPR-COMPANY       TO ITDM-ITEMCOY                  */
			/*     MOVE WSAA-T5448-ITEM    TO ITDM-ITEMITEM                 */
			/*     MOVE T5448              TO ITDM-ITEMTABL                 */
			/*     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   */
			/*     PERFORM 600-FATAL-ERROR                                  */
			return ;
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
		rexpupdrec.rexpupdRec.set(SPACES);
		rexpupdrec.chdrcoy.set(covrmjaIO.getChdrcoy());
		rexpupdrec.chdrnum.set(covrmjaIO.getChdrnum());
		rexpupdrec.life.set(covrmjaIO.getLife());
		rexpupdrec.coverage.set(covrmjaIO.getCoverage());
		rexpupdrec.rider.set(covrmjaIO.getRider());
		rexpupdrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		compute(rexpupdrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		rexpupdrec.clntcoy.set(bsprIO.getFsuco());
		rexpupdrec.l1Clntnum.set(wsaaLifenum);
		if (isNE(covrmjaIO.getJlife(),"00")) {
			rexpupdrec.l2Clntnum.set(wsaaJlifenum);
		}
		compute(rexpupdrec.sumins, 2).set(sub(covrmjaIO.getSumins(),wsaaCessAmt));
		readT54463140();
		rexpupdrec.currency.set(t5446rec.currcode);
		rexpupdrec.effdate.set(bsscIO.getEffectiveDate());
		/*    MOVE T5448-LRKCLS           TO RXUP-RISK-CLASS.              */
		rexpupdrec.riskClass.set(racdmjaIO.getLrkcls());
		rexpupdrec.batctrcde.set(bprdIO.getAuthCode());
		if (isNE(rexpupdrec.currency,chdrlnbIO.getCntcurr())) {
			conlinkrec.amountIn.set(rexpupdrec.sumins);
			conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
			conlinkrec.currOut.set(rexpupdrec.currency);
			callXcvrt3160();
			if (isNE(conlinkrec.amountOut, ZERO)) {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(rexpupdrec.currency);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
			}
			rexpupdrec.sumins.set(conlinkrec.amountOut);
		}
		rexpupdrec.function.set("DECR");
		callProgram(Rexpupd.class, rexpupdrec.rexpupdRec);
		if (isNE(rexpupdrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rexpupdrec.statuz);
			syserrrec.params.set(rexpupdrec.rexpupdRec);
			fatalError600();
		}
	}

protected void readT54463140()
	{
		t54463141();
	}

protected void t54463141()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5446);
		/*    MOVE T5448-LRKCLS           TO ITDM-ITEMITEM.                */
		itdmIO.setItemitem(racdmjaIO.getLrkcls());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(),racdmjaIO.getLrkcls())
		|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5446)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(racdmjaIO.getLrkcls());
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5446);
			itdmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5446rec.t5446Rec.set(itdmIO.getGenarea());
		}
	}

protected void callRcorfnd3150()
	{
		callRcorfnd3151();
	}

protected void callRcorfnd3151()
	{
		rcorfndrec.rcorfndRec.set(SPACES);
		rcorfndrec.function.set("UPDT");
		rcorfndrec.chdrcoy.set(racdmjaIO.getChdrcoy());
		rcorfndrec.chdrnum.set(racdmjaIO.getChdrnum());
		rcorfndrec.life.set(racdmjaIO.getLife());
		rcorfndrec.coverage.set(racdmjaIO.getCoverage());
		rcorfndrec.rider.set(racdmjaIO.getRider());
		rcorfndrec.planSuffix.set(racdmjaIO.getPlanSuffix());
		rcorfndrec.currfrom.set(racdmjaIO.getCurrfrom());
		rcorfndrec.seqno.set(racdmjaIO.getSeqno());
		compute(rcorfndrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		rcorfndrec.effdate.set(bsscIO.getEffectiveDate());
		rcorfndrec.polsum.set(chdrlnbIO.getPolsum());
		rcorfndrec.batckey.set(batcdorrec.batchkey);
		rcorfndrec.newRaAmt.set(ZERO);
		rcorfndrec.language.set(bsscIO.getLanguage());
		rcorfndrec.refund.set(1);
		rcorfndrec.crtable.set(covrmjaIO.getCrtable());
		rcorfndrec.cnttype.set(chdrlnbIO.getCnttype());
		callProgram(Rcorfnd.class, rcorfndrec.rcorfndRec);
		if (isNE(rcorfndrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rcorfndrec.statuz);
			syserrrec.params.set(rcorfndrec.rcorfndRec);
			fatalError600();
		}
	}

protected void callXcvrt3160()
	{
		/*CALL*/
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.company.set(chdrlnbIO.getChdrcoy());
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callCsncalc3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					csncalc3210();
				case next3280:
					next3280();
				case exit3290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void csncalc3210()
	{
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(lifeenqIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(lifeenqIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(lifeenqIO.getStatuz(),varcom.endp)) {
			lifeenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
		if (isEQ(lifeenqIO.getLife(),wsaaLife)) {
			goTo(GotoLabel.next3280);
		}
		wsaaLife.set(lifeenqIO.getLife());
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		csncalcrec.csncalcRec.set(SPACES);
		csncalcrec.function.set("COVR");
		csncalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		csncalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		csncalcrec.life.set(lifeenqIO.getLife());
		csncalcrec.cnttype.set(chdrlnbIO.getCnttype());
		csncalcrec.currency.set(chdrlnbIO.getCntcurr());
		csncalcrec.fsuco.set(bsprIO.getFsuco());
		csncalcrec.language.set(bsscIO.getLanguage());
		csncalcrec.incrAmt.set(ZERO);
		csncalcrec.effdate.set(bsscIO.getEffectiveDate());
		compute(csncalcrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		csncalcrec.planSuffix.set(ZERO);
		csncalcrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Csncalc.class, csncalcrec.csncalcRec);
		if (isNE(csncalcrec.statuz,varcom.oK)
		&& isNE(csncalcrec.statuz,"FACL")
		&& isNE(csncalcrec.statuz,"TRET")) {
			syserrrec.statuz.set(csncalcrec.statuz);
			syserrrec.params.set(csncalcrec.csncalcRec);
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void next3280()
	{
		lifeenqIO.setFunction(varcom.nextr);
	}

protected void processCovr3300()
	{
			covr3301();
		}

protected void covr3301()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(),chdrlnbIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		racdlnbIO.setChdrnum(covrmjaIO.getChdrnum());
		racdlnbIO.setLife(covrmjaIO.getLife());
		racdlnbIO.setCoverage(covrmjaIO.getCoverage());
		racdlnbIO.setRider(covrmjaIO.getRider());
		racdlnbIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		racdlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdlnbIO.getStatuz(),varcom.endp))) {
			deleteOrPrint3400();
		}

		if (racdDel.isTrue()) {
			raamount3.set(wsaaDelRaamount);
			printerFile2.printR5464at01(r5464aT01);
		}
		readLife2800();
		activateRacd3350();
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void activateRacd3350()
	{
		racd3351();
	}

protected void racd3351()
	{
		/* Call ACTVRES to activate new RACD records. This will update*/
		/* them from validflag '3' to validflag '1'.*/
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(covrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(covrmjaIO.getChdrnum());
		actvresrec.cnttype.set(chdrlnbIO.getCnttype());
		actvresrec.currency.set(chdrlnbIO.getCntcurr());
		compute(actvresrec.tranno, 0).set(add(1,chdrlnbIO.getTranno()));
		actvresrec.life.set(covrmjaIO.getLife());
		actvresrec.coverage.set(covrmjaIO.getCoverage());
		actvresrec.rider.set(covrmjaIO.getRider());
		actvresrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		actvresrec.crtable.set(covrmjaIO.getCrtable());
		actvresrec.effdate.set(bsscIO.getEffectiveDate());
		actvresrec.clntcoy.set(bsprIO.getFsuco());
		actvresrec.l1Clntnum.set(wsaaLifenum);
		actvresrec.jlife.set(covrmjaIO.getJlife());
		actvresrec.l2Clntnum.set(wsaaJlifenum);
		actvresrec.oldSumins.set(ZERO);
		actvresrec.newSumins.set(covrmjaIO.getSumins());
		actvresrec.crrcd.set(covrmjaIO.getCrrcd());
		actvresrec.language.set(bsscIO.getLanguage());
		actvresrec.function.set("ACT8");
		actvresrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			fatalError600();
		}
	}

protected void deleteOrPrint3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					racd3401();
				case nextr3408:
					nextr3408();
				case exit3409:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void racd3401()
	{
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)
		&& isNE(racdlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(racdlnbIO.getStatuz(),varcom.endp)
		|| isNE(racdlnbIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(racdlnbIO.getLife(),covrmjaIO.getLife())
		|| isNE(racdlnbIO.getCoverage(),covrmjaIO.getCoverage())
		|| isNE(racdlnbIO.getRider(),covrmjaIO.getRider())
		|| isNE(racdlnbIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())) {
			racdlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3409);
		}
		if (isNE(racdlnbIO.getCestype(),"2")
		|| isNE(racdlnbIO.getRasnum(),SPACES)
		|| isNE(racdlnbIO.getRngmnt(),SPACES)) {
			printNew3450();
			goTo(GotoLabel.nextr3408);
		}
		wsaaRacdDel.set("Y");
		if (newPageReqA.isTrue()
		|| firstTimeDel.isTrue()) {
			printerFile2.printR5464ah01(r5464aH01);
			wsaaOverflowA.set("N");
			wsaaFirstTimeDel.set("N");
		}
		crtable.set(covrmjaIO.getCrtable());
		chdrnum.set(racdlnbIO.getChdrnum());
		life.set(racdlnbIO.getLife());
		coverage.set(racdlnbIO.getCoverage());
		rider.set(racdlnbIO.getRider());
		plnsfx.set(racdlnbIO.getPlanSuffix());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(racdlnbIO.getCmdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		cmdate.set(datcon1rec.extDate);
		raamount2.set(racdlnbIO.getRaAmount());
		wsaaDelRaamount.add(racdlnbIO.getRaAmount());
		printerFile2.printR5464ad01(r5464aD01);
		racdseqIO.setParams(SPACES);
		racdseqIO.setRrn(racdlnbIO.getRrn());
		racdseqIO.setFormat(formatsInner.racdseqrec);
		racdseqIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdseqIO);
		if (isNE(racdseqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdseqIO.getParams());
			fatalError600();
		}
		racdseqIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdseqIO);
		if (isNE(racdseqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdseqIO.getParams());
			fatalError600();
		}
		/* Update the Control Total - No. of Deleted Recs and*/
		/* Total Amount Deleted.*/
		contotrec.totno.set(ct05);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(racdlnbIO.getRaAmount());
		callContot001();
	}

protected void nextr3408()
	{
		racdlnbIO.setFunction(varcom.nextr);
	}

protected void printNew3450()
	{
		new3451();
	}

protected void new3451()
	{
		wsaaSecondDetl.set("Y");
		if (firstTime.isTrue()) {
			/*      IF NOT NEW-PAGE-REQ                                       */
			if (!newPageReqN.isTrue()) {
				/*       WRITE PRINTER-REC1    FROM R5464-D03                   */
				printerFile3.printR5464nd03(r5464nD03);
			}
			wsaaFirstTime.set("N");
		}
		/*  IF NEW-PAGE-REQ*/
		if (newPageReqN.isTrue()) {
			/*   WRITE PRINTER-REC1       FROM R5464-H01*/
			printerFile3.printR5464nh01(r5464nH01);
			/*     WRITE PRINTER-REC1       FROM R5464-D03                   */
			printerFile3.printR5464nd03(r5464nD03);
			/*      MOVE 'N'                 TO WSAA-OVERFLOW                 */
			wsaaOverflowN.set("N");
		}
		if (isNE(racdlnbIO.getChdrnum(),wsaaChdrnum2)) {
			r5464nD04Inner.chdrnum.set(racdlnbIO.getChdrnum());
			wsaaChdrnum2.set(racdlnbIO.getChdrnum());
		}
		else {
			r5464nD04Inner.chdrnum.set(SPACES);
		}
		r5464nD04Inner.crtable.set(covrmjaIO.getCrtable());
		r5464nD04Inner.life.set(racdlnbIO.getLife());
		r5464nD04Inner.coverage.set(racdlnbIO.getCoverage());
		r5464nD04Inner.rider.set(racdlnbIO.getRider());
		r5464nD04Inner.plnsfx.set(racdlnbIO.getPlanSuffix());
		datcon1rec.intDate.set(racdlnbIO.getCmdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		r5464nD04Inner.cmdate.set(datcon1rec.extDate);
		r5464nD04Inner.rasnum.set(racdlnbIO.getRasnum());
		r5464nD04Inner.rngmnt.set(racdlnbIO.getRngmnt());
		/* Read T5454 to retrieve the RETYPE description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5454);
		descIO.setDescitem(racdlnbIO.getRetype());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		r5464nD04Inner.retypedesc.set(descIO.getLongdesc());
		r5464nD04Inner.raamount.set(racdlnbIO.getRaAmount());
		wsaaNewRaamount.add(racdlnbIO.getRaAmount());
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(racdlnbIO.getRaAmount());
		callContot001();
		/* Write detail, checking for page overflow*/
		/*  WRITE PRINTER-REC1          FROM R5464-D04                   */
		printerFile3.printR5464nd04(r5464nD04Inner.r5464nD04);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLS-BACK*/
		/*EXIT*/
	}

protected void writeNewChdr3700()
	{
		chdr3710();
	}

protected void chdr3710()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		chdrmjaIO.setChdrnum(chdrlnbIO.getChdrnum());
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(bsscIO.getEffectiveDate());
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrfrom(bsscIO.getEffectiveDate());
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void writePtrn3800()
	{
		ptrn3810();
	}

protected void ptrn3810()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setBatccoy(batcdorrec.company);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		ptrnIO.setBatctrcde(batcdorrec.trcde);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setValidflag("1");
		ptrnIO.setTransactionDate(wsaaToday);
		ptrnIO.setTransactionTime(wsaaTime);
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(ZERO);
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void unlockContract3900()
	{
		unlockContr3910();
	}

protected void unlockContr3910()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sqlChdrnum);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlracdpf1conn, sqlracdpf1ps, sqlracdpf1rs);
		/* Close the printer files.*/
		printerFile1.close();
		printerFile2.close();
		printerFile3.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(rexpupdrec.chdrcoy);
		zrdecplrec.batctrcde.set(rexpupdrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A090-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData racdseqrec = new FixedLengthStringData(10).init("RACDSEQREC");
	private FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	private FixedLengthStringData racdmjarec = new FixedLengthStringData(10).init("RACDMJAREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData lifeenqrec = new FixedLengthStringData(10).init("LIFEENQREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure R5464T-D02--INNER
 */
private static final class R5464tD02Inner {

	private FixedLengthStringData r5464tD02 = new FixedLengthStringData(91);
	private FixedLengthStringData r5464td02O = new FixedLengthStringData(91).isAPartOf(r5464tD02, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5464td02O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5464td02O, 8);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5464td02O, 12);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5464td02O, 14);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5464td02O, 16);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5464td02O, 18);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5464td02O, 22);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5464td02O, 32);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(r5464td02O, 40);
	private FixedLengthStringData retypedesc = new FixedLengthStringData(30).isAPartOf(r5464td02O, 44);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5464td02O, 74);
}
/*
 * Class transformed  from Data Structure R5464N-D04--INNER
 */
private static final class R5464nD04Inner {

	private FixedLengthStringData r5464nD04 = new FixedLengthStringData(91);
	private FixedLengthStringData r5464nd04O = new FixedLengthStringData(91).isAPartOf(r5464nD04, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5464nd04O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5464nd04O, 8);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5464nd04O, 12);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5464nd04O, 14);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5464nd04O, 16);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5464nd04O, 18);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5464nd04O, 22);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5464nd04O, 32);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(r5464nd04O, 40);
	private FixedLengthStringData retypedesc = new FixedLengthStringData(30).isAPartOf(r5464nd04O, 44);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5464nd04O, 74);
}
}
