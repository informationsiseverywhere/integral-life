package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:09
 * Description:
 * Copybook name: CCALKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ccalkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ccalFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ccalKey = new FixedLengthStringData(64).isAPartOf(ccalFileKey, 0, REDEFINE);
  	public FixedLengthStringData ccalChdrcoy = new FixedLengthStringData(1).isAPartOf(ccalKey, 0);
  	public FixedLengthStringData ccalChdrnum = new FixedLengthStringData(8).isAPartOf(ccalKey, 1);
  	public FixedLengthStringData ccalLife = new FixedLengthStringData(2).isAPartOf(ccalKey, 9);
  	public FixedLengthStringData ccalCoverage = new FixedLengthStringData(2).isAPartOf(ccalKey, 11);
  	public FixedLengthStringData ccalRider = new FixedLengthStringData(2).isAPartOf(ccalKey, 13);
  	public PackedDecimalData ccalPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ccalKey, 15);
  	public PackedDecimalData ccalSeqno = new PackedDecimalData(2, 0).isAPartOf(ccalKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(ccalKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ccalFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ccalFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}