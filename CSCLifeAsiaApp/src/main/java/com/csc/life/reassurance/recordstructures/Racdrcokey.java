package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:21
 * Description:
 * Copybook name: RACDRCOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdrcokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdrcoFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdrcoKey = new FixedLengthStringData(64).isAPartOf(racdrcoFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdrcoChdrcoy = new FixedLengthStringData(1).isAPartOf(racdrcoKey, 0);
  	public FixedLengthStringData racdrcoChdrnum = new FixedLengthStringData(8).isAPartOf(racdrcoKey, 1);
  	public FixedLengthStringData racdrcoLife = new FixedLengthStringData(2).isAPartOf(racdrcoKey, 9);
  	public FixedLengthStringData racdrcoCoverage = new FixedLengthStringData(2).isAPartOf(racdrcoKey, 11);
  	public FixedLengthStringData racdrcoRider = new FixedLengthStringData(2).isAPartOf(racdrcoKey, 13);
  	public PackedDecimalData racdrcoPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdrcoKey, 15);
  	public PackedDecimalData racdrcoSeqno = new PackedDecimalData(2, 0).isAPartOf(racdrcoKey, 18);
  	public PackedDecimalData racdrcoCurrfrom = new PackedDecimalData(8, 0).isAPartOf(racdrcoKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(racdrcoKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdrcoFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdrcoFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}