/*
 * File: P5437.java
 * Date: 30 August 2009 0:25:52
 * Author: Quipoz Limited
 * 
 * Class transformed from P5437.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.screens.S5437ScreenVars;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Facultative Cession Maintenance.
*
* This is a subfile selection program controlled by OPTSWCH.
* It is used to create, modify, delete or enquire upon inactive
* facultative RACD records (i.e. VALIDFLAG = '3' and CESTYPE =
* '2'). This program will never allow the user to access treaty
* RACD records.
*
* This Program is Called from three places on the system - New
* Business, Alterations and Contract AutoCessions.
*
* This program is used both force proposals and in-force
* contracts and therefore retrieves either COVTs or COVRs
* depending on the contract status.
*
* For proposals, this program is accessed from a check box on
* the new business component screens. The ckeck box will only
* be displayed if facultative RACD records exist.
*
* For in-force contracts, this program is accessed from the
* auto cession selection screen.
*
* The user has four options:
*
* 1 - Enquire
*
* 2 - Modify
*
* 3 - Delete
*
* 4 - Create (function key F10 will enable the user to add
*     a new RACD record).
*
*****************************************************************
* </pre>
*/
public class P5437 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5437");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaOpt = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private PackedDecimalData wsaaT5448Itdmfrm = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaCvChdrkey = new FixedLengthStringData(11);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaCvChdrkey, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCvChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCvChdrkey, 2);
	private FixedLengthStringData wsaaCvChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCvChdrkey, 3);
	private FixedLengthStringData wsaaCvLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCvCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCvRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCvPlnsfx = new PackedDecimalData(4, 0);
	private String wsaaOptionSelected = "";

	private FixedLengthStringData wsaaValidFkey = new FixedLengthStringData(1);
	private Validator validFkey = new Validator(wsaaValidFkey, "Y");

	private FixedLengthStringData wsaaCompSelected = new FixedLengthStringData(1);
	private Validator compSelected = new Validator(wsaaCompSelected, "Y");
	private String r060 = "R060";
		/* TABLES */
	private String t5448 = "T5448";
	private String racdlnbrec = "RACDLNBREC";
	private String itdmrec = "ITEMREC";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Reassurance Cession Details New Busines*/
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private T5448rec t5448rec = new T5448rec();
	private Wssplife wssplife = new Wssplife();
	private S5437ScreenVars sv = ScreenProgram.getScreenVars( S5437ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		next1280, 
		exit1290, 
		preExit, 
		exit2090, 
		updateErrorIndicators2120, 
		optswch4080, 
		exit4090
	}

	public P5437() {
		super();
		screenVars = sv;
		new ScreenModel("S5437", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			subfileLoad1080();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5437", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		wsaaValidFkey.set("N");
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		for (wsaaOpt.set(1); !(isGT(wsaaOpt,20)
		|| isEQ(optswchrec.optsType[wsaaOpt.toInt()],SPACES)); wsaaOpt.add(1)){
			if (isEQ(optswchrec.optsType[wsaaOpt.toInt()],"L")) {
				sv.optdsc[wsaaOpt.toInt()].set(optswchrec.optsDsc[wsaaOpt.toInt()]);
			}
			if (isEQ(optswchrec.optsType[wsaaOpt.toInt()],"F")) {
				sv.optdscx.set(optswchrec.optsDsc[wsaaOpt.toInt()]);
			}
		}
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getValidflag(),"3")) {
			covtlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
			wsaaCvChdrcoy.set(covtlnbIO.getChdrcoy());
			wsaaCvChdrnum.set(covtlnbIO.getChdrnum());
			wsaaCvLife.set(covtlnbIO.getLife());
			wsaaCvCoverage.set(covtlnbIO.getCoverage());
			wsaaCvRider.set(covtlnbIO.getRider());
			wsaaCvPlnsfx.set(ZERO);
			wssplife.bigAmt.set(covtlnbIO.getSumins());
			sv.sumins01.set(covtlnbIO.getSumins());
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(chdrlnbIO.getCnttype().toString());
			stringVariable1.append(covtlnbIO.getCrtable().toString());
			wsaaT5448Item.setLeft(stringVariable1.toString());
			wsaaT5448Itdmfrm.set(covtlnbIO.getEffdate());
		}
		else {
			covrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrlnbIO);
			if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrlnbIO.getParams());
				fatalError600();
			}
			wsaaCvChdrcoy.set(covrlnbIO.getChdrcoy());
			wsaaCvChdrnum.set(covrlnbIO.getChdrnum());
			wsaaCvLife.set(covrlnbIO.getLife());
			wsaaCvCoverage.set(covrlnbIO.getCoverage());
			wsaaCvRider.set(covrlnbIO.getRider());
			wsaaCvPlnsfx.set(covrlnbIO.getPlanSuffix());
			wssplife.bigAmt.set(covrlnbIO.getSumins());
			sv.sumins01.set(covrlnbIO.getSumins());
			StringBuilder stringVariable2 = new StringBuilder();
			stringVariable2.append(chdrlnbIO.getCnttype().toString());
			stringVariable2.append(covrlnbIO.getCrtable().toString());
			wsaaT5448Item.setLeft(stringVariable2.toString());
			wsaaT5448Itdmfrm.set(covrlnbIO.getCrrcd());
		}
		sv.sumins02.set(ZERO);
		wssplife.chdrky.set(wsaaCvChdrkey);
		wssplife.life.set(wsaaCvLife);
		sv.life.set(wsaaCvLife);
		wssplife.coverage.set(wsaaCvCoverage);
		sv.coverage.set(wsaaCvCoverage);
		wssplife.rider.set(wsaaCvRider);
		sv.rider.set(wsaaCvRider);
		wssplife.planSuffix.set(wsaaCvPlnsfx);
		wssplife.cntcurr.set(chdrlnbIO.getCntcurr());
		wsspcomn.tranno.set(chdrlnbIO.getTranno());
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		wssplife.occdate.set(chdrlnbIO.getOccdate());
		readT54481100();
		wssplife.lrkcls.set(t5448rec.rrsktyp);
		wssplife.rrevfrq.set(t5448rec.rrevfrq);
	}

protected void subfileLoad1080()
	{
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(wsaaCvChdrcoy);
		racdlnbIO.setChdrnum(wsaaCvChdrnum);
		racdlnbIO.setLife(wsaaCvLife);
		racdlnbIO.setCoverage(wsaaCvCoverage);
		racdlnbIO.setRider(wsaaCvRider);
		racdlnbIO.setPlanSuffix(wsaaCvPlnsfx);
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "SEQNO");
		while ( !(isEQ(racdlnbIO.getStatuz(),varcom.endp))) {
			listRacd1200();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void readT54481100()
	{
		t54481110();
	}

protected void t54481110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(t5448);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setItmfrm(wsaaT5448Itdmfrm);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaT5448Item,itdmIO.getItemitem())
		|| isNE(chdrlnbIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5448Item);
			itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
			itdmIO.setItemtabl(t5448);
			itdmIO.setStatuz(r060);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
	}

protected void listRacd1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFirstRacd1210();
				}
				case next1280: {
					next1280();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFirstRacd1210()
	{
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)
		&& isNE(racdlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(),wsaaCvChdrcoy)
		|| isNE(racdlnbIO.getChdrnum(),wsaaCvChdrnum)
		|| isNE(racdlnbIO.getLife(),wsaaCvLife)
		|| isNE(racdlnbIO.getCoverage(),wsaaCvCoverage)
		|| isNE(racdlnbIO.getRider(),wsaaCvRider)
		|| isNE(racdlnbIO.getPlanSuffix(),wsaaCvPlnsfx)
		|| isNE(racdlnbIO.getSeqno(),ZERO)
		|| isEQ(racdlnbIO.getStatuz(),varcom.endp)) {
			racdlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		sv.sumins02.add(racdlnbIO.getRaAmount());
		if (isNE(racdlnbIO.getCestype(),"2")) {
			goTo(GotoLabel.next1280);
		}
		sv.cmdate.set(racdlnbIO.getCmdate());
		sv.rasnum.set(racdlnbIO.getRasnum());
		sv.rngmnt.set(racdlnbIO.getRngmnt());
		sv.raAmount.set(racdlnbIO.getRaAmount());
		sv.ovrdind.set(racdlnbIO.getOvrdind());
		sv.hrrn.set(racdlnbIO.getRrn());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5437", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void next1280()
	{
		racdlnbIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isEQ(wsaaScrnStatuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaScrnStatuz,"SWCH")) {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("F");
			optswchrec.optsSelCode.set(wsaaScrnStatuz);
			optswchrec.optsSelOptno.set(ZERO);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				scrnparams.errorCode.set(optswchrec.optsStatuz);
				wsspcomn.edterror.set("Y");
			}
			else {
				wsaaValidFkey.set("Y");
			}
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5437", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		wsaaCompSelected.set("N");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNextRecord2130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		wsaaCompSelected.set("Y");
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
			// Ticket #ILIFE-1330 start by akhan203 
			sv.select.set(SPACES);
			// Ticket #ILIFE-1330 end
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5437", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextRecord2130()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5437", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case optswch4080: {
					optswch4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsaaOptionSelected = "N";
		if (validFkey.isTrue()) {
			optswchrec.optsSelType.set("F");
			optswchrec.optsSelCode.set(wsaaScrnStatuz);
			optswchrec.optsSelOptno.set(ZERO);
			wsaaValidFkey.set("N");
			wsaaOptionSelected = "Y";
			goTo(GotoLabel.optswch4080);
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			goTo(GotoLabel.optswch4080);
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			lineSelect4200();
			wsaaOptionSelected = "Y";
		}
		if (isEQ(sv.select,"2")) {
			optswchrec.optsSelOptno.set(2);
			lineSelect4200();
			wsaaOptionSelected = "Y";
		}
		if (isEQ(sv.select,"3")) {
			optswchrec.optsSelOptno.set(3);
			lineSelect4200();
			wsaaOptionSelected = "Y";
		}
	}

protected void optswch4080()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		&& isEQ(wsaaOptionSelected,"N")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			programStack4300();
			goTo(GotoLabel.exit4090);
		}
		programStack4300();
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5437", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,scrnparams.statuz)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void lineSelect4200()
	{
		line4210();
	}

protected void line4210()
	{
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		sv.select.set(SPACES);
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setRrn(sv.hrrn);
		racdlnbIO.setFormat(racdlnbrec);
		racdlnbIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		racdlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		scrnparams.function.set(varcom.supd);
		screenio4100();
	}

protected void programStack4300()
	{
		/*STCK*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*EXIT*/
	}
}
