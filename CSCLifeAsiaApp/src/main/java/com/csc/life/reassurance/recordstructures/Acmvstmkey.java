package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:45
 * Description:
 * Copybook name: ACMVSTMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvstmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvstmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvstmKey = new FixedLengthStringData(64).isAPartOf(acmvstmFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvstmBatccoy = new FixedLengthStringData(1).isAPartOf(acmvstmKey, 0);
  	public FixedLengthStringData acmvstmSacscode = new FixedLengthStringData(2).isAPartOf(acmvstmKey, 1);
  	public PackedDecimalData acmvstmEffdate = new PackedDecimalData(8, 0).isAPartOf(acmvstmKey, 3);
  	public FixedLengthStringData filler = new FixedLengthStringData(56).isAPartOf(acmvstmKey, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvstmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvstmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}