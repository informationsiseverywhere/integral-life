/*
 * File: B5441.java
 * Date: 29 August 2009 21:16:55
 * Author: Quipoz Limited
 * 
 * Class transformed from B5441.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.reassurance.dataaccess.LirrenqTableDAM;
import com.csc.life.reassurance.recordstructures.P5441par;
import com.csc.life.reassurance.reports.R5441Report;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reassurance Redistribution Report.
*
* Overview
* ========
*
* This program uses SQL to extract life and joint life client
* numbers from the LIFE file that have terminated RACD records
* (i.e. validflag '4') attached to them.
*
* Then, it will read through the LIRRENQ records for each client
* number extracted to find any other existing cessions attached
* to the life.
*
* The details will only be printed if the reassured amount is
* greater than zero (indicating a current record).
*
* Processing
* ==========
*
* A Parameter Screen is included to allow users to specify the
* transaction date range. Records within the specified date
* will be selected for printing.
*
* The SQL selection criteria are as follows :
*
*        SELECT :  . RACDPF.CHDRNUM   = LIFEPF.CHDRNUM  and
*                  . RACDPF.VALIDFLAG = '4'             and
*                  . LIFEPF.VALIDFLAG = '1'             and
*                  . P5441-DATEFROM  <= RACDPF.CURRFROM and
*                    P5441-DATETO    >= RACDPF.CURRFROM
*
* Initialise
*       Define the SQL query by declaring a cursor. The SQL will
*       extract all LIFE cleint numbers that meet the criteria
*       specified above.
*
* Read
*       Read the CLNT file to get the client name.
*
* Perform Until End of File
*       Read the LIRRENQ file.
*       If LIRRENQ-RA-AMOUNT > 0
*          print detail line
*       End-if
*
*****************************************************************
* </pre>
*/
public class B5441 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlracdpf1rs = null;
	private java.sql.PreparedStatement sqlracdpf1ps = null;
	private java.sql.Connection sqlracdpf1conn = null;
	private String sqlracdpf1 = "";
	private R5441Report printerFile = new R5441Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5441");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private String lirrenqrec = "LIRRENQREC";
	private String cltsrec = "CLTSREC";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);
		/* ERRORS */
	private String esql = "ESQL";

		/* SQL-RACDPF */
	private FixedLengthStringData sqlRacdrec = new FixedLengthStringData(8);
	private FixedLengthStringData sqlLifcnum = new FixedLengthStringData(8).isAPartOf(sqlRacdrec, 0);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaaPrevClntnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaDatefrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateto = new ZonedDecimalData(8, 0);

	private FixedLengthStringData r5441H01 = new FixedLengthStringData(20);
	private FixedLengthStringData r5441h01O = new FixedLengthStringData(20).isAPartOf(r5441H01, 0);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10).isAPartOf(r5441h01O, 0);
	private FixedLengthStringData dateto = new FixedLengthStringData(10).isAPartOf(r5441h01O, 10);

	private FixedLengthStringData r5441D01 = new FixedLengthStringData(85);
	private FixedLengthStringData r5441d01O = new FixedLengthStringData(85).isAPartOf(r5441D01, 0);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(r5441d01O, 0);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37).isAPartOf(r5441d01O, 8);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5441d01O, 45);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(r5441d01O, 53);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5441d01O, 57);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(r5441d01O, 65);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5441d01O, 68);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Reassurance Experience Enquiry File*/
	private LirrenqTableDAM lirrenqIO = new LirrenqTableDAM();
	private P5441par p5441par = new P5441par();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		eof2060, 
		exit2090, 
		exit2690
	}

	public B5441() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		p5441par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaOverflow.set("Y");
		wsaaPrevClntnum.set(SPACES);
		wsaaDatefrm.set(p5441par.datefrm);
		wsaaDateto.set(p5441par.dateto);
		sqlracdpf1 = " SELECT  LIFEPF.LIFCNUM" +
" FROM   " + appVars.getTableNameOverriden("RACDPF") + " ,  " + appVars.getTableNameOverriden("LIFEPF") + " " +
" WHERE RACDPF.CHDRNUM = LIFEPF.CHDRNUM" +
" AND RACDPF.VALIDFLAG = '4'" +
" AND LIFEPF.VALIDFLAG = '1'" +
" AND RACDPF.CURRFROM >= ?" +
" AND RACDPF.CURRFROM <= ?" +
" GROUP BY LIFEPF.LIFCNUM";
		sqlerrorflag = false;
		try {
			sqlracdpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.reassurance.dataaccess.RacdpfTableDAM(), new com.csc.life.newbusiness.dataaccess.LifepfTableDAM()});
			sqlracdpf1ps = appVars.prepareStatementEmbeded(sqlracdpf1conn, sqlracdpf1);
			appVars.setDBDouble(sqlracdpf1ps, 1, wsaaDatefrm.toDouble());
			appVars.setDBDouble(sqlracdpf1ps, 2, wsaaDateto.toDouble());
			sqlracdpf1rs = appVars.executeQuery(sqlracdpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		datcon1rec.intDate.set(wsaaDateto);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dateto.set(datcon1rec.extDate);
		datcon1rec.intDate.set(wsaaDatefrm);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datefrm.set(datcon1rec.extDate);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2060: {
					eof2060();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlracdpf1rs.next()) {
				appVars.getDBObject(sqlracdpf1rs, 1, sqlLifcnum);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(sqlLifcnum);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		lirrenqIO.setParams(SPACES);
		lirrenqIO.setCompany(bsprIO.getCompany());
		lirrenqIO.setClntpfx("CN");
		lirrenqIO.setClntcoy(bsprIO.getFsuco());
		lirrenqIO.setClntnum(sqlLifcnum);
		lirrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lirrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lirrenqIO.setFitKeysSearch("COMPANY", "CLNTPFX", "CLNTCOY", "CLNTNUM");
		lirrenqIO.setFormat(lirrenqrec);
		while ( !(isEQ(lirrenqIO.getStatuz(),varcom.endp))) {
			readLirrRecords2600();
		}
		
	}

protected void readLirrRecords2600()
	{
		try {
			readLirrRecords2610();
		}
		catch (GOTOException e){
		}
	}

protected void readLirrRecords2610()
	{
		SmartFileCode.execute(appVars, lirrenqIO);
		if (isNE(lirrenqIO.getStatuz(),varcom.oK)
		&& isNE(lirrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lirrenqIO.getParams());
			fatalError600();
		}
		if (isNE(lirrenqIO.getCompany(),bsprIO.getCompany())
		|| isNE(lirrenqIO.getClntpfx(),"CN")
		|| isNE(lirrenqIO.getClntcoy(),bsprIO.getFsuco())
		|| isNE(lirrenqIO.getClntnum(),sqlLifcnum)
		|| isEQ(lirrenqIO.getStatuz(),varcom.endp)) {
			lirrenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		if (isGT(lirrenqIO.getRaAmount(),0)) {
			printDetails2700();
		}
		lirrenqIO.setFunction(varcom.nextr);
	}

protected void printDetails2700()
	{
		printDetails2710();
	}

protected void printDetails2710()
	{
		if (newPageReq.isTrue()) {
			printerFile.printR5441h01(r5441H01);
		}
		if (isEQ(lirrenqIO.getClntnum(),wsaaPrevClntnum)
		&& !newPageReq.isTrue()) {
			clntnum.set(SPACES);
			clntnm.set(SPACES);
		}
		else {
			clntnum.set(lirrenqIO.getClntnum());
			plainname();
			clntnm.set(wsspLongconfname);
			if (newPageReq.isTrue()) {
				wsaaOverflow.set("N");
			}
		}
		chdrnum.set(lirrenqIO.getChdrnum());
		rasnum.set(lirrenqIO.getRasnum());
		rngmnt.set(lirrenqIO.getRngmnt());
		currcode.set(lirrenqIO.getCurrency());
		raamount.set(lirrenqIO.getRaAmount());
		printerFile.printR5441d01(r5441D01);
		wsaaPrevClntnum.set(lirrenqIO.getClntnum());
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlracdpf1conn, sqlracdpf1ps, sqlracdpf1rs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
