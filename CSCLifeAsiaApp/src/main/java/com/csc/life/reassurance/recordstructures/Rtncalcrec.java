package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:08
 * Description:
 * Copybook name: RTNCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rtncalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rtncalcRec = new FixedLengthStringData(64);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rtncalcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rtncalcRec, 5);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rtncalcRec, 9);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(1).isAPartOf(rtncalcRec, 10);
  	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(rtncalcRec, 11);
  	public FixedLengthStringData riskClass = new FixedLengthStringData(4).isAPartOf(rtncalcRec, 19);
  	public FixedLengthStringData reassurer = new FixedLengthStringData(8).isAPartOf(rtncalcRec, 23);
  	public FixedLengthStringData arrangement = new FixedLengthStringData(4).isAPartOf(rtncalcRec, 31);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(rtncalcRec, 35).setUnsigned();
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(rtncalcRec, 43);
  	public PackedDecimalData available = new PackedDecimalData(17, 2).isAPartOf(rtncalcRec, 46);
  	public PackedDecimalData discRetention = new PackedDecimalData(17, 2).isAPartOf(rtncalcRec, 55);


	public void initialize() {
		COBOLFunctions.initialize(rtncalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rtncalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}