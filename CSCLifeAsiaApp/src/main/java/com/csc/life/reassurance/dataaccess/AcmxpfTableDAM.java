package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AcmxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:37
 * Class transformed from ACMXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AcmxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 115;
	public FixedLengthStringData acmxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData acmxpfRecord = acmxrec;
	
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(acmxrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(acmxrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(acmxrec);
	public FixedLengthStringData rldgacct = DD.rldgacct.copy().isAPartOf(acmxrec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(acmxrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(acmxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(acmxrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(acmxrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(acmxrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(acmxrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(acmxrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(acmxrec);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(acmxrec);
	public PackedDecimalData jrnseq = DD.jrnseq.copy().isAPartOf(acmxrec);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(acmxrec);
	public FixedLengthStringData glsign = DD.glsign.copy().isAPartOf(acmxrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(acmxrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(acmxrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(acmxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AcmxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AcmxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AcmxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AcmxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AcmxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AcmxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AcmxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ACMXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"BATCCOY, " +
							"SACSCODE, " +
							"EFFDATE, " +
							"RLDGACCT, " +
							"ORIGCURR, " +
							"SACSTYP, " +
							"TRANNO, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"RDOCNUM, " +
							"JRNSEQ, " +
							"ORIGAMT, " +
							"GLSIGN, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     batccoy,
                                     sacscode,
                                     effdate,
                                     rldgacct,
                                     origcurr,
                                     sacstyp,
                                     tranno,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     rdocnum,
                                     jrnseq,
                                     origamt,
                                     glsign,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		batccoy.clear();
  		sacscode.clear();
  		effdate.clear();
  		rldgacct.clear();
  		origcurr.clear();
  		sacstyp.clear();
  		tranno.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		rdocnum.clear();
  		jrnseq.clear();
  		origamt.clear();
  		glsign.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAcmxrec() {
  		return acmxrec;
	}

	public FixedLengthStringData getAcmxpfRecord() {
  		return acmxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAcmxrec(what);
	}

	public void setAcmxrec(Object what) {
  		this.acmxrec.set(what);
	}

	public void setAcmxpfRecord(Object what) {
  		this.acmxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(acmxrec.getLength());
		result.set(acmxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}