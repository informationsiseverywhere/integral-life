package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:35
 * Description:
 * Copybook name: CSNCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Csncalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData csncalcRec = new FixedLengthStringData(78);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(csncalcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(csncalcRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(csncalcRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(csncalcRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(csncalcRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(csncalcRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(csncalcRec, 22);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(csncalcRec, 24);
  	public FixedLengthStringData fsuco = new FixedLengthStringData(1).isAPartOf(csncalcRec, 27);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(csncalcRec, 28);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(csncalcRec, 31);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(csncalcRec, 34);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(csncalcRec, 39);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(csncalcRec, 42);
  	public PackedDecimalData incrAmt = new PackedDecimalData(17, 2).isAPartOf(csncalcRec, 43);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(csncalcRec, 52);
  	public FixedLengthStringData batckey = new FixedLengthStringData(22).isAPartOf(csncalcRec, 56);


	public void initialize() {
		COBOLFunctions.initialize(csncalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		csncalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}