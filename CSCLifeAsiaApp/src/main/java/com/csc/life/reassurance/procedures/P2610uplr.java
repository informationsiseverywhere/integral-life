/*
 * File: P2610uplr.java
 * Date: 29 August 2009 23:39:52
 * Author: Quipoz Limited
 * 
 * Class transformed from P2610UPLR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Addacmv;
import com.csc.fsu.general.recordstructures.Addacmvrec;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Genlkey;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Rldgkey;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*         CASH RECEIPTS UPDATING - TRANCODE LR - LIFE REASSURER
*
* THIS ROUTINE IS CALLED DYNAMICALLY FROM P2610 - CASH CREATE
* TO CREATE THE DISSECTION FOR WHICH THE CASH APPLIES.
*
* Note that this is a new routine that calls ADDACMV to create
* ACMV records and update the balance records in ACBL.
*
*
*
******************************************************************
* </pre>
*/
public class P2610uplr extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("P2610UPLR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	private Addacmvrec addacmvrec = new Addacmvrec();
	private Cashedrec cashedrec = new Cashedrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private Genlkey wsaaGenlkey = new Genlkey();
	private Rdockey wsaaRdockey = new Rdockey();
	private Rldgkey wsaaRldgkey = new Rldgkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit579, 
		exit589, 
		exit1090
	}

	public P2610uplr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		cashedrec.cashedRec = convertAndSetParam(cashedrec.cashedRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		para011();
		exit090();
	}

protected void para011()
	{
		cashedrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaProg);
		updateDissection1000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void syserr570()
	{
		try {
			para571();
		}
		catch (GOTOException e){
		}
		finally{
			exit579();
		}
	}

protected void para571()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit579);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		cashedrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError580()
	{
		try {
			para581();
		}
		catch (GOTOException e){
		}
		finally{
			exit589();
		}
	}

protected void para581()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit589);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		cashedrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void updateDissection1000()
	{
		try {
			para1000();
		}
		catch (GOTOException e){
		}
	}

protected void para1000()
	{
		if (isEQ(cashedrec.origamt,0)
		&& isEQ(cashedrec.acctamt,0)) {
			goTo(GotoLabel.exit1090);
		}
		addacmvrec.addacmvRec.set(SPACES);
		addacmvrec.contot.set(0);
		varcom.vrcmTranid.set(cashedrec.tranid);
		addacmvrec.termid.set(varcom.vrcmTermid);
		addacmvrec.user.set(varcom.vrcmUser);
		addacmvrec.transactionDate.set(varcom.vrcmDate);
		addacmvrec.transactionTime.set(varcom.vrcmTime);
		wsaaBatckey.batcKey.set(cashedrec.batchkey);
		addacmvrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		addacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		addacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		addacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		addacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		addacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		addacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		wsaaRdockey.rdocKey.set(cashedrec.doctkey);
		addacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
		addacmvrec.transeq.set(wsaaRdockey.rdocTranseq);
		wsaaRldgkey.rldgKey.set(cashedrec.trankey);
		addacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
		addacmvrec.rldgacct.set(wsaaRldgkey.rldgRldgacct);
		wsaaGenlkey.genlKey.set(cashedrec.genlkey);
		addacmvrec.genlcoy.set(wsaaGenlkey.genlGenlcoy);
		addacmvrec.genlcur.set(wsaaGenlkey.genlGenlcur);
		addacmvrec.glcode.set(wsaaGenlkey.genlGenlcde);
		addacmvrec.sacscode.set(cashedrec.sacscode);
		addacmvrec.sacstyp.set(cashedrec.sacstyp);
		addacmvrec.glsign.set(cashedrec.sign);
		addacmvrec.transeq.set(cashedrec.transeq);
		addacmvrec.effdate.set(cashedrec.trandate);
		addacmvrec.trandesc.set(cashedrec.trandesc);
		addacmvrec.origamt.set(cashedrec.origamt);
		addacmvrec.acctamt.set(cashedrec.acctamt);
		addacmvrec.origccy.set(cashedrec.origccy);
		addacmvrec.acctccy.set(cashedrec.acctccy);
		addacmvrec.crate.set(cashedrec.dissrate);
		if (isNE(cashedrec.tranno,NUMERIC)) {
			cashedrec.tranno.set(0);
		}
		addacmvrec.rdocpfx.set("CA");
		addacmvrec.creddte.set(varcom.vrcmMaxDate);
		addacmvrec.tranno.set(cashedrec.tranno);
		addacmvrec.function.set(SPACES);
		callProgram(Addacmv.class, addacmvrec.addacmvRec);
		if (isNE(addacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(addacmvrec.statuz);
			cashedrec.statuz.set(addacmvrec.statuz);
			syserr570();
		}
	}
}
