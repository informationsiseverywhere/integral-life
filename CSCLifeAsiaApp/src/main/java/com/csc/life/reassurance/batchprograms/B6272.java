/*
 * File: B6272.java
 * Date: 29 August 2009 21:21:03
 * Author: Quipoz Limited
 *
 * Class transformed from B6272.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.LiferasTableDAM;
import com.csc.life.reassurance.dataaccess.RactbtcTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Jctlkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  B6272 - Cede Reassurance Premiums.
*  ----------------------------------
*
*
*  Overview.
*  ---------
*
*
*  This program will create the  collections  from  reassurance
*  coverage  type  records.  The  output  from this will be the
*  appropriate ACMV records that reflect the  monies  due  from
*  each reassurance component that is due for billing.
*
*  Reassurance  premiums  will  be  billed  in advance and will
*  cover the period from the current billed to date to  a  date
*  one payment frequency in advance.
*
*  The  Reassurance Coverage Type file, (RACT), will be used as
*  the driver for this file. RACT records that require
*  processing will be selected by SQL executed from within
*  this program.
*
*  The premium will be that held on the  RACT  record  for  the
*  frequency.  If  the  component  is  due to expire during the
*  next frequency for which  reassurance  is  being  calculated
*  then  the premium must be further calculated as pro-rata for
*  the remaining period for which the reassurance component  is
*  active.  Single  premium  components  will  be selected only
*  once and will be updated after this time so that  they  will
*  not be selected again.
*
*  The SQL selection criteria for this program are as follows:
*
*
*       SELECT:
*       -------
*
*       . RACT-CHDRCOY   = Batch Run Company
*       . RACT-VALIDFLAG = '1' or '4'
*       . RACT-BTDATE    is less than or equal to the Batch
*                        Effective Date
*       . RACT-BTDATE    < RACT Reassurance cessation date
*
*       KEY:
*       ----
*
*       . RACT-CHDRCOY
*       . RACT-CHDRNUM
*       . RACT-LIFE
*       . RACT-COVERAGE
*       . RACT-RIDER
*
*  The following control totals will be maintained within  this
*  job:
*
*       1.   RACT Records Read
*       2.   RACT Records Expired
*       3.   Total Amount Billed
*       4.   Total Commission
*       5.   Total Due
*
*  Control  totals  3, 4 and 5 will be automatically maintained
*  by the LIFACMV subroutine which will be called  to  add  the
*  ACMV records.
*
*
*  As  the  job produces financial transactions it is 'batched'
*  using the subroutine 'BATCDOR'.  The  transaction  key  used
*  for  the  batching  is  taken  from  the jobstream details -
*  parameter 1.
*
*  The ACMV records produced by this program will  actually  be
*  written  by  calling  the subroutine LIFACMV. This will also
*  accumulate batch totals which must also be written  away  at
*  the  end  of  each  RACT  record processed by calling BATCUP
*  with a function of 'WRITS'.
*
*  A PTRN record will also be written for each Cont
*  processed.  There  may  be  several  RACT  records  for each
*  contract but only one PTRN record is written for  the  whole
*  contract  no matter how many RACT records were processed for
*  it. The Contract Header  must  be  read,  the  TRANNO  field
*  incremented  by  1  and  re-written  and a PTRN created with
*  this new value in its TRANNO field.
*
*
*  This job will be run periodically, typically monthly.
*
*
*  Processing.
*  -----------
*
*
*  1. Initialise.
*  --------------
*
*  As this batch job updates records from the primary  file  in
*  place  the  commit  counts  must be re-set in the event of a
*  restart after a program failure. The SQL will not  re-select
*  committed  records already processed by the program before a
*  failure.
*
*  Set  up the batching parameters using the subroutine BATCDOR
*  with a function of 'OPEN'.
*
*  Read T5645 - Financial Transaction  Accounting  Rules  using
*  WSAA-PROG as key.
*
*
*  2. Main Processing.
*  -------------------
*
*  Process the RACT records selected for this program by the
*  SQL query. For  each  selected  record  an  ACMV  will  be
*  created   for   each   entry   found   on   T5645.   Perform
*  004-UPDATE-REQD after each read of RACT.
*
*  2.1. Premium Due.
*  -----------------
*
*  The first of these  will  be  the  Premium  Due  which  will
*  equate  to  line  #1 from T5645. Use the Sub-Account Type to
*  read T3695 to obtain the  value  for  the  Internal/External
*  indicator.
*
*  The  premium  on  the RACT record will be the amount to use.
*  The program must account  for  RACT  records  that  are  for
*  single  premium components, instalment premium components or
*  for those rare cases where both types of premium are  to  be
*  found  on the RACT record. There may also be cases where the
*  the instalment premium must be recalculated  each  time  the
*  record is selected by this job.
*
*  Single Premium Only.
*  --------------------
*
*       A  single  premium  only  case may be identified by the
*       fact  that  RACT-SINGP  is  greater  than  zero   while
*       RACT-INSTPREM = zero.
*
*       If  this is the case then the Premium Due for the first
*       ACMV is the value in RACT-SINGP.  Set  this  value  for
*       the ACMV.
*
*       If VALIDFLAG = '1',
*          set the BTDATE to the RCESDTE (risk cessation date ).
*
*       If VALIDFLAG = '4', set it to '2'.
*          leave the BTDATE as is.
*
*       Rewrite the RACT record.
*
*  Instalment Premium Only.
*  ------------------------
*
*       An  instalment  premium  only case may be identified by
*       the fact that RACT-SINGP = zero.
*
*       If this is the case then  the  value  in  RACT-INSTPREM
*       will  be  used as the Premium Due on the ACMV. This may
*       however be zero and this will  mean  that  it  must  be
*       recalculated  each  time  so perform a 'Re-Calculation'
*       section to obtain the instalment amount. For  the  time
*       being  this  section will be empty, it will be added in
*       phase 3  of  Reassurance  which  will  handle  variable
*       premiums and reassurance based on sum at risk.
*
*       After  this  section the premiums may anyway have to be
*       recalculated if the Risk Cessation Date  is  less  than
*       the  BTDATE  plus  1 frequency. So calculate the BTDATE
*       plus 1 frequency and if  the  Risk  Cessation  Date  is
*       less   than  this  recalculate  the  Amount  Due  as  a
*       pro-rata of the amount found on  RACT  or  recalculated
*       by the recalculation section.
*
*       Set the BTDATE to be the BTDATE plus 1 frequency.
*
*       If VALIDFLAG = '4', set it to '2'.
*
*       Rewrite the RACT record.
*
*
*
*  2.2. Commission Due.
*  --------------------
*
*  The second ACMV will be the Premium Due  which  will  equate
*  to  line  #2  from  T5645.  Use the Sub-Account Type to read
*  T3695  to  obtain  the  value  for   the   Internal/External
*  indicator.
*
*  Perform  a  'Calculate  Commission'  section which will work
*  out the amount to place on the ACMV. Call LIFACMV to  create
*  an  ACMV  with  this value using the details from line #2 on
*  T5645.
*
*
*  2.3. Amount Due to Reassurer.
*  -----------------------------
*
*  The third ACMV will be  the  Amount  Due  to  the  Reassurer
*  which   will   equate   to  line  #3  from  T5645.  Use  the
*  Sub-Account Type to read T3695 to obtain the value  for  the
*  Internal/External indicator.
*
*  Calculate  the  value  as  the Premium Due used on the first
*  ACMV minus the Commission Due used on the second ACMV.  Call
*  LIFACMV  to create an ACMV with this value using the details
*  from line #3 on T5645.
*
*
*  After each RACT  has  been  processed  call  BATCUP  with  a
*  function  of  'WRITS'  to  write  away the batch totals that
*  have been accumulated by LIFACMV.
*
*  After each change  of  Contract  Number  read  the  Contract
*  Header  with  a  function of 'READH', increment the value of
*  TRANNO and re-write it. Create a PTRN record with  this  new
*  TRANNO in it.
*
*
*  LIFACMV Fields.
*  ---------------
*
*  The values to be passed to LIFACMV will be as follows:
*
*  Key:      this will be the batch details, the transaction
*            code will be the run parameter 1.
*
*  FUNCTION  'PSTW'
*  RDOCNUM   from T5645
*  TRANNO    calculated - CHDR-TRANNO + 1
*  TRANREF   same as TRANNO
*  RLDGCOY   Batch Run Company
*  SACSCODE  from T5645
*  RLDGACCT  RASNUM
*  ORIGCURR  Contract Currency
*  SACSTYP   from T5645
*  ORIGAMT   amount calculated as Premium Due, Commission Due
*            or the difference between the two
*
*
*****************************************************************
* </pre>
*/
public class B6272 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlract1rs = null;
	private java.sql.PreparedStatement sqlract1ps = null;
	private java.sql.Connection sqlract1conn = null;
	private String sqlract1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6272");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private PackedDecimalData wsaaPremiumDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingleComm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegularComm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommissionDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstalAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalDays = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaActualDays = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaSubroutineName = new FixedLengthStringData(7);
	private PackedDecimalData wsaaOldBtdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscmth = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);
	private FixedLengthStringData wsaaRapayfrq = new FixedLengthStringData(2);
		/* WSAA-FLAGS */
	private String wsaaLifeRead = "";
	private String wsaaEndOfFile = "";
	private String wsaaIgnoreContract = "";

	private FixedLengthStringData wsaaChdrKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaChdrKey, 0);
	private FixedLengthStringData wsaaKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaChdrKey, 1);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* WSAA-LIFE-DETAILS */
	private PackedDecimalData wsaaAge = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-LIFE-DETAILS */
	private PackedDecimalData wsbbAge = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaPremMethInd = new FixedLengthStringData(1);
	private Validator useSingleLife = new Validator(wsaaPremMethInd, "S");
	private Validator useJointLife = new Validator(wsaaPremMethInd, "J");
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSqlEffdate = new ZonedDecimalData(8, 0);
		/* ERRORS */
	private String e308 = "E308";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5647 = "T5647";
	private String t5675 = "T5675";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private String tr695 = "TR695";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String covrrec = "COVRREC";
	private String liferasrec = "LIFERASREC";
	private String ptrnrec = "PTRNREC";
	private String rasarec = "RASAREC";
	private String ractbtcrec = "RACTBTCREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);

		/* SQL-RACTPF */
	private FixedLengthStringData ractrec1 = new FixedLengthStringData(46);
	private FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(ractrec1, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(ractrec1, 1);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(ractrec1, 9);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(ractrec1, 11);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(ractrec1, 13);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(ractrec1, 15);
	private FixedLengthStringData ratype = new FixedLengthStringData(4).isAPartOf(ractrec1, 23);
	private FixedLengthStringData validflag = new FixedLengthStringData(1).isAPartOf(ractrec1, 27);
	private PackedDecimalData rcesdte = new PackedDecimalData(8, 0).isAPartOf(ractrec1, 28);
	private PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(ractrec1, 33);
	private PackedDecimalData btdate = new PackedDecimalData(8, 0).isAPartOf(ractrec1, 38);
	private PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(ractrec1, 43);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Freqcpy freqcpy = new Freqcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Life Logical View for Reassurance -*/
	private LiferasTableDAM liferasIO = new LiferasTableDAM();
	private Premiumrec premiumrec = new Premiumrec();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Reassurance LF for Batch Processing -*/
	private RactbtcTableDAM ractbtcIO = new RactbtcTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5647rec t5647rec = new T5647rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr695rec tr695rec = new Tr695rec();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemkey = new Itemkey();
	private Jctlkey wsaaJctlkey = new Jctlkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readhRactbtc1200,
		writeAcmv1600,
		exit1900,
		eof1180,
		exit1190,
		exit2290,
		exit4190,
		exit4290,
		exit4390,
		regularPrem5120,
		accumPrems5150,
		exit7900
	}

	public B6272() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		open110();
		closeBatch170();
		out185();
	}

protected void open110()
	{
		initialise200();
		setUpSqlCursor300();
		/*PROCESS*/
		while ( !(isEQ(wsaaEndOfFile,"Y"))) {
			mainProcessing1000();
		}

	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
	}

protected void out185()
	{
		closeCursor1200();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
		openBatch220();
	}

protected void initialise210()
	{
		wsaaParmCompany.set(runparmrec.company);
		wsaaSqlEffdate.set(runparmrec.effdate);
	}

protected void openBatch220()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
		wsaaChdrKey.set(SPACES);
		wsaaPremiumDue.set(ZERO);
		wsaaCommissionDue.set(ZERO);
		wsaaInstalAmount.set(ZERO);
		wsaaActualDays.set(ZERO);
		wsaaTotalDays.set(ZERO);
		wsaaEndOfFile = "N";
		wsaaLifeRead = "N";
		wsaaIgnoreContract = "N";
		wsaaOldBtdate.set(varcom.vrcmMaxDate);
		varcom.vrcmTime.set(getCobolTime());
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t5645);
		wsaaItemkey.itemItemitem.set(wsaaProg);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(runparmrec.transcode);
		descIO.setLanguage(runparmrec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		wsaaJctlkey.jctlKey.set(SPACES);
		wsaaJctlkey.jctlJobsts.set("A");
		wsaaJctlkey.jctlJctlpfx.set(smtpfxcpy.jctl);
		wsaaJctlkey.jctlJctlcoy.set(runparmrec.company);
		wsaaJctlkey.jctlJctljn.set(runparmrec.jobname);
		wsaaJctlkey.jctlJctlacyr.set(runparmrec.acctyear);
		wsaaJctlkey.jctlJctlacmn.set(runparmrec.acctmonth);
		wsaaJctlkey.jctlJctljnum.set(runparmrec.jobnum);
	}

protected void setUpSqlCursor300()
	{
		/*BEGIN-READING*/
		sqlract1 = " SELECT  RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.RASNUM, RA.RATYPE, RA.VALIDFLAG, RA.RCESDTE, RA.CRRCD, RA.BTDATE, RA.TRANNO" +
" FROM   " + appVars.getTableNameOverriden("RACTPF") + "  RA" +
" WHERE RA.CHDRCOY = ?" +
" AND (RA.VALIDFLAG = '1'" +
" OR RA.VALIDFLAG = '4')" +
" AND (RA.BTDATE < ?" +
" OR RA.BTDATE = ?)" +
" AND RA.BTDATE < RA.RCESDTE" +
" ORDER BY RA.CHDRCOY, RA.CHDRNUM, RA.LIFE, RA.COVERAGE, RA.RIDER, RA.RASNUM, RA.RATYPE";
		sqlerrorflag = false;
		try {
			sqlract1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.reassurance.dataaccess.RactpfTableDAM());
			sqlract1ps = appVars.prepareStatementEmbeded(sqlract1conn, sqlract1, "RACTPF");
			appVars.setDBString(sqlract1ps, 1, wsaaParmCompany.toString());
			appVars.setDBDouble(sqlract1ps, 2, wsaaSqlEffdate.toDouble());
			appVars.setDBDouble(sqlract1ps, 3, wsaaSqlEffdate.toDouble());
			sqlract1rs = appVars.executeQuery(sqlract1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError();
			throw new RuntimeException("B6272: ERROR (1) - Unexpected return from a transformed GO TO statement: SQL-ERROR");
		}
		while ( !(isEQ(controlrec.flag,"Y"))) {
			updateReqd004();
		}

		/*EXIT*/
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					read1000();
				}
				case readhRactbtc1200: {
					readhRactbtc1200();
				}
				case writeAcmv1600: {
					writeAcmv1600();
				}
				case exit1900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read1000()
	{
		fetchPrimary1100();
		if ((endOfFile.isTrue())) {
			wsaaEndOfFile = "Y";
			goTo(GotoLabel.exit1900);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		wsaaLifeRead = "N";
		if (isEQ(chdrcoy,wsaaKeyChdrcoy)
		&& isEQ(chdrnum,wsaaKeyChdrnum)) {
			if (isEQ(wsaaIgnoreContract,"Y")) {
				contotrec.totno.set(ct02);
				contotrec.totval.set(1);
				callContot001();
				goTo(GotoLabel.exit1900);
			}
			else {
				goTo(GotoLabel.readhRactbtc1200);
			}
		}
		wsaaIgnoreContract = "N";
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrpfx(fsupfxcpy.chdr);
		chdrlifIO.setChdrcoy(chdrcoy);
		wsaaKeyChdrcoy.set(chdrcoy);
		chdrlifIO.setChdrnum(chdrnum);
		wsaaKeyChdrnum.set(chdrnum);
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		if (isNE(chdrlifIO.getChdrpfx(),fsupfxcpy.chdr)
		|| isNE(chdrlifIO.getChdrcoy(),chdrcoy)
		|| isNE(chdrlifIO.getChdrnum(),chdrnum)) {
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		if (isNE(chdrlifIO.getValidflag(),"1")) {
			wsaaIgnoreContract = "Y";
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.exit1900);
		}
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(),1));
		chdrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			conerrrec.params.set(chdrlifIO.getParams());
			databaseError006();
		}
		contotrec.totno.set(ct05);
		contotrec.totval.set(1);
		callContot001();
		ptrnIO.setParams(SPACES);
		ptrnIO.setBatcpfx(smtpfxcpy.batc);
		ptrnIO.setBatccoy(runparmrec.company);
		ptrnIO.setBatcbrn(runparmrec.batcbranch);
		ptrnIO.setBatcactyr(runparmrec.acctyear);
		ptrnIO.setBatcactmn(runparmrec.acctmonth);
		ptrnIO.setBatctrcde(runparmrec.transcode);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setChdrcoy(chdrcoy);
		ptrnIO.setChdrnum(chdrnum);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTransactionDate(runparmrec.effdate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setPtrneff(btdate);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(runparmrec.user);
		ptrnIO.setValidflag("1");
		ptrnIO.setDatesub(runparmrec.effdate);
		ptrnIO.setCrtuser(runparmrec.user.toInternal());   //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(ptrnIO.getStatuz());
			conerrrec.params.set(ptrnIO.getParams());
			databaseError006();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(itdmIO.getStatuz());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(e308);
			databaseError006();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readhRactbtc1200()
	{
		ractbtcIO.setDataKey(SPACES);
		ractbtcIO.setChdrcoy(chdrcoy);
		ractbtcIO.setChdrnum(chdrnum);
		ractbtcIO.setLife(life);
		ractbtcIO.setCoverage(coverage);
		ractbtcIO.setRider(rider);
		ractbtcIO.setRasnum(rasnum);
		ractbtcIO.setRatype(ratype);
		ractbtcIO.setValidflag(validflag);
		ractbtcIO.setFormat(ractbtcrec);
		ractbtcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ractbtcIO);
		if (isNE(ractbtcIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(ractbtcIO.getStatuz());
			conerrrec.params.set(ractbtcIO.getParams());
			databaseError006();
		}
		if (isGT(ractbtcIO.getSingp(),ZERO)
		&& isEQ(ractbtcIO.getInstprem(),ZERO)) {
			singlePrem2100();
			goTo(GotoLabel.writeAcmv1600);
		}
		if (isEQ(ractbtcIO.getSingp(),ZERO)) {
			instalPrem2200();
			goTo(GotoLabel.writeAcmv1600);
		}
		goTo(GotoLabel.exit1900);
	}

protected void writeAcmv1600()
	{
		getCrtable4050();
		acmv014100();
		acmv024200();
		acmv034300();
		batchTotal4400();
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void fetchPrimary1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fetchRecord1110();
				}
				case eof1180: {
					eof1180();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord1110()
	{
		sqlerrorflag = false;
		try {
			if (sqlract1rs.next()) {
				appVars.getDBObject(sqlract1rs, 1, chdrcoy);
				appVars.getDBObject(sqlract1rs, 2, chdrnum);
				appVars.getDBObject(sqlract1rs, 3, life);
				appVars.getDBObject(sqlract1rs, 4, coverage);
				appVars.getDBObject(sqlract1rs, 5, rider);
				appVars.getDBObject(sqlract1rs, 6, rasnum);
				appVars.getDBObject(sqlract1rs, 7, ratype);
				appVars.getDBObject(sqlract1rs, 8, validflag);
				appVars.getDBObject(sqlract1rs, 9, rcesdte);
				appVars.getDBObject(sqlract1rs, 10, crrcd);
				appVars.getDBObject(sqlract1rs, 11, btdate);
				appVars.getDBObject(sqlract1rs, 12, tranno);
			}
			else {
				goTo(GotoLabel.eof1180);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError();
			throw new RuntimeException("B6272: ERROR (2) - Unexpected return from a transformed GO TO statement: SQL-ERROR");
		}
		goTo(GotoLabel.exit1190);
	}

protected void eof1180()
	{
		wsaaEof.set("Y");
	}

protected void closeCursor1200()
	{
		/*START*/
		appVars.freeDBConnectionIgnoreErr(sqlract1conn, sqlract1ps, sqlract1rs);
		/*EXIT*/
	}

protected void singlePrem2100()
	{
		start2110();
	}

protected void start2110()
	{
		wsaaPremiumDue.set(ractbtcIO.getSingp());
		wsaaOldBtdate.set(ractbtcIO.getBtdate());
		if (isEQ(ractbtcIO.getValidflag(),"1")) {
			ractbtcIO.setBtdate(ractbtcIO.getRiskCessDate());
			ractbtcIO.setTranno(chdrlifIO.getTranno());
		}
		if (isEQ(ractbtcIO.getValidflag(),"4")) {
			ractbtcIO.setTranno(chdrlifIO.getTranno());
			ractbtcIO.setValidflag("2");
		}
		ractbtcIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ractbtcIO);
		if (isNE(ractbtcIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(ractbtcIO.getStatuz());
			conerrrec.params.set(ractbtcIO.getParams());
			databaseError006();
		}
	}

protected void instalPrem2200()
	{
		start2210();
	}

protected void start2210()
	{
		calcBtdate6000();
		wsaaPremiumDue.set(ractbtcIO.getInstprem());
		if (isEQ(ractbtcIO.getInstprem(),ZERO)) {
			calcInstprem2250();
		}
		if (isLT(ractbtcIO.getRiskCessDate(),datcon2rec.intDate2)) {
			wsaaInstalAmount.set(wsaaPremiumDue);
			calcAmount3000();
		}
		if (isLTE(ractbtcIO.getRiskCessDate(),datcon2rec.intDate2)) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
		}
		wsaaOldBtdate.set(ractbtcIO.getBtdate());
		if (isEQ(ractbtcIO.getValidflag(),"1")) {
			ractbtcIO.setBtdate(datcon2rec.intDate2);
			ractbtcIO.setTranno(chdrlifIO.getTranno());
		}
		if (isEQ(ractbtcIO.getValidflag(),"4")) {
			ractbtcIO.setBtdate(datcon2rec.intDate2);
			ractbtcIO.setTranno(chdrlifIO.getTranno());
			ractbtcIO.setValidflag("2");
		}
		ractbtcIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ractbtcIO);
		if (isNE(ractbtcIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(ractbtcIO.getStatuz());
			conerrrec.params.set(ractbtcIO.getParams());
			databaseError006();
		}
	}

protected void calcInstprem2250()
	{
		try {
			start2251();
		}
		catch (GOTOException e){
		}
	}

protected void start2251()
	{
		lifeJlife7000();
		itemIO.setParams(SPACES);
		if (useSingleLife.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		else {
			if (useJointLife.isTrue()) {
				if (isEQ(t5687rec.jlifePresent,SPACES)) {
					itemIO.setItemitem(t5687rec.jlPremMeth);
				}
				else {
					itemIO.setItemitem(t5687rec.premmeth);
				}
			}
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemseq(SPACES);
		itemIO.setItemtabl(t5675);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		wsaaSubroutineName.set(t5675rec.premsubr);
		premiumrec.premiumRec.set(SPACES);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(ractbtcIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(ractbtcIO.getChdrnum());
		premiumrec.lifeLife.set(ractbtcIO.getLife());
		premiumrec.covrCoverage.set(ractbtcIO.getCoverage());
		premiumrec.covrRider.set(ractbtcIO.getRider());
		premiumrec.plnsfx.set(ZERO);
		premiumrec.billfreq.set(ractbtcIO.getRapayfrq());
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(ractbtcIO.getChdrcoy());
		covrIO.setChdrnum(ractbtcIO.getChdrnum());
		covrIO.setLife(ractbtcIO.getLife());
		covrIO.setCoverage(ractbtcIO.getCoverage());
		covrIO.setRider(ractbtcIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(covrIO.getStatuz());
			conerrrec.params.set(covrIO.getParams());
			databaseError006();
		}
		if (isNE(covrIO.getChdrcoy(),ractbtcIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),ractbtcIO.getChdrnum())
		|| isNE(covrIO.getLife(),ractbtcIO.getLife())
		|| isNE(covrIO.getCoverage(),ractbtcIO.getCoverage())
		|| isNE(covrIO.getRider(),ractbtcIO.getRider())
		|| isNE(covrIO.getValidflag(),"1")) {
			conerrrec.statuz.set(covrIO.getStatuz());
			conerrrec.params.set(covrIO.getParams());
			databaseError006();
		}
		premiumrec.mortcls.set(covrIO.getMortcls());
		premiumrec.mop.set(chdrlifIO.getBillchnl());
		if (useJointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.crtable.set(ractbtcIO.getRatype());
		premiumrec.currcode.set(chdrlifIO.getCntcurr());
		premiumrec.effectdt.set(chdrlifIO.getOccdate());
		premiumrec.termdate.set(ractbtcIO.getRiskCessDate());
		premiumrec.sumin.set(ractbtcIO.getRaAmount());
		premiumrec.calcPrem.set(ractbtcIO.getInstprem());
		premiumrec.ratingdate.set(chdrlifIO.getOccdate());
		premiumrec.reRateDate.set(chdrlifIO.getOccdate());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAge);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAge);
		datcon3rec.intDate1.set(ractbtcIO.getCrrcd());
		datcon3rec.intDate2.set(ractbtcIO.getRiskCessDate());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			conerrrec.statuz.set(datcon3rec.statuz);
			conerrrec.params.set(datcon3rec.datcon3Rec);
			systemError005();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		getAnny8000();
		premiumrec.language.set(runparmrec.language);
		callProgram(wsaaSubroutineName, premiumrec.premiumRec);
		if (isNE(premiumrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(premiumrec.statuz);
			conerrrec.params.set(premiumrec.premiumRec);
			systemError005();
		}
		wsaaPremiumDue.set(premiumrec.calcPrem);
	}

protected void calcAmount3000()
	{
		start3100();
	}

protected void start3100()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.frequency.set("DY");
		datcon3rec.intDate1.set(ractbtcIO.getBtdate());
		datcon3rec.intDate2.set(ractbtcIO.getRiskCessDate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			conerrrec.statuz.set(datcon3rec.statuz);
			conerrrec.params.set(datcon3rec.datcon3Rec);
			systemError005();
		}
		wsaaActualDays.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.frequency.set("DY");
		datcon3rec.intDate1.set(ractbtcIO.getBtdate());
		datcon3rec.intDate2.set(datcon2rec.intDate2);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			conerrrec.statuz.set(datcon3rec.statuz);
			conerrrec.params.set(datcon3rec.datcon3Rec);
			systemError005();
		}
		wsaaTotalDays.set(datcon3rec.freqFactor);
		compute(wsaaPremiumDue, 3).setRounded(div(mult(wsaaInstalAmount,wsaaActualDays),wsaaTotalDays));
	}

protected void getCrtable4050()
	{
		start4050();
	}

protected void start4050()
	{
		covrIO.setChdrcoy(ractbtcIO.getChdrcoy());
		covrIO.setChdrnum(ractbtcIO.getChdrnum());
		covrIO.setLife(ractbtcIO.getLife());
		covrIO.setCoverage(ractbtcIO.getCoverage());
		covrIO.setRider(ractbtcIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(covrIO.getStatuz());
			conerrrec.params.set(covrIO.getParams());
			systemError005();
		}
		lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
	}

protected void acmv014100()
	{
		try {
			start4110();
		}
		catch (GOTOException e){
		}
	}

protected void start4110()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batctrcde.set(runparmrec.transcode);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rdocnum.set(chdrnum);
		lifacmvrec.tranno.set(ptrnIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaRldgChdrnum.set(ractbtcIO.getChdrnum());
			wsaaRldgLife.set(ractbtcIO.getLife());
			wsaaRldgCoverage.set(ractbtcIO.getCoverage());
			wsaaRldgRider.set(ractbtcIO.getRider());
			wsaaPlan.set(ZERO);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
			lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
		}
		else {
			lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
		}
		lifacmvrec.acctamt.set(wsaaPremiumDue);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaPremiumDue);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.tranref.set(ptrnIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.effdate.set(wsaaOldBtdate);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.transactionDate.set(runparmrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		if (isEQ(wsaaPremiumDue,ZERO)) {
			goTo(GotoLabel.exit4190);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(lifacmvrec.statuz);
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			systemError005();
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			contotrec.totno.set(t5645rec.cnttot04);
		}
		else {
			contotrec.totno.set(t5645rec.cnttot01);
		}
		contotrec.totval.set(wsaaPremiumDue);
		callContot001();
	}

protected void acmv024200()
	{
		try {
			start4210();
		}
		catch (GOTOException e){
		}
	}

protected void start4210()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batctrcde.set(runparmrec.transcode);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rdocnum.set(chdrnum);
		lifacmvrec.tranno.set(ptrnIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.rldgacct.set(rasaIO.getAgntnum());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		calcCommission5000();
		if (isEQ(wsaaCommissionDue,0)) {
			goTo(GotoLabel.exit4290);
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaRldgChdrnum.set(ractbtcIO.getChdrnum());
			wsaaRldgLife.set(ractbtcIO.getLife());
			wsaaRldgCoverage.set(ractbtcIO.getCoverage());
			wsaaRldgRider.set(ractbtcIO.getRider());
			wsaaPlan.set(ZERO);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
			lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		}
		else {
			lifacmvrec.rldgacct.set(rasaIO.getAgntnum());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		}
		lifacmvrec.origamt.set(wsaaCommissionDue);
		lifacmvrec.acctamt.set(wsaaCommissionDue);
		lifacmvrec.tranref.set(ptrnIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.effdate.set(wsaaOldBtdate);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.transactionDate.set(runparmrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(lifacmvrec.statuz);
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			systemError005();
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			contotrec.totno.set(t5645rec.cnttot05);
		}
		else {
			contotrec.totno.set(t5645rec.cnttot02);
		}
		contotrec.totval.set(wsaaCommissionDue);
		callContot001();
	}

protected void acmv034300()
	{
		try {
			start4310();
		}
		catch (GOTOException e){
		}
	}

protected void start4310()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batctrcde.set(runparmrec.transcode);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rdocnum.set(chdrnum);
		lifacmvrec.tranno.set(ptrnIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.rldgacct.set(rasaIO.getRasnum());
		lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		compute(lifacmvrec.origamt, 2).set(sub(wsaaPremiumDue,wsaaCommissionDue));
		lifacmvrec.acctamt.set(lifacmvrec.origamt);
		lifacmvrec.tranref.set(ptrnIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.effdate.set(wsaaOldBtdate);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.transactionDate.set(runparmrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			goTo(GotoLabel.exit4390);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(lifacmvrec.statuz);
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			systemError005();
		}
		contotrec.totno.set(t5645rec.cnttot03);
		contotrec.totval.set(lifacmvrec.origamt);
		callContot001();
	}

protected void batchTotal4400()
	{
		start4410();
	}

protected void start4410()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batctrcde.set(runparmrec.transcode);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcuprec.statuz);
			conerrrec.params.set(batcuprec.batcupRec);
			systemError005();
		}
	}

protected void calcCommission5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5100();
				}
				case regularPrem5120: {
					regularPrem5120();
				}
				case accumPrems5150: {
					accumPrems5150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5100()
	{
		wsaaSingleComm.set(ZERO);
		wsaaRegularComm.set(ZERO);
		if (isEQ(wsaaLifeRead,"N")) {
			lifeJlife7000();
		}
		rasaIO.setDataArea(SPACES);
		rasaIO.setRascoy(runparmrec.company);
		rasaIO.setRasnum(ractbtcIO.getRasnum());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(rasaIO.getParams());
			databaseError006();
		}
		if (isGT(ractbtcIO.getSingp(),ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.regularPrem5120);
		}
		if (isEQ(wsaaBasscmth,SPACES)) {
			goTo(GotoLabel.accumPrems5150);
		}
		wsaaItemkey.itemItemitem.set(wsaaBasscmth);
		readT56477500();
		comlinkrec.clnkallRec.set(SPACES);
		comlinkrec.instprem.set(ractbtcIO.getSingp());
		comlinkrec.annprem.set(ractbtcIO.getSingp());
		callCommission5500();
		wsaaSingleComm.set(comlinkrec.icommtot);
		goTo(GotoLabel.accumPrems5150);
	}

protected void regularPrem5120()
	{
		if (isGT(wsaaPremiumDue,ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.accumPrems5150);
		}
		if (isEQ(wsaaBasicCommMeth,SPACES)) {
			goTo(GotoLabel.accumPrems5150);
		}
		wsaaItemkey.itemItemitem.set(wsaaBasicCommMeth);
		readT56477500();
		comlinkrec.clnkallRec.set(SPACES);
		comlinkrec.instprem.set(ractbtcIO.getInstprem());
		wsaaRapayfrq.set(rasaIO.getRapayfrq());
		comlinkrec.annprem.set(ractbtcIO.getInstprem());
		callCommission5500();
		wsaaRegularComm.set(comlinkrec.icommtot);
	}

protected void accumPrems5150()
	{
		compute(wsaaCommissionDue, 2).set(add(wsaaSingleComm,wsaaRegularComm));
		/*EXIT*/
	}

protected void callCommission5500()
	{
		start5510();
	}

protected void start5510()
	{
		comlinkrec.agent.set(rasaIO.getAgntnum());
		comlinkrec.chdrcoy.set(ractbtcIO.getChdrcoy());
		comlinkrec.chdrnum.set(ractbtcIO.getChdrnum());
		comlinkrec.life.set(ractbtcIO.getLife());
		comlinkrec.coverage.set(ractbtcIO.getCoverage());
		comlinkrec.rider.set(ractbtcIO.getRider());
		comlinkrec.planSuffix.set(ZERO);
		comlinkrec.targetPrem.set(0);
		comlinkrec.ptdate.set(0);
		comlinkrec.currto.set(0);
		if (isGT(chdrlifIO.getPolinc(),1)
		&& isEQ(chdrlifIO.getPolsum(),ZERO)) {
			comlinkrec.planSuffix.set(1);
		}
		comlinkrec.crtable.set(ractbtcIO.getRatype());
		if (useJointLife.isTrue()) {
			comlinkrec.jlife.set("01");
		}
		else {
			comlinkrec.jlife.set("00");
		}
		comlinkrec.method.set(wsaaItemkey.itemItemitem);
		comlinkrec.billfreq.set(rasaIO.getRapayfrq());
		comlinkrec.effdate.set(chdrlifIO.getPtdate());
		comlinkrec.language.set(runparmrec.language);
		comlinkrec.agentClass.set(SPACES);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		wsaaSubroutineName.set(t5647rec.commsubr);
		callProgram(wsaaSubroutineName, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(comlinkrec.statuz);
			conerrrec.params.set(comlinkrec.clnkallRec);
			systemError005();
		}
	}

protected void calcBtdate6000()
	{
		/*START*/
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(ractbtcIO.getRapayfrq());
		datcon2rec.intDate1.set(ractbtcIO.getBtdate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			conerrrec.statuz.set(datcon2rec.statuz);
			conerrrec.params.set(datcon2rec.datcon2Rec);
			systemError005();
		}
		/*EXIT*/
	}

protected void lifeJlife7000()
	{
		try {
			start7100();
		}
		catch (GOTOException e){
		}
	}

protected void start7100()
	{
		liferasIO.setChdrcoy(ractbtcIO.getChdrcoy());
		liferasIO.setChdrnum(ractbtcIO.getChdrnum());
		liferasIO.setLife(ractbtcIO.getLife());
		liferasIO.setJlife("00");
		liferasIO.setFormat(liferasrec);
		liferasIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, liferasIO);
		if (isNE(liferasIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(liferasIO.getStatuz());
			conerrrec.params.set(liferasIO.getParams());
			databaseError006();
		}
		wsaaAge.set(liferasIO.getAnbAtCcd());
		wsaaSex.set(liferasIO.getCltsex());
		liferasIO.setChdrcoy(ractbtcIO.getChdrcoy());
		liferasIO.setChdrnum(ractbtcIO.getChdrnum());
		liferasIO.setLife(ractbtcIO.getLife());
		liferasIO.setJlife("01");
		liferasIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, liferasIO);
		if (isNE(liferasIO.getStatuz(),varcom.oK)
		&& isNE(liferasIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(liferasIO.getParams());
			conerrrec.statuz.set(liferasIO.getStatuz());
			databaseError006();
		}
		if (isEQ(liferasIO.getStatuz(),varcom.mrnf)) {
			wsaaPremMethInd.set("S");
			wsbbSex.set(SPACES);
			wsbbAge.set(ZERO);
		}
		else {
			wsaaPremMethInd.set("J");
			wsbbAge.set(liferasIO.getAnbAtCcd());
			wsbbSex.set(liferasIO.getCltsex());
		}
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(ractbtcIO.getRatype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(itdmIO.getStatuz());
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),runparmrec.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),ractbtcIO.getRatype())) {
			conerrrec.statuz.set(varcom.nogo);
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		wsaaBasicCommMeth.set(t5687rec.basicCommMeth);
		wsaaBasscmth.set(t5687rec.basscmth);
		wsaaLifeRead = "Y";
		if (isNE(ractbtcIO.getRider(),"00")) {
			covrIO.setParams(SPACES);
			covrIO.setChdrcoy(ractbtcIO.getChdrcoy());
			covrIO.setChdrnum(ractbtcIO.getChdrnum());
			covrIO.setLife(ractbtcIO.getLife());
			covrIO.setCoverage(ractbtcIO.getCoverage());
			covrIO.setRider("00");
			covrIO.setPlanSuffix(ZERO);
			covrIO.setFormat(covrrec);
			covrIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(),varcom.oK)) {
				conerrrec.statuz.set(covrIO.getStatuz());
				conerrrec.params.set(covrIO.getParams());
				databaseError006();
			}
			if (isNE(covrIO.getChdrcoy(),ractbtcIO.getChdrcoy())
			|| isNE(covrIO.getChdrnum(),ractbtcIO.getChdrnum())
			|| isNE(covrIO.getLife(),ractbtcIO.getLife())
			|| isNE(covrIO.getCoverage(),ractbtcIO.getCoverage())
			|| isNE(covrIO.getRider(),"00")
			|| isNE(covrIO.getValidflag(),"1")) {
				conerrrec.statuz.set(covrIO.getStatuz());
				conerrrec.params.set(covrIO.getParams());
				databaseError006();
			}
			wsaaTr695Coverage.set(covrIO.getCrtable());
			wsaaTr695Rider.set(ractbtcIO.getRatype());
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemcoy(runparmrec.company);
			itdmIO.setItemtabl(tr695);
			itdmIO.setItemitem(wsaaTr695Key);
			itdmIO.setItmfrm(chdrlifIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				databaseError006();
			}
			if (isNE(itdmIO.getItemcoy(),runparmrec.company)
			|| isNE(itdmIO.getItemtabl(),tr695)
			|| isNE(itdmIO.getItemitem(),wsaaTr695Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				itdmIO.setStatuz(varcom.endp);
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
				wsaaTr695Rider.set("****");
				itdmIO.setDataArea(SPACES);
				itdmIO.setItemcoy(runparmrec.company);
				itdmIO.setItemtabl(tr695);
				itdmIO.setItemitem(wsaaTr695Key);
				itdmIO.setItmfrm(chdrlifIO.getOccdate());
				itdmIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
				SmartFileCode.execute(appVars, itdmIO);
				if (isNE(itdmIO.getStatuz(),varcom.oK)
				&& isNE(itdmIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(itdmIO.getStatuz());
					databaseError006();
				}
				if (isNE(itdmIO.getItemcoy(),runparmrec.company)
				|| isNE(itdmIO.getItemtabl(),tr695)
				|| isNE(itdmIO.getItemitem(),wsaaTr695Key)
				|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
					goTo(GotoLabel.exit7900);
				}
			}
			tr695rec.tr695Rec.set(itdmIO.getGenarea());
			wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
			wsaaBasscmth.set(tr695rec.basscmth);
		}
	}

protected void readT56477500()
	{
		start7510();
	}

protected void start7510()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5647);
		itemIO.setItemitem(wsaaItemkey.itemItemitem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		if (isNE(itemIO.getItemcoy(),runparmrec.company)
		|| isNE(itemIO.getItemtabl(),t5647)
		|| isNE(itemIO.getItemitem(),wsaaItemkey.itemItemitem)) {
			conerrrec.statuz.set(varcom.nogo);
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}

protected void getAnny8000()
	{
		begins8010();
	}

protected void begins8010()
	{
		annyIO.setChdrcoy(covrIO.getChdrcoy());
		annyIO.setChdrnum(covrIO.getChdrnum());
		annyIO.setLife(covrIO.getLife());
		annyIO.setCoverage(covrIO.getCoverage());
		annyIO.setRider(covrIO.getRider());
		annyIO.setPlanSuffix(covrIO.getPlanSuffix());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(annyIO.getParams());
			conerrrec.statuz.set(annyIO.getStatuz());
			databaseError006();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}
}
