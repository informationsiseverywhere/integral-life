package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:17
 * Description:
 * Copybook name: T5450REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5450rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5450Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData basicCommMeths = new FixedLengthStringData(20).isAPartOf(t5450Rec, 0);
  	public FixedLengthStringData[] basicCommMeth = FLSArrayPartOfStructure(5, 4, basicCommMeths, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(basicCommMeths, 0, FILLER_REDEFINE);
  	public FixedLengthStringData basicCommMeth01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData basicCommMeth02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData basicCommMeth03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData basicCommMeth04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData basicCommMeth05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData commrates = new FixedLengthStringData(20).isAPartOf(t5450Rec, 20);
  	public FixedLengthStringData[] commrate = FLSArrayPartOfStructure(5, 4, commrates, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(commrates, 0, FILLER_REDEFINE);
  	public FixedLengthStringData commrate01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData commrate02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData commrate03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData commrate04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData commrate05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData inlprds = new FixedLengthStringData(10).isAPartOf(t5450Rec, 40);
  	public ZonedDecimalData[] inlprd = ZDArrayPartOfStructure(5, 2, 0, inlprds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(inlprds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData inlprd01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData inlprd02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData inlprd03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData inlprd04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData inlprd05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
  	public FixedLengthStringData premsubrs = new FixedLengthStringData(35).isAPartOf(t5450Rec, 50);
  	public FixedLengthStringData[] premsubr = FLSArrayPartOfStructure(5, 7, premsubrs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(35).isAPartOf(premsubrs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData premsubr01 = new FixedLengthStringData(7).isAPartOf(filler3, 0);
  	public FixedLengthStringData premsubr02 = new FixedLengthStringData(7).isAPartOf(filler3, 7);
  	public FixedLengthStringData premsubr03 = new FixedLengthStringData(7).isAPartOf(filler3, 14);
  	public FixedLengthStringData premsubr04 = new FixedLengthStringData(7).isAPartOf(filler3, 21);
  	public FixedLengthStringData premsubr05 = new FixedLengthStringData(7).isAPartOf(filler3, 28);
  	public FixedLengthStringData rjlprems = new FixedLengthStringData(35).isAPartOf(t5450Rec, 85);
  	public FixedLengthStringData[] rjlprem = FLSArrayPartOfStructure(5, 7, rjlprems, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(35).isAPartOf(rjlprems, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rjlprem01 = new FixedLengthStringData(7).isAPartOf(filler4, 0);
  	public FixedLengthStringData rjlprem02 = new FixedLengthStringData(7).isAPartOf(filler4, 7);
  	public FixedLengthStringData rjlprem03 = new FixedLengthStringData(7).isAPartOf(filler4, 14);
  	public FixedLengthStringData rjlprem04 = new FixedLengthStringData(7).isAPartOf(filler4, 21);
  	public FixedLengthStringData rjlprem05 = new FixedLengthStringData(7).isAPartOf(filler4, 28);
  	public FixedLengthStringData rprmclss = new FixedLengthStringData(20).isAPartOf(t5450Rec, 120);
  	public FixedLengthStringData[] rprmcls = FLSArrayPartOfStructure(5, 4, rprmclss, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(20).isAPartOf(rprmclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rprmcls01 = new FixedLengthStringData(4).isAPartOf(filler5, 0);
  	public FixedLengthStringData rprmcls02 = new FixedLengthStringData(4).isAPartOf(filler5, 4);
  	public FixedLengthStringData rprmcls03 = new FixedLengthStringData(4).isAPartOf(filler5, 8);
  	public FixedLengthStringData rprmcls04 = new FixedLengthStringData(4).isAPartOf(filler5, 12);
  	public FixedLengthStringData rprmcls05 = new FixedLengthStringData(4).isAPartOf(filler5, 16);
  	public FixedLengthStringData rratfacs = new FixedLengthStringData(15).isAPartOf(t5450Rec, 140);
  	public ZonedDecimalData[] rratfac = ZDArrayPartOfStructure(5, 3, 2, rratfacs, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(15).isAPartOf(rratfacs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData rratfac01 = new ZonedDecimalData(3, 2).isAPartOf(filler6, 0);
  	public ZonedDecimalData rratfac02 = new ZonedDecimalData(3, 2).isAPartOf(filler6, 3);
  	public ZonedDecimalData rratfac03 = new ZonedDecimalData(3, 2).isAPartOf(filler6, 6);
  	public ZonedDecimalData rratfac04 = new ZonedDecimalData(3, 2).isAPartOf(filler6, 9);
  	public ZonedDecimalData rratfac05 = new ZonedDecimalData(3, 2).isAPartOf(filler6, 12);
  	public FixedLengthStringData slctrates = new FixedLengthStringData(20).isAPartOf(t5450Rec, 155);
  	public FixedLengthStringData[] slctrate = FLSArrayPartOfStructure(5, 4, slctrates, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(20).isAPartOf(slctrates, 0, FILLER_REDEFINE);
  	public FixedLengthStringData slctrate01 = new FixedLengthStringData(4).isAPartOf(filler7, 0);
  	public FixedLengthStringData slctrate02 = new FixedLengthStringData(4).isAPartOf(filler7, 4);
  	public FixedLengthStringData slctrate03 = new FixedLengthStringData(4).isAPartOf(filler7, 8);
  	public FixedLengthStringData slctrate04 = new FixedLengthStringData(4).isAPartOf(filler7, 12);
  	public FixedLengthStringData slctrate05 = new FixedLengthStringData(4).isAPartOf(filler7, 16);
  	public FixedLengthStringData ultmrates = new FixedLengthStringData(20).isAPartOf(t5450Rec, 175);
  	public FixedLengthStringData[] ultmrate = FLSArrayPartOfStructure(5, 4, ultmrates, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(20).isAPartOf(ultmrates, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ultmrate01 = new FixedLengthStringData(4).isAPartOf(filler8, 0);
  	public FixedLengthStringData ultmrate02 = new FixedLengthStringData(4).isAPartOf(filler8, 4);
  	public FixedLengthStringData ultmrate03 = new FixedLengthStringData(4).isAPartOf(filler8, 8);
  	public FixedLengthStringData ultmrate04 = new FixedLengthStringData(4).isAPartOf(filler8, 12);
  	public FixedLengthStringData ultmrate05 = new FixedLengthStringData(4).isAPartOf(filler8, 16);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(305).isAPartOf(t5450Rec, 195, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5450Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5450Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}