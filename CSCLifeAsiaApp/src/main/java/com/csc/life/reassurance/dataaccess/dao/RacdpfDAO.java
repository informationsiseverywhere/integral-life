package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.reassurance.dataaccess.model.B5464DTO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RacdpfDAO extends BaseDAO<Racdpf> {

	public List<B5464DTO> findRacdResult(String wsaaChdrnumfrm,
			String wsaaChdrnumto, int wsaaSqlEffdate, int minRecord,
			int maxRecord);

	public void deleteRacdRcds(List<Racdpf> list);
		
	
    public void insertRacdBulk(List<Racdpf> racdrskBulkOpList);
    public void updateRacdBulk(List<Racdpf> racdrskBulkOpList, int busDate);
	public List<Racdpf> searchRacdResult(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx);
	public List<Racdpf> readRacdRecord(Racdpf racdpf);
	public List<Racdpf> searchRacdRecord(String coy, String chdrnum);
	
	public Map<String, List<Racdpf>> searchRacdlnbRecord(List<String> chdrnumList);
	//ILIFE-6809
	public List<Racdpf> searchRacdmjaResult(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx);
	public List<Racdpf> searchRacdstRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider, int plnsfx, String validflag);
	public Racdpf getRacdbySeqNo(String chdrcoy,String chdrnum,int seqNo);
	public Racdpf getRacdbySeqNo(String chdrcoy, String chdrnum, int seqNo, String life, String coverage, String rider, int plnsfx);
	public void insertBulkRacd(List<Racdpf> racdList);
	public List<Racdpf> getRacdRecord(Racdpf racdpf);
	public int getRacdCntByType(Racdpf racdpf);
	public List<Racdpf> getRacdFaclRecord(Racdpf racdpf);
	public int updateRacd(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx,int rrtdat,int rrtfrm , String validflag);

	public void updateRacdPendcsttoProrateflagBulk(List<Racdpf> racdrskBulkOpList);
}
