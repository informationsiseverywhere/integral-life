package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:41
 * Description:
 * Copybook name: RECOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Recokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData recoFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData recoKey = new FixedLengthStringData(64).isAPartOf(recoFileKey, 0, REDEFINE);
  	public FixedLengthStringData recoChdrcoy = new FixedLengthStringData(1).isAPartOf(recoKey, 0);
  	public FixedLengthStringData recoChdrnum = new FixedLengthStringData(8).isAPartOf(recoKey, 1);
  	public FixedLengthStringData recoLife = new FixedLengthStringData(2).isAPartOf(recoKey, 9);
  	public FixedLengthStringData recoCoverage = new FixedLengthStringData(2).isAPartOf(recoKey, 11);
  	public FixedLengthStringData recoRider = new FixedLengthStringData(2).isAPartOf(recoKey, 13);
  	public PackedDecimalData recoPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(recoKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(recoKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(recoFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		recoFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}