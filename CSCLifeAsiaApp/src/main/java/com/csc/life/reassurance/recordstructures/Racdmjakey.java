package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:20
 * Description:
 * Copybook name: RACDMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdmjaKey = new FixedLengthStringData(64).isAPartOf(racdmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(racdmjaKey, 0);
  	public FixedLengthStringData racdmjaChdrnum = new FixedLengthStringData(8).isAPartOf(racdmjaKey, 1);
  	public FixedLengthStringData racdmjaLife = new FixedLengthStringData(2).isAPartOf(racdmjaKey, 9);
  	public FixedLengthStringData racdmjaCoverage = new FixedLengthStringData(2).isAPartOf(racdmjaKey, 11);
  	public FixedLengthStringData racdmjaRider = new FixedLengthStringData(2).isAPartOf(racdmjaKey, 13);
  	public PackedDecimalData racdmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdmjaKey, 15);
  	public PackedDecimalData racdmjaSeqno = new PackedDecimalData(2, 0).isAPartOf(racdmjaKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(racdmjaKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}