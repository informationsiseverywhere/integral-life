package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5473.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5473Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37);
	private FixedLengthStringData crrcd = new FixedLengthStringData(10);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10);
	private FixedLengthStringData dateto = new FixedLengthStringData(10);
	private FixedLengthStringData lifcnum = new FixedLengthStringData(8);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData ptrneff = new FixedLengthStringData(10);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private ZonedDecimalData recovamt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5473Report() {
		super();
	}


	/**
	 * Print the XML for R5473d01
	 */
	public void printR5473d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		lifcnum.setFieldName("lifcnum");
		lifcnum.setInternal(subString(recordData, 9, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 17, 37));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 54, 4));
		crrcd.setFieldName("crrcd");
		crrcd.setInternal(subString(recordData, 58, 10));
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 68, 3));
		recovamt.setFieldName("recovamt");
		recovamt.setInternal(subString(recordData, 71, 17));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 88, 30));
		ptrneff.setFieldName("ptrneff");
		ptrneff.setInternal(subString(recordData, 118, 10));
		printLayout("R5473d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				lifcnum,
				clntnm,
				crtable,
				crrcd,
				currcode,
				recovamt,
				longdesc,
				ptrneff
			}
		);

	}

	/**
	 * Print the XML for R5473h01
	 */
	public void printR5473h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 1, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 9, 37));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 46, 10));
		datefrm.setFieldName("datefrm");
		datefrm.setInternal(subString(recordData, 56, 10));
		dateto.setFieldName("dateto");
		dateto.setInternal(subString(recordData, 66, 10));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5473h01",			// Record name
			new BaseData[]{			// Fields:
				rasnum,
				clntnm,
				sdate,
				datefrm,
				dateto,
				pagnbr
			}
		);

		currentPrintLine.set(10);
	}


}
