package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;						 
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.reassurance.dataaccess.model.Acmvagt;
import com.csc.life.reassurance.dataaccess.model.Acmxpf;
import com.csc.life.reassurance.dataaccess.model.B5470DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.quipoz.COBOLFramework.database.QPBaseDataSource;
import com.quipoz.COBOLFramework.util.StringUtil;												 
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class AcmvpfDAOImpl extends BaseDAOImpl<Acmvpf> implements AcmvpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(AcmvpfDAOImpl.class);
 
	@Override
	public List<B5470DTO> searchAcmvByRasNumResult(String batccoy,String sacscode, String effdate, int min_record, int max_record) {
		StringBuilder sql = new StringBuilder();
		sql.append("select aa.* from (");
		sql.append(" select  ROW_NUMBER() over(order by a.rldgacct, a.unique_number) rownm,a.rldgacct, a.tranno, a.sacscode, a.sacstyp, a.batccoy, a.batcbrn, a.batcactyr, a.batcactmn, a.batctrcde,  ")
		.append( "a.batcbatch, a.origcurr, a.origamt, a.effdate, a.rdocnum,a.jrnseq, a.glsign, a.trandesc,ra.rascoy,ra.rasnum,ra.currcode,ra.minsta,ra.clntcoy,ra.payclt" )
		.append(",cl.cltaddr01,cl.cltaddr02,cl.cltaddr03,cl.cltaddr04,cl.cltpcode, cl.clttype,cl.surname,cl.givname,cl.salutl,cl.lsurname,cl.lgivname,cl.ethorig ")
		.append(" from acmvpf a left join rasapf ra on ra.rasnum=a.rldgacct and ra.rascoy = a.batccoy left join clts cl on cl.clntnum=ra.payclt and cl.clntcoy=ra.clntcoy")
		.append( " where a.frcdate = 99999999 and a.intextind = 'E' and  cl.clntpfx='CN' and a.batccoy = ? and a.sacscode = ? and a.effdate <= ?")
		//ILife-3009
	//	.append("order by a.rldgacct, a.origcurr, a.effdate, a.sacstyp")
		.append(")aa where aa.rownm>=? and aa.rownm < ?");
		 PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        List<B5470DTO> acmvtoDtoList = null;
		try {
			ps.setString(1, batccoy);
			ps.setString(2, sacscode);
			ps.setString(3, effdate);
			ps.setInt(4, min_record);
			ps.setInt(5, max_record);
			rs = executeQuery(ps);
			acmvtoDtoList = new ArrayList<B5470DTO>();
			while(rs.next()){
				 B5470DTO acmvSearchResultDto = new B5470DTO();
				 acmvSearchResultDto.setSqlRldgacct(rs.getString(2));
				 acmvSearchResultDto.setSqlTranno(rs.getInt(3));
				 acmvSearchResultDto.setSqlSacscode(rs.getString(4));
				 acmvSearchResultDto.setSqlSacstyp(rs.getString(5));
				 acmvSearchResultDto.setSqlBatccoy(rs.getString(6));
				 acmvSearchResultDto.setSqlBatcbrn(rs.getString(7));
				 acmvSearchResultDto.setSqlBatcactyr(rs.getInt(8));
				 acmvSearchResultDto.setSqlBatcactmn(rs.getInt(9));
				 acmvSearchResultDto.setSqlBatctrcde(rs.getString(10));
				 acmvSearchResultDto.setSqlBatcbatch(rs.getString(11));
				 acmvSearchResultDto.setSqlOrigcurr(rs.getString(12));
				 acmvSearchResultDto.setSqlOrigamt(rs.getBigDecimal(13));
				 acmvSearchResultDto.setSqlEffdate(rs.getInt(14));
				 acmvSearchResultDto.setSqlRdocnum(rs.getString(15));
				 acmvSearchResultDto.setSqlJrnseq(rs.getInt(16));
				 acmvSearchResultDto.setSqlGlsign(rs.getString(17));
				 acmvSearchResultDto.setSqlTrandesc(rs.getString(18));
				 acmvSearchResultDto.setSqlRascoy(rs.getString(19));
				 acmvSearchResultDto.setSqlRasnum(rs.getString(20));
				 acmvSearchResultDto.setSqlCurrcode(rs.getString(21));
				 acmvSearchResultDto.setSqlMinsta(rs.getBigDecimal(22));
				 acmvSearchResultDto.setSqlClntcoy(rs.getString(23));
				 acmvSearchResultDto.setSqlPayclt(rs.getString(24));
				 acmvSearchResultDto.setSqlCltaddr01(rs.getString(25));
				 acmvSearchResultDto.setSqlCltaddr02(rs.getString(26));
				 acmvSearchResultDto.setSqlCltaddr03(rs.getString(27));
				 acmvSearchResultDto.setSqlCltaddr04(rs.getString(28));
				 
				 acmvSearchResultDto.setSqlCltpcode(rs.getString(29));
				 acmvSearchResultDto.setSqlClttype(rs.getString(30));
				 acmvSearchResultDto.setSqlSurname(rs.getString(31));
				 acmvSearchResultDto.setSqlGivname(rs.getString(32));
				 acmvSearchResultDto.setSqlSalutl(rs.getString(33));
				 acmvSearchResultDto.setSqlLsurname(rs.getString(34));
				 acmvSearchResultDto.setSqlLgivname(rs.getString(35));
				 acmvSearchResultDto.setSqlEthorig(rs.getString(36));
				 acmvtoDtoList.add(acmvSearchResultDto);
			}
		} catch (SQLException e) {
            LOGGER.error("deleteAcmxpfBulk()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
		return acmvtoDtoList;
	}

	@Override
	public void insertAcmxBulk(List<B5470DTO> acmvBulkList) {
		if (acmvBulkList != null && acmvBulkList.size() > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("insert into acmxpf(BATCCOY,SACSCODE,EFFDATE,RLDGACCT,ORIGCURR,SACSTYP,TRANNO,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,RDOCNUM,JRNSEQ,ORIGAMT,GLSIGN,USRPRF,JOBNM,DATIME) ")
			.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			 PreparedStatement ps = getPrepareStatement(sb.toString());
			 try {
	                for (B5470DTO p : acmvBulkList) {
	                	ps.setString(1, p.getSqlBatccoy());
	                	ps.setString(2, p.getSqlSacscode());
	                	ps.setInt(3, p.getSqlEffdate());
	                	ps.setString(4, p.getSqlRldgacct());
	                	ps.setString(5, p.getSqlOrigcurr());
	                	ps.setString(6, p.getSqlSacstyp());
	                	ps.setInt(7, p.getSqlTranno());
	                	ps.setString(8, p.getSqlBatcbrn());
	                	ps.setInt(9, p.getSqlBatcactyr());
	                	ps.setInt(10, p.getSqlBatcactmn());
	                	ps.setString(11, p.getSqlBatctrcde());
	                	ps.setString(12, p.getSqlBatcbatch());
	                	ps.setString(13, p.getSqlRdocnum());
	                	ps.setInt(14, p.getSqlJrnseq());
	                	ps.setBigDecimal(15, p.getSqlOrigamt());
	                	ps.setString(16, p.getSqlGlsign());
	                	ps.setString(17, getUsrprf());
	                    ps.setString(18, getJobnm());
	                    ps.setTimestamp(19, new Timestamp(System.currentTimeMillis()));
	                    ps.addBatch();
	                }
	                ps.executeBatch();
			}catch (SQLException e) {
	            LOGGER.error("insertAcmxBulk()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        
			}

		
	}
}

	@Override
	public void updateAcmvBulk(List<Acmvagt> acmvBulkList) {
		if (acmvBulkList != null && acmvBulkList.size() > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("update acmvpf set Frcdate=? ,Rcamt=? where unique_number=? and Frcdate = '99999999'");
		
			PreparedStatement ps = getPrepareStatement(sb.toString());
			 try {
	               for (Acmvagt p : acmvBulkList) {
	               		ps.setInt(1, p.getFrcdate());
		               	ps.setDouble(2, p.getRcamt());
		               	ps.setLong(3, p.getUniqueNumber());
		               	ps.addBatch();
		               	
	               }
	               ps.executeBatch();
			}catch (SQLException e) {
	           LOGGER.error("updateAcmxBulk()", e);//IJTI-1485
	           throw new SQLRuntimeException(e);
	       } finally {
	           close(ps, null);
	       
			}
		}
	}
	
	public Acmvagt searchAcmvByAxmcResult(Acmxpf acmxpf){
		StringBuilder sql = new StringBuilder();
		sql.append("select * from acmvagt")
			.append(" where  Batccoy=? and Batcbrn=? and Batcactyr = ? and batcactmn=? and Batctrcde=? and Batcbatch=? and ")
			.append("Rldgacct=? and Origcurr=? and Sacscode=? and Sacstyp=? and Tranno=? and Rdocnum=? and Jrnseq=?");
		 PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        Acmvagt result = null;
		try {
			ps.setString(1,acmxpf.getBatccoy());
			ps.setString(2,acmxpf.getBatcbrn());
			ps.setInt(3,acmxpf.getBatcactyr());
			ps.setInt(4,acmxpf.getBatcactmn());
			ps.setString(5,acmxpf.getBatctrcde());
			ps.setString(6,acmxpf.getBatcbatch());
			ps.setString(7,acmxpf.getRldgacct());
			ps.setString(8,acmxpf.getOrigcurr());
			ps.setString(9,acmxpf.getSacscode());
			ps.setString(10,acmxpf.getSacstyp());
			ps.setInt(11,acmxpf.getTranno());
			ps.setString(12,acmxpf.getRdocnum());
			ps.setInt(13,acmxpf.getJrnseq());
			
			rs = executeQuery(ps);
			while(rs.next()){
				result = new Acmvagt();
				result.setUniqueNumber(rs.getLong(1));
				result.setRldgcoy(rs.getString("RLDGCOY"));
				result.setSacscode(rs.getString("SACSCODE"));
				result.setRldgacct(rs.getString("RLDGACCT"));
				result.setOrigcurr(rs.getString("ORIGCURR"));
				result.setSacstyp(rs.getString("SACSTYP"));
				result.setBatccoy(rs.getString("BATCCOY"));
				result.setBatcbrn(rs.getString("BATCBRN"));
				result.setBatcactyr(rs.getInt("BATCACTYR"));
				result.setBatcactmn(rs.getInt("BATCACTMN"));
				result.setBatctrcde(rs.getString("BATCTRCDE"));
				result.setBatcbatch(rs.getString("BATCBATCH"));
				result.setRdocnum(rs.getString("RDOCNUM"));
				result.setTranno(rs.getInt("TRANNO"));
				result.setJrnseq(rs.getInt("JRNSEQ"));
				result.setOrigamt(rs.getDouble("ORIGAMT"));
				result.setTranref(rs.getString("TRANREF"));
				result.setTrandesc(rs.getString("TRANDESC"));
				result.setCrate(rs.getDouble("CRATE"));
				result.setAcctamt(rs.getInt("ACCTAMT"));
				result.setGenlcoy(rs.getString("GENLCOY"));
				result.setGenlcur(rs.getString("GENLCUR"));
				result.setGlcode(rs.getString("GLCODE"));
				result.setGlsign(rs.getString("GLSIGN"));
				result.setPostyear(rs.getString("POSTYEAR"));
				result.setEffdate(rs.getInt("EFFDATE"));
				result.setRcamt(rs.getDouble("RCAMT"));
				result.setFrcdate(rs.getInt("FRCDATE"));
				result.setSuprflg(rs.getString("SUPRFLG"));
				
				result.setTrdt(rs.getInt("TRDT"));
				result.setTrtm(rs.getInt("TRTM"));
				result.setUserT(rs.getInt("USER_T"));
				result.setTermid(rs.getString("TERMID"));
				result.setUsrprf(rs.getString("USRPRF"));
				result.setJobnm(rs.getString("JOBNM"));
				result.setDatime(rs.getString("DATIME"));
			}
		} catch (SQLException e) {
            LOGGER.error("deleteAcmxpfBulk()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
		return result;
	}
	
	public List<Acmvpf> searchAcmvpfRecord(Acmvpf acmvpf)
	 {
		 List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
		 StringBuilder sb = new StringBuilder("");
		 
		 sb.append("SELECT RLDGACCT,RLDGCOY,SACSCODE,SACSTYP,ORIGCURR,ORIGAMT,EFFDATE,RDOCNUM,FRCDATE,ACCTAMT FROM ACMVPF WHERE RLDGCOY = ? AND SACSCODE = ? AND RLDGACCT = ? AND SACSTYP = ?  ");	
		 LOGGER.info(sb.toString());		
		 PreparedStatement ps = null;
		 ResultSet rs = null;	
		 try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1,acmvpf.getRldgcoy().toString());
				ps.setString(2,acmvpf.getSacscode());
				ps.setString(3,acmvpf.getRldgacct());
				ps.setString(4,acmvpf.getSacstyp());						
				
				
				rs = executeQuery(ps);
				
				while(rs.next()){
					Acmvpf acmvpfLis = new Acmvpf();
					 acmvpfLis.setRldgacct(rs.getString(1));	
					 acmvpfLis.setRldgcoy(rs.getString(2).charAt(0));
					 acmvpfLis.setSacscode(rs.getString(3));
					 acmvpfLis.setSacstyp(rs.getString(4));					
					 acmvpfLis.setOrigcurr(rs.getString(5));
					 acmvpfLis.setOrigamt(rs.getBigDecimal(6));
					 acmvpfLis.setEffdate(rs.getInt(7));
					 acmvpfLis.setRdocnum(rs.getString(8));					 
					 acmvpfLis.setFrcdate(rs.getInt(9));
					 acmvpfLis.setAcctamt(rs.getBigDecimal(10)); //ILIFE-7700
					 acmvpfList.add(acmvpfLis);
					 
				}
				
				
		 }
		 catch (SQLException e) {
				LOGGER.error("searchAcmvpfRecord()",e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
		 return acmvpfList;
		 
	 }
	
	public List<Acmvpf> searchAcmvpfRecordbyTranno(Acmvpf acmvpf)
	 {
		 List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
		 String dbType = IntegralDBProperties.getType().trim();
		 StringBuilder sb = new StringBuilder("");
		 
		 if (QPBaseDataSource.DATABASE_ORACLE.equals(dbType))
			 sb.append("SELECT RLDGACCT,RLDGCOY,SACSCODE,SACSTYP,ORIGCURR,ORIGAMT,EFFDATE,RDOCNUM,FRCDATE,GLSIGN,TRANNO,BATCCOY,TRANDESC,CRATE,ACCTAMT,RCAMT,GENLCUR,GENLCOY,POSTMONTH,POSTYEAR,GLCODE,TRANREF,ORIGAMT,BATCACTMN,BATCACTYR,BATCBATCH,BATCBRN,BATCTRCDE,TERMID FROM ACMVPF WHERE TRIM(RLDGCOY) = ? AND TRIM(SACSCODE) = ? AND (TRIM(RLDGACCT) like ? AND TRIM(RDOCNUM) = ?) AND TRIM(SACSTYP) = ? AND TRIM(TRANNO) = ? ");	
		 else
			 sb.append("SELECT RLDGACCT,RLDGCOY,SACSCODE,SACSTYP,ORIGCURR,ORIGAMT,EFFDATE,RDOCNUM,FRCDATE,GLSIGN,TRANNO,BATCCOY,TRANDESC,CRATE,ACCTAMT,"
				 		+ "RCAMT,GENLCUR,GENLCOY,POSTMONTH,POSTYEAR,GLCODE,TRANREF,ORIGAMT,BATCACTMN,BATCACTYR,BATCBATCH,BATCBRN,BATCTRCDE,TERMID FROM ACMVPF"
				 		+ " WHERE RLDGCOY = ? AND SACSCODE = ? AND (RLDGACCT like ? and RDOCNUM=?)  AND SACSTYP = ?  AND TRANNO = ?");
		 
		 LOGGER.info(sb.toString());		
		 PreparedStatement ps = null;
		 ResultSet rs = null;	
		 try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1,acmvpf.getRldgcoy().toString());
				ps.setString(2,acmvpf.getSacscode());
				ps.setString(3,acmvpf.getRldgacct()+"%");
				ps.setString(4,acmvpf.getRdocnum());
				ps.setString(5,acmvpf.getSacstyp());
				ps.setInt(6,acmvpf.getTranno());
				
				rs = executeQuery(ps);
				
				while(rs.next()){
					Acmvpf acmvpfLis = new Acmvpf();
					acmvpfLis.setRldgacct(rs.getString(1));	
					 acmvpfLis.setRldgcoy(rs.getString(2).charAt(0));
					 acmvpfLis.setSacscode(rs.getString(3));
					 acmvpfLis.setSacstyp(rs.getString(4));					
					 acmvpfLis.setOrigcurr(rs.getString(5));
					 acmvpfLis.setOrigamt(rs.getBigDecimal(6));
					 acmvpfLis.setEffdate(rs.getInt(7));
					 acmvpfLis.setRdocnum(rs.getString(8));					 
					 acmvpfLis.setFrcdate(rs.getInt(9));
					 acmvpfLis.setGlsign(rs.getString(10).charAt(0));
					 acmvpfLis.setTranno(rs.getInt(11));
					 acmvpfLis.setBatccoy(rs.getString(12).charAt(0));
					 acmvpfLis.setTrandesc(rs.getString(13));
					 acmvpfLis.setCrate(rs.getBigDecimal(14));
					 acmvpfLis.setAcctamt(rs.getBigDecimal(15));
					 acmvpfLis.setRcamt(rs.getBigDecimal(16));
					 acmvpfLis.setGenlcur(rs.getString(17));
					 acmvpfLis.setGenlcoy(rs.getString(18).charAt(0));
					 acmvpfLis.setPostmonth(rs.getString(19));
					 acmvpfLis.setPostyear(rs.getString(20));
					 acmvpfLis.setGlcode(rs.getString(21));
					 acmvpfLis.setTranref(rs.getString(22));
					 acmvpfLis.setOrigamt(rs.getBigDecimal(23));
					 acmvpfLis.setBatcactmn(rs.getByte(24));
					 acmvpfLis.setBatcactyr(rs.getShort(25));
					 acmvpfLis.setBatcbatch(rs.getString(26));
					 acmvpfLis.setBatcbrn(rs.getString(27));
					 acmvpfLis.setBatctrcde(rs.getString(28));
					 acmvpfLis.setTermid(rs.getString(29));
					 
					 
					 acmvpfList.add(acmvpfLis);
				}	
				
		 }
		 catch (SQLException e) {
				LOGGER.error("searchAcmvpfRecord()",e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
		 return acmvpfList;
		 
	 }
	
	
	public List<Acmvpf> ReadSurrender100(Acmvpf acmvpf)
	 {
		 List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
	     String dbType = IntegralDBProperties.getType().trim();    //ILIFE-6627 //IJTI-449     
		 StringBuilder sb = new StringBuilder("");
		//ILIFE-6627
		 if (QPBaseDataSource.DATABASE_ORACLE.equals(dbType))
			 sb.append("SELECT RLDGACCT,RLDGCOY,SACSCODE,SACSTYP,ORIGCURR,ORIGAMT,EFFDATE,RDOCNUM,FRCDATE,BATCTRCDE FROM ACMVPF WHERE TRIM(RLDGCOY) = ? AND TRIM(SACSCODE) = ? AND TRIM(RDOCNUM) = ? AND TRIM(SACSTYP) = ?  ");	
		 else
			 sb.append("SELECT RLDGACCT,RLDGCOY,SACSCODE,SACSTYP,ORIGCURR,ORIGAMT,EFFDATE,RDOCNUM,FRCDATE,BATCTRCDE FROM ACMVPF WHERE RLDGCOY = ? AND SACSCODE = ? AND RDOCNUM = ? AND SACSTYP = ?  ");	
		//ILIFE-6627
		 LOGGER.info(sb.toString());		
		 PreparedStatement ps = null;
		 ResultSet rs = null;	
		 try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1,acmvpf.getRldgcoy().toString());
				ps.setString(2,acmvpf.getSacscode());
				ps.setString(3,acmvpf.getRdocnum());
				ps.setString(4,acmvpf.getSacstyp());						
				
				
				rs = executeQuery(ps);
				
				while(rs.next()){
					Acmvpf acmvpfLis = new Acmvpf();
					 acmvpfLis.setRldgacct(rs.getString(1));	
					 acmvpfLis.setRldgcoy(rs.getString(2).charAt(0));
					 acmvpfLis.setSacscode(rs.getString(3));
					 acmvpfLis.setSacstyp(rs.getString(4));					
					 acmvpfLis.setOrigcurr(rs.getString(5));
					 acmvpfLis.setOrigamt(rs.getBigDecimal(6));
					 acmvpfLis.setEffdate(rs.getInt(7));
					 acmvpfLis.setRdocnum(rs.getString(8));					 
					 acmvpfLis.setFrcdate(rs.getInt(9));
					 acmvpfLis.setBatctrcde(rs.getString(10));
					 acmvpfList.add(acmvpfLis);
					 
				}
				
				
		 }
		 catch (SQLException e) {
				LOGGER.error("searchAcmvpfRecord()",e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
		 return acmvpfList;
		 
	 }	
public List<Acmvpf> ReadAcmvpf(Acmvpf acmvpf)
	 {
		 List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
		 StringBuilder sb = new StringBuilder("");
		 
		 sb.append("SELECT RLDGACCT,RLDGCOY,SACSCODE,SACSTYP,ORIGCURR,ORIGAMT,EFFDATE,RDOCNUM,FRCDATE,BATCTRCDE FROM ACMVPF WHERE RLDGCOY = ? AND RDOCNUM = ?  AND SACSCODE=? AND SACSTYP=? ");	
		 LOGGER.info(sb.toString());		
		 PreparedStatement ps = null;
		 ResultSet rs = null;	
		 try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1,acmvpf.getRldgcoy().toString());				
				ps.setString(2,acmvpf.getRdocnum());
				ps.setString(3,acmvpf.getSacscode());						
				ps.setString(4,acmvpf.getSacstyp());
				
				
				rs = executeQuery(ps);
				
				while(rs.next())
				{
					Acmvpf acmvpfLis = new Acmvpf();
					 acmvpfLis.setRldgacct(rs.getString(1));
					 acmvpfLis.setSacscode(rs.getString(3));
					 acmvpfLis.setSacstyp(rs.getString(4));					
					 acmvpfLis.setOrigcurr(rs.getString(5));
					 acmvpfLis.setOrigamt(rs.getBigDecimal(6));
					 acmvpfLis.setEffdate(rs.getInt(7));
					 acmvpfLis.setRdocnum(rs.getString(8));					 
					 acmvpfLis.setFrcdate(rs.getInt(9));
					 acmvpfLis.setBatctrcde(rs.getString(10));
					 acmvpfList.add(acmvpfLis);					 
				}
				
				
		 }
		 catch (SQLException e) {
				LOGGER.error("searchAcmvpfRecord()",e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
		 return acmvpfList;
		 
	 }	

	public Map<Integer, Acmvpf> searchAcmvpfMap(String coy, String chdrnum) {

		Map<Integer, Acmvpf> acmvpfMap = new HashMap<>();
		//ILB-508 Start by dpuhawan
		//String sql = "SELECT UNIQUE_NUMBER,TRANNO FROM ACMVpf WHERE RLDGCOY = ? AND LTRIM(RTRIM(RDOCNUM)) = ? ";
		String sql = "SELECT UNIQUE_NUMBER,TRANNO FROM ACMVpf WHERE RLDGCOY = ? AND RDOCNUM = ? ";
		//ILB-508 End   
		try (PreparedStatement ps = getPrepareStatement(sql)) {
			ps.setString(1, coy);
			//ILB-508 Start by dpuhawan
			//ps.setString(2, chdrnum.trim());
			ps.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.rdocnum.length)); 
			//ILB-508 End
			ResultSet rs = executeQuery(ps);

			while (rs.next()) {
				Acmvpf result = new Acmvpf();
				result.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				result.setTranno(rs.getInt("TRANNO"));
				acmvpfMap.put(result.getTranno(), result);
			}
		} catch (SQLException e) {
			LOGGER.error("searchAcmvpfMap()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		}
		return acmvpfMap;
	}
}