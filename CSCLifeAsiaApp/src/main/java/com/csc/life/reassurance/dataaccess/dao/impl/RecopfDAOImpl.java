package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.RecopfDAO;
import com.csc.life.reassurance.dataaccess.model.Recopf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RecopfDAOImpl extends BaseDAOImpl<Recopf> implements RecopfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RecopfDAOImpl.class);
	
	public int insertRecopf(Recopf recopf) {
      StringBuilder sql = new StringBuilder("INSERT INTO RECOPF(");
      sql.append("CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,");
      sql.append("RASNUM,SEQNO,VALIDFLAG,COSTDATE,RETYPE,RNGMNT,");
      sql.append("SRARAMT,RAAMOUNT,CTDATE,ORIGCURR,PREM,COMPAY,");
      sql.append("TAXAMT,REFUNDFE,BATCCOY,BATCBRN,BATCACTYR,");
      sql.append("BATCACTMN,BATCTRCDE,BATCBATCH,TRANNO,");
      sql.append("RCSTFRQ,USRPRF,JOBNM,DATIME,RPRate,ANNPREM) VALUES(?,?,?,?,?,?,");
      sql.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      PreparedStatement ps = getPrepareStatement(sql.toString());
      int result = 0;
	  
      try {
	       ps.setString(1, recopf.getChdrcoy());
           ps.setString(2, recopf.getChdrnum());
           ps.setString(3, recopf.getLife());
           ps.setString(4, recopf.getCoverage());
           ps.setString(5, recopf.getRider());
           ps.setInt(6, recopf.getPlnsfx());
           ps.setString(7, recopf.getRasnum());
           ps.setInt(8, recopf.getSeqno());
           ps.setString(9, recopf.getValidflag());
           ps.setInt(10, recopf.getCostdate());
           ps.setString(11, recopf.getRetype());
           ps.setString(12, recopf.getRngmnt());
           ps.setBigDecimal(13, recopf.getSraramt());
           ps.setBigDecimal(14, recopf.getRaAmount());
           ps.setInt(15, recopf.getCtdate());
           ps.setString(16, recopf.getOrigcurr());
           ps.setBigDecimal(17, recopf.getPrem());
           ps.setBigDecimal(18, recopf.getCompay());
           ps.setBigDecimal(19, recopf.getTaxamt());
           ps.setBigDecimal(20, recopf.getRefundfe());
           ps.setString(21, recopf.getBatccoy());
           ps.setString(22, recopf.getBatcbrn());
           ps.setInt(23, recopf.getBatcactyr());
           ps.setInt(24, recopf.getBatcactmn());
           ps.setString(25, recopf.getBatctrcde());
           ps.setString(26, recopf.getBatcbatch());
           ps.setInt(27, recopf.getTranno());
           ps.setString(28, recopf.getRcstfrq());
           ps.setString(29, getUsrprf());
           ps.setString(30, getJobnm());
           ps.setTimestamp(31, new Timestamp(System.currentTimeMillis()));
           ps.setBigDecimal(32,recopf.getRPRate());
		   ps.setBigDecimal(33,recopf.getAnnprem());
           result = ps.executeUpdate();
	   } catch (SQLException e) {
		   LOGGER.error("insertRecopf()", e); /* IJTI-1479 */
	       throw new SQLRuntimeException(e);
	   } finally {
	       close(ps, null);
	   }
      
      return result;
	        
  }

   public Recopf searchRecopfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
            int plnsfx, String seqno, String ctdate) {
        StringBuilder sqlRecoSelect1 = new StringBuilder("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,");
        sqlRecoSelect1.append("RASNUM,SEQNO,VALIDFLAG,COSTDATE,RETYPE,RNGMNT,SRARAMT,RAAMOUNT,CTDATE,ORIGCURR,PREM,COMPAY,");
        sqlRecoSelect1.append("TAXAMT,REFUNDFE,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,TRANNO,RCSTFRQ,USRPRF,JOBNM,ANNPREM");
        sqlRecoSelect1.append(" FROM RECOPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX=? AND SEQNO =? AND CTDATE =? AND VALIDFLAG ='1' ");
        sqlRecoSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, RASNUM ASC, TRANNO DESC,  UNIQUE_NUMBER DESC ");

        PreparedStatement psRecoSelect = getPrepareStatement(sqlRecoSelect1.toString());
        ResultSet sqlRecopf1rs = null;
        Recopf recopf = null;
        try {
        	psRecoSelect.setString(1, chdrcoy);
        	psRecoSelect.setString(2, chdrnum);
        	psRecoSelect.setString(3, life);
        	psRecoSelect.setString(4, coverage);
        	psRecoSelect.setString(5, rider);
        	psRecoSelect.setInt(6, plnsfx);
        	psRecoSelect.setString(7, seqno);
        	psRecoSelect.setString(8, ctdate);
        	
        	sqlRecopf1rs = executeQuery(psRecoSelect);
            if (sqlRecopf1rs.next()) {
            	recopf = new Recopf();
            	recopf.setChdrcoy(sqlRecopf1rs.getString("Chdrcoy"));
            	recopf.setChdrnum(sqlRecopf1rs.getString("Chdrnum"));
            	recopf.setLife(sqlRecopf1rs.getString("Life"));
                recopf.setCoverage(sqlRecopf1rs.getString("Coverage"));
                recopf.setRider(sqlRecopf1rs.getString("Rider"));
                recopf.setPlnsfx(sqlRecopf1rs.getInt("Plnsfx"));
                recopf.setRasnum(sqlRecopf1rs.getString("Rasnum"));
                recopf.setSeqno(sqlRecopf1rs.getInt("Seqno"));
                recopf.setValidflag(sqlRecopf1rs.getString("Validflag"));
                recopf.setCostdate(sqlRecopf1rs.getInt("Costdate"));
                recopf.setRetype(sqlRecopf1rs.getString("Retype"));
                recopf.setRngmnt(sqlRecopf1rs.getString("Rngmnt"));
                recopf.setSraramt(sqlRecopf1rs.getBigDecimal("Sraramt"));
                recopf.setRaAmount(sqlRecopf1rs.getBigDecimal("Raamount"));
                recopf.setCtdate(sqlRecopf1rs.getInt("Ctdate"));
                recopf.setOrigcurr(sqlRecopf1rs.getString("Origcurr"));
                recopf.setPrem(sqlRecopf1rs.getBigDecimal("Prem"));
                recopf.setCompay(sqlRecopf1rs.getBigDecimal("Compay"));
                recopf.setTaxamt(sqlRecopf1rs.getBigDecimal("Taxamt"));
                recopf.setRefundfe(sqlRecopf1rs.getBigDecimal("Refundfe"));
                recopf.setBatccoy(sqlRecopf1rs.getString("Batccoy"));
                recopf.setBatcbrn(sqlRecopf1rs.getString("Batcbrn"));
                recopf.setBatcactyr(sqlRecopf1rs.getInt("Batcactyr"));
                recopf.setBatcactmn(sqlRecopf1rs.getInt("Batcactmn"));
                recopf.setBatctrcde(sqlRecopf1rs.getString("Batctrcde"));
                recopf.setBatcbatch(sqlRecopf1rs.getString("Batcbatch"));
                recopf.setTranno(sqlRecopf1rs.getInt("Tranno"));
                recopf.setRcstfrq(sqlRecopf1rs.getString("Rcstfrq"));
                recopf.setUserProfile(sqlRecopf1rs.getString("Usrprf"));
                recopf.setJobName(sqlRecopf1rs.getString("Jobnm"));
				recopf.setJobName(sqlRecopf1rs.getString("Annprem"));
            }

        } catch (SQLException e) {
			LOGGER.error("searchRecopfRecord()", e); 
            throw new SQLRuntimeException(e);
        } finally {
            close(psRecoSelect, sqlRecopf1rs);
        }
        return recopf;

    }
   public Recopf getCompay(String chdrcoy, String chdrnum, String life, String coverage, String rider,
           int plnsfx, String seqno) {
       StringBuilder sqlRecoSelect1 = new StringBuilder("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,SEQNO,VALIDFLAG,COMPAY ");
       sqlRecoSelect1.append(" FROM RECOPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX=? AND SEQNO =? ");
       sqlRecoSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, RASNUM ASC, TRANNO ASC,  UNIQUE_NUMBER ASC ");

       PreparedStatement psRecoSelect = getPrepareStatement(sqlRecoSelect1.toString());
       ResultSet sqlRecopf1rs = null;
       Recopf recopf = null;
       try {
       	psRecoSelect.setString(1, chdrcoy);
       	psRecoSelect.setString(2, chdrnum);
       	psRecoSelect.setString(3, life);
       	psRecoSelect.setString(4, coverage);
       	psRecoSelect.setString(5, rider);
       	psRecoSelect.setInt(6, plnsfx);
       	psRecoSelect.setString(7, seqno);
       	
       	sqlRecopf1rs = executeQuery(psRecoSelect);
           if (sqlRecopf1rs.next()) {
           	recopf = new Recopf();
           	recopf.setChdrcoy(sqlRecopf1rs.getString("Chdrcoy"));
           	recopf.setChdrnum(sqlRecopf1rs.getString("Chdrnum"));
           	recopf.setLife(sqlRecopf1rs.getString("Life"));
            recopf.setCoverage(sqlRecopf1rs.getString("Coverage"));
            recopf.setRider(sqlRecopf1rs.getString("Rider"));
            recopf.setPlnsfx(sqlRecopf1rs.getInt("Plnsfx"));
            recopf.setSeqno(sqlRecopf1rs.getInt("Seqno"));
            recopf.setValidflag(sqlRecopf1rs.getString("Validflag"));
            recopf.setCompay(sqlRecopf1rs.getBigDecimal("Compay"));
           }

       } catch (SQLException e) {
			LOGGER.error("getCompay()", e); 
           throw new SQLRuntimeException(e);
       } finally {
           close(psRecoSelect, sqlRecopf1rs);
       }
       return recopf;
   }
   public Recopf getInitialCompay(String chdrcoy, String chdrnum, String life, String coverage, String rider,
           int plnsfx, String rasnum, String rngmnt) {
       StringBuilder sqlRecoSelect1 = new StringBuilder("SELECT CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,SEQNO,VALIDFLAG,COMPAY ");
       sqlRecoSelect1.append(" FROM RECOPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX=? AND RASNUM = ? AND RNGMNT = ? ");
       sqlRecoSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, TRANNO ASC,  UNIQUE_NUMBER ASC ");

       PreparedStatement psRecoSelect = getPrepareStatement(sqlRecoSelect1.toString());
       ResultSet sqlRecopf1rs = null;
       Recopf recopf = null;
       try {
       	psRecoSelect.setString(1, chdrcoy);
       	psRecoSelect.setString(2, chdrnum);
       	psRecoSelect.setString(3, life);
       	psRecoSelect.setString(4, coverage);
       	psRecoSelect.setString(5, rider);
       	psRecoSelect.setInt(6, plnsfx);
       	psRecoSelect.setString(7, rasnum);
       	psRecoSelect.setString(8, rngmnt);

       	
       	sqlRecopf1rs = executeQuery(psRecoSelect);
           if (sqlRecopf1rs.next()) {
           	recopf = new Recopf();
           	recopf.setChdrcoy(sqlRecopf1rs.getString("Chdrcoy"));
           	recopf.setChdrnum(sqlRecopf1rs.getString("Chdrnum"));
           	recopf.setLife(sqlRecopf1rs.getString("Life"));
            recopf.setCoverage(sqlRecopf1rs.getString("Coverage"));
            recopf.setRider(sqlRecopf1rs.getString("Rider"));
            recopf.setPlnsfx(sqlRecopf1rs.getInt("Plnsfx"));
            recopf.setSeqno(sqlRecopf1rs.getInt("Seqno"));
            recopf.setValidflag(sqlRecopf1rs.getString("Validflag"));
            recopf.setCompay(sqlRecopf1rs.getBigDecimal("Compay"));
           }

       } catch (SQLException e) {
			LOGGER.error("getInitialCompay()", e); 
           throw new SQLRuntimeException(e);
       } finally {
           close(psRecoSelect, sqlRecopf1rs);
       }
       return recopf;
   }

	@Override
	public String getCostFreq(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
		StringBuilder sqlRecoSelect1 = new StringBuilder("SELECT  RCSTFRQ "); //PINNACLE-1834 Frequency Change
	    sqlRecoSelect1.append("FROM RECOPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX=? AND VALIDFLAG='1' ");
	    sqlRecoSelect1.append(" ORDER BY CTDATE DESC");
	
	    PreparedStatement psRecoSelect = getPrepareStatement(sqlRecoSelect1.toString());
	    ResultSet sqlRecopf1rs = null;
	    String rcstfrq = null;
	    try {
	    	psRecoSelect.setString(1, chdrcoy);
	    	psRecoSelect.setString(2, chdrnum);
	    	psRecoSelect.setString(3, life);
	    	psRecoSelect.setString(4, coverage);
	    	psRecoSelect.setString(5, rider);
	    	psRecoSelect.setInt(6, plnsfx);
	    	
	    	sqlRecopf1rs = executeQuery(psRecoSelect);
	        if (sqlRecopf1rs.next()) {
	        	rcstfrq = sqlRecopf1rs.getString("RCSTFRQ");
	        }
	    } catch (SQLException e) {
			LOGGER.error("getCostFreq()", e); 
	       throw new SQLRuntimeException(e);
	   } finally {
	       close(psRecoSelect, sqlRecopf1rs);
	   }
	   return rcstfrq;
	}
}
