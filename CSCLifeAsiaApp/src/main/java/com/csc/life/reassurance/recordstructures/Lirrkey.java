package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:13
 * Description:
 * Copybook name: LIRRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lirrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lirrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lirrKey = new FixedLengthStringData(64).isAPartOf(lirrFileKey, 0, REDEFINE);
  	public FixedLengthStringData lirrCompany = new FixedLengthStringData(1).isAPartOf(lirrKey, 0);
  	public FixedLengthStringData lirrRasnum = new FixedLengthStringData(8).isAPartOf(lirrKey, 1);
  	public FixedLengthStringData lirrRngmnt = new FixedLengthStringData(4).isAPartOf(lirrKey, 9);
  	public FixedLengthStringData lirrClntpfx = new FixedLengthStringData(2).isAPartOf(lirrKey, 13);
  	public FixedLengthStringData lirrClntcoy = new FixedLengthStringData(1).isAPartOf(lirrKey, 15);
  	public FixedLengthStringData lirrClntnum = new FixedLengthStringData(8).isAPartOf(lirrKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(lirrKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lirrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lirrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}