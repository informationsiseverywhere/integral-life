package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:21
 * Description:
 * Copybook name: RACDRCVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdrcvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdrcvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdrcvKey = new FixedLengthStringData(64).isAPartOf(racdrcvFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdrcvChdrcoy = new FixedLengthStringData(1).isAPartOf(racdrcvKey, 0);
  	public FixedLengthStringData racdrcvChdrnum = new FixedLengthStringData(8).isAPartOf(racdrcvKey, 1);
  	public FixedLengthStringData racdrcvLife = new FixedLengthStringData(2).isAPartOf(racdrcvKey, 9);
  	public FixedLengthStringData racdrcvCoverage = new FixedLengthStringData(2).isAPartOf(racdrcvKey, 11);
  	public FixedLengthStringData racdrcvRider = new FixedLengthStringData(2).isAPartOf(racdrcvKey, 13);
  	public PackedDecimalData racdrcvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdrcvKey, 15);
  	public PackedDecimalData racdrcvSeqno = new PackedDecimalData(2, 0).isAPartOf(racdrcvKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(racdrcvKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdrcvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdrcvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}