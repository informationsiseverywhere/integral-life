package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:19
 * Description:
 * Copybook name: RACDCSNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdcsnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdcsnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdcsnKey = new FixedLengthStringData(64).isAPartOf(racdcsnFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdcsnChdrcoy = new FixedLengthStringData(1).isAPartOf(racdcsnKey, 0);
  	public FixedLengthStringData racdcsnChdrnum = new FixedLengthStringData(8).isAPartOf(racdcsnKey, 1);
  	public FixedLengthStringData racdcsnLife = new FixedLengthStringData(2).isAPartOf(racdcsnKey, 9);
  	public FixedLengthStringData racdcsnCoverage = new FixedLengthStringData(2).isAPartOf(racdcsnKey, 11);
  	public FixedLengthStringData racdcsnRider = new FixedLengthStringData(2).isAPartOf(racdcsnKey, 13);
  	public PackedDecimalData racdcsnPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdcsnKey, 15);
  	public PackedDecimalData racdcsnSeqno = new PackedDecimalData(2, 0).isAPartOf(racdcsnKey, 18);
  	public FixedLengthStringData racdcsnCestype = new FixedLengthStringData(1).isAPartOf(racdcsnKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(racdcsnKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdcsnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdcsnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}