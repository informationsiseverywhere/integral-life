package com.csc.life.reassurance.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.reassurance.dataaccess.dao.RasapfDAO;
import com.csc.life.reassurance.dataaccess.model.Rasapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RasapfDAOImpl extends BaseDAOImpl<Rasapf> implements RasapfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(RasapfDAOImpl.class);

	@Override
	public Rasapf getRasapf(String rascoy, String rasnum) {
		Rasapf rasapf = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT RASCOY,RASNUM,VALIDFLAG,CLNTCOY,CLNTNUM,PAYCLT,RAPAYMTH,RAPAYFRQ,FACTHOUS,BANKKEY,BANKACCKEY,");
		sb.append("CURRCODE,DTEPAY,AGNTNUM,REASST FROM RASAPF WHERE RASCOY=? AND RASNUM=? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getConnection().prepareStatement(sb.toString());
			ps.setString(1, rascoy);
			ps.setString(2, rasnum);
			rs = ps.executeQuery();
			if (rs.next()) {
				rasapf = new Rasapf();
				rasapf.setRascoy(rs.getString("RASCOY"));
				rasapf.setRasnum(rs.getString("RASNUM"));
				rasapf.setValidflag(rs.getString("VALIDFLAG"));
				rasapf.setClntcoy(rs.getString("CLNTCOY"));
				rasapf.setClntnum(rs.getString("CLNTNUM"));
				rasapf.setPayclt(rs.getString("PAYCLT"));
				rasapf.setRapaymth(rs.getString("RAPAYMTH"));
				rasapf.setRapayfrq(rs.getString("RAPAYFRQ"));
				rasapf.setFacthous(rs.getString("FACTHOUS"));
				rasapf.setBankkey(rs.getString("BANKKEY"));
				rasapf.setBankacckey(rs.getString("BANKACCKEY"));
				rasapf.setCurrcode(rs.getString("CURRCODE"));
				rasapf.setDtepay(rs.getString("DTEPAY"));
				rasapf.setAgntnum(rs.getString("AGNTNUM"));
				rasapf.setReasst(rs.getString("REASST"));
			}
		} catch (SQLException e) {
			LOGGER.error("getRasapf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return rasapf;
	}
}