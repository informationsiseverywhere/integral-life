package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:13
 * Description:
 * Copybook name: LIRRCONKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lirrconkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lirrconFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lirrconKey = new FixedLengthStringData(64).isAPartOf(lirrconFileKey, 0, REDEFINE);
  	public FixedLengthStringData lirrconCompany = new FixedLengthStringData(1).isAPartOf(lirrconKey, 0);
  	public FixedLengthStringData lirrconChdrnum = new FixedLengthStringData(8).isAPartOf(lirrconKey, 1);
  	public FixedLengthStringData lirrconLife = new FixedLengthStringData(2).isAPartOf(lirrconKey, 9);
  	public FixedLengthStringData lirrconCoverage = new FixedLengthStringData(2).isAPartOf(lirrconKey, 11);
  	public FixedLengthStringData lirrconRider = new FixedLengthStringData(2).isAPartOf(lirrconKey, 13);
  	public PackedDecimalData lirrconPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lirrconKey, 15);
  	public FixedLengthStringData lirrconRasnum = new FixedLengthStringData(8).isAPartOf(lirrconKey, 18);
  	public FixedLengthStringData lirrconRngmnt = new FixedLengthStringData(4).isAPartOf(lirrconKey, 26);
  	public FixedLengthStringData lirrconClntpfx = new FixedLengthStringData(2).isAPartOf(lirrconKey, 30);
  	public FixedLengthStringData lirrconClntcoy = new FixedLengthStringData(1).isAPartOf(lirrconKey, 32);
  	public FixedLengthStringData lirrconClntnum = new FixedLengthStringData(8).isAPartOf(lirrconKey, 33);
  	public FixedLengthStringData filler = new FixedLengthStringData(23).isAPartOf(lirrconKey, 41, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lirrconFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lirrconFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}