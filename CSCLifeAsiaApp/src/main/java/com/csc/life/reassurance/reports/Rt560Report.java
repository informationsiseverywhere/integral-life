package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RT560.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rt560Report extends SMARTReportLayout { 

	private ZonedDecimalData amta = new ZonedDecimalData(17, 2);
	private ZonedDecimalData amtb = new ZonedDecimalData(17, 2);
	private ZonedDecimalData amtma = new ZonedDecimalData(18, 2);
	private ZonedDecimalData amtmb = new ZonedDecimalData(18, 2);
	private ZonedDecimalData amtmc = new ZonedDecimalData(18, 2);
	private ZonedDecimalData amtmd = new ZonedDecimalData(18, 2);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntname = new FixedLengthStringData(50);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData retype = new FixedLengthStringData(1);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rt560Report() {
		super();
	}


	/**
	 * Print the XML for Rd560d1
	 */
	public void printRd560d1(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amta.setFieldName("amta");
		amta.setInternal(subString(recordData, 1, 17));
		printLayout("Rd560d1",			// Record name
			new BaseData[]{			// Fields:
				amta
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rd560d2
	 */
	public void printRd560d2(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amtb.setFieldName("amtb");
		amtb.setInternal(subString(recordData, 1, 17));
		printLayout("Rd560d2",			// Record name
			new BaseData[]{			// Fields:
				amtb
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rd560d3
	 */
	public void printRd560d3(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amtma.setFieldName("amtma");
		amtma.setInternal(subString(recordData, 1, 18));
		printLayout("Rd560d3",			// Record name
			new BaseData[]{			// Fields:
				amtma
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rd560d4
	 */
	public void printRd560d4(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amtmb.setFieldName("amtmb");
		amtmb.setInternal(subString(recordData, 1, 18));
		printLayout("Rd560d4",			// Record name
			new BaseData[]{			// Fields:
				amtmb
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rd560d5
	 */
	public void printRd560d5(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amtmc.setFieldName("amtmc");
		amtmc.setInternal(subString(recordData, 1, 18));
		printLayout("Rd560d5",			// Record name
			new BaseData[]{			// Fields:
				amtmc
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rd560d6
	 */
	public void printRd560d6(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amtmd.setFieldName("amtmd");
		amtmd.setInternal(subString(recordData, 1, 18));
		printLayout("Rd560d6",			// Record name
			new BaseData[]{			// Fields:
				amtmd
			}
		);

	}

	/**
	 * Print the XML for Rt560d01
	 */
	public void printRt560d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 9, 4));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 13, 17));
		rngmnt.setFieldName("rngmnt");
		rngmnt.setInternal(subString(recordData, 30, 4));
		retype.setFieldName("retype");
		retype.setInternal(subString(recordData, 34, 1));
		cmdate.setFieldName("cmdate");
		cmdate.setInternal(subString(recordData, 35, 10));
		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 45, 8));
		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 53, 17));
		printLayout("Rt560d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				crtable,
				sumins,
				rngmnt,
				retype,
				cmdate,
				rasnum,
				raamount
			}
//		ILIFE-1148, Batch L2ACCAGGR Failed
//		, new Object[] {			// indicators
//			new Object[]{"ind31", indicArea.charAt(31)}
//		}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rt560h01
	 */
	public void printRt560h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 11, 10));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rt560h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				sdate,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(6);
	}

	/**
	 * Print the XML for Rt560h02
	 */
	public void printRt560h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 1, 30));
		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 31, 8));
		clntname.setFieldName("clntname");
		clntname.setInternal(subString(recordData, 39, 50));
		printLayout("Rt560h02",			// Record name
			new BaseData[]{			// Fields:
				companynm,
				clntnum,
				clntname
			}
		);

		currentPrintLine.add(5);
	}


}
