package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5595screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 17, 4, 22, 23, 18, 24, 15, 16, 13, 1, 2, 3, 21, 10}; 
	public static int maxRecords = 29;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 20, 3, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5595ScreenVars sv = (S5595ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5595screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5595screensfl, 
			sv.S5595screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5595ScreenVars sv = (S5595ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5595screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5595ScreenVars sv = (S5595ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5595screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5595screensflWritten.gt(0))
		{
			sv.s5595screensfl.setCurrentIndex(0);
			sv.S5595screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5595ScreenVars sv = (S5595ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5595screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5595ScreenVars screenVars = (S5595ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.agntnum.setFieldName("agntnum");
				screenVars.validflag.setFieldName("validflag");
				screenVars.rasnum.setFieldName("rasnum");
				screenVars.rapaymth.setFieldName("rapaymth");
				screenVars.rapayfrq.setFieldName("rapayfrq");
				screenVars.currcode.setFieldName("currcode");
				screenVars.minsta.setFieldName("minsta");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.slt.set(dm.getField("slt"));
			screenVars.agntnum.set(dm.getField("agntnum"));
			screenVars.validflag.set(dm.getField("validflag"));
			screenVars.rasnum.set(dm.getField("rasnum"));
			screenVars.rapaymth.set(dm.getField("rapaymth"));
			screenVars.rapayfrq.set(dm.getField("rapayfrq"));
			screenVars.currcode.set(dm.getField("currcode"));
			screenVars.minsta.set(dm.getField("minsta"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5595ScreenVars screenVars = (S5595ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.agntnum.setFieldName("agntnum");
				screenVars.validflag.setFieldName("validflag");
				screenVars.rasnum.setFieldName("rasnum");
				screenVars.rapaymth.setFieldName("rapaymth");
				screenVars.rapayfrq.setFieldName("rapayfrq");
				screenVars.currcode.setFieldName("currcode");
				screenVars.minsta.setFieldName("minsta");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("slt").set(screenVars.slt);
			dm.getField("agntnum").set(screenVars.agntnum);
			dm.getField("validflag").set(screenVars.validflag);
			dm.getField("rasnum").set(screenVars.rasnum);
			dm.getField("rapaymth").set(screenVars.rapaymth);
			dm.getField("rapayfrq").set(screenVars.rapayfrq);
			dm.getField("currcode").set(screenVars.currcode);
			dm.getField("minsta").set(screenVars.minsta);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5595screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5595ScreenVars screenVars = (S5595ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.slt.clearFormatting();
		screenVars.agntnum.clearFormatting();
		screenVars.validflag.clearFormatting();
		screenVars.rasnum.clearFormatting();
		screenVars.rapaymth.clearFormatting();
		screenVars.rapayfrq.clearFormatting();
		screenVars.currcode.clearFormatting();
		screenVars.minsta.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5595ScreenVars screenVars = (S5595ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.slt.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.validflag.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.rapaymth.setClassString("");
		screenVars.rapayfrq.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.minsta.setClassString("");
	}

/**
 * Clear all the variables in S5595screensfl
 */
	public static void clear(VarModel pv) {
		S5595ScreenVars screenVars = (S5595ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.slt.clear();
		screenVars.agntnum.clear();
		screenVars.validflag.clear();
		screenVars.rasnum.clear();
		screenVars.rapaymth.clear();
		screenVars.rapayfrq.clear();
		screenVars.currcode.clear();
		screenVars.minsta.clear();
	}
}
