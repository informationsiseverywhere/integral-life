package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:51
 * Description:
 * Copybook name: RESTDTYREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Restdtyrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData restdtyRec = new FixedLengthStringData(80);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(restdtyRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(restdtyRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(restdtyRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(restdtyRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(restdtyRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(restdtyRec, 17);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(restdtyRec, 19);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(restdtyRec, 22);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(restdtyRec, 25);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(restdtyRec, 29).setUnsigned();
  	public ZonedDecimalData sumass = new ZonedDecimalData(17, 2).isAPartOf(restdtyRec, 37);
  	public ZonedDecimalData premium = new ZonedDecimalData(13, 2).isAPartOf(restdtyRec, 54);
  	public ZonedDecimalData tax = new ZonedDecimalData(13, 2).isAPartOf(restdtyRec, 67);


	public void initialize() {
		COBOLFunctions.initialize(restdtyRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		restdtyRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}