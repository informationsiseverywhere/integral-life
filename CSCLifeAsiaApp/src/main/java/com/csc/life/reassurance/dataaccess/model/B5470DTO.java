package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;

public class B5470DTO {
    /* SQL-acmv*/
    private String sqlRldgacct;
    private int sqlTranno;
    private String sqlSacscode;
    private String sqlSacstyp;
    private String sqlBatccoy;
    private String sqlBatcbrn;
    private int sqlBatcactyr;
    
    private int sqlBatcactmn;
    private String sqlBatctrcde;
    private String sqlBatcbatch;
    private String sqlOrigcurr;
    private BigDecimal sqlOrigamt;
    private int sqlEffdate;
    private String sqlRdocnum;
    private int sqlJrnseq;
    private String sqlGlsign;
    private String sqlTrandesc;
    
    
    // rasapf
    private String sqlRascoy;
    private String sqlRasnum;
    private String sqlCurrcode;
    private BigDecimal sqlMinsta;
    private String sqlClntcoy;
    private String sqlPayclt;
    
    //clts
    private String sqlCltaddr01;
    private String sqlCltaddr02;
    private String sqlCltaddr03;
    private String sqlCltaddr04;
    private String sqlCltpcode;
    private String sqlClttype;
    private String sqlSurname;
    private String sqlGivname;
    private String sqlSalutl;
    private String sqlLsurname;
    private String sqlLgivname;
    private String sqlEthorig;    
	public String getSqlRldgacct() {
		return sqlRldgacct;
	}
	public void setSqlRldgacct(String sqlRldgacct) {
		this.sqlRldgacct = sqlRldgacct;
	}
	public int getSqlTranno() {
		return sqlTranno;
	}
	public void setSqlTranno(int sqlTranno) {
		this.sqlTranno = sqlTranno;
	}
	public String getSqlSacscode() {
		return sqlSacscode;
	}
	public void setSqlSacscode(String sqlSacscode) {
		this.sqlSacscode = sqlSacscode;
	}
	public String getSqlSacstyp() {
		return sqlSacstyp;
	}
	public void setSqlSacstyp(String sqlSacstyp) {
		this.sqlSacstyp = sqlSacstyp;
	}
	public String getSqlBatccoy() {
		return sqlBatccoy;
	}
	public void setSqlBatccoy(String sqlBatccoy) {
		this.sqlBatccoy = sqlBatccoy;
	}
	public String getSqlBatcbrn() {
		return sqlBatcbrn;
	}
	public void setSqlBatcbrn(String sqlBatcbrn) {
		this.sqlBatcbrn = sqlBatcbrn;
	}
	public int getSqlBatcactyr() {
		return sqlBatcactyr;
	}
	public void setSqlBatcactyr(int sqlBatcactyr) {
		this.sqlBatcactyr = sqlBatcactyr;
	}
	public int getSqlBatcactmn() {
		return sqlBatcactmn;
	}
	public void setSqlBatcactmn(int sqlBatcactmn) {
		this.sqlBatcactmn = sqlBatcactmn;
	}
	public String getSqlBatctrcde() {
		return sqlBatctrcde;
	}
	public void setSqlBatctrcde(String sqlBatctrcde) {
		this.sqlBatctrcde = sqlBatctrcde;
	}
	public String getSqlBatcbatch() {
		return sqlBatcbatch;
	}
	public void setSqlBatcbatch(String sqlBatcbatch) {
		this.sqlBatcbatch = sqlBatcbatch;
	}
	public String getSqlOrigcurr() {
		return sqlOrigcurr;
	}
	public void setSqlOrigcurr(String sqlOrigcurr) {
		this.sqlOrigcurr = sqlOrigcurr;
	}
	public BigDecimal getSqlOrigamt() {
		return sqlOrigamt;
	}
	public void setSqlOrigamt(BigDecimal sqlOrigamt) {
		this.sqlOrigamt = sqlOrigamt;
	}
	public int getSqlEffdate() {
		return sqlEffdate;
	}
	public void setSqlEffdate(int sqlEffdate) {
		this.sqlEffdate = sqlEffdate;
	}
	public String getSqlRdocnum() {
		return sqlRdocnum;
	}
	public void setSqlRdocnum(String sqlRdocnum) {
		this.sqlRdocnum = sqlRdocnum;
	}
	public int getSqlJrnseq() {
		return sqlJrnseq;
	}
	public void setSqlJrnseq(int sqlJrnseq) {
		this.sqlJrnseq = sqlJrnseq;
	}
	public String getSqlGlsign() {
		return sqlGlsign;
	}
	public void setSqlGlsign(String sqlGlsign) {
		this.sqlGlsign = sqlGlsign;
	}
	public String getSqlTrandesc() {
		return sqlTrandesc;
	}
	public void setSqlTrandesc(String sqlTrandesc) {
		this.sqlTrandesc = sqlTrandesc;
	}
	public String getSqlRascoy() {
		return sqlRascoy;
	}
	public void setSqlRascoy(String sqlRascoy) {
		this.sqlRascoy = sqlRascoy;
	}
	public String getSqlRasnum() {
		return sqlRasnum;
	}
	public void setSqlRasnum(String sqlRasnum) {
		this.sqlRasnum = sqlRasnum;
	}
	public String getSqlCurrcode() {
		return sqlCurrcode;
	}
	public void setSqlCurrcode(String sqlCurrcode) {
		this.sqlCurrcode = sqlCurrcode;
	}
	public BigDecimal getSqlMinsta() {
		return sqlMinsta;
	}
	public void setSqlMinsta(BigDecimal sqlMinsta) {
		this.sqlMinsta = sqlMinsta;
	}
	public String getSqlClntcoy() {
		return sqlClntcoy;
	}
	public void setSqlClntcoy(String sqlClntcoy) {
		this.sqlClntcoy = sqlClntcoy;
	}
	public String getSqlPayclt() {
		return sqlPayclt;
	}
	public void setSqlPayclt(String sqlPayclt) {
		this.sqlPayclt = sqlPayclt;
	}
	public String getSqlCltaddr01() {
		return sqlCltaddr01;
	}
	public void setSqlCltaddr01(String sqlCltaddr01) {
		this.sqlCltaddr01 = sqlCltaddr01;
	}
	public String getSqlCltaddr02() {
		return sqlCltaddr02;
	}
	public void setSqlCltaddr02(String sqlCltaddr02) {
		this.sqlCltaddr02 = sqlCltaddr02;
	}
	public String getSqlCltaddr03() {
		return sqlCltaddr03;
	}
	public void setSqlCltaddr03(String sqlCltaddr03) {
		this.sqlCltaddr03 = sqlCltaddr03;
	}
	public String getSqlCltaddr04() {
		return sqlCltaddr04;
	}
	public void setSqlCltaddr04(String sqlCltaddr04) {
		this.sqlCltaddr04 = sqlCltaddr04;
	}
	public String getSqlCltpcode() {
		return sqlCltpcode;
	}
	public void setSqlCltpcode(String sqlCltpcode) {
		this.sqlCltpcode = sqlCltpcode;
	}
	public String getSqlClttype() {
		return sqlClttype;
	}
	public void setSqlClttype(String sqlClttype) {
		this.sqlClttype = sqlClttype;
	}
	public String getSqlSurname() {
		return sqlSurname;
	}
	public void setSqlSurname(String sqlSurname) {
		this.sqlSurname = sqlSurname;
	}
	public String getSqlGivname() {
		return sqlGivname;
	}
	public void setSqlGivname(String sqlGivname) {
		this.sqlGivname = sqlGivname;
	}
	public String getSqlSalutl() {
		return sqlSalutl;
	}
	public void setSqlSalutl(String sqlSalutl) {
		this.sqlSalutl = sqlSalutl;
	}
	public String getSqlLsurname() {
		return sqlLsurname;
	}
	public void setSqlLsurname(String sqlLsurname) {
		this.sqlLsurname = sqlLsurname;
	}
	public String getSqlLgivname() {
		return sqlLgivname;
	}
	public void setSqlLgivname(String sqlLgivname) {
		this.sqlLgivname = sqlLgivname;
	}
	public String getSqlEthorig() {
		return sqlEthorig;
	}
	public void setSqlEthorig(String sqlEthorig) {
		this.sqlEthorig = sqlEthorig;
	}
	
    
    
}
