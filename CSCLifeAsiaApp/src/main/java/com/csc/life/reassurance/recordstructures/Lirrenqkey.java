package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:13
 * Description:
 * Copybook name: LIRRENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lirrenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lirrenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lirrenqKey = new FixedLengthStringData(64).isAPartOf(lirrenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData lirrenqCompany = new FixedLengthStringData(1).isAPartOf(lirrenqKey, 0);
  	public FixedLengthStringData lirrenqClntpfx = new FixedLengthStringData(2).isAPartOf(lirrenqKey, 1);
  	public FixedLengthStringData lirrenqClntcoy = new FixedLengthStringData(1).isAPartOf(lirrenqKey, 3);
  	public FixedLengthStringData lirrenqClntnum = new FixedLengthStringData(8).isAPartOf(lirrenqKey, 4);
  	public FixedLengthStringData lirrenqRasnum = new FixedLengthStringData(8).isAPartOf(lirrenqKey, 12);
  	public FixedLengthStringData lirrenqRngmnt = new FixedLengthStringData(4).isAPartOf(lirrenqKey, 20);
  	public FixedLengthStringData lirrenqChdrnum = new FixedLengthStringData(8).isAPartOf(lirrenqKey, 24);
  	public FixedLengthStringData filler = new FixedLengthStringData(32).isAPartOf(lirrenqKey, 32, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lirrenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lirrenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}