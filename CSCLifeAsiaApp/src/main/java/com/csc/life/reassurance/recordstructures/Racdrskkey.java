package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:22
 * Description:
 * Copybook name: RACDRSKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdrskkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdrskFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdrskKey = new FixedLengthStringData(64).isAPartOf(racdrskFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdrskChdrcoy = new FixedLengthStringData(1).isAPartOf(racdrskKey, 0);
  	public FixedLengthStringData racdrskChdrnum = new FixedLengthStringData(8).isAPartOf(racdrskKey, 1);
  	public FixedLengthStringData racdrskLife = new FixedLengthStringData(2).isAPartOf(racdrskKey, 9);
  	public FixedLengthStringData racdrskCoverage = new FixedLengthStringData(2).isAPartOf(racdrskKey, 11);
  	public FixedLengthStringData racdrskRider = new FixedLengthStringData(2).isAPartOf(racdrskKey, 13);
  	public PackedDecimalData racdrskPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdrskKey, 15);
  	public PackedDecimalData racdrskSeqno = new PackedDecimalData(2, 0).isAPartOf(racdrskKey, 18);
  	public FixedLengthStringData racdrskLrkcls = new FixedLengthStringData(4).isAPartOf(racdrskKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(racdrskKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdrskFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdrskFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}