package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5476screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5476ScreenVars sv = (S5476ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5476screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5476screensfl, 
			sv.S5476screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5476ScreenVars sv = (S5476ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5476screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5476ScreenVars sv = (S5476ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5476screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5476screensflWritten.gt(0))
		{
			sv.s5476screensfl.setCurrentIndex(0);
			sv.S5476screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5476ScreenVars sv = (S5476ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5476screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5476ScreenVars screenVars = (S5476ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.calldtDisp.setFieldName("calldtDisp");
				screenVars.currcode.setFieldName("currcode");
				screenVars.callamt.setFieldName("callamt");
				screenVars.callrecd.setFieldName("callrecd");
				screenVars.rider.setFieldName("rider");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.planSuffix.set(dm.getField("planSuffix"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.calldtDisp.set(dm.getField("calldtDisp"));
			screenVars.currcode.set(dm.getField("currcode"));
			screenVars.callamt.set(dm.getField("callamt"));
			screenVars.callrecd.set(dm.getField("callrecd"));
			screenVars.rider.set(dm.getField("rider"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5476ScreenVars screenVars = (S5476ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.calldtDisp.setFieldName("calldtDisp");
				screenVars.currcode.setFieldName("currcode");
				screenVars.callamt.setFieldName("callamt");
				screenVars.callrecd.setFieldName("callrecd");
				screenVars.rider.setFieldName("rider");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("planSuffix").set(screenVars.planSuffix);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("calldtDisp").set(screenVars.calldtDisp);
			dm.getField("currcode").set(screenVars.currcode);
			dm.getField("callamt").set(screenVars.callamt);
			dm.getField("callrecd").set(screenVars.callrecd);
			dm.getField("rider").set(screenVars.rider);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5476screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5476ScreenVars screenVars = (S5476ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.planSuffix.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.calldtDisp.clearFormatting();
		screenVars.currcode.clearFormatting();
		screenVars.callamt.clearFormatting();
		screenVars.callrecd.clearFormatting();
		screenVars.rider.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5476ScreenVars screenVars = (S5476ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.calldtDisp.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.callamt.setClassString("");
		screenVars.callrecd.setClassString("");
		screenVars.rider.setClassString("");
	}

/**
 * Clear all the variables in S5476screensfl
 */
	public static void clear(VarModel pv) {
		S5476ScreenVars screenVars = (S5476ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.planSuffix.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.calldtDisp.clear();
		screenVars.calldt.clear();
		screenVars.currcode.clear();
		screenVars.callamt.clear();
		screenVars.callrecd.clear();
		screenVars.rider.clear();
	}
}
