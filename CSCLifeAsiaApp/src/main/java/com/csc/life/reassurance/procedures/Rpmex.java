package com.csc.life.reassurance.procedures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.RpmexRec;
import com.csc.life.productdefinition.recordstructures.VpxReinsureRec;
import com.csc.life.reassurance.recordstructures.Rprmiumrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Rpmex extends SMARTCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String SPACES = "";
	private static final String wsaaSubr = "RPMEX";
	private Rprmiumrec rprmiumrec = new Rprmiumrec();
	private RpmexRec rpmexRec = new RpmexRec();
	private Syserrrec syserrrec = new Syserrrec();

	private Map<String, List<String>> inPutParam = new HashMap<>();
	private Map<String, List<List<String>>> inPutParam2 = new HashMap<>();
	private Map<String, List<List<List<String>>>> inPutParam3 = new HashMap<>();
	private List<Lifepf> lifepfList;
	private VpxReinsureRec reinsurerec = new VpxReinsureRec();

	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351", CovrpfDAO.class);
	private RcvdpfDAO rcvdpfDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private ChdrpfDAO chdrDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	protected Agecalcrec agecalcrec = new Agecalcrec();
	private List<String> clntnumList;
	private String noOfCoverages = "";

	@Override
	public void mainline(Object... parmArray) {
		rprmiumrec.rprmiumRec = convertAndSetParam(rprmiumrec.rprmiumRec, parmArray, 0);
		try {
			startSubr010();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}

	}

	protected void startSubr010() {
		rprmiumrec.statuz.set(Varcom.oK);
		rpmexRec.statuz.set(Varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialise();
		exitProgram();
	}

	protected void initialise() {
		setCommonFields();
		readChdrData();
		initMap();
		initCovrList();
		initRcvdList();
		readRcvdpfData();
		setCommonCovrData();
		setLists();
		readLifepf();
		readClntpf();
		readCovrpf();
		setCovrpf();
		readLextpf();
		getReinsureRec();
		callSubroutine();
	}

	protected void setCommonFields() {
		rpmexRec.setCnttype(rprmiumrec.cnttype.toString());
		rpmexRec.setBillfreq(rprmiumrec.billfreq.toString());
		rpmexRec.setCurrcode(rprmiumrec.currcode.toString());
		rpmexRec.setEffectdt(rprmiumrec.effectdt.getbigdata());
		rpmexRec.setLanguage("E");
		rpmexRec.setRatingdate(rprmiumrec.ratingdate.toString());
		rpmexRec.setChdrnum(rprmiumrec.chdrChdrnum.toString());
		rpmexRec.setPremMethod(wsaaSubr);
		rpmexRec.setBilfrmdt(rprmiumrec.bilfrmdt.toInt());
		rpmexRec.setBiltodt(rprmiumrec.biltodt.toInt());
		
	}

	protected void readChdrData() {
		Chdrpf chdrpf = chdrDAO.getchdrRecord(rprmiumrec.chdrChdrcoy.toString(), rprmiumrec.chdrChdrnum.toString());
		Clexpf clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", chdrpf.getCownnum());
		if (clexpf != null)
			rpmexRec.setRstaflag(clexpf.getRstaflag());
		else
			rpmexRec.setRstaflag(SPACE);
	}

	private void initMap() {
		lifepfList = new ArrayList<>();
		inPutParam.put("lifeId", new ArrayList<String>());
		inPutParam.put("lSex", new ArrayList<String>());
		inPutParam.put("dob", new ArrayList<String>());
		inPutParam.put("oppc", new ArrayList<String>());
		inPutParam.put("zmortpct", new ArrayList<String>());
		inPutParam.put("opcda", new ArrayList<String>());
		inPutParam.put("agerate", new ArrayList<String>());
		inPutParam.put("insprem", new ArrayList<String>());
		inPutParam.put("count", new ArrayList<String>());
		inPutParam.put("covrcd", new ArrayList<String>());
		inPutParam.put("reasind", new ArrayList<String>());

		inPutParam3.put("oppcParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("zmortpctParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("opcdaParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("agerateParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("inspremParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("reasindParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("premadjParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("ecestrmParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("lifeIdParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("znadjpercParentList", new ArrayList<List<List<String>>>());

		inPutParam2.put("covrIdParamList", new ArrayList<List<String>>());
		inPutParam2.put("crtableParamList", new ArrayList<List<String>>());
		inPutParam2.put("sumInsParamList", new ArrayList<List<String>>());
		inPutParam2.put("mortClsParamList", new ArrayList<List<String>>());
		inPutParam2.put("riderIdParamList", new ArrayList<List<String>>());
		inPutParam2.put("durationParamList", new ArrayList<List<String>>());
		inPutParam2.put("linkcovParamList", new ArrayList<List<String>>());
		inPutParam2.put("prmBasisParamList", new ArrayList<List<String>>());
		inPutParam2.put("dobParamList", new ArrayList<List<String>>());
		inPutParam2.put("covrcdParamList", new ArrayList<List<String>>());
		inPutParam2.put("countParamList", new ArrayList<List<String>>());
	}

	protected void initCovrList() {
		inPutParam.put("covrId", new ArrayList<String>());
		inPutParam.put("crtable", new ArrayList<String>());
		inPutParam.put("mortCls", new ArrayList<String>());
		inPutParam.put("sumIns", new ArrayList<String>());
		inPutParam.put("duration", new ArrayList<String>());
		inPutParam.put("riderId", new ArrayList<String>());
		inPutParam.put("linkcov", new ArrayList<String>());
	}

	protected void initRcvdList() {
		inPutParam.put("prmBasis", new ArrayList<String>());
	}

	protected void readRcvdpfData() {
		Rcvdpf rcvdpf = new Rcvdpf();
		rcvdpf.setChdrcoy(rprmiumrec.chdrChdrcoy.toString());
		rcvdpf.setChdrnum(rprmiumrec.chdrChdrnum.toString());
		rcvdpf.setLife(rprmiumrec.lifeLife.toString());
		rcvdpf.setCoverage(rprmiumrec.covrCoverage.toString());
		rcvdpf.setRider(rprmiumrec.covrRider.toString());
		rcvdpf.setCrtable(rprmiumrec.crtable.toString());
		rcvdpf = rcvdpfDAO.readRcvdpf(rcvdpf);
		if (rcvdpf != null && rcvdpf.getPrmbasis() != null && rcvdpf.getPrmbasis().equals("S")) {
			inPutParam.get("prmBasis").add("Y");
		} else {
			inPutParam.get("prmBasis").add(SPACES);
		}
	}

	protected void setCommonCovrData() {
		inPutParam.get("covrId").add(rprmiumrec.covrCoverage.toString());
		inPutParam.get("crtable").add(rprmiumrec.crtable.toString());
		inPutParam.get("mortCls").add(rprmiumrec.mortcls.toString());
		inPutParam.get("sumIns").add("0");
		inPutParam.get("duration").add(String.valueOf(rprmiumrec.duration.toInt()));
		inPutParam.get("riderId").add(rprmiumrec.covrRider.toString());
	}

	protected void setLists() {
		inPutParam2.get("covrIdParamList").add(inPutParam.get("covrId"));
		inPutParam2.get("crtableParamList").add(inPutParam.get("crtable"));
		inPutParam2.get("sumInsParamList").add(inPutParam.get("sumIns"));
		inPutParam2.get("mortClsParamList").add(inPutParam.get("mortCls"));
		inPutParam2.get("riderIdParamList").add(inPutParam.get("riderId"));
		inPutParam2.get("durationParamList").add(inPutParam.get("duration"));
		inPutParam2.get("linkcovParamList").add(inPutParam.get("linkcov"));
		inPutParam2.get("prmBasisParamList").add(inPutParam.get("prmBasis"));
		inPutParam2.get("dobParamList").add(inPutParam.get("dob"));
		inPutParam2.get("covrcdParamList").add(inPutParam.get("covrcd"));
	}

	protected void readLifepf() {
		//PINNACLE-1610
		lifepfList = lifepfDAO.getLifeRecords(rprmiumrec.chdrChdrcoy.toString(), rprmiumrec.chdrChdrnum.toString(),"1");
		if (!lifepfList.isEmpty()) {
			setLifeDetails(lifepfList);
		}
	}

	protected void setLifeDetails(List<Lifepf> lifepfList) {
		String lifeNo = null;
		int jLifeInd = 0;
		clntnumList = new ArrayList<>();
		for (Lifepf lif : lifepfList) {
			if (lifeNo != null) {
				if (lif.getLife().equals(lifeNo) && lif.getJlife().equals("00")) {
					setLifeData(lif);
				} else if (lif.getLife().equals(lifeNo) && lif.getJlife().equals("01")) {
					rpmexRec.setLage(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
					inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
					inPutParam.get("lSex").add(lif.getCltsex());
					jLifeInd = 1;
				} else if (!lif.getLife().equals(lifeNo) && jLifeInd != 1) {
					inPutParam.get("lifeId").add(lif.getLife());
					rpmexRec.setLage(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
					inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
					inPutParam.get("lSex").add(lif.getCltsex());
				} else if (!lif.getLife().equals(lifeNo) && jLifeInd == 1) {
					jLifeInd = 0;
					setLifeData(lif);
				}
				lifeNo = lif.getLife();
			} else {
				inPutParam.get("lifeId").add(lif.getLife());
				rpmexRec.setLage(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
				inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
				inPutParam.get("lSex").add(lif.getCltsex());
				lifeNo = lif.getLife();
				jLifeInd = 0;
			}
			clntnumList.add(lif.getLifcnum());
			inPutParam.get("covrcd").add(rprmiumrec.effectdt.toString());
		}
		rpmexRec.setLsex(inPutParam.get("lSex"));
	}

	protected void setLifeData(Lifepf lif) {
		inPutParam.get("lifeId").add(lif.getLife());
		rpmexRec.setLage(String.valueOf(new BigDecimal(lif.getAnbAtCcd())));
		inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
		inPutParam.get("lSex").add(lif.getCltsex());
	}

	protected void readClntpf() {
		inPutParam.put("rstate01", new ArrayList<String>(Collections.nCopies(clntnumList.size(), SPACE)));
		Map<String, Clntpf> clntpfMap = clntpfDAO.searchClntRecord("CN", "9", clntnumList);
		if (clntpfMap != null && !clntpfMap.isEmpty()) {
			for (Map.Entry<String, Clntpf> clnt : clntpfMap.entrySet()) {
				inPutParam.get("rstate01").set(clntnumList.indexOf(clnt.getValue().getClntnum()),
						clnt.getValue().getClntStateCd() != null && !clnt.getValue().getClntStateCd().trim().isEmpty()
								&& !clnt.getValue().getClntStateCd().trim().substring(3).isEmpty()
										? clnt.getValue().getClntStateCd().trim().substring(3) : SPACES);
			}
		}
		rpmexRec.setRstate01(inPutParam.get("rstate01"));
	}

	protected void readCovrpf() {
		List<Covrpf> covrList = covrpfDAO.searchCovrRecordByCoyNumDescUniquNo(rprmiumrec.chdrChdrcoy.toString(),
				rprmiumrec.chdrChdrnum.toString());
		if (covrList != null && !covrList.isEmpty()) {
			for (Covrpf covr : covrList) {
				if (rprmiumrec.crtable.toString().equals(covr.getCrtable())
						&& rprmiumrec.lifeLife.toString().equals(covr.getLife())
						&& rprmiumrec.covrCoverage.toString().equals(covr.getCoverage())
						&& rprmiumrec.covrRider.toString().equals(covr.getRider())) {
					if (covr.getTpdtype() != null && !covr.getTpdtype().trim().isEmpty()) {
						rpmexRec.setTpdtype(((Character) covr.getTpdtype().charAt(3)).toString());
					} else {
						rpmexRec.setTpdtype(SPACE);
					}
					if (covr.getLnkgno() != null && !covr.getLnkgno().trim().isEmpty()) {
						LinkageInfoService linkgService = new LinkageInfoService();
						inPutParam.get("linkcov").add(linkgService.getLinkageInfo(covr.getLnkgno()));
					} else {
						inPutParam.get("linkcov").add(SPACE);
					}
					break;
				}
			}
		} else {
			rpmexRec.setTpdtype(SPACE);
			inPutParam.get("linkcov").add(SPACE);
		}
	}

	protected void setCovrpf() {
		rpmexRec.setCovrCoverage(inPutParam2.get("covrIdParamList"));
		rpmexRec.setCrtable(inPutParam2.get("crtableParamList"));
		rpmexRec.setSumin(inPutParam2.get("sumInsParamList"));
		rpmexRec.setMortcls(inPutParam2.get("mortClsParamList"));
		rpmexRec.setCovrRiderId(inPutParam2.get("riderIdParamList"));
		rpmexRec.setDuration(inPutParam2.get("durationParamList"));
		rpmexRec.setLinkcov(inPutParam2.get("linkcovParamList"));
		rpmexRec.setPrmbasis(inPutParam2.get("prmBasisParamList"));
		rpmexRec.setDob(inPutParam2.get("dobParamList"));
		rpmexRec.setCovrcd(inPutParam2.get("covrcdParamList"));
	}

	protected void readLextpf() {
		List<Lextpf> lextList;
		Lextpf lextpf = new Lextpf();
		lextpf.setChdrcoy(rprmiumrec.chdrChdrcoy.toString());
		lextpf.setChdrnum(rprmiumrec.chdrChdrnum.toString());
		lextList = lextpfDAO.getLextpfData(lextpf);
		if (lextList != null && !lextList.isEmpty()) {
			setLextpfData(lextList);
		} else {
			for (int i = 0; i < inPutParam2.get("covrIdParamList").size(); i++) {
				initLextParamList();
				initLextList();
				for (int j = 0; j < inPutParam2.get("covrIdParamList").get(i).size(); j++) {
					setLextParamLists();
				}
				noOfCoverages = String.valueOf(inPutParam2.get("covrIdParamList").get(i).size());
				inPutParam2.get("countParamList").add(new ArrayList<>(
						Collections.nCopies(inPutParam2.get("covrIdParamList").get(i).size(), String.valueOf(0))));
				setLextParentLists();
			}
			setLextField();
		}
	}

	protected void setLextpfData(List<Lextpf> lextList) {
		int countCvg;
		int countLext;
		for (int i = 0; i < inPutParam.get("lifeId").size(); i++) {
			countCvg = 0;
			inPutParam.put("count", new ArrayList<>());
			initLextParamList();
			for (int j = 0; j < inPutParam2.get("covrIdParamList").get(i).size(); j++) {
				countLext = 0;
				initLextList();
				for (int k = 0; k < lextList.size(); k++) {
					if (inPutParam.get("lifeId").get(i).equals(lextList.get(k).getLife())
							&& inPutParam2.get("covrIdParamList").get(i).get(j).equals(lextList.get(k).getCoverage())) {
						inPutParam.get("oppc").set(countLext, String.valueOf(lextList.get(k).getOppc()));
						inPutParam.get("zmortpct").set(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getZmortpct())));
						inPutParam.get("opcda").set(countLext, lextList.get(k).getOpcda());
						inPutParam.get("agerate").set(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getAgerate())));
						inPutParam.get("insprem").set(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getInsprm())));
						inPutParam.get("reasind").set(countLext, lextList.get(k).getReasind());
						inPutParam.get("premadj").add(countLext, String.valueOf(lextList.get(k).getPremadj()));
						inPutParam.get("ecestrm").add(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getExtCessTerm())));
						inPutParam.get("znadjperc").add(countLext, String.valueOf(lextList.get(k).getZnadjperc()));
						countLext++;
					}
				}
				inPutParam.get("count").add(String.valueOf(countLext));
				setLextParamLists();
				countCvg++;
			}
			noOfCoverages = String.valueOf(countCvg);
			inPutParam2.get("countParamList").add(inPutParam.get("count"));
			setLextParentLists();
		}
		setLextField();
	}

	protected void initLextParamList() {
		inPutParam2.put("oppcParamList", new ArrayList<List<String>>());
		inPutParam2.put("zmortpctParamList", new ArrayList<List<String>>());
		inPutParam2.put("opcdaParamList", new ArrayList<List<String>>());
		inPutParam2.put("agerateParamList", new ArrayList<List<String>>());
		inPutParam2.put("inspremParamList", new ArrayList<List<String>>());
		inPutParam2.put("reasindParamList", new ArrayList<List<String>>());
		inPutParam2.put("premadjParamList", new ArrayList<List<String>>());
		inPutParam2.put("ecestrmParamList", new ArrayList<List<String>>());
		inPutParam2.put("lifeIdParamList", new ArrayList<List<String>>());
		inPutParam2.put("znadjpercParamList", new ArrayList<List<String>>());
	}

	protected void initLextList() {
		inPutParam.put("oppc", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("zmortpct", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("opcda", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("agerate", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("insprem", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("reasind", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("premadj", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("ecestrm", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("znadjperc", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
	}

	protected void setLextField() {
		// LEXT fields
		rpmexRec.setNoOfCoverages(noOfCoverages);
		rpmexRec.setOppc(inPutParam3.get("oppcParentList"));
		rpmexRec.setZmortpct(inPutParam3.get("zmortpctParentList"));
		rpmexRec.setOpcda(inPutParam3.get("opcdaParentList"));
		rpmexRec.setAgerate(inPutParam3.get("agerateParentList"));
		rpmexRec.setInsprm(inPutParam3.get("inspremParentList"));
		rpmexRec.setReasind(inPutParam3.get("reasindParentList"));
		rpmexRec.setCount(inPutParam2.get("countParamList"));
		rpmexRec.setPremadj(inPutParam3.get("premadjParentList"));
		rpmexRec.setEcestrm(inPutParam3.get("ecestrmParentList"));
		rpmexRec.setLifeId(inPutParam3.get("lifeIdParentList"));
		rpmexRec.setZnadjperc(inPutParam3.get("znadjpercParentList"));
	}

	protected void setLextParentLists() {
		inPutParam3.get("oppcParentList").add(inPutParam2.get("oppcParamList"));
		inPutParam3.get("zmortpctParentList").add(inPutParam2.get("zmortpctParamList"));
		inPutParam3.get("opcdaParentList").add(inPutParam2.get("opcdaParamList"));
		inPutParam3.get("agerateParentList").add(inPutParam2.get("agerateParamList"));
		inPutParam3.get("inspremParentList").add(inPutParam2.get("inspremParamList"));
		inPutParam3.get("reasindParentList").add(inPutParam2.get("reasindParamList"));
		inPutParam3.get("premadjParentList").add(inPutParam2.get("premadjParamList"));
		inPutParam3.get("ecestrmParentList").add(inPutParam2.get("ecestrmParamList"));
		inPutParam3.get("lifeIdParentList").add(inPutParam2.get("lifeIdParamList"));
		inPutParam3.get("znadjpercParentList").add(inPutParam2.get("znadjpercParamList"));
	}

	protected void setLextParamLists() {
		inPutParam2.get("oppcParamList").add(inPutParam.get("oppc"));
		inPutParam2.get("zmortpctParamList").add(inPutParam.get("zmortpct"));
		inPutParam2.get("opcdaParamList").add(inPutParam.get("opcda"));
		inPutParam2.get("agerateParamList").add(inPutParam.get("agerate"));
		inPutParam2.get("inspremParamList").add(inPutParam.get("insprem"));
		inPutParam2.get("reasindParamList").add(inPutParam.get("reasind"));
		inPutParam2.get("premadjParamList").add(inPutParam.get("premadj"));
		inPutParam2.get("ecestrmParamList").add(inPutParam.get("ecestrm"));
		inPutParam2.get("lifeIdParamList").add(inPutParam.get("lifeId"));
		inPutParam2.get("znadjpercParamList").add(inPutParam.get("znadjperc"));
	}

	protected void getReinsureRec() {
		callProgram(VpxReinsure.class, rprmiumrec.rprmiumRec, reinsurerec);
	}

	protected void callSubroutine() {
		rpmexRec.initialise();
		if (AppVars.getInstance().getAppConfig().isVpmsEnable() && ExternalisedRules.isCallExternal("RPRMPMEX")) {
			callProgram("RPRMPMEX", rpmexRec, reinsurerec);
		}
		if (rpmexRec.statuz.toString().equals(Varcom.oK.toString())) {
			setPrem();
		} else {
			rprmiumrec.statuz.set(rpmexRec.statuz);
		}
	}

	protected void setPrem() {
		for (int k = 0; k < inPutParam.get("lifeId").size(); k++) {
			for (int j = 0; j < rpmexRec.getCrtable().get(k).size(); j++) {
				if (inPutParam.get("lifeId").get(k).equals(rprmiumrec.lifeLife.toString())
						&& rpmexRec.getCovrCoverage().get(k).get(j).equals(rprmiumrec.covrCoverage.toString())
						&& rpmexRec.getCovrRiderId().get(k).get(j).equals(rprmiumrec.covrRider.toString())) {
					rprmiumrec.calcPrem.set(new BigDecimal(rpmexRec.getReinsPrem().get(k).get(j)));
					rprmiumrec.reinsCommprem.set(new BigDecimal(rpmexRec.getReinsCommprem().get(k).get(j)));
					rprmiumrec.reinsPremBaseRate.set(new BigDecimal(rpmexRec.getReinsPremBaseRate().get(k).get(j)));
					rprmiumrec.reinsAnnPrem.set(new BigDecimal(rpmexRec.getReinsAnnPrem().get(k).get(j)));
				}
			}
		}
	}
}