package com.csc.life.reassurance.dataaccess.dao;

import com.csc.life.reassurance.dataaccess.model.Recopf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RecopfDAO extends BaseDAO<Recopf> {

	public int insertRecopf(Recopf recopf);
	public Recopf searchRecopfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,
	            int plnsfx, String seqno, String ctdate);
	 public Recopf getCompay(String chdrcoy, String chdrnum, String life, String coverage, String rider,
	           int plnsfx, String seqno);
	 public Recopf getInitialCompay(String chdrcoy, String chdrnum, String life, String coverage, String rider,
	           int plnsfx, String rasnum, String rngmnt);
	 public String getCostFreq(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
}
