package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6261screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 19, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 14, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6261ScreenVars sv = (S6261ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6261screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6261ScreenVars screenVars = (S6261ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.numsel.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.branch.setClassString("");
		screenVars.branchnm.setClassString("");
		screenVars.rapaymth.setClassString("");
		screenVars.paymthd.setClassString("");
		screenVars.rapayfrq.setClassString("");
		screenVars.rapayfrqd.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.branchdesc.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.rapayee.setClassString("");
		screenVars.payenme.setClassString("");
		screenVars.facthous.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.currdesc.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.minsta.setClassString("");
		screenVars.clientind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.reasst.setClassString("");
		screenVars.reastdesc.setClassString("");
		screenVars.termdtDisp.setClassString("");
		screenVars.commdtDisp.setClassString("");
		screenVars.nxtpayDisp.setClassString("");
		screenVars.stmtdtDisp.setClassString("");
	}

/**
 * Clear all the variables in S6261screen
 */
	public static void clear(VarModel pv) {
		S6261ScreenVars screenVars = (S6261ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.numsel.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.rasnum.clear();
		screenVars.branch.clear();
		screenVars.branchnm.clear();
		screenVars.rapaymth.clear();
		screenVars.paymthd.clear();
		screenVars.rapayfrq.clear();
		screenVars.rapayfrqd.clear();
		screenVars.bankkey.clear();
		screenVars.bankdesc.clear();
		screenVars.branchdesc.clear();
		screenVars.bankacckey.clear();
		screenVars.bankaccdsc.clear();
		screenVars.rapayee.clear();
		screenVars.payenme.clear();
		screenVars.facthous.clear();
		screenVars.currcode.clear();
		screenVars.currdesc.clear();
		screenVars.agntsel.clear();
		screenVars.agentname.clear();
		screenVars.minsta.clear();
		screenVars.clientind.clear();
		screenVars.ddind.clear();
		screenVars.reasst.clear();
		screenVars.reastdesc.clear();
		screenVars.termdtDisp.clear();
		screenVars.termdt.clear();
		screenVars.commdtDisp.clear();
		screenVars.commdt.clear();
		screenVars.nxtpayDisp.clear();
		screenVars.nxtpay.clear();
		screenVars.stmtdtDisp.clear();
		screenVars.stmtdt.clear();
	}
}
