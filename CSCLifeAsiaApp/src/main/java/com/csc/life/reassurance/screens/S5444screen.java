package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5444screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 5, 18, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5444ScreenVars sv = (S5444ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5444screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5444ScreenVars screenVars = (S5444ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.currds.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.rngmnt.setClassString("");
		screenVars.currdesc.setClassString("");
		screenVars.raAmount.setClassString("");
		screenVars.clntname.setClassString("");
		screenVars.rngmntdesc.setClassString("");
		screenVars.reasper.setClassString("");
		screenVars.recovamt.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.rrevdtDisp.setClassString("");
		screenVars.ctdateDisp.setClassString("");
		screenVars.cmdateDisp.setClassString("");
		screenVars.retypedesc.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.retype.setClassString("");
		screenVars.tabledesc.setClassString("");
		screenVars.lrkcls.setClassString("");
	}

/**
 * Clear all the variables in S5444screen
 */
	public static void clear(VarModel pv) {
		S5444ScreenVars screenVars = (S5444ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.crtable.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.longdesc.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.currcd.clear();
		screenVars.currds.clear();
		screenVars.rasnum.clear();
		screenVars.rngmnt.clear();
		screenVars.currdesc.clear();
		screenVars.raAmount.clear();
		screenVars.clntname.clear();
		screenVars.rngmntdesc.clear();
		screenVars.reasper.clear();
		screenVars.recovamt.clear();
		screenVars.sumins.clear();
		screenVars.rrevdtDisp.clear();
		screenVars.rrevdt.clear();
		screenVars.ctdateDisp.clear();
		screenVars.ctdate.clear();
		screenVars.cmdateDisp.clear();
		screenVars.cmdate.clear();
		screenVars.retypedesc.clear();
		screenVars.currcode.clear();
		screenVars.retype.clear();
		screenVars.tabledesc.clear();
		screenVars.lrkcls.clear();
	}
}
