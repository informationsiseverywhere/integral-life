package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5438
 * @version 1.0 generated on 30/08/09 06:40
 * @author Quipoz
 */
public class S5438ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(301);
	public FixedLengthStringData dataFields = new FixedLengthStringData(173).isAPartOf(dataArea, 0);/* ILIFE-6026 cltnme is changed to clntname, refer DDFieldWindowing of rasnum */
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(dataFields,0);  //ILIFE-6026
	public ZonedDecimalData cmdate = DD.cmdate.copyToZonedDecimal().isAPartOf(dataFields,50);  //ILIFE-6026
	public FixedLengthStringData comt = DD.comt.copy().isAPartOf(dataFields,58);  //ILIFE-6026
	public ZonedDecimalData ctdate = DD.ctdate.copyToZonedDecimal().isAPartOf(dataFields,88);  //ILIFE-6026
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(dataFields,96);  //ILIFE-6026
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,113);  //ILIFE-6026
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(dataFields,121);  //ILIFE-6026
	public FixedLengthStringData rngmntdesc = DD.rngmntdesc.copy().isAPartOf(dataFields,125);  //ILIFE-6026
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,155);  //ILIFE-6026
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 173);  //ILIFE-6026
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);  //ILIFE-6026
	public FixedLengthStringData cmdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData rngmntdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 193);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);  //ILIFE-6026
	public FixedLengthStringData[] cmdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] rngmntdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData cmdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ctdateDisp = new FixedLengthStringData(10);

	public LongData S5438screenWritten = new LongData(0);
	public LongData S5438protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5438ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rasnumOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rngmntOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(raamountOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {comt, rasnum, rngmnt, cmdate, ctdate, sumins, raAmount, rngmntdesc, clntname};
		screenOutFields = new BaseData[][] {comtOut, rasnumOut, rngmntOut, cmdateOut, ctdateOut, suminsOut, raamountOut, rngmntdescOut, clntnameOut};  //ILIFE-6026
		screenErrFields = new BaseData[] {comtErr, rasnumErr, rngmntErr, cmdateErr, ctdateErr, suminsErr, raamountErr, rngmntdescErr, clntnameErr};  //ILIFE-6026
		screenDateFields = new BaseData[] {cmdate, ctdate};
		screenDateErrFields = new BaseData[] {cmdateErr, ctdateErr};
		screenDateDispFields = new BaseData[] {cmdateDisp, ctdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5438screen.class;
		protectRecord = S5438protect.class;
	}

}
