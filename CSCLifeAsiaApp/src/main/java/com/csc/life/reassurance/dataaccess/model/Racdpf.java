package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;

public class Racdpf {
   	private long unique_number;
    private int tranno;
    private int currfrom;
    private int currto;
    private int ctdate;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int planSuffix;
    private String rasnum;
    private int seqno;
    private String validflag;
    private String currcode;
    

	private String retype;
    private String rngmnt;
    private BigDecimal raAmount;
    private int cmdate;
    private BigDecimal reasper;
    private int rrevdt;
    private BigDecimal recovamt;
    private String cestype;
    private String ovrdind;
    private String lrkcls;
    private String userProfile;
    private String jobName;
    private String datime;
    private int rrtdat;
    private int rrtfrm;
    private int pendcstto;
    private String prorateflag;

    
    public long getUnique_number() {
        return unique_number;
    }

    public void setUnique_number(long unique_number) {
        this.unique_number = unique_number;
    }

    public int getTranno() {
        return tranno;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public int getCurrfrom() {
        return currfrom;
    }

    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }

    public int getCurrto() {
        return currto;
    }

    public void setCurrto(int currto) {
        this.currto = currto;
    }

    public int getCtdate() {
        return ctdate;
    }

    public void setCtdate(int ctdate) {
        this.ctdate = ctdate;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getRider() {
        return rider;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public int getPlanSuffix() {
        return planSuffix;
    }

    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }

    public String getRasnum() {
        return rasnum;
    }

    public void setRasnum(String rasnum) {
        this.rasnum = rasnum;
    }

    public int getSeqno() {
        return seqno;
    }

    public void setSeqno(int seqno) {
        this.seqno = seqno;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public String getCurrcode() {
        return currcode;
    }

    public void setCurrcode(String currcode) {
        this.currcode = currcode;
    }

    public String getRetype() {
        return retype;
    }

    public void setRetype(String retype) {
        this.retype = retype;
    }

    public String getRngmnt() {
        return rngmnt;
    }

    public void setRngmnt(String rngmnt) {
        this.rngmnt = rngmnt;
    }

    public BigDecimal getRaAmount() {
        return raAmount;
    }

    public void setRaAmount(BigDecimal raAmount) {
        this.raAmount = raAmount;
    }

    public int getCmdate() {
        return cmdate;
    }

    public void setCmdate(int cmdate) {
        this.cmdate = cmdate;
    }

    public BigDecimal getReasper() {
        return reasper;
    }

    public void setReasper(BigDecimal reasper) {
        this.reasper = reasper;
    }

    public int getRrevdt() {
        return rrevdt;
    }

    public void setRrevdt(int rrevdt) {
        this.rrevdt = rrevdt;
    }

    public BigDecimal getRecovamt() {
        return recovamt;
    }

    public void setRecovamt(BigDecimal recovamt) {
        this.recovamt = recovamt;
    }

    public String getCestype() {
        return cestype;
    }

    public void setCestype(String cestype) {
        this.cestype = cestype;
    }

    public String getOvrdind() {
        return ovrdind;
    }

    public void setOvrdind(String ovrdind) {
        this.ovrdind = ovrdind;
    }

    public String getLrkcls() {
        return lrkcls;
    }

    public void setLrkcls(String lrkcls) {
        this.lrkcls = lrkcls;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }
    /**
	 * @return the rrtdat
	 */
	public int getRrtdat() {
		return rrtdat;
	}

	/**
	 * @param rrtdat the rrtdat to set
	 */
	public void setRrtdat(int rrtdat) {
		this.rrtdat = rrtdat;
	}

	/**
	 * @return the rrtfrm
	 */
	public int getRrtfrm() {
		return rrtfrm;
	}

	/**
	 * @param rrtfrm the rrtfrm to set
	 */
	public void setRrtfrm(int rrtfrm) {
		this.rrtfrm = rrtfrm;
	}
	
	public int getPendcstto() {
		return pendcstto;
	}

	public void setPendcstto(int pendcstto) {
		this.pendcstto = pendcstto;
	}

	public String getProrateflag() {
		return prorateflag;
	}

	public void setProrateflag(String prorateflag) {
		this.prorateflag = prorateflag;
	}
	
	

}
