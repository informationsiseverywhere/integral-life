package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5436
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5436ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(215);
	public FixedLengthStringData dataFields = new FixedLengthStringData(119).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData cltdob = DD.cltdob.copyToZonedDecimal().isAPartOf(dataFields,8);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cltsex = DD.cltsex.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData reasname = DD.reasname.copy().isAPartOf(dataFields,72);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 119);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltdobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cltsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData reasnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 143);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltdobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cltsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] reasnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(115);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(33).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(subfileFields,11);
	public FixedLengthStringData retype = DD.retype.copy().isAPartOf(subfileFields,28);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(subfileFields,29);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 33);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData currencyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData retypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 53);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] currencyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] retypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 113);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData cltdobDisp = new FixedLengthStringData(10);

	public LongData S5436screensflWritten = new LongData(0);
	public LongData S5436screenctlWritten = new LongData(0);
	public LongData S5436screenWritten = new LongData(0);
	public LongData S5436protectWritten = new LongData(0);
	public GeneralTable s5436screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5436screensfl;
	}

	public S5436ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {rngmnt, retype, raAmount, currency, chdrnum};
		screenSflOutFields = new BaseData[][] {rngmntOut, retypeOut, raamountOut, currencyOut, chdrnumOut};
		screenSflErrFields = new BaseData[] {rngmntErr, retypeErr, raamountErr, currencyErr, chdrnumErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {rasnum, reasname, cltsex, cltdob, clntnum, cltname};
		screenOutFields = new BaseData[][] {rasnumOut, reasnameOut, cltsexOut, cltdobOut, clntnumOut, cltnameOut};
		screenErrFields = new BaseData[] {rasnumErr, reasnameErr, cltsexErr, cltdobErr, clntnumErr, cltnameErr};
		screenDateFields = new BaseData[] {cltdob};
		screenDateErrFields = new BaseData[] {cltdobErr};
		screenDateDispFields = new BaseData[] {cltdobDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5436screen.class;
		screenSflRecord = S5436screensfl.class;
		screenCtlRecord = S5436screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5436protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5436screenctl.lrec.pageSubfile);
	}
}
