/*
 * File: P5436.java
 * Date: 30 August 2009 0:25:48
 * Author: Quipoz Limited
 * 
 * Class transformed from P5436.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.LirrenqTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S5436ScreenVars;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Reassurer Experience Scroll.
*
* Overview
* ========
*
* This program forms part of the Reassurance Development.
*
* This program is invoked when an action of 'B' is selected
* from the Reassurance Experience Enquiry Submenu, S5434.
*
* This program will display all LIRR records for the client
* and reassurer selected. If an arrangement is also selected,
* display the LIRR records for that particular arrangement.
*
* Processing
* ==========
*
* 1000-Initialise.
*
* Clear the screen.
* Read the RASA and CLTS files for the reassurer details.
* Read the CLTS for the life assured details.
* Load the subfile with the LIRR records.
*
* 2000-Screen-Edit.
*
* Display the screen.
*
* 3000-Update.
*
* No updating required.
*
* 4000-Where-Next.
*
* Add 1 to the program pointer.
*
*****************************************************************
* </pre>
*/
public class P5436 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5436");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private String t5449 = "T5449";
		/* ERRORS */
	private String r057 = "R057";
		/* FORMATS */
	private String cltsrec = "CLTSREC";
	private String itdmrec = "ITEMREC";
	private String lirrenqrec = "LIRRENQREC";
	private String rasarec = "RASAREC";
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Reassurance Experience Enquiry File*/
	private LirrenqTableDAM lirrenqIO = new LirrenqTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private T5449rec t5449rec = new T5449rec();
	private Wssplife wssplife = new Wssplife();
	private S5436ScreenVars sv = ScreenProgram.getScreenVars( S5436ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr1180, 
		exit1190, 
		preExit, 
		lgnmExit, 
		plainExit, 
		payeeExit
	}

	public P5436() {
		super();
		screenVars = sv;
		new ScreenModel("S5436", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.cltdob.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5436", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(wsspcomn.company);
		rasaIO.setRasnum(wssplife.rasnum);
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.reasname.set(wsspcomn.longconfname);
		sv.rasnum.set(wssplife.rasnum);
		cltsIO.setParams(SPACES);
		cltsIO.setDataKey(wsspcomn.clntkey);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
		sv.clntnum.set(cltsIO.getClntnum());
		sv.cltdob.set(cltsIO.getCltdob());
		sv.cltsex.set(cltsIO.getCltsex());
		lirrenqIO.setParams(SPACES);
		lirrenqIO.setCompany(wsspcomn.company);
		lirrenqIO.setClntpfx(cltsIO.getClntpfx());
		lirrenqIO.setClntcoy(cltsIO.getClntcoy());
		lirrenqIO.setClntnum(cltsIO.getClntnum());
		lirrenqIO.setRasnum(wssplife.rasnum);
		lirrenqIO.setRngmnt(wssplife.rngmnt);
		lirrenqIO.setFormat(lirrenqrec);
		lirrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lirrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lirrenqIO.setFitKeysSearch("COMPANY", "CLNTPFX", "CLNTCOY", "CLNTNUM", "RASNUM");
		while ( !(isEQ(lirrenqIO.getStatuz(),varcom.endp))) {
			processLirrenq1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void processLirrenq1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1110();
				}
				case nextr1180: {
					nextr1180();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1110()
	{
		SmartFileCode.execute(appVars, lirrenqIO);
		if (isNE(lirrenqIO.getStatuz(),varcom.oK)
		&& isNE(lirrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lirrenqIO.getParams());
			fatalError600();
		}
		if (isNE(lirrenqIO.getCompany(),wsspcomn.company)
		|| isNE(lirrenqIO.getClntpfx(),cltsIO.getClntpfx())
		|| isNE(lirrenqIO.getClntcoy(),cltsIO.getClntcoy())
		|| isNE(lirrenqIO.getClntnum(),cltsIO.getClntnum())
		|| isNE(lirrenqIO.getRasnum(),wssplife.rasnum)
		|| (isNE(lirrenqIO.getRngmnt(),wssplife.rngmnt)
		&& isNE(wssplife.rngmnt,SPACES))
		|| isEQ(lirrenqIO.getStatuz(),varcom.endp)) {
			lirrenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(lirrenqIO.getRaAmount(),ZERO)) {
			goTo(GotoLabel.nextr1180);
		}
		sv.subfileFields.set(SPACES);
		sv.rngmnt.set(lirrenqIO.getRngmnt());
		sv.chdrnum.set(lirrenqIO.getChdrnum());
		sv.raAmount.set(lirrenqIO.getRaAmount());
		sv.currency.set(lirrenqIO.getCurrency());
		getArangementType1200();
		sv.retype.set(t5449rec.retype);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5436", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void nextr1180()
	{
		lirrenqIO.setFunction(varcom.nextr);
	}

protected void getArangementType1200()
	{
		para1210();
	}

protected void para1210()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(lirrenqIO.getCompany());
		itdmIO.setItemtabl(t5449);
		itdmIO.setItemitem(lirrenqIO.getRngmnt());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),lirrenqIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5449)
		|| isNE(itdmIO.getItemitem(),lirrenqIO.getRngmnt())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(r057);
			itdmIO.setItemcoy(lirrenqIO.getCompany());
			itdmIO.setItemtabl(t5449);
			itdmIO.setItemitem(lirrenqIO.getRngmnt());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5449rec.t5449Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}
}
