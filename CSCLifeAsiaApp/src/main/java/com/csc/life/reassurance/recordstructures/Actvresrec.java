package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:49
 * Description:
 * Copybook name: ACTVRESREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Actvresrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData actvresRec = new FixedLengthStringData(92);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(actvresRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(actvresRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(actvresRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(actvresRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(actvresRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(actvresRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(actvresRec, 22);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(actvresRec, 24);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(actvresRec, 27);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(1).isAPartOf(actvresRec, 31);
  	public FixedLengthStringData l1Clntnum = new FixedLengthStringData(8).isAPartOf(actvresRec, 32);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(actvresRec, 40);
  	public FixedLengthStringData l2Clntnum = new FixedLengthStringData(8).isAPartOf(actvresRec, 42);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(actvresRec, 50);
  	public PackedDecimalData oldSumins = new PackedDecimalData(17, 2).isAPartOf(actvresRec, 53);
  	public PackedDecimalData newSumins = new PackedDecimalData(17, 2).isAPartOf(actvresRec, 62);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(actvresRec, 71);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(actvresRec, 76);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(actvresRec, 79);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(actvresRec, 82);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(actvresRec, 87);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(actvresRec, 88);


	public void initialize() {
		COBOLFunctions.initialize(actvresRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		actvresRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}