package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:20
 * Description:
 * Copybook name: RACDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdKey = new FixedLengthStringData(64).isAPartOf(racdFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdChdrcoy = new FixedLengthStringData(1).isAPartOf(racdKey, 0);
  	public FixedLengthStringData racdChdrnum = new FixedLengthStringData(8).isAPartOf(racdKey, 1);
  	public FixedLengthStringData racdLife = new FixedLengthStringData(2).isAPartOf(racdKey, 9);
  	public FixedLengthStringData racdCoverage = new FixedLengthStringData(2).isAPartOf(racdKey, 11);
  	public FixedLengthStringData racdRider = new FixedLengthStringData(2).isAPartOf(racdKey, 13);
  	public PackedDecimalData racdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdKey, 15);
  	public PackedDecimalData racdSeqno = new PackedDecimalData(2, 0).isAPartOf(racdKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(racdKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}