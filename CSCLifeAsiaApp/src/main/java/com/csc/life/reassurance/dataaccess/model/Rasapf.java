package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RASAPF", schema = "VM1DTA")
public class Rasapf {
    private long uniqueNumber;
    private String raspfx;
    private String rascoy;
    private String rasnum;
    private String validflag;
    private String clntcoy;
    private String clntnum;
    private String payclt;
    private String rapaymth;
    private String rapayfrq;
    private String facthous;
    private String bankkey;
    private String bankacckey;
    private String currcode;
    private String dtepay;
    private String agntnum;
    private BigDecimal minsta;
    private Integer trdt;
    private Integer trtm;
    private Integer user_t;
    private Integer commdt;
    private Integer termdt;
    private String reasst;
    private Integer stmtdt;
    private Integer nxtpay;
    private String usrprf;
    private String jobnm;
    private Date datime;
    
	public Rasapf() {
	}

	public Rasapf(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	@Id
	@Column(name = "UNIQUE_NUMBER", unique = true, nullable = false, precision = 18, scale = 0)
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	
	@Column(name = "RASPFX", length = 2)
	public String getRaspfx() {
		return raspfx;
	}

	public void setRaspfx(String raspfx) {
		this.raspfx = raspfx;
	}
	
	@Column(name = "RASCOY", length = 1)
	public String getRascoy() {
		return rascoy;
	}
	public void setRascoy(String rascoy) {
		this.rascoy = rascoy;
	}
	
	@Column(name = "RASNUM", length = 8)
	public String getRasnum() {
		return rasnum;
	}
	public void setRasnum(String rasnum) {
		this.rasnum = rasnum;
	}
	
	@Column(name = "VALIDFLAG", length = 1)
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	
	@Column(name = "CLNTCOY", length = 1)
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	
	@Column(name = "CLNTNUM", length = 8)
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	
	@Column(name = "PAYCLT", length = 8)
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	
	@Column(name = "RAPAYMTH", length = 1)
	public String getRapaymth() {
		return rapaymth;
	}
	public void setRapaymth(String rapaymth) {
		this.rapaymth = rapaymth;
	}
	
	@Column(name = "RAPAYFRQ", length = 2)
	public String getRapayfrq() {
		return rapayfrq;
	}
	public void setRapayfrq(String rapayfrq) {
		this.rapayfrq = rapayfrq;
	}
	
	@Column(name = "FACTHOUS", length = 2)
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	
	@Column(name = "BANKKEY", length = 10)
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	
	@Column(name = "BANKACCKEY", length = 20)
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	
	@Column(name = "CURRCODE", length = 3)
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	
	@Column(name = "DTEPAY", precision = 8, scale = 0)
	public String getDtepay() {
		return dtepay;
	}
	public void setDtepay(String dtepay) {
		this.dtepay = dtepay;
	}
	@Column(name = "AGNTNUM", length = 8)
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	@Column(name = "MINSTA", precision = 17)
	public BigDecimal getMinsta() {
		return minsta;
	}
	public void setMinsta(BigDecimal minsta) {
		this.minsta = minsta;
	}
	@Column(name = "TRDT", precision =6, scale = 0)
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	
	@Column(name = "TRTM", precision =6, scale = 0)
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	@Column(name = "USER_T", precision =6, scale = 0)
	public Integer getUser_t() {
		return user_t;
	}
	public void setUser_t(Integer user_t) {
		this.user_t = user_t;
	}
	
	@Column(name = "COMMDT", precision =8, scale = 0)
	public Integer getCommdt() {
		return commdt;
	}
	public void setCommdt(Integer commdt) {
		this.commdt = commdt;
	}
	@Column(name = "TERMDT", precision =8, scale = 0)
	public Integer getTermdt() {
		return termdt;
	}
	public void setTermdt(Integer termdt) {
		this.termdt = termdt;
	}
	
	@Column(name = "REASST", length = 1)
	public String getReasst() {
		return reasst;
	}
	public void setReasst(String reasst) {
		this.reasst = reasst;
	}
	
	@Column(name = "STMTDT",  precision =8, scale = 0)
	public Integer getStmtdt() {
		return stmtdt;
	}
	public void setStmtdt(Integer stmtdt) {
		this.stmtdt = stmtdt;
	}
	
	@Column(name = "NXTPAY",  precision =8, scale = 0)
	public Integer getNxtpay() {
		return nxtpay;
	}
	public void setNxtpay(Integer nxtpay) {
		this.nxtpay = nxtpay;
	}
	
	@Column(name = "USRPRF", length = 10)
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	
	@Column(name = "JOBNM", length = 10)
	public String getJOBNM() {
		return jobnm;
	}
	public void setJOBNM(String jobnm) {
		jobnm = jobnm;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATIME", length = 11)
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	
}
