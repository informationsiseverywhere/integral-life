package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;

import com.csc.life.reassurance.dataaccess.model.Lrrhpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;


public interface LrrhpfDAO extends BaseDAO<Lrrhpf>{
	public List<Lrrhpf> searchlLrrhconRecord(String coy, String chdrnum);
	public void deleteLrrhpfRecord(List<Lrrhpf> taxdpfList);
	public void updateLrrhpfBulk(String coy, String chdrnum,  String life, String coverage, String rider, String plnsfx,
			 int busDate);
	public void updateLrrhpfSsretnAndSsreast(String coy, String chdrnum,  String life, String coverage, String rider, 
			String plnsfx, int busDate);
	public void insertLrrhpfRecord(Lrrhpf lrrhpf);
	 public Lrrhpf getLrrhpfRecord(Lrrhpf lrrhpf);
}
