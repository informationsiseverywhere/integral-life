/*
 * File: Rclmr04.java
 * Date: 30 August 2009 2:03:41
 * Author: Quipoz Limited
 * 
 * Class transformed from RCLMR04.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.recordstructures.Rclmcalrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Reassurer Claim Recovery Calculation Subroutine No.4.
*
* Overview
* ========
*
* This Routine calculates the amount to be recovered from the
* Reassurer in the event of a claim. It will return a
* Percentage of the Sum Assured as the Claim Amount. The
* Percentage is passed to the Routine as an input Parameter in
* the Copybook. The Subroutine will then Post the Claim
* Recovery Amount.
*
* Linkage
* =======
*
* FUNCTION           PIC X(05).
* CHDRCOY            PIC X(01).
* CHDRNUM            PIC X(08).
* LIFE               PIC X(02).
* COVERAGE           PIC X(02).
* RIDER              PIC X(02).
* PLAN-SUFFIX        PIC S9(04) COMP-3.
* POLSUM             PIC S9(04) COMP-3.
* EFFDATE            PIC S9(08) COMP-3.
* BATCKEY            PIC X(18).
* TRANNO             PIC S9(05) COMP-3.
* SEQNO              PIC S9(02) COMP-3.
* RA-AMOUNT          PIC S9(11)V9(02) COMP-3.
* RASNUM             PIC X(08).
* RESERVPROG         PIC X(07).
* BILLFREQ           PIC X(02).
* PTDATE             PIC S9(08) COMP-3.
* ORIGCURR           PIC X(03).
* ACCTCURR           PIC X(03).
* CNTTYPE            PIC X(03).
* CRTABLE            PIC X(05).
* CRRCD              PIC S9(08) COMP-3.
* CONV-UNITS         PIC S9(08) COMP-3.
* JLIFE              PIC X(02).
* SINGP              PIC S9(11)V9(02) COMP-3.
* SUMINS             PIC S9(11)V9(02) COMP-3.
* CLM-PERCENT        PIC S9(03)V9(02) COMP-3.
* PSTATCODE          PIC X.
* LANGUAGE           PIC X.
* RECOV-AMT          PIC S9(11)V9(02) COMP-3.
* STATUZ             PIC X(04).
*
* These fields are all contained within RCLMCALREC.
*
*****************************************************************
* </pre>
*/
public class Rclmr04 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RCLMR04";
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData wsaaRecovAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCrate = new PackedDecimalData(18, 9);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* ERRORS */
	private static final String e049 = "E049";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rclmcalrec rclmcalrec = new Rclmcalrec();

	public Rclmr04() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rclmcalrec.rclmcalRec = convertAndSetParam(rclmcalrec.rclmcalRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		beginning110();
		exit190();
	}

protected void beginning110()
	{
		rclmcalrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaTransTime.set(getCobolTime());
		if (isNE(rclmcalrec.function, "POST")) {
			syserrrec.statuz.set(e049);
			syserr800();
		}
		compute(rclmcalrec.recovAmt, 2).set(div(mult(rclmcalrec.raAmount, rclmcalrec.clmPercent), 100));
		zrdecplrec.amountIn.set(rclmcalrec.recovAmt);
		zrdecplrec.currency.set(rclmcalrec.origcurr);
		a000CallRounding();
		rclmcalrec.recovAmt.set(zrdecplrec.amountOut);
		convertAmounts200();
		postAcmvs300();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void convertAmounts200()
	{
		/*CONVERT*/
		if (isEQ(rclmcalrec.origcurr, rclmcalrec.acctcurr)) {
			wsaaRecovAmt.set(rclmcalrec.recovAmt);
			wsaaCrate.set(1);
		}
		else {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(rclmcalrec.recovAmt);
			callXcvrt500();
			wsaaRecovAmt.set(conlinkrec.amountOut);
			compute(wsaaCrate, 9).set(div(conlinkrec.amountIn, conlinkrec.amountOut));
		}
		/*EXIT*/
	}

protected void postAcmvs300()
	{
		postings310();
	}

protected void postings310()
	{
		/* Read T5688 to find out if contract type wants Component*/
		/* level Accounting.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rclmcalrec.chdrcoy);
		itemIO.setItemtabl(t5688);
		itemIO.setItemitem(rclmcalrec.cnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
		t5688rec.t5688Rec.set(itemIO.getGenarea());
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rclmcalrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError900();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(rclmcalrec.chdrcoy);
		/* MOVE T5645                  TO DESC-DESCTABL.                */
		/* MOVE WSAA-SUBR              TO DESC-DESCITEM.                */
		descIO.setDesctabl(t1688);
		descIO.setDescitem(rclmcalrec.batctrcde);
		descIO.setLanguage(rclmcalrec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			dbError900();
		}
		/* Set up the common fields for all postings*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.batckey.set(rclmcalrec.batckey);
		lifacmvrec.crate.set(wsaaCrate);
		/* MOVE RCLM-RASNUM            TO LIFA-RLDGACCT.                */
		lifacmvrec.rdocnum.set(rclmcalrec.chdrnum);
		lifacmvrec.tranno.set(rclmcalrec.tranno);
		lifacmvrec.rldgcoy.set(rclmcalrec.chdrcoy);
		lifacmvrec.origcurr.set(rclmcalrec.origcurr);
		lifacmvrec.genlcoy.set(rclmcalrec.chdrcoy);
		lifacmvrec.genlcur.set(rclmcalrec.acctcurr);
		lifacmvrec.effdate.set(rclmcalrec.effdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(rclmcalrec.effdate);
		lifacmvrec.transactionTime.set(wsaaTransTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.substituteCode[1].set(rclmcalrec.cnttype);
		lifacmvrec.substituteCode[6].set(rclmcalrec.crtable);
		lifacmvrec.tranref.set(rclmcalrec.chdrnum);
		wsaaRldgChdrnum.set(rclmcalrec.chdrnum);
		wsaaRldgLife.set(rclmcalrec.life);
		wsaaRldgCoverage.set(rclmcalrec.coverage);
		wsaaRldgRider.set(rclmcalrec.rider);
		wsaaPlan.set(rclmcalrec.planSuffix);
		wsaaRldgPlanSuff.set(wsaaPlansuff);
		/*  Create an ACMV for the Recovery Amount*/
		/*  using the details from line #1 for contract & line #2 for*/
		/*  component on T5645.*/
		if (isEQ(rclmcalrec.recovAmt, ZERO)) {
			return ;
		}
		lifacmvrec.origamt.set(rclmcalrec.recovAmt);
		lifacmvrec.acctamt.set(wsaaRecovAmt);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaSub1.set(2);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		else {
			wsaaSub1.set(1);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(rclmcalrec.chdrnum);
		}
		callLifacmv400();
		lifacmvrec.origamt.set(rclmcalrec.recovAmt);
		lifacmvrec.acctamt.set(wsaaRecovAmt);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(rclmcalrec.rasnum);
		wsaaSub1.set(3);
		callLifacmv400();
	}

protected void callLifacmv400()
	{
		/*POSTINGS*/
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserr800();
		}
		/*EXIT*/
	}

protected void callXcvrt500()
	{
		currency510();
	}

protected void currency510()
	{
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(rclmcalrec.effdate);
		conlinkrec.currIn.set(rclmcalrec.origcurr);
		conlinkrec.currOut.set(rclmcalrec.acctcurr);
		conlinkrec.company.set(rclmcalrec.chdrcoy);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr800();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(rclmcalrec.acctcurr);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void syserr800()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rclmcalrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError900()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rclmcalrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(rclmcalrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(rclmcalrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr800();
		}
		/*A000-EXIT*/
	}
}
