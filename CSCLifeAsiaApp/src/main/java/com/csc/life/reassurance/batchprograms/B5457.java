/*
 * File: B5457.java
 * Date: 29 August 2009 21:17:51
 * Author: Quipoz Limited
 * 
 * Class transformed from B5457.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.recordstructures.P5457par;
import com.csc.life.reassurance.reports.R5457Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   Reassurance Costings Report.
*
*   This batch program will report the risk level costings that
*   have occurred within the specified period.
*   The report will be grouped by reassurer, by arrangement,
*   and by currency.
*   Totals will be printed for each currency type within each
*   reassurer within each arrangement.
*   The required RECO and RACD records are selected using SQL.
*                                                             F W
*   The SQL selection criteria are as follows :
*
*         SELECT : CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX,
*                  SEQNO, RASNUM, RNGMNT and CURRCODE from
*                  RACDPF must be equal to RECOPF      AND
*                  RACDPF.VALIDFLAG = '1' or '4'       AND
*                  RECOPF.VALIDFLAG = '1'              AND
*                  P5457-DATETO  >= RECOPF.COSTDATE    AND
*                  P5457-DATEFRM <= RECOPF.COSTDATE
*
*   Initialise
*     - Define the SQL query based on the selection criteria
*       specified aboved.
*
*    Read
*     - Fetch the SQL records.
*
*    Perform    Until End of File
*
*      Edit
*       - Check if it is printing for the first time.
*         If yes, print the report header (R5457H01).
*         If no, check if there is any changes in the Reassurer
*         or Arrangement or Original Currency.
*       - Accumulate PREM01/PREM02 and COMPAY01/COMPAY02.
*
*    End Perform
*
*****************************************************************
* </pre>
*/
public class B5457 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlrecopf1rs = null;
	private java.sql.PreparedStatement sqlrecopf1ps = null;
	private java.sql.Connection sqlrecopf1conn = null;
	private String sqlrecopf1 = "";
	private R5457Report printerFile = new R5457Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5457");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaSqlDatefrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaSqlDateto = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaOrigcurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRngmnt = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaOrigPremTotal = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaOrigCommTotal = new ZonedDecimalData(18, 2);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private String descrec = "DESCREC";
	private String rasarec = "RASAREC";
	private String cltsrec = "CLTSREC";
	private String covrmjarec = "COVRMJAREC";
	private String lifelnbrec = "LIFELNBREC";
		/* ERRORS */
	private String esql = "ESQL";
		/* TABLES */
	private String t5449 = "T5449";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-RECOPF */
	private FixedLengthStringData sqlRecorec = new FixedLengthStringData(71);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlRecorec, 0);
	private FixedLengthStringData sqlRasnum = new FixedLengthStringData(8).isAPartOf(sqlRecorec, 8);
	private FixedLengthStringData sqlRngmnt = new FixedLengthStringData(4).isAPartOf(sqlRecorec, 16);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlRecorec, 20);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlRecorec, 22);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlRecorec, 24);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlRecorec, 26);
	private PackedDecimalData sqlRaamount = new PackedDecimalData(17, 2).isAPartOf(sqlRecorec, 29);
	private FixedLengthStringData sqlOrigcurr = new FixedLengthStringData(3).isAPartOf(sqlRecorec, 38);
	private PackedDecimalData sqlPrem = new PackedDecimalData(17, 2).isAPartOf(sqlRecorec, 41);
	private PackedDecimalData sqlCompay = new PackedDecimalData(17, 2).isAPartOf(sqlRecorec, 50);
	private PackedDecimalData sqlCmdate = new PackedDecimalData(8, 0).isAPartOf(sqlRecorec, 59);
	private PackedDecimalData sqlCostdate = new PackedDecimalData(8, 0).isAPartOf(sqlRecorec, 64);
	private FixedLengthStringData sqlRcstfrq = new FixedLengthStringData(2).isAPartOf(sqlRecorec, 69);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaFoundRecs = new FixedLengthStringData(1).init("N");
	private Validator foundRecs = new Validator(wsaaFoundRecs, "Y");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5457H01 = new FixedLengthStringData(99);
	private FixedLengthStringData r5457h01O = new FixedLengthStringData(99).isAPartOf(r5457H01, 0);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5457h01O, 0);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37).isAPartOf(r5457h01O, 8);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(r5457h01O, 45);
	private FixedLengthStringData rngmntdesc = new FixedLengthStringData(30).isAPartOf(r5457h01O, 49);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10).isAPartOf(r5457h01O, 79);
	private FixedLengthStringData dateto = new FixedLengthStringData(10).isAPartOf(r5457h01O, 89);

	private FixedLengthStringData r5457D01 = new FixedLengthStringData(153);
	private FixedLengthStringData r5457d01O = new FixedLengthStringData(153).isAPartOf(r5457D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5457d01O, 0);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(r5457d01O, 8);
	private FixedLengthStringData clntnm1 = new FixedLengthStringData(37).isAPartOf(r5457d01O, 16);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5457d01O, 53);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5457d01O, 57);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0).isAPartOf(r5457d01O, 67);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(r5457d01O, 70);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5457d01O, 87);
	private FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(r5457d01O, 104);
	private ZonedDecimalData prem = new ZonedDecimalData(17, 2).isAPartOf(r5457d01O, 107);
	private FixedLengthStringData rcstfrq = new FixedLengthStringData(2).isAPartOf(r5457d01O, 124);
	private ZonedDecimalData compay = new ZonedDecimalData(17, 2).isAPartOf(r5457d01O, 126);
	private FixedLengthStringData costdate = new FixedLengthStringData(10).isAPartOf(r5457d01O, 143);

	private FixedLengthStringData r5457T01 = new FixedLengthStringData(37);
	private FixedLengthStringData r5457t01O = new FixedLengthStringData(37).isAPartOf(r5457T01, 0);
	private FixedLengthStringData origcurr1 = new FixedLengthStringData(3).isAPartOf(r5457t01O, 0);
	private ZonedDecimalData prem1 = new ZonedDecimalData(17, 2).isAPartOf(r5457t01O, 3);
	private ZonedDecimalData compay1 = new ZonedDecimalData(17, 2).isAPartOf(r5457t01O, 20);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private P5457par p5457par = new P5457par();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		eof2060, 
		exit2090, 
		arrangement2620, 
		print2680
	}

	public B5457() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		p5457par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaRasnum.set(SPACES);
		wsaaOrigcurr.set(SPACES);
		wsaaRngmnt.set(SPACES);
		wsaaChdrnum.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaOrigPremTotal.set(ZERO);
		wsaaOrigCommTotal.set(ZERO);
		wsaaFirstTime.set("Y");
		wsaaFoundRecs.set("N");
		wsaaOverflow.set("N");
		wsaaSqlDatefrm.set(p5457par.datefrm);
		wsaaSqlDateto.set(p5457par.dateto);
		sqlrecopf1 = " SELECT  RECOPF.CHDRNUM, RECOPF.RASNUM, RECOPF.RNGMNT, RECOPF.LIFE, RECOPF.COVERAGE, RECOPF.RIDER, RECOPF.PLNSFX, RECOPF.RAAMOUNT, RECOPF.ORIGCURR, RECOPF.PREM, RECOPF.COMPAY, RACDPF.CMDATE, RECOPF.COSTDATE, RECOPF.RCSTFRQ" +
" FROM   " + appVars.getTableNameOverriden("RACDPF") + " ,  " + appVars.getTableNameOverriden("RECOPF") + " " +
" WHERE RECOPF.CHDRNUM = RACDPF.CHDRNUM" +
" AND RECOPF.LIFE = RACDPF.LIFE" +
" AND RECOPF.COVERAGE = RACDPF.COVERAGE" +
" AND RECOPF.RIDER = RACDPF.RIDER" +
" AND RECOPF.PLNSFX = RACDPF.PLNSFX" +
" AND RECOPF.SEQNO = RACDPF.SEQNO" +
" AND RECOPF.RASNUM = RACDPF.RASNUM" +
" AND RECOPF.RNGMNT = RACDPF.RNGMNT" +
" AND RECOPF.ORIGCURR = RACDPF.CURRCODE" +
" AND (RACDPF.VALIDFLAG = '1'" +
" OR RACDPF.VALIDFLAG = '4')" +
" AND RECOPF.VALIDFLAG = '1'" +
" AND RECOPF.COSTDATE >= ?" +
" AND RECOPF.COSTDATE <= ?" +
" ORDER BY RECOPF.RASNUM, RECOPF.RNGMNT, RECOPF.CHDRNUM";
		sqlerrorflag = false;
		try {
			sqlrecopf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.reassurance.dataaccess.RacdpfTableDAM(), new com.csc.life.reassurance.dataaccess.RecopfTableDAM()});
			sqlrecopf1ps = appVars.prepareStatementEmbeded(sqlrecopf1conn, sqlrecopf1);
			appVars.setDBDouble(sqlrecopf1ps, 1, wsaaSqlDatefrm.toDouble());
			appVars.setDBDouble(sqlrecopf1ps, 2, wsaaSqlDateto.toDouble());
			sqlrecopf1rs = appVars.executeQuery(sqlrecopf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2060: {
					eof2060();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlrecopf1rs.next()) {
				appVars.getDBObject(sqlrecopf1rs, 1, sqlChdrnum);
				appVars.getDBObject(sqlrecopf1rs, 2, sqlRasnum);
				appVars.getDBObject(sqlrecopf1rs, 3, sqlRngmnt);
				appVars.getDBObject(sqlrecopf1rs, 4, sqlLife);
				appVars.getDBObject(sqlrecopf1rs, 5, sqlCoverage);
				appVars.getDBObject(sqlrecopf1rs, 6, sqlRider);
				appVars.getDBObject(sqlrecopf1rs, 7, sqlPlnsfx);
				appVars.getDBObject(sqlrecopf1rs, 8, sqlRaamount);
				appVars.getDBObject(sqlrecopf1rs, 9, sqlOrigcurr);
				appVars.getDBObject(sqlrecopf1rs, 10, sqlPrem);
				appVars.getDBObject(sqlrecopf1rs, 11, sqlCompay);
				appVars.getDBObject(sqlrecopf1rs, 12, sqlCmdate);
				appVars.getDBObject(sqlrecopf1rs, 13, sqlCostdate);
				appVars.getDBObject(sqlrecopf1rs, 14, sqlRcstfrq);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
		if (foundRecs.isTrue()) {
			printOrigTotal2920();
		}
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		wsaaFoundRecs.set("Y");
		if (firstTime.isTrue()
		|| newPageReq.isTrue()) {
			printHeader2600();
			wsaaRasnum.set(sqlRasnum);
			wsaaRngmnt.set(sqlRngmnt);
			wsaaOrigcurr.set(sqlOrigcurr);
			wsaaFirstTime.set("N");
		}
		else {
			checkForChanges2700();
		}
		wsaaOrigPremTotal.add(sqlPrem);
		wsaaOrigCommTotal.add(sqlCompay);
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(bsprIO.getCompany());
		covrmjaIO.setChdrnum(sqlChdrnum);
		covrmjaIO.setLife(sqlLife);
		covrmjaIO.setPlanSuffix(sqlPlnsfx);
		covrmjaIO.setCoverage(sqlCoverage);
		covrmjaIO.setRider(sqlRider);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.endp)
		&& isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		lifeInformation2800();
		if (isEQ(lifelnbIO.getLifcnum(),wsaaLifenum)
		&& isEQ(sqlChdrnum,wsaaChdrnum)) {
			clntnum.set(SPACES);
			clntnm1.set(SPACES);
		}
		contractInformation2900();
		wsaaChdrnum.set(sqlChdrnum);
		wsaaLifenum.set(lifelnbIO.getLifcnum());
		wsaaCrtable.set(covrmjaIO.getCrtable());
	}

protected void printHeader2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					printHeader2610();
				}
				case arrangement2620: {
					arrangement2620();
				}
				case print2680: {
					print2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void printHeader2610()
	{
		if (isEQ(sqlRasnum,wsaaRasnum)) {
			goTo(GotoLabel.arrangement2620);
		}
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(bupaIO.getCompany());
		rasaIO.setRasnum(sqlRasnum);
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.endp)
		&& isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		datcon1rec.intDate.set(wsaaSqlDatefrm);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datefrm.set(datcon1rec.extDate);
		datcon1rec.intDate.set(wsaaSqlDateto);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dateto.set(datcon1rec.extDate);
		rasnum.set(sqlRasnum);
		plainname();
		clntnm.set(wsspLongconfname);
	}

protected void arrangement2620()
	{
		if (isEQ(sqlRngmnt,wsaaRngmnt)) {
			goTo(GotoLabel.print2680);
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5449);
		descIO.setDescitem(sqlRngmnt);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rngmnt.set(sqlRngmnt);
		rngmntdesc.set(descIO.getLongdesc());
	}

protected void print2680()
	{
		printerFile.printR5457h01(r5457H01);
		wsaaLifenum.set(SPACES);
		/*EXIT*/
	}

protected void checkForChanges2700()
	{
		checkForChanges2710();
	}

protected void checkForChanges2710()
	{
		if (isNE(sqlRasnum,wsaaRasnum)) {
			printOrigTotal2920();
			printHeader2600();
			wsaaOverflow.set("Y");
			wsaaRasnum.set(sqlRasnum);
			wsaaRngmnt.set(sqlRngmnt);
			wsaaOrigcurr.set(sqlOrigcurr);
		}
		else {
			if (isNE(sqlRngmnt,wsaaRngmnt)) {
				printOrigTotal2920();
				printHeader2600();
				wsaaOverflow.set("Y");
				wsaaRngmnt.set(sqlRngmnt);
				wsaaOrigcurr.set(sqlOrigcurr);
			}
			else {
				if (isNE(sqlOrigcurr,wsaaOrigcurr)) {
					printOrigTotal2920();
					printHeader2600();
					wsaaOverflow.set("Y");
					wsaaOrigcurr.set(sqlOrigcurr);
				}
			}
		}
	}

protected void lifeInformation2800()
	{
		lifeInformation2810();
	}

protected void lifeInformation2810()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(bsprIO.getCompany());
		lifelnbIO.setChdrnum(sqlChdrnum);
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		clntnum.set(cltsIO.getClntnum());
		plainname();
		clntnm1.set(wsspLongconfname);
	}

protected void contractInformation2900()
	{
		contractInformation2901();
	}

protected void contractInformation2901()
	{
		if (isEQ(sqlChdrnum,wsaaChdrnum)
		&& !newPageReq.isTrue()) {
			chdrnum.set(SPACES);
		}
		else {
			chdrnum.set(sqlChdrnum);
		}
		//ILIFE-3352 started
		/*if (isEQ(sqlChdrnum,wsaaChdrnum)
		&& isEQ(covrmjaIO.getCrtable(),wsaaCrtable)
		&& !newPageReq.isTrue()) {
			crtable.set(SPACES);
			cmdate.set(SPACES);
			rcestrm.set(ZERO);
			sumins.set(ZERO);
			raamount.set(ZERO);
		}
		else {*/
			crtable.set(covrmjaIO.getCrtable());
			sumins.set(covrmjaIO.getSumins());
			raamount.set(sqlRaamount);
			datcon1rec.intDate.set(sqlCmdate);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			cmdate.set(datcon1rec.extDate);
			rcestrm.set(covrmjaIO.getRiskCessTerm());
			wsaaOverflow.set("N");
		//}
		origcurr.set(sqlOrigcurr);
		prem.set(sqlPrem);
		compay.set(sqlCompay);
		datcon1rec.intDate.set(sqlCostdate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		costdate.set(datcon1rec.extDate);
		rcstfrq.set(sqlRcstfrq);
		printerFile.printR5457d01(r5457D01);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void printOrigTotal2920()
	{
		/*PRINT-ORIG-TOTAL*/
		origcurr1.set(wsaaOrigcurr);
		prem1.set(wsaaOrigPremTotal);
		compay1.set(wsaaOrigCommTotal);
		printerFile.printR5457t01(r5457T01);
		wsaaOrigPremTotal.set(ZERO);
		wsaaOrigCommTotal.set(ZERO);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
