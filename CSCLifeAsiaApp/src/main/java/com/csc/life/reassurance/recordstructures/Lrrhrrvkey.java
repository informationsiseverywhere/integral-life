package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:29
 * Description:
 * Copybook name: LRRHRRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lrrhrrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lrrhrrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lrrhrrvKey = new FixedLengthStringData(64).isAPartOf(lrrhrrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData lrrhrrvClntpfx = new FixedLengthStringData(2).isAPartOf(lrrhrrvKey, 0);
  	public FixedLengthStringData lrrhrrvClntcoy = new FixedLengthStringData(1).isAPartOf(lrrhrrvKey, 2);
  	public FixedLengthStringData lrrhrrvClntnum = new FixedLengthStringData(8).isAPartOf(lrrhrrvKey, 3);
  	public FixedLengthStringData lrrhrrvCompany = new FixedLengthStringData(1).isAPartOf(lrrhrrvKey, 11);
  	public FixedLengthStringData lrrhrrvChdrnum = new FixedLengthStringData(8).isAPartOf(lrrhrrvKey, 12);
  	public FixedLengthStringData lrrhrrvLife = new FixedLengthStringData(2).isAPartOf(lrrhrrvKey, 20);
  	public FixedLengthStringData lrrhrrvCoverage = new FixedLengthStringData(2).isAPartOf(lrrhrrvKey, 22);
  	public FixedLengthStringData lrrhrrvRider = new FixedLengthStringData(2).isAPartOf(lrrhrrvKey, 24);
  	public PackedDecimalData lrrhrrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lrrhrrvKey, 26);
  	public FixedLengthStringData lrrhrrvLrkcls = new FixedLengthStringData(4).isAPartOf(lrrhrrvKey, 29);
  	public PackedDecimalData lrrhrrvTranno = new PackedDecimalData(5, 0).isAPartOf(lrrhrrvKey, 33);
  	public FixedLengthStringData filler = new FixedLengthStringData(28).isAPartOf(lrrhrrvKey, 36, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lrrhrrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lrrhrrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}