package com.csc.life.reassurance.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:39
 * Description:
 * Copybook name: TRMREASREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Trmreasrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData trracalRec = new FixedLengthStringData(129);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(trracalRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(trracalRec, 5);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(trracalRec, 6);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(trracalRec, 14);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(trracalRec, 16);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(trracalRec, 18);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(trracalRec, 20);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(trracalRec, 23);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(trracalRec, 26);
  	public FixedLengthStringData batckey = new FixedLengthStringData(22).isAPartOf(trracalRec, 31);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(trracalRec, 53);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(trracalRec, 56);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(trracalRec, 59);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(trracalRec, 61);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(trracalRec, 66);
  	public FixedLengthStringData acctcurr = new FixedLengthStringData(3).isAPartOf(trracalRec, 69);
  	public FixedLengthStringData crtable = new FixedLengthStringData(5).isAPartOf(trracalRec, 72);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(trracalRec, 77);
  	public PackedDecimalData convUnits = new PackedDecimalData(8, 0).isAPartOf(trracalRec, 82);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(trracalRec, 87);
  	public PackedDecimalData singp = new PackedDecimalData(17, 2).isAPartOf(trracalRec, 89);
  	public PackedDecimalData oldSumins = new PackedDecimalData(17, 2).isAPartOf(trracalRec, 98);
  	public PackedDecimalData newSumins = new PackedDecimalData(17, 2).isAPartOf(trracalRec, 107);
  	public PackedDecimalData clmPercent = new PackedDecimalData(5, 2).isAPartOf(trracalRec, 116);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(1).isAPartOf(trracalRec, 119);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(trracalRec, 120);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(trracalRec, 124);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(trracalRec, 125);


	public void initialize() {
		COBOLFunctions.initialize(trracalRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		trracalRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}