package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5438screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {6, 21, 5, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5438ScreenVars sv = (S5438ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5438screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5438ScreenVars screenVars = (S5438ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.comt.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.rngmnt.setClassString("");
		screenVars.cmdateDisp.setClassString("");
		screenVars.ctdateDisp.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.raAmount.setClassString("");
		screenVars.rngmntdesc.setClassString("");
		screenVars.clntname.setClassString("");  // ILIFE-6026 cltnme is changed to clntname, refer DDFieldWindowing of rasnum
	}

/**
 * Clear all the variables in S5438screen
 */
	public static void clear(VarModel pv) {
		S5438ScreenVars screenVars = (S5438ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.comt.clear();
		screenVars.rasnum.clear();
		screenVars.rngmnt.clear();
		screenVars.cmdateDisp.clear();
		screenVars.cmdate.clear();
		screenVars.ctdateDisp.clear();
		screenVars.ctdate.clear();
		screenVars.sumins.clear();
		screenVars.raAmount.clear();
		screenVars.rngmntdesc.clear();
		screenVars.clntname.clear();  // ILIFE-6026 cltnme is changed to clntname, refer DDFieldWindowing of rasnum
	}
}
