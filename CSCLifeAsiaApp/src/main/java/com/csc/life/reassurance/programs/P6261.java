/*
 * File: P6261.java
 * Date: 30 August 2009 0:41:18
 * Author: Quipoz Limited
 *
 * Class transformed from P6261.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S6261ScreenVars;
import com.csc.life.reassurance.tablestructures.T5632rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
**
** P6261 - Reassurance Maintenance - Details.
** ******************************************
**
** This program will perform the addition, modification and
** enquiry functions on the Reassurance Account details file
** RASA.
**
** Initialise.
** ***********
**
** Skip this section if returning from an optional selection
** further down the stack, (current stack position action flag
** = '*').
**
** The Reassurance Account details will be held in the RASA
** I/O module. Perform a RETRV on RASA to obtain the details.
** If creation is in progress, (WSSP-FLAG = 'C'), all that
** will be present will be the Reassurance Number. Set this on
** the screen and set the branch number from WSSP and obtain
** its description from T1692.
**
** For modification, (WSSP-FLAG = 'M') and enquiry, (WSSP-FLAG
** = 'I'), display all the fields along with all descriptions
** where necessary. Use the copybook section CONFNAME to
** reformat the client names for display. Use the Bank Number
** and Bank Account Number to read CLBL and obtain the bank
** details. Obtain the description of the Bank/Branch from
** BABR.
**
** For enquiry protect all the fields on the screen except for
** the Client Details and bank details indicator.
**
** Validation.
** ***********
**
** Skip this section if returning from an optional selection
** further down the stack, (current stack position action flag
** = '*').
**
** Call the screen I/O module to write and read the screen.
** Validate the fields according to the rules defined by the
** field help with the following additions:
**
**      Client Number - read CLNT and reformat the name.
**
**      Branch Number - use the Client Branch and re-read the
**      branch description from T1692.
**
**
**      Client Details Indicator - may be 'X', '+' or blank.
**
**      Bank Account details Indicator may be 'X' or '+'.
**        If the method of payment T5632 says bank account details
**        are required then force switching to see/enter them, ie set
**        the indicator to 'X'.
**        If the method of payment says bank details are NOT required
**        and the user  has selected and entered them on the following
**        screen, remove the bank details from RASAPF.
**
**
** For all fields which have descriptions look them up again.
** If the refresh key was pressed, (CALC), re-display the screen.
**
** Updating.
** *********
**
** Skip this section if returning from an optional selection
** further down the stack, (current stack position action flag
** = '*').
**
** Release the RASA record with a function of 'RLSE',move all
** fields to the RASA record and write or rewrite the record
** as appropriate.
**
** Next Program.
** *************
**
** If returning from an optional selection further down the
** stack, (current stack position action flag = '*'), then
** move spaces to the current stack position action field and
** restore the next 8 programs in the program stack. Check the
** Client Details indicator. If this is '?' overwrite it with
** '+'.
**
** If not returning from an optional selection further down
** the stack check the Client Details indicator. If this is
** 'X' then the user has opted to view the relevant client
** details screen. If so replace the Client Details Indicator
** with '?' and call GENSSW with an action of 'A'. Store the
** next 8 programs from the program stack in Working Storage
** and load the next 8 programs returned from GENSSW in their
** place.
**
** If control is not going on to the Client Details screen
** then the Reassurance Number should be unlocked. If the
** Client Detials indicator is not '?' call SFTLOCK with the
** following values:
**
**           SFTL-FUNCTION     -    'UNLK'
**           SFTL-COMPANY      -    WSSP-COMPANY
**           SFTL-ENTTYP       -    'RA'
**           SFTL-ENTITY       -    RASNUM
**           all other fields  -    spaces.
**
** On return from SFTLOCK if the status is not O-K perform
** fatal error processing.
**
** Add 1 to the program pointer and exit.
**
*****************************************************************
* </pre>
*/
public class P6261 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6261");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaWsspFlag = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbBankBranchDesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankDesc = new FixedLengthStringData(30).isAPartOf(wsbbBankBranchDesc, 0);
	private FixedLengthStringData wsbbBranchDesc = new FixedLengthStringData(30).isAPartOf(wsbbBankBranchDesc, 30);
	private FixedLengthStringData wsbbOldClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbOldPayee = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbClntsel = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbMop = new FixedLengthStringData(1);
	private FixedLengthStringData wsbbPayfrq = new FixedLengthStringData(2);
	private FixedLengthStringData wsbbBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsbbCurrcode = new FixedLengthStringData(3);
	private FixedLengthStringData wsbbBankacckey = new FixedLengthStringData(20);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsbbFunction = new FixedLengthStringData(5);
	private FixedLengthStringData wsbbReasst = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbClientIsPayee = new FixedLengthStringData(1);
	private Validator clientIsPayee = new Validator(wsbbClientIsPayee, "Y");

	private FixedLengthStringData wsbbClientType = new FixedLengthStringData(1);
	private Validator clientIsPersonal = new Validator(wsbbClientType, "Y");

	private FixedLengthStringData wsbbClientKey = new FixedLengthStringData(12);
	private FixedLengthStringData wsbbClntpfx = new FixedLengthStringData(2).isAPartOf(wsbbClientKey, 0);
	private FixedLengthStringData wsbbClntcoy = new FixedLengthStringData(1).isAPartOf(wsbbClientKey, 2);
	private FixedLengthStringData wsbbClntnum = new FixedLengthStringData(8).isAPartOf(wsbbClientKey, 3);

	private FixedLengthStringData filler1 = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsbbSecProg = FLSArrayPartOfStructure(8, 5, filler1, 0);
		/* ERRORS */
	private String e034 = "E034";
	private String e058 = "E058";
	private String e186 = "E186";
	private String e305 = "E305";
	private String f168 = "F168";
	private String f188 = "F188";
	private String f199 = "F199";
	private String g438 = "G438";
	private String g439 = "G439";
	private String f691 = "F691";
	private String g709 = "G709";
	private String f019 = "F019";
	private String f020 = "F020";
	private String f021 = "F021";
	private String f393 = "F393";
	private String e329 = "E329";
	private String e128 = "E128";
	private String e925 = "E925";
	private String f023 = "F023";
	private String f405 = "F405";
	private String f983 = "F983";
	private String f955 = "F955";
	private String r070 = "R070";
		/* TABLES */
	private String t1692 = "T1692";
	private String t5632 = "T5632";
	private String t5633 = "T5633";
	private String t3629 = "T3629";
	private String t5580 = "T5580";
	private String t5455 = "T5455";
		/* FORMATS */
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String cltsrec = "CLTSREC";
	private String agntrec = "AGNTREC";
	private String babrrec = "BABRREC";
	private String clblrec = "CLBLREC";
	private String rasarec = "RASAREC";
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Gensswrec gensswrec = new Gensswrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5632rec t5632rec = new T5632rec();
	private Cltskey wsaaCltskey = new Cltskey();
	private Batckey wsbbBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6261ScreenVars sv = ScreenProgram.getScreenVars( S6261ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1190,
		lgnmExit,
		plainExit,
		payeeExit,
		exit1269,
		exit1599,
		exit1699,
		preExit,
		exit2090,
		exit2199,
		exit2209,
		exit2219,
		exit2599,
		exit2899,
		exit3090,
		writeReassClrr3202,
		para3203,
		gensww4010,
		nextProgram4020,
		exit4090
	}

	public P6261() {
		super();
		screenVars = sv;
		new ScreenModel("S6261", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1100();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1100()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1190);
		}
		wsbbBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		descIO.setParams(SPACES);
		wsbbClientIsPayee.set(SPACES);
		wsbbMop.set(SPACES);
		wsbbPayfrq.set(SPACES);
		wsbbCurrcode.set(SPACES);
		wsbbClntsel.set(SPACES);
		wsbbOldClntnum.set(SPACES);
		wsbbReasst.set(SPACES);
		wsbbOldPayee.set(SPACES);
		sv.minsta.set(ZERO);
		sv.commdt.set(varcom.vrcmMaxDate);
		sv.termdt.set(varcom.vrcmMaxDate);
		sv.stmtdt.set(varcom.vrcmMaxDate);
		sv.nxtpay.set(varcom.vrcmMaxDate);
		wsbbFunction.set(varcom.retrv);
		callRasaioModule1800();
		wsbbFunction.set(varcom.rlse);
		callRasaioModule1800();
		wsbbFunction.set(SPACES);
		sv.rasnum.set(rasaIO.getRasnum());
		if (isEQ(wsspcomn.flag,"C")) {
			sv.termdtOut[varcom.pr.toInt()].set("Y");
			sv.nxtpayOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.exit1190);
		}
		outputRasaDetails1200();
		if (isEQ(wsspcomn.flag,"M")) {
			verifyPaymthT56321265();
			if (isEQ(t5632rec.bankreq,"Y")) {
				sv.ddind.set("X");
			}
			else {
				if (isEQ(t5632rec.bankreq,"N")
				&& isNE(sv.bankkey,SPACES)) {
					scrnparams.errorCode.set(g439);
				}
			}
		}
		if (isNE(sv.bankkey,SPACES)) {
			getAndDispBankaccdesc1210();
			getAndDispBkBrDet1250();
		}
		getAndDispClientName1230();
		getAndDispBranchDet1240();
		if (clientIsPayee.isTrue()) {
			sv.payenme.set(sv.cltname);
		}
		else {
			cltsIO.setClntnum(sv.rapayee);
			getAndDispPayeeName1220();
		}
		getAndDispPymdescDet1260();
		getAndDispPyfdescDet1270();
		getAndDispCurrDet1280();
		getAndDispAgent1285();
		if (isNE(sv.reasst,SPACES)) {
			getReasstDesc1295();
		}
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.row.set(23);
			scrnparams.column.set(20);
			protectScreen1290();
			if (isNE(sv.bankkey,SPACES)) {
				sv.ddind.set("+");
			}
		}
		if (isEQ(wsspcomn.flag,"M")) {
			sv.commdtOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void outputRasaDetails1200()
	{
		para1201();
	}

protected void para1201()
	{
		sv.clntsel.set(rasaIO.getClntnum());
		wsbbClntsel.set(rasaIO.getClntnum());
		wsbbOldClntnum.set(rasaIO.getClntnum());
		sv.currcode.set(rasaIO.getCurrcode());
		wsbbCurrcode.set(rasaIO.getCurrcode());
		sv.rapaymth.set(rasaIO.getRapaymth());
		wsbbMop.set(rasaIO.getRapaymth());
		sv.rapayfrq.set(rasaIO.getRapayfrq());
		wsbbPayfrq.set(rasaIO.getRapayfrq());
		wsbbOldPayee.set(rasaIO.getPayclt());
		sv.rapayee.set(rasaIO.getPayclt());
		cltsIO.setClntnum(rasaIO.getPayclt());
		if (isEQ(rasaIO.getPayclt(),rasaIO.getClntnum())) {
			wsbbClientIsPayee.set("Y");
		}
		sv.agntsel.set(rasaIO.getAgntnum());
		sv.minsta.set(rasaIO.getMinsta());
		sv.bankkey.set(rasaIO.getBankkey());
		sv.bankacckey.set(rasaIO.getBankacckey());
		sv.commdt.set(rasaIO.getCommdt());
		sv.termdt.set(rasaIO.getTermdt());
		sv.reasst.set(rasaIO.getReasst());
		wsbbReasst.set(rasaIO.getReasst());
		sv.stmtdt.set(rasaIO.getStmtdt());
		sv.nxtpay.set(rasaIO.getNxtpay());
	}

protected void getAndDispBankaccdesc1210()
	{
		/*PARA*/
		clblIO.setDataArea(SPACES);
		clblIO.setBankkey(rasaIO.getBankkey());
		sv.bankkey.set(rasaIO.getBankkey());
		babrIO.setBankkey(rasaIO.getBankkey());
		clblIO.setBankacckey(rasaIO.getBankacckey());
		sv.bankacckey.set(rasaIO.getBankacckey());
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.clntsel);
		callClblioModule1400();
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
		/*EXIT*/
	}

protected void getAndDispPayeeName1220()
	{
		/*PARA*/
		wsbbFunction.set(varcom.readr);
		callCltsioModule1500();
		plainname();
		sv.payenme.set(wsspcomn.longconfname);
		/*EXIT*/
	}

protected void getAndDispClientName1230()
	{
		para1231();
	}

protected void para1231()
	{
		wsbbClientIsPayee.set(SPACES);
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(rasaIO.getClntnum());
		sv.clientind.set(SPACES);
		wsbbFunction.set(varcom.readr);
		callCltsioModule1500();
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
		if (isEQ(cltsIO.getClttype(),"P")) {
			wsbbClientType.set("Y");
		}
		else {
			wsbbClientType.set("N");
		}
	}

protected void getAndDispBranchDet1240()
	{
		/*PARA*/
		sv.branch.set(cltsIO.getServbrh());
		descIO.setDescitem(cltsIO.getServbrh());
		descIO.setDesctabl(t1692);
		callDescioModule1700();
		sv.branchnm.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void getAndDispBkBrDet1250()
	{
		/*PARA*/
		wsbbFunction.set(varcom.readr);
		callBabrioModule1600();
		wsbbBankBranchDesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankDesc);
		sv.branchdesc.set(wsbbBranchDesc);
		/*EXIT*/
	}

protected void getAndDispPymdescDet1260()
	{
		/*PARA*/
		descIO.setDataKey(SPACES);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5632);
		descIO.setDescitem(rasaIO.getRapaymth());
		callDescioModule1700();
		sv.paymthd.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void verifyPaymthT56321265()
	{
		try {
			para1266();
		}
		catch (GOTOException e){
		}
	}

protected void para1266()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(t5632);
		itemIO.setItemitem(sv.rapaymth);
		callItemioModule1300();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.rapaymthErr.set(f168);
		}
		if (isNE(itemIO.getValidflag(),"1")) {
			sv.rapaymthErr.set(e034);
		}
		t5632rec.t5632Rec.set(itemIO.getGenarea());
		if (isNE(t5632rec.bankreq,"Y")
		&& isNE(t5632rec.bankreq,"N")) {
			sv.rapaymthErr.set(g438);
			goTo(GotoLabel.exit1269);
		}
	}

protected void getAndDispPyfdescDet1270()
	{
		/*PARA*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5633);
		descIO.setDescitem(rasaIO.getRapayfrq());
		callDescioModule1700();
		sv.rapayfrqd.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void getAndDispCurrDet1280()
	{
		/*PARA*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(rasaIO.getCurrcode());
		callDescioModule1700();
		sv.currdesc.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void getAndDispAgent1285()
	{
		para1285();
	}

protected void para1285()
	{
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(rasaIO.getAgntnum());
		agntIO.setFormat(agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntIO.getClntnum());
		wsbbFunction.set(varcom.readr);
		callCltsioModule1500();
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void protectScreen1290()
	{
		/*PARA*/
		sv.bankacckeyOut[varcom.pr.toInt()].set("Y");
		sv.bankkeyOut[varcom.pr.toInt()].set("Y");
		sv.clntselOut[varcom.pr.toInt()].set("Y");
		sv.rapayeeOut[varcom.pr.toInt()].set("Y");
		sv.agntselOut[varcom.pr.toInt()].set("Y");
		sv.minstaOut[varcom.pr.toInt()].set("Y");
		sv.commdtOut[varcom.pr.toInt()].set("Y");
		sv.termdtOut[varcom.pr.toInt()].set("Y");
		sv.reasstOut[varcom.pr.toInt()].set("Y");
		sv.nxtpayOut[varcom.pr.toInt()].set("Y");
		sv.rapayfrqOut[varcom.pr.toInt()].set("Y");
		sv.rapaymthOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void getReasstDesc1295()
	{
		/*PARA*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5455);
		descIO.setDescitem(sv.reasst);
		callDescioModule1700();
		sv.reastdesc.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void callItemioModule1300()
	{
		/*PARA*/
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callClblioModule1400()
	{
		/*PARA*/
		clblIO.setFormat(clblrec);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callCltsioModule1500()
	{
		try {
			para1501();
		}
		catch (GOTOException e){
		}
	}

protected void para1501()
	{
		cltsIO.setFormat(cltsrec);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		if (isNE(wsbbFunction,varcom.readr)) {
			cltsIO.setFunction(wsbbFunction);
		}
		else {
			cltsIO.setFunction(varcom.readr);
		}
		SmartFileCode.execute(appVars, cltsIO);
		if (isEQ(wsbbFunction,varcom.readr)) {
			if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.exit1599);
			}
		}
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void callBabrioModule1600()
	{
		try {
			para1600();
		}
		catch (GOTOException e){
		}
	}

protected void para1600()
	{
		babrIO.setFormat(babrrec);
		if (isNE(wsbbFunction,"READR")) {
			babrIO.setFunction(wsbbFunction);
		}
		else {
			babrIO.setFunction(varcom.readr);
		}
		SmartFileCode.execute(appVars, babrIO);
		if (isEQ(wsbbFunction,varcom.readr)) {
			if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.exit1699);
			}
		}
		if (isNE(babrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
	}

protected void callDescioModule1700()
	{
		/*PARA*/
		descIO.setLongdesc(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callRasaioModule1800()
	{
		/*PARA*/
		if (isEQ(wsbbFunction,SPACES)) {
			rasaIO.setFunction(varcom.readr);
		}
		else {
			rasaIO.setFunction(wsbbFunction);
		}
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)
		&& isNE(rasaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(sv.rapayee,SPACES)) {
			sv.numsel.set(sv.rapayee);
		}
		else {
			sv.numsel.set(sv.clntsel);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.clientind,SPACES)
		&& isNE(sv.clientind,"+")
		&& isNE(sv.clientind,"X")) {
			sv.clientindErr.set(g709);
		}
		if (isNE(sv.ddind,SPACES)
		&& isNE(sv.ddind,"+")
		&& isNE(sv.ddind,"X")) {
			sv.ddindErr.set(e186);
		}
		validateAllFields2100();
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateAllFields2100()
	{
		try {
			validateAllFields2101();
		}
		catch (GOTOException e){
		}
	}

protected void validateAllFields2101()
	{
		if (isEQ(sv.commdt,varcom.vrcmMaxDate)
		&& isEQ(wsspcomn.flag,"C")) {
			sv.commdtErr.set(e186);
		}
		if (isNE(sv.commdt,varcom.vrcmMaxDate)
		&& isLTE(sv.termdt,sv.commdt)) {
			sv.termdtErr.set(r070);
		}
		if (isEQ(sv.reasst,SPACES)) {
			sv.reasstErr.set(e186);
		}
		if (isNE(sv.reasst,wsbbReasst)) {
			getReasstDesc1295();
		}
		if (isEQ(sv.clntsel,SPACES)) {
			sv.clntselErr.set(f691);
			sv.cltname.set(SPACES);
		}
		if (isEQ(sv.clntselErr,SPACES)) {
			if (isNE(sv.clntsel,wsbbClntsel)) {
				chgClient2120();
			}
			if (isEQ(sv.rapayee,SPACES)) {
				sv.rapayee.set(sv.clntsel);
			}
			if (isNE(sv.rapayee,wsbbOldPayee)) {
				if (isEQ(wsbbOldPayee,SPACES)) {
					if (isEQ(sv.rapayee,sv.clntsel)) {
						wsbbOldPayee.set(sv.rapayee);
					}
					else {
						sv.rapayeeOut[varcom.chg.toInt()].set("Y");
					}
				}
				else {
					sv.rapayeeOut[varcom.chg.toInt()].set("Y");
					scrnparams.errorCode.set(f019);
				}
			}
		}
		if (isEQ(sv.rapaymth,SPACES)) {
			sv.rapaymthErr.set(f020);
			sv.paymthd.set(SPACES);
			wsbbMop.set(SPACES);
		}
		else {
			if (isNE(sv.rapaymth,wsbbMop)) {
				chgMop2200();
			}
		}
		if (isEQ(sv.rapayfrq,SPACES)) {
			sv.rapayfrqErr.set(f021);
			sv.rapayfrqd.set(SPACES);
			wsbbPayfrq.set(SPACES);
		}
		else {
			if (isNE(sv.rapayfrq,wsbbPayfrq)) {
				chgPayfrq2210();
			}
		}
		if (isEQ(sv.rapayfrqErr,SPACES)
		&& isEQ(sv.rapaymthErr,SPACES)) {
			valMethFreq2220();
		}
		if (isNE(sv.commdt,varcom.vrcmMaxDate)
		&& isNE(sv.rapayfrq,SPACES)
		&& isEQ(sv.rapayfrqErr,SPACES)
		//MIBT-63
		&& isEQ(wsspcomn.flag,"C") || isEQ(wsspcomn.flag,"M")) {
			sv.stmtdt.set(sv.commdt);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(sv.commdt);
			datcon2rec.intDate2.set(0);
			datcon2rec.frequency.set(sv.rapayfrq);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			sv.nxtpay.set(datcon2rec.intDate2);
		}
		if (isEQ(sv.rapayeeOut[varcom.chg.toInt()],"Y")) {
			if (isNE(sv.rapayee,sv.clntsel)) {
				chgRapayee2500();
			}
			wsbbOldPayee.set(sv.rapayee);
		}
		if (isNE(sv.bankkey,SPACES)) {
			if (isNE(sv.rapayee,clblIO.getClntnum())) {
				if (isEQ(sv.rapaymth,wsbbMop)) {
					if (isEQ(t5632rec.bankreq,"Y")) {
						sv.ddind.set("X");
					}
				}
			}
		}
		else {
			if (isEQ(sv.rapaymth,wsbbMop)) {
				if (isEQ(t5632rec.bankreq,"Y")) {
					sv.ddind.set("X");
				}
			}
		}
		if (isEQ(sv.bankkey,SPACES)) {
			if (isEQ(sv.currcode,SPACES)) {
				sv.currcodeErr.set(f405);
			}
			else {
				if (isNE(sv.currcode,SPACES)) {
					chgCurrcode2450();
				}
			}
		}
		if (isNE(sv.currcode,SPACES)
		&& isNE(sv.bankkey,SPACES)
		&& isEQ(sv.bankkeyErr,SPACES)
		&& isNE(sv.bankacckey,SPACES)
		&& isEQ(sv.bankacckeyErr,SPACES)) {
			checkCurrcode2800();
		}
		if (isEQ(sv.currcode,SPACES)
		&& isNE(sv.bankkey,SPACES)) {
			sv.currcode.set(clblIO.getCurrcode());
			checkCurrcode2800();
		}
		if (isEQ(sv.currcodeErr,SPACES)
		&& isNE(sv.currcode,wsbbCurrcode)) {
			chgCurrcode2450();
		}
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.agntsel);
		agntIO.setFormat(agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isEQ(agntIO.getStatuz(),varcom.mrnf)) {
			sv.agentname.set(SPACES);
			sv.agntselErr.set(e305);
			goTo(GotoLabel.exit2199);
		}
		if (isNE(agntIO.getLifagnt(),"Y")) {
			sv.agentname.set(SPACES);
			sv.agntselErr.set(f188);
			goTo(GotoLabel.exit2199);
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntIO.getClntnum());
		wsbbFunction.set(varcom.readr);
		callCltsioModule1500();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.agentname.set(SPACES);
			sv.agntselErr.set(e058);
			goTo(GotoLabel.exit2199);
		}
		if (isNE(cltsIO.getValidflag(),"1")) {
			sv.agentname.set(SPACES);
			sv.agntselErr.set(e329);
		}
		if (isEQ(sv.agntselErr,SPACES)) {
			plainname();
			sv.agentname.set(wsspcomn.longconfname);
		}
	}

protected void chgClient2120()
	{
		para2121();
	}

protected void para2121()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(sv.clntsel);
		wsbbFunction.set(varcom.readr);
		callCltsioModule1500();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.cltname.set(SPACES);
			wsbbClntsel.set(SPACES);
			sv.branch.set(SPACES);
			sv.branchnm.set(SPACES);
			sv.clntselErr.set(f393);
		}
		if (isNE(cltsIO.getValidflag(),"1")) {
			sv.clntselErr.set(e329);
		}
		if (isEQ(sv.clntselErr,SPACES)) {
			plainname();
			sv.cltname.set(wsspcomn.longconfname);
			wsbbClntsel.set(sv.clntsel);
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(t1692);
			descIO.setDescitem(cltsIO.getServbrh());
			sv.branch.set(cltsIO.getServbrh());
			callDescioModule1700();
			sv.branchnm.set(descIO.getLongdesc());
		}
		if (isEQ(cltsIO.getClttype(),"P")) {
			wsbbClientType.set("Y");
		}
		else {
			wsbbClientType.set("N");
		}
	}

protected void chgMop2200()
	{
		try {
			para2201();
		}
		catch (GOTOException e){
		}
	}

protected void para2201()
	{
		verifyPaymthT56321265();
		if (isEQ(t5632rec.bankreq,"Y")) {
			sv.ddind.set("X");
		}
		else {
			sv.bankkey.set(SPACES);
			rasaIO.setBankkey(SPACES);
			sv.bankacckey.set(SPACES);
			rasaIO.setBankacckey(SPACES);
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5632);
		descIO.setDescitem(sv.rapaymth);
		callDescioModule1700();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.rapaymthErr.set(e128);
			descIO.setStatuz(SPACES);
			goTo(GotoLabel.exit2209);
		}
		sv.paymthd.set(descIO.getLongdesc());
		wsbbMop.set(sv.rapaymth);
	}

protected void chgPayfrq2210()
	{
		try {
			para2211();
		}
		catch (GOTOException e){
		}
	}

protected void para2211()
	{
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5633);
		descIO.setDescitem(sv.rapayfrq);
		callDescioModule1700();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.rapayfrqErr.set(e925);
			goTo(GotoLabel.exit2219);
		}
		sv.rapayfrqd.set(descIO.getLongdesc());
		wsbbPayfrq.set(sv.rapayfrq);
	}

protected void valMethFreq2220()
	{
		/*PARA*/
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(t5580);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sv.rapaymth.toString());
		stringVariable1.append(sv.rapayfrq.toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		callItemioModule1300();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.rapaymthErr.set(f199);
			sv.rapayfrqErr.set(f199);
		}
		if (isNE(itemIO.getValidflag(),"1")) {
			sv.rapaymthErr.set(f199);
			sv.rapayfrqErr.set(f199);
		}
		/*EXIT*/
	}

protected void chgCurrcode2450()
	{
		/*PARA*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sv.currcode);
		callDescioModule1700();
		sv.currdesc.set(descIO.getLongdesc());
		wsbbCurrcode.set(sv.currcode);
		/*EXIT*/
	}

protected void chgRapayee2500()
	{
		try {
			para2501();
		}
		catch (GOTOException e){
		}
	}

protected void para2501()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(sv.rapayee);
		cltsIO.setClntcoy(rasaIO.getClntcoy());
		cltsIO.setClntpfx(clblIO.getClntpfx());
		wsbbFunction.set(varcom.readr);
		callCltsioModule1500();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.rapayeeErr.set(f393);
			goTo(GotoLabel.exit2599);
		}
		if (isNE(cltsIO.getValidflag(),"1")) {
			sv.rapayeeErr.set(f023);
			goTo(GotoLabel.exit2599);
		}
		plainname();
		sv.payenme.set(wsspcomn.longconfname);
	}

protected void checkCurrcode2800()
	{
		try {
			para2801();
		}
		catch (GOTOException e){
		}
	}

protected void para2801()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescitem(sv.currcode);
		wsbbCurrcode.set(sv.currcode);
		descIO.setDesctabl(t3629);
		callDescioModule1700();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.currdesc.set(SPACES);
			sv.currcodeErr.set(f955);
			goTo(GotoLabel.exit2899);
		}
		if (isNE(sv.currcode,clblIO.getCurrcode())) {
			sv.currdesc.set(SPACES);
			sv.currcodeErr.set(f983);
			goTo(GotoLabel.exit2899);
		}
		sv.currdesc.set(descIO.getLongdesc());
	}

protected void update3000()
	{
		try {
			para3101();
		}
		catch (GOTOException e){
		}
	}

protected void para3101()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.ddind,"X")) {
			selectKeeps3800();
		}
		if (isNE(sv.clientind,"X")) {
			if (isNE(sv.ddind,"X")) {
				if (isNE(wsspcomn.flag,"I")) {
					noSelectUpdate3200();
					unlkSoftLock3270();
				}
			}
		}
		wsaaCltskey.cltsClntpfx.set("CN");
		wsaaCltskey.cltsClntcoy.set(wsspcomn.fsuco);
		wsaaCltskey.cltsClntnum.set(sv.clntsel);
		wsspcomn.clntkey.set(wsaaCltskey);
	}

protected void selectKeeps3100()
	{
		/*PARA*/
		cltsIO.setClntnum(sv.clntsel);
		cltsIO.setServbrh(sv.branch);
		wsbbFunction.set(varcom.keeps);
		callCltsioModule1500();
		wsbbFunction.set(SPACES);
		/*EXIT*/
	}

protected void noSelectUpdate3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					clrrReassurerUpdte3201();
				}
				case writeReassClrr3202: {
					writeReassClrr3202();
				}
				case para3203: {
					para3203();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void clrrReassurerUpdte3201()
	{
		if (isEQ(wsbbOldClntnum,sv.clntsel)) {
			goTo(GotoLabel.para3203);
		}
		cltrelnrec.clrrrole.set("RL");
		if (isEQ(wsbbOldClntnum,SPACES)) {
			goTo(GotoLabel.writeReassClrr3202);
		}
		if (isEQ(wsspcomn.flag,"M")) {
			cltrelnrec.function.set("REM  ");
		}
		else {
			cltrelnrec.function.set("DEL  ");
		}
		cltrelnrec.clntnum.set(wsbbOldClntnum);
		clrrioCall3700();
	}

protected void writeReassClrr3202()
	{
		cltrelnrec.clntnum.set(sv.clntsel);
		cltrelnrec.function.set("ADD  ");
		clrrioCall3700();
	}

protected void para3203()
	{
		rasaIO.setDtepay(ZERO);
		rasaIO.setClntnum(sv.clntsel);
		rasaIO.setClntcoy(wsspcomn.fsuco);
		rasaIO.setRascoy(wsspcomn.company);
		rasaIO.setRasnum(sv.rasnum);
		rasaIO.setRapaymth(sv.rapaymth);
		rasaIO.setRapayfrq(sv.rapayfrq);
		rasaIO.setCurrcode(sv.currcode);
		rasaIO.setPayclt(sv.rapayee);
		rasaIO.setFacthous(sv.facthous);
		rasaIO.setAgntnum(sv.agntsel);
		rasaIO.setMinsta(sv.minsta);
		rasaIO.setCommdt(sv.commdt);
		rasaIO.setTermdt(sv.termdt);
		rasaIO.setReasst(sv.reasst);
		rasaIO.setStmtdt(sv.stmtdt);
		rasaIO.setNxtpay(sv.nxtpay);
		wsbbFunction.set(varcom.updat);
		callRasaioModule1800();
		wsbbFunction.set(SPACES);
	}

protected void unlkSoftLock3270()
	{
		/*PARA*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(rasaIO.getRasnum());
		sftlockrec.enttyp.set("RA");
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void clrrioCall3700()
	{
		/*CALL*/
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.forepfx.set("RL");
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(sv.rasnum);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void selectKeeps3800()
	{
		para3801();
	}

protected void para3801()
	{
		rasaIO.setDtepay(ZERO);
		rasaIO.setClntnum(sv.clntsel);
		rasaIO.setClntcoy(wsspcomn.fsuco);
		rasaIO.setRascoy(wsspcomn.company);
		rasaIO.setRasnum(sv.rasnum);
		rasaIO.setRapaymth(sv.rapaymth);
		rasaIO.setRapayfrq(sv.rapayfrq);
		rasaIO.setCurrcode(sv.currcode);
		rasaIO.setBankkey(sv.bankkey);
		rasaIO.setAgntnum(sv.agntsel);
		rasaIO.setMinsta(sv.minsta);
		rasaIO.setPayclt(sv.rapayee);
		rasaIO.setCommdt(sv.commdt);
		rasaIO.setTermdt(sv.termdt);
		rasaIO.setReasst(sv.reasst);
		rasaIO.setStmtdt(sv.stmtdt);
		rasaIO.setNxtpay(sv.nxtpay);
		wsbbFunction.set(varcom.keeps);
		callRasaioModule1800();
		wsbbFunction.set(SPACES);
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4000();
				}
				case gensww4010: {
					gensww4010();
				}
				case nextProgram4020: {
					nextProgram4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		gensswrec.function.set(SPACES);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.clientind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsbbBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
			sv.clientind.set("?");
			wsaaWsspFlag.set(wsspcomn.flag);
			wsspcomn.flag.set("I");
			if (clientIsPersonal.isTrue()) {
				gensswrec.function.set("A");
			}
			else {
				gensswrec.function.set("C");
				wsbbClntpfx.set(cltsIO.getClntpfx());
				wsbbClntcoy.set(cltsIO.getClntcoy());
				wsbbClntnum.set(sv.clntsel);
				wsspcomn.clntkey.set(wsbbClientKey);
			}
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.clientind,"?")) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			wsspcomn.flag.set(wsaaWsspFlag);
			sv.clientind.set("+");
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.ddind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsbbBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
				saveProgramStack4200();
			}
			sv.ddind.set("?");
			gensswrec.function.set("B");
			wsbbFunction.set(varcom.keeps);
			callRasaioModule1800();
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.ddind,"?")) {
			wsbbFunction.set(varcom.retrv);
			callRasaioModule1800();
			wsspcomn.nextprog.set(scrnparams.scrname);
			sv.ddind.set("+");
			wsbbFunction.set(varcom.rlse);
			callRasaioModule1800();
			if (isNE(rasaIO.getBankkey(),SPACES)) {
				getAndDispBankaccdesc1210();
				getAndDispBkBrDet1250();
			}
			goTo(GotoLabel.exit4090);
		}
	}

protected void gensww4010()
	{
		if (isEQ(gensswrec.function,SPACES)) {
			goTo(GotoLabel.nextProgram4020);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		else {
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
				loadProgramStack4300();
			}
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsbbSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsbbSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void a100BankDetsCheck()
	{
		/*A110-BANK-DETS-CHECK*/
		if (isNE(rasaIO.getBankkey(),SPACES)
		|| isNE(rasaIO.getBankacckey(),SPACES)) {
			sv.ddind.set("+");
		}
		else {
			sv.ddind.set(SPACES);
		}
		/*A100-EXIT*/
	}
}
