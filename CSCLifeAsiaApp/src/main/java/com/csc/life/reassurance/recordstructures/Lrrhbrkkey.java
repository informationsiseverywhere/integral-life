package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:27
 * Description:
 * Copybook name: LRRHBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lrrhbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lrrhbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lrrhbrkKey = new FixedLengthStringData(64).isAPartOf(lrrhbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData lrrhbrkCompany = new FixedLengthStringData(1).isAPartOf(lrrhbrkKey, 0);
  	public FixedLengthStringData lrrhbrkChdrnum = new FixedLengthStringData(8).isAPartOf(lrrhbrkKey, 1);
  	public PackedDecimalData lrrhbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lrrhbrkKey, 9);
  	public FixedLengthStringData lrrhbrkLife = new FixedLengthStringData(2).isAPartOf(lrrhbrkKey, 12);
  	public FixedLengthStringData lrrhbrkCoverage = new FixedLengthStringData(2).isAPartOf(lrrhbrkKey, 14);
  	public FixedLengthStringData lrrhbrkRider = new FixedLengthStringData(2).isAPartOf(lrrhbrkKey, 16);
  	public FixedLengthStringData lrrhbrkClntpfx = new FixedLengthStringData(2).isAPartOf(lrrhbrkKey, 18);
  	public FixedLengthStringData lrrhbrkClntcoy = new FixedLengthStringData(1).isAPartOf(lrrhbrkKey, 20);
  	public FixedLengthStringData lrrhbrkClntnum = new FixedLengthStringData(8).isAPartOf(lrrhbrkKey, 21);
  	public FixedLengthStringData lrrhbrkLrkcls = new FixedLengthStringData(4).isAPartOf(lrrhbrkKey, 29);
  	public FixedLengthStringData filler = new FixedLengthStringData(31).isAPartOf(lrrhbrkKey, 33, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lrrhbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lrrhbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}