package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6260
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6260ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(41);
	public FixedLengthStringData dataFields = new FixedLengthStringData(9).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 9);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 17);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6260screenWritten = new LongData(0);
	public LongData S6260protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6260ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rasnumOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {action, rasnum};
		screenOutFields = new BaseData[][] {actionOut, rasnumOut};
		screenErrFields = new BaseData[] {actionErr, rasnumErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S6260screen.class;
		protectRecord = S6260protect.class;
	}

}
