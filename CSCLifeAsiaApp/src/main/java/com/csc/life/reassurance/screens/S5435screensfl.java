package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5435screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 6;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 16, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5435ScreenVars sv = (S5435ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5435screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5435screensfl, 
			sv.S5435screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5435ScreenVars sv = (S5435ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5435screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5435ScreenVars sv = (S5435ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5435screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5435screensflWritten.gt(0))
		{
			sv.s5435screensfl.setCurrentIndex(0);
			sv.S5435screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5435ScreenVars sv = (S5435ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5435screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5435ScreenVars screenVars = (S5435ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.lrkcls.setFieldName("lrkcls");
				screenVars.currency.setFieldName("currency");
				screenVars.ssretn.setFieldName("ssretn");
				screenVars.ssreast.setFieldName("ssreast");
				screenVars.ssreasf.setFieldName("ssreasf");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.lrkcls.set(dm.getField("lrkcls"));
			screenVars.currency.set(dm.getField("currency"));
			screenVars.ssretn.set(dm.getField("ssretn"));
			screenVars.ssreast.set(dm.getField("ssreast"));
			screenVars.ssreasf.set(dm.getField("ssreasf"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5435ScreenVars screenVars = (S5435ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.lrkcls.setFieldName("lrkcls");
				screenVars.currency.setFieldName("currency");
				screenVars.ssretn.setFieldName("ssretn");
				screenVars.ssreast.setFieldName("ssreast");
				screenVars.ssreasf.setFieldName("ssreasf");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("lrkcls").set(screenVars.lrkcls);
			dm.getField("currency").set(screenVars.currency);
			dm.getField("ssretn").set(screenVars.ssretn);
			dm.getField("ssreast").set(screenVars.ssreast);
			dm.getField("ssreasf").set(screenVars.ssreasf);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5435screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5435ScreenVars screenVars = (S5435ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.lrkcls.clearFormatting();
		screenVars.currency.clearFormatting();
		screenVars.ssretn.clearFormatting();
		screenVars.ssreast.clearFormatting();
		screenVars.ssreasf.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5435ScreenVars screenVars = (S5435ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.lrkcls.setClassString("");
		screenVars.currency.setClassString("");
		screenVars.ssretn.setClassString("");
		screenVars.ssreast.setClassString("");
		screenVars.ssreasf.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
	}

/**
 * Clear all the variables in S5435screensfl
 */
	public static void clear(VarModel pv) {
		S5435ScreenVars screenVars = (S5435ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.lrkcls.clear();
		screenVars.currency.clear();
		screenVars.ssretn.clear();
		screenVars.ssreast.clear();
		screenVars.ssreasf.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
	}
}
