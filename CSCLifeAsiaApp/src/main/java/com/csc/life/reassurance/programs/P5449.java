/*
 * File: P5449.java
 * Date: 30 August 2009 0:27:23
 * Author: Quipoz Limited
 * 
 * Class transformed from P5449.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.reassurance.procedures.T5449pt;
import com.csc.life.reassurance.screens.S5449ScreenVars;
import com.csc.life.reassurance.tablestructures.T5449rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5449 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5449");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5449rec t5449rec = new T5449rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5449ScreenVars sv = ScreenProgram.getScreenVars( S5449ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5449() {
		super();
		screenVars = sv;
		new ScreenModel("S5449", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5449rec.t5449Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5449rec.qcoshr.set(ZERO);
		t5449rec.qreshr01.set(ZERO);
		t5449rec.qreshr02.set(ZERO);
		t5449rec.qreshr03.set(ZERO);
		t5449rec.qreshr04.set(ZERO);
		t5449rec.qreshr05.set(ZERO);
		t5449rec.qreshr06.set(ZERO);
		t5449rec.qreshr07.set(ZERO);
		t5449rec.qreshr08.set(ZERO);
		t5449rec.qreshr09.set(ZERO);
		t5449rec.qreshr10.set(ZERO);
		t5449rec.relimit01.set(ZERO);
		t5449rec.relimit02.set(ZERO);
		t5449rec.relimit03.set(ZERO);
		t5449rec.relimit04.set(ZERO);
		t5449rec.relimit05.set(ZERO);
		t5449rec.relimit06.set(ZERO);
		t5449rec.relimit07.set(ZERO);
		t5449rec.relimit08.set(ZERO);
		t5449rec.relimit09.set(ZERO);
		t5449rec.relimit10.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.currcode.set(t5449rec.currcode);
		sv.facsh.set(t5449rec.facsh);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.letterType.set(t5449rec.letterType);
		sv.lrkcls.set(t5449rec.lrkcls);
		sv.prefbas.set(t5449rec.prefbas);
		sv.qcoshr.set(t5449rec.qcoshr);
		sv.qreshrs.set(t5449rec.qreshrs);
		sv.rasnums.set(t5449rec.rasnums);
		sv.relimits.set(t5449rec.relimits);
		sv.retype.set(t5449rec.retype);
		sv.rprmmths.set(t5449rec.rprmmths);
		sv.rtytyp.set(t5449rec.rtytyp);
		sv.sslivb.set(t5449rec.sslivb);
		sv.subliv.set(t5449rec.subliv);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5449rec.t5449Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.currcode,t5449rec.currcode)) {
			t5449rec.currcode.set(sv.currcode);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.facsh,t5449rec.facsh)) {
			t5449rec.facsh.set(sv.facsh);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.letterType,t5449rec.letterType)) {
			t5449rec.letterType.set(sv.letterType);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lrkcls,t5449rec.lrkcls)) {
			t5449rec.lrkcls.set(sv.lrkcls);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prefbas,t5449rec.prefbas)) {
			t5449rec.prefbas.set(sv.prefbas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.qcoshr,t5449rec.qcoshr)) {
			t5449rec.qcoshr.set(sv.qcoshr);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.qreshrs,t5449rec.qreshrs)) {
			t5449rec.qreshrs.set(sv.qreshrs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rasnums,t5449rec.rasnums)) {
			t5449rec.rasnums.set(sv.rasnums);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.relimits,t5449rec.relimits)) {
			t5449rec.relimits.set(sv.relimits);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.retype,t5449rec.retype)) {
			t5449rec.retype.set(sv.retype);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rprmmths,t5449rec.rprmmths)) {
			t5449rec.rprmmths.set(sv.rprmmths);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rtytyp,t5449rec.rtytyp)) {
			t5449rec.rtytyp.set(sv.rtytyp);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sslivb,t5449rec.sslivb)) {
			t5449rec.sslivb.set(sv.sslivb);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.subliv,t5449rec.subliv)) {
			t5449rec.subliv.set(sv.subliv);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5449pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
