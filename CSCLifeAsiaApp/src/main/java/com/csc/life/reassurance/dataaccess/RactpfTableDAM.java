package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RactpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:13
 * Class transformed from RACTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RactpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 146;
	public FixedLengthStringData ractrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ractpfRecord = ractrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ractrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ractrec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(ractrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ractrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ractrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ractrec);
	public FixedLengthStringData ratype = DD.ratype.copy().isAPartOf(ractrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(ractrec);
	public FixedLengthStringData rapayfrq = DD.rapayfrq.copy().isAPartOf(ractrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(ractrec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(ractrec);
	public PackedDecimalData raAmount = DD.raamount.copy().isAPartOf(ractrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(ractrec);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(ractrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(ractrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(ractrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(ractrec);
	public PackedDecimalData btdate = DD.btdate.copy().isAPartOf(ractrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(ractrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(ractrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(ractrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ractrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(ractrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ractrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ractrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ractrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RactpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RactpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RactpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RactpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RactpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RactpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RactpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("RACTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"RASNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"RATYPE, " +
							"VALIDFLAG, " +
							"RAPAYFRQ, " +
							"INSTPREM, " +
							"SINGP, " +
							"RAAMOUNT, " +
							"CURRCODE, " +
							"SEX, " +
							"CURRFROM, " +
							"RCESDTE, " +
							"CRRCD, " +
							"BTDATE, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"TRANNO, " +
							"TERMID, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     rasnum,
                                     life,
                                     coverage,
                                     rider,
                                     ratype,
                                     validflag,
                                     rapayfrq,
                                     instprem,
                                     singp,
                                     raAmount,
                                     currcode,
                                     sex,
                                     currfrom,
                                     riskCessDate,
                                     crrcd,
                                     btdate,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     tranno,
                                     termid,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		rasnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		ratype.clear();
  		validflag.clear();
  		rapayfrq.clear();
  		instprem.clear();
  		singp.clear();
  		raAmount.clear();
  		currcode.clear();
  		sex.clear();
  		currfrom.clear();
  		riskCessDate.clear();
  		crrcd.clear();
  		btdate.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		tranno.clear();
  		termid.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRactrec() {
  		return ractrec;
	}

	public FixedLengthStringData getRactpfRecord() {
  		return ractpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRactrec(what);
	}

	public void setRactrec(Object what) {
  		this.ractrec.set(what);
	}

	public void setRactpfRecord(Object what) {
  		this.ractpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ractrec.getLength());
		result.set(ractrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}