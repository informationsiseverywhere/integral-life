package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:43
 * Description:
 * Copybook name: ACMVRASCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvrascpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(218);
  	public FixedLengthStringData dataKey = new FixedLengthStringData(23).isAPartOf(dataArea, 0);
  	public FixedLengthStringData keyData = new FixedLengthStringData(23).isAPartOf(dataKey, 0, REDEFINE);
  	public FixedLengthStringData rldgacct = new FixedLengthStringData(16).isAPartOf(keyData, 0);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(keyData, 16);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(keyData, 19);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(keyData, 21);
  	public FixedLengthStringData nonKey = new FixedLengthStringData(195).isAPartOf(dataArea, 23);
  	public FixedLengthStringData nonKeyData = new FixedLengthStringData(195).isAPartOf(nonKey, 0, REDEFINE);
  	public FixedLengthStringData rldgcoy = new FixedLengthStringData(1).isAPartOf(nonKeyData, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(nonKeyData, 1);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(nonKeyData, 2);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(nonKeyData, 4);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(nonKeyData, 7);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(nonKeyData, 9);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(nonKeyData, 13);
  	public FixedLengthStringData rdocnum = new FixedLengthStringData(9).isAPartOf(nonKeyData, 18);
  	public FixedLengthStringData intextind = new FixedLengthStringData(1).isAPartOf(nonKeyData, 27);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(nonKeyData, 28);
  	public PackedDecimalData jrnseq = new PackedDecimalData(3, 0).isAPartOf(nonKeyData, 31);
  	public PackedDecimalData origamt = new PackedDecimalData(17, 2).isAPartOf(nonKeyData, 33);
  	public FixedLengthStringData tranref = new FixedLengthStringData(30).isAPartOf(nonKeyData, 42);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(nonKeyData, 72);
  	public PackedDecimalData crate = new PackedDecimalData(18, 9).isAPartOf(nonKeyData, 102);
  	public PackedDecimalData acctamt = new PackedDecimalData(17, 2).isAPartOf(nonKeyData, 112);
  	public FixedLengthStringData genlcoy = new FixedLengthStringData(1).isAPartOf(nonKeyData, 121);
  	public FixedLengthStringData genlcur = new FixedLengthStringData(3).isAPartOf(nonKeyData, 122);
  	public FixedLengthStringData glcode = new FixedLengthStringData(14).isAPartOf(nonKeyData, 125);
  	public FixedLengthStringData glsign = new FixedLengthStringData(1).isAPartOf(nonKeyData, 139);
  	public FixedLengthStringData postyear = new FixedLengthStringData(4).isAPartOf(nonKeyData, 140);
  	public FixedLengthStringData postmonth = new FixedLengthStringData(2).isAPartOf(nonKeyData, 144);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(nonKeyData, 146);
  	public PackedDecimalData rcamt = new PackedDecimalData(17, 2).isAPartOf(nonKeyData, 151);
  	public PackedDecimalData frcdate = new PackedDecimalData(8, 0).isAPartOf(nonKeyData, 160);
  	public PackedDecimalData transactionDate = new PackedDecimalData(6, 0).isAPartOf(nonKeyData, 165);
  	public PackedDecimalData transactionTime = new PackedDecimalData(6, 0).isAPartOf(nonKeyData, 169);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(nonKeyData, 173);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(nonKeyData, 177);
  	public FixedLengthStringData filler = new FixedLengthStringData(14).isAPartOf(nonKeyData, 181, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(dataArea);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		dataArea.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}