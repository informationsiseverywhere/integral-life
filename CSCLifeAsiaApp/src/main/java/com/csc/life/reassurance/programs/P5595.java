/*
 * File: P5595.java
 * Date: 30 August 2009 0:31:22
 * Author: Quipoz Limited
 * 
 * Class transformed from P5595.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Clrrkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Largenmrec;
import com.csc.fsu.general.procedures.Largenm;
import com.csc.fsu.general.recordstructures.Clntnamcpy;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S5595ScreenVars;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*          Scroll window of REASSURANCE for a given Client        <001>
*
* This scroll program accepts a client key and provides a scroll
* for all the REASS of this client within the sign-on company.    <001>
*
*****************************************************************
* </pre>
*/
public class P5595 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P2595");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNumofrecs = new ZonedDecimalData(2, 0).setUnsigned();
		/* FORMATS */
	private static final String rasarec = "RASAREC";
	private static final String clrrrec = "CLRRREC";
	private static final String clntrec = "CLNTREC";
	private ClntTableDAM clntIO = new ClntTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Clrrkey wsaaClrrkey = new Clrrkey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Clntnamcpy clntnamcpy = new Clntnamcpy();
	private Largenmrec largenmrec = new Largenmrec();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S5595ScreenVars sv = ScreenProgram.getScreenVars( S5595ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		plnmPlainExit, 
		validateSubfile2600, 
		exit2090, 
		readClrrLoop5300, 
		loadSubfile5350, 
		nextRec5500, 
		endSubfile5800, 
		exit5900
	}

	public P5595() {
		super();
		screenVars = sv;
		new ScreenModel("S5595", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void lgnmLargeName()
	{
		largenmrec.largenmRec.set(SPACES);
		largenmrec.function.set("LNAME");
		callProgram(Largenm.class, largenmrec.largenmRec, clntnamcpy.clntnamCpy, clntnamcpy.wsnmBigField, clntnamcpy.wsnmLanguage, clntnamcpy.wsnmSurnameOut, clntnamcpy.wsnmLargeName);
		if (isEQ(largenmrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(largenmrec.statuz);
			return ;
		}
		return ;
		/*LGNM-LARGE-EXIT*/
	}

protected void fmtnFormatName()
	{
		fmtnPara();
	}

protected void fmtnPara()
	{
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)) {
			if (isNE(clntIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(clntIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(clntIO.getStatuz(), varcom.mrnf)) {
			clntnamcpy.wsnmLargeName.set(SPACES);
			return ;
		}
		if (isNE(clntIO.getValidflag(), 1)) {
			clntnamcpy.wsnmLargeName.set(SPACES);
			return ;
		}
		clntnamcpy.inSurname.set(clntIO.getSurname());
		clntnamcpy.inGivname.set(clntIO.getGivname());
		if (isEQ(clntIO.getClttype(), "C")) {
			clntnamcpy.wsaaCorpname1.set(clntIO.getSurname());
			clntnamcpy.wsaaCorpname2.set(clntIO.getGivname());
			clntnamcpy.wsnmLargeName.set(clntnamcpy.wsaaCorpname);
		}
		else {
			lgnmLargeName();
		}
	}

protected void plnmPlainName()
	{
		try {
			plnmPlainNamePara();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void plnmPlainNamePara()
	{
		clntnamcpy.wsnmSurnameOut.set(clntnamcpy.inSurname);
		if (clntnamcpy.wsnmEng.isTrue()) {
			plnmEnglishPlain();
		}
		goTo(GotoLabel.plnmPlainExit);
	}

protected void plnmEnglishPlain()
	{
		clntnamcpy.wsnmSurnameOut.set(clntnamcpy.inSurname);
		return ;
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1100();
		initSubfile1200();
		readRasaForClient1300();
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		/*    WSSP-VALUE contains the client number that was inquired upon*/
		sv.clntnum.set(wsspwindow.value);
		sv.cname.set(wsspwindow.confirmation);
	}

protected void initSubfile1200()
	{
		scrnparams.function.set(varcom.sclr);
		processScreen("S5595", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

	/**
	* <pre>
	*    Find first Reassurer                                         
	* </pre>
	*/
protected void readRasaForClient1300()
	{
		wsaaClrrkey.clrrKey.set(SPACES);
		wsaaClrrkey.clrrClntpfx.set(fsupfxcpy.clnt);
		wsaaClrrkey.clrrClntcoy.set(wsspcomn.fsuco);
		wsaaClrrkey.clrrClntnum.set(wsspwindow.value);
		/* IF WSSP-WINDOW-TYPE         = 'R'                            */
		/*    MOVE CLRF-RACC           TO WSKY-CLRR-CLRRROLE.           */
		if (isEQ(wsspwindow.windowType, "E")) {
			wsaaClrrkey.clrrClrrrole.set(clntrlsrec.rilf);
		}
		/*  MOVE PRFX-RACC              TO WSKY-CLRR-FOREPFX.            */
		wsaaClrrkey.clrrForepfx.set(SPACES);
		/*  MOVE WSSP-FSUCO             TO WSKY-CLRR-FORECOY.            */
		wsaaClrrkey.clrrForecoy.set(wsspcomn.company);
		clrrIO.setDataKey(wsaaClrrkey.clrrKey);
		clrrIO.setFunction(varcom.begn);
		scrnparams.subfileMore.set("N");
		loadSubfile5000();
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* If only a single reassurer found then return immediately        */
		/* with this reassurer only.                                       */
		if (isEQ(wsaaNumofrecs, 1)
		&& isNE(scrnparams.subfileMore, "Y")) {
			wsspwindow.value.set(sv.rasnum);
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
				case validateSubfile2600: 
					validateSubfile2600();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5595IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5595-DATA-AREA                         */
		/*                         S5595-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*ROLL-UP*/
		if (isNE(scrnparams.statuz, varcom.rolu)) {
			goTo(GotoLabel.validateSubfile2600);
		}
		loadSubfile5000();
		scrnparams.function.set(varcom.norml);
		/* The following line has been changed to follow standar ds        */
		/* and avoid an internal loop.                                     */
		/*    GO TO 2010-SCREEN-IO.                                        */
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
		/* UNREACHABLE CODE
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		 */
	}

protected void validateSubfile2600()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5595", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*  If none are selected then return a value of spaces.*/
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			wsspwindow.value.set(SPACES);
			wsspwindow.confirmation.set(SPACES);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		/*    Check entry of select field*/
		if (isEQ(sv.slt, SPACES)) {
			goTo(GotoLabel.validateSubfile2600);
		}
		/*    WSSP-VALUE will contain the reassurer number selected        */
		/*    from the scroll*/
		wsspwindow.value.set(sv.rasnum);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadSubfile5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5200();
				case readClrrLoop5300: 
					readClrrLoop5300();
				case loadSubfile5350: 
					loadSubfile5350();
				case nextRec5500: 
					nextRec5500();
				case endSubfile5800: 
					endSubfile5800();
				case exit5900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5200()
	{
		wsaaNumofrecs.set(0);
		if (isEQ(scrnparams.subfileMore, "Y")) {
			wsaaNumofrecs.add(1);
			goTo(GotoLabel.loadSubfile5350);
		}
	}

protected void readClrrLoop5300()
	{
		/* If a record was found on CLRR, read the RASA file               */
		clrrIO.setFormat(clrrrec);
		SmartFileCode.execute(appVars, clrrIO);
		clrrIO.setFunction(varcom.nextr);
		if (isEQ(clrrIO.getStatuz(), varcom.endp)
		|| isEQ(clrrIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.endSubfile5800);
		}
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		/*  Check that the correct record was read*/
		if (isNE(clrrIO.getClntpfx(), wsaaClrrkey.clrrClntpfx)
		|| isNE(clrrIO.getClntcoy(), wsaaClrrkey.clrrClntcoy)
		|| isNE(clrrIO.getClntnum(), wsaaClrrkey.clrrClntnum)
		|| isNE(clrrIO.getClrrrole(), wsaaClrrkey.clrrClrrrole)
		|| isNE(clrrIO.getForecoy(), wsspcomn.company)) {
			clrrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.endSubfile5800);
		}
		if (isNE(clrrIO.getUsedToBe(), SPACES)) {
			goTo(GotoLabel.nextRec5500);
		}
		/*    Read the RASA:- Reassurance Account Master File, to obtain th*/
		/*    client number to read the CLTS File.*/
		/*    Use CONFNAME to format the client's name for display.*/
		rasaIO.setDataKey(SPACES);
		/* MOVE CLRR-FOREPFX           TO RASA-RASPFX.                  */
		rasaIO.setRascoy(clrrIO.getForecoy());
		rasaIO.setRasnum(clrrIO.getForenum());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(rasaIO.getParams());
			fatalError600();
		}
		/*  Check subfile size to format on screen*/
		wsaaNumofrecs.add(1);
		if (isGT(wsaaNumofrecs, 14)) {
			scrnparams.subfileMore.set("Y");
			goTo(GotoLabel.exit5900);
		}
	}

protected void loadSubfile5350()
	{
		sv.slt.set(SPACES);
		sv.rasnum.set(rasaIO.getRasnum());
		sv.validflag.set(rasaIO.getValidflag());
		sv.rapaymth.set(rasaIO.getRapaymth());
		sv.rapayfrq.set(rasaIO.getRapayfrq());
		sv.currcode.set(rasaIO.getCurrcode());
		sv.agntnum.set(rasaIO.getAgntnum());
		sv.minsta.set(rasaIO.getMinsta());
		/*ADD-SUBFILE-REC*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5595", sv);
	}

	/**
	* <pre>
	* Screen errors are now handled in the calling program.           
	*    PERFORM 200-SCREEN-ERRORS.                                   
	* </pre>
	*/
protected void nextRec5500()
	{
		clrrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readClrrLoop5300);
	}

protected void endSubfile5800()
	{
		scrnparams.subfileMore.set("N");
	}
}
