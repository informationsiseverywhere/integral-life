/*
 * File: P5432.java
 * Date: 30 August 2009 0:25:26
 * Author: Quipoz Limited
 * 
 * Class transformed from P5432.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RecorcoTableDAM;
import com.csc.life.reassurance.screens.S5432ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5432 - Reassurance Costing Enquiry.
*
* This program forms part of the Reassurance Development.
* It is run off the Reassurance Costing Selection screen, S5431.
* It shows all the RECO records for the selected component.
* It is an enquiry only scrolling subfile.
*
* 1000-INITIALISE.
*
* Clear the subfile.
* Retrieve the CHDRENQ.
* Set up screen header details.
* Load RECO details to the subfile.
*
* 2000-SCREEN-EDIT.
*
* Display the screen.
* Load another subfile page of RECO records if page
* down (ROLU) was pressed and there are still records
* to display.
*
* 3000-UPDATE.
*
* No updating required.
*
* 4000-WHERE-NEXT.
*
* Add 1 to program pointer.
*
*****************************************************************
* </pre>
*/
public class P5432 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5432");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNumber = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(3, 0);
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String lifeenqrec = "LIFEENQREC";
	private String recorcorec = "RECORCOREC";
	private String cltsrec = "CLTSREC";
	private String descrec = "DESCREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
		/*Reassurance Costing Details By Seqno.*/
	private RecorcoTableDAM recorcoIO = new RecorcoTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5432ScreenVars sv = ScreenProgram.getScreenVars( S5432ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		lgnmExit, 
		plainExit, 
		payeeExit
	}

	public P5432() {
		super();
		screenVars = sv;
		new ScreenModel("S5432", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5432", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		sv.numpols.set(chdrenqIO.getPolinc());
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFormat(lifeenqrec);
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			cltsIO.setParams(SPACES);
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFormat(cltsrec);
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		covrenqIO.setParams(SPACES);
		covrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rider.set(covrenqIO.getRider());
		recorcoIO.setParams(SPACES);
		recorcoIO.setChdrcoy(covrenqIO.getChdrcoy());
		recorcoIO.setChdrnum(covrenqIO.getChdrnum());
		recorcoIO.setLife(covrenqIO.getLife());
		recorcoIO.setCoverage(covrenqIO.getCoverage());
		recorcoIO.setRider(covrenqIO.getRider());
		recorcoIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		recorcoIO.setCostdate(varcom.vrcmMaxDate);
		recorcoIO.setSeqno(ZERO);
		recorcoIO.setFormat(recorcorec);
		recorcoIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		recorcoIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		recorcoIO.setFitKeysSearch("LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, recorcoIO);
		if (isNE(recorcoIO.getStatuz(),varcom.oK)
		&& isNE(recorcoIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(recorcoIO.getParams());
			fatalError600();
		}
		wsaaRem.set(1);
		while ( !(isNE(recorcoIO.getChdrcoy(),wsspcomn.company)
		|| isNE(recorcoIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(recorcoIO.getLife(),covrenqIO.getLife())
		|| isNE(recorcoIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(recorcoIO.getRider(),covrenqIO.getRider())
		|| isNE(recorcoIO.getPlanSuffix(),covrenqIO.getPlanSuffix())
		|| isEQ(recorcoIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaRem,0))) {
			processReco1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void processReco1100()
	{
		para1100();
		writeSubfile1150();
		readNext1160();
	}

protected void para1100()
	{
		sv.subfileFields.set(SPACES);
		sv.seqno.set(recorcoIO.getSeqno());
		sv.costdate.set(recorcoIO.getCostdate());
		sv.rasnum.set(recorcoIO.getRasnum());
		sv.rngmnt.set(recorcoIO.getRngmnt());
		sv.sraramt.set(recorcoIO.getSraramt());
		sv.instprem.set(recorcoIO.getPrem());
		sv.compay.set(recorcoIO.getCompay());
		sv.acctcurr.set(recorcoIO.getOrigcurr());
	}

protected void writeSubfile1150()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5432", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNext1160()
	{
		recorcoIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, recorcoIO);
		if (isNE(recorcoIO.getStatuz(),varcom.oK)
		&& isNE(recorcoIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(recorcoIO.getParams());
			fatalError600();
		}
		compute(wsaaNumber, 0).setDivide(scrnparams.subfileRrn, (sv.subfilePage));
		wsaaRem.setRemainder(wsaaNumber);
		scrnparams.subfileMore.set(SPACES);
		if (isEQ(wsaaRem,0)) {
			if (isNE(recorcoIO.getStatuz(),varcom.endp)) {
				scrnparams.subfileMore.set("Y");
			}
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaRem.set(1);
			while ( !(isNE(recorcoIO.getChdrcoy(),wsspcomn.company)
			|| isNE(recorcoIO.getChdrnum(),chdrenqIO.getChdrnum())
			|| isNE(recorcoIO.getLife(),covrenqIO.getLife())
			|| isNE(recorcoIO.getCoverage(),covrenqIO.getCoverage())
			|| isNE(recorcoIO.getRider(),covrenqIO.getRider())
			|| isNE(recorcoIO.getPlanSuffix(),covrenqIO.getPlanSuffix())
			|| isEQ(recorcoIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaRem,0))) {
				processReco1100();
			}
			
			wsspcomn.edterror.set(SPACES);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}
}
