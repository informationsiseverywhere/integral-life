package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5449screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5449ScreenVars sv = (S5449ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5449screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5449ScreenVars screenVars = (S5449ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.retype.setClassString("");
		screenVars.facsh.setClassString("");
		screenVars.lrkcls.setClassString("");
		screenVars.subliv.setClassString("");
		screenVars.sslivb.setClassString("");
		screenVars.qcoshr.setClassString("");
		screenVars.rasnum01.setClassString("");
		screenVars.rasnum02.setClassString("");
		screenVars.rasnum03.setClassString("");
		screenVars.rasnum04.setClassString("");
		screenVars.rasnum05.setClassString("");
		screenVars.rasnum06.setClassString("");
		screenVars.rasnum07.setClassString("");
		screenVars.rasnum08.setClassString("");
		screenVars.rasnum09.setClassString("");
		screenVars.rasnum10.setClassString("");
		screenVars.qreshr01.setClassString("");
		screenVars.qreshr02.setClassString("");
		screenVars.qreshr03.setClassString("");
		screenVars.qreshr04.setClassString("");
		screenVars.qreshr05.setClassString("");
		screenVars.qreshr06.setClassString("");
		screenVars.qreshr07.setClassString("");
		screenVars.qreshr08.setClassString("");
		screenVars.qreshr09.setClassString("");
		screenVars.qreshr10.setClassString("");
		screenVars.relimit01.setClassString("");
		screenVars.relimit02.setClassString("");
		screenVars.relimit03.setClassString("");
		screenVars.relimit04.setClassString("");
		screenVars.relimit05.setClassString("");
		screenVars.relimit06.setClassString("");
		screenVars.relimit07.setClassString("");
		screenVars.relimit08.setClassString("");
		screenVars.relimit09.setClassString("");
		screenVars.relimit10.setClassString("");
		screenVars.rprmmth01.setClassString("");
		screenVars.rprmmth03.setClassString("");
		screenVars.rprmmth02.setClassString("");
		screenVars.rprmmth04.setClassString("");
		screenVars.rprmmth05.setClassString("");
		screenVars.rprmmth06.setClassString("");
		screenVars.rprmmth07.setClassString("");
		screenVars.rprmmth08.setClassString("");
		screenVars.rprmmth09.setClassString("");
		screenVars.rprmmth10.setClassString("");
		screenVars.prefbas.setClassString("");
		screenVars.rtytyp.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.letterType.setClassString("");
	}

/**
 * Clear all the variables in S5449screen
 */
	public static void clear(VarModel pv) {
		S5449ScreenVars screenVars = (S5449ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.retype.clear();
		screenVars.facsh.clear();
		screenVars.lrkcls.clear();
		screenVars.subliv.clear();
		screenVars.sslivb.clear();
		screenVars.qcoshr.clear();
		screenVars.rasnum01.clear();
		screenVars.rasnum02.clear();
		screenVars.rasnum03.clear();
		screenVars.rasnum04.clear();
		screenVars.rasnum05.clear();
		screenVars.rasnum06.clear();
		screenVars.rasnum07.clear();
		screenVars.rasnum08.clear();
		screenVars.rasnum09.clear();
		screenVars.rasnum10.clear();
		screenVars.qreshr01.clear();
		screenVars.qreshr02.clear();
		screenVars.qreshr03.clear();
		screenVars.qreshr04.clear();
		screenVars.qreshr05.clear();
		screenVars.qreshr06.clear();
		screenVars.qreshr07.clear();
		screenVars.qreshr08.clear();
		screenVars.qreshr09.clear();
		screenVars.qreshr10.clear();
		screenVars.relimit01.clear();
		screenVars.relimit02.clear();
		screenVars.relimit03.clear();
		screenVars.relimit04.clear();
		screenVars.relimit05.clear();
		screenVars.relimit06.clear();
		screenVars.relimit07.clear();
		screenVars.relimit08.clear();
		screenVars.relimit09.clear();
		screenVars.relimit10.clear();
		screenVars.rprmmth01.clear();
		screenVars.rprmmth03.clear();
		screenVars.rprmmth02.clear();
		screenVars.rprmmth04.clear();
		screenVars.rprmmth05.clear();
		screenVars.rprmmth06.clear();
		screenVars.rprmmth07.clear();
		screenVars.rprmmth08.clear();
		screenVars.rprmmth09.clear();
		screenVars.rprmmth10.clear();
		screenVars.prefbas.clear();
		screenVars.rtytyp.clear();
		screenVars.currcode.clear();
		screenVars.letterType.clear();
	}
}
