package com.csc.life.reassurance.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CcalpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:19
 * Class transformed from CCALPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CcalpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 118;
	public FixedLengthStringData ccalrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ccalpfRecord = ccalrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ccalrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ccalrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ccalrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ccalrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ccalrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ccalrec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(ccalrec);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(ccalrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(ccalrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ccalrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(ccalrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(ccalrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(ccalrec);
	public PackedDecimalData calldt = DD.calldt.copy().isAPartOf(ccalrec);
	public PackedDecimalData callamt = DD.callamt.copy().isAPartOf(ccalrec);
	public PackedDecimalData callrecd = DD.callrecd.copy().isAPartOf(ccalrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(ccalrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ccalrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ccalrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ccalrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CcalpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CcalpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CcalpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CcalpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CcalpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CcalpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CcalpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CCALPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"RASNUM, " +
							"RNGMNT, " +
							"SEQNO, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"CURRTO, " +
							"CALLDT, " +
							"CALLAMT, " +
							"CALLRECD, " +
							"CURRCODE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     rasnum,
                                     rngmnt,
                                     seqno,
                                     tranno,
                                     validflag,
                                     currfrom,
                                     currto,
                                     calldt,
                                     callamt,
                                     callrecd,
                                     currcode,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		rasnum.clear();
  		rngmnt.clear();
  		seqno.clear();
  		tranno.clear();
  		validflag.clear();
  		currfrom.clear();
  		currto.clear();
  		calldt.clear();
  		callamt.clear();
  		callrecd.clear();
  		currcode.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCcalrec() {
  		return ccalrec;
	}

	public FixedLengthStringData getCcalpfRecord() {
  		return ccalpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCcalrec(what);
	}

	public void setCcalrec(Object what) {
  		this.ccalrec.set(what);
	}

	public void setCcalpfRecord(Object what) {
  		this.ccalpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ccalrec.getLength());
		result.set(ccalrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}