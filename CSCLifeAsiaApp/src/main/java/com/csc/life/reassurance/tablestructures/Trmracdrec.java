package com.csc.life.reassurance.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:38
 * Description:
 * Copybook name: TRMRACDREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Trmracdrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData trmracdRec = new FixedLengthStringData(94);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(trmracdRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(trmracdRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(trmracdRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(trmracdRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(trmracdRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(trmracdRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(trmracdRec, 22);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(trmracdRec, 24);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(trmracdRec, 27);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(trmracdRec, 30);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(trmracdRec, 33);
  	public PackedDecimalData seqno = new PackedDecimalData(2, 0).isAPartOf(trmracdRec, 37);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(1).isAPartOf(trmracdRec, 39);
  	public FixedLengthStringData l1Clntnum = new FixedLengthStringData(8).isAPartOf(trmracdRec, 40);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(trmracdRec, 48);
  	public FixedLengthStringData l2Clntnum = new FixedLengthStringData(8).isAPartOf(trmracdRec, 50);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(trmracdRec, 58);
  	public PackedDecimalData newRaAmt = new PackedDecimalData(17, 2).isAPartOf(trmracdRec, 63);
  	public PackedDecimalData recovamt = new PackedDecimalData(17, 2).isAPartOf(trmracdRec, 72);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(trmracdRec, 81);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(trmracdRec, 86);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(trmracdRec, 90);


	public void initialize() {
		COBOLFunctions.initialize(trmracdRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		trmracdRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}