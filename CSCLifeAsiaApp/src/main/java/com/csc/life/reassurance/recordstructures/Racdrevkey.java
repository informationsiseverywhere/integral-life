package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:21
 * Description:
 * Copybook name: RACDREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdrevKey = new FixedLengthStringData(64).isAPartOf(racdrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdrevChdrcoy = new FixedLengthStringData(1).isAPartOf(racdrevKey, 0);
  	public FixedLengthStringData racdrevChdrnum = new FixedLengthStringData(8).isAPartOf(racdrevKey, 1);
  	public FixedLengthStringData racdrevLife = new FixedLengthStringData(2).isAPartOf(racdrevKey, 9);
  	public FixedLengthStringData racdrevCoverage = new FixedLengthStringData(2).isAPartOf(racdrevKey, 11);
  	public FixedLengthStringData racdrevRider = new FixedLengthStringData(2).isAPartOf(racdrevKey, 13);
  	public PackedDecimalData racdrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdrevKey, 15);
  	public PackedDecimalData racdrevSeqno = new PackedDecimalData(2, 0).isAPartOf(racdrevKey, 18);
  	public PackedDecimalData racdrevTranno = new PackedDecimalData(5, 0).isAPartOf(racdrevKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(racdrevKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}