/*
 * File: Gstcalc.java
 * Date: 29 August 2009 22:51:34
 * Author: Quipoz Limited
 *
 * Class transformed from GSTCALC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.reassurance.recordstructures.Restdtyrec;
import com.csc.life.reassurance.tablestructures.T5443rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Reassurance Government Sales Tax Calculation.
*
* Overview
* ========
*
* This subroutine is called from the reassurance costing batch
* program, B5456, to calculate the amount of GST to be paid.
*
* The GST rates are read from table T5443 using currency and
* component type as the key. The annual premium (passed in the
* linkage) must fall within one of the premium bands on T5443.
*
* Linkage
* =======
*
* STATUZ             PIC X(04).
* COMPANY            PIC X(01).
* CHDRNUM            PIC X(08).
* LIFE               PIC X(02).
* COVERAGE           PIC X(02).
* RIDER              PIC X(02).
* PLNSFX             PIC S9(04) COMP-3.
* CNTCURR            PIC X(03).
* CRTABLE            PIC X(04).
* EFFDATE            PIC 9(08).
* SUMASS             PIC S9(11)V9(02).
* PREMIUM            PIC S9(11)V9(02).
* TAX                PIC S9(11)V9(02).
*
* These fields are all contained within RESTDTYREC.
*
*****************************************************************
* </pre>
*/
public class Gstcalc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GSTCALC";
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaGstamt = new PackedDecimalData(13, 2);
	private FixedLengthStringData wsaaT5443Item = new FixedLengthStringData(7);
		/* WSAA-CONSTANT-AREA */
	//ILIFE-2628 fixed--Array size increased
	private int wsaaT5443Size = 1000;
	private FixedLengthStringData wsaaAsterisk = new FixedLengthStringData(4).init("****");

	private FixedLengthStringData wsaaEndFlag = new FixedLengthStringData(1);
	private Validator wsaaEndRange = new Validator(wsaaEndFlag, "Y");
		/* ERRORS */
	private String h791 = "H791";
	private String r076 = "R076";
		/* TABLES */
	private String t5443 = "T5443";
		/* FORMATS */
	private String itdmrec = "ITEMREC";
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Restdtyrec restdtyrec = new Restdtyrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5443rec t5443rec = new T5443rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit090,
		end180,
		exit390
	}

	public Gstcalc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		restdtyrec.restdtyRec = convertAndSetParam(restdtyrec.restdtyRec, parmArray, 0);
		try {
			subroutineMain000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void subroutineMain000()
	{
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void main010()
	{
		restdtyrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaSub.set(ZERO);
		wsaaPremium.set(ZERO);
		wsaaGstamt.set(ZERO);
		wsaaEndFlag.set(SPACES);
		restdtyrec.tax.set(ZERO);
		if (isLTE(restdtyrec.premium,ZERO)) {
			goTo(GotoLabel.exit090);
		}
		readT5443100();
		computeGstax200();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readT5443100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					gstamtRatetable110();
				}
				case end180: {
					end180();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void gstamtRatetable110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(restdtyrec.company);
		itdmIO.setItemtabl(t5443);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(restdtyrec.cntcurr.toString());
		stringVariable1.append(restdtyrec.crtable.toString());
		wsaaT5443Item.setLeft(stringVariable1.toString());
		itdmIO.setItemitem(wsaaT5443Item);
		itdmIO.setItmfrm(restdtyrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),restdtyrec.company)
		&& isEQ(itdmIO.getItemtabl(),t5443)
		&& isEQ(itdmIO.getItemitem(),wsaaT5443Item)) {
			goTo(GotoLabel.end180);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(restdtyrec.company);
		itdmIO.setItemtabl(t5443);
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(restdtyrec.cntcurr.toString());
		stringVariable2.append(wsaaAsterisk.toString());
		wsaaT5443Item.setLeft(stringVariable2.toString());
		itdmIO.setItemitem(wsaaT5443Item);
		itdmIO.setItmfrm(restdtyrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(itdmIO.getItemcoy(),restdtyrec.company)
		|| isNE(itdmIO.getItemtabl(),t5443)
		|| isNE(itdmIO.getItemitem(),wsaaT5443Item)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(r076);
			syserr570();
		}
	}

protected void end180()
	{
		t5443rec.t5443Rec.set(itdmIO.getGenarea());
		/*EXIT*/
	}

protected void computeGstax200()
	{
		/*GST*/
		wsaaPremium.set(restdtyrec.premium);
		for (wsaaSub.set(1); !(wsaaEndRange.isTrue()); wsaaSub.add(1)){
			findRange300();
		}
		/*EXIT*/
	}

protected void findRange300()
	{
		try {
			start310();
		}
		catch (GOTOException e){
		}
	}

protected void start310()
	{
		if (isGT(wsaaSub,wsaaT5443Size)) {
			syserrrec.params.set(t5443);
			syserrrec.statuz.set(h791);
			syserr570();
		}
		if (isEQ(t5443rec.gstrate[wsaaSub.toInt()],ZERO)
		|| isLT(wsaaPremium,t5443rec.reprmfrm[wsaaSub.toInt()])
		|| isGT(wsaaPremium,t5443rec.reprmto[wsaaSub.toInt()])) {
			goTo(GotoLabel.exit390);
		}
		compute(wsaaGstamt, 3).setRounded(div(mult(wsaaPremium,t5443rec.gstrate[wsaaSub.toInt()]),100));
		restdtyrec.tax.set(wsaaGstamt);
		wsaaEndFlag.set("Y");
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		restdtyrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		restdtyrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
