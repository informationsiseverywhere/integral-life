/*
 * File: Ureserv.java
 * Date: 30 August 2009 2:52:48
 * Author: Quipoz Limited
 * 
 * Class transformed from URESERV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.reassurance.dataaccess.UtrndefTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Unit Reserve Calculation Subroutine.
*
* Overview
* ========
*
* This subroutine will return an estimated value (i.e. sum at
* risk) for a unit-linked component. It will apply the prices
* available as at the effective date of the calculation to the
* units attached to the component as at that date. No suurender
* penalties are applied. The value returned is the unit reserve
* amount to be used for cession creation and costing.
*
* This subroutine is the calculation program found when T6598 is  claim
* read with the reserve method from T5448 as the key.
*
* This subroutine will be called repeatedly for each fund for a
* given component. Only when all funds have been processed for
* a component will a ststus of ENDP be returned to the calling
* program.
*
* This subroutine will determine if any unit movements have
* occurred between the effective date of the calculation and
* the current position. If so, the effects of these should
* be subtracted from the accumulated value.
*
* Linkage Area
* ============
*
* CHDRCOY        PIC X(01)
* CHDRNUM        PIC X(08)
* PLAN-SUFFIX    PIC S9(04) COMP-3.
* POLSUM         PIC S9(04) COMP-3.
* LIFE           PIC X(02).
* JLIFE          PIC X(02).
* COVERAGE       PIC X(02).
* RIDER          PIC X(02).
* CRTABLE        PIC X(04).
* CRRCD          PIC S9(08) COMP-3.
* PTDATE         PIC S9(08) COMP-3.
* EFFDATE        PIC S9(08) COMP-3.
* CONV-UNITS     PIC S9(08) COMP-3.
* LANGUAGE       PIC X(01).
* ESTIMATED-VAL  PIC S9(15)V9(02) COMP-3.
* ACTUAL-VAL     PIC S9(15)V9(02) COMP-3.
* CURRCODE       PIC X(03).
* CHDR-CURR      PIC X(03).
* PSTATCODE      PIC X(02).
* ELEMENT        PIC X(01).
* DESCRIPTION    PIC X(30).
* SINGP          PIC S9(11)V99 COMP-3.
* BILLFREQ       PIC X(02).
* TYPE           PIC X(01).
* FUND           PIC X(04).
* STATUS         PIC X(04).
* ENDF           PIC X(01).
* NE-UNITS       PIC X(01).
* TM-UNITS       PIC X(01).
* PS-NOT-ALLWD   PIC X(01).
*
* These fields are all contained within SRCALCPY.
*
*****************************************************************
* </pre>
*/
public class Ureserv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "URESERV";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaInitialAmount = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaAccumAmount = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5);
	private PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(18, 5);

	private FixedLengthStringData wsaaUtype = new FixedLengthStringData(1);
	private Validator initialUnits = new Validator(wsaaUtype, "I");

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaSwitch, "Y");

	private FixedLengthStringData wsaaPlanSwitch = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaPlanSwitch, "W");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "P");
	private Validator summaryPart = new Validator(wsaaPlanSwitch, "S");

	private FixedLengthStringData wsaaNoMore = new FixedLengthStringData(1);
	private Validator endOfFund = new Validator(wsaaNoMore, "Y");
		/* TABLES */
	private static final String t5515 = "T5515";
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String itdmrec = "ITEMREC";
	private static final String utrndefrec = "UTRNDEFREC";
	private static final String utrsclmrec = "UTRSCLMREC";
	private static final String utrsrec = "UTRSREC";
	private static final String vprnudlrec = "VPRNUDLREC";
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private UtrndefTableDAM utrndefIO = new UtrndefTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
	private Srcalcpy srcalcpy = new Srcalcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr3180, 
		exit3190
	}

	public Ureserv() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		mainlineExit010();
	}

protected void para010()
	{
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		/* If first-time into the subroutine from the calling program,*/
		/* initialise all the required variables.*/
		if (firstTime.isTrue()) {
			wsaaChdrcoy.set(srcalcpy.chdrChdrcoy);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			wsaaPlanSuffix.set(srcalcpy.planSuffix);
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			wsaaVirtualFund.set(SPACES);
			wsaaUnitType.set(SPACES);
			srcalcpy.description.set(SPACES);
			/* Decide which part of the plan is being surrended*/
			if (isEQ(srcalcpy.planSuffix,ZERO)) {
				wsaaPlanSwitch.set("W");
			}
			else {
				if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)
				&& isNE(srcalcpy.polsum,1)) {
					wsaaPlanSuffix.set(0);
					wsaaPlanSwitch.set("S");
				}
				else {
					wsaaPlanSwitch.set("P");
				}
			}
		}
		if (summaryPart.isTrue()) {
			srcalcpy.planSuffix.set(ZERO);
		}
		wsaaNoMore.set("N");
		if (wholePlan.isTrue()) {
			readFundsWholePlan1000();
		}
		else {
			readFundsPartPlan2000();
		}
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void readFundsWholePlan1000()
	{
			para1010();
		}

protected void para1010()
	{
		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(wsaaChdrcoy);
		utrsIO.setChdrnum(wsaaChdrnum);
		utrsIO.setLife(wsaaLife);
		utrsIO.setCoverage(wsaaCoverage);
		utrsIO.setRider(wsaaRider);
		utrsIO.setPlanSuffix(ZERO);
		utrsIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsIO.setUnitType(wsaaUnitType);
		utrsIO.setFormat(utrsrec);
		utrsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsIO.getRider(),srcalcpy.covrRider)
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			srcalcpy.status.set(varcom.endp);
			wsaaSwitch.set("Y");
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(utrsIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsIO.getUnitType());
		while ( !(endOfFund.isTrue()
		|| isNE(utrsIO.getUnitVirtualFund(),wsaaVirtualFund)
		|| isNE(utrsIO.getUnitType(),wsaaUnitType))) {
			accumAmtWholePlan1100();
		}
		
		if (isEQ(utrsIO.getStatuz(),varcom.oK)
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			readUtrn3000();
			readVprc4000();
			readT55155000();
			wsaaVirtualFund.set(utrsIO.getUnitVirtualFund());
			wsaaUnitType.set(utrsIO.getUnitType());
			wsaaLife.set(utrsIO.getLife());
			wsaaRider.set(utrsIO.getRider());
			wsaaCoverage.set(utrsIO.getCoverage());
			wsaaChdrnum.set(utrsIO.getChdrnum());
			if (isEQ(srcalcpy.status,varcom.endp)) {
				wsaaSwitch.set("Y");
			}
			else {
				wsaaSwitch.set("N");
			}
		}
	}

protected void accumAmtWholePlan1100()
	{
		para1110();
	}

protected void para1110()
	{
		if (isEQ(utrsIO.getUnitType(),"I")) {
			wsaaInitialAmount.add(utrsIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
		utrsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsIO.getRider(),srcalcpy.covrRider)
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			srcalcpy.status.set(varcom.endp);
			wsaaNoMore.set("Y");
		}
	}

protected void readFundsPartPlan2000()
	{
			para2010();
		}

protected void para2010()
	{
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setChdrcoy(wsaaChdrcoy);
		utrsclmIO.setChdrnum(wsaaChdrnum);
		utrsclmIO.setLife(wsaaLife);
		utrsclmIO.setCoverage(wsaaCoverage);
		utrsclmIO.setRider(wsaaRider);
		utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
		utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsclmIO.setUnitType(wsaaUnitType);
		utrsclmIO.setFormat(utrsclmrec);
		utrsclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(),varcom.oK)
		&& isNE(utrsclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsclmIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(),srcalcpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(),varcom.endp)) {
			srcalcpy.status.set(varcom.endp);
			wsaaSwitch.set("Y");
			return ;
		}
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		while ( !(endOfFund.isTrue()
		|| isNE(utrsclmIO.getUnitVirtualFund(),wsaaVirtualFund)
		|| isNE(utrsclmIO.getUnitType(),wsaaUnitType))) {
			accumAmtPartPlan2100();
		}
		
		if (isEQ(utrsclmIO.getStatuz(),varcom.oK)
		|| isEQ(utrsclmIO.getStatuz(),varcom.endp)) {
			readUtrn3000();
			readVprc4000();
			readT55155000();
			wsaaChdrnum.set(utrsclmIO.getChdrnum());
			wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
			wsaaUnitType.set(utrsclmIO.getUnitType());
			wsaaLife.set(utrsclmIO.getLife());
			wsaaRider.set(utrsclmIO.getRider());
			wsaaCoverage.set(utrsclmIO.getCoverage());
			wsaaPlanSuffix.set(utrsclmIO.getPlanSuffix());
			if (isEQ(srcalcpy.status,varcom.endp)) {
				wsaaSwitch.set("Y");
			}
			else {
				wsaaSwitch.set("N");
			}
		}
	}

protected void accumAmtPartPlan2100()
	{
		para2110();
	}

protected void para2110()
	{
		if (isEQ(utrsclmIO.getUnitType(),"I")) {
			wsaaInitialAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
		utrsclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(),varcom.oK)
		&& isNE(utrsclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsclmIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(),srcalcpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(),varcom.endp)) {
			srcalcpy.status.set(varcom.endp);
			wsaaNoMore.set("Y");
		}
	}

protected void readUtrn3000()
	{
		utrn3010();
	}

protected void utrn3010()
	{
		utrndefIO.setParams(SPACES);
		utrndefIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		utrndefIO.setChdrnum(srcalcpy.chdrChdrnum);
		utrndefIO.setLife(wsaaLife);
		utrndefIO.setCoverage(wsaaCoverage);
		utrndefIO.setRider(wsaaRider);
		utrndefIO.setPlanSuffix(wsaaPlanSuffix);
		utrndefIO.setUnitVirtualFund(wsaaVirtualFund);
		utrndefIO.setUnitType(wsaaUnitType);
		utrndefIO.setMoniesDate(varcom.vrcmMaxDate);
		utrndefIO.setFormat(utrndefrec);
		utrndefIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrndefIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrndefIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "VRTFND", "UNITYP");		
	    while ( !(isEQ(utrndefIO.getStatuz(),varcom.endp))) {
			loopThruUtrn3100();
		}
		
	}

protected void loopThruUtrn3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					utrn3110();
				case nextr3180: 
					nextr3180();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void utrn3110()
	{
		SmartFileCode.execute(appVars, utrndefIO);
		if (isNE(utrndefIO.getStatuz(),varcom.oK)
		&& isNE(utrndefIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrndefIO.getParams());
			fatalError9000();
		}
		if (isNE(utrndefIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrndefIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrndefIO.getLife(),wsaaLife)
		|| isNE(utrndefIO.getCoverage(),wsaaCoverage)
		|| isNE(utrndefIO.getRider(),wsaaRider)
		|| isNE(utrndefIO.getLife(),wsaaLife)
		|| isNE(utrndefIO.getPlanSuffix(),wsaaPlanSuffix)
		|| isNE(utrndefIO.getUnitVirtualFund(),wsaaVirtualFund)
		|| isNE(utrndefIO.getUnitType(),wsaaUnitType)
		|| isEQ(utrndefIO.getStatuz(),varcom.endp)) {
			utrndefIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
		/* Only include UTRNs that have already been processed.*/
		if (isNE(utrndefIO.getFeedbackInd(),"Y")) {
			goTo(GotoLabel.nextr3180);
		}
		/* If the monies date is greater than the effective date, the*/
		/* UTRN movement occurred between the effective date of the*/
		/* calculation and the current position and needs to be reversed*/
		/* (i.e. subtracted from the total amount).*/
		if (isGT(utrndefIO.getMoniesDate(),srcalcpy.effdate)) {
			if (initialUnits.isTrue()) {
				wsaaInitialAmount.subtract(utrndefIO.getNofUnits());
			}
			else {
				wsaaAccumAmount.subtract(utrndefIO.getNofUnits());
			}
		}
	}

protected void nextr3180()
	{
		utrndefIO.setFunction(varcom.nextr);
	}

protected void readVprc4000()
	{
		read4010();
	}

protected void read4010()
	{
		/*  Read the VPRC file for each fund type.*/
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(srcalcpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType(wsaaUtype);
		vprnudlIO.setEffdate(srcalcpy.effdate);
		vprnudlIO.setFormat(vprnudlrec);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY", "VRTFND", "ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(),wsaaUtype)
		|| isNE(srcalcpy.chdrChdrcoy,vprnudlIO.getCompany())) {
			vprnudlIO.setCompany(srcalcpy.chdrChdrcoy);
			vprnudlIO.setUnitType(wsaaUtype);
			vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
			vprnudlIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (initialUnits.isTrue()) {
			wsaaInitialBidPrice.set(vprnudlIO.getUnitBidPrice());
			/*     MULTIPLY WSAA-INITIAL-AMOUNT BY  WSAA-INITIAL-BID-PRICE  */
			wsaaInitialAmount.multiply(wsaaInitialBidPrice);
			srcalcpy.type.set("I");
			srcalcpy.estimatedVal.set(wsaaInitialAmount);
		}
		else {
			wsaaAccumBidPrice.set(vprnudlIO.getUnitBidPrice());
			/*     MULTIPLY WSAA-ACCUM-AMOUNT   BY  WSAA-ACCUM-BID-PRICE    */
			wsaaAccumAmount.multiply(wsaaAccumBidPrice);
			srcalcpy.type.set("A");
			srcalcpy.estimatedVal.set(wsaaAccumAmount);
		}
		srcalcpy.fund.set(wsaaVirtualFund);
	}

protected void readT55155000()
	{
		t55155010();
	}

protected void t55155010()
	{
		descIO.setParams(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			srcalcpy.description.set(descIO.getLongdesc());
		}
		else {
			srcalcpy.description.set(SPACES);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVirtualFund);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund,itdmIO.getItemitem())
		|| isNE(srcalcpy.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			srcalcpy.currcode.set(SPACES);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			srcalcpy.currcode.set(t5515rec.currcode);
			srcalcpy.element.set(wsaaVirtualFund);
		}
	}

protected void fatalError9000()
	{
		/*ERROR*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
