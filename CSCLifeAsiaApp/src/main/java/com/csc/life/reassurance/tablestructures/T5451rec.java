package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:19
 * Description:
 * Copybook name: T5451REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5451rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5451Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData agerate = new ZonedDecimalData(3, 0).isAPartOf(t5451Rec, 0);
  	public ZonedDecimalData insprm = new ZonedDecimalData(6, 0).isAPartOf(t5451Rec, 3);
  	public ZonedDecimalData oppc = new ZonedDecimalData(5, 2).isAPartOf(t5451Rec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(486).isAPartOf(t5451Rec, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5451Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5451Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}