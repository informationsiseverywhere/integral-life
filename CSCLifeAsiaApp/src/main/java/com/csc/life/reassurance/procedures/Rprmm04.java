/*
 * File: Rprmm04.java
 * Date: 30 August 2009 2:13:34
 * Author: Quipoz Limited
 * 
 * Class transformed from RPRMM04.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.life.reassurance.recordstructures.Rprmiumrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Reassurance Premium Calculation Method 04 -
*  Age Based Calculation subroutine
*
*
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
*
* Build a key.  This key (see below) will read table T5664.
* This table contains the parameters to be used in the
* calculation of the Basic Annual Premium for
* Coverage/Rider components.
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* Mortality Class
* Sex
*
* Access the required table by reading the table directly (ITDM).
* The contents of the table are then stored. This table is dated
* use:
*
*  1) Rating Date
*
* CALCULATE-BASIC-ANNNUAL-PREMIUM (and apply age rates)
* (Age, Sex & Duration taken from linkage)
*
* Obtain the age rates from the (LEXT) record.
*
*  - read all the LEXT records for this contract, life and
*  coverage into the working-storage table. Compute the
*  adjusted age as being the summation of the LEXT age
*  rates plus the ANB @ RCD.
*
* Use the age calculated above to access the table T5664 and
* check the following:
*
*  - that the basic annual premium (indexed by year) from
*  the T5664 table is not zero. If it is zero, then display
*  an error message and skip the additional procedures.
*  Otherwise store the premium as the (BAP).
*
* - we should now have an age rated BAP.
*
* APPLY-RATE-PER-MILLE-LOADINGS
*
* - sum the rates per mille from the LEXT W/S table.
*
*  - add rates per mille to the BAP
*
* - we should now have a BAP with rates / mille applied.
*
* APPLY-DISCOUNT.
*
* Access the discount table T5659 using the key:-
*
* - Discount method from T5664 concatenated with currency.
*
*  - check the sum insured against the volume band ranges
*  and when within a range store the discount amount.
*
*  - compute the BAP as the BAP - discount
*
* - we should now have a BAP with discount applied.
*
* REASRATFAC :
*  - The calling program passes in this value which is from
*    T5450 with item setted up by T5449-RPRMMTH
*
*  - compute the BAP = BAP * reassurance-rate-factor,
*    when it is not = 0.
*
*  - we should now have a BAP with reassurance rate factor
*    applied.
*
* APPLY-PREMIUM-UNIT
*
* - Obtain the risk-unit from T5664
*
*  - multiply BAP by the sum-insured and divide
*    by the risk-unit
*
* - we should now have a BAP with premium applied.
*
* APPLY-PERCENTAGE-LOADINGS
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the BAP as follows:
*
*  BAP = BAP * loading percentage / 100.
*
* - we should now have a loaded BAP.
*
* CALCULATE-INSTALMENT-PREMIUM.
*
* Determine which billing frequency to use.
*
* compute the basic-instalment-premium (BIP) as:-
*
* basic-premium * factor (FACTOR is from T5664).
*
* CALCULATE-ROUNDING.
*
* - round up depending on the rounding factor (obtained from
* the T5659 table).
*
* - if the prem-unit from T5664 is greater than zero, then
* compute the BIP as the rounded number / the premium-unit
* (from T5664). The premium unit is the quantity in which the
* currency is denominated.
*
* CALCULATE-THE-ANNUAL-PREMIUM.
*
* There is no need to calculate the annual premium, because
* at issue time, when the COVR records are being created from
* the COVT records, the annual premium will then be calculated.
*
*****************************************************************
* </pre>
*/
public class Rprmm04 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RPRMM04";
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String t070 = "T070";
		/* TABLES */
	private static final String t5664 = "T5664";
	private static final String t5659 = "T5659";
	private static final String itdmrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaModalFactor = new PackedDecimalData(5, 4);
		/*01  FILLER REDEFINES WSAA-ROUND-NUM.                             
		01  FILLER REDEFINES WSAA-ROUND-NUM.                             
		01  FILLER REDEFINES WSAA-ROUND-NUM.                             
		01  FILLER REDEFINES WSAA-ROUND-NUM.                             */
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler, 12).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 13).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler4, 14).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler6, 15);

	private FixedLengthStringData wsaaT5664Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5664Rrate = new FixedLengthStringData(4).isAPartOf(wsaaT5664Key, 0);
	private FixedLengthStringData wsaaT5664Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 4);
	private FixedLengthStringData wsaaT5664Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 5);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5664rec t5664rec = new T5664rec();
	private T5659rec t5659rec = new T5659rec();
	private Rprmiumrec rprmiumrec = new Rprmiumrec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private List<Lextpf> lextpfSearchList = new ArrayList<Lextpf>();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readLext210, 
		loopForAdjustedAge220, 
		checkSumInsuredRange430, 
		calculateLoadings610
	}

	public Rprmm04() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rprmiumrec.rprmiumRec = convertAndSetParam(rprmiumrec.rprmiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		rprmiumrec.statuz.set(varcom.oK);
		initialize100();
		basicAnnualPremium200();
		ratesPerMillieLoadings300();
		volumeDiscountBap1400();
		applyRateFactor450();
		premiumUnit500();
		percentageLoadings600();
		instalmentPremium700();
		rounding800();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		para100();
	}

protected void para100()
	{
		/* Initialise all working storage fields and set keys to read*/
		/* tables. Include a table (occurs 8) to hold the Options/Extras*/
		/* (LEXT) record details.*/
		wsaaRatesPerMillieTot.set(ZERO);
		wsaaBap.set(ZERO);
		wsaaBip.set(ZERO);
		wsaaAdjustedAge.set(ZERO);
		wsaaDiscountAmt.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaT5659Key.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 8)); wsaaSub.add(1)){
			wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
		}
		/* Build a key.*/
		/* This key (see below)*/
		/* will read table T5664. This table contains the parameters to be*/
		/* used in the calculation of the Basic Annual Premium for*/
		/* Coverage/Rider components.*/
		/* The key is a concatenation of the following fields:-*/
		/*  1) Premium rate setted by calling program from T5450. It is*/
		/*     T5450-SLCTRATE or T5450-ULTMRATE.*/
		/*     In order to decide whether to use which rate, take the*/
		/*     Cession Commencement date on RACD record, and the Costing*/
		/*     Commencement date  and find the differance between them.*/
		/*     If the difference is < Discount/Commission Period specified*/
		/*     on T5450 then the record is within the Discount period*/
		/*     and as such the Select Rate Tables must be used, otherwisw*/
		/*     the Ultimate Rate Tables must be used.*/
		/*  2) Mortality Class*/
		/*  3) Sex*/
		/* Access the required table by reading the table directly (ITDM).*/
		/* The contents of the table are then stored. This table is dated*/
		/* use:*/
		/*  1) Rating Date*/

		wsaaT5664Rrate.set(rprmiumrec.rrate);
		wsaaT5664Mortcls.set(rprmiumrec.mortcls);
		wsaaT5664Sex.set(rprmiumrec.lsex);
		//performance improvement -- Anjali
		int itemFrom;
		if (isEQ(rprmiumrec.ratingdate, ZERO)) {
			itemFrom = varcom.vrcmMaxDate.toInt();
		}
		else {
			itemFrom = rprmiumrec.ratingdate.toInt();
		}

		List<Itempf> itempfList = itemDAO.getItdmByFrmdate(rprmiumrec.chdrChdrcoy.toString(),t5664,wsaaT5664Key.toString(),itemFrom);
		if (null != itempfList && !itempfList.isEmpty()) { 
			t5664rec.t5664Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			return;
		}
		else{
			syserrrec.params.set("IT".concat(rprmiumrec.chdrChdrcoy.toString()).concat(wsaaT5664Key.toString()));
			dbError580();
		}
	}

protected void basicAnnualPremium200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey200();
				case readLext210: 
					readLext210();
				case loopForAdjustedAge220: 
					loopForAdjustedAge220();
					checkT5664Insprm230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupLextKey200()
	{
		/*  - Read all the LEXT records for this contract, life and*/
		/*  coverage into the working-storage table. Compute the*/
		/*  adjusted age as being the summation of the LEXT age*/
		/*  rates plus the ANB @ RCD.*/
		Lextpf lextpf = new Lextpf();
		lextpf.setChdrcoy(rprmiumrec.chdrChdrcoy.toString());
		lextpf.setChdrnum(rprmiumrec.chdrChdrnum.toString());
		lextpf.setLife(rprmiumrec.lifeLife.toString());
		lextpf.setCoverage(rprmiumrec.covrCoverage.toString());
		lextpf.setRider(rprmiumrec.covrRider.toString());
		lextpfSearchList = lextpfDAO.getLextpfListRecord(lextpf);
		//performance improvement -- Anjali
		wsaaSub.set(0);
	}

protected void readLext210()
	{

			for (Lextpf lextpf : lextpfSearchList){
			/*NEXT_SENTENCE*/
		/*  Skip any expired special terms.*/
				if (isLTE(lextpf.getExtCessDate(), rprmiumrec.reRateDate)) {
					continue;
		}
		if (isEQ(rprmiumrec.reasind, "2")
						&& isEQ(lextpf.getReasind(), "1")) {
					continue;
		}
		if (isNE(rprmiumrec.reasind, "2")
						&& isEQ(lextpf.getReasind(), "2")) {
					continue;
		}
		wsaaSub.add(1);
				wsaaLextOppc[wsaaSub.toInt()].set(lextpf.getOppc());
				wsaaRatesPerMillieTot.add(lextpf.getInsprm());
				wsaaAgerateTot.add(lextpf.getAgerate());
			}
			goTo(GotoLabel.loopForAdjustedAge220);
	}

protected void loopForAdjustedAge220()
	{
		compute(wsaaAdjustedAge, 0).set((add(wsaaAgerateTot, rprmiumrec.lage)));
	}

protected void checkT5664Insprm230()
	{
		/* Use the age calculated above to access the table T5664 and*/
		/* check the following:*/
		/*  - that the basic annual premium (indexed by year) from*/
		/*  the T5664 table is not zero. If it is zero, then display*/
		/*  an error message and skip the additional procedures.*/
		/*  Otherwise store the premium as the (BAP).*/
		/*    IF WSAA-ADJUSTED-AGE        < 1                     <V42L018>*/
		if (isLT(wsaaAdjustedAge, 0)) {
			/*    MOVE 100                 TO WSAA-ADJUSTED-AGE     <LA4754>*/
			wsaaAdjustedAge.set(110);
		}
		/*    IF WSAA-ADJUSTED-AGE        < 1                              */
		if (isLT(wsaaAdjustedAge, 0)
		|| isGT(wsaaAdjustedAge, 110)) {
			syserrrec.statuz.set(e107);
			syserr570();
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge, 0)) {
			if (isEQ(t5664rec.insprem, ZERO)) {
				syserrrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5664rec.insprem);
			}
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE         =  100                          */
		/*       IF T5664-INSTPR           =  ZERO                         */
		/*          MOVE E107             TO SYSR-STATUZ                   */
		/*          PERFORM 570-SYSERR                                     */
		/*       ELSE                                                      */
		/*          MOVE T5664-INSTPR     TO WSAA-BAP                      */
		/*       END-IF                                                    */
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			if ((setPrecision(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], 0)
			&& isEQ(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], ZERO))) {
				syserrrec.statuz.set(e107);
				syserr570();
			}
			else {
				compute(wsaaBap, 2).set(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()]);
			}
		}
		else {
			if (isEQ(t5664rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				syserrrec.statuz.set(e107);
				syserr570();
			}
			else {
				wsaaBap.set(t5664rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void ratesPerMillieLoadings300()
	{
		/*PARA*/
		/* APPLY-RATE-PER-MILLE-LOADINGS*/
		/* - sum the rates per mille from the LEXT W/S table.*/
		/* - add rates per mille to the BAP*/
		compute(wsaaBap, 2).set(add(wsaaRatesPerMillieTot, wsaaBap));
		/*EXIT*/
	}

protected void volumeDiscountBap1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readT5659410();
				case checkSumInsuredRange430: 
					checkSumInsuredRange430();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT5659410()
	{
		/* Access the discount table T5659 using the key:-*/
		/* - Discount method from T5664 concatenated with currency.*/
		/* - check the sum insurred against the volume band ranges*/
		/*   and when within a range store the discount amount.*/
		/* - compute the BAP as the BAP - discount*/

		wsaaDisccntmeth.set(t5664rec.disccntmeth);
		wsaaCurrcode.set(rprmiumrec.currcode);
		
		int itemFrom;
		if (isEQ(rprmiumrec.ratingdate, ZERO)) {
			itemFrom = varcom.vrcmMaxDate.toInt();
		}
		else {
			itemFrom = rprmiumrec.ratingdate.toInt();
		}
		List<Itempf> itempfList = itemDAO.getItdmByFrmdate(rprmiumrec.chdrChdrcoy.toString(),t5659,wsaaT5659Key.toString(),itemFrom);
		if (null != itempfList && !itempfList.isEmpty()) { 
			t5659rec.t5659Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			return;
		}
		else{
			syserrrec.params.set("IT".concat(t5659).concat(wsaaT5659Key.toString()));
			dbError580();
		}
		wsaaSub.set(0);
	}

protected void checkSumInsuredRange430()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 4)) {
			return ;
		}
		if (isLT(rprmiumrec.sumin, t5659rec.volbanfr[wsaaSub.toInt()])
		|| isGT(rprmiumrec.sumin, t5659rec.volbanto[wsaaSub.toInt()])) {
			goTo(GotoLabel.checkSumInsuredRange430);
		}
		else {
			wsaaDiscountAmt.set(t5659rec.volbanle[wsaaSub.toInt()]);
		}
		compute(wsaaBap, 2).set(sub(wsaaBap, wsaaDiscountAmt));
		/*EXIT*/
	}

protected void applyRateFactor450()
	{
		/*RATES*/
		/*  The RPRM-REASRATFAC is setted up in calling program by*/
		/*  reading T5450 with item from T5449-RPRMMTH*/
		if (isNE(rprmiumrec.reasratfac, ZERO)) {
			compute(wsaaBap, 2).set(mult(wsaaBap, rprmiumrec.reasratfac));
		}
		/*EXIT*/
	}

protected void premiumUnit500()
	{
		/*PARA*/
		/* APPLY-PREMIUM-UNIT*/
		/* - Obtain the risk-unit from T5664*/
		/* - multiply BAP by the sum-insured and divide*/
		/*   by the risk-unit*/
		compute(wsaaBap, 2).set((div((mult(wsaaBap, rprmiumrec.sumin)), t5664rec.unit)));
		/*EXIT*/
	}

protected void percentageLoadings600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para600();
				case calculateLoadings610: 
					calculateLoadings610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para600()
	{
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the BAP as follows:*/
		/*  BAP = BAP * loading percentage / 100.*/
		wsaaSub.set(0);
	}

protected void calculateLoadings610()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			return ;
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()], 0)) {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, wsaaLextOppc[wsaaSub.toInt()])), 100)));
		}
		goTo(GotoLabel.calculateLoadings610);
	}

protected void instalmentPremium700()
	{
		para700();
		instalmentPrem710();
	}

protected void para700()
	{
		/* CALCULATE-INSTALMENT-PREMIUM.*/
		/* Determine which billing frequency to use.*/
		/* compute the basic-instalment-premium (BIP) as:-*/
		/* basic-premium * factor (FACTOR is from T5664).*/
		wsaaBip.set(0);
	}

protected void instalmentPrem710()
	{
		wsaaModalFactor.set(0);
		if (isEQ(rprmiumrec.billfreq, "01")
		|| isEQ(rprmiumrec.billfreq, "00")) {
			wsaaModalFactor.set(t5664rec.mfacty);
		}
		if (isEQ(rprmiumrec.billfreq, "02")) {
			wsaaModalFactor.set(t5664rec.mfacthy);
		}
		if (isEQ(rprmiumrec.billfreq, "04")) {
			wsaaModalFactor.set(t5664rec.mfactq);
		}
		if (isEQ(rprmiumrec.billfreq, "12")) {
			wsaaModalFactor.set(t5664rec.mfactm);
		}
		if (isEQ(rprmiumrec.billfreq, "13")) {
			wsaaModalFactor.set(t5664rec.mfact4w);
		}
		if (isEQ(rprmiumrec.billfreq, "24")) {
			wsaaModalFactor.set(t5664rec.mfacthm);
		}
		if (isEQ(rprmiumrec.billfreq, "26")) {
			wsaaModalFactor.set(t5664rec.mfact2w);
		}
		if (isEQ(rprmiumrec.billfreq, "52")) {
			wsaaModalFactor.set(t5664rec.mfactw);
		}
		if (isEQ(wsaaModalFactor, 0)) {
			syserrrec.statuz.set(t070);
			syserr570();
		}
		else {
			compute(wsaaBip, 4).set(mult(wsaaBap, wsaaModalFactor));
		}
	}

protected void rounding800()
	{
		para800();
		rounded820();
	}

protected void para800()
	{
		/* - round up depending on the rounding factor (obtained from*/
		/* the T5659 table).*/
		/* - if the prem-unit from T5664 is greater than zero, then*/
		/* compute the BIP as the rounded number / the premium-unit*/
		/* (from T5664). The premium unit is the quantity in which the*/
		/* currency is dominated in.*/
		wsaaRoundNum.set(wsaaBip);
		if (isEQ(t5659rec.rndfact, 1)
		|| isEQ(t5659rec.rndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 10)) {
			if (isLT(wsaaRound1, 5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound100.set(0);
			}
		}
	}

protected void rounded820()
	{
		wsaaBip.set(wsaaRoundNum);
		if (isEQ(t5664rec.premUnit, 0)) {
			rprmiumrec.calcPrem.set(wsaaBip);
		}
		else {
			compute(rprmiumrec.calcPrem, 2).set((div(wsaaBip, t5664rec.premUnit)));
		}
		/*EXIT*/
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rprmiumrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rprmiumrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
