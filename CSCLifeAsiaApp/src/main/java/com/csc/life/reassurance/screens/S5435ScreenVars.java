package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5435
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5435ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(237+DD.cltaddr.length*5); //Starts ILIFE-3212
	public FixedLengthStringData dataFields = new FixedLengthStringData(77+DD.cltaddr.length*5).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltaddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 8);
	public FixedLengthStringData[] cltaddr = FLSArrayPartOfStructure(4, DD.cltaddr.length, cltaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(cltaddrs.length()).isAPartOf(cltaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(filler,0);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*1);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*2);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*3);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*4);
	public ZonedDecimalData cltdob = DD.cltdob.copyToZonedDecimal().isAPartOf(dataFields,8+DD.cltaddr.length*5);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,16+DD.cltaddr.length*5);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,63+DD.cltaddr.length*5);
	public FixedLengthStringData cltsex = DD.cltsex.copy().isAPartOf(dataFields,73+DD.cltaddr.length*5);
	public FixedLengthStringData ctrycode = DD.ctrycode.copy().isAPartOf(dataFields,74+DD.cltaddr.length*5);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 77+DD.cltaddr.length*5); //End ILIFE-3212
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltaddrsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] cltaddrErr = FLSArrayPartOfStructure(4, 4, cltaddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(cltaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData cltaddr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData cltaddr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData cltaddr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData cltdobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cltsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctrycodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 117+DD.cltaddr.length*5); //ILIFE-3212
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData cltaddrsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] cltaddrOut = FLSArrayPartOfStructure(4, 12, cltaddrsOut, 0);
	public FixedLengthStringData[][] cltaddrO = FLSDArrayPartOfArrayStructure(12, 1, cltaddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(cltaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltaddr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] cltaddr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] cltaddr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] cltaddr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] cltdobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cltsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctrycodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(218);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(72).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,13);
	public FixedLengthStringData lrkcls = DD.lrkcls.copy().isAPartOf(subfileFields,15);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,19);
	public ZonedDecimalData ssreasf = DD.ssreasf.copyToZonedDecimal().isAPartOf(subfileFields,21);
	public ZonedDecimalData ssreast = DD.ssreast.copyToZonedDecimal().isAPartOf(subfileFields,38);
	public ZonedDecimalData ssretn = DD.ssretn.copyToZonedDecimal().isAPartOf(subfileFields,55);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 72);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData currencyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lrkclsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData ssreasfErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData ssreastErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData ssretnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 108);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] currencyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lrkclsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] ssreasfOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] ssreastOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] ssretnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 216);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData cltdobDisp = new FixedLengthStringData(10);

	public LongData S5435screensflWritten = new LongData(0);
	public LongData S5435screenctlWritten = new LongData(0);
	public LongData S5435screenWritten = new LongData(0);
	public LongData S5435protectWritten = new LongData(0);
	public GeneralTable s5435screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5435screensfl;
	}

	public S5435ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {lrkcls, currency, ssretn, ssreast, ssreasf, chdrnum, life, coverage, rider};
		screenSflOutFields = new BaseData[][] {lrkclsOut, currencyOut, ssretnOut, ssreastOut, ssreasfOut, chdrnumOut, lifeOut, coverageOut, riderOut};
		screenSflErrFields = new BaseData[] {lrkclsErr, currencyErr, ssretnErr, ssreastErr, ssreasfErr, chdrnumErr, lifeErr, coverageErr, riderErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, cltname, cltaddr01, cltaddr02, cltaddr03, cltaddr04, cltpcode, ctrycode, cltsex, cltdob};
		screenOutFields = new BaseData[][] {clntnumOut, cltnameOut, cltaddr01Out, cltaddr02Out, cltaddr03Out, cltaddr04Out, cltpcodeOut, ctrycodeOut, cltsexOut, cltdobOut};
		screenErrFields = new BaseData[] {clntnumErr, cltnameErr, cltaddr01Err, cltaddr02Err, cltaddr03Err, cltaddr04Err, cltpcodeErr, ctrycodeErr, cltsexErr, cltdobErr};
		screenDateFields = new BaseData[] {cltdob};
		screenDateErrFields = new BaseData[] {cltdobErr};
		screenDateDispFields = new BaseData[] {cltdobDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5435screen.class;
		screenSflRecord = S5435screensfl.class;
		screenCtlRecord = S5435screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5435protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5435screenctl.lrec.pageSubfile);
	}
}
