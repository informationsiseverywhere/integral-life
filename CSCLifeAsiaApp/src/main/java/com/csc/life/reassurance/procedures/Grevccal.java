/*
 * File: Grevccal.java
 * Date: 29 August 2009 22:50:46
 * Author: Quipoz Limited
 *
 * Class transformed from GREVCCAL.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.reassurance.dataaccess.CcalTableDAM;
import com.csc.life.reassurance.dataaccess.CcalrevTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Generic Cash Call Reversal Subroutine.
*
* This is a new subroutine which forms part of Reassurance
* Reversals. It is called upon via T5671 to reverse the cash
* call transaction.
*
* It will be called using the standard generic reversal linkage
* copybook, GREVERSREC. It will delete all CCAL records created
* by the transaction being reversed and it will reinstate the
* previous validflag '2' records.
*
* This subroutine is performed at component level.
*
*****************************************************************
* </pre>
*/
public class Grevccal extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GREVCCAL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String ccalrec = "CCALREC";
	private String ccalrevrec = "CCALREVREC";
		/*Claim Cash Call*/
	private CcalTableDAM ccalIO = new CcalTableDAM();
		/*Cash Call Logical For Reversals*/
	private CcalrevTableDAM ccalrevIO = new CcalrevTableDAM();
	private Greversrec greversrec = new Greversrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr1080,
		exit1090,
		exit1190
	}

	public Grevccal() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		greversrec.statuz.set(varcom.oK);
		ccalIO.setParams(SPACES);
		ccalIO.setChdrcoy(greversrec.chdrcoy);
		ccalIO.setChdrnum(greversrec.chdrnum);
		ccalIO.setLife(greversrec.life);
		ccalIO.setCoverage(greversrec.coverage);
		ccalIO.setRider(greversrec.rider);
		ccalIO.setPlanSuffix(greversrec.planSuffix);
		ccalIO.setSeqno(ZERO);
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ccalIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ccalIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(ccalIO.getStatuz(),varcom.endp))) {
			processCcal1000();
		}

	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void processCcal1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					ccal1010();
				}
				case nextr1080: {
					nextr1080();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void ccal1010()
	{
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)
		&& isNE(ccalIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ccalIO.getParams());
			dbError580();
		}
		if (isNE(ccalIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(ccalIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(ccalIO.getLife(),greversrec.life)
		|| isNE(ccalIO.getCoverage(),greversrec.coverage)
		|| isNE(ccalIO.getRider(),greversrec.rider)
		|| isNE(ccalIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(ccalIO.getStatuz(),varcom.endp)) {
			ccalIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(ccalIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr1080);
		}
		if (isNE(ccalIO.getValidflag(),"1")) {
			syserrrec.params.set(ccalIO.getParams());
			dbError580();
		}
		deleteUpdateCcal1100();
	}

protected void nextr1080()
	{
		ccalIO.setFunction(varcom.nextr);
	}

protected void deleteUpdateCcal1100()
	{
		try {
			ccal1110();
		}
		catch (GOTOException e){
		}
	}

protected void ccal1110()
	{
		ccalrevIO.setParams(SPACES);
		ccalrevIO.setRrn(ccalIO.getRrn());
		ccalrevIO.setFormat(ccalrevrec);
		ccalrevIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, ccalrevIO);
		if (isNE(ccalrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalrevIO.getParams());
			dbError580();
		}
		ccalrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, ccalrevIO);
		if (isNE(ccalrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalrevIO.getParams());
			dbError580();
		}
		ccalrevIO.setParams(SPACES);
		ccalrevIO.setChdrcoy(ccalIO.getChdrcoy());
		ccalrevIO.setChdrnum(ccalIO.getChdrnum());
		ccalrevIO.setLife(ccalIO.getLife());
		ccalrevIO.setCoverage(ccalIO.getCoverage());
		ccalrevIO.setRider(ccalIO.getRider());
		ccalrevIO.setPlanSuffix(ccalIO.getPlanSuffix());
		ccalrevIO.setSeqno(ccalIO.getSeqno());
		ccalrevIO.setTranno(ccalIO.getTranno());
		ccalrevIO.setFormat(ccalrevrec);
		ccalrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ccalrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ccalrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "SEQNO");
		SmartFileCode.execute(appVars, ccalrevIO);
		if (isNE(ccalrevIO.getStatuz(),varcom.oK)
		&& isNE(ccalrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ccalrevIO.getParams());
			dbError580();
		}
		if (isNE(ccalIO.getChdrcoy(),ccalrevIO.getChdrcoy())
		|| isNE(ccalIO.getChdrnum(),ccalrevIO.getChdrnum())
		|| isNE(ccalIO.getLife(),ccalrevIO.getLife())
		|| isNE(ccalIO.getCoverage(),ccalrevIO.getCoverage())
		|| isNE(ccalIO.getRider(),ccalrevIO.getRider())
		|| isNE(ccalIO.getPlanSuffix(),ccalrevIO.getPlanSuffix())
		|| isNE(ccalIO.getSeqno(),ccalrevIO.getSeqno())
		|| isEQ(ccalrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
		ccalrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ccalrevIO);
		if (isNE(ccalrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalrevIO.getParams());
			dbError580();
		}
		ccalrevIO.setValidflag("1");
		ccalrevIO.setCurrto(varcom.vrcmMaxDate);
		ccalrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ccalrevIO);
		if (isNE(ccalrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalrevIO.getParams());
			dbError580();
		}
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
