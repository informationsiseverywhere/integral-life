package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:19
 * Description:
 * Copybook name: RACDBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdbrkKey = new FixedLengthStringData(64).isAPartOf(racdbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(racdbrkKey, 0);
  	public FixedLengthStringData racdbrkChdrnum = new FixedLengthStringData(8).isAPartOf(racdbrkKey, 1);
  	public PackedDecimalData racdbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdbrkKey, 9);
  	public FixedLengthStringData racdbrkLife = new FixedLengthStringData(2).isAPartOf(racdbrkKey, 12);
  	public FixedLengthStringData racdbrkCoverage = new FixedLengthStringData(2).isAPartOf(racdbrkKey, 14);
  	public FixedLengthStringData racdbrkRider = new FixedLengthStringData(2).isAPartOf(racdbrkKey, 16);
  	public PackedDecimalData racdbrkSeqno = new PackedDecimalData(2, 0).isAPartOf(racdbrkKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(racdbrkKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}