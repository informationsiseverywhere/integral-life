package com.csc.life.reassurance.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5457.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5457Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10);
	private ZonedDecimalData compay = new ZonedDecimalData(17, 2);
	private FixedLengthStringData costdate = new FixedLengthStringData(10);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10);
	private FixedLengthStringData dateto = new FixedLengthStringData(10);
	private FixedLengthStringData origcurr = new FixedLengthStringData(3);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData prem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0);
	private FixedLengthStringData rcstfrq = new FixedLengthStringData(2);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4);
	private FixedLengthStringData rngmntdesc = new FixedLengthStringData(30);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5457Report() {
		super();
	}


	/**
	 * Print the XML for R5457d01
	 */
	public void printR5457d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 9, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 17, 37));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 54, 4));
		cmdate.setFieldName("cmdate");
		cmdate.setInternal(subString(recordData, 58, 10));
		rcestrm.setFieldName("rcestrm");
		rcestrm.setInternal(subString(recordData, 68, 3));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 71, 17));
		raamount.setFieldName("raamount");
		raamount.setInternal(subString(recordData, 88, 17));
		origcurr.setFieldName("origcurr");
		origcurr.setInternal(subString(recordData, 105, 3));
		prem.setFieldName("prem");
		prem.setInternal(subString(recordData, 108, 17));
		rcstfrq.setFieldName("rcstfrq");
		rcstfrq.setInternal(subString(recordData, 125, 2));
		compay.setFieldName("compay");
		compay.setInternal(subString(recordData, 127, 17));
		costdate.setFieldName("costdate");
		costdate.setInternal(subString(recordData, 144, 10));
		printLayout("R5457d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				clntnum,
				clntnm,
				crtable,
				cmdate,
				rcestrm,
				sumins,
				raamount,
				origcurr,
				prem,
				rcstfrq,
				compay,
				costdate
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5457h01
	 */
	public void printR5457h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		rasnum.setFieldName("rasnum");
		rasnum.setInternal(subString(recordData, 1, 8));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 9, 37));
		rngmnt.setFieldName("rngmnt");
		rngmnt.setInternal(subString(recordData, 46, 4));
		rngmntdesc.setFieldName("rngmntdesc");
		rngmntdesc.setInternal(subString(recordData, 50, 30));
		datefrm.setFieldName("datefrm");
		datefrm.setInternal(subString(recordData, 80, 10));
		dateto.setFieldName("dateto");
		dateto.setInternal(subString(recordData, 90, 10));
		printLayout("R5457h01",			// Record name
			new BaseData[]{			// Fields:
				pagnbr,
				rasnum,
				clntnm,
				rngmnt,
				rngmntdesc,
				datefrm,
				dateto
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for R5457t01
	 */
	public void printR5457t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		origcurr.setFieldName("origcurr");
		origcurr.setInternal(subString(recordData, 1, 3));
		prem.setFieldName("prem");
		prem.setInternal(subString(recordData, 4, 17));
		compay.setFieldName("compay");
		compay.setInternal(subString(recordData, 21, 17));
		printLayout("R5457t01",			// Record name
			new BaseData[]{			// Fields:
				origcurr,
				prem,
				compay
			}
		);

		currentPrintLine.add(1);
	}


}
