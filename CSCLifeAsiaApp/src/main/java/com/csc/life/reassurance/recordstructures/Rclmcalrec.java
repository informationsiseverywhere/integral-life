package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:27
 * Description:
 * Copybook name: RCLMCALREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rclmcalrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rclmcalRec = new FixedLengthStringData(148);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rclmcalRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rclmcalRec, 5);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rclmcalRec, 6);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rclmcalRec, 14);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rclmcalRec, 16);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rclmcalRec, 18);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(rclmcalRec, 20);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(rclmcalRec, 23);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rclmcalRec, 26);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(rclmcalRec, 31);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 0, FILLER);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rclmcalRec, 50);
  	public PackedDecimalData seqno = new PackedDecimalData(2, 0).isAPartOf(rclmcalRec, 53);
  	public PackedDecimalData raAmount = new PackedDecimalData(17, 2).isAPartOf(rclmcalRec, 55);
  	public FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(rclmcalRec, 64);
  	public FixedLengthStringData reservprog = new FixedLengthStringData(7).isAPartOf(rclmcalRec, 72);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(rclmcalRec, 79);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(rclmcalRec, 81);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(rclmcalRec, 86);
  	public FixedLengthStringData acctcurr = new FixedLengthStringData(3).isAPartOf(rclmcalRec, 89);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rclmcalRec, 92);
  	public FixedLengthStringData crtable = new FixedLengthStringData(5).isAPartOf(rclmcalRec, 95);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(rclmcalRec, 100);
  	public PackedDecimalData convUnits = new PackedDecimalData(8, 0).isAPartOf(rclmcalRec, 105);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(rclmcalRec, 110);
  	public PackedDecimalData singp = new PackedDecimalData(17, 2).isAPartOf(rclmcalRec, 112);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(rclmcalRec, 121);
  	public PackedDecimalData clmPercent = new PackedDecimalData(5, 2).isAPartOf(rclmcalRec, 130);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(1).isAPartOf(rclmcalRec, 133);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rclmcalRec, 134);
  	public PackedDecimalData recovAmt = new PackedDecimalData(17, 2).isAPartOf(rclmcalRec, 135);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rclmcalRec, 144);


	public void initialize() {
		COBOLFunctions.initialize(rclmcalRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rclmcalRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}