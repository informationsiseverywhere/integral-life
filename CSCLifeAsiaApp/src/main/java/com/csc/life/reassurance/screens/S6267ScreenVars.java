package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6267
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6267ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(366);
	public FixedLengthStringData dataFields = new FixedLengthStringData(174).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,58);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,68);
	public FixedLengthStringData payfrqd = DD.payfrqd.copy().isAPartOf(dataFields,85);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(dataFields,95);
	public FixedLengthStringData rapayfrq = DD.rapayfrq.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData ratype = DD.ratype.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData ratypeDesc = DD.ratypedesc.copy().isAPartOf(dataFields,126);
	public ZonedDecimalData totsuma = DD.totsuma.copyToZonedDecimal().isAPartOf(dataFields,156);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 174);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData payfrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData rapayfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ratypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ratypedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData totsumaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 222);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] payfrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] rapayfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ratypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ratypedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] totsumaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);

	public LongData S6267screenWritten = new LongData(0);
	public LongData S6267windowWritten = new LongData(0);
	public LongData S6267protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6267ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rasnumOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rapayfrqOut,new String[] {"02","50","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratypeOut,new String[] {"03","50","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(raamountOut,new String[] {"04","50","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"05","50","-05",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {rasnum, cltname, rapayfrq, payfrqd, ratype, ratypeDesc, totsuma, raAmount, instPrem, currcode, currds, btdate};
		screenOutFields = new BaseData[][] {rasnumOut, cltnameOut, rapayfrqOut, payfrqdOut, ratypeOut, ratypedescOut, totsumaOut, raamountOut, instprmOut, currcodeOut, currdsOut, btdateOut};
		screenErrFields = new BaseData[] {rasnumErr, cltnameErr, rapayfrqErr, payfrqdErr, ratypeErr, ratypedescErr, totsumaErr, raamountErr, instprmErr, currcodeErr, currdsErr, btdateErr};
		screenDateFields = new BaseData[] {btdate};
		screenDateErrFields = new BaseData[] {btdateErr};
		screenDateDispFields = new BaseData[] {btdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6267screen.class;
		protectRecord = S6267protect.class;
	}

}
