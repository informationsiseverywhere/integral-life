/*
 * File: P5431.java
 * Date: 30 August 2009 0:25:18
 * Author: Quipoz Limited
 * 
 * Class transformed from P5431.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.screens.S5431ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5431 - Reassurance Costing Selection.
*
* This program forms part of the Reassurance Development.
* It is run off the Reassurance Costing Submenu screen, S5433.
* It allows component selection for a contract for the enquiry
* of RECO records.
* It is a scrolling selection subfile. Switching after component
* selection is controlled by OPTSWCH.
*
* 1000-INITIALISE.
*
* Bypass this section if returning from a previous selection.
* Clear the subfile.
* Intialise Call to OPTSWCH.
* Retrieve CHDRENQ.
* Set up screen header details.
* Load all associated coverages and riders to the subfile.
*
* 2000-SCREEN-EDIT.
*
* Bypass this section if returning from a previous selection.
* Display the screen.
* Validate the subfile.
* Redisplay the screen if any errors exist.
*
* 3000-UPDATE.
*
* No updating required.
*
* 4000-WHERE-NEXT.
*
* Find selected components, if any.
* Once a selection is found, store the COVRENQ for use in
* the next program and call OPTSWCH to load the program stack
* and call the next program.
* If no selection is found, return to the submenu.
*
*****************************************************************
* </pre>
*/
public class P5431 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5431");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaIndex2 = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";

	private FixedLengthStringData wsaaValidStatcode = new FixedLengthStringData(1).init("N");
	private Validator validStatcode = new Validator(wsaaValidStatcode, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaCoverExit = new FixedLengthStringData(1);
	private Validator wsaaCoverEof = new Validator(wsaaCoverExit, "Y");

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5679 = "T5679";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
		/* FORMATS */
	private String covrenqrec = "COVRENQREC";
	private String lifeenqrec = "LIFEENQREC";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5431ScreenVars sv = ScreenProgram.getScreenVars( S5431ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1190, 
		preExit, 
		updateErrorIndicators2120, 
		optswch4080
	}

	public P5431() {
		super();
		screenVars = sv;
		new ScreenModel("S5431", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			headerDetail1030();
			readLifeDetails1050();
			jointLifeDetails1060();
			contractTypeStatus1070();
			subfileLoad1080();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5431", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void headerDetail1030()
	{
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		sv.numpols.set(chdrenqIO.getPolinc());
	}

protected void readLifeDetails1050()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1060()
	{
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
	}

protected void contractTypeStatus1070()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void subfileLoad1080()
	{
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(wsspcomn.company);
		covrenqIO.setChdrnum(chdrenqIO.getChdrnum());
		covrenqIO.setLife(SPACES);
		covrenqIO.setCoverage(SPACES);
		covrenqIO.setRider(SPACES);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaCoverExit.set("N");
		while ( !(wsaaCoverEof.isTrue())) {
			listCoverages1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void listCoverages1100()
	{
		try {
			readFirstCoverage1110();
		}
		catch (GOTOException e){
		}
	}

protected void readFirstCoverage1110()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(covrenqIO.getStatuz(),varcom.endp)
		|| isNE(covrenqIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrenqIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			wsaaCoverExit.set("Y");
			goTo(GotoLabel.exit1190);
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isEQ(covrenqIO.getRider(),"00")) {
			coverageToScreen1200();
		}
		else {
			riderToScreen1300();
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5431", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void coverageToScreen1200()
	{
		/*COVRENQ-SUBFILE*/
		sv.cmpntnum.set(covrenqIO.getCoverage());
		wsaaCoverage.set(covrenqIO.getCrtable());
		sv.component.set(wsaaCovrComponent);
		getDescription1400();
		covrStatusDescs1500();
		validateComponent1600();
		sv.hlifeno.set(covrenqIO.getLife());
		sv.hsuffix.set(covrenqIO.getPlanSuffix());
		sv.hcoverage.set(covrenqIO.getCoverage());
		sv.hrider.set(covrenqIO.getRider());
		/*EXIT*/
	}

protected void riderToScreen1300()
	{
		/*RIDER-SUBFILE*/
		sv.cmpntnum.set(covrenqIO.getRider());
		wsaaRider.set(covrenqIO.getCrtable());
		sv.component.set(wsaaRidrComponent);
		getDescription1400();
		covrStatusDescs1500();
		validateComponent1600();
		sv.hlifeno.set(covrenqIO.getLife());
		sv.hsuffix.set(covrenqIO.getPlanSuffix());
		sv.hcoverage.set(covrenqIO.getCoverage());
		sv.hrider.set(covrenqIO.getRider());
		/*EXIT*/
	}

protected void getDescription1400()
	{
		para1410();
	}

protected void para1410()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getCrtable());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.deit.fill("?");
		}
		else {
			sv.deit.set(descIO.getLongdesc());
		}
	}

protected void covrStatusDescs1500()
	{
		para1510();
	}

protected void para1510()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getPstatcode());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatcode.fill("?");
		}
		else {
			sv.pstatcode.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrenqIO.getStatcode());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.statcode.fill("?");
		}
		else {
			sv.statcode.set(descIO.getShortdesc());
		}
	}

protected void validateComponent1600()
	{
		component1610();
	}

protected void component1610()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaBatckey.batcBatctrcde.toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		headerStatuzCheck1700();
		if (isNE(wsaaValidStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void headerStatuzCheck1700()
	{
		headerStatuzCheck1710();
	}

protected void headerStatuzCheck1710()
	{
		wsaaValidStatcode.set("N");
		wsaaValidPstatcode.set("N");
		if (isEQ(covrenqIO.getRider(),SPACES)
		|| isEQ(covrenqIO.getRider(),"00")) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| validStatcode.isTrue()); wsaaIndex.add(1)){
				if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrenqIO.getStatcode())) {
					wsaaValidStatcode.set("Y");
					for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
					|| validPstatcode.isTrue()); wsaaIndex.add(1)){
						if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrenqIO.getPstatcode())) {
							wsaaValidPstatcode.set("Y");
						}
					}
				}
			}
		}
		if (isNE(covrenqIO.getRider(),SPACES)
		&& isNE(covrenqIO.getRider(),"00")) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| validStatcode.isTrue()); wsaaIndex.add(1)){
				if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrenqIO.getStatcode())) {
					wsaaValidStatcode.set("Y");
					for (wsaaIndex2.set(1); !(isGT(wsaaIndex2,12)
					|| validPstatcode.isTrue()); wsaaIndex2.add(1)){
						if (isEQ(t5679rec.ridPremStat[wsaaIndex2.toInt()],covrenqIO.getPstatcode())) {
							wsaaValidPstatcode.set("Y");
						}
					}
				}
			}
		}
		if (validStatcode.isTrue()
		&& validPstatcode.isTrue()) {
			wsaaValidStatus = "Y";
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5431", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNextModifiedRecord2130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5431", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2130()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5431", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case optswch4080: {
					optswch4080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			goTo(GotoLabel.optswch4080);
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.select.set(SPACES);
			covrenqIO.setParams(SPACES);
			covrenqIO.setChdrcoy(wsspcomn.company);
			covrenqIO.setChdrnum(chdrenqIO.getChdrnum());
			covrenqIO.setLife(sv.hlifeno);
			covrenqIO.setPlanSuffix(sv.hsuffix);
			covrenqIO.setCoverage(sv.hcoverage);
			covrenqIO.setRider(sv.hrider);
			covrenqIO.setFunction(varcom.reads);
			covrenqIO.setFormat(covrenqrec);
			SmartFileCode.execute(appVars, covrenqIO);
			if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrenqIO.getParams());
				fatalError600();
			}
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
	}

protected void optswch4080()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("S5431", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,scrnparams.statuz)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}
}
