package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5443screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5443ScreenVars sv = (S5443ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5443screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5443ScreenVars screenVars = (S5443ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.reprmfrm02.setClassString("");
		screenVars.reprmto02.setClassString("");
		screenVars.gstrate02.setClassString("");
		screenVars.reprmfrm01.setClassString("");
		screenVars.reprmto01.setClassString("");
		screenVars.gstrate01.setClassString("");
		screenVars.reprmfrm03.setClassString("");
		screenVars.reprmto03.setClassString("");
		screenVars.gstrate03.setClassString("");
		screenVars.reprmfrm04.setClassString("");
		screenVars.reprmto04.setClassString("");
		screenVars.gstrate04.setClassString("");
		screenVars.reprmfrm05.setClassString("");
		screenVars.reprmto05.setClassString("");
		screenVars.gstrate05.setClassString("");
	}

/**
 * Clear all the variables in S5443screen
 */
	public static void clear(VarModel pv) {
		S5443ScreenVars screenVars = (S5443ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.reprmfrm02.clear();
		screenVars.reprmto02.clear();
		screenVars.gstrate02.clear();
		screenVars.reprmfrm01.clear();
		screenVars.reprmto01.clear();
		screenVars.gstrate01.clear();
		screenVars.reprmfrm03.clear();
		screenVars.reprmto03.clear();
		screenVars.gstrate03.clear();
		screenVars.reprmfrm04.clear();
		screenVars.reprmto04.clear();
		screenVars.gstrate04.clear();
		screenVars.reprmfrm05.clear();
		screenVars.reprmto05.clear();
		screenVars.gstrate05.clear();
	}
}
