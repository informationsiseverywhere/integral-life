package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:24
 * Description:
 * Copybook name: RACTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ractkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ractFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ractKey = new FixedLengthStringData(64).isAPartOf(ractFileKey, 0, REDEFINE);
  	public FixedLengthStringData ractChdrcoy = new FixedLengthStringData(1).isAPartOf(ractKey, 0);
  	public FixedLengthStringData ractChdrnum = new FixedLengthStringData(8).isAPartOf(ractKey, 1);
  	public FixedLengthStringData ractLife = new FixedLengthStringData(2).isAPartOf(ractKey, 9);
  	public FixedLengthStringData ractCoverage = new FixedLengthStringData(2).isAPartOf(ractKey, 11);
  	public FixedLengthStringData ractRider = new FixedLengthStringData(2).isAPartOf(ractKey, 13);
  	public FixedLengthStringData ractRasnum = new FixedLengthStringData(8).isAPartOf(ractKey, 15);
  	public FixedLengthStringData ractRatype = new FixedLengthStringData(4).isAPartOf(ractKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(ractKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ractFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ractFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}