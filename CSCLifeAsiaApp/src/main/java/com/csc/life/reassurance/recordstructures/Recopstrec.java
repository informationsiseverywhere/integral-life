package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:42
 * Description:
 * Copybook name: RECOPSTREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Recopstrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData recopstRec = new FixedLengthStringData(167);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(recopstRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(recopstRec, 5);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(recopstRec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 0, FILLER);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(recopstRec, 28);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(recopstRec, 31);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(recopstRec, 35);
  	public FixedLengthStringData chdrpfx = new FixedLengthStringData(2).isAPartOf(recopstRec, 36);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(recopstRec, 38);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(recopstRec, 39);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(recopstRec, 47);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(recopstRec, 49);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(recopstRec, 51);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(recopstRec, 53);
  	public FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(recopstRec, 56);
  	public PackedDecimalData seqno = new PackedDecimalData(2, 0).isAPartOf(recopstRec, 64);
  	public PackedDecimalData costdate = new PackedDecimalData(8, 0).isAPartOf(recopstRec, 66);
  	public FixedLengthStringData validflag = new FixedLengthStringData(1).isAPartOf(recopstRec, 71);
  	public FixedLengthStringData retype = new FixedLengthStringData(1).isAPartOf(recopstRec, 72);
  	public FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(recopstRec, 73);
  	public PackedDecimalData sraramt = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 77);
  	public PackedDecimalData raAmount = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 86);
  	public PackedDecimalData ctdate = new PackedDecimalData(8, 0).isAPartOf(recopstRec, 95);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(recopstRec, 100);
  	public PackedDecimalData prem = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 103);
  	public PackedDecimalData compay = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 112);
  	public PackedDecimalData taxamt = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 121);
  	public PackedDecimalData refundfe = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 130);
  	public PackedDecimalData polsum = new PackedDecimalData(6, 2).isAPartOf(recopstRec, 139);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(recopstRec, 143);
  	public PackedDecimalData recovAmt = new PackedDecimalData(17, 2).isAPartOf(recopstRec, 148);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(recopstRec, 157);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(recopstRec, 161);
  	public FixedLengthStringData rcstfrq = new FixedLengthStringData(2).isAPartOf(recopstRec, 164);
	public PackedDecimalData vpmsflag = new PackedDecimalData(1,0).isAPartOf(recopstRec, 166);


	public void initialize() {
		COBOLFunctions.initialize(recopstRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		recopstRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}