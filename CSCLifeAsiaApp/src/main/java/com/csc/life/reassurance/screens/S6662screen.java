package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6662screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6662ScreenVars sv = (S6662ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6662screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6662ScreenVars screenVars = (S6662ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.reascode01.setClassString("");
		screenVars.shortdesc01.setClassString("");
		screenVars.reascode02.setClassString("");
		screenVars.shortdesc02.setClassString("");
		screenVars.reascode03.setClassString("");
		screenVars.shortdesc03.setClassString("");
		screenVars.reascode04.setClassString("");
		screenVars.shortdesc04.setClassString("");
		screenVars.reascode05.setClassString("");
		screenVars.shortdesc05.setClassString("");
		screenVars.reascode06.setClassString("");
		screenVars.shortdesc06.setClassString("");
		screenVars.reascode07.setClassString("");
		screenVars.shortdesc07.setClassString("");
		screenVars.reascode08.setClassString("");
		screenVars.shortdesc08.setClassString("");
		screenVars.reascode09.setClassString("");
		screenVars.shortdesc09.setClassString("");
		screenVars.reascode10.setClassString("");
		screenVars.shortdesc10.setClassString("");
		screenVars.reascode11.setClassString("");
		screenVars.shortdesc11.setClassString("");
		screenVars.reascode12.setClassString("");
		screenVars.shortdesc12.setClassString("");
		screenVars.reascode13.setClassString("");
		screenVars.shortdesc13.setClassString("");
		screenVars.reascode14.setClassString("");
		screenVars.shortdesc14.setClassString("");
		screenVars.reascode15.setClassString("");
		screenVars.shortdesc15.setClassString("");
		screenVars.reascode16.setClassString("");
		screenVars.shortdesc16.setClassString("");
		screenVars.reascode17.setClassString("");
		screenVars.shortdesc17.setClassString("");
		screenVars.reascode18.setClassString("");
		screenVars.shortdesc18.setClassString("");
		screenVars.reascode19.setClassString("");
		screenVars.shortdesc19.setClassString("");
		screenVars.reascode20.setClassString("");
		screenVars.shortdesc20.setClassString("");
		screenVars.reascode21.setClassString("");
		screenVars.shortdesc21.setClassString("");
		screenVars.reascode22.setClassString("");
		screenVars.shortdesc22.setClassString("");
		screenVars.reascode23.setClassString("");
		screenVars.shortdesc23.setClassString("");
		screenVars.reascode24.setClassString("");
		screenVars.shortdesc24.setClassString("");
		screenVars.reascode25.setClassString("");
		screenVars.shortdesc25.setClassString("");
		screenVars.reascode26.setClassString("");
		screenVars.shortdesc26.setClassString("");
		screenVars.reascode27.setClassString("");
		screenVars.shortdesc27.setClassString("");
		screenVars.reascode28.setClassString("");
		screenVars.shortdesc28.setClassString("");
		screenVars.reascode29.setClassString("");
		screenVars.shortdesc29.setClassString("");
		screenVars.reascode30.setClassString("");
		screenVars.shortdesc30.setClassString("");
	}

/**
 * Clear all the variables in S6662screen
 */
	public static void clear(VarModel pv) {
		S6662ScreenVars screenVars = (S6662ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.reascode01.clear();
		screenVars.shortdesc01.clear();
		screenVars.reascode02.clear();
		screenVars.shortdesc02.clear();
		screenVars.reascode03.clear();
		screenVars.shortdesc03.clear();
		screenVars.reascode04.clear();
		screenVars.shortdesc04.clear();
		screenVars.reascode05.clear();
		screenVars.shortdesc05.clear();
		screenVars.reascode06.clear();
		screenVars.shortdesc06.clear();
		screenVars.reascode07.clear();
		screenVars.shortdesc07.clear();
		screenVars.reascode08.clear();
		screenVars.shortdesc08.clear();
		screenVars.reascode09.clear();
		screenVars.shortdesc09.clear();
		screenVars.reascode10.clear();
		screenVars.shortdesc10.clear();
		screenVars.reascode11.clear();
		screenVars.shortdesc11.clear();
		screenVars.reascode12.clear();
		screenVars.shortdesc12.clear();
		screenVars.reascode13.clear();
		screenVars.shortdesc13.clear();
		screenVars.reascode14.clear();
		screenVars.shortdesc14.clear();
		screenVars.reascode15.clear();
		screenVars.shortdesc15.clear();
		screenVars.reascode16.clear();
		screenVars.shortdesc16.clear();
		screenVars.reascode17.clear();
		screenVars.shortdesc17.clear();
		screenVars.reascode18.clear();
		screenVars.shortdesc18.clear();
		screenVars.reascode19.clear();
		screenVars.shortdesc19.clear();
		screenVars.reascode20.clear();
		screenVars.shortdesc20.clear();
		screenVars.reascode21.clear();
		screenVars.shortdesc21.clear();
		screenVars.reascode22.clear();
		screenVars.shortdesc22.clear();
		screenVars.reascode23.clear();
		screenVars.shortdesc23.clear();
		screenVars.reascode24.clear();
		screenVars.shortdesc24.clear();
		screenVars.reascode25.clear();
		screenVars.shortdesc25.clear();
		screenVars.reascode26.clear();
		screenVars.shortdesc26.clear();
		screenVars.reascode27.clear();
		screenVars.shortdesc27.clear();
		screenVars.reascode28.clear();
		screenVars.shortdesc28.clear();
		screenVars.reascode29.clear();
		screenVars.shortdesc29.clear();
		screenVars.reascode30.clear();
		screenVars.shortdesc30.clear();
	}
}
