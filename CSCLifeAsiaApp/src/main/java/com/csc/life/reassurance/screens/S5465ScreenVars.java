package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5465
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5465ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1058);
	public FixedLengthStringData dataFields = new FixedLengthStringData(530).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData callamt = DD.callamt.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData calldt = DD.calldt.copyToZonedDecimal().isAPartOf(dataFields,25);
	public ZonedDecimalData callrecd = DD.callrecd.copyToZonedDecimal().isAPartOf(dataFields,33);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(dataFields,58);
	public ZonedDecimalData cmdate = DD.cmdate.copyToZonedDecimal().isAPartOf(dataFields,108);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,116);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,119);
	public ZonedDecimalData ctdate = DD.ctdate.copyToZonedDecimal().isAPartOf(dataFields,123);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData currdesc = DD.currdesc.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,197);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,262);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,270);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,317);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,347);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,355);
	public ZonedDecimalData raAmount = DD.raamount.copyToZonedDecimal().isAPartOf(dataFields,363);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,380);
	public ZonedDecimalData reasper = DD.reasper.copyToZonedDecimal().isAPartOf(dataFields,388);
	public ZonedDecimalData recovamt = DD.recovamt.copyToZonedDecimal().isAPartOf(dataFields,393);
	public FixedLengthStringData retype = DD.retype.copy().isAPartOf(dataFields,410);
	public FixedLengthStringData retypedesc = DD.retypedesc.copy().isAPartOf(dataFields,411);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(dataFields,441);
	public FixedLengthStringData rngmntdsc = DD.rngmtdsc.copy().isAPartOf(dataFields,445);
	public ZonedDecimalData rrevdt = DD.rrevdt.copyToZonedDecimal().isAPartOf(dataFields,475);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,483);
	public FixedLengthStringData tabledesc = DD.tabledesc.copy().isAPartOf(dataFields,500);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 530);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData callamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData calldtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData callrecdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cmdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData currdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData raamountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData reasperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData recovamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData retypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData retypedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rngmntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rngmtdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData rrevdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData tabledescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 662);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] callamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] calldtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] callrecdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cmdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] currdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] raamountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] reasperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] recovamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] retypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] retypedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rngmntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] rngmtdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] rrevdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] tabledescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData calldtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cmdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ctdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rrevdtDisp = new FixedLengthStringData(10);

	public LongData S5465screenWritten = new LongData(0);
	public LongData S5465protectWritten = new LongData(0);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return false;
	}


	public S5465ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(callamtOut,new String[] {"20","30","-20",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, crtable, lifcnum, linsname, jlifcnum, jlinsname, occdate, ptdate, longdesc, tabledesc, btdate, currcode, currds, retypedesc, rngmntdsc, callamt, rasnum, rngmnt, retype, cmdate, sumins, raAmount, currcd, recovamt, rrevdt, ctdate, reasper, callrecd, clntname, currdesc, calldt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, crtableOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, occdateOut, ptdateOut, longdescOut, tabledescOut, btdateOut, currcodeOut, currdsOut, retypedescOut, rngmtdscOut, callamtOut, rasnumOut, rngmntOut, retypeOut, cmdateOut, suminsOut, raamountOut, currcdOut, recovamtOut, rrevdtOut, ctdateOut, reasperOut, callrecdOut, clntnameOut, currdescOut, calldtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, crtableErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, occdateErr, ptdateErr, longdescErr, tabledescErr, btdateErr, currcodeErr, currdsErr, retypedescErr, rngmtdscErr, callamtErr, rasnumErr, rngmntErr, retypeErr, cmdateErr, suminsErr, raamountErr, currcdErr, recovamtErr, rrevdtErr, ctdateErr, reasperErr, callrecdErr, clntnameErr, currdescErr, calldtErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, cmdate, rrevdt, ctdate, calldt};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, cmdateErr, rrevdtErr, ctdateErr, calldtErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, cmdateDisp, rrevdtDisp, ctdateDisp, calldtDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5465screen.class;
		protectRecord = S5465protect.class;
	}

}
