package com.csc.life.reassurance.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5436screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5436screensfl";
		lrec.subfileClass = S5436screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 11;
		lrec.pageSubfile = 10;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5436ScreenVars sv = (S5436ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5436screenctlWritten, sv.S5436screensflWritten, av, sv.s5436screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5436ScreenVars screenVars = (S5436ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.rasnum.setClassString("");
		screenVars.reasname.setClassString("");
		screenVars.cltsex.setClassString("");
		screenVars.cltdobDisp.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.cltname.setClassString("");
	}

/**
 * Clear all the variables in S5436screenctl
 */
	public static void clear(VarModel pv) {
		S5436ScreenVars screenVars = (S5436ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.rasnum.clear();
		screenVars.reasname.clear();
		screenVars.cltsex.clear();
		screenVars.cltdobDisp.clear();
		screenVars.cltdob.clear();
		screenVars.clntnum.clear();
		screenVars.cltname.clear();
	}
}
