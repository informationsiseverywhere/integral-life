package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:29
 * Description:
 * Copybook name: COVTCSNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtcsnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtcsnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtcsnKey = new FixedLengthStringData(64).isAPartOf(covtcsnFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtcsnChdrcoy = new FixedLengthStringData(1).isAPartOf(covtcsnKey, 0);
  	public FixedLengthStringData covtcsnChdrnum = new FixedLengthStringData(8).isAPartOf(covtcsnKey, 1);
  	public FixedLengthStringData covtcsnLife = new FixedLengthStringData(2).isAPartOf(covtcsnKey, 9);
  	public FixedLengthStringData covtcsnCoverage = new FixedLengthStringData(2).isAPartOf(covtcsnKey, 11);
  	public FixedLengthStringData covtcsnRider = new FixedLengthStringData(2).isAPartOf(covtcsnKey, 13);
  	public PackedDecimalData covtcsnSeqnbr = new PackedDecimalData(3, 0).isAPartOf(covtcsnKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covtcsnKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtcsnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtcsnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}