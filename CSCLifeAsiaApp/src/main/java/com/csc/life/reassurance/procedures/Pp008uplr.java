/*
 * File: Pp008uplr.java
 * Date: 30 August 2009 1:16:06
 * Author: Quipoz Limited
 *
 * Class transformed from PP008UPLR.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.accounting.recordstructures.Jrnledrec;
import com.csc.fsu.general.procedures.Addacmv;
import com.csc.fsu.general.recordstructures.Addacmvrec;
import com.csc.fsu.general.recordstructures.Genlkey;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Rldgkey;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*         GL Journal Create - Update SACSCODE LR
*
* This routine is called from PP008 - GL Journal Create
* to create the dissection for which the journal applies
*
* The program will call GLKEY to get the Life Policy Account
* from Table T3698 and post the dissection amount to it.
*
* "ADDRTRN" subroutine is called to create the "RTRN"
* records and update the batch control totals.
*
*********************************************************
* </pre>
*/
public class Pp008uplr extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("PP008UPLR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	private Addacmvrec addacmvrec = new Addacmvrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Jrnledrec jrnledrec = new Jrnledrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private Genlkey wsaaGenlkey = new Genlkey();
	private Rdockey wsaaRdockey = new Rdockey();
	private Rldgkey wsaaRldgkey = new Rldgkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit579,
		exit589,
		exit1090
	}

	public Pp008uplr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		jrnledrec.jrnledRec = convertAndSetParam(jrnledrec.jrnledRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		para011();
		exit090();
	}

protected void para011()
	{
		jrnledrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaProg);
		updateDissection1000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void syserr570()
	{
		try {
			para571();
		}
		catch (GOTOException e){
		}
		finally{
			exit579();
		}
	}

protected void para571()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit579);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		jrnledrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError580()
	{
		try {
			para581();
		}
		catch (GOTOException e){
		}
		finally{
			exit589();
		}
	}

protected void para581()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit589);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		jrnledrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void updateDissection1000()
	{
		try {
			para1000();
		}
		catch (GOTOException e){
		}
	}

protected void para1000()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isEQ(jrnledrec.origamt,0)
		&& isEQ(jrnledrec.acctamt,0)) {
			goTo(GotoLabel.exit1090);
		}
		addacmvrec.addacmvRec.set(SPACES);
		addacmvrec.contot.set(0);
		varcom.vrcmTranid.set(jrnledrec.tranid);
		addacmvrec.termid.set(varcom.vrcmTermid);
		addacmvrec.user.set(varcom.vrcmUser);
		addacmvrec.transactionDate.set(varcom.vrcmDate);
		addacmvrec.transactionTime.set(varcom.vrcmTime);
		wsaaBatckey.batcKey.set(jrnledrec.batchkey);
		addacmvrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		addacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		addacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		addacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		addacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		addacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		addacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		wsaaRdockey.rdocKey.set(jrnledrec.doctkey);
		addacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
		addacmvrec.transeq.set(wsaaRdockey.rdocTranseq);
		wsaaRldgkey.rldgKey.set(jrnledrec.trankey);
		addacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
		addacmvrec.rldgacct.set(wsaaRldgkey.rldgRldgacct);
		wsaaGenlkey.genlKey.set(jrnledrec.genlkey);
		addacmvrec.genlcoy.set(wsaaGenlkey.genlGenlcoy);
		addacmvrec.genlcur.set(wsaaGenlkey.genlGenlcur);
		addacmvrec.glcode.set(wsaaGenlkey.genlGenlcde);
		addacmvrec.sacscode.set(jrnledrec.sacscode);
		addacmvrec.sacstyp.set(jrnledrec.sacstyp);
		addacmvrec.glsign.set(jrnledrec.sign);
		addacmvrec.transeq.set(jrnledrec.transeq);
		addacmvrec.effdate.set(jrnledrec.trandate);
		addacmvrec.trandesc.set(jrnledrec.trandesc);
		addacmvrec.origamt.set(jrnledrec.origamt);
		addacmvrec.acctamt.set(jrnledrec.acctamt);
		addacmvrec.origccy.set(jrnledrec.origccy);
		addacmvrec.acctccy.set(jrnledrec.acctccy);
		addacmvrec.crate.set(jrnledrec.dissrate);
		if (isNE(jrnledrec.tranno,NUMERIC)) {
			jrnledrec.tranno.set(0);
		}
		addacmvrec.rdocpfx.set("GE");
		addacmvrec.creddte.set(varcom.vrcmMaxDate);
		addacmvrec.tranno.set(jrnledrec.tranno);
		addacmvrec.function.set(SPACES);
		callProgram(Addacmv.class, addacmvrec.addacmvRec);
		if (isNE(addacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(addacmvrec.statuz);
			jrnledrec.statuz.set(addacmvrec.statuz);
			syserr570();
		}
	}
}
