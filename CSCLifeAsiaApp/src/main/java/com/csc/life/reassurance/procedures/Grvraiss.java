/*
 * File: Grvraiss.java
 * Date: 29 August 2009 22:51:16
 * Author: Quipoz Limited
 *
 * Class transformed from GRVRAISS.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.reassurance.dataaccess.LirrTableDAM;
import com.csc.life.reassurance.dataaccess.LirrconTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhconTableDAM;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdseqTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Generic Reassurance Issue Reversal Subroutine.
*
* This is a new subroutine which forms part of Reassurance
* Reversals. It is called upon via T5671 for contract issue
* reversal for reassurance components.
*
* It will be called using the standard generic reversal linkage
* copybook, GREVERSREC.
*
* Because this subroutine is used for both AFI and CFI, it will
* not rewrite the RACD records as validflag '3', since this would
* leave unwanted RACD records on the file after a CFI. Since RACD
* records are created during pre-issue validation, they can be
* deleted during an AFI because they will always be re-created
* before contract issue.
*
* This subroutine will delete all RACD, LRRH and LIRR records
* created at contract issue.
*
* This subroutine is performed at component level.
*
*****************************************************************
* </pre>
*/
public class Grvraiss extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GRVRAISS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String racdrec = "RACDREC";
	private String lirrconrec = "LIRRCONREC";
	private String lirrrec = "LIRRREVREC";
	private String lrrhconrec = "LRRHCONREC";
	private String lrrhrec = "LRRHREVREC";
	private Greversrec greversrec = new Greversrec();
		/*Reassurer Experience History*/
	private LirrTableDAM lirrIO = new LirrTableDAM();
		/*Reassurer Exp. History Contract Level*/
	private LirrconTableDAM lirrconIO = new LirrconTableDAM();
		/*Life Retention & Reassurance History*/
	private LrrhTableDAM lrrhIO = new LrrhTableDAM();
		/*Life Retention and Reassurance History*/
	private LrrhconTableDAM lrrhconIO = new LrrhconTableDAM();
		/*Reassurance Cession Details Logical*/
	private RacdTableDAM racdIO = new RacdTableDAM();
		/*Reassurance Cession Details By Seq No.*/
	private RacdseqTableDAM racdseqIO = new RacdseqTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr1080,
		exit1090,
		nextr2080,
		exit2090,
		nextr3080,
		exit3090
	}

	public Grvraiss() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		greversrec.statuz.set(varcom.oK);
		racdIO.setParams(SPACES);
		racdIO.setChdrcoy(greversrec.chdrcoy);
		racdIO.setChdrnum(greversrec.chdrnum);
		racdIO.setLife(greversrec.life);
		racdIO.setCoverage(greversrec.coverage);
		racdIO.setRider(greversrec.rider);
		racdIO.setPlanSuffix(greversrec.planSuffix);
		racdIO.setSeqno(ZERO);
		racdIO.setFormat(racdrec);
		racdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(racdIO.getStatuz(),varcom.endp))) {
			processRacd1000();
		}

		lrrhconIO.setParams(SPACES);
		lrrhconIO.setCompany(greversrec.chdrcoy);
		lrrhconIO.setChdrnum(greversrec.chdrnum);
		lrrhconIO.setLife(greversrec.life);
		lrrhconIO.setCoverage(greversrec.coverage);
		lrrhconIO.setRider(greversrec.rider);
		lrrhconIO.setPlanSuffix(greversrec.planSuffix);
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhconIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhconIO.setFitKeysSearch("COMPANY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(lrrhconIO.getStatuz(),varcom.endp))) {
			processLrrh2000();
		}

		lirrconIO.setParams(SPACES);
		lirrconIO.setCompany(greversrec.chdrcoy);
		lirrconIO.setChdrnum(greversrec.chdrnum);
		lirrconIO.setLife(greversrec.life);
		lirrconIO.setCoverage(greversrec.coverage);
		lirrconIO.setRider(greversrec.rider);
		lirrconIO.setPlanSuffix(greversrec.planSuffix);
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lirrconIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lirrconIO.setFitKeysSearch("COMPANY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(lirrconIO.getStatuz(),varcom.endp))) {
			processLirr3000();
		}

	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void processRacd1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					racd1010();
				}
				case nextr1080: {
					nextr1080();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void racd1010()
	{
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(),varcom.oK)
		&& isNE(racdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdIO.getParams());
			dbError580();
		}
		if (isNE(racdIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(racdIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(racdIO.getLife(),greversrec.life)
		|| isNE(racdIO.getCoverage(),greversrec.coverage)
		|| isNE(racdIO.getRider(),greversrec.rider)
		|| isNE(racdIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(racdIO.getStatuz(),varcom.endp)) {
			racdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(racdIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr1080);
		}
		deleteRacd1100();
	}

protected void nextr1080()
	{
		racdIO.setFunction(varcom.nextr);
	}

protected void deleteRacd1100()
	{
		racd1110();
	}

protected void racd1110()
	{
		racdseqIO.setParams(SPACES);
		racdseqIO.setRrn(racdIO.getRrn());
		racdseqIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdseqIO);
		if (isNE(racdseqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdseqIO.getParams());
			dbError580();
		}
		racdseqIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdseqIO);
		if (isNE(racdseqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdseqIO.getParams());
			dbError580();
		}
	}

protected void processLrrh2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					lrrh2010();
				}
				case nextr2080: {
					nextr2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lrrh2010()
	{
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(),varcom.oK)
		&& isNE(lrrhconIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		if (isNE(lrrhconIO.getCompany(),greversrec.chdrcoy)
		|| isNE(lrrhconIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(lrrhconIO.getLife(),greversrec.life)
		|| isNE(lrrhconIO.getCoverage(),greversrec.coverage)
		|| isNE(lrrhconIO.getRider(),greversrec.rider)
		|| isNE(lrrhconIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(lrrhconIO.getStatuz(),varcom.endp)) {
			lrrhconIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(lrrhconIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr2080);
		}
		deleteLrrh2100();
	}

protected void nextr2080()
	{
		lrrhconIO.setFunction(varcom.nextr);
	}

protected void deleteLrrh2100()
	{
		lrrh2110();
	}

protected void lrrh2110()
	{
		lrrhIO.setParams(SPACES);
		lrrhIO.setRrn(lrrhconIO.getRrn());
		lrrhIO.setFormat(lrrhrec);
		lrrhIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, lrrhIO);
		if (isNE(lrrhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lrrhIO.getParams());
			dbError580();
		}
		lrrhIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lrrhIO);
		if (isNE(lrrhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lrrhIO.getParams());
			dbError580();
		}
	}

protected void processLirr3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					lirr3010();
				}
				case nextr3080: {
					nextr3080();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lirr3010()
	{
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(),varcom.oK)
		&& isNE(lirrconIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
		if (isNE(lirrconIO.getCompany(),greversrec.chdrcoy)
		|| isNE(lirrconIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(lirrconIO.getLife(),greversrec.life)
		|| isNE(lirrconIO.getCoverage(),greversrec.coverage)
		|| isNE(lirrconIO.getRider(),greversrec.rider)
		|| isNE(lirrconIO.getPlanSuffix(),greversrec.planSuffix)
		|| isEQ(lirrconIO.getStatuz(),varcom.endp)) {
			lirrconIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
		if (isNE(lirrconIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.nextr3080);
		}
		deleteLirr3100();
	}

protected void nextr3080()
	{
		lirrconIO.setFunction(varcom.nextr);
	}

protected void deleteLirr3100()
	{
		lirr3110();
	}

protected void lirr3110()
	{
		lirrIO.setParams(SPACES);
		lirrIO.setRrn(lirrconIO.getRrn());
		lirrIO.setFormat(lirrrec);
		lirrIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, lirrIO);
		if (isNE(lirrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lirrIO.getParams());
			dbError580();
		}
		lirrIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lirrIO);
		if (isNE(lirrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lirrIO.getParams());
			dbError580();
		}
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
