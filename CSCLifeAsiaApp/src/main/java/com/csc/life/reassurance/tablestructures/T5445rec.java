package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:13
 * Description:
 * Copybook name: T5445REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5445rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5445Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(t5445Rec, 0);
  	public FixedLengthStringData frequency = new FixedLengthStringData(2).isAPartOf(t5445Rec, 3);
  	public ZonedDecimalData refundfe = new ZonedDecimalData(17, 2).isAPartOf(t5445Rec, 5);
  	public FixedLengthStringData rndgrqd = new FixedLengthStringData(1).isAPartOf(t5445Rec, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(477).isAPartOf(t5445Rec, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5445Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5445Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}