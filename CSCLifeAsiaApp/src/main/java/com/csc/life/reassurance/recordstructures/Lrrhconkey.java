package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:27
 * Description:
 * Copybook name: LRRHCONKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lrrhconkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lrrhconFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lrrhconKey = new FixedLengthStringData(64).isAPartOf(lrrhconFileKey, 0, REDEFINE);
  	public FixedLengthStringData lrrhconCompany = new FixedLengthStringData(1).isAPartOf(lrrhconKey, 0);
  	public FixedLengthStringData lrrhconChdrnum = new FixedLengthStringData(8).isAPartOf(lrrhconKey, 1);
  	public FixedLengthStringData lrrhconLife = new FixedLengthStringData(2).isAPartOf(lrrhconKey, 9);
  	public FixedLengthStringData lrrhconCoverage = new FixedLengthStringData(2).isAPartOf(lrrhconKey, 11);
  	public FixedLengthStringData lrrhconRider = new FixedLengthStringData(2).isAPartOf(lrrhconKey, 13);
  	public PackedDecimalData lrrhconPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lrrhconKey, 15);
  	public FixedLengthStringData lrrhconClntpfx = new FixedLengthStringData(2).isAPartOf(lrrhconKey, 18);
  	public FixedLengthStringData lrrhconClntcoy = new FixedLengthStringData(1).isAPartOf(lrrhconKey, 20);
  	public FixedLengthStringData lrrhconClntnum = new FixedLengthStringData(8).isAPartOf(lrrhconKey, 21);
  	public FixedLengthStringData lrrhconLrkcls = new FixedLengthStringData(4).isAPartOf(lrrhconKey, 29);
  	public FixedLengthStringData filler = new FixedLengthStringData(31).isAPartOf(lrrhconKey, 33, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lrrhconFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lrrhconFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}