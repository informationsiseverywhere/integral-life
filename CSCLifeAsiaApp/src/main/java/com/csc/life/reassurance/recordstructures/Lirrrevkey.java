package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:14
 * Description:
 * Copybook name: LIRRREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lirrrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lirrrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lirrrevKey = new FixedLengthStringData(64).isAPartOf(lirrrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData lirrrevCompany = new FixedLengthStringData(1).isAPartOf(lirrrevKey, 0);
  	public FixedLengthStringData lirrrevChdrnum = new FixedLengthStringData(8).isAPartOf(lirrrevKey, 1);
  	public FixedLengthStringData lirrrevLife = new FixedLengthStringData(2).isAPartOf(lirrrevKey, 9);
  	public FixedLengthStringData lirrrevCoverage = new FixedLengthStringData(2).isAPartOf(lirrrevKey, 11);
  	public FixedLengthStringData lirrrevRider = new FixedLengthStringData(2).isAPartOf(lirrrevKey, 13);
  	public PackedDecimalData lirrrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(lirrrevKey, 15);
  	public FixedLengthStringData lirrrevRasnum = new FixedLengthStringData(8).isAPartOf(lirrrevKey, 18);
  	public FixedLengthStringData lirrrevRngmnt = new FixedLengthStringData(4).isAPartOf(lirrrevKey, 26);
  	public FixedLengthStringData lirrrevClntpfx = new FixedLengthStringData(2).isAPartOf(lirrrevKey, 30);
  	public FixedLengthStringData lirrrevClntcoy = new FixedLengthStringData(1).isAPartOf(lirrrevKey, 32);
  	public FixedLengthStringData lirrrevClntnum = new FixedLengthStringData(8).isAPartOf(lirrrevKey, 33);
  	public PackedDecimalData lirrrevTranno = new PackedDecimalData(5, 0).isAPartOf(lirrrevKey, 41);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(lirrrevKey, 44, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lirrrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lirrrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}