package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:08
 * Description:
 * Copybook name: LIFERASKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Liferaskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData liferasFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData liferasKey = new FixedLengthStringData(256).isAPartOf(liferasFileKey, 0, REDEFINE);
  	public FixedLengthStringData liferasChdrcoy = new FixedLengthStringData(1).isAPartOf(liferasKey, 0);
  	public FixedLengthStringData liferasChdrnum = new FixedLengthStringData(8).isAPartOf(liferasKey, 1);
  	public FixedLengthStringData liferasLife = new FixedLengthStringData(2).isAPartOf(liferasKey, 9);
  	public FixedLengthStringData liferasJlife = new FixedLengthStringData(2).isAPartOf(liferasKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(liferasKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(liferasFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		liferasFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}