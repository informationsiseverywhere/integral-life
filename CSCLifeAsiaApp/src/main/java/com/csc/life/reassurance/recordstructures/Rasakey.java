package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:25
 * Description:
 * Copybook name: RASAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rasakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData rasaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData rasaKey = new FixedLengthStringData(64).isAPartOf(rasaFileKey, 0, REDEFINE);
  	public FixedLengthStringData rasaRascoy = new FixedLengthStringData(1).isAPartOf(rasaKey, 0);
  	public FixedLengthStringData rasaRasnum = new FixedLengthStringData(8).isAPartOf(rasaKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(rasaKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(rasaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rasaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}