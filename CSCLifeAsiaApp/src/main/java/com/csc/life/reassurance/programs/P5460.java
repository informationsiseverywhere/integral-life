/*
 * File: P5460.java

 * Date: 30 August 2009 0:27:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P5460.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.RacdenqTableDAM;
import com.csc.life.reassurance.screens.S5460ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5460 - Reassurance Cession Enquiry.
*
* This is a simple subfile enquiry program used to display the
* details of all cessions (RACD records) for a component. Both
* treaty and facultative cessions are displayed. Only activated
* (i.e. validflag '1') records are displayed. This screen runs
* off the plan component enquiry screen in contract enquiry.
*
* The total reassured is accumulated for treaty and facultative
* and dispalyed at the top of the screen.
*
*****************************************************************
* </pre>
*/
public class P5460 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5460");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String racdenqrec = "RACDENQREC";
		/*Contract Enquiry - Coverage Details.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Reassurance Cession Details Enquiry*/
	private RacdenqTableDAM racdenqIO = new RacdenqTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5460ScreenVars sv = ScreenProgram.getScreenVars( S5460ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1190, 
		preExit, 
		exit2090
	}

	public P5460() {
		super();
		screenVars = sv;
		new ScreenModel("S5460", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		subfileLoad1080();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5460", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		covrpf = covrDao.getCacheObject(covrpf);
		sv.ssreast.set(ZERO);
		sv.ssreasf.set(ZERO);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.chdrnum.set(covrpf.getChdrnum());
		sv.sumins.set(covrpf.getSumins());
	}

protected void subfileLoad1080()
	{
		racdenqIO.setParams(SPACES);
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		racdenqIO.setChdrcoy(covrpf.getChdrcoy());
		racdenqIO.setChdrnum(covrpf.getChdrnum());
		racdenqIO.setLife(covrpf.getLife());
		racdenqIO.setCoverage(covrpf.getCoverage());
		racdenqIO.setRider(covrpf.getRider());
		racdenqIO.setSeqno(ZERO);
		racdenqIO.setFormat(racdenqrec);
		racdenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(racdenqIO.getStatuz(),varcom.endp))) {
			listRacd1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void listRacd1100()
	{
		try {
			readFirstRacd1110();
		}
		catch (GOTOException e){
		}
	}

protected void readFirstRacd1110()
	{
		SmartFileCode.execute(appVars, racdenqIO);
		if (isNE(racdenqIO.getStatuz(),varcom.oK)
		&& isNE(racdenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(racdenqIO.getParams());
			fatalError600();
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		if (isNE(racdenqIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(racdenqIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(racdenqIO.getLife(),covrpf.getLife())
		|| isNE(racdenqIO.getCoverage(),covrpf.getCoverage())
		|| isNE(racdenqIO.getRider(),covrpf.getRider())
		|| isEQ(racdenqIO.getStatuz(),varcom.endp)) {
			racdenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		sv.cmdate.set(racdenqIO.getCmdate());
		sv.rasnum.set(racdenqIO.getRasnum());
		sv.rngmnt.set(racdenqIO.getRngmnt());
		sv.retype.set(racdenqIO.getRetype());
		sv.raAmount.set(racdenqIO.getRaAmount());
		sv.ovrdind.set(racdenqIO.getOvrdind());
		sv.ctdate.set(racdenqIO.getCtdate());
		if (isEQ(racdenqIO.getRetype(),"T")) {
			sv.ssreast.add(racdenqIO.getRaAmount());
		}
		else {
			sv.ssreasf.add(racdenqIO.getRaAmount());
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5460", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		racdenqIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
