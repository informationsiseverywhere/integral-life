package com.csc.life.reassurance.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:43
 * Description:
 * Copybook name: TH621REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th621rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th621Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData rprmcls = new FixedLengthStringData(4).isAPartOf(th621Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(th621Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th621Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th621Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}