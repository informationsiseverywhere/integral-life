package com.csc.life.reassurance.dataaccess.dao;

import java.util.List;

import com.csc.life.reassurance.dataaccess.model.Lirrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LirrpfDAO extends BaseDAO<Lirrpf> {

	public List<Lirrpf> searchlLirrconRecord(String coy, String chdrnum);
	public void deleteLirrpfRecord(List<Lirrpf> dataList);
	public void updateLirrpfBulk(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx, int busDate);
	public void updateLirrpfByRngmnt(String coy, String chdrnum, String life, String coverage, String rider, String plnsfx, int busDate, String rngmnt);
}
