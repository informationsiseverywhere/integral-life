/*
 * File: P5434.java
 * Date: 30 August 2009 0:25:38
 * Author: Quipoz Limited
 * 
 * Class transformed from P5434.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.reassurance.dataaccess.LirrTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S5434ScreenVars;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Reassurance Experience Enquiry Submenu.
*
* Overview
* ========
*
* This program forms part of the Reassurance Development.
* It is the Sub-menu for the Reassurance Experience Enquiry
* Suite.
*
* Action A - Company Exposure: will switch to P5435 to display
* all LRRH records for the client and risk class requested.
*
* For action A, the following parameters may be supplied:
* CLIENT     : Mandatory
* RISK CLASS : Optional
*
* Action B - Reassurer Exposure: will switch to P5436 to display
* all LIRR records for the client, reassurer and arrangement
* requested.
*
* For action B, the following parameters may be supplied:
* CLIENT     : Mandatory
* REASSURER  : Mandatory
* ARRANGEMENT: Optional
*
* Processing
* ==========
*
* 1000-INITIALISE.
*
* Initialise screen.
*
* 2000-SCREEN-EDIT.
*
* Display the screen.
* Validate all fields including batch request if required.
* If errors redisplay screen.
* Store the CLTS record selected.
*
* 3000-UPDATE.
*
* Update the WSSP storage area.
*
* 4000-WHERE-NEXT.
*
* Store the next 4 programs.
* Move 1 to the program pointer.
*
*****************************************************************
* </pre>
*/
public class P5434 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5434");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private static final String e058 = "E058";
	private static final String e070 = "E070";
	private static final String e186 = "E186";
	private static final String e331 = "E331";
	private static final String e374 = "E374";
	private static final String cltsrec = "CLTSREC";
	private static final String lrrhrec = "LRRHREC";
	private static final String lirrrec = "LIRRREC";
	private static final String rasarec = "RASAREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private LirrTableDAM lirrIO = new LirrTableDAM();
	private LrrhTableDAM lrrhIO = new LrrhTableDAM();
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Clntkey wsaaClntkey = new Clntkey();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S5434ScreenVars sv = ScreenProgram.getScreenVars( S5434ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080
	}

	public P5434() {
		super();
		screenVars = sv;
		new ScreenModel("S5434", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					checkSanctions2030();
				case checkForErrors2080: 
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void checkSanctions2030()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.clttwo,SPACES)
		&& isNE(sv.action,SPACES)
		&& isEQ(sv.actionErr,SPACES)) {
			sv.clttwoErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.clttwo);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.clttwoErr.set(e058);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.clttwoErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.clnt);
			sdasancrec.entycoy.set(wsspcomn.fsuco);
			sdasancrec.entynum.set(sv.clttwo);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.clttwoErr.set(sdasancrec.statuz);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		if (isEQ(sv.actionErr,SPACES)
		&& isEQ(sv.clttwoErr,SPACES)) {
			if (isEQ(sv.action,"A")) {
				validateActionA2100();
			}
			else {
				if (isEQ(sv.action,"B")) {
					validateActionB2200();
				}
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateActionA2100()
	{
		para2110();
	}

protected void para2110()
	{
		if (isNE(sv.rasnum,SPACES)) {
			sv.rasnumErr.set(e374);
		}
		if (isNE(sv.rngmnt,SPACES)) {
			sv.rngmntErr.set(e374);
		}
		if (isNE(sv.lrkcls,SPACES)) {
			lrrhIO.setParams(SPACES);
			lrrhIO.setClntpfx(cltsIO.getClntpfx());
			lrrhIO.setClntcoy(cltsIO.getClntcoy());
			lrrhIO.setClntnum(cltsIO.getClntnum());
			lrrhIO.setCompany(wsspcomn.company);
			lrrhIO.setLrkcls(sv.lrkcls);
			lrrhIO.setFormat(lrrhrec);
			lrrhIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lrrhIO);
			if (isNE(lrrhIO.getStatuz(),varcom.oK)
			&& isNE(lrrhIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(lrrhIO.getParams());
				fatalError600();
			}
			if (isNE(lrrhIO.getStatuz(),varcom.oK)) {
				sv.lrkclsErr.set(e331);
			}
		}
	}

protected void validateActionB2200()
	{
		para2210();
	}

protected void para2210()
	{
		if (isEQ(sv.rasnum,SPACES)) {
			sv.rasnumErr.set(e186);
		}
		if (isEQ(sv.rasnumErr,SPACES)) {
			rasaIO.setParams(SPACES);
			rasaIO.setRascoy(wsspcomn.company);
			rasaIO.setRasnum(sv.rasnum);
			rasaIO.setFormat(rasarec);
			rasaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, rasaIO);
			if (isNE(rasaIO.getStatuz(),varcom.oK)) {
				sv.rasnumErr.set(e331);
			}
		}
		if (isNE(sv.lrkcls,SPACES)) {
			sv.lrkclsErr.set(e374);
		}
		if (isNE(sv.rngmnt,SPACES)
		&& isEQ(sv.rasnumErr,SPACES)) {
			lirrIO.setParams(SPACES);
			lirrIO.setRasnum(sv.rasnum);
			lirrIO.setRngmnt(sv.rngmnt);
			lirrIO.setCompany(wsspcomn.company);
			lirrIO.setClntpfx(cltsIO.getClntpfx());
			lirrIO.setClntcoy(cltsIO.getClntcoy());
			lirrIO.setClntnum(cltsIO.getClntnum());
			lirrIO.setFormat(lirrrec);
			lirrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lirrIO);
			if (isNE(lirrIO.getStatuz(),varcom.oK)
			&& isNE(lirrIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(lirrIO.getParams());
				fatalError600();
			}
			if (isNE(lirrIO.getStatuz(),varcom.oK)) {
				sv.rngmntErr.set(e331);
			}
		}
	}

protected void update3000()
	{
		para3010();
	}

protected void para3010()
	{
		wsspcomn.flag.set("I");
		wsspcomn.sbmaction.set(scrnparams.action);
		wsspcomn.lastActn.set(sv.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		wsaaClntkey.clntClntpfx.set("CN");
		wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
		wsaaClntkey.clntClntnum.set(sv.clttwo);
		wsspcomn.clntkey.set(wsaaClntkey);
		wssplife.lrkcls.set(sv.lrkcls);
		wssplife.rasnum.set(sv.rasnum);
		wssplife.rngmnt.set(sv.rngmnt);
	}

protected void whereNext4000()
	{
		/*PARA*/
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}
}
