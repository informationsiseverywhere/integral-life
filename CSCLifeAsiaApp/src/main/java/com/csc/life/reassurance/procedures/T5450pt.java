/*
 * File: T5450pt.java
 * Date: 30 August 2009 2:21:01
 * Author: Quipoz Limited
 * 
 * Class transformed from T5450PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.reassurance.tablestructures.T5450rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5450.
*
*
*****************************************************************
* </pre>
*/
public class T5450pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Reassurance Premium Basis                  S5450");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(73);
	private FixedLengthStringData filler9 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Premium Class");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 37);
	private FixedLengthStringData filler11 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 45);
	private FixedLengthStringData filler12 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 53);
	private FixedLengthStringData filler13 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 61);
	private FixedLengthStringData filler14 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 69);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(78);
	private FixedLengthStringData filler15 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine005, 7, FILLER).init("-----------------------------------------------------------------------");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler17 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 7, FILLER).init("Prem Calctn.    Single Life");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 37);
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 45);
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 53);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 61);
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 69);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler23 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 23, FILLER).init("Joint Life");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 37);
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 45);
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 53);
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 61);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 69);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler29 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine008, 7, FILLER).init("Prem Rates .         Select");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 37);
	private FixedLengthStringData filler31 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 45);
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 53);
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 61);
	private FixedLengthStringData filler34 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 69);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler35 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 28, FILLER).init("Ultimate");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 37);
	private FixedLengthStringData filler37 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 45);
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 53);
	private FixedLengthStringData filler39 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 61);
	private FixedLengthStringData filler40 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 69);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(71);
	private FixedLengthStringData filler41 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 7, FILLER).init("Initial Discount/Comm. Period");
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 61).setPattern("ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 69).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler47 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler48 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 7, FILLER).init("Commission Basis");
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 37);
	private FixedLengthStringData filler49 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 45);
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 53);
	private FixedLengthStringData filler51 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 61);
	private FixedLengthStringData filler52 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 69);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(73);
	private FixedLengthStringData filler53 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler54 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 7, FILLER).init("Commission Rates");
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 37);
	private FixedLengthStringData filler55 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 45);
	private FixedLengthStringData filler56 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 53);
	private FixedLengthStringData filler57 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 61);
	private FixedLengthStringData filler58 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 69);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler59 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler60 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 7, FILLER).init("Rate Factor");
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine013, 37).setPattern("Z.ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine013, 45).setPattern("Z.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine013, 53).setPattern("Z.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine013, 61).setPattern("Z.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 2).isAPartOf(wsaaPrtLine013, 69).setPattern("Z.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(28);
	private FixedLengthStringData filler65 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5450rec t5450rec = new T5450rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5450pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5450rec.t5450Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo012.set(t5450rec.premsubr01);
		fieldNo017.set(t5450rec.rjlprem01);
		fieldNo022.set(t5450rec.slctrate01);
		fieldNo027.set(t5450rec.ultmrate01);
		fieldNo037.set(t5450rec.basicCommMeth01);
		fieldNo032.set(t5450rec.inlprd01);
		fieldNo047.set(t5450rec.rratfac01);
		fieldNo042.set(t5450rec.commrate01);
		fieldNo007.set(t5450rec.rprmcls01);
		fieldNo013.set(t5450rec.premsubr02);
		fieldNo018.set(t5450rec.rjlprem02);
		fieldNo023.set(t5450rec.slctrate02);
		fieldNo028.set(t5450rec.ultmrate02);
		fieldNo033.set(t5450rec.inlprd02);
		fieldNo038.set(t5450rec.basicCommMeth02);
		fieldNo043.set(t5450rec.commrate02);
		fieldNo008.set(t5450rec.rprmcls02);
		fieldNo048.set(t5450rec.rratfac02);
		fieldNo009.set(t5450rec.rprmcls03);
		fieldNo014.set(t5450rec.premsubr03);
		fieldNo019.set(t5450rec.rjlprem03);
		fieldNo024.set(t5450rec.slctrate03);
		fieldNo029.set(t5450rec.ultmrate03);
		fieldNo034.set(t5450rec.inlprd03);
		fieldNo039.set(t5450rec.basicCommMeth03);
		fieldNo044.set(t5450rec.commrate03);
		fieldNo049.set(t5450rec.rratfac03);
		fieldNo010.set(t5450rec.rprmcls04);
		fieldNo015.set(t5450rec.premsubr04);
		fieldNo020.set(t5450rec.rjlprem04);
		fieldNo025.set(t5450rec.slctrate04);
		fieldNo030.set(t5450rec.ultmrate04);
		fieldNo035.set(t5450rec.inlprd04);
		fieldNo040.set(t5450rec.basicCommMeth04);
		fieldNo045.set(t5450rec.commrate04);
		fieldNo050.set(t5450rec.rratfac04);
		fieldNo016.set(t5450rec.premsubr05);
		fieldNo021.set(t5450rec.rjlprem05);
		fieldNo026.set(t5450rec.slctrate05);
		fieldNo031.set(t5450rec.ultmrate05);
		fieldNo036.set(t5450rec.inlprd05);
		fieldNo041.set(t5450rec.basicCommMeth05);
		fieldNo046.set(t5450rec.commrate05);
		fieldNo051.set(t5450rec.rratfac05);
		fieldNo011.set(t5450rec.rprmcls05);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
