package com.csc.life.reassurance.dataaccess.dao;

import com.csc.life.reassurance.dataaccess.model.Rasapf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RasapfDAO extends BaseDAO<Rasapf> {
	public Rasapf getRasapf(String rascoy, String rasnum);
}
