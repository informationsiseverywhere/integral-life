package com.csc.life.reassurance.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6261
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6261ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1002);
	public FixedLengthStringData dataFields = new FixedLengthStringData(506).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,107);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData branch = DD.branch.copy().isAPartOf(dataFields,147);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData branchnm = DD.branchnm.copy().isAPartOf(dataFields,179);
	public FixedLengthStringData clientind = DD.clientind.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,220);
	public ZonedDecimalData commdt = DD.commdt.copyToZonedDecimal().isAPartOf(dataFields,267);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData currdesc = DD.currdesc.copy().isAPartOf(dataFields,278);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,308);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,309);
	public ZonedDecimalData minsta = DD.minsta.copyToZonedDecimal().isAPartOf(dataFields,311);
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,328);
	public ZonedDecimalData nxtpay = DD.nxtpay.copyToZonedDecimal().isAPartOf(dataFields,338);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,346);
	public FixedLengthStringData paymthd = DD.paymthd.copy().isAPartOf(dataFields,393);
	public FixedLengthStringData rapayee = DD.rapayee.copy().isAPartOf(dataFields,423);
	public FixedLengthStringData rapayfrq = DD.rapayfrq.copy().isAPartOf(dataFields,433);
	public FixedLengthStringData rapayfrqd = DD.rapayfrqd.copy().isAPartOf(dataFields,435);
	public FixedLengthStringData rapaymth = DD.rapaymth.copy().isAPartOf(dataFields,465);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(dataFields,466);
	public FixedLengthStringData reasst = DD.reasst.copy().isAPartOf(dataFields,474);
	public FixedLengthStringData reastdesc = DD.reastdesc.copy().isAPartOf(dataFields,475);
	public ZonedDecimalData stmtdt = DD.stmtdt.copyToZonedDecimal().isAPartOf(dataFields,490);
	public ZonedDecimalData termdt = DD.termdt.copyToZonedDecimal().isAPartOf(dataFields,498);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 506);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData branchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData branchnmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData clientindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData commdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData currdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData minstaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData nxtpayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData paymthdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData rapayeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData rapayfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData rapayfrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rapaymthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rasnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData reasstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData reastdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stmtdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData termdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 630);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] branchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] branchnmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] clientindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] commdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] currdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] minstaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] nxtpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] paymthdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] rapayeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] rapayfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] rapayfrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rapaymthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rasnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] reasstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] reastdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stmtdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] termdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData commdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nxtpayDisp = new FixedLengthStringData(10);
	public FixedLengthStringData stmtdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData termdtDisp = new FixedLengthStringData(10);

	public LongData S6261screenWritten = new LongData(0);
	public LongData S6261protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6261ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntselOut,new String[] {"01","21","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rapaymthOut,new String[] {"09","21","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rapayfrqOut,new String[] {"10","21","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rapayeeOut,new String[] {"08","21","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcodeOut,new String[] {"30","21","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntselOut,new String[] {"11","21","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minstaOut,new String[] {"12","21","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clientindOut,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasstOut,new String[] {"13","21","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(termdtOut,new String[] {"04","05","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(commdtOut,new String[] {"02","03","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nxtpayOut,new String[] {"06","07","-06",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {numsel, clntsel, cltname, rasnum, branch, branchnm, rapaymth, paymthd, rapayfrq, rapayfrqd, bankkey, bankdesc, branchdesc, bankacckey, bankaccdsc, rapayee, payenme, facthous, currcode, currdesc, agntsel, agentname, minsta, clientind, ddind, reasst, reastdesc, termdt, commdt, nxtpay, stmtdt};
		screenOutFields = new BaseData[][] {numselOut, clntselOut, cltnameOut, rasnumOut, branchOut, branchnmOut, rapaymthOut, paymthdOut, rapayfrqOut, rapayfrqdOut, bankkeyOut, bankdescOut, branchdescOut, bankacckeyOut, bankaccdscOut, rapayeeOut, payenmeOut, facthousOut, currcodeOut, currdescOut, agntselOut, agentnameOut, minstaOut, clientindOut, ddindOut, reasstOut, reastdescOut, termdtOut, commdtOut, nxtpayOut, stmtdtOut};
		screenErrFields = new BaseData[] {numselErr, clntselErr, cltnameErr, rasnumErr, branchErr, branchnmErr, rapaymthErr, paymthdErr, rapayfrqErr, rapayfrqdErr, bankkeyErr, bankdescErr, branchdescErr, bankacckeyErr, bankaccdscErr, rapayeeErr, payenmeErr, facthousErr, currcodeErr, currdescErr, agntselErr, agentnameErr, minstaErr, clientindErr, ddindErr, reasstErr, reastdescErr, termdtErr, commdtErr, nxtpayErr, stmtdtErr};
		screenDateFields = new BaseData[] {termdt, commdt, nxtpay, stmtdt};
		screenDateErrFields = new BaseData[] {termdtErr, commdtErr, nxtpayErr, stmtdtErr};
		screenDateDispFields = new BaseData[] {termdtDisp, commdtDisp, nxtpayDisp, stmtdtDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6261screen.class;
		protectRecord = S6261protect.class;
	}

}
