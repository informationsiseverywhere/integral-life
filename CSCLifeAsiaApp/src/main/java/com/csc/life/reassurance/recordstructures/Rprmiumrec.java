package com.csc.life.reassurance.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:07
 * Description:
 * Copybook name: RPRMIUMREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rprmiumrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rprmiumRec = new FixedLengthStringData(206);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rprmiumRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rprmiumRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(rprmiumRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(rprmiumRec, 18);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(rprmiumRec, 20);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(rprmiumRec, 22);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(rprmiumRec, 24);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(rprmiumRec, 27);
  	public FixedLengthStringData mop = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 29);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(rprmiumRec, 30);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rprmiumRec, 32);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 36);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rprmiumRec, 37);
  	public PackedDecimalData effectdt = new PackedDecimalData(8, 0).isAPartOf(rprmiumRec, 40);
  	public PackedDecimalData termdate = new PackedDecimalData(8, 0).isAPartOf(rprmiumRec, 45);
  	public PackedDecimalData sumin = new PackedDecimalData(17, 2).isAPartOf(rprmiumRec, 50);
  	public PackedDecimalData calcPrem = new PackedDecimalData(17, 2).isAPartOf(rprmiumRec, 59);
  	public PackedDecimalData ratingdate = new PackedDecimalData(8, 0).isAPartOf(rprmiumRec, 68);
  	public PackedDecimalData reRateDate = new PackedDecimalData(8, 0).isAPartOf(rprmiumRec, 73);
  	public PackedDecimalData lage = new PackedDecimalData(3, 0).isAPartOf(rprmiumRec, 78);
  	public PackedDecimalData jlage = new PackedDecimalData(3, 0).isAPartOf(rprmiumRec, 80);
  	public FixedLengthStringData lsex = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 82);
  	public FixedLengthStringData jlsex = new FixedLengthStringData(10).isAPartOf(rprmiumRec, 83);
  	public ZonedDecimalData duration = new ZonedDecimalData(2, 0).isAPartOf(rprmiumRec, 93).setUnsigned();
  	public FixedLengthStringData reasind = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 95);
  	public FixedLengthStringData freqann = new FixedLengthStringData(2).isAPartOf(rprmiumRec, 96);
  	public FixedLengthStringData arrears = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 98);
  	public FixedLengthStringData advance = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 99);
  	public PackedDecimalData guarperd = new PackedDecimalData(3, 0).isAPartOf(rprmiumRec, 100);
  	public PackedDecimalData intanny = new PackedDecimalData(5, 2).isAPartOf(rprmiumRec, 102);
  	public PackedDecimalData capcont = new PackedDecimalData(17, 2).isAPartOf(rprmiumRec, 105);
  	public FixedLengthStringData withprop = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 114);
  	public FixedLengthStringData withoprop = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 115);
  	public FixedLengthStringData ppind = new FixedLengthStringData(1).isAPartOf(rprmiumRec, 116);
  	public FixedLengthStringData nomlife = new FixedLengthStringData(8).isAPartOf(rprmiumRec, 117);
  	public PackedDecimalData dthpercn = new PackedDecimalData(5, 2).isAPartOf(rprmiumRec, 125);
  	public PackedDecimalData dthperco = new PackedDecimalData(5, 2).isAPartOf(rprmiumRec, 128);
  	public PackedDecimalData riskCessTerm = new PackedDecimalData(3, 0).isAPartOf(rprmiumRec, 131);
  	public PackedDecimalData benCessTerm = new PackedDecimalData(3, 0).isAPartOf(rprmiumRec, 133);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rprmiumRec, 135);
  	public PackedDecimalData reasratfac = new PackedDecimalData(3, 2).isAPartOf(rprmiumRec, 138);
  	public FixedLengthStringData rrate = new FixedLengthStringData(4).isAPartOf(rprmiumRec, 140);
  	public PackedDecimalData reinsCommprem = new PackedDecimalData(17,2).isAPartOf(rprmiumRec, 144);
  	public PackedDecimalData seqNo = new PackedDecimalData(4,0).isAPartOf(rprmiumRec, 153);
  	public PackedDecimalData bilfrmdt = new PackedDecimalData(8, 0).isAPartOf(rprmiumRec, 157);
  	public PackedDecimalData biltodt = new PackedDecimalData(8, 0).isAPartOf(rprmiumRec, 165);
  	public PackedDecimalData reinsPremBaseRate = new PackedDecimalData(16,15).isAPartOf(rprmiumRec, 173);
	public PackedDecimalData reinsAnnPrem = new PackedDecimalData(17,2).isAPartOf(rprmiumRec, 189);
  	
	public void initialize() {
		COBOLFunctions.initialize(rprmiumRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rprmiumRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}