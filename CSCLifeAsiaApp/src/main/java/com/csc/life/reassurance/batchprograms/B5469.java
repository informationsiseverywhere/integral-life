/*
 * File: B5469.java
 * Date: 29 August 2009 21:19:25
 * Author: Quipoz Limited
 * 
 * Class transformed from B5469.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.recordstructures.P5469par;
import com.csc.life.reassurance.reports.R5469Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Reassurance Cessions Report
*
* This batch program will report all the cessions that are
* associated to a particular reassurer within the specified
* acounting year. The cessions will be grouped by reassurer,
* by currency and by transaction type.
*
* Totals will be printed for each transaction type within each
* currency within each reassurer.
*
* The required RACD and PTRN records are selected using SQL.
*
* A parameter screen (P5469) is used to allow users to input
* a date range. Date From must be less than Date To.
*
* The SQL selection criteria are as follows :
*
*         SELECT :   RACDPF.CHDRNUM = PTRNPF.CHDRNUM  AND
*                    RACDPF.TRANNO  = PTRNPF.TRANNO   AND
*                    RACDPF.VALIDFLAG = '1'           AND
*                    RACDPF.CURRFROM >= P5469-DATEFRM AND
*                    RACDPF.CURRFROM <= P5469-DATETO  AND
*                    RACDPF.RETYPE    = BPRD-SYSTEM-PARAM01
*
* Initialise
*     - Define the SQL query based on the selection criteria
*       specified aboved.
*
* Read
*     - Fetch the SQL records.
*
* Perform Until End of File
*
*      Edit
*       - Check if it is printing for the first time.
*         If yes, print the report headers (R5469H01 and
*                                           R5469H02)
*         If no, check if there is any change in the
*         Reassurer or Currency.
*         (Refer 2700-Check-for-Changes).
*       - Add SQL-RAAMOUNT to both the currency sum reassured
*         total.
*       - Read the COVR files to retrieve the required info.
*       - Retrieve the rest of the information from CLNT and
*         CHDR and print it (R5469D01).
*
* End Perform
*
*****************************************************************
* </pre>
*/
public class B5469 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlracdpf1rs = null;
	private java.sql.PreparedStatement sqlracdpf1ps = null;
	private java.sql.Connection sqlracdpf1conn = null;
	private String sqlracdpf1 = "";
	private R5469Report printerFile = new R5469Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5469");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaSqlDatefrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaSqlDateto = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaSqlSystemParam02 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaCurrSaTotal = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaCurrSrTotal = new ZonedDecimalData(18, 2);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3629 = "T3629";
	private static final String t1693 = "T1693";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaFoundRecs = new FixedLengthStringData(1).init("N");
	private Validator foundRecs = new Validator(wsaaFoundRecs, "Y");
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData r5469H02 = new FixedLengthStringData(8);
	private FixedLengthStringData r5469h02O = new FixedLengthStringData(8).isAPartOf(r5469H02, 0);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(r5469h02O, 0);

	private FixedLengthStringData r5469T01 = new FixedLengthStringData(39);
	private FixedLengthStringData r5469t01O = new FixedLengthStringData(39).isAPartOf(r5469T01, 0);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(r5469t01O, 0);
	private ZonedDecimalData totamnt01 = new ZonedDecimalData(18, 2).isAPartOf(r5469t01O, 3);
	private ZonedDecimalData totamnt02 = new ZonedDecimalData(18, 2).isAPartOf(r5469t01O, 21);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private P5469par p5469par = new P5469par();
	private FormatsInner formatsInner = new FormatsInner();
	private R5469D01Inner r5469D01Inner = new R5469D01Inner();
	private R5469H01Inner r5469H01Inner = new R5469H01Inner();
	private SqlRacdpfInner sqlRacdpfInner = new SqlRacdpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2060, 
		exit2090, 
		currency2620, 
		print2680
	}

	public B5469() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspLongconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspLongconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspLongconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspLongconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Open required files.*/
		printerFile.openOutput();
		p5469par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaRasnum.set(SPACES);
		wsaaCurrcode.set(SPACES);
		wsaaChdrnum.set(SPACES);
		wsaaCurrSaTotal.set(ZERO);
		wsaaCurrSrTotal.set(ZERO);
		wsaaFirstTime.set("Y");
		wsaaFoundRecs.set("N");
		wsaaOverflow.set("N");
		/* Select RACD and PTRN records*/
		/* Define the query required by declaring a cursor*/
		/* Retrieve all records that exists in RACDPF, PTRNPF and*/
		/* LIFEPF and fall within the accounting year-mth*/
		wsaaSqlDatefrm.set(p5469par.datefrm);
		wsaaSqlDateto.set(p5469par.dateto);
		/*  MOVE BPRD-SYSTEM-PARAM01    TO WSAA-SQL-SYSTEM-PARAM01.      */
		wsaaSqlSystemParam02.set(bprdIO.getSystemParam02());
		sqlracdpf1 = " SELECT  RACDPF.CHDRNUM, RACDPF.RASNUM, RACDPF.CURRCODE, RACDPF.CMDATE, RACDPF.LIFE, RACDPF.COVERAGE, RACDPF.RIDER, RACDPF.PLNSFX, RACDPF.RAAMOUNT, RACDPF.RNGMNT, RACDPF.LRKCLS, PTRNPF.BATCTRCDE" +
" FROM   " + getAppVars().getTableNameOverriden("RACDPF") + " ,  " + getAppVars().getTableNameOverriden("PTRNPF") + " " +
" WHERE RACDPF.CHDRNUM = PTRNPF.CHDRNUM" +
" AND RACDPF.TRANNO = PTRNPF.TRANNO" +
" AND RACDPF.VALIDFLAG = '1'" +
" AND RACDPF.CURRFROM >= ?" +
" AND RACDPF.CURRFROM <= ?" +
" AND RACDPF.RETYPE = ?" +
" AND RACDPF.CHDRCOY = ?" +
" ORDER BY RACDPF.RASNUM, RACDPF.CURRCODE, RACDPF.CHDRNUM, PTRNPF.BATCTRCDE, RACDPF.CMDATE";
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlracdpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.reassurance.dataaccess.RacdpfTableDAM(), new com.csc.fsu.general.dataaccess.PtrnpfTableDAM()});
			sqlracdpf1ps = getAppVars().prepareStatementEmbeded(sqlracdpf1conn, sqlracdpf1);
			getAppVars().setDBNumber(sqlracdpf1ps, 1, wsaaSqlDatefrm);
			getAppVars().setDBNumber(sqlracdpf1ps, 2, wsaaSqlDateto);
			//ILIFE-1280 by smalchi2 STARTS
			getAppVars().setDBString(sqlracdpf1ps, 3, wsaaSqlSystemParam02.trimTrailing());
			//ENDS
			getAppVars().setDBString(sqlracdpf1ps, 4, bsprIO.getCompany());
			sqlracdpf1rs = getAppVars().executeQuery(sqlracdpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case eof2060: 
					eof2060();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlracdpf1rs)) {
				getAppVars().getDBObject(sqlracdpf1rs, 1, sqlRacdpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlracdpf1rs, 2, sqlRacdpfInner.sqlRasnum);
				getAppVars().getDBObject(sqlracdpf1rs, 3, sqlRacdpfInner.sqlCurrcode);
				getAppVars().getDBObject(sqlracdpf1rs, 4, sqlRacdpfInner.sqlCmdate);
				getAppVars().getDBObject(sqlracdpf1rs, 5, sqlRacdpfInner.sqlLife);
				getAppVars().getDBObject(sqlracdpf1rs, 6, sqlRacdpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlracdpf1rs, 7, sqlRacdpfInner.sqlRider);
				getAppVars().getDBObject(sqlracdpf1rs, 8, sqlRacdpfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlracdpf1rs, 9, sqlRacdpfInner.sqlRaamount);
				getAppVars().getDBObject(sqlracdpf1rs, 10, sqlRacdpfInner.sqlRngmnt);
				getAppVars().getDBObject(sqlracdpf1rs, 11, sqlRacdpfInner.sqlLrkcls);
				getAppVars().getDBObject(sqlracdpf1rs, 12, sqlRacdpfInner.sqlBatctrcde);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
		/* This is to print the currency total on the last page only*/
		/* if records have been selected.*/
		if (foundRecs.isTrue()) {
			printTotal2900();
		}
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		wsaaFoundRecs.set("Y");
		/* Check if the report is being printed for the first time or*/
		/* new page required ie. WSAA-OVERFLOW = 'Y'*/
		if (firstTime.isTrue()
		|| newPageReq.isTrue()) {
			printHeader2600();
			wsaaRasnum.set(sqlRacdpfInner.sqlRasnum);
			wsaaCurrcode.set(sqlRacdpfInner.sqlCurrcode);
			wsaaFirstTime.set("N");
		}
		else {
			checkForChanges2700();
		}
		wsaaCurrSrTotal.add(sqlRacdpfInner.sqlRaamount);
		/* Read the COVRMJA file*/
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(bsprIO.getCompany());
		covrmjaIO.setChdrnum(sqlRacdpfInner.sqlChdrnum);
		covrmjaIO.setLife(sqlRacdpfInner.sqlLife);
		covrmjaIO.setPlanSuffix(sqlRacdpfInner.sqlPlnsfx);
		covrmjaIO.setCoverage(sqlRacdpfInner.sqlCoverage);
		covrmjaIO.setRider(sqlRacdpfInner.sqlRider);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		datcon1rec.intDate.set(sqlRacdpfInner.sqlCmdate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r5469D01Inner.cmdate.set(datcon1rec.extDate);
		r5469D01Inner.raamount.set(sqlRacdpfInner.sqlRaamount);
		if (isEQ(sqlRacdpfInner.sqlChdrnum, wsaaChdrnum)
		&& !newPageReq.isTrue()) {
			r5469D01Inner.chdrnum.set(SPACES);
		}
		else {
			r5469D01Inner.chdrnum.set(sqlRacdpfInner.sqlChdrnum);
			wsaaOverflow.set("N");
		}
		lifeInformation2800();
		/* Check if life assured has changed*/
		if (isEQ(lifelnbIO.getLifcnum(),wsaaLifenum)
		&& isEQ(sqlRacdpfInner.sqlChdrnum, wsaaChdrnum)) {
			r5469D01Inner.clntnum.set(SPACES);
			r5469D01Inner.clntsname.set(SPACES);
			/*****                                CLNTNM  OF R5469D01-O         */
		}
		if (isNE(sqlRacdpfInner.sqlChdrnum, wsaaChdrnum)
		|| isNE(lifelnbIO.getLifcnum(),wsaaLifenum)) {
			wsaaCurrSaTotal.add(covrmjaIO.getSumins());
			/* Read T5449 to retrieve the Risk Class.*/
			/*        MOVE 'IT'               TO ITEM-ITEMPFX                  */
			/*        MOVE BSPR-COMPANY       TO ITEM-ITEMCOY                  */
			/*        MOVE T5449              TO ITEM-ITEMTABL                 */
			/*        MOVE SQL-RNGMNT         TO ITEM-ITEMITEM                 */
			/*        MOVE SPACES             TO ITEM-ITEMSEQ                  */
			/*        MOVE READR              TO ITEM-FUNCTION                 */
			/*        MOVE ITEMREC            TO ITEM-FORMAT                   */
			/*        CALL 'ITEMIO'           USING ITEM-PARAMS                */
			/*        IF ITEM-STATUZ          NOT = O-K                        */
			/*           MOVE ITEM-PARAMS     TO SYSR-PARAMS                   */
			/*           PERFORM 600-FATAL-ERROR                               */
			/*        END-IF                                                   */
			/*        MOVE ITEM-GENAREA       TO T5449-T5449-REC               */
			/*        MOVE T5449-LRKCLS       TO LRKCLS OF R5469D01-O          */
			datcon1rec.intDate.set(covrmjaIO.getCrrcd());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			r5469D01Inner.crrcd.set(datcon1rec.extDate);
			if (isNE(covrmjaIO.getRiskCessTerm(),ZERO)) {
				r5469D01Inner.rcestrm.set(covrmjaIO.getRiskCessTerm());
			}
			else {
				datcon3rec.datcon3Rec.set(SPACES);
				datcon3rec.intDate1.set(covrmjaIO.getCrrcd());
				datcon3rec.intDate2.set(covrmjaIO.getRiskCessDate());
				datcon3rec.frequency.set("01");
				datcon3rec.freqFactor.set(0);
				callProgram(Datcon3.class, datcon3rec.datcon3Rec);
				if (isNE(datcon3rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon3rec.datcon3Rec);
					syserrrec.statuz.set(datcon3rec.statuz);
					fatalError600();
				}
				r5469D01Inner.rcestrm.set(datcon3rec.freqFactor);
			}
			/**        MOVE COVRMJA-SUMINS     TO SUMINS  OF R5469D01-O         */
		}
		else {
			r5469D01Inner.lrkcls.set(SPACES);
			r5469D01Inner.crrcd.set(SPACES);
			r5469D01Inner.rcestrm.set(ZERO);
			r5469D01Inner.sumins.set(ZERO);
		}
		r5469D01Inner.lrkcls.set(sqlRacdpfInner.sqlLrkcls);
		r5469D01Inner.sumins.set(covrmjaIO.getSumins());
		printerFile.printR5469d01(r5469D01Inner.r5469D01);
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
		/* Update Control Totals - No. of Treaty/Facultative Records*/
		/*   IF BPRD-SYSTEM-PARAM01      = 'F'                            */
		if (isEQ(bprdIO.getSystemParam02(),"F")) {
			contotrec.totno.set(ct01);
		}
		else {
			contotrec.totno.set(ct02);
		}
		contotrec.totval.set(1);
		callContot001();
		wsaaLifenum.set(lifelnbIO.getLifcnum());
		wsaaChdrnum.set(sqlRacdpfInner.sqlChdrnum);
	}

protected void printHeader2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					printHeader2610();
				case currency2620: 
					currency2620();
				case print2680: 
					print2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void printHeader2610()
	{
		/* Set up header information.*/
		if (isEQ(sqlRacdpfInner.sqlRasnum, wsaaRasnum)) {
			goTo(GotoLabel.currency2620);
		}
		/* Read RASA records.*/
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(bupaIO.getCompany());
		rasaIO.setRasnum(sqlRacdpfInner.sqlRasnum);
		rasaIO.setFormat(formatsInner.rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.mrnf)//smalchi2 for ILIFE-2191 STARTS
		&& isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			fatalError600();
		}
		/* Read CLTS records.*/
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(rasaIO.getClntnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* To print the different header.*/
		/*   IF BPRD-SYSTEM-PARAM01 = 'T'                                 */
		if (isEQ(bprdIO.getSystemParam02(),"T")) {
			r5469H01Inner.shortnam.set("                      Treaty");
		}
		else {
			r5469H01Inner.shortnam.set("                 Facultative");
		}
		r5469H01Inner.rasnum.set(sqlRacdpfInner.sqlRasnum);
		plainname();
		r5469H01Inner.clntnm.set(wsspLongconfname);
		r5469H01Inner.cltaddr01.set(cltsIO.getCltaddr01());
		r5469H01Inner.cltaddr02.set(cltsIO.getCltaddr02());
		r5469H01Inner.cltaddr03.set(cltsIO.getCltaddr03());
		/* Read T1693 to retrieve the Life Company.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		r5469H01Inner.descrip.set(descIO.getLongdesc());
		datcon1rec.intDate.set(wsaaSqlDatefrm);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r5469H01Inner.datefrm.set(datcon1rec.extDate);
		datcon1rec.intDate.set(wsaaSqlDateto);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r5469H01Inner.dateto.set(datcon1rec.extDate);
	}

protected void currency2620()
	{
		if (isEQ(sqlRacdpfInner.sqlCurrcode, wsaaCurrcode)) {
			goTo(GotoLabel.print2680);
		}
		/* Get Currency Code Description from T3629.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlRacdpfInner.sqlCurrcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*    MOVE DESC-LONGDESC          TO CURRDESC OF R5469H01-O.       */
		r5469H01Inner.longdesc.set(descIO.getLongdesc());
		r5469H01Inner.currcode.set(sqlRacdpfInner.sqlCurrcode);
	}

protected void print2680()
	{
		printerFile.printR5469h01(r5469H01Inner.r5469H01);
		printerFile.printR5469h02(r5469H02);
		wsaaOverflow.set("Y");
		wsaaLifenum.set(SPACES);
		/*EXIT*/
	}

protected void checkForChanges2700()
	{
		/*CHECK-FOR-CHANGES*/
		if (isNE(sqlRacdpfInner.sqlRasnum, wsaaRasnum)) {
			printTotal2900();
			printHeader2600();
			wsaaRasnum.set(sqlRacdpfInner.sqlRasnum);
			wsaaCurrcode.set(sqlRacdpfInner.sqlCurrcode);
		}
		else {
			if (isNE(sqlRacdpfInner.sqlCurrcode, wsaaCurrcode)) {
				printTotal2900();
				printHeader2600();
				wsaaCurrcode.set(sqlRacdpfInner.sqlCurrcode);
			}
		}
		/*EXIT*/
	}

protected void lifeInformation2800()
	{
		lifeInformation2810();
	}

protected void lifeInformation2810()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(bsprIO.getCompany());
		lifelnbIO.setChdrnum(sqlRacdpfInner.sqlChdrnum);
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		r5469D01Inner.clntnum.set(cltsIO.getClntnum());
		plainname();
		r5469D01Inner.clntsname.set(wsspLongconfname);
		/* MOVE WSSP-LONGCONFNAME      TO CLNTNM OF R5469D01-O.         */
		/* Read T1688 to retrieve the transaction code description.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(sqlRacdpfInner.sqlBatctrcde);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		r5469D01Inner.descrip.set(descIO.getLongdesc());
	}

protected void printTotal2900()
	{
		/*PRINT-TOTAL*/
		currcode.set(wsaaCurrcode);
		totamnt01.set(wsaaCurrSaTotal);
		totamnt02.set(wsaaCurrSrTotal);
		printerFile.printR5469t01(r5469T01);
		wsaaCurrSaTotal.set(ZERO);
		wsaaCurrSrTotal.set(ZERO);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlracdpf1conn, sqlracdpf1ps, sqlracdpf1rs);
		/* Close any open files.*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData rasarec = new FixedLengthStringData(10).init("RASAREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
}
/*
 * Class transformed  from Data Structure SQL-RACDPF--INNER
 */
private static final class SqlRacdpfInner { 

		/* SQL-RACDPF */
	private FixedLengthStringData sqlRacdrec = new FixedLengthStringData(54);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlRacdrec, 0);
	private FixedLengthStringData sqlRasnum = new FixedLengthStringData(8).isAPartOf(sqlRacdrec, 8);
	private FixedLengthStringData sqlCurrcode = new FixedLengthStringData(3).isAPartOf(sqlRacdrec, 16);
	private PackedDecimalData sqlCmdate = new PackedDecimalData(8, 0).isAPartOf(sqlRacdrec, 19);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 24);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 26);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlRacdrec, 28);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlRacdrec, 30);
	private PackedDecimalData sqlRaamount = new PackedDecimalData(17, 2).isAPartOf(sqlRacdrec, 33);
	private FixedLengthStringData sqlRngmnt = new FixedLengthStringData(4).isAPartOf(sqlRacdrec, 42);
	private FixedLengthStringData sqlLrkcls = new FixedLengthStringData(4).isAPartOf(sqlRacdrec, 46);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(sqlRacdrec, 50);
}
/*
 * Class transformed  from Data Structure R5469-H01--INNER
 */
private static final class R5469H01Inner { 

	private FixedLengthStringData r5469H01 = new FixedLengthStringData(156+DD.cltaddr.length*3);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData r5469h01O = new FixedLengthStringData(156+DD.cltaddr.length*3).isAPartOf(r5469H01, 0);
	private FixedLengthStringData shortnam = new FixedLengthStringData(28).isAPartOf(r5469h01O, 0);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5469h01O, 28);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37).isAPartOf(r5469h01O, 36);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(r5469h01O, 73);
	private FixedLengthStringData cltaddr01 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5469h01O, 103);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData cltaddr02 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5469h01O, 103+DD.cltaddr.length);
	private FixedLengthStringData cltaddr03 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5469h01O, 103+DD.cltaddr.length*2);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10).isAPartOf(r5469h01O, 103+DD.cltaddr.length*3);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData dateto = new FixedLengthStringData(10).isAPartOf(r5469h01O, 113+DD.cltaddr.length*3);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(r5469h01O, 123+DD.cltaddr.length*3);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r5469h01O, 126+DD.cltaddr.length*3);
}
/*
 * Class transformed  from Data Structure R5469-D01--INNER
 */
private static final class R5469D01Inner { 

	private FixedLengthStringData r5469D01 = new FixedLengthStringData(137);
	private FixedLengthStringData r5469d01O = new FixedLengthStringData(137).isAPartOf(r5469D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5469d01O, 0);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(r5469d01O, 8);
	private FixedLengthStringData clntsname = new FixedLengthStringData(30).isAPartOf(r5469d01O, 16);
	private FixedLengthStringData crrcd = new FixedLengthStringData(10).isAPartOf(r5469d01O, 46);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0).isAPartOf(r5469d01O, 56);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(r5469d01O, 59);
	private FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(r5469d01O, 76);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5469d01O, 80);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5469d01O, 90);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(r5469d01O, 107);
}
}
