/*
 * File: P5465.java
 * Date: 30 August 2009 0:28:10
 * Author: Quipoz Limited
 * 
 * Class transformed from P5465.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.reassurance.dataaccess.CcalTableDAM;
import com.csc.life.reassurance.dataaccess.RacdrcvTableDAM;
import com.csc.life.reassurance.dataaccess.RasaTableDAM;
import com.csc.life.reassurance.screens.S5465ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*  P5465 - Reassurance Cash Call Transaction Screen.
*
*  Overview.
*  ---------
*
*  This Transaction Program is called from the Reassurance
*  Cash Calls Program, P5462. Depending on the Action chosen
*  this Program creates a new CCAL Record, modifies or can
*  enquire into an existing CCAL Record.
*
*  Initialise  all  relevant  variables  and prepare the screen
*  for display.
*
*  Set a 'First Time' flag for use in the update section.
*
*  Perform a RETRV on the CHDRMJA, RACDMJA, COVRLNB and CCAL
*  files.
*
*  Use CHDRMJA to display the heading  details  on  the  screen
*  using  T5688  for  the  contract  type  description  and the
*  Client  file  for  the  relevant  client  names.  The  short
*  descriptions  for  the  contracts  risk  and  premium status
*  codes should be  obtained  from  T3623,  (Risk  Status)  and
*  T3588, (Premium Status).
*
*  Set  the  Contract Currency in the heading from the Contract
*  Header.
*
*  Display the RACDMJA details on the screen looking up  all  of
*  the descriptions from DESC where appropriate. These are
*  protected at all times.
*
*  The total Sum Assured must be Calculated and displayed. This
*  is done by a BEGN on the COVRLNB Records with Plan Suffix = 0
*  and all the COVRs which match the Company, Life, Coverage and
*  Rider are Accumulated.
*
*  The lower portion of the Screen is used to display the CCAL
*  details. Only three fields are shown: Cash Call Amount
*  Recieved, amount received so far, and Cash Call Date. In
*  enquiry mode, all these fields are protected, whilst in Create
*  mode all are initialised except the date which is set to the
*  date on the Sub-Menu.
*
*  It will be Impossible to DELETE CCAL Records - A Reversal of
*  the Process will instead be necessary.
*
*  For modify, the amount received so far is protected and
*  updated upon the entry of a new Cash Call amount.
*
*  If WSSP-FLAG = 'C' Write a new CCAL Record.
*
*  If WSSP-FLAG = 'M' The old CCAL Record is written with a
*  ValidFlag of '2' and a New one is Created. The appropriate
*  ACMVs are written.
*
*  The 4000- Section will simply add 1 to Program pointer.
*
****************************************************************
* </pre>
*/
public class P5465 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5465");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaCurrfrom = new PackedDecimalData(8, 0).init(ZERO);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO);
		/* ERRORS */
	private static final String r073 = "R073";
	private static final String e355 = "E355";
	private static final String f399 = "F399";
	private static final String h926 = "H926";
	private static final String rfik = "RFIK";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t3629 = "T3629";
	private static final String t5449 = "T5449";
	private static final String t5688 = "T5688";
	private static final String t5645 = "T5645";
	private static final String t5454 = "T5454";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String ccalrec = "CCALREC   ";
	private static final String rasarec = "RASAREC   ";
	private CcalTableDAM ccalIO = new CcalTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Reassurance Cash Calls Logical File*/
	private RacdrcvTableDAM racdrcvIO = new RacdrcvTableDAM();
		/*Reassurance Account File.*/
	private RasaTableDAM rasaIO = new RasaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private T5645rec t5645rec = new T5645rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S5465ScreenVars sv = ScreenProgram.getScreenVars( S5465ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		racdrcv1060, 
		exit1999
	}

	public P5465() {
		super();
		screenVars = sv;
		new ScreenModel("S5465", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case racdrcv1060: 
					racdrcv1060();
					cashCall1070();
				case exit1999: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{	
	//ILJ-49 Starts
			cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
			if(cntDteFlag)	{
				sv.iljCntDteFlag.set("Y");
			} else {
				sv.iljCntDteFlag.set("N");
			}
	//ILJ-49 End 
		wsaaTransTime.set(getCobolTime());
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1999);
		}
		sv.dataArea.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			fatalError600();
		}
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		sv.crtable.set(covrlnbIO.getCrtable());
		sv.sumins.set(covrlnbIO.getSumins());
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDescitem(chdrmjaIO.getCntcurr());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill("?");
		}
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setDesctabl(t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.tabledesc.set(descIO.getLongdesc());
		}
		else {
			sv.tabledesc.set(SPACES);
		}
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setDesctabl(t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.longdesc.set(descIO.getLongdesc());
		}
		else {
			sv.longdesc.set(SPACES);
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.currcode.set(chdrmjaIO.getCntcurr());
		lifemjaIO.setParams(SPACES);
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrlnbIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.linsnameErr.set(e355);
			sv.linsname.set(SPACES);
			goTo(GotoLabel.racdrcv1060);
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.linsnameErr.set(e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.racdrcv1060);
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1200();
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void racdrcv1060()
	{
		racdrcvIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, racdrcvIO);
		if (isNE(racdrcvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(racdrcvIO.getParams());
			syserrrec.statuz.set(racdrcvIO.getStatuz());
			fatalError600();
		}
		sv.rasnum.set(racdrcvIO.getRasnum());
		rasaIO.setParams(SPACES);
		rasaIO.setRascoy(wsspcomn.company);
		rasaIO.setRasnum(sv.rasnum);
		rasaIO.setFormat(rasarec);
		rasaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rasaIO);
		if (isEQ(rasaIO.getStatuz(),varcom.oK)) {
			cltsIO.setClntnum(rasaIO.getClntnum());
			getClientDetails1200();
			plainname();
			sv.clntname.set(wsspcomn.longconfname);
		}
		else {
			syserrrec.params.set(rasaIO.getParams());
			syserrrec.statuz.set(rasaIO.getStatuz());
			fatalError600();
		}
		sv.rngmnt.set(racdrcvIO.getRngmnt());
		descIO.setDescitem(racdrcvIO.getRngmnt());
		descIO.setDesctabl(t5449);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rngmntdsc.set(descIO.getLongdesc());
		}
		else {
			sv.rngmntdsc.set(SPACES);
		}
		sv.cmdate.set(racdrcvIO.getCmdate());
		sv.ctdate.set(racdrcvIO.getCtdate());
		sv.rrevdt.set(racdrcvIO.getRrevdt());
		sv.retype.set(racdrcvIO.getRetype());
		descIO.setDescitem(racdrcvIO.getRetype());
		descIO.setDesctabl(t5454);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.retypedesc.set(descIO.getLongdesc());
		}
		else {
			sv.retypedesc.set(SPACES);
		}
		sv.reasper.set(racdrcvIO.getReasper());
		sv.currcd.set(racdrcvIO.getCurrcode());
		descIO.setDescitem(racdrcvIO.getCurrcode());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currdesc.set(descIO.getLongdesc());
		}
		else {
			sv.currdesc.fill("?");
		}
		sv.raAmount.set(racdrcvIO.getRaAmount());
		sv.recovamt.set(racdrcvIO.getRecovamt());
		readRasa1300();
	}

protected void cashCall1070()
	{
		if (isEQ(wsspcomn.flag,"C")) {
			sv.calldt.set(wssplife.effdate);
			sv.callamt.set(ZERO);
			sv.callrecd.set(ZERO);
			return ;
		}
		ccalIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalIO.getParams());
			fatalError600();
		}
		sv.callrecd.set(ccalIO.getCallrecd());
		if (isEQ(wsspcomn.flag,"I")) {
			sv.callamt.set(ccalIO.getCallamt());
			sv.calldt.set(ccalIO.getCalldt());
		}
		else {
			sv.callamt.set(ZERO);
			sv.calldt.set(wssplife.effdate);
		}
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readRasa1300()
	{
		/*RASA*/
		rasaIO.setDataArea(SPACES);
		rasaIO.setRascoy(racdrcvIO.getChdrcoy());
		rasaIO.setRasnum(racdrcvIO.getRasnum());
		rasaIO.setFunction(varcom.readr);
		rasaIO.setFormat(rasarec);
		SmartFileCode.execute(appVars, rasaIO);
		if (isNE(rasaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rasaIO.getParams());
			syserrrec.statuz.set(rasaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		/*SCREENIO*/
		return ;
	}

protected void screenEdit2000()
	{
					screenIo2001();
					validate2020();
					checkForErrors2080();
				}

protected void screenIo2001()
	{
		/*    CALL 'S5465IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5465-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.callamt, ZERO)) {
			zrdecplrec.amountIn.set(sv.callamt);
			zrdecplrec.currency.set(sv.currcd);
			callRounding5000();
			if (isNE(zrdecplrec.amountOut, sv.callamt)) {
				sv.callamtErr.set(rfik);
			}
		}
		/* If we are Posting Receipts, the Sum Accumulated so*/
		/* far must be updated by the New Cash Call Amount.*/
		if (isEQ(wsspcomn.flag,"M")) {
			compute(sv.callrecd, 2).set(add(ccalIO.getCallrecd(),sv.callamt));
		}
		if (isGT(sv.callrecd,racdrcvIO.getRecovamt())
		|| isGT(sv.callamt,racdrcvIO.getRecovamt())) {
			sv.callamtErr.set(r073);
			return ;
		}
		if (isEQ(sv.callamt,0)) {
			sv.callamtErr.set(h926);
		}
		if (isNE(wsspcomn.flag,"C")) {
			return ;
		}
		if (isNE(sv.callamt,ZERO)) {
			sv.callrecd.set(sv.callamt);
		}
		if (isEQ(sv.callamt,ZERO)) {
			sv.callamtErr.set(f399);
		}
		if (isEQ(sv.callrecd,ZERO)) {
			sv.callrecdErr.set(f399);
		}
		if (isEQ(sv.calldt,99999999)) {
			sv.calldtErr.set(f399);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
			updateDatabase3001();
		}

protected void updateDatabase3001()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		|| isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"C")) {
			rlseCcal3100();
			writeNewCcal3300();
			accountingMovements3500();
		}
		else {
			if (isEQ(wsspcomn.flag,"M")) {
				rlseCcal3100();
				rewriteCcal3200();
				writeNewCcal3300();
				accountingMovements3500();
			}
		}
	}

protected void rlseCcal3100()
	{
		/*RELEASED*/
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalIO.getParams());
			syserrrec.statuz.set(ccalIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void rewriteCcal3200()
	{
		starts3201();
	}

protected void starts3201()
	{
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalIO.getParams());
			syserrrec.statuz.set(ccalIO.getStatuz());
			fatalError600();
		}
		ccalIO.setValidflag("2");
		ccalIO.setCurrto(sv.calldt);
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalIO.getParams());
			syserrrec.statuz.set(ccalIO.getStatuz());
			fatalError600();
		}
	}

protected void writeNewCcal3300()
	{
		starts3301();
	}

protected void starts3301()
	{
		ccalIO.setParams(SPACES);
		ccalIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ccalIO.setChdrnum(chdrmjaIO.getChdrnum());
		ccalIO.setLife(covrlnbIO.getLife());
		ccalIO.setCoverage(covrlnbIO.getCoverage());
		ccalIO.setRider(covrlnbIO.getRider());
		ccalIO.setPlanSuffix(racdrcvIO.getPlanSuffix());
		ccalIO.setRasnum(racdrcvIO.getRasnum());
		ccalIO.setRngmnt(racdrcvIO.getRngmnt());
		ccalIO.setCurrcode(racdrcvIO.getCurrcode());
		ccalIO.setSeqno(racdrcvIO.getSeqno());
		setPrecision(ccalIO.getTranno(), 0);
		ccalIO.setTranno(add(1,chdrmjaIO.getTranno()));
		ccalIO.setValidflag("1");
		ccalIO.setCurrto(varcom.vrcmMaxDate);
		ccalIO.setCalldt(sv.calldt);
		ccalIO.setCurrfrom(wssplife.effdate);
		ccalIO.setCallamt(sv.callamt);
		ccalIO.setCallrecd(sv.callrecd);
		ccalIO.setFormat(ccalrec);
		ccalIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ccalIO);
		if (isNE(ccalIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ccalIO.getParams());
			syserrrec.statuz.set(ccalIO.getStatuz());
			fatalError600();
		}
	}

protected void accountingMovements3500()
	{
		acmvs3501();
	}

protected void acmvs3501()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ccalIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ccalIO.getChdrcoy());
		/* MOVE T5645                  TO DESC-DESCTABL.                */
		/* MOVE WSAA-PROG              TO DESC-DESCITEM.                */
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.jrnseq.set(ZERO);
		if (isEQ(racdrcvIO.getCurrcode(),rasaIO.getCurrcode())) {
			lifacmvrec.origcurr.set(racdrcvIO.getCurrcode());
			lifacmvrec.genlcur.set(racdrcvIO.getCurrcode());
			lifacmvrec.origamt.set(ccalIO.getCallamt());
			lifacmvrec.acctamt.set(ccalIO.getCallamt());
			lifacmvrec.crate.set(1);
		}
		else {
			lifacmvrec.origcurr.set(racdrcvIO.getCurrcode());
			lifacmvrec.genlcur.set(rasaIO.getCurrcode());
			lifacmvrec.origamt.set(ccalIO.getCallamt());
			conlinkrec.amountIn.set(ccalIO.getCallamt());
			convertAmounts3900();
			lifacmvrec.acctamt.set(conlinkrec.amountOut);
			compute(lifacmvrec.crate, 9).set(div(conlinkrec.amountIn,conlinkrec.amountOut));
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(ccalIO.getChdrnum());
		lifacmvrec.rldgacct.set(ccalIO.getRasnum());
		lifacmvrec.rldgcoy.set(ccalIO.getChdrcoy());
		lifacmvrec.genlcoy.set(ccalIO.getChdrcoy());
		lifacmvrec.effdate.set(ccalIO.getCalldt());
		lifacmvrec.tranno.set(ccalIO.getTranno());
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(ccalIO.getCalldt());
		lifacmvrec.transactionTime.set(wsaaTransTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.substituteCode[1].set(SPACES);
		lifacmvrec.tranref.set(ccalIO.getChdrnum());
		/*  Create an ACMV for the Cash Call Amount*/
		if (isEQ(wsspcomn.flag,"M")) {
			modifyAcmvs3600();
		}
		else {
			createAcmvs3700();
		}
	}

protected void modifyAcmvs3600()
	{
		/*MODPOST*/
		if (isEQ(ccalIO.getCallamt(),ZERO)) {
			return ;
		}
		wsaaSub1.set(3);
		callLifacmv3800();
		wsaaSub1.set(4);
		callLifacmv3800();
		/*EXIT*/
	}

protected void createAcmvs3700()
	{
		/*MODPOST*/
		if (isEQ(ccalIO.getCallamt(),ZERO)) {
			return ;
		}
		wsaaSub1.set(1);
		callLifacmv3800();
		wsaaSub1.set(2);
		callLifacmv3800();
		/*EXIT*/
	}

protected void callLifacmv3800()
	{
		/*LIFACMCV*/
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void convertAmounts3900()
	{
		conversion3901();
	}

protected void conversion3901()
	{
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(wssplife.effdate);
		conlinkrec.currIn.set(racdrcvIO.getCurrcode());
		conlinkrec.currOut.set(rasaIO.getCurrcode());
		conlinkrec.amountIn.set(ccalIO.getCallamt());
		conlinkrec.company.set(ccalIO.getChdrcoy());
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(conlinkrec.currOut);
		callRounding5000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(wsspcomn.company);
		/* MOVE RASA-CURRCODE          TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
		/**  END OF CONFNAME ***********************************************/
	}
}
