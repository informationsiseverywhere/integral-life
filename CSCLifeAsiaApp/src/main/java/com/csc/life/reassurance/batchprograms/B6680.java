/*
 * File: B6680.java
 * Date: 29 August 2009 21:23:27
 * Author: Quipoz Limited
 * 
 * Class transformed from B6680.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.reassurance.dataaccess.AcmxTableDAM;
import com.csc.life.reassurance.dataaccess.dao.AcmxpfDAO;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;
//import com.csc.life.reassurance.dataaccess.dao.AcmxDAO;

/**
 * <pre>
 *
 *  ACMX DELETION PRIOR TO REASSURANCE STATEMENTS/PAYMENTS
 *  ======================================================
 * 
 *  OVERVIEW.
 * 
 *  This program must run before the Reassurance Statement
 *  program - (B5470).
 * 
 *  It will delete all the ACMXs for the contract range input.
 *  If no range is entered, all records will be deleted.
 * 
 *  Control totals used in this program:
 * 
 *  01 - No. of ACMX recs read
 *  02 - No. of ACMX recs deleted
 *
 *****************************************************************
 * </pre>
 */
public class B6680 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5)
			.init("B6680");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2)
			.init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	/* ERRORS */
	private String ivrm = "IVRM";
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

	private AcmxpfDAO amcxDao = getApplicationContext().getBean("acmxDAO",AcmxpfDAO.class);
	/* Reassurance Payments (Extract from ACMV) */
	private AcmxTableDAM acmxIO = new AcmxTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit2090
	}

	public B6680() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void restart0900() {
		/* RESTART */
		/* EXIT */
	}

	protected void initialise1000() {
		/* INITIALISE */
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		acmxIO.setDataArea(SPACES);
		acmxIO.setStatuz(varcom.oK);
		bupaIO.setDataArea(lsaaBuparec);
		acmxIO.setBatccoy(bsprIO.getCompany());
		acmxIO.setFunction(varcom.begnh);
		/* EXIT */
	}

	protected void readFile2000() {
	//	 ILIFE-2525 Life Batch Performance Improvement by liwei
//				try {
//					readFile2010();
//				} catch (GOTOException e) {
//				}
	}

	protected void readFile2010() {
		// ILIFE-2525 Life Batch Performance Improvement by liwei
		// SmartFileCode.execute(appVars, acmxIO);
		// if (isNE(acmxIO.getStatuz(),varcom.oK)
		// && isNE(acmxIO.getStatuz(),varcom.endp)) {
		// syserrrec.params.set(acmxIO.getParams());
		// fatalError600();
		// }
		// if (isNE(acmxIO.getBatccoy(),bsprIO.getCompany())) {
		// acmxIO.setFunction("REWRT");
		// SmartFileCode.execute(appVars, acmxIO);
		// if (isNE(acmxIO.getStatuz(),varcom.oK)) {
		// syserrrec.params.set(acmxIO.getParams());
		// fatalError600();
		// }
		// wsspEdterror.set(varcom.endp);
		// goTo(GotoLabel.exit2090);
		// }
		// if (isEQ(acmxIO.getStatuz(),varcom.endp)) {
		// wsspEdterror.set(varcom.endp);
		// goTo(GotoLabel.exit2090);
		// }
		 contotrec.totno.set(ct01);
		 contotrec.totval.set(1);
		 callContot001();
	}

	protected void edit2500() {
		/* EDIT */
		wsspEdterror.set(varcom.oK);
	}

	@SuppressWarnings("static-access")
	protected void update3000() {
		/* UPDATE */
		// acmxIO.setStatuz(varcom.oK);
		// acmxIO.setFunction(varcom.delet);
		// SmartFileCode.execute(appVars, acmxIO);
		// if (isNE(acmxIO.getStatuz(),varcom.oK)) {
		// syserrrec.params.set(acmxIO.getParams());
		// fatalError600();
		// }
		// ILIFE-2525 Life Batch Performance Improvement by liwei

		wsspEdterror.set(varcom.endp);
		/* EXIT */
	}

	protected void commit3500() {
		int count = amcxDao.deleteAcmxpfBulk(bsprIO.getCompany().toString());
		 contotrec.totno.set(ct01);
		 contotrec.totval.set(count);
		 callContot001();
		contotrec.totno.set(ct02);
		contotrec.totval.set(count);
		 callContot001();
		 count = 0;
	}

	protected void rollback3600() {
		/* ROLL */
		/* EXIT */
	}

	protected void close4000() {
		/* CLOSE-FILES */
		lsaaStatuz.set(varcom.oK);
		/* EXIT */
	}
}
