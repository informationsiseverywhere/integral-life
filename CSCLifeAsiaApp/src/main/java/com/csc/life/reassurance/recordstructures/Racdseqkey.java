package com.csc.life.reassurance.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:22
 * Description:
 * Copybook name: RACDSEQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdseqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdseqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdseqKey = new FixedLengthStringData(64).isAPartOf(racdseqFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdseqChdrcoy = new FixedLengthStringData(1).isAPartOf(racdseqKey, 0);
  	public FixedLengthStringData racdseqChdrnum = new FixedLengthStringData(8).isAPartOf(racdseqKey, 1);
  	public FixedLengthStringData racdseqLife = new FixedLengthStringData(2).isAPartOf(racdseqKey, 9);
  	public FixedLengthStringData racdseqCoverage = new FixedLengthStringData(2).isAPartOf(racdseqKey, 11);
  	public FixedLengthStringData racdseqRider = new FixedLengthStringData(2).isAPartOf(racdseqKey, 13);
  	public PackedDecimalData racdseqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdseqKey, 15);
  	public PackedDecimalData racdseqSeqno = new PackedDecimalData(2, 0).isAPartOf(racdseqKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(racdseqKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdseqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdseqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}