package com.csc.life.reassurance.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;


public class Acmvagt {
	private long uniqueNumber;
	private String rldgcoy;
	private String sacscode;
	private String rldgacct;
	private String origcurr;
	private String sacstyp;
	private String batccoy;
	private String batcbrn;
	private int batcactyr;
	private int batcactmn;
	private String batctrcde;
	private String batcbatch;
	private String rdocnum;
	private int tranno;
	private int jrnseq;
	private double origamt;
	private String tranref;
	private String trandesc;
	private double crate;
	private double acctamt;
	private String genlcoy;
	private String genlcur;
	private String glcode;
	private String glsign;
	private String postyear;
	private String postmonth;
	private int effdate;
	private double rcamt;
	private int frcdate;
	private String suprflg;
	private int trdt;
	private int trtm;
	private int userT;
	private String termid;
	private String usrprf;
	private String jobnm;
	private String datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public int getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}
	public int getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}
	public String getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(String rdocnum) {
		this.rdocnum = rdocnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(int jrnseq) {
		this.jrnseq = jrnseq;
	}
	public double getOrigamt() {
		return origamt;
	}
	public void setOrigamt(double origamt) {
		this.origamt = origamt;
	}
	public String getTranref() {
		return tranref;
	}
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}
	public String getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}
	public double getCrate() {
		return crate;
	}
	public void setCrate(double crate) {
		this.crate = crate;
	}
	public double getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(double acctamt) {
		this.acctamt = acctamt;
	}
	public String getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(String genlcoy) {
		this.genlcoy = genlcoy;
	}
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	public String getGlcode() {
		return glcode;
	}
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	public String getGlsign() {
		return glsign;
	}
	public void setGlsign(String glsign) {
		this.glsign = glsign;
	}
	public String getPostyear() {
		return postyear;
	}
	public void setPostyear(String postyear) {
		this.postyear = postyear;
	}
	public String getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(String postmonth) {
		this.postmonth = postmonth;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public double getRcamt() {
		return rcamt;
	}
	public void setRcamt(double rcamt) {
		this.rcamt = rcamt;
	}
	public int getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(int frcdate) {
		this.frcdate = frcdate;
	}
	public String getSuprflg() {
		return suprflg;
	}
	public void setSuprflg(String suprflg) {
		this.suprflg = suprflg;
	}
	public int getTrdt() {
		return trdt;
	}
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	public int getTrtm() {
		return trtm;
	}
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	public int getUserT() {
		return userT;
	}
	public void setUserT(int userT) {
		this.userT = userT;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	
	
}
