/*
 * File: Rexpupd.java
 * Date: 30 August 2009 2:12:01
 * Author: Quipoz Limited
 * 
 * Class transformed from REXPUPD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.reassurance.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.reassurance.dataaccess.LirrconTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhconTableDAM;
import com.csc.life.reassurance.recordstructures.Rexpupdrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Life Experience Update Subroutine.
*
* Overview
* ========
*
* The life reassurance experience data is stored on two files,
* LIRR and LRRH. This subroutine is called when cessions are
* activated (ACTVRES) or terminated (TRMRACD) to update these
* files with the existing in-force cession information. It is
* used even if there are no cessions for a component (i.e. the
* entire sum assured is retained) and will update both the life
* and the joint life for a joint life risk.
*
* There are tow valid functions, INCR and DECR, used to increase
* and decrease the value of reassurance respectively.
*
* Linkage Area
* ============
*
* FUNCTION       PIC X(05)
* STATUZ         PIC X(04)
* CHDRCOY        PIC X(01).
* CHDRNUM        PIC X(08).
* LIFE           PIC X(02).
* COVERAGE       PIC X(02).
* RIDER          PIC X(02).
* PLAN-SUFFIX    PIC S9(04) COMP-3.
* CLNTCOY        PIC X(01).
* L1-CLNTNUM     PIC X(08).
* L2-CLNTNUM     PIC X(08).
* RISK-CLASS     PIC X(03).
* REASSURER      PIC X(08).
* ARRANGEMENT    PIC X(04).
* CESTYPE        PIC X(01).
* CURRENCY       PIC X(03).
* SUMINS         PIC S9(11)V9(02) COMP-3.
* EFFDATE        PIC S9(08) COMP-3.
*
* These fields are all contained within REXPUPDREC.
*
*****************************************************************
* </pre>
*/
public class Rexpupd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REXPUPD";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
		/* FORMATS */
	private static final String lirrconrec = "LIRRCONREC";
	private static final String lrrhconrec = "LRRHCONREC";
		/* ERRORS */
	private static final String e049 = "E049";
	private LirrconTableDAM lirrconIO = new LirrconTableDAM();
	private LrrhconTableDAM lrrhconIO = new LrrhconTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rexpupdrec rexpupdrec = new Rexpupdrec();
	private PackedDecimalData wsaasumins = new PackedDecimalData(17, 0);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next2050
	}

	public Rexpupd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rexpupdrec.rexpupdRec = convertAndSetParam(rexpupdrec.rexpupdRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		rexpupdrec.statuz.set(varcom.oK);
		if (isNE(rexpupdrec.function, "INCR")
		&& isNE(rexpupdrec.function, "DECR")) {
			syserrrec.statuz.set(e049);
			syserr570();
		}
		if (isEQ(rexpupdrec.reassurer, SPACES)) {
			wsaaClntnum.set(rexpupdrec.l1Clntnum);
			updateLifeCoOnly1000();
			if (isNE(rexpupdrec.l2Clntnum, SPACES)) {
				wsaaClntnum.set(rexpupdrec.l2Clntnum);
				updateLifeCoOnly1000();
			}
		}
		else {
			wsaaClntnum.set(rexpupdrec.l1Clntnum);
			updateAll2000();
			if (isNE(rexpupdrec.l2Clntnum, SPACES)) {
				wsaaClntnum.set(rexpupdrec.l2Clntnum);
				updateAll2000();
			}
		}
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void updateLifeCoOnly1000()
	{
		update1010();
	}

protected void update1010()
	{
		/* In this section the life company exposure is updated for the*/
		/* lives assured and the risk class specified in the parameters.*/
		/* This section is only performed for tranches of a sum assured*/
		/* that are not reassured (i.e. no reassurer for the tranche).*/
		lrrhconIO.setParams(SPACES);
		lrrhconIO.setCompany(rexpupdrec.chdrcoy);
		lrrhconIO.setChdrnum(rexpupdrec.chdrnum);
		lrrhconIO.setLife(rexpupdrec.life);
		lrrhconIO.setCoverage(rexpupdrec.coverage);
		lrrhconIO.setRider(rexpupdrec.rider);
		lrrhconIO.setPlanSuffix(rexpupdrec.planSuffix);
		lrrhconIO.setClntpfx(fsupfxcpy.clnt);
		lrrhconIO.setClntcoy(rexpupdrec.clntcoy);
		lrrhconIO.setClntnum(wsaaClntnum);
		lrrhconIO.setLrkcls(rexpupdrec.riskClass);
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)
		&& isNE(lrrhconIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			if (isEQ(rexpupdrec.function, "DECR")) {
				return ;
			}
			else {
				writeLrrh1100();
			}
		}
		else {
			if (isNE(lrrhconIO.getTranno(), rexpupdrec.tranno)) {
				lrrhconIO.setCurrto(rexpupdrec.effdate);
				lrrhconIO.setValidflag("2");
				rewriteLrrh1200();
			}
			if (isNE(rexpupdrec.currency, lrrhconIO.getCurrency())) {
				conlinkrec.amountIn.set(rexpupdrec.sumins);
				conlinkrec.currIn.set(rexpupdrec.currency);
				conlinkrec.currOut.set(lrrhconIO.getCurrency());
				zrdecplrec.currency.set(lrrhconIO.getCurrency());
				callXcvrt3000();
				rexpupdrec.sumins.set(conlinkrec.amountOut);
			}
			if (isEQ(rexpupdrec.function, "DECR")) {
				setPrecision(lrrhconIO.getSsretn(), 2);
				lrrhconIO.setSsretn(sub(lrrhconIO.getSsretn(), rexpupdrec.sumins));
				if (isLT(lrrhconIO.getSsretn(), 0)) {
					lrrhconIO.setSsretn(0);
				}
			}
			else {
				setPrecision(lrrhconIO.getSsretn(), 2);
				lrrhconIO.setSsretn(add(lrrhconIO.getSsretn(), rexpupdrec.sumins));
			}
			if (isEQ(lrrhconIO.getTranno(), rexpupdrec.tranno)) {
				rewriteLrrh1200();
			}
			else {
				writeValidflag1Lrrh1300();
			}
		}
	}

protected void writeLrrh1100()
	{
		writr1110();
	}

protected void writr1110()
	{
		lrrhconIO.setParams(SPACES);
		lrrhconIO.setClntpfx(fsupfxcpy.clnt);
		lrrhconIO.setClntcoy(rexpupdrec.clntcoy);
		lrrhconIO.setClntnum(wsaaClntnum);
		lrrhconIO.setCompany(rexpupdrec.chdrcoy);
		lrrhconIO.setChdrnum(rexpupdrec.chdrnum);
		lrrhconIO.setLife(rexpupdrec.life);
		lrrhconIO.setCoverage(rexpupdrec.coverage);
		lrrhconIO.setRider(rexpupdrec.rider);
		lrrhconIO.setPlanSuffix(rexpupdrec.planSuffix);
		lrrhconIO.setCurrfrom(rexpupdrec.effdate);
		lrrhconIO.setCurrto(varcom.vrcmMaxDate);
		lrrhconIO.setValidflag("1");
		lrrhconIO.setLrkcls(rexpupdrec.riskClass);
		lrrhconIO.setCurrency(rexpupdrec.currency);
		lrrhconIO.setSsretn(rexpupdrec.sumins);
		lrrhconIO.setSsreast(ZERO);
		lrrhconIO.setSsreasf(ZERO);
		lrrhconIO.setTranno(rexpupdrec.tranno);
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
	}

protected void rewriteLrrh1200()
	{
		/*REWRT*/
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void writeValidflag1Lrrh1300()
	{
		/*WRITR*/
		lrrhconIO.setCurrfrom(rexpupdrec.effdate);
		lrrhconIO.setCurrto(varcom.vrcmMaxDate);
		lrrhconIO.setValidflag("1");
		lrrhconIO.setTranno(rexpupdrec.tranno);
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void updateAll2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update2010();
				case next2050: 
					next2050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update2010()
	{
		/* When a tranche of reassured risk is activated or terminated,*/
		/* this section will be performed to update both the LIRR records*/
		/* for the reassurer and arrangement and the LRRH records for the*/
		/* life company.*/
		lirrconIO.setParams(SPACES);
		lirrconIO.setCompany(rexpupdrec.chdrcoy);
		lirrconIO.setChdrnum(rexpupdrec.chdrnum);
		lirrconIO.setLife(rexpupdrec.life);
		lirrconIO.setCoverage(rexpupdrec.coverage);
		lirrconIO.setRider(rexpupdrec.rider);
		lirrconIO.setPlanSuffix(rexpupdrec.planSuffix);
		lirrconIO.setRasnum(rexpupdrec.reassurer);
		lirrconIO.setRngmnt(rexpupdrec.arrangement);
		lirrconIO.setClntpfx(fsupfxcpy.clnt);
		lirrconIO.setClntcoy(rexpupdrec.clntcoy);
		lirrconIO.setClntnum(wsaaClntnum);
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(), varcom.oK)
		&& isNE(lirrconIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
		if (isNE(lirrconIO.getStatuz(), varcom.oK)) {
			if (isEQ(rexpupdrec.function, "DECR")) {
				goTo(GotoLabel.next2050);
			}
			else {
				writeLirr2100();
			}
		}
		else {
			if (isNE(lirrconIO.getTranno(), rexpupdrec.tranno)) {
				lirrconIO.setCurrto(rexpupdrec.effdate);
				lirrconIO.setValidflag("2");
				rewriteLirr2200();
			}
			if (isNE(rexpupdrec.currency, lirrconIO.getCurrency())) {
				conlinkrec.amountIn.set(rexpupdrec.sumins);
				conlinkrec.currIn.set(rexpupdrec.currency);
				conlinkrec.currOut.set(lirrconIO.getCurrency());
				zrdecplrec.currency.set(lirrconIO.getCurrency());
				callXcvrt3000();
				rexpupdrec.sumins.set(conlinkrec.amountOut);
			}
			if (isEQ(rexpupdrec.function, "DECR")) {
				setPrecision(lirrconIO.getRaAmount(), 2);
				lirrconIO.setRaAmount(sub(lirrconIO.getRaAmount(), rexpupdrec.sumins));
				if (isLT(lirrconIO.getRaAmount(), 0)) {
					lirrconIO.setRaAmount(0);
				}
			}
			else {
				setPrecision(lirrconIO.getRaAmount(), 2);
				lirrconIO.setRaAmount(add(lirrconIO.getRaAmount(), rexpupdrec.sumins));
			}
			if (isEQ(lirrconIO.getTranno(), rexpupdrec.tranno)) {
				rewriteLirr2200();
			}
			else {
				writeValidflag1Lirr2300();
			}
		}
	}

protected void next2050()
	{
		lrrhconIO.setParams(SPACES);
		lrrhconIO.setCompany(rexpupdrec.chdrcoy);
		lrrhconIO.setChdrnum(rexpupdrec.chdrnum);
		lrrhconIO.setLife(rexpupdrec.life);
		lrrhconIO.setCoverage(rexpupdrec.coverage);
		lrrhconIO.setRider(rexpupdrec.rider);
		lrrhconIO.setPlanSuffix(rexpupdrec.planSuffix);
		lrrhconIO.setClntpfx(fsupfxcpy.clnt);
		lrrhconIO.setClntcoy(rexpupdrec.clntcoy);
		lrrhconIO.setClntnum(wsaaClntnum);
		lrrhconIO.setLrkcls(rexpupdrec.riskClass);
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)
		&& isNE(lrrhconIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			if (isEQ(rexpupdrec.function, "DECR")) {
				return ;
			}
			else {
				writeLrrh2400();
			}
		}
		else {
			if (isNE(lrrhconIO.getTranno(), rexpupdrec.tranno)) {
				lrrhconIO.setCurrto(rexpupdrec.effdate);
				lrrhconIO.setValidflag("2");
				rewriteLrrh1200();
			}
			if (isNE(rexpupdrec.currency, lrrhconIO.getCurrency())) {
				conlinkrec.amountIn.set(rexpupdrec.sumins);
				conlinkrec.currIn.set(rexpupdrec.currency);
				conlinkrec.currOut.set(lrrhconIO.getCurrency());
				zrdecplrec.currency.set(lrrhconIO.getCurrency());
				callXcvrt3000();
				rexpupdrec.sumins.set(conlinkrec.amountOut);
			}
			if (isEQ(rexpupdrec.function, "DECR")) {
				if (isEQ(rexpupdrec.cestype, "1")) {
					setPrecision(lrrhconIO.getSsreast(), 2);
					lrrhconIO.setSsreast(sub(lrrhconIO.getSsreast(), rexpupdrec.sumins));
					if (isLT(lrrhconIO.getSsreast(), 0)) {
						lrrhconIO.setSsreast(0);
					}
				}
				else {
					setPrecision(lrrhconIO.getSsreasf(), 2);
					lrrhconIO.setSsreasf(sub(lrrhconIO.getSsreasf(), rexpupdrec.sumins));
					if (isLT(lrrhconIO.getSsreasf(), 0)) {
						lrrhconIO.setSsreasf(0);
					}
				}
			}
			else {
				if (isEQ(rexpupdrec.cestype, "1")) {
					setPrecision(lrrhconIO.getSsreast(), 2);
					lrrhconIO.setSsreast(add(lrrhconIO.getSsreast(), rexpupdrec.sumins));
				}
				else {
					setPrecision(lrrhconIO.getSsreasf(), 2);
					lrrhconIO.setSsreasf(add(lrrhconIO.getSsreasf(), rexpupdrec.sumins));
				}
			}
			if (isEQ(lrrhconIO.getTranno(), rexpupdrec.tranno)) {
				rewriteLrrh1200();
			}
			else {
				writeValidflag1Lrrh1300();
			}
		}
	}

protected void writeLirr2100()
	{
		writr2110();
	}

protected void writr2110()
	{
		lirrconIO.setParams(SPACES);
		lirrconIO.setClntpfx(fsupfxcpy.clnt);
		lirrconIO.setClntcoy(rexpupdrec.clntcoy);
		lirrconIO.setClntnum(wsaaClntnum);
		lirrconIO.setCompany(rexpupdrec.chdrcoy);
		lirrconIO.setChdrnum(rexpupdrec.chdrnum);
		lirrconIO.setLife(rexpupdrec.life);
		lirrconIO.setCoverage(rexpupdrec.coverage);
		lirrconIO.setRider(rexpupdrec.rider);
		lirrconIO.setPlanSuffix(rexpupdrec.planSuffix);
		lirrconIO.setCurrfrom(rexpupdrec.effdate);
		lirrconIO.setCurrto(varcom.vrcmMaxDate);
		lirrconIO.setValidflag("1");
		lirrconIO.setRasnum(rexpupdrec.reassurer);
		lirrconIO.setRngmnt(rexpupdrec.arrangement);
		lirrconIO.setCurrency(rexpupdrec.currency);		 
		 
		wsaasumins.setRounded(rexpupdrec.sumins);
	 
		lirrconIO.setRaAmount(wsaasumins);
		lirrconIO.setTranno(rexpupdrec.tranno);
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
	}

protected void rewriteLirr2200()
	{
		/*REWRT*/
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void writeValidflag1Lirr2300()
	{
		/*WRITR*/
		lirrconIO.setCurrfrom(rexpupdrec.effdate);
		lirrconIO.setCurrto(varcom.vrcmMaxDate);
		lirrconIO.setValidflag("1");
		lirrconIO.setTranno(rexpupdrec.tranno);
		lirrconIO.setFormat(lirrconrec);
		lirrconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrconIO.getParams());
			dbError580();
		}
		/*EXIT*/
	}

protected void writeLrrh2400()
	{
		writr2410();
	}

protected void writr2410()
	{
		lrrhconIO.setParams(SPACES);
		lrrhconIO.setClntpfx(fsupfxcpy.clnt);
		lrrhconIO.setClntcoy(rexpupdrec.clntcoy);
		lrrhconIO.setClntnum(wsaaClntnum);
		lrrhconIO.setCompany(rexpupdrec.chdrcoy);
		lrrhconIO.setChdrnum(rexpupdrec.chdrnum);
		lrrhconIO.setLife(rexpupdrec.life);
		lrrhconIO.setCoverage(rexpupdrec.coverage);
		lrrhconIO.setRider(rexpupdrec.rider);
		lrrhconIO.setPlanSuffix(rexpupdrec.planSuffix);
		lrrhconIO.setCurrfrom(rexpupdrec.effdate);
		lrrhconIO.setCurrto(varcom.vrcmMaxDate);
		lrrhconIO.setValidflag("1");
		lrrhconIO.setLrkcls(rexpupdrec.riskClass);
		lrrhconIO.setCurrency(rexpupdrec.currency);
		lrrhconIO.setTranno(rexpupdrec.tranno);
		lrrhconIO.setSsretn(ZERO);
		if (isEQ(rexpupdrec.cestype, "1")) {
			lrrhconIO.setSsreast(rexpupdrec.sumins);
			lrrhconIO.setSsreasf(0);
		}
		else {
			lrrhconIO.setSsreasf(rexpupdrec.sumins);
			lrrhconIO.setSsreast(0);
		}
		lrrhconIO.setFormat(lrrhconrec);
		lrrhconIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhconIO.getParams());
			dbError580();
		}
	}

protected void callXcvrt3000()
	{
		call3010();
	}

protected void call3010()
	{
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.cashdate.set(rexpupdrec.effdate);
		conlinkrec.company.set(rexpupdrec.chdrcoy);
		/* MOVE 'SURR'                 TO CLNK-FUNCTION.                */
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserr570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rexpupdrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		rexpupdrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(rexpupdrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(rexpupdrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A000-EXIT*/
	}
}
