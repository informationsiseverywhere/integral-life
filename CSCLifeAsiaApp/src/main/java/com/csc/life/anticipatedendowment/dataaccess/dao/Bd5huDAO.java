package com.csc.life.anticipatedendowment.dataaccess.dao;

import java.util.List;

import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Bd5huDAO extends BaseDAO<Rgpdetpf>{
	public List<Rgpdetpf> loadDataByBatch(int BatchID);
	public int populateBd5huTemp(String coy, String effDate, int batchExtractSize);
}
