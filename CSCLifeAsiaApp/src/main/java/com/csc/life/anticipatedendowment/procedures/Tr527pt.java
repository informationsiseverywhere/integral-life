/*
 * File: Tr527pt.java
 * Date: 30 August 2009 2:42:51
 * Author: Quipoz Limited
 * 
 * Class transformed from TR527PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.anticipatedendowment.tablestructures.Tr527rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR527.
*
*
*****************************************************************
* </pre>
*/
public class Tr527pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Cash Manipulation Option Subroutines            SR527");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(41);
	private FixedLengthStringData filler7 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Options for manipulating Cash Accounts:");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(71);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine004, 9, FILLER).init("Mandatory Options :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 31);
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine004, 41);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(71);
	private FixedLengthStringData filler11 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 31);
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine005, 41);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(71);
	private FixedLengthStringData filler13 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 31);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 41);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(71);
	private FixedLengthStringData filler15 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 31);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine007, 41);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(72);
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine008, 8, FILLER).init("----------------------------------------------------------------");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(71);
	private FixedLengthStringData filler19 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine009, 9, FILLER).init("Default Option    :");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 31);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 41);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(71);
	private FixedLengthStringData filler22 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine010, 9, FILLER).init("Other Options     :");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 31);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 41);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(71);
	private FixedLengthStringData filler25 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 31);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 41);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(71);
	private FixedLengthStringData filler27 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 31);
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 41);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(71);
	private FixedLengthStringData filler29 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 31);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 41);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(71);
	private FixedLengthStringData filler31 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 31);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine014, 41);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(71);
	private FixedLengthStringData filler33 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 31);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine015, 41);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(71);
	private FixedLengthStringData filler35 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 31);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine016, 41);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(71);
	private FixedLengthStringData filler37 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 31);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine017, 41);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(71);
	private FixedLengthStringData filler39 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine018, 31);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine018, 41);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr527rec tr527rec = new Tr527rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr527pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr527rec.tr527Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tr527rec.payOption01);
		fieldNo006.set(tr527rec.pymdesc01);
		fieldNo007.set(tr527rec.payOption02);
		fieldNo008.set(tr527rec.pymdesc02);
		fieldNo009.set(tr527rec.payOption03);
		fieldNo010.set(tr527rec.pymdesc03);
		fieldNo011.set(tr527rec.payOption04);
		fieldNo012.set(tr527rec.pymdesc04);
		fieldNo013.set(tr527rec.payOption05);
		fieldNo014.set(tr527rec.pymdesc05);
		fieldNo015.set(tr527rec.payOption06);
		fieldNo016.set(tr527rec.pymdesc06);
		fieldNo017.set(tr527rec.payOption07);
		fieldNo018.set(tr527rec.pymdesc07);
		fieldNo019.set(tr527rec.payOption08);
		fieldNo020.set(tr527rec.pymdesc08);
		fieldNo021.set(tr527rec.payOption09);
		fieldNo022.set(tr527rec.pymdesc09);
		fieldNo023.set(tr527rec.payOption10);
		fieldNo024.set(tr527rec.pymdesc10);
		fieldNo025.set(tr527rec.payOption11);
		fieldNo026.set(tr527rec.pymdesc11);
		fieldNo027.set(tr527rec.payOption12);
		fieldNo028.set(tr527rec.pymdesc12);
		fieldNo029.set(tr527rec.payOption13);
		fieldNo030.set(tr527rec.pymdesc13);
		fieldNo031.set(tr527rec.payOption14);
		fieldNo032.set(tr527rec.pymdesc14);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
