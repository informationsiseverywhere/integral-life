package com.csc.life.anticipatedendowment.dataaccess.dao;


import java.util.List;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;

public interface Bd5ksTempDAO extends BaseDAO<Object>{
	public boolean deleteBd5ksTempAllRecords();
	public int insertBd5ksTempRecords(int batchExtractSize,String loancoy, int effDate);
	public List<Zraepf> loadDataByNEWINTCP(int minRecord);
}
