package com.csc.life.anticipatedendowment.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR527
 * @version 1.0 generated on 30/08/09 07:18
 * @author Quipoz
 */
public class Sr527ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1088);
	public FixedLengthStringData dataFields = new FixedLengthStringData(576).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData pymdescs = new FixedLengthStringData(420).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] pymdesc = FLSArrayPartOfStructure(14, 30, pymdescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(420).isAPartOf(pymdescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData pymdesc01 = DD.pymdesc.copy().isAPartOf(filler,0);
	public FixedLengthStringData pymdesc02 = DD.pymdesc.copy().isAPartOf(filler,30);
	public FixedLengthStringData pymdesc03 = DD.pymdesc.copy().isAPartOf(filler,60);
	public FixedLengthStringData pymdesc04 = DD.pymdesc.copy().isAPartOf(filler,90);
	public FixedLengthStringData pymdesc05 = DD.pymdesc.copy().isAPartOf(filler,120);
	public FixedLengthStringData pymdesc06 = DD.pymdesc.copy().isAPartOf(filler,150);
	public FixedLengthStringData pymdesc07 = DD.pymdesc.copy().isAPartOf(filler,180);
	public FixedLengthStringData pymdesc08 = DD.pymdesc.copy().isAPartOf(filler,210);
	public FixedLengthStringData pymdesc09 = DD.pymdesc.copy().isAPartOf(filler,240);
	public FixedLengthStringData pymdesc10 = DD.pymdesc.copy().isAPartOf(filler,270);
	public FixedLengthStringData pymdesc11 = DD.pymdesc.copy().isAPartOf(filler,300);
	public FixedLengthStringData pymdesc12 = DD.pymdesc.copy().isAPartOf(filler,330);
	public FixedLengthStringData pymdesc13 = DD.pymdesc.copy().isAPartOf(filler,360);
	public FixedLengthStringData pymdesc14 = DD.pymdesc.copy().isAPartOf(filler,390);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,459);
	public FixedLengthStringData payOptions = new FixedLengthStringData(112).isAPartOf(dataFields, 464);
	public FixedLengthStringData[] payOption = FLSArrayPartOfStructure(14, 8, payOptions, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(112).isAPartOf(payOptions, 0, FILLER_REDEFINE);
	public FixedLengthStringData payOption01 = DD.zrpayopt.copy().isAPartOf(filler1,0);
	public FixedLengthStringData payOption02 = DD.zrpayopt.copy().isAPartOf(filler1,8);
	public FixedLengthStringData payOption03 = DD.zrpayopt.copy().isAPartOf(filler1,16);
	public FixedLengthStringData payOption04 = DD.zrpayopt.copy().isAPartOf(filler1,24);
	public FixedLengthStringData payOption05 = DD.zrpayopt.copy().isAPartOf(filler1,32);
	public FixedLengthStringData payOption06 = DD.zrpayopt.copy().isAPartOf(filler1,40);
	public FixedLengthStringData payOption07 = DD.zrpayopt.copy().isAPartOf(filler1,48);
	public FixedLengthStringData payOption08 = DD.zrpayopt.copy().isAPartOf(filler1,56);
	public FixedLengthStringData payOption09 = DD.zrpayopt.copy().isAPartOf(filler1,64);
	public FixedLengthStringData payOption10 = DD.zrpayopt.copy().isAPartOf(filler1,72);
	public FixedLengthStringData payOption11 = DD.zrpayopt.copy().isAPartOf(filler1,80);
	public FixedLengthStringData payOption12 = DD.zrpayopt.copy().isAPartOf(filler1,88);
	public FixedLengthStringData payOption13 = DD.zrpayopt.copy().isAPartOf(filler1,96);
	public FixedLengthStringData payOption14 = DD.zrpayopt.copy().isAPartOf(filler1,104);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(128).isAPartOf(dataArea, 576);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData pymdescsErr = new FixedLengthStringData(56).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] pymdescErr = FLSArrayPartOfStructure(14, 4, pymdescsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(56).isAPartOf(pymdescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pymdesc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData pymdesc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData pymdesc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData pymdesc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData pymdesc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData pymdesc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData pymdesc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData pymdesc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData pymdesc09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData pymdesc10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData pymdesc11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData pymdesc12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData pymdesc13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
	public FixedLengthStringData pymdesc14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData zrpayoptsErr = new FixedLengthStringData(56).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] zrpayoptErr = FLSArrayPartOfStructure(14, 4, zrpayoptsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(56).isAPartOf(zrpayoptsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrpayopt01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zrpayopt02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zrpayopt03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData zrpayopt04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData zrpayopt05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData zrpayopt06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData zrpayopt07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData zrpayopt08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData zrpayopt09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData zrpayopt10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData zrpayopt11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData zrpayopt12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData zrpayopt13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData zrpayopt14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(384).isAPartOf(dataArea, 704);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData pymdescsOut = new FixedLengthStringData(168).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] pymdescOut = FLSArrayPartOfStructure(14, 12, pymdescsOut, 0);
	public FixedLengthStringData[][] pymdescO = FLSDArrayPartOfArrayStructure(12, 1, pymdescOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(168).isAPartOf(pymdescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pymdesc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] pymdesc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] pymdesc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] pymdesc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] pymdesc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] pymdesc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] pymdesc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] pymdesc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] pymdesc09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] pymdesc10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] pymdesc11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] pymdesc12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] pymdesc13Out = FLSArrayPartOfStructure(12, 1, filler4, 144);
	public FixedLengthStringData[] pymdesc14Out = FLSArrayPartOfStructure(12, 1, filler4, 156);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData zrpayoptsOut = new FixedLengthStringData(168).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] zrpayoptOut = FLSArrayPartOfStructure(14, 12, zrpayoptsOut, 0);
	public FixedLengthStringData[][] zrpayoptO = FLSDArrayPartOfArrayStructure(12, 1, zrpayoptOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(168).isAPartOf(zrpayoptsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrpayopt01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] zrpayopt02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] zrpayopt03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] zrpayopt04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] zrpayopt05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] zrpayopt06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] zrpayopt07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] zrpayopt08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] zrpayopt09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] zrpayopt10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] zrpayopt11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] zrpayopt12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] zrpayopt13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] zrpayopt14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr527screenWritten = new LongData(0);
	public LongData Sr527protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr527ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zrpayopt01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt13Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrpayopt14Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, payOption01, pymdesc01, payOption02, pymdesc02, payOption03, pymdesc03, payOption04, pymdesc04, payOption05, pymdesc05, payOption06, pymdesc06, payOption07, pymdesc07, payOption08, pymdesc08, payOption09, pymdesc09, payOption10, pymdesc10, payOption11, pymdesc11, payOption12, pymdesc12, payOption13, pymdesc13, payOption14, pymdesc14};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, zrpayopt01Out, pymdesc01Out, zrpayopt02Out, pymdesc02Out, zrpayopt03Out, pymdesc03Out, zrpayopt04Out, pymdesc04Out, zrpayopt05Out, pymdesc05Out, zrpayopt06Out, pymdesc06Out, zrpayopt07Out, pymdesc07Out, zrpayopt08Out, pymdesc08Out, zrpayopt09Out, pymdesc09Out, zrpayopt10Out, pymdesc10Out, zrpayopt11Out, pymdesc11Out, zrpayopt12Out, pymdesc12Out, zrpayopt13Out, pymdesc13Out, zrpayopt14Out, pymdesc14Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, zrpayopt01Err, pymdesc01Err, zrpayopt02Err, pymdesc02Err, zrpayopt03Err, pymdesc03Err, zrpayopt04Err, pymdesc04Err, zrpayopt05Err, pymdesc05Err, zrpayopt06Err, pymdesc06Err, zrpayopt07Err, pymdesc07Err, zrpayopt08Err, pymdesc08Err, zrpayopt09Err, pymdesc09Err, zrpayopt10Err, pymdesc10Err, zrpayopt11Err, pymdesc11Err, zrpayopt12Err, pymdesc12Err, zrpayopt13Err, pymdesc13Err, zrpayopt14Err, pymdesc14Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr527screen.class;
		protectRecord = Sr527protect.class;
	}

}
