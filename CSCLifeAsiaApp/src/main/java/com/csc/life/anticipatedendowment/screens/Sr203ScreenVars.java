package com.csc.life.anticipatedendowment.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import java.util.List;
import java.util.Map;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR203
 * @version 1.0 generated on 30/08/09 07:08
 * @author Quipoz
 */
public class Sr203ScreenVars extends SmartVarModel { 


	//ILIFE-4361 starts
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	//ILIFE-4361 ends
	public FixedLengthStringData letterPrintFlag = DD.letprtflag.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tcsd = DD.tcsd.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData totalamts = new FixedLengthStringData(36).isAPartOf(dataFields, 2);
	public ZonedDecimalData[] totalamt = ZDArrayPartOfStructure(2, 18, 2, totalamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(totalamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData totalamt01 = DD.totalamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData totalamt02 = DD.totalamt.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData trandatex = DD.trandatex.copyToZonedDecimal().isAPartOf(dataFields,38);
	public FixedLengthStringData trandsc = DD.trandsc.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData zrnofrecs = new FixedLengthStringData(6).isAPartOf(dataFields, 76);
	public ZonedDecimalData[] zrnofrec = ZDArrayPartOfStructure(2, 3, 0, zrnofrecs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(zrnofrecs, 0, FILLER_REDEFINE);
	public ZonedDecimalData zrnofrec01 = DD.zrnofrec.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData zrnofrec02 = DD.zrnofrec.copyToZonedDecimal().isAPartOf(filler1,3);
	public FixedLengthStringData sacsflag = DD.sacsflag.copy().isAPartOf(dataFields, 82);
	//ILIFE-4361 
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData letprtflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData tcsdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData totalamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] totalamtErr = FLSArrayPartOfStructure(2, 4, totalamtsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(totalamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData totalamt01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData totalamt02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData trandatexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData trandscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zrnofrecsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] zrnofrecErr = FLSArrayPartOfStructure(2, 4, zrnofrecsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(zrnofrecsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrnofrec01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zrnofrec02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData sacsflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	//ILIFE-4361 
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] letprtflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] tcsdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData totalamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] totalamtOut = FLSArrayPartOfStructure(2, 12, totalamtsOut, 0);
	public FixedLengthStringData[][] totalamtO = FLSDArrayPartOfArrayStructure(12, 1, totalamtOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(totalamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] totalamt01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] totalamt02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] trandatexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] trandscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData zrnofrecsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] zrnofrecOut = FLSArrayPartOfStructure(2, 12, zrnofrecsOut, 0);
	public FixedLengthStringData[][] zrnofrecO = FLSDArrayPartOfArrayStructure(12, 1, zrnofrecOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(zrnofrecsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrnofrec01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] zrnofrec02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] sacsflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);

	//ILIFE-4361 starts
	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	//ILIFE-4361 ends
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData curramt = DD.curramt.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public FixedLengthStringData hlifcnum = DD.hlifcnum.copy().isAPartOf(subfileFields,26);
	public ZonedDecimalData tranamt = DD.tranamt.copyToZonedDecimal().isAPartOf(subfileFields,34);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(subfileFields,51);
	public FixedLengthStringData sacstypw = DD.sacstypw.copy().isAPartOf(subfileFields,53);
	//ILIFE-4361 
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData curramtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData hlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData tranamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData sacscodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData sacstypwErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	//ILIFE-4361 
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea,getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] curramtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] hlifcnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] tranamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] sacscodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] sacstypwOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	//ILIFE-4361
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData trandatexDisp = new FixedLengthStringData(10);
	
	public Map<String, List<String>> sacscodeMap = null;
	public Map sacstypwMap 	= null;
	
	public LongData Sr203screensflWritten = new LongData(0);
	public LongData Sr203screenctlWritten = new LongData(0);
	public LongData Sr203screenWritten = new LongData(0);
	public LongData Sr203protectWritten = new LongData(0);
	public GeneralTable sr203screensfl = new GeneralTable(AppVars.getInstance());
	
	//ILIFE-4361 starts
	public static int[] screenSflPfInds = new int[] {6, 7, 8, 9, 10}; 
	public static int[] screenSflAffectedInds = new int[] {6, 7, 8, 9, 10};
	//ILIFE-4361 ends

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr203screensfl;
	}

	public Sr203ScreenVars() {
		super();
		initialiseScreenVars();
	}

	//ILIFE-4361 starts
	{
		screenIndicArea = DD.indicarea.copy();
	}
	//ILIFE-4361 ends
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tranamtOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trandatexOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrnofrec01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totalamt01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tcsdOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(letprtflagOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscodeOut,new String[] {"08",null, "-08", "09", null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstypwOut,new String[] {"10",null, "-10", "09", null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacsflagOut,new String[] {null,null, null, "09", null, null, null, null, null, null, null, null});
		//ILIFE-4361 starts
		screenSflFields = getscreenLSflFields();
		screenSflOutFields = getscreenSflOutFields();
		screenSflErrFields = getscreenSflErrFields();
		//ILIFE-4361 ends
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		//ILIFE-4361 starts
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		//ILIFE-4361 ends

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr203screen.class;
		screenSflRecord = Sr203screensfl.class;
		screenCtlRecord = Sr203screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr203protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr203screenctl.lrec.pageSubfile);
	}
	
	/**
	 * ILIFE-4361 
	 * @return
	 */
	public int getDataAreaSize()
	{
		return 227;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getDataFieldsSize()
	{
		return 83;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getErrorIndicatorSize()
	{
		return 36;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getOutputFieldSize()
	{
		return 108;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getSubfileAreaSize()
	{
		return 153;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getSubfileFieldsSize()
	{
		return 55;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getErrorSubfileSize()
	{
		return 24;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public int getOutputSubfileSize()
	{
		return 72;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {trandsc, trandatex, zrnofrec01, totalamt01, tcsd, letterPrintFlag, zrnofrec02, totalamt02, sacsflag};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {trandscOut, trandatexOut, zrnofrec01Out, totalamt01Out, tcsdOut, letprtflagOut, zrnofrec02Out, totalamt02Out, sacsflagOut};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {trandscErr, trandatexErr, zrnofrec01Err, totalamt01Err, tcsdErr, letprtflagErr, zrnofrec02Err, totalamt02Err, sacsflagErr};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {hlifcnum, curramt, chdrnum, tranamt, sacscode, sacstypw};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {hlifcnumOut, curramtOut, chdrnumOut, tranamtOut, sacscodeOut, sacstypwOut};
	}
	
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {hlifcnumErr, curramtErr, chdrnumErr, tranamtErr, sacscodeErr, sacstypwErr};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {trandatex};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {trandatexDisp};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {trandatexErr};
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public static int[] getScreenSflPfInds()
	{
		return screenSflPfInds;
	}
	
	/**
	 * ILIFE-4361
	 * 
	 * @return
	 */
	public static int[] getScreenSflAffectedInds()
	{
		return screenSflAffectedInds;
	}
	
	
}
