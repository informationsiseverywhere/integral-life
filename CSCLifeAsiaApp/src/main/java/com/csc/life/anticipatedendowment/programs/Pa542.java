package com.csc.life.anticipatedendowment.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.common.constants.SmartTableConstants;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.anticipatedendowment.procedures.Zrgetusr;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.life.anticipatedendowment.recordstructures.Zrgtusrrec;
import com.csc.life.anticipatedendowment.screens.Sa542ScreenVars;
import com.csc.life.anticipatedendowment.tablestructures.Tr528rec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;


import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;

import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/**
 * <pre>
 * 
 * This program is part of POS042 Surivival Benefit Withdrawal.
 * Partial withdrawal is not allowed.
 * There is no need to consider repayment of loan in the policy.
 * Payee & Bank Account should be defaulted to Policy Payee, 
 * and user can modify.Only Payment Method field is editable 
 * while the others are non-editable.
 * 
 * </pre>
 */
public class Pa542 extends ScreenProgCS{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA542");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private static final String t5688 = "T5688";


	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Tr528rec tr528rec = new Tr528rec();
	private Wssplife wssplife = new Wssplife();
	private Sa542ScreenVars sv = ScreenProgram.getScreenVars(Sa542ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
//	private FormatsInner formatsInner = new FormatsInner();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private final DescDAO descDao = getApplicationContext().getBean("descDAO",DescDAO.class);
	
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private Ptrnpf ptrnpf = null;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",PtrnpfDAO.class);
	private Batckey wsaaBatckey1 = new Batckey();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Td5h7rec td5h7rec = new Td5h7rec();
	private Sftlockrec sftlockrec = new Sftlockrec(); 
	private ZraepfDAO zraeDAO = getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf(); 
	private List<Zraepf> zraepfList; 
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private T5645rec t5645rec = new T5645rec();
	private int chdrTranno;
	private List<Chdrpf> chdrBulkUpdtList; 
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	
	private Regpsubrec regpsubrec3 = new Regpsubrec();
	private T5688rec t5688rec = new T5688rec();
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
//	private String regtvstrec = "REGTVSTREC"; 
//	private RgpdetpfDAO rgpdetpfDAO = getApplicationContext().getBean("rgpdetpfDAO", RgpdetpfDAO.class);//ILIFE-3955
//	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String zraerec = "ZRAEREC";
	private static final String BNKDCRDT = "BNKDCRDT";
	private static final String CHEQUE = "CHEQUE";
	private Zrcshoprec zrcshoprec = new Zrcshoprec(); 
	
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private ZonedDecimalData wsaaCurrFrom = new ZonedDecimalData(8);
	private static final String t3629 = "T3629";
	private FixedLengthStringData wsRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsRldgacct, 0);
	private FixedLengthStringData wsRldgLife = new FixedLengthStringData(2).isAPartOf(wsRldgacct, 8);
	private FixedLengthStringData wsRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsRldgacct, 10);
	private FixedLengthStringData wsRldgRider = new FixedLengthStringData(2).isAPartOf(wsRldgacct, 12);
	private FixedLengthStringData wsRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsRldgacct, 14);
	private Zrgtusrrec zrgtusrrec = new Zrgtusrrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Subcoderec subcoderec = new Subcoderec();
	private T3629rec t3629rec = new T3629rec();
	private ZonedDecimalData wsaaNetPymt = new ZonedDecimalData(17, 2);
	boolean cmrpy005Permission = false; 
	private ZonedDecimalData wsaaConvertedNet = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAdjamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaConvertedAdj = new ZonedDecimalData(17, 2);
	private static final String t6694 = "T6694";
	private T6694rec t6694rec = new T6694rec();
	private static final String tr528 = "TR528";

	
	
	
	private int wsaaSwitch = 0;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit1090, checkDestination2060, checkForErrors2080, exit2090, exit4090
	}

	public Pa542() {
		super();
		screenVars = sv;
		new ScreenModel("Sa542", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams,
				parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea,
				parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams,
				parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea,
				parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	 * ***   Initialise fields for showing on screen.
	 * </pre>
	 */
	protected void initialise1000() {
		try {
			initialise1010();
		} catch (GOTOException e) {
		}
	}

	protected void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			//			goTo(GotoLabel.exit1090);
			return;
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.refcode.set(SPACES);
		wsaaSwitch = 0;
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(chdrpf == null ){
			syserrrec.params.set(wsspcomn.chdrChdrcoy.toString()+wsspcomn.chdrChdrnum.toString());
			fatalError600();
		}

		sv.ownername.set(plainname(chdrpf.getCownnum()));//IJTI-1410
		zraepf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410

		List<Zraepf> zraepfList;
		List<String> chdrnumList = new ArrayList<String>();
		chdrnumList.add(chdrpf.getChdrnum());//IJTI-1410
		zraepfList =  zraeDAO.searchZraepfRecord(chdrnumList, chdrpf.getChdrcoy().toString());

		if(zraepfList!= null && !zraepfList.isEmpty()){
			sv.rgpymop.set(zraepf.getWdrgpymop());
			keepZraeIO(zraepfList);
			checkBankDetails(zraepfList);
		}

		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(new BigDecimal(getCurrentBusinessDate()));
		itempf.setItmto(new BigDecimal(getCurrentBusinessDate()));
		itempf.setItemtabl("TD5H7");
		itempf.setItemitem(chdrpf.getCnttype());//IJTI-1410
		itempf.setValidflag("1");
		List<Itempf> itempfList  = itemDAO.findByItemDates(itempf);
		if(itempfList!=null && !itempfList.isEmpty()){
			td5h7rec.td5h7Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
		}
		sv.intRate.set(td5h7rec.intrate);
		sv.intcalfreq.set(td5h7rec.intcalfreq);
		sv.intcapfreq.set(td5h7rec.intcapfreq);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.occdate.set(chdrpf.getOccdate());
		sv.currcd.set(chdrpf.getCntcurr());
		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.register.set(chdrpf.getReg());
		sv.chdrstatus.set(descDao.getdescData("IT", "T3623",chdrpf.getStatcode(),wsspcomn.company.toString(), wsspcomn.language.toString()).getLongdesc());//IJTI-1410
		sv.premstatus.set(descDao.getdescData("IT", "T3588",chdrpf.getPstcde(),wsspcomn.company.toString(), wsspcomn.language.toString()).getShortdesc());//IJTI-1410
		sv.ctypedes.set(descDao.getdescData("IT", t5688,chdrpf.getCnttype(), wsspcomn.company.toString(),wsspcomn.language.toString()).getShortdesc());//IJTI-1410
		sv.lifcnum.set(chdrpf.getCownnum());
		sv.linsname.setLeft(plainname(chdrpf.getCownnum()).toString());
		chdrTranno = chdrpf.getTranno() + 1; 

		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645","PA542");
		if(itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			acblenqListLPAE = acblpfDAO.getAcblenqRecord(
					wsspcomn.company.toString(), StringUtils.rightPad(chdrpf.getChdrnum(), 16),t5645rec.sacscode[1].toString(),t5645rec.sacstype[1].toString());//IJTI-1410

			acblenqListLPAS =acblpfDAO.getAcblenqRecord(
					wsspcomn.company.toString(), StringUtils.rightPad(chdrpf.getChdrnum(), 16),t5645rec.sacscode[2].toString(),t5645rec.sacstype[2].toString());//IJTI-1410

			if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
				accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
				accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);		            

			}
			if (acblenqListLPAS != null &&    !acblenqListLPAS.isEmpty()) {
				accAmtLPAS =  acblenqListLPAS.get(0).getSacscurbal();				
				accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
			}
			sv.accamnt.set(add(accAmtLPAE,accAmtLPAS));
		} else {
			syserrrec.params.set("t5645:" + wsaaProg);
			fatalError600();
		}
	}
	protected void checkBankDetails(List<Zraepf> zraepfList){

		/* If Bank Account Details already exists for this record,*/
		/* then put an '+' indicator in the option switching*/
		/* otherwise put spaces there.*/
		if(zraepfList!= null && !zraepfList.isEmpty()){
			for(Zraepf zraepf1 : zraepfList){
				if (isEQ(zraepf1.getWdbankkey(), SPACES)
						&& isEQ(zraepf1.getWdbankacckey(), SPACES)) {
					sv.ddind.set(SPACES);
				}
				else {
					sv.ddind.set("+");
				}
			}
		}

	}
	protected StringBuilder plainname(String clntnum) {
		/* PLAIN-100 */
		clntpf = clntpfDAO.findClientByClntnum(clntnum);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
		stringVariable1.append(", ");
		stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
		return stringVariable1 ;
	}

	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 *     Retrieve screen fields and exit.
	 * </pre>
	 */
	protected void preScreenEdit() {
		preStart();
	}

	protected void preStart() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
	}

	protected void screenEdit2000() {
		try {
			screenIo2010();
			validate2020();					
			checkBankDetails2035();
			checkForErrors();
		} 
		catch (GOTOException e) {

		}					
	}

	protected void screenIo2010() {

		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			checkForErrors();
		}
		if (isNE(scrnparams.statuz, varcom.oK)) {	
			wsspcomn.edterror.set("Y");
			if (isNE(scrnparams.statuz, varcom.calc)) {
				scrnparams.errorCode.set(errorsInner.curs);
				checkForErrors();
			}
		}
		wsspcomn.chdrCownnum.set(chdrpf.getCownnum());//IJTI-1410
		wsspcomn.chdrClntpfx.set("CN");
		wsspcomn.chdrClntcoy.set(wsspcomn.fsuco);
	}

	protected void validate2020() {
		if(isEQ(wsspcomn.sbmaction,"B")){
			checkPayment2030();
			if ((sv.rgpymop.toString().equalsIgnoreCase(BNKDCRDT))
					|| (sv.rgpymop.toString().trim().equalsIgnoreCase(CHEQUE))) {
			} else {
				sv.ddindErr.set(errorsInner.rrfp);
				checkForErrors();
				return;
			}


			/* Validate fields. */

			if ((isNE(sv.ddind, "+") && isNE(sv.ddind, "X") && isNE(sv.ddind,
					SPACES))) {
				sv.ddindErr.set(errorsInner.h118);
				wsspcomn.edterror.set("Y");
			}			
		}
		/*if (isEQ(wsspcomn.flag, "I")) {
			if ((isEQ(sv.ddind, "X") && isEQ(zraeIO.getBankkey(), SPACES))) {
				sv.ddindErr.set(errorsInner.e493);
				wsspcomn.edterror.set("Y");
			}
			checkForErrors();
		}*/
	}
	
	protected void checkPayment2030() {

		if (isEQ(sv.rgpymop, SPACES) &&  isEQ(wsspcomn.sbmaction,"B")) {
			sv.rgpymopErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			checkForErrors();
		}
	}

	protected void checkBankDetails2035(){

		Itempf itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(sv.rgpymop.toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_TR528);
		List<Itempf> itempfList = itemDAO.findByItemSeq(itempf);
		
		if(itempfList!=null && !itempfList.isEmpty()){
			tr528rec.tr528Rec.set(StringUtil.rawToString( itempfList.get(0).getGenarea()));
		}
		
		setBankIndicator2200();
	}

	protected void checkForErrors() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			 goTo(GotoLabel.exit2090);
			//return;
		}
	}


	protected void setBankIndicator2200() {		
		if (isNE(sv.ddindErr, SPACES)) {
			return;
		}
		if (isEQ(tr528rec.bankaccreq, "Y")) {
			if ((isEQ(zraeIO.getBankkey(), SPACES))
					|| (isEQ(zraeIO.getBankacckey(), SPACES))) {

				sv.ddind.set("X");
				wsspcomn.flag.set(SPACES);

			} else {
				if (isNE(sv.ddind, "X")) {
					sv.ddind.set("+");
				}
			}
		} else {

			if (isEQ(sv.ddind, "X")) {
				sv.ddindErr.set(errorsInner.e570);
				sv.ddind.set(SPACES);
			} else {
				zraeIO.setBankkey(SPACES);
				zraeIO.setBankacckey(SPACES);
				sv.ddind.set(SPACES);
			}
		} 
	}


	protected void update3000() {
		/* UPDATE-DATABASE */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return;
		}
		if (isEQ(sv.ddind, "X")) {
			return;
		}
		if(isEQ(wsspcomn.sbmaction,"B")){
			updateZrae3100();
		}
		
	}

	protected void updateZrae3100() {
		
		checkPopup();
		if(isEQ(sv.ansrepy, "N") || isEQ(sv.ansrepy, "X") || isEQ(sv.ansrepy, SPACES)) {
			return;
		}
		update3110();		 
	}

	protected void checkPopup() {				
			if(isEQ(wsspcomn.sbmaction , "B"))
				sv.refcode.set("1");
	}	

	protected void update3110() {		
		zraepfList = new ArrayList<>();
		updateZraepf();
	    writePtrn3900();
	    updateChdrRecord3020(); 
	    postings360();
	    descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	    if(sv.rgpymop.toString().trim().equalsIgnoreCase(BNKDCRDT)){
	    	if(zraepfList!= null && !zraepfList.isEmpty()){
	    		zraepf = zraepfList.get(0);
	    		wsaaNetPymt.set(sv.accamnt);	    		
	    		currencyCheck211(zraepf);	
	    		callPayreq(zraepf);
	    	}
	    } 
	    
	}
	

protected void callPayreq(Zraepf zraepf)
	{
	/* Setup the linkage variables before calling the Cash Option*/
	/* sub-routine.*/
	/* Read table T3629 in order to obtain the bankcode            *   */
	itempf = new Itempf();
    itempf.setItempfx("IT");
    itempf.setItemcoy(zraepf.getChdrcoy());
    itempf.setItemtabl(t3629);
	itempf.setItemitem(zraepf.getPaycurr());
		
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	
	if (itempf == null) {
		syserrrec.params.set(t3629 + zraepf.getPaycurr());//IJTI-1410
		fatalError600();
	}
	else{
		t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	itempf = new Itempf();
    itempf.setItempfx("IT");
    itempf.setItemcoy(zraepf.getChdrcoy());
    itempf.setItemtabl(tr528);
	itempf.setItemitem(zraepf.getWdrgpymop());
		
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	
	if (itempf == null) {
		syserrrec.params.set(tr528 + zraepf.getWdrgpymop());//IJTI-1410
		fatalError600();
	}
	else{
		tr528rec.tr528Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	zrcshoprec.rec.set(SPACES);
	zrcshoprec.bankcode.set(t3629rec.bankcode);
	zraeIO.setFunction(varcom.retrv);
	SmartFileCode.execute(appVars, zraeIO);
	if (isNE(zraeIO.getStatuz(), varcom.oK)) {
		regpsubrec3.subStatuz.set(zraeIO.getStatuz());
		return ;
	}
	if (isNE(zraeIO.getBankkey(), SPACES)) {
		zrcshoprec.bankkey.set(zraeIO.getBankkey());
		zrcshoprec.bankacckey.set(zraeIO.getBankacckey());
	}
	zrcshoprec.clntcoy.set(wsspcomn.fsuco);
	zrcshoprec.clntnum.set(zraepf.getPayclt());
	zrcshoprec.paycurr.set(zraepf.getPaycurr());
	zrcshoprec.cntcurr.set(chdrpf.getCntcurr());
	zrcshoprec.reqntype.set(tr528rec.paymentMethod);
	if (isNE(zraepf.getPaycurr(), chdrpf.getCntcurr())) {
		zrcshoprec.pymt.set(wsaaConvertedNet);		
	}
	else {
		zrcshoprec.pymt.set(wsaaNetPymt);
	}
	
	zrcshoprec.tranref.set(zraeIO.getChdrnum());
	zrcshoprec.chdrnum.set(zraeIO.getChdrnum());
	zrcshoprec.chdrcoy.set(zraeIO.getChdrcoy());
	zrcshoprec.tranno.set(chdrTranno);
	zrcshoprec.effdate.set(datcon1rec.intDate.toInt());
	zrcshoprec.language.set(wsspcomn.language);
	if(descpf != null)
	zrcshoprec.trandesc.set(descpf.getLongdesc());
	getUserNumber();
	zrcshoprec.user.set(zrgtusrrec.usernum);
	
	zrcshoprec.frmRldgacct.set(zraepf.getChdrnum());
	/* Use new procedure division copybook SUBCODE to perform the*/
	/* substitution in the GLCODE field.*/
	subcoderec.codeRec.set(SPACES);
	subcoderec.code[1].set(chdrpf.getCnttype());
	
	zrcshoprec.batckey.set(wsaaBatckey);
	zrcshoprec.sacscode.set(t5645rec.sacscode03);
	zrcshoprec.sacstyp.set(t5645rec.sacstype03);
	zrcshoprec.sign.set(t5645rec.sign03);
	zrcshoprec.cnttot.set(t5645rec.cnttot03);
	zrcshoprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	subcoderec.glcode.set(t5645rec.glmap03);
	subcode();
	zrcshoprec.glcode.set(subcoderec.glcode);
 
	
	callProgram((sv.rgpymop), zrcshoprec.rec);
	
	if (isNE(zrcshoprec.statuz, varcom.oK)) {
		syserrrec.params.set(zrcshoprec.rec);
		syserrrec.statuz.set(zrcshoprec.statuz);
		fatalError600();
	} 		
		
	}

protected void getUserNumber()
{
	/*START*/
	/* Using ZRGETUSR sub-routine, get the User Number which is*/
	/* not available from the new batch programs.*/
	zrgtusrrec.rec.set(SPACES);
	zrgtusrrec.userid.set(wsspcomn.userid);
	zrgtusrrec.function.set("USID");
	zrgtusrrec.usernum.set(0);
	callProgram(Zrgetusr.class, zrgtusrrec.rec);
	if (isNE(zrgtusrrec.statuz, varcom.oK)) {
		syserrrec.params.set(zrgtusrrec.rec);
		syserrrec.statuz.set(zrgtusrrec.statuz);
		fatalError600();
	}
	/*EXIT*/
}
protected void currencyCheck211(Zraepf zraepf)
{
	if (isEQ(chdrpf.getCntcurr(), zraepf.getPaycurr())) {
		return ;
	}
	
	conlinkrec.clnk002Rec.set(SPACES);
	conlinkrec.amountOut.set(ZERO);
	conlinkrec.rateUsed.set(ZERO);
	conlinkrec.cashdate.set(zraepf.getNpaydate());
	conlinkrec.currIn.set(chdrpf.getCntcurr());
	conlinkrec.currOut.set(zraepf.getPaycurr());
	conlinkrec.amountIn.set(wsaaNetPymt);
	conlinkrec.company.set(zraepf.getChdrcoy());
	conlinkrec.function.set("CVRT");
	callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
	if (isNE(conlinkrec.statuz, varcom.oK)) {
		regpsubrec3.subStatuz.set(conlinkrec.statuz);
		return ;
	}
	zrdecplrec.amountIn.set(conlinkrec.amountOut);
	a000CallRounding();
	conlinkrec.amountOut.set(zrdecplrec.amountOut);
	wsaaConvertedNet.set(conlinkrec.amountOut);
	
	
}



protected void subcode()
{
	sbcd100Start();
}

protected void sbcd100Start()
	{
		if (isNE(subcoderec.code[1], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[1]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[2], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[2]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[3], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[3]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[4], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[4]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[5], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[5]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[6], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[6]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
		}
	}

	protected void whereNext4000() {
			try {
				if(isEQ(wsspcomn.sbmaction,"B")){
					nextProgram4000();	
				} else {
					nextProgram4030();
				}
			} catch (GOTOException e) {
		}
	}

	protected void nextProgram4000() {
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			//goTo(GotoLabel.exit4090);
			return;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1) {
				restoreProgram4100();
			}
		}
 		if (isEQ(sv.ddind, "X")) {
			zraeIO.setFormat(zraerec);
			zraeIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(), varcom.oK)) {
				 syserrrec.params.set(zraeIO.getParams());
				 fatalError600();
			}
			popUp4010();
			genssw4020();
			return;
		} else {
			if (isEQ(sv.ddind, "?")) {
				checkBankDetails4400();
			}
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		} else {
			if(isEQ(wsspcomn.sbmaction , "B")){
				if(isEQ(sv.ansrepy, "Y") || isEQ(sv.ansrepy, "N") || isEQ(sv.ansrepy, "X")){					
					unlkSoftLock3270();
					zraeIO.setFunction(varcom.rlse);
					SmartFileCode.execute(appVars, zraeIO);
					if (isNE(zraeIO.getStatuz(), varcom.oK)) {
						 syserrrec.params.set(zraeIO.getParams());
						 fatalError600();
					}
					wsspcomn.programPtr.add(1);						
				}
				else{
					wsspcomn.nextprog.set(scrnparams.scrname);
				}
				
			}
		}	
	}

	protected void popUp4010() {
		gensswrec.function.set(SPACES);
		if ((isEQ(sv.ddind, "X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1) {
				saveProgramStack4200();
			}
			sv.ddind.set("?");
			gensswrec.function.set("A");
		}
	}

	protected void genssw4020() {
		if (isEQ(gensswrec.function, SPACES)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			//goTo(GotoLabel.exit4090);
			return;
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz, varcom.oK))
				&& (isNE(gensswrec.statuz, varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			//goTo(GotoLabel.exit4090);
			return;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1) {
			loadProgramStack4300();
		}
		nextProgram4030();
	}

	protected void nextProgram4030() {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	protected void restoreProgram4100() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void saveProgramStack4200() {
		/* PARA */
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void loadProgramStack4300() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void checkBankDetails4400() {		
		zraeIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			fatalError600();
		}
		if ((isEQ(zraeIO.getBankkey(), SPACES))
				&& (isEQ(zraeIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		} else {
			sv.ddind.set("+");
		}
	}

	protected void writePtrn3900() {

		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		ptrnpf.setTranno(chdrTranno);
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		wsaaBatckey1.set(wsspcomn.batchkey);
		ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
		ptrnpf.setCrtuser(wsspcomn.userid.toString());  //IJS-523
		List<Ptrnpf> list = new ArrayList<Ptrnpf>();
		list.add(ptrnpf);
		if (!ptrnpfDAO.insertPtrnPF(list)) {
			syserrrec.params.set(chdrpf.getChdrcoy().toString()
					.concat(chdrpf.getChdrnum()));//IJTI-1410
			fatalError600();
		}
	}

	/*
	 * Class transformed from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
		private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
		private FixedLengthStringData e570 = new FixedLengthStringData(4).init("E570");
		private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
		private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
		private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
		private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
		private FixedLengthStringData rrfp = new FixedLengthStringData(4).init("RRFP");
		private FixedLengthStringData g439 = new FixedLengthStringData(4).init("G439");
		
	}

	protected void unlkSoftLock3270(){
		/*    Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(zraepf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		/*  MOVE 'T703'                 TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	} 
	
	protected void updateZraepf(){
		zraepf.setTranno(chdrTranno);
		zraepf.setWdrgpymop(sv.rgpymop.toString());

		/*zraeIO.setPayclt(chdrpf.getCownnum());*/
		zraeIO.setPaycurr(chdrpf.getBillcurr());
		zraeIO.setFormat(zraerec);

		zraeIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)) {
			zraeIO.setParams(zraeIO.getParams());
			fatalError600();
		}
		if (isEQ(tr528rec.bankaccreq, "Y")) {	
			readBankDetailsFromRegt(zraepf);
		} else {
			zraepf.setWdbankkey(SPACE);
			zraepf.setWdbankacckey(SPACE);
			sv.ddind.set(SPACE);
		}
		
		
		List<String> chdrnumList = new ArrayList<String>();
		chdrnumList.add(chdrpf.getChdrnum());//IJTI-1410
		zraepfList = zraeDAO.searchZraepfRecord(chdrnumList, chdrpf.getChdrcoy().toString());

		if(zraepfList!= null && !zraepfList.isEmpty()){
			for(Zraepf Zraepftmp : zraepfList){
				Zraepftmp.setApcaplamt(new BigDecimal(0));
				Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
				Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
				Zraepftmp.setFlag("1");
				Zraepftmp.setApintamt(BigDecimal.ZERO);
				Zraepftmp.setTotamnt(0);
				Zraepftmp.setWdbankkey(zraepf.getWdbankkey());
				Zraepftmp.setWdbankacckey(zraepf.getWdbankacckey());
				Zraepftmp.setWdrgpymop(sv.rgpymop.toString());
				Zraepftmp.setTranno(chdrTranno);
			}
		}
		zraeDAO.updateZraeWithBankDetails(zraepfList);	
	} 
	
	protected void updateChdrRecord3020(){
		chdrBulkUpdtList = new ArrayList<>();
		chdrpf.setUniqueNumber(chdrpf.getUniqueNumber());
		chdrpf.setTranno(chdrTranno);
		chdrBulkUpdtList.add(chdrpf);		
		chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
	} 
	
	protected void postings360(){

		lifacmvrec1.lifacmvRec.set(SPACES);
		wsaaSequenceNo.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		/*Read T1688 for transaction code description.*/                 
		
		if(descpf!=null)
			lifacmvrec1.trandesc.set(descpf.getLongdesc());
		wsaaBatckey.set(wsspcomn.batchkey);
		lifacmvrec1.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.rdocnum.set(zraepf.getChdrnum());
		compute(lifacmvrec1.tranno, 0).set(chdrTranno);
		lifacmvrec1.rldgcoy.set(chdrpf.getChdrcoy());
		lifacmvrec1.origcurr.set(chdrpf.getBillcurr());
		lifacmvrec1.tranref.set(zraepf.getChdrnum());
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.genlcoy.set(chdrpf.getChdrcoy());
		lifacmvrec1.genlcur.set(chdrpf.getBillcurr());
		lifacmvrec1.effdate.set(datcon1rec.intDate.toInt());
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(varcom.vrcmUser);
		lifacmvrec1.termid.set(varcom.vrcmTermid);
		lifacmvrec1.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec1.rldgacct.set(zraepf.getChdrnum());
		lifacmvrec1.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec1.glcode.set(t5645rec.glmap[1]);
		lifacmvrec1.glsign.set(t5645rec.sign[1]);
		lifacmvrec1.contot.set(t5645rec.cnttot[1]);
		lifacmvrec1.origamt.set(accAmtLPAE); 
		lifacmvrec1.acctamt.set(accAmtLPAE);

		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			return ; 
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec1.glcode.set(t5645rec.glmap[2]);
		lifacmvrec1.glsign.set(t5645rec.sign[2]);
		lifacmvrec1.contot.set(t5645rec.cnttot[2]);
		lifacmvrec1.origamt.set(accAmtLPAS);
		lifacmvrec1.acctamt.set(accAmtLPAS);

		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
		}
		

		if(sv.rgpymop.toString().trim().equalsIgnoreCase(BNKDCRDT)){
			return;				
		}else if (sv.rgpymop.toString().trim().equalsIgnoreCase(CHEQUE)) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode[4]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[4]);
			lifacmvrec1.glcode.set(t5645rec.glmap[4]);
			lifacmvrec1.glsign.set(t5645rec.sign[4]);
			lifacmvrec1.contot.set(t5645rec.cnttot[4]);
		}

		lifacmvrec1.origamt.set(sv.accamnt);   
		lifacmvrec1.acctamt.set(sv.accamnt);
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			return ; 
		}
	}

protected void a000CallRounding(){
//	A100-CALL
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(regpsubrec3.subCompany);
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(chdrpf.getCntcurr());
	zrdecplrec.batctrcde.set(regpsubrec3.subBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		regpsubrec3.subStatuz.set(zrdecplrec.statuz);
	}
//	A000-EXIT
}
protected void keepZraeIO(List<Zraepf> zraepfList){
	for(Zraepf Zraepftmp : zraepfList){
		zraeIO.setChdrnum(Zraepftmp.getChdrnum());
		zraeIO.setChdrcoy(Zraepftmp.getChdrcoy());
		zraeIO.setLife(Zraepftmp.getLife());
		zraeIO.setCoverage(Zraepftmp.getCoverage());
		zraeIO.setRider(Zraepftmp.getRider());
		zraeIO.setPaycurr(Zraepftmp.getPaycurr());
		zraeIO.setPayclt(Zraepftmp.getPayclt());

		if(Zraepftmp.getWdbankacckey() != null && Zraepftmp.getWdbankkey() !=null){
			zraeIO.setBankacckey(Zraepftmp.getWdbankacckey());
			zraeIO.setBankkey(Zraepftmp.getWdbankkey());
		}
	}
	zraeIO.setFormat(zraerec);

	zraeIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, zraeIO);
	if (isNE(zraeIO.getStatuz(),varcom.oK)) {
		zraeIO.setParams(zraeIO.getParams());
		fatalError600();
	}

}
protected void readBankDetailsFromRegt(Zraepf zraepf){
	zraeIO.setFunction("RETRV");
	SmartFileCode.execute(appVars, zraeIO);
	if (isNE(zraeIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(zraeIO.getParams());
		fatalError600();
	}
	// new column of method of payment and bank Key and code 
	zraepf.setWdbankkey(zraeIO.getBankkey().toString());
	zraepf.setWdbankacckey(zraeIO.getBankacckey().toString());
} 
public  Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   }

}
