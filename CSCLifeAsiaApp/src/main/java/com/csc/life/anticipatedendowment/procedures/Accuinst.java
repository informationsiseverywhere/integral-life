/*
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.life.anticipatedendowment.dataaccess.ZrlndscTableDAM;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Accuinst extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("ACCUINST");
	private FixedLengthStringData wsaaVersion = new  FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Varcom varcom = new Varcom();
	private Zrcshoprec zrcshoprec = new Zrcshoprec();
	
	public Accuinst() {
		super();
	}

	public void mainline(Object... parmArray) {
		zrcshoprec.rec = convertAndSetParam(zrcshoprec.rec, parmArray, 0);
		try {
			mainline0000();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void mainline0000() {
		para0010();
		exit0090();
	}

	protected void para0010() {
		zrcshoprec.statuz.set(varcom.oK);
		wsaaTranDate.set(getCobolDate());
		wsaaTranTime.set(999999);
		processCashAccount2000();
	}

	protected void exit0090() {
		exitProgram();
	}

	
	protected void processCashAccount2000()// accumulate interest for LP AE
	{
		start2010();
	}

	protected void start2010() {
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(zrcshoprec.batccoy);
		lifacmvrec.batcbrn.set(zrcshoprec.batcbrn);
		lifacmvrec.batcactyr.set(zrcshoprec.batcactyr);
		lifacmvrec.batcactmn.set(zrcshoprec.batcactmn);
		lifacmvrec.batctrcde.set(zrcshoprec.batctrcde);
		lifacmvrec.batcbatch.set(zrcshoprec.batcbatch);
		lifacmvrec.rldgcoy.set(zrcshoprec.chdrcoy);
		lifacmvrec.genlcoy.set(zrcshoprec.chdrcoy);
		lifacmvrec.rdocnum.set(zrcshoprec.chdrnum);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(zrcshoprec.cntcurr);
		lifacmvrec.origamt.set(zrcshoprec.pymt);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(zrcshoprec.tranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(zrcshoprec.effdate);
		lifacmvrec.tranref.set(zrcshoprec.chdrnum);
		lifacmvrec.trandesc.set(zrcshoprec.trandesc);
		lifacmvrec.rldgacct.set(zrcshoprec.chdrnum);
		lifacmvrec.termid.set(zrcshoprec.termid);
		lifacmvrec.transactionDate.set(wsaaTranDate);
		lifacmvrec.transactionTime.set(wsaaTranTime);
		lifacmvrec.user.set(zrcshoprec.user);
		lifacmvrec.sacscode.set(zrcshoprec.sacscode);
		lifacmvrec.sacstyp.set(zrcshoprec.sacstyp);
		lifacmvrec.glcode.set(zrcshoprec.glcode);
		lifacmvrec.glsign.set(zrcshoprec.sign);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			zrcshoprec.errorFormat.set(lifacmvrec.lifacmvRec);
			zrcshoprec.statuz.set(lifacmvrec.statuz);
			exit0090();
		}
	}
}
