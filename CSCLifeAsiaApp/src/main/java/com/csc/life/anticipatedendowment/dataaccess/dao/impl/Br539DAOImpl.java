package com.csc.life.anticipatedendowment.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.Br539DAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Br539DTO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br539DAOImpl extends BaseDAOImpl<Loanpf> implements Br539DAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(Br539DAOImpl.class);
	
	public List<Loanpf> loadDataByBatch(int BatchID) {
		StringBuilder sb = new StringBuilder(); //ILIFE-8840
		sb.append("SELECT * FROM VM1DTA.BR539DATA "); //ILB-475
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Loanpf> loanList = new ArrayList<Loanpf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(BatchID));
			
			rs = ps.executeQuery();
			while(rs.next()){
				Loanpf loanpf = new Loanpf();
				Br539DTO br539DTO = new Br539DTO();
				loanpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				loanpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				loanpf.setChdrnum(rs.getString("CHDRNUM").trim());
				loanpf.setLoannumber(rs.getInt("LOANNUMBER"));
				loanpf.setLoantype(rs.getString("LOANTYPE").trim());
				loanpf.setLoancurr(rs.getString("LOANCURR").trim());
				loanpf.setValidflag(rs.getString("VALIDFLAG").trim());
				loanpf.setFtranno(rs.getLong("FTRANNO"));
				loanpf.setLtranno(rs.getLong("LTRANNO"));
				loanpf.setLoanorigam(rs.getDouble("LOANORIGAM"));
				loanpf.setLoansdate(rs.getInt("LOANSTDATE"));
				loanpf.setLstcaplamt(rs.getDouble("LSTCAPLAMT"));
				loanpf.setLstcapdate(rs.getInt("LSTCAPDATE"));
				loanpf.setNxtcapdate(rs.getInt("NXTCAPDATE"));
				loanpf.setLstintbdte(rs.getInt("LSTINTBDTE"));
				loanpf.setNxtintbdte(rs.getInt("NXTINTBDTE"));
				br539DTO.setUniqueNumber(rs.getLong("CH_UNIQUE_NUMBER"));
				br539DTO.setChdrcoy(rs.getString("CHDRCOY").trim());
				br539DTO.setChdrnum(rs.getString("CHDRNUM").trim());
				br539DTO.setCurrfrom(rs.getInt("CURRFROM"));
				br539DTO.setTranno(rs.getInt("TRANNO"));
				br539DTO.setCnttype(rs.getString("CNTTYPE").trim());
				br539DTO.setChdrpfx(rs.getString("CHDRPFX").trim());
				br539DTO.setServunit(rs.getString("SERVUNIT").trim());
				br539DTO.setOccdate(rs.getInt("OCCDATE"));
				br539DTO.setCcdate(rs.getInt("CCDATE"));
				br539DTO.setCownpfx(rs.getString("COWNPFX").trim());
				br539DTO.setCowncoy(rs.getString("COWNCOY").trim());
				br539DTO.setCownnum(rs.getString("COWNNUM").trim());				
				br539DTO.setCollchnl(rs.getString("COLLCHNL").trim());
				br539DTO.setCntbranch(rs.getString("CNTBRANCH").trim());
				br539DTO.setAgntpfx(rs.getString("AGNTPFX").trim());				
				br539DTO.setAgntcoy(rs.getString("AGNTCOY").trim());
				br539DTO.setAgntnum(rs.getString("AGNTNUM").trim());				
				br539DTO.setClntpfx(rs.getString("CLNTPFX").trim());
				br539DTO.setClntcoy(rs.getString("CLNTCOY").trim());
				br539DTO.setClntnum(rs.getString("CLNTNUM").trim());
				br539DTO.setMandref(rs.getString("MANDREF").trim());
				br539DTO.setPtdate(rs.getInt("PTDATE"));
				br539DTO.setBillchnl(rs.getString("BILLCHNL").trim());
				br539DTO.setBillfreq(rs.getString("BILLFREQ").trim());
				br539DTO.setBillcurr(rs.getString("BILLCURR").trim());
				br539DTO.setNextdate(rs.getInt("NEXTDATE"));				
				br539DTO.setBankkey(rs.getObject("BANKKEY") != null ? (rs.getString("BANKKEY")) : "");
				br539DTO.setBankacckey(rs.getObject("BANKACCKEY") != null ? (rs.getString("BANKACCKEY")) : "");
				br539DTO.setMandstat(rs.getObject("MANDSTAT") != null ? (rs.getString("MANDSTAT")) : "");	
				br539DTO.setFacthous(rs.getObject("FACTHOUS") != null ? (rs.getString("FACTHOUS")) : "");
				br539DTO.setCnt(rs.getInt("CNT"));
				loanpf.setBr539DTO(br539DTO);
				loanList.add(loanpf);	  
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);	 //ILIFE-8840
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return loanList;
	}	
	
	public List<Loanpf> populateBr539Temp(String tableID, String memName, int batchExtractSize, int BatchID) {
		//initializeBr539Temp();
		//int rows = 0;
		StringBuilder sb = new StringBuilder(); //ILIFE-8840
		//sb.append("INSERT INTO VM1DTA.BR539DATA "); //ILB-475
		//sb.append("(CNT,BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LOANNUMBER, LOANTYPE, LOANCURR, VALIDFLAG, FTRANNO, LTRANNO, LOANORIGAM, LOANSTDATE, LSTCAPLAMT, LSTCAPDATE, NXTCAPDATE, LSTINTBDTE, NXTINTBDTE, CH_UNIQUE_NUMBER, CURRFROM, TRANNO, CNTTYPE, CHDRPFX, SERVUNIT, OCCDATE, CCDATE, COWNPFX, COWNCOY, COWNNUM, COLLCHNL, CNTBRANCH, AGNTPFX, AGNTCOY, AGNTNUM, CLNTPFX, CLNTCOY, CLNTNUM, MANDREF, PTDATE, BILLCHNL, BILLFREQ, BILLCURR, NEXTDATE, BANKKEY, BANKACCKEY, MANDSTAT, FACTHOUS, DATIME) ");
		sb.append("SELECT count(*) over(partition by MAIN.CHDRCOY,MAIN.CHDRNUM) as cnt, MAIN.batchnum, MAIN.UNIQUE_NUMBER, MAIN.CHDRCOY, MAIN.CHDRNUM, MAIN.LOANNUMBER, MAIN.LOANTYPE, ");
		sb.append("MAIN.LOANCURR, MAIN.VALIDFLAG, MAIN.FTRANNO, MAIN.LTRANNO, MAIN.LOANORIGAM, MAIN.LOANSTDATE, MAIN.LSTCAPLAMT, MAIN.LSTCAPDATE, MAIN.NXTCAPDATE, "); 
		sb.append("MAIN.LSTINTBDTE, MAIN.NXTINTBDTE, ");  	 //ILIFE-8840
		sb.append("MAIN.CH_UNIQUE_NUMBER, MAIN.CURRFROM, MAIN.TRANNO, MAIN.CNTTYPE, MAIN.CHDRPFX, MAIN.SERVUNIT, MAIN.OCCDATE, MAIN.CCDATE, MAIN.COWNPFX, MAIN.COWNCOY, MAIN.COWNNUM, MAIN.COLLCHNL, MAIN.CNTBRANCH, MAIN.AGNTPFX, MAIN.AGNTCOY, MAIN.AGNTNUM, ");
		sb.append("MAIN.CLNTPFX, MAIN.CLNTCOY, MAIN.CLNTNUM, ");		
		sb.append("MAIN.MANDREF, MAIN.PTDATE, MAIN.BILLCHNL, MAIN.BILLFREQ, MAIN.BILLCURR, MAIN.NEXTDATE, ");
		sb.append("MAIN.BANKKEY, MAIN.BANKACCKEY, MAIN.MANDSTAT, MAIN.FACTHOUS, MAIN.DATIME FROM( ");
		sb.append("SELECT " );		
		sb.append("floor((row_number()over(ORDER BY LO.CHDRCOY ASC, LO.CHDRNUM ASC, LO.LOANNUMBER DESC)-1)/?) batchnum, ");
		sb.append("LO.UNIQUE_NUMBER, LO.CHDRCOY, LO.CHDRNUM, LO.LOANNUMBER, LO.LOANTYPE, ");
		sb.append("LO.LOANCURR, LO.VALIDFLAG, LO.FTRANNO, LO.LTRANNO, LO.LOANORIGAM, LO.LOANSTDATE, LO.LSTCAPLAMT, LO.LSTCAPDATE, LO.NXTCAPDATE, "); 
		sb.append("LO.LSTINTBDTE, LO.NXTINTBDTE, ");  	 //ILIFE-8840
		sb.append("CH.UNIQUE_NUMBER CH_UNIQUE_NUMBER, CH.CURRFROM, CH.TRANNO, CH.CNTTYPE, CH.CHDRPFX, CH.SERVUNIT, CH.OCCDATE, CH.CCDATE, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM, CH.COLLCHNL, CH.CNTBRANCH, CH.AGNTPFX, CH.AGNTCOY, CH.AGNTNUM, ");
		sb.append("CL.CLNTPFX, CL.CLNTCOY, CL.CLNTNUM, ");		
		sb.append("PY.MANDREF, PY.PTDATE, PY.BILLCHNL, PY.BILLFREQ, PY.BILLCURR, PY.NEXTDATE, ");
		sb.append("MA.BANKKEY, MA.BANKACCKEY, MA.MANDSTAT, CB.FACTHOUS, ? DATIME ");//ILIFE-2995
		sb.append("  FROM ");
	    sb.append(tableID);
	    sb.append(" LO ");
		sb.append("INNER JOIN CHDRPF CH ON  CH.CHDRCOY = LO.CHDRCOY  AND CH.CHDRNUM=LO.CHDRNUM  AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG in ('1','3') ");
		sb.append("INNER JOIN CLRRPF CL ON CL.FOREPFX = CH.CHDRPFX AND CL.FORECOY = CH.CHDRCOY AND CL.FORENUM = concat(CH.CHDRNUM, '1') AND CL.CLRRROLE = 'PY' ");
		sb.append("INNER JOIN PAYRPF PY ON PY.CHDRCOY = LO.CHDRCOY  AND PY.CHDRNUM=LO.CHDRNUM AND PY.PAYRSEQNO = 1 AND PY.VALIDFLAG = '1' ");
		sb.append("LEFT OUTER JOIN MANDPF MA ON MA.PAYRCOY = PY.CHDRCOY AND MA.PAYRNUM = PY.CHDRNUM AND MA.MANDREF = PY.MANDREF AND MA.VALIDFLAG = '1' ");
		sb.append("LEFT OUTER JOIN CLBAPF CB ON CB.CLNTCOY = PY.CHDRCOY AND CB.CLNTNUM = PY.CHDRNUM AND CB.BANKKEY = MA.BANKKEY AND CB.BANKACCKEY = MA.BANKACCKEY AND CB.CLNTPFX = 'CN' AND NOT (CB.VALIDFLAG <> '1') "); 
		sb.append("   WHERE LO.MEMBER_NAME = ? ");
		sb.append(") MAIN WHERE MAIN.BATCHNUM = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;	
		List<Loanpf> loanList = new ArrayList<Loanpf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setTimestamp(2, getDatime()); //ILIFE-8840
			 ps.setString(3, memName); //ILIFE-8840
			 ps.setInt(4, BatchID); //ILIFE-8840
			 rs = ps.executeQuery();
				while(rs.next()){
					Loanpf loanpf = new Loanpf();
					Br539DTO br539DTO = new Br539DTO();
					loanpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					loanpf.setChdrcoy(rs.getString("CHDRCOY").trim());
					loanpf.setChdrnum(rs.getString("CHDRNUM").trim());
					loanpf.setLoannumber(rs.getInt("LOANNUMBER"));
					loanpf.setLoantype(rs.getString("LOANTYPE").trim());
					loanpf.setLoancurr(rs.getString("LOANCURR").trim());
					loanpf.setValidflag(rs.getString("VALIDFLAG").trim());
					loanpf.setFtranno(rs.getLong("FTRANNO"));
					loanpf.setLtranno(rs.getLong("LTRANNO"));
					loanpf.setLoanorigam(rs.getDouble("LOANORIGAM"));
					loanpf.setLoansdate(rs.getInt("LOANSTDATE"));
					loanpf.setLstcaplamt(rs.getDouble("LSTCAPLAMT"));
					loanpf.setLstcapdate(rs.getInt("LSTCAPDATE"));
					loanpf.setNxtcapdate(rs.getInt("NXTCAPDATE"));
					loanpf.setLstintbdte(rs.getInt("LSTINTBDTE"));
					loanpf.setNxtintbdte(rs.getInt("NXTINTBDTE"));
					br539DTO.setUniqueNumber(rs.getLong("CH_UNIQUE_NUMBER"));
					br539DTO.setChdrcoy(rs.getString("CHDRCOY").trim());
					br539DTO.setChdrnum(rs.getString("CHDRNUM").trim());
					br539DTO.setCurrfrom(rs.getInt("CURRFROM"));
					br539DTO.setTranno(rs.getInt("TRANNO"));
					br539DTO.setCnttype(rs.getString("CNTTYPE").trim());
					br539DTO.setChdrpfx(rs.getString("CHDRPFX").trim());
					br539DTO.setServunit(rs.getString("SERVUNIT").trim());
					br539DTO.setOccdate(rs.getInt("OCCDATE"));
					br539DTO.setCcdate(rs.getInt("CCDATE"));
					br539DTO.setCownpfx(rs.getString("COWNPFX"));//ILIFE-9488
					br539DTO.setCowncoy(rs.getString("COWNCOY").trim());
					br539DTO.setCownnum(rs.getString("COWNNUM").trim());				
					br539DTO.setCollchnl(rs.getString("COLLCHNL") != null ? rs.getString("COLLCHNL").trim() : "");//ILIFE-9488
					br539DTO.setCntbranch(rs.getString("CNTBRANCH").trim());
					br539DTO.setAgntpfx(rs.getString("AGNTPFX") != null ? rs.getString("AGNTPFX").trim() : "");//ILIFE-9488		
					br539DTO.setAgntcoy(rs.getString("AGNTCOY").trim());
					br539DTO.setAgntnum(rs.getString("AGNTNUM").trim());				
					br539DTO.setClntpfx(rs.getString("CLNTPFX").trim());
					br539DTO.setClntcoy(rs.getString("CLNTCOY").trim());
					br539DTO.setClntnum(rs.getString("CLNTNUM").trim());
					br539DTO.setMandref(rs.getString("MANDREF").trim());
					br539DTO.setPtdate(rs.getInt("PTDATE"));
					br539DTO.setBillchnl(rs.getString("BILLCHNL").trim());
					br539DTO.setBillfreq(rs.getString("BILLFREQ").trim());
					br539DTO.setBillcurr(rs.getString("BILLCURR").trim());
					br539DTO.setNextdate(rs.getInt("NEXTDATE"));				
					br539DTO.setBankkey(rs.getObject("BANKKEY") != null ? (rs.getString("BANKKEY")) : "");
					br539DTO.setBankacckey(rs.getObject("BANKACCKEY") != null ? (rs.getString("BANKACCKEY")) : "");
					br539DTO.setMandstat(rs.getObject("MANDSTAT") != null ? (rs.getString("MANDSTAT")) : "");	
					br539DTO.setFacthous(rs.getObject("FACTHOUS") != null ? (rs.getString("FACTHOUS")) : "");
					br539DTO.setCnt(rs.getInt("CNT"));
					loanpf.setBr539DTO(br539DTO);
					loanList.add(loanpf);	  
				}				
			
		}catch (SQLException e) {
			LOGGER.error("populateBr539Temp()", e);	 //ILIFE-8840
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return loanList;
	}
	
	private void initializeBr539Temp() {

		StringBuilder sb = new StringBuilder(); //ILIFE-8840
		sb.append("DELETE FROM VM1DTA.BR539DATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate(); //ILIFE-8840
			
		}catch (SQLException e) {
			LOGGER.error("populateBr539Temp()", e);	 //ILIFE-8840
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}	
}