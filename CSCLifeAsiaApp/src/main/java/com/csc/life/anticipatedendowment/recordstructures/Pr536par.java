package com.csc.life.anticipatedendowment.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:56
 * Description:
 * Copybook name: PR536PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pr536par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(4);
  	public ZonedDecimalData zradvlead = new ZonedDecimalData(2, 0).isAPartOf(parmRecord, 0);
  	public ZonedDecimalData zrcshlead = new ZonedDecimalData(2, 0).isAPartOf(parmRecord, 2);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}