/*
 * File: Loanpf.java
 * Date: March 11, 2016
 * Author: CSC
 * Created by: dpuhawan
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

public class Loanpf implements Serializable {

	private static final long serialVersionUID = -1625548138278824432L;

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int loannumber;
	private String loantype;
	private String loancurr;
    private String validflag;
    private long ftranno;
    private long ltranno;
    private double loanorigam;
    private int loansdate;
    private double lstcaplamt;
    private int lstcapdate;
    private int nxtcapdate;
    private int lstintbdte;
    private int nxtintbdte;   
    private double tplstmdty;
    private String termid;
    private int user_t;
    private int trdt;
    private int trtm;
    private String usrprf;
    private String jobnm;
    private Date datime;
    private Br539DTO br539DTO;
    private Br535DTO br535DTO;
    
    public Loanpf() {}
    
    public Loanpf(Loanpf loanpf) {
    	this.uniqueNumber = loanpf.getUniqueNumber();
    	this.chdrcoy = loanpf.getChdrcoy();
    	this.chdrnum = loanpf.getChdrnum();
    	this.loannumber = loanpf.getLoannumber();
    	this.loantype = loanpf.getLoantype();
    	this.loancurr = loanpf.getLoancurr();
    	this.validflag = loanpf.getValidflag();
    	this.ftranno = loanpf.getFtranno();
    	this.ltranno = loanpf.getLtranno();
    	this.loanorigam = loanpf.getLoanorigam();
    	this.loansdate = loanpf.getLoansdate();
    	this.lstcaplamt = loanpf.getLstcaplamt();
    	this.lstcapdate = loanpf.getLstcapdate();
    	this.nxtcapdate = loanpf.getNxtcapdate();
    	this.lstintbdte = loanpf.getLstintbdte();
    	this.nxtintbdte = loanpf.getNxtintbdte();
    	this.tplstmdty = loanpf.getTplstmdty();
    	this.termid = loanpf.getTermid();
    	this.user_t = loanpf.getUser_t();
    	this.trdt = loanpf.getTrdt();
    	this.trtm = loanpf.getTrtm();
    	this.usrprf = loanpf.getUsrprf();
    	this.jobnm = loanpf.getJobnm();
    }
    
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getLoannumber() {
		return loannumber;
	}
	public void setLoannumber(int loannumber) {
		this.loannumber = loannumber;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public String getLoancurr() {
		return loancurr;
	}
	public void setLoancurr(String loancurr) {
		this.loancurr = loancurr;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public long getFtranno() {
		return ftranno;
	}
	public void setFtranno(long ftranno) {
		this.ftranno = ftranno;
	}
	public long getLtranno() {
		return ltranno;
	}
	public void setLtranno(long ltranno) {
		this.ltranno = ltranno;
	}
	public double getLoanorigam() {
		return loanorigam;
	}
	public void setLoanorigam(double loanorigam) {
		this.loanorigam = loanorigam;
	}
	public int getLoansdate() {
		return loansdate;
	}
	public void setLoansdate(int loansdate) {
		this.loansdate = loansdate;
	}
	public double getLstcaplamt() {
		return lstcaplamt;
	}
	public void setLstcaplamt(double lstcaplamt) {
		this.lstcaplamt = lstcaplamt;
	}
	public int getLstcapdate() {
		return lstcapdate;
	}
	public void setLstcapdate(int lstcapdate) {
		this.lstcapdate = lstcapdate;
	}
	public int getNxtcapdate() {
		return nxtcapdate;
	}
	public void setNxtcapdate(int nxtcapdate) {
		this.nxtcapdate = nxtcapdate;
	}
	public int getLstintbdte() {
		return lstintbdte;
	}
	public void setLstintbdte(int lstintbdte) {
		this.lstintbdte = lstintbdte;
	}
	public int getNxtintbdte() {
		return nxtintbdte;
	}
	public void setNxtintbdte(int nxtintbdte) {
		this.nxtintbdte = nxtintbdte;
	}
	public double getTplstmdty() {
		return tplstmdty;
	}
	public void setTplstmdty(double tplstmdty) {
		this.tplstmdty = tplstmdty;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getUser_t() {
		return user_t;
	}
	public void setUser_t(int user_t) {
		this.user_t = user_t;
	}
	public int getTrdt() {
		return trdt;
	}
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	public int getTrtm() {
		return trtm;
	}
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public Br539DTO getBr539DTO() {
		return br539DTO;
	}
	public void setBr539DTO(Br539DTO br539dto) {
		br539DTO = br539dto;
	}
	public Br535DTO getBr535DTO() {
		return br535DTO;
	}
	public void setBr535DTO(Br535DTO br535dto) {
		br535DTO = br535dto;
	}	

}
