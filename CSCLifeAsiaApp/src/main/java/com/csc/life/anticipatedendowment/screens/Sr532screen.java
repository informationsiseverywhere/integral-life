package com.csc.life.anticipatedendowment.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
 
public class Sr532screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 17, 22, 5, 18, 23, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr532ScreenVars sv = (Sr532ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr532screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr532ScreenVars screenVars = (Sr532ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrcoy.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.cntdesc.setClassString("");
		screenVars.life.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.zrdate01Disp.setClassString("");
		screenVars.prcnt01.setClassString("");
		screenVars.zrpaydt01Disp.setClassString("");
		screenVars.paysts01.setClassString("");
		screenVars.paymentMethod01.setClassString("");
		screenVars.payOption01.setClassString("");
		screenVars.paid01.setClassString("");
		screenVars.zrdate02Disp.setClassString("");
		screenVars.prcnt02.setClassString("");
		screenVars.zrpaydt02Disp.setClassString("");
		screenVars.paysts02.setClassString("");
		screenVars.paymentMethod02.setClassString("");
		screenVars.payOption02.setClassString("");
		screenVars.paid02.setClassString("");
		screenVars.zrdate03Disp.setClassString("");
		screenVars.prcnt03.setClassString("");
		screenVars.zrpaydt03Disp.setClassString("");
		screenVars.paysts03.setClassString("");
		screenVars.paymentMethod03.setClassString("");
		screenVars.payOption03.setClassString("");
		screenVars.paid03.setClassString("");
		screenVars.zrdate04Disp.setClassString("");
		screenVars.prcnt04.setClassString("");
		screenVars.zrpaydt04Disp.setClassString("");
		screenVars.paysts04.setClassString("");
		screenVars.paymentMethod04.setClassString("");
		screenVars.payOption04.setClassString("");
		screenVars.paid04.setClassString("");
		screenVars.zrdate05Disp.setClassString("");
		screenVars.prcnt05.setClassString("");
		screenVars.zrpaydt05Disp.setClassString("");
		screenVars.paysts05.setClassString("");
		screenVars.paymentMethod05.setClassString("");
		screenVars.payOption05.setClassString("");
		screenVars.paid05.setClassString("");
		screenVars.zrdate06Disp.setClassString("");
		screenVars.prcnt06.setClassString("");
		screenVars.zrpaydt06Disp.setClassString("");
		screenVars.paysts06.setClassString("");
		screenVars.paymentMethod06.setClassString("");
		screenVars.payOption06.setClassString("");
		screenVars.paid06.setClassString("");
		screenVars.zrdate07Disp.setClassString("");
		screenVars.prcnt07.setClassString("");
		screenVars.zrpaydt07Disp.setClassString("");
		screenVars.paysts07.setClassString("");
		screenVars.paymentMethod07.setClassString("");
		screenVars.payOption07.setClassString("");
		screenVars.paid07.setClassString("");
		screenVars.zrdate08Disp.setClassString("");
		screenVars.prcnt08.setClassString("");
		screenVars.zrpaydt08Disp.setClassString("");
		screenVars.paysts08.setClassString("");
		screenVars.paymentMethod08.setClassString("");
		screenVars.payOption08.setClassString("");
		screenVars.paid08.setClassString("");
		screenVars.paid09.setClassString("");
		screenVars.zrdate09Disp.setClassString("");
		screenVars.payOption09.setClassString("");
		screenVars.pymdesc.setClassString("");
		screenVars.payeesel.setClassString("");
		screenVars.payeenme.setClassString("");
		screenVars.payMethod.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.optdsc.setClassString("");
		screenVars.optind.setClassString("");
		screenVars.paycurr.setClassString("");
		screenVars.currdesc.setClassString("");
		screenVars.cntcurr.setClassString("");
	}
/**
 * Clear all the variables in Sr532screen
 */
	public static void clear(VarModel pv) {
		Sr532ScreenVars screenVars = (Sr532ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrcoy.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.cntdesc.clear();
		screenVars.life.clear();
		screenVars.lifename.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.zrdate01Disp.clear();
		screenVars.zrdate01.clear();
		screenVars.prcnt01.clear();
		screenVars.zrpaydt01Disp.clear();
		screenVars.zrpaydt01.clear();
		screenVars.paysts01.clear();
		screenVars.paymentMethod01.clear();
		screenVars.payOption01.clear();
		screenVars.paid01.clear();
		screenVars.zrdate02Disp.clear();
		screenVars.zrdate02.clear();
		screenVars.prcnt02.clear();
		screenVars.zrpaydt02Disp.clear();
		screenVars.zrpaydt02.clear();
		screenVars.paysts02.clear();
		screenVars.paymentMethod02.clear();
		screenVars.payOption02.clear();
		screenVars.paid02.clear();
		screenVars.zrdate03Disp.clear();
		screenVars.zrdate03.clear();
		screenVars.prcnt03.clear();
		screenVars.zrpaydt03Disp.clear();
		screenVars.zrpaydt03.clear();
		screenVars.paysts03.clear();
		screenVars.paymentMethod03.clear();
		screenVars.payOption03.clear();
		screenVars.paid03.clear();
		screenVars.zrdate04Disp.clear();
		screenVars.zrdate04.clear();
		screenVars.prcnt04.clear();
		screenVars.zrpaydt04Disp.clear();
		screenVars.zrpaydt04.clear();
		screenVars.paysts04.clear();
		screenVars.paymentMethod04.clear();
		screenVars.payOption04.clear();
		screenVars.paid04.clear();
		screenVars.zrdate05Disp.clear();
		screenVars.zrdate05.clear();
		screenVars.prcnt05.clear();
		screenVars.zrpaydt05Disp.clear();
		screenVars.zrpaydt05.clear();
		screenVars.paysts05.clear();
		screenVars.paymentMethod05.clear();
		screenVars.payOption05.clear();
		screenVars.paid05.clear();
		screenVars.zrdate06Disp.clear();
		screenVars.zrdate06.clear();
		screenVars.prcnt06.clear();
		screenVars.zrpaydt06Disp.clear();
		screenVars.zrpaydt06.clear();
		screenVars.paysts06.clear();
		screenVars.paymentMethod06.clear();
		screenVars.payOption06.clear();
		screenVars.paid06.clear();
		screenVars.zrdate07Disp.clear();
		screenVars.zrdate07.clear();
		screenVars.prcnt07.clear();
		screenVars.zrpaydt07Disp.clear();
		screenVars.zrpaydt07.clear();
		screenVars.paysts07.clear();
		screenVars.paymentMethod07.clear();
		screenVars.payOption07.clear();
		screenVars.paid07.clear();
		screenVars.zrdate08Disp.clear();
		screenVars.zrdate08.clear();
		screenVars.prcnt08.clear();
		screenVars.zrpaydt08Disp.clear();
		screenVars.zrpaydt08.clear();
		screenVars.paysts08.clear();
		screenVars.paymentMethod08.clear();
		screenVars.payOption08.clear();
		screenVars.paid08.clear();
		screenVars.paid09.clear();
		screenVars.zrdate09Disp.clear();
		screenVars.zrdate09.clear();
		screenVars.payOption09.clear();
		screenVars.pymdesc.clear();
		screenVars.payeesel.clear();
		screenVars.payeenme.clear();
		screenVars.payMethod.clear();
		screenVars.descrip.clear();
		screenVars.optdsc.clear();
		screenVars.optind.clear();
		screenVars.paycurr.clear();
		screenVars.currdesc.clear();
		screenVars.cntcurr.clear();
	}
}
