/*
 * File: LoanpfDAOImpl.java
 * Date: March 11, 2016
 * Author: CSC
 * Created by: dpuhawan
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.dataaccess.dao.impl;

import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LoanpfDAOImpl  extends BaseDAOImpl<Loanpf> implements LoanpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoanpfDAOImpl.class);	
	protected Varcom varcom = new Varcom(); 	
	//ILPI-229 START by dpuhawan
	/* LoanPF links to the ff tables:
	 * CHDRPF
	 * CLRRPF
	 * PAYRPF
	 * MANDPF
	 * CLBAPF
	 */
	/* Retrieve all valid LOANPF records based on specific params*/	
	public void insertLoanRecords(List<Loanpf> loanList){
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.LOANPF(CHDRCOY, CHDRNUM, LOANNUMBER, LOANTYPE, LOANCURR, VALIDFLAG, FTRANNO, LTRANNO, LOANORIGAM, LOANSTDATE, LSTCAPLAMT, LSTCAPDATE, NXTCAPDATE, LSTINTBDTE, NXTINTBDTE, TPLSTMDTY, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME) ");
		sb.append("SELECT CHDRCOY, CHDRNUM, LOANNUMBER, LOANTYPE, LOANCURR, ?, FTRANNO, LTRANNO, LOANORIGAM, LOANSTDATE, LSTCAPLAMT, LSTCAPDATE, NXTCAPDATE, ?, ?, TPLSTMDTY, TERMID, USER_T, ?, ?, ?, ?, ? FROM LOANPF "); 		
		sb.append("WHERE UNIQUE_NUMBER=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;	
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Loanpf loan : loanList) {
				ps.setString(1, loan.getValidflag());
				ps.setInt(2, loan.getLstintbdte());
			    ps.setInt(3, loan.getNxtintbdte());				
			    ps.setInt(4, Integer.parseInt(getCobolDate()));
			    ps.setInt(5, Integer.parseInt(varcom.vrcmTime.toString()));
			    ps.setString(6, getUsrprf());			    
			    ps.setString(7, getJobnm());		
			    ps.setTimestamp(8, getDatime());					    
			    ps.setLong(9, loan.getUniqueNumber());
			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("insertLoanRecords()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}		
	}
	public void rewrtLoanRecords(List<Loanpf> loanList, int effDate){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE LOANPF SET VALIDFLAG=2,LTRANNO=?, ");
		sb.append("TRDT=?, ");
		sb.append("TRTM=?, ");
		sb.append("USRPRF=?, ");
		sb.append("JOBNM=?, ");
		sb.append("DATIME=? ");
		sb.append("WHERE UNIQUE_NUMBER=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Loanpf loan : loanList) {
				ps.setLong(1, loan.getLtranno());
			    ps.setInt(2, Integer.parseInt(getCobolDate()));
			    ps.setInt(3, Integer.parseInt(varcom.vrcmTime.toString()));
			    ps.setString(4, getUsrprf());			    
			    ps.setString(5, getJobnm());		
			    ps.setTimestamp(6, getDatime());					    
			    ps.setLong(7, loan.getUniqueNumber());
			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("updateLoanRecords()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}		
	}   
	
	public boolean updateLoanRecords(List<Loanpf> loanList){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE LOANPF SET VALIDFLAG=1, ");
		sb.append("LSTINTBDTE=?, ");
		sb.append("LOANSTDATE=?, ");
		sb.append("NXTINTBDTE=?, ");
		sb.append("TRDT=?, ");
		sb.append("TRTM=?, ");
		sb.append("USRPRF=?, ");
		sb.append("JOBNM=?, ");
		sb.append("DATIME=? ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Loanpf loan : loanList) {
				ps.setInt(1, loan.getLstintbdte());
			    ps.setInt(2, loan.getLoansdate());
			    ps.setInt(3, loan.getNxtintbdte());
			    ps.setInt(4, Integer.parseInt(getCobolDate()));
			    ps.setInt(5, Integer.parseInt(varcom.vrcmTime.toString()));
			    ps.setString(6, getUsrprf());			    
			    ps.setString(7, getJobnm());		
			    ps.setTimestamp(8, getDatime());					    
			    ps.setString(9, loan.getChdrcoy());
			    ps.setString(10, loan.getChdrnum());
			    ps.setInt(11, loan.getLoannumber());
			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("updateLoanRecords()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return true;
	}
	
	//ILPI-229 END
	
	//ILIFE-2505

	public boolean updateLoanValRecords(List<Loanpf> loanList){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE LOANPF SET VALIDFLAG=1, ");
		sb.append("LSTINTBDTE=?, ");
		sb.append("LOANSTDATE=?, ");
		sb.append("NXTINTBDTE=?, ");
		sb.append("TRDT=?, ");
		sb.append("TRTM=?, ");
		sb.append("USRPRF=?, ");
		sb.append("JOBNM=?, ");
		sb.append("DATIME=? ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=? AND VALIDFLAG = '1' ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Loanpf loan : loanList) {
				ps.setInt(1, loan.getLstintbdte());
			    ps.setInt(2, loan.getLoansdate());
			    ps.setInt(3, loan.getNxtintbdte());
			    ps.setInt(4, Integer.parseInt(getCobolDate()));
			    ps.setInt(5, Integer.parseInt(varcom.vrcmTime.toString()));
			    ps.setString(6, getUsrprf());			    
			    ps.setString(7, getJobnm());		
			    ps.setTimestamp(8, getDatime());					    
			    ps.setString(9, loan.getChdrcoy());
			    ps.setString(10, loan.getChdrnum());
			    ps.setInt(11, loan.getLoannumber());
			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("updateLoanRecords()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return true;
	}
	
	public List<Loanpf> loadDataByLoanNum(String chdrcoy, String chdrnum, String loanNumber) {
		StringBuilder sb = new StringBuilder("");
		if(!"0".equals(loanNumber))
		{
			sb.append("SELECT * FROM LOANPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LOANNUMBER = ? ");		
		}
		else
		{
			sb.append("SELECT * FROM LOANPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG=1 ");
		}
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LOANNUMBER DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Loanpf> loanList = new ArrayList<Loanpf>();
		try{
			ps = getPrepareStatement(sb.toString());

//			ps.setString(1, chdrcoy);
//			ps.setString(2, chdrnum);
			ps.setString(1, StringUtil.fillSpace(chdrcoy.trim(), DD.chdrcoy.length)); 
 			ps.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length)); 
			if(!"0".equals(loanNumber))
			{			
				ps.setString(3, loanNumber);
			}
			
			rs = ps.executeQuery();
			while(rs.next()){
				Loanpf loanpf = new Loanpf();
				loanpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				loanpf.setChdrcoy(chdrcoy);
				loanpf.setChdrnum(rs.getString("CHDRNUM").trim());
				loanpf.setLoannumber(rs.getInt("LOANNUMBER"));
				loanpf.setLoantype(rs.getString("LOANTYPE").trim());
				loanpf.setLoancurr(rs.getString("LOANCURR").trim());
				loanpf.setValidflag(rs.getString("VALIDFLAG").trim());
				loanpf.setFtranno(rs.getLong("FTRANNO"));
				loanpf.setLtranno(rs.getLong("LTRANNO"));
				loanpf.setLoanorigam(rs.getDouble("LOANORIGAM"));
				loanpf.setLoansdate(rs.getInt("LOANSTDATE"));
				loanpf.setLstcaplamt(rs.getDouble("LSTCAPLAMT"));
				loanpf.setLstcapdate(rs.getInt("LSTCAPDATE"));
				loanpf.setNxtcapdate(rs.getInt("NXTCAPDATE"));
				loanpf.setLstintbdte(rs.getInt("LSTINTBDTE"));
				loanpf.setNxtintbdte(rs.getInt("NXTINTBDTE"));
				loanpf.setTplstmdty(rs.getDouble("TPLSTMDTY"));
				loanpf.setTermid(rs.getString("TERMID").trim());
				loanpf.setUser_t(rs.getInt("USER_T"));
				loanpf.setTrdt(rs.getInt("TRDT"));
				loanpf.setTrtm(rs.getInt("TRTM"));
				loanpf.setUsrprf(rs.getString("USRPRF").trim());
				loanpf.setJobnm(rs.getString("JOBNM").trim());
				loanpf.setDatime(rs.getDate("DATIME"));
				loanList.add(loanpf);	
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByLoanNum()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return loanList;
	}	
	
	
	public boolean updateLoanCapRecords(List<Loanpf> loanList){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE LOANPF SET VALIDFLAG=?, LSTCAPDATE=?, LSTCAPLAMT=?, NXTCAPDATE=? ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=? AND VALIDFLAG = '1' ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Loanpf loan : loanList) {
				ps.setString(1, loan.getValidflag());
			    ps.setInt(2, loan.getLstcapdate());
			    ps.setDouble(3, loan.getLstcaplamt());
			    ps.setInt(4, loan.getNxtcapdate());					    
			    ps.setString(5, loan.getChdrcoy());
			    ps.setString(6, loan.getChdrnum());
			    ps.setInt(7, loan.getLoannumber());

			    ps.addBatch();				
			}		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("updateLoanRecords()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return true;
	}
	
	public boolean updateLoanRecordsUniqueNo(List<Loanpf> loanList){
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE LOANPF SET VALIDFLAG=?, ");
		sb.append("LSTINTBDTE=?, ");
		sb.append("NXTINTBDTE=?, ");
		sb.append("TRDT=?, ");
		sb.append("TRTM=?, ");
		sb.append("USRPRF=?, ");
		sb.append("JOBNM=?, ");
		sb.append("DATIME=? ");
		sb.append("WHERE UNIQUE_NUMBER=?");    
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Loanpf loan : loanList) {
				ps.setString(1, loan.getValidflag());
				ps.setInt(2, loan.getLstintbdte());
			    ps.setInt(3, loan.getNxtintbdte());
			    ps.setInt(4, Integer.parseInt(getCobolDate()));
			    ps.setInt(5, Integer.parseInt(varcom.vrcmTime.toString()));
			    ps.setString(6, getUsrprf());			    
			    ps.setString(7, getJobnm());		
			    ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));					    
			    ps.setLong(9, loan.getUniqueNumber());
			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("updateLoanRecordsUniqueNo()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return isUpdated;
	} 	
	
	//ICIL-179 Start
	
	private long loanRecord = 0;
	public long getLoanRecord(String company, String chdrnum) {
		
		StringBuilder sqlSelectloanRecord = new StringBuilder("SELECT COUNT(*) AS ROW_COUNT FROM LOANPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1' AND LOANTYPE IN('A', 'P')");
					      
			PreparedStatement psloanRecordSelect = null;
	        ResultSet rs = null;
	       
	        try {	
	        	 psloanRecordSelect = getConnection().prepareStatement(sqlSelectloanRecord.toString());
	        	 psloanRecordSelect.setString(1, company.trim());
	        	 psloanRecordSelect.setString(2, chdrnum.trim());	        	 
	        			        	 
	        	 rs = psloanRecordSelect.executeQuery();

	            while (rs.next()) {
	            	loanRecord = Long.parseLong(rs.getString("ROW_COUNT"));
	            	return loanRecord;
	            }
	        } catch (SQLException e) {
	        	LOGGER.error("findTotalCount()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psloanRecordSelect, rs);
	        }
	        return 0;	
	}
	//ICIL-179 End
	public Loanpf getLoanpfRecord(String company, String chdrnum,int loannumber) {
		StringBuilder sqlSelectloanRecord = new StringBuilder("SELECT * FROM LOANPF WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER <= ?");
					      
		PreparedStatement psloanRecordSelect = null;
        ResultSet rs = null;
        Loanpf loanpf = null;
        try {	
        	 psloanRecordSelect = getConnection().prepareStatement(sqlSelectloanRecord.toString());
        	 psloanRecordSelect.setString(1, company.trim());
        	 psloanRecordSelect.setString(2, chdrnum.trim());	        	 
        	 psloanRecordSelect.setInt(3, loannumber);        	 
        	 rs = psloanRecordSelect.executeQuery();

            while (rs.next()) {
            	loanpf = new Loanpf();
            	loanpf.setChdrnum(rs.getString("CHDRNUM"));
            	loanpf.setChdrcoy(rs.getString("CHDRCOY"));
            	loanpf.setLoannumber(rs.getInt("LOANNUMBER"));
            }
        } catch (SQLException e) {
			LOGGER.error("getLoanpfRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psloanRecordSelect, rs);
        }
        return loanpf;	
	}
		
	//ICIL-14 Start
	public boolean updateLoanValidflag(Loanpf loanpf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE LOANPF SET VALIDFLAG=? ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=? AND VALIDFLAG = '1' ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, loanpf.getValidflag());			    
			    ps.setString(2, loanpf.getChdrcoy());
			    ps.setString(3, loanpf.getChdrnum());
			    ps.setInt(4, loanpf.getLoannumber());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateLoanValidflag()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return true;
	}
	 public boolean deleteLoanpfRecord(Loanpf loanpf){
		 StringBuilder sb = new StringBuilder("");
			sb.append("DELETE FROM LOANPF");
			sb.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=? AND VALIDFLAG='1' AND UNIQUE_NUMBER = ? ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	

				    ps.setString(1, loanpf.getChdrcoy());
				    ps.setString(2, loanpf.getChdrnum());
				    ps.setInt(3, loanpf.getLoannumber());
				    ps.setLong(4, loanpf.getUniqueNumber());
				    ps.executeUpdate();				
					
				
			}catch (SQLException e) {
				LOGGER.error("updateLoanValidflag()", e); /* IJTI-1479 */	
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return true;
	 }
	 public boolean updateLoanpftranno(Loanpf loanpf){
		 StringBuilder sb = new StringBuilder("");
			sb.append("UPDATE LOANPF SET VALIDFLAG=? ");
			sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LOANNUMBER=?  AND UNIQUE_NUMBER = ?  ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	
				 ps.setString(1, loanpf.getValidflag());
				 ps.setString(2, loanpf.getChdrcoy());
				 ps.setString(3, loanpf.getChdrnum());
				 ps.setInt(4, loanpf.getLoannumber());
				 ps.setLong(5, loanpf.getUniqueNumber());   
				 ps.executeUpdate();				
					
				
			}catch (SQLException e) {
				LOGGER.error("updateLoanpftranno()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return true;
	 }
	 public List<Loanpf> loadValidflagRecord(String chdrcoy, String chdrnum, String loanNumber, String validflag){
		 StringBuilder sb = new StringBuilder("");
			sb.append("SELECT * FROM LOANPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LOANNUMBER = ? AND VALIDFLAG=? ORDER BY LTRANNO DESC");
		
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			List<Loanpf> loanList = new ArrayList<Loanpf>();
			try{
				ps = getPrepareStatement(sb.toString());
				ps.setString(1, StringUtil.fillSpace(chdrcoy.trim(), DD.chdrcoy.length));
 				ps.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length));
	 			ps.setString(3, StringUtil.fillSpace(loanNumber.trim(), DD.loannumber.length));
	 			ps.setString(4, StringUtil.fillSpace(validflag.trim(), DD.validflag.length));
				rs = ps.executeQuery();				
				while(rs.next()){
					Loanpf loanpf = new Loanpf();
					loanpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					loanpf.setChdrcoy(chdrcoy);
					loanpf.setChdrnum(rs.getString("CHDRNUM").trim());
					loanpf.setLoannumber(rs.getInt("LOANNUMBER"));
					loanpf.setLoantype(rs.getString("LOANTYPE").trim());
					loanpf.setLoancurr(rs.getString("LOANCURR").trim());
					loanpf.setValidflag(rs.getString("VALIDFLAG").trim());
					loanpf.setFtranno(rs.getLong("FTRANNO"));
					loanpf.setLtranno(rs.getLong("LTRANNO"));
					loanpf.setLoanorigam(rs.getDouble("LOANORIGAM"));
					loanpf.setLoansdate(rs.getInt("LOANSTDATE"));
					loanpf.setLstcaplamt(rs.getDouble("LSTCAPLAMT"));
					loanpf.setLstcapdate(rs.getInt("LSTCAPDATE"));
					loanpf.setNxtcapdate(rs.getInt("NXTCAPDATE"));
					loanpf.setLstintbdte(rs.getInt("LSTINTBDTE"));
					loanpf.setNxtintbdte(rs.getInt("NXTINTBDTE"));
					loanpf.setTplstmdty(rs.getDouble("TPLSTMDTY"));
					loanpf.setTermid(rs.getString("TERMID").trim());
					loanpf.setUser_t(rs.getInt("USER_T"));
					loanpf.setTrdt(rs.getInt("TRDT"));
					loanpf.setTrtm(rs.getInt("TRTM"));
					loanpf.setUsrprf(rs.getString("USRPRF").trim());
					loanpf.setJobnm(rs.getString("JOBNM").trim());
					loanpf.setDatime(rs.getDate("DATIME"));
					loanList.add(loanpf);		
				 }
			}catch (SQLException e) {
				LOGGER.error("updateLoanpftranno()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return loanList;
	 }
	 
	 public void insertLoanRec(List<Loanpf> loanList){
			StringBuilder sb = new StringBuilder("");
			sb.append("INSERT INTO VM1DTA.LOANPF(CHDRCOY, CHDRNUM, LOANNUMBER, LOANTYPE, LOANCURR, VALIDFLAG, FTRANNO, LTRANNO, LOANORIGAM, LOANSTDATE, LSTCAPLAMT, LSTCAPDATE, NXTCAPDATE, LSTINTBDTE, NXTINTBDTE, TPLSTMDTY, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME) ");
			sb.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "); 		
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;	
			try{
				ps = getPrepareStatement(sb.toString());	
				for (Loanpf loan : loanList) {
					ps.setString(1, loan.getChdrcoy());
					ps.setString(2, loan.getChdrnum());
					ps.setInt(3,loan.getLoannumber());
					ps.setString(4, loan.getLoantype());
					ps.setString(5, loan.getLoancurr());
					ps.setString(6, loan.getValidflag());
					ps.setLong(7, loan.getFtranno());
					ps.setLong(8, loan.getLtranno());
					ps.setDouble(9, loan.getLoanorigam());
					ps.setInt(10, loan.getLoansdate());
					ps.setDouble(11, loan.getLstcaplamt());
					ps.setInt(12, loan.getLstcapdate());
					ps.setInt(13, loan.getNxtcapdate());
					ps.setInt(14, loan.getLstintbdte());
				    ps.setInt(15, loan.getNxtintbdte());
					ps.setDouble(16, loan.getTplstmdty());
					ps.setString(17, loan.getTermid());
					ps.setInt(18, loan.getUser_t());
				    ps.setInt(19, Integer.parseInt(getCobolDate()));
				    ps.setInt(20, Integer.parseInt(varcom.vrcmTime.toString()));
				    ps.setString(21, getUsrprf());			    
				    ps.setString(22, getJobnm());		
				    ps.setTimestamp(23, getDatime());	
				    
				    ps.addBatch();				
				}		
				ps.executeBatch();
			}catch (SQLException e) {
				LOGGER.error("insertLoanRec()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);			
			}		
		}
}
