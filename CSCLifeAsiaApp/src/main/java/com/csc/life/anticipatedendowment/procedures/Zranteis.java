/*
 * File: Zranteis.java
 * Date: 30 August 2009 2:55:32
 * Author: Quipoz Limited
 *
 * Class transformed from ZRANTEIS.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.anticipatedendowment.tablestructures.Tr527rec;
import com.csc.life.anticipatedendowment.tablestructures.Tr528rec;
import com.csc.life.anticipatedendowment.tablestructures.Tr529rec;
import com.csc.life.anticipatedendowment.tablestructures.Tr530rec;
import com.csc.life.cashdividends.dataaccess.CovttrdTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.tablestructures.Th502rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  OVERVIEW
*  ========
*
*  This Subroutine creates Anticipated Endowment (ZRAE) Records
*  at both the Issue and the Component Add Stages. It is
*  accessed via Table T5671.
*
*  It will read TR527 with contract type to get mandatory options
*  and payment option.
*  It will use the payment option to read TR528 for verification.
*  before creating the file ZRAEPF, the benefit payment schedule
*  is retrieved from TR529 for term based coverage or TR530 for
*  age based coverage.
*  Depends on the table setup in T6640 and TH502 for the coverage
*  code, it will write new records to HSCD cash dividend file.
*****************************************************************
* </pre>
*/
public class Zranteis extends COBOLConvCodeModel {
	private static final Logger LOGGER = LoggerFactory.getLogger(Zranteis.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ZRANTEIS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaPcpdLoad = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaPcpdLoadDec = new PackedDecimalData(5, 2).init(0);
	private PackedDecimalData wsaaQuotient = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaRemainder = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaStartDate = new PackedDecimalData(8, 0);

		/* WSAA-FLAGS */
	private FixedLengthStringData wsaaTermFound = new FixedLengthStringData(1);
	private Validator termFound = new Validator(wsaaTermFound, "Y");

	private FixedLengthStringData wsaaAgeFound = new FixedLengthStringData(1);
	private Validator ageFound = new Validator(wsaaAgeFound, "Y");

	private FixedLengthStringData wsaaZeroPercent = new FixedLengthStringData(1);
	private Validator nonZeroPercent = new Validator(wsaaZeroPercent, "N");
		/* WSAA-SUBSCRIPTS */
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaNewSub = new ZonedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaHorz = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaVert = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaTermSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgeSub = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaZeroPays = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData pcpdArray = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaPercentage = FLSArrayPartOfStructure(10, 40, pcpdArray, 0);
	private ZonedDecimalData[][] wsaaPcpd = ZDArrayPartOfArrayStructure(8, 5, 0, wsaaPercentage, 0);
		/* ERRORS */
	private String g586 = "G586";
	private String g588 = "G588";
	private String hl49 = "HL49";
	private String h951 = "H951";
		/* FORMATS */
	private String zraerec = "ZRAEREC";
	private String covrrec = "COVRREC";
	private String liferec = "LIFEREC";
	private String itemrec = "ITEMREC";
	private String covttrdrec = "COVTTRDREC";
	private String hcsdrec = "HCSDREC";
		/* TABLES */
	private String t6640 = "T6640";
	private String tr529 = "TR529";
	private String tr530 = "TR530";
	private String tr527 = "TR527";
	private String tr528 = "TR528";
	private String th502 = "TH502";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*COVT - Traditional Business*/
	private CovttrdTableDAM covttrdIO = new CovttrdTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private Isuallrec isuallrec = new Isuallrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6640rec t6640rec = new T6640rec();
	private Th502rec th502rec = new Th502rec();
	private Tr527rec tr527rec = new Tr527rec();
	private Tr528rec tr528rec = new Tr528rec();
	private Tr529rec tr529rec = new Tr529rec();
	private Tr530rec tr530rec = new Tr530rec();
	private Varcom varcom = new Varcom();
		/*Anticipated Endownments Logical*/
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	boolean isEndMat = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit4999,
		exit4299,
		exit6690,
		errorProg610
	}

	public Zranteis() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void mainline1000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		isEndMat = FeaConfg.isFeatureExist(isuallrec.company.toString(), "PRPRO001", appVars, "IT");
		readCovrRecord2000();
		if(!isEndMat || (isEndMat && isEQ(chdrlnbIO.getStatcode(),"IF"))){
			dealWithZraeRecords3000();
		}
		else {
			//IBPLIFE-6444 Added to update the transaction number
			updateTransactionNumber();
		}
		updateFields6000();
		rewriteCovrRecord7000();
		exitProgram();
	}

protected void readCovrRecord2000()
	{
		start2010();
	}

protected void start2010()
	{
		covrIO.setChdrcoy(isuallrec.company);
		covrIO.setChdrnum(isuallrec.chdrnum);
		covrIO.setLife(isuallrec.life);
		covrIO.setCoverage(isuallrec.coverage);
		covrIO.setRider(isuallrec.rider);
		covrIO.setPlanSuffix(isuallrec.planSuffix);
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void dealWithZraeRecords3000()
	{
		/*START*/
		tableReads4000();
		writeZraeRecord5000();
		/*EXIT*/
	}

protected void tableReads4000()
	{
		try {
			start4001();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void start4001()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tr527);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		tr527rec.tr527Rec.set(itemIO.getGenarea());
		if (isNE(tr527rec.payOption05,SPACES)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(isuallrec.company);
			itemIO.setItemtabl(tr528);
			itemIO.setItemitem(tr527rec.payOption05);
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			tr528rec.tr528Rec.set(itemIO.getGenarea());
		}
		else {
			tr528rec.paymentMethod.set(SPACES);
		}
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tr529);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),tr529)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())) {
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			tr529rec.tr529Rec.set(itdmIO.getGenarea());
			loadTr529Array4100();
		}
		if (isNE(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4999);
		}
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tr530);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),tr530)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			tr530rec.tr530Rec.set(itdmIO.getGenarea());
			loadTr530Array4200();
		}
	}

protected void loadTr529Array4100()
	{
		start4101();
	}

protected void start4101()
	{
		wsaaVert.set(1);
		wsaaHorz.set(1);
		for (wsaaPcpdLoad.set(1); !(isGT(wsaaPcpdLoad,80)); wsaaPcpdLoad.add(1)){
			wsaaPcpd[wsaaVert.toInt()][wsaaHorz.toInt()].set(tr529rec.zrpcpd[wsaaPcpdLoad.toInt()]);
			wsaaHorz.add(1);
			wsaaPcpdLoadDec.set(wsaaPcpdLoad);
			compute(wsaaQuotient, 2).setDivide(wsaaPcpdLoadDec, (8));
			wsaaRemainder.setRemainder(wsaaQuotient);
			if (isEQ(wsaaRemainder,ZERO)) {
				wsaaVert.add(1);
				wsaaHorz.set(1);
			}
		}
		for (wsaaTermSub.set(1); !(isGT(wsaaTermSub,10)
		|| termFound.isTrue()); wsaaTermSub.add(1)){
			if (isGTE(tr529rec.zrterm[wsaaTermSub.toInt()],covrIO.getRiskCessTerm())) {
				wsaaTermFound.set("Y");
				wsaaTermSub.subtract(1);
			}
		}
		if (isNE(wsaaTermFound,"Y")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h951);
			fatalError600();
		}
		wsaaStartDate.set(covrIO.getCrrcd());
		for (wsaaHorz.set(1); !(isGT(wsaaHorz,8)); wsaaHorz.add(1)){
			zraeIO.setPrcnt(wsaaHorz, wsaaPcpd[wsaaTermSub.toInt()][wsaaHorz.toInt()]);
			zraeIO.setPaydte(wsaaHorz, varcom.vrcmMaxDate);
			zraeIO.setPaid(wsaaHorz, ZERO);
			zraeIO.setPaymmeth(wsaaHorz, tr528rec.paymentMethod);
			zraeIO.setZrpayopt(wsaaHorz, tr527rec.payOption05);
			if (isEQ(tr529rec.zrnoyrs[wsaaHorz.toInt()],0)) {
				zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
			}
			else {
				datcon2rec.freqFactor.set(tr529rec.zrnoyrs[wsaaHorz.toInt()]);
				callDatcon24300();
			}
		}
	}

protected void loadTr530Array4200()
	{
		try {
			start4201();
			checkDates4250();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void start4201()
	{
		wsaaVert.set(1);
		wsaaHorz.set(1);
		for (wsaaPcpdLoad.set(1); !(isGT(wsaaPcpdLoad,80)); wsaaPcpdLoad.add(1)){
			wsaaPcpd[wsaaVert.toInt()][wsaaHorz.toInt()].set(tr530rec.zrpcpd[wsaaPcpdLoad.toInt()]);
			wsaaHorz.add(1);
			wsaaPcpdLoadDec.set(wsaaPcpdLoad);
			compute(wsaaQuotient, 2).setDivide(wsaaPcpdLoadDec, (8));
			wsaaRemainder.setRemainder(wsaaQuotient);
			if (isEQ(wsaaRemainder,ZERO)) {
				wsaaVert.add(1);
				wsaaHorz.set(1);
			}
		}
		for (wsaaAgeSub.set(1); !(isGT(wsaaAgeSub,10)
		|| ageFound.isTrue()); wsaaAgeSub.add(1)){
			if (isGTE(covrIO.getAnbAtCcd(),tr530rec.zragfr[wsaaAgeSub.toInt()])
			&& isLTE(covrIO.getAnbAtCcd(),tr530rec.zragto[wsaaAgeSub.toInt()])) {
				wsaaAgeFound.set("Y");
				wsaaAgeSub.subtract(1);
			}
		}
		readLife4400();
		wsaaStartDate.set(lifeIO.getCltdob());
		for (wsaaHorz.set(1); !(isGT(wsaaHorz,8)); wsaaHorz.add(1)){
			zraeIO.setPrcnt(wsaaHorz, wsaaPcpd[wsaaAgeSub.toInt()][wsaaHorz.toInt()]);
			zraeIO.setPaydte(wsaaHorz, varcom.vrcmMaxDate);
			zraeIO.setPaid(wsaaHorz, ZERO);
			zraeIO.setPaymmeth(wsaaHorz, SPACES);
			zraeIO.setZrpayopt(wsaaHorz, tr527rec.payOption05);
			if (isEQ(tr530rec.zrage[wsaaHorz.toInt()],ZERO)) {
				zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
			}
			else {
				datcon2rec.freqFactor.set(tr530rec.zrage[wsaaHorz.toInt()]);
				callDatcon24300();
			}
		}
	}

protected void checkDates4250()
	{
		wsaaZeroPercent.set("Y");
		for (wsaaSub.set(1); !(isGT(wsaaSub,8)
		|| nonZeroPercent.isTrue()); wsaaSub.add(1)){
			if (isNE(zraeIO.getPrcnt(wsaaSub),ZERO)) {
				wsaaZeroPercent.set("N");
				wsaaSub.subtract(1);
			}
		}
		if (isEQ(wsaaSub,1)) {
			goTo(GotoLabel.exit4299);
		}
		compute(wsaaZeroPays, 0).set(sub(wsaaSub,1));
		while ( !(isGT(wsaaSub,8))) {
			compute(wsaaNewSub, 0).set(sub(wsaaSub,wsaaZeroPays));
			zraeIO.setPrcnt(wsaaNewSub, zraeIO.getPrcnt(wsaaSub));
			zraeIO.setZrduedte(wsaaNewSub, zraeIO.getZrduedte(wsaaSub));
			if (isEQ(wsaaSub,8)) {
				wsaaNewSub.add(1);
				while ( !(isGT(wsaaNewSub,8))) {
					zraeIO.setZrduedte(wsaaSub, varcom.vrcmMaxDate);
					zraeIO.setPrcnt(wsaaSub, ZERO);
					wsaaNewSub.add(1);
				}

			}
			wsaaSub.add(1);
		}

	}

protected void callDatcon24300()
	{
		/*START*/
		datcon2rec.intDate1.set(wsaaStartDate);
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isGT(datcon2rec.intDate2,covrIO.getRiskCessDate())) {
			zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
		}
		else {
			zraeIO.setZrduedte(wsaaHorz, datcon2rec.intDate2);
		}
		/*EXIT*/
	}

protected void readLife4400()
	{
		start4401();
	}

protected void start4401()
	{
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(covrIO.getChdrcoy());
		lifeIO.setChdrnum(covrIO.getChdrnum());
		lifeIO.setLife(covrIO.getLife());
		lifeIO.setJlife(covrIO.getJlife());
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
	}

protected void writeZraeRecord5000()
	{
		start5001();
	}

protected void start5001()
	{
		zraeIO.setChdrcoy(isuallrec.company);
		zraeIO.setChdrnum(isuallrec.chdrnum);
		zraeIO.setLife(isuallrec.life);
		zraeIO.setCoverage(isuallrec.coverage);
		zraeIO.setRider(isuallrec.rider);
		zraeIO.setPlanSuffix(isuallrec.planSuffix);
		zraeIO.setValidflag("1");
		zraeIO.setTranno(chdrlnbIO.getTranno());
		zraeIO.setCurrfrom(isuallrec.effdate);
		zraeIO.setCurrto(varcom.vrcmMaxDate);
		zraeIO.setTotamnt(ZERO);
		zraeIO.setNextPaydate(zraeIO.getZrduedte01());
		zraeIO.setPaycurr(chdrlnbIO.getCntcurr());
		zraeIO.setPayclt(chdrlnbIO.getCownnum());
		zraeIO.setFunction(varcom.writr);
		zraeIO.setFormat(zraerec);	
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		
		
	}

protected void updateFields6000()
	{
		/*START*/
		readT66406100();
		setBonusIndicator6200();
		annivProcessDate6300();
		bonusDeclareDate6400();
		hcsdDetails6600();
		/*EXIT*/
	}

protected void readT66406100()
	{
		start6110();
	}

protected void start6110()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6640)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getCrtable());
			syserrrec.statuz.set(g588);
			fatalError600();
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());
		}
	}

protected void setBonusIndicator6200()
	{
		bonusIndicator6210();
		bonusMethod6220();
	}

protected void bonusIndicator6210()
	{
		if (isNE(t6640rec.zbondivalc,SPACES)) {
			readTh5026500();
		}
		else {
			th502rec.th502Rec.set(SPACES);
		}
	}

protected void bonusMethod6220()
	{
		if (isNE(t6640rec.zbondivalc,SPACES)) {
			//if (isEQ(th502rec.zrevbonalc,"X")) {
			if (isEQ(th502rec.zrevbonalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
				if (isEQ(t6640rec.revBonusMeth,SPACES)) {
					syserrrec.statuz.set(g586);
					fatalError600();
				}
			}
		        //if(isEQ(th502rec.zcshdivalc,"X"))
				if(isEQ(th502rec.zcshdivalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
				if(isEQ(t6640rec.zcshdivmth,SPACES)) {
					syserrrec.statuz.set(g586);
					fatalError600();
				}
			}
		}
		/*SET-BONUS-INDICATOR*/
		covrIO.setBonusInd(t6640rec.zbondivalc);
		/*EXIT*/
	}

protected void annivProcessDate6300()
	{
		/*START*/
		covrIO.setAnnivProcDate(99999999);
		datcon2rec.intDate1.set(covrIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		covrIO.setAnnivProcDate(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void bonusDeclareDate6400()
	{
		/*START*/
		if (isEQ(th502rec.zconannalc,"X")) {
			covrIO.setUnitStatementDate(covrIO.getCrrcd());
		}
		else {
			covrIO.setUnitStatementDate(99999999);
		}
		/*EXIT*/
	}

protected void readTh5026500()
	{
		start6510();
	}

protected void start6510()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(th502);
		itemIO.setItemitem(t6640rec.zbondivalc);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th502rec.th502Rec.set(itemIO.getGenarea());
		if (isEQ(th502rec.zrevbonalc,SPACES)
		&& isEQ(th502rec.zcshdivalc,SPACES)) {
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void hcsdDetails6600()
	{
		try {
			hcsdDet6600();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void hcsdDet6600()
	{
	    //if (isNE(th502rec.zcshdivalc,"X")){
		if (isNE(th502rec.zcshdivalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
			goTo(GotoLabel.exit6690);
		}
		covttrdIO.setDataKey(SPACES);
		covttrdIO.setChdrcoy(isuallrec.company);
		covttrdIO.setChdrnum(isuallrec.chdrnum);
		covttrdIO.setLife(isuallrec.life);
		covttrdIO.setCoverage(isuallrec.coverage);
		covttrdIO.setRider(isuallrec.rider);
		covttrdIO.setPlanSuffix(isuallrec.planSuffix);
		covttrdIO.setFormat(covttrdrec);
		covttrdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covttrdIO);
		if (isNE(covttrdIO.getStatuz(),varcom.oK)
		&& isNE(covttrdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covttrdIO.getStatuz());
			syserrrec.params.set(covttrdIO.getParams());
			fatalError600();
		}
		if (isEQ(covttrdIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit6690);
		}
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(isuallrec.company);
		hcsdIO.setChdrnum(isuallrec.chdrnum);
		hcsdIO.setLife(isuallrec.life);
		hcsdIO.setCoverage(isuallrec.coverage);
		hcsdIO.setRider(isuallrec.rider);
		hcsdIO.setPlanSuffix(isuallrec.planSuffix);
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(covrIO.getTranno());
		hcsdIO.setEffdate(covrIO.getCrrcd());
		hcsdIO.setZdivopt(covttrdIO.getZdivopt());
		hcsdIO.setZcshdivmth(t6640rec.zcshdivmth);
		hcsdIO.setPaycoy(covttrdIO.getPaycoy());
		hcsdIO.setPayclt(covttrdIO.getPayclt());
		hcsdIO.setPaymth(covttrdIO.getPaymth());
		hcsdIO.setFacthous(covttrdIO.getFacthous());
		hcsdIO.setBankkey(covttrdIO.getBankkey());
		hcsdIO.setBankacckey(covttrdIO.getBankacckey());
		hcsdIO.setPaycurr(covttrdIO.getPaycurr());
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			fatalError600();
		}
	}

protected void rewriteCovrRecord7000()
	{
		start7000();
	}

protected void start7000()
	{
		covrIO.setChdrcoy(isuallrec.company);
		covrIO.setChdrnum(isuallrec.chdrnum);
		covrIO.setLife(isuallrec.life);
		covrIO.setCoverage(isuallrec.coverage);
		covrIO.setRider(isuallrec.rider);
		covrIO.setPlanSuffix(isuallrec.planSuffix);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		isuallrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}

protected void updateTransactionNumber() {
		zraeIO.setChdrcoy(isuallrec.company);
		zraeIO.setChdrnum(isuallrec.chdrnum);
		zraeIO.setLife(isuallrec.life);
		zraeIO.setCoverage(isuallrec.coverage);
		zraeIO.setRider(isuallrec.rider);
		zraeIO.setPlanSuffix(isuallrec.planSuffix);
		zraeIO.setValidflag("1");
		zraeIO.setFunction(varcom.readh);
		zraeIO.setFormat(zraerec);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zraeIO.getValidflag(), "1")) {
			zraeIO.setTranno(chdrlnbIO.getTranno());
			zraeIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(zraeIO.getParams());
				fatalError600();
			}
		}
	}
}
