package com.csc.life.anticipatedendowment.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sa542protect extends ScreenRecord {
	 

		public static final String ROUTINE = QPUtilities.getThisClass();
		public static final boolean overlay = true;
		public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
		public static RecInfo lrec = new RecInfo();
		static {
			lrec.recordName = ROUTINE + "Written";
			COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
		}

	/**
	 * Writes a record to the screen.
	 * @param errorInd - will be set on if an error occurs
	 * @param noRecordFoundInd - will be set on if no changed record is found
	 */
		public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sa542ScreenVars sv = (Sa542ScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sa542protectWritten, null, av, null, ind2, ind3);
		}

		public static void read(Indicator ind2, Indicator ind3) {}

		public static void clearClassString(VarModel pv) {
			Sa542ScreenVars screenVars = (Sa542ScreenVars)pv;
		}

	/**
	 * Clear all the variables in Sa542protect
	 */
		public static void clear(VarModel pv) {
			Sa542ScreenVars screenVars = (Sa542ScreenVars) pv;
		}


}
