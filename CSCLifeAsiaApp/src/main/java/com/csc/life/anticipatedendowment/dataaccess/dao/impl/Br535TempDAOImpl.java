package com.csc.life.anticipatedendowment.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.Br535TempDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Br535DTO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br535TempDAOImpl extends BaseDAOImpl<Object> implements Br535TempDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br535TempDAOImpl.class);	

	

	//ILIFE-8840 start
	@Override
	public List<Loanpf> loadDataByNEWINTCP(String tableID, String memName,int intBatchExtractSize, int batchID) {

		 StringBuilder sb = new StringBuilder();
	        sb.append("SELECT * FROM (");
	        sb.append("SELECT floor((row_number()over(ORDER BY LO.CHDRCOY ASC, LO.CHDRNUM ASC, LO.LOANNUMBER DESC)-1)/?) batchnum, LO.* ");
	        sb.append("from (select LO.CHDRCOY,LO.CHDRNUM, LO.LOANNUMBER, LO.LOANTYPE, LO.LOANCURR, LO.VALIDFLAG, LO.LOANORIGAM, LO.LOANSTDATE, LO.LSTCAPLAMT, ");
	        sb.append("LO.LSTCAPDATE, LO.NXTCAPDATE, LO.LSTINTBDTE, LO.NXTINTBDTE, ");
			sb.append("CH.TRANNO, CH.CNTTYPE, CH.CHDRPFX, CH.SERVUNIT, CH.OCCDATE, CH.CCDATE, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM,");
			sb.append("CH.COLLCHNL, CH.CNTBRANCH, CH.CNTCURR, CH.POLSUM, CH.POLINC, CH.AGNTPFX, CH.AGNTCOY, CH.AGNTNUM, CL.CLNTPFX,");
			sb.append("CL.CLNTCOY, CL.CLNTNUM,");
			sb.append("PY.MANDREF, PY.PTDATE, PY.BILLCHNL, PY.BILLFREQ, PY.BILLCURR, PY.NEXTDATE, ");
			sb.append("MA.BANKKEY, MA.BANKACCKEY, MA.MANDSTAT, CB.FACTHOUS ");			
			sb.append("FROM ");
			sb.append(tableID);
			sb.append(" LO ");
			sb.append("INNER JOIN CHDRPF CH ON  CH.CHDRCOY = LO.CHDRCOY  AND CH.CHDRNUM=LO.CHDRNUM  AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG in ('1','3') ");
			sb.append("INNER JOIN CLRRPF CL ON CL.FOREPFX = CH.CHDRPFX AND CL.FORECOY = CH.CHDRCOY AND CL.FORENUM = concat(CH.CHDRNUM, '1') AND CL.CLRRROLE = 'PY' ");
			sb.append("INNER JOIN PAYRPF PY ON PY.CHDRCOY = LO.CHDRCOY  AND PY.CHDRNUM=LO.CHDRNUM AND PY.PAYRSEQNO = 1 AND PY.VALIDFLAG = '1' ");
			sb.append("LEFT OUTER JOIN MANDPF MA ON MA.PAYRCOY = PY.CHDRCOY AND MA.PAYRNUM = PY.CHDRNUM AND MA.MANDREF = PY.MANDREF AND MA.VALIDFLAG = '1' ");
			sb.append("LEFT OUTER JOIN CLBAPF CB ON CB.CLNTCOY = PY.CHDRCOY AND CB.CLNTNUM = PY.CHDRNUM AND CB.BANKKEY = MA.BANKKEY AND CB.BANKACCKEY = MA.BANKACCKEY ");
			sb.append("AND CB.CLNTPFX = 'CN' AND NOT (CB.VALIDFLAG <> '1') ");
			sb.append("WHERE LO.MEMBER_NAME = ? ");
	        sb.append(") LO)cc where cc.batchnum=?  ");
	        LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Loanpf> loanList = new ArrayList<>();
	
		try{
			ps = getPrepareStatement(sb.toString());
		
			ps.setInt(1, intBatchExtractSize);
			 ps.setString(2, memName); 
        	ps.setInt(3, batchID);
		//ILIFE-8840 end	
			rs = ps.executeQuery();
			while(rs.next()){

				Loanpf loanpf = new Loanpf();
				loanpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				loanpf.setChdrnum(rs.getString("CHDRNUM").trim());
				loanpf.setLoannumber(rs.getInt("LOANNUMBER"));
				loanpf.setLoantype(rs.getString("LOANTYPE").trim());
				loanpf.setLoancurr(rs.getString("LOANCURR").trim());
				loanpf.setValidflag(rs.getString("VALIDFLAG").trim());
				loanpf.setLoanorigam(rs.getDouble("LOANORIGAM"));
				loanpf.setLoansdate(rs.getInt("LOANSTDATE"));
				loanpf.setLstcaplamt(rs.getDouble("LSTCAPLAMT"));
				loanpf.setLstcapdate(rs.getInt("LSTCAPDATE"));
				loanpf.setNxtcapdate(rs.getInt("NXTCAPDATE"));
				loanpf.setLstintbdte(rs.getInt("LSTINTBDTE"));
				loanpf.setNxtintbdte(rs.getInt("NXTINTBDTE"));
				Br535DTO br535DTO = new Br535DTO();
				br535DTO.setTranno(rs.getInt("TRANNO"));
				br535DTO.setCnttype(rs.getString("CNTTYPE").trim());
				br535DTO.setChdrpfx(rs.getString("CHDRPFX").trim());
				br535DTO.setServunit(rs.getString("SERVUNIT").trim());
				br535DTO.setOccdate(rs.getInt("OCCDATE"));
				br535DTO.setCcdate(rs.getInt("CCDATE"));
				br535DTO.setCownpfx(rs.getString("COWNPFX").trim());
				br535DTO.setCowncoy(rs.getString("COWNCOY").trim());
				br535DTO.setCownnum(rs.getString("COWNNUM").trim());				
				br535DTO.setCollchnl(rs.getString("COLLCHNL").trim());
				br535DTO.setCntbranch(rs.getString("CNTBRANCH").trim());
				br535DTO.setCntcurr(rs.getString("CNTCURR").trim());
				br535DTO.setPolinc(rs.getObject("POLINC") != null ? (rs.getInt("POLINC")) : 0);
				br535DTO.setPolsum(rs.getObject("POLSUM") != null ? (rs.getInt("POLSUM")) : 0);
				br535DTO.setAgntpfx(rs.getString("AGNTPFX").trim());				
				br535DTO.setAgntcoy(rs.getString("AGNTCOY").trim());
				br535DTO.setAgntnum(rs.getString("AGNTNUM").trim());
				br535DTO.setClntpfx(rs.getString("CLNTPFX").trim());
				br535DTO.setClntcoy(rs.getString("CLNTCOY").trim());
				br535DTO.setClntnum(rs.getString("CLNTNUM").trim());
				br535DTO.setMandref(rs.getString("MANDREF").trim());
				br535DTO.setPtdate(rs.getInt("PTDATE"));
				br535DTO.setBillchnl(rs.getString("BILLCHNL").trim());
				br535DTO.setBillfreq(rs.getString("BILLFREQ").trim());
				br535DTO.setBillcurr(rs.getString("BILLCURR").trim());
				br535DTO.setNextdate(rs.getInt("NEXTDATE"));	
				br535DTO.setBankkey(rs.getObject("BANKKEY") != null ? (rs.getString("BANKKEY")) : "");
				br535DTO.setBankacckey(rs.getObject("BANKACCKEY") != null ? (rs.getString("BANKACCKEY")) : "");
				br535DTO.setMandstat(rs.getObject("MANDSTAT") != null ? (rs.getString("MANDSTAT")) : "");	
				br535DTO.setFacthous(rs.getObject("FACTHOUS") != null ? (rs.getString("FACTHOUS")) : "");
				loanpf.setBr535DTO(br535DTO);
				loanList.add(loanpf);	
			}
		}catch (SQLException e) {
				LOGGER.error("loadDataByEffdate()", e);	
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
			return loanList;
	}
	
}
