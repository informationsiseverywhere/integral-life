/*
 * File: Zrcshopt.java
 * Date: 30 August 2009 2:55:48
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRCSHOPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.life.anticipatedendowment.dataaccess.ZrlndscTableDAM;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This program will use the Payments due from the Anticipated
*  Endowment Release to pay the existing Loans.
*
*
*****************************************************************
* </pre>
*/
public class Zrcshopt extends COBOLConvCodeModel {
	private static final Logger LOGGER = LoggerFactory.getLogger(Zrcshopt.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("ZRCSHOPT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaContractAmt = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaLoanPaid = new PackedDecimalData(13, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaHeldLoanNumber = new PackedDecimalData(2, 0);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String zrlndscrec = "ZRLNDSCREC";
		/* TABLES */
	private String t5645 = "T5645";
	private Cashedrec cashedrec = new Cashedrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T5645rec t5645rec = new T5645rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Varcom varcom = new Varcom();
	private Zrcshoprec zrcshoprec = new Zrcshoprec();
		/*Descending Loan Number*/
	private ZrlndscTableDAM zrlndscIO = new ZrlndscTableDAM();

	public Zrcshopt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		zrcshoprec.rec = convertAndSetParam(zrcshoprec.rec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void mainline0000()
	{
		para0010();
		exit0090();
	}

protected void para0010()
	{
		zrcshoprec.statuz.set(varcom.oK);
		wsaaTranDate.set(getCobolDate());
		wsaaTranTime.set(999999);
		payOffDebt1000();
		processCashAccount2000();
	}

protected void exit0090()
	{
		exitProgram();
	}

protected void payOffDebt1000()
	{
		start1010();
	}

protected void start1010()
	{
		readT56451100();
		readLoanRec1200();
		wsaaGlCompany.set(zrcshoprec.chdrcoy);
		wsaaGlCurrency.set(SPACES);
		wsaaTranTermid.set(zrcshoprec.termid);
		wsaaTranUser.set(zrcshoprec.user);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(zrcshoprec.chdrcoy);
		wsaaTranEntity.set(zrcshoprec.chdrnum);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(zrcshoprec.batckey);
		cashedrec.doctNumber.set(zrcshoprec.chdrnum);
		cashedrec.doctCompany.set(zrcshoprec.batccoy);
		cashedrec.trandate.set(zrcshoprec.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(1);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(zrlndscIO.getLoanCurrency());
		cashedrec.dissrate.set(0);
		cashedrec.trandesc.set(zrcshoprec.trandesc);
		cashedrec.chdrcoy.set(zrcshoprec.chdrcoy);
		cashedrec.chdrnum.set(zrcshoprec.chdrnum);
		cashedrec.tranno.set(zrcshoprec.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.trankey.set(wsaaTrankey);
		wsaaContractAmt.set(zrcshoprec.pymt);
		cashedrec.language.set(zrcshoprec.language);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)
		|| isEQ(wsaaContractAmt,0)); wsaaSub1.add(1)){
			loans1300();
		}
		wsaaSequenceNo.set(cashedrec.transeq);
		compute(wsaaLoanPaid, 2).set(sub(zrcshoprec.pymt,wsaaContractAmt));
		invalidateLoanRec1400();
	}

protected void readT56451100()
	{
		start1110();
	}

protected void start1110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zrcshoprec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			zrcshoprec.errorFormat.set(itemIO.getParams());
			zrcshoprec.statuz.set(itemIO.getStatuz());
			exit0090();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readLoanRec1200()
	{
		start1210();
	}

protected void start1210()
	{
		zrlndscIO.setParams(SPACES);
		zrlndscIO.setChdrcoy(zrcshoprec.chdrcoy);
		zrlndscIO.setChdrnum(zrcshoprec.chdrnum);
		zrlndscIO.setLoanNumber(99);
		zrlndscIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrlndscIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrlndscIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		zrlndscIO.setFormat(zrlndscrec);
		SmartFileCode.execute(appVars, zrlndscIO);
		if (isNE(zrlndscIO.getStatuz(),varcom.oK)
		|| isNE(zrlndscIO.getChdrcoy(),zrcshoprec.chdrcoy)
		|| isNE(zrlndscIO.getChdrnum(),zrcshoprec.chdrnum)) {
			zrcshoprec.errorFormat.set(zrlndscIO.getParams());
			zrcshoprec.statuz.set(zrlndscIO.getStatuz());
			exit0090();
		}
		wsaaHeldLoanNumber.set(zrlndscIO.getLoanNumber());
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(zrcshoprec.chdrcoy);
		totloanrec.chdrnum.set(zrcshoprec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(zrcshoprec.effdate);
		totloanrec.tranno.set(zrcshoprec.tranno);
		totloanrec.batchkey.set(zrcshoprec.batckey);
		totloanrec.tranTerm.set(zrcshoprec.termid);
		totloanrec.tranDate.set(wsaaTranDate);
		totloanrec.tranTime.set(wsaaTranTime);
		totloanrec.tranUser.set(zrcshoprec.user);
		totloanrec.language.set(zrcshoprec.language);
		totloanrec.postFlag.set("Y");
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			zrcshoprec.errorFormat.set("HRTOTLON");
			zrcshoprec.statuz.set(totloanrec.statuz);
			exit0090();
		}
	}

protected void loans1300()
	{
		start1310();
	}

protected void start1310()
	{
		wsaaGlMap.set(t5645rec.glmap[wsaaSub1.toInt()]);
		cashedrec.sign.set(t5645rec.sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaContractAmt);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			zrcshoprec.statuz.set(cashedrec.statuz);
			zrcshoprec.errorFormat.set("LOANPYMT");
			exit0090();
		}
		wsaaContractAmt.set(cashedrec.docamt);
	}

protected void invalidateLoanRec1400()
	{
		start1410();
	}

protected void start1410()
	{
		zrlndscIO.setParams(SPACES);
		zrlndscIO.setChdrcoy(zrcshoprec.chdrcoy);
		zrlndscIO.setChdrnum(zrcshoprec.chdrnum);
		zrlndscIO.setLoanNumber(wsaaHeldLoanNumber);
		zrlndscIO.setFunction(varcom.readh);
		zrlndscIO.setFormat(zrlndscrec);
		SmartFileCode.execute(appVars, zrlndscIO);
		if (isNE(zrlndscIO.getStatuz(),varcom.oK)) {
			zrcshoprec.errorFormat.set(zrlndscIO.getParams());
			zrcshoprec.statuz.set(zrlndscIO.getStatuz());
			exit0090();
		}
		zrlndscIO.setLastTranno(zrcshoprec.tranno);
		zrlndscIO.setLoanOriginalAmount(wsaaContractAmt);
		zrlndscIO.setLastCapnLoanAmt(wsaaContractAmt);
		if (isEQ(wsaaContractAmt,0)) {
			zrlndscIO.setValidflag("2");
		}
		zrlndscIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, zrlndscIO);
		if (isNE(zrlndscIO.getStatuz(),varcom.oK)) {
			zrcshoprec.errorFormat.set(zrlndscIO.getParams());
			zrcshoprec.statuz.set(zrlndscIO.getStatuz());
			exit0090();
		}
	}

protected void processCashAccount2000()
	{
		start2010();
	}

protected void start2010()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(zrcshoprec.batccoy);
		lifacmvrec.batcbrn.set(zrcshoprec.batcbrn);
		lifacmvrec.batcactyr.set(zrcshoprec.batcactyr);
		lifacmvrec.batcactmn.set(zrcshoprec.batcactmn);
		lifacmvrec.batctrcde.set(zrcshoprec.batctrcde);
		lifacmvrec.batcbatch.set(zrcshoprec.batcbatch);
		lifacmvrec.rldgcoy.set(zrcshoprec.chdrcoy);
		lifacmvrec.genlcoy.set(zrcshoprec.chdrcoy);
		lifacmvrec.rdocnum.set(zrcshoprec.chdrnum);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(zrlndscIO.getLoanCurrency());
		lifacmvrec.origamt.set(wsaaLoanPaid);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(zrcshoprec.tranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(zrcshoprec.effdate);
		lifacmvrec.tranref.set(zrcshoprec.chdrnum);
		lifacmvrec.trandesc.set(zrcshoprec.trandesc);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(zrcshoprec.chdrnum);
		wsaaLoanNumber.set(zrlndscIO.getLoanNumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.termid.set(zrcshoprec.termid);
		lifacmvrec.transactionDate.set(wsaaTranDate);
		lifacmvrec.transactionTime.set(wsaaTranTime);
		lifacmvrec.user.set(zrcshoprec.user);
		lifacmvrec.sacscode.set(zrcshoprec.sacscode);
		lifacmvrec.sacstyp.set(zrcshoprec.sacstyp);
		lifacmvrec.glcode.set(zrcshoprec.glcode);
		lifacmvrec.glsign.set(zrcshoprec.sign);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			zrcshoprec.errorFormat.set(lifacmvrec.lifacmvRec);
			zrcshoprec.statuz.set(lifacmvrec.statuz);
			exit0090();
		}
	}
}
