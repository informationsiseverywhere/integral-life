/*
 * File: Autopays.java
 * Date: 29 August 2009 20:16:01
 * Author: Quipoz Limited
 * 
 * Class transformed from AUTOPAYS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This program is basically making use of PAYREQ sub-routine
*  to create a Payment requisition.
*
*
*****************************************************************
* </pre>
*/
public class Autopays extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("AUTOPAYS");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Payreqrec payreqrec = new Payreqrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrcshoprec zrcshoprec = new Zrcshoprec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9020
	}

	public Autopays() {
		super();
	}

public void mainline(Object... parmArray)
	{
		zrcshoprec.rec = convertAndSetParam(zrcshoprec.rec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit099();
	}

protected void para010()
	{
		zrcshoprec.statuz.set(varcom.oK);
		payreqrec.effdate.set(zrcshoprec.effdate);
		payreqrec.paycurr.set(zrcshoprec.paycurr);
		payreqrec.pymt.set(zrcshoprec.pymt);
		payreqrec.trandesc.set(zrcshoprec.trandesc);
		payreqrec.bankkey.set(zrcshoprec.bankkey);
		payreqrec.bankacckey.set(zrcshoprec.bankacckey);
		payreqrec.frmGlDet.set(zrcshoprec.frmGlDet);
		payreqrec.termid.set(zrcshoprec.termid);
		payreqrec.user.set(zrcshoprec.user);
		payreqrec.reqntype.set(zrcshoprec.reqntype);
		payreqrec.clntcoy.set(zrcshoprec.clntcoy);
		payreqrec.clntnum.set(zrcshoprec.clntnum);
		payreqrec.bankcode.set(zrcshoprec.bankcode);
		payreqrec.cheqno.set(zrcshoprec.cheqno);
		payreqrec.batckey.set(zrcshoprec.batckey);
		payreqrec.tranref.set(zrcshoprec.tranref);
		payreqrec.language.set(zrcshoprec.language);
		payreqrec.zbatctrcde.set(zrcshoprec.batctrcde);	
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(payreqrec.statuz);
			syserrrec.params.set(payreqrec.rec);
			fatalError9000();
		}
	}

protected void exit099()
	{
		exitProgram();
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		zrcshoprec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
