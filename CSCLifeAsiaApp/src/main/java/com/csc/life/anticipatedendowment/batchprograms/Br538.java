/*
 * File: Br538.java
 * Date: 29 August 2009 22:28:17
 * Author: Quipoz Limited
 * 
 * Class transformed from BR538.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.contractservicing.dataaccess.LonxpfTableDAM;

import com.csc.life.regularprocessing.recordstructures.P6671par;

import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;

import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (For L2NEWINTBL and L2NEWINTCP)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the L2NEWINTBL and L2NEWINTCP
* Program, BR539 and BR535.
* It is run in single-thread, and writes the selected LOANPF
* records to multiple members . Each of these members will be read
* by a copy of BR539 run in multi-thread environment.
*
* SQL will be used to access the LOANPF physical file. The
* splitter program will extract LOANPF records that meet the
* following criteria:
*
* i    Chdroy     =  <run-company>
* ii   Validflag  =  '1'
* iii  Payment Date  <>  0
* iv   Payment Date  <=  Batch Effective Date
* v    Chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
*      order by chdrcoy, chdrnum
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* A Splitter Program's purpose is to find a pending transactions f om
* the database and create multiple extract files for processing
* by multiple copies of the subsequent program, each running in it s
* own thread. Each subsequent program therefore processes a descre e
* portion of the total transactions.
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidily isolate potential transactions, not
* to process the transaction. The primary concern for Splitter
* program design is elasped time speed and this is acheived by
* minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF, (Create Temporary File), will create
* a duplicate of a physical file, (created under Smart and defined
* as a Query file), which will have as many members as is defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records evenly
* amoungst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
****************************************************************** ****
 * </pre>
 */
public class Br538 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger logger = LoggerFactory.getLogger(Br538.class);
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlloanCursorrs = null;
	private java.sql.PreparedStatement sqlloanCursorps = null;
	private java.sql.Connection sqlloanCursorconn = null;
	private String sqlloanCursor = "";
	private int loanCursorLoopIndex = 0;
	private DiskFileDAM lonx01 = new DiskFileDAM("LONX01");
	private DiskFileDAM lonx02 = new DiskFileDAM("LONX02");
	private DiskFileDAM lonx03 = new DiskFileDAM("LONX03");
	private DiskFileDAM lonx04 = new DiskFileDAM("LONX04");
	private DiskFileDAM lonx05 = new DiskFileDAM("LONX05");
	private DiskFileDAM lonx06 = new DiskFileDAM("LONX06");
	private DiskFileDAM lonx07 = new DiskFileDAM("LONX07");
	private DiskFileDAM lonx08 = new DiskFileDAM("LONX08");
	private DiskFileDAM lonx09 = new DiskFileDAM("LONX09");
	private DiskFileDAM lonx10 = new DiskFileDAM("LONX10");
	private DiskFileDAM lonx11 = new DiskFileDAM("LONX11");
	private DiskFileDAM lonx12 = new DiskFileDAM("LONX12");
	private DiskFileDAM lonx13 = new DiskFileDAM("LONX13");
	private DiskFileDAM lonx14 = new DiskFileDAM("LONX14");
	private DiskFileDAM lonx15 = new DiskFileDAM("LONX15");
	private DiskFileDAM lonx16 = new DiskFileDAM("LONX16");
	private DiskFileDAM lonx17 = new DiskFileDAM("LONX17");
	private DiskFileDAM lonx18 = new DiskFileDAM("LONX18");
	private DiskFileDAM lonx19 = new DiskFileDAM("LONX19");
	private DiskFileDAM lonx20 = new DiskFileDAM("LONX20");
	private LonxpfTableDAM lonxpfData = new LonxpfTableDAM();
	/*
	 * Change the record length to that of the temporary file. This can be found
	 * by doing a DSPFD of the file being duplicated by the CRTTMPF process.
	 */
	private FixedLengthStringData loan01Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan02Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan03Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan04Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan05Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan06Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan07Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan08Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan09Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan10Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan11Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan12Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan13Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan14Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan15Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan16Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan17Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan18Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan19Rec = new FixedLengthStringData(24);
	private FixedLengthStringData loan20Rec = new FixedLengthStringData(24);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR538");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaalonxfn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaalonxfn, 0, FILLER).init("LONX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaalonxfn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaalonxfn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

	

	/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaArlxInd = FLSInittedArray(1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaArlxInd, 0);
	/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private ZonedDecimalData wsaaZeroes = new ZonedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private int contot01 = 0;
	private int contot02 = 0;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();	
	private P6671par p6671par = new P6671par();	
	private ZonedDecimalData strEffDate = new ZonedDecimalData(8, 0).init(ZERO);
	/*  SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

	/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaalonxData = FLSInittedArray(1000, 105);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaalonxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaalonxData, 1);
	private PackedDecimalData[] wsaaLoannum = PDArrayPartOfArrayStructure(3, 0, wsaalonxData, 9);
	private FixedLengthStringData[] wsaaLoantyp = FLSDArrayPartOfArrayStructure(1, wsaalonxData, 12);
	private FixedLengthStringData[] wsaaLoancur = FLSDArrayPartOfArrayStructure(3, wsaalonxData, 13);
	private FixedLengthStringData[] wsaavalidflag = FLSDArrayPartOfArrayStructure(1, wsaalonxData, 16);
	private PackedDecimalData[] wsaaFtranno = PDArrayPartOfArrayStructure(5, 0, wsaalonxData, 21);
	private PackedDecimalData[] wsaaLtranno = PDArrayPartOfArrayStructure(5, 0, wsaalonxData, 26);
	private PackedDecimalData[] wsaaLoanorigamt = PDArrayPartOfArrayStructure(17, 2, wsaalonxData, 31);
	private PackedDecimalData[] wsaaloanstartdate = PDArrayPartOfArrayStructure(8, 0, wsaalonxData, 48);
	private PackedDecimalData[] wsaalstcapamount = PDArrayPartOfArrayStructure(17, 2, wsaalonxData, 56);
	private PackedDecimalData[] wsaalstcapdate =PDArrayPartOfArrayStructure(8, 0, wsaalonxData, 73);
	private PackedDecimalData[] wsaanxtcapdate = PDArrayPartOfArrayStructure(8, 0, wsaalonxData, 81);
	private PackedDecimalData[] wsaalstintdate = PDArrayPartOfArrayStructure(8, 0, wsaalonxData, 89);
	private PackedDecimalData[] wsaanxtintdate = PDArrayPartOfArrayStructure(8, 0, wsaalonxData, 97);	
	
	public Br538() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/** This section is invoked in restart mode. Note section 1100 must */
		/** clear each temporary file member as the members are not under */
		/** commitment control. Note also that Splitter programs must have */
		/** a Restart Method of 1 (re-run). Thus no updating should occur */
		/** which would result in different records being returned from the */
		/** primary file in a restart! */
		/* EXIT */
	}

	protected void initialise1000() {
		/* Check the restart method is compatible with the program. */
		/* This program is restartable but will always re-run from */
		/* the beginning. This is because the temporary file members */
		/* are not under commitment control. */
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/* Do a ovrdbf for each temporary file member. (Note we have */
		/* allowed for a maximum of 20.) */
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/* Since this program runs from a parameter screen move the */
		/* internal parameter area to the original parameter copybook */
		/* to retrieve the contract range */
		
		wsaaCompany.set(bsprIO.getCompany());
		strEffDate.set(bsscIO.getEffectiveDate());
		
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES) && isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		} else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}	
		
		/* The next thing we must do is construct the name of the */
		/* temporary file which we will be working with. */
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		
	
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)) {
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contot01 +=bprdIO.getThreadsSubsqntProc().toInt();
		iy.set(1);
		if(isEQ(bprdIO.getSystemParam04(),"RL")){
			getINTBLcontract();
		}else{
			getINTCPcontract();
		}


	}	
	protected void getINTBLcontract(){
		sqlloanCursor = " SELECT LO.CHDRCOY, LO.CHDRNUM, LO.LOANNUMBER, LO.LOANTYPE,LO.LOANCURR, LO.VALIDFLAG, LO.FTRANNO, LO.LTRANNO, LO.LOANORIGAM, LO.LOANSTDATE, LO.LSTCAPLAMT, LO.LSTCAPDATE, LO.NXTCAPDATE,LO.LSTINTBDTE, LO.NXTINTBDTE  "
				+ " FROM   " + getAppVars().getTableNameOverriden("LOANPF") + " LO "
				+ " WHERE LO.VALIDFLAG = '1' "
				+ " AND LO.CHDRCOY = ?"
				+ " AND LO.LSTCAPLAMT > 0 "
				+ " AND LO.LSTINTBDTE < ? "
				+ " AND LO.NXTINTBDTE <= ? "
				+ " ORDER BY LO.CHDRCOY ASC, LO.CHDRNUM ASC, LO.LOANNUMBER DESC ";
		/* Initialise the array which will hold all the records */
		/* returned from each fetch. (INITIALIZE will not work as the */
		/* the array is indexed. */		
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)) {
			initialize(wsaalonxData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlloanCursorconn = getAppVars()
					.getDBConnectionForTable(new com.csc.life.contractservicing.dataaccess.LoanpfTableDAM());
			sqlloanCursorps = getAppVars().prepareStatementEmbeded(sqlloanCursorconn, sqlloanCursor, "LOANPF");
			getAppVars().setDBString(sqlloanCursorps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlloanCursorps, 2, strEffDate);
			getAppVars().setDBNumber(sqlloanCursorps, 3, strEffDate);			
			sqlloanCursorrs = getAppVars().executeQuery(sqlloanCursorps);
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}
protected void getINTCPcontract(){
	sqlloanCursor = " SELECT LO.CHDRCOY, LO.CHDRNUM, LO.LOANNUMBER, LO.LOANTYPE,LO.LOANCURR, LO.VALIDFLAG, LO.FTRANNO, LO.LTRANNO, LO.LOANORIGAM, LO.LOANSTDATE, LO.LSTCAPLAMT, LO.LSTCAPDATE, LO.NXTCAPDATE,LO.LSTINTBDTE, LO.NXTINTBDTE "
			+ " FROM   " + getAppVars().getTableNameOverriden("LOANPF") + " LO "		
			+ " WHERE LO.VALIDFLAG = '1' "
			+ " AND LO.CHDRCOY = ?"
			/*+ " AND LO.CHDRNUM BETWEEN ? AND ? "*/
			+ " AND LO.LSTCAPLAMT > 0 "
			+ " AND (LO.NXTCAPDATE < ?  OR LO.NXTCAPDATE = ?) "
			+ " ORDER BY LO.CHDRCOY, LO.CHDRNUM,LO.LOANNUMBER desc ";
	/* Initialise the array which will hold all the records */
	/* returned from each fetch. (INITIALIZE will not work as the */
	/* the array is indexed. */	
	for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)) {
		initialize(wsaalonxData[wsaaInd.toInt()]);
	}
	wsaaInd.set(1);
	sqlerrorflag = false;
	try {
		sqlloanCursorconn = getAppVars()
				.getDBConnectionForTable(new com.csc.life.contractservicing.dataaccess.LoanpfTableDAM());
		sqlloanCursorps = getAppVars().prepareStatementEmbeded(sqlloanCursorconn, sqlloanCursor, "LOANPF");
		getAppVars().setDBString(sqlloanCursorps, 1, wsaaCompany);
		getAppVars().setDBNumber(sqlloanCursorps, 2, strEffDate);
		getAppVars().setDBNumber(sqlloanCursorps, 3, strEffDate);			
		sqlloanCursorrs = getAppVars().executeQuery(sqlloanCursorps);
	} catch (SQLException ex) {
		sqlerrorflag = true;
		getAppVars().setSqlErrorCode(ex);
	}
	}
	protected void openThreadMember1100() {
		openThreadMember1110();
	}

	protected void openThreadMember1110() {
		/* Clear the member for this thread in case we are in restart */
		/* mode in which case it might already contain data. */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaalonxfn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/* Do the override. */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(LONX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaalonxfn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Open the file. */
		if (isEQ(iz, 1)) {
			lonx01.openOutput();
		} else if (isEQ(iz, 2)) {
			lonx02.openOutput();
		} else if (isEQ(iz, 3)) {
			lonx03.openOutput();
		} else if (isEQ(iz, 4)) {
			lonx04.openOutput();
		} else if (isEQ(iz, 5)) {
			lonx05.openOutput();
		} else if (isEQ(iz, 6)) {
			lonx06.openOutput();
		} else if (isEQ(iz, 7)) {
			lonx07.openOutput();
		} else if (isEQ(iz, 8)) {
			lonx08.openOutput();
		} else if (isEQ(iz, 9)) {
			lonx09.openOutput();
		} else if (isEQ(iz, 10)) {
			lonx10.openOutput();
		} else if (isEQ(iz, 11)) {
			lonx11.openOutput();
		} else if (isEQ(iz, 12)) {
			lonx12.openOutput();
		} else if (isEQ(iz, 13)) {
			lonx13.openOutput();
		} else if (isEQ(iz, 14)) {
			lonx14.openOutput();
		} else if (isEQ(iz, 15)) {
			lonx15.openOutput();
		} else if (isEQ(iz, 16)) {
			lonx16.openOutput();
		} else if (isEQ(iz, 17)) {
			lonx17.openOutput();
		} else if (isEQ(iz, 18)) {
			lonx18.openOutput();
		} else if (isEQ(iz, 19)) {
			lonx19.openOutput();
		} else if (isEQ(iz, 20)) {
			lonx20.openOutput();
		}
	}

	protected void readFile2000() {
		readFiles2010();
	}

	protected void readFiles2010() {
		/* Now a block of records is fetched into the array. */
		/* Also on the first entry into the program we must set up the */
		/* WSAA-PREV-CHDRNUM = the present chdrnum. */
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return;
		}
		if (isNE(wsaaInd, 1)) {
			return;
		}
		sqlerrorflag = false;
		try {
			for (loanCursorLoopIndex = 1; isLTE(loanCursorLoopIndex, wsaaRowsInBlock.toInt())
					&& getAppVars().fetchNext(sqlloanCursorrs); loanCursorLoopIndex++) {
				getAppVars().getDBObject(sqlloanCursorrs, 1, wsaaChdrcoy[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 2, wsaaChdrnum[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 3, wsaaLoannum[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 4, wsaaLoantyp[loanCursorLoopIndex]);				
				getAppVars().getDBObject(sqlloanCursorrs, 5, wsaaLoancur[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 6, wsaavalidflag[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 7, wsaaFtranno[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 8, wsaaLtranno[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 9, wsaaLoanorigamt[loanCursorLoopIndex]);				
				getAppVars().getDBObject(sqlloanCursorrs, 10, wsaaloanstartdate[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 11, wsaalstcapamount[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 12, wsaalstcapdate[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 13, wsaanxtcapdate[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 14, wsaalstintdate[loanCursorLoopIndex]);
				getAppVars().getDBObject(sqlloanCursorrs, 15, wsaanxtintdate[loanCursorLoopIndex]);
							
			}
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* We must detect :- */
		/* a) no rows returned on the first fetch */
		/* b) the last row returned (in the block) */
		/* IF Either of the above cases occur, then an */
		/* SQLCODE = +100 is returned. */
		/* The 3000 section is continued for case (b). */		
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(Varcom.endp);
		}

		// Modified the if condition for MIBT-110
		if (isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
		else {
			/*  On the first entry to the program we must set up the*/
			/*  WSAA-PREV-AGNTNUM = present agntnum.                           */
			if (firstTime.isTrue()) {
				/*       MOVE WSAA-CHDRNUM (WSAA-IND) TO WSAA-PREV-CHDRNUM      */
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				/*       MOVE WSAA-AGNTNUM (WSAA-IND) TO WSAA-PREV-AGNTNUM      */
				wsaaFirstTime.set("N");
			}
		}
	}

	protected void edit2500() {
		/* READ */
		/* Check record is required for processsing but do not do any othe */
		/* reads to any other file. The exception to this rule are reads t */
		/* ITEM and these should be kept to a mimimum. Do not do any soft */
		/* locking in Splitter programs. The soft lock should be done by */
		/* the subsequent program which reads the temporary file. */
		wsspEdterror.set(varcom.oK);
		/* EXIT */
	}

	protected void update3000() {
		/* UPDATE */
		/* In the update section we write a record to the temporary file */
		/* member identified by the value of IY. If it is possible to */
		/* to include the RRN from the primary file we should pass */
		/* this data as the subsequent program can then use it to do */
		/* a direct read which is faster than a normal read. */
		wsaaInd.set(1);
		logger.info("update3000  while ( !(isGT(wsaaInd, wsaaRowsInBlock) start");
		while (!(isGT(wsaaInd, wsaaRowsInBlock) || eofInBlock.isTrue())) {
			loadThreads3100();
		}
		logger.info("update3000  while ( !(isGT(wsaaInd, wsaaRowsInBlock) end");
		/* Re-initialise the block for next fetch and point to first row. */
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)) {
			initialize(wsaalonxData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		/* EXIT */
	}

	protected void loadThreads3100() {
		/* START */
		/* If the the CHDRNUM being processed is not equal to the */
		/* to the previous we should move to the next output file member. */
		/* The condition is here to allow for checking the last chdrnum */
		/* of the old block with the first of the new and to move to the */
		/* next PAYX member to write to if they have changed. */
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			iy.add(1);
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		}
		/*  Load from storage all LINX data for the same agent until       */
		/*  the AGNTNUM on LINX has changed.                               */
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		

		/* EXIT */
	}

	protected void loadSameContracts3200() {
		start3200();
	}

	protected void start3200() {
		lonxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		lonxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		lonxpfData.loanNumber.set(wsaaLoannum[wsaaInd.toInt()]);
		lonxpfData.loanType.set(wsaaLoantyp[wsaaInd.toInt()]);
		lonxpfData.loanCurrency.set(wsaaLoancur[wsaaInd.toInt()]);
		lonxpfData.validflag.set(wsaavalidflag[wsaaInd.toInt()]);
		lonxpfData.firstTranno.set(wsaaFtranno[wsaaInd.toInt()]);
		lonxpfData.lastTranno.set(wsaaLtranno[wsaaInd.toInt()]);
		lonxpfData.loanOriginalAmount.set(wsaaLoanorigamt[wsaaInd.toInt()]);
		lonxpfData.loanStartDate.set(wsaaloanstartdate[wsaaInd.toInt()]);
		lonxpfData.lastCapnLoanAmt.set(wsaalstcapamount[wsaaInd.toInt()]);
		lonxpfData.lastCapnDate.set(wsaalstcapdate[wsaaInd.toInt()]);
		lonxpfData.nextCapnDate.set(wsaanxtcapdate[wsaaInd.toInt()]);
		lonxpfData.lastIntBillDate.set(wsaalstintdate[wsaaInd.toInt()]);
		lonxpfData.nextIntBillDate.set(wsaanxtintdate[wsaaInd.toInt()]);		
		
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy, 1)) {
			lonx01.write(lonxpfData);
		}
		if (isEQ(iy, 2)) {
			lonx02.write(lonxpfData);
		}
		if (isEQ(iy, 3)) {
			lonx03.write(lonxpfData);
		}
		if (isEQ(iy, 4)) {
			lonx04.write(lonxpfData);
		}
		if (isEQ(iy, 5)) {
			lonx05.write(lonxpfData);
		}
		if (isEQ(iy, 6)) {
			lonx06.write(lonxpfData);
		}
		if (isEQ(iy, 7)) {
			lonx07.write(lonxpfData);
		}
		if (isEQ(iy, 8)) {
			lonx08.write(lonxpfData);
		}
		if (isEQ(iy, 9)) {
			lonx09.write(lonxpfData);
		}
		if (isEQ(iy, 10)) {
			lonx10.write(lonxpfData);
		}
		if (isEQ(iy, 11)) {
			lonx11.write(lonxpfData);
		}
		if (isEQ(iy, 12)) {
			lonx12.write(lonxpfData);
		}
		if (isEQ(iy, 13)) {
			lonx13.write(lonxpfData);
		}
		if (isEQ(iy, 14)) {
			lonx14.write(lonxpfData);
		}
		if (isEQ(iy, 15)) {
			lonx15.write(lonxpfData);
		}
		if (isEQ(iy, 16)) {
			lonx16.write(lonxpfData);
		}
		if (isEQ(iy, 17)) {
			lonx17.write(lonxpfData);
		}
		if (isEQ(iy, 18)) {
			lonx18.write(lonxpfData);
		}
		if (isEQ(iy, 19)) {
			lonx19.write(lonxpfData);
		}
		if (isEQ(iy, 20)) {
			lonx20.write(lonxpfData);
		}
		/*    Log the number of extracted records.*/
		contot02++;
		if(contot02 == wsaaRowsInBlock.toInt()){
			contotrec.totval.set(contot02);
			contotrec.totno.set(ct02);
			callContot001();
			contot02 =0;
		}
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/
		if (isLTE(wsaaInd, wsaaRowsInBlock) 
				&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

	protected void commit3500() {
		contotrec.totval.set(contot01);
		contotrec.totno.set(ct01);
		callContot001();
		contot01 = 0;

		contotrec.totval.set(contot02);
		contotrec.totno.set(ct02);
		callContot001();
		contot02 = 0;
	}

	protected void rollback3600() {
		/* ROLLBACK */
		/** There is no pre-rollback processing to be done */
		/* EXIT */
	}

	protected void close4000() {
		/* CLOSE-FILES */
		/* Close the open files and remove the overrides. */
getAppVars().freeDBConnectionIgnoreErr(sqlloanCursorconn, sqlloanCursorps, sqlloanCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)) {
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/* EXIT */
	}

	protected void close4100() {
		close4110();
	}

	protected void close4110() {
		if (isEQ(iz, 1)) {
			lonx01.close();
		}
		if (isEQ(iz, 2)) {
			lonx02.close();
		}
		if (isEQ(iz, 3)) {
			lonx03.close();
		}
		if (isEQ(iz, 4)) {
			lonx04.close();
		}
		if (isEQ(iz, 5)) {
			lonx05.close();
		}
		if (isEQ(iz, 6)) {
			lonx06.close();
		}
		if (isEQ(iz, 7)) {
			lonx07.close();
		}
		if (isEQ(iz, 8)) {
			lonx08.close();
		}
		if (isEQ(iz, 9)) {
			lonx09.close();
		}
		if (isEQ(iz, 10)) {
			lonx10.close();
		}
		if (isEQ(iz, 11)) {
			lonx11.close();
		}
		if (isEQ(iz, 12)) {
			lonx12.close();
		}
		if (isEQ(iz, 13)) {
			lonx13.close();
		}
		if (isEQ(iz, 14)) {
			lonx14.close();
		}
		if (isEQ(iz, 15)) {
			lonx15.close();
		}
		if (isEQ(iz, 16)) {
			lonx16.close();
		}
		if (isEQ(iz, 17)) {
			lonx17.close();
		}
		if (isEQ(iz, 18)) {
			lonx18.close();
		}
		if (isEQ(iz, 19)) {
			lonx19.close();
		}
		if (isEQ(iz, 20)) {
			lonx20.close();
		}
	}
	protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
