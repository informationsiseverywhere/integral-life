/*
 * File: Zrgetusr.java
 * Date: 30 August 2009 2:56:09
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRGETUSR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.recordstructures.Zrgtusrrec;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.dataaccess.UsrlTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  ZRGETUSR - Retrieve User Information Subroutine
*  -----------------------------------------------
*
*  This subroutine is called to return the user information to
*  the calling program.
*
*  Given
*  -----
*
*    FUNCTION, STATUS, USERID, USERNUM, GROUPNAME, STRDATE,
*    ENDDATE, COMPANY, BRANCH
*
*    NOTE.... Function is either 'USID' or 'USNO'
*      Where
*             'USID' => Retrieve user information by giving
*                       user ID.
*
*             'USNO' => Retrieve user information by giving
*                       user number.
*
*  Functionality
*  -------------
*
*    If the function is 'USID'
*       Read USRD using the user ID from the linkage
*       Return user number, group name, start date, end date
*          sign-on company and sign-on branch
*    End
*
*    If the function is 'USNO'
*       Read USRL using the user number from the linkage
*       Return user ID, group name, start date, end date
*          sign-on company and sign-on branch
*    End
*
*****************************************************************
* </pre>
*/
public class Zrgetusr extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrgetusr.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("ZRGETUSR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String usrdrec = "USRDREC";
	private static final String usrlrec = "USRLREC";
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	private UsrlTableDAM usrlIO = new UsrlTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Zrgtusrrec zrgtusrrec = new Zrgtusrrec();

	public Zrgetusr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		zrgtusrrec.rec = convertAndSetParam(zrgtusrrec.rec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		LOGGER.debug("Flow Ended");
		}
	}

protected void mainline000()
	{
		start010();
		exit099();
	}

protected void start010()
	{
		zrgtusrrec.statuz.set(varcom.oK);
		if (isEQ(zrgtusrrec.function, "USID")){
			if (isEQ(zrgtusrrec.userid, SPACES)) {
				zrgtusrrec.statuz.set("PARM");
			}
			else {
				searchUserByName100();
			}
		}
		else if (isEQ(zrgtusrrec.function, "USNO")){
			if (isEQ(zrgtusrrec.usernum, 0)) {
				zrgtusrrec.statuz.set("PARM");
			}
			else {
				searchUserByNumber200();
			}
		}
		else{
			zrgtusrrec.statuz.set("FUNC");
		}
	}

protected void exit099()
	{
		exitProgram();
	}

protected void searchUserByName100()
	{
		start110();
	}

protected void start110()
	{
		usrdIO.setParams(SPACES);
		usrdIO.setUserid(zrgtusrrec.userid);
		usrdIO.setFormat(usrdrec);
		usrdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, usrdIO);
		if (isNE(usrdIO.getStatuz(), varcom.oK)
		&& isNE(usrdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(usrdIO.getStatuz());
			syserrrec.params.set(usrdIO.getParams());
			fatalError600();
		}
		if (isEQ(usrdIO.getStatuz(), varcom.oK)) {
			zrgtusrrec.usernum.set(usrdIO.getUsernum());
			zrgtusrrec.groupname.set(usrdIO.getGroupName());
			zrgtusrrec.strdate.set(usrdIO.getUserStartDate());
			zrgtusrrec.enddate.set(usrdIO.getUserEndDate());
			zrgtusrrec.company.set(usrdIO.getCompany());
			zrgtusrrec.branch.set(usrdIO.getBranch());
		}
	}

protected void searchUserByNumber200()
	{
		start210();
	}

protected void start210()
	{
		usrlIO.setParams(SPACES);
		usrlIO.setUsernum(zrgtusrrec.usernum);
		usrlIO.setFormat(usrlrec);
		usrlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, usrlIO);
		if (isNE(usrlIO.getStatuz(), varcom.oK)
		&& isNE(usrlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(usrlIO.getStatuz());
			syserrrec.params.set(usrlIO.getParams());
			fatalError600();
		}
		if (isEQ(usrlIO.getStatuz(), varcom.oK)) {
			zrgtusrrec.userid.set(usrlIO.getUserid());
			zrgtusrrec.groupname.set(usrlIO.getGroupName());
			zrgtusrrec.strdate.set(usrlIO.getUserStartDate());
			zrgtusrrec.enddate.set(usrlIO.getUserEndDate());
			zrgtusrrec.company.set(usrlIO.getCompany());
			zrgtusrrec.branch.set(usrlIO.getBranch());
		}
	}

protected void fatalError600()
	{
		/*START*/
		syserrrec.syserrType.set("1");
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.subrname.set(wsaaProg);
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exit099();
	}
}
