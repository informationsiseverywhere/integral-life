/*
 * File: Grvaeiss.java
 * Date: 29 August 2009 22:51:12
 * Author: Quipoz Limited
 * 
 * Class transformed from GRVAEISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  OVERVIEW
*  ========
*
*  This Subroutine deletes Anticipated Endowment (ZRAE) Records
*  which are created at both the Issue and the Component Add
*  Stages. It is accessed via Table T5671.
*
*
*****************************************************************
* </pre>
*/
public class Grvaeiss extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String zraerec = "ZRAEREC";
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Greversrec greversrec = new Greversrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Grvaeiss() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*STARTS*/
		allProcessing1000();
		exitProgram();
	}

protected void allProcessing1000()
	{
		start1001();
	}

protected void start1001()
	{
		/* Read and Hold ZRAE Record:*/
		zraeIO.setChdrcoy(greversrec.chdrcoy);
		zraeIO.setChdrnum(greversrec.chdrnum);
		zraeIO.setLife(greversrec.life);
		zraeIO.setCoverage(greversrec.coverage);
		zraeIO.setRider(greversrec.rider);
		zraeIO.setPlanSuffix(greversrec.planSuffix);
		zraeIO.setFunction(varcom.readh);
		zraeIO.setFormat(zraerec);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		if (isNE(greversrec.chdrcoy, zraeIO.getChdrcoy())
		|| isNE(greversrec.chdrnum, zraeIO.getChdrnum())
		|| isNE(greversrec.life, zraeIO.getLife())
		|| isNE(greversrec.coverage, zraeIO.getCoverage())
		|| isNE(greversrec.rider, zraeIO.getRider())) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		/* The record read must be a valid record i.e VALIDFLAG = '1'*/
		/* and must have the same TRANNO as the GREV-TRANNO.*/
		if (isNE(zraeIO.getValidflag(), "1")
		|| isNE(zraeIO.getTranno(), greversrec.tranno)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		/* Delete the ZRAE Record.*/
		zraeIO.setFunction(varcom.delet);
		zraeIO.setFormat(zraerec);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		
		zraeIO.setChdrcoy(greversrec.chdrcoy);
		zraeIO.setChdrnum(greversrec.chdrnum);
		zraeIO.setLife(greversrec.life);
		zraeIO.setCoverage(greversrec.coverage);
		zraeIO.setRider(greversrec.rider);
		zraeIO.setPlanSuffix(greversrec.planSuffix);
		zraeIO.setFunction(varcom.readh);
		zraeIO.setFormat(zraerec);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)
		&& isNE(zraeIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		if(isNE(zraeIO.getStatuz(),varcom.mrnf)) {
			zraeIO.setValidflag("1");
			zraeIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(zraeIO.getParams());
				fatalError600();
			}
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError601();
				case errorProg610: 
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
