/*
 * File: Br539.java
 * Date: 14 March 2016 22:17:18
 * Author: CSC
 *
 * Created by: dpuhawan
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 * 
 * ILPI-229 Batch Performance Upgrade - L2NEwINTBL
 * Redesign program logic to incorporate data chunking for more efficient batch processing of volume of records.
 * New DAO layers where also introduced to enable PrepareStatement.executeBatch() for:
 * 1. Bulk Updates
 * 2. Bulk Inserts
 * 3. Bulk Deletes
 */
package com.csc.life.anticipatedendowment.batchprograms;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsuframework.core.FeaConfg;										  
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Instintcalc;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

//ILPI-229 START by dpuhawan
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.anticipatedendowment.dataaccess.dao.Br539DAO;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Br539DTO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
//ILPI-229 END
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER; //ILIFE-8840 

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   New Interest Billing program.
*
*
*   Control totals:
*     01  -  Number of LOAN records processed
*
*
*****************************************************************
* </pre>
*/
public class Br539 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR539");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaInterestDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaFacthous = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBankacckey = new FixedLengthStringData(20);
	private PackedDecimalData wsaaIntFromDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).setUnsigned();
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private static final int wsaaUser = 0;
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
		/* ERRORS */
	private static final String e723 = "E723";
	private static final String t5645 = "T5645";

		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();	
	private Intcalcrec intcalcrec = new Intcalcrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T3629rec t3629rec = new T3629rec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	private FixedLengthStringData slckEntity = new FixedLengthStringData(16);
	private PackedDecimalData wsaanofDay = new PackedDecimalData(3, 0); //ICIL-682																			   
	
	
	//ILPI-229 START by dpuhawan	
	private Map<String, List<Itempf>> t3629ListMap;
	private Map<String, List<Itempf>> t6633ListMap;
	
	private List<Loanpf> loanList;
	private List<Loanpf> loanBulkUpdtList;
	private List<Chdrpf> chdrBulkUpdtList;
	private List<Bextpf> bextBulkInsList;
	private List<Ptrnpf> ptrnBulkInsList;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private LoanpfDAO loanpfDAO =  getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private BextpfDAO bextpfDAO =  getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Br539DAO br539DAO =  getApplicationContext().getBean("br539DAO", Br539DAO.class);
	private static final String h134 = "H134";
	private static final String hl63 = "HL63";	
	private Bextpf bextpf;
	private Ptrnpf ptrnpf;	
	private Chdrpf chdrpf;
	private static final Logger LOGGER = LoggerFactory.getLogger(Br539.class);
	private int intBatchID;
	private int intBatchExtractSize;
	private int intNxtBilDate;
	private String strSacscode;
	private String strSacsType;
	private String strglMap;
	private String strglSign;
	private int intIndex;
	private int intBatchStep;
	private String strCompany;
	private String strEffDate;
	private Loanpf loanpf;
	private Br539DTO br539dto;
	private Iterator<Loanpf> iteratorList;	
	boolean hasRecords;
	String descrec;
	private int chdrTranno;
	private int ctrCT01; 
	private int ctrCT02;
	private boolean noRecordFound;
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	//ILPI-229 END
	private String prevChdrnum = new String();
	private String currChdrnum = new String();;
	private int rcdCnt =0;
	boolean cslnd004Permission = false;  //ICIL-682
//ILIFE-8840 start
	private FixedLengthStringData wsaalonxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaalonxFn, 0, FILLER).init("LONX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaalonxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaalonxFn, 6).setUnsigned();
	
	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
    private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
    private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
    private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
    private Datcon2Pojo datcon2Pojo = null;
    private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
    private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
    private AcblpfDAO acblpfDAO =  getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
    private List<Acblpf> acblpfInsertAll = new ArrayList<Acblpf>();
    
 //ILIFE-8840 end
	
    
	
	

/**
 * Contains all possible labels used by goTo action.
 */
	public Br539() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

	/**
	* The mainline method is the default entry point to the class
	*/

protected void restart0900()
{
	/*RESTART*/
	/** Place any additional restart processing in here.*/
	/*EXIT*/
}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

/* Initialise1000 Section - Start
 * @see com.csc.smart400framework.parent.Mainb#initialise1000()
 */
protected void initialise1000()
	{		
		strCompany = bsprIO.getCompany().toString();
		strEffDate = bsscIO.getEffectiveDate().toString();	
		intBatchID = 0;
		intBatchStep = 0;
		hasRecords = false;
		noRecordFound = false;
		loanList = new ArrayList<Loanpf>();
		cslnd004Permission = FeaConfg.isFeatureExist("2", "CSLND004", appVars, "IT");	//ICIL-682																				  
 //ILIFE-8840 start
		initialise1010();
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
 //ILIFE-8840 end
		if (bprdIO.systemParam01.isNumeric())
			if (bprdIO.systemParam01.toInt() > 0)
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			else
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();	
		
		//int extractRows = br539DAO.populateBr539Temp(wsaalonxFn.toString(), wsaaThreadMember.toString(),intBatchExtractSize); //ILIFE-8840
		performDataChunk();
		//if (!loanList.isEmpty()) { 
			//performDataChunk();	
			
			if (!loanList.isEmpty()){
				getSystemDateTime1040();
				readSmartTables(bsprIO.getCompany().toString().trim());
				readT5645(bsprIO.getCompany().toString().trim());
				getItemDescription();
			}			
		//} 
			else{
				LOGGER.info("No loan records retrieved for processing.");
				noRecordFound = true;
				return;	
			}
}

protected void performDataChunk(){	
	intBatchID = intBatchStep;		
	if (loanList.size() > 0) 
		loanList.clear();
	loanList =  br539DAO.populateBr539Temp(wsaalonxFn.toString(), wsaaThreadMember.toString(),intBatchExtractSize,intBatchID);
//	br539DAO.loadDataByBatch(intBatchID);
	if (loanList.isEmpty())
		wsspEdterror.set(varcom.endp);
	else 
		iteratorList = loanList.iterator();	
}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaCnttype.set(SPACES);
		wsaaFacthous.set(SPACES);
		wsaaBankkey.set(SPACES);
		wsaaBankacckey.set(SPACES);
		wsaaIntFromDate.set(ZERO);
		wsaaInterestDate.set(ZERO);

		loanBulkUpdtList = new ArrayList<Loanpf>();
		chdrBulkUpdtList = new ArrayList<Chdrpf>();		
		bextBulkInsList = new ArrayList<Bextpf>();
		ptrnBulkInsList = new ArrayList<Ptrnpf>();
		t3629ListMap =  new HashMap<String, List<Itempf>>();
		t6633ListMap =  new HashMap<String, List<Itempf>>();
		descrec = "DESCREC";
		ctrCT01 = 0;
		ctrCT02 = 0;
	}

protected void getSystemDateTime1040()
{
	wsaaTransactionDate.set(getCobolDate());
	wsaaTransactionTime.set(varcom.vrcmTime);
}

private void readSmartTables(String company){
	t3629ListMap = itemDAO.loadSmartTable("IT", company, "T3629");
	t6633ListMap = itemDAO.loadSmartTable("IT", company, "T6633");	
}

protected void readT5645(String company){
	/* Read the accounting rules table*/
	
	List<Itempf> itempfList = itemDAO.getAllItemitem("IT",company,t5645,wsaaProg.toString().trim());
	if (null != itempfList && !itempfList.isEmpty()) { 
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));				
		}				
	else {
			syserrrec.params.set(t5645rec.t5645Rec);
			syserrrec.statuz.set(h134);
			fatalError600();		
		}	
	}	


protected void getItemDescription(){
	/* Get item description.*/
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(strCompany);
	descIO.setDesctabl(t5645);
	descIO.setDescitem(wsaaProg);
	descIO.setLanguage(bsscIO.getLanguage());
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(descIO.getParams());
		syserrrec.statuz.set(descIO.getStatuz());
		fatalError600();
	}
	wsaaTrandesc.set(descIO.getLongdesc());
}
/* Initialise1000 Section - Ends */

/*ReadFile2000 Sections - Starts */
protected void readFile2000(){
	if (!loanList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!loanList.isEmpty()){
				loanpf = new Loanpf();
				loanpf = iteratorList.next();
				br539dto = loanpf.getBr539DTO();	
				currChdrnum = br539dto.getChdrnum();
			}
		}else {		
			loanpf = new Loanpf();
			loanpf = iteratorList.next();
			br539dto = loanpf.getBr539DTO();
			currChdrnum = br539dto.getChdrnum();
		}	
	}else wsspEdterror.set(varcom.endp);
	
	if(isNE(prevChdrnum,currChdrnum)){
		prevChdrnum = currChdrnum;
		rcdCnt = 1;
	
	} else {
		rcdCnt++;
	}
}
/*ReadFile2000 Sections - Ends */

protected void removeSftlock2200()
{
	start2200();
}

protected void start2200()
{
	/* Unlock Contract Header record*/
	sftlockrec.function.set("UNLK");
	sftlockrec.company.set(strCompany);
	sftlockrec.enttyp.set("CH");
	slckEntity.set(loanpf.getChdrnum());
	sftlockrec.entity.set(slckEntity.toString());
	sftlockrec.transaction.set(bprdIO.getAuthCode());
	sftlockrec.user.set(999999);
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}
protected void edit2500()	{	
	wsspEdterror.set(varcom.oK);
	ctrCT01++;
	softlock2600();
	if (isEQ(sftlockrec.statuz, "LOCK")) ctrCT02++;
}
protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT01 = 0;

	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT02 = 0;
}

protected void softlock2600()
{
	start2610();
}

protected void start2610()
{
	/* Soft lock the contract, if it is to be processed.               */
	sftlockrec.function.set("LOCK");
	sftlockrec.company.set(strCompany);
	sftlockrec.enttyp.set("CH");
	slckEntity.set(loanpf.getChdrnum());
	sftlockrec.entity.set(slckEntity.toString());
	sftlockrec.transaction.set(bprdIO.getAuthCode());
	sftlockrec.user.set(999999);
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)
	&& isNE(sftlockrec.statuz, "LOCK")) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}

//ILPI-229 START by dpuhawan
protected void update3000()
{
	chdrTranno = br539dto.getTranno() + 1;
	if(isEQ(rcdCnt,br539dto.getCnt())){	
		updateChdrRecord3020();
	}
	calculateInterest3030();
	updateLoanRecord3040();
	postInterestAcmvs3050();
	writeBextRecord3060();
	if(isEQ(rcdCnt,br539dto.getCnt())){	
		writePtrnRecord3070();	
		updateBatchControlTotals3080();	
	}
	removeSftlock2200();	
}

protected void updateChdrRecord3020(){
	chdrpf = new Chdrpf();
	chdrpf.setUniqueNumber(br539dto.getUniqueNumber());
	chdrpf.setTranno(chdrTranno);	
	chdrBulkUpdtList.add(chdrpf);		
}
protected void commitChdrBulkUpdate()
{	 
	boolean isUpdateChdr = chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
	if (!isUpdateChdr) {
		LOGGER.error("Update CHDR record failed.");
		fatalError600();
	}else chdrBulkUpdtList.clear();
}

protected void calculateInterest3030()
{
	intcalcrec.intcalcRec.set(SPACES);
	intcalcrec.loanNumber.set(loanpf.getLoannumber());
	intcalcrec.chdrcoy.set(loanpf.getChdrcoy());
	intcalcrec.chdrnum.set(loanpf.getChdrnum());
	intcalcrec.cnttype.set(br539dto.getCnttype());
	intcalcrec.interestTo.set(strEffDate);
	wsaaInterestDate.set(loanpf.getNxtintbdte());
	intcalcrec.interestFrom.set(loanpf.getLstintbdte());
	wsaaIntFromDate.set(loanpf.getLstintbdte());
	intcalcrec.loanorigam.set(loanpf.getLstcaplamt());
	intcalcrec.lastCaplsnDate.set(loanpf.getLstcapdate());
	intcalcrec.loanStartDate.set(loanpf.getLoansdate());
	intcalcrec.interestAmount.set(ZERO);
	intcalcrec.loanCurrency.set(loanpf.getLoancurr());
	intcalcrec.loanType.set(loanpf.getLoantype());	
	callProgram(Instintcalc.class, intcalcrec.intcalcRec);
	if (isNE(intcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(intcalcrec.intcalcRec);
		syserrrec.statuz.set(intcalcrec.statuz);
		fatalError600();
	}	
	if (isNE(intcalcrec.interestAmount, 0)) {
		zrdecplcPojo.setAmountIn(intcalcrec.interestAmount.getbigdata());
     	zrdecplcPojo.setCurrency(loanpf.getLoancurr());
 		zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
 		zrdecplcPojo.setBatctrcde(batcdorrec.trcde.toString());
 		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
 		if (!varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
 			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			fatalError600();
 		}
 		intcalcrec.interestAmount.set(zrdecplcPojo.getAmountOut());
		
	}		
		
}

protected void updateLoanRecord3040()
{
	intNxtBilDate=0;
	loanpf.setValidflag("1");
	loanpf.setLstintbdte(loanpf.getNxtintbdte());
	calcNextBillDate3041();
	loanpf.setNxtintbdte(intNxtBilDate);	
	loanBulkUpdtList.add(loanpf);
	
}

protected void commitLoanUpdate(){
	boolean isUpdateChdr = loanpfDAO.updateLoanRecordsUniqueNo(loanBulkUpdtList);
	if (!isUpdateChdr) {
		LOGGER.error("Update LOAN record failed.");
		fatalError600();
	}else loanBulkUpdtList.clear();	
}

protected void calcNextBillDate3041()
{
	readT6633();
	wsaaLoanDate.set(loanpf.getLoansdate());
	//ICIL-682 start
	if(cslnd004Permission) {
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(wsaanofDay.toInt());
		datcon2Pojo.setIntDate1(wsaaLoanDate.toString());
		datcon2Pojo.setIntDate2("0");
		datcon2Pojo.setFrequency("DY");
		callDatcon23260();
		wsaaLoanDate.set(datcon2Pojo.getIntDate2());
	}
	//ICIL-682 end				 
	if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(1);
		datcon2Pojo.setIntDate1(Integer.toString(loanpf.getLoansdate()));
		datcon2Pojo.setIntDate2("0");
		datcon2Pojo.setFrequency("01");
		
		while ( !(isGT(datcon2Pojo.getIntDate2(), loanpf.getNxtintbdte()))) {
			callDatcon23260();
			datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
		}

		intNxtBilDate = Integer.parseInt(datcon2Pojo.getIntDate2());
	}

	if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(1);
		datcon2Pojo.setIntDate1(Integer.toString((br539dto.getOccdate())));
		datcon2Pojo.setIntDate2("0");
		datcon2Pojo.setFrequency("01");
		
		while ( !(isGT(datcon2Pojo.getIntDate2(), loanpf.getNxtintbdte()))) {
			callDatcon23260();
			datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
		}

		intNxtBilDate = Integer.parseInt(datcon2Pojo.getIntDate2());
		return;
	}
	/* Get here so the next interest calc. date isn't based on*/
	/* loan or contract anniversarys.*/
	wsaaNewDate.set(ZERO);
	/* Check if table T6633 has a fixed day of the month specified*/
	/* ... If not, use the Loan day*/
	wsaaDayCheck.set(t6633rec.interestDay);
	wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
	//ILIFE-7065 --START
	wsaaNewYear.set(wsaaLoanYear);
	wsaaNewMonth.set(wsaaLoanMonth);
	wsaaNewDay.set(wsaaLoanDay);//MIBR-164
	//ILIFE-7065 --ENDS
	if (!daysLessThan28.isTrue()) {
		dateSet3270();
	}
	else {
		if (isNE(t6633rec.interestDay, ZERO)) {
			wsaaNewDay.set(t6633rec.interestDay);
		}
		else {
			wsaaNewDay.set(wsaaLoanDay);
		}
	}
	
	datcon2Pojo = new Datcon2Pojo();
	datcon2Pojo.setIntDate1(wsaaNewDate.toString());
	datcon2Pojo.setFreqFactor(1);
	/* Check if table T6633 has a fixed frequency for interest*/
	/* calcs, ...If not, use 1 year as the default interest calc.*/
	/* frequency.*/
	if (isEQ(t6633rec.interestFrequency, SPACES)) {
		datcon2Pojo.setFrequency("01");
	}
	else {
		datcon2Pojo.setFrequency(t6633rec.interestFrequency.toString());
	}	
	
	callDatcon23260();
	if (isLTE(datcon2Pojo.getIntDate2(), loanpf.getNxtintbdte())) {
		datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
		datcon2Loop3240();
	}
	wsaaNewDate.set(datcon2Pojo.getIntDate2());
	wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
	if (!daysLessThan28.isTrue()) {
		dateSet3270();
		intNxtBilDate = wsaaNewDate.toInt();
	}
	else {
		intNxtBilDate = Integer.parseInt(datcon2Pojo.getIntDate2());
	}	
}

protected void postInterestAcmvs3050()
{
	/* Set up LIFACMV fields.*/
	lifacmvrec.lifacmvRec.set(SPACES);
	lifacmvrec.batccoy.set(batcdorrec.company);
	lifacmvrec.batcbrn.set(batcdorrec.branch);
	lifacmvrec.batcactyr.set(batcdorrec.actyear);
	lifacmvrec.batcactmn.set(batcdorrec.actmonth);
	lifacmvrec.batctrcde.set(batcdorrec.trcde);
	lifacmvrec.batcbatch.set(batcdorrec.batch);
	lifacmvrec.rldgcoy.set(loanpf.getChdrcoy());
	lifacmvrec.genlcoy.set(loanpf.getChdrcoy());
	lifacmvrec.rdocnum.set(loanpf.getChdrnum());
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.origcurr.set(loanpf.getLoancurr());
	lifacmvrec.origamt.set(intcalcrec.interestAmount);
	wsaaSequenceNo.add(1);
	lifacmvrec.jrnseq.set(wsaaSequenceNo);
	lifacmvrec.rcamt.set(0);
	lifacmvrec.crate.set(0);
	lifacmvrec.acctamt.set(0);
	lifacmvrec.contot.set(0);
	lifacmvrec.tranno.set(chdrTranno);
	lifacmvrec.frcdate.set(99999999);
	lifacmvrec.effdate.set(loanpf.getNxtintbdte());
	lifacmvrec.tranref.set(loanpf.getChdrnum());
	lifacmvrec.trandesc.set(wsaaTrandesc);
	wsaaRldgacct.set(SPACES);
	wsaaRldgChdrnum.set(loanpf.getChdrnum());
	wsaaRldgLoanno.set(loanpf.getLoannumber());
	lifacmvrec.rldgacct.set(wsaaRldgacct);	
	lifacmvrec.termid.set(wsaaTermid);
	lifacmvrec.transactionDate.set(wsaaTransactionDate);
	lifacmvrec.transactionTime.set(wsaaTransactionTime);
	lifacmvrec.user.set(wsaaUser);
	/* Post the interest to the Loan debit account (depending on*/
	/* whether it is a Policy Loan, an APL or a Cash Deposit).*/
	getGLAcctCodes(loanpf.getLoantype(), br539dto.getMandref(),1);
	lifacmvrec.sacscode.set(strSacscode);
	lifacmvrec.sacstyp.set(strSacsType);
	lifacmvrec.glcode.set(strglMap);
	lifacmvrec.glsign.set(strglSign);
	lifacmvrec.function.set("PSTW");
	lifacmvrec.substituteCode[1].set(wsaaCnttype);
	if (isNE(lifacmvrec.origamt, ZERO)) {
		/*    CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.             */
		lifacmvrec.agnt.set("N");
		List<Acblpf> acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}
	/* Post the Interest to the total Interest Accrued account*/
	/* - whether it is the Policy, APL or Cash Deposit subaccount.*/
	wsaaSequenceNo.add(1);
	lifacmvrec.jrnseq.set(wsaaSequenceNo);
	getGLAcctCodes(loanpf.getLoantype(), br539dto.getMandref(),2);
	lifacmvrec.sacscode.set(strSacscode);
	lifacmvrec.sacstyp.set(strSacsType);
	lifacmvrec.glcode.set(strglMap);
	lifacmvrec.glsign.set(strglSign);
	lifacmvrec.function.set("PSTW");
	if (isNE(lifacmvrec.origamt, ZERO)) {
		/*    CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.             */
		lifacmvrec.agnt.set("N");
		List<Acblpf> acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			/*           PERFORM 600-FATAL-ERROR.                              */
			fatalError600();
		}
	}			
}

protected void writeBextRecord3060()
{	
	bextpf = new Bextpf();
	bextpf.setInstfrom(loanpf.getLstintbdte());
	bextpf.setInstto(loanpf.getNxtintbdte());
	bextpf.setBtdate(loanpf.getNxtintbdte());
	bextpf.setBilldate(loanpf.getNxtintbdte());
	bextpf.setBillcd(loanpf.getNxtintbdte());
	bextpf.setInstamt01(Double.parseDouble(intcalcrec.interestAmount.toString()));
	bextpf.setInstamt02(0);
	bextpf.setInstamt03(0);
	bextpf.setInstamt04(0);
	bextpf.setInstamt05(0);	
	bextpf.setInstamt06(Double.parseDouble(intcalcrec.interestAmount.toString()));
	bextpf.setInstjctl(bsprIO.getRecKeyData().toString().trim().substring(0, 24));
	bextpf.setChdrnum(loanpf.getChdrnum());
	bextpf.setChdrcoy(loanpf.getChdrcoy());		
	bextpf.setChdrpfx(br539dto.getChdrpfx());
	bextpf.setServunit(br539dto.getServunit());
	bextpf.setCnttype(br539dto.getCnttype());
	bextpf.setOccdate(br539dto.getOccdate());
	bextpf.setCcdate(br539dto.getCcdate());
	bextpf.setCownpfx(br539dto.getCownpfx());
	bextpf.setCowncoy(br539dto.getCowncoy());
	bextpf.setCownnum(br539dto.getCownnum());
	bextpf.setInstcchnl(br539dto.getCollchnl());
	bextpf.setCntbranch(br539dto.getCntbranch());
	bextpf.setAgntpfx(br539dto.getAgntpfx());
	bextpf.setAgntcoy(br539dto.getAgntcoy());
	bextpf.setAgntnum(br539dto.getAgntnum());						
	bextpf.setOutflag("");
	bextpf.setPayflag("");
	bextpf.setBilflag("");
	bextpf.setGrupkey("");
	bextpf.setMembsel("");
	bextpf.setSupflag("N");	
	bextpf.setPayrpfx(br539dto.getClntpfx());
	bextpf.setPayrcoy(br539dto.getClntcoy());
	bextpf.setPayrnum(br539dto.getClntnum());	
	bextpf.setMandref(br539dto.getMandref());
	bextpf.setPtdate(br539dto.getPtdate());
	bextpf.setBillchnl(br539dto.getBillchnl());
	bextpf.setInstbchnl(br539dto.getBillchnl());
	bextpf.setInstfreq(br539dto.getBillfreq());
	bextpf.setCntcurr(br539dto.getBillcurr());		
	bextpf.setBankkey(br539dto.getBankkey());
	bextpf.setBankacckey(br539dto.getBankacckey());
	bextpf.setFacthous(br539dto.getFacthous());
	bextpf.setMandstat(br539dto.getMandstat());
	readT3629(br539dto.getBillcurr());
	bextpf.setBankcode(t3629rec.bankcode.toString());
	bextpf.setNextdate(br539dto.getNextdate());		
	getGLAcctCodes(loanpf.getLoantype(), br539dto.getMandref(),1);
	bextpf.setGlmap(strglMap);
	bextpf.setSacscode(strSacscode);
	bextpf.setSacstyp(strSacsType);
	bextpf.setEffdatex(0);
	bextpf.setDdderef(0);	
	bextBulkInsList.add(bextpf);
}

protected void commitBextPFInsert(){	 
	boolean isInsertBextPF = bextpfDAO.insertBextPF(bextBulkInsList);
	if (!isInsertBextPF) {
		LOGGER.error("Insert BextPF record failed.");
		fatalError600();
	}else bextBulkInsList.clear();
}

protected void getGLAcctCodes(String loanType, String mandRef, int intIncrement){
	
	if (isEQ(loanType.toUpperCase(), "P")){
		intIndex = 0;
	}
	else if (isEQ(loanType.toUpperCase(), "A")){
		intIndex = 2;
	}
	else if (isEQ(loanType.toUpperCase(), "E")){		
		intIndex = 4;
	}
	else if (isEQ(loanType.toUpperCase(), "D")){
		intIndex = 6;
	}	
	if (isNE(mandRef, SPACES)) {
		strSacscode = t5645rec.sacscode[intIndex + intIncrement].toString();
		strSacsType = t5645rec.sacstype[intIndex + intIncrement].toString();
		strglMap = t5645rec.glmap[intIndex + intIncrement].toString();
		strglSign = t5645rec.sign[intIndex + intIncrement].toString();
	}
	else {
		strSacscode = "";
		strSacsType = "";
		strglMap = "";
		strglSign = "";
	}
}

protected void writePtrnRecord3070(){

	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrcoy(loanpf.getChdrcoy());
	ptrnpf.setChdrpfx(br539dto.getChdrpfx());
	ptrnpf.setChdrnum(loanpf.getChdrnum());
	ptrnpf.setTranno(chdrTranno);
	ptrnpf.setTrdt(wsaaTransactionDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	if(isEQ(batcdorrec.trcde.toString(),"BA69"))
	ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
	else
	ptrnpf.setPtrneff(loanpf.getNxtintbdte());
	ptrnpf.setUserT(wsaaUser);
	ptrnpf.setBatccoy(batcdorrec.company.toString());
	ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
	ptrnpf.setBatcbrn(batcdorrec.branch.toString());
	ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
	ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
	ptrnBulkInsList.add(ptrnpf);
}

protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else ptrnBulkInsList.clear();
}

protected void commitAcblPFInsert(){
	 if (acblpfInsertAll != null && !acblpfInsertAll.isEmpty()) 
     	acblpfDAO.processAcblpfList(acblpfInsertAll, false); 
	
}

protected void updateBatchControlTotals3080(){
	batcuprec.batcupRec.set(SPACES);
	batcuprec.trancnt.set(1);
	batcuprec.etreqcnt.set(0);
	batcuprec.sub.set(0);
	batcuprec.bcnt.set(0);
	batcuprec.bval.set(0);
	batcuprec.ascnt.set(0);
	batcuprec.batcpfx.set(batcdorrec.prefix);
	batcuprec.batccoy.set(batcdorrec.company);
	batcuprec.batcbrn.set(batcdorrec.branch);
	batcuprec.batcactyr.set(batcdorrec.actyear);
	batcuprec.batctrcde.set(batcdorrec.trcde);
	batcuprec.batcactmn.set(batcdorrec.actmonth);
	batcuprec.batcbatch.set(batcdorrec.batch);
	batcuprec.function.set("WRITS");
	callProgram(Batcup.class, batcuprec.batcupRec);
	if (isNE(batcuprec.statuz, varcom.oK)) {
		syserrrec.params.set(batcuprec.batcupRec);
		syserrrec.statuz.set(batcuprec.statuz);
		fatalError600();
	}	
}
//ILPI-229 END



protected void datcon2Loop3240()
	{
		callDatcon23260();
		if (isLTE(datcon2Pojo.getIntDate2(), wsaaInterestDate)) {
			datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			datcon2Loop3240();
		}
		wsaaNewDate.set(datcon2Pojo.getIntDate2());
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3270();
			intNxtBilDate = wsaaNewDate.toInt();
		}
		else {
			intNxtBilDate = Integer.parseInt(datcon2Pojo.getIntDate2());
		}
	}

protected void readT6633()
{
	String keyItemitem = br539dto.getCnttype().concat(loanpf.getLoantype());
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6633ListMap.containsKey(keyItemitem)){	
		itempfList = t6633ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();			
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){					
					t6633rec.t6633Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}else{
				t6633rec.t6633Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t6633rec.t6633Rec);
		syserrrec.statuz.set(e723);
		fatalError600();		
	}			
	//ICIL-682 start
	if ((cslnd004Permission)) {
		wsaanofDay.set(t6633rec.nofDay); 
	}
	//ICIL-682 end				 
}


protected void callDatcon23260()
	{
		/*START*/
	datcon2Utils.calDatcon2(datcon2Pojo);
	if (!"****".equals(datcon2Pojo.getStatuz())) {
			syserrrec.params.set(datcon2Pojo);
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void dateSet3270()
	{
		/*START*/
		/* We have to check that the date we are going to call Datcon2*/
		/* with is a valid from-date... ie If the interest/capn day in*/
		/* T6633 is > 28, we have to make sure the from-date isn't*/
		/* something like 31/02/nnnn or 31/06/nnnn*/
//	MIBT-179
	if(isEQ(wsaaDayCheck, ZERO)){
		wsaaDayCheck.set(wsaaLoanDay);
	}
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}

protected void readT3629(String billCurr)
{
	/* Read T3629 for bank code.*/
	String keyItemitem = billCurr.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3629ListMap.containsKey(keyItemitem)){	
		itempfList = t3629ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){					
					t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}else{
				t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t3629rec.t3629Rec);
		syserrrec.statuz.set(hl63);
		fatalError600();		
	}		
}

protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
		commitChdrBulkUpdate();		
		commitLoanUpdate();
		commitBextPFInsert();
		commitPtrnBulkInsert();
	}
}

protected void rollback3600(){
	/*ROLLBACK*/
	/*EXIT*/
}

protected void close4000(){
	if (!loanList.isEmpty()){
		
		t3629ListMap.clear();
		t3629ListMap = null;
		
		t6633ListMap.clear();
		t6633ListMap = null;
		
		loanList.clear();
		loanList = null;
		
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;
		
		loanBulkUpdtList.clear();
		loanBulkUpdtList = null;
		
		bextBulkInsList.clear();
		bextBulkInsList = null;
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;	
		
		if (acblpfInsertAll != null) {
	       	acblpfInsertAll.clear();
	       	acblpfInsertAll = null;
	       }  
				
		lsaaStatuz.set(varcom.oK);
	}
}

/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);}

}
