package com.csc.life.anticipatedendowment.dataaccess;

import com.csc.life.anticipatedendowment.dataaccess.dao.ZraepfDAO;
import com.csc.life.anticipatedendowment.dataaccess.dao.impl.ZraepfDAOImpl;

public class AnticipatedEndowmentDAOFactory {
	
	private AnticipatedEndowmentDAOFactory() {
		
	}
	
	/**
	 * @return AnticipatedEndowmentDAOFactory
	 */
	public static AnticipatedEndowmentDAOFactory getInstance() {
		return new AnticipatedEndowmentDAOFactory();
	}
	
	//ILIFE-1827 START by avemula
	//Table: ZRAEPF, Interface: ZraepfDAO, Implementation: ZraepfDAOImpl
	/**
	 * @return ZraepfDAOImpl
	 */
	public ZraepfDAO getZraepfDAO()	{
		return new ZraepfDAOImpl();
	}
	//ILIFE-1827 END


}
