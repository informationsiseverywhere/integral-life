/*
 * File: Br539.java
 * Date: 14 March 2016 22:17:18
 * Author: CSC
 *
 * Created by: dpuhawan
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 * 
 * ILPI-229 Batch Performance Upgrade - L2NEwINTBL
 * Redesign program logic to incorporate data chunking for more efficient batch processing of volume of records.
 * New DAO layers where also introduced to enable PrepareStatement.executeBatch() for:
 * 1. Bulk Updates
 * 2. Bulk Inserts
 * 3. Bulk Deletes
 */
package com.csc.life.anticipatedendowment.batchprograms;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;

import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.dataaccess.dao.RgpdetpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

//ILPI-229 START by dpuhawan
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.anticipatedendowment.dataaccess.dao.Bd5huDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5huDTO;




//ILPI-229 END

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   New Interest Billing program.
*
*
*   Control totals:
*     01  -  Number of LOAN records processed
*
*
*****************************************************************
* </pre>
*/
public class Bd5hu extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BD5HU");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaInterestDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaFacthous = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBankacckey = new FixedLengthStringData(20);
	private PackedDecimalData wsaaIntFromDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).setUnsigned();
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private static final int wsaaUser = 0;
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
		/* ERRORS */
	private static final String e723 = "E723";
	private static final String t5645 = "T5645";

		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();	
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	
	private T5645rec t5645rec = new T5645rec();
	private Td5h7rec td5h7rec = new Td5h7rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();	
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	
	//ILPI-229 START by dpuhawan	
	private Map<String, List<Itempf>> t5645ListMap;
	
	private Map<String, List<Itempf>> td5h7ListMap;
	
	private List<Rgpdetpf> regpList;
	private List<Rgpdetpf> regpBulkUpdtList;
	private List<Rgpdetpf> regpBulkTmptList;
	private List<Chdrpf> chdrBulkUpdtList;
	private List<Ptrnpf> ptrnBulkInsList;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RgpdetpfDAO regpdetpfDAO =  getApplicationContext().getBean("rgpdetpfDAO", RgpdetpfDAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Bd5huDAO bd5huDAO =  getApplicationContext().getBean("bd5huDAO", Bd5huDAO.class);
	private static final String h134 = "H134";
		
	private Ptrnpf ptrnpf;	
	private Chdrpf chdrpf;
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5hu.class);
	private int intBatchID;
	private int intBatchExtractSize;
	private int intNxtBilDate;
	private int intBatchStep;
	private String strCompany;
	private String strEffDate;
	private Rgpdetpf rpdetpf;
	private Bd5huDTO bd5hudto;
	private Iterator<Rgpdetpf> iteratorList;	
	boolean hasRecords;
	String descrec;
	private String chdrTranno;
	private int ctrCT01; 
	private int ctrCT02;
	private boolean noRecordFound;
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private List<Acmvpf> acmvpfList;
	private Map<String, Descpf> t5645Map = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf descpfT5645;
	private String prevChdrnum = new String();
	private String currChdrnum = new String();;
	private int rcdCnt =0;
  	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);

	//ILPI-229 END

/**
 * Contains all possible labels used by goTo action.
 */
	public Bd5hu() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

	/**
	* The mainline method is the default entry point to the class
	*/

protected void restart0900()
{
	/*RESTART*/
	/** Place any additional restart processing in here.*/
	/*EXIT*/
}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

/* Initialise1000 Section - Start
 * @see com.csc.smart400framework.parent.Mainb#initialise1000()
 */
protected void initialise1000()
	{		
		strCompany = bsprIO.getCompany().toString();
		strEffDate = bsscIO.getEffectiveDate().toString();	
		intBatchID = 0;
		intBatchStep = 0;
		hasRecords = false;
		noRecordFound = false;
		regpList = new ArrayList<Rgpdetpf>();

		if (bprdIO.systemParam01.isNumeric())
			if (bprdIO.systemParam01.toInt() > 0)
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			else
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
		
		int extractRows = bd5huDAO.populateBd5huTemp(strCompany, strEffDate, intBatchExtractSize);
		if (extractRows > 0) {
			performDataChunk();	
			
			if (!regpList.isEmpty()){
				initialise1010();
				getSystemDateTime1040();
				readSmartTables(bsprIO.getCompany().toString().trim());
				readT5645();
				getItemDescription();
			}			
		}
		else{
			LOGGER.info("No Annuity Payment records retrieved for processing.");
			noRecordFound = true;
			return;	
		}
	}

protected void performDataChunk(){	
	intBatchID = intBatchStep;		
	if (regpList.size() > 0) regpList.clear();
	regpList = bd5huDAO.loadDataByBatch(intBatchID);
	if (regpList.isEmpty())	wsspEdterror.set(varcom.endp);
	else iteratorList = regpList.iterator();		
}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaCnttype.set(SPACES);
		wsaaFacthous.set(SPACES);
		wsaaBankkey.set(SPACES);
		wsaaBankacckey.set(SPACES);
		wsaaIntFromDate.set(ZERO);
		wsaaInterestDate.set(ZERO);
		
		regpBulkUpdtList = new ArrayList<Rgpdetpf>();
		chdrBulkUpdtList = new ArrayList<Chdrpf>();		
		regpBulkTmptList = new ArrayList<Rgpdetpf>();
		ptrnBulkInsList = new ArrayList<Ptrnpf>();
		t5645ListMap =  new HashMap<String, List<Itempf>>();
		
		td5h7ListMap =  new HashMap<String, List<Itempf>>();
		descrec = "DESCREC";
		ctrCT01 = 0;
		ctrCT02 = 0;
	}

protected void getSystemDateTime1040()
{
	wsaaTransactionDate.set(getCobolDate());
	wsaaTransactionTime.set(varcom.vrcmTime);
}

private void readSmartTables(String company){
	t5645ListMap = itemDAO.loadSmartTable("IT", company, "T5645");
	
	td5h7ListMap = itemDAO.loadSmartTable("IT", company, "TD5H7");	
}

protected void readT5645(){
	/* Read the accounting rules table*/
	String keyItemitem = wsaaProg.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5645rec.t5645Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}

protected void getItemDescription(){
	/* Get item description.*/
	t5645Map = descDAO.getItems("IT", strCompany, "T5645", bsscIO.getLanguage().toString());
    if (t5645Map.containsKey(wsaaProg)) {
        descpfT5645 = t5645Map.get(wsaaProg);
    } else {
    	descpfT5645 = new Descpf();
    	descpfT5645.setLongdesc("");
    }
	
	wsaaTrandesc.set(descpfT5645.getLongdesc());
}
/* Initialise1000 Section - Ends */

/*ReadFile2000 Sections - Starts */
protected void readFile2000(){
	if (!regpList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!regpList.isEmpty()){
				rpdetpf = new Rgpdetpf();
				rpdetpf = iteratorList.next();
				bd5hudto = rpdetpf.getBd5huDTO();	
				currChdrnum = rpdetpf.getChdrnum();
			}
		}else {		
			rpdetpf = new Rgpdetpf();
			rpdetpf = iteratorList.next();
			bd5hudto = rpdetpf.getBd5huDTO();
			currChdrnum = rpdetpf.getChdrnum();
		}	
	}else wsspEdterror.set(varcom.endp);
	
	if(isNE(prevChdrnum,currChdrnum)){
		prevChdrnum = currChdrnum;
		rcdCnt = 1;
		interestAmount.set(ZERO);
		String keyItemitem = bd5hudto.getCnttype().trim();
		readTd5h7(keyItemitem);		
	
	} else {
		rcdCnt++;
	}
}
protected void readTd5h7(String keyItemitem)
{	
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (td5h7ListMap.containsKey(keyItemitem)){	
		itempfList = td5h7ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();			
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){					
					td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}else{
				td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	
	if (!itemFound) {
		keyItemitem = "***";/* IJTI-1523 */
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (td5h7ListMap.containsKey(keyItemitem)){	
			itempfList = td5h7ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){					
						td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound2 = true;
					}
				}else{
					td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound2 = true;					
				}			
			}		
		}		
		if (!itemFound2) {
			syserrrec.params.set(td5h7rec.td5h7Rec);
			syserrrec.statuz.set(e723);
			fatalError600();	
		}		
	}		
}

/*ReadFile2000 Sections - Ends */

protected void edit2500()	{	
	wsspEdterror.set(varcom.oK);
	ctrCT01++;
	softlock2600();
	if (isEQ(sftlockrec.statuz, "LOCK")) {
		//contotrec.totno.set(ct02);
		//contotrec.totval.set(1);
		//callContot001();
		ctrCT02++;
		wsspEdterror.set(SPACES);
		return ;
	}
}
protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT01 = 0;

	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT02 = 0;
}

protected void softlock2600()
{
	start2610();
}

protected void start2610()
{
	/* Soft lock the contract, if it is to be processed.               */
	sftlockrec.function.set("LOCK");
	sftlockrec.company.set(strCompany);
	sftlockrec.enttyp.set("CH");
	sftlockrec.entity.set(rpdetpf.getChdrnum());
	sftlockrec.transaction.set(bprdIO.getAuthCode());
	sftlockrec.user.set(999999);
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)
	&& isNE(sftlockrec.statuz, "LOCK")) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}

//ILPI-229 START by dpuhawan
protected void update3000()
{
	calculateInterest3030();
	updateRegpRecord3040();
	if(isEQ(rcdCnt,bd5hudto.getCnt())){	
		regpBulkUpdtList.addAll(regpBulkTmptList);
		regpBulkTmptList.clear();
		updateChdrRecord3020();		
		postInterestAcmvs3050();	
		writePtrnRecord3070();			
		updateBatchControlTotals3080();		
	} 	
	removeSftlock2200();
}
protected void removeSftlock2200()
{
	/* Unlock Contract Header record*/
	sftlockrec.function.set("UNLK");
	sftlockrec.company.set(strCompany);
	sftlockrec.enttyp.set("CH");	
	sftlockrec.entity.set(rpdetpf.getChdrnum());
	sftlockrec.transaction.set(bprdIO.getAuthCode());
	sftlockrec.user.set(999999);
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}
protected void updateChdrRecord3020(){
	chdrpf = new Chdrpf();
	chdrpf.setUniqueNumber(bd5hudto.getUniqueNumber());
	chdrpf.setTranno(bd5hudto.getTranno() + 1);
	chdrTranno = Long.toString(bd5hudto.getTranno() + 1);
	chdrBulkUpdtList.add(chdrpf);		
}
protected void commitChdrBulkUpdate()
{	 
	boolean isUpdateChdr = chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
	if (!isUpdateChdr) {
		LOGGER.error("Update CHDR record failed.");
		fatalError600();
	}else chdrBulkUpdtList.clear();
}

protected void calculateInterest3030()
{
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(rpdetpf.getChdrcoy());
	annypyintcalcrec.chdrnum.set(rpdetpf.getChdrnum());
	annypyintcalcrec.cnttype.set(bd5hudto.getCnttype());
	annypyintcalcrec.interestTo.set(rpdetpf.getApnxtintbdte());
	wsaaInterestDate.set(rpdetpf.getApnxtintbdte());
	annypyintcalcrec.interestFrom.set(rpdetpf.getAplstintbdte());
	wsaaIntFromDate.set(rpdetpf.getAplstintbdte());
	annypyintcalcrec.annPaymt.set(rpdetpf.getApcaplamt());	
	annypyintcalcrec.lastCaplsnDate.set(rpdetpf.getAplstcapdate());	
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(rpdetpf.getCurrcd());
	annypyintcalcrec.transaction.set("ANNUITY");
	
	callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
	if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(annypyintcalcrec.intcalcRec);
		syserrrec.statuz.set(annypyintcalcrec.statuz);
		fatalError600();
	}	
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5000();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}		
		
}

protected void updateRegpRecord3040()
{
	intNxtBilDate=0;	
	calcNextBillDate3041();
//	rpdetpf.getApintamt().add(annypyintcalcrec.interestAmount.getbigdata());
	rpdetpf.setApintamt(add(rpdetpf.getApintamt(),annypyintcalcrec.interestAmount.getbigdata()).getbigdata());
	rpdetpf.setAplstintbdte(rpdetpf.getApnxtintbdte());	
	rpdetpf.setApnxtintbdte(intNxtBilDate);	
	regpBulkTmptList.add(rpdetpf);
	
}

protected void commitRegpUpdate(){
	boolean isUpdateRegp= regpdetpfDAO.updateRegpRecIntDate(regpBulkUpdtList);
	if (!isUpdateRegp) {
		LOGGER.error("Update REGP record failed.");
		fatalError600();
	}else {regpBulkUpdtList.clear();
	}	
}

protected void calcNextBillDate3041()
 {
	
	if (isNE(td5h7rec.intcalfreq,SPACES)) {
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(rpdetpf.getApnxtintbdte());
		datcon2rec.frequency.set(td5h7rec.intcalfreq);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		intNxtBilDate = datcon2rec.intDate2.toInt();
	}		
}

protected void postInterestAcmvs3050()
{
	/* Set up LIFACMV fields.*/
	lifacmvrec.lifacmvRec.set(SPACES);
	lifacmvrec.batccoy.set(batcdorrec.company);
	lifacmvrec.batcbrn.set(batcdorrec.branch);
	lifacmvrec.batcactyr.set(batcdorrec.actyear);
	lifacmvrec.batcactmn.set(batcdorrec.actmonth);
	lifacmvrec.batctrcde.set(batcdorrec.trcde);
	lifacmvrec.batcbatch.set(batcdorrec.batch);
	lifacmvrec.rldgcoy.set(rpdetpf.getChdrcoy());
	lifacmvrec.genlcoy.set(rpdetpf.getChdrcoy());
	lifacmvrec.rdocnum.set(rpdetpf.getChdrnum());
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.origcurr.set(rpdetpf.getCurrcd());
	lifacmvrec.origamt.set(interestAmount);
	wsaaSequenceNo.add(1);
	lifacmvrec.jrnseq.set(wsaaSequenceNo);
	lifacmvrec.rcamt.set(0);
	lifacmvrec.crate.set(0);
	lifacmvrec.acctamt.set(0);
	lifacmvrec.contot.set(0);
	lifacmvrec.tranno.set(chdrTranno);
	lifacmvrec.frcdate.set(99999999);
	lifacmvrec.effdate.set(rpdetpf.getAplstintbdte());
	lifacmvrec.tranref.set(rpdetpf.getChdrnum());
	lifacmvrec.trandesc.set(wsaaTrandesc);	
	lifacmvrec.rldgacct.set(rpdetpf.getChdrnum());	
	lifacmvrec.termid.set(wsaaTermid);
	lifacmvrec.transactionDate.set(wsaaTransactionDate);
	lifacmvrec.transactionTime.set(wsaaTransactionTime);
	lifacmvrec.user.set(wsaaUser);
	lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
	lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
	lifacmvrec.glcode.set(t5645rec.glmap[1]);
	lifacmvrec.glsign.set(t5645rec.sign[1]);
	lifacmvrec.function.set("PSTW");
	lifacmvrec.substituteCode[1].set(wsaaCnttype);
	if (isNE(lifacmvrec.origamt, ZERO)) {
		/*    CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.             */
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}
				
}


protected void writePtrnRecord3070(){

	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrcoy(rpdetpf.getChdrcoy());
	ptrnpf.setChdrpfx(bd5hudto.getChdrpfx());
	ptrnpf.setChdrnum(rpdetpf.getChdrnum());
	ptrnpf.setTranno(Integer.parseInt(chdrTranno));
	ptrnpf.setTrdt(wsaaTransactionDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setPtrneff(rpdetpf.getAplstintbdte());
	ptrnpf.setUserT(wsaaUser);
	ptrnpf.setBatccoy(batcdorrec.company.toString());
	ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
	ptrnpf.setBatcbrn(batcdorrec.branch.toString());
	ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
	ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
	ptrnBulkInsList.add(ptrnpf);
}

protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else ptrnBulkInsList.clear();
}

protected void updateBatchControlTotals3080(){
	batcuprec.batcupRec.set(SPACES);
	batcuprec.trancnt.set(1);
	batcuprec.etreqcnt.set(0);
	batcuprec.sub.set(0);
	batcuprec.bcnt.set(0);
	batcuprec.bval.set(0);
	batcuprec.ascnt.set(0);
	batcuprec.batcpfx.set(batcdorrec.prefix);
	batcuprec.batccoy.set(batcdorrec.company);
	batcuprec.batcbrn.set(batcdorrec.branch);
	batcuprec.batcactyr.set(batcdorrec.actyear);
	batcuprec.batctrcde.set(batcdorrec.trcde);
	batcuprec.batcactmn.set(batcdorrec.actmonth);
	batcuprec.batcbatch.set(batcdorrec.batch);
	batcuprec.function.set("WRITS");
	callProgram(Batcup.class, batcuprec.batcupRec);
	if (isNE(batcuprec.statuz, varcom.oK)) {
		syserrrec.params.set(batcuprec.batcupRec);
		syserrrec.statuz.set(batcuprec.statuz);
		fatalError600();
	}	
}



protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
		commitChdrBulkUpdate();		
		commitRegpUpdate();
		commitPtrnBulkInsert();
	}
}

protected void rollback3600(){
	/*ROLLBACK*/
	/*EXIT*/
}

protected void close4000(){
	if (!regpList.isEmpty()){
		t5645ListMap.clear();
		t5645ListMap = null;
		
		
		td5h7ListMap.clear();
		td5h7ListMap = null;
		
		regpList.clear();
		regpList = null;
		
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;	
		
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;	
				
		lsaaStatuz.set(varcom.oK);
	}
}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rpdetpf.getCurrcd());
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);}

}
