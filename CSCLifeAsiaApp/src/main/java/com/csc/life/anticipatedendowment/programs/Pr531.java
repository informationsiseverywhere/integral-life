/*
 * File: Pr531.java
 * Date: 30 August 2009 1:39:10
 * Author: Quipoz Limited
 * 
 * Class transformed from PR531.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.anticipatedendowment.screens.Sr531ScreenVars;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*   Alterations Sub Menu.
*
*****************************************************************
* </pre>
*/
public class Pr531 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR531");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String f910 = "F910";
	private static final String f917 = "F917";
	private static final String rl02 = "RL02";
	private static final String e944 = "E944";
	private static final String rrhh = "RRHH";
	private static final String e767 = "E767";
	
	
		/* FORMATS */
	private static final String zraerec = "ZRAEREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sr531ScreenVars sv = ScreenProgram.getScreenVars( Sr531ScreenVars.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private  Zraepf zraepf = new Zraepf(); 
	 
	private ZraepfDAO zraeDAO = getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	 private T5645rec t5645rec = new T5645rec();
	 private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	 private Acblpf acblpf = new Acblpf();
	 
	public String[] screendesc = new String[10];
	
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private Tr386rec tr386rec = new Tr386rec();
	private boolean isFeatureConfig = false;
	private static final String PRPRO001  = "PRPRO001"; 
	
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit12090, 
		exit3090
	}

	public Pr531() {
		super();
		screenVars = sv;
		new ScreenModel("Sr531", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		}
}
protected void initialise1000()
	{
		/*INITIALISE*/
	 isFeatureConfig  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), PRPRO001, appVars, "IT");
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		a7050Begin();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}


protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		sv.errorIndicators.set(SPACES);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
				if(isFeatureConfig && isEQ(sv.action.toString() , "B")){
					checkAntEndAllocation();
				}
			}
			else {
				verifyBatchControl2900();
			}
		}
		
		
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			syserrrec.params.set(sanctnrec.sanctnRec);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
			validateKey12210();
	}

protected void validateKey12210()
	{
		/*if (isNE(sv.action,"A")) {
			sv.actionErr.set(e005);
		}*/
		checkChdrExist2300();
		if (isNE(sv.chdrselErr,SPACES)) {
			return ;
		}
		checkCovrExist2310();
		if (isNE(sv.chdrselErr,SPACES)) {
			return ;
		}
		checkAntiEndowRec2400();
		if (isNE(sv.chdrselErr,SPACES)) {
			return ;
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(f910);
		}
	}

protected void checkChdrExist2300()
	{
			start2310();
	}

protected void start2310()
	{

	chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			sv.chdrselErr.set(f917);
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		chdrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		/*IBPLIFE-9342 */
		chdrpf = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), sv.chdrsel.toString().trim()); 		//ILB-1248

		if(null==chdrpf){
			sv.chdrselErr.set(e767);
			return;

		}
		else{
			chdrpfDAO.setCacheObject(chdrpf);
		}
	}

protected void checkCovrExist2310()
{
	covrpfList = covrpfDAO.readCovrForBilling(wsspcomn.company.toString(), sv.chdrsel.toString().trim()); //ILB-1248
	if (covrpfList.isEmpty()) {
		return;
	}
	else{
		covrpf = covrpfList.get(0);
		covrpfDAO.setCacheObject(covrpf);	
	}
}
protected void checkAntiEndowRec2400()
	{
		start2410();
	}

protected void start2410()
	{
		zraeIO.setDataKey(SPACES);
		zraeIO.setChdrcoy(wsspcomn.company);
		zraeIO.setChdrnum(sv.chdrsel);
		zraeIO.setPlanSuffix(ZERO);
		zraeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali

		zraeIO.setFormat(zraerec);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)
		&& isNE(zraeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zraeIO.getStatuz(),varcom.endp)
		|| isEQ(zraeIO.getValidflag(),"2")
		|| isNE(zraeIO.getChdrnum(),sv.chdrsel)) {
			sv.chdrselErr.set(rl02);
		}
		if (isEQ(sv.action,"A")) {
			zraeIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(),varcom.oK)
			&& isNE(zraeIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(zraeIO.getParams());
				syserrrec.statuz.set(zraeIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		try {
			updateWssp3010();
			batching3080();
		}
		catch (GOTOException e){
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(sv.action,"A")) {
			wsspcomn.flag.set("X");
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}


protected void a7050Begin()
{
	
	wsaaTr386Lang.set(wsspcomn.language);
	wsaaTr386Pgm.set(wsaaProg);
	Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
	
	
	tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
			if(isFeatureConfig){
			sv.scrndesc.set(tr386rec.progdesc[2].toString());
			sv.endflg.set("Y");
			}
			else
			{
			sv.scrndesc.set(tr386rec.progdesc[1].toString());
			sv.endflg.set("N");
			}
		

	
	
}


protected void checkAntEndAllocation(){

	zraepf = zraeDAO.getAllItem(sv.chdrsel.toString());
	if (zraepf == null) {
		sv.chdrselErr.set(e944);
		return;
	}
	Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645",wsaaProg.toString());
	if(itempf != null)
	{
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), StringUtils.rightPad(sv.chdrsel.toString(), 16), t5645rec.sacstype01.toString().trim(), chdrenqIO.getCntcurr().toString()); 
		int balance =  acblpf!=null?acblpf.getSacscurbal().intValue():0;
		if(balance== 0) {
			sv.chdrselErr.set(rrhh);
			return;
		}
	}
	else {
		syserrrec.params.set("t5645:" + wsaaProg);
		fatalError600();
	}


}
}
