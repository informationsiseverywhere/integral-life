/*
 * File: Br536.java
 * Date: 29 August 2009 22:16:45
 * Author: Quipoz Limited
 * 
 * Class transformed from BR536.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTallyAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.financials.dataaccess.PaytpfTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Upper;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Upperrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.anticipatedendowment.dataaccess.dao.ZraepfDAO;
import com.csc.life.anticipatedendowment.recordstructures.Pr536par;
import com.csc.life.anticipatedendowment.reports.Rr536Report;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.lifeasiaframework.dataaccess.LifeAsiaDAOFactory;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.T1667rec;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.batch.cls.Sectmpf;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   Payments Due Report
*
*      This program will produce:
*       - a report of all Payments due within the given lead
*         time (from the first parameter).
*       - advices for Payments which is due in a certain time
*         (from the second parameter).
*
*   Control totals:
*     01  -  No of pages printed
*     02  -  No of ZRAE records read
*     03  -  No of advice processed
*
*****************************************************************
* </pre>
*/
public class Br536 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlzraepf1rs = null;
	private java.sql.PreparedStatement sqlzraepf1ps = null;
	private java.sql.Connection sqlzraepf1conn = null;
	private String sqlzraepf1 = "";
	private PaytpfTableDAM paytpf = new PaytpfTableDAM();
	private Rr536Report printerFile = new Rr536Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR536");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaIdx = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCount = new PackedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaPercent = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaCurrAmount = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaAcctAmount = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaContTotal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMnthTotal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotTotal = new PackedDecimalData(17, 2);
	private String wsaaFirsttime = "";
	private ZonedDecimalData wsaaPrevMonth = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevYear = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaEndListing = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaEndAdvice = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMonths = new FixedLengthStringData(36).init("JanFebMarAprMayJunJulAugSepOctNovDec");

	private FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(wsaaMonths, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaMonth = FLSArrayPartOfStructure(12, 3, filler, 0);
		/* WSAA-DATES */
	private ZonedDecimalData wsaaDate8 = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaDate8, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaCent = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaDate6 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 2).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(wsaaDate6, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaYy = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2).setUnsigned();
	private ZonedDecimalData wsaaDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();

	private FixedLengthStringData wsaaDispDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaDispMm = new FixedLengthStringData(2).isAPartOf(wsaaDispDate, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaDispDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaDispDd = new FixedLengthStringData(2).isAPartOf(wsaaDispDate, 3);
	private FixedLengthStringData filler4 = new FixedLengthStringData(1).isAPartOf(wsaaDispDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaDispCent = new FixedLengthStringData(2).isAPartOf(wsaaDispDate, 6);
	private FixedLengthStringData wsaaDispYy = new FixedLengthStringData(2).isAPartOf(wsaaDispDate, 8);
		/* WSAA-SUBSCRIPTS */
	private PackedDecimalData wsaaSub = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaNext = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaFirstBlank = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaEmbedded = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaSalut = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaAddresses = new FixedLengthStringData(DD.cltaddr.length*6); //Starts ILIFE-3212
	private FixedLengthStringData wsaaAddr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaAddresses, 0);
	private FixedLengthStringData wsaaAddr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaAddresses, DD.cltaddr.length*1);
	private FixedLengthStringData wsaaAddr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaAddresses, DD.cltaddr.length*2);
	private FixedLengthStringData wsaaAddr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaAddresses, DD.cltaddr.length*3);
	private FixedLengthStringData wsaaAddr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaAddresses, DD.cltaddr.length*4);
	private FixedLengthStringData wsaaAddr6 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(wsaaAddresses, DD.cltaddr.length*5);

	private FixedLengthStringData filler5 = new FixedLengthStringData(wsaaAddresses.length()).isAPartOf(wsaaAddresses, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaAddr = FLSArrayPartOfStructure(6, DD.cltaddr.length, filler5, 0); // End ILIFE-3212
	private FixedLengthStringData wsaaAddrFlags = new FixedLengthStringData(6).init(SPACES);

	private FixedLengthStringData filler7 = new FixedLengthStringData(6).isAPartOf(wsaaAddrFlags, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaAddrFlag = FLSArrayPartOfStructure(6, 1, filler7, 0);
	private FixedLengthStringData wsaaEndSql = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaPaytFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler8 = new FixedLengthStringData(4).isAPartOf(wsaaPaytFn, 0, FILLER).init("PAYT");
	private FixedLengthStringData wsaaPaytRunid = new FixedLengthStringData(2).isAPartOf(wsaaPaytFn, 4);
	private ZonedDecimalData wsaaPaytJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaPaytFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String cltsrec = "CLTSREC";
		/* TABLES */
	private static final String t1667 = "T1667";
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5688 = "T5688";
	private static final String t3583 = "T3583";
	private static final String tr393 = "TR393";
		/* ERRORS */
	private static final String esql = "ESQL";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler11 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler13 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler13, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler13, 8);
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaDuedates = new FixedLengthStringData(40);
	private PackedDecimalData[] wsaaDuedate = PDArrayPartOfStructure(8, 8, 0, wsaaDuedates, 0);

	private FixedLengthStringData wsaaPercents = new FixedLengthStringData(24);
	private PackedDecimalData[] wsaaPrcnt = PDArrayPartOfStructure(8, 5, 2, wsaaPercents, 0);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rr536H01 = new FixedLengthStringData(93);
	private FixedLengthStringData rr536h01O = new FixedLengthStringData(93).isAPartOf(rr536H01, 0);
	private FixedLengthStringData datecfrom = new FixedLengthStringData(10).isAPartOf(rr536h01O, 0);
	private FixedLengthStringData datecto = new FixedLengthStringData(10).isAPartOf(rr536h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr536h01O, 20);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr536h01O, 21);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr536h01O, 51);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr536h01O, 61);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr536h01O, 63);

	private FixedLengthStringData rr536T01 = new FixedLengthStringData(51);
	private FixedLengthStringData rr536t01O = new FixedLengthStringData(51).isAPartOf(rr536T01, 0);
	private FixedLengthStringData cnttyp = new FixedLengthStringData(3).isAPartOf(rr536t01O, 0);
	private FixedLengthStringData ldesc = new FixedLengthStringData(30).isAPartOf(rr536t01O, 3);
	private ZonedDecimalData totalamt = new ZonedDecimalData(18, 2).isAPartOf(rr536t01O, 33);

	private FixedLengthStringData rr536T02 = new FixedLengthStringData(25);
	private FixedLengthStringData rr536t02O = new FixedLengthStringData(25).isAPartOf(rr536T02, 0);
	private FixedLengthStringData mthsdesc = new FixedLengthStringData(3).isAPartOf(rr536t02O, 0);
	private ZonedDecimalData zrsacprd = new ZonedDecimalData(4, 0).isAPartOf(rr536t02O, 3);
	private ZonedDecimalData tbalance = new ZonedDecimalData(18, 2).isAPartOf(rr536t02O, 7);

	private FixedLengthStringData rr536F01 = new FixedLengthStringData(18);
	private FixedLengthStringData rr536f01O = new FixedLengthStringData(18).isAPartOf(rr536F01, 0);
	private ZonedDecimalData totsuma = new ZonedDecimalData(18, 2).isAPartOf(rr536f01O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Upperrec upperrec = new Upperrec();
	private Pr536par pr536par = new Pr536par();
	private T3629rec t3629rec = new T3629rec();
	private T1667rec t1667rec = new T1667rec();
	private Tr393rec tr393rec = new Tr393rec();
	private InitRecordInner initRecordInner = new InitRecordInner();
	private Rr536D01Inner rr536D01Inner = new Rr536D01Inner();
	private SqlZraepfInner sqlZraepfInner = new SqlZraepfInner();
	private TmpRecordInner tmpRecordInner = new TmpRecordInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2060, 
		exit2090, 
		reRead3120, 
		exit3190, 
		blankAddressLines3270, 
		continue3280, 
		exit3290
	}

	public Br536() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		wsaaPaytRunid.set(bprdIO.getSystemParam04());
		wsaaPaytJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(1);
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaPaytFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/* Allocate the tape file exclusively to this job to prevent       */
		/* any other program, or person, from writing to this file during  */
		/* its execution.                                                  */
		wsaaQcmdexc.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("ALCOBJ OBJ((");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/", SPACES);
		stringVariable1.addExpression(wsaaPaytFn, SPACES);
		stringVariable1.addExpression(" *FILE *EXCL");
		stringVariable1.addExpression(" *FIRST))");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Do the override.                                             */
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(PAYTPF) TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaPaytFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.addExpression(" SEQONLY(*YES 1000)");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Open required files.*/
		printerFile.openOutput();
		/*                 TMP-FILE.                                       */
		paytpf.openOutput();
		pr536par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaPrevMonth.set(0);
		wsaaPrevYear.set(0);
		wsaaContTotal.set(0);
		wsaaMnthTotal.set(0);
		wsaaTotTotal.set(0);
		wsaaCount.set(0);
		wsaaPrevCnttype.set(SPACES);
		wsaaFirsttime = "Y";
		datcon2rec.freqFactor.set(pr536par.zrcshlead);
		datcon2rec.frequency.set("12");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaEndListing.set(datcon2rec.intDate2);
		datcon2rec.freqFactor.set(pr536par.zradvlead);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		/* Select ZRAE and CHDR records*/
		/* Retrieve all valid CHDR records corresponding to ZRAE*/
		/* records which are due according to the parameter entered.*/
		//MIBT-116
		//ILIFE-1827 START by avemula
		/*sqlzraepf1 = " SELECT  A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX, A.ZRDUEDTE01, A.ZRDUEDTE02, A.ZRDUEDTE03, A.ZRDUEDTE04, A.ZRDUEDTE05, A.ZRDUEDTE06, A.ZRDUEDTE07, A.ZRDUEDTE08, A.PRCNT01, A.PRCNT02, A.PRCNT03, A.PRCNT04, A.PRCNT05, A.PRCNT06, A.PRCNT07, A.PRCNT08, A.NPAYDATE, C.CNTTYPE, C.AGNTNUM, C.COWNPFX, C.AGNTCOY, C.COWNNUM, EXTRACT(MONTH FROM TO_DATE(A.NPAYDATE,'YYYYMMDD')) AS MNTH, EXTRACT(YEAR FROM TO_DATE(A.NPAYDATE,'YYYYMMDD')) AS YR" +
" FROM   " + getAppVars().getTableNameOverriden("ZRAEPF") + "  A,  " + getAppVars().getTableNameOverriden("CHDRPF") + "  C" +
" WHERE A.CHDRNUM = C.CHDRNUM" +
" AND C.VALIDFLAG = '1'" +
" AND A.VALIDFLAG = '1'" +
" AND A.NPAYDATE >= ?" +
" AND A.NPAYDATE <= ?" +
" ORDER BY C.CNTTYPE, A.NPAYDATE";
		 Open the cursor (this runs the query)
		sqlerrorflag = false;
		try {
			sqlzraepf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.anticipatedendowment.dataaccess.ZraepfTableDAM(), new com.csc.fsu.general.dataaccess.ChdrpfTableDAM()});
			sqlzraepf1ps = getAppVars().prepareStatementEmbeded(sqlzraepf1conn, sqlzraepf1);
			getAppVars().setDBNumber(sqlzraepf1ps, 1, wsaaEffdate);
			getAppVars().setDBNumber(sqlzraepf1ps, 2, wsaaEndListing);
			sqlzraepf1rs = getAppVars().executeQuery(sqlzraepf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}*/
		
		sqlerrorflag = false;
		try{
			//GlrppfDAO glrppfDAO = FsuDAOFactory.getAccountingInstance().getGlrppfDAO();
			ZraepfDAO zraepfDAO = LifeAsiaDAOFactory.anticipatedEndowmentInstance().getZraepfDAO();
			sqlzraepf1rs = zraepfDAO.getContractsForZRAE(getAppVars(),  wsaaEffdate, wsaaEndListing); 
		}catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		
		//ILIFE-1827 END
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datecfrom.set(datcon1rec.extDate);
		datcon1rec.intDate.set(wsaaEndListing);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datecto.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case eof2060: 
					eof2060();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlzraepf1rs)) {
				getAppVars().getDBObject(sqlzraepf1rs, 1, sqlZraepfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlzraepf1rs, 2, sqlZraepfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlzraepf1rs, 3, sqlZraepfInner.sqlLife);
				getAppVars().getDBObject(sqlzraepf1rs, 4, sqlZraepfInner.sqlCoverage);
				getAppVars().getDBObject(sqlzraepf1rs, 5, sqlZraepfInner.sqlRider);
				getAppVars().getDBObject(sqlzraepf1rs, 6, sqlZraepfInner.sqlPlanSfx);
				getAppVars().getDBObject(sqlzraepf1rs, 7, sqlZraepfInner.sqlZrduedte01);
				getAppVars().getDBObject(sqlzraepf1rs, 8, sqlZraepfInner.sqlZrduedte02);
				getAppVars().getDBObject(sqlzraepf1rs, 9, sqlZraepfInner.sqlZrduedte03);
				getAppVars().getDBObject(sqlzraepf1rs, 10, sqlZraepfInner.sqlZrduedte04);
				getAppVars().getDBObject(sqlzraepf1rs, 11, sqlZraepfInner.sqlZrduedte05);
				getAppVars().getDBObject(sqlzraepf1rs, 12, sqlZraepfInner.sqlZrduedte06);
				getAppVars().getDBObject(sqlzraepf1rs, 13, sqlZraepfInner.sqlZrduedte07);
				getAppVars().getDBObject(sqlzraepf1rs, 14, sqlZraepfInner.sqlZrduedte08);
				getAppVars().getDBObject(sqlzraepf1rs, 15, sqlZraepfInner.sqlPrcnt01);
				getAppVars().getDBObject(sqlzraepf1rs, 16, sqlZraepfInner.sqlPrcnt02);
				getAppVars().getDBObject(sqlzraepf1rs, 17, sqlZraepfInner.sqlPrcnt03);
				getAppVars().getDBObject(sqlzraepf1rs, 18, sqlZraepfInner.sqlPrcnt04);
				getAppVars().getDBObject(sqlzraepf1rs, 19, sqlZraepfInner.sqlPrcnt05);
				getAppVars().getDBObject(sqlzraepf1rs, 20, sqlZraepfInner.sqlPrcnt06);
				getAppVars().getDBObject(sqlzraepf1rs, 21, sqlZraepfInner.sqlPrcnt07);
				getAppVars().getDBObject(sqlzraepf1rs, 22, sqlZraepfInner.sqlPrcnt08);
				getAppVars().getDBObject(sqlzraepf1rs, 23, sqlZraepfInner.sqlNpaydate);
				getAppVars().getDBObject(sqlzraepf1rs, 24, sqlZraepfInner.sqlCnttype);
				getAppVars().getDBObject(sqlzraepf1rs, 25, sqlZraepfInner.sqlAgntnum);
				getAppVars().getDBObject(sqlzraepf1rs, 26, sqlZraepfInner.sqlCownpfx);
				getAppVars().getDBObject(sqlzraepf1rs, 27, sqlZraepfInner.sqlCowncoy);
				getAppVars().getDBObject(sqlzraepf1rs, 28, sqlZraepfInner.sqlCownnum);
				getAppVars().getDBObject(sqlzraepf1rs, 29, sqlZraepfInner.sqlMonth);
				getAppVars().getDBObject(sqlzraepf1rs, 30, sqlZraepfInner.sqlYear);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		wsaaCount.add(1);
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		if (isGT(wsaaCount,0)) {
			printContractTotal2700();
			printMonthTotal2800();
			totsuma.set(wsaaTotTotal);
			printerFile.printRr536f01(rr536F01, indicArea);
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		getComponentDetails2600();
		wsspEdterror.set(varcom.oK);
		/* Preventing control totals being printed in the beginning*/
		/* of the report.*/
		if (isEQ(wsaaFirsttime, "Y")) {
			wsaaFirsttime = "N";
			wsaaPrevCnttype.set(sqlZraepfInner.sqlCnttype);
			wsaaPrevMonth.set(sqlZraepfInner.sqlMonth);
			wsaaPrevYear.set(sqlZraepfInner.sqlYear);
		}
		else {
			/* Print control totals for a when Contract Type has changed.*/
			if (isNE(wsaaPrevCnttype, sqlZraepfInner.sqlCnttype)
			|| isNE(wsaaPrevMonth, sqlZraepfInner.sqlMonth)) {
				printContractTotal2700();
				wsaaPrevCnttype.set(sqlZraepfInner.sqlCnttype);
				wsaaContTotal.set(0);
			}
			/* Print control totals for a when the month of the Due dates*/
			/* has changed.*/
			if (isNE(wsaaPrevMonth, sqlZraepfInner.sqlMonth)) {
				printMonthTotal2800();
				wsaaPrevMonth.set(sqlZraepfInner.sqlMonth);
				wsaaPrevYear.set(sqlZraepfInner.sqlYear);
				wsaaMnthTotal.set(0);
			}
		}
		findCorrectPercentage2900();
		compute(wsaaCurrAmount, 2).set(div(mult(wsaaPercent, covrlnbIO.getSumins()), 100));
		getRate3100();
		compute(wsaaAcctAmount, 9).set(mult(wsaaCurrAmount,wsaaNominalRate));
		wsaaContTotal.add(wsaaAcctAmount);
		wsaaMnthTotal.add(wsaaAcctAmount);
		wsaaTotTotal.add(wsaaAcctAmount);
	}

protected void getComponentDetails2600()
	{
		start2610();
	}

protected void start2610()
	{
		covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setChdrcoy(sqlZraepfInner.sqlChdrcoy);
		covrlnbIO.setChdrnum(sqlZraepfInner.sqlChdrnum);
		covrlnbIO.setLife(sqlZraepfInner.sqlLife);
		covrlnbIO.setCoverage(sqlZraepfInner.sqlCoverage);
		covrlnbIO.setRider(sqlZraepfInner.sqlRider);
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFunction(varcom.readr);
		covrlnbIO.setFormat(covrlnbrec);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
		|| isNE(covrlnbIO.getValidflag(), "1")) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void printContractTotal2700()
	{
		start2710();
	}

protected void start2710()
	{
		cnttyp.set(wsaaPrevCnttype);
		descIO.setDescitem(wsaaPrevCnttype);
		descIO.setDesctabl(t5688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(sqlZraepfInner.sqlChdrcoy);
		descIO.setItemseq(SPACES);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		ldesc.set(descIO.getLongdesc());
		totalamt.set(wsaaContTotal);
		printerFile.printRr536t01(rr536T01, indicArea);
	}

protected void printMonthTotal2800()
	{
		/*START*/
		mthsdesc.set(wsaaMonth[wsaaPrevMonth.toInt()]);
		zrsacprd.set(wsaaPrevYear);
		tbalance.set(wsaaMnthTotal);
		printerFile.printRr536t02(rr536T02, indicArea);
		wsaaOverflow.set("Y");
		/*EXIT*/
	}

protected void findCorrectPercentage2900()
	{
		start2910();
	}

protected void start2910()
	{
		wsaaDuedates.set("99999999");
		wsaaPercents.set(ZERO);
		wsaaDuedate[1].set(sqlZraepfInner.sqlZrduedte01);
		wsaaDuedate[2].set(sqlZraepfInner.sqlZrduedte02);
		wsaaDuedate[3].set(sqlZraepfInner.sqlZrduedte03);
		wsaaDuedate[4].set(sqlZraepfInner.sqlZrduedte04);
		wsaaDuedate[5].set(sqlZraepfInner.sqlZrduedte05);
		wsaaDuedate[6].set(sqlZraepfInner.sqlZrduedte06);
		wsaaDuedate[7].set(sqlZraepfInner.sqlZrduedte07);
		wsaaDuedate[8].set(sqlZraepfInner.sqlZrduedte08);
		wsaaPrcnt[1].set(sqlZraepfInner.sqlPrcnt01);
		wsaaPrcnt[2].set(sqlZraepfInner.sqlPrcnt02);
		wsaaPrcnt[3].set(sqlZraepfInner.sqlPrcnt03);
		wsaaPrcnt[4].set(sqlZraepfInner.sqlPrcnt04);
		wsaaPrcnt[5].set(sqlZraepfInner.sqlPrcnt05);
		wsaaPrcnt[6].set(sqlZraepfInner.sqlPrcnt06);
		wsaaPrcnt[7].set(sqlZraepfInner.sqlPrcnt07);
		wsaaPrcnt[8].set(sqlZraepfInner.sqlPrcnt08);
		/* Find the correct percentage for this period of Payment.*/
		wsaaIdx.set(1);
		while ( !(isEQ(wsaaDuedate[wsaaIdx.toInt()], sqlZraepfInner.sqlNpaydate))) {
			wsaaIdx.add(1);
		}
		
		wsaaPercent.set(wsaaPrcnt[wsaaIdx.toInt()]);
	}

protected void update3000()
	{
		writeDetail3080();
	}

protected void writeDetail3080()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr536h01(rr536H01, indicArea);
			wsaaOverflow.set("N");
		}
		datcon1rec.intDate.set(sqlZraepfInner.sqlNpaydate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rr536D01Inner.datecls.set(datcon1rec.extDate);
		rr536D01Inner.agnt.set(sqlZraepfInner.sqlAgntnum);
		rr536D01Inner.chdrnum.set(sqlZraepfInner.sqlChdrnum);
		datcon1rec.intDate.set(covrlnbIO.getCrrcd());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rr536D01Inner.dateops.set(datcon1rec.extDate);
		rr536D01Inner.pcestrm.set(covrlnbIO.getPremCessTerm());
		rr536D01Inner.cntcurr.set(covrlnbIO.getPremCurrency());
		rr536D01Inner.sumins.set(covrlnbIO.getSumins());
		rr536D01Inner.prcnt.set(wsaaPercent);
		rr536D01Inner.origamt.set(wsaaCurrAmount);
		rr536D01Inner.acctamt.set(wsaaAcctAmount);
		/*  Write detail, checking for page overflow*/
		printerFile.printRr536d01(rr536D01Inner.rr536D01, indicArea);
	}

protected void getRate3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCurrencyRecord3110();
				case reRead3120: 
					reRead3120();
					findRate3150();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCurrencyRecord3110()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(sqlZraepfInner.sqlChdrcoy);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(covrlnbIO.getPremCurrency());
	}

protected void reRead3120()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		rr536D01Inner.ledgcurr.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaIdx.set(1);
		while ( !(isNE(wsaaNominalRate, ZERO)
		|| isGT(wsaaIdx, 7))) {
			findRate3150();
		}
		
		if (isEQ(wsaaNominalRate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.reRead3120);
			}
			else {
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}
		}
		else {
			goTo(GotoLabel.exit3190);
		}
	}

protected void findRate3150()
	{
		if (isGTE(bsscIO.getEffectiveDate(),t3629rec.frmdate[wsaaIdx.toInt()])
		&& isLTE(bsscIO.getEffectiveDate(),t3629rec.todate[wsaaIdx.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaIdx.toInt()]);
		}
		else {
			wsaaIdx.add(1);
		}
	}

protected void writeAdviceRec3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3210();
					currencyFormatTable3220();
					reformatSumInsurred3230();
					getOwnerDetails3260();
				case blankAddressLines3270: 
					blankAddressLines3270();
				case continue3280: 
					continue3280();
				case exit3290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3210()
	{
		tmpRecordInner.tmpRecord.set(initRecordInner.initRecord);
		wsaaDate8.set(sqlZraepfInner.sqlNpaydate);
		wsaaDispYy.set(wsaaYy);
		wsaaDispCent.set(wsaaCent);
		wsaaDispMm.set(wsaaMm);
		wsaaDispDd.set(wsaaDd);
		tmpRecordInner.tmpDuedate.set(wsaaDispDate);
		wsaaDate8.set(covrlnbIO.getCrrcd());
		wsaaDispYy.set(wsaaYy);
		wsaaDispCent.set(wsaaCent);
		wsaaDispMm.set(wsaaMm);
		wsaaDispDd.set(wsaaDd);
		tmpRecordInner.tmpRiskcomm.set(wsaaDispDate);
		tmpRecordInner.tmpChdrnum.set(sqlZraepfInner.sqlChdrnum);
		tmpRecordInner.tmpAgntnum.set(sqlZraepfInner.sqlAgntnum);
		tmpRecordInner.tmpCownnum.set(sqlZraepfInner.sqlCownnum);
		tmpRecordInner.tmpTerm.set(covrlnbIO.getPremCessTerm());
		tmpRecordInner.tmpCntcurr.set(covrlnbIO.getPremCurrency());
		tmpRecordInner.tmpLedgcurr.set(t3629rec.ledgcurr);
		tmpRecordInner.tmpSumin.set(covrlnbIO.getSumins());
		tmpRecordInner.tmpPercent.set(wsaaPercent);
		tmpRecordInner.tmpCurramt.set(wsaaCurrAmount);
		tmpRecordInner.tmpAcctamt.set(wsaaAcctAmount);
		tmpRecordInner.tmpCnttype.set(sqlZraepfInner.sqlCnttype);
		descIO.setDescitem(sqlZraepfInner.sqlCnttype);
		descIO.setDesctabl(t5688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(sqlZraepfInner.sqlChdrcoy);
		descIO.setItemseq(SPACES);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		tmpRecordInner.tmpCnttypeDesc.set(descIO.getLongdesc());
	}

protected void currencyFormatTable3220()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0 ");
		itemIO.setItemtabl(t1667);
		itemIO.setItemitem(t3629rec.ledgcurr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CURRENCY "+t3629rec.ledgcurr.toString() +" NOT FOUND IN T1667");
			goTo(GotoLabel.exit3290);
		}
		t1667rec.t1667Rec.set(itemIO.getGenarea());
	}

protected void reformatSumInsurred3230()
	{
		tmpRecordInner.tmpSumin.set(inspectReplaceAll(tmpRecordInner.tmpSumin, ",", "@"));
		tmpRecordInner.tmpSumin.set(inspectReplaceAll(tmpRecordInner.tmpSumin, ".", t1667rec.decimalCharacter));
		tmpRecordInner.tmpSumin.set(inspectReplaceAll(tmpRecordInner.tmpSumin, "@", t1667rec.delimitingCharacter));
		/*REFORMAT-CURR-AMOUNT*/
		tmpRecordInner.tmpCurramt.set(inspectReplaceAll(tmpRecordInner.tmpCurramt, ",", "@"));
		tmpRecordInner.tmpCurramt.set(inspectReplaceAll(tmpRecordInner.tmpCurramt, ".", t1667rec.decimalCharacter));
		tmpRecordInner.tmpCurramt.set(inspectReplaceAll(tmpRecordInner.tmpCurramt, "@", t1667rec.delimitingCharacter));
		/*REFORMAT-ACCT-AMOUNT*/
		tmpRecordInner.tmpAcctamt.set(inspectReplaceAll(tmpRecordInner.tmpAcctamt, ",", "@"));
		tmpRecordInner.tmpAcctamt.set(inspectReplaceAll(tmpRecordInner.tmpAcctamt, ".", t1667rec.decimalCharacter));
		tmpRecordInner.tmpAcctamt.set(inspectReplaceAll(tmpRecordInner.tmpAcctamt, "@", t1667rec.delimitingCharacter));
	}

protected void getOwnerDetails3260()
	{
		namadrsrec.clntPrefix.set(sqlZraepfInner.sqlCownpfx);
		namadrsrec.clntCompany.set("9 ");
		namadrsrec.clntNumber.set(sqlZraepfInner.sqlCownnum);
		namadrsrec.pcodeFlag.set("1");
		namadrsrec.function.set("LGNMF");
		/* MOVE 'E'                    TO NMAD-LANGUAGE.                */
		namadrsrec.language.set(bsscIO.getLanguage());
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		tmpRecordInner.tmpOwnerName.set(namadrsrec.name);
		getClientSalut3300();
		tmpRecordInner.tmpSalutName.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaSalut, SPACES);
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(tmpRecordInner.tmpOwnerName, "  ");
		stringVariable1.setStringInto(tmpRecordInner.tmpSalutName);
		wsaaAddrFlags.set(SPACES);
		wsaaAddr1.set(namadrsrec.addr1);
		wsaaAddr2.set(namadrsrec.addr2);
		wsaaAddr3.set(namadrsrec.addr3);
		wsaaAddr4.set(namadrsrec.addr4);
		wsaaAddr5.set(namadrsrec.addr5);
		wsaaAddr6.set(namadrsrec.pcode);
	}

protected void blankAddressLines3270()
	{
		wsaaFirstBlank.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,6)); wsaaSub.add(1)){
			if (isNE(wsaaAddr[wsaaSub.toInt()],SPACES)) {
				wsaaAddrFlag[wsaaSub.toInt()].set("Y");
			}
			else {
				if (isEQ(wsaaFirstBlank,ZERO)) {
					wsaaFirstBlank.set(wsaaSub);
				}
			}
		}
		wsaaEmbedded.set(ZERO);
		wsaaEmbedded.add(inspectTallyAll(wsaaAddrFlags, " Y"));
		if (isEQ(wsaaEmbedded,ZERO)) {
			goTo(GotoLabel.continue3280);
		}
		for (wsaaSub.set(wsaaFirstBlank); !(isGT(wsaaSub,5)); wsaaSub.add(1)){
			compute(wsaaNext, 0).set(add(1,wsaaSub));
			wsaaAddr[wsaaSub.toInt()].set(wsaaAddr[wsaaNext.toInt()]);
			wsaaAddr[wsaaNext.toInt()].set(SPACES);
			wsaaAddrFlag[wsaaSub.toInt()].set(wsaaAddrFlag[wsaaNext.toInt()]);
			wsaaAddrFlag[wsaaNext.toInt()].set(SPACES);
		}
		goTo(GotoLabel.blankAddressLines3270);
	}

protected void continue3280()
	{
		tmpRecordInner.tmpOwnerAddr1.set(wsaaAddr1);
		tmpRecordInner.tmpOwnerAddr2.set(wsaaAddr2);
		tmpRecordInner.tmpOwnerAddr3.set(wsaaAddr3);
		tmpRecordInner.tmpOwnerAddr4.set(wsaaAddr4);
		tmpRecordInner.tmpOwnerAddr5.set(wsaaAddr5);
		tmpRecordInner.tmpOwnerPcode.set(wsaaAddr6);
		paytpf.write(tmpRecordInner.tmpRecord);
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void getClientSalut3300()
	{
		start3310();
		readTr3933320();
		getSalutl3350();
	}

protected void start3310()
	{
		cltsIO.setDataKey(SPACES);
		wsaaSalut.set(SPACES);
		cltsIO.setClntpfx(sqlZraepfInner.sqlCownpfx);
		cltsIO.setClntcoy("9 ");
		cltsIO.setClntnum(sqlZraepfInner.sqlCownnum);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void readTr3933320()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getFsuco());
		itemIO.setItemtabl(tr393);
		itemIO.setItemitem(bsprIO.getFsuco());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr393rec.tr393Rec.set(itemIO.getGenarea());
	}

protected void getSalutl3350()
	{
		if (isEQ(tr393rec.salutind,"S")) {
			descT35833400();
			wsaaSalut.set(descIO.getShortdesc());
		}
		else {
			wsaaSalut.set(cltsIO.getSalutl());
		}
		/*EXIT*/
	}

protected void descT35833400()
	{
		start3410();
	}

protected void start3410()
	{
		upperrec.upperRec.set(SPACES);
		upperrec.name.set(cltsIO.getSalutl());
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz,varcom.oK)) {
			wsaaItemitem.set(cltsIO.getSalutl());
		}
		else {
			wsaaItemitem.set(upperrec.name);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getFsuco());
		descIO.setDesctabl(t3583);
		descIO.setDescitem(wsaaItemitem);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setShortdesc(SPACES);
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		printerFile.close();
		paytpf.close();
		callProgram(Sectmpf.class, bprdIO.getRunLibrary(), wsaaPaytFn, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		wsaaQcmdexc.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("DLCOBJ OBJ((");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/", SPACES);
		stringVariable1.addExpression(wsaaPaytFn, SPACES);
		stringVariable1.addExpression(" *FILE *EXCL");
		stringVariable1.addExpression(" *FIRST))");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaQcmdexc.set(SPACES);
		/*  Remove the override.*/
		wsaaQcmdexc.set("DLTOVR FILE(PAYTPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		lsaaStatuz.set(varcom.oK);
	}

protected void sqlError9000()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure TMP-RECORD--INNER
 */
private static final class TmpRecordInner { 

		/*FD  TMP-FILE                                                     */
	private FixedLengthStringData tmpRecord = new FixedLengthStringData(309+DD.cltaddr.length*5); //ILIFE-3212
	private FixedLengthStringData tmpChdrnum = new FixedLengthStringData(8).isAPartOf(tmpRecord, 0);
	private FixedLengthStringData tmpAgntnum = new FixedLengthStringData(8).isAPartOf(tmpRecord, 9);
	private FixedLengthStringData tmpCnttype = new FixedLengthStringData(3).isAPartOf(tmpRecord, 18);
	private FixedLengthStringData tmpCnttypeDesc = new FixedLengthStringData(30).isAPartOf(tmpRecord, 22);
	private FixedLengthStringData tmpCntcurr = new FixedLengthStringData(3).isAPartOf(tmpRecord, 53);
	private FixedLengthStringData tmpLedgcurr = new FixedLengthStringData(3).isAPartOf(tmpRecord, 57);
	private FixedLengthStringData tmpDuedate = new FixedLengthStringData(10).isAPartOf(tmpRecord, 61);
	private FixedLengthStringData tmpRiskcomm = new FixedLengthStringData(10).isAPartOf(tmpRecord, 72);
	private ZonedDecimalData tmpTerm = new ZonedDecimalData(3, 0).isAPartOf(tmpRecord, 83).setPattern("ZZ9");
	private ZonedDecimalData tmpPercent = new ZonedDecimalData(5, 2).isAPartOf(tmpRecord, 87).setPattern("ZZ9.99");
	private ZonedDecimalData tmpSumin = new ZonedDecimalData(17, 2).isAPartOf(tmpRecord, 94).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData tmpCurramt = new ZonedDecimalData(17, 2).isAPartOf(tmpRecord, 117).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData tmpAcctamt = new ZonedDecimalData(17, 2).isAPartOf(tmpRecord, 140).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData tmpOwnerDetails = new FixedLengthStringData(146+DD.cltaddr.length*5).isAPartOf(tmpRecord, 163); //ILIFE-3212
	private FixedLengthStringData tmpCownnum = new FixedLengthStringData(8).isAPartOf(tmpOwnerDetails, 0);
	private FixedLengthStringData tmpOwnerName = new FixedLengthStringData(60).isAPartOf(tmpOwnerDetails, 9);
	private FixedLengthStringData tmpSalutName = new FixedLengthStringData(60).isAPartOf(tmpOwnerDetails, 70);
	private FixedLengthStringData tmpOwnerAddr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(tmpOwnerDetails, 131); //Starts ILIFE-3212
	private FixedLengthStringData tmpOwnerAddr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(tmpOwnerDetails, 132+DD.cltaddr.length*1);
	private FixedLengthStringData tmpOwnerAddr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(tmpOwnerDetails, 133+DD.cltaddr.length*2);
	private FixedLengthStringData tmpOwnerAddr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(tmpOwnerDetails, 134+DD.cltaddr.length*3);
	private FixedLengthStringData tmpOwnerAddr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(tmpOwnerDetails, 135+DD.cltaddr.length*4);
	private FixedLengthStringData tmpOwnerPcode = new FixedLengthStringData(10).isAPartOf(tmpOwnerDetails, 136+DD.cltaddr.length*5); //End ILIFE-3212
}
/*
 * Class transformed  from Data Structure INIT-RECORD--INNER
 */
private static final class InitRecordInner { 

	private FixedLengthStringData initRecord = new FixedLengthStringData(309+DD.cltaddr.length*5); //ILIFE-3212
	private FixedLengthStringData initChdrnum = new FixedLengthStringData(8).isAPartOf(initRecord, 0).init(SPACES);
	private FixedLengthStringData initAgntnum = new FixedLengthStringData(8).isAPartOf(initRecord, 9).init(SPACES);
	private FixedLengthStringData initCnttype = new FixedLengthStringData(3).isAPartOf(initRecord, 18).init(SPACES);
	private FixedLengthStringData initCnttypeDesc = new FixedLengthStringData(30).isAPartOf(initRecord, 22).init(SPACES);
	private FixedLengthStringData initCntcurr = new FixedLengthStringData(3).isAPartOf(initRecord, 53).init(SPACES);
	private FixedLengthStringData initLedgcurr = new FixedLengthStringData(3).isAPartOf(initRecord, 57).init(SPACES);
	private FixedLengthStringData initDuedate = new FixedLengthStringData(10).isAPartOf(initRecord, 61).init(SPACES);
	private FixedLengthStringData initRiskcomm = new FixedLengthStringData(10).isAPartOf(initRecord, 72).init(SPACES);
	private ZonedDecimalData initTerm = new ZonedDecimalData(3, 0).isAPartOf(initRecord, 83).init(ZERO).setPattern("ZZ9");
	private ZonedDecimalData initPercent = new ZonedDecimalData(5, 2).isAPartOf(initRecord, 87).init(ZERO).setPattern("ZZ9.99");
	private ZonedDecimalData initSumin = new ZonedDecimalData(17, 2).isAPartOf(initRecord, 94).init(ZERO).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData initCurramt = new ZonedDecimalData(17, 2).isAPartOf(initRecord, 117).init(ZERO).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData initAcctamt = new ZonedDecimalData(17, 2).isAPartOf(initRecord, 140).init(ZERO).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData initOwnerDetails = new FixedLengthStringData(146+DD.cltaddr.length*5).isAPartOf(initRecord, 163); //ILIFE-3212
	private FixedLengthStringData initCownnum = new FixedLengthStringData(8).isAPartOf(initOwnerDetails, 0).init(SPACES);
	private FixedLengthStringData initOwnerName = new FixedLengthStringData(60).isAPartOf(initOwnerDetails, 9).init(SPACES);
	private FixedLengthStringData initSalutName = new FixedLengthStringData(60).isAPartOf(initOwnerDetails, 70).init(SPACES);
	private FixedLengthStringData initOwnerAddr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(initOwnerDetails, 131).init(SPACES); //Starts ILIFE-3212
	private FixedLengthStringData initOwnerAddr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(initOwnerDetails, 132+DD.cltaddr.length*1).init(SPACES);
	private FixedLengthStringData initOwnerAddr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(initOwnerDetails, 133+DD.cltaddr.length*2).init(SPACES);
	private FixedLengthStringData initOwnerAddr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(initOwnerDetails, 134+DD.cltaddr.length*3).init(SPACES);
	private FixedLengthStringData initOwnerAddr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(initOwnerDetails, 135+DD.cltaddr.length*4).init(SPACES);
	private FixedLengthStringData initOwnerPcode = new FixedLengthStringData(10).isAPartOf(initOwnerDetails, 136+DD.cltaddr.length*5).init(SPACES); //End ILIFE-3212
}
/*
 * Class transformed  from Data Structure SQL-ZRAEPF--INNER
 */
private static final class SqlZraepfInner { 

		/* SQL-ZRAEPF */
	private FixedLengthStringData sqlZraerec = new FixedLengthStringData(127);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlZraerec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlZraerec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlZraerec, 9);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlZraerec, 11);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlZraerec, 13);
	private PackedDecimalData sqlPlanSfx = new PackedDecimalData(4, 0).isAPartOf(sqlZraerec, 15);
	private PackedDecimalData sqlZrduedte01 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 18);
	private PackedDecimalData sqlZrduedte02 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 23);
	private PackedDecimalData sqlZrduedte03 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 28);
	private PackedDecimalData sqlZrduedte04 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 33);
	private PackedDecimalData sqlZrduedte05 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 38);
	private PackedDecimalData sqlZrduedte06 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 43);
	private PackedDecimalData sqlZrduedte07 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 48);
	private PackedDecimalData sqlZrduedte08 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 53);
	private PackedDecimalData sqlPrcnt01 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 58);
	private PackedDecimalData sqlPrcnt02 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 61);
	private PackedDecimalData sqlPrcnt03 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 64);
	private PackedDecimalData sqlPrcnt04 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 67);
	private PackedDecimalData sqlPrcnt05 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 70);
	private PackedDecimalData sqlPrcnt06 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 73);
	private PackedDecimalData sqlPrcnt07 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 76);
	private PackedDecimalData sqlPrcnt08 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 79);
	private PackedDecimalData sqlNpaydate = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 82);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlZraerec, 87);
	private FixedLengthStringData sqlAgntnum = new FixedLengthStringData(8).isAPartOf(sqlZraerec, 90);
	private FixedLengthStringData sqlCownpfx = new FixedLengthStringData(8).isAPartOf(sqlZraerec, 98);
	private FixedLengthStringData sqlCowncoy = new FixedLengthStringData(8).isAPartOf(sqlZraerec, 106);
	private FixedLengthStringData sqlCownnum = new FixedLengthStringData(8).isAPartOf(sqlZraerec, 114);
	private PackedDecimalData sqlMonth = new PackedDecimalData(2, 0).isAPartOf(sqlZraerec, 122);
	private PackedDecimalData sqlYear = new PackedDecimalData(4, 0).isAPartOf(sqlZraerec, 124);
}
/*
 * Class transformed  from Data Structure RR536-D01--INNER
 */
private static final class Rr536D01Inner { 

	private FixedLengthStringData rr536D01 = new FixedLengthStringData(101);
	private FixedLengthStringData rr536d01O = new FixedLengthStringData(101).isAPartOf(rr536D01, 0);
	private FixedLengthStringData datecls = new FixedLengthStringData(10).isAPartOf(rr536d01O, 0);
	private FixedLengthStringData agnt = new FixedLengthStringData(8).isAPartOf(rr536d01O, 10);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr536d01O, 18);
	private FixedLengthStringData dateops = new FixedLengthStringData(10).isAPartOf(rr536d01O, 26);
	private ZonedDecimalData pcestrm = new ZonedDecimalData(3, 0).isAPartOf(rr536d01O, 36);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rr536d01O, 39);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(rr536d01O, 42);
	private ZonedDecimalData prcnt = new ZonedDecimalData(5, 2).isAPartOf(rr536d01O, 59);
	private ZonedDecimalData origamt = new ZonedDecimalData(17, 2).isAPartOf(rr536d01O, 64);
	private FixedLengthStringData ledgcurr = new FixedLengthStringData(3).isAPartOf(rr536d01O, 81);
	private ZonedDecimalData acctamt = new ZonedDecimalData(17, 2).isAPartOf(rr536d01O, 84);
}
}
