package com.csc.life.anticipatedendowment.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:21
 * Description:
 * Copybook name: ZRGTUSRREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrgtusrrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(56);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(rec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 5);
  	public FixedLengthStringData userid = new FixedLengthStringData(10).isAPartOf(rec, 9);
  	public PackedDecimalData usernum = new PackedDecimalData(6, 0).isAPartOf(rec, 19).setUnsigned();
  	public FixedLengthStringData groupname = new FixedLengthStringData(10).isAPartOf(rec, 23);
  	public FixedLengthStringData strdate = new FixedLengthStringData(10).isAPartOf(rec, 33);
  	public FixedLengthStringData enddate = new FixedLengthStringData(10).isAPartOf(rec, 43);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rec, 53);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rec, 54);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}