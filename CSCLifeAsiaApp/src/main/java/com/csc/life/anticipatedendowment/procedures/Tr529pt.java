/*
 * File: Tr529pt.java
 * Date: 30 August 2009 2:42:58
 * Author: Quipoz Limited
 * 
 * Class transformed from TR529PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.anticipatedendowment.tablestructures.Tr529rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR529.
*
*
*****************************************************************
* </pre>
*/
public class Tr529pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr529rec tr529rec = new Tr529rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr529pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr529rec.tr529Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo015.set(tr529rec.zrterm01);
		generalCopyLinesInner.fieldNo024.set(tr529rec.zrterm02);
		generalCopyLinesInner.fieldNo033.set(tr529rec.zrterm03);
		generalCopyLinesInner.fieldNo042.set(tr529rec.zrterm04);
		generalCopyLinesInner.fieldNo051.set(tr529rec.zrterm05);
		generalCopyLinesInner.fieldNo060.set(tr529rec.zrterm06);
		generalCopyLinesInner.fieldNo069.set(tr529rec.zrterm07);
		generalCopyLinesInner.fieldNo078.set(tr529rec.zrterm08);
		generalCopyLinesInner.fieldNo087.set(tr529rec.zrterm09);
		generalCopyLinesInner.fieldNo096.set(tr529rec.zrterm10);
		generalCopyLinesInner.fieldNo007.set(tr529rec.zrnoyrs01);
		generalCopyLinesInner.fieldNo008.set(tr529rec.zrnoyrs02);
		generalCopyLinesInner.fieldNo009.set(tr529rec.zrnoyrs03);
		generalCopyLinesInner.fieldNo010.set(tr529rec.zrnoyrs04);
		generalCopyLinesInner.fieldNo011.set(tr529rec.zrnoyrs05);
		generalCopyLinesInner.fieldNo012.set(tr529rec.zrnoyrs06);
		generalCopyLinesInner.fieldNo013.set(tr529rec.zrnoyrs07);
		generalCopyLinesInner.fieldNo014.set(tr529rec.zrnoyrs08);
		generalCopyLinesInner.fieldNo016.set(tr529rec.zrpcpd01);
		generalCopyLinesInner.fieldNo017.set(tr529rec.zrpcpd02);
		generalCopyLinesInner.fieldNo018.set(tr529rec.zrpcpd03);
		generalCopyLinesInner.fieldNo019.set(tr529rec.zrpcpd04);
		generalCopyLinesInner.fieldNo020.set(tr529rec.zrpcpd05);
		generalCopyLinesInner.fieldNo021.set(tr529rec.zrpcpd06);
		generalCopyLinesInner.fieldNo022.set(tr529rec.zrpcpd07);
		generalCopyLinesInner.fieldNo023.set(tr529rec.zrpcpd08);
		generalCopyLinesInner.fieldNo025.set(tr529rec.zrpcpd09);
		generalCopyLinesInner.fieldNo026.set(tr529rec.zrpcpd10);
		generalCopyLinesInner.fieldNo027.set(tr529rec.zrpcpd11);
		generalCopyLinesInner.fieldNo028.set(tr529rec.zrpcpd12);
		generalCopyLinesInner.fieldNo029.set(tr529rec.zrpcpd13);
		generalCopyLinesInner.fieldNo030.set(tr529rec.zrpcpd14);
		generalCopyLinesInner.fieldNo031.set(tr529rec.zrpcpd15);
		generalCopyLinesInner.fieldNo032.set(tr529rec.zrpcpd16);
		generalCopyLinesInner.fieldNo034.set(tr529rec.zrpcpd17);
		generalCopyLinesInner.fieldNo035.set(tr529rec.zrpcpd18);
		generalCopyLinesInner.fieldNo036.set(tr529rec.zrpcpd19);
		generalCopyLinesInner.fieldNo037.set(tr529rec.zrpcpd20);
		generalCopyLinesInner.fieldNo038.set(tr529rec.zrpcpd21);
		generalCopyLinesInner.fieldNo039.set(tr529rec.zrpcpd22);
		generalCopyLinesInner.fieldNo040.set(tr529rec.zrpcpd23);
		generalCopyLinesInner.fieldNo041.set(tr529rec.zrpcpd24);
		generalCopyLinesInner.fieldNo043.set(tr529rec.zrpcpd25);
		generalCopyLinesInner.fieldNo044.set(tr529rec.zrpcpd26);
		generalCopyLinesInner.fieldNo045.set(tr529rec.zrpcpd27);
		generalCopyLinesInner.fieldNo046.set(tr529rec.zrpcpd28);
		generalCopyLinesInner.fieldNo047.set(tr529rec.zrpcpd29);
		generalCopyLinesInner.fieldNo048.set(tr529rec.zrpcpd30);
		generalCopyLinesInner.fieldNo049.set(tr529rec.zrpcpd31);
		generalCopyLinesInner.fieldNo050.set(tr529rec.zrpcpd32);
		generalCopyLinesInner.fieldNo052.set(tr529rec.zrpcpd33);
		generalCopyLinesInner.fieldNo053.set(tr529rec.zrpcpd34);
		generalCopyLinesInner.fieldNo054.set(tr529rec.zrpcpd35);
		generalCopyLinesInner.fieldNo055.set(tr529rec.zrpcpd36);
		generalCopyLinesInner.fieldNo056.set(tr529rec.zrpcpd37);
		generalCopyLinesInner.fieldNo057.set(tr529rec.zrpcpd38);
		generalCopyLinesInner.fieldNo058.set(tr529rec.zrpcpd39);
		generalCopyLinesInner.fieldNo059.set(tr529rec.zrpcpd40);
		generalCopyLinesInner.fieldNo061.set(tr529rec.zrpcpd41);
		generalCopyLinesInner.fieldNo062.set(tr529rec.zrpcpd42);
		generalCopyLinesInner.fieldNo063.set(tr529rec.zrpcpd43);
		generalCopyLinesInner.fieldNo064.set(tr529rec.zrpcpd44);
		generalCopyLinesInner.fieldNo065.set(tr529rec.zrpcpd45);
		generalCopyLinesInner.fieldNo066.set(tr529rec.zrpcpd46);
		generalCopyLinesInner.fieldNo067.set(tr529rec.zrpcpd47);
		generalCopyLinesInner.fieldNo068.set(tr529rec.zrpcpd48);
		generalCopyLinesInner.fieldNo070.set(tr529rec.zrpcpd49);
		generalCopyLinesInner.fieldNo071.set(tr529rec.zrpcpd50);
		generalCopyLinesInner.fieldNo072.set(tr529rec.zrpcpd51);
		generalCopyLinesInner.fieldNo073.set(tr529rec.zrpcpd52);
		generalCopyLinesInner.fieldNo074.set(tr529rec.zrpcpd53);
		generalCopyLinesInner.fieldNo075.set(tr529rec.zrpcpd54);
		generalCopyLinesInner.fieldNo076.set(tr529rec.zrpcpd55);
		generalCopyLinesInner.fieldNo077.set(tr529rec.zrpcpd56);
		generalCopyLinesInner.fieldNo079.set(tr529rec.zrpcpd57);
		generalCopyLinesInner.fieldNo080.set(tr529rec.zrpcpd58);
		generalCopyLinesInner.fieldNo081.set(tr529rec.zrpcpd59);
		generalCopyLinesInner.fieldNo082.set(tr529rec.zrpcpd60);
		generalCopyLinesInner.fieldNo083.set(tr529rec.zrpcpd61);
		generalCopyLinesInner.fieldNo084.set(tr529rec.zrpcpd62);
		generalCopyLinesInner.fieldNo085.set(tr529rec.zrpcpd63);
		generalCopyLinesInner.fieldNo086.set(tr529rec.zrpcpd64);
		generalCopyLinesInner.fieldNo088.set(tr529rec.zrpcpd65);
		generalCopyLinesInner.fieldNo089.set(tr529rec.zrpcpd66);
		generalCopyLinesInner.fieldNo090.set(tr529rec.zrpcpd67);
		generalCopyLinesInner.fieldNo091.set(tr529rec.zrpcpd68);
		generalCopyLinesInner.fieldNo092.set(tr529rec.zrpcpd69);
		generalCopyLinesInner.fieldNo093.set(tr529rec.zrpcpd70);
		generalCopyLinesInner.fieldNo094.set(tr529rec.zrpcpd71);
		generalCopyLinesInner.fieldNo095.set(tr529rec.zrpcpd72);
		generalCopyLinesInner.fieldNo097.set(tr529rec.zrpcpd73);
		generalCopyLinesInner.fieldNo098.set(tr529rec.zrpcpd74);
		generalCopyLinesInner.fieldNo099.set(tr529rec.zrpcpd75);
		generalCopyLinesInner.fieldNo100.set(tr529rec.zrpcpd76);
		generalCopyLinesInner.fieldNo101.set(tr529rec.zrpcpd77);
		generalCopyLinesInner.fieldNo102.set(tr529rec.zrpcpd78);
		generalCopyLinesInner.fieldNo103.set(tr529rec.zrpcpd79);
		generalCopyLinesInner.fieldNo104.set(tr529rec.zrpcpd80);
		generalCopyLinesInner.fieldNo105.set(tr529rec.zredsumas);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Anticipated Endowment Rules - Term              SR529");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(56);
	private FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine004, 24, FILLER).init("Benefit Payment (at end of year)");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(70);
	private FixedLengthStringData filler11 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Coverage");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 13).setPattern("ZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 20).setPattern("ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 28).setPattern("ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 36).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 44).setPattern("ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 52).setPattern("ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 60).setPattern("ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine005, 68).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(6);
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Term");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 3).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 3).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 3).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 3).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler56 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 3).setPattern("ZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(73);
	private FixedLengthStringData filler65 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 3).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler74 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 3).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler83 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 3).setPattern("ZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler92 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 3).setPattern("ZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(73);
	private FixedLengthStringData filler101 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 3).setPattern("ZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(73);
	private FixedLengthStringData filler110 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Sum Assured Reduced by Benefit Payment Amount  Non-Blank denotes YES");
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 72);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(28);
	private FixedLengthStringData filler111 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
}
}
