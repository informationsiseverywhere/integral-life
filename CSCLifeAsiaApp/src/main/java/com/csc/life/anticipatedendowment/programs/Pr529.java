/*
 * File: Pr529.java
 * Date: 30 August 2009 1:38:55
 * Author: Quipoz Limited
 *
 * Class transformed from PR529.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.anticipatedendowment.procedures.Tr529pt;
import com.csc.life.anticipatedendowment.screens.Sr529ScreenVars;
import com.csc.life.anticipatedendowment.tablestructures.Tr529rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Pr529 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR529");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr529rec tr529rec = new Tr529rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr529ScreenVars sv = ScreenProgram.getScreenVars( Sr529ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		other3080,
		exit3090
	}

	public Pr529() {
		super();
		screenVars = sv;
		new ScreenModel("Sr529", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr529rec.tr529Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			return ;
		}
		tr529rec.zrnoyrs01.set(ZERO);
		tr529rec.zrnoyrs02.set(ZERO);
		tr529rec.zrnoyrs03.set(ZERO);
		tr529rec.zrnoyrs04.set(ZERO);
		tr529rec.zrnoyrs05.set(ZERO);
		tr529rec.zrnoyrs06.set(ZERO);
		tr529rec.zrnoyrs07.set(ZERO);
		tr529rec.zrnoyrs08.set(ZERO);
		tr529rec.zrpcpd01.set(ZERO);
		tr529rec.zrpcpd02.set(ZERO);
		tr529rec.zrpcpd03.set(ZERO);
		tr529rec.zrpcpd04.set(ZERO);
		tr529rec.zrpcpd05.set(ZERO);
		tr529rec.zrpcpd06.set(ZERO);
		tr529rec.zrpcpd07.set(ZERO);
		tr529rec.zrpcpd08.set(ZERO);
		tr529rec.zrpcpd09.set(ZERO);
		tr529rec.zrpcpd10.set(ZERO);
		tr529rec.zrpcpd11.set(ZERO);
		tr529rec.zrpcpd12.set(ZERO);
		tr529rec.zrpcpd13.set(ZERO);
		tr529rec.zrpcpd14.set(ZERO);
		tr529rec.zrpcpd15.set(ZERO);
		tr529rec.zrpcpd16.set(ZERO);
		tr529rec.zrpcpd17.set(ZERO);
		tr529rec.zrpcpd18.set(ZERO);
		tr529rec.zrpcpd19.set(ZERO);
		tr529rec.zrpcpd20.set(ZERO);
		tr529rec.zrpcpd21.set(ZERO);
		tr529rec.zrpcpd22.set(ZERO);
		tr529rec.zrpcpd23.set(ZERO);
		tr529rec.zrpcpd24.set(ZERO);
		tr529rec.zrpcpd25.set(ZERO);
		tr529rec.zrpcpd26.set(ZERO);
		tr529rec.zrpcpd27.set(ZERO);
		tr529rec.zrpcpd28.set(ZERO);
		tr529rec.zrpcpd29.set(ZERO);
		tr529rec.zrpcpd30.set(ZERO);
		tr529rec.zrpcpd31.set(ZERO);
		tr529rec.zrpcpd32.set(ZERO);
		tr529rec.zrpcpd33.set(ZERO);
		tr529rec.zrpcpd34.set(ZERO);
		tr529rec.zrpcpd35.set(ZERO);
		tr529rec.zrpcpd36.set(ZERO);
		tr529rec.zrpcpd37.set(ZERO);
		tr529rec.zrpcpd38.set(ZERO);
		tr529rec.zrpcpd39.set(ZERO);
		tr529rec.zrpcpd40.set(ZERO);
		tr529rec.zrpcpd41.set(ZERO);
		tr529rec.zrpcpd42.set(ZERO);
		tr529rec.zrpcpd43.set(ZERO);
		tr529rec.zrpcpd44.set(ZERO);
		tr529rec.zrpcpd45.set(ZERO);
		tr529rec.zrpcpd46.set(ZERO);
		tr529rec.zrpcpd47.set(ZERO);
		tr529rec.zrpcpd48.set(ZERO);
		tr529rec.zrpcpd49.set(ZERO);
		tr529rec.zrpcpd50.set(ZERO);
		tr529rec.zrpcpd51.set(ZERO);
		tr529rec.zrpcpd52.set(ZERO);
		tr529rec.zrpcpd53.set(ZERO);
		tr529rec.zrpcpd54.set(ZERO);
		tr529rec.zrpcpd55.set(ZERO);
		tr529rec.zrpcpd56.set(ZERO);
		tr529rec.zrpcpd57.set(ZERO);
		tr529rec.zrpcpd58.set(ZERO);
		tr529rec.zrpcpd59.set(ZERO);
		tr529rec.zrpcpd60.set(ZERO);
		tr529rec.zrpcpd61.set(ZERO);
		tr529rec.zrpcpd62.set(ZERO);
		tr529rec.zrpcpd63.set(ZERO);
		tr529rec.zrpcpd64.set(ZERO);
		tr529rec.zrpcpd65.set(ZERO);
		tr529rec.zrpcpd66.set(ZERO);
		tr529rec.zrpcpd67.set(ZERO);
		tr529rec.zrpcpd68.set(ZERO);
		tr529rec.zrpcpd69.set(ZERO);
		tr529rec.zrpcpd70.set(ZERO);
		tr529rec.zrpcpd71.set(ZERO);
		tr529rec.zrpcpd72.set(ZERO);
		tr529rec.zrpcpd73.set(ZERO);
		tr529rec.zrpcpd74.set(ZERO);
		tr529rec.zrpcpd75.set(ZERO);
		tr529rec.zrpcpd76.set(ZERO);
		tr529rec.zrpcpd77.set(ZERO);
		tr529rec.zrpcpd78.set(ZERO);
		tr529rec.zrpcpd79.set(ZERO);
		tr529rec.zrpcpd80.set(ZERO);
		tr529rec.zrterm01.set(ZERO);
		tr529rec.zrterm02.set(ZERO);
		tr529rec.zrterm03.set(ZERO);
		tr529rec.zrterm04.set(ZERO);
		tr529rec.zrterm05.set(ZERO);
		tr529rec.zrterm06.set(ZERO);
		tr529rec.zrterm07.set(ZERO);
		tr529rec.zrterm08.set(ZERO);
		tr529rec.zrterm09.set(ZERO);
		tr529rec.zrterm10.set(ZERO);
	}

protected void generalArea1045()
	{
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.zredsumas.set(tr529rec.zredsumas);
		sv.zrnoyrss.set(tr529rec.zrnoyrss);
		sv.zrpcpds.set(tr529rec.zrpcpds);
		sv.zrterms.set(tr529rec.zrterms);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080:
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr529rec.tr529Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zredsumas, tr529rec.zredsumas)) {
			tr529rec.zredsumas.set(sv.zredsumas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrnoyrss, tr529rec.zrnoyrss)) {
			tr529rec.zrnoyrss.set(sv.zrnoyrss);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrpcpds, tr529rec.zrpcpds)) {
			tr529rec.zrpcpds.set(sv.zrpcpds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrterms, tr529rec.zrterms)) {
			tr529rec.zrterms.set(sv.zrterms);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr529pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
