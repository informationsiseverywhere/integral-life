package com.csc.life.anticipatedendowment.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR536.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr536Report extends SMARTReportLayout { 

	private ZonedDecimalData acctamt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData agnt = new FixedLengthStringData(8);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData cnttyp = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData datecfrom = new FixedLengthStringData(10);
	private FixedLengthStringData datecls = new FixedLengthStringData(10);
	private FixedLengthStringData datecto = new FixedLengthStringData(10);
	private FixedLengthStringData dateops = new FixedLengthStringData(10);
	private FixedLengthStringData ldesc = new FixedLengthStringData(30);
	private FixedLengthStringData ledgcurr = new FixedLengthStringData(3);
	private FixedLengthStringData mthsdesc = new FixedLengthStringData(3);
	private ZonedDecimalData origamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData pcestrm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData prcnt = new ZonedDecimalData(5, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData tbalance = new ZonedDecimalData(18, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totalamt = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totsuma = new ZonedDecimalData(18, 2);
	private ZonedDecimalData zrsacprd = new ZonedDecimalData(4, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr536Report() {
		super();
	}


	/**
	 * Print the XML for Rr536d01
	 */
	public void printRr536d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datecls.setFieldName("datecls");
		datecls.setInternal(subString(recordData, 1, 10));
		agnt.setFieldName("agnt");
		agnt.setInternal(subString(recordData, 11, 8));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 19, 8));
		dateops.setFieldName("dateops");
		dateops.setInternal(subString(recordData, 27, 10));
		pcestrm.setFieldName("pcestrm");
		pcestrm.setInternal(subString(recordData, 37, 3));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 40, 3));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 43, 17));
		prcnt.setFieldName("prcnt");
		prcnt.setInternal(subString(recordData, 60, 5));
		origamt.setFieldName("origamt");
		origamt.setInternal(subString(recordData, 65, 17));
		ledgcurr.setFieldName("ledgcurr");
		ledgcurr.setInternal(subString(recordData, 82, 3));
		acctamt.setFieldName("acctamt");
		acctamt.setInternal(subString(recordData, 85, 17));
		printLayout("Rr536d01",			// Record name
			new BaseData[]{			// Fields:
				datecls,
				agnt,
				chdrnum,
				dateops,
				pcestrm,
				cntcurr,
				sumins,
				prcnt,
				origamt,
				ledgcurr,
				acctamt
			}
		);

	}

	/**
	 * Print the XML for Rr536f01
	 */
	public void printRr536f01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		totsuma.setFieldName("totsuma");
		totsuma.setInternal(subString(recordData, 1, 18));
		printLayout("Rr536f01",			// Record name
			new BaseData[]{			// Fields:
				totsuma
			}
		);

		currentPrintLine.add(6);
	}

	/**
	 * Print the XML for Rr536h01
	 */
	public void printRr536h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datecfrom.setFieldName("datecfrom");
		datecfrom.setInternal(subString(recordData, 1, 10));
		datecto.setFieldName("datecto");
		datecto.setInternal(subString(recordData, 11, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 21, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 22, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 52, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 62, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 64, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr536h01",			// Record name
			new BaseData[]{			// Fields:
				datecfrom,
				datecto,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(12);
	}

	/**
	 * Print the XML for Rr536t01
	 */
	public void printRr536t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cnttyp.setFieldName("cnttyp");
		cnttyp.setInternal(subString(recordData, 1, 3));
		ldesc.setFieldName("ldesc");
		ldesc.setInternal(subString(recordData, 4, 30));
		totalamt.setFieldName("totalamt");
		totalamt.setInternal(subString(recordData, 34, 18));
		printLayout("Rr536t01",			// Record name
			new BaseData[]{			// Fields:
				cnttyp,
				ldesc,
				totalamt
			}
		);

		currentPrintLine.add(4);
	}

	/**
	 * Print the XML for Rr536t02
	 */
	public void printRr536t02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		mthsdesc.setFieldName("mthsdesc");
		mthsdesc.setInternal(subString(recordData, 1, 3));
		zrsacprd.setFieldName("zrsacprd");
		zrsacprd.setInternal(subString(recordData, 4, 4));
		tbalance.setFieldName("tbalance");
		tbalance.setInternal(subString(recordData, 8, 18));
		printLayout("Rr536t02",			// Record name
			new BaseData[]{			// Fields:
				mthsdesc,
				zrsacprd,
				tbalance
			}
		);

		currentPrintLine.add(4);
	}


}
