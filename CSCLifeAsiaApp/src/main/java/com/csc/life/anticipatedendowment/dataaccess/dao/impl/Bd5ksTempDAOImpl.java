package com.csc.life.anticipatedendowment.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.Bd5ksTempDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5ksDTO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Bd5ksTempDAOImpl extends BaseDAOImpl<Object> implements Bd5ksTempDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5ksTempDAOImpl.class);	

	public boolean deleteBd5ksTempAllRecords(){	
			StringBuilder sb = new StringBuilder("DELETE FROM BD5KSTEMP");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	
	            ps.executeUpdate();
			}catch (SQLException e) {
				LOGGER.error("Bd5ksTempDAOImpl()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return true;
		
	}
	
	public int insertBd5ksTempRecords(int batchExtractSize,String coy, int effDate){
		int rows = 0;
		StringBuilder sb = new StringBuilder("insert into BD5KSTEMP ");
		sb.append("(BATCHID, CNT, CHDRCOY,CHDRNUM,UNIQUE_NUMBER, PLNSFX,LIFE,COVERAGE,RIDER,PAYCURR, VALIDFLAG, FLAG, APCAPLAMT, APINTAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, APNXTINTBDTE,TRANNO,CNTTYPE,CHDRPFX,SERVUNIT,OCCDATE,CCDATE, COWNPFX,COWNCOY,COWNNUM,COLLCHNL,CNTBRANCH,CNTCURR,POLSUM, POLINC,AGNTPFX,AGNTCOY,AGNTNUM,DATIME) ");
		sb.append("select * from ( ");
		sb.append("select floor((row_number()over(ORDER BY RP.CHDRCOY ASC, RP.CHDRNUM ASC)-1)/?) ROWNM, count(*) over(partition by RP.CHDRCOY,RP.CHDRNUM) as cnt,");
		sb.append("RP.CHDRCOY,RP.CHDRNUM,RP.UNIQUE_NUMBER,RP.PLNSFX,RP.LIFE,RP.COVERAGE,RP.RIDER,  RP.PAYCURR, RP.VALIDFLAG, RP.FLAG, RP.APCAPLAMT, RP.APINTAMT,RP.APLSTCAPDATE,"); 
		sb.append("RP.APNXTCAPDATE, RP.APLSTINTBDTE, RP.APNXTINTBDTE, ");
		sb.append("CH.TRANNO, CH.CNTTYPE, CH.CHDRPFX, CH.SERVUNIT, CH.OCCDATE, CH.CCDATE, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM,");
		sb.append("CH.COLLCHNL, CH.CNTBRANCH, CH.CNTCURR, CH.POLSUM, CH.POLINC, CH.AGNTPFX, CH.AGNTCOY, CH.AGNTNUM, ? DATIME ");
		sb.append("FROM  ZRAEPF RP ");
		sb.append("INNER JOIN CHDRPF CH ON  CH.CHDRCOY = RP.CHDRCOY  AND CH.CHDRNUM=RP.CHDRNUM  AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG in ('1','3') ");
		sb.append("WHERE RP.FLAG = '1'  ");
		sb.append("AND RP.VALIDFLAG = '1'   ");
		sb.append("AND RP.APCAPLAMT > 0 ");
		sb.append(" AND RP.CHDRCOY = ?  AND (RP.APNXTCAPDATE < ?  OR RP.APNXTCAPDATE = ?) ");
		sb.append(") loan order by CHDRCOY, CHDRNUM");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			ps.setString(3, coy);
			ps.setInt(4, effDate);
			ps.setInt(5, effDate);
			rows = ps.executeUpdate();
		}catch (SQLException e) {
			LOGGER.error("insertBd5ksTempRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return rows;
	}
	
	public List<Zraepf> loadDataByNEWINTCP(int BatchID){
		StringBuilder sb = new StringBuilder("SELECT * FROM (");
		sb.append(" SELECT BATCHID,CNT, CHDRCOY, CHDRNUM,UNIQUE_NUMBER, PLNSFX,LIFE,COVERAGE,RIDER, PAYCURR,");
		sb.append(" VALIDFLAG, FLAG, APCAPLAMT, APINTAMT,APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, APNXTINTBDTE, TRANNO, CNTTYPE, CHDRPFX, SERVUNIT, OCCDATE,");
		sb.append(" CCDATE, COWNPFX, COWNCOY, COWNNUM, COLLCHNL, CNTBRANCH, CNTCURR, POLSUM, POLINC, AGNTPFX, AGNTCOY, AGNTNUM ");
		sb.append(" FROM BD5KSTEMP");		
		sb.append(") TEMP WHERE BATCHID = ? ");	
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		//ILIFE-3210
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Zraepf> zraeList = new ArrayList<Zraepf>();
		try{
			ps = getPrepareStatement(sb.toString());
			
			ps.setInt(1, BatchID);	
			rs = ps.executeQuery();
			while(rs.next()){
				Zraepf zraepf = new Zraepf();
				zraepf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				zraepf.setChdrcoy(rs.getString("CHDRCOY").trim());
				zraepf.setChdrnum(rs.getString("CHDRNUM").trim());
				zraepf.setPlnsfx(rs.getInt("Plnsfx"));
                zraepf.setLife(rs.getString("Life"));
                zraepf.setCoverage(rs.getString("Coverage"));
                zraepf.setRider(rs.getString("Rider"));                
                zraepf.setValidflag(rs.getString("Validflag"));
                zraepf.setFlag(rs.getString("Flag"));
                zraepf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
                zraepf.setApintamt(rs.getBigDecimal("APINTAMT"));
                zraepf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
                zraepf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
                zraepf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
                zraepf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));
                zraepf.setPaycurr(rs.getString("PAYCURR"));
				Bd5ksDTO bd5ksdto = new Bd5ksDTO();
				bd5ksdto.setCnt(rs.getInt("CNT"));
				bd5ksdto.setTranno(rs.getInt("TRANNO"));
				bd5ksdto.setCnttype(rs.getString("CNTTYPE").trim());
				bd5ksdto.setChdrpfx(rs.getString("CHDRPFX").trim());
				bd5ksdto.setServunit(rs.getString("SERVUNIT").trim());
				bd5ksdto.setOccdate(rs.getInt("OCCDATE"));
				bd5ksdto.setCcdate(rs.getInt("CCDATE"));
				bd5ksdto.setCownpfx(rs.getString("COWNPFX").trim());
				bd5ksdto.setCowncoy(rs.getString("COWNCOY").trim());
				bd5ksdto.setCownnum(rs.getString("COWNNUM").trim());				
				bd5ksdto.setCollchnl(rs.getString("COLLCHNL").trim());
				bd5ksdto.setCntbranch(rs.getString("CNTBRANCH").trim());
				bd5ksdto.setCntcurr(rs.getString("CNTCURR").trim());
				bd5ksdto.setPolinc(rs.getObject("POLINC") != null ? (rs.getInt("POLINC")) : 0);
				bd5ksdto.setPolsum(rs.getObject("POLSUM") != null ? (rs.getInt("POLSUM")) : 0);
				bd5ksdto.setAgntpfx(rs.getString("AGNTPFX").trim());				
				bd5ksdto.setAgntcoy(rs.getString("AGNTCOY").trim());
				bd5ksdto.setAgntnum(rs.getString("AGNTNUM").trim());
				
				zraepf.setBd5ksDTO(bd5ksdto);
				zraeList.add(zraepf);	
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByEffdate()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zraeList;
	}
	

}
