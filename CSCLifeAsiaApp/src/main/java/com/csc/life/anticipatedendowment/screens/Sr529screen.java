package com.csc.life.anticipatedendowment.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr529screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr529ScreenVars sv = (Sr529ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr529screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr529ScreenVars screenVars = (Sr529ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.zrterm01.setClassString("");
		screenVars.zrterm02.setClassString("");
		screenVars.zrterm03.setClassString("");
		screenVars.zrterm04.setClassString("");
		screenVars.zrterm05.setClassString("");
		screenVars.zrterm06.setClassString("");
		screenVars.zrterm07.setClassString("");
		screenVars.zrterm08.setClassString("");
		screenVars.zrterm09.setClassString("");
		screenVars.zrterm10.setClassString("");
		screenVars.zrnoyrs01.setClassString("");
		screenVars.zrnoyrs02.setClassString("");
		screenVars.zrnoyrs03.setClassString("");
		screenVars.zrnoyrs04.setClassString("");
		screenVars.zrnoyrs05.setClassString("");
		screenVars.zrnoyrs06.setClassString("");
		screenVars.zrnoyrs07.setClassString("");
		screenVars.zrnoyrs08.setClassString("");
		screenVars.zrpcpd01.setClassString("");
		screenVars.zrpcpd02.setClassString("");
		screenVars.zrpcpd03.setClassString("");
		screenVars.zrpcpd04.setClassString("");
		screenVars.zrpcpd05.setClassString("");
		screenVars.zrpcpd06.setClassString("");
		screenVars.zrpcpd07.setClassString("");
		screenVars.zrpcpd08.setClassString("");
		screenVars.zrpcpd09.setClassString("");
		screenVars.zrpcpd10.setClassString("");
		screenVars.zrpcpd11.setClassString("");
		screenVars.zrpcpd12.setClassString("");
		screenVars.zrpcpd13.setClassString("");
		screenVars.zrpcpd14.setClassString("");
		screenVars.zrpcpd15.setClassString("");
		screenVars.zrpcpd16.setClassString("");
		screenVars.zrpcpd17.setClassString("");
		screenVars.zrpcpd18.setClassString("");
		screenVars.zrpcpd19.setClassString("");
		screenVars.zrpcpd20.setClassString("");
		screenVars.zrpcpd21.setClassString("");
		screenVars.zrpcpd22.setClassString("");
		screenVars.zrpcpd23.setClassString("");
		screenVars.zrpcpd24.setClassString("");
		screenVars.zrpcpd25.setClassString("");
		screenVars.zrpcpd26.setClassString("");
		screenVars.zrpcpd27.setClassString("");
		screenVars.zrpcpd28.setClassString("");
		screenVars.zrpcpd29.setClassString("");
		screenVars.zrpcpd30.setClassString("");
		screenVars.zrpcpd31.setClassString("");
		screenVars.zrpcpd32.setClassString("");
		screenVars.zrpcpd33.setClassString("");
		screenVars.zrpcpd34.setClassString("");
		screenVars.zrpcpd35.setClassString("");
		screenVars.zrpcpd36.setClassString("");
		screenVars.zrpcpd37.setClassString("");
		screenVars.zrpcpd38.setClassString("");
		screenVars.zrpcpd39.setClassString("");
		screenVars.zrpcpd40.setClassString("");
		screenVars.zrpcpd41.setClassString("");
		screenVars.zrpcpd42.setClassString("");
		screenVars.zrpcpd43.setClassString("");
		screenVars.zrpcpd44.setClassString("");
		screenVars.zrpcpd45.setClassString("");
		screenVars.zrpcpd46.setClassString("");
		screenVars.zrpcpd47.setClassString("");
		screenVars.zrpcpd48.setClassString("");
		screenVars.zrpcpd49.setClassString("");
		screenVars.zrpcpd50.setClassString("");
		screenVars.zrpcpd51.setClassString("");
		screenVars.zrpcpd52.setClassString("");
		screenVars.zrpcpd53.setClassString("");
		screenVars.zrpcpd54.setClassString("");
		screenVars.zrpcpd55.setClassString("");
		screenVars.zrpcpd56.setClassString("");
		screenVars.zrpcpd57.setClassString("");
		screenVars.zrpcpd58.setClassString("");
		screenVars.zrpcpd59.setClassString("");
		screenVars.zrpcpd60.setClassString("");
		screenVars.zrpcpd61.setClassString("");
		screenVars.zrpcpd62.setClassString("");
		screenVars.zrpcpd63.setClassString("");
		screenVars.zrpcpd64.setClassString("");
		screenVars.zrpcpd65.setClassString("");
		screenVars.zrpcpd66.setClassString("");
		screenVars.zrpcpd67.setClassString("");
		screenVars.zrpcpd68.setClassString("");
		screenVars.zrpcpd69.setClassString("");
		screenVars.zrpcpd70.setClassString("");
		screenVars.zrpcpd71.setClassString("");
		screenVars.zrpcpd72.setClassString("");
		screenVars.zrpcpd73.setClassString("");
		screenVars.zrpcpd74.setClassString("");
		screenVars.zrpcpd75.setClassString("");
		screenVars.zrpcpd76.setClassString("");
		screenVars.zrpcpd77.setClassString("");
		screenVars.zrpcpd78.setClassString("");
		screenVars.zrpcpd79.setClassString("");
		screenVars.zrpcpd80.setClassString("");
		screenVars.zredsumas.setClassString("");
	}

/**
 * Clear all the variables in Sr529screen
 */
	public static void clear(VarModel pv) {
		Sr529ScreenVars screenVars = (Sr529ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.zrterm01.clear();
		screenVars.zrterm02.clear();
		screenVars.zrterm03.clear();
		screenVars.zrterm04.clear();
		screenVars.zrterm05.clear();
		screenVars.zrterm06.clear();
		screenVars.zrterm07.clear();
		screenVars.zrterm08.clear();
		screenVars.zrterm09.clear();
		screenVars.zrterm10.clear();
		screenVars.zrnoyrs01.clear();
		screenVars.zrnoyrs02.clear();
		screenVars.zrnoyrs03.clear();
		screenVars.zrnoyrs04.clear();
		screenVars.zrnoyrs05.clear();
		screenVars.zrnoyrs06.clear();
		screenVars.zrnoyrs07.clear();
		screenVars.zrnoyrs08.clear();
		screenVars.zrpcpd01.clear();
		screenVars.zrpcpd02.clear();
		screenVars.zrpcpd03.clear();
		screenVars.zrpcpd04.clear();
		screenVars.zrpcpd05.clear();
		screenVars.zrpcpd06.clear();
		screenVars.zrpcpd07.clear();
		screenVars.zrpcpd08.clear();
		screenVars.zrpcpd09.clear();
		screenVars.zrpcpd10.clear();
		screenVars.zrpcpd11.clear();
		screenVars.zrpcpd12.clear();
		screenVars.zrpcpd13.clear();
		screenVars.zrpcpd14.clear();
		screenVars.zrpcpd15.clear();
		screenVars.zrpcpd16.clear();
		screenVars.zrpcpd17.clear();
		screenVars.zrpcpd18.clear();
		screenVars.zrpcpd19.clear();
		screenVars.zrpcpd20.clear();
		screenVars.zrpcpd21.clear();
		screenVars.zrpcpd22.clear();
		screenVars.zrpcpd23.clear();
		screenVars.zrpcpd24.clear();
		screenVars.zrpcpd25.clear();
		screenVars.zrpcpd26.clear();
		screenVars.zrpcpd27.clear();
		screenVars.zrpcpd28.clear();
		screenVars.zrpcpd29.clear();
		screenVars.zrpcpd30.clear();
		screenVars.zrpcpd31.clear();
		screenVars.zrpcpd32.clear();
		screenVars.zrpcpd33.clear();
		screenVars.zrpcpd34.clear();
		screenVars.zrpcpd35.clear();
		screenVars.zrpcpd36.clear();
		screenVars.zrpcpd37.clear();
		screenVars.zrpcpd38.clear();
		screenVars.zrpcpd39.clear();
		screenVars.zrpcpd40.clear();
		screenVars.zrpcpd41.clear();
		screenVars.zrpcpd42.clear();
		screenVars.zrpcpd43.clear();
		screenVars.zrpcpd44.clear();
		screenVars.zrpcpd45.clear();
		screenVars.zrpcpd46.clear();
		screenVars.zrpcpd47.clear();
		screenVars.zrpcpd48.clear();
		screenVars.zrpcpd49.clear();
		screenVars.zrpcpd50.clear();
		screenVars.zrpcpd51.clear();
		screenVars.zrpcpd52.clear();
		screenVars.zrpcpd53.clear();
		screenVars.zrpcpd54.clear();
		screenVars.zrpcpd55.clear();
		screenVars.zrpcpd56.clear();
		screenVars.zrpcpd57.clear();
		screenVars.zrpcpd58.clear();
		screenVars.zrpcpd59.clear();
		screenVars.zrpcpd60.clear();
		screenVars.zrpcpd61.clear();
		screenVars.zrpcpd62.clear();
		screenVars.zrpcpd63.clear();
		screenVars.zrpcpd64.clear();
		screenVars.zrpcpd65.clear();
		screenVars.zrpcpd66.clear();
		screenVars.zrpcpd67.clear();
		screenVars.zrpcpd68.clear();
		screenVars.zrpcpd69.clear();
		screenVars.zrpcpd70.clear();
		screenVars.zrpcpd71.clear();
		screenVars.zrpcpd72.clear();
		screenVars.zrpcpd73.clear();
		screenVars.zrpcpd74.clear();
		screenVars.zrpcpd75.clear();
		screenVars.zrpcpd76.clear();
		screenVars.zrpcpd77.clear();
		screenVars.zrpcpd78.clear();
		screenVars.zrpcpd79.clear();
		screenVars.zrpcpd80.clear();
		screenVars.zredsumas.clear();
	}
}
