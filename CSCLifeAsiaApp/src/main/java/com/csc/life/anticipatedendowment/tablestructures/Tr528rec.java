package com.csc.life.anticipatedendowment.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:59
 * Description:
 * Copybook name: TR528REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr528rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr528Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bankaccreq = new FixedLengthStringData(1).isAPartOf(tr528Rec, 0);
  	public FixedLengthStringData paymentMethod = new FixedLengthStringData(1).isAPartOf(tr528Rec, 1);
  	public FixedLengthStringData defaultSel = new FixedLengthStringData(1).isAPartOf(tr528Rec, 2);
  	public FixedLengthStringData filler = new FixedLengthStringData(497).isAPartOf(tr528Rec, 3, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr528Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr528Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}