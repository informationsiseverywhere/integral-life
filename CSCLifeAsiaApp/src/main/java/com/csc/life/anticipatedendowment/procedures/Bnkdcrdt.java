
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Bnkdcrdt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("BNKDCRDT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Payreqrec payreqrec = new Payreqrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrcshoprec zrcshoprec = new Zrcshoprec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9020
	}

	public Bnkdcrdt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		zrcshoprec.rec = convertAndSetParam(zrcshoprec.rec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		wsaaTranDate.set(getCobolDate());
		wsaaTranTime.set(999999);
	    start2010();
		para010();
		exit099();
	}

protected void para010() // bank direct credit
	{
		zrcshoprec.statuz.set(varcom.oK);
		payreqrec.effdate.set(zrcshoprec.effdate);
		payreqrec.paycurr.set(zrcshoprec.paycurr);
		payreqrec.pymt.set(zrcshoprec.pymt);
		payreqrec.trandesc.set(zrcshoprec.trandesc);
		payreqrec.bankkey.set(zrcshoprec.bankkey);
		payreqrec.bankacckey.set(zrcshoprec.bankacckey);
		payreqrec.frmGlDet.set(zrcshoprec.frmGlDet);
		payreqrec.termid.set(zrcshoprec.termid);
		payreqrec.user.set(zrcshoprec.user);
		payreqrec.reqntype.set(zrcshoprec.reqntype);
		payreqrec.clntcoy.set(zrcshoprec.clntcoy);
		payreqrec.clntnum.set(zrcshoprec.clntnum);
		payreqrec.bankcode.set(zrcshoprec.bankcode);
		payreqrec.cheqno.set(zrcshoprec.cheqno);
		payreqrec.batckey.set(zrcshoprec.batckey);
		payreqrec.tranref.set(zrcshoprec.tranref);
		payreqrec.language.set(zrcshoprec.language);
		payreqrec.zbatctrcde.set(zrcshoprec.batctrcde); 
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(payreqrec.statuz);
			syserrrec.params.set(payreqrec.rec);
			error9010();
		}
	}
protected void start2010() {
	lifacmvrec.lifacmvRec.set(SPACES);
	lifacmvrec.batccoy.set(zrcshoprec.batccoy);
	lifacmvrec.batcbrn.set(zrcshoprec.batcbrn);
	lifacmvrec.batcactyr.set(zrcshoprec.batcactyr);
	lifacmvrec.batcactmn.set(zrcshoprec.batcactmn);
	lifacmvrec.batctrcde.set(zrcshoprec.batctrcde);
	lifacmvrec.batcbatch.set(zrcshoprec.batcbatch);
	lifacmvrec.rldgcoy.set(zrcshoprec.chdrcoy);
	lifacmvrec.genlcoy.set(zrcshoprec.chdrcoy);
	lifacmvrec.rdocnum.set(zrcshoprec.chdrnum);
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.origcurr.set(zrcshoprec.cntcurr);
	lifacmvrec.origamt.set(zrcshoprec.pymt);
	lifacmvrec.jrnseq.set(0);
	lifacmvrec.rcamt.set(0);
	lifacmvrec.crate.set(0);
	lifacmvrec.acctamt.set(0);
	lifacmvrec.contot.set(0);
	lifacmvrec.tranno.set(zrcshoprec.tranno);
	lifacmvrec.frcdate.set(99999999);
	lifacmvrec.effdate.set(zrcshoprec.effdate);
	lifacmvrec.tranref.set(zrcshoprec.chdrnum);
	lifacmvrec.trandesc.set(zrcshoprec.trandesc);
	lifacmvrec.rldgacct.set(zrcshoprec.chdrnum);
	lifacmvrec.termid.set(zrcshoprec.termid);
	lifacmvrec.transactionDate.set(wsaaTranDate);
	lifacmvrec.transactionTime.set(wsaaTranTime);
	lifacmvrec.user.set(zrcshoprec.user);
	lifacmvrec.sacscode.set(zrcshoprec.sacscode);
	lifacmvrec.sacstyp.set(zrcshoprec.sacstyp);
	lifacmvrec.glcode.set(zrcshoprec.glcode);
	lifacmvrec.glsign.set(zrcshoprec.sign);
	lifacmvrec.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz, varcom.oK)) {
		zrcshoprec.errorFormat.set(lifacmvrec.lifacmvRec);
		zrcshoprec.statuz.set(lifacmvrec.statuz);
		error9010();
	}
}

protected void exit099()
	{
		exitProgram();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			exit9020();
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		zrcshoprec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
