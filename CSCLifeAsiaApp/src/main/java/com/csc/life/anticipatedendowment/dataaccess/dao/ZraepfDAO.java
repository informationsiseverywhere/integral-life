package com.csc.life.anticipatedendowment.dataaccess.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.csc.life.anticipatedendowment.dataaccess.model.Zraepf;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.datatype.PackedDecimalData;

public interface ZraepfDAO  extends BaseDAO<Object> {
	
	//ILIFE-1827 START by avemula
	/* Select ZRAE and CHDR records*/
	/* Retrieve all valid CHDR records corresponding to ZRAE*/
	/* records which are due according to the parameter entered.*/
	public ResultSet getContractsForZRAE(SMARTAppVars appVars,  PackedDecimalData wsaaEffdate, PackedDecimalData wsaaEndListing) throws SQLException ;
	//ILIFE-1827 END
	public Zraepf getZraepf(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
	public int updateZraepf(int currto, String validflag, long uniqueNumber);
	public void insertZraeRecord(Zraepf zraepf);

}
