package com.csc.life.anticipatedendowment.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr527screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr527ScreenVars sv = (Sr527ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr527screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr527ScreenVars screenVars = (Sr527ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.payOption01.setClassString("");
		screenVars.pymdesc01.setClassString("");
		screenVars.payOption02.setClassString("");
		screenVars.pymdesc02.setClassString("");
		screenVars.payOption03.setClassString("");
		screenVars.pymdesc03.setClassString("");
		screenVars.payOption04.setClassString("");
		screenVars.pymdesc04.setClassString("");
		screenVars.payOption05.setClassString("");
		screenVars.pymdesc05.setClassString("");
		screenVars.payOption06.setClassString("");
		screenVars.pymdesc06.setClassString("");
		screenVars.payOption07.setClassString("");
		screenVars.pymdesc07.setClassString("");
		screenVars.payOption08.setClassString("");
		screenVars.pymdesc08.setClassString("");
		screenVars.payOption09.setClassString("");
		screenVars.pymdesc09.setClassString("");
		screenVars.payOption10.setClassString("");
		screenVars.pymdesc10.setClassString("");
		screenVars.payOption11.setClassString("");
		screenVars.pymdesc11.setClassString("");
		screenVars.payOption12.setClassString("");
		screenVars.pymdesc12.setClassString("");
		screenVars.payOption13.setClassString("");
		screenVars.pymdesc13.setClassString("");
		screenVars.payOption14.setClassString("");
		screenVars.pymdesc14.setClassString("");
	}

/**
 * Clear all the variables in Sr527screen
 */
	public static void clear(VarModel pv) {
		Sr527ScreenVars screenVars = (Sr527ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.payOption01.clear();
		screenVars.pymdesc01.clear();
		screenVars.payOption02.clear();
		screenVars.pymdesc02.clear();
		screenVars.payOption03.clear();
		screenVars.pymdesc03.clear();
		screenVars.payOption04.clear();
		screenVars.pymdesc04.clear();
		screenVars.payOption05.clear();
		screenVars.pymdesc05.clear();
		screenVars.payOption06.clear();
		screenVars.pymdesc06.clear();
		screenVars.payOption07.clear();
		screenVars.pymdesc07.clear();
		screenVars.payOption08.clear();
		screenVars.pymdesc08.clear();
		screenVars.payOption09.clear();
		screenVars.pymdesc09.clear();
		screenVars.payOption10.clear();
		screenVars.pymdesc10.clear();
		screenVars.payOption11.clear();
		screenVars.pymdesc11.clear();
		screenVars.payOption12.clear();
		screenVars.pymdesc12.clear();
		screenVars.payOption13.clear();
		screenVars.pymdesc13.clear();
		screenVars.payOption14.clear();
		screenVars.pymdesc14.clear();
	}
}
