package com.csc.life.anticipatedendowment.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sa542screen extends ScreenRecord{ 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa542ScreenVars sv = (Sa542ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa542screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa542ScreenVars screenVars = (Sa542ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.currdesc.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.btdate.setClassString("");
		screenVars.intcalfreq.setClassString("");
		screenVars.intcapfreq.setClassString("");
		screenVars.intRate.setClassString("");
		screenVars.accamnt.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.register.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.rgpymop.setClassString("");
		screenVars.refcode.setClassString("");
		screenVars.ansrepy.setClassString("");
	}

/**
 * Clear all the variables in Sa542screen
 */
	public static void clear(VarModel pv) {
		Sa542ScreenVars screenVars = (Sa542ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.chdrstatus.clear();
		screenVars.rstate.clear();
		screenVars.planSuffix.clear();
		screenVars.currcd.clear();
		screenVars.currdesc.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.btdate.clear();
		screenVars.intcalfreq.clear();
		screenVars.intcapfreq.clear();
		screenVars.intRate.setClassString("");
		screenVars.accamnt.clear();
		screenVars.ddind.clear();
		screenVars.register.clear();
		screenVars.premstatus.clear();
		screenVars.rgpymop.clear();
		screenVars.refcode.clear();
		screenVars.ansrepy.clear();
		
		}
}
