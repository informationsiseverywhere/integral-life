package com.csc.life.anticipatedendowment.dataaccess.dao;


import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Bd5hvTempDAO extends BaseDAO<Object>{
	public boolean deleteBd5hvTempAllRecords();
	public int insertBd5hvTempRecords(int batchExtractSize,String loancoy, int effDate);
	public List<Rgpdetpf> loadDataByNEWINTCP(int minRecord);
}
