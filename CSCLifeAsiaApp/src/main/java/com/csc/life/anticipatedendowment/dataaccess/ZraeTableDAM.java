package com.csc.life.anticipatedendowment.dataaccess;

import java.math.BigDecimal;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZraeTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:20
 * Class transformed from ZRAE.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZraeTableDAM extends ZraepfTableDAM {

	public ZraeTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZRAE");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "ZRDUEDTE01, " +
		            "ZRDUEDTE02, " +
		            "ZRDUEDTE03, " +
		            "ZRDUEDTE04, " +
		            "ZRDUEDTE05, " +
		            "ZRDUEDTE06, " +
		            "ZRDUEDTE07, " +
		            "ZRDUEDTE08, " +
		            "PRCNT01, " +
		            "PRCNT02, " +
		            "PRCNT03, " +
		            "PRCNT04, " +
		            "PRCNT05, " +
		            "PRCNT06, " +
		            "PRCNT07, " +
		            "PRCNT08, " +
		            "PAYDTE01, " +
		            "PAYDTE02, " +
		            "PAYDTE03, " +
		            "PAYDTE04, " +
		            "PAYDTE05, " +
		            "PAYDTE06, " +
		            "PAYDTE07, " +
		            "PAYDTE08, " +
		            "PAID01, " +
		            "PAID02, " +
		            "PAID03, " +
		            "PAID04, " +
		            "PAID05, " +
		            "PAID06, " +
		            "PAID07, " +
		            "PAID08, " +
		            "PAYMMETH01, " +
		            "PAYMMETH02, " +
		            "PAYMMETH03, " +
		            "PAYMMETH04, " +
		            "PAYMMETH05, " +
		            "PAYMMETH06, " +
		            "PAYMMETH07, " +
		            "PAYMMETH08, " +
		            "ZRPAYOPT01, " +
		            "ZRPAYOPT02, " +
		            "ZRPAYOPT03, " +
		            "ZRPAYOPT04, " +
		            "ZRPAYOPT05, " +
		            "ZRPAYOPT06, " +
		            "ZRPAYOPT07, " +
		            "ZRPAYOPT08, " +
		            "TOTAMNT, " +
		            "NPAYDATE, " +
		            "PAYCLT, " +
		            "FLAG, " +
		            "APCAPLAMT, " +
		            "APINTAMT, " +
		            "APLSTCAPDATE, " +
		            "APNXTCAPDATE, " +
		            "APLSTINTBDTE, " +
		            "APNXTINTBDTE, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "PAYCURR, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               validflag,
                               tranno,
                               currfrom,
                               currto,
                               zrduedte01,
                               zrduedte02,
                               zrduedte03,
                               zrduedte04,
                               zrduedte05,
                               zrduedte06,
                               zrduedte07,
                               zrduedte08,
                               prcnt01,
                               prcnt02,
                               prcnt03,
                               prcnt04,
                               prcnt05,
                               prcnt06,
                               prcnt07,
                               prcnt08,
                               paydte01,
                               paydte02,
                               paydte03,
                               paydte04,
                               paydte05,
                               paydte06,
                               paydte07,
                               paydte08,
                               paid01,
                               paid02,
                               paid03,
                               paid04,
                               paid05,
                               paid06,
                               paid07,
                               paid08,
                               paymentMethod01,
                               paymentMethod02,
                               paymentMethod03,
                               paymentMethod04,
                               paymentMethod05,
                               paymentMethod06,
                               paymentMethod07,
                               paymentMethod08,
                               payOption01,
                               payOption02,
                               payOption03,
                               payOption04,
                               payOption05,
                               payOption06,
                               payOption07,
                               payOption08,
                               totamnt,
                               nextPaydate,
                               payclt,
                               flag,
                               apcaplamt,
                               apintamt,
                               aplstcapdate,
                               apnxtcapdate,
                               aplstintbdte,
                               apnxtintbdte,
                               bankkey,
                               bankacckey,
                               paycurr,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(382);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getZrduedte01().toInternal()
					+ getZrduedte02().toInternal()
					+ getZrduedte03().toInternal()
					+ getZrduedte04().toInternal()
					+ getZrduedte05().toInternal()
					+ getZrduedte06().toInternal()
					+ getZrduedte07().toInternal()
					+ getZrduedte08().toInternal()
					+ getPrcnt01().toInternal()
					+ getPrcnt02().toInternal()
					+ getPrcnt03().toInternal()
					+ getPrcnt04().toInternal()
					+ getPrcnt05().toInternal()
					+ getPrcnt06().toInternal()
					+ getPrcnt07().toInternal()
					+ getPrcnt08().toInternal()
					+ getPaydte01().toInternal()
					+ getPaydte02().toInternal()
					+ getPaydte03().toInternal()
					+ getPaydte04().toInternal()
					+ getPaydte05().toInternal()
					+ getPaydte06().toInternal()
					+ getPaydte07().toInternal()
					+ getPaydte08().toInternal()
					+ getPaid01().toInternal()
					+ getPaid02().toInternal()
					+ getPaid03().toInternal()
					+ getPaid04().toInternal()
					+ getPaid05().toInternal()
					+ getPaid06().toInternal()
					+ getPaid07().toInternal()
					+ getPaid08().toInternal()
					+ getPaymentMethod01().toInternal()
					+ getPaymentMethod02().toInternal()
					+ getPaymentMethod03().toInternal()
					+ getPaymentMethod04().toInternal()
					+ getPaymentMethod05().toInternal()
					+ getPaymentMethod06().toInternal()
					+ getPaymentMethod07().toInternal()
					+ getPaymentMethod08().toInternal()
					+ getPayOption01().toInternal()
					+ getPayOption02().toInternal()
					+ getPayOption03().toInternal()
					+ getPayOption04().toInternal()
					+ getPayOption05().toInternal()
					+ getPayOption06().toInternal()
					+ getPayOption07().toInternal()
					+ getPayOption08().toInternal()
					+ getTotamnt().toInternal()
					+ getNextPaydate().toInternal()
					+ getPayclt().toInternal()
					+ getFlag().toInternal()
					+ getApcaplamt().toInternal()
					+ getApintamt().toInternal()
                    + getAplstcapdate().toInternal()
                    + getApnxtcapdate().toInternal()
                    + getAplstintbdte().toInternal()
                    + getApnxtintbdte().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getPaycurr().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, zrduedte01);
			what = ExternalData.chop(what, zrduedte02);
			what = ExternalData.chop(what, zrduedte03);
			what = ExternalData.chop(what, zrduedte04);
			what = ExternalData.chop(what, zrduedte05);
			what = ExternalData.chop(what, zrduedte06);
			what = ExternalData.chop(what, zrduedte07);
			what = ExternalData.chop(what, zrduedte08);
			what = ExternalData.chop(what, prcnt01);
			what = ExternalData.chop(what, prcnt02);
			what = ExternalData.chop(what, prcnt03);
			what = ExternalData.chop(what, prcnt04);
			what = ExternalData.chop(what, prcnt05);
			what = ExternalData.chop(what, prcnt06);
			what = ExternalData.chop(what, prcnt07);
			what = ExternalData.chop(what, prcnt08);
			what = ExternalData.chop(what, paydte01);
			what = ExternalData.chop(what, paydte02);
			what = ExternalData.chop(what, paydte03);
			what = ExternalData.chop(what, paydte04);
			what = ExternalData.chop(what, paydte05);
			what = ExternalData.chop(what, paydte06);
			what = ExternalData.chop(what, paydte07);
			what = ExternalData.chop(what, paydte08);
			what = ExternalData.chop(what, paid01);
			what = ExternalData.chop(what, paid02);
			what = ExternalData.chop(what, paid03);
			what = ExternalData.chop(what, paid04);
			what = ExternalData.chop(what, paid05);
			what = ExternalData.chop(what, paid06);
			what = ExternalData.chop(what, paid07);
			what = ExternalData.chop(what, paid08);
			what = ExternalData.chop(what, paymentMethod01);
			what = ExternalData.chop(what, paymentMethod02);
			what = ExternalData.chop(what, paymentMethod03);
			what = ExternalData.chop(what, paymentMethod04);
			what = ExternalData.chop(what, paymentMethod05);
			what = ExternalData.chop(what, paymentMethod06);
			what = ExternalData.chop(what, paymentMethod07);
			what = ExternalData.chop(what, paymentMethod08);
			what = ExternalData.chop(what, payOption01);
			what = ExternalData.chop(what, payOption02);
			what = ExternalData.chop(what, payOption03);
			what = ExternalData.chop(what, payOption04);
			what = ExternalData.chop(what, payOption05);
			what = ExternalData.chop(what, payOption06);
			what = ExternalData.chop(what, payOption07);
			what = ExternalData.chop(what, payOption08);
			what = ExternalData.chop(what, totamnt);
			what = ExternalData.chop(what, nextPaydate);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, flag);
			what = ExternalData.chop(what, apcaplamt);
			what = ExternalData.chop(what, apintamt);
			what = ExternalData.chop(what, aplstcapdate);
			what = ExternalData.chop(what, apnxtcapdate);
			what = ExternalData.chop(what, aplstintbdte);
			what = ExternalData.chop(what, apnxtintbdte);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, paycurr);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getZrduedte01() {
		return zrduedte01;
	}
	public void setZrduedte01(Object what) {
		setZrduedte01(what, false);
	}
	public void setZrduedte01(Object what, boolean rounded) {
		if (rounded)
			zrduedte01.setRounded(what);
		else
			zrduedte01.set(what);
	}	
	public PackedDecimalData getZrduedte02() {
		return zrduedte02;
	}
	public void setZrduedte02(Object what) {
		setZrduedte02(what, false);
	}
	public void setZrduedte02(Object what, boolean rounded) {
		if (rounded)
			zrduedte02.setRounded(what);
		else
			zrduedte02.set(what);
	}	
	public PackedDecimalData getZrduedte03() {
		return zrduedte03;
	}
	public void setZrduedte03(Object what) {
		setZrduedte03(what, false);
	}
	public void setZrduedte03(Object what, boolean rounded) {
		if (rounded)
			zrduedte03.setRounded(what);
		else
			zrduedte03.set(what);
	}	
	public PackedDecimalData getZrduedte04() {
		return zrduedte04;
	}
	public void setZrduedte04(Object what) {
		setZrduedte04(what, false);
	}
	public void setZrduedte04(Object what, boolean rounded) {
		if (rounded)
			zrduedte04.setRounded(what);
		else
			zrduedte04.set(what);
	}	
	public PackedDecimalData getZrduedte05() {
		return zrduedte05;
	}
	public void setZrduedte05(Object what) {
		setZrduedte05(what, false);
	}
	public void setZrduedte05(Object what, boolean rounded) {
		if (rounded)
			zrduedte05.setRounded(what);
		else
			zrduedte05.set(what);
	}	
	public PackedDecimalData getZrduedte06() {
		return zrduedte06;
	}
	public void setZrduedte06(Object what) {
		setZrduedte06(what, false);
	}
	public void setZrduedte06(Object what, boolean rounded) {
		if (rounded)
			zrduedte06.setRounded(what);
		else
			zrduedte06.set(what);
	}	
	public PackedDecimalData getZrduedte07() {
		return zrduedte07;
	}
	public void setZrduedte07(Object what) {
		setZrduedte07(what, false);
	}
	public void setZrduedte07(Object what, boolean rounded) {
		if (rounded)
			zrduedte07.setRounded(what);
		else
			zrduedte07.set(what);
	}	
	public PackedDecimalData getZrduedte08() {
		return zrduedte08;
	}
	public void setZrduedte08(Object what) {
		setZrduedte08(what, false);
	}
	public void setZrduedte08(Object what, boolean rounded) {
		if (rounded)
			zrduedte08.setRounded(what);
		else
			zrduedte08.set(what);
	}	
	public PackedDecimalData getPrcnt01() {
		return prcnt01;
	}
	public void setPrcnt01(Object what) {
		setPrcnt01(what, false);
	}
	public void setPrcnt01(Object what, boolean rounded) {
		if (rounded)
			prcnt01.setRounded(what);
		else
			prcnt01.set(what);
	}	
	public PackedDecimalData getPrcnt02() {
		return prcnt02;
	}
	public void setPrcnt02(Object what) {
		setPrcnt02(what, false);
	}
	public void setPrcnt02(Object what, boolean rounded) {
		if (rounded)
			prcnt02.setRounded(what);
		else
			prcnt02.set(what);
	}	
	public PackedDecimalData getPrcnt03() {
		return prcnt03;
	}
	public void setPrcnt03(Object what) {
		setPrcnt03(what, false);
	}
	public void setPrcnt03(Object what, boolean rounded) {
		if (rounded)
			prcnt03.setRounded(what);
		else
			prcnt03.set(what);
	}	
	public PackedDecimalData getPrcnt04() {
		return prcnt04;
	}
	public void setPrcnt04(Object what) {
		setPrcnt04(what, false);
	}
	public void setPrcnt04(Object what, boolean rounded) {
		if (rounded)
			prcnt04.setRounded(what);
		else
			prcnt04.set(what);
	}	
	public PackedDecimalData getPrcnt05() {
		return prcnt05;
	}
	public void setPrcnt05(Object what) {
		setPrcnt05(what, false);
	}
	public void setPrcnt05(Object what, boolean rounded) {
		if (rounded)
			prcnt05.setRounded(what);
		else
			prcnt05.set(what);
	}	
	public PackedDecimalData getPrcnt06() {
		return prcnt06;
	}
	public void setPrcnt06(Object what) {
		setPrcnt06(what, false);
	}
	public void setPrcnt06(Object what, boolean rounded) {
		if (rounded)
			prcnt06.setRounded(what);
		else
			prcnt06.set(what);
	}	
	public PackedDecimalData getPrcnt07() {
		return prcnt07;
	}
	public void setPrcnt07(Object what) {
		setPrcnt07(what, false);
	}
	public void setPrcnt07(Object what, boolean rounded) {
		if (rounded)
			prcnt07.setRounded(what);
		else
			prcnt07.set(what);
	}	
	public PackedDecimalData getPrcnt08() {
		return prcnt08;
	}
	public void setPrcnt08(Object what) {
		setPrcnt08(what, false);
	}
	public void setPrcnt08(Object what, boolean rounded) {
		if (rounded)
			prcnt08.setRounded(what);
		else
			prcnt08.set(what);
	}	
	public PackedDecimalData getPaydte01() {
		return paydte01;
	}
	public void setPaydte01(Object what) {
		setPaydte01(what, false);
	}
	public void setPaydte01(Object what, boolean rounded) {
		if (rounded)
			paydte01.setRounded(what);
		else
			paydte01.set(what);
	}	
	public PackedDecimalData getPaydte02() {
		return paydte02;
	}
	public void setPaydte02(Object what) {
		setPaydte02(what, false);
	}
	public void setPaydte02(Object what, boolean rounded) {
		if (rounded)
			paydte02.setRounded(what);
		else
			paydte02.set(what);
	}	
	public PackedDecimalData getPaydte03() {
		return paydte03;
	}
	public void setPaydte03(Object what) {
		setPaydte03(what, false);
	}
	public void setPaydte03(Object what, boolean rounded) {
		if (rounded)
			paydte03.setRounded(what);
		else
			paydte03.set(what);
	}	
	public PackedDecimalData getPaydte04() {
		return paydte04;
	}
	public void setPaydte04(Object what) {
		setPaydte04(what, false);
	}
	public void setPaydte04(Object what, boolean rounded) {
		if (rounded)
			paydte04.setRounded(what);
		else
			paydte04.set(what);
	}	
	public PackedDecimalData getPaydte05() {
		return paydte05;
	}
	public void setPaydte05(Object what) {
		setPaydte05(what, false);
	}
	public void setPaydte05(Object what, boolean rounded) {
		if (rounded)
			paydte05.setRounded(what);
		else
			paydte05.set(what);
	}	
	public PackedDecimalData getPaydte06() {
		return paydte06;
	}
	public void setPaydte06(Object what) {
		setPaydte06(what, false);
	}
	public void setPaydte06(Object what, boolean rounded) {
		if (rounded)
			paydte06.setRounded(what);
		else
			paydte06.set(what);
	}	
	public PackedDecimalData getPaydte07() {
		return paydte07;
	}
	public void setPaydte07(Object what) {
		setPaydte07(what, false);
	}
	public void setPaydte07(Object what, boolean rounded) {
		if (rounded)
			paydte07.setRounded(what);
		else
			paydte07.set(what);
	}	
	public PackedDecimalData getPaydte08() {
		return paydte08;
	}
	public void setPaydte08(Object what) {
		setPaydte08(what, false);
	}
	public void setPaydte08(Object what, boolean rounded) {
		if (rounded)
			paydte08.setRounded(what);
		else
			paydte08.set(what);
	}	
	public PackedDecimalData getPaid01() {
		return paid01;
	}
	public void setPaid01(Object what) {
		setPaid01(what, false);
	}
	public void setPaid01(Object what, boolean rounded) {
		if (rounded)
			paid01.setRounded(what);
		else
			paid01.set(what);
	}	
	public PackedDecimalData getPaid02() {
		return paid02;
	}
	public void setPaid02(Object what) {
		setPaid02(what, false);
	}
	public void setPaid02(Object what, boolean rounded) {
		if (rounded)
			paid02.setRounded(what);
		else
			paid02.set(what);
	}	
	public PackedDecimalData getPaid03() {
		return paid03;
	}
	public void setPaid03(Object what) {
		setPaid03(what, false);
	}
	public void setPaid03(Object what, boolean rounded) {
		if (rounded)
			paid03.setRounded(what);
		else
			paid03.set(what);
	}	
	public PackedDecimalData getPaid04() {
		return paid04;
	}
	public void setPaid04(Object what) {
		setPaid04(what, false);
	}
	public void setPaid04(Object what, boolean rounded) {
		if (rounded)
			paid04.setRounded(what);
		else
			paid04.set(what);
	}	
	public PackedDecimalData getPaid05() {
		return paid05;
	}
	public void setPaid05(Object what) {
		setPaid05(what, false);
	}
	public void setPaid05(Object what, boolean rounded) {
		if (rounded)
			paid05.setRounded(what);
		else
			paid05.set(what);
	}	
	public PackedDecimalData getPaid06() {
		return paid06;
	}
	public void setPaid06(Object what) {
		setPaid06(what, false);
	}
	public void setPaid06(Object what, boolean rounded) {
		if (rounded)
			paid06.setRounded(what);
		else
			paid06.set(what);
	}	
	public PackedDecimalData getPaid07() {
		return paid07;
	}
	public void setPaid07(Object what) {
		setPaid07(what, false);
	}
	public void setPaid07(Object what, boolean rounded) {
		if (rounded)
			paid07.setRounded(what);
		else
			paid07.set(what);
	}	
	public PackedDecimalData getPaid08() {
		return paid08;
	}
	public void setPaid08(Object what) {
		setPaid08(what, false);
	}
	public void setPaid08(Object what, boolean rounded) {
		if (rounded)
			paid08.setRounded(what);
		else
			paid08.set(what);
	}	
	public FixedLengthStringData getPaymentMethod01() {
		return paymentMethod01;
	}
	public void setPaymentMethod01(Object what) {
		paymentMethod01.set(what);
	}	
	public FixedLengthStringData getPaymentMethod02() {
		return paymentMethod02;
	}
	public void setPaymentMethod02(Object what) {
		paymentMethod02.set(what);
	}	
	public FixedLengthStringData getPaymentMethod03() {
		return paymentMethod03;
	}
	public void setPaymentMethod03(Object what) {
		paymentMethod03.set(what);
	}	
	public FixedLengthStringData getPaymentMethod04() {
		return paymentMethod04;
	}
	public void setPaymentMethod04(Object what) {
		paymentMethod04.set(what);
	}	
	public FixedLengthStringData getPaymentMethod05() {
		return paymentMethod05;
	}
	public void setPaymentMethod05(Object what) {
		paymentMethod05.set(what);
	}	
	public FixedLengthStringData getPaymentMethod06() {
		return paymentMethod06;
	}
	public void setPaymentMethod06(Object what) {
		paymentMethod06.set(what);
	}	
	public FixedLengthStringData getPaymentMethod07() {
		return paymentMethod07;
	}
	public void setPaymentMethod07(Object what) {
		paymentMethod07.set(what);
	}	
	public FixedLengthStringData getPaymentMethod08() {
		return paymentMethod08;
	}
	public void setPaymentMethod08(Object what) {
		paymentMethod08.set(what);
	}	
	public FixedLengthStringData getPayOption01() {
		return payOption01;
	}
	public void setPayOption01(Object what) {
		payOption01.set(what);
	}	
	public FixedLengthStringData getPayOption02() {
		return payOption02;
	}
	public void setPayOption02(Object what) {
		payOption02.set(what);
	}	
	public FixedLengthStringData getPayOption03() {
		return payOption03;
	}
	public void setPayOption03(Object what) {
		payOption03.set(what);
	}	
	public FixedLengthStringData getPayOption04() {
		return payOption04;
	}
	public void setPayOption04(Object what) {
		payOption04.set(what);
	}	
	public FixedLengthStringData getPayOption05() {
		return payOption05;
	}
	public void setPayOption05(Object what) {
		payOption05.set(what);
	}	
	public FixedLengthStringData getPayOption06() {
		return payOption06;
	}
	public void setPayOption06(Object what) {
		payOption06.set(what);
	}	
	public FixedLengthStringData getPayOption07() {
		return payOption07;
	}
	public void setPayOption07(Object what) {
		payOption07.set(what);
	}	
	public FixedLengthStringData getPayOption08() {
		return payOption08;
	}
	public void setPayOption08(Object what) {
		payOption08.set(what);
	}	
	public PackedDecimalData getTotamnt() {
		return totamnt;
	}
	public void setTotamnt(Object what) {
		setTotamnt(what, false);
	}
	public void setTotamnt(Object what, boolean rounded) {
		if (rounded)
			totamnt.setRounded(what);
		else
			totamnt.set(what);
	}	
	public PackedDecimalData getNextPaydate() {
		return nextPaydate;
	}
	public void setNextPaydate(Object what) {
		setNextPaydate(what, false);
	}
	public void setNextPaydate(Object what, boolean rounded) {
		if (rounded)
			nextPaydate.setRounded(what);
		else
			nextPaydate.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	
	public FixedLengthStringData getFlag() {
		return flag;
	}
	public void setFlag(Object what) {
		flag.set(what);
	}	
	
	public PackedDecimalData getApcaplamt() {
		return apcaplamt;
	}
	public void setApcaplamt(Object what) {
		setApcaplamt(what, false);
	}
	public void setApcaplamt(Object what, boolean rounded) {
		if (rounded)
			apcaplamt.setRounded(what);
		else
			apcaplamt.set(what);
	}
	
	public PackedDecimalData getApintamt() {
		return apintamt;
	}
	public void setApintamt(Object what) {
		setApintamt(what, false);
	}
	public void setApintamt(Object what, boolean rounded) {
		if (rounded)
			apintamt.setRounded(what);
		else
			apintamt.set(what);
	}
	
	public PackedDecimalData getAplstcapdate() {
		return aplstcapdate;
	}
	public void setAplstcapdate(Object what) {
		setAplstcapdate(what, false);
	}
	public void setAplstcapdate(Object what, boolean rounded) {
		if (rounded)
			aplstcapdate.setRounded(what);
		else
			aplstcapdate.set(what);
	}
	
	public PackedDecimalData getApnxtcapdate() {
		return apnxtcapdate;
	}
	public void setApnxtcapdate(Object what) {
		setApnxtcapdate(what, false);
	}
	public void setApnxtcapdate(Object what, boolean rounded) {
		if (rounded)
			apnxtcapdate.setRounded(what);
		else
			apnxtcapdate.set(what);
	}
	
	public PackedDecimalData getAplstintbdte() {
		return aplstintbdte;
	}
	public void setAplstintbdte(Object what) {
		setAplstintbdte(what, false);
	}
	public void setAplstintbdte(Object what, boolean rounded) {
		if (rounded)
			aplstintbdte.setRounded(what);
		else
			aplstintbdte.set(what);
	}
	
	public PackedDecimalData getApnxtintbdte() {
		return apnxtintbdte;
	}
	public void setApnxtintbdte(Object what) {
		setApnxtintbdte(what, false);
	}
	public void setApnxtintbdte(Object what, boolean rounded) {
		if (rounded)
			apnxtintbdte.setRounded(what);
		else
			apnxtintbdte.set(what);
	}
	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getPaycurr() {
		return paycurr;
	}
	public void setPaycurr(Object what) {
		paycurr.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getZrpayopts() {
		return new FixedLengthStringData(payOption01.toInternal()
										+ payOption02.toInternal()
										+ payOption03.toInternal()
										+ payOption04.toInternal()
										+ payOption05.toInternal()
										+ payOption06.toInternal()
										+ payOption07.toInternal()
										+ payOption08.toInternal());
	}
	public void setZrpayopts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getZrpayopts().getLength()).init(obj);
	
		what = ExternalData.chop(what, payOption01);
		what = ExternalData.chop(what, payOption02);
		what = ExternalData.chop(what, payOption03);
		what = ExternalData.chop(what, payOption04);
		what = ExternalData.chop(what, payOption05);
		what = ExternalData.chop(what, payOption06);
		what = ExternalData.chop(what, payOption07);
		what = ExternalData.chop(what, payOption08);
	}
	public FixedLengthStringData getZrpayopt(BaseData indx) {
		return getZrpayopt(indx.toInt());
	}
	public FixedLengthStringData getZrpayopt(int indx) {

		switch (indx) {
			case 1 : return payOption01;
			case 2 : return payOption02;
			case 3 : return payOption03;
			case 4 : return payOption04;
			case 5 : return payOption05;
			case 6 : return payOption06;
			case 7 : return payOption07;
			case 8 : return payOption08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setZrpayopt(BaseData indx, Object what) {
		setZrpayopt(indx.toInt(), what);
	}
	public void setZrpayopt(int indx, Object what) {

		switch (indx) {
			case 1 : setPayOption01(what);
					 break;
			case 2 : setPayOption02(what);
					 break;
			case 3 : setPayOption03(what);
					 break;
			case 4 : setPayOption04(what);
					 break;
			case 5 : setPayOption05(what);
					 break;
			case 6 : setPayOption06(what);
					 break;
			case 7 : setPayOption07(what);
					 break;
			case 8 : setPayOption08(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getZrduedtes() {
		return new FixedLengthStringData(zrduedte01.toInternal()
										+ zrduedte02.toInternal()
										+ zrduedte03.toInternal()
										+ zrduedte04.toInternal()
										+ zrduedte05.toInternal()
										+ zrduedte06.toInternal()
										+ zrduedte07.toInternal()
										+ zrduedte08.toInternal());
	}
	public void setZrduedtes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getZrduedtes().getLength()).init(obj);
	
		what = ExternalData.chop(what, zrduedte01);
		what = ExternalData.chop(what, zrduedte02);
		what = ExternalData.chop(what, zrduedte03);
		what = ExternalData.chop(what, zrduedte04);
		what = ExternalData.chop(what, zrduedte05);
		what = ExternalData.chop(what, zrduedte06);
		what = ExternalData.chop(what, zrduedte07);
		what = ExternalData.chop(what, zrduedte08);
	}
	public PackedDecimalData getZrduedte(BaseData indx) {
		return getZrduedte(indx.toInt());
	}
	public PackedDecimalData getZrduedte(int indx) {

		switch (indx) {
			case 1 : return zrduedte01;
			case 2 : return zrduedte02;
			case 3 : return zrduedte03;
			case 4 : return zrduedte04;
			case 5 : return zrduedte05;
			case 6 : return zrduedte06;
			case 7 : return zrduedte07;
			case 8 : return zrduedte08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setZrduedte(BaseData indx, Object what) {
		setZrduedte(indx, what, false);
	}
	public void setZrduedte(BaseData indx, Object what, boolean rounded) {
		setZrduedte(indx.toInt(), what, rounded);
	}
	public void setZrduedte(int indx, Object what) {
		setZrduedte(indx, what, false);
	}
	public void setZrduedte(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setZrduedte01(what, rounded);
					 break;
			case 2 : setZrduedte02(what, rounded);
					 break;
			case 3 : setZrduedte03(what, rounded);
					 break;
			case 4 : setZrduedte04(what, rounded);
					 break;
			case 5 : setZrduedte05(what, rounded);
					 break;
			case 6 : setZrduedte06(what, rounded);
					 break;
			case 7 : setZrduedte07(what, rounded);
					 break;
			case 8 : setZrduedte08(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPrcnts() {
		return new FixedLengthStringData(prcnt01.toInternal()
										+ prcnt02.toInternal()
										+ prcnt03.toInternal()
										+ prcnt04.toInternal()
										+ prcnt05.toInternal()
										+ prcnt06.toInternal()
										+ prcnt07.toInternal()
										+ prcnt08.toInternal());
	}
	public void setPrcnts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPrcnts().getLength()).init(obj);
	
		what = ExternalData.chop(what, prcnt01);
		what = ExternalData.chop(what, prcnt02);
		what = ExternalData.chop(what, prcnt03);
		what = ExternalData.chop(what, prcnt04);
		what = ExternalData.chop(what, prcnt05);
		what = ExternalData.chop(what, prcnt06);
		what = ExternalData.chop(what, prcnt07);
		what = ExternalData.chop(what, prcnt08);
	}
	public PackedDecimalData getPrcnt(BaseData indx) {
		return getPrcnt(indx.toInt());
	}
	public PackedDecimalData getPrcnt(int indx) {

		switch (indx) {
			case 1 : return prcnt01;
			case 2 : return prcnt02;
			case 3 : return prcnt03;
			case 4 : return prcnt04;
			case 5 : return prcnt05;
			case 6 : return prcnt06;
			case 7 : return prcnt07;
			case 8 : return prcnt08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPrcnt(BaseData indx, Object what) {
		setPrcnt(indx, what, false);
	}
	public void setPrcnt(BaseData indx, Object what, boolean rounded) {
		setPrcnt(indx.toInt(), what, rounded);
	}
	public void setPrcnt(int indx, Object what) {
		setPrcnt(indx, what, false);
	}
	public void setPrcnt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setPrcnt01(what, rounded);
					 break;
			case 2 : setPrcnt02(what, rounded);
					 break;
			case 3 : setPrcnt03(what, rounded);
					 break;
			case 4 : setPrcnt04(what, rounded);
					 break;
			case 5 : setPrcnt05(what, rounded);
					 break;
			case 6 : setPrcnt06(what, rounded);
					 break;
			case 7 : setPrcnt07(what, rounded);
					 break;
			case 8 : setPrcnt08(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPaymmeths() {
		return new FixedLengthStringData(paymentMethod01.toInternal()
										+ paymentMethod02.toInternal()
										+ paymentMethod03.toInternal()
										+ paymentMethod04.toInternal()
										+ paymentMethod05.toInternal()
										+ paymentMethod06.toInternal()
										+ paymentMethod07.toInternal()
										+ paymentMethod08.toInternal());
	}
	public void setPaymmeths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPaymmeths().getLength()).init(obj);
	
		what = ExternalData.chop(what, paymentMethod01);
		what = ExternalData.chop(what, paymentMethod02);
		what = ExternalData.chop(what, paymentMethod03);
		what = ExternalData.chop(what, paymentMethod04);
		what = ExternalData.chop(what, paymentMethod05);
		what = ExternalData.chop(what, paymentMethod06);
		what = ExternalData.chop(what, paymentMethod07);
		what = ExternalData.chop(what, paymentMethod08);
	}
	public FixedLengthStringData getPaymmeth(BaseData indx) {
		return getPaymmeth(indx.toInt());
	}
	public FixedLengthStringData getPaymmeth(int indx) {

		switch (indx) {
			case 1 : return paymentMethod01;
			case 2 : return paymentMethod02;
			case 3 : return paymentMethod03;
			case 4 : return paymentMethod04;
			case 5 : return paymentMethod05;
			case 6 : return paymentMethod06;
			case 7 : return paymentMethod07;
			case 8 : return paymentMethod08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPaymmeth(BaseData indx, Object what) {
		setPaymmeth(indx.toInt(), what);
	}
	public void setPaymmeth(int indx, Object what) {

		switch (indx) {
			case 1 : setPaymentMethod01(what);
					 break;
			case 2 : setPaymentMethod02(what);
					 break;
			case 3 : setPaymentMethod03(what);
					 break;
			case 4 : setPaymentMethod04(what);
					 break;
			case 5 : setPaymentMethod05(what);
					 break;
			case 6 : setPaymentMethod06(what);
					 break;
			case 7 : setPaymentMethod07(what);
					 break;
			case 8 : setPaymentMethod08(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPaydtes() {
		return new FixedLengthStringData(paydte01.toInternal()
										+ paydte02.toInternal()
										+ paydte03.toInternal()
										+ paydte04.toInternal()
										+ paydte05.toInternal()
										+ paydte06.toInternal()
										+ paydte07.toInternal()
										+ paydte08.toInternal());
	}
	public void setPaydtes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPaydtes().getLength()).init(obj);
	
		what = ExternalData.chop(what, paydte01);
		what = ExternalData.chop(what, paydte02);
		what = ExternalData.chop(what, paydte03);
		what = ExternalData.chop(what, paydte04);
		what = ExternalData.chop(what, paydte05);
		what = ExternalData.chop(what, paydte06);
		what = ExternalData.chop(what, paydte07);
		what = ExternalData.chop(what, paydte08);
	}
	public PackedDecimalData getPaydte(BaseData indx) {
		return getPaydte(indx.toInt());
	}
	public PackedDecimalData getPaydte(int indx) {

		switch (indx) {
			case 1 : return paydte01;
			case 2 : return paydte02;
			case 3 : return paydte03;
			case 4 : return paydte04;
			case 5 : return paydte05;
			case 6 : return paydte06;
			case 7 : return paydte07;
			case 8 : return paydte08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPaydte(BaseData indx, Object what) {
		setPaydte(indx, what, false);
	}
	public void setPaydte(BaseData indx, Object what, boolean rounded) {
		setPaydte(indx.toInt(), what, rounded);
	}
	public void setPaydte(int indx, Object what) {
		setPaydte(indx, what, false);
	}
	public void setPaydte(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setPaydte01(what, rounded);
					 break;
			case 2 : setPaydte02(what, rounded);
					 break;
			case 3 : setPaydte03(what, rounded);
					 break;
			case 4 : setPaydte04(what, rounded);
					 break;
			case 5 : setPaydte05(what, rounded);
					 break;
			case 6 : setPaydte06(what, rounded);
					 break;
			case 7 : setPaydte07(what, rounded);
					 break;
			case 8 : setPaydte08(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getPaids() {
		return new FixedLengthStringData(paid01.toInternal()
										+ paid02.toInternal()
										+ paid03.toInternal()
										+ paid04.toInternal()
										+ paid05.toInternal()
										+ paid06.toInternal()
										+ paid07.toInternal()
										+ paid08.toInternal());
	}
	public void setPaids(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPaids().getLength()).init(obj);
	
		what = ExternalData.chop(what, paid01);
		what = ExternalData.chop(what, paid02);
		what = ExternalData.chop(what, paid03);
		what = ExternalData.chop(what, paid04);
		what = ExternalData.chop(what, paid05);
		what = ExternalData.chop(what, paid06);
		what = ExternalData.chop(what, paid07);
		what = ExternalData.chop(what, paid08);
	}
	public PackedDecimalData getPaid(BaseData indx) {
		return getPaid(indx.toInt());
	}
	public PackedDecimalData getPaid(int indx) {

		switch (indx) {
			case 1 : return paid01;
			case 2 : return paid02;
			case 3 : return paid03;
			case 4 : return paid04;
			case 5 : return paid05;
			case 6 : return paid06;
			case 7 : return paid07;
			case 8 : return paid08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPaid(BaseData indx, Object what) {
		setPaid(indx, what, false);
	}
	public void setPaid(BaseData indx, Object what, boolean rounded) {
		setPaid(indx.toInt(), what, rounded);
	}
	public void setPaid(int indx, Object what) {
		setPaid(indx, what, false);
	}
	public void setPaid(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setPaid01(what, rounded);
					 break;
			case 2 : setPaid02(what, rounded);
					 break;
			case 3 : setPaid03(what, rounded);
					 break;
			case 4 : setPaid04(what, rounded);
					 break;
			case 5 : setPaid05(what, rounded);
					 break;
			case 6 : setPaid06(what, rounded);
					 break;
			case 7 : setPaid07(what, rounded);
					 break;
			case 8 : setPaid08(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		validflag.clear();
		tranno.clear();
		currfrom.clear();
		currto.clear();
		zrduedte01.clear();
		zrduedte02.clear();
		zrduedte03.clear();
		zrduedte04.clear();
		zrduedte05.clear();
		zrduedte06.clear();
		zrduedte07.clear();
		zrduedte08.clear();
		prcnt01.clear();
		prcnt02.clear();
		prcnt03.clear();
		prcnt04.clear();
		prcnt05.clear();
		prcnt06.clear();
		prcnt07.clear();
		prcnt08.clear();
		paydte01.clear();
		paydte02.clear();
		paydte03.clear();
		paydte04.clear();
		paydte05.clear();
		paydte06.clear();
		paydte07.clear();
		paydte08.clear();
		paid01.clear();
		paid02.clear();
		paid03.clear();
		paid04.clear();
		paid05.clear();
		paid06.clear();
		paid07.clear();
		paid08.clear();
		paymentMethod01.clear();
		paymentMethod02.clear();
		paymentMethod03.clear();
		paymentMethod04.clear();
		paymentMethod05.clear();
		paymentMethod06.clear();
		paymentMethod07.clear();
		paymentMethod08.clear();
		payOption01.clear();
		payOption02.clear();
		payOption03.clear();
		payOption04.clear();
		payOption05.clear();
		payOption06.clear();
		payOption07.clear();
		payOption08.clear();
		totamnt.clear();
		nextPaydate.clear();
		payclt.clear();
		bankkey.clear();
		bankacckey.clear();
		paycurr.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}