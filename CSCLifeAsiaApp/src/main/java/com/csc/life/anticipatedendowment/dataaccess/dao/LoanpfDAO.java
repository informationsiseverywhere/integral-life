/*
 * File: LoanpfDAO.java
 * Date: March 11, 2016
 * Author: CSC
 * Created by: dpuhawan
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.dataaccess.dao;

import java.util.List;

import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LoanpfDAO  extends BaseDAO<Loanpf> {
	
	//ILPI-229 START by dpuhawan
	/* LoanPF links to the ff tables:
	 * CHDRPF
	 * CLRRPF
	 * PAYRPF
	 * MANDPF
	 * CLBAPF
	 */
	/* Retrieve all valid LOANPF records based on specific params*/
	public void rewrtLoanRecords(List<Loanpf> loanList, int effDate);
	public void insertLoanRecords(List<Loanpf> loanList);
	public boolean updateLoanRecords(List<Loanpf> loanList);
	//ILPI-229 END
	//Add it for batch by xma3
    public List<Loanpf> loadDataByLoanNum(String chdrcoy, String chdrnum, String loanNumber);
    public boolean updateLoanValRecords(List<Loanpf> loanList);
    public boolean updateLoanCapRecords(List<Loanpf> loanList);
    public boolean updateLoanRecordsUniqueNo(List<Loanpf> loanList);
    public long getLoanRecord(String company,String chdrnum);  //ICIL-179
    public boolean updateLoanValidflag(Loanpf loanpf);
    public boolean deleteLoanpfRecord(Loanpf loanpf);
    public boolean updateLoanpftranno(Loanpf loanpf);
    public List<Loanpf> loadValidflagRecord(String chdrcoy, String chdrnum, String loanNumber, String validflag);
    public void insertLoanRec(List<Loanpf> loanList);
    //ILB-446 by wli31
    public Loanpf getLoanpfRecord(String company, String chdrnum,int loannumber);   
}
