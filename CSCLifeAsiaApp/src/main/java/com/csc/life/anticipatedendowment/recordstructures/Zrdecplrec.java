package com.csc.life.anticipatedendowment.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:21
 * Description:
 * Copybook name: ZRDECPLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrdecplrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData zrdecplRec = new FixedLengthStringData(52);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(zrdecplRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(zrdecplRec, 1);
  	public ZonedDecimalData amountIn = new ZonedDecimalData(18, 3).isAPartOf(zrdecplRec, 5);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(zrdecplRec, 23);
  	public ZonedDecimalData amountOut = new ZonedDecimalData(18, 3).isAPartOf(zrdecplRec, 26);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(zrdecplRec, 44);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(zrdecplRec, 48);


	public void initialize() {
		COBOLFunctions.initialize(zrdecplRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrdecplRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}