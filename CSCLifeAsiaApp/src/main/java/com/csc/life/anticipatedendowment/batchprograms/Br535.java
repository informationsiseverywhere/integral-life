/*
 * File: Br535.java
 * Date: 29 August 2009 22:16:02
 * Author: Quipoz Limited
 *
 * Class transformed from BR535.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.dataaccess.dao.Br535TempDAO;

import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Br535DTO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.procedures.Nfloan;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.contractservicing.tablestructures.T6633rec;

import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER; //ILIFE-8840


/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   New Interest Capitalization program.
*
*
*   Control totals:
*     01  -  Number of LOAN records processed
*
*
*****************************************************************
* </pre>
*/
public class Br535 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR535");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	protected ZonedDecimalData wsaaSqlEffdate = new ZonedDecimalData(8, 0);
	protected FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);
	protected PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaFacthous = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBankacckey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	protected PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaPostedAmount = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotalLoanDebt = new PackedDecimalData(17, 2);
	protected FixedLengthStringData wsaaReadPayr = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaChdrnumOld = new FixedLengthStringData(8);
	protected ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).setUnsigned();
	protected PackedDecimalData wsaaBaseIdx = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	private final String wsaaSign = "-";
		/*                                                         <V4L001>
		  Storage for T5645 table items.                         <V4L001>
		                                                         <V4L001>*/
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5645Sub = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);

		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	protected ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	protected static final int wsaaUser = 0;

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();
	private PackedDecimalData wsaanofDay = new PackedDecimalData(3, 0); //ICIL-677

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
		/* ERRORS */
	private static final String e723 = "E723";
	private static final String h791 = "H791";
	private static final String t5645 = "T5645";
	private static final String t3695 = "T3695";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);


	private DescTableDAM descIO = new DescTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Intcalcrec intcalcrec = new Intcalcrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	protected T3629rec t3629rec = new T3629rec();
	private T5645rec t5645rec = new T5645rec();
	protected T6633rec t6633rec = new T6633rec();
	private T3695rec t3695rec = new T3695rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	
	private FixedLengthStringData wsaaMandstat = new FixedLengthStringData(2);
	
	//ILIFE-2505
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	
	private LoanpfDAO loanpfDAO =  getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private BextpfDAO bextpfDAO =  getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private AcblpfDAO acblpfDAO =  getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	protected PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);	
	protected Br535TempDAO br535TempDAO = getApplicationContext().getBean("br535TempDAO", Br535TempDAO.class);	
	
	private Map<String, List<Itempf>> t6633ListMap = new HashMap<String, List<Itempf>>();	
	private Map<String, List<Itempf>> t3629ListMap = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> t3695ListMap = new HashMap<String, List<Itempf>>();
	
	protected Br535DTO br535dto = new Br535DTO();
	protected List<Loanpf> loanpfList = new ArrayList<Loanpf>();
	private List<Chdrpf> chdrBulkUpdtList = new ArrayList<Chdrpf>();	
	protected List<Loanpf> loanBulkUpdtList = new ArrayList<Loanpf>();
	protected List<Bextpf> bextBulkInsList = new ArrayList<Bextpf>();
	protected List<Loanpf> loanCapnBulkUpdtList  = new ArrayList<Loanpf>();
	private List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
	protected List<Ptrnpf> ptrnBulkInsList = new ArrayList<Ptrnpf>();
	
	protected Loanpf loanpf = new Loanpf(); 
	private Acblpf acblpf;
	protected Ptrnpf ptrnpf;
	protected Iterator<Loanpf> iterator;
	private Iterator<Acmvpf> acmvpfIter;
	private Chdrpf chdrpf;	
	protected Bextpf bextpf;
	
	protected String strSacscode;
	protected String strSacsType;
	protected String strglMap;
	protected String strglSign;
	
	
	protected int intBatchExtractSize;
	protected int ctrCT01; 
	protected int ctrCT02;
	protected int wsaaIntFromDate;
	boolean cslnd004Permission = false;  //ICIL-677
	
	private static final String hl63 = "HL63";	
	private static final String h420 = "H420";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br535.class);
 //ILIFE-8840 start	
	private FixedLengthStringData wsaalonxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaalonxFn, 0, FILLER).init("LONX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaalonxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaalonxFn, 6).setUnsigned();
	
    private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
    private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
    private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
    
	private int batchID = 0;
	private List<Acblpf> acblpfInsertAll = new ArrayList<Acblpf>();
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private Datcon2Pojo datcon2Pojo = null;
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	
 //ILIFE-8840 end
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		exit2640,
		datcon2Loop2775,
		exit2779,
		readT36292945,
		readNextAcmv3125,
		exit3129,
		datcon4Loop3225,
		exit3229
	}

	public Br535() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
	wsspEdterror.set(varcom.oK);
	cslnd004Permission = FeaConfg.isFeatureExist("2", "CSLND004", appVars, "IT");	//ICIL-677
	wsaaParmCompany.set(bsprIO.getCompany());
	wsaaSqlEffdate.set(bsscIO.getEffectiveDate());
	wsaaCnttype.set(SPACES);
	wsaaFacthous.set(SPACES);
	wsaaBankkey.set(SPACES);
	wsaaBankacckey.set(SPACES);
	wsaaTranno.set(ZERO);
	wsaaSequenceNo.set(ZERO);
	wsaaChdrnumOld.set(SPACES);
	wsaaMandstat.set(SPACES);
	ctrCT01 = 0;
	ctrCT02 = 0;
	bupaIO.setDataArea(lsaaBuparec);
	String coy = bsprIO.getCompany().toString();
    readT5645(coy);
    t6633ListMap = itemDAO.loadSmartTable("IT", coy, "T6633");	
    t3629ListMap = itemDAO.loadSmartTable("IT", coy, "T3629");
    t3695ListMap = itemDAO.loadSmartTable("IT", coy, "T3695");
   // getItemDescription();
    wsaaTransactionDate.set(getCobolDate());
	wsaaTransactionTime.set(varcom.vrcmTime);
    //For reading the loanpf
 	//ILIFE-8840 start
	wsaaRunid.set(bprdIO.getSystemParam04());
	wsaaJobno.set(bsscIO.getScheduleNumber());
	wsaaThreadNumber.set(bsprIO.getProcessOccNum());
	//ILIFE-8840 end
    if (bprdIO.systemParam01.isNumeric()) {
        if (bprdIO.systemParam01.toInt() > 0) {
        	intBatchExtractSize = bprdIO.systemParam01.toInt();
        } else {
        	intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
        }
    } else {
    	intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
    }  
    
    performDataChunk();
	
	}

protected void readT5645(String company){
	/* Read the accounting rules table*/

	
	List<Itempf> itempfList = itemDAO.getAllItemitem("IT",company,t5645,wsaaProg.toString().trim());
	boolean itemFound = false;
	if (null != itempfList && !itempfList.isEmpty()) { 
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));	

		for (wsaaT5645Sub.set(1); !(isGT(wsaaT5645Sub,15)
				|| isGTE(wsaaT5645Offset,wsaaT5645Size)); wsaaT5645Sub.add(1)){
			wsaaT5645Offset.add(1);
			wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
		}

		if (isGTE(wsaaT5645Offset,wsaaT5645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5645rec.t5645Rec);
			fatalError600();
		}

		itemFound = true;		
	}		
	if (!itemFound) {
		syserrrec.statuz.set(h791);
		syserrrec.params.set(t5645);
		fatalError600();
	}	
}

protected void getItemDescription(){
	/* Get item description.*/
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(wsaaParmCompany);
	descIO.setDesctabl(t5645);
	descIO.setDescitem(wsaaProg);
	descIO.setLanguage(bsscIO.getLanguage());
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(descIO.getParams());
		syserrrec.statuz.set(descIO.getStatuz());
		fatalError600();
	}
	wsaaTrandesc.set(descIO.getLongdesc());
}


protected void performDataChunk(){

	
	if (loanpfList.size() > 0) 
		loanpfList.clear();
	loanpfList = br535TempDAO.loadDataByNEWINTCP(wsaalonxFn.toString(),wsaaThreadMember.toString(),intBatchExtractSize, batchID);
	

	if(loanpfList != null && loanpfList.size()>0){
    	iterator = loanpfList.iterator();    	   	
    }else{
    	wsspEdterror.set(varcom.endp);
    }
}
	



protected void readFile2000()
	{
	
	
	 //ILIFE-8840 start	
	 if(iterator.hasNext()){
	    	loanpf = iterator.next();
	    	br535dto = loanpf.getBr535DTO();
		} else {
			batchID++;
			loanpfList = br535TempDAO.loadDataByNEWINTCP(wsaalonxFn.toString(),wsaaThreadMember.toString(),intBatchExtractSize, batchID);
	        if (loanpfList != null && loanpfList.size() > 0) {
	        	iterator = loanpfList.iterator(); 
	        } else {
	            wsspEdterror.set(varcom.endp);
	            return;
	        }
		}
	 //ILIFE-8840 end
	
		
	}


protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaChdrnumOld,SPACES)) {
			removeSftlock2200();
		}
	}

protected void removeSftlock2200()
	{		
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(wsaaParmCompany);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(wsaaChdrnumOld);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}


protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT01 = 0;
	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT02 = 0;
	
}

protected void edit2500()
	{
		/* If Contract number has changed, we need to unlock record*/
		/* previously locked.*/
		wsspEdterror.set(varcom.oK);
		if (isNE(wsaaChdrnumOld, SPACES) && isGT(wsaaCommitCnt, ZERO)){ //ILIFE-2505 //MIBT-119
			removeSftlock2200();
		}
		/* Update control total - we have a LOAN record to process
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();*/
		
		
		/* Loan Type.*/
		/* The Loan Types are 'P for Policy Loan, 'A' for APL and 'E'*/
		/* for Cash Deposit.*/
		/*    Add 4th 4 - Advance Premium Deposit (APA). Set the index to  */
		/*    12 for APA. Loan type for APA is 'D'.                <V4L001>*/
		/*                                                         <V4L001>*/
		if (isEQ(loanpf.getLoantype(), "P")){
			wsaaBaseIdx.set(0);
		}
		else if (isEQ(loanpf.getLoantype(), "A")){
			wsaaBaseIdx.set(4);
		}
		else if (isEQ(loanpf.getLoantype(), "E")){
			wsaaBaseIdx.set(8);
		}
		else if (isEQ(loanpf.getLoantype(), "D")){
			wsaaBaseIdx.set(12);
			/***** WHEN OTHER*/
			/*****     MOVE BOMB               TO SYSR-STATUZ*/
			/*****     PERFORM 600-FATAL-ERROR*/
		}
		/* Process Loan record read - calculate Interest since last*/
		/* interest billing date and post ACMVs accordingly.*/
		wsaaCurbal.set(ZERO);
		wsaaPostedAmount.set(ZERO);
		wsaaTotalLoanDebt.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		wsaaReadPayr.set(SPACES);
		softlock2600();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			//contotrec.totno.set(ct02);
			//contotrec.totval.set(1);
			//callContot001();
			ctrCT02++;
			wsspEdterror.set(SPACES);
			wsaaChdrnumOld.set(SPACES);
			return ;
		}
		updateContractHeader2600();
		/* Check whether we need to calculate any Interest due*/
		/* - user may not have run Billing interest program first*/
		if (isGTE(loanpf.getLstintbdte(), wsaaSqlEffdate)) {
			return ;
		}
		calculateInterest2700();
		updateLoanRecord2750();
		
		ctrCT01++;
		postInterestAcmvs2800();
		writeBextRecord2900();
	}

protected void softlock2600()
	{		
		/* Soft lock the contract, if it is to be processed.               */
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsaaParmCompany);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(loanpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void updateContractHeader2600()
	{
		try {
			updateChdrRecord3010();
			//saveFields2620();
			readT6633();
		}
		catch (GOTOException e){
		}
	}



protected void updateChdrRecord3010(){
	
	/* If Contract has already been updated because there is more*/
	/* more one loan against the contract, do not increment the*/
	/* TRANNO.*/
	if (isEQ(wsaaChdrnumOld, loanpf.getChdrnum())) {
		goTo(GotoLabel.exit2640);
	}
	
	chdrpf = new Chdrpf();
	chdrpf.setChdrcoy(loanpf.getChdrcoy().charAt(0));
	chdrpf.setChdrnum(loanpf.getChdrnum());
	chdrpf.setTranno(br535dto.getTranno() + 1);
	chdrBulkUpdtList.add(chdrpf);	
	//chdrBulkUpdtList.put(Long.toString(loanpf.getUniqueNumber()), loanpf);		
}

protected void readT6633()
	{

	wsaaTranno.set(br535dto.getTranno()+1);
	wsaaCnttype.set(br535dto.getCnttype());
	wsaaChdrnumOld.set(loanpf.getChdrnum());
	
		boolean itemFound = false;
		String keyItemitem = br535dto.getCnttype().concat(loanpf.getLoantype());
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t6633ListMap.containsKey(keyItemitem)){	
			itempfList = t6633ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();			
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if(Integer.parseInt(wsaaSqlEffdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) ){					
						t6633rec.t6633Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;
					}
				}else{
					t6633rec.t6633Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}	
			}
		}
		if (!itemFound) {
			syserrrec.params.set(t6633rec.t6633Rec);
			syserrrec.statuz.set(e723);
			fatalError600();		
		}	
		//ICIL-677 start
		if ((cslnd004Permission)) {
			wsaanofDay.set(t6633rec.nofDay); 
		}
		//ICIL-677 end
	}


protected void calculateInterest2700()
	{
		
		/* Set up INTCALC linkage and call...*/
		/* Don't forget that the value returned is in LOAN currency.*/
		intcalcrec.intcalcRec.set(SPACES);
		intcalcrec.loanNumber.set(loanpf.getLoannumber());
		intcalcrec.chdrcoy.set(loanpf.getChdrcoy());
		intcalcrec.chdrnum.set(loanpf.getChdrnum());
		intcalcrec.cnttype.set(wsaaCnttype);
		intcalcrec.interestTo.set(wsaaSqlEffdate);
		intcalcrec.interestFrom.set(loanpf.getLstintbdte());
		wsaaIntFromDate=loanpf.getLstintbdte();
		intcalcrec.loanorigam.set(loanpf.getLstcaplamt());
		intcalcrec.lastCaplsnDate.set(loanpf.getLstcapdate());
		intcalcrec.loanStartDate.set(loanpf.getLoansdate());
		intcalcrec.interestAmount.set(ZERO);
		intcalcrec.loanCurrency.set(loanpf.getLoancurr());
		intcalcrec.loanType.set(loanpf.getLoantype());
		callProgram(Intcalc.class, intcalcrec.intcalcRec);
		if (isNE(intcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(intcalcrec.intcalcRec);
			syserrrec.statuz.set(intcalcrec.statuz);
			fatalError600();
		}
		/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
		if (isNE(intcalcrec.interestAmount, 0)) {
			zrdecplcPojo.setAmountIn(intcalcrec.interestAmount.getbigdata());
	     	zrdecplcPojo.setCurrency(loanpf.getLoancurr());
	 		zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
	 		zrdecplcPojo.setBatctrcde(batcdorrec.trcde.toString());
	 		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
	 		if (!varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
	 			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
				fatalError600();
	 		}
	 		intcalcrec.interestAmount.set(zrdecplcPojo.getAmountOut());
		}
	}

protected void updateLoanRecord2750()
	{
		/* Update LOAN record - reset next & last interest billing*/
		/* dates.*/
		loanpf.setValidflag("1");
		loanpf.setLstintbdte(wsaaSqlEffdate.toInt());
		nextIntBillDate2770();
		loanBulkUpdtList.add(loanpf);

	}

protected void nextIntBillDate2770()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2770();
				case datcon2Loop2775:
					datcon2Loop2775();
				case exit2779:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2770()
	{
		/* Check the interest  details on T6633 in the following*/
		/* order: i) Calculate interest on Loan anniv ... Y/N*/
		/*       ii) Calculate interest on Policy anniv.. Y/N*/
		/*      iii) Check Int freq & whether a specific Day is chosen*/
		wsaaLoanDate.set(loanpf.getLoansdate());
		//ICIL-677 start
		if(cslnd004Permission) {
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(wsaanofDay.toInt());
			datcon2Pojo.setIntDate1(wsaaLoanDate.toString());
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("DY");
			callDatcon22780();
			wsaaLoanDate.set(datcon2Pojo.getIntDate2());
		}
		//ICIL-677 end
		wsaaEffdate.set(wsaaSqlEffdate);
		wsaaContractDate.set(br535dto.getOccdate());
		/* Check for loan anniversary flag set*/
		/* If set,*/
		/*    set next interest billing date to be on the next loan*/
		/*    anniv date after the Effective date we are using now.*/
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(1);
			datcon2Pojo.setIntDate1(Integer.toString(loanpf.getLoansdate()));
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("01");
			while ( !(isGT(datcon2Pojo.getIntDate2(), wsaaSqlEffdate))) {
				callDatcon22780();
				datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			}
			loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
			goTo(GotoLabel.exit2779);
		}
		if (isEQ(t6633rec.policyAnnivInterest,"Y")) {
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(1);
			datcon2Pojo.setIntDate1(Integer.toString((br535dto.getOccdate())));
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("01");

			while ( !(isGT(datcon2Pojo.getIntDate2(),wsaaSqlEffdate))) {
				callDatcon22780();
				datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			}

			loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
			goTo(GotoLabel.exit2779);
		}
		/* Get here so the next interest calc. date isn't based on*/
		/* loan or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* Check if table T6633 has a fixed day of the month specified*/
		/* ... If not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet2790();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		//wsaaNewDay.set(wsaaLoanDay); //MIBT-119
		if(!cslnd004Permission) {
			wsaaNewDay.set(wsaaLoanDay);
		}	
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setIntDate1(wsaaNewDate.toString());
		datcon2Pojo.setFreqFactor(1);

		if (isEQ(t6633rec.interestFrequency,SPACES)) {
			datcon2Pojo.setFrequency("01");
		}
		else {
			datcon2Pojo.setFrequency(t6633rec.interestFrequency.toString());
		}
	}

protected void datcon2Loop2775()
	{
		callDatcon22780();
		if (isLTE(datcon2Pojo.getIntDate2(), wsaaSqlEffdate)) {
			datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			goTo(GotoLabel.datcon2Loop2775);
		}
		wsaaNewDate.set(datcon2Pojo.getIntDate2());
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet2790();
			loanpf.setNxtintbdte(wsaaNewDate.toInt());
		}
		else {
			loanpf.setNxtintbdte(Integer.parseInt(datcon2Pojo.getIntDate2()));
		}
	}

protected void callDatcon22780()
	{
		/*START*/
	datcon2Utils.calDatcon2(datcon2Pojo);
	if (!"****".equals(datcon2Pojo.getStatuz())) {
		syserrrec.params.set(datcon2Pojo);
		syserrrec.statuz.set(datcon2Pojo.getStatuz());
		fatalError600();
	}
		/*EXIT*/
	}

protected void dateSet2790()
	{
		/*START*/
		/* We have to check that the date we are going to call Datcon2*/
		/* with is a valid from-date... ie If the interest/capn day in*/
		/* T6633 is > 28, we have to make sure the from-date isn't*/
		/* something like 31/02/nnnn or 31/06/nnnn*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}

protected void postInterestAcmvs2800()
	{
		
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(loanpf.getChdrcoy());
		lifacmvrec.genlcoy.set(loanpf.getChdrcoy());
		lifacmvrec.rdocnum.set(loanpf.getChdrnum());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(loanpf.getLoancurr());
		lifacmvrec.origamt.set(intcalcrec.interestAmount);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(wsaaSqlEffdate);
		lifacmvrec.tranref.set(loanpf.getChdrnum());
		lifacmvrec.trandesc.set(wsaaTrandesc);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(loanpf.getChdrnum());
		wsaaLoanNumber.set(loanpf.getLoannumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		/* Post the interest to the Loan debit account (depending on*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx,1));
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaT5645Idx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(wsaaCnttype);
		lifacmvrec.agnt.set("N");
		List<Acblpf> acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx,2));
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaT5645Idx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		lifacmvrec.function.set("PSTW");
		acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}


protected void writeBextRecord2900()
{	
	bextpf = new Bextpf();

	bextpf.setInstfrom(wsaaIntFromDate);
	bextpf.setInstto(wsaaSqlEffdate.toInt());
	bextpf.setBtdate(wsaaSqlEffdate.toInt());
	bextpf.setBilldate(wsaaSqlEffdate.toInt());
	bextpf.setBillcd(wsaaSqlEffdate.toInt());
	bextpf.setInstamt01(Double.parseDouble(intcalcrec.interestAmount.toString()));
	bextpf.setInstamt02(0);
	bextpf.setInstamt03(0);
	bextpf.setInstamt04(0);
	bextpf.setInstamt05(0);	
	bextpf.setInstamt06(Double.parseDouble(intcalcrec.interestAmount.toString()));
	bextpf.setInstjctl(bsprIO.getRecKeyData().toString().trim().substring(0, 24));
	bextpf.setChdrnum(loanpf.getChdrnum());
	bextpf.setChdrcoy(loanpf.getChdrcoy());		
	bextpf.setChdrpfx(br535dto.getChdrpfx());
	bextpf.setServunit(br535dto.getServunit());
	bextpf.setCnttype(br535dto.getCnttype());
	bextpf.setOccdate(br535dto.getOccdate());
	bextpf.setCcdate(br535dto.getCcdate());
	bextpf.setCownpfx(br535dto.getCownpfx());
	bextpf.setCowncoy(br535dto.getCowncoy());
	bextpf.setCownnum(br535dto.getCownnum());
	bextpf.setInstcchnl(br535dto.getCollchnl());
	bextpf.setCntbranch(br535dto.getCntbranch());
	bextpf.setAgntpfx(br535dto.getAgntpfx());
	bextpf.setAgntcoy(br535dto.getAgntcoy());
	bextpf.setAgntnum(br535dto.getAgntnum());						
	bextpf.setOutflag("");
	bextpf.setPayflag("");
	bextpf.setBilflag("");
	bextpf.setGrupkey("");
	bextpf.setMembsel("");
	bextpf.setSupflag("N");	
	bextpf.setPayrpfx(br535dto.getClntpfx());
	bextpf.setPayrcoy(br535dto.getClntcoy());
	bextpf.setPayrnum(br535dto.getClntnum());	
	bextpf.setMandref(br535dto.getMandref());
	bextpf.setPtdate(br535dto.getPtdate());
	bextpf.setBillchnl(br535dto.getBillchnl());
	bextpf.setInstbchnl(br535dto.getBillchnl());
	bextpf.setInstfreq(br535dto.getBillfreq());
	bextpf.setCntcurr(br535dto.getBillcurr());		
	bextpf.setBankkey(br535dto.getBankkey());
	bextpf.setBankacckey(br535dto.getBankacckey());
	bextpf.setFacthous(br535dto.getFacthous());
	bextpf.setMandstat(br535dto.getMandstat());
	readT3629();
	bextpf.setBankcode(t3629rec.bankcode.toString());
	bextpf.setNextdate(br535dto.getNextdate());		
	getGLAcctCodes(br535dto.getMandref(),1);
	bextpf.setGlmap(strglMap);
	bextpf.setSacscode(strSacscode);
	bextpf.setSacstyp(strSacsType);
	bextpf.setEffdatex(0);
	bextpf.setDdderef(0);	
	bextpf.setJobno(0);
	bextBulkInsList.add(bextpf);
}

protected void commitBextPFInsert(){	 
	boolean isInsertBextPF = bextpfDAO.insertBextPF(bextBulkInsList);
	if (!isInsertBextPF) {
		LOGGER.error("Insert BextPF record failed.");
		fatalError600();
	}else {
		bextBulkInsList.clear();
	}
}

protected void getGLAcctCodes(String mandRef, int intIncrement){
	
	if (isNE(mandRef, SPACES)) {
		strSacscode = t5645rec.sacscode[wsaaBaseIdx.toInt() + intIncrement].toString();
		strSacsType = t5645rec.sacstype[wsaaBaseIdx.toInt() + intIncrement].toString();
		strglMap = t5645rec.glmap[wsaaBaseIdx.toInt() + intIncrement].toString();
		strglSign = t5645rec.sign[wsaaBaseIdx.toInt() + intIncrement].toString();
	}
	else {
		strSacscode = "";
		strSacsType = "";
		strglMap = "";
		strglSign = "";
	}
}
protected void readT3629()
	{
		/* Read T3629 for bank code.*/
		
		String keyItemitem = br535dto.getBillcurr().trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t3629ListMap.containsKey(keyItemitem)){	
			itempfList = t3629ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				itemFound = true;
			}		
		}
		if (!itemFound) {
			syserrrec.params.set(t3629rec.t3629Rec);
			syserrrec.statuz.set(hl63);
			fatalError600();		
		}		
	}

protected void update3000()
	{
		/*UPDATE*/
		totalLoanDebt3100();
		capitalise3200();
		checkSurrVal3300();
		ptrnBatcup3400();
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void totalLoanDebt3100()
	{
		
		/* Find current amount of Interest owed - read ACBL record.*/
		wsaaCurbal.set(ZERO);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(loanpf.getChdrnum());
		wsaaLoanNumber.set(loanpf.getLoannumber());
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 1));
		acblpf = acblpfDAO.loadDataByBatch(loanpf.getChdrcoy(), wsaaT5645Sacscode[wsaaT5645Idx.toInt()].toString(), wsaaRldgacct.toString(), loanpf.getLoancurr(), wsaaT5645Sacstype[wsaaT5645Idx.toInt()].toString());
		if (acblpf == null) {
			wsaaCurbal.set(ZERO);
		}
		else {
			/*    MOVE ACBL-SACSCURBAL     TO WSAA-CURBAL                   */
			if (isEQ(loanpf.getLoantype(), "D")) {
				compute(wsaaCurbal, 2).set(mult(acblpf.getSacscurbal(), -1));
			}
			else {
				wsaaCurbal.set(acblpf.getSacscurbal());
			}
		}
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx,3));
		/*    MOVE T5645-SACSTYPE(WSAA-T5645-IDX)                          */
		/*                                TO WSAA-SACSTYPE.                */
		wsaaSacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		wsaaPostedAmount.set(ZERO);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(loanpf.getChdrnum());
		wsaaLoanNumber.set(loanpf.getLoannumber());
	
		
		acmvpfList = acmvpfDAO.loadDataByBatch(wsaaRldgacct.toString(), wsaaSacscode.toString(), wsaaSacstype.toString(), loanpf.getLstcapdate(), loanpf.getChdrcoy());
		
		
		 if(acmvpfList != null && acmvpfList.size()>0){
		    acmvpfIter = acmvpfList.iterator();    	      	 
		    while (acmvpfIter.hasNext()) {
			processAcmvpfList(acmvpfIter.next());
		   }
		 }

		/* Add the last capitalised amount to the total of any changes*/
		/* in the principal*/
		/* - this now gives us a new principal amount which we can add*/
		/* to the total interest over the last capitalisation period*/
		/* to get our total loan debt value.*/
		/* Sum principal*/
		compute(wsaaPostedAmount, 2).set(add(loanpf.getLstcaplamt(), wsaaPostedAmount));
		/* Sum interest accrued and principal*/
		compute(wsaaTotalLoanDebt, 2).set(add(wsaaCurbal, wsaaPostedAmount));
	}



protected void processAcmvpfList(Acmvpf acmvpf)
	{
		
		/* Read the table T3695 for the field sign of the posting.         */		
		readT3695(acmvpf.getSacstyp().trim());
		
		if (isEQ(t3695rec.sign,"-")) {
			if (isEQ(acmvpf.getGlsign(),"-")) {
				acmvpf.setGlsign("+");
			}
			else {
				acmvpf.setGlsign("-");
			}
		}
		if (isEQ(acmvpf.getEffdate(), loanpf.getLstcapdate())) {
			if (isEQ(loanpf.getLoansdate(), loanpf.getLstcapdate())
			&& isEQ(acmvpf.getGlsign(), wsaaSign)) {
				/*     AND ACMVLON-GLSIGN      = '-'                            */
				/*     AND ACMVLON-GLSIGN  NOT = WSAA-SIGN              <LA2108>*/
				/*    AND ACMVLON-JOB-NAME NOT = 'L2ANTERLS'            <LA2108>*/
				/*CONTINUE_STMT*/
			}
			else {
				return ;
			}
		}
		/* Get here so we must have a valid record*/
		/* Check currency of ACMV against currency of loan - if diff,*/
		/* we must convert ACMV value to a value in the Loan currency*/
		if (isNE(acmvpf.getOrigcurr(), loanpf.getLoancurr())) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(acmvpf.getOrigamt());
			conlinkrec.statuz.set(SPACES);
			conlinkrec.function.set("CVRT");
			conlinkrec.currIn.set(acmvpf.getOrigcurr());
			conlinkrec.cashdate.set(99999999);
			conlinkrec.currOut.set(loanpf.getLoancurr());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(loanpf.getChdrcoy());
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError600();
			}
			else {
				/*         IF ACMVLON-GLSIGN   =  '-'                           */
				/*            IF ACMVLON-GLSIGN  NOT = WSAA-SIGN           <V4L001>*/
				if (isNE(conlinkrec.amountOut, 0)) {	
					zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata());
					zrdecplcPojo.setCurrency(loanpf.getLoancurr());
					zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
					zrdecplcPojo.setBatctrcde(batcdorrec.trcde.toString());
					zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
					if (!varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
						syserrrec.statuz.set(zrdecplcPojo.getStatuz());
						fatalError600();
					}
					conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut());
				}
				if (isEQ(acmvpf.getGlsign(), wsaaSign)) {
					compute(conlinkrec.amountOut, 2).set(mult(conlinkrec.amountOut, -1));
				}
				wsaaPostedAmount.add(conlinkrec.amountOut);
			}
		}
		else {
			if (isEQ(acmvpf.getGlsign(),wsaaSign)) {
				setPrecision(acmvpf.getOrigamt(), 2);
				acmvpf.setOrigamt(mult(acmvpf.getOrigamt(),-1).getbigdata());
			}
			wsaaPostedAmount.getbigdata().add(acmvpf.getOrigamt());
		}
	}

protected void readT3695(String sacstype){

	/* Read the accounting rules table*/
	String keyItemitem = sacstype;
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3695ListMap.containsKey(keyItemitem)){	
		itempfList = t3695ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;		
		}		
	}
	if (!itemFound) {
		syserrrec.statuz.set(h420);
		syserrrec.params.set(t3695);
		fatalError600();
	}	
}


protected void capitalise3200()
	{
		start3200();
		updateLoan3205();
	}

protected void start3200()
	{
		/* If no Interest accrued, just update Loan record.*/
		if (isEQ(wsaaCurbal, ZERO)) {
			return ;
		}
		/* Set up LIFACMV fields.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(loanpf.getChdrcoy());
		lifacmvrec.genlcoy.set(loanpf.getChdrcoy());
		lifacmvrec.rdocnum.set(loanpf.getChdrnum());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(loanpf.getLoancurr());
		/* Move total Interest accrued over period to Loan principal*/
		/* Sub A/C.*/
		lifacmvrec.origamt.set(wsaaCurbal);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(wsaaSqlEffdate);
		lifacmvrec.tranref.set(loanpf.getChdrnum());
		lifacmvrec.trandesc.set(wsaaTrandesc);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(loanpf.getChdrnum());
		wsaaLoanNumber.set(loanpf.getLoannumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		/* Post the Interest to the Loan principal account.*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx,3));
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaT5645Idx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(wsaaCnttype);
		lifacmvrec.agnt.set("N");
		List<Acblpf> acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx,4));
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaT5645Idx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		lifacmvrec.function.set("PSTW");
		acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void updateLoan3205()
	{
		
		loanpf.setValidflag("1");
		loanpf.setLstcapdate(wsaaSqlEffdate.toInt());
		loanpf.setLstcaplamt(Double.parseDouble(wsaaTotalLoanDebt.toString()));
		nextCapnDate3220();
		loanCapnBulkUpdtList.add(loanpf);
	}

protected void nextCapnDate3220()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3220();
				case datcon4Loop3225:
					datcon4Loop3225();
				case exit3229:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3220()
	{
		/* Check the Capitalisation details on T6633 in the following*/
		/* order: i) Capitalise on Loan anniv ... Y/N*/
		/*       ii) Capitalise on Policy anniv.. Y/N*/
		/*      iii) Check Cap freq & whether a specific Day is chosen*/
		wsaaLoanDate.set(loanpf.getLoansdate());
		//ICIL-677 start
		if(cslnd004Permission) {
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(wsaanofDay.toInt());
			datcon2Pojo.setIntDate1(wsaaLoanDate.toString());
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("DY");
			callDatcon22780();
			wsaaLoanDate.set(datcon2Pojo.getIntDate2());
		}
		//ICIL-677 end
		wsaaEffdate.set(wsaaSqlEffdate);
		wsaaContractDate.set(br535dto.getOccdate());
		wsaaNewDate.set(ZERO);
		/* Check for loan anniversary flag set*/
		/* IF set,*/
		/*  set next capitalisation  date to be on the next loan anniv*/
		/*  date after the Effective date we are using now.*/
		if (isEQ(t6633rec.annloan, "Y")) {	
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(1);
			datcon2Pojo.setIntDate1(Integer.toString(loanpf.getLoansdate()));
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("01");
			while ( !(isGT(datcon2Pojo.getIntDate2(), wsaaSqlEffdate))) {
				callDatcon22780();
				datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			}

			loanpf.setNxtcapdate(Integer.parseInt(datcon2Pojo.getIntDate2()));
			goTo(GotoLabel.exit3229);
		}
		if (isEQ(t6633rec.annpoly,"Y")) {
			datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(1);
			datcon2Pojo.setIntDate1(Integer.toString((br535dto.getOccdate())));
			datcon2Pojo.setIntDate2("0");
			datcon2Pojo.setFrequency("01");

			while ( !(isGT(datcon2Pojo.getIntDate2(),wsaaSqlEffdate))) {
				callDatcon22780();
				datcon2Pojo.setIntDate1(datcon2Pojo.getIntDate2());
			}

			loanpf.setNxtcapdate(Integer.parseInt(datcon2Pojo.getIntDate2()));
			goTo(GotoLabel.exit3229);
		}
		/* Get here so the next capitalisation date isn't based on loan*/
		/* or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified*/
		/* ...if not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.day);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet2790();
		}
		else {
			if (isNE(t6633rec.day, ZERO)) {
				wsaaNewDay.set(t6633rec.day);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		if(!cslnd004Permission) {
			wsaaNewDay.set(wsaaLoanDay);
		}
		//wsaaNewDay.set(wsaaLoanDay); //MIBT-119
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(wsaaNewDate);
		datcon4rec.freqFactor.set(1);
		datcon4rec.billday.set(wsaaNewDay);
		datcon4rec.billmonth.set(wsaaNewMonth);
		if (isEQ(t6633rec.compfreq,SPACES)) {
			datcon4rec.frequency.set("01");
		}
		else {
			datcon4rec.frequency.set(t6633rec.compfreq);
		}
	}

protected void datcon4Loop3225()
	{
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		if (isLTE(datcon4rec.intDate2, wsaaSqlEffdate)) {
			datcon4rec.intDate1.set(datcon4rec.intDate2);
			goTo(GotoLabel.datcon4Loop3225);
		}
		wsaaNewDate.set(datcon4rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet2790();
			loanpf.setNxtcapdate(wsaaNewDate.toInt());
		}
		else {
			loanpf.setNxtcapdate(datcon4rec.intDate2.toInt());
		}
	}

protected void checkSurrVal3300()
	{
		
		/* Skip this section if it is not a Policy Loan or an APL.*/
		if (isNE(loanpf.getLoantype(), "A")
		&& isNE(loanpf.getLoantype(), "P")) {
			return ;
		}
		
		/* We just call NFLOAN subroutine here to see whether contract*/
		/* we are using can continue to support the loan(s)*/
		/* statuz of O-K.*/
		/* We are not worried whether it returns O-K or NAPL..... if*/
		/* the statuz is NAPL, then a line will appear on the R6244*/
		/* report and the user can then deal with Contract + Loan(s)*/
		nfloanrec.nfloanRec.set(SPACES);
		nfloanrec.language.set(bsscIO.getLanguage());
		nfloanrec.chdrcoy.set(loanpf.getChdrcoy());
		nfloanrec.chdrnum.set(loanpf.getChdrnum());
		nfloanrec.cntcurr.set(br535dto.getCntcurr());
		nfloanrec.effdate.set(wsaaSqlEffdate);
		nfloanrec.ptdate.set(br535dto.getPtdate());
		nfloanrec.batcbrn.set(batcdorrec.branch);
		nfloanrec.company.set(bsprIO.getCompany());
		nfloanrec.billfreq.set(br535dto.getBillfreq());
		nfloanrec.cnttype.set(br535dto.getCnttype());
		nfloanrec.polsum.set(br535dto.getPolsum());
		nfloanrec.polinc.set(br535dto.getPolinc());
		nfloanrec.batcBatctrcde.set(batcdorrec.trcde);
		nfloanrec.outstamt.set(ZERO);
		callProgram(Nfloan.class, nfloanrec.nfloanRec);
		if (isNE(nfloanrec.statuz,varcom.oK)
		&& isNE(nfloanrec.statuz,"NAPL")) {
			syserrrec.params.set(nfloanrec.nfloanRec);
			syserrrec.statuz.set(nfloanrec.statuz);
			fatalError600();
		}
	}

protected void ptrnBatcup3400()
	{
		
		if (ptrnpf != null && isEQ(ptrnpf.getChdrnum(), loanpf.getChdrnum())) {
			return ;
		}
		
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrcoy(loanpf.getChdrcoy());
		ptrnpf.setChdrpfx(br535dto.getChdrpfx());
		ptrnpf.setChdrnum(loanpf.getChdrnum());
		ptrnpf.setTranno(br535dto.getTranno()+1);//ILIFE-5112
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setPtrneff(wsaaSqlEffdate.toInt());
		ptrnpf.setUserT(wsaaUser);
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
		ptrnBulkInsList.add(ptrnpf);
	
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set("BA");	//ILIFE-3210
		batcuprec.batccoy.set(batcdorrec.company);
		batcuprec.batcbrn.set(batcdorrec.branch);
		batcuprec.batcactyr.set(batcdorrec.actyear);
		batcuprec.batctrcde.set(batcdorrec.trcde);
		batcuprec.batcactmn.set(batcdorrec.actmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}



protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else {
		ptrnBulkInsList.clear();
	}
}

protected void commit3500()
	{
		/*COMMIT*/
	 commitControlTotals();
	 commitChdrBulkUpdate();	
	 commitLoanUpdate();
	 commitBextPFInsert();
	 commitLoanCapUpdate();
	 commitPtrnBulkInsert();
	 commitAcblPFInsert();
	 
	}

protected void commitChdrBulkUpdate()
{	 
	boolean isUpdateContractHeader = chdrpfDAO.updateChdrTrannoByChdrnum(chdrBulkUpdtList);
	if (!isUpdateContractHeader) {
		LOGGER.error("Update Contract Header failed.");
		fatalError600();
	}else {
		chdrBulkUpdtList.clear();
	}
}

protected void commitLoanUpdate(){
	 boolean isUpdateLoanRec = loanpfDAO.updateLoanValRecords(loanBulkUpdtList);
	if (!isUpdateLoanRec) {
		LOGGER.error("Update Loan Record failed.");
		fatalError600();
	}else {
		loanBulkUpdtList.clear();
	}
}

protected void commitLoanCapUpdate(){
	 boolean isUpdateLoanCapRec = loanpfDAO.updateLoanCapRecords(loanCapnBulkUpdtList);
	if (!isUpdateLoanCapRec) {
		LOGGER.error("Update Loan Record failed.");
		fatalError600();
	}else {
		loanCapnBulkUpdtList.clear();
	}
}

protected void commitAcblPFInsert(){
	 if (acblpfInsertAll != null && !acblpfInsertAll.isEmpty()) 
     	acblpfDAO.processAcblpfList(acblpfInsertAll, false); 
}



protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
	    
	    
		lsaaStatuz.set(varcom.oK);
		
		t6633ListMap.clear();
		t6633ListMap = null;
		
		t3629ListMap.clear();
		t3629ListMap = null;
		
		t3695ListMap.clear();
		t3695ListMap = null;
		
		
		
		loanpfList.clear();
		loanpfList = null;
		
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;
		
		loanBulkUpdtList.clear();
		loanBulkUpdtList = null;
		
		bextBulkInsList.clear();
		bextBulkInsList = null;
		
		loanCapnBulkUpdtList.clear();
		loanCapnBulkUpdtList = null;
		
		acmvpfList.clear();
		acmvpfList = null;
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;	
		
		if (acblpfInsertAll != null) {
	       	acblpfInsertAll.clear();
	       	acblpfInsertAll = null;
	       }  

		/*EXIT*/
	}

/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}


}
