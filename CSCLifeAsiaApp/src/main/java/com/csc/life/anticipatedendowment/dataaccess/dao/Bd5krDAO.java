package com.csc.life.anticipatedendowment.dataaccess.dao;

import java.util.List;

import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
	
public interface Bd5krDAO extends BaseDAO<Zraepf>{
	public List<Zraepf> loadDataByBatch(int BatchID);
	public int populateBd5krTemp(String coy, String effDate, int batchExtractSize);
}
