/*
 * File: Pr530.java
 * Date: 30 August 2009 1:39:02
 * Author: Quipoz Limited
 *
 * Class transformed from PR530.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.anticipatedendowment.procedures.Tr530pt;
import com.csc.life.anticipatedendowment.screens.Sr530ScreenVars;
import com.csc.life.anticipatedendowment.tablestructures.Tr530rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Pr530 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR530");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr530rec tr530rec = new Tr530rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr530ScreenVars sv = ScreenProgram.getScreenVars( Sr530ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		generalArea1045,
		preExit,
		exit2090,
		other3080,
		exit3090
	}

	public Pr530() {
		super();
		screenVars = sv;
		new ScreenModel("Sr530", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr530rec.tr530Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr530rec.zrage01.set(ZERO);
		tr530rec.zrage02.set(ZERO);
		tr530rec.zrage03.set(ZERO);
		tr530rec.zrage04.set(ZERO);
		tr530rec.zrage05.set(ZERO);
		tr530rec.zrage06.set(ZERO);
		tr530rec.zrage07.set(ZERO);
		tr530rec.zrage08.set(ZERO);
		tr530rec.zragfr01.set(ZERO);
		tr530rec.zragfr02.set(ZERO);
		tr530rec.zragfr03.set(ZERO);
		tr530rec.zragfr04.set(ZERO);
		tr530rec.zragfr05.set(ZERO);
		tr530rec.zragfr06.set(ZERO);
		tr530rec.zragfr07.set(ZERO);
		tr530rec.zragfr08.set(ZERO);
		tr530rec.zragfr09.set(ZERO);
		tr530rec.zragfr10.set(ZERO);
		tr530rec.zragto01.set(ZERO);
		tr530rec.zragto02.set(ZERO);
		tr530rec.zragto03.set(ZERO);
		tr530rec.zragto04.set(ZERO);
		tr530rec.zragto05.set(ZERO);
		tr530rec.zragto06.set(ZERO);
		tr530rec.zragto07.set(ZERO);
		tr530rec.zragto08.set(ZERO);
		tr530rec.zragto09.set(ZERO);
		tr530rec.zragto10.set(ZERO);
		tr530rec.zrpcpd01.set(ZERO);
		tr530rec.zrpcpd02.set(ZERO);
		tr530rec.zrpcpd03.set(ZERO);
		tr530rec.zrpcpd04.set(ZERO);
		tr530rec.zrpcpd05.set(ZERO);
		tr530rec.zrpcpd06.set(ZERO);
		tr530rec.zrpcpd07.set(ZERO);
		tr530rec.zrpcpd08.set(ZERO);
		tr530rec.zrpcpd09.set(ZERO);
		tr530rec.zrpcpd10.set(ZERO);
		tr530rec.zrpcpd11.set(ZERO);
		tr530rec.zrpcpd12.set(ZERO);
		tr530rec.zrpcpd13.set(ZERO);
		tr530rec.zrpcpd14.set(ZERO);
		tr530rec.zrpcpd15.set(ZERO);
		tr530rec.zrpcpd16.set(ZERO);
		tr530rec.zrpcpd17.set(ZERO);
		tr530rec.zrpcpd18.set(ZERO);
		tr530rec.zrpcpd19.set(ZERO);
		tr530rec.zrpcpd20.set(ZERO);
		tr530rec.zrpcpd21.set(ZERO);
		tr530rec.zrpcpd22.set(ZERO);
		tr530rec.zrpcpd23.set(ZERO);
		tr530rec.zrpcpd24.set(ZERO);
		tr530rec.zrpcpd25.set(ZERO);
		tr530rec.zrpcpd26.set(ZERO);
		tr530rec.zrpcpd27.set(ZERO);
		tr530rec.zrpcpd28.set(ZERO);
		tr530rec.zrpcpd29.set(ZERO);
		tr530rec.zrpcpd30.set(ZERO);
		tr530rec.zrpcpd31.set(ZERO);
		tr530rec.zrpcpd32.set(ZERO);
		tr530rec.zrpcpd33.set(ZERO);
		tr530rec.zrpcpd34.set(ZERO);
		tr530rec.zrpcpd35.set(ZERO);
		tr530rec.zrpcpd36.set(ZERO);
		tr530rec.zrpcpd37.set(ZERO);
		tr530rec.zrpcpd38.set(ZERO);
		tr530rec.zrpcpd39.set(ZERO);
		tr530rec.zrpcpd40.set(ZERO);
		tr530rec.zrpcpd41.set(ZERO);
		tr530rec.zrpcpd42.set(ZERO);
		tr530rec.zrpcpd43.set(ZERO);
		tr530rec.zrpcpd44.set(ZERO);
		tr530rec.zrpcpd45.set(ZERO);
		tr530rec.zrpcpd46.set(ZERO);
		tr530rec.zrpcpd47.set(ZERO);
		tr530rec.zrpcpd48.set(ZERO);
		tr530rec.zrpcpd49.set(ZERO);
		tr530rec.zrpcpd50.set(ZERO);
		tr530rec.zrpcpd51.set(ZERO);
		tr530rec.zrpcpd52.set(ZERO);
		tr530rec.zrpcpd53.set(ZERO);
		tr530rec.zrpcpd54.set(ZERO);
		tr530rec.zrpcpd55.set(ZERO);
		tr530rec.zrpcpd56.set(ZERO);
		tr530rec.zrpcpd57.set(ZERO);
		tr530rec.zrpcpd58.set(ZERO);
		tr530rec.zrpcpd59.set(ZERO);
		tr530rec.zrpcpd60.set(ZERO);
		tr530rec.zrpcpd61.set(ZERO);
		tr530rec.zrpcpd62.set(ZERO);
		tr530rec.zrpcpd63.set(ZERO);
		tr530rec.zrpcpd64.set(ZERO);
		tr530rec.zrpcpd65.set(ZERO);
		tr530rec.zrpcpd66.set(ZERO);
		tr530rec.zrpcpd67.set(ZERO);
		tr530rec.zrpcpd68.set(ZERO);
		tr530rec.zrpcpd69.set(ZERO);
		tr530rec.zrpcpd70.set(ZERO);
		tr530rec.zrpcpd71.set(ZERO);
		tr530rec.zrpcpd72.set(ZERO);
		tr530rec.zrpcpd73.set(ZERO);
		tr530rec.zrpcpd74.set(ZERO);
		tr530rec.zrpcpd75.set(ZERO);
		tr530rec.zrpcpd76.set(ZERO);
		tr530rec.zrpcpd77.set(ZERO);
		tr530rec.zrpcpd78.set(ZERO);
		tr530rec.zrpcpd79.set(ZERO);
		tr530rec.zrpcpd80.set(ZERO);
	}

protected void generalArea1045()
	{
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.zrages.set(tr530rec.zrages);
		sv.zragfrs.set(tr530rec.zragfrs);
		sv.zragtos.set(tr530rec.zragtos);
		sv.zredsumas.set(tr530rec.zredsumas);
		sv.zrpcpds.set(tr530rec.zrpcpds);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr530rec.tr530Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrages,tr530rec.zrages)) {
			tr530rec.zrages.set(sv.zrages);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zragfrs,tr530rec.zragfrs)) {
			tr530rec.zragfrs.set(sv.zragfrs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zragtos,tr530rec.zragtos)) {
			tr530rec.zragtos.set(sv.zragtos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zredsumas,tr530rec.zredsumas)) {
			tr530rec.zredsumas.set(sv.zredsumas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zrpcpds,tr530rec.zrpcpds)) {
			tr530rec.zrpcpds.set(sv.zrpcpds);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr530pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
