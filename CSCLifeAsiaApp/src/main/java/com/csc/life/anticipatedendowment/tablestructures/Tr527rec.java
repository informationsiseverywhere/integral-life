package com.csc.life.anticipatedendowment.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:58
 * Description:
 * Copybook name: TR527REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr527rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr527Rec = new FixedLengthStringData(532);
  	public FixedLengthStringData pymdescs = new FixedLengthStringData(420).isAPartOf(tr527Rec, 0);
  	public FixedLengthStringData[] pymdesc = FLSArrayPartOfStructure(14, 30, pymdescs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(420).isAPartOf(pymdescs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData pymdesc01 = new FixedLengthStringData(30).isAPartOf(filler, 0);
  	public FixedLengthStringData pymdesc02 = new FixedLengthStringData(30).isAPartOf(filler, 30);
  	public FixedLengthStringData pymdesc03 = new FixedLengthStringData(30).isAPartOf(filler, 60);
  	public FixedLengthStringData pymdesc04 = new FixedLengthStringData(30).isAPartOf(filler, 90);
  	public FixedLengthStringData pymdesc05 = new FixedLengthStringData(30).isAPartOf(filler, 120);
  	public FixedLengthStringData pymdesc06 = new FixedLengthStringData(30).isAPartOf(filler, 150);
  	public FixedLengthStringData pymdesc07 = new FixedLengthStringData(30).isAPartOf(filler, 180);
  	public FixedLengthStringData pymdesc08 = new FixedLengthStringData(30).isAPartOf(filler, 210);
  	public FixedLengthStringData pymdesc09 = new FixedLengthStringData(30).isAPartOf(filler, 240);
  	public FixedLengthStringData pymdesc10 = new FixedLengthStringData(30).isAPartOf(filler, 270);
  	public FixedLengthStringData pymdesc11 = new FixedLengthStringData(30).isAPartOf(filler, 300);
  	public FixedLengthStringData pymdesc12 = new FixedLengthStringData(30).isAPartOf(filler, 330);
  	public FixedLengthStringData pymdesc13 = new FixedLengthStringData(30).isAPartOf(filler, 360);
  	public FixedLengthStringData pymdesc14 = new FixedLengthStringData(30).isAPartOf(filler, 390);
  	public FixedLengthStringData payOptions = new FixedLengthStringData(112).isAPartOf(tr527Rec, 420);
  	public FixedLengthStringData[] payOption = FLSArrayPartOfStructure(14, 8, payOptions, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(112).isAPartOf(payOptions, 0, FILLER_REDEFINE);
  	public FixedLengthStringData payOption01 = new FixedLengthStringData(8).isAPartOf(filler1, 0);
  	public FixedLengthStringData payOption02 = new FixedLengthStringData(8).isAPartOf(filler1, 8);
  	public FixedLengthStringData payOption03 = new FixedLengthStringData(8).isAPartOf(filler1, 16);
  	public FixedLengthStringData payOption04 = new FixedLengthStringData(8).isAPartOf(filler1, 24);
  	public FixedLengthStringData payOption05 = new FixedLengthStringData(8).isAPartOf(filler1, 32);
  	public FixedLengthStringData payOption06 = new FixedLengthStringData(8).isAPartOf(filler1, 40);
  	public FixedLengthStringData payOption07 = new FixedLengthStringData(8).isAPartOf(filler1, 48);
  	public FixedLengthStringData payOption08 = new FixedLengthStringData(8).isAPartOf(filler1, 56);
  	public FixedLengthStringData payOption09 = new FixedLengthStringData(8).isAPartOf(filler1, 64);
  	public FixedLengthStringData payOption10 = new FixedLengthStringData(8).isAPartOf(filler1, 72);
  	public FixedLengthStringData payOption11 = new FixedLengthStringData(8).isAPartOf(filler1, 80);
  	public FixedLengthStringData payOption12 = new FixedLengthStringData(8).isAPartOf(filler1, 88);
  	public FixedLengthStringData payOption13 = new FixedLengthStringData(8).isAPartOf(filler1, 96);
  	public FixedLengthStringData payOption14 = new FixedLengthStringData(8).isAPartOf(filler1, 104);


	public void initialize() {
		COBOLFunctions.initialize(tr527Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr527Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}