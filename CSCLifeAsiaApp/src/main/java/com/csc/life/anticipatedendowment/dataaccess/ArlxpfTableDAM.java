package com.csc.life.anticipatedendowment.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ArlxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:54
 * Class transformed from ARLXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ArlxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 32;
	public FixedLengthStringData arlxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData arlxpfRecord = arlxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(arlxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(arlxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(arlxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(arlxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(arlxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(arlxrec);
	public PackedDecimalData nextPaydate = DD.npaydate.copy().isAPartOf(arlxrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(arlxrec);
	public FixedLengthStringData cownnum = DD.cownum.copy().isAPartOf(arlxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ArlxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for ArlxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ArlxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ArlxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ArlxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ArlxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ArlxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ARLXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"NPAYDATE, " +
							"VALIDFLAG, " +
							"COWNNUM, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     nextPaydate,
                                     validflag,
                                     cownnum,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		nextPaydate.clear();
  		validflag.clear();
  		cownnum.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getArlxrec() {
  		return arlxrec;
	}

	public FixedLengthStringData getArlxpfRecord() {
  		return arlxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setArlxrec(what);
	}

	public void setArlxrec(Object what) {
  		this.arlxrec.set(what);
	}

	public void setArlxpfRecord(Object what) {
  		this.arlxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(arlxrec.getLength());
		result.set(arlxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}