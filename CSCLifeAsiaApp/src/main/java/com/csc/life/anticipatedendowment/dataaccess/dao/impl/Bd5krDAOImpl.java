package com.csc.life.anticipatedendowment.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.Bd5krDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5krDTO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Bd5krDAOImpl extends BaseDAOImpl<Zraepf> implements Bd5krDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5krDAOImpl.class);
	
	public List<Zraepf> loadDataByBatch(int BatchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM VM1DTA.BD5KRTEMP ");
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Zraepf> zraeList = new ArrayList<Zraepf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(BatchID));
			
			rs = ps.executeQuery();
			while(rs.next()){
				Zraepf zraepf = new Zraepf();
				Bd5krDTO bd5krDTO = new Bd5krDTO();
				zraepf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				zraepf.setChdrcoy(rs.getString("Chdrcoy"));
                zraepf.setChdrnum(rs.getString("Chdrnum"));
                zraepf.setPlnsfx(rs.getInt("Plnsfx"));
                zraepf.setLife(rs.getString("Life"));
                zraepf.setCoverage(rs.getString("Coverage"));
                zraepf.setRider(rs.getString("Rider"));                
                zraepf.setValidflag(rs.getString("Validflag"));
                zraepf.setTranno(rs.getInt("RPTRANNO")); 
                zraepf.setFlag(rs.getString("Flag"));
                zraepf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
                zraepf.setApintamt(rs.getBigDecimal("APINTAMT") == null?BigDecimal.ZERO:rs.getBigDecimal("APINTAMT"));	
                zraepf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
                zraepf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
                zraepf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
                zraepf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));                
                zraepf.setPaycurr(rs.getString("PAYCURR"));
                
                bd5krDTO.setCnt(rs.getInt("CNT"));
				bd5krDTO.setUniqueNumber(rs.getLong("CH_UNIQUE_NUMBER"));
				bd5krDTO.setChdrcoy(rs.getString("CHDRCOY").trim());
				bd5krDTO.setChdrnum(rs.getString("CHDRNUM").trim());
				bd5krDTO.setCurrfrom(rs.getInt("CURRFROM"));
				bd5krDTO.setTranno(rs.getInt("TRANNO"));
				bd5krDTO.setCnttype(rs.getString("CNTTYPE").trim());
				bd5krDTO.setChdrpfx(rs.getString("CHDRPFX").trim());
				bd5krDTO.setServunit(rs.getString("SERVUNIT").trim());
				bd5krDTO.setOccdate(rs.getInt("OCCDATE"));
				bd5krDTO.setCcdate(rs.getInt("CCDATE"));
				bd5krDTO.setCownpfx(rs.getString("COWNPFX").trim());
				bd5krDTO.setCowncoy(rs.getString("COWNCOY").trim());
				bd5krDTO.setCownnum(rs.getString("COWNNUM").trim());				
				bd5krDTO.setCollchnl(rs.getString("COLLCHNL").trim());
				bd5krDTO.setCntbranch(rs.getString("CNTBRANCH").trim());
				bd5krDTO.setAgntpfx(rs.getString("AGNTPFX").trim());				
				bd5krDTO.setAgntcoy(rs.getString("AGNTCOY").trim());
				bd5krDTO.setAgntnum(rs.getString("AGNTNUM").trim());
				zraepf.setBd5krDTO(bd5krDTO);
				zraeList.add(zraepf);	
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zraeList;
	}	
	
	public int populateBd5krTemp(String coy, String effDate, int batchExtractSize) {
		initializeBd5krTemp();
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.BD5KRTemp ");
		sb.append("(BATCHID, CNT, UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,VALIDFLAG,PAYCURR,RPTRANNO, FLAG, APCAPLAMT, APINTAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, APNXTINTBDTE, USRPRF, JOBNM, CH_UNIQUE_NUMBER, CURRFROM, TRANNO, CNTTYPE, CHDRPFX, SERVUNIT, OCCDATE, CCDATE, COWNPFX, COWNCOY, COWNNUM, COLLCHNL, CNTBRANCH, AGNTPFX, AGNTCOY, AGNTNUM, DATIME) ");
		sb.append("SELECT * FROM( ");
		sb.append("SELECT " );		
		sb.append("floor((row_number()over(ORDER BY RP.CHDRCOY ASC, RP.CHDRNUM ASC)-1)/?) batchnum, count(*) over(partition by RP.CHDRCOY,RP.CHDRNUM) as cnt, ");
		sb.append("RP.UNIQUE_NUMBER, RP.CHDRCOY,RP.CHDRNUM,RP.PLNSFX,RP.LIFE,RP.COVERAGE,RP.RIDER,RP.VALIDFLAG,RP.PAYCURR,RP.TRANNO RPTRANNO,");
		sb.append("RP.FLAG, RP.APCAPLAMT LSTCAPLAMT,RP.APINTAMT, RP.APLSTCAPDATE LSTCAPDATE, RP.APNXTCAPDATE NXTCAPDATE, "); 
		sb.append("RP.APLSTINTBDTE LSTINTBDTE, RP.APNXTINTBDTE NXTINTBDTE,RP.USRPRF, RP.JOBNM, ");  	
		sb.append("CH.UNIQUE_NUMBER CH_UNIQUE_NUMBER, CH.CURRFROM, CH.TRANNO, CH.CNTTYPE, CH.CHDRPFX, CH.SERVUNIT, CH.OCCDATE, CH.CCDATE, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM, CH.COLLCHNL, CH.CNTBRANCH, CH.AGNTPFX, CH.AGNTCOY, CH.AGNTNUM, ? DATIME ");		 
		sb.append("FROM   ZRAEPF RP 	 ");
		sb.append("INNER JOIN CHDRPF CH ON  CH.CHDRCOY = RP.CHDRCOY  AND CH.CHDRNUM=RP.CHDRNUM  AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG in ('1') ");		 
		sb.append("WHERE RP.FLAG = '1' ");
		sb.append("AND RP.VALIDFLAG = '1' ");
		sb.append("AND RP.APCAPLAMT > 0 ");
		sb.append("AND RP.CHDRCOY = ? ");
		sb.append("AND RP.APLSTINTBDTE < ? ");
		sb.append("AND RP.APNXTINTBDTE <= ? ");
		sb.append(") MAIN ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			ps.setString(3, coy);
			ps.setString(4, effDate);
			ps.setString(5, effDate);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBd5krTemp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBd5krTemp() {
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM VM1DTA.BD5KRTemp ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBd5krTemp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}	
}
