/*
 * File: Tr530pt.java
 * Date: 30 August 2009 2:43:09
 * Author: Quipoz Limited
 * 
 * Class transformed from TR530PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.anticipatedendowment.tablestructures.Tr530rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR530.
*
*
*****************************************************************
* </pre>
*/
public class Tr530pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Anticipated Endowment Rules - Age               SR530");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(70);
	private FixedLengthStringData filler9 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine004, 10, FILLER).init("Partial PaymentPayable at Policy Anniversary for child aged");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(7);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Age at");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(70);
	private FixedLengthStringData filler12 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" entry");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 13).setPattern("ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 20).setPattern("ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 28).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 36).setPattern("ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 52).setPattern("ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 60).setPattern("ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 68).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 1).setPattern("ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 5).setPattern("ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 5).setPattern("ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 5).setPattern("ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 5).setPattern("ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 1).setPattern("ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 5).setPattern("ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(73);
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 1).setPattern("ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 5).setPattern("ZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 1).setPattern("ZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 5).setPattern("ZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 5).setPattern("ZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 5).setPattern("ZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(73);
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 1).setPattern("ZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 3, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 5).setPattern("ZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 11).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 51).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 59).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(73);
	private FixedLengthStringData filler120 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Sum Assured Reduced by Benefit Payment Amount  Non-Blank denotes YES");
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 72);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(28);
	private FixedLengthStringData filler121 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr530rec tr530rec = new Tr530rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr530pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr530rec.tr530Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo015.set(tr530rec.zragfr01);
		fieldNo025.set(tr530rec.zragfr02);
		fieldNo035.set(tr530rec.zragfr03);
		fieldNo045.set(tr530rec.zragfr04);
		fieldNo055.set(tr530rec.zragfr05);
		fieldNo065.set(tr530rec.zragfr06);
		fieldNo075.set(tr530rec.zragfr07);
		fieldNo085.set(tr530rec.zragfr08);
		fieldNo095.set(tr530rec.zragfr09);
		fieldNo105.set(tr530rec.zragfr10);
		fieldNo007.set(tr530rec.zrage01);
		fieldNo008.set(tr530rec.zrage02);
		fieldNo009.set(tr530rec.zrage03);
		fieldNo010.set(tr530rec.zrage04);
		fieldNo011.set(tr530rec.zrage05);
		fieldNo012.set(tr530rec.zrage06);
		fieldNo013.set(tr530rec.zrage07);
		fieldNo014.set(tr530rec.zrage08);
		fieldNo017.set(tr530rec.zrpcpd01);
		fieldNo018.set(tr530rec.zrpcpd02);
		fieldNo019.set(tr530rec.zrpcpd03);
		fieldNo020.set(tr530rec.zrpcpd04);
		fieldNo021.set(tr530rec.zrpcpd05);
		fieldNo022.set(tr530rec.zrpcpd06);
		fieldNo023.set(tr530rec.zrpcpd07);
		fieldNo024.set(tr530rec.zrpcpd08);
		fieldNo027.set(tr530rec.zrpcpd09);
		fieldNo028.set(tr530rec.zrpcpd10);
		fieldNo029.set(tr530rec.zrpcpd11);
		fieldNo030.set(tr530rec.zrpcpd12);
		fieldNo031.set(tr530rec.zrpcpd13);
		fieldNo032.set(tr530rec.zrpcpd14);
		fieldNo033.set(tr530rec.zrpcpd15);
		fieldNo034.set(tr530rec.zrpcpd16);
		fieldNo037.set(tr530rec.zrpcpd17);
		fieldNo038.set(tr530rec.zrpcpd18);
		fieldNo039.set(tr530rec.zrpcpd19);
		fieldNo040.set(tr530rec.zrpcpd20);
		fieldNo041.set(tr530rec.zrpcpd21);
		fieldNo042.set(tr530rec.zrpcpd22);
		fieldNo043.set(tr530rec.zrpcpd23);
		fieldNo044.set(tr530rec.zrpcpd24);
		fieldNo047.set(tr530rec.zrpcpd25);
		fieldNo048.set(tr530rec.zrpcpd26);
		fieldNo049.set(tr530rec.zrpcpd27);
		fieldNo050.set(tr530rec.zrpcpd28);
		fieldNo051.set(tr530rec.zrpcpd29);
		fieldNo052.set(tr530rec.zrpcpd30);
		fieldNo053.set(tr530rec.zrpcpd31);
		fieldNo054.set(tr530rec.zrpcpd32);
		fieldNo057.set(tr530rec.zrpcpd33);
		fieldNo058.set(tr530rec.zrpcpd34);
		fieldNo059.set(tr530rec.zrpcpd35);
		fieldNo060.set(tr530rec.zrpcpd36);
		fieldNo061.set(tr530rec.zrpcpd37);
		fieldNo062.set(tr530rec.zrpcpd38);
		fieldNo063.set(tr530rec.zrpcpd39);
		fieldNo064.set(tr530rec.zrpcpd40);
		fieldNo067.set(tr530rec.zrpcpd41);
		fieldNo068.set(tr530rec.zrpcpd42);
		fieldNo069.set(tr530rec.zrpcpd43);
		fieldNo070.set(tr530rec.zrpcpd44);
		fieldNo071.set(tr530rec.zrpcpd45);
		fieldNo072.set(tr530rec.zrpcpd46);
		fieldNo073.set(tr530rec.zrpcpd47);
		fieldNo074.set(tr530rec.zrpcpd48);
		fieldNo077.set(tr530rec.zrpcpd49);
		fieldNo078.set(tr530rec.zrpcpd50);
		fieldNo079.set(tr530rec.zrpcpd51);
		fieldNo080.set(tr530rec.zrpcpd52);
		fieldNo081.set(tr530rec.zrpcpd53);
		fieldNo082.set(tr530rec.zrpcpd54);
		fieldNo083.set(tr530rec.zrpcpd55);
		fieldNo084.set(tr530rec.zrpcpd56);
		fieldNo087.set(tr530rec.zrpcpd57);
		fieldNo088.set(tr530rec.zrpcpd58);
		fieldNo089.set(tr530rec.zrpcpd59);
		fieldNo090.set(tr530rec.zrpcpd60);
		fieldNo091.set(tr530rec.zrpcpd61);
		fieldNo092.set(tr530rec.zrpcpd62);
		fieldNo093.set(tr530rec.zrpcpd63);
		fieldNo094.set(tr530rec.zrpcpd64);
		fieldNo097.set(tr530rec.zrpcpd65);
		fieldNo098.set(tr530rec.zrpcpd66);
		fieldNo099.set(tr530rec.zrpcpd67);
		fieldNo100.set(tr530rec.zrpcpd68);
		fieldNo101.set(tr530rec.zrpcpd69);
		fieldNo102.set(tr530rec.zrpcpd70);
		fieldNo103.set(tr530rec.zrpcpd71);
		fieldNo104.set(tr530rec.zrpcpd72);
		fieldNo107.set(tr530rec.zrpcpd73);
		fieldNo108.set(tr530rec.zrpcpd74);
		fieldNo109.set(tr530rec.zrpcpd75);
		fieldNo110.set(tr530rec.zrpcpd76);
		fieldNo111.set(tr530rec.zrpcpd77);
		fieldNo112.set(tr530rec.zrpcpd78);
		fieldNo113.set(tr530rec.zrpcpd79);
		fieldNo114.set(tr530rec.zrpcpd80);
		fieldNo115.set(tr530rec.zredsumas);
		fieldNo016.set(tr530rec.zragto01);
		fieldNo026.set(tr530rec.zragto02);
		fieldNo036.set(tr530rec.zragto03);
		fieldNo046.set(tr530rec.zragto04);
		fieldNo056.set(tr530rec.zragto05);
		fieldNo066.set(tr530rec.zragto06);
		fieldNo076.set(tr530rec.zragto07);
		fieldNo086.set(tr530rec.zragto08);
		fieldNo096.set(tr530rec.zragto09);
		fieldNo106.set(tr530rec.zragto10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
