/*
 * File: Br621.java
 * Date: 29 August 2009 22:28:17
 * Author: Quipoz Limited
 * 
 * Class transformed from BR621.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.anticipatedendowment.dataaccess.ArlxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (For Anticipated Endowment Release Batch Processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Anticipated Endowmentment
* Payment Program, BR622.
* It is run in single-thread, and writes the selected ZRAE
* records to multiple members . Each of these members will be read
* by a copy of BR622 run in multi-thread environment.
*
* SQL will be used to access the ZRAE physical file. The
* splitter program will extract ZRAE records that meet the
* following criteria:
*
* i    Chdroy     =  <run-company>
* ii   Validflag  =  '1'
* iii  Payment Date  <>  0
* iv   Payment Date  <=  Batch Effective Date
* v    Chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
*      order by chdrcoy, chdrnum
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* A Splitter Program's purpose is to find a pending transactions f om
* the database and create multiple extract files for processing
* by multiple copies of the subsequent program, each running in it s
* own thread. Each subsequent program therefore processes a descre e
* portion of the total transactions.
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidily isolate potential transactions, not
* to process the transaction. The primary concern for Splitter
* program design is elasped time speed and this is acheived by
* minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF, (Create Temporary File), will create
* a duplicate of a physical file, (created under Smart and defined
* as a Query file), which will have as many members as is defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records evenly
* amoungst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
****************************************************************** ****
* </pre>
*/
public class Br621 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected boolean sqlerrorflag;
	protected java.sql.ResultSet sqlarlxCursorrs = null;
	private java.sql.PreparedStatement sqlarlxCursorps = null;
	private java.sql.Connection sqlarlxCursorconn = null;
	private String sqlarlxCursor = "";
	protected int arlxCursorLoopIndex = 0;
	protected DiskFileDAM arlx01 = new DiskFileDAM("ARLX01");
	protected DiskFileDAM arlx02 = new DiskFileDAM("ARLX02");
	protected DiskFileDAM arlx03 = new DiskFileDAM("ARLX03");
	protected DiskFileDAM arlx04 = new DiskFileDAM("ARLX04");
	protected DiskFileDAM arlx05 = new DiskFileDAM("ARLX05");
	protected DiskFileDAM arlx06 = new DiskFileDAM("ARLX06");
	protected DiskFileDAM arlx07 = new DiskFileDAM("ARLX07");
	protected DiskFileDAM arlx08 = new DiskFileDAM("ARLX08");
	protected DiskFileDAM arlx09 = new DiskFileDAM("ARLX09");
	protected DiskFileDAM arlx10 = new DiskFileDAM("ARLX10");
	protected DiskFileDAM arlx11 = new DiskFileDAM("ARLX11");
	protected DiskFileDAM arlx12 = new DiskFileDAM("ARLX12");
	protected DiskFileDAM arlx13 = new DiskFileDAM("ARLX13");
	protected DiskFileDAM arlx14 = new DiskFileDAM("ARLX14");
	protected DiskFileDAM arlx15 = new DiskFileDAM("ARLX15");
	protected DiskFileDAM arlx16 = new DiskFileDAM("ARLX16");
	protected DiskFileDAM arlx17 = new DiskFileDAM("ARLX17");
	protected DiskFileDAM arlx18 = new DiskFileDAM("ARLX18");
	protected DiskFileDAM arlx19 = new DiskFileDAM("ARLX19");
	protected DiskFileDAM arlx20 = new DiskFileDAM("ARLX20");
	private ArlxpfTableDAM arlxpf = new ArlxpfTableDAM();
	protected ArlxpfTableDAM arlxpfData = new ArlxpfTableDAM();
		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData arlx01Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx02Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx03Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx04Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx05Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx06Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx07Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx08Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx09Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx10Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx11Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx12Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx13Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx14Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx15Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx16Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx17Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx18Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx19Rec = new FixedLengthStringData(24);
	private FixedLengthStringData arlx20Rec = new FixedLengthStringData(24);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR621");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	protected FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	protected Validator firstTime = new Validator(wsaaFirstTime, "Y");

	protected FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	protected Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaArlxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaArlxFn, 0, FILLER).init("ARLX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaArlxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaArlxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	protected PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	protected ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaArlxData = FLSInittedArray (1000, 32);
	protected FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaArlxData, 0);
	protected FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaArlxData, 1);
	protected FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaArlxData, 9);
	protected FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaArlxData, 11);
	protected FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaArlxData, 13);
	protected PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaArlxData, 15);
	protected PackedDecimalData[] wsaaNpaydate = PDArrayPartOfArrayStructure(8, 0, wsaaArlxData, 18);
	protected FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaArlxData, 23);
	private FixedLengthStringData[] wsaaCownnum = FLSDArrayPartOfArrayStructure(8, wsaaArlxData, 24);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaArlxInd = FLSInittedArray (1000, 8);
	protected BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaArlxInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private ZonedDecimalData wsaaZeroes = new ZonedDecimalData(8, 0).init(ZERO);
	protected FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	protected static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	protected IntegerData wsaaInd = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private P6671par p6671par = new P6671par();

	public Br621() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		sqlarlxCursor = " SELECT  ZR.CHDRCOY, ZR.CHDRNUM, ZR.LIFE, ZR.COVERAGE, ZR.RIDER, ZR.PLNSFX, ZR.NPAYDATE, ZR.VALIDFLAG, CH.COWNNUM" +
" FROM   " + getAppVars().getTableNameOverriden("ZRAEPF") + " ZR " +
" INNER JOIN VM1DTA.CHDRPF CH ON ZR.CHDRCOY = CH.CHDRCOY AND ZR.CHDRNUM = CH.CHDRNUM AND ZR.VALIDFLAG = CH.VALIDFLAG " + 
" WHERE ZR.CHDRCOY = ?" +
" AND ZR.VALIDFLAG = ?" +
" AND ZR.NPAYDATE <> ?" +
" AND ZR.NPAYDATE <= ?" +
" AND ZR.CHDRNUM BETWEEN ? AND ?" +
" ORDER BY ZR.CHDRCOY, ZR.CHDRNUM, ZR.LIFE, ZR.COVERAGE, ZR.RIDER, ZR.PLNSFX, ZR.NPAYDATE";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlarlxCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.anticipatedendowment.dataaccess.ZraepfTableDAM());
			sqlarlxCursorps = getAppVars().prepareStatementEmbeded(sqlarlxCursorconn, sqlarlxCursor, "ZRAEPF");
			getAppVars().setDBString(sqlarlxCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlarlxCursorps, 2, wsaa1);
			getAppVars().setDBNumber(sqlarlxCursorps, 3, wsaaZeroes);
			getAppVars().setDBNumber(sqlarlxCursorps, 4, wsaaEffdate);
			getAppVars().setDBString(sqlarlxCursorps, 5, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlarlxCursorps, 6, wsaaChdrnumTo);
			sqlarlxCursorrs = getAppVars().executeQuery(sqlarlxCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaArlxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ARLX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaArlxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			arlx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			arlx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			arlx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			arlx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			arlx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			arlx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			arlx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			arlx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			arlx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			arlx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			arlx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			arlx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			arlx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			arlx14.openOutput();
		}
		if (isEQ(iz, 15)) {
			arlx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			arlx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			arlx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			arlx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			arlx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			arlx20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaNpaydate[wsaaInd.toInt()].set(ZERO);
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaValidflag[wsaaInd.toInt()].set(SPACES);
		wsaaCownnum[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFiles2010();
	}

protected void readFiles2010()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (arlxCursorLoopIndex = 1; isLT(arlxCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlarlxCursorrs); arlxCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlarlxCursorrs, 1, wsaaChdrcoy[arlxCursorLoopIndex], wsaaNullInd[arlxCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlarlxCursorrs, 2, wsaaChdrnum[arlxCursorLoopIndex], wsaaNullInd[arlxCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlarlxCursorrs, 3, wsaaLife[arlxCursorLoopIndex], wsaaNullInd[arlxCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlarlxCursorrs, 4, wsaaCoverage[arlxCursorLoopIndex], wsaaNullInd[arlxCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlarlxCursorrs, 5, wsaaRider[arlxCursorLoopIndex]);
				getAppVars().getDBObject(sqlarlxCursorrs, 6, wsaaPlnsfx[arlxCursorLoopIndex]);
				getAppVars().getDBObject(sqlarlxCursorrs, 7, wsaaNpaydate[arlxCursorLoopIndex]);
				getAppVars().getDBObject(sqlarlxCursorrs, 8, wsaaValidflag[arlxCursorLoopIndex]);
				getAppVars().getDBObject(sqlarlxCursorrs, 9, wsaaCownnum[arlxCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		//MIBT-110 STARTS
//		if (isEQ(getAppVars().getSqlErrorCode(), 100)
//		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
		if (sqlerrorflag || isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
		//MIBT-110 ENDS
			wsspEdterror.set(varcom.endp);
		}
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		/*  Check record is required for processsing but do not do any othe*/
		/*  reads to any other file. The exception to this rule are reads t*/
		/*  ITEM and these should be kept to a mimimum. Do not do any soft*/
		/*  locking in Splitter programs. The soft lock should be done by*/
		/*  the subsequent program which reads the temporary file.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/* In the update section we write a record to the temporary file*/
		/* member identified by the value of IY. If it is possible to*/
		/* to include the RRN from the primary file we should pass*/
		/* this data as the subsequent program can then use it to do*/
		/* a direct read which is faster than a normal read.*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next PAYX member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all ARLX data for the same contract until*/
		/*  the CHDRNUM on ARLX has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		arlxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		arlxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		arlxpfData.life.set(wsaaLife[wsaaInd.toInt()]);
		arlxpfData.coverage.set(wsaaCoverage[wsaaInd.toInt()]);
		arlxpfData.rider.set(wsaaRider[wsaaInd.toInt()]);
		arlxpfData.planSuffix.set(wsaaPlnsfx[wsaaInd.toInt()]);
		arlxpfData.nextPaydate.set(wsaaNpaydate[wsaaInd.toInt()]);
		arlxpfData.validflag.set(wsaaValidflag[wsaaInd.toInt()]);
		arlxpfData.cownnum.set(wsaaCownnum[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy, 1)) {
			arlx01.write(arlxpfData);
		}
		if (isEQ(iy, 2)) {
			arlx02.write(arlxpfData);
		}
		if (isEQ(iy, 3)) {
			arlx03.write(arlxpfData);
		}
		if (isEQ(iy, 4)) {
			arlx04.write(arlxpfData);
		}
		if (isEQ(iy, 5)) {
			arlx05.write(arlxpfData);
		}
		if (isEQ(iy, 6)) {
			arlx06.write(arlxpfData);
		}
		if (isEQ(iy, 7)) {
			arlx07.write(arlxpfData);
		}
		if (isEQ(iy, 8)) {
			arlx08.write(arlxpfData);
		}
		if (isEQ(iy, 9)) {
			arlx09.write(arlxpfData);
		}
		if (isEQ(iy, 10)) {
			arlx10.write(arlxpfData);
		}
		if (isEQ(iy, 11)) {
			arlx11.write(arlxpfData);
		}
		if (isEQ(iy, 12)) {
			arlx12.write(arlxpfData);
		}
		if (isEQ(iy, 13)) {
			arlx13.write(arlxpfData);
		}
		if (isEQ(iy, 14)) {
			arlx14.write(arlxpfData);
		}
		if (isEQ(iy, 15)) {
			arlx15.write(arlxpfData);
		}
		if (isEQ(iy, 16)) {
			arlx16.write(arlxpfData);
		}
		if (isEQ(iy, 17)) {
			arlx17.write(arlxpfData);
		}
		if (isEQ(iy, 18)) {
			arlx18.write(arlxpfData);
		}
		if (isEQ(iy, 19)) {
			arlx19.write(arlxpfData);
		}
		if (isEQ(iy, 20)) {
			arlx20.write(arlxpfData);
		}
		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/
		if (isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)
		&& isLTE(wsaaInd, wsaaRowsInBlock)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			arlx01.close();
		}
		if (isEQ(iz, 2)) {
			arlx02.close();
		}
		if (isEQ(iz, 3)) {
			arlx03.close();
		}
		if (isEQ(iz, 4)) {
			arlx04.close();
		}
		if (isEQ(iz, 5)) {
			arlx05.close();
		}
		if (isEQ(iz, 6)) {
			arlx06.close();
		}
		if (isEQ(iz, 7)) {
			arlx07.close();
		}
		if (isEQ(iz, 8)) {
			arlx08.close();
		}
		if (isEQ(iz, 9)) {
			arlx09.close();
		}
		if (isEQ(iz, 10)) {
			arlx10.close();
		}
		if (isEQ(iz, 11)) {
			arlx11.close();
		}
		if (isEQ(iz, 12)) {
			arlx12.close();
		}
		if (isEQ(iz, 13)) {
			arlx13.close();
		}
		if (isEQ(iz, 14)) {
			arlx14.close();
		}
		if (isEQ(iz, 15)) {
			arlx15.close();
		}
		if (isEQ(iz, 16)) {
			arlx16.close();
		}
		if (isEQ(iz, 17)) {
			arlx17.close();
		}
		if (isEQ(iz, 18)) {
			arlx18.close();
		}
		if (isEQ(iz, 19)) {
			arlx19.close();
		}
		if (isEQ(iz, 20)) {
			arlx20.close();
		}
	}
}
