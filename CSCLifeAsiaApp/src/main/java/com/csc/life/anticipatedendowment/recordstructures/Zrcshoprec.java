package com.csc.life.anticipatedendowment.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:20
 * Description:
 * Copybook name: ZRCSHOPREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrcshoprec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(223);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 0);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(rec, 4);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rec, 12);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rec, 13);
  	public FixedLengthStringData paycurr = new FixedLengthStringData(3).isAPartOf(rec, 21);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rec, 24);
  	public PackedDecimalData pymt = new PackedDecimalData(13, 2).isAPartOf(rec, 27);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rec, 34);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(rec, 37);
  	public FixedLengthStringData bankkey = new FixedLengthStringData(10).isAPartOf(rec, 67);
  	public FixedLengthStringData bankacckey = new FixedLengthStringData(20).isAPartOf(rec, 77);
  	public FixedLengthStringData frmGlDet = new FixedLengthStringData(37).isAPartOf(rec, 97);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(frmGlDet, 0);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(frmGlDet, 2);
  	public FixedLengthStringData glcode = new FixedLengthStringData(14).isAPartOf(frmGlDet, 4);
  	public FixedLengthStringData sign = new FixedLengthStringData(1).isAPartOf(frmGlDet, 18);
  	public FixedLengthStringData frmRldgacct = new FixedLengthStringData(16).isAPartOf(frmGlDet, 19);
  	public FixedLengthStringData cnttot = new FixedLengthStringData(2).isAPartOf(frmGlDet, 35);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(rec, 134);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(rec, 138);
  	public FixedLengthStringData reqntype = new FixedLengthStringData(1).isAPartOf(rec, 142);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(1).isAPartOf(rec, 143);
  	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(rec, 144);
  	public FixedLengthStringData bankcode = new FixedLengthStringData(2).isAPartOf(rec, 152);
  	public FixedLengthStringData cheqno = new FixedLengthStringData(9).isAPartOf(rec, 154);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(rec, 163);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(batckey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public FixedLengthStringData tranref = new FixedLengthStringData(30).isAPartOf(rec, 182);
  	public FixedLengthStringData errorFormat = new FixedLengthStringData(10).isAPartOf(rec, 212);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rec, 222);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}