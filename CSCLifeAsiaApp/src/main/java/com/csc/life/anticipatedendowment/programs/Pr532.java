/*
 * File: Pr532.java
 * Date: 30 August 2009 1:39:18
 * Author: Quipoz Limited
 *
 * Class transformed from PR532.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.dataaccess.ClbaddbTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
/*import com.csc.life.anticipatedendowment.dataaccess.dao.ZraepfDAO;*/
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.anticipatedendowment.screens.Sr532ScreenVars;
import com.csc.life.anticipatedendowment.tablestructures.Tr527rec;
import com.csc.life.anticipatedendowment.tablestructures.Tr528rec;
import com.csc.life.anticipatedendowment.tablestructures.Tr529rec;
import com.csc.life.anticipatedendowment.tablestructures.Tr530rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Confmsgrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Confmsg;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*   Anticipated Endowment Options.
*
*      This program allows us to change the Payment Options
*      of the Anticipated Endowment products and enter the
*      Client's Bank Account details (if it is required).
*
*
*   Overview.
*   ---------
*   Firstly, this program will retrieve the Contract Header
*   information which has been previously kept in the Sub
*   Menu program. Then it will read the Payments Due details
*   for that Contract from the ZRAEPF and hold it. Then it
*   displays all existing details regarding that Contract
*   on the screen.
*
*   Then it will prompt the user for the new Payment Options,
*   the Payee Number and the Payment Method for that Contract.
*
*   It will check the Payment Options entered against table
*   TR527 to see whether it is a valid Option for that Contract
*   Type.
*
*   It will also check that the Payee Number entered exist
*   in the system and still alive.
*
*   Then it will check the Payment Method entered against
*   table T3672 to see whether it is a valid Option.
*   If the Payment Method chosen requires Bank Account details
*   then it will force the Bank Details option field to be
*   selected and a pop up window will be displayed for the
*   user to enter the Bank Account details.
*   On the other hand, if Bank Account details is not required
*   for that Payment Method then the Bank Details option field
*   will be protected.
*
*   It will then create a PTRN record for the changes just have
*   been made and update the Contract Header record with the new
*   Transaction Number in it.
*
*   The current ZRAE record itself will be invalidated by giving
*   a valid flag '2' and a new ZRAE record will be created with
*   all the changes and the Transaction Number of the PTRN record
*   in it.
*
*   Note: The changes made will reflect only the remaining
*         (UnPaid) Payments Due.
*
*
*****************************************************************
* </pre>
*/
public class Pr532 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR532");
	private int wsaaSwitch = 0;
	private PackedDecimalData wsaaIdx = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaTot = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaLast = new PackedDecimalData(5, 0);
	private final int wsaaMax = 8;
	private PackedDecimalData wsaaTotal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
		/* WSAA-TEMP-DATA */
	private FixedLengthStringData wsaaOldPayee = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaOldBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaOldBankacckey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaNewPayee = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaNewBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaNewBankacckey = new FixedLengthStringData(20);
	private Isuallrec isuallrec = new Isuallrec();
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(35);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 8, FILLER).init("01");
	private static final String e058 = "E058";
	private static final String e186 = "E186";
	private static final String e190 = "E190";
	private static final String f782 = "F782";
	private static final String f982 = "F982";
	private static final String g599 = "G599";
	private static final String g900 = "G900";
	private static final String r005 = "R005";
	private static final String r006 = "R006";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t3672 = "T3672";
	private static final String tr527 = "TR527";
	private static final String tr528 = "TR528";
	private static final String t5688 = "T5688";
	private static final String descrec = "DESCREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String zraerec = "ZRAEREC";
	private static final String clrfrec = "CLRFREC";
	private static final String cltsrec = "CLTSREC";
	private static final String clbaddbrec = "CLBADDBREC";
	private static final String ptrnrec = "PTRNREC";
	private String covrrec = "COVRREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ClbaddbTableDAM clbaddbIO = new ClbaddbTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Batcuprec batcuprec1 = new Batcuprec(); 
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Confmsgrec confmsgrec = new Confmsgrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Tr527rec tr527rec = new Tr527rec();
	private Tr528rec tr528rec = new Tr528rec();
	private T3672rec t3672rec = new T3672rec();
	private Wssplife wssplife = new Wssplife();
	private Sr532ScreenVars sv = ScreenProgram.getScreenVars( Sr532ScreenVars.class);
	private Itempf itempf = null;
	private List<Itempf> itempflist = null;
	private boolean endowFlag = false;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	
	private Tr529rec tr529rec = new Tr529rec();
	private Tr530rec tr530rec = new Tr530rec();
	private String tr529 = "TR529";
	private String tr530 = "TR530";
	private Batckey wsaaBatchkeytemp = new Batckey();
	private static final String TA66 = "TA66";
	private static final String batcbatch = "00001";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaPcpdLoad = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaPcpdLoadDec = new PackedDecimalData(5, 2).init(0);
	private PackedDecimalData wsaaQuotient = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaRemainder = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaStartDate = new PackedDecimalData(8, 0);

		/* WSAA-FLAGS */
	private FixedLengthStringData wsaaTermFound = new FixedLengthStringData(1);
	private Validator termFound = new Validator(wsaaTermFound, "Y");

	private FixedLengthStringData wsaaAgeFound = new FixedLengthStringData(1);
	private Validator ageFound = new Validator(wsaaAgeFound, "Y");

	private FixedLengthStringData wsaaZeroPercent = new FixedLengthStringData(1);
	private Validator nonZeroPercent = new Validator(wsaaZeroPercent, "N");
		/* WSAA-SUBSCRIPTS */
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaNewSub = new ZonedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaHorz = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaVert = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaTermSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgeSub = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaZeroPays = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData pcpdArray = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaPercentage = FLSArrayPartOfStructure(10, 40, pcpdArray, 0);
	private ZonedDecimalData[][] wsaaPcpd = ZDArrayPartOfArrayStructure(8, 5, 0, wsaaPercentage, 0);
	private String h951 = "H951";
	private String liferec = "LIFEREC";
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private List<Itempf> itempftr527 =null;  	
  	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
  	
  	
  	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	private String waspayopt= "";
 	private static final String BNKDCRDT = "BNKDCRDT";
	private static final String ACCUINST = "ACCUINST";
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private int chdrTranno;
	private T5645rec t5645rec = new T5645rec();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
//	private Zraepf zraepf = new Zraepf(); 
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Zraepf> listZraepf = new ArrayList<Zraepf>();

	public Pr532() {
		super();
		screenVars = sv;
		new ScreenModel("Sr532", AppVars.getInstance(), sv);
	}
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit4999,
		exit4299,
		exit6690,
		errorProg610
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Skip this section if returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.optindOut[varcom.pr.toInt()].set("Y");
		sv.dataArea.set(SPACES);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		wsaaSwitch = 0;
		sv.paid01.set(ZERO);
		sv.paid02.set(ZERO);
		sv.paid03.set(ZERO);
		sv.paid04.set(ZERO);
		sv.paid05.set(ZERO);
		sv.paid06.set(ZERO);
		sv.paid07.set(ZERO);
		sv.paid08.set(ZERO);
		sv.paid09.set(ZERO);
		sv.prcnt01.set(ZERO);
		sv.prcnt02.set(ZERO);
		sv.prcnt03.set(ZERO);
		sv.prcnt04.set(ZERO);
		sv.prcnt05.set(ZERO);
		sv.prcnt06.set(ZERO);
		sv.prcnt07.set(ZERO);
		sv.prcnt08.set(ZERO);
		sv.zrdate01.set(ZERO);
		sv.zrdate02.set(ZERO);
		sv.zrdate03.set(ZERO);
		sv.zrdate04.set(ZERO);
		sv.zrdate05.set(ZERO);
		sv.zrdate06.set(ZERO);
		sv.zrdate07.set(ZERO);
		sv.zrdate08.set(ZERO);
		sv.zrdate09.set(ZERO);
		sv.zrpaydt01.set(ZERO);
		sv.zrpaydt02.set(ZERO);
		sv.zrpaydt03.set(ZERO);
		sv.zrpaydt04.set(ZERO);
		sv.zrpaydt05.set(ZERO);
		sv.zrpaydt06.set(ZERO);
		sv.zrpaydt07.set(ZERO);
		sv.zrpaydt08.set(ZERO);
		endowFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "PRPRO001", appVars, "IT");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		initOptswch1100();
		/* Retrieve and Release the contract header*/
		if (isEQ(wsspcomn.flag, "P") || isEQ(wsspcomn.flag, "X")) {
			
			covrpf = covrpfDAO.getCacheObject(covrpf);
		}
		
		if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {
			covtlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}

		chdrenqIO.setFunction(varcom.retrv);
		
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			if(isEQ(chdrenqIO.getStatuz(), varcom.mrnf)) {
				chdrpf = chdrDao.getCacheObject(chdrpf);
				if(chdrpf ==null){
					syserrrec.params.set(chdrenqIO.getParams());
					fatalError600();
				} else {
					chdrenqIO.setChdrcoy(chdrpf.getChdrcoy());
					chdrenqIO.setChdrnum(chdrpf.getChdrnum());
					chdrenqIO.setCntcurr(chdrpf.getCntcurr());
					chdrenqIO.setCnttype(chdrpf.getCnttype());
					chdrenqIO.setTranno(chdrpf.getTranno());
					chdrenqIO.setCownnum(chdrpf.getCownnum());
				}
			} else {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			
		} else {
			chdrenqIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
		}
		
		if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "P") || isEQ(wsspcomn.flag, "X")) {
			dealWithZraeRecords3000();
			loadScreenFields1300();
			readtr528();
		} else {
			if (isEQ(wsspcomn.flag, "M")) {
				readZraeWhileModify();
			} else if (isEQ(wsspcomn.flag, "I")) {
				readHoldZrae1200();	//IJS-555
			}
			loadScreenFields1300();
		}
		
		if(endowFlag){
			
		ChangePayMethod();
	    sv.paymmeth08Out[varcom.nd.toInt()].set("Y");
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645",wsaaProg.toString().trim());
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		acblenqListLPAE = acblpfDAO.getAcblenqRecord(
				wsspcomn.company.toString(), StringUtils.rightPad(chdrenqIO.getChdrnum().toString(), 16),t5645rec.sacscode[1].toString(),t5645rec.sacstype[1].toString());
 		acblenqListLPAS =acblpfDAO.getAcblenqRecord(
				wsspcomn.company.toString(), StringUtils.rightPad(chdrenqIO.getChdrnum().toString(), 16),t5645rec.sacscode[2].toString(),t5645rec.sacstype[2].toString());
		
		if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
			accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
			accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);	
		}
		if (acblenqListLPAS != null &&    !acblenqListLPAS.isEmpty()) {
			accAmtLPAS =  acblenqListLPAS.get(0).getSacscurbal();
			accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
			}
		chdrTranno = chdrenqIO.tranno.toInt() + 1;
		
		}
		
	}

protected void initOptswch1100()
	{
		start1110();
	}

protected void start1110()
	{
		/* Intialise Call to OPTSWCH, necessary when WHERE-NEXT*/
		/* section performs a call to OPTSWCH.*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsVrsnPrfx.set(SPACES);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optdsc.set(optswchrec.optsDsc[1]);
	}

protected void readHoldZrae1200()
	{
		start1210();
	}

protected void start1210()
	{
		zraeIO.setDataKey(SPACES);
		zraeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		zraeIO.setChdrnum(chdrenqIO.getChdrnum());
		zraeIO.setPlanSuffix(ZERO);
		zraeIO.setFunction(varcom.begnh);
		zraeIO.setFormat(zraerec);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)
		|| isEQ(zraeIO.getStatuz(), varcom.endp)
		|| isEQ(zraeIO.getValidflag(), "2")
		|| isNE(zraeIO.getChdrnum(), chdrenqIO.getChdrnum())) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
		/* Keep trying if ZRAE record wZRAEd is held by someone else.*/
		if (isEQ(zraeIO.getStatuz(), varcom.held)) {
			initOptswch1100();
			return ;
			/*****     GO TO 1110-START.                                        */
		}
		/* If Bank Account Details already exists for this record,*/
		/* then put an '+' indicator in the option switching*/
		/* otherwise put spaces there.*/
		if (isEQ(zraeIO.getBankkey(), SPACES)
		&& isEQ(zraeIO.getBankacckey(), SPACES)) {
			sv.optind.set(" ");
		}
		else {
			sv.optind.set("+");
		}
		/* Save current data from ZRAE record for rewriting purpose.*/
		wsaaOldPayee.set(zraeIO.getPayclt());
		wsaaOldBankkey.set(zraeIO.getBankkey());
		wsaaOldBankacckey.set(zraeIO.getBankacckey());
	}

protected void loadScreenFields1300()
	{
		start1310();
		clrfio1330();
		continue1350();
	}

protected void start1310()
	{
		sv.chdrcoy.set(zraeIO.getChdrcoy());
		sv.chdrnum.set(zraeIO.getChdrnum());
		sv.life.set(zraeIO.getLife());
		sv.coverage.set(zraeIO.getCoverage());
		sv.rider.set(zraeIO.getRider());
		sv.payeesel.set(zraeIO.getPayclt());
		sv.paycurr.set(zraeIO.getPaycurr());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		if(endowFlag){
			sv.actionflag.set("Y");
		}
		else{
			sv.actionflag.set("N");
		}
		/* Get Contract Type Description*/
		
		if (isNE(wsspcomn.flag, "C") || isNE(wsspcomn.flag, "P")) 
		{
		sv.payMethod.set(zraeIO.getPaymentMethod08());
		}
		
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setDesccoy(chdrenqIO.getChdrcoy());
		descIO.setItemseq(SPACES);
		descIO.setLanguage("E");
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.cntdesc.set(descIO.getLongdesc());
		/* Get LIFE Client Number*/
		clrfIO.setDataKey(SPACES);
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(zraeIO.getChdrcoy());
		wsaaChdrnum.set(zraeIO.getChdrnum());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("LF");
		clrfIO.setFormat(clrfrec);
		/*    MOVE READR                  TO CLRF-FUNCTION                 */
		clrfIO.setFunction(varcom.begn);
	}

protected void clrfio1330()
	{
		SmartFileCode.execute(appVars, clrfIO);
		/*    IF CLRF-STATUZ          NOT =  O-K AND MRNF                  */
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		/*    IF CLRF-STATUZ              = MRNF                           */
		/*        MOVE E058               TO SYSR-STATUZ.                  */
		if (isEQ(clrfIO.getStatuz(), varcom.oK)
		&& isEQ(clrfIO.getForepfx(), "CH")
		&& isEQ(clrfIO.getForecoy(), zraeIO.getChdrcoy())
		&& isEQ(clrfIO.getForenum(), wsaaForenum)
		&& isEQ(clrfIO.getClrrrole(), "LF")) {
			if (isNE(clrfIO.getUsedToBe(), SPACES)) {
				clrfIO.setFunction(varcom.nextr);
				clrfio1330();
				return ;
			}
		}
		/* Get LIFE Client Name*/
		namadrsrec.clntPrefix.set(clrfIO.getClntpfx());
		namadrsrec.clntCompany.set(clrfIO.getClntcoy());
		namadrsrec.clntNumber.set(clrfIO.getClntnum());
		namadrsrec.function.set("PYNMN");
		namadrsrec.language.set(wsspcomn.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.lifename.set(namadrsrec.name);
		/* If PAYEE Number already exist then get PAYEE Client Name.*/
		if (isEQ(zraeIO.getPayclt(), SPACES)) {
			sv.payeenme.set(SPACES);
			return ;
		}
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(zraeIO.getPayclt());
		namadrsrec.function.set("PYNMN");
		namadrsrec.language.set(wsspcomn.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.payeenme.set(namadrsrec.name);

	}

protected void readtr528(){

	itempflist = itemDAO.getAllitems("IT",wsspcomn.company.toString(),tr528);
	if(itempflist != null){
		for(Itempf itempf : itempflist){
			tr528rec.tr528Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if(isEQ(tr528rec.defaultSel,"Y")){
				if(sv.payOption09 != null)
					sv.payOption09.set(itempf.getItemitem());
				if(sv.payMethod != null)
					sv.payMethod.set(tr528rec.paymentMethod); 
				break;
			}
		}
		
	}
	
}


protected void ChangePayMethod()
{
	
	itempflist = itemDAO.getAllitems("IT",wsspcomn.company.toString(),tr528);
	if(itempflist != null){
		for(Itempf itempf : itempflist){
			tr528rec.tr528Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
		
			sv.mapLoccde.put(itempf.getItemitem(),tr528rec.paymentMethod.toString()); 
			
		}
		
	}

	

}

protected void continue1350()
	{
		sv.zrdate09.set(zraeIO.getNextPaydate());
		wsaaIdx.set(1);
		wsaaTotal.set(0);
		while ( !(isGT(wsaaIdx, wsaaMax)
		|| isEQ(zraeIO.getZrduedte(wsaaIdx), varcom.vrcmMaxDate))) {
			loadPaymentDetails1400();
		}
		sv.payOption09.set(waspayopt);
		sv.paid09.set(zraeIO.getTotamnt());
		compute(wsaaTot, 0).set(sub(wsaaIdx, 1));
		/*EXIT*/
	}

protected void loadPaymentDetails1400()
	{
		start1410();
	}

protected void start1410()
	{
		sv.zrdate[wsaaIdx.toInt()].set(zraeIO.getZrduedte(wsaaIdx));
		if (isEQ(zraeIO.getPaydte(wsaaIdx), varcom.vrcmMaxDate) || isEQ(zraeIO.getPaydte(wsaaIdx), ZERO)) {
			/*      MOVE SPACES             TO SR532-ZRPAYDT(WSAA-IDX)       */
			sv.zrpaydt[wsaaIdx.toInt()].set(ZERO);
		}
		else {
			wsaaTotal.add(zraeIO.getPaid(wsaaIdx));
			sv.paysts[wsaaIdx.toInt()].set("Y");
			sv.zrpaydt[wsaaIdx.toInt()].set(zraeIO.getPaydte(wsaaIdx));
			sv.paid[wsaaIdx.toInt()].set(zraeIO.getPaid(wsaaIdx));
		}
		sv.prcnt[wsaaIdx.toInt()].set(zraeIO.getPrcnt(wsaaIdx));
		sv.payOption[wsaaIdx.toInt()].set(zraeIO.getZrpayopt(wsaaIdx));
		waspayopt = zraeIO.getZrpayopt(wsaaIdx).toString();
		sv.paymentMethod[wsaaIdx.toInt()].set(zraeIO.getPaymmeth(wsaaIdx));
		
		
		
		
		wsaaIdx.add(1);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
	if (isEQ(wsspcomn.flag, "I")) {
		scrnparams.function.set(varcom.prot);
		}

		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning  from  an optional selection  */
		/* (current stack position action flag = '*').                  */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			/* Retrieve the changes that have been made in the Window       */
			/* program.                                                     */
			zraeIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(zraeIO.getParams());
				syserrrec.statuz.set(zraeIO.getStatuz());
				fatalError600();
			}
			if (isEQ(zraeIO.getBankkey(), SPACES)
			&& isEQ(zraeIO.getBankacckey(), SPACES)) {
				sv.optind.set(" ");
			}
			else {
				sv.optind.set("+");
			}
			zraeIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(zraeIO.getParams());
				syserrrec.statuz.set(zraeIO.getStatuz());
				fatalError600();
			}
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'SR532IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   SR532-DATA-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		sv.errorIndicators.set(SPACES);
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		/* Redisplay on CALC*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		validateOptions2100();
		//Ticket ILIFE-946 Fix (One line) - Updated if condition to invoke validation
		if (isNE(sv.payOption09, SPACES) || isNE(sv.payMethod, SPACES)) {
			validatePayee2200();
			validatePayMethod2300();
			checkClientsBank2400();
			checkPaymentCurrency2500();
			checkOptswch2600();
		}
		else {
			/* Get Next Payment Index.*/
			wsaaLast.set(1);
			while ( !(isEQ(zraeIO.getZrduedte(wsaaLast), zraeIO.getNextPaydate()))) {
				wsaaLast.add(1);
			}

			sv.payMethod.set(SPACES);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateOptions2100()
	{
		start2110();
	}

protected void start2110()
	{
		/* If new Payment Option is blank (spaces) then skip this*/
		/* validation.*/
		if (isEQ(sv.payOption09, SPACES)) {
			sv.pymdesc.set(SPACES);
			return ;
		}
		/* Get available Options for this Contract Type.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr527);
		itemIO.setItemitem(sv.cnttype);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.zrpayopt09Err.set(r006);
			sv.pymdesc.set(SPACES);
		}
		else {
			/* Check whether the Option is Valid for this Contract Type.*/
			tr527rec.tr527Rec.set(itemIO.getGenarea());
			sv.zrpayopt09Err.set(r005);
			for (wsaaIdx.set(1); !(isGT(wsaaIdx, 10)); wsaaIdx.add(1)){
				if (isEQ(tr527rec.payOption[wsaaIdx.toInt()], sv.payOption09)) {
					sv.zrpayopt09Err.set(SPACES);
					wsaaIdx.set(11);
				}
			}
			/* If Option is Valid then get the details from TR528.*/
			if (isEQ(sv.zrpayopt09Err, SPACES)) {
				itemIO.setItempfx("IT");
				itemIO.setItemcoy(wsspcomn.company);
				itemIO.setItemtabl(tr528);
				itemIO.setItemitem(sv.payOption09);
				itemIO.setItemseq(SPACES);
				itemIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(itemIO.getParams());
					syserrrec.statuz.set(itemIO.getStatuz());
					fatalError600();
				}
				else {
					tr528rec.tr528Rec.set(itemIO.getGenarea());
				}
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(tr528);
				descIO.setDescitem(sv.payOption09);
				descIO.setItemseq(SPACES);
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if (isNE(descIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(descIO.getParams());
					syserrrec.statuz.set(descIO.getStatuz());
					fatalError600();
				}
				else {
					sv.pymdesc.set(descIO.getLongdesc());
				}
			}
			/* Get Next Payment Index.*/
			wsaaLast.set(1);
			while ( !(isEQ(zraeIO.getZrduedte(wsaaLast), zraeIO.getNextPaydate()))) {
				wsaaLast.add(1);
			}

		}
	}

protected void validatePayee2200()
	{
		start2210();
	}

protected void start2210()
	{
		/* Invalidate blank fields when the chosen Payment Option*/
		/* requires the Payee and bank details.*/
		if (isEQ(sv.payeesel, SPACES)
		&& isEQ(tr528rec.bankaccreq, "Y")) {
			sv.payeeselErr.set(e186);
			sv.payeenme.set(SPACES);
			return ;
		}
		/* Still checking whether Client Number exist when it is*/
		/* entered although the Payment Option does not require one.*/
		if (isEQ(sv.payeesel, SPACES)) {
			return ;
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.payeesel);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* Check that the Payee is still alive*/
		if (isNE(cltsIO.getCltdod(), varcom.vrcmMaxDate)) {
			sv.payeeselErr.set(f782);
			return ;
		}
		/* If Client Number exists then get the Client Name, otherwise*/
		/* return an error message.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.payeeselErr.set(e058);
			sv.payeenme.set(SPACES);
		}
		else {
			namadrsrec.clntPrefix.set(cltsIO.getClntpfx());
			namadrsrec.clntCompany.set(cltsIO.getClntcoy());
			namadrsrec.clntNumber.set(cltsIO.getClntnum());
			namadrsrec.function.set("PYNMN");
			namadrsrec.language.set(wsspcomn.language);
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(namadrsrec.statuz);
				fatalError600();
			}
			sv.payeenme.set(namadrsrec.name);
		}
	}

protected void validatePayMethod2300()
	{
		start2310();
	}

protected void start2310()
	{
		/* Invalidate blank fields when the chosen Payment Method*/
		/* requires the Payee and bank details.*/
		if (isEQ(sv.payMethod, SPACES)
		&& isEQ(tr528rec.bankaccreq, "Y")) {
			sv.zrpaymmethErr.set(e186);
			sv.descrip.set(SPACES);
			return ;
		}
		/* Still checking whether the Payment Method chosen is valid*/
		/* although the Payment Option does not require one.*/
		if (isEQ(sv.payMethod, SPACES)) {
			return ;
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t3672);
		itemIO.setItemitem(sv.payMethod);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/* If Payment Method chosen is Valid then get the description,*/
		/* otherwise return an error message.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.zrpaymmethErr.set(e190);
			sv.descrip.set(SPACES);
		}
		else {
			t3672rec.t3672Rec.set(itemIO.getGenarea());
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(t3672);
			descIO.setDescitem(sv.payMethod);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				syserrrec.statuz.set(descIO.getStatuz());
				fatalError600();
			}
			else {
				sv.descrip.set(descIO.getLongdesc());
			}
		}
		/* Check whether Bank Account Details is required or not for*/
		/* this Payment Method. If it is required then force the*/
		/* selection of Bank Details option at least once to check*/
		/* whether the Bank Account details are correct. Otherwise*/
		/* protect the Bank Account Details option field.*/
		if (isEQ(t3672rec.bankaccreq, "Y")
		&& wsaaSwitch == 0) {
			wsaaSwitch = 1;
			sv.optind.set("X");
		}
		if (isEQ(t3672rec.bankaccreq, "N")) {
			if (isEQ(zraeIO.getBankkey(), SPACES)
			&& isEQ(zraeIO.getBankacckey(), SPACES)) {
				sv.optind.set(" ");
			}
			else {
				sv.optind.set("+");
			}
			sv.optindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void checkClientsBank2400()
	{
		start2410();
	}

protected void start2410()
	{
		/* Skip the following section when the Bank detail is blank*/
		/* and it is not required for this Payment Method.*/
		if (isEQ(t3672rec.bankaccreq, "N")
		&& isEQ(zraeIO.getBankkey(), SPACES)
		&& isEQ(zraeIO.getBankacckey(), SPACES)) {
			return ;
		}
		/* Give an error message when the Bank Account details is*/
		/* required but the user has not specified one and has not*/
		/* intended to give one.*/
		if (isEQ(t3672rec.bankaccreq, "Y")
		&& isEQ(zraeIO.getBankkey(), SPACES)
		&& isEQ(zraeIO.getBankacckey(), SPACES)
		&& isNE(sv.optind, "X")) {
			sv.optindErr.set(g900);
			return ;
		}
		/* Check to see that the Bank Account details is for the*/
		/* correct Payee. (It is possible that the Bank Account*/
		/* details already exist and the user changes the Payee*/
		/* without changing the Bank Account details)*/
		if (isEQ(zraeIO.getBankkey(), SPACES)
		&& isEQ(zraeIO.getBankacckey(), SPACES)) {
			return ;
		}
		clbaddbIO.setDataKey(SPACES);
		clbaddbIO.setBankkey(zraeIO.getBankkey());
		clbaddbIO.setBankacckey(zraeIO.getBankacckey());
		clbaddbIO.setClntcoy(wsspcomn.fsuco);
		clbaddbIO.setClntnum(sv.payeesel);
		clbaddbIO.setFormat(clbaddbrec);
		clbaddbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clbaddbIO);
		if (isNE(clbaddbIO.getStatuz(), varcom.oK)
		&& isNE(clbaddbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clbaddbIO.getParams());
			syserrrec.statuz.set(clbaddbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clbaddbIO.getStatuz(), varcom.mrnf)
		&& isNE(sv.optind, "X")) {
			sv.optindErr.set(g599);
		}
	}

protected void checkPaymentCurrency2500()
	{
		start2510();
	}

protected void start2510()
	{
		/* Invalidate blank fields when the chosen Payment Method*/
		/* requires the Payee and bank details.*/
		if (isEQ(sv.paycurr, SPACES)
		&& isEQ(tr528rec.bankaccreq, "Y")) {
			sv.paycurrErr.set(e186);
			sv.currdesc.set(SPACES);
			return ;
		}
		/* Still checking whether the Payment Currency chosen is valid*/
		/* although the Payment Option does not require one.*/
		if (isEQ(sv.paycurr, SPACES)) {
			return ;
		}
		/* Check Payment Currency entered by the user against Table*/
		/* T3629 for valid Currency.*/
		/* If it is a Valid one then get the Currency description,*/
		/* otherwise display Error message.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(sv.paycurr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.paycurrErr.set(f982);
			sv.currdesc.set(SPACES);
		}
		else {
			descIO.setDescpfx("IT");
			descIO.setDesctabl(t3629);
			descIO.setDescitem(sv.paycurr);
			descIO.setDesccoy(wsspcomn.company);
			descIO.setItemseq(SPACES);
			descIO.setLanguage("E");
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				syserrrec.statuz.set(descIO.getStatuz());
				fatalError600();
			}
			sv.currdesc.set(descIO.getLongdesc());
		}
	}

protected void checkOptswch2600()
	{
		start2610();
	}

protected void start2610()
	{
		/* Validation of the OPTSWITCH function. The 'CHCK' function*/
		/* validates the selections made on the Calling Screen.*/
		if (isNE(sv.optind, SPACES)) {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelCode.set("CBOX");
			optswchrec.optsInd[1].set(sv.optind);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz, varcom.oK)) {
				sv.optindErr.set(optswchrec.optsStatuz);
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
	if (isEQ(wsspcomn.flag, "I")) {
		return;
	}
		updateDatabase3010();
		
	}

protected void updateDatabase3010()
	{
		zraeIO.setPayclt(sv.payeesel);
		zraeIO.setPaycurr(sv.paycurr);
		/* If Bank Details Option fields is selected then KEEPS the*/
		/* ZRAE record so that the window program (P6I47) can access*/
		/* it and skip the updating section.*/
		if (isEQ(sv.optind, "X")) {
			zraeIO.setFormat(zraerec);
			zraeIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, zraeIO);
			if (isNE(zraeIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(zraeIO.getParams());
				syserrrec.statuz.set(zraeIO.getStatuz());
				fatalError600();
			}
			return ;
		}
		/* If returning from an optional selection (the current stack*/
		/* position action flag = '*') then skip the updating section.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
	
		if (isEQ(wsspcomn.flag, "X") && isNE(wsspcomn.sbmaction, "A")) {
			updateContractHeader3100();
			writePtrnBatcup3200();
		}
		updateZraeRec3300();
		if (isEQ(wsspcomn.flag, "X")) {
			releaseSoftlock3400();
		}
		appVars.commit();
		/* Setup the appropriate Confirmation message.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(scrnparams.statuz, varcom.subm)) {
			confmsgrec.type.set("A");
		}
		else {
			confmsgrec.type.set("C");
		}
		confmsgrec.language.set(wsspcomn.language);
		confmsgrec.transaction.set("T000");
		callProgram(Confmsg.class, confmsgrec.confmsgRec);
		if (isEQ(confmsgrec.statuz, varcom.oK)) {
			wsspcomn.msgarea.set(confmsgrec.message);
		}
		else {
			wsspcomn.msgarea.set("CMESSAGE ERROR");
		}
	}

protected void updateContractHeader3100()
	{
	
		readHoldChdr3110();
		rewriteChdr3150();
	
	
		
	}

protected void readHoldChdr3110()
	{
		chdrenqIO.setDataKey(SPACES);
		chdrenqIO.setChdrcoy(zraeIO.getChdrcoy());
		chdrenqIO.setChdrnum(zraeIO.getChdrnum());
		chdrenqIO.setFunction(varcom.readh);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
	}

protected void rewriteChdr3150()
	{
		setPrecision(chdrenqIO.getTranno(), 0);
		chdrenqIO.setTranno(add(chdrenqIO.getTranno(), 1));
		chdrenqIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writePtrnBatcup3200()
	{
		writePtrn3200();
		updateBatchHeader3250();
	}

protected void writePtrn3200()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
	
		
		ptrnIO.setTranno(chdrenqIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrenqIO.getChdrnum());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setCrtuser(wsspcomn.userid); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
	}

protected void updateBatchHeader3250()
	{
		batcuprec1.batcupRec.set(SPACES);
		batcuprec1.batchkey.set(wsspcomn.batchkey);
		batcuprec1.trancnt.set(1);
		batcuprec1.etreqcnt.set(ZERO);
		batcuprec1.sub.set(ZERO);
		batcuprec1.bcnt.set(ZERO);
		batcuprec1.bval.set(ZERO);
		batcuprec1.ascnt.set(ZERO);
		batcuprec1.statuz.set(ZERO);
		batcuprec1.function.set(SPACES);
		callProgram(Batcup.class, batcuprec1.batcupRec);
		if (isNE(batcuprec1.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec1.batcupRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateZraeRec3300()
	{
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "P")|| isEQ(wsspcomn.flag, "X"))
	{
		writeNewZrae3350();
		return;		
	
	}
	if(!(isEQ(wsspcomn.flag, "M") && listZraepf.isEmpty())) {
		rewriteOldZrae3310();
	}
			writeNewZrae3350();
			
			if (isEQ(wsspcomn.flag, "X"))
			{
			if(endowFlag && waspayopt.equalsIgnoreCase(ACCUINST) && isEQ(sv.payOption09, BNKDCRDT)){//ICIL-749
				calculateInterest();
				postings360();
				updateZrae();
			}
			}
			
	}

protected void rewriteOldZrae3310()
	{
		/* Save the changed data from ZRAE record and restore the old*/
		/* data for rewriting purpose.*/
		wsaaNewPayee.set(zraeIO.getPayclt());
		wsaaNewBankkey.set(zraeIO.getBankkey());
		wsaaNewBankacckey.set(zraeIO.getBankacckey());
		zraeIO.setPayclt(wsaaOldPayee);
		zraeIO.setBankkey(wsaaOldBankkey);
		zraeIO.setBankacckey(wsaaOldBankacckey);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaToday);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callDatcon25000();
		zraeIO.setCurrto(datcon2rec.intDate2);
		zraeIO.setValidflag("2");
		zraeIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
	}

protected void writeNewZrae3350()
	{
		/* Restore the changed data of ZRAE record to be written.*/
		/* MOVE SR532-PAYCURR          TO ZRAE-PAYCURR.*/
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "P")|| isEQ(wsspcomn.flag, "X")) {
		
		wsaaNewPayee.set(zraeIO.getPayclt());
	}
		zraeIO.setPayclt(wsaaNewPayee);
		zraeIO.setBankkey(wsaaNewBankkey);
		zraeIO.setBankacckey(wsaaNewBankacckey);
		/* Update all the changes on the Payment Options and Payment*/
		/* Method for all the remaining Payments Due.*/
		for (wsaaIdx.set(wsaaLast); !(isGT(wsaaIdx, wsaaTot)); wsaaIdx.add(1)){
			zraeIO.setZrpayopt(wsaaIdx, sv.payOption09);
			zraeIO.setPaymmeth(wsaaIdx, sv.payMethod);
		}
		zraeIO.setTranno(chdrenqIO.getTranno());
		if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "P")|| isEQ(wsspcomn.flag, "X"))
		{
			zraeIO.setTranno(1);
		}
		
		zraeIO.setCurrfrom(wsaaToday);
		zraeIO.setCurrto(99999999);
		zraeIO.setValidflag("1");
		if(endowFlag){ 
		zraeIO.setFlag("1");
		}
		zraeIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			fatalError600();
		}
	}

protected void releaseSoftlock3400()
	{
		start3410();
	}

protected void start3410()
	{
		/* Release SOFTLOCK on Contract Header of this contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrenqIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		/* On returning to the program, reset OPTSWITCH and recalculat*/
		/* the benefit payable.  Set the manual adjustment flag if the*/
		/* linkage flag is set to 'N', otherwise display the hidden*/
		/* field and literal if the benefit payable had changed.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			optswchCall4100();
			/* To make it go back to the Transaction program again.*/
			/* Remove the following line to make it go back to the*/
			/* Sub Menu program directly.*/
			return ;
		}
		wsspcomn.nextprog.set(wsaaProg);
		/* If the user has requested to look at Bank Account details,*/
		/* use OPTSWITCH to switch to P6I47.*/
		if (isEQ(sv.optind, "X")) {
			optswchCall4100();
			sv.optind.set(optswchrec.optsInd[1]);
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void optswchCall4100()
	{
		call4110();
	}

protected void call4110()
	{
		/* The 'STCK' function saves the original switching stack and*/
		/* replaces it with the stack specified on T1661.*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void callDatcon25000()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}




protected void dealWithZraeRecords3000()
{
	/*START*/
	tableReads4000();
	writeZraeRecord5000();
	/*EXIT*/
}

protected void tableReads4000()
{
	try {
		start4001();
	}
	catch (GOTOException e){
	}
}

protected void start4001()
{

	itempflist=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),tr527, chdrenqIO.getCnttype().toString());
	
	if(itempflist.size() ==0)
	{
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("tr527").concat(chdrenqIO.getCnttype().toString()));
		fatalError600();
	}
	else
	{
		tr527rec.tr527Rec.set(StringUtil.rawToString(itempflist.get(0).getGenarea()));

	}
	
	
	if (isNE(tr527rec.payOption05,SPACES)) {

		
		itempflist=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),tr528, tr527rec.payOption05.toString());
		
		if(itempflist.size() ==0)
		{
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("tr528").concat(tr527rec.payOption05.toString()));
			fatalError600();	
		}
		else
		{
			tr528rec.tr528Rec.set(StringUtil.rawToString(itempflist.get(0).getGenarea()));
		}
		
		
	}
	else {
		tr528rec.paymentMethod.set(SPACES);
	}
	
	if(isEQ(wsspcomn.flag, "P") || isEQ(wsspcomn.flag, "X")){

		itempflist=itempfDAO.getAllItemitemByDateFrm("IT",wsspcomn.company.toString(),tr529, covrpf.getCrtable(),wsaaToday.toInt());

		if(itempflist.size() ==0)
		{
			itempflist=itempfDAO.getAllItemitemByDateFrm("IT",wsspcomn.company.toString(),tr530, covrpf.getCrtable(),wsaaToday.toInt());

			if(itempflist.size() ==0)
			{
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("tr530").concat(covrpf.getCrtable()));
				fatalError600();
			}else{


				tr530rec.tr530Rec.set(StringUtil.rawToString(itempflist.get(0).getGenarea()));
				loadTr530Array4200();

			}

		}
		else {
			tr529rec.tr529Rec.set(StringUtil.rawToString(itempflist.get(0).getGenarea()));
			loadTr529Array4100();
			goTo(GotoLabel.exit4999);
		}
	}
	else{
		itempflist=itempfDAO.getAllItemitemByDateFrm("IT",wsspcomn.company.toString(),tr529, covtlnbIO.getCrtable().toString(),wsaaToday.toInt());

		if(itempflist.size() ==0)
		{
			itempflist=itempfDAO.getAllItemitemByDateFrm("IT",wsspcomn.company.toString(),tr530, covtlnbIO.getCrtable().toString(),wsaaToday.toInt());

			if(itempflist.size() ==0)
			{
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("tr530").concat(covtlnbIO.getCrtable().toString()));
				fatalError600();
			}else{


				tr530rec.tr530Rec.set(StringUtil.rawToString(itempflist.get(0).getGenarea()));
				loadTr530Array4200();

			}

		}
		else {
			tr529rec.tr529Rec.set(StringUtil.rawToString(itempflist.get(0).getGenarea()));
			loadTr529Array4100();
			goTo(GotoLabel.exit4999);
		}
	}
	
	
}

protected void loadTr529Array4100()
{
	start4101();
}

protected void start4101()
{
	wsaaVert.set(1);
	wsaaHorz.set(1);
	for (wsaaPcpdLoad.set(1); !(isGT(wsaaPcpdLoad,80)); wsaaPcpdLoad.add(1)){
		wsaaPcpd[wsaaVert.toInt()][wsaaHorz.toInt()].set(tr529rec.zrpcpd[wsaaPcpdLoad.toInt()]);
		wsaaHorz.add(1);
		wsaaPcpdLoadDec.set(wsaaPcpdLoad);
		compute(wsaaQuotient, 2).setDivide(wsaaPcpdLoadDec, (8));
		wsaaRemainder.setRemainder(wsaaQuotient);
		if (isEQ(wsaaRemainder,ZERO)) {
			wsaaVert.add(1);
			wsaaHorz.set(1);
		}
	}
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {
		for (wsaaTermSub.set(1); !(isGT(wsaaTermSub,10)
				|| termFound.isTrue()); wsaaTermSub.add(1)){
			if (isGTE(tr529rec.zrterm[wsaaTermSub.toInt()],covtlnbIO.getRiskCessTerm())) {
				wsaaTermFound.set("Y");
				wsaaTermSub.subtract(1);
			}
		}
	}
	else{
		for (wsaaTermSub.set(1); !(isGT(wsaaTermSub,10)
				|| termFound.isTrue()); wsaaTermSub.add(1)){
			if (isGTE(tr529rec.zrterm[wsaaTermSub.toInt()],covrpf.getRiskCessTerm())) {
				wsaaTermFound.set("Y");
				wsaaTermSub.subtract(1);
			}
		}	
	}
	if (isNE(wsaaTermFound,"Y")) {
		
		syserrrec.statuz.set(h951);
		fatalError600();
	}
	wsaaStartDate.set(wsaaToday);
	
	for (wsaaHorz.set(1); !(isGT(wsaaHorz,8)); wsaaHorz.add(1)){
		zraeIO.setPrcnt(wsaaHorz, wsaaPcpd[wsaaTermSub.toInt()][wsaaHorz.toInt()]);
		zraeIO.setPaydte(wsaaHorz, varcom.vrcmMaxDate);
		zraeIO.setPaid(wsaaHorz, ZERO);
		zraeIO.setPaymmeth(wsaaHorz, tr528rec.paymentMethod);
		zraeIO.setZrpayopt(wsaaHorz, tr527rec.payOption05);
		if (isEQ(tr529rec.zrnoyrs[wsaaHorz.toInt()],0)) {
			zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
		}
		else {
			datcon2rec.freqFactor.set(tr529rec.zrnoyrs[wsaaHorz.toInt()]);
			callDatcon24300();
		}
	}
}

protected void loadTr530Array4200()
{
	try {
		start4201();
		checkDates4250();
	}
	catch (GOTOException e){
	}
}

protected void start4201()
{
	wsaaVert.set(1);
	wsaaHorz.set(1);
	for (wsaaPcpdLoad.set(1); !(isGT(wsaaPcpdLoad,80)); wsaaPcpdLoad.add(1)){
		wsaaPcpd[wsaaVert.toInt()][wsaaHorz.toInt()].set(tr530rec.zrpcpd[wsaaPcpdLoad.toInt()]);
		wsaaHorz.add(1);
		wsaaPcpdLoadDec.set(wsaaPcpdLoad);
		compute(wsaaQuotient, 2).setDivide(wsaaPcpdLoadDec, (8));
		wsaaRemainder.setRemainder(wsaaQuotient);
		if (isEQ(wsaaRemainder,ZERO)) {
			wsaaVert.add(1);
			wsaaHorz.set(1);
		}
	}
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {

		for (wsaaAgeSub.set(1); !(isGT(wsaaAgeSub,10)
				|| ageFound.isTrue()); wsaaAgeSub.add(1)){
			if (isGTE(covtlnbIO.getAnbAtCcd01(),tr530rec.zragfr[wsaaAgeSub.toInt()])
					&& isLTE(covtlnbIO.getAnbAtCcd01(),tr530rec.zragto[wsaaAgeSub.toInt()])) {
				wsaaAgeFound.set("Y");
				wsaaAgeSub.subtract(1);
			}
		}
		readLifeCovt();
	}


	else{
		for (wsaaAgeSub.set(1); !(isGT(wsaaAgeSub,10)
				|| ageFound.isTrue()); wsaaAgeSub.add(1)){
			if (isGTE(covrpf.getAnbAtCcd(),tr530rec.zragfr[wsaaAgeSub.toInt()])
					&& isLTE(covrpf.getAnbAtCcd(),tr530rec.zragto[wsaaAgeSub.toInt()])) {
				wsaaAgeFound.set("Y");
				wsaaAgeSub.subtract(1);
			}
		}
		readLife4400();
	}
	wsaaStartDate.set(lifeIO.getCltdob());
	for (wsaaHorz.set(1); !(isGT(wsaaHorz,8)); wsaaHorz.add(1)){
		zraeIO.setPrcnt(wsaaHorz, wsaaPcpd[wsaaAgeSub.toInt()][wsaaHorz.toInt()]);
		zraeIO.setPaydte(wsaaHorz, varcom.vrcmMaxDate);
		zraeIO.setPaid(wsaaHorz, ZERO);
		zraeIO.setPaymmeth(wsaaHorz, SPACES);
		zraeIO.setZrpayopt(wsaaHorz, tr527rec.payOption05);
		if (isEQ(tr530rec.zrage[wsaaHorz.toInt()],ZERO)) {
			zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
		}
		else {
			datcon2rec.freqFactor.set(tr530rec.zrage[wsaaHorz.toInt()]);
			callDatcon24300();
		}
	}
}

protected void checkDates4250()
{
	wsaaZeroPercent.set("Y");
	for (wsaaSub.set(1); !(isGT(wsaaSub,8)
	|| nonZeroPercent.isTrue()); wsaaSub.add(1)){
		if (isNE(zraeIO.getPrcnt(wsaaSub),ZERO)) {
			wsaaZeroPercent.set("N");
			wsaaSub.subtract(1);
		}
	}
	if (isEQ(wsaaSub,1)) {
		goTo(GotoLabel.exit4299);
	}
	compute(wsaaZeroPays, 0).set(sub(wsaaSub,1));
	while ( !(isGT(wsaaSub,8))) {
		compute(wsaaNewSub, 0).set(sub(wsaaSub,wsaaZeroPays));
		zraeIO.setPrcnt(wsaaNewSub, zraeIO.getPrcnt(wsaaSub));
		zraeIO.setZrduedte(wsaaNewSub, zraeIO.getZrduedte(wsaaSub));
		if (isEQ(wsaaSub,8)) {
			wsaaNewSub.add(1);
			while ( !(isGT(wsaaNewSub,8))) {
				zraeIO.setZrduedte(wsaaSub, varcom.vrcmMaxDate);
				zraeIO.setPrcnt(wsaaSub, ZERO);
				wsaaNewSub.add(1);
			}

		}
		wsaaSub.add(1);
	}

}

protected void callDatcon24300()
{
	/*START*/
	datcon2rec.intDate1.set(wsaaStartDate);
	datcon2rec.frequency.set("01");
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {
	if (isGT(datcon2rec.intDate2,covtlnbIO.getRiskCessDate())) {
		zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
	}
	else {
		zraeIO.setZrduedte(wsaaHorz, datcon2rec.intDate2);
	}
	}
	else{
		if (isGT(datcon2rec.intDate2,covrpf.getRiskCessDate())) {
			zraeIO.setZrduedte(wsaaHorz, varcom.vrcmMaxDate);
		}
		else {
			zraeIO.setZrduedte(wsaaHorz, datcon2rec.intDate2);
		}
	}
}

protected void readLife4400()
{
	start4401();
}

protected void start4401()
{
	lifeIO.setParams(SPACES);
	lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
	lifeIO.setChdrnum(chdrenqIO.getChdrnum());
	lifeIO.setLife(covrpf.getLife());
	lifeIO.setJlife(covrpf.getJlife());
	lifeIO.setCurrfrom(varcom.vrcmMaxDate);
	lifeIO.setFunction(varcom.begn);
	lifeIO.setFormat(liferec);
	SmartFileCode.execute(appVars, lifeIO);
	if (isNE(lifeIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lifeIO.getParams());
		syserrrec.statuz.set(lifeIO.getStatuz());
		fatalError600();
	}
}

protected void readLifeCovt()
{
	lifeIO.setParams(SPACES);
	lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
	lifeIO.setChdrnum(chdrenqIO.getChdrnum());
	lifeIO.setLife(covtlnbIO.getLife());
	lifeIO.setJlife(covtlnbIO.getJlife());
	lifeIO.setCurrfrom(varcom.vrcmMaxDate);
	lifeIO.setFunction(varcom.begn);
	lifeIO.setFormat(liferec);
	SmartFileCode.execute(appVars, lifeIO);
	if (isNE(lifeIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lifeIO.getParams());
		syserrrec.statuz.set(lifeIO.getStatuz());
		fatalError600();
	}
}

protected void writeZraeRecord5000()
{
	start5001();
}

protected void start5001()
{
	zraeIO.setChdrcoy(wsspcomn.company);
	zraeIO.setChdrnum(chdrenqIO.getChdrnum());
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {
		zraeIO.setLife(covtlnbIO.getLife());
		zraeIO.setCoverage(covtlnbIO.getCoverage());
		zraeIO.setRider(covtlnbIO.getRider());
	}
	else{
		zraeIO.setLife(covrpf.getLife());
		zraeIO.setCoverage(covrpf.getCoverage());
		zraeIO.setRider(covrpf.getRider());	
	}
	zraeIO.setPlanSuffix("0");
	zraeIO.setValidflag("1");
	zraeIO.setTranno(chdrenqIO.getTranno());
	zraeIO.setCurrfrom(wsaaToday);
	zraeIO.setCurrto(varcom.vrcmMaxDate);
	zraeIO.setTotamnt(ZERO);
	zraeIO.setNextPaydate(zraeIO.getZrduedte01());
	zraeIO.setPaycurr(chdrenqIO.getCntcurr());
	zraeIO.setPayclt(chdrenqIO.getCownnum()); 
	zraeIO.setFunction(varcom.writr);
	zraeIO.setFormat(zraerec);

}



protected void calculateInterest(){/* Set up INTCALC linkage and call...*/
	/* Don't forget that the value returned is in LOAN currency.*/
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(zraeIO.getChdrcoy());
	annypyintcalcrec.chdrnum.set(zraeIO.getChdrnum());
	annypyintcalcrec.cnttype.set(chdrenqIO.getCnttype());
	annypyintcalcrec.interestTo.set(new BigDecimal(getCurrentBusinessDate()));
	annypyintcalcrec.interestFrom.set(zraeIO.getAplstintbdte()); 
	annypyintcalcrec.annPaymt.set(zraeIO.getApcaplamt());
	annypyintcalcrec.lastCaplsnDate.set(zraeIO.getAplstcapdate());		
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(zraeIO.getPaycurr());
	annypyintcalcrec.transaction.set("ENDOWMENT");
	
	callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
	if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(annypyintcalcrec.intcalcRec);
		syserrrec.statuz.set(annypyintcalcrec.statuz);
		fatalError600();	
	}
	/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 5000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5000();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}}



protected void updateZrae()
{
    List<Zraepf> zraepfList;
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(chdrenqIO.getChdrnum().toString());
    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, chdrenqIO.getChdrcoy().toString());
    
    if(zraepfList!= null && !zraepfList.isEmpty()){
    	for(Zraepf Zraepftmp : zraepfList){
    		Zraepftmp.setApcaplamt(new BigDecimal(0));
    		Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
    		Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
    		Zraepftmp.setFlag("2");
    		Zraepftmp.setApintamt(BigDecimal.ZERO);
    		Zraepftmp.setTotamnt(0);
    	}
     }
    zraepfDAO.updateZraeRecCapDate(zraepfList);	
}


protected void postings360(){
	
	lifacmvrec1.lifacmvRec.set(SPACES);
	wsaaRldgacct.set(SPACES);
	wsaaSequenceNo.set(ZERO);
	lifacmvrec1.function.set("PSTW");
	/* Read T1688 for transaction code description.                 */
	descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	if(descpf!=null)
	lifacmvrec1.trandesc.set(descpf.getLongdesc());
	wsaaBatckey.set(wsspcomn.batchkey);
	lifacmvrec1.batccoy.set(wsaaBatckey.batcBatccoy);
	lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
	lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
	lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
	lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
	lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
	lifacmvrec1.rdocnum.set(chdrenqIO.getChdrnum());
	compute(lifacmvrec1.tranno, 0).set(chdrTranno);
	lifacmvrec1.rldgcoy.set(chdrenqIO.getChdrcoy());
	lifacmvrec1.origcurr.set(zraeIO.getPaycurr());
	lifacmvrec1.tranref.set(chdrenqIO.getChdrnum());
	lifacmvrec1.crate.set(ZERO);
	lifacmvrec1.genlcoy.set(chdrenqIO.getChdrcoy());
	lifacmvrec1.genlcur.set(zraeIO.getPaycurr());
	lifacmvrec1.effdate.set(datcon1rec.intDate.toInt());
	lifacmvrec1.rcamt.set(ZERO);
	lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec1.transactionDate.set(ZERO);
	lifacmvrec1.transactionTime.set(ZERO);
	lifacmvrec1.user.set(varcom.vrcmUser);
	lifacmvrec1.termid.set(varcom.vrcmTermid);
	
	lifacmvrec1.rldgacct.set(chdrenqIO.getChdrnum());
	lifacmvrec1.sacscode.set(t5645rec.sacscode[1]);// LP AE
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[1]);
	lifacmvrec1.glcode.set(t5645rec.glmap[1]);
	lifacmvrec1.glsign.set(t5645rec.sign[1]);
	lifacmvrec1.contot.set(t5645rec.cnttot[1]);
	lifacmvrec1.origamt.set(accAmtLPAE); 
	lifacmvrec1.acctamt.set(accAmtLPAE);
 	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
		return ; 
	}
	lifacmvrec1.sacscode.set(t5645rec.sacscode[2]);// LP AS
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[2]);
	lifacmvrec1.glcode.set(t5645rec.glmap[2]);
	lifacmvrec1.glsign.set(t5645rec.sign[2]);
	lifacmvrec1.contot.set(t5645rec.cnttot[2]);
	accAmtLPAS=	add(accAmtLPAS,interestAmount).getbigdata();
	lifacmvrec1.origamt.set(accAmtLPAS);
	lifacmvrec1.acctamt.set(accAmtLPAS);
	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
	}
	
	lifacmvrec1.sacscode.set(t5645rec.sacscode[3]);// LP S
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[3]);
	lifacmvrec1.glcode.set(t5645rec.glmap[3]);
	lifacmvrec1.glsign.set(t5645rec.sign[3]);
	lifacmvrec1.contot.set(t5645rec.cnttot[3]);	
	lifacmvrec1.origamt.set(add(accAmtLPAE,accAmtLPAS));   
	lifacmvrec1.acctamt.set(add(accAmtLPAE,accAmtLPAS));
	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
		return ; 
	}
	
 }
public Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   } 
protected void callRounding5000()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(wsspcomn.company);
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(zraeIO.getPaycurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*EXIT*/
}


	protected void readZraeWhileModify() {
		zraepf.setChdrcoy(chdrenqIO.getChdrcoy().toString());
		zraepf.setChdrnum(chdrenqIO.getChdrnum().toString());
		listZraepf = zraepfDAO.getZraeData(zraepf);
		if (listZraepf.isEmpty()) {
			dealWithZraeRecords3000();
			readtr528();
		}
		else {
			readHoldZrae1200();
		}
	}

}