package com.csc.life.anticipatedendowment.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:22
 * Description:
 * Copybook name: ZRLNDSCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrlndsckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrlndscFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrlndscKey = new FixedLengthStringData(64).isAPartOf(zrlndscFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrlndscChdrcoy = new FixedLengthStringData(1).isAPartOf(zrlndscKey, 0);
  	public FixedLengthStringData zrlndscChdrnum = new FixedLengthStringData(8).isAPartOf(zrlndscKey, 1);
  	public PackedDecimalData zrlndscLoanNumber = new PackedDecimalData(2, 0).isAPartOf(zrlndscKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(zrlndscKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrlndscFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrlndscFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}