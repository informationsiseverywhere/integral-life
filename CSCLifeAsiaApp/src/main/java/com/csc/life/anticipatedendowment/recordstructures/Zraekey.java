package com.csc.life.anticipatedendowment.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:20
 * Description:
 * Copybook name: ZRAEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zraekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zraeFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zraeKey = new FixedLengthStringData(64).isAPartOf(zraeFileKey, 0, REDEFINE);
  	public FixedLengthStringData zraeChdrcoy = new FixedLengthStringData(1).isAPartOf(zraeKey, 0);
  	public FixedLengthStringData zraeChdrnum = new FixedLengthStringData(8).isAPartOf(zraeKey, 1);
  	public FixedLengthStringData zraeLife = new FixedLengthStringData(2).isAPartOf(zraeKey, 9);
  	public FixedLengthStringData zraeCoverage = new FixedLengthStringData(2).isAPartOf(zraeKey, 11);
  	public FixedLengthStringData zraeRider = new FixedLengthStringData(2).isAPartOf(zraeKey, 13);
  	public PackedDecimalData zraePlanSuffix = new PackedDecimalData(4, 0).isAPartOf(zraeKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(zraeKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zraeFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zraeFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}