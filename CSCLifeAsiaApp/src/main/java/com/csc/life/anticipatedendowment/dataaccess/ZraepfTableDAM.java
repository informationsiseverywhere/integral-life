package com.csc.life.anticipatedendowment.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZraepfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:01
 * Class transformed from ZRAEPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZraepfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 449;
	public FixedLengthStringData zraerec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zraepfRecord = zraerec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zraerec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zraerec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zraerec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zraerec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zraerec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(zraerec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(zraerec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zraerec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(zraerec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte01 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte02 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte03 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte04 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte05 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte06 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte07 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData zrduedte08 = DD.zrduedte.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt01 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt02 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt03 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt04 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt05 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt06 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt07 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData prcnt08 = DD.prcnt.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte01 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte02 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte03 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte04 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte05 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte06 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte07 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paydte08 = DD.paydte.copy().isAPartOf(zraerec);
	public PackedDecimalData paid01 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid02 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid03 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid04 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid05 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid06 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid07 = DD.paid.copy().isAPartOf(zraerec);
	public PackedDecimalData paid08 = DD.paid.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod01 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod02 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod03 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod04 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod05 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod06 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod07 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData paymentMethod08 = DD.paymmeth.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption01 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption02 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption03 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption04 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption05 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption06 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption07 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public FixedLengthStringData payOption08 = DD.zrpayopt.copy().isAPartOf(zraerec);
	public PackedDecimalData totamnt = DD.totamnt.copy().isAPartOf(zraerec);
	public PackedDecimalData nextPaydate = DD.npaydate.copy().isAPartOf(zraerec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(zraerec);
	
	public FixedLengthStringData flag = DD.validflag.copy().isAPartOf(zraerec);
	public PackedDecimalData apcaplamt = DD.apcaplamt.copy().isAPartOf(zraerec);
	public PackedDecimalData apintamt = DD.apintamt.copy().isAPartOf(zraerec);
	public PackedDecimalData aplstcapdate = DD.aplstcapdate.copy().isAPartOf(zraerec);
	public PackedDecimalData apnxtcapdate = DD.apnxtcapdate.copy().isAPartOf(zraerec);
	public PackedDecimalData aplstintbdte = DD.aplstintbdte.copy().isAPartOf(zraerec);
	public PackedDecimalData apnxtintbdte = DD.apnxtintbdte.copy().isAPartOf(zraerec);
	
	
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(zraerec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(zraerec);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(zraerec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zraerec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zraerec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zraerec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZraepfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZraepfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZraepfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZraepfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZraepfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZraepfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZraepfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZRAEPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"CURRFROM, " +
							"CURRTO, " +
							"ZRDUEDTE01, " +
							"ZRDUEDTE02, " +
							"ZRDUEDTE03, " +
							"ZRDUEDTE04, " +
							"ZRDUEDTE05, " +
							"ZRDUEDTE06, " +
							"ZRDUEDTE07, " +
							"ZRDUEDTE08, " +
							"PRCNT01, " +
							"PRCNT02, " +
							"PRCNT03, " +
							"PRCNT04, " +
							"PRCNT05, " +
							"PRCNT06, " +
							"PRCNT07, " +
							"PRCNT08, " +
							"PAYDTE01, " +
							"PAYDTE02, " +
							"PAYDTE03, " +
							"PAYDTE04, " +
							"PAYDTE05, " +
							"PAYDTE06, " +
							"PAYDTE07, " +
							"PAYDTE08, " +
							"PAID01, " +
							"PAID02, " +
							"PAID03, " +
							"PAID04, " +
							"PAID05, " +
							"PAID06, " +
							"PAID07, " +
							"PAID08, " +
							"PAYMMETH01, " +
							"PAYMMETH02, " +
							"PAYMMETH03, " +
							"PAYMMETH04, " +
							"PAYMMETH05, " +
							"PAYMMETH06, " +
							"PAYMMETH07, " +
							"PAYMMETH08, " +
							"ZRPAYOPT01, " +
							"ZRPAYOPT02, " +
							"ZRPAYOPT03, " +
							"ZRPAYOPT04, " +
							"ZRPAYOPT05, " +
							"ZRPAYOPT06, " +
							"ZRPAYOPT07, " +
							"ZRPAYOPT08, " +
							"TOTAMNT, " +
							"NPAYDATE, " +
							"PAYCLT, " +
							"FLAG, " +
							"APCAPLAMT, " +
				            "APINTAMT, " +
				            "APLSTCAPDATE, " +
				            "APNXTCAPDATE, " +
				            "APLSTINTBDTE, " +
				            "APNXTINTBDTE, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"PAYCURR, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     tranno,
                                     currfrom,
                                     currto,
                                     zrduedte01,
                                     zrduedte02,
                                     zrduedte03,
                                     zrduedte04,
                                     zrduedte05,
                                     zrduedte06,
                                     zrduedte07,
                                     zrduedte08,
                                     prcnt01,
                                     prcnt02,
                                     prcnt03,
                                     prcnt04,
                                     prcnt05,
                                     prcnt06,
                                     prcnt07,
                                     prcnt08,
                                     paydte01,
                                     paydte02,
                                     paydte03,
                                     paydte04,
                                     paydte05,
                                     paydte06,
                                     paydte07,
                                     paydte08,
                                     paid01,
                                     paid02,
                                     paid03,
                                     paid04,
                                     paid05,
                                     paid06,
                                     paid07,
                                     paid08,
                                     paymentMethod01,
                                     paymentMethod02,
                                     paymentMethod03,
                                     paymentMethod04,
                                     paymentMethod05,
                                     paymentMethod06,
                                     paymentMethod07,
                                     paymentMethod08,
                                     payOption01,
                                     payOption02,
                                     payOption03,
                                     payOption04,
                                     payOption05,
                                     payOption06,
                                     payOption07,
                                     payOption08,
                                     totamnt,
                                     nextPaydate,
                                     payclt,
                                     flag,
                                     apcaplamt,
                                     apintamt,
                                     aplstcapdate,
                                     apnxtcapdate,
                                     aplstintbdte,
                                     apnxtintbdte,
                                     bankkey,
                                     bankacckey,
                                     paycurr,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		tranno.clear();
  		currfrom.clear();
  		currto.clear();
  		zrduedte01.clear();
  		zrduedte02.clear();
  		zrduedte03.clear();
  		zrduedte04.clear();
  		zrduedte05.clear();
  		zrduedte06.clear();
  		zrduedte07.clear();
  		zrduedte08.clear();
  		prcnt01.clear();
  		prcnt02.clear();
  		prcnt03.clear();
  		prcnt04.clear();
  		prcnt05.clear();
  		prcnt06.clear();
  		prcnt07.clear();
  		prcnt08.clear();
  		paydte01.clear();
  		paydte02.clear();
  		paydte03.clear();
  		paydte04.clear();
  		paydte05.clear();
  		paydte06.clear();
  		paydte07.clear();
  		paydte08.clear();
  		paid01.clear();
  		paid02.clear();
  		paid03.clear();
  		paid04.clear();
  		paid05.clear();
  		paid06.clear();
  		paid07.clear();
  		paid08.clear();
  		paymentMethod01.clear();
  		paymentMethod02.clear();
  		paymentMethod03.clear();
  		paymentMethod04.clear();
  		paymentMethod05.clear();
  		paymentMethod06.clear();
  		paymentMethod07.clear();
  		paymentMethod08.clear();
  		payOption01.clear();
  		payOption02.clear();
  		payOption03.clear();
  		payOption04.clear();
  		payOption05.clear();
  		payOption06.clear();
  		payOption07.clear();
  		payOption08.clear();
  		totamnt.clear();
  		nextPaydate.clear();
  		payclt.clear();
  		flag.clear();
  		apcaplamt.clear();
        apintamt.clear();
        aplstcapdate.clear();
        apnxtcapdate.clear();
        aplstintbdte.clear();
        apnxtintbdte.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		paycurr.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZraerec() {
  		return zraerec;
	}

	public FixedLengthStringData getZraepfRecord() {
  		return zraepfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZraerec(what);
	}

	public void setZraerec(Object what) {
  		this.zraerec.set(what);
	}

	public void setZraepfRecord(Object what) {
  		this.zraepfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zraerec.getLength());
		result.set(zraerec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}