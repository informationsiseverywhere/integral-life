package com.csc.life.anticipatedendowment.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:04
 * Description:
 * Copybook name: TR530REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr530rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr530Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zrages = new FixedLengthStringData(16).isAPartOf(tr530Rec, 0);
  	public ZonedDecimalData[] zrage = ZDArrayPartOfStructure(8, 2, 0, zrages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(zrages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrage01 = new ZonedDecimalData(2, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData zrage02 = new ZonedDecimalData(2, 0).isAPartOf(filler, 2);
  	public ZonedDecimalData zrage03 = new ZonedDecimalData(2, 0).isAPartOf(filler, 4);
  	public ZonedDecimalData zrage04 = new ZonedDecimalData(2, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData zrage05 = new ZonedDecimalData(2, 0).isAPartOf(filler, 8);
  	public ZonedDecimalData zrage06 = new ZonedDecimalData(2, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData zrage07 = new ZonedDecimalData(2, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData zrage08 = new ZonedDecimalData(2, 0).isAPartOf(filler, 14);
  	public FixedLengthStringData zragfrs = new FixedLengthStringData(20).isAPartOf(tr530Rec, 16);
  	public ZonedDecimalData[] zragfr = ZDArrayPartOfStructure(10, 2, 0, zragfrs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(zragfrs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zragfr01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData zragfr02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData zragfr03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData zragfr04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData zragfr05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData zragfr06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData zragfr07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData zragfr08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData zragfr09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData zragfr10 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
  	public FixedLengthStringData zragtos = new FixedLengthStringData(20).isAPartOf(tr530Rec, 36);
  	public ZonedDecimalData[] zragto = ZDArrayPartOfStructure(10, 2, 0, zragtos, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(zragtos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zragto01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData zragto02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData zragto03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData zragto04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData zragto05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData zragto06 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
  	public ZonedDecimalData zragto07 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData zragto08 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
  	public ZonedDecimalData zragto09 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 16);
  	public ZonedDecimalData zragto10 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 18);
  	public FixedLengthStringData zredsumas = new FixedLengthStringData(1).isAPartOf(tr530Rec, 56);
  	public FixedLengthStringData zrpcpds = new FixedLengthStringData(400).isAPartOf(tr530Rec, 57);
  	public ZonedDecimalData[] zrpcpd = ZDArrayPartOfStructure(80, 5, 2, zrpcpds, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(400).isAPartOf(zrpcpds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrpcpd01 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData zrpcpd02 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 5);
  	public ZonedDecimalData zrpcpd03 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 10);
  	public ZonedDecimalData zrpcpd04 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 15);
  	public ZonedDecimalData zrpcpd05 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 20);
  	public ZonedDecimalData zrpcpd06 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 25);
  	public ZonedDecimalData zrpcpd07 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 30);
  	public ZonedDecimalData zrpcpd08 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 35);
  	public ZonedDecimalData zrpcpd09 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 40);
  	public ZonedDecimalData zrpcpd10 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 45);
  	public ZonedDecimalData zrpcpd11 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 50);
  	public ZonedDecimalData zrpcpd12 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 55);
  	public ZonedDecimalData zrpcpd13 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 60);
  	public ZonedDecimalData zrpcpd14 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 65);
  	public ZonedDecimalData zrpcpd15 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 70);
  	public ZonedDecimalData zrpcpd16 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 75);
  	public ZonedDecimalData zrpcpd17 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 80);
  	public ZonedDecimalData zrpcpd18 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 85);
  	public ZonedDecimalData zrpcpd19 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 90);
  	public ZonedDecimalData zrpcpd20 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 95);
  	public ZonedDecimalData zrpcpd21 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 100);
  	public ZonedDecimalData zrpcpd22 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 105);
  	public ZonedDecimalData zrpcpd23 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 110);
  	public ZonedDecimalData zrpcpd24 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 115);
  	public ZonedDecimalData zrpcpd25 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 120);
  	public ZonedDecimalData zrpcpd26 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 125);
  	public ZonedDecimalData zrpcpd27 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 130);
  	public ZonedDecimalData zrpcpd28 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 135);
  	public ZonedDecimalData zrpcpd29 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 140);
  	public ZonedDecimalData zrpcpd30 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 145);
  	public ZonedDecimalData zrpcpd31 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 150);
  	public ZonedDecimalData zrpcpd32 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 155);
  	public ZonedDecimalData zrpcpd33 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 160);
  	public ZonedDecimalData zrpcpd34 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 165);
  	public ZonedDecimalData zrpcpd35 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 170);
  	public ZonedDecimalData zrpcpd36 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 175);
  	public ZonedDecimalData zrpcpd37 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 180);
  	public ZonedDecimalData zrpcpd38 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 185);
  	public ZonedDecimalData zrpcpd39 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 190);
  	public ZonedDecimalData zrpcpd40 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 195);
  	public ZonedDecimalData zrpcpd41 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 200);
  	public ZonedDecimalData zrpcpd42 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 205);
  	public ZonedDecimalData zrpcpd43 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 210);
  	public ZonedDecimalData zrpcpd44 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 215);
  	public ZonedDecimalData zrpcpd45 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 220);
  	public ZonedDecimalData zrpcpd46 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 225);
  	public ZonedDecimalData zrpcpd47 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 230);
  	public ZonedDecimalData zrpcpd48 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 235);
  	public ZonedDecimalData zrpcpd49 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 240);
  	public ZonedDecimalData zrpcpd50 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 245);
  	public ZonedDecimalData zrpcpd51 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 250);
  	public ZonedDecimalData zrpcpd52 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 255);
  	public ZonedDecimalData zrpcpd53 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 260);
  	public ZonedDecimalData zrpcpd54 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 265);
  	public ZonedDecimalData zrpcpd55 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 270);
  	public ZonedDecimalData zrpcpd56 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 275);
  	public ZonedDecimalData zrpcpd57 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 280);
  	public ZonedDecimalData zrpcpd58 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 285);
  	public ZonedDecimalData zrpcpd59 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 290);
  	public ZonedDecimalData zrpcpd60 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 295);
  	public ZonedDecimalData zrpcpd61 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 300);
  	public ZonedDecimalData zrpcpd62 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 305);
  	public ZonedDecimalData zrpcpd63 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 310);
  	public ZonedDecimalData zrpcpd64 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 315);
  	public ZonedDecimalData zrpcpd65 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 320);
  	public ZonedDecimalData zrpcpd66 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 325);
  	public ZonedDecimalData zrpcpd67 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 330);
  	public ZonedDecimalData zrpcpd68 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 335);
  	public ZonedDecimalData zrpcpd69 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 340);
  	public ZonedDecimalData zrpcpd70 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 345);
  	public ZonedDecimalData zrpcpd71 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 350);
  	public ZonedDecimalData zrpcpd72 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 355);
  	public ZonedDecimalData zrpcpd73 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 360);
  	public ZonedDecimalData zrpcpd74 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 365);
  	public ZonedDecimalData zrpcpd75 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 370);
  	public ZonedDecimalData zrpcpd76 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 375);
  	public ZonedDecimalData zrpcpd77 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 380);
  	public ZonedDecimalData zrpcpd78 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 385);
  	public ZonedDecimalData zrpcpd79 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 390);
  	public ZonedDecimalData zrpcpd80 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 395);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(43).isAPartOf(tr530Rec, 457, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr530Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr530Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}