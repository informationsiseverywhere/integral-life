/*
 * File: Bd5ks.java
 * Date: 29 August 2009 22:16:02
 * Author: Quipoz Limited
 *
 * Class transformed from BR535.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 * 
 * ICIL-748 POS023 endowment and maturity bsd - Batch changes - L2NEWINTCP
 */
package com.csc.life.anticipatedendowment.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;

import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.life.anticipatedendowment.dataaccess.dao.Bd5ksTempDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5ksDTO;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   New Endowment Interest Capitalization program.
*
*
*   Control totals:
*     01  -  Number of LOAN records processed
*
*
*****************************************************************
* </pre>
*/
public class Bd5ks extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BD5KS");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSqlEffdate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaFacthous = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBankacckey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);	
	private FixedLengthStringData wsaaReadPayr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnumOld = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).setUnsigned();
	private PackedDecimalData wsaaBaseIdx = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	private final String wsaaSign = "-";
		/*                                                         <V4L001>
		  Storage for T5645 table items.                         <V4L001>
		                                                         <V4L001>*/
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5645Sub = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);

		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private static final int wsaaUser = 0;

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
		/* ERRORS */
	private static final String e723 = "E723";
	private static final String h791 = "H791";
	private static final String t5645 = "T5645";
	private static final String t3695 = "T3695";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);


	private DescTableDAM descIO = new DescTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	
	
	private T5645rec t5645rec = new T5645rec();
	private Td5h7rec td5h7rec = new Td5h7rec();
	private T3695rec t3695rec = new T3695rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	
	private FixedLengthStringData wsaaMandstat = new FixedLengthStringData(2);
	
	
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	
	private AcblpfDAO acblpfDAO =  getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private Bd5ksTempDAO bd5kstempdao = getApplicationContext().getBean("bd5ksTempDAO", Bd5ksTempDAO.class);	
	
	private Map<String, List<Itempf>> t5645ListMap = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> td5h7ListMap = new HashMap<String, List<Itempf>>();	
	
	private Map<String, List<Itempf>> t3695ListMap = new HashMap<String, List<Itempf>>();
	
	private Bd5ksDTO bd5ksdto = new Bd5ksDTO();
	private List<Zraepf> zraeList = new ArrayList<Zraepf>();
	private List<Chdrpf> chdrBulkUpdtList = new ArrayList<Chdrpf>();	
	private List<Zraepf> zraeBulkUpdtList = new ArrayList<Zraepf>();
	private List<Zraepf> zraeBulkTmptList = new ArrayList<Zraepf>();
	
	private List<Zraepf> zraeCapnBulkUpdtList  = new ArrayList<Zraepf>();
	private List<Acmvpf> acmvpfList = new ArrayList<Acmvpf>();
	private List<Ptrnpf> ptrnBulkInsList = new ArrayList<Ptrnpf>();
	
	private Zraepf zraepf = new Zraepf(); 
	private Acblpf acblpf;
	private Ptrnpf ptrnpf;
	private Iterator<Zraepf> iterator;
	private Iterator<Acmvpf> acmvpfIter;
	private Chdrpf chdrpf;	
	
	
	private String strSacscode;
	private String strSacsType;
	private String strglMap;
	private String strglSign;
	
	
	private int minRecord = 0;
	private String prevChdrnum = new String();
	private String currChdrnum = new String();
	private int rcdCnt =0;
	private int incrRange;
	private int ctrCT01; 
	private int ctrCT02;
	private int wsaaIntFromDate;
	
	private static final String hl63 = "HL63";	
	private static final String h420 = "H420";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5ks.class);
	private Map<String, Descpf> t5645Map = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf descpfT5645;
	private boolean noRecordFound;
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	public PackedDecimalData capAmount = new PackedDecimalData(17, 2);
/**
 * Contains all possible labels used by goTo action.
 */
	

	public Bd5ks() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsspEdterror.set(varcom.oK);
		wsaaParmCompany.set(bsprIO.getCompany());
		wsaaSqlEffdate.set(bsscIO.getEffectiveDate());
		wsaaCnttype.set(SPACES);
		wsaaFacthous.set(SPACES);
		wsaaBankkey.set(SPACES);
		wsaaBankacckey.set(SPACES);
		wsaaTranno.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		wsaaChdrnumOld.set(SPACES);
		wsaaMandstat.set(SPACES);
		
		ctrCT01 = 0;
		ctrCT02 = 0;
		
		String coy = bsprIO.getCompany().toString();
	    t5645ListMap = itemDAO.loadSmartTable("IT", coy, "T5645");
	    readT5645();
	    td5h7ListMap = itemDAO.loadSmartTable("IT", coy, "TD5H7");	
	    noRecordFound = false;
	    t3695ListMap = itemDAO.loadSmartTable("IT", coy, "T3695");
	    getItemDescription();
	    wsaaTransactionDate.set(getCobolDate());
		wsaaTransactionTime.set(varcom.vrcmTime);
	    //For reading the loanpf
	
	    if (bprdIO.systemParam01.isNumeric()) {
	        if (bprdIO.systemParam01.toInt() > 0) {
	            incrRange = bprdIO.systemParam01.toInt();
	        } else {
	            incrRange = bprdIO.cyclesPerCommit.toInt();
	        }
	    } else {
	        incrRange = bprdIO.cyclesPerCommit.toInt();
	    }  
	    if(bd5kstempdao.deleteBd5ksTempAllRecords()){
	    	int extractRows = bd5kstempdao.insertBd5ksTempRecords(incrRange,wsaaParmCompany.toString(), wsaaSqlEffdate.toInt());
			if (extractRows > 0) {
				performDataChunk();	
			}
			else{
				LOGGER.info("No records retrieved for processing.");
				noRecordFound = true;
				return;	
			}
	    }

	}

protected void readT5645(){
	/* Read the accounting rules table*/
	String keyItemitem = wsaaProg.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
			
			for (wsaaT5645Sub.set(1); !(isGT(wsaaT5645Sub,15)
					|| isGTE(wsaaT5645Offset,wsaaT5645Size)); wsaaT5645Sub.add(1)){
						wsaaT5645Offset.add(1);
						wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
						wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
						wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
						wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
					}
			
			if (isGTE(wsaaT5645Offset,wsaaT5645Size)) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t5645rec.t5645Rec);
				fatalError600();
			}
			
			itemFound = true;		
		}		
	}
	if (!itemFound) {
		syserrrec.statuz.set(h791);
		syserrrec.params.set(t5645);
		fatalError600();
	}	
}

protected void getItemDescription(){
	/* Get item description.*/
	t5645Map = descDAO.getItems("IT", wsaaParmCompany.toString(), "T5645", bsscIO.getLanguage().toString());
    if (t5645Map.containsKey(wsaaProg)) {
        descpfT5645 = t5645Map.get(wsaaProg);
    } else {
    	descpfT5645 = new Descpf();
    	descpfT5645.setLongdesc("");
    }
	
	wsaaTrandesc.set(descpfT5645.getLongdesc());
}

protected void performDataChunk(){

	zraeList = bd5kstempdao.loadDataByNEWINTCP(minRecord);
	
    if(zraeList != null && zraeList.size()>0){
    	iterator = zraeList.iterator();    	   	
    }else{
    	wsspEdterror.set(varcom.endp);
    }

    
}


protected void readFile2000()
	{
		readFile2010();		
	}

protected void readFile2010()
	{
	if(zraeList != null && zraeList.size()>0){
		 if(iterator.hasNext()){
			 	zraepf = new Zraepf(); 
		    	zraepf = iterator.next();
		    	bd5ksdto = zraepf.getBd5ksDTO();		    	
				currChdrnum = zraepf.getChdrnum();
		    } else {
			 minRecord++;
	            // for read paging
			 zraeList = bd5kstempdao.loadDataByNEWINTCP(minRecord);
			 if(zraeList != null && zraeList.size()>0){
			    	iterator = zraeList.iterator(); 
			    	if(iterator.hasNext()){
					 	zraepf = new Zraepf(); 
				    	zraepf = iterator.next();
				    	bd5ksdto = zraepf.getBd5ksDTO();		    	
						currChdrnum = zraepf.getChdrnum();
				    } 
			    }else{			    	
			    	wsspEdterror.set(varcom.endp);
			    }
			}	
		 
		 if(isNE(prevChdrnum,currChdrnum)){
				prevChdrnum = currChdrnum;
				rcdCnt = 1;
				interestAmount.set(ZERO);
				String keyItemitem = bd5ksdto.getCnttype();				
				readTd5h7(keyItemitem);	
			
			} else {
				rcdCnt++;
			}
	}
	else{			    	
    	wsspEdterror.set(varcom.endp);
    }
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaChdrnumOld,SPACES)) {
			removeSftlock2200();
		}
	}

protected void removeSftlock2200()
	{		
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(wsaaParmCompany);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(wsaaChdrnumOld);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}


protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT01 = 0;
	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT02 = 0;
	
}

protected void edit2500()
	{
		/* If Contract number has changed, we need to unlock record*/
		/* previously locked.*/
		wsspEdterror.set(varcom.oK);
		
		
		
		/* Process Regp record read - calculate Interest since last*/
		/* interest billing date and post ACMVs accordingly.*/
		wsaaCurbal.set(ZERO);		
		wsaaSequenceNo.set(ZERO);
		wsaaReadPayr.set(SPACES);
		softlock2600();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			//contotrec.totno.set(ct02);
			//contotrec.totval.set(1);
			//callContot001();
			ctrCT02++;
			wsspEdterror.set(SPACES);
			wsaaChdrnumOld.set(SPACES);
			return ;
		}
		
		/* Check whether we need to calculate any Interest due*/
		/* - user may not have run Billing interest program first*/
		if (isGTE(zraepf.getAplstintbdte(), wsaaSqlEffdate)) {
			return ;
		}
		calculateInterest2700();
		updateRegpRecord();	
		ctrCT01++;
		if(isEQ(rcdCnt,bd5ksdto.getCnt())){	
			
			updateContractHeader2600();		
			postInterestAcmvs2800();	
		}
	}

protected void softlock2600()
	{		
		/* Soft lock the contract, if it is to be processed.               */
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsaaParmCompany);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(zraepf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void updateContractHeader2600()
{	
	/* If Contract has already been updated because there is more*/
	/* more one loan against the contract, do not increment the*/
	/* TRANNO.*/
	
	chdrpf = new Chdrpf();
	chdrpf.setChdrcoy(zraepf.getChdrcoy().charAt(0));
	chdrpf.setChdrnum(zraepf.getChdrnum());
	chdrpf.setTranno(bd5ksdto.getTranno() + 1);
	chdrBulkUpdtList.add(chdrpf);	
	
}

protected void readTd5h7(String keyItemitem)
{
	wsaaTranno.set(bd5ksdto.getTranno()+1);
	wsaaCnttype.set(bd5ksdto.getCnttype());
	wsaaChdrnumOld.set(zraepf.getChdrnum());

	boolean itemFound = false;
	
	List<Itempf> itempfList = new ArrayList<Itempf>();
	if (td5h7ListMap.containsKey(keyItemitem)){	
		itempfList = td5h7ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();			
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(wsaaSqlEffdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(wsaaSqlEffdate.toString()) <= Integer.parseInt(itempf.getItmto().toString())){			
					td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}else{
				td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}	
		}
	}
	
	if (!itemFound) {
		keyItemitem = "***";/* IJTI-1523 */
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (td5h7ListMap.containsKey(keyItemitem)){	
			itempfList = td5h7ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(wsaaSqlEffdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(wsaaSqlEffdate.toString()) <= Integer.parseInt(itempf.getItmto().toString())){			
						td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound2 = true;
					}
				}else{
					td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound2 = true;					
				}				
			}		
		}		
		if (!itemFound2) {
			syserrrec.params.set(td5h7rec.td5h7Rec);
			syserrrec.statuz.set(e723);
			fatalError600();	
		}		
	}
}

protected void calculateInterest2700()
	{
		
		/* Set up INTCALC linkage and call...*/
		/* Don't forget that the value returned is in LOAN currency.*/
		annypyintcalcrec.intcalcRec.set(SPACES);
		annypyintcalcrec.chdrcoy.set(zraepf.getChdrcoy());
		annypyintcalcrec.chdrnum.set(zraepf.getChdrnum());
		annypyintcalcrec.cnttype.set(wsaaCnttype);
		annypyintcalcrec.interestTo.set(zraepf.getApnxtcapdate());
		annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
		wsaaIntFromDate=zraepf.getAplstintbdte();
		annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
		annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());		
		annypyintcalcrec.interestAmount.set(ZERO);
		annypyintcalcrec.currency.set(zraepf.getPaycurr());
		annypyintcalcrec.transaction.set("ENDOWMENT");
		
		callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
		if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(annypyintcalcrec.intcalcRec);
			syserrrec.statuz.set(annypyintcalcrec.statuz);
			fatalError600();
		}
		/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
		if (isNE(annypyintcalcrec.interestAmount, 0)) {
			zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
			callRounding5000();
			annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
			interestAmount.add(annypyintcalcrec.interestAmount);
		}
	}

protected void updateRegpRecord()
	{
		/* Update LOAN record - reset next & last interest billing*/
		/* dates.*/
		zraepf.setApintamt(annypyintcalcrec.interestAmount.getbigdata());
		zraepf.setAplstintbdte(zraepf.getApnxtcapdate());
		nextIntBillDate2770();
		

	}

protected void nextIntBillDate2770()
	{	
		if (isNE(td5h7rec.intcalfreq,SPACES)) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(zraepf.getAplstintbdte());
			datcon2rec.frequency.set(td5h7rec.intcalfreq);
			datcon2rec.freqFactor.set(1);
			callDatcon22780();	
			zraepf.setApnxtintbdte(datcon2rec.intDate2.toInt());
		}
	}


protected void callDatcon22780()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}


protected void postInterestAcmvs2800()
	{
		
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(zraepf.getChdrcoy());
		lifacmvrec.genlcoy.set(zraepf.getChdrcoy());
		lifacmvrec.rdocnum.set(zraepf.getChdrnum());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(zraepf.getPaycurr());
		lifacmvrec.origamt.set(interestAmount);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(wsaaSqlEffdate);
		lifacmvrec.tranref.set(zraepf.getChdrnum());
		lifacmvrec.trandesc.set(wsaaTrandesc);
		lifacmvrec.rldgacct.set(zraepf.getChdrnum());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		/* Post the interest to the Loan debit account (depending on*/
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(wsaaCnttype);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}		
	}


protected void update3000()
	{
		/*UPDATE*/
		updateRegp();
		if(isEQ(rcdCnt,bd5ksdto.getCnt())){
			zraeBulkUpdtList.addAll(zraeBulkTmptList);
			zraeBulkTmptList.clear();
			totalLoanDebt3100();
			capitalise3200();		
			ptrnBatcup3400();
		}
		if (isNE(wsaaChdrnumOld, SPACES)){ //ILIFE-2505 //MIBT-119
			removeSftlock2200();
		}
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void totalLoanDebt3100()
	{		
		/* Find current amount of Interest owed - read ACBL record.*/
		wsaaCurbal.set(ZERO);
		acblpf = acblpfDAO.loadDataByBatch(zraepf.getChdrcoy(), t5645rec.sacscode[1].toString(),
				zraepf.getChdrnum(), zraepf.getPaycurr(), t5645rec.sacstype[1].toString());/* IJTI-1523 */
		if (acblpf != null) 
			wsaaCurbal.set(acblpf.getSacscurbal());		
		else 
			wsaaCurbal.set(ZERO);
		wsaaCurbal = sub(ZERO,wsaaCurbal);		
	}





protected void readT3695(String sacstype){

	/* Read the accounting rules table*/
	String keyItemitem = sacstype;
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3695ListMap.containsKey(keyItemitem)){	
		itempfList = t3695ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;		
		}		
	}
	if (!itemFound) {
		syserrrec.statuz.set(h420);
		syserrrec.params.set(t3695);
		fatalError600();
	}	
}


protected void capitalise3200()
	{		
		/* If no Interest accrued, just update Loan record.*/
		if (isEQ(wsaaCurbal, ZERO)) {
			return ;
		}
		/* Set up LIFACMV fields.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(zraepf.getChdrcoy());
		lifacmvrec.genlcoy.set(zraepf.getChdrcoy());
		lifacmvrec.rdocnum.set(zraepf.getChdrnum());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(zraepf.getPaycurr());
		/* Move total Interest accrued over period to Loan principal*/
		/* Sub A/C.*/
		lifacmvrec.origamt.set(wsaaCurbal);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(zraepf.getAplstcapdate());
		lifacmvrec.tranref.set(zraepf.getChdrnum());
		lifacmvrec.trandesc.set(wsaaTrandesc);
		
		lifacmvrec.rldgacct.set(zraepf.getChdrnum());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		/* Post the Interest to the Loan principal account.*/		
		lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec.glcode.set(t5645rec.glmap[2]);
		lifacmvrec.glsign.set(t5645rec.sign[2]);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(wsaaCnttype);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[3]);
		lifacmvrec.glcode.set(t5645rec.glmap[3]);
		lifacmvrec.glsign.set(t5645rec.sign[3]);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void updateRegp()
	{		
		zraepf.setAplstcapdate(zraepf.getApnxtcapdate());
		zraepf.setApcaplamt(add(zraepf.getApcaplamt(),zraepf.getApintamt()).getbigdata());
		zraepf.setApintamt(BigDecimal.ZERO);
		nextCapnDate3220();
		zraeBulkTmptList.add(zraepf);
	}

protected void nextCapnDate3220()
	{		
		/* Check the Capitalisation details on TD5H7 in the following*/
		
		if (isNE(td5h7rec.intcapfreq,SPACES)) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(zraepf.getAplstcapdate());
			datcon2rec.frequency.set(td5h7rec.intcapfreq);
			datcon2rec.freqFactor.set(1);
			callDatcon22780();			
			zraepf.setApnxtcapdate(datcon2rec.intDate2.toInt());
		}
	}

protected void ptrnBatcup3400()
	{
		
		if (ptrnpf != null && isEQ(ptrnpf.getChdrnum(), zraepf.getChdrnum())) {
			return ;
		}
		
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrcoy(zraepf.getChdrcoy());
		ptrnpf.setChdrpfx(bd5ksdto.getChdrpfx());
		ptrnpf.setChdrnum(zraepf.getChdrnum());
		ptrnpf.setTranno(bd5ksdto.getTranno()+1);//ILIFE-5112
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setPtrneff(zraepf.getAplstcapdate());
		ptrnpf.setUserT(wsaaUser);
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
		ptrnBulkInsList.add(ptrnpf);
	
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set("BA");	//ILIFE-3210
		batcuprec.batccoy.set(batcdorrec.company);
		batcuprec.batcbrn.set(batcdorrec.branch);
		batcuprec.batcactyr.set(batcdorrec.actyear);
		batcuprec.batctrcde.set(batcdorrec.trcde);
		batcuprec.batcactmn.set(batcdorrec.actmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}



protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else {
		ptrnBulkInsList.clear();
	}
}

protected void commit3500()
	{
		/*COMMIT*/
		if (!noRecordFound){
		 commitControlTotals();
		 commitChdrBulkUpdate();	
		 commitRegpUpdate();
		// commitRegpCapUpdate();
		 commitPtrnBulkInsert();
		}
	 
	}

protected void commitChdrBulkUpdate()
{	 
	boolean isUpdateContractHeader = chdrpfDAO.updateChdrTrannoByChdrnum(chdrBulkUpdtList);
	if (!isUpdateContractHeader) {
		LOGGER.error("Update Contract Header failed.");
		fatalError600();
	}else {
		chdrBulkUpdtList.clear();
	}
}

protected void commitRegpUpdate(){
	 boolean isUpdateLoanRec = zraepfDAO.updateZrae(zraeBulkUpdtList);
	if (!isUpdateLoanRec) {
		LOGGER.error("Update Loan Record failed.");
		fatalError600();
	}else {
		zraeBulkUpdtList.clear();
	}
}

/*protected void commitRegpCapUpdate(){
	 boolean isUpdateLoanCapRec = ZraepfDAO.updateRegpRecCapDate(regpCapnBulkUpdtList);
	if (!isUpdateLoanCapRec) {
		LOGGER.error("Update Loan Record failed.");
		fatalError600();
	}else {
		regpCapnBulkUpdtList.clear();
	}
}*/



protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
	    bd5kstempdao.deleteBd5ksTempAllRecords();
	    
		lsaaStatuz.set(varcom.oK);
		
		t5645ListMap.clear();
		t5645ListMap = null;
		
		td5h7ListMap.clear();
		td5h7ListMap = null;
				
		t3695ListMap.clear();
		t3695ListMap = null;
		
		
		
		zraeList.clear();
		zraeList = null;
		
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;
		
		zraeBulkUpdtList.clear();
		zraeBulkUpdtList = null;
		
		
		
		zraeCapnBulkUpdtList.clear();
		zraeCapnBulkUpdtList = null;
		
		acmvpfList.clear();
		acmvpfList = null;
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;	

		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(zraepf.getPaycurr());
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}


}
