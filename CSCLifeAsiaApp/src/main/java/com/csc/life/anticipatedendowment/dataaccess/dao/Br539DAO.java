package com.csc.life.anticipatedendowment.dataaccess.dao;

import java.util.List;

import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br539DAO extends BaseDAO<Loanpf>{
	public List<Loanpf> loadDataByBatch(int BatchID);
	public List<Loanpf> populateBr539Temp(String tableID, String memName, int batchExtractSize, int BatchID); //ILIFE-8840
}
