package com.csc.life.anticipatedendowment.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:00
 * Description:
 * Copybook name: TR529REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr529rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr529Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zredsumas = new FixedLengthStringData(1).isAPartOf(tr529Rec, 0);
  	public FixedLengthStringData zrnoyrss = new FixedLengthStringData(16).isAPartOf(tr529Rec, 1);
  	public ZonedDecimalData[] zrnoyrs = ZDArrayPartOfStructure(8, 2, 0, zrnoyrss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(zrnoyrss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrnoyrs01 = new ZonedDecimalData(2, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData zrnoyrs02 = new ZonedDecimalData(2, 0).isAPartOf(filler, 2);
  	public ZonedDecimalData zrnoyrs03 = new ZonedDecimalData(2, 0).isAPartOf(filler, 4);
  	public ZonedDecimalData zrnoyrs04 = new ZonedDecimalData(2, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData zrnoyrs05 = new ZonedDecimalData(2, 0).isAPartOf(filler, 8);
  	public ZonedDecimalData zrnoyrs06 = new ZonedDecimalData(2, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData zrnoyrs07 = new ZonedDecimalData(2, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData zrnoyrs08 = new ZonedDecimalData(2, 0).isAPartOf(filler, 14);
  	public FixedLengthStringData zrpcpds = new FixedLengthStringData(400).isAPartOf(tr529Rec, 17);
  	public ZonedDecimalData[] zrpcpd = ZDArrayPartOfStructure(80, 5, 2, zrpcpds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(400).isAPartOf(zrpcpds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrpcpd01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData zrpcpd02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData zrpcpd03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData zrpcpd04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData zrpcpd05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData zrpcpd06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData zrpcpd07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData zrpcpd08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData zrpcpd09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData zrpcpd10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData zrpcpd11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
  	public ZonedDecimalData zrpcpd12 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 55);
  	public ZonedDecimalData zrpcpd13 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 60);
  	public ZonedDecimalData zrpcpd14 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 65);
  	public ZonedDecimalData zrpcpd15 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 70);
  	public ZonedDecimalData zrpcpd16 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 75);
  	public ZonedDecimalData zrpcpd17 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 80);
  	public ZonedDecimalData zrpcpd18 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData zrpcpd19 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 90);
  	public ZonedDecimalData zrpcpd20 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 95);
  	public ZonedDecimalData zrpcpd21 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 100);
  	public ZonedDecimalData zrpcpd22 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 105);
  	public ZonedDecimalData zrpcpd23 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 110);
  	public ZonedDecimalData zrpcpd24 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 115);
  	public ZonedDecimalData zrpcpd25 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 120);
  	public ZonedDecimalData zrpcpd26 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 125);
  	public ZonedDecimalData zrpcpd27 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 130);
  	public ZonedDecimalData zrpcpd28 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 135);
  	public ZonedDecimalData zrpcpd29 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 140);
  	public ZonedDecimalData zrpcpd30 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 145);
  	public ZonedDecimalData zrpcpd31 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 150);
  	public ZonedDecimalData zrpcpd32 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 155);
  	public ZonedDecimalData zrpcpd33 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 160);
  	public ZonedDecimalData zrpcpd34 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 165);
  	public ZonedDecimalData zrpcpd35 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 170);
  	public ZonedDecimalData zrpcpd36 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 175);
  	public ZonedDecimalData zrpcpd37 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 180);
  	public ZonedDecimalData zrpcpd38 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 185);
  	public ZonedDecimalData zrpcpd39 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 190);
  	public ZonedDecimalData zrpcpd40 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 195);
  	public ZonedDecimalData zrpcpd41 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 200);
  	public ZonedDecimalData zrpcpd42 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 205);
  	public ZonedDecimalData zrpcpd43 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 210);
  	public ZonedDecimalData zrpcpd44 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 215);
  	public ZonedDecimalData zrpcpd45 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 220);
  	public ZonedDecimalData zrpcpd46 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 225);
  	public ZonedDecimalData zrpcpd47 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 230);
  	public ZonedDecimalData zrpcpd48 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 235);
  	public ZonedDecimalData zrpcpd49 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 240);
  	public ZonedDecimalData zrpcpd50 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 245);
  	public ZonedDecimalData zrpcpd51 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 250);
  	public ZonedDecimalData zrpcpd52 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 255);
  	public ZonedDecimalData zrpcpd53 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 260);
  	public ZonedDecimalData zrpcpd54 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 265);
  	public ZonedDecimalData zrpcpd55 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 270);
  	public ZonedDecimalData zrpcpd56 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 275);
  	public ZonedDecimalData zrpcpd57 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 280);
  	public ZonedDecimalData zrpcpd58 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 285);
  	public ZonedDecimalData zrpcpd59 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 290);
  	public ZonedDecimalData zrpcpd60 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 295);
  	public ZonedDecimalData zrpcpd61 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 300);
  	public ZonedDecimalData zrpcpd62 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 305);
  	public ZonedDecimalData zrpcpd63 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 310);
  	public ZonedDecimalData zrpcpd64 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 315);
  	public ZonedDecimalData zrpcpd65 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 320);
  	public ZonedDecimalData zrpcpd66 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 325);
  	public ZonedDecimalData zrpcpd67 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 330);
  	public ZonedDecimalData zrpcpd68 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 335);
  	public ZonedDecimalData zrpcpd69 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 340);
  	public ZonedDecimalData zrpcpd70 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 345);
  	public ZonedDecimalData zrpcpd71 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 350);
  	public ZonedDecimalData zrpcpd72 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 355);
  	public ZonedDecimalData zrpcpd73 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 360);
  	public ZonedDecimalData zrpcpd74 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 365);
  	public ZonedDecimalData zrpcpd75 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 370);
  	public ZonedDecimalData zrpcpd76 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 375);
  	public ZonedDecimalData zrpcpd77 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 380);
  	public ZonedDecimalData zrpcpd78 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 385);
  	public ZonedDecimalData zrpcpd79 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 390);
  	public ZonedDecimalData zrpcpd80 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 395);
  	public FixedLengthStringData zrterms = new FixedLengthStringData(30).isAPartOf(tr529Rec, 417);
  	public ZonedDecimalData[] zrterm = ZDArrayPartOfStructure(10, 3, 0, zrterms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(30).isAPartOf(zrterms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrterm01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData zrterm02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
  	public ZonedDecimalData zrterm03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData zrterm04 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 9);
  	public ZonedDecimalData zrterm05 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData zrterm06 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData zrterm07 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData zrterm08 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 21);
  	public ZonedDecimalData zrterm09 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 24);
  	public ZonedDecimalData zrterm10 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 27);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(53).isAPartOf(tr529Rec, 447, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr529Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr529Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}