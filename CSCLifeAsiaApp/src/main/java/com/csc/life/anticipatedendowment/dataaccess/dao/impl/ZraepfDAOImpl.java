/*
 * File: ZraepfDAOImpl.java
 * Date: October 15, 2015
 * Author: CSC
 * 
 * Copyright (2015) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.dataaccess.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.ZraepfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Zraepf;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZraepfDAOImpl  extends BaseDAOImpl<Object> implements ZraepfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ZraepfDAOImpl.class);
	//ILIFE-1827 START by avemula
	/**
	 * Select ZRAE and CHDR records
	 * Retrieve all valid CHDR records corresponding to ZRAE
	 * records which are due according to the parameter entered
	 */
	public ResultSet getContractsForZRAE(SMARTAppVars appVars,  PackedDecimalData wsaaEffdate, PackedDecimalData wsaaEndListing) throws SQLException {
		
		ResultSet sqlzraepf1rs = null;
		PreparedStatement sqlzraepf1ps = null;
		Connection sqlzraepf1conn = null;
		String sqlzraepf1 = "";
		
		//sqlzraepf1 = " SELECT  A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX, A.ZRDUEDTE01, A.ZRDUEDTE02, A.ZRDUEDTE03, A.ZRDUEDTE04, A.ZRDUEDTE05, A.ZRDUEDTE06, A.ZRDUEDTE07, A.ZRDUEDTE08, A.PRCNT01, A.PRCNT02, A.PRCNT03, A.PRCNT04, A.PRCNT05, A.PRCNT06, A.PRCNT07, A.PRCNT08, A.NPAYDATE, C.CNTTYPE, C.AGNTNUM, C.COWNPFX, C.AGNTCOY, C.COWNNUM, EXTRACT(MONTH FROM TO_DATE(A.NPAYDATE,'YYYYMMDD')) AS MNTH, EXTRACT(YEAR FROM TO_DATE(A.NPAYDATE,'YYYYMMDD')) AS YR" +
		sqlzraepf1 = " SELECT  A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX, A.ZRDUEDTE01, A.ZRDUEDTE02, A.ZRDUEDTE03, A.ZRDUEDTE04, A.ZRDUEDTE05, A.ZRDUEDTE06, A.ZRDUEDTE07, A.ZRDUEDTE08, A.PRCNT01, A.PRCNT02, A.PRCNT03, A.PRCNT04, A.PRCNT05, A.PRCNT06, A.PRCNT07, A.PRCNT08, A.NPAYDATE, C.CNTTYPE, C.AGNTNUM, C.COWNPFX, C.AGNTCOY, C.COWNNUM, VM1DTA.INTEGRAL_EXTRACT('MONTH', A.NPAYDATE) AS MNTH, VM1DTA.INTEGRAL_EXTRACT('YEAR', A.NPAYDATE) AS YR" +
					 " FROM   " + appVars.getTableNameOverriden("ZRAEPF") + "  A,  " + appVars.getTableNameOverriden("CHDRPF") + "  C" +
					 " WHERE A.CHDRNUM = C.CHDRNUM" +
					 " AND C.VALIDFLAG = '1'" +
					 " AND A.VALIDFLAG = '1'" +
					 " AND A.NPAYDATE >= ?" +
					 " AND A.NPAYDATE <= ?" +
					 " AND C.CHDRCOY = '2'" +  /*ILIFE-5322*/
					 " ORDER BY C.CNTTYPE, A.NPAYDATE";
		/* Open the cursor (this runs the query)*/
		/*boolean sqlerrorflag = false;
		try {*/
		sqlzraepf1conn = appVars.getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.anticipatedendowment.dataaccess.ZraepfTableDAM(), new com.csc.fsu.general.dataaccess.ChdrpfTableDAM()});
		sqlzraepf1ps = appVars.prepareStatementEmbeded(sqlzraepf1conn, sqlzraepf1);
		appVars.setDBNumber(sqlzraepf1ps, 1, wsaaEffdate);
		appVars.setDBNumber(sqlzraepf1ps, 2, wsaaEndListing);
		sqlzraepf1rs = appVars.executeQuery(sqlzraepf1ps);
		/*}
		catch (SQLException ex){
			sqlerrorflag = true;
			appVars.setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}*/
		return sqlzraepf1rs;
	}
	
	//ILIFE-1827 END
	
   public Zraepf getZraepf(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx) {
	   
	   StringBuffer sql = new StringBuffer("SELECT * FROM VM1DTA.ZRAEPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=?");
	   sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
	   
	   LOGGER.info(sql.toString());
	   PreparedStatement ps = null;
	   ResultSet rs = null;
	   Zraepf zraepf = null;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6, plnsfx);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				zraepf = new Zraepf();
				zraepf.setChdrcoy(rs.getString("CHDRCOY"));
				zraepf.setChdrnum(rs.getString("CHDRNUM"));
				zraepf.setLife(rs.getString("LIFE"));
				zraepf.setCoverage(rs.getString("COVERAGE"));
				zraepf.setRider(rs.getString("RIDER"));
				zraepf.setPlnsfx(rs.getInt("PLNSFX"));
				zraepf.setZrpayopt01(rs.getString("ZRPAYOPT01"));
				zraepf.setZrpayopt02(rs.getString("ZRPAYOPT02"));
				zraepf.setZrpayopt03(rs.getString("ZRPAYOPT03"));
				zraepf.setZrpayopt04(rs.getString("ZRPAYOPT04"));
				zraepf.setZrpayopt05(rs.getString("ZRPAYOPT05"));
				zraepf.setZrpayopt06(rs.getString("ZRPAYOPT06"));
				zraepf.setZrpayopt07(rs.getString("ZRPAYOPT07"));
				zraepf.setZrpayopt08(rs.getString("ZRPAYOPT08"));
				zraepf.setBankkey(rs.getString("BANKKEY"));
				zraepf.setBankacckey(rs.getString("BANKACCKEY"));
				zraepf.setPayclt(rs.getString("PAYCLT"));
				zraepf.setTranno(rs.getInt("TRANNO"));
				zraepf.setZrduedte01(rs.getInt("ZRDUEDTE01"));
				zraepf.setZrduedte02(rs.getInt("ZRDUEDTE02"));
				zraepf.setZrduedte03(rs.getInt("ZRDUEDTE03"));
				zraepf.setZrduedte04(rs.getInt("ZRDUEDTE04"));
				zraepf.setZrduedte05(rs.getInt("ZRDUEDTE05"));
				zraepf.setZrduedte06(rs.getInt("ZRDUEDTE06"));
				zraepf.setZrduedte07(rs.getInt("ZRDUEDTE07"));
				zraepf.setZrduedte08(rs.getInt("ZRDUEDTE08"));
				zraepf.setNpaydate(rs.getInt("NPAYDATE"));
				zraepf.setPaymmeth01(rs.getString("PAYMMETH01"));
				zraepf.setPaymmeth02(rs.getString("PAYMMETH02"));
				zraepf.setPaymmeth03(rs.getString("PAYMMETH03"));
				zraepf.setPaymmeth04(rs.getString("PAYMMETH04"));
				zraepf.setPaymmeth05(rs.getString("PAYMMETH05"));
				zraepf.setPaymmeth06(rs.getString("PAYMMETH06"));
				zraepf.setPaymmeth07(rs.getString("PAYMMETH07"));
				zraepf.setPaymmeth08(rs.getString("PAYMMETH08"));
				zraepf.setPrcnt01(rs.getInt("PRCNT01"));
				zraepf.setPrcnt02(rs.getInt("PRCNT02"));
				zraepf.setPrcnt03(rs.getInt("PRCNT03"));
				zraepf.setPrcnt04(rs.getInt("PRCNT04"));
				zraepf.setPrcnt05(rs.getInt("PRCNT05"));
				zraepf.setPrcnt06(rs.getInt("PRCNT06"));
				zraepf.setPrcnt07(rs.getInt("PRCNT07"));
				zraepf.setPrcnt08(rs.getInt("PRCNT08"));
				zraepf.setTotamnt(rs.getBigDecimal("TOTAMNT"));
				zraepf.setFlag(rs.getString("FLAG"));
				zraepf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
				zraepf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
				zraepf.setPaycurr(rs.getString("PAYCURR"));
				zraepf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				zraepf.setValidflag(rs.getString("VALIDFLAG"));
				zraepf.setPaydte01(rs.getInt("PAYDTE01"));
				zraepf.setPaydte02(rs.getInt("PAYDTE02"));
				zraepf.setPaydte03(rs.getInt("PAYDTE03"));
				zraepf.setPaydte04(rs.getInt("PAYDTE04"));
				zraepf.setPaydte05(rs.getInt("PAYDTE05"));
				zraepf.setPaydte06(rs.getInt("PAYDTE06"));
				zraepf.setPaydte07(rs.getInt("PAYDTE07"));
				zraepf.setPaydte08(rs.getInt("PAYDTE08"));
				zraepf.setPaid01(rs.getBigDecimal("PAID01"));
				zraepf.setPaid02(rs.getBigDecimal("PAID02"));
				zraepf.setPaid03(rs.getBigDecimal("PAID03"));
				zraepf.setPaid04(rs.getBigDecimal("PAID04"));
				zraepf.setPaid05(rs.getBigDecimal("PAID05"));
				zraepf.setPaid06(rs.getBigDecimal("PAID06"));
				zraepf.setPaid07(rs.getBigDecimal("PAID07"));
				zraepf.setPaid08(rs.getBigDecimal("PAID08"));
			}
		
		 }catch(SQLException e) {
		  LOGGER.error("getZraepf()", e); 
		  throw new SQLRuntimeException(e);
		} finally {
		  close(ps, rs);			
	   }		
			    
		return zraepf;
			
   }
   
   public int updateZraepf(int currto, String validflag, long uniqueNumber) {
	   StringBuffer sql = new StringBuffer("UPDATE VM1DTA.ZRAEPF SET CURRTO=?, VALIDFLAG = ? WHERE UNIQUE_NUMBER=?");
	   LOGGER.info(sql.toString());
	   PreparedStatement ps = null;
	   int result = 0;
	   try {
		   ps = getPrepareStatement(sql.toString());
			ps.setInt(1, currto);
			ps.setString(2, validflag);
			ps.setLong(3, uniqueNumber);
			result = ps.executeUpdate();
	   }catch(SQLException e) {
			  LOGGER.error("updateZraepf()", e); 
			  throw new SQLRuntimeException(e);
			} finally {
			  close(ps, null);			
		   }	
	   return result;
   }
   
   public void insertZraeRecord(Zraepf zraepf) {
       
		StringBuffer sql  = new StringBuffer("INSERT INTO ZRAEPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,ZRDUEDTE01,");
		sql.append("ZRDUEDTE02,ZRDUEDTE03,ZRDUEDTE04,ZRDUEDTE05,ZRDUEDTE06,ZRDUEDTE07,ZRDUEDTE08,PRCNT01,PRCNT02,PRCNT03,PRCNT04,PRCNT05,PRCNT06,PRCNT07,");
		sql.append("PRCNT08,PAYDTE01,PAYDTE02,PAYDTE03,PAYDTE04,PAYDTE05,PAYDTE06,PAYDTE07,PAYDTE08,PAID01,PAID02,PAID03,PAID04,PAID05,PAID06,PAID07, ");
		sql.append("PAID08,PAYMMETH01,PAYMMETH02,PAYMMETH03,PAYMMETH04,PAYMMETH05,PAYMMETH06,PAYMMETH07,PAYMMETH08,ZRPAYOPT01,ZRPAYOPT02,ZRPAYOPT03,");
		sql.append("ZRPAYOPT04,ZRPAYOPT05,ZRPAYOPT06,ZRPAYOPT07,ZRPAYOPT08,TOTAMNT,NPAYDATE,PAYCLT,BANKKEY,BANKACCKEY,PAYCURR,USRPRF,JOBNM,DATIME,FLAG,");
		sql.append("APCAPLAMT,APINTAMT,APLSTCAPDATE,APNXTCAPDATE,APLSTINTBDTE,APNXTINTBDTE,WDRGPYMOP,WDBANKKEY,WDBANKACCKEY)");
		sql.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      
           PreparedStatement ps = getPrepareStatement(sql.toString());
           int count;
           try {        	    
           	    ps.setString(1,zraepf.getChdrcoy());	
           	    ps.setString(2,zraepf.getChdrnum());	
           	    ps.setString(3,zraepf.getLife());	
           	    ps.setString(4,zraepf.getCoverage());	
           	    ps.setString(5,zraepf.getRider());	
           	    ps.setInt(6,zraepf.getPlnsfx());	
           	    ps.setString(7,zraepf.getValidflag());	
           	    ps.setInt(8,zraepf.getTranno());	
           	    ps.setInt(9,zraepf.getCurrfrom());	
           	    ps.setInt(10,zraepf.getCurrto());	
           	    ps.setInt(11,zraepf.getZrduedte01());	
           	    ps.setInt(12,zraepf.getZrduedte02());	
           	    ps.setInt(13,zraepf.getZrduedte03());	
           	    ps.setInt(14,zraepf.getZrduedte04());	
           	    ps.setInt(15,zraepf.getZrduedte05());	
           	    ps.setInt(16,zraepf.getZrduedte06());	
           	    ps.setInt(17,zraepf.getZrduedte07());	
           	    ps.setInt(18,zraepf.getZrduedte08());	
           	    ps.setInt(19,zraepf.getPrcnt01());	
           	    ps.setInt(20,zraepf.getPrcnt02());	
           	    ps.setInt(21,zraepf.getPrcnt03());	
           	    ps.setInt(22,zraepf.getPrcnt04());	
           	    ps.setInt(23,zraepf.getPrcnt05());	
           	    ps.setInt(24,zraepf.getPrcnt06());	
           	    ps.setInt(25,zraepf.getPrcnt07());	
           	    ps.setInt(26,zraepf.getPrcnt08());	
           	    ps.setInt(27,zraepf.getPaydte01());	
           	    ps.setInt(28,zraepf.getPaydte02());	
           	    ps.setInt(29,zraepf.getPaydte03());	
           	    ps.setInt(30,zraepf.getPaydte04());	
           	    ps.setInt(31,zraepf.getPaydte05());	
           	    ps.setInt(32,zraepf.getPaydte06());	
           	    ps.setInt(33,zraepf.getPaydte07());	
           	    ps.setInt(34,zraepf.getPaydte08());	
           	    ps.setBigDecimal(35,zraepf.getPaid01());	
           	    ps.setBigDecimal(36,zraepf.getPaid02());	
           	    ps.setBigDecimal(37,zraepf.getPaid03());	
           	    ps.setBigDecimal(38,zraepf.getPaid04());	
           	    ps.setBigDecimal(39,zraepf.getPaid05());	
           	    ps.setBigDecimal(40,zraepf.getPaid06());	
           	    ps.setBigDecimal(41,zraepf.getPaid07());	
           	    ps.setBigDecimal(42,zraepf.getPaid08());	
           	    ps.setString(43,zraepf.getPaymmeth01());	
           	    ps.setString(44,zraepf.getPaymmeth02());	
           	    ps.setString(45,zraepf.getPaymmeth03());	
           	    ps.setString(46,zraepf.getPaymmeth04());	
           	    ps.setString(47,zraepf.getPaymmeth05());	
           	    ps.setString(48,zraepf.getPaymmeth06());	
           	    ps.setString(49,zraepf.getPaymmeth07());	
           	    ps.setString(50,zraepf.getPaymmeth08());	
           	    ps.setString(51,zraepf.getZrpayopt01());	
           	    ps.setString(52,zraepf.getZrpayopt02());	
           	    ps.setString(53,zraepf.getZrpayopt03());	
           	    ps.setString(54,zraepf.getZrpayopt04());	
           	    ps.setString(55,zraepf.getZrpayopt05());	
           	    ps.setString(56,zraepf.getZrpayopt06());	
           	    ps.setString(57,zraepf.getZrpayopt07());	
           	    ps.setString(58,zraepf.getZrpayopt08());
           	    ps.setBigDecimal(59,zraepf.getTotamnt());
           	    ps.setInt(60,zraepf.getNpaydate());
           	    ps.setString(61,zraepf.getPayclt());
           	    ps.setString(62,zraepf.getBankkey());
           	    ps.setString(63,zraepf.getBankacckey());
           	    ps.setString(64,zraepf.getPaycurr());
           	    ps.setString(65,getUsrprf());
           	    ps.setString(66,getJobnm());
           	    ps.setTimestamp(67,getDatime());
           	    ps.setString(68,zraepf.getFlag());
           	    ps.setBigDecimal(69,zraepf.getApcaplamt());
           	    ps.setBigDecimal(70,zraepf.getApintamt());
           	    ps.setInt(71,(zraepf.getAplstcapdate()== null? 0 : zraepf.getAplstcapdate()));
           	    ps.setInt(72,(zraepf.getApnxtcapdate()== null? 0 : zraepf.getApnxtcapdate()));
           	    ps.setInt(73,(zraepf.getAplstintbdte()== null? 0 : zraepf.getAplstintbdte()));
           	    ps.setInt(74,(zraepf.getApnxtintbdte()== null? 0 : zraepf.getApnxtintbdte())); 
           	    ps.setString(75,zraepf.getWdrgpymop());
           	    ps.setString(76,zraepf.getWdbankkey());
           	    ps.setString(77,zraepf.getWdbankacckey());
           	    
                count=ps.executeUpdate();
                         
           } catch (SQLException e) {
               LOGGER.error("insertZraeRcd()", e);
               throw new SQLRuntimeException(e);
           } finally {
               close(ps, null);
           }
   }
}
