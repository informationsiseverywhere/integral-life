package com.csc.life.anticipatedendowment.dataaccess.dao;


import java.util.List;


import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br535TempDAO extends BaseDAO<Object>{
	public List<Loanpf> loadDataByNEWINTCP(String tableID, String memName,int intBatchExtractSize, int batchID); //ILIFE-8840
}
