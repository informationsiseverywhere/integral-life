package com.csc.life.anticipatedendowment.dataaccess.model;

import java.io.Serializable;


public class Bd5hvDTO implements Serializable{
	
	private int cnt;
	private int tranno = 0;
	private String cnttype = "";	
	private String chdrpfx = "";
	private String servunit = "";
	private int occdate = 0;
	private int ccdate = 0;
	private String cownpfx  = "";
	private String cowncoy = "";
	private String cownnum = "";
	private String collchnl = "";
	private String cntbranch = "";
	private String agntpfx = "";
	private String agntcoy = "";
	private String agntnum = "";	
	private String clntpfx = "";
	private String clntcoy = "";
	private String clntnum = "";
	private String mandref = "";
	private int ptdate = 0;
	private String billchnl = "";
	private String billfreq = "";
	private String billcurr = "";
	private int nextdate = 0;
	private String bankkey = "";
	private String bankacckey = "";
	private String mandstat = "";
	private String facthous = "";
	private String cntcurr = "";
	private int polsum = 0;
	private int polinc = 0;
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getServunit() {
		return servunit;
	}
	public void setServunit(String servunit) {
		this.servunit = servunit;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public int getCcdate() {
		return ccdate;
	}
	public void setCcdate(int ccdate) {
		this.ccdate = ccdate;
	}
	public String getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}
	public String getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(String collchnl) {
		this.collchnl = collchnl;
	}
	public String getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}
	public String getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(String agntpfx) {
		this.agntpfx = agntpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(String clntpfx) {
		this.clntpfx = clntpfx;
	}
	public String getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(String clntcoy) {
		this.clntcoy = clntcoy;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}
	public int getNextdate() {
		return nextdate;
	}
	public void setNextdate(int nextdate) {
		this.nextdate = nextdate;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public int getPolsum() {
		return polsum;
	}
	public void setPolsum(int polsum) {
		this.polsum = polsum;
	}
	public int getPolinc() {
		return polinc;
	}
	public void setPolinc(int polinc) {
		this.polinc = polinc;
	}
	
	
}
