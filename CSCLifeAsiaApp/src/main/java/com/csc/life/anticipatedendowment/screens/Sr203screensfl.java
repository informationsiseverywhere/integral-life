package com.csc.life.anticipatedendowment.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sr203screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//ILIFE-4361
	//public static final int[] pfInds = new int[] {4, 17, 22, 5, 18, 23, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	//ILIFE-4361
	//public static int[] affectedInds = new int[] {6, 7, 4, 5, 1, 2, 3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 21, 27, 60}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
		//ILIFE-4361 starts
//		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr203screensfl.getRowCount())) {
//			ind3.setOn();
//			return;
//		}
//		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr203screensfl, 
//			sv.Sr203screensflWritten , ind2, ind3, maxRecords);
//		if (ind2.isOn() || ind3.isOn()) {
//			return;
//		}
//		setSubfileData(tm.bufferedRow, av, pv);
//		if (av.getInd(nextChangeIndicator)) {
//			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
//		} else {
//			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
//		}
//		clearInds(av, pfInds);
//		tm.write();
		//ILIFE-4361 ends
		Subfile.write(av, pv, ind2, ind3, sv.sr203screensfl, maxRecords, sv.Sr203screensflWritten,  ROUTINE, sv.getScreenSflPfInds(),nextChangeIndicator);
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		//ILIFE-4361 starts
//		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
//		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr203screensfl, ind2);
//		setSubfileData(tm.bufferedRow, av, pv);
//		if (av.getInd(nextChangeIndicator)) {
//			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
//		} else {
//			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
//		}
//		tm.update();
		//ILIFE-4361 ends
		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
		//ILIFE-4361
		Subfile.update( av,  pv,ind2,ROUTINE,sv.sr203screensfl,nextChangeIndicator);
		
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
		//ILIFE-4361 stats
//		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr203screensfl, ind2, ind3, sflIndex);
//		getSubfileData(dm, av, pv);
//		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
//		// we return to the start of the subfile for subsequent calls
//		if (ind3.isOn() && sv.Sr203screensflWritten.gt(0))
//		{
//			sv.sr203screensfl.setCurrentIndex(0);
//			sv.Sr203screensflWritten.set(0);
//		}
//		restoreInds(dm, av, affectedInds);
		Subfile.readNextChangedRecord( av,  pv,
				 ind2,  ind3,  sflIndex,sv.sr203screensfl, ROUTINE,sv.Sr203screensflWritten,sv.getScreenSflAffectedInds());
		//ILIFE-4361 ends
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
		//ILIFE-4361
		Subfile.chain( av,  pv, record,  ind2,  ind3, sv.sr203screensfl,  ROUTINE, sv.getScreenSflAffectedInds());
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}
	
	//ILIFE-4361 starts

//	public static void getSubfileData(DataModel dm, COBOLAppVars av,
//		 VarModel pv) {
//		if (dm != null) {
//			Sr203ScreenVars screenVars = (Sr203ScreenVars) pv;
//			if (screenVars.screenIndicArea.getFieldName() == null) {
//				screenVars.screenIndicArea.setFieldName("screenIndicArea");
//				screenVars.hlifcnum.setFieldName("hlifcnum");
//				screenVars.curramt.setFieldName("curramt");
//				screenVars.chdrnum.setFieldName("chdrnum");
//				screenVars.tranamt.setFieldName("tranamt");
//			}
//			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
//			screenVars.hlifcnum.set(dm.getField("hlifcnum"));
//			screenVars.curramt.set(dm.getField("curramt"));
//			screenVars.chdrnum.set(dm.getField("chdrnum"));
//			screenVars.tranamt.set(dm.getField("tranamt"));
//		}
//	}
//
//	public static void setSubfileData(DataModel dm, COBOLAppVars av,
//		 VarModel pv) {
//		if (dm != null) {
//			Sr203ScreenVars screenVars = (Sr203ScreenVars) pv;
//			if (screenVars.screenIndicArea.getFieldName() == null) {
//				screenVars.screenIndicArea.setFieldName("screenIndicArea");
//				screenVars.hlifcnum.setFieldName("hlifcnum");
//				screenVars.curramt.setFieldName("curramt");
//				screenVars.chdrnum.setFieldName("chdrnum");
//				screenVars.tranamt.setFieldName("tranamt");
//			}
//			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
//			dm.getField("hlifcnum").set(screenVars.hlifcnum);
//			dm.getField("curramt").set(screenVars.curramt);
//			dm.getField("chdrnum").set(screenVars.chdrnum);
//			dm.getField("tranamt").set(screenVars.tranamt);
//		}
//	}
	
	//ILIFE-4361 ends

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr203screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		//ILIFE-4361 starts
//		gt.set1stScreenRow();
//		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
//		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
//		clearFormatting(pv);
		//ILIFE-4361 ends
		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
		//ILIFE-4361 
		Subfile.set1stScreenRow( gt,  appVars,  pv,sv.getScreenSflAffectedInds());
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		//ILIFE-4361 starts
//		gt.setNextScreenRow();
//		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
//		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
//		clearFormatting(pv);
		//ILIFE-4361 ends
		Sr203ScreenVars sv = (Sr203ScreenVars) pv;
		//ILIFE-4361 
		Subfile.setNextScreenRow( gt,  appVars,  pv,sv.getScreenSflAffectedInds());
	}

	public static void clearFormatting(VarModel pv) {
		//ILIFE-4361 starts
//		Sr203ScreenVars screenVars = (Sr203ScreenVars)pv;
//		screenVars.screenIndicArea.clearFormatting();
//		screenVars.hlifcnum.clearFormatting();
//		screenVars.curramt.clearFormatting();
//		screenVars.chdrnum.clearFormatting();
//		screenVars.tranamt.clearFormatting();
//		clearClassString(pv);
		Subfile.clearFormatting(pv);
		//ILIFE-4361 ends
	}

	//ILIFE-4361 starts
//	public static void clearClassString(VarModel pv) {
//		Sr203ScreenVars screenVars = (Sr203ScreenVars)pv;
//		screenVars.screenIndicArea.setClassString("");
//		screenVars.hlifcnum.setClassString("");
//		screenVars.curramt.setClassString("");
//		screenVars.chdrnum.setClassString("");
//		screenVars.tranamt.setClassString("");
//	}
	//ILIFE-4361 ends

/**
 * Clear all the variables in Sr203screensfl
 */
	public static void clear(VarModel pv) {
		Subfile.clear(pv);
		
	}
}
