/*
 * File: Pr203.java
 * Date: 30 August 2009 1:16:47
 * Author: Quipoz Limited
 * 
 * Class transformed from PR203.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
//MIBT-242 STARTS
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

//MIBT-242 ENDS
import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.procedures.Addrtrn;
import com.csc.fsu.financials.tablestructures.Tr204rec;
import com.csc.fsu.general.dataaccess.BbalTableDAM;
import com.csc.fsu.general.dataaccess.RbnkTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Cktrdsc;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Addrtrnrec;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Cktrdscrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Wsspdocs;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3698rec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.anticipatedendowment.screens.Sr203ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Gettim;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*      *
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*      *
*REMARKS.
*
* The remarks section should detail the main processes of the 
* program, and any special functions.
*
*****************************************************************
* </pre>
*/
public class Pr203 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(Pr203.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR203");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).setUnsigned();
	private final int wsaaSflPage = 10;
	private FixedLengthStringData wsaaReceipt = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaAmountRecvd = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaPolisyAmt = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaStampduty = new ZonedDecimalData(17, 2).setUnsigned();
	private FixedLengthStringData wsaaCntnum = new FixedLengthStringData(8);
	private String wsaaSign = "";
	private String wsaaTrnseq = "";
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaClientNumber = new FixedLengthStringData(8).init(SPACES);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(30, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
		/* WSAA-GENLKEY-CODE */
	private FixedLengthStringData wsaaGenlcde = new FixedLengthStringData(20);

	private FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(wsaaGenlcde, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(14).isAPartOf(filler4, 6);
		/* USERID */
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaUserid = new FixedLengthStringData(10).isAPartOf(wsaaUser, 0, REDEFINE);
	private ZonedDecimalData wsaaUserNo = new ZonedDecimalData(6, 0).isAPartOf(wsaaUserid, 0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaPageNo = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaTotRec = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaTotChdrnum = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTotTranamt = new ZonedDecimalData(18, 2);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaBillcurr = new FixedLengthStringData(3).init(SPACES);
		/* WSAA-LETRQST-TYPE */
	private FixedLengthStringData wsaaLetterType = new FixedLengthStringData(7);

	private FixedLengthStringData filler7 = new FixedLengthStringData(7).isAPartOf(wsaaLetterType, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaLtrContr = new FixedLengthStringData(3).isAPartOf(filler7, 0);
	private FixedLengthStringData wsaaLtrTrans = new FixedLengthStringData(4).isAPartOf(filler7, 3);
	private String wsaaTr204NotFound = "";

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String tr204 = "TR204";
	private static final String tr384 = "TR384";
	private static final String t5645 = "T5645";
	private static final String t7508 = "T7508";
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String payrrec = "PAYRREC";
	private static final String rbnkrec = "RBNKREC";
	private static final String clrfrec = "CLRFREC";
	private static final String itemrec = "ITEMREC";
	private static final String bbalrec = "BBALREC";
	private BbalTableDAM bbalIO = new BbalTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	
	//IJTI-1310 starts
	protected RbnkTableDAM rbnkIO = getRbnkTableDAM();
	
	/**
	 * Getter for RbnkTableDAM
	 * 
	 * @return - RbnkTableDAM instance
	 */
	public RbnkTableDAM getRbnkTableDAM() {
		return new RbnkTableDAM();
	}
	//IJTI-1310 ends
	
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Cktrdscrec cktrdscrec = new Cktrdscrec();
	private T5645rec t5645rec = new T5645rec();
	private Tr204rec tr204rec = new Tr204rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Addrtrnrec addrtrnrec = new Addrtrnrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Alocnorec alocnorec = new Alocnorec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T7508rec t7508rec = new T7508rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspdocs wsspdocs = new Wsspdocs();
	//ILIFE-4361
	private Sr203ScreenVars sv = getPScreenVars();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	private SimpleDateFormat integralDateFormat = new SimpleDateFormat("yyyyMMdd");
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private boolean ctaxPermission = false;
	private Tr59xrec tr59xrec = new Tr59xrec();
	String CurTaxFromDate,PreTaxFromDate,CurrTaxtoDate;
	/*ILIFE-8098 Start*/
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);	/*ILIFE-8098 */
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);/*ILIFE-8098 */
	Covtpf covtpf = null; /*ILIFE-8098 */
	Covrpf covrpf = null; /*ILIFE-8098 */
	List<Covtpf> covtpflist = new ArrayList<Covtpf>();
	List<Covrpf> covrpflist = new ArrayList<Covrpf>();
	private String  singpremType;
	private BigDecimal singp;
	/*ILIFE-8098 END*/
	private boolean billingCollectionJpnFlag;
	private static final String BTPRO027 = "BTPRO027";
	private static final String t3698 = "T3698";
	private T3698rec t3698rec = new T3698rec();


	/**
	 * ILIFE-4361
	 * 
	 * @return Sr203ScreenVars
	 */
	protected Sr203ScreenVars getPScreenVars()
	{
		return ScreenProgram.getScreenVars(Sr203ScreenVars.class);
	}
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1290, 
		exit2090, 
		maintainBalance2650
	}

	public Pr203() {
		super();
		screenVars = sv;
		new ScreenModel("Sr203", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaScrnStatuz.set(SPACES);
		wsaaTotRec.set(0);
		wsaaTotChdrnum.set(0);
		wsaaTotTranamt.set(0);
		wsaaPageNo.set(1);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaGenlcde.set(wsspdocs.genlkey);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		/* Clear subfile.*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR203", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Set screen fields*/
		sv.zrnofrec01.set(ZERO);
		sv.zrnofrec02.set(ZERO);
		sv.totalamt01.set(ZERO);
		sv.totalamt02.set(ZERO);
		sv.trandatex.set(varcom.maxdate);
		/* MOVE 'Y'                    TO SR203-TCSD.                   */
		/* MOVE 'Y'                    TO SR203-LETTER-PRINT-FLAG.      */
		sv.tcsd.set("N");
		sv.letterPrintFlag.set("N");
		/* Get today's date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sv.trandatex.set(datcon1rec.intDate);
		callProgram(Gettim.class, wsaaTime);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		getTransDesc1100();
		loadHeading1200();
		/* Load the subfile {SFL-PAGE} times with blank lines.*/
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaSflPage);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			loadBlankSubfile1300();
		}
		scrnparams.subfileMore.set("Y");
		scrnparams.subfileRrn.set(1);
		loadT5645Info1400();
		readStampdutyTable1500();
		billingCollectionJpnFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),BTPRO027, appVars, "IT");
		if(billingCollectionJpnFlag) {
			setSacscode();
		}
		else {
			sv.sacsflagOut[Varcom.nd.toInt()].set("Y");
		}
	}

protected void getTransDesc1100()
	{
		getTransDesc1110();
	}

protected void getTransDesc1110()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
	}

protected void loadHeading1200()
	{
		try {
			loadHeading1210();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadHeading1210()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
		loopEndVar2.set(wsaaX);
		for (int loopVar2 = 0; !(isEQ(loopVar2, loopEndVar2.toInt())); loopVar2 += 1){
			moveChar1230();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.trandsc.set(wsaaHeading);
		goTo(GotoLabel.exit1290);
	}

protected void moveChar1230()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void loadBlankSubfile1300()
	{
		/*LOAD-BLANK-SUBFILE*/
		/*    INITIALIZE SR203-SUBFILE-FIELDS.*/
		sv.chdrnum.set(SPACES);
		sv.hlifcnum.set(SPACES);
		sv.tranamt.set(0);
		sv.curramt.set(0);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR203", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadT5645Info1400()
	{
		loadDataFromT56451410();
	}

protected void loadDataFromT56451410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.begn);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readStampdutyTable1500()
	{
		readStampdutyTable1510();
	}

protected void readStampdutyTable1510()
	{
		/* This part will read in Stamp Duty rates table.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemtabl(tr204);
		itdmIO.setItemitem(wsspcomn.ledgcurr);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
			|| isNE(itdmIO.getItemtabl(), tr204)
			|| isNE(itdmIO.getItemitem(), wsspcomn.ledgcurr)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				wsaaTr204NotFound = "Y";
				/*****        MOVE R011           TO SR203-LETPRTFLAG-ERR           */
			}
			else {
				wsaaTr204NotFound = "N";
				tr204rec.tr204Rec.set(itdmIO.getGenarea());
			}
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.statuz.set(varcom.oK);
		sv.zrnofrec[2].set(wsaaTotChdrnum);
		sv.totalamt[2].set(wsaaTotTranamt);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateCtlScreen2020();
			checkForErrors2050();
			validateSflScreen2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SR203IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SR203-DATA-AREA                         */
		/*                         SR203-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateCtlScreen2020()
	{
		sv.errorIndicators.set(SPACES);
		if (isEQ(sv.zrnofrec[1], 0)) {
			sv.zrnofrec01Err.set(errorsInner.f579);
		}
		if (isEQ(sv.totalamt[1], 0)) {
			sv.totalamt01Err.set(errorsInner.f645);
		}
		if (isGT(sv.trandatex, wsaaToday)) {
			sv.trandatexErr.set(errorsInner.f990);
		}
		if (!(isEQ(sv.tcsd, "Y")
		|| isEQ(sv.tcsd, "N"))) {
			sv.tcsdErr.set(errorsInner.g709);
		}
		if (!(isEQ(sv.letterPrintFlag, "Y")
		|| isEQ(sv.letterPrintFlag, "N"))) {
			sv.letprtflagErr.set(errorsInner.g709);
		}
		if (isEQ(sv.tcsd, "Y")
		&& isEQ(wsaaTr204NotFound, "Y")) {
			sv.tcsdErr.set(errorsInner.r011);
		}
		if (isNE(wsspcomn.ledgcurr, SPACES)
		&& isNE(sv.totalamt[1], ZERO)) {
			zrdecplrec.currency.set(wsspcomn.ledgcurr);
			zrdecplrec.amountIn.set(sv.totalamt[1]);
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.totalamt[1])) {
				sv.totalamt01Err.set(errorsInner.rfik);
			}
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSflScreen2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR203", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		sv.totalamt[2].set(wsaaTotTranamt);
		sv.zrnofrec[2].set(wsaaTotChdrnum);
		if (isEQ(wsaaScrnStatuz, varcom.rolu)
		&& isEQ(wsspcomn.edterror, varcom.oK)) {
			if ((setPrecision(wsaaTotRec, 0)
			&& isLT(wsaaTotRec, (mult(wsaaPageNo, wsaaSflPage))))) {
				scrnparams.errorCode.set(errorsInner.f499);
				wsspcomn.edterror.set("Y");
			}
			else {
				PackedDecimalData loopEndVar3 = new PackedDecimalData(7, 0);
				loopEndVar3.set(wsaaSflPage);
				for (int loopVar3 = 0; !(isEQ(loopVar3, loopEndVar3.toInt())); loopVar3 += 1){
					loadBlankSubfile1300();
				}
				scrnparams.subfileMore.set("Y");
				wsspcomn.edterror.set("Y");
				wsaaPageNo.add(1);
			}
			return ;
		}
		if (isNE(wsaaTotChdrnum, sv.zrnofrec[1])) {
			sv.zrnofrec01Err.set(errorsInner.e198);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(wsaaTotTranamt, sv.totalamt[1])) {
			sv.totalamt01Err.set(errorsInner.e198);
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case maintainBalance2650: 
					maintainBalance2650();
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (!billingCollectionJpnFlag && isEQ(sv.chdrnum, SPACES)
		&& isEQ(sv.tranamt, 0)) {
			goTo(GotoLabel.maintainBalance2650);
		}
		if(billingCollectionJpnFlag && isEQ(sv.chdrnum, SPACES) && isEQ(sv.tranamt, 0)
			&& isEQ(sv.sacscode, SPACES) && isEQ(sv.sacstypw, SPACE)) {
				goTo(GotoLabel.maintainBalance2650);
		}
		if(billingCollectionJpnFlag) {
			boolean errorflag = false;
			if(isEQ(sv.tranamt, 0)) {
				sv.tranamtErr.set(errorsInner.e199);
				errorflag = true;
			}
						
			if(isEQ(sv.chdrnum, SPACES)) {
				sv.chdrnumErr.set(errorsInner.e696);
				errorflag = true;
			}
				
			if(isEQ(sv.sacscode, SPACES)) {
				sv.sacscodeErr.set(errorsInner.rulw);
				errorflag = true;
			}
			if(isEQ(sv.sacstypw, SPACE)) {
				sv.sacstypwErr.set(errorsInner.rulv);
				errorflag = true;
			}
			if(errorflag) {
				wsspcomn.edterror.set("Y");
			}
			if(isEQ(sv.chdrnum, 0))
				sv.chdrnum.set(SPACES);
		}
		if (isNE(sv.chdrnum, SPACES)) {
			chdrlnbIO.setDataKey(SPACES);
			chdrlnbIO.setChdrcoy(wsspcomn.company);
			chdrlnbIO.setChdrnum(sv.chdrnum);
			chdrlnbIO.setFormat(chdrlnbrec);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
			&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
				sv.chdrnumErr.set(errorsInner.h946);
			}
		}
		if (isEQ(sv.chdrnum, SPACES)
				&& isNE(sv.tranamt, 0)) {
			sv.chdrnumErr.set(errorsInner.f259);
		}
		if(!billingCollectionJpnFlag && (isEQ(sv.tranamt, 0)
						&& isNE(sv.chdrnum, SPACES))) {
					sv.tranamtErr.set(errorsInner.f645);
		}
		if (isNE(wsspcomn.ledgcurr, SPACES)
		&& isNE(sv.tranamt, ZERO)) {
			zrdecplrec.currency.set(wsspcomn.ledgcurr);
			zrdecplrec.amountIn.set(sv.tranamt);
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.tranamt)) {
				sv.tranamtErr.set(errorsInner.rfik);
			}
		}
	}

protected void maintainBalance2650()
	{
		compute(wsaaTotTranamt, 2).set(sub(add(wsaaTotTranamt, sv.tranamt), sv.curramt));
		if (isEQ(sv.hlifcnum, SPACES)
		&& isNE(sv.chdrnum, SPACES)) {
			wsaaTotChdrnum.add(1);
			wsaaTotRec.add(1);
		}
		if (isNE(sv.hlifcnum, SPACES)
		&& isEQ(sv.chdrnum, SPACES)) {
			wsaaTotChdrnum.subtract(1);
			wsaaTotRec.subtract(1);
		}
		/*UPDATE-SFL-LINE*/
		sv.hlifcnum.set(sv.chdrnum);
		sv.curramt.set(sv.tranamt);
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR203", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR203", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsaaScrnStatuz, varcom.kill)) {
			return ;
		}
		/*  Update database files as required*/
		scrnparams.statuz.set(varcom.oK);
		for (scrnparams.subfileRrn.set(1); !(isEQ(scrnparams.statuz, varcom.mrnf)); scrnparams.subfileRrn.add(1)){
			readSfl3100();
		}
		updateBankBalance3900();
		scrnparams.statuz.set(varcom.oK);
		appVars.commit();
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void readSfl3100()
	{
		/*READ-SUBFILE-LOOP*/
		scrnparams.function.set(varcom.sread);
		processScreen("SR203", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.mrnf)) {
			return ;
		}
		/* Update database with SFL data (TRANSEQ incremented) via*/
		/* update subroutine.*/
		if (isNE(sv.chdrnum, SPACES)) {
			updateRoutine3200();
		}
		/*END-OF-LOOP*/
	}

protected void updateRoutine3200()
	{
		updateRoutine3201();
	}

protected void updateRoutine3201()
	{
		wsaaCntnum.set(sv.chdrnum);
		wsaaAmountRecvd.set(sv.tranamt);
		createReceiptNo3300();
		getTrandesc3400();
		calculatePolicyAmt3600();
		writeRtrnHeader3500();
		writeRbnkHeader3510();
		writeRtrnD13700();
		if (isEQ(sv.tcsd, "Y")
		&& isGT(wsaaStampduty, 0)) {
			writeRtrnD23760();
		}
		dryProcessing8000();
		if (isEQ(sv.letterPrintFlag, "Y")) {
			writeLetrqst3800();
		}
		updateZctxAmount();
	}
protected BigDecimal Calculateamt(BigDecimal total,BigDecimal pct){
	BigDecimal amt=total.multiply(pct);
	BigDecimal num = new BigDecimal(100);
	amt=amt.divide(num);
	return amt;
	
}
protected void updateZctxAmount(){
	ctaxPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPROP07", appVars, "IT"); 
    Chdrpf chdrpf=new Chdrpf();
    chdrpf=chdrpfDAO.getRecordByChdrnum("CH",wsspcomn.company.toString(), sv.chdrnum.toString(),sv.trandatex.toString());
    if(chdrpf!=null){
	Itempf itempf=null;
	itempf=itempfDAO.findItemByItdm("2","TR59X", chdrpf.getCnttype());
    if (itempf!=null && ctaxPermission) {
	tr59xrec.tr59xrec.set(StringUtil.rawToString(itempf.getGenarea()));
	Zctxpf zctxpf=new Zctxpf();
	autogenerateFromTaxDate();
    zctxpf.setDatefrm(Integer.parseInt(CurTaxFromDate));
	zctxpf.setChdrnum(sv.chdrnum.toString());
	Zctxpf zctxpfrec=new Zctxpf();
	zctxpfrec=zctxpfDAO.readRecordByDate(zctxpf);
	/*ILIFE-8098 Start*/
			covtpflist = covtpfDAO.getSingpremtype(wsspcomn.company.toString(),sv.chdrnum.toString());
			if(!ObjectUtils.isEmpty(covtpflist)){
			covtpf = covtpflist.get(0);
			singpremType=covtpf.getSingpremtype();
			singp=covtpf.getSingp();
			}
			if(StringUtils.isEmpty(singpremType)){
				covrpflist = covrpfDAO.getSingpremtype(wsspcomn.company.toString(),sv.chdrnum.toString());
				if(!ObjectUtils.isEmpty(covrpflist)){
				covrpf = covrpflist.get(0);
				singpremType=covrpf.getSingpremtype();
				singp=covrpf.getSingp();
				}
			
			}
			if(zctxpfrec!=null){
				if(!StringUtils.isEmpty(singpremType) && singpremType.equalsIgnoreCase("ROP") && zctxpfrec.getRollflag().equalsIgnoreCase("N")){
					zctxpf.setAmount((sub(sv.totalamt01,singp)).getbigdata());
					if(zctxpf.getAmount().intValue()<= 0){
						zctxpf.setAmount(BigDecimal.ZERO);
					}
					zctxpf.setZsgtamt((add(Calculateamt(zctxpf.getAmount(),zctxpfrec.getZsgtpct()),zctxpfrec.getZsgtamt())).getbigdata());
					zctxpf.setZoeramt((add(Calculateamt(zctxpf.getAmount(),zctxpfrec.getZoerpct()),zctxpfrec.getZoeramt())).getbigdata());
					zctxpf.setZdedamt((add(Calculateamt(zctxpf.getAmount(),zctxpfrec.getZdedpct()),zctxpfrec.getZdedamt())).getbigdata());
					zctxpf.setZundamt((add(Calculateamt(zctxpf.getAmount(),zctxpfrec.getZundpct()),zctxpfrec.getZundamt())).getbigdata());
					zctxpf.setZspsamt((add(Calculateamt(zctxpf.getAmount(),zctxpfrec.getZspspct()),zctxpfrec.getZspsamt())).getbigdata());
					zctxpf.setZslrysamt((add(Calculateamt(zctxpf.getAmount(),zctxpfrec.getZslryspct()),zctxpfrec.getZslrysamt())).getbigdata()); //ILIFE-7345 
					zctxpf.setRollflag("Y");
					
				}else{
				
				zctxpf.setAmount((add(zctxpfrec.getAmount(),sv.tranamt.getbigdata())).getbigdata());
				zctxpf.setZsgtamt((add(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZsgtpct()),zctxpfrec.getZsgtamt())).getbigdata());
				zctxpf.setZoeramt((add(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZoerpct()),zctxpfrec.getZoeramt())).getbigdata());
				zctxpf.setZdedamt((add(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZdedpct()),zctxpfrec.getZdedamt())).getbigdata());
				zctxpf.setZundamt((add(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZundpct()),zctxpfrec.getZundamt())).getbigdata());
				zctxpf.setZspsamt((add(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZspspct()),zctxpfrec.getZspsamt())).getbigdata());
				zctxpf.setZslrysamt((add(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZslryspct()),zctxpfrec.getZslrysamt())).getbigdata()); //ILIFE-7345
				zctxpf.setRollflag(zctxpfrec.getRollflag());
				}/*ILIFE-8098 END*/ 
		zctxpfDAO.updateAmount(zctxpf);
	                   }
	else{
		zctxpf.setDatefrm(Integer.parseInt(PreTaxFromDate));
		zctxpfrec=zctxpfDAO.readRecordByDate(zctxpf);
		if(zctxpfrec!=null){
		 zctxpf.setChdrcoy(wsspcomn.company.toString());
  	     zctxpf.setClntnum(chdrpf.getCownnum());
  	     zctxpf.setCnttype(chdrpf.getCnttype());
  	     zctxpf.setZsgtpct(zctxpfrec.getZsgtpct());
         zctxpf.setZoerpct(zctxpfrec.getZoerpct());
  	     zctxpf.setZdedpct(zctxpfrec.getZdedpct());
  	     zctxpf.setZundpct(zctxpfrec.getZundpct());
         zctxpf.setZspspct(zctxpfrec.getZspspct());
         zctxpf.setZslryspct(zctxpfrec.getZslryspct());//ILIFE-7345
         zctxpf.setZsgtamt(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZsgtpct()));
 		 zctxpf.setZoeramt(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZoerpct()));
 		 zctxpf.setZdedamt(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZdedpct()));
 		 zctxpf.setZundamt(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZundpct()));
 		 zctxpf.setZspsamt(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZspspct()));
 		 zctxpf.setZslrysamt(Calculateamt(sv.tranamt.getbigdata(),zctxpfrec.getZslryspct()));//ILIFE-7345
 		 zctxpf.setTotprcnt(zctxpfrec.getTotprcnt());
         zctxpf.setEffdate(sv.trandatex.toInt());
         zctxpf.setDatefrm(Integer.parseInt(CurTaxFromDate));
         zctxpf.setDateto(Integer.parseInt(CurrTaxtoDate));
		 zctxpf.setAmount(sv.tranamt.getbigdata());
		 zctxpf.setRollflag("Y");
	     zctxpfDAO.insertZctxpfRecord(zctxpf);
	     }
	                                     }
              }
}


}
protected void autogenerateFromTaxDate(){
	Calendar proposalDate = Calendar.getInstance();
    Calendar taxCurFromDate = Calendar.getInstance();
    Calendar taxPreFromDate = Calendar.getInstance();
    Calendar taxToDate = Calendar.getInstance();
	try
	 {
	    proposalDate.setTime(integralDateFormat.parse(sv.trandatex.toString()));
	    int month=proposalDate.get(Calendar.MONTH)+1;
	    if(month>=7)//ILIFE-3807
	    {
	    	taxCurFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR)+1, 5, 30);
	    	taxPreFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    }
	    else{ 
	    	taxCurFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR), 5, 30);
	    	taxPreFromDate.set(proposalDate.get(Calendar.YEAR)-2, 6, 01);
	    	
	    }
	  }catch (ParseException e) {
			
			LOGGER.error("Exception occured :",e);
		}
		
		CurrTaxtoDate=integralDateFormat.format(taxToDate.getTime());
		CurTaxFromDate=integralDateFormat.format(taxCurFromDate.getTime());
		PreTaxFromDate=integralDateFormat.format(taxPreFromDate.getTime());
}

protected void createReceiptNo3300()
	{
		createRcptno3301();
	}

protected void createRcptno3301()
	{
		/* Allocate next automatic Receipt Number.*/
		alocnorec.function.set("NEXT ");
		alocnorec.prefix.set(fsupfxcpy.cash);
		alocnorec.genkey.set(wsspcomn.bankcode);
		alocnorec.company.set(wsspcomn.company);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isEQ(alocnorec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, varcom.oK)) {
			sv.letterPrintFlag.set(alocnorec.statuz);
			return ;
		}
		wsaaReceipt.set(alocnorec.alocNo);
	}

protected void getTrandesc3400()
	{
		getTrandesc3410();
	}

	/**
	* <pre>
	*   Regs:  copy CHDRLNBSKM.
	*   Notice:  WSAA-CNTNUM has to be initialised
	*            before proceeding to read the file.
	* </pre>
	*/
protected void getTrandesc3410()
	{
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(wsaaCntnum);
		chdrlnbIO.setFunction("READR");
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		/* IF CHDRLNB-STATUZ           NOT = O-K*/
		/*    MOVE SPACES              TO WSAA-TRANDESC*/
		/*    MOVE ALNO-STATUZ         TO SR203-LETPRTFLAG-ERR*/
		/* ELSE*/
		/*    IF CHDRLNB-STATCODE = 'PR'*/
		/*       MOVE 'New Business Premium Receipt'*/
		/*                             TO WSAA-TRANDESC*/
		/*    ELSE*/
		/*       MOVE 'Renewal Premium Receipt'*/
		/*                             TO WSAA-TRANDESC*/
		/*    END-IF*/
		/*    MOVE CHDRLNB-CNTTYPE  TO WSAA-CNTTYPE*/
		/*    MOVE CHDRLNB-CNTCURR  TO WSAA-CNTCURR*/
		/*    MOVE CHDRLNB-BILLCURR  TO WSAA-BILLCURR*/
		/* END-IF.*/
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		wsaaCnttype.set(chdrlnbIO.getCnttype());
		wsaaCntcurr.set(chdrlnbIO.getCntcurr());
		wsaaBillcurr.set(chdrlnbIO.getBillcurr());
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(chdrlnbIO.getChdrcoy());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getChdrnum(), SPACES);
		stringVariable1.addExpression(wsaaPayrseqno, SPACES);
		stringVariable1.setStringInto(clrfIO.getForenum());
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		wsaaClientNumber.set(clrfIO.getClntnum());
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.clntPrefix.set(clrfIO.getClntpfx());
		namadrsrec.clntCompany.set(clrfIO.getClntcoy());
		namadrsrec.clntNumber.set(clrfIO.getClntnum());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		wsaaTrandesc.set(namadrsrec.name);
	}

protected void writeRtrnHeader3500()
	{
		movingValuesToHeader3510();
	}

protected void movingValuesToHeader3510()
	{
		addrtrnrec.addrtrnRec.set(SPACES);
		addrtrnrec.function.set("NPSTW");
		addrtrnrec.statuz.set(varcom.oK);
		addrtrnrec.batckey.set(wsspcomn.batchkey);
		addrtrnrec.rdocpfx.set("CA");
		addrtrnrec.rdoccoy.set(wsspcomn.company);
		addrtrnrec.rdocnum.set(wsaaReceipt);
		addrtrnrec.rldgpfx.set("CN");
		addrtrnrec.rldgcoy.set(wsspcomn.fsuco);
		addrtrnrec.rldgacct.set(wsaaClientNumber);
		addrtrnrec.genlpfx.set("GE");
		addrtrnrec.genlcoy.set(wsspcomn.company);
		addrtrnrec.genlcur.set(wsspcomn.ledgcurr);
		addrtrnrec.glcode.set(wsaaGenlkey);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		addrtrnrec.postyear.set(wsaaBatckey.batcBatcactyr);
		addrtrnrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		addrtrnrec.sacscode.set("CN");
		addrtrnrec.sacstyp.set("1");
		addrtrnrec.contot.set(wsspdocs.contot);
		addrtrnrec.transeq.set("0000");
		addrtrnrec.trandesc.set(wsaaTrandesc);
		addrtrnrec.origccy.set(wsspcomn.ledgcurr);
		/* MOVE 0                      TO ADDR-CURRRATE.                */
		addrtrnrec.currrate.set(1);
		addrtrnrec.effdate.set(sv.trandatex);
		addrtrnrec.origamt.set(wsaaAmountRecvd);
		addrtrnrec.acctamt.set(wsaaAmountRecvd);
		addrtrnrec.chdrstcds.set(SPACES);
		addrtrnrec.glsign.set(wsspdocs.sign);
		addrtrnrec.bankcode.set(wsspcomn.bankcode);
		addrtrnrec.bankkey.set(SPACES);
		addrtrnrec.chdrkey.set(SPACES);
		addrtrnrec.tranno.set(0);
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(addrtrnrec.addrtrnRec);
			syserrrec.statuz.set(addrtrnrec.statuz);
			fatalError600();
		}
	}

protected void writeRbnkHeader3510()
	{
		moveValuesToHeader3510();
	}

protected void moveValuesToHeader3510()
	{
		//	MIBT-242 STARTS
	try {
		if (rbnkIO.getClusterWsaaInUse().equals("Y")){
			rbnkIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, rbnkIO);
			if (isNE(rbnkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(rbnkIO.getParams());
				syserrrec.statuz.set(rbnkIO.getStatuz());
				fatalError600();
			}
		}
	} catch (SQLException e) {
		LOGGER.error("Exception :-_",e);
	}
	//MIBT-242 ENDS
		rbnkIO.setDataArea(SPACES);
		rbnkIO.setRdocpfx("CA");
		rbnkIO.setRdoccoy(wsspcomn.company);
		rbnkIO.setRdocnum(wsaaReceipt);
		rbnkIO.setChqnum(SPACES);
		rbnkIO.setTchqdate(varcom.vrcmMaxDate);
		rbnkIO.setBankdescs(SPACES);
		rbnkIO.setRcptrev(SPACES);
		rbnkIO.setZchqtyp(SPACES);
		rbnkIO.setPaytype("1");
		rbnkIO.setBankkey(SPACES);
		rbnkIO.setDocorigamt(wsaaAmountRecvd);
		rbnkIO.setOrigccy(wsspcomn.ledgcurr);
		rbnkIO.setScrate(1);
		rbnkIO.setDocacctamt(wsaaAmountRecvd);
		rbnkIO.setAcctccy(wsspcomn.ledgcurr);
		rbnkIO.setFunction(varcom.writr);
		rbnkIO.setFormat(rbnkrec);
		SmartFileCode.execute(appVars, rbnkIO);
		if (isNE(rbnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rbnkIO.getParams());
			syserrrec.statuz.set(rbnkIO.getStatuz());
			fatalError600();
		}
	}

protected void calculatePolicyAmt3600()
	{
		/*CALCULATE-POLISY-AMT*/
		wsaaIndex.set(1);
		wsaaStampduty.set(0);
		wsaaPolisyAmt.set(0);
		if (isEQ(sv.tcsd, "Y")) {
			while ( !(isGT(wsaaIndex, 5))) {
				pickupStampdutyPayable3660();
			}
			
		}
		compute(wsaaPolisyAmt, 2).set(sub(wsaaAmountRecvd, wsaaStampduty));
		/*EXIT*/
	}

protected void pickupStampdutyPayable3660()
	{
		/*PICKUP-STAMPDUTY-PAYABLE*/
		if (isGTE(wsaaAmountRecvd, tr204rec.zrcptfrm[wsaaIndex.toInt()])
		&& isLTE(wsaaAmountRecvd, tr204rec.zrcptto[wsaaIndex.toInt()])) {
			wsaaStampduty.set(tr204rec.sduty[wsaaIndex.toInt()]);
			wsaaIndex.set(5);
		}
		wsaaIndex.add(1);
		/*EXIT*/
	}

protected void writeRtrnD13700()
	{
		movingValuesToDisec13710();
	}

protected void movingValuesToDisec13710()
	{
		cktrdscrec.cktrdscRec.set(SPACES);
		cktrdscrec.function.set("CHKCS");
		if(billingCollectionJpnFlag) {
			cktrdscrec.trancode.set(sv.sacscode);
			cktrdscrec.trantyp.set(sv.sacstypw);
		}
		else {
			cktrdscrec.trancode.set(t5645rec.sacscode[1]);
			cktrdscrec.trantyp.set(t5645rec.sacstype[1]);
		}
		cktrdscrec.company.set(wsspcomn.company);
		cktrdscrec.language.set(wsspcomn.language);
		callProgram(Cktrdsc.class, cktrdscrec.cktrdscRec);
		if (isNE(cktrdscrec.statuz, varcom.oK)
		&& isNE(cktrdscrec.statuz, varcom.nogo)) {
			syserrrec.params.set(cktrdscrec.cktrdscRec);
			syserrrec.statuz.set(cktrdscrec.statuz);
			fatalError600();
		}
		addrtrnrec.addrtrnRec.set(SPACES);
		addrtrnrec.function.set("PSTW");
		addrtrnrec.statuz.set(varcom.oK);
		addrtrnrec.batckey.set(wsspcomn.batchkey);
		addrtrnrec.rdocpfx.set("CA");
		addrtrnrec.rdoccoy.set(wsspcomn.company);
		addrtrnrec.rdocnum.set(wsaaReceipt);
		addrtrnrec.rldgpfx.set("CH");
		addrtrnrec.rldgcoy.set(wsspcomn.company);
		addrtrnrec.rldgacct.set(wsaaCntnum);
		addrtrnrec.genlpfx.set("GE");
		addrtrnrec.genlcoy.set(wsspcomn.company);
		addrtrnrec.genlcur.set(wsspcomn.ledgcurr);
		addrtrnrec.glcode.set(t5645rec.glmap[1]);
		addrtrnrec.postyear.set(wsaaBatckey.batcBatcactyr);
		addrtrnrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		if(billingCollectionJpnFlag) {
			addrtrnrec.sacscode.set(sv.sacscode);
			addrtrnrec.sacstyp.set(sv.sacstypw);
		}
		else {
			addrtrnrec.sacscode.set(t5645rec.sacscode[1]);
			addrtrnrec.sacstyp.set(t5645rec.sacstype[1]);
		}
		addrtrnrec.contot.set(t5645rec.cnttot[1]);
		addrtrnrec.transeq.set("0001");
		/* MOVE WSAA-HEDLINE           TO ADDR-TRANDESC.                */
		addrtrnrec.trandesc.set(cktrdscrec.trandesc);
		addrtrnrec.effdate.set(sv.trandatex);
		/* If the contract currency of the contract being entered is a*/
		/* foreign currency and the contract is still in Proposal stage*/
		/* (ie. this is a new business case), then the issue transaction*/
		/* expects a contract suspense amount in the CONTRACT currency*/
		/* and so we must perform a conversion. For all other cases*/
		/* (ie. RENEWAL premium or where the contract currency is the same*/
		/* as the ledger (bank code) currency, no conversion is necessary;*/
		/* The system will be expecting contract suspense money in the*/
		/* currency in which it is entered.*/
		/*     IF (CHDRLNB-STATCODE       = 'PR'                           */
		if ((isEQ(chdrlnbIO.getStatcode(), "PS")
		&& isNE(wsaaCntcurr, wsspcomn.ledgcurr))
		|| (isNE(chdrlnbIO.getStatcode(), "PS")
		&& isNE(wsaaBillcurr, wsspcomn.ledgcurr))) {
			processConversion5000();
			addrtrnrec.currrate.set(conlinkrec.rateUsed);
			addrtrnrec.origccy.set(wsaaCntcurr);
			addrtrnrec.origamt.set(conlinkrec.amountOut);
			addrtrnrec.acctamt.set(conlinkrec.amountIn);
		}
		else {
			addrtrnrec.currrate.set(1);
			addrtrnrec.origccy.set(wsspcomn.ledgcurr);
			addrtrnrec.origamt.set(wsaaPolisyAmt);
			addrtrnrec.acctamt.set(wsaaPolisyAmt);
		}
		addrtrnrec.glsign.set(t5645rec.sign[1]);
		addrtrnrec.chdrstcds.set(SPACES);
		addrtrnrec.bankcode.set(wsspcomn.bankcode);
		addrtrnrec.bankkey.set(SPACES);
		addrtrnrec.chdrkey.set(SPACES);
		addrtrnrec.tranno.set(0);
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(addrtrnrec.addrtrnRec);
			syserrrec.statuz.set(addrtrnrec.statuz);
			fatalError600();
		}
	}

protected void writeRtrnD23760()
	{
		movingValuesToDisec23760();
	}

protected void movingValuesToDisec23760()
	{
		addrtrnrec.addrtrnRec.set(SPACES);
		addrtrnrec.function.set("PSTW");
		addrtrnrec.statuz.set(varcom.oK);
		addrtrnrec.batckey.set(wsspcomn.batchkey);
		addrtrnrec.rdocpfx.set("CA");
		addrtrnrec.rdoccoy.set(wsspcomn.company);
		addrtrnrec.rdocnum.set(wsaaReceipt);
		addrtrnrec.rldgpfx.set("CH");
		addrtrnrec.rldgcoy.set(wsspcomn.company);
		addrtrnrec.rldgacct.set(wsaaCntnum);
		addrtrnrec.genlpfx.set("GE");
		addrtrnrec.genlcoy.set(wsspcomn.company);
		addrtrnrec.genlcur.set(wsspcomn.ledgcurr);
		addrtrnrec.glcode.set(t5645rec.glmap[2]);
		addrtrnrec.postyear.set(wsaaBatckey.batcBatcactyr);
		addrtrnrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		addrtrnrec.sacscode.set(t5645rec.sacscode[2]);
		addrtrnrec.sacstyp.set(t5645rec.sacstype[2]);
		addrtrnrec.contot.set(t5645rec.cnttot[2]);
		addrtrnrec.transeq.set("0002");
		/* MOVE WSAA-HEDLINE           TO ADDR-TRANDESC.                */
		addrtrnrec.trandesc.set(cktrdscrec.trandesc);
		addrtrnrec.origccy.set(wsspdocs.origccy);
		addrtrnrec.currrate.set(0);
		addrtrnrec.effdate.set(sv.trandatex);
		addrtrnrec.origamt.set(wsaaStampduty);
		addrtrnrec.acctamt.set(wsaaStampduty);
		addrtrnrec.chdrstcds.set(SPACES);
		addrtrnrec.glsign.set(t5645rec.sign[2]);
		addrtrnrec.bankcode.set(wsspcomn.bankcode);
		addrtrnrec.bankkey.set(SPACES);
		addrtrnrec.chdrkey.set(SPACES);
		addrtrnrec.tranno.set(0);
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(addrtrnrec.addrtrnRec);
			syserrrec.statuz.set(addrtrnrec.statuz);
			fatalError600();
		}
	}

protected void writeLetrqst3800()
	{
		moveInData3810();
	}

protected void moveInData3810()
	{
		letrqstrec.function.set("ADD");
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(wsspcomn.company);
		getLetterType3860();
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(sv.trandatex);
		if (isNE(chdrlnbIO.getPayrnum(), SPACES)) {
			letrqstrec.clntnum.set(chdrlnbIO.getPayrnum());
			letrqstrec.clntcoy.set(chdrlnbIO.getPayrcoy());
		}
		else {
			letrqstrec.clntnum.set(chdrlnbIO.getCownnum());
			letrqstrec.clntcoy.set(chdrlnbIO.getCowncoy());
		}
		letrqstrec.otherKeys.set(wsaaCntnum);
		letrqstrec.servunit.set(SPACES);
		//ILIFE-320
//		letrqstrec.letterType.set(wsspcomn.branch);
		/*       IF CHDRLNB-STATCODE = 'PR'                                */		
		if (isEQ(chdrlnbIO.getStatcode(),"PS")) {
			letrqstrec.rdocpfx.set("NB");
		}
		else {
			letrqstrec.rdocpfx.set("RE");
		}
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(wsaaReceipt);
		letrqstrec.chdrcoy.set(wsspcomn.company);
		letrqstrec.chdrnum.set(wsaaReceipt);
		letrqstrec.tranno.set(0);
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			sv.letprtflagErr.set(errorsInner.h393);
		}
	}

protected void getLetterType3860()
	{
		buildLetterType3861();
		getLetterType3865();
	}

protected void buildLetterType3861()
	{
		wsaaLtrTrans.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		if (isNE(wsaaCnttype, SPACES)) {
			wsaaLtrContr.set(wsaaCnttype);
		}
		else {
			wsaaLtrContr.set("***");
		}
	}

protected void getLetterType3865()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.                */
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaLetterType);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isNE(wsaaLtrContr, "***")) {
				wsaaLtrContr.set("***");
				itemIO.setItemitem(wsaaLetterType);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), varcom.oK)
				&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
					syserrrec.params.set(itemIO.getParams());
					syserrrec.statuz.set(itemIO.getStatuz());
					fatalError600();
				}
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*    Update Bank Balance.
	* </pre>
	*/
protected void updateBankBalance3900()
	{
		updateBbal3901();
	}

	/**
	* <pre>
	* Add the total of money going into the Bank Code to the total
	* stored on the corresponding record of the Bank Balance file
	* (if not found, add a new record).
	* </pre>
	*/
protected void updateBbal3901()
	{
		bbalIO.setBbalpfx(smtpfxcpy.bbal);
		bbalIO.setBbalcoy(wsspcomn.company);
		bbalIO.setBbalcode(wsspcomn.bankcode);
		bbalIO.setFormat(bbalrec);
		bbalIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bbalIO);
		if (isNE(bbalIO.getStatuz(), varcom.oK)
		&& isNE(bbalIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(bbalIO.getParams());
			fatalError600();
		}
		if (isEQ(bbalIO.getStatuz(), varcom.mrnf)) {
			bbalIO.setBbalbal(ZERO);
			bbalIO.setFunction(varcom.writr);
		}
		else {
			bbalIO.setFunction(varcom.rewrt);
		}
		bbalIO.setBbaltdte(wsaaToday);
		bbalIO.setBbaltime(wsaaTime);
		if (isEQ(wsspdocs.sign, "+")
		|| isEQ(wsspdocs.sign, SPACES)) {
			setPrecision(bbalIO.getBbalbal(), 2);
			bbalIO.setBbalbal(add(bbalIO.getBbalbal(), sv.totalamt02));
		}
		else {
			setPrecision(bbalIO.getBbalbal(), 2);
			bbalIO.setBbalbal(sub(bbalIO.getBbalbal(), sv.totalamt02));
		}
		SmartFileCode.execute(appVars, bbalIO);
		if (isNE(bbalIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bbalIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void processConversion5000()
	{
		go5001();
	}

	/**
	* <pre>
	* This SECTION writes any adjustment needed for the
	* conversion process.
	* </pre>
	*/
protected void go5001()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(wsspcomn.ledgcurr);
		conlinkrec.currOut.set(wsaaCntcurr);
		conlinkrec.amountIn.set(wsaaPolisyAmt);
		conlinkrec.amountOut.set(0);
		conlinkrec.rateUsed.set(0);
		conlinkrec.company.set(wsspcomn.company);
		conlinkrec.cashdate.set(sv.trandatex);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz, varcom.oK)) {
			zrdecplrec.currency.set(wsaaCntcurr);
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaTrnseq = "0001";
			adjustRupiahBalance5210();
			wsaaTrnseq = "0002";
			if (isEQ(t5645rec.sign[1], "+")) {
				wsaaSign = "-";
			}
			else {
				wsaaSign = "+";
				adjustForcurrBalance5220();
			}
		}
		else {
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
	}

protected void adjustRupiahBalance5210()
	{
		go5210();
	}

protected void go5210()
	{
		/* Write balancing amount into ACMV to anticipate*/
		/* the conversion amount generated by GLUPDATE when*/
		/* the subroutine encounters a non-base-currency*/
		/* transaction written by 3710-MOVING-VALUES-TO-DISEC1*/
		/* Set up LIFAcmv fields.*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.batccoy.set(wsspcomn.company);
		lifacmvrec1.batcbrn.set(wsspcomn.branch);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.rldgcoy.set(wsspcomn.company);
		lifacmvrec1.rldgacct.set(wsaaCntnum);
		lifacmvrec1.rdocnum.set(wsaaReceipt);
		lifacmvrec1.genlcoy.set(wsspcomn.company);
		lifacmvrec1.genlcur.set(wsspcomn.ledgcurr);
		lifacmvrec1.glcode.set(t5645rec.glmap[3]);
		lifacmvrec1.origcurr.set(wsspcomn.ledgcurr);
		lifacmvrec1.origamt.set(conlinkrec.amountIn);
		lifacmvrec1.acctamt.set(conlinkrec.amountIn);
		lifacmvrec1.crate.set(1);
		lifacmvrec1.jrnseq.set(wsaaTrnseq);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.tranno.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.effdate.set(sv.trandatex);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.tranref.set(wsaaCntnum);
		lifacmvrec1.trandesc.set("Bulk RCPT - CCY Conv Adj.");
		lifacmvrec1.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[3]);
		lifacmvrec1.glsign.set(t5645rec.sign[1]);
		lifacmvrec1.transactionTime.set(wsaaTime);
		lifacmvrec1.transactionDate.set(wsaaToday);
		lifacmvrec1.user.set(varcom.vrcmUser);
		lifacmvrec1.termid.set(varcom.vrcmTermid);
		lifacmvrec1.function.set("NPSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
	}

protected void adjustForcurrBalance5220()
	{
		go5220();
	}

protected void go5220()
	{
		/* This SUBR. balance the non-base-currency amount*/
		/* recorded by 3700-WRTIE-RTRN-D1 section by reversing*/
		/* the GLSIGN and put the amount into ACMV trans. file.*/
		/* Set up LIFAcmv fields.*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.batccoy.set(wsspcomn.company);
		lifacmvrec1.batcbrn.set(wsspcomn.branch);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.rldgcoy.set(wsspcomn.company);
		lifacmvrec1.rldgacct.set(wsaaCntnum);
		lifacmvrec1.rdocnum.set(wsaaReceipt);
		lifacmvrec1.genlcoy.set(wsspcomn.company);
		lifacmvrec1.genlcur.set(wsspcomn.ledgcurr);
		lifacmvrec1.glcode.set(t5645rec.glmap[3]);
		lifacmvrec1.origcurr.set(conlinkrec.currOut);
		lifacmvrec1.origamt.set(conlinkrec.amountOut);
		lifacmvrec1.acctamt.set(conlinkrec.amountIn);
		lifacmvrec1.crate.set(conlinkrec.rateUsed);
		lifacmvrec1.jrnseq.set(wsaaTrnseq);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.tranno.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.effdate.set(sv.trandatex);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.tranref.set(wsaaCntnum);
		lifacmvrec1.trandesc.set("Bulk RCPT - CCY Conv Adj.");
		lifacmvrec1.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[3]);
		lifacmvrec1.glsign.set(wsaaSign);
		lifacmvrec1.transactionTime.set(wsaaTime);
		lifacmvrec1.transactionDate.set(wsaaToday);
		wsaaUserNo.set(ZERO);
		lifacmvrec1.user.set(varcom.vrcmUser);
		lifacmvrec1.termid.set(varcom.vrcmTermid);
		lifacmvrec1.function.set("NPSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is active       */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlnbIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlnbIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypStatcode.set(chdrlnbIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlnbIO.getPstatcode());
		drypDryprcRecInner.drypBillcd.set(chdrlnbIO.getBillcd());
		drypDryprcRecInner.drypTranno.set(chdrlnbIO.getTranno());
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}

protected void setSacscode() {
	List<String> sacstypeList;
	sv.sacscodeMap = new HashMap<>();
	for(int i=1; i<=t5645rec.sacscode.length; i++) {
		sacstypeList = new ArrayList<>();
		if(!t5645rec.sacscode[i].toString().trim().isEmpty()) {
			if(!sv.sacscodeMap.containsKey(t5645rec.sacscode[i].toString())) {
				sacstypeList.add(t5645rec.sacstype[i].toString().trim());
				sv.sacscodeMap.put(t5645rec.sacscode[i].toString(), sacstypeList);
			}
			else {
				sv.sacscodeMap.get(t5645rec.sacscode[i].toString()).add(t5645rec.sacstype[i].toString());
			}
		}
		else 
			break;
	}
}
/**
 * ILIFE-4361
 * 
 * @return
 */
public FixedLengthStringData getWsaaReceipt() {
	return wsaaReceipt;
}

/**
 * ILIFE-4361
 * 
 * @param wsaaReceipt
 */
public void setWsaaReceipt(FixedLengthStringData wsaaReceipt) {
	this.wsaaReceipt = wsaaReceipt;
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e198 = new FixedLengthStringData(4).init("E198");
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData f499 = new FixedLengthStringData(4).init("F499");
	private FixedLengthStringData f579 = new FixedLengthStringData(4).init("F579");
	private FixedLengthStringData f645 = new FixedLengthStringData(4).init("F645");
	private FixedLengthStringData f990 = new FixedLengthStringData(4).init("F990");
	private FixedLengthStringData r011 = new FixedLengthStringData(4).init("R011");
	private FixedLengthStringData g709 = new FixedLengthStringData(4).init("G709");
	private FixedLengthStringData h946 = new FixedLengthStringData(4).init("H946");
	private FixedLengthStringData h393 = new FixedLengthStringData(4).init("H393");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e696 = new FixedLengthStringData(4).init("E696");
	private FixedLengthStringData e199 = new FixedLengthStringData(4).init("E199");
	private FixedLengthStringData rulv = new FixedLengthStringData(4).init("RULV");
	private FixedLengthStringData rulw = new FixedLengthStringData(4).init("RULw");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
}
}
