/*
 * File: Zrvaerls.java
 * Date: 30 August 2009 2:57:37
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRVAERLS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*  ZRVAERLS - Reverse Anticipated Endowment Release
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*
*  REVERSE CONTRACT HEADER
*  ~~~~~~~~~~~~~~~~~~~~~~~
*  READH on the Contract Header record and REWRT it with the new
*  TRANNO.
*
*  REVERSE ZRAE RECORD
*  ~~~~~~~~~~~~~~~~~~~
*  BEGNH on the ZRAE record to get the last record and then
*  delete it.
*  Then do another BEGNH on the ZRAE record to get the previous
*  record of the deleted record.
*  This should be a valid flag '2' record and REWRT it with valid
*  flag '1', change the CurrTo date to 99999999, change the TRANNO
*  to the new TRANNO.
*
*  REVERSE LOAN RECORDS
*  ~~~~~~~~~~~~~~~~~~~~
*  Do a BEGNH on the LOANENQ file for the tranno being reversed
*  and see if any Loans (contract or APLs) were paid off during
*  the Anticipated Endowment Release process.
*     - check this by comparing the Last-Tranno field with the
*       TRANNO we are trying to reverse now
*     - if so, we must re-instate the Loans affected dates
*       Set the LOANENQ record Validflags back to '1'
*  If the Loan records found are Cash accounts' record then
*  delete the record provided that they have the same TRANNO.
*
*  REVERSE ACMVS
*  ~~~~~~~~~~~~~
*  Do a BEGNH on the ACMVREV file for this transaction no. and
*  call LIFACMV to post the reversal to the sub-account.
*
*****************************************************
* </pre>
*/
public class Zrvaerls extends SMARTCodeModel {
	private static final Logger LOGGER = LoggerFactory.getLogger(Zrvaerls.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZRVAERLS";
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLstInterestDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNxtInterestDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t6633 = "T6633";
		/* FORMATS */
	private static final String zraerec = "ZRAEREC";
	private static final String chdrclmrec = "CHDRCLMREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
	private static final String descrec = "DESCREC";
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String loanenqrec = "LOANENQREC";
	private static final String arcmrec = "ARCMREC";
		/* ERRORS */
	private static final String e723 = "E723";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private Reverserec reverserec = new Reverserec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();

	public Zrvaerls() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		LOGGER.debug("Flow Ended");
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		moveLinkage1010();
	}

protected void moveLinkage1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		/* Get todays date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* Read Table T1688 to obtain Batch Trans Description for*/
		/* updating ACMV.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void process2000()
	{
		/*PROCESS*/
		reverseContractHeader2100();
		reverseZraeRecord2200();
		reverseLoanRecord2300();
		processAcmvs2500();
		processAcmvOptical2600();
		/*EXIT*/
	}

protected void reverseContractHeader2100()
	{
		reverseContractHeader2110();
		doUpdate2120();
	}

protected void reverseContractHeader2110()
	{
		chdrclmIO.setParams(SPACES);
		chdrclmIO.setChdrnum(reverserec.chdrnum);
		chdrclmIO.setChdrcoy(reverserec.company);
		chdrclmIO.setFormat(chdrclmrec);
		chdrclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)
		&& isNE(chdrclmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrclmIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrclmIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrclmIO.getValidflag(),"1")
		|| isEQ(chdrclmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
	}

protected void doUpdate2120()
	{
		chdrclmIO.setTranno(reverserec.newTranno);
		chdrclmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void reverseZraeRecord2200()
	{
		readZraeRecord2210();
		getPreviousZraeRecord2220();
		rewriteZraeRecord2230();
	}

protected void readZraeRecord2210()
	{
		zraeIO.setParams(SPACES);
		zraeIO.setChdrcoy(reverserec.company);
		zraeIO.setChdrnum(reverserec.chdrnum);
		zraeIO.setPlanSuffix(ZERO);
		zraeIO.setFormat(zraerec);
		/* MOVE BEGNH                  TO ZRAE-FUNCTION.                */
		zraeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zraeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zraeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)
		|| isNE(zraeIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zraeIO.getChdrcoy(),reverserec.company)
		|| isNE(zraeIO.getValidflag(),"1")
		|| isNE(zraeIO.getTranno(),reverserec.tranno)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			xxxxFatalError();
		}
		/* MOVE DELET                  TO ZRAE-FUNCTION.                */
		zraeIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void getPreviousZraeRecord2220()
	{
		zraeIO.setParams(SPACES);
		zraeIO.setChdrcoy(reverserec.company);
		zraeIO.setChdrnum(reverserec.chdrnum);
		zraeIO.setPlanSuffix(ZERO);
		zraeIO.setFormat(zraerec);
		/* MOVE BEGNH                  TO ZRAE-FUNCTION.                */
		zraeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)
		|| isNE(zraeIO.getValidflag(),"2")) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewriteZraeRecord2230()
	{
		zraeIO.setCurrto(varcom.vrcmMaxDate);
		zraeIO.setValidflag("1");
		/*    MOVE REVE-NEW-TRANNO        TO ZRAE-TRANNO.                  */
		/* MOVE REWRT                  TO ZRAE-FUNCTION.                */
		zraeIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zraeIO.getParams());
			syserrrec.statuz.set(zraeIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void reverseLoanRecord2300()
	{
		/*START*/
		loanenqIO.setParams(SPACES);
		loanenqIO.setChdrcoy(reverserec.company);
		loanenqIO.setChdrnum(reverserec.chdrnum);
		loanenqIO.setLoanNumber(ZERO);
		/* MOVE BEGNH                  TO LOANENQ-FUNCTION.             */
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(loanenqrec);
		while ( !(isEQ(loanenqIO.getStatuz(),varcom.endp))) {
			processLoans2400();
		}
		
		/*EXIT*/
	}

protected void processLoans2400()
	{
			start2410();
		}

protected void start2410()
	{
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(),varcom.oK)
		&& isNE(loanenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			xxxxFatalError();
		}
		/* If no Loan records found for this contract, then  EXIT*/
		if (isNE(reverserec.company,loanenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,loanenqIO.getChdrnum())
		|| isEQ(loanenqIO.getStatuz(),varcom.endp)) {
			loanenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so we have found a Loan record.......*/
		/* Check the Last Tranno on the Loan record with the Tranno to*/
		/* reversed. If it matches then we will update the record*/
		/* otherwise read next record.*/
		if (isEQ(loanenqIO.getLastTranno(),reverserec.tranno)) {
			/* If it is a Cash Deposit record (Loan Type = 'E') then*/
			/* DELETE this record.*/
			if (isEQ(loanenqIO.getLoanType(),"E")) {
				/*         MOVE DELET          TO LOANENQ-FUNCTION              */
				loanenqIO.setFunction(varcom.deltd);
				SmartFileCode.execute(appVars, loanenqIO);
				if (isNE(loanenqIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(loanenqIO.getParams());
					syserrrec.statuz.set(loanenqIO.getStatuz());
					xxxxFatalError();
				}
			}
			else {
				obtainInterestDates2800();
				loanenqIO.setLastIntBillDate(wsaaLstInterestDate);
				loanenqIO.setNextIntBillDate(wsaaNxtInterestDate);
				loanenqIO.setValidflag("1");
				loanenqIO.setLastTranno(reverserec.newTranno);
				/*         MOVE REWRT          TO LOANENQ-FUNCTION              */
				loanenqIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, loanenqIO);
				if (isNE(loanenqIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(loanenqIO.getParams());
					syserrrec.statuz.set(loanenqIO.getStatuz());
					xxxxFatalError();
				}
			}
		}
		loanenqIO.setFunction(varcom.nextr);
	}

protected void processAcmvs2500()
	{
		start2510();
	}

protected void start2510()
	{
		/* Read the Account movements file (ACMVPF) using the logical*/
		/* view ACMVREV with a key of contract number/transaction*/
		/* number from linkage.*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvs2700();
		}
		
	}

protected void processAcmvOptical2600()
	{
		start2610();
				}

protected void start2610()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater*/
		/* than the last period Archived then there is no need to*/
		/* attempt to read the Optical Device.*/
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
				}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
				}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
				}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
			}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs2700();
		}
		
		/* If the next policy is found then we must call the COLD API*/
		/* again with a function of CLOSE.*/
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				syserrrec.statuz.set(acmvrevIO.getStatuz());
				xxxxFatalError();
			}
		}
	}

protected void reverseAcmvs2700()
	{
		start2710();
		readNextAcmv2780();
	}

protected void start2710()
	{
		/* Check If ACMV just read has same trancode as transaction*/
		/* we are trying to reverse.*/
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			return ;
		}
		/* Post equal & opposite amount to ACMV just read.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
	}

protected void readNextAcmv2780()
	{
		/* Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void obtainInterestDates2800()
	{
		start2810();
	}

protected void start2810()
	{
		readT5645Table2900();
		/* Use entries on the T5645 record read to get Loan interest*/
		/* ACMVs for this loan number.*/
		if (isEQ(loanenqIO.getLoanType(),"P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
		}
		wsaaRldgacct.set(SPACES);
		wsaaLstInterestDate.set(ZERO);
		wsaaChdrnum.set(loanenqIO.getChdrnum());
		wsaaLoanNumber.set(loanenqIO.getLoanNumber());
		/* We will begin reading on the ACMV original surrender*/
		/* transactions and read backwards to get the movements*/
		/* before the original surrender (nextp)*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			searchAcmvRecs3000();
		}
		
		/* If any interest records for the loan being processed*/
		/* (before the surrender movements) i.e there is a value*/
		/* in WSAA-LST-INTEREST-DATE use this otherwise no interest*/
		/* has been calculated since the loan start date.*/
		if (isEQ(wsaaLstInterestDate,ZERO)) {
			wsaaLstInterestDate.set(loanenqIO.getLoanStartDate());
		}
		/* Calculate next interest date from the last interest date*/
		/* retrieved (i.e the effective date on the relevant ACMV).*/
		readT6633Table3100();
		calcNextInterestDate3200();
	}

protected void readT5645Table2900()
	{
		start2910();
	}

protected void start2910()
	{
		/* Read the accounting rules table*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(loanenqIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalErrors();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void searchAcmvRecs3000()
	{
			start3010();
		}

protected void start3010()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalErrors();
		}
		if (isNE(acmvrevIO.getRldgcoy(),loanenqIO.getChdrcoy())
		|| isNE(acmvrevIO.getRdocnum(),loanenqIO.getChdrnum())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isLT(acmvrevIO.getTranno(),reverserec.tranno)
		&& isEQ(acmvrevIO.getSacscode(),wsaaSacscode)
		&& isEQ(acmvrevIO.getSacstyp(),wsaaSacstype)
		&& isEQ(acmvrevIO.getRldgacct(),wsaaRldgacct)) {
			wsaaLstInterestDate.set(acmvrevIO.getEffdate());
			acmvrevIO.setStatuz(varcom.endp);
		}
		else {
			acmvrevIO.setFunction(varcom.nextp);
		}
	}

protected void readT6633Table3100()
	{
		start3110();
	}

protected void start3110()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(loanenqIO.getChdrcoy());
		itdmIO.setItemtabl(t6633);
		wsaaT6633Cnttype.set(chdrclmIO.getCnttype());
		wsaaT6633Type.set(loanenqIO.getLoanType());
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(wsaaLstInterestDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalErrors();
		}
		if (isNE(itdmIO.getItemcoy(),loanenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t6633)
		|| isNE(itdmIO.getItemitem(),wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6633Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			xxxxFatalErrors();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void calcNextInterestDate3200()
	{
			start3290();
		}

protected void start3290()
	{
		/* Check the interest  details on T6633 in the following*/
		/* order: i) Calculate interest on Loan anniv ... Y/N*/
		/*       ii) Calculate interest on Policy anniv.. Y/N*/
		/*      iii) Check Int freq & whether a specific Day is chosen*/
		wsaaLoanDate.set(loanenqIO.getLoanStartDate());
		wsaaEffdate.set(wsaaLstInterestDate);
		wsaaContractDate.set(chdrclmIO.getOccdate());
		/* Check for loan anniversary flag set*/
		/* IF set,*/
		/* set next interest billing date to be on the next loan anniv*/
		/* date after the Effective date we are using now.*/
		if (isEQ(t6633rec.loanAnnivInterest,"Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(loanenqIO.getLoanStartDate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2,wsaaEffdate))) {
				callDatcon43300();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}
			
			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Check for contract anniversary flag set*/
		/* IF set,*/
		/* set next interest billing date to be on the next contract*/
		/* anniversary date after the Effective date we are using now.*/
		if (isEQ(t6633rec.policyAnnivInterest,"Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(chdrclmIO.getOccdate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2,wsaaEffdate))) {
				callDatcon43300();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}
			
			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Get here so the next interest calc. date isn't based on loan*/
		/* or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified*/
		/* ...if not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3400();
		}
		else {
			if (isNE(t6633rec.interestDay,ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.intDate1.set(wsaaNewDate);
		datcon4rec.freqFactor.set(1);
		/* Check if table T6633 has a fixed frequency for interest,*/
		/* If not, use 1 year as the default interest calc. frequency.*/
		if (isEQ(t6633rec.interestFrequency,SPACES)) {
			datcon4rec.frequency.set("01");
		}
		else {
			datcon4rec.frequency.set(t6633rec.interestFrequency);
		}
		while ( !(isGT(datcon4rec.intDate2,wsaaEffdate))) {
			callDatcon43300();
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}
		
		wsaaNxtInterestDate.set(datcon4rec.intDate2);
	}

protected void callDatcon43300()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalErrors();
		}
		/*EXIT*/
	}

protected void dateSet3400()
	{
		/*START*/
		/* We have to check that the date we are going to call Datcon4*/
		/* with is a valid from-date... ie IF the interest/capn day in*/
		/* T6633 is > 28, we have to make sure the from-date isn't*/
		/* something like 31/02/nnnn or 31/06/nnnn*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner { 
	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
	}
}
