/*
 * File: Zrtotlon.java
 * Date: 30 August 2009 2:56:30
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRTOTLON.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.AcmvlonTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*             TOTAL LOANS SUBROUTINE.
*             -----------------------
*
* NOTE:
* -----
* This program is cloned from TOTLOAN in that it differentiates
* between the Loan accounts (Loan Type of 'A' and 'P') and Cash
* accounts (Loan Type of 'E') in that it only processes Loan
* records according to the Loan Type given.
*
*
* Overview.
* ---------
*
* This Subroutine is called to calculate just how many Loans or
*  Cash Deposits a contract has associated with it and what the
*  total Principal & Interest.
* IF this subroutine is called by a Claims related program,
*  such as Death, Maturities or Surrenders, then a Function
*  of 'POST' is specified - this means that in addition to
*  calculating Principal & Interest to the specified date, any
*  Interest which hasn't been Posted yet WILL be posted by this
*  routine.
* The Principal & Interest values returned by this subroutine
*  are in the CONTRACT CURRENCY, although if any Interest has
*  to be posted, (as in Function = 'POST'), then the postings
*  will be made in the LOAN Currency
*
*
*Given:
*      Function, Company, Contract number, Loan Type,
*       Effective date, Batchkey, Tranno, Tranid, Language
*
* NOTE.... Function are 'ALL', 'CASH', 'LOAN' or  'DPST'
*  where
*        'ALL'  => Process all loan types
*
*        'CASH' => Process loan type equal to 'E'
*
*        'LOAN' => Process loan type equal to either 'A' or 'P'
*
*        'DPST' => Process loan type equal 'D'
*
*
*Gives:
*      Status, Total Principal, Total Interest, Number of Loans
*       on contract
*
*Functionality
*-------------
*
* Read Contract to get contract type CNTTYPE and contract
*  currency CNTCURR.
*
* BEGN on LOAN file & get 1st record
* Store principal amount WSAA-PRINCIPAL
*
* Add up all existing ACMVs for loan interest which relate
*  to this particular LOAN record and are dated since the
*  last capitalisation date. ( value in CoNTract CURRency )
*
* IF effective-date > last interest date on Loan record
*
*   Set interest to date = effective date
*   Set interest from date = last interest date on loan record
*
*   calculate interest earned since last interest date for this
*    LOAN record:
*
*    CALL INTCALC giving:
*           Loan no, Company, contract no, interest from date,
*           interest to date, loan prin at last capn date,
*           last loan capn date, loan commencement date
*       getting:
*          interest amount
*
*    Store interest amount ( in contract currency CNTCURR )
*    IF TOTL-POST-FLAG = 'Y',then write interest ACMVs
*    write interest ACMVs
*    REWRITe LOAN record with last & next interest billing dates
*     amended.
* END IF
*
*READ NEXT LOAN record and repeat above process
*
*WHEN no more LOAN records,
* we have total interest amount
* Get current principal amount by adding/subtracting all Loan
* principal ACMVs since start of loan. (in CoNTract CURRency )
*
* NOTE. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*------
*      ALL VALUES ARE CALCULATED & RETURNED IN CONTRACT CURRENCY
*                                              --------
*      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
*RETURN Loan principal and Loan interest to calling routine
*
*****************************************************************
* </pre>
*/
public class Zrtotlon extends SMARTCodeModel {
	private static final Logger LOGGER = LoggerFactory.getLogger(Zrtotlon.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZRTOTLON";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaProcess = "";
	private ZonedDecimalData wsaaLoanCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSign = new FixedLengthStringData(1);
	private int wsaaMaxDate = 0;
	private PackedDecimalData wsaaPostedAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInterest = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalInterest = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrincipal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIntcInterest = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaBaseIdx = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);
		/*                                                         <V4L001>
		  Storage for T5645 table items.                         <V4L001>
		                                                         <V4L001>*/
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5645Sub = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
		/* FORMATS */
	private static final String acmvlonrec = "ACMVLONREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String itdmrec = "ITEMREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String loanrec = "LOANREC   ";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t6633 = "T6633";
		/* ERRORS */
	private static final String e723 = "E723";
	private static final String h791 = "H791";
	private AcmvlonTableDAM acmvlonIO = new AcmvlonTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Intcalcrec intcalcrec = new Intcalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Totloanrec totloanrec = new Totloanrec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNextAmv2450, 
		exit2490, 
		datcon2Loop3550, 
		updateLoan3550
	}

	public Zrtotlon() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		totloanrec.totloanRec = convertAndSetParam(totloanrec.totloanRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		LOGGER.debug("Flow Ended");
		}
	}

protected void mainline100()
	{
		control100();
		exit190();
	}

protected void control100()
	{
		initialise1000();
		processLoans2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		totloanrec.statuz.set(varcom.oK);
		wsaaMaxDate = 99999999;
		wsaaLoanCount.set(ZERO);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		wsaaPostedAmount.set(ZERO);
		wsaaInterest.set(ZERO);
		wsaaPrincipal.set(ZERO);
		wsaaIntcInterest.set(ZERO);
		wsaaSacscode.set(SPACES);
		wsaaSacstype.set(SPACES);
		wsaaSign.set(SPACES);
		wsaaSequenceNo.set(ZERO);
		wsaaBatckey.set(totloanrec.batchkey);
		/* Get contract type to pass to interest calculation routine*/
		/* later on and get contract currency.*/
		readContract1100();
		readT5645Table1200();
	}

protected void readContract1100()
	{
		/*START*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrnum(totloanrec.chdrnum);
		chdrenqIO.setChdrcoy(totloanrec.chdrcoy);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError99500();
		}
		/*EXIT*/
	}

protected void readT5645Table1200()
	{
		start1200();
	}

protected void start1200()
	{
		/* Read the Accounting Rules table*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(totloanrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		/*    MOVE READR                  TO ITEM-FUNCTION.                */
		/*    CALL 'ITEMIO'            USING ITEM-PARAMS.                  */
		/*    IF  ITEM-STATUZ         NOT =  O-K                           */
		/*        MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
		/*        MOVE ITEM-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 99500-DATABASE-ERROR.                            */
		/*    MOVE ITEM-GENAREA           TO T5645-T5645-REC.              */
		/*                                                         <V4L001>*/
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		wsaaT5645Offset.set(ZERO);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT56451300();
		}
		
		/* Get item description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(totloanrec.chdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setLanguage(totloanrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void loadT56451300()
	{
		start1310();
	}

	/**
	* <pre>
	************************                                  <V4L001>
	* </pre>
	*/
protected void start1310()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), totloanrec.chdrcoy)
		|| isNE(itemIO.getItemtabl(), t5645)
		|| isNE(itemIO.getItemitem(), wsaaSubr)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				databaseError99500();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*                                                         <V4L001>*/
		/* Insert the T5645 mutiple ITEMSEQ items into WSAA. The var       */
		/* WSAA-T5645-OFFSET should be init to zero before loading T5645.  */
		/*                                                         <V4L001>*/
		for (wsaaT5645Sub.set(1); !(isGT(wsaaT5645Sub, 15)
		|| isGTE(wsaaT5645Offset, wsaaT5645Size)); wsaaT5645Sub.add(1)){
			wsaaT5645Offset.add(1);
			wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
		}
		if (isGTE(wsaaT5645Offset, wsaaT5645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5645);
			databaseError99500();
		}
		itemIO.setFunction(varcom.nextr);
	}

protected void processLoans2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* Do a BEGN on LOAN file to get 1st record*/
		loanIO.setParams(SPACES);
		loanIO.setChdrcoy(totloanrec.chdrcoy);
		loanIO.setChdrnum(totloanrec.chdrnum);
		loanIO.setLoanNumber(ZERO);
		loanIO.setFormat(loanrec);
		if (isEQ(totloanrec.postFlag, "Y")) {
			/*     MOVE BEGNH              TO LOAN-FUNCTION                 */
			loanIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			loanIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		}
		else {
			loanIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			loanIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		}
		while ( !(isEQ(loanIO.getStatuz(), varcom.endp))) {
			processLoan2100();
		}
		
		/* If no loans for this contract, exit subroutine*/
		if (isEQ(loanIO.getStatuz(), varcom.endp)
		&& isEQ(wsaaLoanCount, ZERO)) {
			return ;
		}
		/* We have at least 1 loan for this contract*/
		totloanrec.loanCount.set(wsaaLoanCount);
		totloanrec.interest.set(wsaaInterest);
		totloanrec.principal.set(wsaaPrincipal);
	}

protected void processLoan2100()
	{
		start2100();
	}

protected void start2100()
	{
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)
		&& isNE(loanIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(loanIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* BEGNH is replaced by BEGN,so record is not locked,REWRT is   */
		/* not necessary.                                               */
		if (isNE(loanIO.getChdrcoy(), totloanrec.chdrcoy)
		|| isNE(loanIO.getChdrnum(), totloanrec.chdrnum)) {
			/* OR LOAN-STATUZ              =  ENDP                          */
			/*     IF TOTL-POST-FLAG       =  'Y'                   <LA3993>*/
			/*         MOVE REWRT          TO LOAN-FUNCTION         <LA3993>*/
			/*         CALL 'LOANIO'       USING LOAN-PARAMS        <LA3993>*/
			/*         IF  LOAN-STATUZ     NOT =  O-K               <LA3993>*/
			/*            MOVE LOAN-PARAMS TO SYSR-PARAMS           <LA3993>*/
			/*            MOVE LOAN-STATUZ TO SYSR-STATUZ           <LA3993>*/
			/*            PERFORM 99500-DATABASE-ERROR              <LA3993>*/
			/*         END-IF                                       <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			loanIO.setStatuz(varcom.endp);
			return ;
		}
		/*    IF LOAN-LOAN-TYPE       NOT = 'A' AND 'E' AND 'P'*/
		/*         MOVE BOMB              TO SYSR-STATUZ*/
		/*         PERFORM 99500-DATABASE-ERROR.*/
		loanIO.setFunction(varcom.nextr);
		wsaaProcess = "N";
		if (isEQ(totloanrec.function, "ALL")){
			wsaaProcess = "Y";
		}
		else if (isEQ(totloanrec.function, "CASH")){
			if (isEQ(loanIO.getLoanType(), "E")) {
				wsaaProcess = "Y";
			}
		}
		else if (isEQ(totloanrec.function, "LOAN")){
			if (isEQ(loanIO.getLoanType(), "A")
			|| isEQ(loanIO.getLoanType(), "P")) {
				wsaaProcess = "Y";
			}
		}
		else if (isEQ(totloanrec.function, "DPST")){
			if (isEQ(loanIO.getLoanType(), "D")) {
				wsaaProcess = "Y";
			}
		}
		else{
			syserrrec.statuz.set(varcom.bomb);
			databaseError99500();
		}
		if (isEQ(wsaaProcess, "N")) {
			return ;
		}
		wsaaLoanCount.add(1);
		/* Initialise index to read the Accounting Rules in Table*/
		/* T5645. Currently the Accounting Rules for this process*/
		/* are stored in the following order:*/
		/*    1st 4 - Policy Loan*/
		/*    2nd 4 - APL*/
		/*    3rd 4 - Cash Deposit (Anticipated Endowment)*/
		/* Thus, we initialise the index with 0, 4, and 8 for Policy*/
		/* Loan, APL and Cash Deposit respectively depending on the*/
		/* Loan Type.*/
		/* The Loan Types are 'P for Policy Loan, 'A' for APL and 'E'*/
		/* for Cash Deposit.*/
		/*    Add 4th 4 - Advance Premium Deposit (APA). Set the index to  */
		/*    12 for APA. Loan type for APA is 'D'.                <V4L001>*/
		/*                                                         <V4L001>*/
		if (isEQ(loanIO.getLoanType(), "P")){
			wsaaBaseIdx.set(0);
		}
		else if (isEQ(loanIO.getLoanType(), "A")){
			wsaaBaseIdx.set(4);
		}
		else if (isEQ(loanIO.getLoanType(), "E")){
			wsaaBaseIdx.set(8);
		}
		else if (isEQ(loanIO.getLoanType(), "D")){
			wsaaBaseIdx.set(12);
		}
		/* Sum all existing Loan interest ACMVs which exist for this*/
		/* Loan and are dated since the last capitalisation date on*/
		/* the loan.*/
		sumInterestAcmvs2300();
		wsaaInterest.add(wsaaPostedAmount);
		if (isLT(loanIO.getLastIntBillDate(), totloanrec.effectiveDate) && isGT(loanIO.getLastCapnLoanAmt(),ZERO)) {
			/*     Set up INTCALC linkage and call...*/
			intcalcrec.intcalcRec.set(SPACES);
			intcalcrec.loanNumber.set(loanIO.getLoanNumber());
			intcalcrec.chdrcoy.set(loanIO.getChdrcoy());
			intcalcrec.chdrnum.set(loanIO.getChdrnum());
			intcalcrec.cnttype.set(chdrenqIO.getCnttype());
			intcalcrec.interestTo.set(totloanrec.effectiveDate);
			intcalcrec.interestFrom.set(loanIO.getLastIntBillDate());
			intcalcrec.loanorigam.set(loanIO.getLastCapnLoanAmt());
			intcalcrec.lastCaplsnDate.set(loanIO.getLastCapnDate());
			intcalcrec.loanStartDate.set(loanIO.getLoanStartDate());
			intcalcrec.interestAmount.set(ZERO);
			intcalcrec.loanCurrency.set(loanIO.getLoanCurrency());
			intcalcrec.loanType.set(loanIO.getLoanType());
			callProgram(Intcalc.class, intcalcrec.intcalcRec);
			if (isNE(intcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(intcalcrec.intcalcRec);
				syserrrec.statuz.set(intcalcrec.statuz);
				systemError99000();
			}
			/* Get here and we want to add wsaa-posted-amount to result*/
			/* from intcalc and add into wsaa-total-interest - then*/
			/* clear out wsaa-interest for next loan record to use.*/
			/* After all LOAN records processed, return values to*/
			/* calling program.*/
			wsaaIntcInterest.set(intcalcrec.interestAmount);
			/* Make sure interest returned from INTCALC is in Contract*/
			/* currency.*/
			if (isNE(loanIO.getLoanCurrency(), chdrenqIO.getCntcurr())) {
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(intcalcrec.interestAmount);
				conlinkrec.statuz.set(SPACES);
				conlinkrec.function.set("REAL");
				/*      MOVE 'SURR'           TO CLNK-FUNCTION*/
				conlinkrec.currIn.set(loanIO.getLoanCurrency());
				conlinkrec.cashdate.set(wsaaMaxDate);
				conlinkrec.currOut.set(chdrenqIO.getCntcurr());
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.company.set(loanIO.getChdrcoy());
				callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz, varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					systemError99000();
				}
				else {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					a000CallRounding();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
					intcalcrec.interestAmount.set(conlinkrec.amountOut);
				}
			}
			/*    ADD INTC-INTEREST-AMOUNT TO WSAA-INTEREST*/
			/* Need to differentiate between Loans' Interest and Cash*/
			/* Deposits' Interest.*/
			/* Advance Premium Deposit (APA) will similar to Cash Deposit.     */
			/*                                                         <V4L001>*/
			if (isEQ(loanIO.getLoanType(), "E")
			|| isEQ(loanIO.getLoanType(), "D")) {
				wsaaInterest.subtract(intcalcrec.interestAmount);
			}
			else {
				wsaaInterest.add(intcalcrec.interestAmount);
			}
		}
		/* Need to sum all loan principal ACMVs since last*/
		/* capitalisation date to get current principal value.*/
		sumLoanPrincipal2500();
		wsaaPrincipal.add(wsaaPostedAmount);
		/* If loan is not in contract currency, then convert last loan*/
		/* capitalisation amount to the contract currency.*/
		/* NOTE... we will not add Principal to the sum of the ACMVs*/
		/* IF LOAN-START-DATE is the same as the TOTL-EFFECTIVE-DATE*/
		/* other-wise we will get double the principal amount*/
		if (isNE(totloanrec.effectiveDate, loanIO.getLoanStartDate())) {
			if (isNE(loanIO.getLoanCurrency(), chdrenqIO.getCntcurr())) {
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(loanIO.getLastCapnLoanAmt());
				conlinkrec.statuz.set(SPACES);
				conlinkrec.function.set("REAL");
				/*        MOVE 'SURR'          TO CLNK-FUNCTION*/
				conlinkrec.currIn.set(loanIO.getLoanCurrency());
				conlinkrec.cashdate.set(wsaaMaxDate);
				conlinkrec.currOut.set(chdrenqIO.getCntcurr());
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.company.set(loanIO.getChdrcoy());
				callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz, varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					systemError99000();
				}
				else {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					a000CallRounding();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
					wsaaPrincipal.add(conlinkrec.amountOut);
				}
			}
			else {
				wsaaPrincipal.add(loanIO.getLastCapnLoanAmt());
			}
		}
		/* If Post-Flag = 'Y', we must post an interest ACMV.*/
		if (isEQ(totloanrec.postFlag, "Y") && isGT(loanIO.getLastCapnLoanAmt(),ZERO)) {
			postInterestAcmv3000();
			updateLoan3500();
		}
		loanIO.setFunction(varcom.nextr);
	}

protected void sumInterestAcmvs2300()
	{
		start2300();
	}

protected void start2300()
	{
		/* Use entry 2 on the T5645 record read to get Loan interest*/
		/* ACMVs for this loan number.*/
		/*  COMPUTE WSAA-T5645-IDX      =  WSAA-BASE-IDX + 2.            */
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 4));
		/*    MOVE T5645-SACSCODE(WSAA-T5645-IDX)                          */
		/*                                TO WSAA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE(WSAA-T5645-IDX)                          */
		/*                                TO WSAA-SACSTYPE.                */
		/*    MOVE T5645-SIGN(WSAA-T5645-IDX)                              */
		/*                                TO WSAA-SIGN.                    */
		wsaaSacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		wsaaSign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		wsaaPostedAmount.set(ZERO);
		acmvlonIO.setParams(SPACES);
		acmvlonIO.setRldgcoy(totloanrec.chdrcoy);
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(totloanrec.chdrnum);
		wsaaLoanNumber.set(loanIO.getLoanNumber());
		acmvlonIO.setRldgacct(wsaaRldgacct);
		acmvlonIO.setSacscode(wsaaSacscode);
		acmvlonIO.setSacstyp(wsaaSacstype);
		acmvlonIO.setEffdate(loanIO.getLastCapnDate());
		acmvlonIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvlonIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvlonIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvlonIO.setFormat(acmvlonrec);
		while ( !(isEQ(acmvlonIO.getStatuz(), varcom.endp))) {
			processAcmvlons2400();
		}
		
	}

protected void processAcmvlons2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2400();
				case readNextAmv2450: 
					readNextAmv2450();
				case exit2490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2400()
	{
		SmartFileCode.execute(appVars, acmvlonIO);
		if (isNE(acmvlonIO.getStatuz(), varcom.oK)
		&& isNE(acmvlonIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvlonIO.getParams());
			syserrrec.statuz.set(acmvlonIO.getStatuz());
			databaseError99500();
		}
		/* NOTE... we will add ACMVs to the running total WHEN the*/
		/* ACMV effective date = LOAN-LAST-CAPN-DATE , but only WHEN*/
		/* LOAN-START-DATE is the same as the TOTL-EFFECTIVE-DATE*/
		/* ... there won't be any interest postings anyway because it*/
		/* is the Loan commencement day and therefore the only*/
		/* movements will be those on the Principal*/
		/* i.e. a Loan could be registered on 1/1/91 for 100.00 and*/
		/*   a claim done on the same day ... so this program has to*/
		/*   pick up these movements - this particular case ONLY*/
		/*   happens when the LOAN-START-DATE = TOTL-EFFECTIVE-DATE*/
		if (isNE(totloanrec.effectiveDate, loanIO.getLoanStartDate())) {
			if (isNE(acmvlonIO.getRldgcoy(), totloanrec.chdrcoy)
			|| isNE(acmvlonIO.getSacscode(), wsaaSacscode)
			|| isNE(acmvlonIO.getSacstyp(), wsaaSacstype)
			|| isNE(acmvlonIO.getRldgacct(), wsaaRldgacct)
			|| isEQ(acmvlonIO.getStatuz(), varcom.endp)) {
				acmvlonIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit2490);
			}
			if (isEQ(acmvlonIO.getEffdate(), loanIO.getLastCapnDate())) {
				if (isEQ(loanIO.getLoanStartDate(), loanIO.getLastCapnDate())
				&& isEQ(acmvlonIO.getGlsign(), wsaaSign)) {
					/*CONTINUE_STMT*/
				}
				else {
					goTo(GotoLabel.readNextAmv2450);
				}
			}
		}
		else {
			if (isNE(acmvlonIO.getRldgcoy(), totloanrec.chdrcoy)
			|| isNE(acmvlonIO.getSacscode(), wsaaSacscode)
			|| isNE(acmvlonIO.getSacstyp(), wsaaSacstype)
			|| isNE(acmvlonIO.getRldgacct(), wsaaRldgacct)
			|| isEQ(acmvlonIO.getStatuz(), varcom.endp)) {
				acmvlonIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit2490);
			}
		}
		/* Get here so we must have a valid record*/
		/* Check currency of ACMV against currency of contract - if*/
		/* diff we must convert ACMV value to a value in the contract*/
		/* currency.*/
		if (isNE(acmvlonIO.getOrigcurr(), chdrenqIO.getCntcurr())) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(acmvlonIO.getOrigamt());
			conlinkrec.statuz.set(SPACES);
			conlinkrec.function.set("REAL");
			/*     MOVE 'SURR'             TO CLNK-FUNCTION*/
			conlinkrec.currIn.set(acmvlonIO.getOrigcurr());
			conlinkrec.cashdate.set(wsaaMaxDate);
			conlinkrec.currOut.set(chdrenqIO.getCntcurr());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(loanIO.getChdrcoy());
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				systemError99000();
			}
			else {
				if (isEQ(acmvlonIO.getGlsign(), "-")) {
					compute(conlinkrec.amountOut, 2).set(mult(conlinkrec.amountOut, -1));
				}
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				wsaaPostedAmount.add(conlinkrec.amountOut);
			}
		}
		else {
			if (isEQ(acmvlonIO.getGlsign(), "-")) {
				setPrecision(acmvlonIO.getOrigamt(), 2);
				acmvlonIO.setOrigamt(mult(acmvlonIO.getOrigamt(), -1));
			}
			wsaaPostedAmount.add(acmvlonIO.getOrigamt());
		}
	}

protected void readNextAmv2450()
	{
		/* Read next ACMV record*/
		acmvlonIO.setFunction(varcom.nextr);
	}

protected void sumLoanPrincipal2500()
	{
		start2500();
	}

protected void start2500()
	{
		/* Use entry 1 on the T5645 record read to get Loan principal*/
		/* ACMVs for this loan number.*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 1));
		/*    MOVE T5645-SACSCODE(WSAA-T5645-IDX)                          */
		/*                                TO WSAA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE(WSAA-T5645-IDX)                          */
		/*                                TO WSAA-SACSTYPE.                */
		/*    MOVE T5645-SACSTYPE(WSAA-T5645-IDX)                          */
		/*                                TO WSAA-SIGN.                    */
		wsaaSacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		wsaaSign.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		wsaaPostedAmount.set(ZERO);
		acmvlonIO.setParams(SPACES);
		acmvlonIO.setRldgcoy(totloanrec.chdrcoy);
		acmvlonIO.setStatuz(SPACES);
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(totloanrec.chdrnum);
		wsaaLoanNumber.set(loanIO.getLoanNumber());
		acmvlonIO.setRldgacct(wsaaRldgacct);
		acmvlonIO.setSacscode(wsaaSacscode);
		acmvlonIO.setSacstyp(wsaaSacstype);
		acmvlonIO.setEffdate(loanIO.getLastCapnDate());
		acmvlonIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvlonIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvlonIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvlonIO.setFormat(acmvlonrec);
		while ( !(isEQ(acmvlonIO.getStatuz(), varcom.endp))) {
			processAcmvlons2400();
		}
		
	}

protected void postInterestAcmv3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* If no interest accrued, then just update LOAN record - don't*/
		/*  write a Zero ACMV.*/
		if (isEQ(wsaaIntcInterest, ZERO)) {
			return ;
		}
		/* Use entries 3 & 4 on the T5645 record read earlier for our*/
		/* interest postings if it is a policy loan being processed*/
		/* or Use entries 7 & 8 on the T5645 record read earlier for*/
		/* our interest postings if it is an APL loan.*/
		/* Set up lifacmv fields.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(totloanrec.chdrnum);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.batccoy.set(totloanrec.chdrcoy);
		lifacmvrec.rldgcoy.set(totloanrec.chdrcoy);
		lifacmvrec.genlcoy.set(totloanrec.chdrcoy);
		lifacmvrec.batckey.set(totloanrec.batchkey);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(loanIO.getLoanCurrency());
		lifacmvrec.origamt.set(wsaaIntcInterest);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(totloanrec.tranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(totloanrec.effectiveDate);
		lifacmvrec.tranref.set(totloanrec.chdrnum);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(totloanrec.chdrnum);
		wsaaLoanNumber.set(loanIO.getLoanNumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.transactionDate.set(totloanrec.tranDate);
		lifacmvrec.transactionTime.set(totloanrec.tranTime);
		lifacmvrec.user.set(totloanrec.tranUser);
		lifacmvrec.termid.set(totloanrec.tranTerm);
		lifacmvrec.substituteCode[1].set(chdrenqIO.getCnttype());
		/* Post the interest to the Loan debit account*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 3));
		/*    MOVE T5645-SACSCODE(WSAA-T5645-IDX)                          */
		/*                                TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE(WSAA-T5645-IDX)                          */
		/*                                TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-GLMAP(WSAA-T5645-IDX)                             */
		/*                                TO LIFA-GLCODE.                  */
		/*    MOVE T5645-SIGN(WSAA-T5645-IDX)                              */
		/*                                TO LIFA-GLSIGN.                  */
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaT5645Idx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
		/* Post the interest to the interest income account*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 4));
		/*    MOVE T5645-SACSCODE(WSAA-T5645-IDX)                          */
		/*                                TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE(WSAA-T5645-IDX)                          */
		/*                                TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-GLMAP(WSAA-T5645-IDX)                             */
		/*                                TO LIFA-GLCODE.                  */
		/*    MOVE T5645-SIGN(WSAA-T5645-IDX)                              */
		/*                                TO LIFA-GLSIGN.                  */
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaT5645Idx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaT5645Idx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaT5645Idx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaT5645Idx.toInt()]);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void updateLoan3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3500();
				case datcon2Loop3550: 
					datcon2Loop3550();
				case updateLoan3550: 
					updateLoan3550();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3500()
	{
		/* Read T6633 so that we can work out what the next interest*/
		/* billing date should be set to.*/
		readT66333600();
		/* Check the interest  details on T6633 in the following*/
		/*  order: i) Calculate interest on Loan anniv ... Y/N*/
		/*        ii) Calculate interest on Policy anniv.. Y/N*/
		/*       iii) Check Int freq & whether a specific Day is chosen*/
		loanIO.setLastIntBillDate(totloanrec.effectiveDate);
		wsaaLoanDate.set(loanIO.getLoanStartDate());
		wsaaEffdate.set(totloanrec.effectiveDate);
		wsaaContractDate.set(chdrenqIO.getOccdate());
		/* Check for loan anniversary flag set*/
		/* IF set,*/
		/*    set next interest billing date to be on the next loan*/
		/*    anniv date after the Effective date we are using now.*/
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(loanIO.getLoanStartDate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2, totloanrec.effectiveDate))) {
				callDatcon23700();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}
			
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			goTo(GotoLabel.updateLoan3550);
		}
		/* Check for contract anniversary flag set*/
		/* IF set,*/
		/*  set next interest billing date to be on the next contract*/
		/*  anniversary date after the Effective date we are using now.*/
		if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(chdrenqIO.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2, totloanrec.effectiveDate))) {
				callDatcon23700();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}
			
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			goTo(GotoLabel.updateLoan3550);
		}
		/* Get here so the next interest calc. date isn't based on loan*/
		/* or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* Check if table T6633 has a fixed day of the month specified*/
		/* ...if not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3800();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		/* Check if table T6633 has a fixed frequency for interest,*/
		/* calcs ...if not, use 1 year as the default interest calc.*/
		/* frequency.*/
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon2rec.frequency.set("01");
		}
		else {
			datcon2rec.frequency.set(t6633rec.interestFrequency);
		}
	}

protected void datcon2Loop3550()
	{
		callDatcon23700();
		if (isLTE(datcon2rec.intDate2, totloanrec.effectiveDate)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			goTo(GotoLabel.datcon2Loop3550);
		}
		if (!daysLessThan28.isTrue()) {
			if (wsaaMonthCheckInner.february.isTrue()) {
				wsaaNewDate.set(datcon2rec.intDate2);
				wsaaNewDay.set(t6633rec.interestDay);
				loanIO.setNextIntBillDate(wsaaNewDate);
			}
			else {
				if (wsaaMonthCheckInner.april.isTrue()
				|| wsaaMonthCheckInner.june.isTrue()
				|| wsaaMonthCheckInner.september.isTrue()
				|| wsaaMonthCheckInner.november.isTrue()) {
					if (daysInJan.isTrue()) {
						wsaaNewDate.set(datcon2rec.intDate2);
						wsaaNewDay.set(t6633rec.interestDay);
						loanIO.setNextIntBillDate(wsaaNewDate);
					}
					else {
						loanIO.setNextIntBillDate(datcon2rec.intDate2);
					}
				}
				else {
					loanIO.setNextIntBillDate(datcon2rec.intDate2);
				}
			}
		}
		else {
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
		}
	}

	/**
	* <pre>
	**** Having now got the next interest billing date, REWRiTe the
	**** LOAN record
	* </pre>
	*/
protected void updateLoan3550()
	{
		/* MOVE REWRT                  TO LOAN-FUNCTION.                */
		loanIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			databaseError99500();
		}
		/*EXIT*/
	}

protected void readT66333600()
	{
		start3600();
	}

protected void start3600()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(totloanrec.chdrcoy);
		itdmIO.setItemtabl(t6633);
		wsaaT6633Cnttype.set(chdrenqIO.getCnttype());
		wsaaT6633Type.set(loanIO.getLoanType());
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(totloanrec.effectiveDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(), totloanrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT6633Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			databaseError99500();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void callDatcon23700()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError99000();
		}
		/*EXIT*/
	}

protected void dateSet3800()
	{
		/*START*/
		/* We have to check that the date we are going to call Datcon2*/
		/* with is a valid from-date... ie IF the interest/capn day in*/
		/* T6633 is > 28, we have to make sure the from-date isn't*/
		/* something like 31/02/nnnn or 31/06/nnnn*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		totloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		start99500();
		exit99590();
	}

protected void start99500()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		totloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void a000CallRounding()
	{
		a100Call();
	}

protected void a100Call()
	{
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(totloanrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrenqIO.getCntcurr());
		if (isEQ(wsaaBatckey.batcBatctrcde, SPACES)) {
			zrdecplrec.batctrcde.set("****");
		}
		else {
			zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		}
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner { 

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}
}
