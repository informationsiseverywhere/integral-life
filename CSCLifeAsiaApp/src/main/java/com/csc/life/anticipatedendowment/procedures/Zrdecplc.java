/*
 * File: Zrdecplc.java
 * Date: 30 August 2009 2:55:53
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRDECPLC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.anticipatedendowment.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.recordstructures.Zrdecplrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1667rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This program will, given an amount and a currency code, access
* table T1667 (used also by the AMTCON subroutine) and convert
* the amount given to one with the required number of decimal
* places as specified in this table for the corresponding
* currency.
*
* This been designed specifically for those cases where an amount
* has been calculated (outside of this routine) but the number of
* decimal places used may be inappropriate for the currency.
* eg. Indonesian Rupiah has no need for any decimal places so it
* is meaningless and may be confusing to display amounts in this
* currency with a value in the decimal portion of the number.
*
* FUNCTION
*    No specific function required (as yet).
*
*
* LINKAGE AREA:
*     Function                PIC X(04)  '    '
*     Amount In               PIC S9(15)V9(03)
*     Currency                PIC X(03).
*     Amount Out              PIC S9(15)V9(03)
*     Status                  PIC X(04)
*    (These fields are contained within ZRDPLACREC.)
*
* EXAMPLES:
*
*   AMOUNT-IN  CURRENCY    Decimal Places    AMOUNT-OUT   STATUS
*                              (T1667)
* 25689732.57     RP              0          25689733.00   ****
* 12348976.45     RP              0          12348976.00   ****
* 56874321.15     USD             2          56874321.15   ****
* 47824948.29     @@@             1          47824948.30   ****
*
* Where @@@ is some other currency defined on T1667 as having
* only 1 decimal place.
*
***********************************************************
* </pre>
*/
public class Zrdecplc extends COBOLConvCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrdecplc.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ZonedDecimalData wsaaNumber = new ZonedDecimalData(18, 3);

	private FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(wsaaNumber, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaa3Decimals = new ZonedDecimalData(18, 3).isAPartOf(filler, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(18).isAPartOf(wsaaNumber, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaa2Decimals = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(18).isAPartOf(wsaaNumber, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaa1Decimal = new ZonedDecimalData(16, 1).isAPartOf(filler3, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(18).isAPartOf(wsaaNumber, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaa0Decimals = new ZonedDecimalData(15, 0).isAPartOf(filler5, 0);

	private ZonedDecimalData wsaaNoOfDec = new ZonedDecimalData(1, 0).setUnsigned();
	private Validator n0DecPlaces = new Validator(wsaaNoOfDec, "0");
	private Validator n1DecPlace = new Validator(wsaaNoOfDec, "1");
	private Validator n2DecPlaces = new Validator(wsaaNoOfDec, "2");
	private Validator n3DecPlaces = new Validator(wsaaNoOfDec, "3");
		/* TABLES */
	private String t1667 = "T1667";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T1667rec t1667rec = new T1667rec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090
	}

	public Zrdecplc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		zrdecplrec.zrdecplRec = convertAndSetParam(zrdecplrec.zrdecplRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
				LOGGER.debug("Flow Ended");
		}
	}

protected void mainline000()
	{
		try {
			validation010();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
		finally{
			exit090();
		}
	}

protected void validation010()
	{
		zrdecplrec.statuz.set(varcom.oK);
		wsaaNumber.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1667);
		itemIO.setItemitem(zrdecplrec.currency);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			zrdecplrec.statuz.set(itemIO.getStatuz());
			goTo(GotoLabel.exit090);
		}
		t1667rec.t1667Rec.set(itemIO.getGenarea());
		wsaaNoOfDec.set(t1667rec.fieldDecimalPlaces);
		if (n0DecPlaces.isTrue()) {
			wsaa0Decimals.setRounded(zrdecplrec.amountIn);
			zrdecplrec.amountOut.set(wsaa0Decimals);
		}
		else {
			if (n1DecPlace.isTrue()) {
				wsaa1Decimal.setRounded(zrdecplrec.amountIn);
				zrdecplrec.amountOut.set(wsaa1Decimal);
			}
			else {
				if (n2DecPlaces.isTrue()) {
					wsaa2Decimals.setRounded(zrdecplrec.amountIn);
					zrdecplrec.amountOut.set(wsaa2Decimals);
				}
				else {
					if (n3DecPlaces.isTrue()) {
						wsaa3Decimals.setRounded(zrdecplrec.amountIn);
						zrdecplrec.amountOut.set(wsaa3Decimals);
					}
				}
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}
}
