package com.csc.life.anticipatedendowment.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.Bd5huDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5huDTO;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Bd5huDAOImpl extends BaseDAOImpl<Rgpdetpf> implements Bd5huDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5huDAOImpl.class);
	
	public List<Rgpdetpf> loadDataByBatch(int BatchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM VM1DTA.BD5HUDATA ");
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Rgpdetpf> regpList = new ArrayList<Rgpdetpf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(BatchID));
			
			rs = ps.executeQuery();
			while(rs.next()){
				Rgpdetpf rpdetpf = new Rgpdetpf();
				Bd5huDTO bd5huDTO = new Bd5huDTO();
				rpdetpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				rpdetpf.setChdrcoy(rs.getString("Chdrcoy"));
                rpdetpf.setChdrnum(rs.getString("Chdrnum"));
                rpdetpf.setPlnsfx(rs.getInt("Plnsfx"));
                rpdetpf.setLife(rs.getString("Life"));
                rpdetpf.setCoverage(rs.getString("Coverage"));
                rpdetpf.setRider(rs.getString("Rider"));                
                rpdetpf.setValidflag(rs.getString("Validflag"));
                rpdetpf.setTranno(rs.getString("RPTRANNO"));               
                rpdetpf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
                rpdetpf.setApintamt(rs.getBigDecimal("APINTAMT") == null?BigDecimal.ZERO:rs.getBigDecimal("APINTAMT"));	
                rpdetpf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
                rpdetpf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
                rpdetpf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
                rpdetpf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));                
                rpdetpf.setCurrcd(rs.getString("CURRCD"));
                
                bd5huDTO.setCnt(rs.getInt("CNT"));
				bd5huDTO.setUniqueNumber(rs.getLong("CH_UNIQUE_NUMBER"));
				bd5huDTO.setChdrcoy(rs.getString("CHDRCOY").trim());
				bd5huDTO.setChdrnum(rs.getString("CHDRNUM").trim());
				bd5huDTO.setCurrfrom(rs.getInt("CURRFROM"));
				bd5huDTO.setTranno(rs.getInt("TRANNO"));
				bd5huDTO.setCnttype(rs.getString("CNTTYPE").trim());
				bd5huDTO.setChdrpfx(rs.getString("CHDRPFX").trim());
				bd5huDTO.setServunit(rs.getString("SERVUNIT").trim());
				bd5huDTO.setOccdate(rs.getInt("OCCDATE"));
				bd5huDTO.setCcdate(rs.getInt("CCDATE"));
				bd5huDTO.setCownpfx(rs.getString("COWNPFX").trim());
				bd5huDTO.setCowncoy(rs.getString("COWNCOY").trim());
				bd5huDTO.setCownnum(rs.getString("COWNNUM").trim());				
				bd5huDTO.setCollchnl(rs.getString("COLLCHNL").trim());
				bd5huDTO.setCntbranch(rs.getString("CNTBRANCH").trim());
				bd5huDTO.setAgntpfx(rs.getString("AGNTPFX").trim());				
				bd5huDTO.setAgntcoy(rs.getString("AGNTCOY").trim());
				bd5huDTO.setAgntnum(rs.getString("AGNTNUM").trim());
				rpdetpf.setBd5huDTO(bd5huDTO);
				regpList.add(rpdetpf);	
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return regpList;
	}	
	
	public int populateBd5huTemp(String coy, String effDate, int batchExtractSize) {
		initializeBd5huTemp();
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.BD5HUDATA ");
		sb.append("(BATCHID, CNT, UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,VALIDFLAG,CURRCD,RPTRANNO, APCAPLAMT, APINTAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, APNXTINTBDTE, USRPRF, JOBNM, CH_UNIQUE_NUMBER, CURRFROM, TRANNO, CNTTYPE, CHDRPFX, SERVUNIT, OCCDATE, CCDATE, COWNPFX, COWNCOY, COWNNUM, COLLCHNL, CNTBRANCH, AGNTPFX, AGNTCOY, AGNTNUM, DATIME) ");
		sb.append("SELECT * FROM( ");
		sb.append("SELECT " );		
		sb.append("floor((row_number()over(ORDER BY RP.CHDRCOY ASC, RP.CHDRNUM ASC)-1)/?) batchnum, count(*) over(partition by RP.CHDRCOY,RP.CHDRNUM) as cnt, ");
		sb.append("RP.UNIQUE_NUMBER, RP.CHDRCOY,RP.CHDRNUM,RP.PLNSFX,RP.LIFE,RP.COVERAGE,RP.RIDER,RP.VALIDFLAG,R.CURRCD,RP.TRANNO RPTRANNO,");
		sb.append("RP.APCAPLAMT LSTCAPLAMT,RP.APINTAMT, RP.APLSTCAPDATE LSTCAPDATE, RP.APNXTCAPDATE NXTCAPDATE, "); 
		sb.append("RP.APLSTINTBDTE LSTINTBDTE, RP.APNXTINTBDTE NXTINTBDTE,RP.USRPRF, RP.JOBNM, ");  	
		sb.append("CH.UNIQUE_NUMBER CH_UNIQUE_NUMBER, CH.CURRFROM, CH.TRANNO, CH.CNTTYPE, CH.CHDRPFX, CH.SERVUNIT, CH.OCCDATE, CH.CCDATE, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM, CH.COLLCHNL, CH.CNTBRANCH, CH.AGNTPFX, CH.AGNTCOY, CH.AGNTNUM, ? DATIME ");		 
		sb.append("FROM   REGPPF R ");
		sb.append("INNER JOIN RGPDETPF  RP ON  R.CHDRCOY = RP.CHDRCOY  AND R.CHDRNUM=RP.CHDRNUM  AND R.VALIDFLAG in ('1') ");
		sb.append("INNER JOIN CHDRPF CH ON  CH.CHDRCOY = RP.CHDRCOY  AND CH.CHDRNUM=RP.CHDRNUM  AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG in ('1') ");		 
		sb.append("WHERE RP.VALIDFLAG = '1' ");
		sb.append("AND RP.APCAPLAMT > 0 ");
		sb.append("AND RP.CHDRCOY = ? ");
		sb.append("AND RP.APLSTINTBDTE < ? ");
		sb.append("AND RP.APNXTINTBDTE <= ? ");
	//	sb.append("AND RP.CHDRNUM <= '00028831' ");
		sb.append(") MAIN ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			ps.setString(3, coy);
			ps.setString(4, effDate);
			ps.setString(5, effDate);
			
			rows = ps.executeUpdate();

		}catch (SQLException e) {
			LOGGER.error("populateBd5huTemp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}
	
	private void initializeBd5huTemp() {
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM VM1DTA.BD5HUDATA ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBd5huTemp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}	
}
