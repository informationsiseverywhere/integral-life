package com.csc.life.anticipatedendowment.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.anticipatedendowment.dataaccess.dao.Bd5hvTempDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5hvDTO;
import com.csc.life.productdefinition.dataaccess.model.Rgpdetpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Bd5hvTempDAOImpl extends BaseDAOImpl<Object> implements Bd5hvTempDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5hvTempDAOImpl.class);	

	public boolean deleteBd5hvTempAllRecords(){	
			StringBuilder sb = new StringBuilder("DELETE FROM BD5HVDATA");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	
	            ps.executeUpdate();
			}catch (SQLException e) {
				LOGGER.error("Bd5hvTempDAOImpl()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}		
			
			return true;
		
	}
	
	public int insertBd5hvTempRecords(int batchExtractSize,String coy, int effDate){
		int rows = 0;
		StringBuilder sb = new StringBuilder("insert into BD5HVDATA (BATCHID, CNT, CHDRCOY, CHDRNUM, UNIQUE_NUMBER, PLNSFX, LIFE, COVERAGE, RIDER, CURRCD, VALIDFLAG, APCAPLAMT, APINTAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, APNXTINTBDTE, TRANNO, CNTTYPE, CHDRPFX, SERVUNIT, OCCDATE, CCDATE, COWNPFX, COWNCOY, COWNNUM, COLLCHNL, CNTBRANCH, CNTCURR, POLSUM, POLINC, AGNTPFX, AGNTCOY, AGNTNUM, DATIME) ");
		sb.append("select * from ( ");
		sb.append("select floor((row_number()over(ORDER BY RP.CHDRCOY ASC, RP.CHDRNUM ASC)-1)/?) ROWNM, count(*) over(partition by RP.CHDRCOY,RP.CHDRNUM) as cnt,");
		sb.append("RP.CHDRCOY,RP.CHDRNUM,RP.UNIQUE_NUMBER,RP.PLNSFX,RP.LIFE,RP.COVERAGE,RP.RIDER,  RP.CURRCD, RP.VALIDFLAG, RP.APCAPLAMT, RP.APINTAMT,RP.APLSTCAPDATE,"); 
		sb.append("RP.APNXTCAPDATE, RP.APLSTINTBDTE, RP.APNXTINTBDTE, ");
		sb.append("CH.TRANNO, CH.CNTTYPE, CH.CHDRPFX, CH.SERVUNIT, CH.OCCDATE, CH.CCDATE, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM,");
		sb.append("CH.COLLCHNL, CH.CNTBRANCH, CH.CNTCURR, CH.POLSUM, CH.POLINC, CH.AGNTPFX, CH.AGNTCOY, CH.AGNTNUM, ? AS DATIME ");
		sb.append("FROM   REGPPF R ");
		sb.append("INNER JOIN RGPDETPF  RP ON  R.CHDRCOY = RP.CHDRCOY  AND R.CHDRNUM=RP.CHDRNUM  AND R.VALIDFLAG in ('1') ");
		sb.append("INNER JOIN CHDRPF CH ON  CH.CHDRCOY = RP.CHDRCOY  AND CH.CHDRNUM=RP.CHDRNUM  AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG in ('1','3') ");
		sb.append("WHERE RP.VALIDFLAG = '1'  ");
		sb.append("AND RP.APCAPLAMT > 0 ");
		sb.append(" AND RP.CHDRCOY = ?  AND (RP.APNXTCAPDATE < ?  OR RP.APNXTCAPDATE = ?) ");
		sb.append(") loan order by CHDRCOY, CHDRNUM");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			ps.setString(3, coy);
			ps.setInt(4, effDate);
			ps.setInt(5, effDate);
			rows = ps.executeUpdate();
		}catch (SQLException e) {
			LOGGER.error("insertBd5hvTempRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return rows;
	}
	
	public List<Rgpdetpf> loadDataByNEWINTCP(int BatchID){
		StringBuilder sb = new StringBuilder("SELECT * FROM (");
		sb.append(" SELECT BATCHID,CNT, CHDRCOY, CHDRNUM,UNIQUE_NUMBER, PLNSFX,LIFE,COVERAGE,RIDER, CURRCD,");
		sb.append(" VALIDFLAG, APCAPLAMT, APINTAMT,APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, APNXTINTBDTE, TRANNO, CNTTYPE, CHDRPFX, SERVUNIT, OCCDATE,");
		sb.append(" CCDATE, COWNPFX, COWNCOY, COWNNUM, COLLCHNL, CNTBRANCH, CNTCURR, POLSUM, POLINC, AGNTPFX, AGNTCOY, AGNTNUM ");
		sb.append(" FROM BD5HVDATA");		
		sb.append(") TEMP WHERE BATCHID = ? ");	
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC ");
		//ILIFE-3210
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Rgpdetpf> regpList = new ArrayList<Rgpdetpf>();
		try{
			ps = getPrepareStatement(sb.toString());
			
			ps.setInt(1, BatchID);	
			rs = ps.executeQuery();
			while(rs.next()){
				Rgpdetpf rgpdetpf = new Rgpdetpf();
				rgpdetpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				rgpdetpf.setChdrcoy(rs.getString("CHDRCOY").trim());
				rgpdetpf.setChdrnum(rs.getString("CHDRNUM").trim());
				rgpdetpf.setPlnsfx(rs.getInt("Plnsfx"));
                rgpdetpf.setLife(rs.getString("Life"));
                rgpdetpf.setCoverage(rs.getString("Coverage"));
                rgpdetpf.setRider(rs.getString("Rider"));                
                rgpdetpf.setValidflag(rs.getString("Validflag"));
                rgpdetpf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
                rgpdetpf.setApintamt(rs.getBigDecimal("APINTAMT"));
                rgpdetpf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
                rgpdetpf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
                rgpdetpf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
                rgpdetpf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));
                rgpdetpf.setCurrcd(rs.getString("Currcd"));
				Bd5hvDTO bd5hvdto = new Bd5hvDTO();
				bd5hvdto.setCnt(rs.getInt("CNT"));
				bd5hvdto.setTranno(rs.getInt("TRANNO"));
				bd5hvdto.setCnttype(rs.getString("CNTTYPE").trim());
				bd5hvdto.setChdrpfx(rs.getString("CHDRPFX").trim());
				bd5hvdto.setServunit(rs.getString("SERVUNIT").trim());
				bd5hvdto.setOccdate(rs.getInt("OCCDATE"));
				bd5hvdto.setCcdate(rs.getInt("CCDATE"));
				bd5hvdto.setCownpfx(rs.getString("COWNPFX").trim());
				bd5hvdto.setCowncoy(rs.getString("COWNCOY").trim());
				bd5hvdto.setCownnum(rs.getString("COWNNUM").trim());				
				bd5hvdto.setCollchnl(rs.getString("COLLCHNL").trim());
				bd5hvdto.setCntbranch(rs.getString("CNTBRANCH").trim());
				bd5hvdto.setCntcurr(rs.getString("CNTCURR").trim());
				bd5hvdto.setPolinc(rs.getObject("POLINC") != null ? (rs.getInt("POLINC")) : 0);
				bd5hvdto.setPolsum(rs.getObject("POLSUM") != null ? (rs.getInt("POLSUM")) : 0);
				bd5hvdto.setAgntpfx(rs.getString("AGNTPFX").trim());				
				bd5hvdto.setAgntcoy(rs.getString("AGNTCOY").trim());
				bd5hvdto.setAgntnum(rs.getString("AGNTNUM").trim());
				
				rgpdetpf.setBd5hvDTO(bd5hvdto);
				regpList.add(rgpdetpf);	
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByEffdate()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return regpList;
	}
	

}
