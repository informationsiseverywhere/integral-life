package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UtrxpfDAO extends BaseDAO<Utrxpf> {
	public List<Utrxpf> searchUtrxpfRecord(String tableId, String memName, int batchExtractSize, int batchID);
	public void insertUtrxRecoed(List<Utrxpf> utrxBulkOpList, String tableId);
}