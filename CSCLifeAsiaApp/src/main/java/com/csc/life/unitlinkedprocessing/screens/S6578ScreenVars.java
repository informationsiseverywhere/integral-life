package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6578
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6578ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(260);
	public FixedLengthStringData dataFields = new FixedLengthStringData(164).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData ustprcoms = new FixedLengthStringData(120).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] ustprcom = FLSArrayPartOfStructure(2, 60, ustprcoms, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(ustprcoms, 0, FILLER_REDEFINE);
	public FixedLengthStringData ustprcom1 = new FixedLengthStringData(60).isAPartOf(filler, 0);
	public FixedLengthStringData ustprcom2 = new FixedLengthStringData(60).isAPartOf(filler, 60);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 164);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ustprcomsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] ustprcomErr = FLSArrayPartOfStructure(2, 4, ustprcomsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(ustprcomsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ustprcom1Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData ustprcom2Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 188);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData ustprcomsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] ustprcomOut = FLSArrayPartOfStructure(2, 12, ustprcomsOut, 0);
	public FixedLengthStringData[][] ustprcomO = FLSDArrayPartOfArrayStructure(12, 1, ustprcomOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(ustprcomsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ustprcom1Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] ustprcom2Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6578screenWritten = new LongData(0);
	public LongData S6578protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6578ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ustprcom1Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ustprcom2Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6578screen.class;
		protectRecord = S6578protect.class;
	}

}
