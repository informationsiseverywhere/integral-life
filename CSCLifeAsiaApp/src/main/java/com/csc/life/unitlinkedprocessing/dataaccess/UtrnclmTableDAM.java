package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrnclmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:04
 * Class transformed from UTRNCLM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrnclmTableDAM extends UtrnpfTableDAM {

	public UtrnclmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UTRNCLM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", VRTFND"
		             + ", UNITYP";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VRTFND, " +
		            "UNITYP, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "CRTABLE, " +
		            "CNTCURR, " +
		            "FDBKIND, " +
		            "CNTAMNT, " +
		            "FNDCURR, " +
		            "FUNDAMNT, " +
		            "CONTYP, " +
		            "TRIGER, " +
		            "TRIGKY, " +
		            "PRCSEQ, " +
		            "SVP, " +
		            "DISCFA, " +
		            "PERSUR, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "VRTFND ASC, " +
		            "UNITYP ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "VRTFND DESC, " +
		            "UNITYP DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               coverage,
                               rider,
                               planSuffix,
                               unitVirtualFund,
                               unitType,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               crtable,
                               cntcurr,
                               feedbackInd,
                               contractAmount,
                               fundCurrency,
                               fundAmount,
                               contractType,
                               triggerModule,
                               triggerKey,
                               procSeqNo,
                               svp,
                               discountFactor,
                               surrenderPercent,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(coverage.toInternal());
	nonKeyFiller40.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(unitVirtualFund.toInternal());
	nonKeyFiller70.setInternal(unitType.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(175);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getPlanSuffix().toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getCrtable().toInternal()
					+ getCntcurr().toInternal()
					+ getFeedbackInd().toInternal()
					+ getContractAmount().toInternal()
					+ getFundCurrency().toInternal()
					+ getFundAmount().toInternal()
					+ getContractType().toInternal()
					+ getTriggerModule().toInternal()
					+ getTriggerKey().toInternal()
					+ getProcSeqNo().toInternal()
					+ getSvp().toInternal()
					+ getDiscountFactor().toInternal()
					+ getSurrenderPercent().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, feedbackInd);
			what = ExternalData.chop(what, contractAmount);
			what = ExternalData.chop(what, fundCurrency);
			what = ExternalData.chop(what, fundAmount);
			what = ExternalData.chop(what, contractType);
			what = ExternalData.chop(what, triggerModule);
			what = ExternalData.chop(what, triggerKey);
			what = ExternalData.chop(what, procSeqNo);
			what = ExternalData.chop(what, svp);
			what = ExternalData.chop(what, discountFactor);
			what = ExternalData.chop(what, surrenderPercent);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(Object what) {
		feedbackInd.set(what);
	}	
	public PackedDecimalData getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(Object what) {
		setContractAmount(what, false);
	}
	public void setContractAmount(Object what, boolean rounded) {
		if (rounded)
			contractAmount.setRounded(what);
		else
			contractAmount.set(what);
	}	
	public FixedLengthStringData getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(Object what) {
		fundCurrency.set(what);
	}	
	public PackedDecimalData getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(Object what) {
		setFundAmount(what, false);
	}
	public void setFundAmount(Object what, boolean rounded) {
		if (rounded)
			fundAmount.setRounded(what);
		else
			fundAmount.set(what);
	}	
	public FixedLengthStringData getContractType() {
		return contractType;
	}
	public void setContractType(Object what) {
		contractType.set(what);
	}	
	public FixedLengthStringData getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(Object what) {
		triggerModule.set(what);
	}	
	public FixedLengthStringData getTriggerKey() {
		return triggerKey;
	}
	public void setTriggerKey(Object what) {
		triggerKey.set(what);
	}	
	public PackedDecimalData getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(Object what) {
		setProcSeqNo(what, false);
	}
	public void setProcSeqNo(Object what, boolean rounded) {
		if (rounded)
			procSeqNo.setRounded(what);
		else
			procSeqNo.set(what);
	}	
	public PackedDecimalData getSvp() {
		return svp;
	}
	public void setSvp(Object what) {
		setSvp(what, false);
	}
	public void setSvp(Object what, boolean rounded) {
		if (rounded)
			svp.setRounded(what);
		else
			svp.set(what);
	}	
	public PackedDecimalData getDiscountFactor() {
		return discountFactor;
	}
	public void setDiscountFactor(Object what) {
		setDiscountFactor(what, false);
	}
	public void setDiscountFactor(Object what, boolean rounded) {
		if (rounded)
			discountFactor.setRounded(what);
		else
			discountFactor.set(what);
	}	
	public PackedDecimalData getSurrenderPercent() {
		return surrenderPercent;
	}
	public void setSurrenderPercent(Object what) {
		setSurrenderPercent(what, false);
	}
	public void setSurrenderPercent(Object what, boolean rounded) {
		if (rounded)
			surrenderPercent.setRounded(what);
		else
			surrenderPercent.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		coverage.clear();
		rider.clear();
		unitVirtualFund.clear();
		unitType.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		planSuffix.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		crtable.clear();
		cntcurr.clear();
		feedbackInd.clear();
		contractAmount.clear();
		fundCurrency.clear();
		fundAmount.clear();
		contractType.clear();
		triggerModule.clear();
		triggerKey.clear();
		procSeqNo.clear();
		svp.clear();
		discountFactor.clear();
		surrenderPercent.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}