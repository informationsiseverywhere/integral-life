package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5536screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5536ScreenVars sv = (S5536ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5536screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5536ScreenVars screenVars = (S5536ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.billfreq01.setClassString("");
		screenVars.billfreq02.setClassString("");
		screenVars.billfreq03.setClassString("");
		screenVars.maxPerioda01.setClassString("");
		screenVars.pcUnitsa01.setClassString("");
		screenVars.pcInitUnitsa01.setClassString("");
		screenVars.maxPeriodb01.setClassString("");
		screenVars.pcUnitsb01.setClassString("");
		screenVars.pcInitUnitsb01.setClassString("");
		screenVars.maxPeriodc01.setClassString("");
		screenVars.pcUnitsc01.setClassString("");
		screenVars.pcInitUnitsc01.setClassString("");
		screenVars.maxPerioda02.setClassString("");
		screenVars.pcUnitsa02.setClassString("");
		screenVars.pcInitUnitsa02.setClassString("");
		screenVars.maxPeriodb02.setClassString("");
		screenVars.pcUnitsb02.setClassString("");
		screenVars.pcInitUnitsb02.setClassString("");
		screenVars.maxPeriodc02.setClassString("");
		screenVars.pcUnitsc02.setClassString("");
		screenVars.pcInitUnitsc02.setClassString("");
		screenVars.maxPerioda03.setClassString("");
		screenVars.pcUnitsa03.setClassString("");
		screenVars.pcInitUnitsa03.setClassString("");
		screenVars.maxPeriodb03.setClassString("");
		screenVars.pcUnitsb03.setClassString("");
		screenVars.pcInitUnitsb03.setClassString("");
		screenVars.maxPeriodc03.setClassString("");
		screenVars.pcUnitsc03.setClassString("");
		screenVars.pcInitUnitsc03.setClassString("");
		screenVars.maxPerioda04.setClassString("");
		screenVars.pcUnitsa04.setClassString("");
		screenVars.pcInitUnitsa04.setClassString("");
		screenVars.maxPeriodb04.setClassString("");
		screenVars.pcUnitsb04.setClassString("");
		screenVars.pcInitUnitsb04.setClassString("");
		screenVars.maxPeriodc04.setClassString("");
		screenVars.pcUnitsc04.setClassString("");
		screenVars.pcInitUnitsc04.setClassString("");
		screenVars.billfreq04.setClassString("");
		screenVars.billfreq05.setClassString("");
		screenVars.billfreq06.setClassString("");
		screenVars.maxPeriodd01.setClassString("");
		screenVars.pcUnitsd01.setClassString("");
		screenVars.pcInitUnitsd01.setClassString("");
		screenVars.maxPeriode01.setClassString("");
		screenVars.pcUnitse01.setClassString("");
		screenVars.pcInitUnitse01.setClassString("");
		screenVars.maxPeriodf01.setClassString("");
		screenVars.pcUnitsf01.setClassString("");
		screenVars.pcInitUnitsf01.setClassString("");
		screenVars.maxPeriodd02.setClassString("");
		screenVars.pcUnitsd02.setClassString("");
		screenVars.pcInitUnitsd02.setClassString("");
		screenVars.maxPeriode02.setClassString("");
		screenVars.pcUnitse02.setClassString("");
		screenVars.pcInitUnitse02.setClassString("");
		screenVars.maxPeriodf02.setClassString("");
		screenVars.pcUnitsf02.setClassString("");
		screenVars.pcInitUnitsf02.setClassString("");
		screenVars.maxPeriodd03.setClassString("");
		screenVars.pcUnitsd03.setClassString("");
		screenVars.pcInitUnitsd03.setClassString("");
		screenVars.maxPeriode03.setClassString("");
		screenVars.pcUnitse03.setClassString("");
		screenVars.pcInitUnitse03.setClassString("");
		screenVars.maxPeriodf03.setClassString("");
		screenVars.pcUnitsf03.setClassString("");
		screenVars.pcInitUnitsf03.setClassString("");
		screenVars.maxPeriodd04.setClassString("");
		screenVars.pcUnitsd04.setClassString("");
		screenVars.pcInitUnitsd04.setClassString("");
		screenVars.maxPeriode04.setClassString("");
		screenVars.pcUnitse04.setClassString("");
		screenVars.pcInitUnitse04.setClassString("");
		screenVars.maxPeriodf04.setClassString("");
		screenVars.pcUnitsf04.setClassString("");
		screenVars.pcInitUnitsf04.setClassString("");
		screenVars.yotalmth.setClassString("");
		
		
		screenVars.maxPerioda05.setClassString("");
		screenVars.maxPerioda06 .setClassString("");
		screenVars.maxPerioda07.setClassString("");
		
		screenVars.maxPeriodb05 .setClassString("");
		screenVars.maxPeriodb06.setClassString("");
		screenVars.maxPeriodb07 .setClassString("");
		
		screenVars.maxPeriodc05.setClassString("");  
		screenVars.maxPeriodc06 .setClassString("");
		screenVars.maxPeriodc07 .setClassString("");
		
		
		screenVars.maxPeriodd05 .setClassString("");
		screenVars.maxPeriodd06 .setClassString("");
		screenVars.maxPeriodd07 .setClassString("");
		
		screenVars.maxPeriode05 .setClassString("");
		screenVars.maxPeriode06 .setClassString("");
		screenVars.maxPeriode07 .setClassString("");
		
		
		screenVars.maxPeriodf05 .setClassString("");
		screenVars.maxPeriodf06 .setClassString("");
		screenVars.maxPeriodf07 .setClassString("");
		
		
		screenVars.pcInitUnitsa05 .setClassString("");
		screenVars.pcInitUnitsa06 .setClassString("");
		screenVars.pcInitUnitsa07 .setClassString("");
		
		
		screenVars.pcInitUnitsb05.setClassString("");
		screenVars.pcInitUnitsb06 .setClassString("");
		screenVars.pcInitUnitsb07 .setClassString("");
		
		
		screenVars.pcInitUnitsc05 .setClassString("");
		screenVars.pcInitUnitsc06 .setClassString("");
		screenVars.pcInitUnitsc07 .setClassString("");
		screenVars.pcInitUnitsd05 .setClassString("");
		screenVars.pcInitUnitsd06 .setClassString("");
		screenVars.pcInitUnitsd07 .setClassString("");
		
		
		screenVars.pcInitUnitse05 .setClassString("");
		screenVars.pcInitUnitse06 .setClassString("");
		screenVars.pcInitUnitse07 .setClassString("");
		
		screenVars.pcInitUnitsf05 .setClassString("");
		screenVars.pcInitUnitsf06 .setClassString(""); 
		screenVars.pcInitUnitsf07 .setClassString(""); 
		
		
		screenVars.pcUnitsa05 .setClassString("");
		screenVars.pcUnitsa06 .setClassString("");
		screenVars.pcUnitsa07 .setClassString("");
		
		
		screenVars.pcUnitsb05 .setClassString("");
		screenVars.pcUnitsb06 .setClassString("");
		screenVars.pcUnitsb07 .setClassString("");
		
		
		screenVars.pcUnitsc05 .setClassString("");
		screenVars.pcUnitsc06 .setClassString("");
		screenVars.pcUnitsc07 .setClassString(""); 
		
		
		screenVars.pcUnitsd05 .setClassString("");
		screenVars.pcUnitsd06 .setClassString("");
		screenVars.pcUnitsd07 .setClassString("");
		
		screenVars.pcUnitse05 .setClassString("");
		screenVars.pcUnitse06 .setClassString("");
		screenVars.pcUnitse07 .setClassString("");
		
		screenVars.pcUnitsf05 .setClassString("");
		screenVars.pcUnitsf06 .setClassString("");
		screenVars.pcUnitsf07 .setClassString("");
	}

/**
 * Clear all the variables in S5536screen
 */
	public static void clear(VarModel pv) {
		S5536ScreenVars screenVars = (S5536ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.billfreq01.clear();
		screenVars.billfreq02.clear();
		screenVars.billfreq03.clear();
		screenVars.maxPerioda01.clear();
		screenVars.pcUnitsa01.clear();
		screenVars.pcInitUnitsa01.clear();
		screenVars.maxPeriodb01.clear();
		screenVars.pcUnitsb01.clear();
		screenVars.pcInitUnitsb01.clear();
		screenVars.maxPeriodc01.clear();
		screenVars.pcUnitsc01.clear();
		screenVars.pcInitUnitsc01.clear();
		screenVars.maxPerioda02.clear();
		screenVars.pcUnitsa02.clear();
		screenVars.pcInitUnitsa02.clear();
		screenVars.maxPeriodb02.clear();
		screenVars.pcUnitsb02.clear();
		screenVars.pcInitUnitsb02.clear();
		screenVars.maxPeriodc02.clear();
		screenVars.pcUnitsc02.clear();
		screenVars.pcInitUnitsc02.clear();
		screenVars.maxPerioda03.clear();
		screenVars.pcUnitsa03.clear();
		screenVars.pcInitUnitsa03.clear();
		screenVars.maxPeriodb03.clear();
		screenVars.pcUnitsb03.clear();
		screenVars.pcInitUnitsb03.clear();
		screenVars.maxPeriodc03.clear();
		screenVars.pcUnitsc03.clear();
		screenVars.pcInitUnitsc03.clear();
		screenVars.maxPerioda04.clear();
		screenVars.pcUnitsa04.clear();
		screenVars.pcInitUnitsa04.clear();
		screenVars.maxPeriodb04.clear();
		screenVars.pcUnitsb04.clear();
		screenVars.pcInitUnitsb04.clear();
		screenVars.maxPeriodc04.clear();
		screenVars.pcUnitsc04.clear();
		screenVars.pcInitUnitsc04.clear();
		screenVars.billfreq04.clear();
		screenVars.billfreq05.clear();
		screenVars.billfreq06.clear();
		screenVars.maxPeriodd01.clear();
		screenVars.pcUnitsd01.clear();
		screenVars.pcInitUnitsd01.clear();
		screenVars.maxPeriode01.clear();
		screenVars.pcUnitse01.clear();
		screenVars.pcInitUnitse01.clear();
		screenVars.maxPeriodf01.clear();
		screenVars.pcUnitsf01.clear();
		screenVars.pcInitUnitsf01.clear();
		screenVars.maxPeriodd02.clear();
		screenVars.pcUnitsd02.clear();
		screenVars.pcInitUnitsd02.clear();
		screenVars.maxPeriode02.clear();
		screenVars.pcUnitse02.clear();
		screenVars.pcInitUnitse02.clear();
		screenVars.maxPeriodf02.clear();
		screenVars.pcUnitsf02.clear();
		screenVars.pcInitUnitsf02.clear();
		screenVars.maxPeriodd03.clear();
		screenVars.pcUnitsd03.clear();
		screenVars.pcInitUnitsd03.clear();
		screenVars.maxPeriode03.clear();
		screenVars.pcUnitse03.clear();
		screenVars.pcInitUnitse03.clear();
		screenVars.maxPeriodf03.clear();
		screenVars.pcUnitsf03.clear();
		screenVars.pcInitUnitsf03.clear();
		screenVars.maxPeriodd04.clear();
		screenVars.pcUnitsd04.clear();
		screenVars.pcInitUnitsd04.clear();
		screenVars.maxPeriode04.clear();
		screenVars.pcUnitse04.clear();
		screenVars.pcInitUnitse04.clear();
		screenVars.maxPeriodf04.clear();
		screenVars.pcUnitsf04.clear();
		screenVars.pcInitUnitsf04.clear();
		screenVars.yotalmth.clear();
		
		
		screenVars.maxPerioda05.clear(); 
		screenVars.maxPerioda06 .clear(); 
		screenVars.maxPerioda07.clear(); 
		
		screenVars.maxPeriodb05 .clear(); 
		screenVars.maxPeriodb06.clear(); 
		screenVars.maxPeriodb07 .clear(); 
		
		screenVars.maxPeriodc05.clear();  
		screenVars.maxPeriodc06 .clear(); 
		screenVars.maxPeriodc07 .clear(); 
		
		
		screenVars.maxPeriodd05 .clear(); 
		screenVars.maxPeriodd06 .clear(); 
		screenVars.maxPeriodd07 .clear(); 
		
		screenVars.maxPeriode05 .clear(); 
		screenVars.maxPeriode06 .clear(); 
		screenVars.maxPeriode07 .clear(); 
		
		
		screenVars.maxPeriodf05 .clear(); 
		screenVars.maxPeriodf06 .clear(); 
		screenVars.maxPeriodf07 .clear(); 
		
		
		screenVars.pcInitUnitsa05 .clear(); 
		screenVars.pcInitUnitsa06 .clear(); 
		screenVars.pcInitUnitsa07 .clear(); 
		
		
		screenVars.pcInitUnitsb05.clear();  
		screenVars.pcInitUnitsb06 .clear(); 
		screenVars.pcInitUnitsb07 .clear(); 
		
		
		screenVars.pcInitUnitsc05 .clear(); 
		screenVars.pcInitUnitsc06 .clear(); 
		screenVars.pcInitUnitsc07 .clear(); 
		
		screenVars.pcInitUnitsd05 .clear(); 
		screenVars.pcInitUnitsd06 .clear(); 
		screenVars.pcInitUnitsd07 .clear(); 
		
		
		screenVars.pcInitUnitse05 .clear(); 
		screenVars.pcInitUnitse06 .clear(); 
		screenVars.pcInitUnitse07 .clear(); 
		
		screenVars.pcInitUnitsf05 .clear(); 
		screenVars.pcInitUnitsf06 .clear(); 
		screenVars.pcInitUnitsf07 .clear(); 
		
		
		screenVars.pcUnitsa05 .clear(); 
		screenVars.pcUnitsa06 .clear(); 
		screenVars.pcUnitsa07 .clear(); 
		
		
		screenVars.pcUnitsb05 .clear(); 
		screenVars.pcUnitsb06 .clear(); 
		screenVars.pcUnitsb07 .clear(); 
		
		
		screenVars.pcUnitsc05 .clear(); 
		screenVars.pcUnitsc06 .clear(); 
		screenVars.pcUnitsc07 .clear(); 
		
		
		screenVars.pcUnitsd05 .clear(); 
		screenVars.pcUnitsd06 .clear(); 
		screenVars.pcUnitsd07 .clear(); 
		
		screenVars.pcUnitse05 .clear(); 
		screenVars.pcUnitse06 .clear(); 
		screenVars.pcUnitse07 .clear(); 
		
		screenVars.pcUnitsf05 .clear(); 
		screenVars.pcUnitsf06 .clear(); 
		screenVars.pcUnitsf07 .clear(); 
	}
}
