package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:52
 * Description:
 * Copybook name: UTRSCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrsclmKey = new FixedLengthStringData(64).isAPartOf(utrsclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsclmChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsclmKey, 0);
  	public FixedLengthStringData utrsclmChdrnum = new FixedLengthStringData(8).isAPartOf(utrsclmKey, 1);
  	public PackedDecimalData utrsclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsclmKey, 9);
  	public FixedLengthStringData utrsclmLife = new FixedLengthStringData(2).isAPartOf(utrsclmKey, 12);
  	public FixedLengthStringData utrsclmCoverage = new FixedLengthStringData(2).isAPartOf(utrsclmKey, 14);
  	public FixedLengthStringData utrsclmRider = new FixedLengthStringData(2).isAPartOf(utrsclmKey, 16);
  	public FixedLengthStringData utrsclmUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsclmKey, 18);
  	public FixedLengthStringData utrsclmUnitType = new FixedLengthStringData(1).isAPartOf(utrsclmKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(utrsclmKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}