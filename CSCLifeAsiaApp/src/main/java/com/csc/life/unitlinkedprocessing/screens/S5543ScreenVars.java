package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5543
 * @version 1.0 generated on 30/08/09 06:43
 * @author Quipoz
 */
public class S5543ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1180);
	public FixedLengthStringData dataFields = new FixedLengthStringData(504).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData vfundescs = new FixedLengthStringData(360).isAPartOf(dataFields, 60);
	public FixedLengthStringData[] vfundesc = FLSArrayPartOfStructure(12, 30, vfundescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(360).isAPartOf(vfundescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData vfundesc01 = DD.vrtdesc.copy().isAPartOf(filler,0);
	public FixedLengthStringData vfundesc02 = DD.vrtdesc.copy().isAPartOf(filler,30);
	public FixedLengthStringData vfundesc03 = DD.vrtdesc.copy().isAPartOf(filler,60);
	public FixedLengthStringData vfundesc04 = DD.vrtdesc.copy().isAPartOf(filler,90);
	public FixedLengthStringData vfundesc05 = DD.vrtdesc.copy().isAPartOf(filler,120);
	public FixedLengthStringData vfundesc06 = DD.vrtdesc.copy().isAPartOf(filler,150);
	public FixedLengthStringData vfundesc07 = DD.vrtdesc.copy().isAPartOf(filler,180);
	public FixedLengthStringData vfundesc08 = DD.vrtdesc.copy().isAPartOf(filler,210);
	public FixedLengthStringData vfundesc09 = DD.vrtdesc.copy().isAPartOf(filler,240);
	public FixedLengthStringData vfundesc10 = DD.vrtdesc.copy().isAPartOf(filler,270);
	public FixedLengthStringData vfundesc11 = DD.vrtdesc.copy().isAPartOf(filler,300);
	public FixedLengthStringData vfundesc12 = DD.vrtdesc.copy().isAPartOf(filler,330);
	public FixedLengthStringData unitVirtualFunds = new FixedLengthStringData(48).isAPartOf(dataFields, 420);
	public FixedLengthStringData[] unitVirtualFund = FLSArrayPartOfStructure(12, 4, unitVirtualFunds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(48).isAPartOf(unitVirtualFunds, 0, FILLER_REDEFINE);
	public FixedLengthStringData unitVirtualFund01 = DD.vrtfnd.copy().isAPartOf(filler1,0);
	public FixedLengthStringData unitVirtualFund02 = DD.vrtfnd.copy().isAPartOf(filler1,4);   
	public FixedLengthStringData unitVirtualFund03 = DD.vrtfnd.copy().isAPartOf(filler1,8);
	public FixedLengthStringData unitVirtualFund04 = DD.vrtfnd.copy().isAPartOf(filler1,12);
	public FixedLengthStringData unitVirtualFund05 = DD.vrtfnd.copy().isAPartOf(filler1,16);
	public FixedLengthStringData unitVirtualFund06 = DD.vrtfnd.copy().isAPartOf(filler1,20);
	public FixedLengthStringData unitVirtualFund07 = DD.vrtfnd.copy().isAPartOf(filler1,24);
	public FixedLengthStringData unitVirtualFund08 = DD.vrtfnd.copy().isAPartOf(filler1,28);
	public FixedLengthStringData unitVirtualFund09 = DD.vrtfnd.copy().isAPartOf(filler1,32);
	public FixedLengthStringData unitVirtualFund10 = DD.vrtfnd.copy().isAPartOf(filler1,36);
	public FixedLengthStringData unitVirtualFund11 = DD.vrtfnd.copy().isAPartOf(filler1,40);
	public FixedLengthStringData unitVirtualFund12 = DD.vrtfnd.copy().isAPartOf(filler1,44);
	
	public FixedLengthStringData allowperiods = new FixedLengthStringData(36).isAPartOf(dataFields, 468);
	public FixedLengthStringData[] allowperiod = FLSArrayPartOfStructure(12, 3, allowperiods, 0, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(36).isAPartOf(allowperiods, 0, FILLER_REDEFINE);
	public FixedLengthStringData allowperiod01 = DD.allowperiod.copy().isAPartOf(filler6,0); 
	public FixedLengthStringData allowperiod02 = DD.allowperiod.copy().isAPartOf(filler6,3);
	public FixedLengthStringData allowperiod03 = DD.allowperiod.copy().isAPartOf(filler6,6);
	public FixedLengthStringData allowperiod04 = DD.allowperiod.copy().isAPartOf(filler6,9); 
	public FixedLengthStringData allowperiod05 = DD.allowperiod.copy().isAPartOf(filler6,12);
	public FixedLengthStringData allowperiod06 = DD.allowperiod.copy().isAPartOf(filler6,15);
	public FixedLengthStringData allowperiod07 = DD.allowperiod.copy().isAPartOf(filler6,18); 
	public FixedLengthStringData allowperiod08 = DD.allowperiod.copy().isAPartOf(filler6,21);
	public FixedLengthStringData allowperiod09 = DD.allowperiod.copy().isAPartOf(filler6,24);
	public FixedLengthStringData allowperiod10 = DD.allowperiod.copy().isAPartOf(filler6,27);
	public FixedLengthStringData allowperiod11 = DD.allowperiod.copy().isAPartOf(filler6,30);
	public FixedLengthStringData allowperiod12 = DD.allowperiod.copy().isAPartOf(filler6,33);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 504);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData vrtdescsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] vrtdescErr = FLSArrayPartOfStructure(12, 4, vrtdescsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(vrtdescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData vrtdesc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData vrtdesc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData vrtdesc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData vrtdesc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData vrtdesc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData vrtdesc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData vrtdesc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData vrtdesc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData vrtdesc09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData vrtdesc10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData vrtdesc11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData vrtdesc12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData vrtfndsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] vrtfndErr = FLSArrayPartOfStructure(12, 4, vrtfndsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(vrtfndsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData vrtfnd01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData vrtfnd02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData vrtfnd03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData vrtfnd04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData vrtfnd05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData vrtfnd06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData vrtfnd07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData vrtfnd08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData vrtfnd09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData vrtfnd10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData vrtfnd11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData vrtfnd12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	
	public FixedLengthStringData allowperiodsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData[] allowperiodErr = FLSArrayPartOfStructure(12, 4, allowperiodsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(48).isAPartOf(allowperiodsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData allowperiod01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData allowperiod02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData allowperiod03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData allowperiod04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData allowperiod05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData allowperiod06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData allowperiod07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData allowperiod08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData allowperiod09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData allowperiod10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData allowperiod11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData allowperiod12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(508).isAPartOf(dataArea, 672);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData vrtdescsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] vrtdescOut = FLSArrayPartOfStructure(12, 12, vrtdescsOut, 0);
	public FixedLengthStringData[][] vrtdescO = FLSDArrayPartOfArrayStructure(12, 1, vrtdescOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(vrtdescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] vrtdesc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] vrtdesc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] vrtdesc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] vrtdesc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] vrtdesc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] vrtdesc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] vrtdesc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] vrtdesc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] vrtdesc09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] vrtdesc10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] vrtdesc11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] vrtdesc12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData vrtfndsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 12, vrtfndsOut, 0);
	public FixedLengthStringData[][] vrtfndO = FLSDArrayPartOfArrayStructure(12, 1, vrtfndOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(vrtfndsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] vrtfnd01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] vrtfnd02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] vrtfnd03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] vrtfnd04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] vrtfnd05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] vrtfnd06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] vrtfnd07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] vrtfnd08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] vrtfnd09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] vrtfnd10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] vrtfnd11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] vrtfnd12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	
	public FixedLengthStringData allowperiodsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 362);
	public FixedLengthStringData[] allowperiodOut = FLSArrayPartOfStructure(12, 12, vrtfndsOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(144).isAPartOf(allowperiodsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] allowperiod01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] allowperiod02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] allowperiod03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] allowperiod04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] allowperiod05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] allowperiod06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] allowperiod07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] allowperiod08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] allowperiod09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] allowperiod10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] allowperiod11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] allowperiod12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5543screenWritten = new LongData(0);
	public LongData S5543protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	private ZonedDecimalData[] ZDArrayPartOfStructure(int i, int j, int k, ZonedDecimalData allowperiods2, int l,
			int m) {
		// TODO Auto-generated method stub
		return null;
	}


	public S5543ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itmfrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itmtoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc01Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc02Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc03Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd04Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc04Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd05Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc05Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd06Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc06Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd07Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc07Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd08Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc08Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd09Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc09Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd10Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc10Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd11Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc11Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd12Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtdesc12Out,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfnd01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod01Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod02Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod03Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod04Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod05Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod06Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod07Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod08Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod09Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod10Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod11Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(allowperiod12Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, unitVirtualFund01, vfundesc01, unitVirtualFund02, vfundesc02, unitVirtualFund03, vfundesc03, unitVirtualFund04, vfundesc04, unitVirtualFund05, vfundesc05, unitVirtualFund06, vfundesc06, unitVirtualFund07, vfundesc07, unitVirtualFund08, vfundesc08, unitVirtualFund09, vfundesc09, unitVirtualFund10, vfundesc10, unitVirtualFund11, vfundesc11, unitVirtualFund12, vfundesc12,allowperiod01,allowperiod02,allowperiod03,allowperiod04,allowperiod05,allowperiod06,allowperiod07,allowperiod08,allowperiod09,allowperiod10,allowperiod11,allowperiod12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, vrtfnd01Out, vrtdesc01Out, vrtfnd02Out, vrtdesc02Out, vrtfnd03Out, vrtdesc03Out, vrtfnd04Out, vrtdesc04Out, vrtfnd05Out, vrtdesc05Out, vrtfnd06Out, vrtdesc06Out, vrtfnd07Out, vrtdesc07Out, vrtfnd08Out, vrtdesc08Out, vrtfnd09Out, vrtdesc09Out, vrtfnd10Out, vrtdesc10Out, vrtfnd11Out, vrtdesc11Out, vrtfnd12Out, vrtdesc12Out,allowperiod01Out,allowperiod02Out,allowperiod03Out,allowperiod04Out,allowperiod05Out,allowperiod06Out,allowperiod07Out,allowperiod08Out,allowperiod09Out,allowperiod10Out,allowperiod11Out,allowperiod12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, vrtfnd01Err, vrtdesc01Err, vrtfnd02Err, vrtdesc02Err, vrtfnd03Err, vrtdesc03Err, vrtfnd04Err, vrtdesc04Err, vrtfnd05Err, vrtdesc05Err, vrtfnd06Err, vrtdesc06Err, vrtfnd07Err, vrtdesc07Err, vrtfnd08Err, vrtdesc08Err, vrtfnd09Err, vrtdesc09Err, vrtfnd10Err, vrtdesc10Err, vrtfnd11Err, vrtdesc11Err, vrtfnd12Err, vrtdesc12Err,allowperiod01Err,allowperiod02Err,allowperiod03Err,allowperiod01Err,allowperiod04Err,allowperiod05Err,allowperiod06Err,allowperiod07Err,allowperiod08Err,allowperiod09Err,allowperiod10Err,allowperiod11Err,allowperiod12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5543screen.class;
		protectRecord = S5543protect.class;
	}

}
