package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.IncipfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Incipf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class IncipfDAOImpl extends BaseDAOImpl<Incipf> implements IncipfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(IncipfDAOImpl.class);
	
	public List<Incipf> searchIncirevRecord(String coy, String chdrnum) {
		String sql = "SELECT UNIQUE_NUMBER FROM INCIREV WHERE CHDRCOY=? AND CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		List<Incipf> searchResult = new LinkedList<Incipf>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Incipf t = new Incipf();
				t.setUniqueNumber(rs.getLong(1));
				searchResult.add(t);
			}

		} catch (SQLException e) {
			LOGGER.error("searchIncirevRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return searchResult;
	}   
	
	public void deleteIncipfRecord(List<Incipf> dataList){
		String sql = "DELETE FROM INCIPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Incipf l:dataList){
				ps.setLong(1, l.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteIncipfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}