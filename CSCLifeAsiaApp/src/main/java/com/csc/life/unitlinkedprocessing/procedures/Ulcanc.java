/*
 * File: Ulcanc.java
 * Date: 30 August 2009 2:48:49
 * Author: Quipoz Limited
 * 
 * Class transformed from ULCANC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.dataaccess.ChdrtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*    ULCANC - Cancellation of initial units.
*
*    Parameters  passed  to  this  generic  subroutine  from the
* calling module will be in the copybook ANNPROCREC:
*
*         Plan key
*         Batch key
*         Effective Date
*
*    Firstly Read UTRSPF with the logical UTRS and only select
* records of the following:
*
*         Keyed on : Component
*
*         Select   : IF  Unit type = 'I'
*                    and Deemed unit balance > Real unit balance
*
*         Omit     : IF Real unit balance = 0
*
*    For each  of the records selected  the following processing
* will be required:
*
*    Calculate  the  number  of  deemed  units to be surrendered.
* If this is greater than the difference between deemed and real
* units use the difference as the surrender.
*
*    We now need to  write a record to the Unit transaction file
* (UTRNPF) using the  logical  view  UTRN. Initialise the record
* and set up the following fields:
*
*         - component key from UTRSPF
*         - fund and type from UTRSPF
*         - for the tran no field read and hold the CHDRPF file
*           for this contract using logical view CHDRTRN.
*           Increment the tran no field, update the tran id
*           field and rewrite the record to the database. Use
*           the new CHDRPF tran no on this record
*         - terminal id, transaction and user details as written
*           to the CHDRPF tran id field
*         - batch details from the parameters
*         - unit sub account 'INIT'
*         - taken from T6647 - deallocation field (key = trans
*           code/contract type, if no match use trans
*           code/'***').
*         - number of deemed units = as calculated.
*         - Number of real units = 0
*         - monies date as effective date from linkage
*         - CR table from COVRPF
*         - contract currency from CHDRPF
*         - feedback indicator space
*         - fund currency from T5515.
*         - sub account code,type and account from line 01 of
*           T5645 using program name as the key.
*         - processing sequence number from T6647
*         - SVP = 1
*         - CR commence date = from COVRPF
*         - surrender percentage = If no of deemed units has
*           been set to 0 previously this field will contain
*           the management charge from T5515.
*         - cancel init units indicator = 'Y'.
*
*
****************************************************************
* </pre>
*/
public class Ulcanc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("ULCANC");
		/* ERRORS */
	private String e308 = "E308";
	private String h116 = "H116";
	private String utrnrec = "UTRNREC   ";
	private String itemrec = "ITEMREC   ";
	private String chdrtrnrec = "CHDRTRNREC";
		/* TABLES */
	private String t6647 = "T6647";
	private String t5645 = "T5645";
	private String t5515 = "T5515";
	private String t5688 = "T5688";
	private String wsaaKeyBreak = "N";
	private ZonedDecimalData wsaaCharge = new ZonedDecimalData(10, 5);
	private ZonedDecimalData wsaaUnitLeft = new ZonedDecimalData(10, 5);

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);
	private Annprocrec annprocrec = new Annprocrec();
		/*Contract Header logical view for tran n*/
	private ChdrtrnTableDAM chdrtrnIO = new ChdrtrnTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
		/*UTRN LOGICAL VIEW.*/
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
		/*Unit transaction summary*/
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190, 
		exit579, 
		exit589, 
		readUtrs1020, 
		exit1900, 
		tryAgain4200, 
		exit4900
	}

	public Ulcanc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		annprocrec.annpllRec = convertAndSetParam(annprocrec.annpllRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para100();
				}
				case exit190: {
					exit190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		syserrrec.subrname.set(wsaaProg);
		varcom.vrcmTime.set(getCobolTime());
		annprocrec.statuz.set(varcom.oK);
		wsaaKeyBreak = "N";
		readT56455000();
		if (isEQ(annprocrec.annchg,ZERO)
		|| isEQ(annprocrec.initUnitChargeFreq,ZERO)) {
			goTo(GotoLabel.exit190);
		}
		while ( !(isEQ(wsaaKeyBreak,"Y"))) {
			mainProcessing1000();
		}
		
	}

protected void exit190()
	{
		exitProgram();
	}

protected void stop199()
	{
		stopRun();
	}

protected void systemError570()
	{
		try {
			para570();
		}
		catch (GOTOException e){
		}
		finally{
			exit579();
		}
	}

protected void para570()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit579);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		annprocrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError580()
	{
		try {
			para580();
		}
		catch (GOTOException e){
		}
		finally{
			exit589();
		}
	}

protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit589);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		annprocrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					setUpUtrsKey1010();
				}
				case readUtrs1020: {
					readUtrs1020();
				}
				case exit1900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setUpUtrsKey1010()
	{
		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(annprocrec.company);
		utrsIO.setChdrnum(annprocrec.chdrnum);
		utrsIO.setLife(annprocrec.life);
		utrsIO.setCoverage(annprocrec.coverage);
		utrsIO.setRider(annprocrec.rider);
		utrsIO.setPlanSuffix(annprocrec.planSuffix);
		utrsIO.setUnitType("I");
		utrsIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","COVERAGE","RIDER","PLNSFX","UNITYP");

	}

protected void readUtrs1020()
	{
		SmartFileCode.execute(appVars, utrsIO);
		if ((isNE(utrsIO.getStatuz(),varcom.oK))
		&& (isNE(utrsIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrsIO.getParams());
			syserrrec.statuz.set(utrsIO.getStatuz());
			databaseError580();
		}
		if (isEQ(utrsIO.getStatuz(),varcom.endp)) {
			wsaaKeyBreak = "Y";
			goTo(GotoLabel.exit1900);
		}
		if ((isNE(utrsIO.getChdrnum(),annprocrec.chdrnum))
		|| (isNE(utrsIO.getChdrcoy(),annprocrec.company))
		|| (isNE(utrsIO.getLife(),annprocrec.life))
		|| (isNE(utrsIO.getCoverage(),annprocrec.coverage))
		|| (isNE(utrsIO.getRider(),annprocrec.rider))) {
			wsaaKeyBreak = "Y";
			goTo(GotoLabel.exit1900);
		}
		if ((isNE(utrsIO.getUnitType(),"I"))
		|| (isLTE(utrsIO.getCurrentDunitBal(),utrsIO.getCurrentUnitBal()))
		|| (isEQ(utrsIO.getCurrentUnitBal(),0))
		|| (isNE(utrsIO.getPlanSuffix(),annprocrec.planSuffix))) {
			utrsIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readUtrs1020);
		}
		readT55152000();
		wsaaCharge.set(0);
		compute(wsaaCharge, 6).setRounded(div((div(mult(utrsIO.getCurrentDunitBal(),annprocrec.annchg),100)),annprocrec.initUnitChargeFreq));
		compute(wsaaUnitLeft, 5).set(sub(utrsIO.getCurrentDunitBal(),utrsIO.getCurrentUnitBal()));
		if (isGT(wsaaCharge,wsaaUnitLeft)) {
			wsaaCharge.set(wsaaUnitLeft);
		}
		compute(wsaaCharge, 5).set(mult(wsaaCharge,-1));
		readChdrtrn3000();
		readT56886000();
		readT66474000();
		utrnIO.setParams(SPACES);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setChdrcoy(annprocrec.company);
		utrnIO.setChdrnum(annprocrec.chdrnum);
		utrnIO.setCoverage(annprocrec.coverage);
		utrnIO.setLife(annprocrec.life);
		utrnIO.setRider(annprocrec.rider);
		utrnIO.setPlanSuffix(annprocrec.planSuffix);
		utrnIO.setTranno(chdrtrnIO.getTranno());
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setTransactionDate(annprocrec.effdate);
		utrnIO.setUser(annprocrec.user);
		utrnIO.setBatccoy(annprocrec.batccoy);
		utrnIO.setBatcbrn(annprocrec.batcbrn);
		utrnIO.setBatcactyr(annprocrec.batcactyr);
		utrnIO.setBatcactmn(annprocrec.batcactmn);
		utrnIO.setBatctrcde(annprocrec.batctrcde);
		utrnIO.setBatcbatch(annprocrec.batcbatch);
		utrnIO.setCrtable(annprocrec.crtable);
		utrnIO.setCntcurr(chdrtrnIO.getCntcurr());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setContractType(chdrtrnIO.getCnttype());
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(annprocrec.crdate);
		utrnIO.setFundCurrency(t5515rec.currcode);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		utrnIO.setMoniesDate(annprocrec.effdate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setNofUnits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		utrnIO.setFundAmount(0);
		utrnIO.setContractAmount(0);
		utrnIO.setDiscountFactor(0);
		utrnIO.setInciNum(0);
		utrnIO.setNofDunits(wsaaCharge);
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setCanInitUnitInd("Y");
		utrnIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		utrnIO.setUnitType("I");
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			databaseError580();
		}
		utrsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readUtrs1020);
	}

protected void readT55152000()
	{
		para2010();
	}

protected void para2010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(annprocrec.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(utrsIO.getUnitVirtualFund());
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItmfrm(annprocrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if ((isNE(itdmIO.getItemcoy(),annprocrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5515))
		|| (isNE(itdmIO.getItemitem(),utrsIO.getUnitVirtualFund()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(utrsIO.getUnitVirtualFund());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h116);
			databaseError580();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void readChdrtrn3000()
	{
		/*PARA*/
		chdrtrnIO.setParams(SPACES);
		chdrtrnIO.setChdrnum(annprocrec.chdrnum);
		chdrtrnIO.setChdrcoy(annprocrec.company);
		chdrtrnIO.setFormat(chdrtrnrec);
		chdrtrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrtrnIO);
		if (isNE(chdrtrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrtrnIO.getParams());
			syserrrec.statuz.set(chdrtrnIO.getStatuz());
			databaseError580();
		}
		/*EXIT*/
	}

protected void readT66474000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4010();
				}
				case tryAgain4200: {
					tryAgain4200();
				}
				case exit4900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(annprocrec.company);
		itdmIO.setItemtabl(t6647);
		itdmIO.setItmfrm(annprocrec.effdate);
		wsaaT6647Batctrcde.set(annprocrec.batctrcde);
		wsaaT6647Cnttype.set(chdrtrnIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.tryAgain4200);
		}
		if ((isNE(itdmIO.getItemcoy(),annprocrec.company))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Item))) {
			goTo(GotoLabel.tryAgain4200);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		goTo(GotoLabel.exit4900);
	}

protected void tryAgain4200()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(annprocrec.company);
		itdmIO.setItemtabl(t6647);
		itdmIO.setItmfrm(annprocrec.effdate);
		wsaaT6647Batctrcde.set(annprocrec.batctrcde);
		wsaaT6647Cnttype.set("***");
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.exit4900);
		}
		if ((isNE(itdmIO.getItemcoy(),annprocrec.company))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Item))) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.exit4900);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void readT56455000()
	{
		para5010();
	}

protected void para5010()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(annprocrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT56886000()
	{
		read6000();
	}

protected void read6000()
	{
		itdmIO.setItemcoy(chdrtrnIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrtrnIO.getCnttype());
		itdmIO.setItmfrm(chdrtrnIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),chdrtrnIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrtrnIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrtrnIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError580();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}
}
