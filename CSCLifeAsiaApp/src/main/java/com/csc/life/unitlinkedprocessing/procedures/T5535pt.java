/*
 * File: T5535pt.java
 * Date: 30 August 2009 2:21:38
 * Author: Quipoz Limited
 * 
 * Class transformed from T5535PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5535rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5535.
*
*
*****************************************************************
* </pre>
*/
public class T5535pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("Loyalty/Persistency Allocation Basis         S5535");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(41);
	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 14);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 24, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 31);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(65);
	private FixedLengthStringData filler9 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(51).isAPartOf(wsaaPrtLine004, 14, FILLER).init("After Durations    Extra % Alloc      Premium/Units");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(59);
	private FixedLengthStringData filler11 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine005, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 58);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(59);
	private FixedLengthStringData filler14 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine006, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 58);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(59);
	private FixedLengthStringData filler17 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 58);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(59);
	private FixedLengthStringData filler20 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 58);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(59);
	private FixedLengthStringData filler23 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 58);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(59);
	private FixedLengthStringData filler26 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 58);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(59);
	private FixedLengthStringData filler29 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 58);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(59);
	private FixedLengthStringData filler32 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine012, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 58);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(59);
	private FixedLengthStringData filler35 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 58);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(59);
	private FixedLengthStringData filler38 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 18).setPattern("ZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 37).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 58);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5535rec t5535rec = new T5535rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5535pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5535rec.t5535Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5535rec.unitAfterDur01);
		fieldNo008.set(t5535rec.unitExtPcAlloc01);
		fieldNo009.set(t5535rec.percOrAmtInd01);
		fieldNo010.set(t5535rec.unitAfterDur02);
		fieldNo011.set(t5535rec.unitExtPcAlloc02);
		fieldNo012.set(t5535rec.percOrAmtInd02);
		fieldNo013.set(t5535rec.unitAfterDur03);
		fieldNo014.set(t5535rec.unitExtPcAlloc03);
		fieldNo015.set(t5535rec.percOrAmtInd03);
		fieldNo016.set(t5535rec.unitAfterDur04);
		fieldNo017.set(t5535rec.unitExtPcAlloc04);
		fieldNo018.set(t5535rec.percOrAmtInd04);
		fieldNo019.set(t5535rec.unitAfterDur05);
		fieldNo020.set(t5535rec.unitExtPcAlloc05);
		fieldNo021.set(t5535rec.percOrAmtInd05);
		fieldNo022.set(t5535rec.unitAfterDur06);
		fieldNo023.set(t5535rec.unitExtPcAlloc06);
		fieldNo024.set(t5535rec.percOrAmtInd06);
		fieldNo025.set(t5535rec.unitAfterDur07);
		fieldNo026.set(t5535rec.unitExtPcAlloc07);
		fieldNo027.set(t5535rec.percOrAmtInd07);
		fieldNo028.set(t5535rec.unitAfterDur08);
		fieldNo029.set(t5535rec.unitExtPcAlloc08);
		fieldNo030.set(t5535rec.percOrAmtInd08);
		fieldNo031.set(t5535rec.unitAfterDur09);
		fieldNo032.set(t5535rec.unitExtPcAlloc09);
		fieldNo033.set(t5535rec.percOrAmtInd09);
		fieldNo034.set(t5535rec.unitAfterDur10);
		fieldNo035.set(t5535rec.unitExtPcAlloc10);
		fieldNo036.set(t5535rec.percOrAmtInd10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
