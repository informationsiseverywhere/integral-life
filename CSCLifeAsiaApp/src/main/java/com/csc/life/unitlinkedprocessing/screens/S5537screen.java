package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5537screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5537ScreenVars sv = (S5537ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5537screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5537ScreenVars screenVars = (S5537ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.toterm01.setClassString("");
		screenVars.toterm02.setClassString("");
		screenVars.toterm03.setClassString("");
		screenVars.toterm04.setClassString("");
		screenVars.toterm05.setClassString("");
		screenVars.toterm06.setClassString("");
		screenVars.toterm07.setClassString("");
		screenVars.toterm08.setClassString("");
		screenVars.toterm09.setClassString("");
		screenVars.ageIssageTo01.setClassString("");
		screenVars.actAllocBasis01.setClassString("");
		screenVars.actAllocBasis02.setClassString("");
		screenVars.actAllocBasis03.setClassString("");
		screenVars.actAllocBasis04.setClassString("");
		screenVars.actAllocBasis05.setClassString("");
		screenVars.actAllocBasis06.setClassString("");
		screenVars.actAllocBasis07.setClassString("");
		screenVars.actAllocBasis08.setClassString("");
		screenVars.actAllocBasis09.setClassString("");
		screenVars.ageIssageTo02.setClassString("");
		screenVars.actAllocBasis10.setClassString("");
		screenVars.actAllocBasis11.setClassString("");
		screenVars.actAllocBasis12.setClassString("");
		screenVars.actAllocBasis13.setClassString("");
		screenVars.actAllocBasis14.setClassString("");
		screenVars.actAllocBasis15.setClassString("");
		screenVars.actAllocBasis16.setClassString("");
		screenVars.actAllocBasis17.setClassString("");
		screenVars.actAllocBasis18.setClassString("");
		screenVars.ageIssageTo03.setClassString("");
		screenVars.actAllocBasis19.setClassString("");
		screenVars.actAllocBasis20.setClassString("");
		screenVars.actAllocBasis21.setClassString("");
		screenVars.actAllocBasis22.setClassString("");
		screenVars.actAllocBasis23.setClassString("");
		screenVars.actAllocBasis24.setClassString("");
		screenVars.actAllocBasis25.setClassString("");
		screenVars.actAllocBasis26.setClassString("");
		screenVars.actAllocBasis27.setClassString("");
		screenVars.ageIssageTo04.setClassString("");
		screenVars.actAllocBasis28.setClassString("");
		screenVars.actAllocBasis29.setClassString("");
		screenVars.actAllocBasis30.setClassString("");
		screenVars.actAllocBasis31.setClassString("");
		screenVars.actAllocBasis32.setClassString("");
		screenVars.actAllocBasis33.setClassString("");
		screenVars.actAllocBasis34.setClassString("");
		screenVars.actAllocBasis35.setClassString("");
		screenVars.actAllocBasis36.setClassString("");
		screenVars.ageIssageTo05.setClassString("");
		screenVars.actAllocBasis37.setClassString("");
		screenVars.actAllocBasis38.setClassString("");
		screenVars.actAllocBasis39.setClassString("");
		screenVars.actAllocBasis40.setClassString("");
		screenVars.actAllocBasis41.setClassString("");
		screenVars.actAllocBasis42.setClassString("");
		screenVars.actAllocBasis43.setClassString("");
		screenVars.actAllocBasis44.setClassString("");
		screenVars.actAllocBasis45.setClassString("");
		screenVars.ageIssageTo06.setClassString("");
		screenVars.actAllocBasis46.setClassString("");
		screenVars.actAllocBasis47.setClassString("");
		screenVars.actAllocBasis48.setClassString("");
		screenVars.actAllocBasis49.setClassString("");
		screenVars.actAllocBasis50.setClassString("");
		screenVars.actAllocBasis51.setClassString("");
		screenVars.actAllocBasis52.setClassString("");
		screenVars.actAllocBasis53.setClassString("");
		screenVars.actAllocBasis54.setClassString("");
		screenVars.ageIssageTo07.setClassString("");
		screenVars.actAllocBasis55.setClassString("");
		screenVars.actAllocBasis56.setClassString("");
		screenVars.actAllocBasis57.setClassString("");
		screenVars.actAllocBasis58.setClassString("");
		screenVars.actAllocBasis59.setClassString("");
		screenVars.actAllocBasis60.setClassString("");
		screenVars.actAllocBasis61.setClassString("");
		screenVars.actAllocBasis62.setClassString("");
		screenVars.actAllocBasis63.setClassString("");
		screenVars.ageIssageTo08.setClassString("");
		screenVars.actAllocBasis64.setClassString("");
		screenVars.actAllocBasis65.setClassString("");
		screenVars.actAllocBasis66.setClassString("");
		screenVars.actAllocBasis67.setClassString("");
		screenVars.actAllocBasis68.setClassString("");
		screenVars.actAllocBasis69.setClassString("");
		screenVars.actAllocBasis70.setClassString("");
		screenVars.actAllocBasis71.setClassString("");
		screenVars.actAllocBasis72.setClassString("");
		screenVars.ageIssageTo09.setClassString("");
		screenVars.actAllocBasis73.setClassString("");
		screenVars.actAllocBasis74.setClassString("");
		screenVars.actAllocBasis75.setClassString("");
		screenVars.actAllocBasis76.setClassString("");
		screenVars.actAllocBasis77.setClassString("");
		screenVars.actAllocBasis78.setClassString("");
		screenVars.actAllocBasis79.setClassString("");
		screenVars.actAllocBasis80.setClassString("");
		screenVars.actAllocBasis81.setClassString("");
		screenVars.ageIssageTo10.setClassString("");
		screenVars.actAllocBasis82.setClassString("");
		screenVars.actAllocBasis83.setClassString("");
		screenVars.actAllocBasis84.setClassString("");
		screenVars.actAllocBasis85.setClassString("");
		screenVars.actAllocBasis86.setClassString("");
		screenVars.actAllocBasis87.setClassString("");
		screenVars.actAllocBasis88.setClassString("");
		screenVars.actAllocBasis89.setClassString("");
		screenVars.actAllocBasis90.setClassString("");
		screenVars.agecont.setClassString("");
		screenVars.trmcont.setClassString("");
	}

/**
 * Clear all the variables in S5537screen
 */
	public static void clear(VarModel pv) {
		S5537ScreenVars screenVars = (S5537ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.toterm01.clear();
		screenVars.toterm02.clear();
		screenVars.toterm03.clear();
		screenVars.toterm04.clear();
		screenVars.toterm05.clear();
		screenVars.toterm06.clear();
		screenVars.toterm07.clear();
		screenVars.toterm08.clear();
		screenVars.toterm09.clear();
		screenVars.ageIssageTo01.clear();
		screenVars.actAllocBasis01.clear();
		screenVars.actAllocBasis02.clear();
		screenVars.actAllocBasis03.clear();
		screenVars.actAllocBasis04.clear();
		screenVars.actAllocBasis05.clear();
		screenVars.actAllocBasis06.clear();
		screenVars.actAllocBasis07.clear();
		screenVars.actAllocBasis08.clear();
		screenVars.actAllocBasis09.clear();
		screenVars.ageIssageTo02.clear();
		screenVars.actAllocBasis10.clear();
		screenVars.actAllocBasis11.clear();
		screenVars.actAllocBasis12.clear();
		screenVars.actAllocBasis13.clear();
		screenVars.actAllocBasis14.clear();
		screenVars.actAllocBasis15.clear();
		screenVars.actAllocBasis16.clear();
		screenVars.actAllocBasis17.clear();
		screenVars.actAllocBasis18.clear();
		screenVars.ageIssageTo03.clear();
		screenVars.actAllocBasis19.clear();
		screenVars.actAllocBasis20.clear();
		screenVars.actAllocBasis21.clear();
		screenVars.actAllocBasis22.clear();
		screenVars.actAllocBasis23.clear();
		screenVars.actAllocBasis24.clear();
		screenVars.actAllocBasis25.clear();
		screenVars.actAllocBasis26.clear();
		screenVars.actAllocBasis27.clear();
		screenVars.ageIssageTo04.clear();
		screenVars.actAllocBasis28.clear();
		screenVars.actAllocBasis29.clear();
		screenVars.actAllocBasis30.clear();
		screenVars.actAllocBasis31.clear();
		screenVars.actAllocBasis32.clear();
		screenVars.actAllocBasis33.clear();
		screenVars.actAllocBasis34.clear();
		screenVars.actAllocBasis35.clear();
		screenVars.actAllocBasis36.clear();
		screenVars.ageIssageTo05.clear();
		screenVars.actAllocBasis37.clear();
		screenVars.actAllocBasis38.clear();
		screenVars.actAllocBasis39.clear();
		screenVars.actAllocBasis40.clear();
		screenVars.actAllocBasis41.clear();
		screenVars.actAllocBasis42.clear();
		screenVars.actAllocBasis43.clear();
		screenVars.actAllocBasis44.clear();
		screenVars.actAllocBasis45.clear();
		screenVars.ageIssageTo06.clear();
		screenVars.actAllocBasis46.clear();
		screenVars.actAllocBasis47.clear();
		screenVars.actAllocBasis48.clear();
		screenVars.actAllocBasis49.clear();
		screenVars.actAllocBasis50.clear();
		screenVars.actAllocBasis51.clear();
		screenVars.actAllocBasis52.clear();
		screenVars.actAllocBasis53.clear();
		screenVars.actAllocBasis54.clear();
		screenVars.ageIssageTo07.clear();
		screenVars.actAllocBasis55.clear();
		screenVars.actAllocBasis56.clear();
		screenVars.actAllocBasis57.clear();
		screenVars.actAllocBasis58.clear();
		screenVars.actAllocBasis59.clear();
		screenVars.actAllocBasis60.clear();
		screenVars.actAllocBasis61.clear();
		screenVars.actAllocBasis62.clear();
		screenVars.actAllocBasis63.clear();
		screenVars.ageIssageTo08.clear();
		screenVars.actAllocBasis64.clear();
		screenVars.actAllocBasis65.clear();
		screenVars.actAllocBasis66.clear();
		screenVars.actAllocBasis67.clear();
		screenVars.actAllocBasis68.clear();
		screenVars.actAllocBasis69.clear();
		screenVars.actAllocBasis70.clear();
		screenVars.actAllocBasis71.clear();
		screenVars.actAllocBasis72.clear();
		screenVars.ageIssageTo09.clear();
		screenVars.actAllocBasis73.clear();
		screenVars.actAllocBasis74.clear();
		screenVars.actAllocBasis75.clear();
		screenVars.actAllocBasis76.clear();
		screenVars.actAllocBasis77.clear();
		screenVars.actAllocBasis78.clear();
		screenVars.actAllocBasis79.clear();
		screenVars.actAllocBasis80.clear();
		screenVars.actAllocBasis81.clear();
		screenVars.ageIssageTo10.clear();
		screenVars.actAllocBasis82.clear();
		screenVars.actAllocBasis83.clear();
		screenVars.actAllocBasis84.clear();
		screenVars.actAllocBasis85.clear();
		screenVars.actAllocBasis86.clear();
		screenVars.actAllocBasis87.clear();
		screenVars.actAllocBasis88.clear();
		screenVars.actAllocBasis89.clear();
		screenVars.actAllocBasis90.clear();
		screenVars.agecont.clear();
		screenVars.trmcont.clear();
	}
}
