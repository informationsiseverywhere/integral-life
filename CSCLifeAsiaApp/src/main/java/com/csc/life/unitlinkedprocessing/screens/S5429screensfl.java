package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5429screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 60;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 1, 2, 3, 10, 9, 1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 22, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5429ScreenVars sv = (S5429ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5429screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5429screensfl, 
			sv.S5429screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5429ScreenVars sv = (S5429ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5429screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5429ScreenVars sv = (S5429ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5429screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5429screensflWritten.gt(0))
		{
			sv.s5429screensfl.setCurrentIndex(0);
			sv.S5429screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5429ScreenVars sv = (S5429ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5429screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5429ScreenVars screenVars = (S5429ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.initBarePrice.setFieldName("initBarePrice");
				screenVars.initBidPrice.setFieldName("initBidPrice");
				screenVars.initOfferPrice.setFieldName("initOfferPrice");
				screenVars.accBarePrice.setFieldName("accBarePrice");
				screenVars.accBidPrice.setFieldName("accBidPrice");
				screenVars.accOfferPrice.setFieldName("accOfferPrice");
				screenVars.update.setFieldName("update");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.initBarePrice.set(dm.getField("initBarePrice"));
			screenVars.initBidPrice.set(dm.getField("initBidPrice"));
			screenVars.initOfferPrice.set(dm.getField("initOfferPrice"));
			screenVars.accBarePrice.set(dm.getField("accBarePrice"));
			screenVars.accBidPrice.set(dm.getField("accBidPrice"));
			screenVars.accOfferPrice.set(dm.getField("accOfferPrice"));
			screenVars.update.set(dm.getField("update"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5429ScreenVars screenVars = (S5429ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.initBarePrice.setFieldName("initBarePrice");
				screenVars.initBidPrice.setFieldName("initBidPrice");
				screenVars.initOfferPrice.setFieldName("initOfferPrice");
				screenVars.accBarePrice.setFieldName("accBarePrice");
				screenVars.accBidPrice.setFieldName("accBidPrice");
				screenVars.accOfferPrice.setFieldName("accOfferPrice");
				screenVars.update.setFieldName("update");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("initBarePrice").set(screenVars.initBarePrice);
			dm.getField("initBidPrice").set(screenVars.initBidPrice);
			dm.getField("initOfferPrice").set(screenVars.initOfferPrice);
			dm.getField("accBarePrice").set(screenVars.accBarePrice);
			dm.getField("accBidPrice").set(screenVars.accBidPrice);
			dm.getField("accOfferPrice").set(screenVars.accOfferPrice);
			dm.getField("update").set(screenVars.update);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5429screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5429ScreenVars screenVars = (S5429ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.initBarePrice.clearFormatting();
		screenVars.initBidPrice.clearFormatting();
		screenVars.initOfferPrice.clearFormatting();
		screenVars.accBarePrice.clearFormatting();
		screenVars.accBidPrice.clearFormatting();
		screenVars.accOfferPrice.clearFormatting();
		screenVars.update.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5429ScreenVars screenVars = (S5429ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.initBarePrice.setClassString("");
		screenVars.initBidPrice.setClassString("");
		screenVars.initOfferPrice.setClassString("");
		screenVars.accBarePrice.setClassString("");
		screenVars.accBidPrice.setClassString("");
		screenVars.accOfferPrice.setClassString("");
		screenVars.update.setClassString("");
	}

/**
 * Clear all the variables in S5429screensfl
 */
	public static void clear(VarModel pv) {
		S5429ScreenVars screenVars = (S5429ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.initBarePrice.clear();
		screenVars.initBidPrice.clear();
		screenVars.initOfferPrice.clear();
		screenVars.accBarePrice.clear();
		screenVars.accBidPrice.clear();
		screenVars.accOfferPrice.clear();
		screenVars.update.clear();
	}
}
