package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:57
 * Description:
 * Copybook name: SURDTRGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Surdtrgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData surdtrgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData surdtrgKey = new FixedLengthStringData(64).isAPartOf(surdtrgFileKey, 0, REDEFINE);
  	public FixedLengthStringData surdtrgChdrcoy = new FixedLengthStringData(1).isAPartOf(surdtrgKey, 0);
  	public FixedLengthStringData surdtrgChdrnum = new FixedLengthStringData(8).isAPartOf(surdtrgKey, 1);
  	public PackedDecimalData surdtrgTranno = new PackedDecimalData(5, 0).isAPartOf(surdtrgKey, 9);
  	public PackedDecimalData surdtrgPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(surdtrgKey, 12);
  	public FixedLengthStringData surdtrgLife = new FixedLengthStringData(2).isAPartOf(surdtrgKey, 15);
  	public FixedLengthStringData surdtrgCoverage = new FixedLengthStringData(2).isAPartOf(surdtrgKey, 17);
  	public FixedLengthStringData surdtrgRider = new FixedLengthStringData(2).isAPartOf(surdtrgKey, 19);
  	public FixedLengthStringData surdtrgVirtualFund = new FixedLengthStringData(4).isAPartOf(surdtrgKey, 21);
  	public FixedLengthStringData surdtrgFieldType = new FixedLengthStringData(1).isAPartOf(surdtrgKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(surdtrgKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(surdtrgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		surdtrgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}