/*
 * File: Br508.java
 * Date: 29 August 2009 22:11:20
 * Author: Quipoz Limited
 * 
 * Class transformed from BR508.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.anticipatedendowment.recordstructures.Pr507par;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.reports.Rr508Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*         Fund Valuation New Business Extraction Program
*         ----------------------------------------------
*
*   This program will extract all contracts that have been
*   inforce within the range of dates specified on the parameter
*   screen for inputs.
*
*   - Read sequentially records fron CHDRLNB with Validflag = '1'
*     and the corresponding record in PTRNENQ where the
*     transaction date is within the input date range.
*   - Check each contract against T5679 to ensure that it has a
*     valid contract risk statii
*   - If Valid Contract Read corresponding RTRNSAC record
*   - If there is no RTRNSAC record, skip the contract.Otherwise,
*     read UTRN (sequentially) with BATCTRCDE = 'T642' & COVR
*     files to retrieve the required information,
*   - where UNITYPE/VRTFND not = spaces for the contract and
*     and Batch Transaction = 'T642'.
*   - For valid records of UTRN read COVR for information.
*
*****************************************************************
* </pre>
*/
public class Br508 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlptrnpf1rs = null;
	private java.sql.PreparedStatement sqlptrnpf1ps = null;
	private java.sql.Connection sqlptrnpf1conn = null;
	private String sqlptrnpf1 = "";
	private Rr508Report printerFile = new Rr508Report();
	private SortFileDAM sortFile = new SortFileDAM("BR508SRT");
	private FixedLengthStringData printerRec = new FixedLengthStringData(320);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR508");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaContractTot = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaPremTot = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaFundTot = new ZonedDecimalData(18, 5);
	private ZonedDecimalData wsaaUnitsTot = new ZonedDecimalData(16, 5);
	private ZonedDecimalData wsaaDateFrm = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateTo = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private Validator invalidContract = new Validator(wsaaValidContract, "N");

	private FixedLengthStringData wsaaValidRecord = new FixedLengthStringData(1);
	private Validator validRecord = new Validator(wsaaValidRecord, "Y");
	private Validator invalidRecord = new Validator(wsaaValidRecord, "N");

	private FixedLengthStringData wsaaRtrnsacRecord = new FixedLengthStringData(1);
	private Validator rtrnsacRecordFound = new Validator(wsaaRtrnsacRecord, "Y");
	private Validator rtrnsacRecordNotFound = new Validator(wsaaRtrnsacRecord, "N");
	private FixedLengthStringData wsaaValidFlag2 = new FixedLengthStringData(1).init("2");
	private FixedLengthStringData wsaaIssueCode = new FixedLengthStringData(4).init("T642");
	private FixedLengthStringData wsaaUalfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-PTRNPF */
	private FixedLengthStringData sqlPtrnrec = new FixedLengthStringData(16);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlPtrnrec, 0);
	private PackedDecimalData sqlPtrneff = new PackedDecimalData(8, 0).isAPartOf(sqlPtrnrec, 8);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(sqlPtrnrec, 13);
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3629 = "T3629";
	private static final String t5515 = "T5515";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";

	private FixedLengthStringData wsaaEofSort = new FixedLengthStringData(1).init("N");
	private Validator endOfSortFile = new Validator(wsaaEofSort, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

		/* COPY TR386REC.                                       <LA3235>*/
	private FixedLengthStringData rr508H01 = new FixedLengthStringData(110);
	private FixedLengthStringData rr508h01O = new FixedLengthStringData(110).isAPartOf(rr508H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr508h01O, 0);
	private FixedLengthStringData ualfnd = new FixedLengthStringData(4).isAPartOf(rr508h01O, 10);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rr508h01O, 14);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rr508h01O, 44);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rr508h01O, 47);
	private FixedLengthStringData ultype = new FixedLengthStringData(1).isAPartOf(rr508h01O, 77);
	private FixedLengthStringData statcode = new FixedLengthStringData(2).isAPartOf(rr508h01O, 78);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rr508h01O, 80);

	private FixedLengthStringData rr508D01 = new FixedLengthStringData(101);
	private FixedLengthStringData rr508d01O = new FixedLengthStringData(101).isAPartOf(rr508D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr508d01O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rr508d01O, 8);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(rr508d01O, 12);
	private FixedLengthStringData rundte = new FixedLengthStringData(10).isAPartOf(rr508d01O, 29);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(rr508d01O, 39);
	private FixedLengthStringData ptrneff = new FixedLengthStringData(10).isAPartOf(rr508d01O, 49);
	private ZonedDecimalData priceused = new ZonedDecimalData(9, 5).isAPartOf(rr508d01O, 59);
	private ZonedDecimalData nofunt = new ZonedDecimalData(16, 5).isAPartOf(rr508d01O, 68);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2).isAPartOf(rr508d01O, 84);

	private FixedLengthStringData rr508S01 = new FixedLengthStringData(56);
	private FixedLengthStringData rr508s01O = new FixedLengthStringData(56).isAPartOf(rr508S01, 0);
	private ZonedDecimalData totccount = new ZonedDecimalData(9, 0).isAPartOf(rr508s01O, 0);
	private ZonedDecimalData totprm = new ZonedDecimalData(13, 2).isAPartOf(rr508s01O, 9);
	private ZonedDecimalData totfundval = new ZonedDecimalData(18, 5).isAPartOf(rr508s01O, 22);
	private ZonedDecimalData totnofunts = new ZonedDecimalData(16, 5).isAPartOf(rr508s01O, 40);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pr507par pr507par = new Pr507par();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private FormatsInner formatsInner = new FormatsInner();
	private SortRecInner sortRecInner = new SortRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2108, 
		exit2109, 
		nextRtrnsac2680, 
		exit2690, 
		nextUtrn2780, 
		exit2790
	}

	public Br508() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		pr507par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		wsaaUalfnd.set(SPACES);
		/* Read T5645 for sub-account code and type (for reading ACBLs    .*/
		/* for suspense)*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5679 for valid statii for contract header and coverage   .*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/*--- Read TR386 table to get screen literals                      */
		/* MOVE ITEMREC                TO ITEM-FORMAT.          <LA3235>*/
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>*/
		/* MOVE BSPR-COMPANY           TO ITEM-ITEMCOY.         <LA3235>*/
		/* MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>*/
		/* MOVE BSSC-LANGUAGE          TO ITEM-ITEMITEM.        <LA3235>*/
		/* MOVE WSAA-PROG              TO ITEM-ITEMITEM(2:5).   <LA3235>*/
		/* MOVE READR                  TO ITEM-FUNCTION.        <LA3235>*/
		/*                                                      <LA3235>*/
		/* CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>*/
		/*                                                      <LA3235>*/
		/* IF  ITEM-STATUZ             NOT = O-K                <LA3235>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <LA3235>*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <LA3235>*/
		/*     PERFORM 600-FATAL-ERROR                          <LA3235>*/
		/* END-IF.                                              <LA3235>*/
		/*                                                      <LA3235>*/
		/* MOVE ITEM-GENAREA        TO TR386-TR386-REC.         <LA3235>*/
		if (isEQ(pr507par.datefrm,varcom.maxdate)
		|| isEQ(pr507par.datefrm,varcom.vrcmMaxDate)) {
			wsaaDateFrm.set(0);
		}
		else {
			wsaaDateFrm.set(pr507par.datefrm);
		}
		wsaaDateTo.set(pr507par.dateto);
		sqlptrnpf1 = " SELECT  CHDRNUM, PTRNEFF, TRANNO" +
" FROM   " + getAppVars().getTableNameOverriden("PTRNPF") + " " +
" WHERE VALIDFLAG <> ?" +
" AND PTRNEFF >= ?" +
" AND PTRNEFF <= ?" +
" AND BATCTRCDE = ?"
+ " AND CHDRCOY = ? ";
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlptrnpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.PtrnpfTableDAM());
			sqlptrnpf1ps = getAppVars().prepareStatementEmbeded(sqlptrnpf1conn, sqlptrnpf1, "PTRNPF");
			getAppVars().setDBString(sqlptrnpf1ps, 1, wsaaValidFlag2);
			getAppVars().setDBNumber(sqlptrnpf1ps, 2, wsaaDateFrm);
			getAppVars().setDBNumber(sqlptrnpf1ps, 3, wsaaDateTo);
			getAppVars().setDBString(sqlptrnpf1ps, 4, wsaaIssueCode);
			getAppVars().setDBString(sqlptrnpf1ps, 5, bsprIO.getCompany());
			sqlptrnpf1rs = getAppVars().executeQuery(sqlptrnpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		sortFile.openOutput();
		retrieveData2050();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortRecInner.sortUalfnd, true);
		fs1.addSortKey(sortRecInner.sortStatcode, true);
		fs1.addSortKey(sortRecInner.sortChdrnum, true);
		fs1.sort();
		sortFile.openInput();
		retrieveSorted2200();
		sortFile.close();
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void retrieveData2050()
	{
		/*RETRIEVAL*/
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			releaseToSort2100();
		}
		
		/*EXIT*/
	}

protected void releaseToSort2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					release2101();
				case eof2108: 
					eof2108();
				case exit2109: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void release2101()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlptrnpf1rs)) {
				getAppVars().getDBObject(sqlptrnpf1rs, 1, sqlChdrnum);
				getAppVars().getDBObject(sqlptrnpf1rs, 2, sqlPtrneff);
				getAppVars().getDBObject(sqlptrnpf1rs, 3, sqlTranno);
			}
			else {
				goTo(GotoLabel.eof2108);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* Check record is required for processing.*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(sqlChdrnum);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		wsaaValidContract.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| validContract.isTrue()); wsaaIndex.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],chdrlnbIO.getStatcode())) {
				wsaaValidContract.set("Y");
			}
		}
		if (invalidContract.isTrue()) {
			goTo(GotoLabel.exit2109);
		}
		wsaaValidRecord.set("N");
		wsaaRtrnsacRecord.set("N");
		rtrnsacIO.setParams(SPACES);
		rtrnsacIO.setRldgcoy(bsprIO.getCompany());
		rtrnsacIO.setSacscode(t5645rec.sacscode01);
		rtrnsacIO.setRldgacct(sqlChdrnum);
		rtrnsacIO.setSacstyp(t5645rec.sacstype01);
		rtrnsacIO.setOrigccy(chdrlnbIO.getCntcurr());
		rtrnsacIO.setFormat(formatsInner.rtrnsacrec);
		rtrnsacIO.setFunction(varcom.begn);
		while ( !(isEQ(rtrnsacIO.getStatuz(),varcom.endp)
		|| rtrnsacRecordFound.isTrue())) {
			readRtrnsac2600();
		}
		
		if (rtrnsacRecordNotFound.isTrue()) {
			goTo(GotoLabel.exit2109);
		}
		utrnIO.setParams(SPACES);
		utrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		utrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		utrnIO.setLife(SPACES);
		utrnIO.setCoverage(SPACES);
		utrnIO.setRider(SPACES);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setUnitVirtualFund(SPACES);
		utrnIO.setUnitType(SPACES);
		utrnIO.setTranno(rtrnsacIO.getTranno());
		utrnIO.setFunction(varcom.begn);
		utrnIO.setFormat(formatsInner.utrnrec);
		while ( !(isEQ(utrnIO.getStatuz(),varcom.endp))) {
			readUtrn2700();
		}
		
		goTo(GotoLabel.exit2109);
	}

protected void eof2108()
	{
		wsaaEofSort.set("Y");
	}

protected void retrieveSorted2200()
	{
		/*STARTED*/
		wsaaContractTot.set(0);
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			printReport2250();
		}
		
		/* To print Summary total of details*/
		if (isGT(wsaaContractTot,0)) {
			printSummary2900();
		}
		/*EXIT*/
	}

protected void printReport2250()
	{
			print2251();
		}

protected void print2251()
	{
		sortFile.read(sortRecInner.sortRec);
		if (sortFile.isAtEnd()) {
			wsaaEofSort.set("Y");
			return ;
		}
		/* Check for change in Fund Code*/
		if (isNE(wsaaUalfnd,SPACES)) {
			if (isNE(wsaaUalfnd, sortRecInner.sortUalfnd)) {
				printSummary2900();
			}
		}
		/* Check for change in Fund Code, Status code or New page*/
		if (!(isEQ(wsaaUalfnd, sortRecInner.sortUalfnd)
		&& isEQ(wsaaStatcode, sortRecInner.sortStatcode))
		|| newPageReq.isTrue()) {
			headerDetails2800();
		}
		chdrnum.set(sortRecInner.sortChdrnum);
		crtable.set(sortRecInner.sortCrtable);
		singp.set(sortRecInner.sortSingp);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortRecInner.sortRundte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rundte.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortRecInner.sortOccdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		occdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortRecInner.sortPtrneff);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		ptrneff.set(datcon1rec.extDate);
		priceused.set(sortRecInner.sortPriceused);
		nofunt.set(sortRecInner.sortNofunt);
		fundamnt.set(sortRecInner.sortFundamnt);
		printerFile.printRr508d01(rr508D01);
		/* Add detail total to Summary total.*/
		wsaaContractTot.add(1);
		wsaaPremTot.add(sortRecInner.sortSingp);
		wsaaFundTot.add(sortRecInner.sortFundamnt);
		wsaaUnitsTot.add(sortRecInner.sortNofunt);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void readRtrnsac2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readRtrnsac2610();
				case nextRtrnsac2680: 
					nextRtrnsac2680();
				case exit2690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readRtrnsac2610()
	{
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(),varcom.oK)
		&& isNE(rtrnsacIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			fatalError600();
		}
		if (isNE(rtrnsacIO.getRldgcoy(),bsprIO.getCompany())
		|| isNE(rtrnsacIO.getSacscode(),t5645rec.sacscode01)
		|| isNE(rtrnsacIO.getRldgacct(),sqlChdrnum)
		|| isNE(rtrnsacIO.getSacstyp(),t5645rec.sacstype01)
		|| isNE(rtrnsacIO.getOrigccy(),chdrlnbIO.getCntcurr())
		|| isEQ(rtrnsacIO.getStatuz(),varcom.endp)) {
			rtrnsacIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		if (isNE(rtrnsacIO.getTranno(),sqlTranno)) {
			goTo(GotoLabel.nextRtrnsac2680);
		}
		wsaaRtrnsacRecord.set("Y");
	}

protected void nextRtrnsac2680()
	{
		rtrnsacIO.setFunction(varcom.nextr);
	}

protected void readUtrn2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readUtrn2710();
				case nextUtrn2780: 
					nextUtrn2780();
				case exit2790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readUtrn2710()
	{
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)
		&& isNE(utrnIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
		if (isNE(utrnIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(utrnIO.getStatuz(),varcom.endp)) {
			utrnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2790);
		}
		if (isNE(utrnIO.getTranno(),rtrnsacIO.getTranno())) {
			goTo(GotoLabel.nextUtrn2780);
		}
		if (isEQ(utrnIO.getUnitVirtualFund(),SPACES)
		|| isEQ(utrnIO.getUnitType(),SPACES)) {
			goTo(GotoLabel.nextUtrn2780);
		}
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrIO.setLife(utrnIO.getLife());
		covrIO.setCoverage(utrnIO.getCoverage());
		covrIO.setRider(utrnIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		sortRecInner.sortChdrnum.set(chdrlnbIO.getChdrnum());
		sortRecInner.sortOccdate.set(chdrlnbIO.getOccdate());
		sortRecInner.sortStatcode.set(chdrlnbIO.getStatcode());
		sortRecInner.sortCntcurr.set(chdrlnbIO.getCntcurr());
		sortRecInner.sortPtrneff.set(sqlPtrneff);
		sortRecInner.sortOrigamt.set(rtrnsacIO.getOrigamt());
		sortRecInner.sortCrtable.set(covrIO.getCrtable());
		sortRecInner.sortRundte.set(covrIO.getReserveUnitsDate());
		if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
			sortRecInner.sortSingp.set(covrIO.getSingp());
		}
		else {
			sortRecInner.sortSingp.set(covrIO.getInstprem());
		}
		if (isEQ(utrnIO.getFundAmount(),0)) {
			sortRecInner.sortFundamnt.set(utrnIO.getContractAmount());
		}
		else {
			sortRecInner.sortFundamnt.set(utrnIO.getFundAmount());
		}
		sortRecInner.sortNofunt.set(utrnIO.getNofUnits());
		sortRecInner.sortPriceused.set(utrnIO.getPriceUsed());
		sortRecInner.sortUalfnd.set(utrnIO.getUnitVirtualFund());
		sortFile.write(sortRecInner.sortRec);
	}

protected void nextUtrn2780()
	{
		utrnIO.setFunction(varcom.nextr);
	}

protected void headerDetails2800()
	{
		headerDetails2810();
	}

protected void headerDetails2810()
	{
		initialize(indicArea);
		/* Read T5515 with Fund Code to retrieve the Fund Currency and*/
		/* Unit Type*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(sortRecInner.sortUalfnd);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		/* Read T5515 again to get the Fund Code Description*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5515);
		descIO.setDescitem(sortRecInner.sortUalfnd);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		ualfnd.set(sortRecInner.sortUalfnd);
		wsaaUalfnd.set(sortRecInner.sortUalfnd);
		descrip.set(descIO.getLongdesc());
		/* Read T3629 to get the Currency description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		currcode.set(t5515rec.currcode);
		currencynm.set(descIO.getLongdesc());
		ultype.set(t5515rec.unitType);
		if (isEQ(t5515rec.unitType,"I")) {
			/*    MOVE 'Initial'           TO LNGDSC     OF RR508H01-O      */
			/*    MOVE TR386-PROGDESC-1    TO LNGDSC     OF RR508H01-O      */
			/*    MOVE TR386-PROGDESC-01   TO LNGDSC     OF RR508H01-O      */
			indicTable[1].set("1");
			indicTable[2].set("0");
			indicTable[3].set("0");
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				/*    MOVE 'Accumulation'      TO LNGDSC     OF RR508H01-O      */
				/*    MOVE TR386-PROGDESC-2    TO LNGDSC     OF RR508H01-O      */
				/*    MOVE TR386-PROGDESC-02   TO LNGDSC     OF RR508H01-O      */
				indicTable[2].set("1");
				indicTable[1].set("0");
				indicTable[3].set("0");
			}
			else {
				if (isEQ(t5515rec.unitType,"B")) {
					/*    MOVE 'Both'              TO LNGDSC     OF RR508H01-O      */
					/*    MOVE TR386-PROGDESC-3    TO LNGDSC     OF RR508H01-O      */
					/*    MOVE TR386-PROGDESC-03   TO LNGDSC     OF RR508H01-O      */
					indicTable[3].set("1");
					indicTable[1].set("0");
					indicTable[2].set("0");
				}
			}
		}
		/* Read T3623 to get the Risk Status description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3623);
		descIO.setDescitem(sortRecInner.sortStatcode);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		statcode.set(sortRecInner.sortStatcode);
		wsaaStatcode.set(sortRecInner.sortStatcode);
		longdesc.set(descIO.getLongdesc());
		printerFile.printRr508h01(rr508H01);
		/*                              INDICATORS INDIC-AREA.   <S19FIX>*/
		wsaaOverflow.set("N");
	}

protected void printSummary2900()
	{
		/*PRINT-SUNNARY*/
		totccount.set(wsaaContractTot);
		totprm.set(wsaaPremTot);
		totfundval.set(wsaaFundTot);
		totnofunts.set(wsaaUnitsTot);
		printerFile.printRr508s01(rr508S01);
		initialize(wsaaContractTot);
		initialize(wsaaPremTot);
		initialize(wsaaFundTot);
		initialize(wsaaUnitsTot);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		printerFile.close();
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure SORT-REC--INNER
 */
private static final class SortRecInner { 

	private FixedLengthStringData sortRec = new FixedLengthStringData(73);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortRec, 0);
	private PackedDecimalData sortOccdate = new PackedDecimalData(8, 0).isAPartOf(sortRec, 8);
	private PackedDecimalData sortRundte = new PackedDecimalData(8, 0).isAPartOf(sortRec, 13);
	private PackedDecimalData sortSingp = new PackedDecimalData(17, 2).isAPartOf(sortRec, 18);
	private FixedLengthStringData sortCrtable = new FixedLengthStringData(4).isAPartOf(sortRec, 27);
	private FixedLengthStringData sortStatcode = new FixedLengthStringData(2).isAPartOf(sortRec, 31);
	private FixedLengthStringData sortCntcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 33);
	private FixedLengthStringData sortUalfnd = new FixedLengthStringData(4).isAPartOf(sortRec, 36);
	private PackedDecimalData sortPtrneff = new PackedDecimalData(8, 0).isAPartOf(sortRec, 40);
	private PackedDecimalData sortNofunt = new PackedDecimalData(16, 5).isAPartOf(sortRec, 45);
	private PackedDecimalData sortPriceused = new PackedDecimalData(9, 5).isAPartOf(sortRec, 54);
	private PackedDecimalData sortFundamnt = new PackedDecimalData(13, 2).isAPartOf(sortRec, 59);
	private PackedDecimalData sortOrigamt = new PackedDecimalData(13, 2).isAPartOf(sortRec, 66);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData rtrnsacrec = new FixedLengthStringData(10).init("RTRNREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
}
}
