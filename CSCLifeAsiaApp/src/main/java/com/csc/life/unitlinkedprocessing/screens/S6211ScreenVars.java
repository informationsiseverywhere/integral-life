package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6211
 * @version 1.0 generated on 30/08/09 06:50
 * @author Quipoz
 */
public class S6211ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(188);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(58).isAPartOf(subfileArea, 0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData unitBidPrice = DD.ubidpr.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public ZonedDecimalData unitBarePrice = DD.ubrepr.copyToZonedDecimal().isAPartOf(subfileFields,27);
	public FixedLengthStringData unitType = DD.ultype.copy().isAPartOf(subfileFields,36);
	public ZonedDecimalData unitOfferPrice = DD.uoffpr.copyToZonedDecimal().isAPartOf(subfileFields,37);
	public ZonedDecimalData updateDate = DD.upddte.copyToZonedDecimal().isAPartOf(subfileFields,46);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,54);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 58);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData ubidprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData ubreprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData ultypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData uoffprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData upddteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 90);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] ubidprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] ubreprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] ultypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] uoffprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] upddteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 186);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData updateDateDisp = new FixedLengthStringData(10);

	public LongData S6211screensflWritten = new LongData(0);
	public LongData S6211screenctlWritten = new LongData(0);
	public LongData S6211screenWritten = new LongData(0);
	public LongData S6211protectWritten = new LongData(0);
	public GeneralTable s6211screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6211screensfl;
	}

	public S6211ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {unitVirtualFund, shortdesc, unitType, effdate, unitBarePrice, unitBidPrice, unitOfferPrice, updateDate};
		screenSflOutFields = new BaseData[][] {vrtfndOut, shortdescOut, ultypeOut, effdateOut, ubreprOut, ubidprOut, uoffprOut, upddteOut};
		screenSflErrFields = new BaseData[] {vrtfndErr, shortdescErr, ultypeErr, effdateErr, ubreprErr, ubidprErr, uoffprErr, upddteErr};
		screenSflDateFields = new BaseData[] {effdate, updateDate};
		screenSflDateErrFields = new BaseData[] {effdateErr, upddteErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp, updateDateDisp};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6211screen.class;
		screenSflRecord = S6211screensfl.class;
		screenCtlRecord = S6211screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6211protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6211screenctl.lrec.pageSubfile);
	}
}
