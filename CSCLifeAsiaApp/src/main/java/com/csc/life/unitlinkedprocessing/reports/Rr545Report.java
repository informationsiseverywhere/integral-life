package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR545.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr545Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cname = new FixedLengthStringData(47);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData dateReportVariable = new FixedLengthStringData(10);
	private FixedLengthStringData orccy = new FixedLengthStringData(3);
	private ZonedDecimalData origamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData rdocnum = new FixedLengthStringData(9);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr545Report() {
		super();
	}


	/**
	 * Print the XML for Rr545d01
	 */
	public void printRr545d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		rdocnum.setFieldName("rdocnum");
		rdocnum.setInternal(subString(recordData, 1, 9));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.setInternal(subString(recordData, 10, 10));
		cname.setFieldName("cname");
		cname.setInternal(subString(recordData, 20, 47));
		orccy.setFieldName("orccy");
		orccy.setInternal(subString(recordData, 67, 3));
		origamt.setFieldName("origamt");
		origamt.setInternal(subString(recordData, 70, 17));
		printLayout("Rr545d01",			// Record name
			new BaseData[]{			// Fields:
				rdocnum,
				dateReportVariable,
				cname,
				orccy,
				origamt
			}
		);

	}

	/**
	 * Print the XML for Rr545h01
	 */
	public void printRr545h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 32, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 42, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 44, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("Rr545h01",			// Record name
			new BaseData[]{			// Fields:
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for Rr545h02
	 */
	public void printRr545h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rr545h02",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

	}

	/**
	 * Print the XML for Rr545h03
	 */
	public void printRr545h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(21).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rr545h03",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind21", indicArea.charAt(21)}
			}
		);

	}


}
