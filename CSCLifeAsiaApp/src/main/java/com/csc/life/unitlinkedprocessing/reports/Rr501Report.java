package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR501.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rr501Report extends SMARTReportLayout { 

	private FixedLengthStringData agntnum = new FixedLengthStringData(8);
	private FixedLengthStringData billchnl = new FixedLengthStringData(2);
	private FixedLengthStringData billfreq = new FixedLengthStringData(2);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData cownnum = new FixedLengthStringData(8);
	private FixedLengthStringData crcode = new FixedLengthStringData(4);
	private FixedLengthStringData crrcd = new FixedLengthStringData(10);
	private FixedLengthStringData date01 = new FixedLengthStringData(10);
	private FixedLengthStringData descrip01 = new FixedLengthStringData(30);
	private FixedLengthStringData descrip02 = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2);
	private FixedLengthStringData statcode01 = new FixedLengthStringData(2);
	private FixedLengthStringData statcode02 = new FixedLengthStringData(2);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr501Report() {
		super();
	}


	/**
	 * Print the XML for Rr501d01
	 */
	public void printRr501d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 9, 3));
		descrip01.setFieldName("descrip01");
		descrip01.setInternal(subString(recordData, 12, 30));
		statcode01.setFieldName("statcode01");
		statcode01.setInternal(subString(recordData, 42, 2));
		date01.setFieldName("date01");
		date01.setInternal(subString(recordData, 44, 10));
		cownnum.setFieldName("cownnum");
		cownnum.setInternal(subString(recordData, 54, 8));
		agntnum.setFieldName("agntnum");
		agntnum.setInternal(subString(recordData, 62, 8));
		billfreq.setFieldName("billfreq");
		billfreq.setInternal(subString(recordData, 70, 2));
		billchnl.setFieldName("billchnl");
		billchnl.setInternal(subString(recordData, 72, 2));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 74, 3));
		sacscurbal.setFieldName("sacscurbal");
		sacscurbal.setInternal(subString(recordData, 77, 17));
		printLayout("Rr501d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				cnttype,
				descrip01,
				statcode01,
				date01,
				cownnum,
				agntnum,
				billfreq,
				billchnl,
				cntcurr,
				sacscurbal
			}
		);

	}

	/**
	 * Print the XML for Rr501d02
	 */
	public void printRr501d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		crcode.setFieldName("crcode");
		crcode.setInternal(subString(recordData, 1, 4));
		descrip02.setFieldName("descrip02");
		descrip02.setInternal(subString(recordData, 5, 30));
		statcode02.setFieldName("statcode02");
		statcode02.setInternal(subString(recordData, 35, 2));
		crrcd.setFieldName("crrcd");
		crrcd.setInternal(subString(recordData, 37, 10));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 47, 17));
		singp.setFieldName("singp");
		singp.setInternal(subString(recordData, 64, 17));
		printLayout("Rr501d02",			// Record name
			new BaseData[]{			// Fields:
				crcode,
				descrip02,
				statcode02,
				crrcd,
				sumins,
				singp
			}
		);

	}

	/**
	 * Print the XML for Rr501h01
	 */
	public void printRr501h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 42, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 52, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 54, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr501h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(11);
	}


}
