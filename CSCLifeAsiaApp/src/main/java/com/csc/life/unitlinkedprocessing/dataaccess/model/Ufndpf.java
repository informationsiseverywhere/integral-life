package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Ufndpf {
	private long uniqueNumber;
	private String company;
	private String virtualFund;
	private String unitType;
	private BigDecimal nofunts;
	private String userProfile;
	private String jobName;
	private String datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getVirtualFund() {
		return virtualFund;
	}

	public void setVirtualFund(String virtualFund) {
		this.virtualFund = virtualFund;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public BigDecimal getNofunts() {
		return nofunts;
	}

	public void setNofunts(BigDecimal nofunts) {
		this.nofunts = nofunts;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
}