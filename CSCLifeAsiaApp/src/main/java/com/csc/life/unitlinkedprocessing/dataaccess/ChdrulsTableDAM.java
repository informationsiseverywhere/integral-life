package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrulsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:32:54
 * Class transformed from CHDRULS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrulsTableDAM extends ChdrpfTableDAM {

	public ChdrulsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRULS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "VALIDFLAG, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "STATCODE, " +
		            "OCCDATE, " +
		            "COWNPFX, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "JOWNNUM, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "DESPPFX, " +
		            "DESPCOY, " +
		            "DESPNUM, " +
		            "ASGNCOY, " +
		            "ASGNNUM, " +
		            "CNTBRANCH, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CNTCURR, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "BTDATE, " +
		            "PTDATE, " +
		            "REG, " +
		            "POLINC, " +
		            "NXTSFX, " +
		            "SINSTAMT06, " +
		            "ISAM06, " +
		            "INSTFROM, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "PSTCDE, " +
		            "POLSUM, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               validflag,
                               servunit,
                               cnttype,
                               tranno,
                               tranid,
                               statcode,
                               occdate,
                               cownpfx,
                               cowncoy,
                               cownnum,
                               jownnum,
                               payrcoy,
                               payrnum,
                               desppfx,
                               despcoy,
                               despnum,
                               asgncoy,
                               asgnnum,
                               cntbranch,
                               agntcoy,
                               agntnum,
                               cntcurr,
                               billfreq,
                               billchnl,
                               btdate,
                               ptdate,
                               register,
                               polinc,
                               nxtsfx,
                               sinstamt06,
                               inststamt06,
                               instfrom,
                               facthous,
                               bankkey,
                               bankacckey,
                               pstatcode,
                               polsum,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(230);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getValidflag().toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getStatcode().toInternal()
					+ getOccdate().toInternal()
					+ getCownpfx().toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getJownnum().toInternal()
					+ getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getDesppfx().toInternal()
					+ getDespcoy().toInternal()
					+ getDespnum().toInternal()
					+ getAsgncoy().toInternal()
					+ getAsgnnum().toInternal()
					+ getCntbranch().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getCntcurr().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getBtdate().toInternal()
					+ getPtdate().toInternal()
					+ getRegister().toInternal()
					+ getPolinc().toInternal()
					+ getNxtsfx().toInternal()
					+ getSinstamt06().toInternal()
					+ getInststamt06().toInternal()
					+ getInstfrom().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getPstatcode().toInternal()
					+ getPolsum().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, cownpfx);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, jownnum);
			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, desppfx);
			what = ExternalData.chop(what, despcoy);
			what = ExternalData.chop(what, despnum);
			what = ExternalData.chop(what, asgncoy);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, nxtsfx);
			what = ExternalData.chop(what, sinstamt06);
			what = ExternalData.chop(what, inststamt06);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, polsum);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public FixedLengthStringData getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(Object what) {
		cownpfx.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getJownnum() {
		return jownnum;
	}
	public void setJownnum(Object what) {
		jownnum.set(what);
	}	
	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getDesppfx() {
		return desppfx;
	}
	public void setDesppfx(Object what) {
		desppfx.set(what);
	}	
	public FixedLengthStringData getDespcoy() {
		return despcoy;
	}
	public void setDespcoy(Object what) {
		despcoy.set(what);
	}	
	public FixedLengthStringData getDespnum() {
		return despnum;
	}
	public void setDespnum(Object what) {
		despnum.set(what);
	}	
	public FixedLengthStringData getAsgncoy() {
		return asgncoy;
	}
	public void setAsgncoy(Object what) {
		asgncoy.set(what);
	}	
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public PackedDecimalData getNxtsfx() {
		return nxtsfx;
	}
	public void setNxtsfx(Object what) {
		setNxtsfx(what, false);
	}
	public void setNxtsfx(Object what, boolean rounded) {
		if (rounded)
			nxtsfx.setRounded(what);
		else
			nxtsfx.set(what);
	}	
	public PackedDecimalData getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(Object what) {
		setSinstamt06(what, false);
	}
	public void setSinstamt06(Object what, boolean rounded) {
		if (rounded)
			sinstamt06.setRounded(what);
		else
			sinstamt06.set(what);
	}	
	public PackedDecimalData getInststamt06() {
		return inststamt06;
	}
	public void setInststamt06(Object what) {
		setInststamt06(what, false);
	}
	public void setInststamt06(Object what, boolean rounded) {
		if (rounded)
			inststamt06.setRounded(what);
		else
			inststamt06.set(what);
	}	
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPolsum() {
		return polsum;
	}
	public void setPolsum(Object what) {
		setPolsum(what, false);
	}
	public void setPolsum(Object what, boolean rounded) {
		if (rounded)
			polsum.setRounded(what);
		else
			polsum.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSinstamts() {
		return new FixedLengthStringData(sinstamt06.toInternal()
);
	}
	public void setSinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sinstamt06);
	}
	public PackedDecimalData getSinstamt(BaseData indx) {
		return getSinstamt(indx.toInt());
	}
	public PackedDecimalData getSinstamt(int indx) {

		switch (indx) {
			case 1 : return sinstamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSinstamt(BaseData indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(BaseData indx, Object what, boolean rounded) {
		setSinstamt(indx.toInt(), what, rounded);
	}
	public void setSinstamt(int indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSinstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInststamts() {
		return new FixedLengthStringData(inststamt06.toInternal()
);
	}
	public void setInststamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInststamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, inststamt06);
	}
	public PackedDecimalData getInststamt(BaseData indx) {
		return getInststamt(indx.toInt());
	}
	public PackedDecimalData getInststamt(int indx) {

		switch (indx) {
			case 1 : return inststamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInststamt(BaseData indx, Object what) {
		setInststamt(indx, what, false);
	}
	public void setInststamt(BaseData indx, Object what, boolean rounded) {
		setInststamt(indx.toInt(), what, rounded);
	}
	public void setInststamt(int indx, Object what) {
		setInststamt(indx, what, false);
	}
	public void setInststamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInststamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		validflag.clear();
		servunit.clear();
		cnttype.clear();
		tranno.clear();
		tranid.clear();
		statcode.clear();
		occdate.clear();
		cownpfx.clear();
		cowncoy.clear();
		cownnum.clear();
		jownnum.clear();
		payrcoy.clear();
		payrnum.clear();
		desppfx.clear();
		despcoy.clear();
		despnum.clear();
		asgncoy.clear();
		asgnnum.clear();
		cntbranch.clear();
		agntcoy.clear();
		agntnum.clear();
		cntcurr.clear();
		billfreq.clear();
		billchnl.clear();
		btdate.clear();
		ptdate.clear();
		register.clear();
		polinc.clear();
		nxtsfx.clear();
		sinstamt06.clear();
		inststamt06.clear();
		instfrom.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		pstatcode.clear();
		polsum.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}