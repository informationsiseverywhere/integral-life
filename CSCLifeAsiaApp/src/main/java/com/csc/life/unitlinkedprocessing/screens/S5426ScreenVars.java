package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5426
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5426ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(65);
	public FixedLengthStringData dataFields = new FixedLengthStringData(17).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData jobno = DD.jobno.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 17);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData jobnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 29);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] jobnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(158);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(60).isAPartOf(subfileArea, 0);
	public ZonedDecimalData acumut = DD.acumut.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData ffnddsc = DD.ffnddsc.copy().isAPartOf(subfileFields,9);
	public ZonedDecimalData initut = DD.initut.copyToZonedDecimal().isAPartOf(subfileFields,34);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,43);
	public ZonedDecimalData updateDate = DD.upddte.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 60);
	public FixedLengthStringData acumutErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData ffnddscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData initutErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData upddteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 84);
	public FixedLengthStringData[] acumutOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] ffnddscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] initutOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] upddteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 156);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData updateDateDisp = new FixedLengthStringData(10);

	public LongData S5426screensflWritten = new LongData(0);
	public LongData S5426screenctlWritten = new LongData(0);
	public LongData S5426screenWritten = new LongData(0);
	public LongData S5426protectWritten = new LongData(0);
	public GeneralTable s5426screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5426screensfl;
	}

	public S5426ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vrtfndOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ffnddscOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(initutOut,new String[] {"02","05","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acumutOut,new String[] {"03","06","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(upddteOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jobnoOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {unitVirtualFund, ffnddsc, initut, acumut, updateDate, tranno};
		screenSflOutFields = new BaseData[][] {vrtfndOut, ffnddscOut, initutOut, acumutOut, upddteOut, trannoOut};
		screenSflErrFields = new BaseData[] {vrtfndErr, ffnddscErr, initutErr, acumutErr, upddteErr, trannoErr};
		screenSflDateFields = new BaseData[] {updateDate};
		screenSflDateErrFields = new BaseData[] {upddteErr};
		screenSflDateDispFields = new BaseData[] {updateDateDisp};

		screenFields = new BaseData[] {company, effdate, jobno};
		screenOutFields = new BaseData[][] {companyOut, effdateOut, jobnoOut};
		screenErrFields = new BaseData[] {companyErr, effdateErr, jobnoErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5426screen.class;
		screenSflRecord = S5426screensfl.class;
		screenCtlRecord = S5426screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5426protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5426screenctl.lrec.pageSubfile);
	}
}
