/*
 * File: P5412up.java
 * Date: 30 August 2009 0:24:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P5412UP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.recordstructures.P5412rec;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*           Update subroutine for P5412
*
*   UNIT PRICE PRICE ENTRY SUBMENU UPDATE SUBROUTINE
*
* This subroutine checks for an open batch by calling
* 'BATCCHK' and if not found opens a new batch by
* calling 'BATCDOR'.
*
* The new batch is returned to the mainline in
* the R-Rec
*
* If more then one active or inactive batch is found an
* error is returned to the mainline to force the user
* to decide which batch he means.
*
*
*****************************************************
* </pre>
*/
public class P5412up extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "P5412UP";
		/* ERRORS */
	private String e132 = "E132";
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private P5412rec p5412rec = new P5412rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont050, 
		exit090
	}

	public P5412up() {
		super();
	}

public void mainline(Object... parmArray)
	{
		p5412rec.p5412Rec = convertAndSetParam(p5412rec.p5412Rec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					mainline010();
					checkBatch030();
					openNewBatch040();
				}
				case cont050: {
					cont050();
				}
				case exit090: {
					exit090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void mainline010()
	{
		syserrrec.subrname.set(wsaaSubr);
		p5412rec.statuz.set(varcom.oK);
	}

protected void checkBatch030()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(p5412rec.batchkey);
		batcchkrec.tranid.set(p5412rec.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz,varcom.oK)) {
			p5412rec.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.exit090);
		}
		if (isEQ(batcchkrec.statuz,varcom.dupr)) {
			p5412rec.statuz.set(e132);
			goTo(GotoLabel.exit090);
		}
		if (isEQ(batcchkrec.statuz,"INAC")) {
			batcdorrec.function.set("ACTIV");
			p5412rec.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.cont050);
		}
		if (isNE(batcchkrec.statuz,varcom.mrnf)) {
			p5412rec.statuz.set(batcchkrec.statuz);
			goTo(GotoLabel.exit090);
		}
	}

protected void openNewBatch040()
	{
		batcdorrec.function.set("AUTO");
	}

protected void cont050()
	{
		batcdorrec.tranid.set(p5412rec.tranid);
		batcdorrec.batchkey.set(p5412rec.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			p5412rec.statuz.set(batcdorrec.statuz);
			goTo(GotoLabel.exit090);
		}
		p5412rec.batchkey.set(batcdorrec.batchkey);
	}

protected void exit090()
	{
		exitProgram();
	}
}
