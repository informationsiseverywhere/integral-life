package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6520
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6520ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(111);
	public FixedLengthStringData dataFields = new FixedLengthStringData(63).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 63);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 75);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(127);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(45).isAPartOf(subfileArea, 0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData stmtlevel = DD.stmtlevel.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(subfileFields,9);
	public ZonedDecimalData ustmno = DD.ustmno.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData stmtType = DD.ustmtyp.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 45);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData stmtlevelErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData ustmnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData ustmtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 65);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] stmtlevelOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] ustmnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] ustmtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 125);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6520screensflWritten = new LongData(0);
	public LongData S6520screenctlWritten = new LongData(0);
	public LongData S6520screenWritten = new LongData(0);
	public LongData S6520protectWritten = new LongData(0);
	public GeneralTable s6520screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6520screensfl;
	}

	public S6520ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {effdate, stmtlevel, stmtType, ustmno, trandesc};
		screenSflOutFields = new BaseData[][] {effdateOut, stmtlevelOut, ustmtypOut, ustmnoOut, trandescOut};
		screenSflErrFields = new BaseData[] {effdateErr, stmtlevelErr, ustmtypErr, ustmnoErr, trandescErr};
		screenSflDateFields = new BaseData[] {effdate};
		screenSflDateErrFields = new BaseData[] {effdateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp};

		screenFields = new BaseData[] {chdrnum, cownnum, ownername};
		screenOutFields = new BaseData[][] {chdrnumOut, cownnumOut, ownernameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cownnumErr, ownernameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6520screen.class;
		screenSflRecord = S6520screensfl.class;
		screenCtlRecord = S6520screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6520protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6520screenctl.lrec.pageSubfile);
	}
}
