/*
 * File: P5488.java
 * Date: 30 August 2009 0:29:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P5488.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

 //ILIFE-8089 start
import java.math.BigDecimal;
import java.util.List;
 //ILIFE-8089 end
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
 //ILIFE-8089 start
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
 //ILIFE-8089 end
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
 //ILIFE-8089 start
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
 //ILIFE-8089 end
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.unitlinkedprocessing.dataaccess.RdirTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S5488ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* FUND REDIRECTION.
*
*     P5488 - Fund Redirection
*
*     This  is  an on-line transaction to capture any modifications
*     made to  the  future  investment of funds. At proposal stage,
*     the investor  can  choose  to  invest his premium in up to 10
*     different funds  if  he  chose  a unit linked contract. These
*     details are  then  converted into a record in ULNKPF at issue
*     stage. The office will then reference to this file every time
*     they buy  units  for  the  investor.  At  a  later  date, the
*     investor may want to change his investment plan due to market
*     trends or  other  reasons. He may want to stop investing in a
*     particular fund  or  start  investing  in a new fund. This is
*     where this program comes in to make his wishes come true!
*
*     In addition to  capturing the modifications, this program can
*     also be used as an inquiry program.
*
* *
* *               suffix or the whole plan).
*
* *               polices(non existed plan suffix).
*
* *     format the structure  of  the  program  flow according to the
*     standards as follows:-
*
* *
* *
* *
* *
* *
* *     storages. Some of the information we need should have already
*     passed in from the linkage area.
*
* *     CHDRLIF-TRANNO and add 1 to it.
*
* *     is a  plan  level  redirection.
*
*          Move 'Y' to WSAA-PLAN-LEVEL
*          Move 'P' to percentage/amount indicator(on the screen)
*
* *               equal  to  the the number of policies included(from
*               CHDRLIF)
*
* *                    parts of the key from linkage, move the record
*                    details(funds,  amount  and  percentage/amount
*                    indicator) to the corresponding screen fields
*
* *
* *                    indicator with space and the amounts with 0.
*
* *     level redirection  and  we  need  to  find  out if the chosen
*     policy is  an  existing  policy.  This is done by reading the
*     ULNKPF(using the  logical  view  ULNK)  with the key from the
*     linkage area.
*
* *
* *                    summarised  policies,  so read ULNK again with
*                    plan    suffix    =    0.    Move    'Y'    to
*                    WSAA-BREAKOUT-POLICY.
*
* *
* *
* *               error.  Move  the  details(funds,  percentages  and
*               perc/amt indicator) to the screen fields.  Move 'N'
*               to WSAA-PLAN-LEVEL.
*
* *     linkage area(if MRNF,  try  again  with plan suffix = 0)  for
*     the    risk    commencement    date(CRRCD)    and    coverage
*     table(CRTABLE). Move these  two fields to the screen.  To get
*     the description  CRTABLE, we read the Description File DESCPF
*     (using the logical  view  DESC)  using T5687 as the table and
*     CRTABLE as the  item for the key(company from linkage).  Then
*     move the description to the corresponding screen field.
*
* *     follows:-
*
*          If WSAA-PLAN-LEVEL = 'Y'
*              move 0  to the instalment amount
*          Else
*              If the status of the first read = O-K(from above)
*                  move COVR-SINGP to the screen field
*              Else
*                 If the status of the second read = O-K
*                     compute WSAA-INSTALMENT = COVR-SINGP /
*                                               (the number of
*                                                summarised
*                                                records from
*                                                linkage).
*
* *     linkage  area.   Move   age   next  birthday(ANBCCD)  to  the
*     corresponding screen  field.  Read  CLNTPF(using logical view
*     CLTS)  to get the  name  of  the  life  assured.  The  client
*     number(LIFCNUM) is taken  from  LIFEPF. To format the name we
*     use the subroutine in CONFNAME copybook as follows:-
*
* *
* *
* *     so move this field to the corresponding screen field and also
*     client number(LIFCNUM).
*
* *     key from linkage) to  get  the  fund  split  plan. T5540 is a
*     dated table, so we  have  to  use  the logical ITDM, Once the
*     table  is   sucessfully   read,   move  ITDM-GENAREA  to  the
*     T5540-T5540-REC.   Then  move  T5540-FUND-SPLIT-PLAN  to  the
*     corresponding screen field.
*
* *     linkage i.e.  Contract  number, Life number, Coverage number,
*     Rider number and Plan suffix.
*
*
* *     the screen. If  it  is an inquiry transaction(WSSP-FLAG = I),
*     we should protect  the screen fields and not letting the user
*     to modify the the  contents. This is done by moving 'PROT' to
*     the   screen    function.   If   it   is   not   an   inquiry
*     transaction(WSSP-FLAG not = I), we move 'NORML' to the screen
*     function.
*
*
* *
* *     validating the  screen.  This section is performed repeatedly
*     until no screen  errors  from  Section  100-  in the copybook
*     MAINF.
*
* *     already been set  up in 1000-INITIALISATION). This is done by
*     calling the screen IO module. Once the user has pressed ENTER
*     after  the   screen  has  been  displayed,  the  system  will
*     automatically read the  screen  and  pass the control back to
*     this program with  the  content of the screen. Validation can
*     then begin.
*
* *
* *               hence no validations required.
*
* *
* *               default plan.  Move the plan to the screen and exit
*               this section, redisplay the screen.
*
* *
* *
* *
* *
* *               and amount  entries  are  blank and fund split plan
*               not =  blank
*
* *                    the default plan.  Move the plan to the screen
*                    and exit this section.
*
* *          (Must at least have one set of non-blank entries)
*
* *                    blank and vice versa.
*
* *
* *                    to the instalment amount on the screen.
*
* *
* *                    equal to 100%.
*
* *               T5540(already read in 1000-) as the item. The table
*               entry gives a  list  of  valid  funds. Validate the
*               captured funds against the list. If the fund is not
*               one of those in the list, it is an invalid entry.
*
*
* *
* *
* *
* *
* *               policies for  the chosen component.  So process all
*               the ULNK  records  of the selected contract number,
*               life, coverage and rider as follows:-
*
* *                    parts  of  the  key  from  linkage(Function  =
*                    BEGNH)
*
* *                    to date to today's date - 1 day(Use subroutine
*                    DATCON2). Rewrite the record.
*
* *
* *                    redirection details  for  each  plan suffix as
*                    follows:-
*
* *                    suffix(ULNK).
*
* *                    plan suffix(ULNK).
*
*            Loop 1.
*
* *                    plan suffix(ULNK).
*
* *                    the record  is read, initialise the funds with
*                    space  and   amounts  with  zeroes.  Move  new
*                    details   to   the   record,   including   the
*                    percentage/amount   indicator.   Move   1   to
*                    validflag field,  set  current  from  date  to
*                    today's date and move '99999999' to current to
*                    date.  Move  CHDRLIF-TRANNO   to  ULNK-TRANNO.
*                    Write this record with function WRITR.
*
*               If number of summarised = policy included or plan
*                   suffix(ULNK) = 0
*                   next sentence
*               Else
*                   Subtract 1 from the plan suffix(ULNK).
*
* *
* *
* *               policy(with existing plan suffix).
*
* *               Move 2 to  validflag  field. Set current to date to
*               today's  date  -  1  day(use  subroutine  DATCON2).
*               Rewrite the record.
*
* *               the funds  with  space and amounts with zeroes(ULNK
*               record), move  the new redirection details from the
*               screen  to   the   ULNK   record  fields  including
*               percentage/amount  indicator. Set  the current from
*               date to today's  date  and move 99999999 to current
*               to   date.  Move  1  to   validflag   field.   Move
*               CHDRLIF-TRANNO  to ULNK-TRANNO.  Write  the  record
*               with Function WRITR.
*
* *
* *               policies summarised,  hence  we  have  to perform a
*               break-out.
*
* *               subroutines BRKOUT  and  GENOUT and they are called
*               by the program  P6251AT  at  the  end  of  all  the
*               modifications. The updating  of  ULNK is also to be
*               done in P6251AT.  In  order  for  P6251AT  to  have
*               something  to work  upon,  we  have  to  write  the
*               modified  details   as   a   record  to  a  details
*               file(RDIR)   for  each  modification.  This  record
*               should include  the  captured  details  of ULNK and
*               various linkage fields, particularly the followings
*
*               VALIDFLAG = 1
*               CURRFROM = today's date
*               CURRTO = 99999999
*               TRANNO = CHDRLIF-TRANNO
*
* *
* *
* *
*****************************************************************
* </pre>
*/
public class P5488 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5488");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsaaAge = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaSummRecs = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaInstalment = new PackedDecimalData(17, 0);
	private FixedLengthStringData wsaaUlnkFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaKillNextprog = new FixedLengthStringData(5);
	private String wsaaPlanLevel = "";
	private String wsaaBreakout = "";

	private FixedLengthStringData wsaaFndItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFundSplit = new FixedLengthStringData(4).isAPartOf(wsaaFndItem, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaFndItem, 4, FILLER).init(SPACES);
	private PackedDecimalData wsaaRedisp = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaNotBlank = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTotal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaT5515Sub = new PackedDecimalData(2, 0).init(ZERO);

	private FixedLengthStringData wsaaStoredFunds = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaStoredFund = FLSArrayPartOfStructure(10, 4, wsaaStoredFunds, 0);

	private FixedLengthStringData wsaaStoredAmounts = new FixedLengthStringData(170);
	private ZonedDecimalData[] wsaaStoredAmount = ZDArrayPartOfStructure(10, 17, 2, wsaaStoredAmounts, 0);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbT5551Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbKeypart1 = new FixedLengthStringData(5).isAPartOf(wsbbT5551Key, 0);
	private FixedLengthStringData wsbbKeypart2 = new FixedLengthStringData(3).isAPartOf(wsbbT5551Key, 5);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
		/* TABLES */
	private static final String t2240 = "T2240";
	private static final String t5687 = "T5687";
	private static final String t5515 = "T5515";
	private static final String t5671 = "T5671";
	private static final String t5551 = "T5551";
	private static final String th510 = "TH510";
	private static final String t7508 = "T7508";
	private static final String ulnkrec = "ULNKREC";
	private static final String itemrec = "ITEMREC";
	private static final String rdirrec = "RDIRREC";
	private static final String hitdrec = "HITDREC";
		/*    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
//	private CltsTableDAM cltsIO = new CltsTableDAM(); //ILIFE-8089
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM(); //ILIFE-8089
	private RdirTableDAM rdirIO = new RdirTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T2240rec t2240rec = new T2240rec();
	private T5510rec t5510rec = new T5510rec();
	private T5543rec t5543rec = new T5543rec();
	private T5515rec t5515rec = new T5515rec();
	private T5551rec t5551rec = new T5551rec();
	private T5671rec t5671rec = new T5671rec();
	private Th510rec th510rec = new Th510rec();
	private T7508rec t7508rec = new T7508rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private S5488ScreenVars sv = getLScreenVars();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	
	private PackedDecimalData cnttypercd = new PackedDecimalData(8);//ILIFE-8293
	private ZonedDecimalData outdate1 = null;
	private ZonedDecimalData[] outdates = new ZonedDecimalData[12];
	private ZonedDecimalData outdate2 = null;
	private ZonedDecimalData cnttypepresentdate = null;
	private int[] cnttypeallowedperiod = new int[12];
	private boolean fundPermission = false;
	private int allowperiod =0;
	
	//ILIFE-8089 start
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO= getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	private int covrpfCount = 0;
	private Clntpf clts;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<Clntpf> clntpfList;
	//ILIFE-8089 end
	private boolean fundList = false;//ILIFE-8164
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lifemja1070, 
		loop2086, 
		nextFund2087, 
		checkForErrors2088, 
		exit2090, 
		formatScr2120, 
		exit2190, 
		loop2220, 
		loop22250, 
		loop32260, 
		exit2290, 
		nN3040, 
		summarised3050, 
		fundPrc3060, 
		inc3065, 
		writeRdir3070, 
		diary3080, 
		exit3090, 
		fundAmt3230, 
		inc3235, 
		write3240, 
		fields3420, 
		nxtScrn4020, 
		nextProgram4030, 
		exit4090, 
		screenFields5020, 
		loop5030, 
		exit5090, 
		matchProgName
	}

	public P5488() {
		super();
		screenVars = sv;
		new ScreenModel("S5488", AppVars.getInstance(), sv);
	}

	protected S5488ScreenVars getLScreenVars(){
		return ScreenProgram.getScreenVars( S5488ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

 //ILIFE-8089 start
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
 //ILIFE-8089 end
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					chdrmja1020();
					covrmja1030();
					begnCovr1040();
				case lifemja1070: 
					lifemja1070();
					formScreen1080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.instprem.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.rcdate.set(varcom.vrcmMaxDate);
		sv.unitAllocPercAmt01.set(ZERO);
		sv.unitAllocPercAmt02.set(ZERO);
		sv.unitAllocPercAmt03.set(ZERO);
		sv.unitAllocPercAmt04.set(ZERO);
		sv.unitAllocPercAmt05.set(ZERO);
		sv.unitAllocPercAmt06.set(ZERO);
		sv.unitAllocPercAmt07.set(ZERO);
		sv.unitAllocPercAmt08.set(ZERO);
		sv.unitAllocPercAmt09.set(ZERO);
		sv.unitAllocPercAmt10.set(ZERO);
		fundPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP086", appVars, "IT");
		fundList = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP106", appVars, "IT");//ILIFE-8164
	}

protected void chdrmja1020()
	{
		/*    Retrieve Contract Header*/
	//ILIFE-8089 start	
			chdrpf = chdrpfDAO.getCacheObject(chdrpf);
			if(null==chdrpf) {
				chdrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
				&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					fatalError600();
				}
				else {
					chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
					if(null==chdrpf) {
						fatalError600();
					}
					else {
						chdrpfDAO.setCacheObject(chdrpf);
					}
				}
			}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		
		compute(wsaaTranno, 0).set(add(1, chdrpf.getTranno()));
		wsaaSummRecs.set(chdrpf.getPolsum());
//ILIFE-8089 end
	}

protected void covrmja1030()
	{
		/*    Retrieve COVRMJA*/
	//ILIFE-8089 start
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					syserrrec.statuz.set(covrmjaIO.getStatuz());
					fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		//ILIFE-8089 end
	}


protected void allowperiodcalulation2021()
{   int i =0;
     int j=0;
	allowperiod=0;
	int k=0;
	
	  
    initialize(datcon1rec.datcon1Rec);
    datcon1rec.datcon1Rec.set(SPACES);
    datcon1rec.function.set(varcom.tday);
    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    cnttypepresentdate = datcon1rec.intDate;
	
	
	if (fundPermission == true )
	{
		cnttypercd.set(covrpf.getCrrcd());//ILIFE-8293
		t55432084();
		for(FixedLengthStringData funds:sv.unitVirtualFund) { //loop through all funds selected on UI
		for(i=0; i<12;i++)
		{
			if(funds!=null && funds.equals(t5543rec.unitVirtualFund[i])) {
				break;
			}
			
		}
		if(i==12)
			k=11;
		else
			k=i;
		cnttypeallowedperiod[j]=0;
		if(!isEQ(t5543rec.allowperiod[i],SPACES)  && !isEQ(t5543rec.unitVirtualFund[i],SPACES)) {
			cnttypeallowedperiod[j] = t5543rec.allowperiod[i].toInt();
		
		}
	
		outdates[j]=null;
		if (cnttypeallowedperiod[j]!=0)
		{
			
			initialize(datcon2rec.datcon2Rec);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(cnttypercd);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(cnttypeallowedperiod[j]);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);  
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				datcon2rec.freqFactorx.set(ZERO);
			}
			outdates[j] = datcon2rec.intDate2;
		 }
	
		if (outdates[j]!= null && isLT(cnttypepresentdate.toInt(),outdates[j]) ) { 
			sv.vrtfndErr[j].set(errorsInner.rrku);
			wsspcomn.edterror.set("Y");
			
		}
		j++;
		}
	}  
}
	
protected void begnCovr1040()
	{
		/*    If Whole Plan selected BEGN COVR to find highest plan suff*/
//ILIFE-8089 start
		if (isNE(covrpf.getPlanSuffix(), ZERO)) {
			wsaaPlanLevel = "N";
			goTo(GotoLabel.lifemja1070);
		}
		wsaaPlanLevel = "Y";
	//	covrpf.setPlanSuffix(9999); //ILIFE-8089
		covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
		/*covrmjaIO.setPlanSuffix("9999");
		covrmjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		if (isEQ(covrpf.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(covrpf.getChdrnum(), chdrpf.getChdrnum())) {
			goTo(GotoLabel.lifemja1070);
		}
		/*syserrrec.params.set(covrmjaIO.getParams());*/
		fatalError600();
//ILIFE-8089 end
	}

protected void lifemja1070()
	{
	//ILIFE-8089 start
		lifepf = lifepfDAO.getLifeRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
		if(lifepf == null) {
			fatalError600();
		}
	
		/*lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}*/
		sv.lifenum.set(lifepf.getLifcnum());
		wsaaAge.set(lifepf.getAnbAtCcd());
		
		clts = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifepf.getLifcnum());
		if(clts==null){
			sv.linsname.fill("?");
		}
		/*sv.anbrcd.set(wsaaAge);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		    MOVE LIFEMJA-CHDRCOY       TO CLTS-CLNTCOY.                  
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		/*if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.linsname.fill("?");
		}*/
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	//ILIFE-8089 end
	}

protected void formScreen1080()
	{
		wsaaKillNextprog.set(wsspcomn.nextprog);
		loadScreen5000();
		//ILIFE-8164-START
		if(fundList)
		{
			readT55516000();
			t55432084();
			int ix = 0;
			for(FixedLengthStringData recData:t5543rec.unitVirtualFund) {
				if(recData != null) {
					sv.newFundList[ix].set(recData);	
				}
				ix++;
			}
			
			
				sv.newFundList01Out[varcom.nd.toInt()].set("N");
				sv.newFundList02Out[varcom.nd.toInt()].set("N");
				sv.newFundList03Out[varcom.nd.toInt()].set("N");
				sv.newFundList04Out[varcom.nd.toInt()].set("N");
				sv.newFundList05Out[varcom.nd.toInt()].set("N");
				sv.newFundList06Out[varcom.nd.toInt()].set("N");
				sv.newFundList07Out[varcom.nd.toInt()].set("N");
				sv.newFundList08Out[varcom.nd.toInt()].set("N");
				sv.newFundList09Out[varcom.nd.toInt()].set("N");
				sv.newFundList10Out[varcom.nd.toInt()].set("N");
				sv.newFundList11Out[varcom.nd.toInt()].set("N");
				sv.newFundList12Out[varcom.nd.toInt()].set("N");
				
			}
		else {

			sv.newFundList01Out[varcom.nd.toInt()].set("Y");
			sv.newFundList02Out[varcom.nd.toInt()].set("Y");
			sv.newFundList03Out[varcom.nd.toInt()].set("Y");
			sv.newFundList04Out[varcom.nd.toInt()].set("Y");
			sv.newFundList05Out[varcom.nd.toInt()].set("Y");
			sv.newFundList06Out[varcom.nd.toInt()].set("Y");
			sv.newFundList07Out[varcom.nd.toInt()].set("Y");
			sv.newFundList08Out[varcom.nd.toInt()].set("Y");
			sv.newFundList09Out[varcom.nd.toInt()].set("Y");
			sv.newFundList10Out[varcom.nd.toInt()].set("Y");
			sv.newFundList11Out[varcom.nd.toInt()].set("Y");
			sv.newFundList12Out[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-8164-END
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		wsaaStoredFunds.set(sv.unitVirtualFunds);
		wsaaStoredAmounts.set(sv.unitAllocPercAmts);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2070();
					perAmtInd2075();
					fundPer2080();
					t55432084();
				case loop2086: 
					loop2086();
				case nextFund2087: 
					nextFund2087();
				case checkForErrors2088: 
					checkForErrors2088();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*2065-SCREEN-IO.                                                  
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S5488IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5488-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		allowperiodcalulation2021();
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.checkForErrors2088);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2070()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*CF9 pressed and fund split plan not blank                    */
		/*IF SCRN-STATUZ              = 'CALC' AND                     */
		/*   S5488-VIRT-FUND-SPLIT-METHOD NOT = SPACE                  */
		/*   NEXT SENTENCE                                             */
		/*ELSE                                                         */
		/*   GO 2075-PER-AMT-IND.                                      */
		/*Read T5510 for the default plan                              */
		/*PERFORM 2100-READ-TABLE.                                     */
		/*IF S5488-FNDSPL-ERR       NOT = SPACE                        */
		/*   GO 2088-CHECK-FOR-ERRORS.                                 */
		/*GO 2065-SCREEN-IO.                                           */
		if (isNE(sv.virtFundSplitMethod, SPACES)) {
			validateFundSplit2400();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.checkForErrors2088);
		}
		
	}

protected void perAmtInd2075()
	{
		/*    Validate Percentage/Amount indicator*/
		if (isNE(sv.percOrAmntInd, "A")
		&& isNE(sv.percOrAmntInd, "P")) {
			sv.prcamtindErr.set(errorsInner.g347);
			goTo(GotoLabel.checkForErrors2088);
		}
		if (isEQ(wsaaPlanLevel, "Y")
		&& isNE(sv.percOrAmntInd, "P")) {
			/*     MOVE U054                  TO S5488-PRCAMTIND-ERR         */
			sv.prcamtindErr.set(errorsInner.h081);
			goTo(GotoLabel.checkForErrors2088);
		}
	}

protected void fundPer2080()
	{
		/*    Validate Fund & Percentage/Amount*/
		fundPerAmt2200();
		if (isGT(wsaaRedisp, ZERO)) {
			wsaaRedisp.set(ZERO);
			/*       GO 2065-SCREEN-IO.                                        */
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void t55432084()
	{
		/*    Read T5515 to validate the table entries                     */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		for (wsaaT5515Sub.set(1); !(isGT(wsaaT5515Sub, 10)); wsaaT5515Sub.add(1)){
			readT55152300();
		}
		if (isNE(sv.errorIndicators, SPACES)
		|| isEQ(wsspcomn.edterror, "Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2088);
		}
		/* Read T5543 using virtual fund from T5540                     */
		/*    Read T5543 using virtual fund from T5551                     */
		readT55516000();
		if (isNE(sv.errorIndicators, SPACES)
		|| isEQ(wsspcomn.edterror, "Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2088);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.begn);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl("T5543");
		itemIO.setItemitem(t5551rec.alfnds);
		/*MOVE WSAA-FUND-SPLIT-PLAN     TO ITEM-ITEMITEM.              */
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.fndsplErr.set(errorsInner.g028);
		}
		/*    Validate funds against table entries*/
		wsaaSub.set(1);
		wsaaSub2.set(1);
		t5543rec.t5543Rec.set(itemIO.getGenarea());
	}

protected void loop2086()
	{
		if (isEQ(sv.unitVirtualFund[wsaaSub.toInt()], t5543rec.unitVirtualFund[wsaaSub2.toInt()])) {
			goTo(GotoLabel.nextFund2087);
		}
		wsaaSub2.add(1);
		if (isGT(wsaaSub2, 12)) {
			/*    MOVE G028                   TO                            */
			/*    MOVE H427                   TO                       <005>*/
			sv.vrtfndErr[wsaaSub.toInt()].set(errorsInner.h456);
			goTo(GotoLabel.checkForErrors2088);
		}
		goTo(GotoLabel.loop2086);
	}

protected void nextFund2087()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 10)) {
			goTo(GotoLabel.checkForErrors2088);
		}
		if (isEQ(sv.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			goTo(GotoLabel.nextFund2087);
		}
		wsaaSub2.set(1);
		goTo(GotoLabel.loop2086);
	}

protected void checkForErrors2088()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(scrnparams.statuz, varcom.calc)) {
			goTo(GotoLabel.exit2090);
		}
		/*REDISPLAY*/
		wsspcomn.edterror.set("Y");
	}

protected void readTable2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read2110();
				case formatScr2120: 
					formatScr2120();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2110()
	{
		/*    Initialize the screen subfile area.                          */
		sv.unitVirtualFunds.set(SPACES);
		sv.unitAllocPercAmts.set(ZERO);
		/*    Read T5510 for the default plan*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5510");
		itdmIO.setItmfrm(chdrpf.getOccdate());//ILIFE-8089
		wsaaFundSplit.set(sv.virtFundSplitMethod);
		itdmIO.setItemitem(wsaaFndItem);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.mrnf)) {
			sv.fndsplErr.set(errorsInner.g103);
			goTo(GotoLabel.exit2190);
		}
		if (isNE(itdmIO.getItemitem(), wsaaFundSplit)) {
			sv.fndsplErr.set(errorsInner.g103);
			goTo(GotoLabel.exit2190);
		}
		t5510rec.t5510Rec.set(itdmIO.getGenarea());
		wsaaSub.set(1);
	}

protected void formatScr2120()
	{
		if (isNE(t5510rec.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			sv.unitVirtualFund[wsaaSub.toInt()].set(t5510rec.unitVirtualFund[wsaaSub.toInt()]);
			sv.unitAllocPercAmt[wsaaSub.toInt()].set(t5510rec.unitPremPercent[wsaaSub.toInt()]);
		}
		else {
			return ;
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.formatScr2120);
		}
	}

protected void fundPerAmt2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					sub2210();
				case loop2220: 
					loop2220();
					t55102230();
				case loop22250: 
					loop22250();
				case loop32260: 
					loop32260();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void sub2210()
	{
		wsaaSub.set(1);
		wsaaNotBlank.set(SPACES);
	}

protected void loop2220()
	{
		/*    Check if all occurrences of fund & amount are blank*/
		if (isNE(sv.unitAllocPercAmt[wsaaSub.toInt()], ZERO)) {
			wsaaNotBlank.set("Y");
		}
		if (isNE(sv.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			wsaaNotBlank.set("Y");
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.loop2220);
		}
	}

protected void t55102230()
	{
		/*    Percentage/Amount ind = 'P' & all entries blank*/
		if (isNE(wsaaNotBlank, "Y")
		&& isEQ(sv.percOrAmntInd, "P")
		&& isNE(sv.virtFundSplitMethod, SPACES)) {
			readTable2100();
			wsaaRedisp.set(1);
			goTo(GotoLabel.exit2290);
		}
		/*OCCS*/
		/*    Check at least one set of non-blank entries*/
		if (isNE(wsaaNotBlank, "Y")) {
			sv.vrtfndErr[1].set(errorsInner.e207);
			goTo(GotoLabel.exit2290);
		}
		wsaaSub.set(1);
	}

protected void loop22250()
	{
		/*    Check both fund & percentage/amount are present for entry*/
		if (isEQ(sv.unitAllocPercAmt[wsaaSub.toInt()], ZERO)
		&& isNE(sv.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			sv.ualprcErr[wsaaSub.toInt()].set(errorsInner.h366);
			goTo(GotoLabel.exit2290);
		}
		if (isNE(sv.unitAllocPercAmt[wsaaSub.toInt()], ZERO)
		&& isEQ(sv.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			sv.vrtfndErr[wsaaSub.toInt()].set(errorsInner.h366);
			goTo(GotoLabel.exit2290);
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.loop22250);
		}
		wsaaSub.set(1);
		wsaaTotal.set(ZERO);
	}

protected void loop32260()
	{
		/*    Add non-blank amounts*/
		if (isGT(sv.unitAllocPercAmt[wsaaSub.toInt()], ZERO)) {
			wsaaTotal.add(sv.unitAllocPercAmt[wsaaSub.toInt()]);
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.loop32260);
		}
		/*CHK-TOT*/
		/*    Check total fund = instalment amount*/
		if (isEQ(sv.percOrAmntInd, "A")
		&& isNE(wsaaTotal, sv.instprem)) {
			sv.ualprc01Err.set(errorsInner.g106);
			return ;
		}
		if (isEQ(sv.percOrAmntInd, "P")
		&& isNE(wsaaTotal, 100)) {
			sv.ualprc01Err.set(errorsInner.e631);
		}
	}

protected void readT55152300()
	{
		start2300();
	}

protected void start2300()
	{
		/*     VALIDATE T5515 TO CHECK FUNDS EXIST                         */
		if (isEQ(sv.unitVirtualFund[wsaaT5515Sub.toInt()], SPACES)) {
			return ;
		}
		/* MOVE SPACES                 TO ITEM-DATA-AREA        <A06707>*/
		/* MOVE 'IT'                   TO ITEM-ITEMPFX          <A06707>*/
		/* MOVE CHDRMJA-CHDRCOY        TO ITEM-ITEMCOY          <A06707>*/
		/* MOVE T5515                  TO ITEM-ITEMTABL         <A06707>*/
		/* MOVE S5488-UNIT-VIRTUAL-FUND(WSAA-T5515-SUB)         <A06707>*/
		/*                             TO ITEM-ITEMITEM         <A06707>*/
		/* MOVE READR                  TO ITEM-FUNCTION         <A06707>*/
		/*                                                      <A06707>*/
		/* CALL 'ITEMIO'               USING ITEM-PARAMS        <A06707>*/
		/*                                                      <A06707>*/
		/* IF (ITEM-STATUZ             NOT = O-K) AND           <A06707>*/
		/*    (ITEM-STATUZ             NOT = MRNF)              <A06707>*/
		/*    MOVE ITEM-PARAMS         TO SYSR-PARAMS           <A06707>*/
		/*    PERFORM 600-FATAL-ERROR                           <A06707>*/
		/* END-IF.                                              <A06707>*/
		/* IF (ITEM-STATUZ             = MRNF)                  <A06707>*/
		/*    MOVE G037    TO S5488-VRTFND-ERR(WSAA-T5515-SUB)  <A06707>*/
		/*    GO TO 2390-EXIT                                   <A06707>*/
		/* END-IF.                                              <A06707>*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());//ILIFE-8089
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(sv.unitVirtualFund[wsaaT5515Sub.toInt()]);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isEQ(itdmIO.getStatuz(), varcom.endp))
		|| (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t5515))
		|| (isNE(itdmIO.getItemitem(), sv.unitVirtualFund[wsaaT5515Sub.toInt()]))) {
			sv.vrtfndErr[wsaaT5515Sub.toInt()].set(errorsInner.g037);
			return ;
		}
	}

protected void validateFundSplit2400()
	{
		/*START*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			readTable2100();
			return ;
		}
		if (isNE(sv.unitVirtualFunds, wsaaStoredFunds)
		|| isNE(sv.unitAllocPercAmts, wsaaStoredAmounts)) {
			sv.fndsplErr.set(errorsInner.h404);
			wsspcomn.edterror.set("Y");
			return ;
		}
		else {
			readTable2100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
					process3030();
				case nN3040: 
					nN3040();
				case summarised3050: 
					summarised3050();
				case fundPrc3060: 
					fundPrc3060();
				case inc3065: 
					inc3065();
				case writeRdir3070: 
					writeRdir3070();
				case diary3080: 
					diary3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaUlnkFlag, "N")) {
			goTo(GotoLabel.exit3090);
		}
		if (isNE(wsaaPlanLevel, "Y")) {
			goTo(GotoLabel.nN3040);
		}
		/*BEGNH-ULNK*/
		/*    BEGNH ULNK to process all policies for chosen component*/
		begnhUlnk3100();
	}

protected void process3030()
	{
		/*    Rewrites & writes new ULNK record*/
		updateUlnk3200();
		/*    GO 3090-EXIT.                                                */
		goTo(GotoLabel.diary3080);
	}

protected void nN3040()
	{
		/*    Processing for when redirection applies only to 1 policy*/
		if (isEQ(wsaaPlanLevel, "N")
		&& isEQ(wsaaBreakout, "Y")) {
			goTo(GotoLabel.summarised3050);
		}
		begnhUlnk3100();
		updateUlnk3200();
		/*    GO 3090-EXIT.                                                */
		goTo(GotoLabel.diary3080);
	}

protected void summarised3050()
	{
		/*    Processing for a Summarised Policy - format RDIR record*/
		initRdir3400();
		rdirIO.setValidflag("1");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rdirIO.setCurrfrom(datcon1rec.intDate);
		rdirIO.setCurrto("99999999");
		rdirIO.setTranno(wsaaTranno);
		rdirIO.setChdrcoy(ulnkIO.getChdrcoy());
		rdirIO.setChdrnum(ulnkIO.getChdrnum());
		rdirIO.setLife(ulnkIO.getLife());
		rdirIO.setCoverage(ulnkIO.getCoverage());
		rdirIO.setRider(ulnkIO.getRider());
		rdirIO.setPlanSuffix(covrpf.getPlanSuffix());//ILIFE-8089
		rdirIO.setPercOrAmntInd(sv.percOrAmntInd);
		wsaaSub.set(1);
		wsaaSub2.set(ZERO);
	}

protected void fundPrc3060()
	{
		/*    Move screen occurrences of fund & amount to record*/
		if (isEQ(sv.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			goTo(GotoLabel.inc3065);
		}
		wsaaSub2.add(1);
		if (isGT(wsaaSub2, 10)) {
			goTo(GotoLabel.writeRdir3070);
		}
		rdirIO.setUalprc(wsaaSub2, sv.unitAllocPercAmt[wsaaSub.toInt()]);
		rdirIO.setUalfnd(wsaaSub2, sv.unitVirtualFund[wsaaSub.toInt()]);
	}

protected void inc3065()
	{
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.fundPrc3060);
		}
	}

protected void writeRdir3070()
	{
		/*    Writes RDIR record*/
		rdirIO.setFormat(rdirrec);
		rdirIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, rdirIO);
		if (isNE(rdirIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rdirIO.getParams());
			fatalError600();
		}
	}

protected void diary3080()
	{
		dryProcessing8000();
	}

protected void begnhUlnk3100()
	{
		begnh3110();
	}

protected void begnh3110()
	{
		/*    BEGNH ULNK*/
//ILIFE-8089 start
		ulnkIO.setChdrcoy(covrpf.getChdrcoy());
		ulnkIO.setLife(covrpf.getLife());
		ulnkIO.setCoverage(covrpf.getCoverage());
		ulnkIO.setRider(covrpf.getRider());
		ulnkIO.setChdrnum(chdrpf.getChdrnum());
		ulnkIO.setPlanSuffix(covrpf.getPlanSuffix());
		ulnkIO.setFunction("BEGNH");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isEQ(ulnkIO.getStatuz(), varcom.oK)
		&& isEQ(ulnkIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(ulnkIO.getChdrnum(), chdrpf.getChdrnum())) {
			return ;
//ILIFE-8089 end
		}
		syserrrec.params.set(ulnkIO.getParams());
		fatalError600();
	}

protected void updateUlnk3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					rewrite3210();
					newRec3220();
				case fundAmt3230: 
					fundAmt3230();
				case inc3235: 
					inc3235();
				case write3240: 
					write3240();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void rewrite3210()
	{
		/*    Rewrites ULNK record for redirection details*/
		ulnkIO.setValidflag("2");
		ulnkIO.setFndSpl(sv.virtFundSplitMethod);//ILIFE-5308
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		ulnkIO.setCurrto(datcon2rec.intDate2);
		ulnkIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
	}

protected void newRec3220()
	{
		/*    Write new ULNK record*/
		ulnkIO.setUalfnds(SPACES);
		wsaaSub.set(1);
		while ( !(isEQ(wsaaSub, 11))) {
			zeroise3300();
		}
		
		ulnkIO.setPercOrAmntInd(sv.percOrAmntInd);
		ulnkIO.setTranno(wsaaTranno);
		ulnkIO.setValidflag("1");
		ulnkIO.setCurrfrom(datcon1rec.intDate);
		ulnkIO.setCurrto("99999999");
		wsaaSub.set(1);
		wsaaSub2.set(ZERO);
	}

protected void fundAmt3230()
	{
		/*    Formats Fund & Amount fields from screen*/
		if (isEQ(sv.unitVirtualFund[wsaaSub.toInt()], SPACES)) {
			goTo(GotoLabel.inc3235);
		}
		wsaaSub2.add(1);
		if (isGT(wsaaSub2, 10)) {
			goTo(GotoLabel.write3240);
		}
		ulnkIO.setUalprc(wsaaSub2, sv.unitAllocPercAmt[wsaaSub.toInt()]);
		ulnkIO.setUalfnd(wsaaSub2, sv.unitVirtualFund[wsaaSub.toInt()]);
		a100ReadT5515();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			a200CheckHitd();
		}
	}

protected void inc3235()
	{
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.fundAmt3230);
		}
	}

protected void write3240()
	{
		ulnkIO.setFormat(ulnkrec);
		ulnkIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void zeroise3300()
	{
		/*ZEROISE*/
		/*    Zeroise aeach occurrence of UALPRC*/
		ulnkIO.setUalprc(wsaaSub, ZERO);
		wsaaSub.add(1);
		/*EXIT*/
	}

protected void initRdir3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3410();
				case fields3420: 
					fields3420();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Initialises fields on RDIR record
	* </pre>
	*/
protected void start3410()
	{
		wsaaSub.set(1);
	}

protected void fields3420()
	{
		rdirIO.setUalfnd(wsaaSub, SPACES);
		rdirIO.setUalprc(wsaaSub, ZERO);
		rdirIO.setUspcpr(wsaaSub, ZERO);
		/*INC*/
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.fields3420);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					kill4005();
					nextCovr4010();
				case nxtScrn4020: 
					nxtScrn4020();
				case nextProgram4030: 
					nextProgram4030();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void kill4005()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(wsaaKillNextprog);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
	}

protected void nextCovr4010()
	{
		/*    Reads next COVR record where Whole Plan has been selected*/
//ILIFE-8089 start
	if(covrpfList != null && covrpfList.size() > 0)
	{
		for(Covrpf covr : covrpfList)
		{
			if (covr == null) {
				fatalError600();
			}
		if (isEQ(wsaaPlanLevel, "N")) {
			goTo(GotoLabel.nextProgram4030);
		}
		wsaaUlnkFlag.set(SPACES);
		/*    Checks to see if need to read next COVR*/
		if (isEQ(covr.getPlanSuffix(), ZERO)) {
			goTo(GotoLabel.nextProgram4030);
		}
		/*covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		if (isEQ(covrpf.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(covrpf.getChdrnum(), chdrpf.getChdrnum())) {
			goTo(GotoLabel.nxtScrn4020);
		}
		else {
			goTo(GotoLabel.nextProgram4030);
		}
		}
	}
	}
//ILIFE-8089 end
protected void nxtScrn4020()
	{
		/*    Loop back round program if another screen display required*/
		loadScreen5000();
		wsspcomn.nextprog.set(scrnparams.scrname);
		goTo(GotoLabel.exit4090);
	}

protected void nextProgram4030()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void loadScreen5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					format5010();
				case screenFields5020: 
					screenFields5020();
					tableDesc5025();
				case loop5030: 
					loop5030();
					instAmt5040();
				case exit5090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void format5010()
	{
		/*    Formats screen for COVR & ULNK records*/
		/*    Read ULNK*/
		ulnkIO.setParams(SPACES);
//ILIFE-8089 start
		ulnkIO.setChdrcoy(covrpf.getChdrcoy());
		ulnkIO.setChdrnum(covrpf.getChdrnum());
		ulnkIO.setLife(covrpf.getLife());
		ulnkIO.setJlife(lifepf.getJlife());
		ulnkIO.setCoverage(covrpf.getCoverage());
		ulnkIO.setRider(covrpf.getRider());
		ulnkIO.setChdrnum(chdrpf.getChdrnum());
		ulnkIO.setPlanSuffix(covrpf.getPlanSuffix());
//ILIFE-8089 end
		ulnkIO.setFunction("READR");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)
		&& isNE(ulnkIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		if (isNE(ulnkIO.getStatuz(), varcom.mrnf)) {
			wsaaBreakout = "N";
			goTo(GotoLabel.screenFields5020);
		}
		ulnkIO.setPlanSuffix(ZERO);
		ulnkIO.setFunction("READR");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)
		&& isNE(ulnkIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		if (isEQ(ulnkIO.getStatuz(), varcom.oK)) {
			wsaaBreakout = "Y";
		}
		else {
			wsaaUlnkFlag.set("N");
			goTo(GotoLabel.exit5090);
		}
	}

protected void screenFields5020()
	{
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		/* MOVE CHDRMJA-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());//ILIFE-8089
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */	
			}
		}
		sv.crtable.set(covrpf.getCrtable());//ILIFE-8089
		sv.rcdate.set(covrpf.getCrrcd());//ILIFE-8089
		sv.chdrnum.set(ulnkIO.getChdrnum());
		sv.life.set(ulnkIO.getLife());
		sv.coverage.set(ulnkIO.getCoverage());
		sv.rider.set(ulnkIO.getRider());
		sv.planSuffix.set(ulnkIO.getPlanSuffix());
		sv.percOrAmntInd.set(ulnkIO.getPercOrAmntInd());
		wsaaSub.set(1);
	}

protected void tableDesc5025()
	{
		/*    Get description CRTABLE*/
		descIO.setDescitem(covrpf.getCrtable());//ILIFE-8089
		descIO.setDesctabl(t5687);
		callDescio5300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill("?");
		}
	}

protected void loop5030()
	{
		sv.unitVirtualFund[wsaaSub.toInt()].set(ulnkIO.getUalfnd(wsaaSub));
//ILIFE-8089
		if (isEQ(covrpf.getPlanSuffix(), ZERO)
		&& isNE(ulnkIO.getPercOrAmntInd(), "P")
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(sv.unitAllocPercAmt[wsaaSub.toInt()], 2).set(sub(ulnkIO.getUalprc(wsaaSub), (div(mult(ulnkIO.getUalprc(wsaaSub), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum()))));
		}
		else {
			sv.unitAllocPercAmt[wsaaSub.toInt()].set(ulnkIO.getUalprc(wsaaSub));
		}
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 10)) {
			goTo(GotoLabel.loop5030);
		}
	}

protected void instAmt5040()
	{
		/* IF WSAA-PLAN-LEVEL          = 'Y'                            */
		/*    MOVE ZERO                TO S5488-INSTPREM                */
		/*    GO 5050-FUND-SPLIT.                                       */
		/* IF WSAA-BREAKOUT            = 'N'                            */
		/*    MOVE COVRMJA-SINGP          TO S5488-INSTPREM             */
		/* ELSE                                                         */
		/*    COMPUTE WSAA-INSTALMENT  = COVRMJA-SINGP / WSAA-SUMM-RECS */
		/*    MOVE WSAA-INSTALMENT     TO S5488-INSTPREM.               */
		/*  Display INSTPREM on screen                                  */
		if (isEQ(wsaaBreakout, "N")) {
			sv.instprem.set(covrpf.getInstprem());
		}
		else {
			compute(wsaaInstalment, 2).set(div(covrpf.getInstprem(), wsaaSummRecs));
			sv.instprem.set(wsaaInstalment);
		}
		/*FUND-SPLIT*/
		/* MOVE SPACES                 TO ITDM-DATA-KEY.                */
		/* MOVE 'IT'                   TO ITDM-ITEMPFX.                 */
		/* MOVE BEGN                   TO ITDM-FUNCTION.                */
		/* MOVE WSSP-COMPANY           TO ITDM-ITEMCOY.                 */
		/* MOVE T5540                  TO ITDM-ITEMTABL.                */
		/* MOVE COVRMJA-CRTABLE           TO ITDM-ITEMITEM.             */
		/* MOVE VRCM-MAX-DATE          TO ITDM-ITMFRM.                  */
		/* CALL 'ITDMIO'               USING ITDM-PARAMS.               */
		/* IF ITDM-STATUZ              NOT = O-K AND                    */
		/*                             NOT = MRNF                       */
		/*    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* MOVE ITDM-GENAREA           TO T5540-T5540-REC.              */
		/*    MOVE T5540-FUND-SPLIT-PLAN  TO                               */
		/*                          WSAA-FUND-SPLIT-PLAN                   */
		/*                          S5488-VIRT-FUND-SPLIT-METHOD.          */
		/* MOVE T5540-ALFNDS           TO WSAA-FUND-SPLIT-PLAN.    <002>*/
		/* MOVE T5540-FUND-SPLIT-PLAN  TO                          <002>*/
		sv.virtFundSplitMethod.set(SPACES);
	}

protected void callDescio5300()
	{
		/*READ*/
		/*    Call description IO module to read item description*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readT55516000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6010();
				case matchProgName: 
					matchProgName();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6010()
	{
		/* Coverage/Rider Switching.                                       */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(sv.crtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/* Match the T5671 Item key with prog on T5551.                    */
		x.set(0);
	}

protected void matchProgName()
	{
		x.add(1);
		if (isGT(x, 4)) {
			scrnparams.errorCode.set(errorsInner.e302);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
			if (isEQ(t5671rec.edtitm[x.toInt()], SPACES)) {
				sv.crtableErr.set(errorsInner.g513);
				wsspcomn.edterror.set("Y");
				return ;
			}
			else {
				wsbbKeypart1.set(t5671rec.edtitm[x.toInt()]);
			}
		}
		else {
			goTo(GotoLabel.matchProgName);
		}
		itdmIO.setFunction(varcom.begn);
		//start life performance atiwari
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
		//end
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5551);
		wsbbKeypart2.set(chdrpf.getCntcurr());//ILIFE-8089
		itdmIO.setItemitem(wsbbT5551Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());//ILIFE-8089
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5551)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			sv.crtableErr.set(errorsInner.g513);
			wsspcomn.edterror.set("Y");
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void a100ReadT5515()
	{
		a110T5515();
	}

protected void a110T5515()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub2));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//start life performance atiwari
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		//End
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5515)
		|| isNE(itdmIO.getItemitem(), ulnkIO.getUalfnd(wsaaSub2))
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void a200CheckHitd()
	{
		a210Hitd();
	}

protected void a210Hitd()
	{
		hitdIO.setParams(SPACES);
//ILIFE-8089 start
		hitdIO.setChdrcoy(covrpf.getChdrcoy());
		hitdIO.setChdrnum(covrpf.getChdrnum());
		hitdIO.setLife(covrpf.getLife());
		hitdIO.setCoverage(covrpf.getCoverage());
		hitdIO.setRider(covrpf.getRider());
		hitdIO.setPlanSuffix(covrpf.getPlanSuffix());
//ILIFE-8089 end
		hitdIO.setZintbfnd(ulnkIO.getUalfnd(wsaaSub2));
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)
		&& isNE(hitdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError600();
		}
		if (isEQ(hitdIO.getStatuz(), varcom.oK)) {
			return ;
		}
		hitdIO.setZlstintdte(varcom.vrcmMaxDate);
		hitdIO.setZlstsmtdt(varcom.vrcmMaxDate);
		hitdIO.setZlstfndval(ZERO);
		hitdIO.setZlstsmtno(ZERO);
		hitdIO.setZlstsmtbal(ZERO);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(th510);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub2));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//start life performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		//end
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), th510)
		|| isNE(itdmIO.getItemitem(), ulnkIO.getUalfnd(wsaaSub2))
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemtabl(th510);
			itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub2));
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		th510rec.th510Rec.set(itdmIO.getGenarea());
		a400GetNextFreq();
		hitdIO.setZnxtintdte(datcon2rec.intDate2);
		hitdIO.setTranno(wsaaTranno);
		hitdIO.setValidflag("1");
		hitdIO.setEffdate(datcon1rec.intDate);
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError600();
		}
	}

protected void a400GetNextFreq()
	{
		a410Next();
	}

protected void a410Next()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(th510rec.zintalofrq);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isNE(th510rec.zintfixdd, ZERO)) {
			/*     MOVE TH510-ZINTFIXDD    TO DTC2-INT-DATE-2 (5:2) <LA4737>*/
			datcon2rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrpf.getCnttype());//ILIFE-8089
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum());//ILIFE-8089
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set("9");
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
//ILIFE-8089 start
		drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
		drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypRcesdte.set(covrpf.getRiskCessDate());
		if (isEQ(covrpf.getUnitStatementDate(), 0)) {
			drypDryprcRecInner.drypCbunst.set(covrpf.getCrrcd());
		}
		else {
			drypDryprcRecInner.drypCbunst.set(covrpf.getUnitStatementDate());
		}
//ILIFE-8089 end
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData g347 = new FixedLengthStringData(4).init("G347");
	private FixedLengthStringData h081 = new FixedLengthStringData(4).init("H081");
	private FixedLengthStringData h366 = new FixedLengthStringData(4).init("H366");
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData g028 = new FixedLengthStringData(4).init("G028");
	private FixedLengthStringData h456 = new FixedLengthStringData(4).init("H456");
	private FixedLengthStringData e207 = new FixedLengthStringData(4).init("E207");
	private FixedLengthStringData g106 = new FixedLengthStringData(4).init("G106");
	private FixedLengthStringData g103 = new FixedLengthStringData(4).init("G103");
	private FixedLengthStringData g037 = new FixedLengthStringData(4).init("G037");
	private FixedLengthStringData e302 = new FixedLengthStringData(4).init("E302");
	private FixedLengthStringData g513 = new FixedLengthStringData(4).init("G513");
	private FixedLengthStringData h404 = new FixedLengthStringData(4).init("H404");
	private FixedLengthStringData rrku = new FixedLengthStringData(4).init("RRKU");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}
}