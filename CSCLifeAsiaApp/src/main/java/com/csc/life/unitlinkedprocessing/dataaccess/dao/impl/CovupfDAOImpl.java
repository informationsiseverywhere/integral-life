package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.CovupfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Covupf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovupfDAOImpl extends BaseDAOImpl<Covupf> implements CovupfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CovupfDAOImpl.class);

    public List<Covupf> searchCovuTempRecord(String tableName, String memName) {
        StringBuilder sqlCovuSelect1 = new StringBuilder("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER FROM ");
        sqlCovuSelect1.append(tableName);
        sqlCovuSelect1.append(" WHERE MEMBER_NAME=? ORDER BY UNIQUE_NUMBER ASC");

        PreparedStatement psCovuSelect = getPrepareStatement(sqlCovuSelect1.toString());
        ResultSet sqlCovupf1rs = null;
        List<Covupf> utrnpfList = new ArrayList<>();
        try {
        	psCovuSelect.setString(1, memName);
            sqlCovupf1rs = executeQuery(psCovuSelect);
            while (sqlCovupf1rs.next()) {
            	Covupf c = new Covupf();
            	c.setChdrcoy(sqlCovupf1rs.getString(1));
            	c.setChdrnum(sqlCovupf1rs.getString(2));
            	c.setLife(sqlCovupf1rs.getString(3));
            	c.setCoverage(sqlCovupf1rs.getString(4));
            	c.setRider(sqlCovupf1rs.getString(5));
            	c.setPlanSuffix(sqlCovupf1rs.getInt(6));
            	c.setUniqueNUmber(sqlCovupf1rs.getLong(7));
            	
                 utrnpfList.add(c);
                }
            } catch (SQLException e) {
            LOGGER.error("searchCovuTempRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovuSelect, sqlCovupf1rs);
        }
        return utrnpfList;

    }
}