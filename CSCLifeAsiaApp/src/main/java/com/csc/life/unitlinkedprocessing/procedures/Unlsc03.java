/*
 * File: Unlsc03.java
 * Date: 30 August 2009 2:52:00
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLSC03.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6656rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Full Surrender Charge/Penalty Calculation Method #3.
* ----------------------------------------------------
*
* PROCESSING.
* ----------
*   Look-up the  "Surrender Value Penalty (SVP)"  factor table  T6656,
*   keyed on  surrender charge  method plus  component premium status.
*   We use the 'Nth' factor on  table T6656  which has  50 occurences.
*   The occurence we use is the age of policy (RCD till Surrender Eff.
*   Date). The  factor obtained  is then divided  by the  value in the
*   divisor field to give a calculated factor. This new factor is used
*   in the charge/penalty calculations.
*
*   Surrender charge calculation is based on sum of sub a/c balance of
*   ACBL where the sacscode and sacstype is obtain from T5645.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Unlsc03 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLSC03";
	private PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(18, 5);

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPart = new Validator(wsaaPlanSwitch, 3);
	private PackedDecimalData ix = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSubAcBal = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(13, 9);
	private PackedDecimalData wsaaSvpFactor = new PackedDecimalData(14, 9);
	private ZonedDecimalData wsaaSrYears = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanSuffixx = new FixedLengthStringData(4).isAPartOf(wsaaPlanSuffix, 0, REDEFINE);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	private FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaNoT6656Entry = new FixedLengthStringData(1);
	private Validator noT6656Entry = new Validator(wsaaNoT6656Entry, "Y");

	private FixedLengthStringData wsaaT6656Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6656Meth = new FixedLengthStringData(4).isAPartOf(wsaaT6656Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(4).isAPartOf(wsaaT6656Key, 4);
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5551 = "T5551";
	private static final String t6656 = "T6656";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5645rec t5645rec = new T5645rec();
	private T5551rec t5551rec = new T5551rec();
	private T6656rec t6656rec = new T6656rec();
	private Srcalcpy srcalcpy = new Srcalcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT5551400
	}

	public Unlsc03() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialzation100();
		surrChargeCalc200();
		/*EXIT*/
		exitProgram();
	}

protected void initialzation100()
	{
		/*INIT*/
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaPlanSwitch.set(srcalcpy.type);
		wsaaEstimateTotAmt.set(srcalcpy.estimatedVal);
		wsaaSvpFactor.set(ZERO);
		wsaaSrYears.set(ZERO);
		/*EXIT*/
	}

protected void surrChargeCalc200()
	{
			para200();
		}

protected void para200()
	{
		sumSubAcBal300();
		if (isEQ(wsaaSubAcBal,ZERO)) {
			srcalcpy.actualVal.set(ZERO);
			return ;
		}
		readTableT5551400();
		readTableT6656500();
		if (noT6656Entry.isTrue()) {
			srcalcpy.actualVal.set(ZERO);
			return ;
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaSrYears.set(datcon3rec.freqFactor);
		wsaaSrYears.add(1);
		if (isLT(wsaaSrYears,1)) {
			wsaaSrYears.set(1);
		}
		/* IF  WSAA-SR-YEARS           > 50                             */
		if (isGT(wsaaSrYears, 49)) {
			wsaaSrYears.set(50);
		}
		wsaaSvpFactor.set(t6656rec.svpFactor[wsaaSrYears.toInt()]);
		compute(wsaaFactor, 9).set(div(wsaaSvpFactor,t6656rec.divisor));
		compute(srcalcpy.actualVal, 10).setRounded(mult(wsaaSubAcBal,wsaaFactor));
		//ILIFE-6578 starts
		/*if (isLT(wsaaEstimateTotAmt,srcalcpy.actualVal)) {
			//srcalcpy.actualVal.set(wsaaEstimateTotAmt);   
		}*/  //ILIFE-6578   ends  
	}

protected void sumSubAcBal300()
	{
		/*PARA*/
		wsaaSubAcBal.set(ZERO);
		readTableT5645310();
		for (ix.set(1); !(isGT(ix,15)
		|| isEQ(t5645rec.sacscode[ix.toInt()],SPACES)); ix.add(1)){
			accumSubAcBal320();
		}
		/*EXIT*/
	}

protected void readTableT5645310()
	{
		readT5645311();
	}

protected void readT5645311()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		else {
			t5645rec.t5645Rec.set(SPACES);
		}
	}

protected void accumSubAcBal320()
	{
			para320();
		}

protected void para320()
	{
		acblIO.setDataKey(SPACES);
		acblIO.setRldgcoy(srcalcpy.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode[ix.toInt()]);
		acblIO.setOrigcurr(srcalcpy.chdrCurr);
		acblIO.setSacstyp(t5645rec.sacstype[ix.toInt()]);
		if (isEQ(acblIO.getSacscode(),"LE")) {
			wsaaRldgChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaRldgLife.set(srcalcpy.lifeLife);
			wsaaRldgCoverage.set(srcalcpy.covrCoverage);
			wsaaRldgRider.set(srcalcpy.covrRider);
			wsaaPlanSuffix.set(srcalcpy.planSuffix);
			wsaaRldgPlnsfx.set(wsaaPlnsfx);
			acblIO.setRldgacct(wsaaRldgacct);
		}
		else {
			acblIO.setRldgacct(srcalcpy.chdrChdrnum);
		}
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		if (isEQ(t5645rec.sign[ix.toInt()],"-")) {
			setPrecision(acblIO.getSacscurbal(), 2);
			acblIO.setSacscurbal(sub(0,acblIO.getSacscurbal()));
		}
		wsaaSubAcBal.add(acblIO.getSacscurbal());
	}

protected void readTableT5551400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initLang400();
				case readT5551400: 
					readT5551400();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initLang400()
	{
		wsaaT5551Key.set(srcalcpy.language);
	}

protected void readT5551400()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5551);
		wsaaT5551Crtable.set(srcalcpy.crtable);
		wsaaT5551Currcode.set(srcalcpy.currcode);
		itdmIO.setItemitem(wsaaT5551Item);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemitem(),wsaaT5551Item)
		&& isEQ(itdmIO.getStatuz(),varcom.oK)) {
			wsaaT5551Key.set("*");
			goTo(GotoLabel.readT5551400);
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsaaT5551Item)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT6656500()
	{
		para500();
	}

protected void para500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6656);
		wsaaT6656Meth.set(t5551rec.svcmeth);
		wsaaPremStat.set(srcalcpy.pstatcode);
		itdmIO.setItemitem(wsaaT6656Key);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6656)
		|| isNE(itdmIO.getItemitem(),wsaaT6656Key)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			wsaaNoT6656Entry.set("Y");
		}
		else {
			wsaaNoT6656Entry.set(SPACES);
			t6656rec.t6656Rec.set(itdmIO.getGenarea());
		}
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}


}


