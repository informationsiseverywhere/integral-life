/*
 * File: P6326.java
 * Date: 30 August 2009 0:43:25
 * Author: Quipoz Limited
 * 
 * Class transformed from P6326.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Undactncpy;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.life.newbusiness.recordstructures.Rlrdtrmrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.underwriting.dataaccess.UndcTableDAM;
import com.csc.life.underwriting.tablestructures.T6768rec;
import com.csc.life.underwriting.tablestructures.Tr675rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S6326ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Centre;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;

import com.csc.smart400framework.dataaccess.dao.ItemDAO;

import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.dao.ClntpfDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Clntpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import java.math.BigDecimal;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* UNIT LINKED GENERIC COMPONENT - PROPOSAL
*
* Overview
* --------
*
* The program  will  be able to determine if this is a single premium
* or  regular  premium component by checking the Billing Frequency on
* the  Contract  Header. For single premium products this will always
* be '00'  and  for  regular  premium  products  this  will always be
* greater than '00'.
*
* This  program  will  not create nor maintain Coverage/Rider records
* directly  on   to  the  COVR  data  set.  Instead  it  will  create
* Transaction record(s)  which will have a similar format to the COVR
* records and  these  will  be  converted to COVR records at Contract
* Issue.
*
* Plan Processing  -  this  will  be  applicable  if  the  'Number of
* Policies in Plan' field on  the  Contract  Header  is non zero. The
* essence of Plan Processing  as it affects this particular component
* is as follows:
*
*  When a Coverage/Rider is selected it  applies  to all policies
*  within  the Plan, however, certain of  the  variables  on  the
*  Coverage/Rider  screen  may  be defined  as  pertaining  to  a
*  sub-set of the policies and after these  have been entered the
*  system will then prompt the user to  enter the values required
*  for the other policies within the plan.
*
*  For  Unit  Linked  components,  which this program deals with,
*  there is also a need  to  identify Fund Direction data for the
*  coverage  details  defined   within  this  program.  The  next
*  program on the 'Stack' will  be  the  one  that  handles  Fund
*  Direction details.  This  will  follow  this  program whenever
*  'Enter' is pressed and all  details  are valid on this screen.
*  Control will return  to  this  program  after  Fund  Direction
*  details  have been  handled  and  the  Coverage/Rider  details
*  previously defined will be re-displayed.
*
*  Once  the  Fund  Direction  details have been defined for this
*  particular  set of Coverage/Rider details the user may use the
*  Roll   keys   to   move   forward   or  backward  to  a  fresh
*  Coverage/Rider  screen in order to change the fields for which
*  variations  are  allowed.  This  will be done by re-displaying
*  screen S6326 with certain of the fields protected.
*
*  When   on   the  'First'  screen,  ie.  processing  the  first
*  transaction  record  pertaining  to the first definition, then
*  all the relevant enterable fields will be input capable.
*
*  For each group of Coverage/Rider variations defined a separate
*  transaction  record  will  be  created.   The  program  should
*  maintain  a count when processing these  records  so  that  it
*  always  knows  when it is  processing  the  first  transaction
*  record.
*
*  Each transaction record will have a field  that  shows  to how
*  many policies within the Plan it applies.  It  will  also hold
*  the total number of policies within the Plan.
*
*  Allowable   Variations   -   when  processing  the  second  or
*  subsequent  set of definitions these are the only fields which
*  the user  may alter, the rest will be defaulted from the first
*  screen and will all be protected:
*
*       Premium Cessation Age
*
*       Premium Cessation Term
*
*       Maturity Age
*
*       Maturity term
*
*       Lien Code
*
*       Premium
*
*       Number Applicable.
*
* The user will be  able  to  select  Options/Extras and Reassurance
* on the screen.
*
* Initialise
* ----------
*
* Skip this section if  returning  from  processing Fund Direction,
* Options/Extras or Reassurance (current stack position action
*   flag = '*').
*
* The  details  of the contract being processed will have been stored
* in  the  CHDRLNB  I/O module.  Use the RETRV function to obtain the
* contract  header  details. This will contain a field indicating the
* total  number  of policies in the plan. Use this information to set
* up  the  'Total  Policies  in Plan' field on the screen. If this is
* zero  then  Plan  Processing  is  not applicable so non-display and
* protect  the  prompts  and the fields for 'Total Policies in Plan',
* 'Number Available' and 'Number Applicable'.
*
* The  key  of  the  Coverage/Rider  being  processed  will have been
* validated by a previous program using the logical view COVTLNB.
*
* 1.  Perform  a RETRV on COVTLNB. This will provide a key of Company
* (1  byte),  Contract  (8 bytes), Life (2 bytes), Coverage (2 bytes)
* and  Rider  (2 bytes). If the Effective Date is zero then this is a
* new Coverage/Rider and transaction record(s) have yet to be created
* otherwise the existing data should be displayed so use the same key
* to  perform  a  READS on COVTUNL and set the current details on the
* screen.
*
* 2.  Obtain the main Life details by using the Company, Contract and
* Life  returned from COVTLNB plus a Joint Life of '00' to do a READR
* on LIFELNB.
*
* 3.    Use    the   Client   Number   from   the   LIFELNB   record,
* (LIFELNB-LIFCNUM),  to  do  a READR on CLTS to obtain the main life
* details.
*
* 4. Use the same LIFELNB key except with a Joint Life set to '01' to
* READR  on LIFELNB again in order to retrieve the Joint Life details
* if they exist.
*
* 5. If found then use the Client number on that record to obtain the
* Joint Life details for display as at 3 above.
*
* The  clients' names that will appear as 'confirmation' descriptions
* on  the  screen  will be formatted by a standard routine which will
* appear in the program in the form of a copybook. This has yet to be
* defined so just perform a dummy section until it has been created.
*
* The Premium Calculation subroutine to be called will vary depending
* on the Premium Calculation field on T5687. If this is blank then no
* premium  calculation  is required so do not call any subroutine. If
* there  is an entry in this field then use this in the key to access
* T5675.  Use  the Premium Calculation Subroutine name from the extra
* data screen on T5675 as the subroutine to call when calculating the
* premium.
*
* The 'Total Policies in Plan' is taken from  the Contract Header and
* is protected. The 'Number Available' is  calculated  by the program
* as the 'Total Policies in Plan' minus  the  'Number Applicable' and
* is protected. The 'Number Applicable' is entered  by  the  user  to
* indicate  to  how  many policies within the  Plan  he  wishes  this
* definition  to  apply.  It is defaulted by the  system  to  be  the
* 'Number Available' unless the program is  displaying  data  from an
* existing transaction record when it will be  taken from there. Note
* that if the user has decreased the 'Total  Policies in Plan' on the
* Contract Header screen the number from  the  transaction record may
* be invalid as it may be too great. This will be displayed as it  is
* from the transaction number record to act as a warning to the user
* that it must be reduced  or  alternatively  'Roll' should be used to
* reduce the number applicable on a previous or subsequent screen.
*
* It will  be necessary to work out now what the "Credit" is in terms
* of how  many policies within the Plan are available for definition.
* The 'Number  of Policies Within Plan' from the Contract Header will
* give the  total  number  available  and if there are no transaction
* records as  yet then the "Credit" is this sum, otherwise it is this
* sum minus the previous 'Number of Policies  Within  Plan'  from the
* first transaction record. Note that this may  be  a negative figure
* as the user may have reduced the number of policies within the Plan
* on the Contract Header screen. The system should  not return to the
* previous program if the "Credit" is non-zero.
*
* Obtain  the   general   Coverage/Rider  details  including  Premium
* Calculation method details from  T5687 using ITEMIO with a function
* of  READS.   The  key   is  SMTP-ITEM,  Company,  'T5687'  and  the
* Coverage/Rider code.  This  replaces  the  call  to  the subroutine
* GETMETH.
*
* Obtain the long description  from DESC using DESCIO with a function
* of READS. (See the call to GETEDSC within GETMETH for details.
*
* Obtain the Unit  Linked  Edit  rules  from T5551 using ITEMIO. This
* table will be accessed in the following way:
*
*    Use a key of SMTP-ITEM, Company, 'T5671', Transaction Code and
*    Coverage/Rider Code  to read T5671 - Generic Component Control
*    Table.
*
*    This will give a  list  of up to 4 programs with associated AT
*    module names  and  Validation  Item  Codes.  Match the current
*    program name  against  this  list  and  select  the associated
*    Validation Item Code.
*
*    Use  this  to  construct a key of SMTP-ITEM, Company, 'T5551',
*    Validation  Item  Code  and  the  Currency  Code to read T5551
*    directly using ITEMIO and a function of READS.
*
* Attempt  to read the  Options/extras   transaction  file  for  this
* Coverage/Rider. If a record is  found  then  place  a  '+'  in  the
* 'Options/Extras' indicator to indicate that there are some.
*
*
* Check WSSP-FLAG.  If  'I'(enquiry  mode),  set indicator to protect
* all input capable fields  except  the  indicator  field  for Options
* & Extras.
*
* Check the  Maximum and Minimum values for the Sum Insured on T5551.
* If both  maximum  and  minimum  values  are  zero  then no entry is
* allowed so  protect and non-display the field and its prompt on the
* screen.
*
* Check the  first  entry on T5551 for the 'From' and 'To' values for
* both Premium  Term and Maturity Term. If the 'From' and 'To' values
* are the  same for either Premium Term or Maturity Term then protect
* the  corresponding 'Age' and Date fields on the screen, display the
* value of  the  term in the Term field and calculate and display the
* corresponding Cessation Date.
*
*
* Validation
* ----------
*
* Skip this section if  returning  from  processing Fund Direction,
* Options/Extras or Reassurance. ( current stack postion action
*  flag = '*').
*
* If in enquiry mode, skip  all  field validation except that for the
* Options/Extras & Reassurance indicators.
*
* Read the screen using 'S6326IO'.
*
* The 'KILL'  function is only  allowable  if the "Credit" is zero or
* the  "Credit"  = the 'Number of Policies in Plan' from the Contract
* Header.
*
* If the 'KILL' function was requested skip all validation.
*
* Validate the screen according  to  the  rules  defined by the field
* help.  For all fields  which have descriptions, look them up again.
* For the calculation of  instalment  premium  to work all the fields
* must first be validated  and be correct.
*
*    Check  the  consistency  of  Maturity  Age,  Term and Date and
*    Premium Age,  Term and Date. If the date has been entered then
*    this  takes priority and the other fields should not have been
*    entered,  (unless the term was calculated and protected in the
*    Initialise  section).  If  the  date has been entered then the
*    term should be calculated from the date.
*
*    If the age and term fields are logically consistent then check
*    that there  are  entries  for  them  on  T5551.  If a term was
*    entered then  check  that  T5551  has  an entry for a range of
*    years against  the  term.  If it does not then inform the user
*    that  term  entry is not allowed. Otherwise if age was entered
*    make  a  similar  check  for  age  with  a corresponding error
*    message if no entries are found on the table.
*
*    If there is a valid range entry then check that the entered or
*    calculated  values  are within the correct ranges as described
*    on T5551.  The  ranges  on T5551 are held as integer years and
*    should  be  moved  to work fields that allow for years, months
*    and   days  for  the  comparison  with  what  was  entered  or
*    calculated  on  the  screen.  The screen fields should also be
*    held  for comparison purposes as year, month and day fields to
*    allow  for  the  case where the user enters a termination date
*    and the term is calculated as a non integer number of years.
*
*    Check that the life's age at Maturity cessation and at Premium
*    cessation fall within the limits as specified on T5551.
*
* Validate  the Sum Insured against the Maximum and Minimum values on
* T5551. If  both  maximum  and minimum values are zero then no entry
* will have  been  made so set the data-set field to zero. Ortherwise
* check  that  the entered or calculated value falls within the range
* on T5551.
*
*
* Validation for both types of component:
*
*    If the Lien Code is entered  check  that it is one of the Lien
*    codes specified in the Edit Rules.
*
*    Calculate the Maturity and Premium cessation dates.
*
*    If the Reserve  Units option is 'N' then the Reserve Date must
*    not be entered.  If  the  Reserve Units Option is 'Y' then the
*    Reserve Units Date must be entered.
*
* PREMIUM CALCULATION.
*
* The premium amount is  required  on all products and all validating
* must be successfully completed before it is calculated. If there is
* no premium method defined  (i.e  the  relevant code was blank), the
* premium amount must  be  entered.  Otherwise,  it  is  optional and
* always calculated. The premium  may  be  calculated  from  the  sum
* assured, OR the sum assured from the premium.
*
* To calculate, call  the  relevant calculation subroutine worked out
* above passing:
*
*       - 'CALC' for the function
*       - Coverage code
*       - Contract number
*       - Life number
*       - Joint life number  (only  if it is a rider on one
*         life only, otherwise pass '00')
*       - Coverage number
*       - Rider number ('00' for coverages)
*       - Effective date (from the contract header)
*       - Termination date (i.e. term cessation date)
*       - Sum assured
*       - Calculation "Basis"
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium guarantee period (from T5687)
*       - Premium amount ("instalment")
*       - Return Status
*       - Original commencement date (from header)
*
*  Subroutine may look up:
*       - Life and Joint-life details
*       - Options/Extras
*
* On return from the  premium  calculation subroutine there may be an
* error message associated with  the  premium  itself. If so use this
* error message and highlight the premium field.
*
* The 'Number Applicable' field  may be increased to greater than the
* 'Number Available' because we  must  allow the user to set a figure
* for one set of definitions  on  this  screen  knowing that the user
* will reduce the figure on another screen afterwards.
*
* The 'Charge Options' indicator may  already have been set to '+' if
* charge options exist. The  user  may  request  to access the charge
* options by entering 'X'.  No  other  values  other  than  blank are
* allowed.
*
* If 'CALC' was pressed re-display the screen.
*
*  Updating
*  --------
*
* Skip this section if  returning  from  processing Fund Direction or
* Charge Options (current stack position action flag = '*').
*
* If the 'KILL' function key was pressed or if in enquiry mode, or if
* no changes were made, skip the updating.
*
* Either add, rewrite or delete the COVTUNL  record  as  required. If
* adding a new record initialise the fields. If the user has  reduced
* the 'Number Applicable' to zero, the record should be  deleted.  If
* this  happens the program should ensure that  it  takes  this  into
* account  when maintaining its count of the transaction records that
* it has processed.
*
* If the "Credit" is less than  zero  and  the user has accounted for
* all the policies as specified in the Contract Header then there may
* be extra transaction records that  are now redundant. As the system
* will  know  that  the  "Credit" is not zero, i.e. more policies are
* accounted  for than actually exist on the Contract Header, then the
* user  will  not  be allowed to exit this component. Instead he will
* have to roll forward to the definitions that are no longer required
* and reduce their 'Number Applicable' figures to zero.
*
* Next Program
* ------------
*
* The  first  thing  to  consider is the handling of an Option/Extras
* request.  If  the  indicator  is 'X', a request to visit the Option
* & Extras screen has been made. In this case:
*
*       - change the options/extras request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSW to retrieve the program switching required,
*       and  move  them to the stack.
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
* On return from this request, the current stack "action" will be '*'
* and the options/extras indicator will be '?'.  To handle the return
* from options and extras:
*
*       - re-set the 'Options/Extras indicator to '+',
*
*       - restore the saved programs to the program stack.
*
*       - set  WSSP-NEXTPROG to  the current screen  name  (thus
*          returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*       - change the Reassurance request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSWCH with and  action  of 'B' to retrieve the
*           program switching required,  and  move  them to the
*           stack,
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*       - blank  out  the  stack  "action",  restore  the  saved
*          programs to the program stack,
*
*       - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* The  following processing for the 4000 section will be standard for
* all scrolling generic component processing programs.
*
* If control is passed  to  the  4000  section  on the way out of the
* program, ie.  after  screen  I/O,  then  the current stack position
* action flag will  be blank.  If the 4000 section is being performed
* after returning  from  processing  another program further down the
* stack then the current stack position action flag will be '*'.
*
* Processing on the way out:
*
*    If one of  the  Roll  keys  has  been pressed and any RELEVANT
*    field has  been changed, (in this case the 'Number Applicable'
*    or the 'Premium'), set the current select action field to '*',
*    add 1 to the program pointer and exit.
*
*    If one of the Roll keys has been pressed and no relevant field
*    has   been  changed,  (ie.  neither  'Number  Applicable'  nor
*    'Premium'), then loop round within the program and display the
*    next/previous screen as requested.
*
*    If 'Enter' has been pressed  and  "Credit" is non-zero set the
*    current select action field to  '*',  add  1  to  the  program
*    pointer and exit.
*
*    If 'Enter' has been pressed and  "Credit" is zero add 1 to the
*    program pointer and exit.
*
*    If 'KILL' has been requested,  (CF11), then move spaces to the
*    current program entry in the program stack.
*
*  Processing on the way in:
*
*    Remove the  asterisk  from  the  current stack position action
*    flag.
*
*    If one of the Roll  keys  has  been  pressed  then  loop round
*    within the program and display  the next or previous screen as
*    requested.
*
*    If 'Enter' has been pressed  and "Credit" has a positive value
*    then loop round within the program and display the next screen
*    for input.
*
*    If 'Enter' has been pressed  and "Credit" has a negative value
*    then loop round within  the  program, display the details from
*    the  first   transaction   record,   highlight   the   'Number
*    Applicable' and give  a  message indicating that more policies
*    have been defined than are on the Contract Header.
*
*
*
*  Modifications
*  -------------
*
*
* Create  a   new  Physical  File  -  COVTPF,  Coverage  Transactions
* file,(based on COVRPF) and a Logical  View  - COVTUNL with which to
* access it in this subsystem. It will contain the existing Coverage/
* Rider details plus a field indicating the number of policies within
* the  Plan to which this definition applies and a field showing  how
* many  policies  there  are  within  the  Plan.  The key is Company,
* Contract Number, Life, Coverage and Rider and is non-unique.
*
* Add the following fields to the screen:
*
*    Total Policies in Plan, (4 characters, protected)
*
*    Number Available,       (4 characters, protected)
*
*    Number Applicable,      (4 characters, unprotected)
*
*    Change 'Single Premium'/'Instalment' prompt to read 'Premium'.
*
* Remove the following fields from the screen:
*
*    Remove the 'Action' field.
*
*    "Unit  Link  Single  Premium  Investment"  title (replace with
*    General  Coverage/Rider  Details  long description from T5687,
*  centred and underlined - see P0017 for an example.
*
* The field 'Charge Options' will  now  be moved to the bottom of the
* screen and its valid values will now be blank or '+'. The windowing
* on this field will now  be  removed and if the user requests access
* to the Charge Options screen he  will place a '+' in this field and
* the program will  use  generalised  secondary switching to transfer
* control to the charge options function. Control will be returned to
* this program after the charge options program has completed.
*
* Change tables used to dated tables.  The  date  to  use will be the
* Original Contract Commencement  Date.  This  was  originally  to be
* found in WSSP-OCCDATE but  will  now  be obtained directly from the
* Contract Header record after a RETRV has been performed against it.
*
* Remove the read  of  table T5540, the data required by this program
* is now on T5687.
*
*  Call ITEMIO directly and not through READITM.
*
* Drop the cursor sensitive CALC checking.  Do the look-ups anyway.
*
* Do not call DEFFUPS for default Follow-ups as these will now be set
* up within the Life  New  Business system, therefore also remove all
* validation of follow-ups.
*
* Table  T6332  is now redundant. Take the minimum and maximum fields
* from this  extra  data screen and add them to the extra data screen
* of  T5551.  This  table,  (T5551),  will now be used by both single
* premium and regular premium components. The Sum Insured Factor from
* T6332  will  no  longer  be  used,  instead the Premium Calculation
* method  will work out the Sum Insured for Single Premium components
* and return the value to this program.
*
* Drop  all   validation   of   S5414-STATFUND,  S5414-STAT-SECT  and
* S5414-STAT-SUBSECT as they are  output only fields and are obtained
* from T5687.
*
* Put in stacked based program switching.
*
* Take the validation and processing for Single Premium products from
* P6326  and  for Regular Premium products from P5414. This basically
* has no  change  as far as validation and field values are concerned
* apart from  what  has been specified here to account for Plan based
* processing, variations and the new program switching techniques.
*
* Create the LIFELNB  physical file and I/O module. This will contain
* all the fields  on  LIFEPF  and  will be keyed on Company, Contract
* Number, Life Number  and  Joint  Life Number and will selected on a
* Valid Flag of '3' only.
*
*
* Examples
* --------
*
* Let us assume that we  have  a plan of 10 policies. The user wishes
* to have three variations:
*
*                              1st    2nd     3rd
*     TOTAL IN PLAN      10 |  10  |  10   |  10   |
*     NUMBER AVAILABLE   10 |  10  |   5   |   2   |
*     NUMBER APPLICABLE  10 |   5  |   3   |   2   |
*
* If on the first entry the 'Number Applicable' is reduced to 5, so a
* COVTUNL record will be  written  with the details captured from off
* the screen and the 'Number Applicable' COVTUNL field set to 5.  The
* screen will be  presented  again  with the static fields protected.
* The 'Number Available' and the 'Number Applicable' will both be 5.
*
* On the second entry,  the  variable  fields  are  changed  and  the
* 'Number Applicable' field  is  reduced  to  3.  So, another COVTUNL
* record is written  and  the  screen re-presented, this time showing
* the 'Number Available' as 2 and the 'Number Applicable' as 2.
*
* On the third, and  in  this  case  the  final  entry,  the  'Number
* Applicable' is left as  2  so  another  a  COVTUNL  record  will be
* written.  As there are  no  further  policies  to  account  for, if
* 'Enter' was pressed at this point, processing can continue.
*
* In this example only three COVTUNL records will be written:-
*
*      COVTUNL RECORDS:-
*
*                         Number       No. of      other
*                       Applicable    Policies     coverage
*                                     in Plan      details
*
*                     -------------------------------------------
*                     |            |          |
*                     |    5       |   10     |  ---------------->
*                     |            |          |
*                     -------------------------------------------
*
*                     -------------------------------------------
*                     |            |          |
*                     |    3       |   10     |  ---------------->
*                     |            |          |
*                     -------------------------------------------
*
*                     -------------------------------------------
*                     |            |          |
*                     |    2       |   10     |  ---------------->
*                     |            |          |
*                     -------------------------------------------
*
* Let us take another example where COVTUNL records already exist and
* there has been no change in the number of Policies in the Plan:-
*
*      If the 'Roll' keys are  used  and  there  is  no change to the
*      'Number  Applicable'  field  the  next/previous  record  (same
*      screen) can be shown. (If no details on the screen are changed
*      at all, then  the  transaction  record  need  not  actually be
*      updated.)
*
*      If the 'Number  Applicable' field is altered, then the COVTUNL
*      record must be re-written  or  deleted.  Even if the roll keys
*      are pressed, processing  must  continue to the next applicable
*      program for the  Unit Linked Coverage/Rider (which in the case
*      is Fund Direction).  If  'Enter'  is pressed, the "CREDIT" (as
*      defined above) will be checked  and  if it is equal to zero or
*      the 'Number Applicable'  equals  the number of policies in the
*      Plan, then it is  OK  to  finish.  Otherwise, if there are not
*      enough  policies   accounted   for   ("Credit"  is  positive),
*      additional ones must  be entered (blank screen displayed).  If
*      there are too many ("Credit" negative), details from the first
*      transaction  record  must  be  re-displayed  highlighting  the
*      error.
*
* Finally, there is the  case where COVTUNL records already exist and
* there has been a change in the number of Policies in the Plan:-
*
*      When displaying the  screens,  show the number of policies now
*      in the Plan and also  set  the number available to this on the
*      first screen.  (Subsequent screens have their number available
*      calculated from the  number  in the plan, less those accounted
*      for.)
*
*      Processing is now the  same  as  the case above. The user must
*      either change the number applicable on at least one record, or
*      add another set of details to satisfy the validation.
*
*
*****************************************************************
* </pre>
*/
public class P6326 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6326");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private final int wsaaMaxOcc = 8;

	private FixedLengthStringData wsaaCtrltime = new FixedLengthStringData(1);
	private Validator firsttime = new Validator(wsaaCtrltime, "Y");
	private Validator nonfirst = new Validator(wsaaCtrltime, "N");
	private String premReqd = "N";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCcy = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private String wsaaT5551Error = "N";

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator nonplan = new Validator(wsaaPlanproc, "N");
	private Validator plan = new Validator(wsaaPlanproc, "Y");
	private PackedDecimalData wsaaNumavail = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaWorkCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaLumpSum = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaUndwrule = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(5).init(SPACES);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaT6768Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6768Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6768Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT6768Curr = new FixedLengthStringData(3).isAPartOf(wsaaT6768Key, 4).init(SPACES);
	private FixedLengthStringData wsaaT6768Sex = new FixedLengthStringData(1).isAPartOf(wsaaT6768Key, 7).init(SPACES);

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaT6768Found = new FixedLengthStringData(1);
	private Validator t6768Found = new Validator(wsaaT6768Found, "Y");
	private Validator t6768NotFound = new Validator(wsaaT6768Found, "N");

	private FixedLengthStringData wsaaYaFlag = new FixedLengthStringData(1);
	private Validator yaFound = new Validator(wsaaYaFlag, "Y");

	private FixedLengthStringData wsaaXaFlag = new FixedLengthStringData(1);
	private Validator xaFound = new Validator(wsaaXaFlag, "Y");
	private ZonedDecimalData wsddMatage = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddMattrm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAnbAtCcd2 = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaSex2 = new FixedLengthStringData(1).init(SPACES);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzMatage = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzMattrm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData storeSeqnbr = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData subname = new FixedLengthStringData(10).init(SPACES);
	private ZonedDecimalData premiumCalcInd = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub3 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private FixedLengthStringData totPolsInPlan = new FixedLengthStringData(4);
	private ZonedDecimalData singPremInd = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private ZonedDecimalData regPremInd = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private ZonedDecimalData transactionCount = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData n6326PrevNumapp = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaUnderwritingReqd = new FixedLengthStringData(1).init("N");
	private Validator underwritingReqd = new Validator(wsaaUnderwritingReqd, "Y");
	private ZonedDecimalData wsaaAge00Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAge01Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAdjustedAge = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUndwAge = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaXa = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaYa = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZa = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaSub00 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub01 = new PackedDecimalData(3, 0).init(0);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(22, 5);

	private FixedLengthStringData wsaaCentreString = new FixedLengthStringData(80);
	private FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(wsaaCentreString, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCentreHeading = new FixedLengthStringData(32).isAPartOf(wsaaCentreString, 24);
	private FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(wsaaCentreString, 56, FILLER).init(SPACES);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler5, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler7, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private ZonedDecimalData sub4 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData billFrequency = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaS6326Mattrm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaS6326PremCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaS6326Matage = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaS6326PremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaAnbatccd = new ZonedDecimalData(3, 0).setUnsigned();
	private static final int wsaaMaxMort = 6;

	private FixedLengthStringData wsaaError1Flag = new FixedLengthStringData(1);
	private Validator wsaaError1 = new Validator(wsaaError1Flag, "Y");

	private FixedLengthStringData wsaaError2Flag = new FixedLengthStringData(1);
	private Validator wsaaError2 = new Validator(wsaaError2Flag, "Y");

	private FixedLengthStringData wsaaError3Flag = new FixedLengthStringData(1);
	private Validator wsaaError3 = new Validator(wsaaError3Flag, "Y");

	private FixedLengthStringData wsaaFoundFlag = new FixedLengthStringData(1);
	private Validator wsaaFound = new Validator(wsaaFoundFlag, "Y");

		/*01  WSBB-UL01-TYPES.
		                                OCCURS 17.*/
	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsaaCovtlnbParams = new FixedLengthStringData(219);
	private FixedLengthStringData wsaaCovtlnbDataKey = new FixedLengthStringData(64).isAPartOf(wsaaCovtlnbParams, 49);
	private FixedLengthStringData wsaaCovtlnbCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovtlnbDataKey, 11);

	private FixedLengthStringData wsaaKillFlag = new FixedLengthStringData(1);
	private Validator forcedKill = new Validator(wsaaKillFlag, "Y");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private UndcTableDAM undcIO = new UndcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T2240rec t2240rec = new T2240rec();
	private T5551rec t5551rec = new T5551rec();
	private T5585rec t5585rec = new T5585rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T6768rec t6768rec = new T6768rec();
	private Tr675rec tr675rec = new Tr675rec();
	private Gensswrec gensswrec = new Gensswrec();
	private Freqcpy freqcpy = new Freqcpy();
	private T5687rec t5687rec = new T5687rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Premiumrec premiumrec = new Premiumrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Undactncpy undactncpy = new Undactncpy();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S6326ScreenVars sv = ScreenProgram.getScreenVars( S6326ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
	private FixedLengthStringData wsaaZsredtrm = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZsbsmeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSubprog = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPremind = new FixedLengthStringData(1);
	private Rlrdtrmrec rlrdtrmrec = new Rlrdtrmrec();
	private T6598rec t6598rec = new T6598rec();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private PackedDecimalData wsspCurrfrom = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private Th615rec th615rec = new Th615rec();
	boolean ispermission=true;
	/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/
	private boolean loadingFlag = false;/* BRD-306 */
	private boolean liencdFlag = false; 

	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private Rlvrpf rlvrpf=null;
	private RlvrpfDAO rlvrpfDAO = getApplicationContext().getBean("rlvrpfDAO", RlvrpfDAO.class);

	//BRD-NBP-012-STARTS
	private T6625rec t6625rec = new T6625rec();
	private String GMIB="";
	private String GMDB="";
	private String GMWB="";
	//BRD-NBP-012-ENDS
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	//ILIFE-7805 - Starts
	private boolean singPremTypeFlag = false; 	
	private String singlepremiumind = "";
	private FixedLengthStringData wsaaSingPremDrpVal = new FixedLengthStringData(3);
	//ILIFE-7805 - Ends	
	//ILIFE-7845
	private boolean riskPremflag = false;
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";
	//ILIFE-7845
	/*ILIFE-7934 : Start*/
	private boolean mulProdflag = false;
	private static final String IL_PROD_SETUP_FEATURE_ID="NBPRP096";
	private static final String OIR1 = "OIR1";
	private static final String OIS1 = "OIS1";
	private static final String SIR1 = "SIR1";
	private static final String SIS1 = "SIS1";
	/*ILIFE-7934 : End*/

	private static final String ROLLOVER_FEATURE_ID="NBPRP104";
	private boolean rolloverFlag = false;
//	ILIFE-8098 Start
	private List<Zctxpf> zctxpfList = new ArrayList<Zctxpf>();
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);	/*ILIFE-8098 */
	Covtpf covtpf = null; /*ILIFE-8098 */
	List<Covtpf> covtpflist = new ArrayList<Covtpf>();
	Zctxpf zctxpf=null;
	private ClntpfDAO clntDao;	//ILIFE-8537
	private Clntpf clntpf=null; //ILIFE-8537
//	ILIFE-8098 END
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private int fupno = 0;
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	private T5661rec t5661rec = new T5661rec();
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private Ta610rec ta610rec = new Ta610rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	boolean NBPRP056Permission  = false;
	private T3644rec t3644rec = new T3644rec();
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	private static final String t5661 = "T5661";
	private static final String NBPRP056="NBPRP056";
	private List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494
	private boolean stampDutyflag = false;
	private String stateCode="";
	private com.csc.fsu.clients.dataaccess.model.Clntpf clnt;
	private com.csc.fsu.clients.dataaccess.dao.ClntpfDAO clntDAO = getApplicationContext().getBean("clntpfDAO", com.csc.fsu.clients.dataaccess.dao.ClntpfDAO.class);
	private PackedDecimalData wsaaCommissionPrem = new PackedDecimalData(17, 2).init(0).setUnsigned();   //IBPLIFE-5237
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1010, 
		cont1015, 
		cont1030, 
		loop11250, 
		loop21300, 
		matchProgName, 
		cont1890, 
		exit1900, 
		mattrm1510, 
		exit1540, 
		premCessTerm1560, 
		exit1590, 
		nextColumn1820, 
		moveDefaults1850, 
		redisplay2480, 
		exit2090, 
		checkLumpSum2527, 
		checkMaturityFields2530, 
		datesCont2541, 
		ageAnniversary2541a, 
		check2542, 
		termExact2542, 
		check2543, 
		checkOccurance2545, 
		checkTermFields2550, 
		checkComplete2555, 
		checkMortcls2560, 
		loop2565, 
		checkLiencd2570, 
		loop2575, 
		checkMore2580, 
		exit2590, 
		calc2710, 
		calc2720, 
		exit2790, 
		adjust2a00, 
		adjust2b00, 
		go2c00, 
		loop5615, 
		premCessTerm5650, 
		loop5652, 
		exit5699, 
		loop5715, 
		premCessAge5750, 
		loop5752, 
		exit5799, 
		keep3400, 
		exit3490, 
		cont3555, 
		cont4710, 
		cont4715, 
		cont4717, 
		cont4720, 
		exit4790, 
		rolu4805, 
		cont4810, 
		cont4820, 
		readCovtunl4830, 
		cont4835, 
		cont4837, 
		cont4840, 
		exit4890, 
		gotItem5000, 
		taxCalc5000
	}

	public P6326() {
		super();
		screenVars = sv;
		new ScreenModel("S6326", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			//TMLII-268 Starts
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268 Ends
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1100();
				case cont1010: 
					cont1010();
					plan1010();
				case cont1015: 
					cont1015();
					cont1017();
					premmeth1020();
				case cont1030: 
					cont1030();
				case loop11250: 
					loop11250();
				case loop21300: 
					loop21300();
				case matchProgName: 
					matchProgName();
					cont1060();
				case cont1890: 
					cont1890();
				case exit1900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
	    wsspcomn.tranno.set("1");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
/*Commented MIBT-130 changes for ILIFE-749 defect
			//MIBT-130 STARTS
			if (isNE(wsspcomn.flag, "I")) {  //MTL002
				 sv.singlePremium.set(0);		//MTL002
				 calcPremium2700();			//MTL002
				 sv.singprmErr.set(" ");    //MTL002
			}
			//MIBT-130 ENDS
*/
			//BRD-NBP-012-STARTS
			if (isEQ(sv.optind, "?")){
				covtlnbIO.setFunction("RETRV");
				SmartFileCode.execute(appVars, covtlnbIO);
				if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(covtlnbIO.getParams());
					fatalError600();
				}				
				GMIB = covtlnbIO.getGmib().toString();
				GMDB = covtlnbIO.getGmdb().toString();
				GMWB = covtlnbIO.getGmwb().toString();
			}
			//BRD-NBP-012-ENDS
			goTo(GotoLabel.exit1900);
		}
		sv.dataArea.set(SPACES);
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		//ILIFE-2964
		//ispermission=FeaConfg.isFeatureExist(wsspcomn.company.toString(),"BCLM01",appVars,"IT");
		if(!ispermission)
			sv.optsmodeOut[varcom.nd.toInt()].set("Y");
		//ILIFE-2964
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		undactncpy.undActions.set(wsspcomn.undAction);
		wssplife.fupno.set(ZERO);
		/* INITIALISE NUMERIC AND VARIOUS FIELDS ON THE SCREEN*/
		singPremInd.set(ZERO);
		sv.singlePremium.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*BRD-306 START */
		sv.loadper.set(ZERO);
		sv.adjustageamt.set(0);
		sv.rateadj.set(ZERO);
		sv.fltmort.set(ZERO);
		sv.premadj.set(ZERO);
		/*BRD-306 END */
		sv.sumin.set(ZERO);
		sv.numapp.set(ZERO);
		sv.numavail.set(ZERO);
		sv.polinc.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.matage.set(ZERO);
		sv.mattrm.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.lumpContrib.set(ZERO);
		/* MOVE 'N'                    TO S6326-RESERVE-UNITS-IND.      */
		wsaaFirstTaxCalc.set("Y");
		sv.reserveUnitsDate.set(varcom.vrcmMaxDate);
		sv.premcess.set(varcom.vrcmMaxDate);
		sv.mattcess.set(varcom.vrcmMaxDate);
		sv.currcd.set(wsspcomn.trancurr);
		wsaaCtrltime.set("N");
		wsaaT5551Error = "N";
		/*    Initialise WSSP field for use through linkage.*/
		wssplife.bigAmt.set(ZERO);
		wssplife.occdate.set(ZERO);
		/* Read CHDRLNB (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*BRD-306 START */
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		//sv.optindOut[varcom.nd.toInt()].set("N");//BRD-NBP-012
		/*BRD-306 END */
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			}
		}
		totPolsInPlan.set(chdrlnbIO.getPolinc());
		sv.polinc.set(chdrlnbIO.getPolinc());
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Read the PAYR record to get the Billing Details.                */
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.currcd.set(payrIO.getCntcurr());
		/*MOVE CHDRLNB-CNTCURR          TO S6326-CURRCD.               */
		/*    If the 'Number of Policies in Plan' field on the Contract*/
		/*    Header is not zeros then Plan Processing is applicable.*/
		if (isEQ(chdrlnbIO.getPolinc(), ZERO)) {
			wsaaPlanproc.set("N");
		}
		else {
			wsaaPlanproc.set("Y");
		}
		/*    If Plan Processing is applicable, protect and non-display*/
		/*    the Plan Processing fields (number available, number*/
		/*    applicable and total number of policies in plan).*/
		if (nonplan.isTrue()) {
			sv.numapp.set(1);
			sv.numavail.set(1);
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.numappOut[varcom.nd.toInt()].set("Y");
			sv.numavailOut[varcom.pr.toInt()].set("Y");
			sv.numavailOut[varcom.nd.toInt()].set("Y");
			sv.polincOut[varcom.pr.toInt()].set("Y");
			sv.polincOut[varcom.nd.toInt()].set("Y");
		}
		/*    If the billing frequency on the Contract Header is greater*/
		/*    than zero then this is a regular premium component.*/
		/*    Otherwise the component is a single premium one.*/
		/*IF CHDRLNB-BILLFREQ         = ZEROS                          */
		if (isEQ(payrIO.getBillfreq(), ZERO)) {
			singPremInd.set(1);
		}
		/*MOVE CHDRLNB-BILLFREQ       TO BILL-FREQUENCY.               */
		billFrequency.set(payrIO.getBillfreq());
		/*    Retrieve the key of the coverage/rider being processed*/
		/*    from the COVTLNB logical view.*/
		covtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getRider(), "00")) {
			goTo(GotoLabel.cont1010);
		}
		/*    If we are dealing with a coverage we should skip this part   */
		wsaaCovtlnbParams.set(covtlnbIO.getParams());
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setDataKey(wsaaCovtlnbDataKey);
		covtlnbIO.setRider(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.endp)
		&& isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(covtlnbIO.getCoverage(), wsaaCovtlnbCoverage)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			wsaaKillFlag.set("Y");
			goTo(GotoLabel.exit1900);
		}
		/* TO HAVE GOT THIS FAR MEANS A COVERAGE MUST EXIST             */
		/* FOR THE RIDER WE ARE EITHER ACCESSING OR CREATING.           */
		covtlnbIO.setParams(wsaaCovtlnbParams);

		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts */
		rlrdtrmrec.rlrdtrmRec.set(SPACES); 
		wsaaZsredtrm.set(SPACES); 
		wsaaSubprog.set(SPACES); 
		wsaaPremind.set(SPACES); 
		wsaaZsbsmeth.set(SPACES);
		/*ILIFE-2964 IL_BASE_ITEM-001-Base ends */
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
	}

protected void cont1010()
	{
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.rider.set(covtlnbIO.getRider());
		covtunlIO.setDataKey(covtlnbIO.getDataKey());
		covtunlIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		covtunlIO.setDataKey(covtlnbIO.getDataKey());
		covtunlIO.setSeqnbr(0);
		covtunlIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		if (isEQ(covtunlIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		&& isEQ(covtunlIO.getChdrnum(), covtlnbIO.getChdrnum())
		&& isEQ(covtunlIO.getLife(), covtlnbIO.getLife())
		&& isEQ(covtunlIO.getCoverage(), covtlnbIO.getCoverage())
		&& isEQ(covtunlIO.getRider(), covtlnbIO.getRider())) {
			/*NEXT_SENTENCE*/
		}
		else {
			covtunlIO.setStatuz(varcom.endp);
		}
		/*    If COVTUNL-STATUZ = ENDP then this is the first time.*/
		if (isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			covtunlIO.setNonKey(SPACES);
			covtunlIO.setReserveUnitsInd(SPACES);
			covtunlIO.setLiencd(SPACES);
			covtunlIO.setSeqnbr(ZERO);
			covtunlIO.setAnbccd(1, ZERO);
			covtunlIO.setAnbccd(2, ZERO);
			covtunlIO.setSumins(ZERO);
			covtunlIO.setPremCessDate(ZERO);
			covtunlIO.setPremCessAge(ZERO);
			covtunlIO.setPremCessTerm(ZERO);
			covtunlIO.setRiskCessDate(ZERO);
			covtunlIO.setRiskCessAge(ZERO);
			covtunlIO.setRiskCessTerm(ZERO);
			covtunlIO.setBenCessDate(ZERO);
			covtunlIO.setBenCessAge(ZERO);
			covtunlIO.setBenCessTerm(ZERO);
			covtunlIO.setPolinc(ZERO);
			covtunlIO.setReserveUnitsDate(ZERO);
			covtunlIO.setNumapp(ZERO);
			covtunlIO.setSingp(ZERO);
			covtunlIO.setInstprem(ZERO);
			covtunlIO.setZbinstprem(ZERO);
			covtunlIO.setZlinstprem(ZERO);
			covtunlIO.setcommPrem(ZERO);     //IBPLIFE-5237
			covtunlIO.setEffdate(ZERO);
			wsaaCtrltime.set("Y");
		}
		covtunlIO.setPayrseqno(1);
	}

protected void plan1010()
	{
		if (firsttime.isTrue()) {
			covtunlIO.setPolinc(ZERO);
			covtunlIO.setNumapp(ZERO);
			covtunlIO.setSeqnbr(1);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		covtunlIO.setTermid(varcom.vrcmTermid);
		covtunlIO.setUser(varcom.vrcmUser);
		covtunlIO.setTransactionDate(varcom.vrcmDate);
		covtunlIO.setTransactionTime(varcom.vrcmTime);
		covtunlIO.setCrtable(covtlnbIO.getCrtable());
		covtunlIO.setFormat(formatsInner.covtunlrec);
		covtunlIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
			goTo(GotoLabel.exit1900);
		}
		/*itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5687");
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covtlnbIO.getCrtable())
		|| isNE(itdmIO.getValidflag(), "1")) {
			syserrrec.statuz.set(errorsInner.e366);
			syserrrec.syserrType.set("2");
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());*/
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(covtlnbIO.getCrtable().toString());
		itempf.setItemtabl("T5687");
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);//ILIFE-3955
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t5687rec.t5687Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		} 

		/*ILIFE-7805 - Start*/
		singPremTypeFlag = isSinglePremTypeComp();		
		if (!singPremTypeFlag) {			
			sv.singpremtypeOut[varcom.nd.toInt()].set("Y");
		}
		/*ILIFE-7805 - End*/

		/* If  Plan  processing is to occur, then calculate the "credit"*/
		/* as follows:*/
		/* - subtract  the  'No  of  policies in the Plan' from the*/
		/*   first  COVTUNL  record  read  above (if no COVTUNL,*/
		/*       this is zero) from the 'No of policies in the Plan'*/
		/*       (from the Contract-Header).*/
		if (plan.isTrue()) {
			compute(wsaaCredit, 0).set((sub(chdrlnbIO.getPolinc(), covtunlIO.getPolinc())));
		}
		/* If this is a single policy plan*/
		/*    if it is the first time (no COVTUNL records)*/
		/*     or there is no credit*/
		/*   - plan processing is not required.*/
		if (isEQ(chdrlnbIO.getPolinc(), 1)
		&& (isEQ(covtunlIO.getPolinc(), 0)
		|| isEQ(covtunlIO.getPolinc(), 1))) {
			wsaaPlanproc.set("N");
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		/* Set  the  number of  policies  available  to  the  number  of*/
		/* policies on the plan.  If  COVTUNL  records are to be written*/
		/* for the first time  (as determined above), default the number*/
		/* applicable to the number available.*/
		wsaaFirstSeqnbr.set(covtunlIO.getSeqnbr());
		wsaaNumavail.set(chdrlnbIO.getPolinc());
		/* BRD-306 starts */
		loadingFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		liencdFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),
				"NBPRP057", appVars, "IT");
		if(liencdFlag){
			sv.liencdOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.liencdOut[varcom.nd.toInt()].set("N");
		}
		/* BRD-306 ends */
		if (firsttime.isTrue()) {
			covtunlIO.setNumapp(wsaaNumavail);
			goTo(GotoLabel.cont1015);
		}
		/* ELSE                                                         */
		sv.mattcess.set(covtunlIO.getRiskCessDate());
		sv.premcess.set(covtunlIO.getPremCessDate());
		sv.matage.set(covtunlIO.getRiskCessAge());
		sv.premCessAge.set(covtunlIO.getPremCessAge());
		sv.mattrm.set(covtunlIO.getRiskCessTerm());
		sv.premCessTerm.set(covtunlIO.getPremCessTerm());
		if (isEQ(covtunlIO.getInstprem(), 0)) {
			/*    IF PAYR-BILLFREQ = '00'                           <A05691>*/
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				sv.singlePremium.set(covtunlIO.getSingp());
				sv.zbinstprem.set(covtunlIO.getZbinstprem());
				sv.zlinstprem.set(covtunlIO.getZlinstprem());
				/*BRD-306 START */
				sv.loadper.set(covtunlIO.getLoadper());
				sv.rateadj.set(covtunlIO.getRateadj());
				sv.fltmort.set(covtunlIO.getFltmort());
				sv.premadj.set(covtunlIO.getPremadj());
				sv.adjustageamt.set(covtunlIO.getAgeadj());
				/*BRD-306 END */
				sv.lumpContrib.set(0);
			}
			else {
				sv.lumpContrib.set(covtunlIO.getSingp());
				sv.zbinstprem.set(covtunlIO.getSingp());
				sv.zlinstprem.set(covtunlIO.getSingp());
				sv.singlePremium.set(0);
			}
		}
		else {
			sv.singlePremium.set(covtunlIO.getInstprem());
			sv.zbinstprem.set(covtunlIO.getZbinstprem());
			sv.zlinstprem.set(covtunlIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtunlIO.getLoadper());
			sv.rateadj.set(covtunlIO.getRateadj());
			sv.fltmort.set(covtunlIO.fltmort);
			sv.premadj.set(covtunlIO.getPremadj());
			sv.adjustageamt.set(covtunlIO.getAgeadj());
			/*BRD-306 END */
			sv.lumpContrib.set(covtunlIO.getSingp());
		}
		sv.sumin.set(covtunlIO.getSumins());
		/*    Why was this line *'d out as it means that every time you    */
		/*    Modify a Coverage, you have to re-enter the MORT CLASS!!!!   */
		/*    MOVE COVTUNL-MORTCLS        TO S6326-MORTCLS.*/
		sv.mortcls.set(covtunlIO.getMortcls());
		sv.liencd.set(covtunlIO.getLiencd());
		sv.reserveUnitsInd.set(covtunlIO.getReserveUnitsInd());
		sv.singpremtype.set(covtunlIO.getSingpremtype());//ILIFE-7805
		if (isNE(covtunlIO.getReserveUnitsDate(), ZERO)) {
			sv.reserveUnitsDate.set(covtunlIO.getReserveUnitsDate());
		}
	}

protected void cont1015()
	{
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtunlIO.getNumapp());
	}

	/**
	* <pre>
	* To  determine  which premium calculation method to use decide   
	* whether or  not  it  is  a Single or Joint-life case (it is a   
	* joint  life  case,  the  joint  life record was found above).   
	* Then:                                                           
	*  - if it  is  a  single-life  case use, the single-method       
	*       from  T5687. The age to be used for validation will       
	*       be the age of the main life.                              
	*  - if the joint-life indicator (from T5687) is blank, and       
	*       if  it  is  a Joint-life case, use the joint-method       
	*       from  T5687. The age to be used for validation will       
	*       be the age of the main life.                              
	*  -    The  age to be used for validation will be the age        
	*       of the main or joint life, depending on which was         
	*       selected.                                                 
	* </pre>
	*/
protected void cont1017()
	{
		/* GET LIFE INSURED AND JOINT LIFE DETAILS NEEDED FOR PREMIUM*/
		/* CALCULATION AND FOR SHOWING THE LIFE NAME ON THE SCREEN.*/
		sv.chdrnum.set(covtlnbIO.getChdrnum());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		sv.life.set(covtlnbIO.getLife());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		/*    IF LIFELNB-STATUZ           NOT = O-K                        */
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.anbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaCltdob.set(lifelnbIO.getCltdob());
		wsaaSex.set(lifelnbIO.getCltsex());
		sv.lifenum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		/*ILIFE-7934 : Starts*/
		mulProdflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), IL_PROD_SETUP_FEATURE_ID, appVars, "IT");
		if(mulProdflag){
			sv.mortcls.set(lifelnbIO.getSmoking());
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*ILIFE-7934 : Ends*/
		/* MOVE LIFELNB-CHDRCOY        TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/* CHECK FOR EXISTENCE OF JOINT LIFE DETAILS.*/
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			wsaaLifeind.set("S");
		}
		else {
			wsaaLifeind.set("J");
		}
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
		else {
			sv.jlifcnum.set(lifelnbIO.getLifcnum());
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			wsaaAnbAtCcd2.set(lifelnbIO.getAnbAtCcd());
			wsaaSex2.set(lifelnbIO.getCltsex());
			/*       MOVE LIFELNB-CHDRCOY  TO CLTS-CLNTCOY                  */
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setFunction("READR");
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
			}
		}
		/* Does Underwriting apply to this Proposal?                       */
		/* Read TR675 to find out.                                         */
		/*itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr675);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr675)
		|| isNE(itdmIO.getItemitem(), chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaUnderwritingReqd.set("N");
		}
		else {
			wsaaUnderwritingReqd.set("Y");
			tr675rec.tr675Rec.set(itdmIO.getGenarea());
		}*/
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItemtabl(tablesInner.tr675.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				tr675rec.tr675Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
			wsaaUnderwritingReqd.set("Y");
		} 
		else
		{
			wsaaUnderwritingReqd.set("N");
		}
		/*    Obtain the general Coverage/Rider details (including Premium*/
		/*    Calculation Method details from T5687 using the coverage/*/
		/*    rider code as part of the key (COVTLNB-CRTABLE).*/
		/* MOVE BEGN                   TO ITDM-FUNCTION.                */
		/* MOVE WSSP-COMPANY           TO ITDM-ITEMCOY.                 */
		/* MOVE 'T5687'                TO ITDM-ITEMTABL.                */
		/* MOVE COVTLNB-CRTABLE        TO ITDM-ITEMITEM.                */
		/* MOVE CHDRLNB-OCCDATE        TO ITDM-ITMFRM.                  */
		/* CALL 'ITDMIO'               USING ITDM-PARAMS.               */
		/* IF  ITDM-STATUZ             NOT = O-K                        */
		/*                        AND  NOT = ENDP                       */
		/*    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
		/*    MOVE ITDM-STATUZ         TO SYSR-STATUZ                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* IF    ITDM-ITEMCOY NOT = WSSP-COMPANY                        */
		/*   OR  ITDM-ITEMTABL         NOT =  T5687                     */
		/*   OR  ITDM-ITEMITEM         NOT =  COVTLNB-CRTABLE           */
		/*   OR  ITDM-VALIDFLAG        NOT = '1'                        */
		/*       MOVE E366                TO SYSR-STATUZ                */
		/*       MOVE '2'                 TO SYSR-SYSERR-TYPE   <A05691>*/
		/*       PERFORM 600-FATAL-ERROR.                               */
		/* MOVE ITDM-GENAREA           TO T5687-T5687-REC.              */
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		if (isEQ(t5687rec.premmeth, SPACES)) {
			/* Nothing to do. */
		}
		/* IF T5687-RIIND              = 'N' OR SPACES          <R96REA>*/
		/*    MOVE SPACES              TO S6326-RATYPIND        <R96REA>*/
		/*    MOVE 'Y'                 TO S6326-RATYPIND-OUT (ND<R96REA>*/
		/*    MOVE 'Y'                 TO S6326-RATYPIND-OUT (PR<R96REA>*/
		/* ELSE                                                 <R96REA>*/
		/*    PERFORM 1100-CHECK-RACT.                          <R96REA>*/
		checkRacd1100();

	
			
			/*ILIFE-2964 IL_BASE_ITEM-001-Base development  starts*/
			if (isEQ(t5687rec.zsredtrm, "Y") && ispermission) { 
				m100CheckMbns();
			}
		else { 
			sv.optsmodeOut[varcom.nd.toInt()].set("Y");
			sv.optsmodeOut[varcom.pr.toInt()].set("Y");
		}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/
			
		/*    Obtain the long description from DESC using DESCIO.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
		descIO.setDescitem(covtlnbIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*    MOVE DESC-LONGDESC          TO WSAA-HEDLINE.                 */
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaHedline.fill("?");
		}
		else {
			wsaaHedline.set(descIO.getLongdesc());
		}
	}

protected void premmeth1020()
	{
		/*  - use the  premium-method  selected  from  T5687, if not*/
		/*       blank,  to access T5675.  This gives the subroutine*/
		/*       to use for the calculation.*/
		/*IF ITDM-ITEMITEM            = SPACES                         */
		if (isEQ(t5687rec.premmeth, SPACES)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		premReqd = "N";
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/* ILIFE-3142 End*/
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void cont1030()
	{
		/**---------------Set up and centre heading---------------**/
		/*LOAD-HEADING*/
		wsaaCentreHeading.set(wsaaHedline);
		callProgram(Centre.class, wsaaCentreString);
		wsaaHeading.set(wsaaCentreHeading);
		wsaaHeadingChar[1].set(SPACES);
		/*MOVE SPACE                  TO WSAA-HEADING-CHAR (26).       */
		wsaaHeadingChar[32].set(SPACES);
		sub4.set(1);
	}

protected void loop11250()
	{
		if (isEQ(wsaaHeadingChar[sub4.toInt()], SPACES)) {
			sub4.add(1);
			goTo(GotoLabel.loop11250);
		}
		sub4.subtract(1);
		//wsaaHeadingChar[sub4.toInt()].set(wsaaStartUnderline);
		/*MOVE 26                     TO SUB4.                         */
		sub4.set(32);
	}

protected void loop21300()
	{
		if (isEQ(wsaaHeadingChar[sub4.toInt()], SPACES)) {
			sub4.subtract(1);
			goTo(GotoLabel.loop21300);
		}
		sub4.add(1);
		//wsaaHeadingChar[sub4.toInt()].set(wsaaEndUnderline);
//		MIBT-254 STARTS
		sv.crtabdesc.set(wsaaHeading.trim());
//		MIBT-254 ENDS
		/* Read the latest premium tollerance allowed.                     */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(payrIO.getCntcurr());
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		/* THIS SECTION READS THE EDIT RULES TABLE ENTRY ASSOCIATED WITH*/
		/* THIS PARTICULAR PRODUCT.*/
		/* IF THE ENTRY WAS SUCESSFULLY READ LAST TIME THIS PROGRAM WAS*/
		/* EXECUTED, IT IS NOT READ AGAIN.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/*MATCH PROG WITH PROG ON 5551***********/
		x.set(0);
	}

protected void matchProgName()
	{
		x.add(1);
		if (isGT(x, 4)) {
			scrnparams.errorCode.set(errorsInner.e302);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1900);
		}
		if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
			wsbbTran.set(t5671rec.edtitm[x.toInt()]);
		}
		else {
			goTo(GotoLabel.matchProgName);
		}
		/*    Read T5551 to obtain the Unit Linked Edit Rules.*/
		/*itdmIO.setFunction(varcom.begn);
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setItemtabl(tablesInner.t5551);
		wsbbCurrency.set(payrIO.getCntcurr());
		/*MOVE CHDRLNB-CNTCURR        TO WSBB-CURRENCY.                
		itdmIO.setItemitem(wsbbTranCurrency);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5551)
		|| isNE(itdmIO.getItemitem(), wsbbTranCurrency)
		|| isNE(itdmIO.getValidflag(), "1")) {
			scrnparams.errorCode.set(errorsInner.g155);
			wsaaT5551Error = "Y";
			goTo(GotoLabel.cont1890);

		}*/
		/*       MOVE '2'                 TO SYSR-SYSERR-TYPE      <011>*/
		/*       MOVE G155                TO SYSR-STATUZ                */
		/*       PERFORM 600-FATAL-ERROR.                              
		t5551rec.t5551Rec.set(itdmIO.getGenarea());*/
		/* If T5551-RSVFLG = N, do not display Reserve Units Indicator     */
		/* and Date fields                                                 */
		wsbbCurrency.set(payrIO.getCntcurr());
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsbbTranCurrency.toString());
		itempf.setItemtabl(tablesInner.t5551.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);//ILIFE-3955
		if(itempfList !=null && itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getValidflag(), "1")) {
					scrnparams.errorCode.set(errorsInner.g155);
					wsaaT5551Error = "Y";
					goTo(GotoLabel.cont1890);
				}
				else
				{
					t5551rec.t5551Rec.set(StringUtil.rawToString(it.getGenarea()));
				}
			}			
		} 
		
		
		if (firsttime.isTrue()) {
			sv.reserveUnitsInd.set(t5551rec.rsvflg);
			if (isEQ(t5551rec.rsvflg, "N")) {
				sv.rsuninOut[varcom.pr.toInt()].set("Y");
				sv.rsuninOut[varcom.nd.toInt()].set("Y");
				sv.rundteOut[varcom.pr.toInt()].set("Y");
				sv.rundteOut[varcom.nd.toInt()].set("Y");
				sv.reserveUnitsDate.set(varcom.vrcmMaxDate);
			}
		}
		/*    If both the maximum and minimum values for the Sum*/
		/*    Insured on T5551 are zeros then no entry is allowed in*/
		/*    that field - protect and non-display it*/
		if ((isEQ(t5551rec.sumInsMax, ZERO)
		&& isEQ(t5551rec.sumInsMin, ZERO))) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5551rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin, sv.numapp), sv.numavail)));
			}
		}
		/*  - if all   the   valid   mortality  classes  are  blank,*/
		/*       non-display  and  protect  this  field. If there is*/
		/*       only  one  mortality  class, display and protect it*/
		/*       (no validation will be required).*/
		if (isEQ(t5551rec.mortclss, SPACES)) {
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.mortcls01, SPACES)
		&& isEQ(t5551rec.mortcls02, SPACES)
		&& isEQ(t5551rec.mortcls03, SPACES)
		&& isEQ(t5551rec.mortcls04, SPACES)
		&& isEQ(t5551rec.mortcls05, SPACES)
		&& isEQ(t5551rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5551rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*  - if all the valid lien codes are blank, non-display and*/
		/*       protect  this field. Otherwise, this is an optional*/
		/*       field.*/
		if (isEQ(t5551rec.liencds, SPACES)) {
			/*sv.liencdOut[varcom.nd.toInt()].set("Y");*/
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/*    If both the maximum and minimum values for the Lump Sum      */
		/*    Payment on T5551 are zeros then no entry is allowed in       */
		/*    that field - protect and non-display it                      */
		if ((isEQ(t5551rec.maxLumpSum, ZERO)
		&& isEQ(t5551rec.minLumpSum, ZERO))) {
			sv.lmpcntOut[varcom.pr.toInt()].set("Y");
			sv.lmpcntOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t5551rec.maxLumpSum, t5551rec.minLumpSum)
		&& isNE(t5551rec.maxLumpSum, 0)) {
			sv.lmpcntOut[varcom.pr.toInt()].set("Y");
			sv.lumpContrib.set(t5551rec.maxLumpSum);
			if (plan.isTrue()) {
				compute(sv.lumpContrib, 3).setRounded((div(mult(sv.lumpContrib, sv.numapp), sv.numavail)));
			}
		}
		/*  - using  the  age  next  birthday  (ANB at RCD) from the*/
		/*       applicable  life  (see above), look up Issue Age on*/
		/*       the AGE and TERM  sections.  If  the  age fits into*/
		/*       a "slot" in  one  of  these sections,  and the risk*/
		/*       cessation  limits   are  the   same,   default  and*/
		/*       protect the risk cessation fields. Also do the same*/
		/*       for the premium  cessation  details.  In this case,*/
		/*       also  calculate  the  risk  and  premium  cessation*/
		/*       dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		checkDefaults1800();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5551rec.eaage, SPACES))) {
			mattcess1500();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5551rec.eaage, SPACES))) {
			premCessDate1550();
		}
		if (isNE(t5551rec.eaage, SPACES)) {
			sv.premcessOut[varcom.pr.toInt()].set("Y");
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void cont1060()
	{
		/* OPTIONS AND EXTRAS*/
		/* If options and extras are  not  allowed (as defined by T5608)*/
		/* non-display and protect the fields.*/
		/* Otherwise,  read the  options  and  extras  details  for  the*/
		/* current coverage/rider.  If any  records  exist, put a '+' in*/
		/* the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5551rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
			/* MOVE 'Y'                 TO S6326-RATYPIND-OUT (PR)  <010>*/
			sv.optextindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
		//BRD-NBP-012-STARTS
	//	ItempfDAO itempfDAO = DAOFactory.getItempfDAO(); //ILIFE-3955
		Itempf itempf = new Itempf();
		itempf.setItemtabl("T6625");
		itempf.setItemitem(covtlnbIO.getCrtable().toString());
		itempf.setItemcoy(covtlnbIO.getChdrcoy().toString().trim());
		itempf.setItempfx("IT");
		itempf.setItmfrm(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf); //ILIFE-3955
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {
				t6625rec.t6625Rec.set(new String(it.getGenarea()));
			}
		}
		if(isNE(t6625rec.annuty, "V")) {
			sv.optindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			sv.optindOut[varcom.nd.toInt()].set("N");
		}
		//BRD-NBP-012-ENDS
		

	}

	/**
	* <pre>
	*    If this is an Enquiry protect all thr fields except the
	*    Charge Options indicator.
	* </pre>
	*/
protected void cont1890()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.premcessOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.singprmOut[varcom.pr.toInt()].set("Y");
			sv.lmpcntOut[varcom.pr.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.numavailOut[varcom.pr.toInt()].set("Y");
			sv.rundteOut[varcom.pr.toInt()].set("Y");
			sv.rsuninOut[varcom.pr.toInt()].set("Y");
			sv.singpremtypeOut[varcom.pr.toInt()].set("Y");//ILIFE-7805
		}
		//BRD-NBP-012-STARTS
		if (isNE(covtlnbIO.getGmib(),SPACE)){
			sv.optind.set("+");
		}
		if (isNE(covtlnbIO.getGmdb(),SPACE)){
			sv.optind.set("+");
		}
		if (isNE(covtlnbIO.getGmwb(),SPACE)){
			sv.optind.set("+");
		}
		//BRD-NBP-012-ENDS
		/*ILIFE-7934: Starts*/
		if(mulProdflag && (isEQ(covtlnbIO.getCrtable(),OIR1)||isEQ(covtlnbIO.getCrtable(),OIS1)
				||isEQ(covtlnbIO.getCrtable(),SIS1)||isEQ(covtlnbIO.getCrtable(),SIR1))){
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		/*ILIFE-7934: Ends*/
	}

	/**
	* <pre>
	*1100-CHECK-RACT SECTION.                                 <R96REA>
	*1110-READ-RACT.                                          <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	**** MOVE COVTLNB-CHDRCOY        TO RACT-CHDRCOY.            <031>
	**** MOVE COVTLNB-CHDRNUM        TO RACT-CHDRNUM.            <031>
	**** MOVE COVTLNB-LIFE           TO RACT-LIFE.               <031>
	**** MOVE COVTLNB-COVERAGE       TO RACT-COVERAGE.           <031>
	**** MOVE COVTLNB-RIDER          TO RACT-RIDER.              <031>
	**** MOVE SPACES                 TO RACT-RASNUM              <031>
	****                                RACT-RATYPE              <031>
	****                                RACT-VALIDFLAG.          <031>
	**** MOVE BEGN                   TO RACT-FUNCTION.           <031>
	**** MOVE RACTREC                TO RACT-FORMAT.             <031>
	****                                                         <031>
	**** CALL 'RACTIO'               USING RACT-PARAMS.          <031>
	****                                                         <031>
	**** IF RACT-STATUZ          NOT = O-K                       <031>
	****                     AND NOT = ENDP                      <031>
	****    MOVE RACT-PARAMS         TO SYSR-PARAMS              <031>
	****    MOVE RACT-STATUZ         TO SYSR-STATUZ              <031>
	****    PERFORM 600-FATAL-ERROR.                             <031>
	****                                                         <031>
	**** IF RACT-CHDRCOY         NOT = COVTLNB-CHDRCOY           <031>
	**** OR RACT-CHDRNUM         NOT = COVTLNB-CHDRNUM           <031>
	**** OR RACT-LIFE            NOT = COVTLNB-LIFE              <031>
	**** OR RACT-COVERAGE        NOT = COVTLNB-COVERAGE          <031>
	**** OR RACT-RIDER           NOT = COVTLNB-RIDER             <031>
	**** OR RACT-VALIDFLAG       NOT = '1'                       <031>
	**** OR RACT-STATUZ              = ENDP                      <031>
	****    MOVE ENDP                TO RACT-STATUZ.             <031>
	****                                                         <031>
	**** IF RACT-STATUZ              = ENDP                      <031>
	****    IF S6326-RATYPIND        = 'X'                       <031>
	****        NEXT SENTENCE                                    <031>
	****    ELSE                                                 <031>
	****        MOVE ' '             TO S6326-RATYPIND           <031>
	**** ELSE                                                    <031>
	****    MOVE '+'                 TO S6326-RATYPIND.          <031>
	****                                                      <R96REA>
	**** MOVE COVTLNB-CHDRCOY        TO RACTLNB-CHDRCOY.      <R96REA>
	**** MOVE COVTLNB-CHDRNUM        TO RACTLNB-CHDRNUM.      <R96REA>
	**** MOVE COVTLNB-LIFE           TO RACTLNB-LIFE.         <R96REA>
	**** MOVE COVTLNB-COVERAGE       TO RACTLNB-COVERAGE.     <R96REA>
	**** MOVE COVTLNB-RIDER          TO RACTLNB-RIDER.        <R96REA>
	**** MOVE SPACES                 TO RACTLNB-RASNUM.       <R96REA>
	**** MOVE SPACES                 TO RACTLNB-RATYPE.       <R96REA>
	**** MOVE SPACES                 TO RACTLNB-VALIDFLAG.    <R96REA>
	**** MOVE BEGN                   TO RACTLNB-FUNCTION.     <R96REA>
	**** MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****                             AND NOT = ENDP           <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM 600-FATAL-ERROR.                         <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-CHDRCOY          NOT = COVTLNB-CHDRCOY    <R96REA>
	****    OR RACTLNB-CHDRNUM       NOT = COVTLNB-CHDRNUM    <R96REA>
	****    OR RACTLNB-LIFE          NOT = COVTLNB-LIFE       <R96REA>
	****    OR RACTLNB-COVERAGE      NOT = COVTLNB-COVERAGE   <R96REA>
	****    OR RACTLNB-RIDER         NOT = COVTLNB-RIDER      <R96REA>
	****    OR RACTLNB-VALIDFLAG     NOT = '3'                <R96REA>
	****    OR RACTLNB-STATUZ        = ENDP                   <R96REA>
	****     MOVE ENDP               TO RACTLNB-STATUZ        <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           = ENDP                   <R96REA>
	****     IF S6326-RATYPIND       = 'X'                    <R96REA>
	****         NEXT SENTENCE                                <R96REA>
	****     ELSE                                             <R96REA>
	****         MOVE ' '            TO S6326-RATYPIND        <R96REA>
	****     END-IF                                           <R96REA>
	**** ELSE                                                 <R96REA>
	****     MOVE '+'                TO S6326-RATYPIND        <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	*1190-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	* </pre>
	*/
protected void checkRacd1100()
	{
		readRacd1110();
	}

	/**
	* <pre>
	*************************                                 <R96REA>
	* </pre>
	*/
protected void readRacd1110()
	{
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		racdlnbIO.setLife(covtlnbIO.getLife());
		racdlnbIO.setCoverage(covtlnbIO.getCoverage());
		racdlnbIO.setRider(covtlnbIO.getRider());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setCestype("2");
		racdlnbIO.setFunction(varcom.begn);
//		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)
		&& isNE(racdlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(racdlnbIO.getLife(), covtlnbIO.getLife())
		|| isNE(racdlnbIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(racdlnbIO.getRider(), covtlnbIO.getRider())
		|| isNE(racdlnbIO.getPlanSuffix(), ZERO)
		|| isNE(racdlnbIO.getSeqno(), ZERO)
		|| isNE(racdlnbIO.getCestype(), "2")
		|| isEQ(racdlnbIO.getStatuz(), varcom.endp)) {
			if (isEQ(sv.ratypind, "X")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.ratypind.set(SPACES);
				sv.ratypindOut[varcom.nd.toInt()].set("Y");
				sv.ratypindOut[varcom.pr.toInt()].set("Y");
			}
		}
		else {
			sv.ratypind.set("+");
			sv.ratypindOut[varcom.nd.toInt()].set("N");
			sv.ratypindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void mattcess1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1500();
				case mattrm1510: 
					mattrm1510();
				case exit1540: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1500()
	{
		if (isEQ(sv.matage, 0)) {
			goTo(GotoLabel.mattrm1510);
		}
		if (isEQ(t5551rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.matage, wszzAnbAtCcd));
		}
		if (isEQ(t5551rec.eaage, "E")
		|| isEQ(t5551rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.matage);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.mattcessErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1540);
		}
		sv.mattcess.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1540);
	}

protected void mattrm1510()
	{
		if (isEQ(sv.mattrm, 0)) {
			return ;
		}
		if (isEQ(t5551rec.eaage, "A")
		|| isEQ(t5551rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.mattrm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5551rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.mattrm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.mattcessErr.set(datcon2rec.statuz);
			return ;
		}
		sv.mattcess.set(datcon2rec.intDate2);
	}

protected void premCessDate1550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1550();
				case premCessTerm1560: 
					premCessTerm1560();
				case exit1590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1550()
	{
		if (isEQ(sv.premCessAge, 0)) {
			goTo(GotoLabel.premCessTerm1560);
		}
		if (isEQ(t5551rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5551rec.eaage, "E")
		|| isEQ(t5551rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.premCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.premcessErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1590);
		}
		sv.premcess.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1590);
	}

protected void premCessTerm1560()
	{
		if (isEQ(sv.premCessTerm, 0)) {
			return ;
		}
		if (isEQ(t5551rec.eaage, "A")
		|| isEQ(t5551rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.premCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5551rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.premcessErr.set(datcon2rec.statuz);
			return ;
		}
		sv.premcess.set(datcon2rec.intDate2);
	}

protected void callDatcon21600()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkDefaults1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					searchTable1810();
				case nextColumn1820: 
					nextColumn1820();
				case moveDefaults1850: 
					moveDefaults1850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*  - using  the  age  next  birthday  (ANB at RCD) from the
	*       applicable  life  (see above), look up Issue Age on
	*       the AGE and TERM  sections.  If  the  age fits into
	*       a "slot" in  one  of these sections, and the
	*       risk cessation limits  are  the  same,  default and
	*       protect the risk cessation fields. Also do the same
	*       for the premium  cessation  details.  In this case,
	*       also  calculate  the  risk  and  premium  cessation
	*       dates.
	* </pre>
	*/
protected void searchTable1810()
	{
		sub1.set(0);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
	}

protected void nextColumn1820()
	{
		sub1.add(1);
		if (isGT(sub1, wsaaMaxOcc)) {
			goTo(GotoLabel.moveDefaults1850);
		}
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5551rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5551rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5551rec.maturityAgeFrom[sub1.toInt()], t5551rec.maturityAgeTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddMatage.set(t5551rec.maturityAgeTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5551rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5551rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5551rec.premCessageFrom[sub1.toInt()], t5551rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5551rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5551rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5551rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5551rec.maturityTermFrom[sub1.toInt()], t5551rec.maturityTermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddMattrm.set(t5551rec.maturityTermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5551rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5551rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5551rec.premCesstermFrom[sub1.toInt()], t5551rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5551rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		goTo(GotoLabel.nextColumn1820);
	}

protected void moveDefaults1850()
	{
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.matage.set(wsddMatage);
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.mattrm.set(wsddMattrm);
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		lextIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lextIO.setChdrnum(covtlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), lextIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), lextIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(), lextIO.getLife())
		|| isNE(covtlnbIO.getCoverage(), lextIO.getCoverage())
		|| isNE(covtlnbIO.getRider(), lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/*     SPECIAL EXIT PROCESSING                                   * */
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (forcedKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*  If there are no more records to display on a split plan        */
		/*  then give a message 'no more forward records', originally      */
		/*  if PgDn was attempted on a final covt record the program       */
		/*  lost its way.                                                  */
		if (isEQ(scrnparams.statuz, varcom.rolu)
		&& isEQ(covtunlIO.getNumapp(), ZERO)
		&& isEQ(wsaaNumavail, ZERO)
		&& isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			sv.polinc.set(chdrlnbIO.getPolinc());
			sv.numavail.set(wsaaNumavail);
			sv.numapp.set(covtunlIO.getNumapp());
			scrnparams.errorCode.set(errorsInner.e026);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5000();
		}
		//ILIFE-3188 started
		if(isEQ(wsspcomn.flag, "I")){
			sv.taxamtOut[varcom.pr.toInt()].set("Y");	
		}
		//ILIE-3188 ended
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
				case redisplay2480: 
					redisplay2480();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'S6326IO' USING SCRN-SCREEN-PARAMS                      */
		/*                                S6326-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested and the current credit is zero or*/
		/* equal to the number of policies in the plan (all or nothing),*/
		/* then skip the  validation.  Otherwise,  highlight  this as an*/
		/* error and then skip the remainder of the validation.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			if (isEQ(wsaaCredit, 0)
			|| isEQ(wsaaCredit, chdrlnbIO.getPolinc())) {
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*          GO TO 2900-EXIT                                        */
				goTo(GotoLabel.exit2090);
			}
			else {
				sv.numappErr.set(errorsInner.g622);
				goTo(GotoLabel.redisplay2480);
			}
		}
		if (isEQ(wsaaT5551Error, "Y")) {
			scrnparams.errorCode.set(errorsInner.g155);
			goTo(GotoLabel.redisplay2480);
		}
		a100CheckLimit();
		if (isNE(wsspcomn.flag, "I")) {
			/*    PERFORM 5000-VALIDATE-SCREEN.                            */
			validateScreen2500();
		}
		/*ILIFE-7805 - Start*/
		if(isEQ(wsaaSingPremDrpVal, SPACES) || isNE(wsaaSingPremDrpVal, sv.singpremtype)){
			wsaaSingPremDrpVal.set(sv.singpremtype);
			if(isEQ(wsaaSingPremDrpVal,"ROP"))
			{
				singlepremiumind = "X";			
			}		
		}			
		/*ILIFE-7805 - End*/
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed. If*/
		/* options  and  extras  are  requested,  DO  NOT  CALCULATE THE*/
		/* PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		/* If  reassurance   already exists, there will be a '+' in this   */
		/* field.  A  request  to access the details is made by entering   */
		/* 'X'.  No  other  values  (other  than  blank) are allowed.      */
		if (isNE(sv.ratypind, " ")
		&& isNE(sv.ratypind, "+")
		&& isNE(sv.ratypind, "X")) {
			sv.ratypindErr.set(errorsInner.g620);
		}
		/* Tax indicator must either be ' ', '+' or 'X'                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		//BRD-NBP-012-STARTS
		if (isNE(sv.optind, " ")
				&& isNE(sv.optind, "+")
				&& isNE(sv.optind, "X")) {
					sv.optindErr.set(errorsInner.g620);
		}
		//BRD-NBP-012-ENDS
		/*    Move the premium term to WSSP area to be used in linkage     */
		/*    for P5013.                                                   */
		/* IF S6326-OPTEXTIND          = 'X'                       <029>*/
		/*    MOVE S6326-PREM-CESS-TERM                            <029>*/
		/*                             TO WSSP-BIG-AMT             <029>*/
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 2900-EXIT.                                     <029>*/
		/*    GO TO 2090-EXIT.                                     <029>*/
		/* AQR 7595 - If X in Options Indicator and errors exist           */
		/* skip date processing, which was causing IVSD error              */
		if (isEQ(sv.optextind, "X")
		&& isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2480);
		}
		if (isEQ(sv.optextind, "X")) {
			if (isNE(sv.premCessTerm, ZERO)) {
				wssplife.bigAmt.set(sv.premCessTerm);
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*          GO TO 2900-EXIT                                        */
				goTo(GotoLabel.exit2090);
			}
			else {
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(sv.premcess);
				callDatcon32600();
				wsaaFreqFactor.set(datcon3rec.freqFactor);
				wssplife.fupno.set(wsaaFreqFactor);
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*             GO TO 2900-EXIT                                     */
				goTo(GotoLabel.exit2090);
			}
		}
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& isNE(wsspcomn.flag, "I")) {
			calcPremium2700();
		}
		/* Check if the tax indicator entry is valid or not.               */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		//BRD-NBP-012-STARTS
		if (isNE(sv.optind, " ")
		&& isNE(sv.optind, "+")
		&& isNE(sv.optind, "X")) {
			sv.optindErr.set(errorsInner.g620);
		}
		//BRD-NBP-012-ENDS
		/* If this proposal requires underwriting, determine the Age       */
		/* to use for the Underwriting based on Age and SA.                */
		if (underwritingReqd.isTrue()) {
			calcUndwAge2900();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2480);
		}
		compute(sv.zbinstprem, 2).set(sub(sv.singlePremium, sv.zlinstprem));
		/* If 'ROLD' was entered,check this is not the first page.*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(wsaaFirstSeqnbr, covtunlIO.getSeqnbr())) {
			scrnparams.errorCode.set(errorsInner.e027);
			goTo(GotoLabel.redisplay2480);
		}
		validateOccupationOrOccupationClass();	//ICIL-1494
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz, varcom.calc)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}

		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
		if (ispermission  && isNE(sv.optsmode, " ") 
				&& isNE(sv.optsmode, "+")
				&& isNE(sv.optsmode, "X")) {
					sv.optsmodeErr.set(errorsInner.g620);
				}
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends */
	}

protected void redisplay2480()
	{
		wsspcomn.edterror.set("Y");
	}




protected void validateOccupationOrOccupationClass() {
	isFollowUpRequired=false;
	NBPRP056Permission  = FeaConfg.isFeatureExist("2",NBPRP056, appVars, "IT");
	if(NBPRP056Permission &&  lifelnbIO != null) {
		readTA610();
		String occupation = lifelnbIO.getOccup().toString();
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu); 
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu); 
						break;
					}
				}	
			}
		}
	}
	
}

private void readTA610() {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tA610);
	itempf.setItemitem(covtlnbIO.getCrtable().toString());
	itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
	itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
	ta610List = itempfDAO.findByItemDates(itempf);	//ICIL-1494
	if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
		ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
	}	  
}


protected void getOccupationClass2900(String occupation) {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
}
	/**
	* <pre>
	*5000-VALIDATE-SCREEN SECTION.                                    
	*5000-PARA.                                                       
	* </pre>
	*/
protected void validateScreen2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2510();
					checkSumin2525();
				case checkLumpSum2527: 
					checkLumpSum2527();
				case checkMaturityFields2530: 
					checkMaturityFields2530();
					checkPcessFields2535();
					checkAgeTerm2540();
					riderCont2541();
				case datesCont2541: 
					datesCont2541();
				case ageAnniversary2541a: 
					ageAnniversary2541a();
				case check2542: 
					check2542();
				case termExact2542: 
					termExact2542();
				case check2543: 
					check2543();
				case checkOccurance2545: 
					checkOccurance2545();
				case checkTermFields2550: 
					checkTermFields2550();
				case checkComplete2555: 
					checkComplete2555();
				case checkMortcls2560: 
					checkMortcls2560();
				case loop2565: 
					loop2565();
				case checkLiencd2570: 
					checkLiencd2570();
				case loop2575: 
					loop2575();
				case checkMore2580: 
					checkMore2580();
				case exit2590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2510()
	{
		/* If plan processing, and no policies applicable,*/
		/*   skip the validation as this COVTUNL is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2590);
		}
		/*    IF  S6326-NUMAVAIL  = S6326-POLINC                       */
		/*        MOVE H363      TO S6326-NUMAPP-ERR                   */
		/*    ELSE                                                     */
		/*        GO TO 5350-EXIT.                                     */
		if (isEQ(sv.numapp, ZERO)) {
			sv.numappErr.set(errorsInner.h363);
		}
		/*VALIDATE-ALL*/
		/* Before  the  premium amount is calculated, the screen must be*/
		/* valid.  So  all  editing  is  completed before the premium is*/
		/* calculated.*/
		/*  1) Check  the  sum-assured,  if  applicable, against the*/
		/*       limits (NB. these apply to the plan, so adjust first).*/
		/*    - if only one sum insured is allowed, re-calculate plan*/
		/*      level sum insured (if applicable).*/
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(t5551rec.sumInsMin, sv.numapp), sv.polinc)));
			}
		}
	}

protected void checkSumin2525()
	{
		if (plan.isTrue()) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin, sv.polinc), sv.numapp)));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)) {
			/*       GO TO 2530-CHECK-MATURITY-FIELDS.                      */
			goTo(GotoLabel.checkLumpSum2527);
		}
		if (isLT(wsaaSumin, t5551rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin, t5551rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

protected void checkLumpSum2527()
	{
		/*  1a) Check  the  Lump-Sum,  if  applicable, against the         */
		/*       limits (NB. these apply to the plan, so adjust first).    */
		/*    - if only one Lump Sum is allowed, re-calculate plan         */
		/*      level Lump Sum (if applicable).                            */
		if (isEQ(t5551rec.maxLumpSum, t5551rec.minLumpSum)
		&& isNE(t5551rec.maxLumpSum, 0)) {
			if (plan.isTrue()) {
				compute(sv.lumpContrib, 3).setRounded((div(mult(t5551rec.minLumpSum, sv.numapp), sv.polinc)));
			}
		}
		if (plan.isTrue()) {
			compute(wsaaLumpSum, 3).setRounded((div(mult(sv.lumpContrib, sv.polinc), sv.numapp)));
		}
		else {
			wsaaLumpSum.set(sv.lumpContrib);
		}
		if (isEQ(t5551rec.maxLumpSum, t5551rec.minLumpSum)) {
			goTo(GotoLabel.checkMaturityFields2530);
		}
		if (isLT(wsaaLumpSum, t5551rec.minLumpSum)) {
			sv.lmpcntErr.set(errorsInner.h369);
		}
		if (isGT(wsaaLumpSum, t5551rec.maxLumpSum)) {
			sv.lmpcntErr.set(errorsInner.h368);
		}
	}

	/**
	* <pre>
	*  2) Check the consistency of the risk age and term fields
	*       and  premium  age and term fields. Either, risk age
	*       and  premium age must be used or risk term and
	*       premium  term  must  be  used.  They  must  not  be
	*       combined. Note that  these  only need validating if
	*       they were not defaulted.
	*     NOTE: Age and Term fields may now be mixed.                 
	* </pre>
	*/
protected void checkMaturityFields2530()
	{
		if (isGT(sv.matage, 0)
		&& isGT(sv.mattrm, 0)) {
			sv.matageErr.set(errorsInner.f220);
			sv.mattrmErr.set(errorsInner.f220);
		}
		if (isNE(t5551rec.eaage, SPACES)
		&& isEQ(sv.matage, 0)
		&& isEQ(sv.mattrm, 0)
		&& isEQ(sv.singlePremium, 0)){					//ILIFE-4212
			sv.matageErr.set(errorsInner.e560);
			sv.mattrmErr.set(errorsInner.e560);
			sv.singprmErr.set(errorsInner.G070);		//ILIFE-4212
		}
	}

protected void checkPcessFields2535()
	{
		if (isGT(sv.premCessAge, 0)
		&& isGT(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)
		&& isEQ(sv.matageErr, SPACES)
		&& isEQ(sv.mattrmErr, SPACES)) {
			sv.premCessAge.set(sv.matage);
			sv.premCessTerm.set(sv.mattrm);
		}
		if (isNE(t5551rec.eaage, SPACES)
		&& isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
		if (isEQ(sv.matage, 0)
		&& isEQ(sv.mattrm, 0)
		&& isEQ(sv.pcessageErr, SPACES)
		&& isEQ(sv.pcesstrmErr, SPACES)) {
			sv.matage.set(sv.premCessAge);
			sv.mattrm.set(sv.premCessTerm);
		}
	}

protected void checkAgeTerm2540()
	{
		if ((isNE(sv.matageErr, SPACES))
		|| (isNE(sv.mattrmErr, SPACES))
		|| (isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*    IF S6326-MATAGE      > 0 AND                                 */
		/*       S6326-PREM-CESS-AGE      = 0                              */
		/*        MOVE F224               TO S6326-PCESSAGE-ERR            */
		/*        IF NOT DEFAULT-RA                                        */
		/*           MOVE F224            TO S6326-MATAGE-ERR.             */
		/*    IF S6326-MATTRM     > 0 AND                                  */
		/*       S6326-PREM-CESS-TERM     = 0                              */
		/*        MOVE F225               TO S6326-PCESSTRM-ERR            */
		/*        IF NOT DEFAULT-RT                                        */
		/*           MOVE F225            TO S6326-MATTRM-ERR.             */
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.matageErr, SPACES))
		|| (isNE(sv.mattrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*  To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5551).*/
		if (!wsaaDefaultsInner.defaultRt.isTrue()
		&& !wsaaDefaultsInner.defaultRa.isTrue()) {
			if (isEQ(sv.matage, 0)
			&& isEQ(sv.mattrm, 0)) {
				sv.matageErr.set(errorsInner.h366);
				sv.mattrmErr.set(errorsInner.h366);
			}
		}
		if (!wsaaDefaultsInner.defaultPt.isTrue()
		&& !wsaaDefaultsInner.defaultPa.isTrue()) {
			if (isEQ(sv.premCessAge, 0)
			&& isEQ(sv.premCessTerm, 0)) {
				sv.pcessageErr.set(errorsInner.h366);
				sv.pcesstrmErr.set(errorsInner.h366);
			}
		}
		/* IF S6326-RESERVE-UNITS-DATE NOT = 0                          */
		/*    IF  S6326-RESERVE-UNITS-DATE < CHDRLNB-OCCDATE            */
		/*        MOVE H359 TO S6326-RUNDTE-ERR.                        */
		if ((isEQ(covtunlIO.getReserveUnitsDate(), ZERO)
		|| isEQ(covtunlIO.getReserveUnitsDate(), varcom.vrcmMaxDate))
		&& isNE(sv.reserveUnitsDate, 0)
		&& isLT(sv.reserveUnitsDate, chdrlnbIO.getOccdate())) {
			sv.rundteErr.set(errorsInner.h359);
		}
		if (isNE(t5551rec.eaage, SPACES)
		|| isEQ(sv.mattcess, varcom.vrcmMaxDate)) {
			mattcess1500();
		}
		else {
			if (isEQ(sv.mattcessErr, SPACES)) {
				if (isNE(sv.matage, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.matage);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.mattcess)) {
						/*                MOVE U029    TO S6326-MATAGE-ERR              */
						sv.matageErr.set(errorsInner.h040);
						sv.mattcessErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.mattcess)) {
							/*                   MOVE U029 TO S6326-MATAGE-ERR              */
							sv.matageErr.set(errorsInner.h040);
							sv.mattcessErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.mattrm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.mattrm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.mattcess)) {
							/*                   MOVE U029 TO S6326-MATTRM-ERR              */
							sv.mattrmErr.set(errorsInner.h040);
							sv.mattcessErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.mattcess)) {
								/*                      MOVE U029 TO S6326-MATTRM-ERR           */
								sv.mattrmErr.set(errorsInner.h040);
								sv.mattcessErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(t5551rec.eaage, SPACES)
		|| isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			premCessDate1550();
		}
		else {
			if (isEQ(sv.premcessErr, SPACES)) {
				if (isNE(sv.premCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.premCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.premcess)) {
						/*                MOVE U029    TO S6326-PCESSAGE-ERR            */
						sv.pcessageErr.set(errorsInner.h040);
						sv.premcessErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.premcess)) {
							/*                   MOVE U029 TO S6326-PCESSAGE-ERR            */
							sv.pcessageErr.set(errorsInner.h040);
							sv.premcessErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.premCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.premCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.premcess)) {
							/*                   MOVE U029 TO S6326-PCESSTRM-ERR            */
							sv.pcesstrmErr.set(errorsInner.h040);
							sv.premcessErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.premcess)) {
								/*                      MOVE U029 TO S6326-PCESSTRM-ERR         */
								sv.pcesstrmErr.set(errorsInner.h040);
								sv.premcessErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(sv.mattcessErr, SPACES)
		|| isNE(sv.premcessErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			sv.premcess.set(sv.mattcess);
		}
		if (isGT(sv.premcess, sv.mattcess)) {
			sv.premcessErr.set(errorsInner.e566);
			sv.mattcessErr.set(errorsInner.e566);
		}
		
		//ILIFE-4372 - LIFE VPMS Extended Validations - Added client min age validation
		boolean extValidation = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV01", appVars, "IT");
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && extValidation && isEQ(chdrlnbIO.cnttype,"RUL"))
		{
			if (isLT(sv.anbAtCcd, 19)) {
				sv.anbccdErr.set("RGKW");				
			}
		}
	}

protected void riderCont2541()
	{
		if (isEQ(sv.rider, "00")) {
			goTo(GotoLabel.datesCont2541);
		}
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */
		covtunlIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtunlIO.setChdrnum(covtlnbIO.getChdrnum());
		covtunlIO.setLife(covtlnbIO.getLife());
		covtunlIO.setCoverage(covtlnbIO.getCoverage());
		covtunlIO.setRider("00");
		covtunlIO.setSeqnbr(ZERO);
		covtunlIO.setFunction(varcom.begn);
		covtunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtunlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		if (isNE(covtunlIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtunlIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtunlIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtunlIO.getRider(), "00")) {
			covtunlIO.setStatuz(varcom.endp);
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		/* Validate that the riders Risk and Premium Cessation dates are   */
		/* not greater than the Coverages to which it is attached. A rider */
		/* can not mature after its driving coverage.                      */
		if (isGT(sv.mattcess, covtunlIO.getRiskCessDate())) {
			sv.mattcessErr.set(errorsInner.h033);
		}
		if (isGT(sv.premcess, covtunlIO.getPremCessDate())) {
			sv.premcessErr.set(errorsInner.h044);
		}
		/* Re-Read the Rider record in order to position the file and data */
		/* at the correct information to process.                          */
		covtunlIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtunlIO.setChdrnum(covtlnbIO.getChdrnum());
		covtunlIO.setLife(covtlnbIO.getLife());
		covtunlIO.setCoverage(covtlnbIO.getCoverage());
		covtunlIO.setRider(covtlnbIO.getRider());
		covtunlIO.setSeqnbr(covtlnbIO.getSeqnbr());
		covtunlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
	}

protected void datesCont2541()
	{
		if (isNE(sv.mattcessErr, SPACES)
		|| isNE(sv.premcessErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*  Calculate cessasion age and term.*/
		if (isEQ(sv.mattcess, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.check2542);
		}
		if (isEQ(t5551rec.eaage, "A")) {
			goTo(GotoLabel.ageAnniversary2541a);
		}
		/* MOVE WSZZ-CLTDOB            TO DTC3-INT-DATE-1.              */
		/* MOVE S6326-MATTCESS   TO DTC3-INT-DATE-2.                    */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-MATAGE.                  */
		/* MOVE S6326-PREMCESS   TO DTC3-INT-DATE-2.                    */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-AGE.           */
		if (isEQ(sv.matage, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			wszzMatage.set(datcon3rec.freqFactor);
		}
		else {
			wszzMatage.set(sv.matage);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.check2542);
	}

protected void ageAnniversary2541a()
	{
		if (isEQ(sv.matage, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			wszzMatage.set(datcon3rec.freqFactor);
			wszzMatage.add(wszzAnbAtCcd);
		}
		else {
			wszzMatage.set(sv.matage);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
			wszzPremCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void check2542()
	{
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.check2543);
		}
		if (isEQ(t5551rec.eaage, "E")
		|| isEQ(t5551rec.eaage, " ")) {
			goTo(GotoLabel.termExact2542);
		}
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE S6326-MATTCESS   TO DTC3-INT-DATE-2.                    */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-MATTRM.                  */
		/* MOVE S6326-PREMCESS   TO DTC3-INT-DATE-2.                    */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-TERM.          */
		if (isEQ(sv.mattrm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			wszzMattrm.set(datcon3rec.freqFactor);
		}
		else {
			wszzMattrm.set(sv.mattrm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		goTo(GotoLabel.check2543);
	}

protected void termExact2542()
	{
		if (isEQ(sv.mattrm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			wszzMattrm.set(datcon3rec.freqFactor);
			compute(wszzMattrm, 5).set(sub(wszzMattrm, wszzAnbAtCcd));
		}
		else {
			wszzMattrm.set(sv.mattrm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
			compute(wszzPremCessTerm, 5).set(sub(wszzPremCessTerm, wszzAnbAtCcd));
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
	}

protected void check2543()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		//ILIFE-4899 start
	boolean extValidation = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV01", appVars, "IT");
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && extValidation))
	{
		sv.matageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.mattrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
		//ILIFE-4899 end
		}
	}

	/**
	* <pre>
	* Check each possible option.
	* </pre>
	*/
protected void checkOccurance2545()
	{
		x.add(1);
		if (isGT(x, wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2555);
		}
		if ((isEQ(t5551rec.ageIssageFrm[x.toInt()], 0)
		&& isEQ(t5551rec.ageIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5551rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5551rec.ageIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkTermFields2550);
		}
		if (isGTE(wszzMatage, t5551rec.maturityAgeFrom[x.toInt()])
		&& isLTE(wszzMatage, t5551rec.maturityAgeTo[x.toInt()])) {
			sv.matageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge, t5551rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge, t5551rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2550()
	{
		if ((isEQ(t5551rec.termIssageFrm[x.toInt()], 0)
		&& isEQ(t5551rec.termIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5551rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5551rec.termIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkOccurance2545);
		}
		if (isGTE(wszzMattrm, t5551rec.maturityTermFrom[x.toInt()])
		&& isLTE(wszzMattrm, t5551rec.maturityTermTo[x.toInt()])) {
			sv.mattrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm, t5551rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm, t5551rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2545);
	}

protected void checkComplete2555()
	{
		if (isNE(sv.mattrmErr, SPACES)
		&& isEQ(sv.mattrm, ZERO)) {
			sv.mattcessErr.set(sv.mattrmErr);
			sv.mattrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, ZERO)) {
			sv.premcessErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.matageErr, SPACES)
		&& isEQ(sv.matage, ZERO)) {
			sv.mattcessErr.set(sv.matageErr);
			sv.matageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, ZERO)) {
			sv.premcessErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2560()
	{
		/*  3) Mortality-Class,  if the mortality class appears on a*/
		/*       coverage/rider  screen  it  is  a  compulsory field*/
		/*       because it will  be used in calculating the premium*/
		/*       amount. The mortality class entered must one of the*/
		/*       ones in the edit rules table.*/
		if(isNE(t5551rec.mortclss,SPACES) && isEQ(sv.mortcls,SPACES)){
			sv.mortclsErr.set(errorsInner.e186);
		}
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()], "Y")) {
			goTo(GotoLabel.checkLiencd2570);
		}
		x.set(0);
	}

protected void loop2565()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2570);
		}
		if (isNE(t5551rec.mortcls[x.toInt()], sv.mortcls)) {
			goTo(GotoLabel.loop2565);
		}
	}

protected void checkLiencd2570()
	{
		if (isEQ(sv.liencdOut[varcom.pr.toInt()], "Y")
		|| isEQ(sv.liencd, SPACES)) {
			goTo(GotoLabel.checkMore2580);
		}
		x.set(0);
	}

protected void loop2575()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2580);
		}
		if (isEQ(t5551rec.liencd[x.toInt()], SPACES)
		|| isNE(t5551rec.liencd[x.toInt()], sv.liencd)) {
			goTo(GotoLabel.loop2575);
		}
	}

protected void checkMore2580()
	{
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)
		|| (isEQ(sv.mattrm, SPACES)
		&& isEQ(sv.matage, SPACES)
		&& isEQ(sv.premCessTerm, SPACES)
		&& isEQ(sv.premCessAge, SPACES))) {
			sv.matageErr.set(errorsInner.e560);
		}
		/*    IF (FIRST-TIME) OR (NONPLAN)*/
		if (isEQ(sv.reserveUnitsInd, "N")
		&& isNE(sv.reserveUnitsDate, varcom.vrcmMaxDate)) {
			sv.rundteErr.set(errorsInner.g099);
			sv.rsuninErr.set(errorsInner.g099);
		}
		else {
			if (isEQ(sv.reserveUnitsInd, "Y")
			&& isEQ(sv.reserveUnitsDate, varcom.vrcmMaxDate)) {
				sv.rundteErr.set(errorsInner.e186);
			}
		}
		if (isEQ(sv.numapp, 0)) {
			if (isEQ(sv.numavail, sv.polinc)) {
				sv.numappErr.set(errorsInner.h363);
			}
		}
		/*ILIFE-7805 - Start*/
		if(singPremTypeFlag && isEQ(sv.singpremtype,SPACES)){
			sv.singpremtypeErr.set(errorsInner.e186);
		}
		/*ILIFE-7805 - End*/
		wsaaWorkCredit.set(wsaaCredit);
		if (nonfirst.isTrue()) {
			wsaaWorkCredit.add(covtunlIO.getNumapp());
		}
		wsaaWorkCredit.subtract(sv.numapp);
		if (isLT(wsaaWorkCredit, 0)
		&& isGT(sv.numapp, sv.numavail)) {
			/*   MOVE U012              TO S6326-NUMAPP-ERR            <018>*/
			sv.numappErr.set(errorsInner.h437);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.singlePremium, ZERO)) {
			zrdecplrec.amountIn.set(sv.singlePremium);
			zrdecplrec.currency.set(sv.currcd);
			a200CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.singlePremium)) {
				sv.singprmErr.set(errorsInner.rfik);
				wsspcomn.edterror.set("Y");
			}
		}
		if (isNE(sv.lumpContrib, ZERO)) {
			zrdecplrec.amountIn.set(sv.lumpContrib);
			zrdecplrec.currency.set(sv.currcd);
			a200CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.lumpContrib)) {
				sv.lmpcntErr.set(errorsInner.rfik);
				wsspcomn.edterror.set("Y");
			}
		}
		if (isEQ(wsspcomn.edterror, "Y")) {
			/*   GO TO 5350-EXIT.                                          */
			return ;
		}
		if (isEQ(singPremInd, 1)) {
			/*   GO TO 5350-EXIT.                                          */
			return ;
		}
	}

protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcPremium2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2700();
				case calc2710: 
					calc2710();
				case calc2720: 
					calc2720();
				case exit2790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2700()
	{
		/* If plan processing, and no policies applicable,*/
		/*   skip the validation as this COVTUNL is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2790);
		}
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the sum insured OR the sum insured from the*/
		/* premium.*/
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/* worked out above passing:*/
		/* Check for a joint life for separate processing.                 */
		if (isNE(sv.jlifcnum, "NONE    ")
		&& isEQ(premReqd, "N")) {
			goTo(GotoLabel.calc2720);
		}
		if (isEQ(premReqd, "N")) {
			goTo(GotoLabel.calc2710);
		}
		if (isEQ(sv.singlePremium, 0)) {
			/*    MOVE U019                TO S6326-SINGPRM-ERR.            */
			sv.singprmErr.set(errorsInner.g818);
		}
		goTo(GotoLabel.exit2790);
	}

protected void calc2710()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtlnbIO.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.jlage.set(wsaaAnbAtCcd2);
		premiumrec.jlsex.set(wsaaSex2);
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(sv.premcess);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		callDatcon32600();
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor, .99999));
		/*MOVE CHDRLNB-CNTCURR        TO CPRM-CURRCODE.                */
		premiumrec.currcode.set(payrIO.getCntcurr());
		/*  (wsaa-sumin already adjusted for plan processing)*/
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		/* MOVE PAYR-BILLFREQ          TO CPRM-BILLFREQ.        <A05691>*/
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			premiumrec.billfreq.set("00");
		}
		else {
			premiumrec.billfreq.set(payrIO.getBillfreq());
		}
		premiumrec.mop.set(payrIO.getBillchnl());
		/*MOVE CHDRLNB-BILLFREQ       TO CPRM-BILLFREQ.                */
		/*MOVE CHDRLNB-BILLCHNL       TO CPRM-MOP                      */
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(sv.singlePremium);
		premiumrec.calcBasPrem.set(sv.singlePremium);
		premiumrec.calcLoaPrem.set(ZERO);
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), sv.numapp)));
		}
		/* As this is a unit linked Program, there is no need           */
		/* for the linkage to be set up with  ANNY Details.             */
		/* Therefore, all fields are initialised before the call        */
		/* to the Subroutine.                                           */
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			premiumrec.cnttype.set(chdrlnbIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec,vpxacblrec.vpxacblRec);
			if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
			}
			if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.singprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2790);
			}
			//ILIFE-7845
			premiumrec.riskPrem.set(ZERO);
			riskPremflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), RISKPREM_FEATURE_ID, appVars, "IT");
			if (riskPremflag) {
				premiumrec.cnttype.set(chdrlnbIO.getCnttype());
				premiumrec.crtable.set(covtlnbIO.getCrtable());
				premiumrec.calcTotPrem.set(add(premiumrec.zstpduty01, premiumrec.calcPrem));
				
			if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
			{
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
			}
			}	
			//ILIFE-7845
		}
		/*Ticket #ILIFE-2005 - End*/
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.singprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2790);
		}
		/* Adjust premium calculated for plan processing.*/
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.numapp), sv.polinc)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.numapp), sv.polinc)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.numapp), sv.polinc)));
		}
		/* Put possibly calculated sum insured back on the screen.*/
		if (plan.isTrue()) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, sv.numapp), sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		/*BRD-306 END */
		/* Having calculated it, the  entered value, if any, is compared*/
		/* with it to check that  it  is within acceptable limits of the*/
		/* automatically calculated figure.  If  it  is  less  than  the*/
		/* amount calculated and  within  tolerance  then  the  manually*/
		/* entered amount is allowed.  If  the entered value exceeds the*/
		/* calculated one, the calculated value is used.*/
		if (isEQ(sv.singlePremium, 0)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		if (isGTE(sv.singlePremium, premiumrec.calcPrem)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		/* check the tolerance amount against the table entry read above.  */
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.singlePremium));
		sv.singprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.singprmErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2800();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.singprmErr, SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.singlePremium, sv.zlinstprem));
		}
		goTo(GotoLabel.exit2790);
	}

protected void calc2720()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtlnbIO.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("01");
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(sv.premcess);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsaaSex2);
		premiumrec.jlage.set(wsaaAnbAtCcd2);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(payrIO.getCntcurr());
		/*  (wsaa-sumin already adjusted for plan processing)              */
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		/* MOVE PAYR-BILLFREQ          TO CPRM-BILLFREQ.        <A05691>*/
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			premiumrec.billfreq.set("00");
		}
		else {
			premiumrec.billfreq.set(payrIO.getBillfreq());
		}
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(sv.singlePremium);
		premiumrec.calcBasPrem.set(sv.singlePremium);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.commissionPrem.set(ZERO);   //IBPLIFE-5237
		if (plan.isTrue()) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), sv.numapp)));
		}
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			premiumrec.cnttype.set(chdrlnbIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec);
			/*ILIFE-8537 - Starts*/
			premiumrec.commTaxInd.set("Y");
			premiumrec.prevSumIns.set(ZERO);
			clntDao = DAOFactory.getClntpfDAO();
			clntpf=clntDao.getClientByClntnum(cltsIO.getClntnum().toString());
			if(null!=clntpf && null!=clntpf.getOldclntstatecd() && !clntpf.getOldclntstatecd().isEmpty()){
				premiumrec.stateAtIncep.set(clntpf.getOldclntstatecd());
			}else{
				premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
			}
			stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
			if(stampDutyflag){
				clnt=clntDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifelnbIO.getLifcnum().toString());
				if(clnt.getClntStateCd()!= null){
					stateCode=clnt.getClntStateCd().substring(3).trim();
				}
				premiumrec.rstate01.set(stateCode);
			}
			premiumrec.inputPrevPrem.set(ZERO);
			premiumrec.setPmexCall.set("Y");
			/*ILIFE-8537 - Ends*/
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			if (isEQ(premiumrec.statuz, varcom.bomb)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			if (isNE(premiumrec.statuz, varcom.oK)) {
				sv.singprmErr.set(premiumrec.statuz);
				return ;
			}
			
			//ILIFE-7845
			premiumrec.riskPrem.set(ZERO);
			riskPremflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), RISKPREM_FEATURE_ID, appVars, "IT");
			if (riskPremflag) {	
				premiumrec.cnttype.set(chdrlnbIO.getCnttype());
				premiumrec.crtable.set(covtlnbIO.getCrtable());
				premiumrec.calcTotPrem.set(add(premiumrec.zstpduty01, premiumrec.calcPrem));
			if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
			{
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
			}
			}	
			//ILIFE-7845
		}
		/*Ticket #IVE-792 - End		*/	
		/*Ticket #ILIFE-2005 - End*/
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.singprmErr.set(premiumrec.statuz);
			return ;
		}
		/* Adjust premium calculated for plan processing.                  */
		
		wsaaCommissionPrem.set(premiumrec.commissionPrem);
		
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.numapp), sv.polinc)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.numapp), sv.polinc)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.numapp), sv.polinc)));
		}
		/* Put possibly calculated sum insured back on the screen.         */
		if (plan.isTrue()) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, sv.numapp), sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		/*    MOVE CPRM-CALC-PREM         TO S6326-SINGLE-PREMIUM.    <026>*/
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		if (isEQ(sv.singlePremium, 0)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			return ;
		}
		if (isGTE(sv.singlePremium, premiumrec.calcPrem)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			return ;
		}
		/* check the tolerance amount against the table entry read above.  */
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.singlePremium));
		sv.singprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.singprmErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2800();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.singprmErr, SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.singlePremium, sv.zlinstprem));
		}
	}

protected void searchForTolerance2800()
	{
		/*  Calculate tolerance Limit and Check whether it is greater    */
		/*  than the maximum tolerance amount in table T5667.            */
		if (isEQ(payrIO.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
			if (isLTE(wsaaDiff, wsaaTol)
			&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
				/*         MOVE SPACES          TO S6326-SINGPRM-ERR.    <V42013>*/
				sv.singprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()], 0)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
				if (isLTE(wsaaDiff, wsaaTol)
				&& isLTE(wsaaDiff, t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.singprmErr.set(SPACES);
				}
			}
		}
		/*EXIT*/
	}

protected void calcUndwAge2900()
	{
		start2910();
	}

protected void start2910()
	{
		/* If the proposal is joint-life case, determine the adjusted      */
		/* combined age, using T5585.                                      */
		if (singlif.isTrue()) {
			wsaaUndwAge.set(wsaaAnbAtCcd);
			return ;
		}
		/* Must be Joint-life so get combined age                          */
		/* Read T5585 to get Joint Life Age parameters                     */
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5585);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(covtlnbIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(covtlnbIO.getCrtable(), itdmIO.getItemitem())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5585)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getCrtable());
			syserrrec.statuz.set(errorsInner.f261);
			fatalError600();
		}
		t5585rec.t5585Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
		/* Adjust the lives                                                */
		wsaaSub00.set(ZERO);
		wsaaSub01.set(ZERO);
		wsaaAge00Adjusted.set(wsaaAnbAtCcd);
		wsaaAge01Adjusted.set(wsaaAnbAtCcd2);
		if (isEQ(wsaaSex, t5585rec.sexageadj)) {
			adjustAge002a00();
		}
		if (isEQ(wsaaSex2, t5585rec.sexageadj)) {
			adjustAge012b00();
		}
		compute(wsaaAgeDifference, 0).set(sub(wsaaAge00Adjusted, wsaaAge01Adjusted));
		wsaaSub.set(0);
		ageDifferance2c00();
		wsaaUndwAge.set(wsaaAdjustedAge);
	}

protected void adjustAge002a00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust2a00: 
		adjust2a00();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust2a00()
	{
		wsaaSub00.add(1);
		if (isGT(wsaaSub00, 9)) {
			syserrrec.statuz.set(errorsInner.h043);
			return ;
		}
		if (isLTE(wsaaAnbAtCcd, t5585rec.agelimit[wsaaSub00.toInt()])) {
			compute(wsaaAge00Adjusted, 0).set(add(wsaaAnbAtCcd, t5585rec.ageadj[wsaaSub00.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust2a00);
		}
		/*A00-EXIT*/
	}

protected void adjustAge012b00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust2b00: 
		adjust2b00();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust2b00()
	{
		wsaaSub01.add(1);
		if (isGT(wsaaSub01, 9)) {
			premiumrec.statuz.set(errorsInner.h043);
			return ;
		}
		if (isLTE(wsaaAnbAtCcd2, t5585rec.agelimit[wsaaSub01.toInt()])) {
			compute(wsaaAge01Adjusted, 0).set(add(wsaaAnbAtCcd2, t5585rec.ageadj[wsaaSub01.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust2b00);
		}
		/*B00-EXIT*/
	}

protected void ageDifferance2c00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case go2c00: 
		go2c00();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go2c00()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 18)) {
			syserrrec.statuz.set(errorsInner.f262);
			return ;
		}
		if (isGT(wsaaAgeDifference, t5585rec.agedif[wsaaSub.toInt()])) {
			goTo(GotoLabel.go2c00);
		}
		if (isEQ(t5585rec.hghlowage, "H")) {
			if (isGT(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
		if (isEQ(t5585rec.hghlowage, "L")) {
			if (isLTE(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
	}

protected void calculateTerm5600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					mattrm5601();
				case loop5615: 
					loop5615();
				case premCessTerm5650: 
					premCessTerm5650();
				case loop5652: 
					loop5652();
				case exit5699: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void mattrm5601()
	{
		wsaaAnbatccd.set(sv.anbAtCcd);
		if (isEQ(sv.mattrm, ZERO)) {
			goTo(GotoLabel.premCessTerm5650);
		}
		compute(wsaaS6326Matage, 0).set(add(sv.mattrm, wsaaAnbatccd));
		/*CHECK-RANGE*/
		sub3.set(0);
	}

protected void loop5615()
	{
		sub3.add(1);
		if (isGT(sub3, wsaaMaxOcc)) {
			goTo(GotoLabel.premCessTerm5650);
		}
		if ((isEQ(t5551rec.maturityAgeTo[sub3.toInt()], 0))
		&& (isEQ(t5551rec.maturityAgeFrom[sub3.toInt()], 0))) {
			goTo(GotoLabel.loop5615);
		}
		if ((isGT(wsaaS6326Matage, t5551rec.maturityAgeTo[sub3.toInt()]))
		|| (isLT(wsaaS6326Matage, t5551rec.maturityAgeFrom[sub3.toInt()]))) {
			sv.mattrmErr.set(errorsInner.g330);
			goTo(GotoLabel.loop5615);
		}
		else {
			sv.mattrmErr.set(SPACES);
			goTo(GotoLabel.premCessTerm5650);
		}
	}

protected void premCessTerm5650()
	{
		if (isEQ(sv.premCessTerm, ZERO)) {
			goTo(GotoLabel.exit5699);
		}
		compute(wsaaS6326PremCessAge, 0).set(add(sv.premCessTerm, wsaaAnbatccd));
		/*CHECK-RANGE*/
		sub4.set(0);
	}

protected void loop5652()
	{
		sub4.add(1);
		if (isGT(sub4, wsaaMaxOcc)) {
			return ;
		}
		if ((isEQ(t5551rec.premCessageTo[sub4.toInt()], 0))
		&& (isEQ(t5551rec.premCessageFrom[sub4.toInt()], 0))) {
			goTo(GotoLabel.loop5652);
		}
		if ((isGT(wsaaS6326PremCessAge, t5551rec.premCessageTo[sub4.toInt()]))
		|| (isLT(wsaaS6326PremCessAge, t5551rec.premCessageFrom[sub4.toInt()]))) {
			sv.pcesstrmErr.set(errorsInner.g330);
			goTo(GotoLabel.loop5652);
		}
		else {
			sv.pcesstrmErr.set(SPACES);
			return ;
		}
	}

protected void calculateAge5700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					matage5701();
				case loop5715: 
					loop5715();
				case premCessAge5750: 
					premCessAge5750();
				case loop5752: 
					loop5752();
				case exit5799: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void matage5701()
	{
		wsaaAnbatccd.set(sv.anbAtCcd);
		if (isEQ(sv.matage, ZERO)) {
			goTo(GotoLabel.premCessAge5750);
		}
		compute(wsaaS6326Mattrm, 0).set(sub(sv.matage, wsaaAnbatccd));
		/*CHECK-RANGE*/
		sub1.set(0);
	}

protected void loop5715()
	{
		sub1.add(1);
		if (isGT(sub1, wsaaMaxOcc)) {
			goTo(GotoLabel.premCessAge5750);
		}
		if ((isEQ(t5551rec.maturityTermTo[sub1.toInt()], 0))
		&& (isEQ(t5551rec.maturityTermFrom[sub1.toInt()], 0))) {
			goTo(GotoLabel.loop5715);
		}
		if ((isGT(wsaaS6326Mattrm, t5551rec.maturityTermTo[sub1.toInt()]))
		|| (isLT(wsaaS6326Mattrm, t5551rec.maturityTermFrom[sub1.toInt()]))) {
			sv.matageErr.set(errorsInner.g330);
			goTo(GotoLabel.loop5715);
		}
		else {
			sv.matageErr.set(SPACES);
			goTo(GotoLabel.premCessAge5750);
		}
	}

protected void premCessAge5750()
	{
		if (isEQ(sv.premCessAge, ZERO)) {
			goTo(GotoLabel.exit5799);
		}
		compute(wsaaS6326PremCessTerm, 0).set(sub(sv.premCessAge, wsaaAnbatccd));
		/*CHECK-RANGE*/
		sub2.set(0);
	}

protected void loop5752()
	{
		sub2.add(1);
		if (isGT(sub2, wsaaMaxOcc)) {
			return ;
		}
		if ((isEQ(t5551rec.premCesstermTo[sub2.toInt()], 0))
		&& (isEQ(t5551rec.premCesstermFrom[sub2.toInt()], 0))) {
			goTo(GotoLabel.loop5752);
		}
		if ((isGT(wsaaS6326PremCessTerm, t5551rec.premCesstermTo[sub2.toInt()]))
		|| (isLT(wsaaS6326PremCessTerm, t5551rec.premCesstermFrom[sub2.toInt()]))) {
			sv.pcessageErr.set(errorsInner.g330);
			goTo(GotoLabel.loop5752);
		}
		else {
			sv.pcessageErr.set(SPACES);
			return ;
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loadWsspFields3010();
					checkCredits3020();
					getUnderwriting3030();
					if (isFollowUpRequired) {
						fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
						fupno = fluppfAvaList.size();	//ICIL-1494
						createFollowUps3400();
					}
				case keep3400: 
					keep3400();
				case exit3490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}


protected void createFollowUps3400()
{
	try {
		writeFollowUps3430();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void writeFollowUps3430()
{
	for (wsaaCount.set(1); !(isGT(wsaaCount, 5)) && isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)) {
		boolean entryFlag = false;
		//ILIFE-9209 STARTS
		if(fluppfAvaList!=null && !fluppfAvaList.isEmpty()) {
			for (Fluppf fluppfdata : fluppfAvaList) {
				if(isEQ(fluppfdata.getFupCde(),ta610rec.fupcdes[wsaaCount.toInt()])) 
					entryFlag=true;
			}
				
		}
		//ILIFE-9209 ENDS
		if(!entryFlag) {		//ILIFE-9209 
		writeFollowUp3500();
		}
	}

fluppfDAO.insertFlupRecord(fluplnbList);
}

protected void writeFollowUp3500()
{
	try {
		fluppf = new Fluppf();
		lookUpStatus3510();
		lookUpDescription3520();
		writeRecord3530();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void lookUpStatus3510() {
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
{ 
	descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());

}

protected void writeRecord3530()
{
	fupno++;
	
	fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
	fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	setPrecision(fupno, 0);
	fluppf.setFupNo(fupno);
	fluppf.setLife("01");
	fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
	fluppf.setFupDt(wsaaToday.toInt());
	fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString());
	fluppf.setFupTyp('P');
	fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	fluppf.setFupRmk(descpf.getLongdesc());/* IJTI-1523 */
	fluppf.setjLife("00");
	fluppf.setTrdt(varcom.vrcmDate.toInt());
	fluppf.setTrtm(varcom.vrcmTime.toInt());
	fluppf.setUserT(varcom.vrcmUser.toInt());
	fluppf.setEffDate(wsaaToday.toInt());
	fluppf.setCrtDate(wsaaToday.toInt());
	fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
	fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
	fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
	fluplnbList.add(fluppf);
}
protected void loadWsspFields3010()
	{
		/*     SPECIAL EXIT PROCESSING                                   **/
		/* Skip this section  if  F11 (kill) key pressed*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| forcedKill.isTrue()) {
			goTo(GotoLabel.exit3490);
		}
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| isEQ(sv.taxind, "X")
		||	isEQ(sv.optind, "X")
    	|| isEQ(singlepremiumind, "X")) {//BRD-NBP-012-STARTS /* ILIFE-7805 */
			goTo(GotoLabel.exit3490);
		}
		/* Updating occurs with  the Creation, Deletion or Updating of a*/
		/* COVTUNL transaction record.*/
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.keep3400);
		}
		/*       GO TO 3490-EXIT.*/
		/* If OPTIONS / EXTRAS selected do not write / update or delete*/
		/* record at this stage.*/
		/* Only bypass the update of the component record if            */
		/* the Reassurance check box has not been selected as           */
		/* component details are required by the Reassurance            */
		/* program.                                                     */
		if (isEQ(sv.optextind, "X")
		&& isNE(sv.ratypind, "X")) {
			goTo(GotoLabel.exit3490);
		}
	}

protected void checkCredits3020()
	{
		/* Before  updating any  records,  calculate  the  new  "credit"*/
		/* amount.*/
		/*  - add to  the  current  credit  amount,  the  number  of*/
		/*       policies previously  applicable  from  the  COVTUNL*/
		/*       record (zero if  there was no COVTUNL) and subtract*/
		/*       the number applicable entered on the screen.*/
		/*  - a positive  credit means that additional policies must*/
		/*       be added to the plan.*/
		/*  - a negative  credit  means  that  some policies must be*/
		/*       removed from the plan.*/
		if (nonfirst.isTrue()) {
			wsaaCredit.add(covtunlIO.getNumapp());
		}
		wsaaCredit.subtract(sv.numapp);
	}

protected void getUnderwriting3030()
	{
		/* Before  updating any  records,  determine the Underwriting      */
		/* Rule for the Component.                                         */
		if (underwritingReqd.isTrue()
		&& isGT(sv.sumin, 0)) {
			getUndwRule3900();
		}
		/* If the number  applicable  is greater than zero, write/update*/
		/* the COVTUNL record.  If the number applicable is zero, delete*/
		/* the COVTUNL record.*/
		wsaaSaveSeqnbr.set(covtunlIO.getSeqnbr());
		covtunlIO.setDataKey(covtlnbIO.getDataKey());
		covtunlIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		covtunlIO.setSeqnbr(wsaaSaveSeqnbr);
		if (isGT(sv.numapp, 0)) {
			if (firsttime.isTrue()) {
				initcovr3500();
				setupcovtunl3550();
				//BRD-NBP-012-STARTS
				if (isEQ(sv.optind, "+")){				
					covtunlIO.setGmib(GMIB);
					covtunlIO.setGmdb(GMDB);
					covtunlIO.setGmwb(GMWB);
				}
				//BRD-NBP-012-ENDS
				covtunlIO.setFunction(varcom.writr);
				wsaaCtrltime.set("N");
				initundc3600();
				undcIO.setFunction(varcom.writr);
			}
			else {
				setupcovtunl3550();
				//BRD-NBP-012-STARTS
				if (isEQ(sv.optind, "+")){
					covtunlIO.setGmib(GMIB);
					covtunlIO.setGmdb(GMDB);
					covtunlIO.setGmwb(GMWB);
				}
				//BRD-NBP-012-ENDS
				/*        MOVE UPDAT            TO COVTUNL-FUNCTION.             */
				/*       MOVE UPDAT            TO COVTUNL-FUNCTION.             */
				covtunlIO.setFunction(varcom.updat);
				undcIO.setFunction(varcom.updat);
			}
		}
		if (isEQ(sv.numapp, 0)) {
			/*       IF COVTUNL-NUMAPP = 0*/
			/*          GO TO 3490-EXIT*/
			/*       ELSE*/
			covtunlIO.setChdrcoy(covtlnbIO.getChdrcoy());
			covtunlIO.setChdrnum(covtlnbIO.getChdrnum());
			covtunlIO.setLife(covtlnbIO.getLife());
			covtunlIO.setCoverage(covtlnbIO.getCoverage());
			covtunlIO.setRider(covtlnbIO.getRider());
			covtunlIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covtunlIO);
			if (isNE(covtunlIO.getStatuz(), varcom.oK)
			&& isNE(covtunlIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(covtunlIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(covtunlIO.getStatuz(), varcom.mrnf)) {
					goTo(GotoLabel.exit3490);
				}
				else {
					covtunlIO.setFunction(varcom.delet);
					if (isEQ(wsaaFirstSeqnbr, covtunlIO.getSeqnbr())) {
						wsaaFirstSeqnbr.set(ZERO);
					}
				}
			}
		}
		updateCovtunl3700();

	
		/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts */
		if (isEQ(t5687rec.zsredtrm, "Y") && ispermission) {
			m800ReadMrta();
			if (isNE(wsaaPremind, "Y")) {
				m300GetMethod();
				rlrdtrmrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
				rlrdtrmrec.chdrnum.set(chdrlnbIO.getChdrnum());
				rlrdtrmrec.life.set(covtunlIO.getLife());
				rlrdtrmrec.coverage.set(covtunlIO.getCoverage());
				rlrdtrmrec.rider.set(covtunlIO.getRider());
				rlrdtrmrec.sumins.set(sv.sumin);
				if (isEQ(sv.mattrm, ZERO)) {
						compute(rlrdtrmrec.riskTerm, 0).set(sub(sv.matage, sv.anbAtCcd));
				}
				else {
					rlrdtrmrec.riskTerm.set(sv.mattrm);
				}
				m500DeleteBenefitSchedule();
				m400CalcBenefitSchedule();
				
			}
		} 


		/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/

		if (isGT(sv.sumin, 0)
		&& underwritingReqd.isTrue()
		&& isEQ(wsaaT6768Found, "Y")) {
			updateUndc3800();
		}
	}
/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
protected void m100BenefitSchdExe() 
{
	m100Start1();
}

protected void m100Start1()

{
	wsspCurrfrom.set(chdrlnbIO.getOccdate());	
	sv.optsmode.set("?");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
		save4510();
	}
	gensswrec.function.set("O");
	gensww4210();
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
		load4530();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}

protected void m200BenefitSchdRet() 
{
	/*M200-START*/
	sv.optsmode.set("+");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		restore4610();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
	/*M200-EXIT*/
}
protected void m300GetMethod() 
{
	m300Start();
}

protected void m300Start() 
{
	itemIO.setDataKey(SPACES);
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setItemtabl(tablesInner.t6598);
	itemIO.setItempfx("IT");
	itemIO.setItemitem(t5687rec.zsbsmeth);
	itemIO.setFunction(varcom.readr);
	
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		t6598rec.t6598Rec.set(SPACES);
	}
	else {
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}
	wsaaSubprog.set(t6598rec.calcprog);
}

protected void m400CalcBenefitSchedule() 
{
	/*M400-START1*/
	callProgram(wsaaSubprog, rlrdtrmrec.rlrdtrmRec);
	if (isEQ(rlrdtrmrec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(rlrdtrmrec.statuz);
		fatalError600();
	}
	/*M400-EXIT1*/
}

protected void m500DeleteBenefitSchedule() 
{
	/*M500-START1*/
	mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
	mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
	mbnsIO.setLife(covtunlIO.getLife());
	mbnsIO.setCoverage(covtunlIO.getCoverage());
	mbnsIO.setRider(covtunlIO.getRider());
	mbnsIO.setYrsinf(ZERO);
	mbnsIO.setFormat(formatsInner.mbnsrec);
	mbnsIO.setFunction(varcom.begn);
	mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

	while ( !(isEQ(mbnsIO.getStatuz(), varcom.endp))) {
		m600DeleteRecord();
	}
	
	/*M500-EXIT1*/
}

protected void m600DeleteRecord() 
{
	m600Start1();
}

protected void m600Start1() 
{
	SmartFileCode.execute(appVars, mbnsIO);
	if (isNE(mbnsIO.getStatuz(), varcom.oK)
	&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
		syserrrec.statuz.set(mbnsIO.getStatuz());
		fatalError600();
	}
	if (isNE(chdrlnbIO.getChdrcoy(), mbnsIO.getChdrcoy())
	|| isNE(chdrlnbIO.getChdrnum(), mbnsIO.getChdrnum())
	|| isNE(covtunlIO.getLife(), mbnsIO.getLife())
	|| isNE(covtunlIO.getCoverage(), mbnsIO.getCoverage())
	|| isNE(covtunlIO.getRider(), mbnsIO.getRider())) {
		mbnsIO.setStatuz(varcom.endp);
	}
	if (isEQ(mbnsIO.getStatuz(), varcom.endp)) {
		return ;
	}
	mbnsIO.setFunction(varcom.delet);
	SmartFileCode.execute(appVars, mbnsIO);
	if (isNE(mbnsIO.getStatuz(), varcom.oK)) {
		syserrrec.statuz.set(mbnsIO.getStatuz());
		fatalError600();
	}
	mbnsIO.setFunction(varcom.nextr);
}

protected void m800ReadMrta() 
{
	m800Start1();
}

protected void m800Start1() 
{
	mrtaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
	mrtaIO.setChdrnum(chdrlnbIO.getChdrnum());
	mrtaIO.setFormat(formatsInner.mrtarec);
	mrtaIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, mrtaIO);
	if (isNE(mrtaIO.getStatuz(), varcom.oK)
	&& isNE(mrtaIO.getStatuz(), varcom.mrnf)) {
		syserrrec.statuz.set(mrtaIO.getStatuz());
		syserrrec.params.set(mrtaIO.getParams());
		fatalError600();
	}
	if (isNE(mrtaIO.getStatuz(), varcom.oK)) {
		return ;
	}
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itemIO.setItemtabl(tablesInner.th615);
	itemIO.setItemitem(mrtaIO.getMlresind());
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)
	|| isNE(itemIO.getStatuz(), varcom.oK)) {
		syserrrec.statuz.set(itemIO.getStatuz());
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	th615rec.th615Rec.set(itemIO.getGenarea());
	wsaaPremind.set(th615rec.premind);
}

/*ILIFE- IL_BASE_ITEM-75-Base development ends*/
protected void keep3400()
	{
		covtunlIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		//ILIFE-8123 
		rolloverFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), ROLLOVER_FEATURE_ID, appVars, "IT"); 
		 if( rolloverFlag && isEQ(sv.singpremtype,"CTX")){
			 updateRlvr();
		 }
		
	}

	//ILIFE-8123-starts
	private void updateRlvr() {
		rlvrpf = new Rlvrpf();
		rlvrpf.setChdrcoy(covtlnbIO.getChdrcoy().toString());
		rlvrpf.setChdrnum(sv.chdrnum.toString());
		rlvrpf.setValidFlag("2");
		rlvrpf.setLife(covtlnbIO.getLife().toString());
		rlvrpf.setCoverage(covtlnbIO.getCoverage().toString());
		rlvrpfDAO.updateRollovRecord(rlvrpf);
	}

	//ILIFE-8123-ends
	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void initcovr3500()
	{
		/*PARA*/
		/* Before creating a  new COVTUNL record initialise the coverage*/
		/* fields.*/
		covtunlIO.setCrtable(covtlnbIO.getCrtable());
		covtunlIO.setPayrseqno(1);
		covtunlIO.setRiskCessAge(0);
		covtunlIO.setPremCessAge(0);
		covtunlIO.setBenCessAge(0);
		covtunlIO.setRiskCessTerm(0);
		covtunlIO.setPremCessTerm(0);
		covtunlIO.setBenCessTerm(0);
		covtunlIO.setInstprem(0);
		covtunlIO.setZbinstprem(0);
		covtunlIO.setZlinstprem(0);
		covtunlIO.setSingp(0);
		covtunlIO.setSumins(0);
		covtunlIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtunlIO.setPremCessDate(varcom.vrcmMaxDate);
		covtunlIO.setBenCessDate(varcom.vrcmMaxDate);
		covtunlIO.setReserveUnitsDate(varcom.vrcmMaxDate);
		covtunlIO.setEffdate(varcom.vrcmMaxDate);
		covtunlIO.setAnbccd(2, ZERO);
		covtunlIO.setSex(2, SPACES);
		covtunlIO.setCntcurr(chdrlnbIO.getCntcurr());
		/*EXIT*/
	}

protected void setupcovtunl3550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3550();
				case cont3555: 
					cont3555();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3550()
	{
		/* Check for changes in the COVTUNL Record.*/
		if (isNE(sv.mattcess, covtunlIO.getRiskCessDate())) {
			covtunlIO.setRiskCessDate(sv.mattcess);
		}
		if (isNE(sv.premcess, covtunlIO.getPremCessDate())) {
			covtunlIO.setPremCessDate(sv.premcess);
		}
		if (isNE(sv.matage, covtunlIO.getRiskCessAge())) {
			covtunlIO.setRiskCessAge(sv.matage);
		}
		if (isNE(sv.premCessAge, covtunlIO.getPremCessAge())) {
			covtunlIO.setPremCessAge(sv.premCessAge);
		}
		if (isNE(sv.mattrm, covtunlIO.getRiskCessTerm())) {
			covtunlIO.setRiskCessTerm(sv.mattrm);
		}
		if (isNE(sv.premCessTerm, covtunlIO.getPremCessTerm())) {
			covtunlIO.setPremCessTerm(sv.premCessTerm);
		}
		if (isNE(sv.sumin, covtunlIO.getSumins())) {
			covtunlIO.setSumins(sv.sumin);
		}
		if (isNE(sv.zbinstprem, covtunlIO.getZbinstprem())) {
			covtunlIO.setZbinstprem(sv.zbinstprem);
		}
		if (isNE(sv.zlinstprem, covtunlIO.getZlinstprem())) {
			covtunlIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtunlIO.setLoadper(sv.loadper);
			covtunlIO.setRateadj(sv.rateadj);
			covtunlIO.setFltmort(sv.fltmort);
			covtunlIO.setPremadj(sv.premadj);
			/*BRD-306 END */
		}
		if(isNE(wsaaCommissionPrem,ZERO)){
			covtunlIO.setcommPrem(wsaaCommissionPrem);              //IBPLIFE-5237
		}
		if (isNE(stateCode, covtunlIO.getZclstate())&& stampDutyflag) {
			covtunlIO.setZclstate(stateCode);
		}
		/*    Single Premium Componant                                     */
		/*IF CHDRLNB-BILLFREQ  = '00'                             <001>*/
		/* IF PAYR-BILLFREQ = '00'                             <A05691>*/
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			if (isNE(sv.singlePremium, covtunlIO.getSingp())) {
				covtunlIO.setSingp(sv.singlePremium);
			}
			goTo(GotoLabel.cont3555);
		}
		/*       GO TO 3555-CONT                                <SPLPRM>*/
		/*    ELSE                                              <SPLPRM>*/
		/*       GO TO 3555-CONT.                               <SPLPRM>*/
		/*    Regular Premium Componant                                    */
		if (isNE(sv.singlePremium, covtunlIO.getInstprem())) {
			covtunlIO.setInstprem(sv.singlePremium);
		}
		/*    Possible Lump Sum!*/
		if (isNE(sv.lumpContrib, covtunlIO.getSingp())) {
			covtunlIO.setSingp(sv.lumpContrib);
		}
	}

protected void cont3555()
	{
		if (isNE(sv.mortcls, covtunlIO.getMortcls())) {
			covtunlIO.setMortcls(sv.mortcls);
		}
		if (isNE(sv.liencd, covtunlIO.getLiencd())) {
			covtunlIO.setLiencd(sv.liencd);
		}
		if (isNE(sv.polinc, covtunlIO.getPolinc())) {
			covtunlIO.setPolinc(sv.polinc);
		}
		if (isNE(sv.reserveUnitsInd, covtunlIO.getReserveUnitsInd())) {
			covtunlIO.setReserveUnitsInd(sv.reserveUnitsInd);
		}
		if (isNE(sv.reserveUnitsDate, covtunlIO.getReserveUnitsDate())) {
			covtunlIO.setReserveUnitsDate(sv.reserveUnitsDate);
		}
		if (isNE(sv.numapp, covtunlIO.getNumapp())) {
			covtunlIO.setNumapp(sv.numapp);
		}
		/*IF CHDRLNB-BILLFREQ         NOT = COVTUNL-BILLFREQ           */
		/*   MOVE CHDRLNB-BILLFREQ    TO COVTUNL-BILLFREQ              */
		/* IF PAYR-BILLFREQ            NOT = COVTUNL-BILLFREQ   <A05691>*/
		/*    MOVE PAYR-BILLFREQ       TO COVTUNL-BILLFREQ      <A05691>*/
		/*    MOVE 'Y'                 TO WSAA-UPDATE-FLAG.             */
		/* If the bill frequency has changed and this is a single premium  */
		/* premium component set the COVTUNL Frequency to '00',            */
		/* otherwise set it the PAYR bill frequency.                       */
		if (isNE(payrIO.getBillfreq(), covtunlIO.getBillfreq())) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				covtunlIO.setBillfreq("00");
			}
			else {
				covtunlIO.setBillfreq(payrIO.getBillfreq());
			}
		}
		/*IF CHDRLNB-BILLCHNL         NOT = COVTUNL-BILLCHNL           */
		/*   MOVE CHDRLNB-BILLCHNL    TO COVTUNL-BILLCHNL              */
		if (isNE(payrIO.getBillchnl(), covtunlIO.getBillchnl())) {
			covtunlIO.setBillchnl(payrIO.getBillchnl());
		}
		if (isNE(chdrlnbIO.getOccdate(), covtunlIO.getEffdate())) {
			covtunlIO.setEffdate(chdrlnbIO.getOccdate());
		}
		if (isNE(wsaaAnbAtCcd, covtunlIO.getAnbccd(1))) {
			covtunlIO.setAnbccd(1, wsaaAnbAtCcd);
		}
		if (isNE(wsaaSex, covtunlIO.getSex(1))) {
			covtunlIO.setSex(1, wsaaSex);
		}
		if (isEQ(sv.jlifcnum, "NONE")) {
			covtunlIO.setAnbccd(2, ZERO);
			covtunlIO.setSex(2, SPACES);
			return ;
		}
		if (isNE(wsaaAnbAtCcd2, covtunlIO.getAnbccd(2))) {
			covtunlIO.setAnbccd(2, wsaaAnbAtCcd2);
		}
		if (isNE(wsaaSex2, covtunlIO.getSex(2))) {
			covtunlIO.setSex(2, wsaaSex2);
		}
		/*      The following line is only required to update existing    */
		/*      records new ones created from today should have been      */
		/*      initialised to 1 in the first place.                      */
		if (isNE(covtunlIO.getPayrseqno(), 1)) {
			covtunlIO.setPayrseqno(1);
		}
		/*ILIFE-7805 - Start*/
		if (isNE(sv.singpremtype, covtunlIO.getSingpremtype())) {
			covtunlIO.setSingpremtype(sv.singpremtype);
		}
		/*ILIFE-7805 - End*/
	}

protected void initundc3600()
	{
		/*PARA*/
		/* Before creating a  new Underwriting Component record            */
		/* initialize the fields.                                          */
		if (isEQ(sv.sumin, 0)) {
			return ;
		}
		undcIO.setChdrnum(ZERO);
		undcIO.setChdrcoy(ZERO);
		undcIO.setLife(ZERO);
		undcIO.setCoverage(ZERO);
		undcIO.setRider(ZERO);
		undcIO.setCurrfrom(ZERO);
		undcIO.setCurrto(varcom.vrcmMaxDate);
		undcIO.setUndwrule(SPACES);
		undcIO.setSpecind(SPACES);
		undcIO.setTranno(1);
		undcIO.setValidflag("0");
		/*EXIT*/
	}

protected void updateCovtunl3700()
	{
		para3710();
	}

protected void para3710()
	{

//	ILIFE-8098 Start
	zctxpfList=zctxpfDAO.readContributionTaxList(covtunlIO.getChdrcoy().toString(),covtunlIO.getChdrnum().toString());
	if(zctxpfList.size()>0){
    zctxpf = zctxpfList.get(0);
		if(!StringUtils.isEmpty(sv.singpremtype) && sv.singpremtype.toString().equalsIgnoreCase("ROP")){
			zctxpf.setAmount((sub(zctxpf.getAmount(),sv.singlePremium)).getbigdata());
			 if(zctxpf.getAmount().intValue()<=0){
					zctxpf.setAmount(BigDecimal.ZERO);
				}
			zctxpf.setZsgtamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZsgtpct()),zctxpf.getZsgtamt())).getbigdata());
			zctxpf.setZoeramt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZoerpct()),zctxpf.getZoeramt())).getbigdata());
			zctxpf.setZdedamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZdedpct()),zctxpf.getZdedamt())).getbigdata());
			zctxpf.setZundamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZundpct()),zctxpf.getZundamt())).getbigdata());
			zctxpf.setZspsamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZspspct()),zctxpf.getZspsamt())).getbigdata());
			zctxpf.setZslrysamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZslryspct()),zctxpf.getZslrysamt())).getbigdata());
			zctxpf.setRollflag("N");
			zctxpfDAO.updateAmount(zctxpf);
			
	}else{
		covtpflist = covtpfDAO.getSingpremtype(wsspcomn.company.toString(),sv.chdrnum.toString());
if(covtpflist.size()>0){
	covtpf = covtpflist.get(0);
			if(covtpf.getSingpremtype().equalsIgnoreCase("ROP")){
				zctxpf.setAmount(covtpf.getSingp());
				zctxpf.setZsgtamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZsgtpct()),zctxpf.getZsgtamt())).getbigdata());
				zctxpf.setZoeramt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZoerpct()),zctxpf.getZoeramt())).getbigdata());
				zctxpf.setZdedamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZdedpct()),zctxpf.getZdedamt())).getbigdata());
				zctxpf.setZundamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZundpct()),zctxpf.getZundamt())).getbigdata());
				zctxpf.setZspsamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZspspct()),zctxpf.getZspsamt())).getbigdata());
				zctxpf.setZslrysamt((add(Calculateamt(zctxpf.getAmount(),zctxpf.getZslryspct()),zctxpf.getZslrysamt())).getbigdata()); 
				zctxpf.setRollflag("N");
				zctxpfDAO.updateAmount(zctxpf);
																}
			
			
								}
	}
		
	}	
	
	//	ILIFE-8098 End
	    covtunlIO.setTermid(varcom.vrcmTermid);
		covtunlIO.setUser(varcom.vrcmUser);
		covtunlIO.setTransactionDate(varcom.vrcmDate);
		covtunlIO.setTransactionTime(varcom.vrcmTime);
		covtunlIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtunlIO.setChdrnum(covtlnbIO.getChdrnum());
		covtunlIO.setLife(covtlnbIO.getLife());
		covtunlIO.setCoverage(covtlnbIO.getCoverage());
		covtunlIO.setRider(covtlnbIO.getRider());
		covtunlIO.setCntcurr(chdrlnbIO.getCntcurr());
		covtunlIO.setRiskprem(premiumrec.riskPrem);//ILIFE-7845
		covtunlIO.setFormat(formatsInner.covtunlrec);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
	}
protected BigDecimal Calculateamt(BigDecimal total,BigDecimal pct){
	BigDecimal amt=total.multiply(pct);
	BigDecimal num = new BigDecimal(100);
	amt=amt.divide(num);
	return amt;
	
}
protected void updateUndc3800()
	{
		para3810();
	}

protected void para3810()
	{
		undcIO.setChdrcoy(covtlnbIO.getChdrcoy());
		undcIO.setChdrnum(covtlnbIO.getChdrnum());
		undcIO.setLife(covtlnbIO.getLife());
		undcIO.setJlife(covtlnbIO.getJlife());
		undcIO.setCoverage(covtlnbIO.getCoverage());
		undcIO.setRider(covtlnbIO.getRider());
		undcIO.setCurrfrom(covtunlIO.getEffdate());
		undcIO.setCurrto(varcom.vrcmMaxDate);
		undcIO.setUndwrule(wsaaUndwrule);
		undcIO.setSpecind(SPACES);
		undcIO.setTranno(1);
		undcIO.setValidflag("3");
		undcIO.setFormat(formatsInner.undcrec);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			syserrrec.statuz.set(undcIO.getStatuz());
			fatalError600();
		}
	}

protected void getUndwRule3900()
	{
		start3910();
	}

	/**
	* <pre>
	* For the given Sum Assured and Age, read the Underwriting        
	* based on Age & SA table T6768.                                  
	* Read T6768 with a key of CRTABLE, CURRENCY and SEX, for the     
	* given coverage.                                                 
	* If the Proposal is a joint-life case, the table will be read    
	* with just CRTABLE and CURRENCY.                                 
	* T6768 holds a matrix of Underwriting rules held in a            
	* two-dimensional table with  Age as the vertical ('y') axis and  
	* SA as the horizontal ('x') axis.                                
	* Locate  the occurrence of Age that is greater than or equal to  
	* the  Age  Next  Birthday.                                       
	* Locate the occurrence of Term that is greater  than  or  equal  
	* to the Term.                                                    
	* Locate the appropriate method.                                  
	* </pre>
	*/
protected void start3910()
	{
		wsaaT6768Crtable.set(covtunlIO.getCrtable());
		wsaaT6768Curr.set(chdrlnbIO.getCntcurr());
		if (singlif.isTrue()) {
			wsaaT6768Sex.set(wsaaSex);
		}
		else {
			wsaaT6768Sex.set(SPACES);
		}
		/* Read T6768 to determine the underwriting rule based on the age  */
		/* of the life and the sum assured. This is not a straightforward  */
		/* read due to the table having two continuation items.            */
		itemIO.setDataKey(SPACES);
		wsaaItemtabl.set(tablesInner.t6768);
		wsaaItemitem.set(wsaaT6768Key);
		/* Work out the array's Y index based on AGE (WSAA-YA).            */
		wsaaYa.set(ZERO);
		wsaaZa.set(ZERO);
		wsaaYaFlag.set("N");
		while ( !(yaFound.isTrue()
		|| t6768NotFound.isTrue())) {
			determineYa3a00();
		}
		
		/* If the key does not exist on T6768 there will be no             */
		/* Age/Sum Assured underwriting rule.                              */
		if (t6768NotFound.isTrue()) {
			wsaaUndwrule.set(SPACES);
			return ;
		}
		/* Work out the array's X index based on SUMINS (WSAA-XA).         */
		wsaaXa.set(ZERO);
		wsaaXaFlag.set("N");
		while ( !(xaFound.isTrue())) {
			determineXa3b00();
		}
		
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the RULE.   */
		/* All we have to do now is to work out the corresponding position */
		/* in the copybook by using these co-ordinates.(The copybook is    */
		/* defined as one-dimensional.)                                    */
		wsaaYa.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaYa, 5));
		wsaaIndex.add(wsaaXa);
		wsaaUndwrule.set(t6768rec.undwrule[wsaaIndex.toInt()]);
	}

protected void determineYa3a00()
	{
		init3a10();
	}

protected void init3a10()
	{
		x100ReadItdm();
		if (t6768NotFound.isTrue()) {
			return ;
		}
		for (wsaaZa.set(1); !(isGT(wsaaZa, 10)); wsaaZa.add(1)){
			if (isLTE(wsaaUndwAge, t6768rec.undage[wsaaZa.toInt()])) {
				wsaaYa.set(wsaaZa);
				wsaaZa.set(11);
				yaFound.setTrue();
			}
		}
		if (yaFound.isTrue()) {
			return ;
		}
		/* If the age does not fall into the range specified and an age    */
		/* continuation item exists, read T6768 again using the            */
		/* continuation item as the key.                                   */
		if (isGT(wsaaZa, 10)) {
			if (isNE(t6768rec.agecont, SPACES)) {
				wsaaItemitem.set(t6768rec.agecont);
				x100ReadItdm();
			}
			else {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(errorsInner.e390);
				fatalError600();
			}
		}
	}

protected void determineXa3b00()
	{
		init3b10();
	}

protected void init3b10()
	{
		for (wsaaZa.set(1); !(isGT(wsaaZa, 5)); wsaaZa.add(1)){
			if (isLTE(sv.sumin, t6768rec.undsa[wsaaZa.toInt()])) {
				wsaaXa.set(wsaaZa);
				wsaaZa.set(6);
				xaFound.setTrue();
			}
		}
		if (xaFound.isTrue()) {
			return ;
		}
		/* If the sum assured does not fall into the range specified and a */
		/* sum assured continuation item exists, read T6768 again using    */
		/* continuation item as the key.                                   */
		if (isGT(wsaaZa, 5)) {
			if (isNE(t6768rec.sacont, SPACES)) {
				wsaaItemitem.set(t6768rec.sacont);
				x100ReadItdm();
			}
			else {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(errorsInner.e389);
				fatalError600();
			}
		}
	}

protected void whereNext4000()
	{
		para4000();
	}

protected void para4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/* Return to last marked program on stack if F11 (kill)*/
		/* key pressed*/
		if (forcedKill.isTrue()) {
			wsaaKillFlag.set("N");
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(sv.optextind, "X")) {
			optionsExe4500();
		}
		else {
			if (isEQ(sv.optextind, "?")) {
				optionsRet4600();
			}
			else {
				if (isEQ(sv.ratypind, "X")) {
					reassuranceExe4200();
				}
				else {
					if (isEQ(sv.ratypind, "?")) {
						reassuranceRet4300();
					}
					else { 
						if (isEQ(sv.optsmode, "X")) { 
							m100BenefitSchdExe();
						}
					else { 
						if (isEQ(sv.optsmode, "?")) { 
							m200BenefitSchdRet();
		 			    }


					else {
						if (isEQ(sv.taxind, "X")) {
							taxExe5100();
						}
						else {
							if (isEQ(sv.taxind, "?")) {
								taxRet5200();
							}
							//BRD-NBP-012-STARTS
							else {
								if (isEQ(sv.optind, "X")) {
									optind5300();
								}
								else {
									if (isEQ(sv.optind, "?")) {
										optind5400();
									}
									//BRD-NBP-012-ENDS
									//ILIFE-7805 - START 
								else { /* Set up switching for Single Premium Type 'Rollover'                      */
								if (singlepremiumind.equals("X")) {
									singlePremiumSwitch();
								}
								else {
									if (singlepremiumind.equals("?")) {
										singlePremiumSwitchReturn();
									}//ILIFE-7805 - END
							else {
								if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], " ")) {
									wayout4700();
								}
								else {
									wayin4800();
								}
								
							}
						}
							}
						}
							}
						}	
					} 
				}
			}
		}
	}
}		
	}
protected void reassuranceExe4200()
	{
		para4200();
	}

protected void para4200()
	{
		/*    If the reassurance details have been selected,(value - 'X'), */
		/*    then set an asterisk in the program stack action field to    */
		/*    ensure that control returns here, set the parameters for     */
		/*    generalised secondary switching and save the original        */
		/*    programs from the program stack.                             */
		/*    Set up WSSP-CURRFORM for next programs.              <R96REA>*/
		wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
		sv.ratypind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			save4510();
		}
		gensswrec.function.set("R");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call     
	*   the generalised secondary switching module to obtain the      
	*   next 8 programs and load them into the program stack.         
	* </pre>
	*/
protected void gensww4210()
	{
		para4211();
	}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*    MOVE V045                TO SCRN-ERROR-CODE          (017)*/
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
	}

protected void reassuranceRet4300()
	{
		/*PARA*/
		sv.ratypind.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* ADD 1               TO WSSP-PROGRAM-PTR.                     */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void optionsExe4500()
	{
		para4500();
	}

protected void para4500()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* Charge/Options request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the Charge/Options request indicator to '?',*/
		sv.optextind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'U' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		gensswrec.function.set("U");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/*  MOVE WSAA-PROG              TO WSSP-NEXTPROG.*/
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  Charge/Options  indicator  will  be  '?'.  To*/
		/* handle the return from Charge/Options:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.optextind.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		/* Only blank out the action and set the next program           */
		/* if the Reassurance check box has not been selected.          */
		/* MOVE ' '                 TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).*/
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
//		sv.optextind.set("+");
		checkLext1900();
		/* MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.                   */
		if (isNE(sv.ratypind, "X")) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		}
		/*EXIT*/
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4700();
				case cont4710: 
					cont4710();
				case cont4715: 
					cont4715();
				case cont4717: 
					cont4717();
				case cont4720: 
					cont4720();
				case exit4790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4700()
	{
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		/*    IF SCRN-STATUZ              = 'KILL'                         */
		/*       MOVE ' '              TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) */
		/*       GO TO 4790-EXIT.                                          */
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4710);
		}
		/*    If 'Enter' has been  pressed and "Credit" is non-zero*/
		/*    set the current select  action  field to '*', add 1*/
		/*    to the program pointer and exit.*/
		/*    Do not set the action field to '*' for enquiry as we do not  */
		/*    wish to return to this program if 'CREDIT is non-zero.       */
		if (isNE(wsaaCredit, 0)) {
			if (isNE(wsspcomn.flag, "I")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			}
		}
		/*    If 'Enter' has been  pressed and "Credit" is zero add*/
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4790);
	}

protected void cont4710()
	{
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has been changed set the current*/
		/*    select action field to '*',  add 1 to the program*/
		/*    pointer and exit.*/
		if (isNE(sv.numapp, covtunlIO.getNumapp())) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4790);
		}
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has not been changed then loop*/
		/*    round within the program and display the next /*/
		/*    previous screen as requested  (including BEGN).*/
		/*  Save the last used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covtunlIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covtunlIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4715);
			}
			else {
				getTheFirstRecord4750();
				goTo(GotoLabel.exit4790);
			}
		}
		/*    MOVED THIS CODE TO 4750-GET-THE-FIRST-RECORD            */
		/*MOVE BEGN              TO COVTUNL-FUNCTION              */
		/*CALL 'COVTUNLIO' USING COVTUNL-PARAMS                   */
		/*IF COVTUNL-STATUZ NOT = O-K                             */
		/*              AND NOT = ENDP                            */
		/*   MOVE COVTUNL-PARAMS TO SYSR-PARAMS                   */
		/*   PERFORM 600-FATAL-ERROR                              */
		/*ELSE                                                    */
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covtunlIO.setFunction(varcom.nextr);
		}
		else {
			covtunlIO.setFunction(varcom.nextp);
		}
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		if (isNE(covtunlIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtunlIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtunlIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtunlIO.getRider(), covtlnbIO.getRider())) {
			covtunlIO.setStatuz(varcom.endp);
		}
	}

protected void cont4715()
	{
		if (isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			covtunlIO.setNonKey(SPACES);
			covtunlIO.setAnbccd(1, 0);
			covtunlIO.setSingp(0);
			covtunlIO.setInstprem(0);
			covtunlIO.setNumapp(0);
			covtunlIO.setPremCessAge(0);
			covtunlIO.setPremCessTerm(0);
			covtunlIO.setPolinc(0);
			covtunlIO.setRiskCessAge(0);
			covtunlIO.setRiskCessTerm(0);
			covtunlIO.setSumins(0);
			covtunlIO.setBenCessAge(0);
			covtunlIO.setBenCessTerm(0);
			covtunlIO.setPayrseqno(1);
			wsaaCtrltime.set("Y");
			goTo(GotoLabel.cont4717);
		}
		/*ELSE                                                         */
		wsaaCtrltime.set("N");
		sv.mattcess.set(covtunlIO.getRiskCessDate());
		sv.premcess.set(covtunlIO.getPremCessDate());
		sv.matage.set(covtunlIO.getRiskCessAge());
		sv.premCessAge.set(covtunlIO.getPremCessAge());
		sv.mattrm.set(covtunlIO.getRiskCessTerm());
		sv.premCessTerm.set(covtunlIO.getPremCessTerm());
		if (isEQ(covtunlIO.getInstprem(), 0)) {
			sv.singlePremium.set(covtunlIO.getSingp());
			sv.lumpContrib.set(ZERO);
		}
		else {
			sv.singlePremium.set(covtunlIO.getInstprem());
			sv.lumpContrib.set(covtunlIO.getSingp());
		}
		sv.zbinstprem.set(covtunlIO.getZbinstprem());
		sv.zlinstprem.set(covtunlIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtunlIO.getLoadper());
		sv.rateadj.set(covtunlIO.getRateadj());
		sv.fltmort.set(covtunlIO.fltmort);
		sv.premadj.set(covtunlIO.getPremadj());
		sv.adjustageamt.set(covtunlIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtunlIO.getSumins());
		sv.mortcls.set(covtunlIO.getMortcls());
		sv.liencd.set(covtunlIO.getLiencd());
		sv.singpremtype.set(covtunlIO.getSingpremtype());//ILIFE-7805
	}

protected void cont4717()
	{
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covtunlIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covtunlIO.getNumapp());
			goTo(GotoLabel.cont4720);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covtunlIO.getSeqnbr(), 0);
			covtunlIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covtunlIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4720()
	{
		if (isEQ(covtunlIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtunlIO.getSeqnbr());
		}
		/*  If the record we are about to display is invalid i.e weve PgDn */
		/*  got an ENDP statuz and we were in modify mode then we display  */
		/*  the current valid record with an error message.                */
		if (isEQ(scrnparams.statuz, varcom.rolu)
		&& isEQ(covtunlIO.getNumapp(), ZERO)
		&& isEQ(wsaaNumavail, ZERO)
		&& isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtunlIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			compute(sv.sumin, 1).setRounded((div(mult(t5551rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covtunlIO.getSumins())) {
			sv.singlePremium.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(ZERO);
			sv.adjustageamt.set(0);
			sv.rateadj.set(ZERO);
			sv.fltmort.set(ZERO);
			sv.premadj.set(ZERO);
			/*BRD-306 END */
			calcPremium2700();
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void getTheFirstRecord4750()
	{
		para4750();
	}

protected void para4750()
	{
		covtunlIO.setSeqnbr(ZERO);
		covtunlIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		if (isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			covtunlIO.setNonKey(SPACES);
			covtunlIO.setAnbccd(1, 0);
			covtunlIO.setSingp(0);
			covtunlIO.setInstprem(0);
			covtunlIO.setNumapp(0);
			covtunlIO.setPremCessAge(0);
			covtunlIO.setPremCessTerm(0);
			covtunlIO.setPolinc(0);
			covtunlIO.setRiskCessAge(0);
			covtunlIO.setRiskCessTerm(0);
			covtunlIO.setSumins(0);
			covtunlIO.setBenCessAge(0);
			covtunlIO.setBenCessTerm(0);
			covtunlIO.setPayrseqno(1);
			wsaaCtrltime.set("Y");
		}
		else {
			wsaaCtrltime.set("N");
			sv.mattcess.set(covtunlIO.getRiskCessDate());
			sv.premcess.set(covtunlIO.getPremCessDate());
			sv.matage.set(covtunlIO.getRiskCessAge());
			sv.premCessAge.set(covtunlIO.getPremCessAge());
			sv.mattrm.set(covtunlIO.getRiskCessTerm());
			sv.premCessTerm.set(covtunlIO.getPremCessTerm());
		}
		if (isEQ(covtunlIO.getInstprem(), 0)) {
			sv.singlePremium.set(covtunlIO.getSingp());
			sv.lumpContrib.set(ZERO);
		}
		else {
			sv.singlePremium.set(covtunlIO.getInstprem());
			sv.lumpContrib.set(covtunlIO.getSingp());
		}
		sv.zbinstprem.set(covtunlIO.getZbinstprem());
		sv.zlinstprem.set(covtunlIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtunlIO.getLoadper());
		sv.rateadj.set(covtunlIO.getRateadj());
		sv.fltmort.set(covtunlIO.fltmort);
		sv.premadj.set(covtunlIO.getPremadj());
		sv.adjustageamt.set(covtunlIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtunlIO.getSumins());
		sv.mortcls.set(covtunlIO.getMortcls());
		sv.liencd.set(covtunlIO.getLiencd());
		sv.singpremtype.set(covtunlIO.getSingpremtype());//ILIFE-7805
		/*    When displaying the next record, or a blank screen to        */
		/*    enter a new record, calculate the number available as        */
		/*    the current number less the number applicable on the         */
		/*    current screen.                                              */
		/*    If displaying a blank screen for a new record, default       */
		/*    the number applicable to the number available. If this       */
		/*    is less than zero, default it to zero.                       */
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (isEQ(covtunlIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtunlIO.getSeqnbr());
		}
		wsaaNumavail.set(chdrlnbIO.getPolinc());
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtunlIO.getNumapp());
		/* Re-calculate default SI if applicable                           */
		/*  then re-calculate default premium if SI is different           */
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			compute(sv.sumin, 1).setRounded((div(mult(t5551rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covtunlIO.getSumins())) {
			sv.singlePremium.set(0);
			calcPremium2700();
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void wayin4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case rolu4805: 
					rolu4805();
				case cont4810: 
					cont4810();
				case cont4820: 
					cont4820();
				case readCovtunl4830: 
					readCovtunl4830();
				case cont4835: 
					cont4835();
				case cont4837: 
					cont4837();
				case cont4840: 
					cont4840();
				case exit4890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4820);
		}
		if (isEQ(wsaaCredit, 0)) {
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4890);
		}
		if (isLTE(sv.numapp, sv.numavail)
		&& isLT(wsaaCredit, ZERO)) {
			goTo(GotoLabel.rolu4805);
		}
		if (isLT(wsaaCredit, 0)) {
			goTo(GotoLabel.cont4810);
		}
	}

	/**
	* <pre>
	*    If 'Enter' has been pressed and 'Credit' has a
	*    positive value then force the program to "roll up".
	* </pre>
	*/
protected void rolu4805()
	{
		scrnparams.statuz.set(varcom.rolu);
		goTo(GotoLabel.cont4820);
	}

protected void cont4810()
	{
		/*   If 'Enter' has been pressed and "Credit" has a negative*/
		/*   value, loop round within the program displaying the*/
		/*   details from the first transaction record, with the*/
		/*   'Number Applicable' highlighted, and give a message*/
		/*   indicating that more policies have been defined than*/
		/*   are on the Contract Header.*/
		covtunlIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtunlIO.setChdrnum(covtlnbIO.getChdrnum());
		covtunlIO.setLife(covtlnbIO.getLife());
		covtunlIO.setCoverage(covtlnbIO.getCoverage());
		covtunlIO.setRider(covtlnbIO.getRider());
		covtunlIO.setSeqnbr(0);
		/* The Error Message was being moved to the screen and not         */
		/* to the appropriate Screen Field:- S6326-NumApp-Err.             */
		/* The validation is moved to the 2000-section                     */
		/*MOVE U012                   TO S6326-NUMAPP-ERR.             */
		covtunlIO.setFunction(varcom.begn);
		goTo(GotoLabel.readCovtunl4830);
	}

protected void cont4820()
	{
		/*    If one of the Roll keys has been pressed then loop*/
		/*    round within the program and display the next or*/
		/*    previous screen as requested (including  BEGN,*/
		/*    available count etc., see above).*/
		/*  Save the last used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covtunlIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covtunlIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4835);
			}
			else {
				covtunlIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covtunlIO);
				if (isNE(covtunlIO.getStatuz(), varcom.oK)
				&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covtunlIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covtunlIO.setFunction(varcom.nextr);
		}
		else {
			covtunlIO.setFunction(varcom.nextp);
		}
	}

protected void readCovtunl4830()
	{
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		if (isNE(covtunlIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtunlIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtunlIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtunlIO.getRider(), covtlnbIO.getRider())) {
			covtunlIO.setStatuz(varcom.endp);
		}
	}

protected void cont4835()
	{
		if (isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			covtunlIO.setNonKey(SPACES);
			covtunlIO.setReserveUnitsInd(sv.reserveUnitsInd);
			covtunlIO.setLiencd(sv.liencd);
			covtunlIO.setAnbccd(1, wsaaAnbAtCcd);
			covtunlIO.setSumins(sv.sumin);
			covtunlIO.setPremCessDate(sv.premcess);
			covtunlIO.setPremCessAge(sv.premCessAge);
			covtunlIO.setPremCessTerm(sv.premCessTerm);
			covtunlIO.setRiskCessDate(sv.mattcess);
			covtunlIO.setRiskCessAge(sv.matage);
			covtunlIO.setRiskCessTerm(sv.mattrm);
			covtunlIO.setReserveUnitsDate(sv.reserveUnitsDate);
			covtunlIO.setPolinc(ZERO);
			covtunlIO.setZbinstprem(sv.zbinstprem);
			covtunlIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtunlIO.setLoadper(sv.loadper);
			covtunlIO.setRateadj(sv.rateadj);
			covtunlIO.setFltmort(sv.fltmort);
			covtunlIO.setPremadj(sv.premadj);
			covtunlIO.setPremadj(sv.adjustageamt);
			covtunlIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaCtrltime.set("Y");
			/*IF CHDRLNB-BILLFREQ    = '00'                      <004>*/
			/*      IF PAYR-BILLFREQ    = '00'                      <A05691>*/
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				covtunlIO.setSingp(sv.singlePremium);
				covtunlIO.setInstprem(0);
				goTo(GotoLabel.cont4837);
			}
			else {
				covtunlIO.setInstprem(sv.singlePremium);
				covtunlIO.setSingp(sv.lumpContrib);
				goTo(GotoLabel.cont4837);
			}
		}
		/*ELSE                                                         */
		wsaaCtrltime.set("N");
		sv.reserveUnitsDate.set(covtunlIO.getReserveUnitsDate());
		sv.mattcess.set(covtunlIO.getRiskCessDate());
		sv.premcess.set(covtunlIO.getPremCessDate());
		sv.matage.set(covtunlIO.getRiskCessAge());
		sv.premCessAge.set(covtunlIO.getPremCessAge());
		sv.mattrm.set(covtunlIO.getRiskCessTerm());
		sv.premCessTerm.set(covtunlIO.getPremCessTerm());
		if (isEQ(covtunlIO.getInstprem(), 0)) {
			sv.singlePremium.set(covtunlIO.getSingp());
			sv.lumpContrib.set(0);
		}
		else {
			sv.singlePremium.set(covtunlIO.getInstprem());
			sv.lumpContrib.set(covtunlIO.getSingp());
		}
		sv.zbinstprem.set(covtunlIO.getZbinstprem());
		sv.zlinstprem.set(covtunlIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtunlIO.getLoadper());
		sv.rateadj.set(covtunlIO.getRateadj());
		sv.fltmort.set(covtunlIO.fltmort);
		sv.premadj.set(covtunlIO.getPremadj());
		sv.adjustageamt.set(covtunlIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtunlIO.getSumins());
		sv.mortcls.set(covtunlIO.getMortcls());
		sv.liencd.set(covtunlIO.getLiencd());
		sv.singpremtype.set(covtunlIO.getSingpremtype());//ILIFE-7805
	}

protected void cont4837()
	{
		/*    When displaying the first record again,  set the*/
		/*    number available as the number included in the plan*/
		if (isEQ(covtunlIO.getFunction(), varcom.begn)) {
			wsaaNumavail.set(chdrlnbIO.getPolinc());
			wsaaFirstSeqnbr.set(covtunlIO.getSeqnbr());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covtunlIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covtunlIO.getNumapp());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covtunlIO.getSeqnbr(), 0);
			covtunlIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covtunlIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4840()
	{
		if (isEQ(covtunlIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtunlIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtunlIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			compute(sv.sumin, 1).setRounded((div(mult(t5551rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covtunlIO.getSumins())) {
			sv.singlePremium.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(ZERO);
			sv.adjustageamt.set(0);
			sv.rateadj.set(ZERO);
			sv.fltmort.set(ZERO);
			sv.premadj.set(ZERO);
			/*BRD-306 END */
			calcPremium2700();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void checkCalcTax5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5000();
				case gotItem5000: 
					gotItem5000();
				case taxCalc5000: 
					taxCalc5000();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5000()
	{
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.taxCalc5000);
		}
		/* Read table TR52E                                                */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getItemcoy(), wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.tr52e)
		&& isEQ(itdmIO.getItemitem(), wsaaTr52eKey)
		&& isEQ(itdmIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.gotItem5000);
		}
		/* re-read using generic component                                */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getItemcoy(), wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.tr52e)
		&& isEQ(itdmIO.getItemitem(), wsaaTr52eKey)
		&& isEQ(itdmIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.gotItem5000);
		}
		/* re-read using generic contract and component                   */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr52e)
		|| isNE(itdmIO.getItemitem(), wsaaTr52eKey)
		|| isNE(itdmIO.getStatuz(), varcom.oK)) {
			itdmIO.setItemtabl(tablesInner.tr52e);
			itdmIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
	}

protected void gotItem5000()
	{
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
	}

protected void taxCalc5000()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.jrnseq.set(ZERO);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			txcalcrec.register.set(chdrlnbIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCcy.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				txcalcrec.amountIn.set(sv.singlePremium);
			}
			else {
				if (isEQ(tr52erec.zbastyp, "Y")) {
					compute(txcalcrec.amountIn, 2).set(add(sv.zbinstprem, sv.lumpContrib));
				}
				else {
					compute(txcalcrec.amountIn, 2).set(add(sv.singlePremium, sv.lumpContrib));
				}
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			txcalcrec.txcode.set(tr52drec.txcode);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				compute(wsaaTaxamt, 2).set(add(sv.singlePremium, sv.lumpContrib));
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void taxExe5100()
	{
		start5100();
	}

protected void start5100()
	{
		/*  - Keep the CHDR/COVT record                                    */
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.flag, "I")) {
			covtlnbIO.setZbinstprem(sv.zbinstprem);
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				covtlnbIO.setSingp(sv.singlePremium);
			}
			else {
				covtlnbIO.setInstprem(sv.singlePremium);
				covtlnbIO.setSingp(sv.lumpContrib);
			}
			covtlnbIO.setEffdate(chdrlnbIO.getOccdate());
		}
		covtlnbIO.setFunction("KEEPS");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet5200()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.taxind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}
//BRD-NBP-012-STARTS
protected void optind5300()
{
	covtlnbIO.setFunction("KEEPS");
	covtlnbIO.setFormat(formatsInner.covtlnbrec);
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(covtlnbIO.getParams());
		syserrrec.statuz.set(covtlnbIO.getStatuz());
		fatalError600();
	}
	/* Use standard GENSWITCH table switching using the next option    */
	/* on table T1675.                                                 */
	/*  - change the request indicator to '?',                         */
	sv.optind.set("?");
	/*  - save the next 8 programs from the program stack,             */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
	/*  - call GENSSWCH with the next table action to retreive the     */
	/*    next program.                                                */
	gensswrec.function.set("K");
	gensww4210();
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
		load4530();
	}
	/*  - set the current stack "action" to '*',                       */
	/*  - add one to the program pointer and exit.                     */
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}
protected void optind5400()
{
	/*START*/
	/* On return from this  request, the current stack "action" will   */
	/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
	/* handle the return from options and extras:                      */
	/* Note that to have selected this option in the first place then  */
	/* details must exist.......set the flag for re-selection.         */
	sv.optind.set("+");
	/*  - restore the saved programs to the program stack              */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	/*  - blank  out  the  stack  "action"                             */
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	/*       Set  WSSP-NEXTPROG to  the current screen                 */
	/*       name (thus returning to re-display the screen).           */
	wsspcomn.nextprog.set(scrnparams.scrname);
	/*EXIT*/
}
//BRD-NBP-012-ENDS

/*ILIFE-7805 - Start*/
protected void singlePremiumSwitch()
{
	
	singlepremiumind = "?";
	/*  - save the next 8 programs from the program stack,             */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
	/*  - call GENSSWCH with the next table action to retreive the     */
	/*    next program.                                                */
	gensswrec.function.set("S");
	wsspcomn.grossprem.set(sv.singlePremium);
	gensww4210();
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
		load4530();
	}
	/*  - set the current stack "action" to '*',                       */
	/*  - add one to the program pointer and exit.                     */
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}

protected void singlePremiumSwitchReturn()
{
	
	/*  - restore the saved programs to the program stack              */
	singlepremiumind="+";
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	/*  - blank  out  the  stack  "action"                             */
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	/*       Set  WSSP-NEXTPROG to  the current screen                 */
	/*       name (thus returning to re-display the screen).           */
	wsspcomn.nextprog.set(scrnparams.scrname);
	/*EXIT*/
}

/**
 * This method determines if the component is Single Premium Type or not and returns boolean value
 * 
 * @return boolean
 */
protected boolean isSinglePremTypeComp(){	
	
	boolean superAnnuFlag = false;
	boolean singPrmTypCompFlag = false;
	boolean cntTypeFlag = false;	
	boolean singlePremIndFlag = false;
	String itemItem = "";
	if(chdrlnbIO.getChdrcoy() != null){
	    superAnnuFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP07", appVars, "IT");
	}	
	if(superAnnuFlag){
		//1.1. Check TD5J9 (Single Premium Type Components) for Contract Type (3 characters) + Component Code (4 characters)
		if(chdrlnbIO.getCnttype() != null && covtlnbIO.getCrtable() != null){
			itemItem = chdrlnbIO.getCnttype().toString().trim()+covtlnbIO.getCrtable().toString().trim();
		}		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(itemItem);
		itempf.setItemtabl("TD5J9");		
		itempf = itemDAO.getItempfRecord(itempf);
		if(itempf != null && itempf.getGenarea() != null) {						
			singPrmTypCompFlag = true;			
		} 
		
		//1.2. Component is single premium or not by checking single premium indicator in T5687
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			singlePremIndFlag = true;
		}
		
		//1.3. Check contract type (3 characters) has valid item in TR59X (Existence of contract type e.g SIS in TR59X)
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString().trim());
		itempf.setItemtabl("TR59X");		
		itempf = itemDAO.getItempfRecord(itempf);
		if(itempf != null && itempf.getGenarea() != null) {						
			cntTypeFlag = true;			
		} 
		
		//1.4. Check if all conditions required for Single premium type component are met or not
		if(singPrmTypCompFlag && cntTypeFlag && singlePremIndFlag){
			singPremTypeFlag = true;
		}
	}	
	
	return singPremTypeFlag;
}
/*ILIFE-7805 - End*/


protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifenum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrlnbIO.getCnttype());
		chkrlrec.crtable.set(covtlnbIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtlnbIO.getLife());
		chkrlrec.chdrnum.set(chdrlnbIO.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CallRounding()
	{
		/*A200-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A290-EXIT*/
	}

protected void x100ReadItdm()
	{
		x100Init();
	}

protected void x100Init()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(wsaaItemtabl);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setParams(syserrrec.params);
			itdmIO.setStatuz(syserrrec.statuz);
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6768)
		|| isNE(itdmIO.getItemitem(), wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT6768Found.set("N");
		}
		else {
			wsaaT6768Found.set("Y");
			t6768rec.t6768Rec.set(itdmIO.getGenarea());
		}
	}

/*ILIFE-2964 IL_BASE_ITEM-001-Base development starts*/
protected void m100CheckMbns() 
{
	m100Start();
}
protected void m100Start() 
{
	mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
	mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
	mbnsIO.setLife(covtlnbIO.getLife());
	mbnsIO.setCoverage(covtlnbIO.getCoverage());
	mbnsIO.setRider(covtlnbIO.getRider());
	mbnsIO.setYrsinf(ZERO);
	mbnsIO.setFormat(formatsInner.mbnsrec);
	mbnsIO.setFunction(varcom.begn);
	mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
	SmartFileCode.execute(appVars, mbnsIO);
	if (isNE(mbnsIO.getStatuz(), varcom.oK)
	&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
		syserrrec.statuz.set(mbnsIO.getStatuz());
		syserrrec.params.set(mbnsIO.getParams());
		fatalError600();
	}
	if (isEQ(mbnsIO.getStatuz(), varcom.endp)) {	
		return ;
	}
	if (isNE(mbnsIO.getRider(), covtlnbIO.getRider())
	|| isNE(mbnsIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
	|| isNE(mbnsIO.getChdrnum(), chdrlnbIO.getChdrnum())
	|| isNE(mbnsIO.getLife(), covtlnbIO.getLife())
	|| isNE(mbnsIO.getCoverage(), covtlnbIO.getCoverage())) {
		mbnsIO.setStatuz(varcom.endp);
	}
	else {
		sv.optsmode.set("+");
	}
}

/*ILIFE-2964 IL_BASE_ITEM-001-Base development ends*/

/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e026 = new FixedLengthStringData(4).init("E026");
	private FixedLengthStringData e027 = new FixedLengthStringData(4).init("E027");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e302 = new FixedLengthStringData(4).init("E302");
	private FixedLengthStringData e366 = new FixedLengthStringData(4).init("E366");
	private FixedLengthStringData e389 = new FixedLengthStringData(4).init("E389");
	private FixedLengthStringData e390 = new FixedLengthStringData(4).init("E390");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f261 = new FixedLengthStringData(4).init("F261");
	private FixedLengthStringData f262 = new FixedLengthStringData(4).init("F262");
	private FixedLengthStringData g099 = new FixedLengthStringData(4).init("G099");
	private FixedLengthStringData g155 = new FixedLengthStringData(4).init("G155");
	private FixedLengthStringData g330 = new FixedLengthStringData(4).init("G330");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g622 = new FixedLengthStringData(4).init("G622");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h043 = new FixedLengthStringData(4).init("H043");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData h363 = new FixedLengthStringData(4).init("H363");
	private FixedLengthStringData h366 = new FixedLengthStringData(4).init("H366");
	private FixedLengthStringData h368 = new FixedLengthStringData(4).init("H368");
	private FixedLengthStringData h369 = new FixedLengthStringData(4).init("H369");
	private FixedLengthStringData h437 = new FixedLengthStringData(4).init("H437");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h040 = new FixedLengthStringData(4).init("H040");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData G070 = new FixedLengthStringData(4).init("G070");			//ILIFE-4212
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).init("T5551");
	private FixedLengthStringData t5585 = new FixedLengthStringData(5).init("T5585");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t6768 = new FixedLengthStringData(5).init("T6768");
	private FixedLengthStringData tr675 = new FixedLengthStringData(5).init("TR675");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData th615 = new FixedLengthStringData(5).init("TH615");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData covtunlrec = new FixedLengthStringData(10).init("COVTUNLREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	private FixedLengthStringData undcrec = new FixedLengthStringData(10).init("UNDCREC");
	private FixedLengthStringData mbnsrec = new FixedLengthStringData(10).init("MBNSREC");
	private FixedLengthStringData mrtarec = new FixedLengthStringData(10).init("MRTAREC");
}
}
