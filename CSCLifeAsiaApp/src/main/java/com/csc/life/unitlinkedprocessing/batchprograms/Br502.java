/*
 * File: Br502.java
 * Date: 29 August 2009 22:09:44
 * Author: Quipoz Limited
 * 
 * Class transformed from BR502.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.regularprocessing.dataaccess.LifernlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.RturpfTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsaccTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              Sterling Unit Reserve Extraction
*              --------------------------------
*
*  This batch program will extract all contracts in ULNK with
*  validflag '1'.
*
*  For each contract, it will read the COVRMJA to retrieve the
*  STATCODE, CRTABLE & SUMINS. Then, it will read UTRSACC &
*  VPRNUDL for the Virtual Fund & its corresponding unit price.
*  The information will then be written to a temporary file,
*  RTURPF for printing.
*
*****************************************************************
* </pre>
*/
public class Br502 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlulnkpf1rs = null;
	private java.sql.PreparedStatement sqlulnkpf1ps = null;
	private java.sql.Connection sqlulnkpf1conn = null;
	private String sqlulnkpf1 = "";
	private RturpfTableDAM rturpf = new RturpfTableDAM();
	private FixedLengthStringData rturpfRec = new FixedLengthStringData(43);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR502");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaUnitFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUnitFn, 0, FILLER).init("RTUR");
	private FixedLengthStringData wsaaUnitRunid = new FixedLengthStringData(2).isAPartOf(wsaaUnitFn, 4);
	private ZonedDecimalData wsaaUnitJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUnitFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
		/* ERRORS */
	private String esql = "ESQL";
	private String covrmjarec = "COVRMJAREC";
	private String utrsaccrec = "UTRSACCREC";
	private String vprnudlrec = "VPRNUDLREC";
	private String lifernlrec = "LIFERNLREC";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

		/* SQL-ULNKPF */
	private FixedLengthStringData sqlUlnkrec = new FixedLengthStringData(20);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlUlnkrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlUlnkrec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlUlnkrec, 9);
	private FixedLengthStringData sqlJlife = new FixedLengthStringData(2).isAPartOf(sqlUlnkrec, 11);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlUlnkrec, 13);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlUlnkrec, 15);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlUlnkrec, 17);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life File Renewals*/
	private LifernlTableDAM lifernlIO = new LifernlTableDAM();
		/*Temporary Unit Reserve File*/
	private RturpfTableDAM rturpfData = new RturpfTableDAM();
		/*Unit transaction summary (Accum Units)*/
	private UtrsaccTableDAM utrsaccIO = new UtrsaccTableDAM();
		/*VPRCPF view for now price for units*/
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2060, 
		exit2090, 
		exit2690, 
		exit2790
	}

	public Br502() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaUnitRunid.set(bprdIO.getSystemParam04());
		wsaaUnitJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(RTURPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaUnitFn.toString());
		stringVariable1.append(") ");
		stringVariable1.append("MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		rturpf.openOutput();
		sqlulnkpf1 = " SELECT  CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX" +
" FROM   " + appVars.getTableNameOverriden("ULNKPF") + " " +
" WHERE VALIDFLAG = '1'" +
" ORDER BY CHDRNUM";
		sqlerrorflag = false;
		try {
			sqlulnkpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.unitlinkedprocessing.dataaccess.UlnkpfTableDAM());
			sqlulnkpf1ps = appVars.prepareStatementEmbeded(sqlulnkpf1conn, sqlulnkpf1, "ULNKPF");
			sqlulnkpf1rs = appVars.executeQuery(sqlulnkpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2060: {
					eof2060();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlulnkpf1rs.next()) {
				appVars.getDBObject(sqlulnkpf1rs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlulnkpf1rs, 2, sqlChdrnum);
				appVars.getDBObject(sqlulnkpf1rs, 3, sqlLife);
				appVars.getDBObject(sqlulnkpf1rs, 4, sqlJlife);
				appVars.getDBObject(sqlulnkpf1rs, 5, sqlCoverage);
				appVars.getDBObject(sqlulnkpf1rs, 6, sqlRider);
				appVars.getDBObject(sqlulnkpf1rs, 7, sqlPlnsfx);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(sqlChdrcoy);
		covrmjaIO.setChdrnum(sqlChdrnum);
		covrmjaIO.setLife(sqlLife);
		covrmjaIO.setCoverage(sqlCoverage);
		covrmjaIO.setRider(sqlRider);
		covrmjaIO.setPlanSuffix(sqlPlnsfx);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		rturpfData.chdrnum.set(sqlChdrnum);
		rturpfData.statcode.set(covrmjaIO.getStatcode());
		rturpfData.crtable.set(covrmjaIO.getCrtable());
		rturpfData.sumins.set(covrmjaIO.getSumins());
		lifernlIO.setParams(SPACES);
		lifernlIO.setChdrcoy(sqlChdrcoy);
		lifernlIO.setChdrnum(sqlChdrnum);
		lifernlIO.setLife(sqlLife);
		lifernlIO.setJlife(sqlJlife);
		lifernlIO.setFormat(lifernlrec);
		lifernlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifernlIO);
		if (isNE(lifernlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifernlIO.getParams());
			syserrrec.statuz.set(lifernlIO.getStatuz());
			fatalError600();
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(lifernlIO.getCltdob());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(rturpfData.age, 5).set(add(0.99999,datcon3rec.freqFactor));
		utrsaccIO.setParams(SPACES);
		utrsaccIO.setChdrcoy(sqlChdrcoy);
		utrsaccIO.setChdrnum(sqlChdrnum);
		utrsaccIO.setLife(sqlLife);
		utrsaccIO.setCoverage(sqlCoverage);
		utrsaccIO.setRider(sqlRider);
		utrsaccIO.setPlanSuffix(sqlPlnsfx);
		utrsaccIO.setFormat(utrsaccrec);
		utrsaccIO.setFunction(varcom.begn);
		while ( !(isEQ(utrsaccIO.getStatuz(),varcom.endp))) {
			readUtrsacc2600();
		}
		
	}

protected void readUtrsacc2600()
	{
		try {
			readUtrsacc2610();
		}
		catch (GOTOException e){
		}
	}

protected void readUtrsacc2610()
	{
		SmartFileCode.execute(appVars, utrsaccIO);
		if (isNE(utrsaccIO.getStatuz(),varcom.oK)
		&& isNE(utrsaccIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsaccIO.getParams());
			fatalError600();
		}
		if (isNE(utrsaccIO.getChdrcoy(),sqlChdrcoy)
		|| isNE(utrsaccIO.getChdrnum(),sqlChdrnum)
		|| isNE(utrsaccIO.getLife(),sqlLife)
		|| isNE(utrsaccIO.getCoverage(),sqlCoverage)
		|| isNE(utrsaccIO.getRider(),sqlRider)
		|| isNE(utrsaccIO.getPlanSuffix(),sqlPlnsfx)
		|| isEQ(utrsaccIO.getStatuz(),varcom.endp)) {
			utrsaccIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		rturpfData.vfund.set(utrsaccIO.getUnitVirtualFund());
		rturpfData.currentDunitBal.set(utrsaccIO.getCurrentDunitBal());
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(sqlChdrcoy);
		vprnudlIO.setUnitVirtualFund(utrsaccIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(utrsaccIO.getUnitType());
		vprnudlIO.setEffdate(bsscIO.getEffectiveDate());
		vprnudlIO.setFunction(varcom.begn);
		vprnudlIO.setFormat(vprnudlrec);
		fundPrice2700();
		rturpf.write(rturpfData);
		utrsaccIO.setFunction(varcom.nextr);
	}

protected void fundPrice2700()
	{
		try {
			fundPrice2710();
		}
		catch (GOTOException e){
		}
	}

protected void fundPrice2710()
	{
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError600();
		}
		if (isNE(sqlChdrcoy,vprnudlIO.getCompany())
		|| isNE(utrsaccIO.getUnitVirtualFund(),vprnudlIO.getUnitVirtualFund())
		|| isNE(utrsaccIO.getUnitType(),vprnudlIO.getUnitType())
		|| isEQ(vprnudlIO.getStatuz(),varcom.endp)) {
			rturpfData.unitBidPrice.set(ZERO);
			goTo(GotoLabel.exit2790);
		}
		rturpfData.unitBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		rturpf.close();
		wsaaQcmdexc.set("DLTOVR FILE(RTURPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
