/*
 * File: ZSPTILET.java
 * Date: Feb 24, 2015
 * Author: vchawda
 * 
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;

import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* ZSPTILET - Single Premium Top-Up Letter Trigger Module for Intrest Bearing Fund
*
* This routine will generate a Single Premium Top-Up Letter if all
* of the contract's HITRs have been fully processed.
*
*
****************************************************************** ****
* </pre>
*/
public class Zsptilet extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zsptilet.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	public int wsaaPrintFlag = 0;
	private final String wsaaSubr = "ZSPTILET";
	private FixedLengthStringData wsaaEndProc = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaUtrnrevBatctrcde = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaOthLife = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 1);
	private FixedLengthStringData wsaaOthCoverage = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 3);
	private FixedLengthStringData wsaaOthRider = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 5);
	private PackedDecimalData wsaaOthPlnsfx = new PackedDecimalData(4, 0).isAPartOf(wsaaOtherKeys, 7).setUnsigned();
	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(4).isAPartOf(wsaaOtherKeys, 10);

	private FixedLengthStringData wsaaUtrnInd = new FixedLengthStringData(1);
	private Validator utrnsNotFound = new Validator(wsaaUtrnInd, "Y");

	private FixedLengthStringData wsaaHitrInd = new FixedLengthStringData(1);
	private Validator hitrsNotFound = new Validator(wsaaHitrInd, "Y");
		/* ERRORS */
	private static final String g437 = "G437";
		/* TABLES */
	private static final String tr384 = "TR384";
	private static final String itemrec = "ITEMREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private HitrTableDAM hitraloIO = new HitrTableDAM();
	//private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Varcom varcom = new Varcom();
	private Udtrigrec udtrigrec = new Udtrigrec();

	public Zsptilet() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		/*  Perform initialisation processing*/
		initialProcessing1000();
		
		hitrProcessing3000();
		/* HITR record has not been fully processed so exit program */
		if (isEQ(wsaaEndProc, "Y")) {
			return;
		}
		if (hitrsNotFound.isTrue()) {
			return ;
		}
		/*  HITRs have been fully processed so trigger Fund*/
		/*  Switch Letter*/
		if (wsaaPrintFlag<1){
			letterProcessing4000();
			wsaaPrintFlag++;
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialProcessing1000()
	{
		/*PARA*/
		udtrigrec.statuz.set(varcom.oK);
		wsaaEndProc.set(SPACES);
		wsaaUtrnInd.set(SPACES);
		wsaaHitrInd.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}

protected void hitrProcessing3000() {
	para3010();
}

protected void para3010() {
	/* Check to see if all HITRs for contract's UDTG transaction */
	/* have been fully processed */
	hitraloIO.setParams(SPACES);
	hitraloIO.setChdrcoy(udtrigrec.chdrcoy);
	hitraloIO.setChdrnum(udtrigrec.chdrnum);
	hitraloIO.setTranno(udtrigrec.tranno);
	hitraloIO.setLife(udtrigrec.life);
	hitraloIO.setCoverage(udtrigrec.coverage);
	hitraloIO.setRider(udtrigrec.rider);
	hitraloIO.setPlanSuffix(udtrigrec.planSuffix);
	hitraloIO.setZintbfnd(SPACES);
	
	hitraloIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, hitraloIO);
	if (isNE(hitraloIO.getStatuz(), varcom.oK)
			&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(hitraloIO.getParams());
		syserrrec.statuz.set(hitraloIO.getStatuz());
		fatalError9000();
	}
	if (isNE(hitraloIO.getChdrcoy(), udtrigrec.chdrcoy)
			|| isNE(hitraloIO.getChdrnum(), udtrigrec.chdrnum)
			|| isNE(hitraloIO.getTranno(), udtrigrec.tranno)
			|| isEQ(hitraloIO.getStatuz(), varcom.endp)) { // MIBT-321 code
															// added
		hitrsNotFound.setTrue();
		return;
	}
	wsaaUtrnrevBatctrcde.set(hitraloIO.getBatctrcde());// MIBT-321 code
														// added
	while (!(isEQ(hitraloIO.getStatuz(), varcom.endp) || isEQ(wsaaEndProc,
			"Y"))) {
		checkHitrFeedbackInd3100();
	}

}

protected void checkHitrFeedbackInd3100() {
	para12110();
}

protected void para12110() {
	/* fully processed */
	if (isEQ(hitraloIO.getFeedbackInd(), SPACES)) {
		wsaaEndProc.set("Y");
		return;
	}
	hitraloIO.setFunction(varcom.nextr);
	SmartFileCode.execute(appVars, hitraloIO);
	if (isNE(hitraloIO.getStatuz(), varcom.oK)
			&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(hitraloIO.getParams());
		syserrrec.statuz.set(hitraloIO.getStatuz());
		fatalError9000();
	}
	if (isNE(hitraloIO.getChdrcoy(), udtrigrec.chdrcoy)
			|| isNE(hitraloIO.getChdrnum(), udtrigrec.chdrnum)
			|| isNE(hitraloIO.getTranno(), udtrigrec.tranno)) {
		hitraloIO.setStatuz(varcom.endp);
	}
}
protected void letterProcessing4000()
	{
		para4010();
	}

protected void para4010()
	{
		/*  Fund Switch Letter request*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setChdrcoy(udtrigrec.chdrcoy);
		chdrmjaIO.setChdrnum(udtrigrec.chdrnum);
		/* MOVE READH                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.format.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(wsaaUtrnrevBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***");
			stringVariable2.addExpression(wsaaUtrnrevBatctrcde);
			stringVariable2.setStringInto(itemIO.getItemitem());
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError9000();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(g437);
				fatalError9000();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		letrqstrec.statuz.set(SPACES);
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.requestCompany.set(udtrigrec.chdrcoy);
		letrqstrec.chdrcoy.set(udtrigrec.chdrcoy);
		letrqstrec.rdoccoy.set(udtrigrec.chdrcoy);
		letrqstrec.rdocnum.set(udtrigrec.chdrnum);
		letrqstrec.chdrnum.set(udtrigrec.chdrnum);
		letrqstrec.tranno.set(udtrigrec.tranno);
		wsaaLanguage.set(udtrigrec.language);
		wsaaOthLife.set(udtrigrec.life);
		wsaaOthCoverage.set(udtrigrec.coverage);
		wsaaOthRider.set(udtrigrec.rider);
		wsaaOthPlnsfx.set(udtrigrec.planSuffix);
		wsaaTrcde.set(wsaaUtrnrevBatctrcde);
		letrqstrec.otherKeys.set(wsaaOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		/*ERROR*/
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		udtrigrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
