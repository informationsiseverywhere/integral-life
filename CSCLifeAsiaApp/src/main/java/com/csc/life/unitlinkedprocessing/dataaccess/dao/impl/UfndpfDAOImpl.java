package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.UfndpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ufndpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UfndpfDAOImpl extends BaseDAOImpl<Ufndpf> implements UfndpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UfndpfDAOImpl.class);

	public Map<String, List<Ufndpf>> searchUfndRecord(List<String> virtualFundList) {
		StringBuilder sqlUfndSelect1 = new StringBuilder("SELECT UNIQUE_NUMBER, COMPANY, VRTFUND, UNITYP, NOFUNTS ");
		sqlUfndSelect1.append(" FROM UFNDUDL WHERE ");
		sqlUfndSelect1.append(getSqlInStr("VRTFUND", virtualFundList));
		sqlUfndSelect1.append(" ORDER BY COMPANY ASC, VRTFUND ASC, UNITYP ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psUfndSelect = getPrepareStatement(sqlUfndSelect1.toString());
		ResultSet sqlUfndpf1rs = null;
		Map<String, List<Ufndpf>> utrnpfMap = new HashMap<>();
		try {
			sqlUfndpf1rs = executeQuery(psUfndSelect);
			while (sqlUfndpf1rs.next()) {
				Ufndpf utrnpf = new Ufndpf();
				utrnpf.setUniqueNumber(sqlUfndpf1rs.getLong(1));
				utrnpf.setCompany(sqlUfndpf1rs.getString(2));
				utrnpf.setVirtualFund(sqlUfndpf1rs.getString(3));
				utrnpf.setUnitType(sqlUfndpf1rs.getString(4));
				utrnpf.setNofunts(sqlUfndpf1rs.getBigDecimal(5));

				if (utrnpfMap.containsKey(utrnpf.getVirtualFund())) {
					utrnpfMap.get(utrnpf.getVirtualFund()).add(utrnpf);
				} else {
					List<Ufndpf> ufndpfList = new ArrayList<>();
					ufndpfList.add(utrnpf);
					utrnpfMap.put(utrnpf.getVirtualFund(), ufndpfList);
				}

			}
		} catch (SQLException e) {
			LOGGER.error("searchUfndRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUfndSelect, sqlUfndpf1rs);
		}
		return utrnpfMap;
	}

	public int countUfndRecord() {

		StringBuilder sqlUfndSelect1 = new StringBuilder("SELECT COUNT(*) FROM UFNDUDL ");
		PreparedStatement psUfndSelect = getPrepareStatement(sqlUfndSelect1.toString());
		ResultSet sqlUfndpf1rs = null;
		try {
			sqlUfndpf1rs = executeQuery(psUfndSelect);
			if (sqlUfndpf1rs.next()) {
				return sqlUfndpf1rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error("countUfndRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUfndSelect, sqlUfndpf1rs);
		}
		return 0;
	}
	
	public List<Ufndpf> searchAllUfndRecord() {
		StringBuilder sqlUfndSelect1 = new StringBuilder("SELECT UNIQUE_NUMBER, COMPANY, VRTFUND, UNITYP, NOFUNTS ");
		sqlUfndSelect1.append(" FROM UFNDUDL ");
		sqlUfndSelect1.append(" ORDER BY COMPANY ASC, VRTFUND ASC, UNITYP ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psUfndSelect = getPrepareStatement(sqlUfndSelect1.toString());
		ResultSet sqlUfndpf1rs = null;
		List<Ufndpf> ufndpfList = new ArrayList<>();
		try {
			sqlUfndpf1rs = executeQuery(psUfndSelect);
			while (sqlUfndpf1rs.next()) {
				Ufndpf utrnpf = new Ufndpf();
				utrnpf.setUniqueNumber(sqlUfndpf1rs.getLong(1));
				utrnpf.setCompany(sqlUfndpf1rs.getString(2));
				utrnpf.setVirtualFund(sqlUfndpf1rs.getString(3));
				utrnpf.setUnitType(sqlUfndpf1rs.getString(4));
				utrnpf.setNofunts(sqlUfndpf1rs.getBigDecimal(5));
				ufndpfList.add(utrnpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchAllUfndRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUfndSelect, sqlUfndpf1rs);
		}
		return ufndpfList;
	}
}