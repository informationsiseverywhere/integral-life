package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5412screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 17, 22, 18, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5412ScreenVars sv = (S5412ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5412screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5412ScreenVars screenVars = (S5412ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.dteeffDisp.setClassString("");
		screenVars.jobno.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.todateDisp.setClassString("");
		screenVars.action.setClassString("");
		screenVars.unitPartFund.setClassString("");
	}

/**
 * Clear all the variables in S5412screen
 */
	public static void clear(VarModel pv) {
		S5412ScreenVars screenVars = (S5412ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.dteeffDisp.clear();
		screenVars.dteeff.clear();
		screenVars.jobno.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.todateDisp.clear();
		screenVars.todate.clear();
		screenVars.action.clear();
		screenVars.unitPartFund.clear();
	}
}
