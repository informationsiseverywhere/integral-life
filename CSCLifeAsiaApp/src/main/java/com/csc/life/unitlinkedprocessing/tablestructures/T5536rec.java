package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:28
 * Description:
 * Copybook name: T5536REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5536rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5536Rec = new FixedLengthStringData(752); //500
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(12).isAPartOf(t5536Rec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(6, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData maxPeriodas = new FixedLengthStringData(28).isAPartOf(t5536Rec, 12);
  	public ZonedDecimalData[] maxPerioda = ZDArrayPartOfStructure(7, 4, 0, maxPeriodas, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(maxPeriodas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxPerioda01 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData maxPerioda02 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData maxPerioda03 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData maxPerioda04 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData maxPerioda05 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData maxPerioda06 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 20);
  	public ZonedDecimalData maxPerioda07 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 24);
  	
  	public FixedLengthStringData maxPeriodbs = new FixedLengthStringData(28).isAPartOf(t5536Rec, 40);  //28
  	public ZonedDecimalData[] maxPeriodb = ZDArrayPartOfStructure(7, 4, 0, maxPeriodbs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(28).isAPartOf(maxPeriodbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxPeriodb01 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData maxPeriodb02 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData maxPeriodb03 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData maxPeriodb04 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData maxPeriodb05 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 16);
  	public ZonedDecimalData maxPeriodb06 = new ZonedDecimalData(4, 0).isAPartOf(filler2,20);
  	public ZonedDecimalData maxPeriodb07 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 24);
  	
  	public FixedLengthStringData maxPeriodcs = new FixedLengthStringData(28).isAPartOf(t5536Rec, 68); //44
  	public ZonedDecimalData[] maxPeriodc = ZDArrayPartOfStructure(7, 4, 0, maxPeriodcs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(28).isAPartOf(maxPeriodcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxPeriodc01 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData maxPeriodc02 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 4);
  	public ZonedDecimalData maxPeriodc03 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 8);
  	public ZonedDecimalData maxPeriodc04 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData maxPeriodc05 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 16);
  	public ZonedDecimalData maxPeriodc06 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 20);
  	public ZonedDecimalData maxPeriodc07 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 24);
  	
  	public FixedLengthStringData maxPeriodds = new FixedLengthStringData(28).isAPartOf(t5536Rec, 96); //60
  	public ZonedDecimalData[] maxPeriodd = ZDArrayPartOfStructure(7, 4, 0, maxPeriodds, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(28).isAPartOf(maxPeriodds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxPeriodd01 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData maxPeriodd02 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 4);
  	public ZonedDecimalData maxPeriodd03 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 8);
  	public ZonedDecimalData maxPeriodd04 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData maxPeriodd05 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 16);
  	public ZonedDecimalData maxPeriodd06 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 20);
  	public ZonedDecimalData maxPeriodd07 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 24);
  	
  	public FixedLengthStringData maxPeriodes = new FixedLengthStringData(28).isAPartOf(t5536Rec, 124); //76
  	public ZonedDecimalData[] maxPeriode = ZDArrayPartOfStructure(7, 4, 0, maxPeriodes, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(28).isAPartOf(maxPeriodes, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxPeriode01 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData maxPeriode02 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 4);
  	public ZonedDecimalData maxPeriode03 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 8);
  	public ZonedDecimalData maxPeriode04 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 12);
  	public ZonedDecimalData maxPeriode05 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 16);
  	public ZonedDecimalData maxPeriode06 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 20);
  	public ZonedDecimalData maxPeriode07 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 24);
  	
  	public FixedLengthStringData maxPeriodfs = new FixedLengthStringData(28).isAPartOf(t5536Rec, 152); //92
  	public ZonedDecimalData[] maxPeriodf = ZDArrayPartOfStructure(7, 4, 0, maxPeriodfs, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(28).isAPartOf(maxPeriodfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxPeriodf01 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 0);
  	public ZonedDecimalData maxPeriodf02 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 4);
  	public ZonedDecimalData maxPeriodf03 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 8);
  	public ZonedDecimalData maxPeriodf04 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 12);
  	public ZonedDecimalData maxPeriodf05 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 16);
  	public ZonedDecimalData maxPeriodf06 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 20);
  	public ZonedDecimalData maxPeriodf07 = new ZonedDecimalData(4, 0).isAPartOf(filler6, 24);
  	
  	public FixedLengthStringData pcInitUnitsas = new FixedLengthStringData(35).isAPartOf(t5536Rec, 180); //108
  	public ZonedDecimalData[] pcInitUnitsa = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsas, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcInitUnitsa01 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 0);
  	public ZonedDecimalData pcInitUnitsa02 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 5);
  	public ZonedDecimalData pcInitUnitsa03 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 10);
  	public ZonedDecimalData pcInitUnitsa04 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 15);
  	public ZonedDecimalData pcInitUnitsa05 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 20);
  	public ZonedDecimalData pcInitUnitsa06 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 25);
  	public ZonedDecimalData pcInitUnitsa07 = new ZonedDecimalData(5, 2).isAPartOf(filler7, 30);
  	
  	
  	public FixedLengthStringData pcInitUnitsbs = new FixedLengthStringData(35).isAPartOf(t5536Rec, 215); //128
  	public ZonedDecimalData[] pcInitUnitsb = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsbs, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcInitUnitsb01 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 0);
  	public ZonedDecimalData pcInitUnitsb02 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 5);
  	public ZonedDecimalData pcInitUnitsb03 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 10);
  	public ZonedDecimalData pcInitUnitsb04 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 15);
  	public ZonedDecimalData pcInitUnitsb05 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 20);
  	public ZonedDecimalData pcInitUnitsb06 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 25);
  	public ZonedDecimalData pcInitUnitsb07 = new ZonedDecimalData(5, 2).isAPartOf(filler8, 30);
  	
  	public FixedLengthStringData pcInitUnitscs = new FixedLengthStringData(35).isAPartOf(t5536Rec, 250); //148
  	public ZonedDecimalData[] pcInitUnitsc = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitscs, 0);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(35).isAPartOf(pcInitUnitscs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcInitUnitsc01 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 0);
  	public ZonedDecimalData pcInitUnitsc02 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 5);
  	public ZonedDecimalData pcInitUnitsc03 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 10);
  	public ZonedDecimalData pcInitUnitsc04 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 15);
  	public ZonedDecimalData pcInitUnitsc05 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 20);
  	public ZonedDecimalData pcInitUnitsc06 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 25);
  	public ZonedDecimalData pcInitUnitsc07 = new ZonedDecimalData(5, 2).isAPartOf(filler9, 30);
  	
  	public FixedLengthStringData pcInitUnitsds = new FixedLengthStringData(35).isAPartOf(t5536Rec, 285); //168
  	public ZonedDecimalData[] pcInitUnitsd = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsds, 0);
  	public FixedLengthStringData filler10 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcInitUnitsd01 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 0);
  	public ZonedDecimalData pcInitUnitsd02 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 5);
  	public ZonedDecimalData pcInitUnitsd03 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 10);
  	public ZonedDecimalData pcInitUnitsd04 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 15);
  	public ZonedDecimalData pcInitUnitsd05 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 20);
  	public ZonedDecimalData pcInitUnitsd06 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 25);
  	public ZonedDecimalData pcInitUnitsd07 = new ZonedDecimalData(5, 2).isAPartOf(filler10, 30);
  	
  	public FixedLengthStringData pcInitUnitses = new FixedLengthStringData(35).isAPartOf(t5536Rec, 320); //188
  	public ZonedDecimalData[] pcInitUnitse = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitses, 0);
  	public FixedLengthStringData filler11 = new FixedLengthStringData(35).isAPartOf(pcInitUnitses, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcInitUnitse01 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 0);
  	public ZonedDecimalData pcInitUnitse02 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 5);
  	public ZonedDecimalData pcInitUnitse03 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 10);
  	public ZonedDecimalData pcInitUnitse04 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 15);
  	public ZonedDecimalData pcInitUnitse05 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 20);
  	public ZonedDecimalData pcInitUnitse06 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 25);
  	public ZonedDecimalData pcInitUnitse07 = new ZonedDecimalData(5, 2).isAPartOf(filler11, 30);
  	
  	public FixedLengthStringData pcInitUnitsfs = new FixedLengthStringData(35).isAPartOf(t5536Rec, 355); //208
  	public ZonedDecimalData[] pcInitUnitsf = ZDArrayPartOfStructure(7, 5, 2, pcInitUnitsfs, 0);
  	public FixedLengthStringData filler12 = new FixedLengthStringData(35).isAPartOf(pcInitUnitsfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcInitUnitsf01 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 0);
  	public ZonedDecimalData pcInitUnitsf02 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 5);
  	public ZonedDecimalData pcInitUnitsf03 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 10);
  	public ZonedDecimalData pcInitUnitsf04 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 15);
  	public ZonedDecimalData pcInitUnitsf05 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 20);
  	public ZonedDecimalData pcInitUnitsf06 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 25);
  	public ZonedDecimalData pcInitUnitsf07 = new ZonedDecimalData(5, 2).isAPartOf(filler12, 30);
  	
  	
  	public FixedLengthStringData pcUnitsas = new FixedLengthStringData(35).isAPartOf(t5536Rec, 390); //228
  	public ZonedDecimalData[] pcUnitsa = ZDArrayPartOfStructure(7, 5, 2, pcUnitsas, 0);
  	public FixedLengthStringData filler13 = new FixedLengthStringData(35).isAPartOf(pcUnitsas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcUnitsa01 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 0);
  	public ZonedDecimalData pcUnitsa02 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 5);
  	public ZonedDecimalData pcUnitsa03 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 10);
  	public ZonedDecimalData pcUnitsa04 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 15);
 	public ZonedDecimalData pcUnitsa05 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 20);
  	public ZonedDecimalData pcUnitsa06 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 25);
  	public ZonedDecimalData pcUnitsa07 = new ZonedDecimalData(5, 2).isAPartOf(filler13, 30);
  	
  	
  	public FixedLengthStringData pcUnitsbs = new FixedLengthStringData(35).isAPartOf(t5536Rec, 425); //248
  	public ZonedDecimalData[] pcUnitsb = ZDArrayPartOfStructure(7, 5, 2, pcUnitsbs, 0);
  	public FixedLengthStringData filler14 = new FixedLengthStringData(35).isAPartOf(pcUnitsbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcUnitsb01 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 0);
  	public ZonedDecimalData pcUnitsb02 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 5);
  	public ZonedDecimalData pcUnitsb03 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 10);
  	public ZonedDecimalData pcUnitsb04 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 15);
  	public ZonedDecimalData pcUnitsb05 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 20);
  	public ZonedDecimalData pcUnitsb06 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 25);
  	public ZonedDecimalData pcUnitsb07 = new ZonedDecimalData(5, 2).isAPartOf(filler14, 30);
  	
  	public FixedLengthStringData pcUnitscs = new FixedLengthStringData(35).isAPartOf(t5536Rec, 460);  //268
  	public ZonedDecimalData[] pcUnitsc = ZDArrayPartOfStructure(7, 5, 2, pcUnitscs, 0); 
  	public FixedLengthStringData filler15 = new FixedLengthStringData(35).isAPartOf(pcUnitscs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcUnitsc01 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 0);
  	public ZonedDecimalData pcUnitsc02 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 5);
  	public ZonedDecimalData pcUnitsc03 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 10);
  	public ZonedDecimalData pcUnitsc04 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 15);
  	public ZonedDecimalData pcUnitsc05 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 5);
  	public ZonedDecimalData pcUnitsc06 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 10);
  	public ZonedDecimalData pcUnitsc07 = new ZonedDecimalData(5, 2).isAPartOf(filler15, 15);
  	
  	
  	public FixedLengthStringData pcUnitsds = new FixedLengthStringData(35).isAPartOf(t5536Rec, 495);  //288
  	public ZonedDecimalData[] pcUnitsd = ZDArrayPartOfStructure(7, 5, 2, pcUnitsds, 0);
  	public FixedLengthStringData filler16 = new FixedLengthStringData(35).isAPartOf(pcUnitsds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcUnitsd01 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 0);
  	public ZonedDecimalData pcUnitsd02 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 5);
  	public ZonedDecimalData pcUnitsd03 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 10);
  	public ZonedDecimalData pcUnitsd04 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 15);
  	public ZonedDecimalData pcUnitsd05 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 20);
  	public ZonedDecimalData pcUnitsd06 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 25);
  	public ZonedDecimalData pcUnitsd07 = new ZonedDecimalData(5, 2).isAPartOf(filler16, 30);
  	
  	public FixedLengthStringData pcUnitses = new FixedLengthStringData(35).isAPartOf(t5536Rec, 530);  //308
  	public ZonedDecimalData[] pcUnitse = ZDArrayPartOfStructure(7, 5, 2, pcUnitses, 0);
  	public FixedLengthStringData filler17 = new FixedLengthStringData(35).isAPartOf(pcUnitses, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcUnitse01 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 0);
  	public ZonedDecimalData pcUnitse02 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 5);
  	public ZonedDecimalData pcUnitse03 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 10);
  	public ZonedDecimalData pcUnitse04 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 15);
 	public ZonedDecimalData pcUnitse05 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 20);
  	public ZonedDecimalData pcUnitse06 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 25);
  	public ZonedDecimalData pcUnitse07 = new ZonedDecimalData(5, 2).isAPartOf(filler17, 30);
  	
  	
  	public FixedLengthStringData pcUnitsfs = new FixedLengthStringData(35).isAPartOf(t5536Rec, 565);  //328
  	public ZonedDecimalData[] pcUnitsf = ZDArrayPartOfStructure(7, 5, 2, pcUnitsfs, 0);
  	public FixedLengthStringData filler18 = new FixedLengthStringData(35).isAPartOf(pcUnitsfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcUnitsf01 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 0);
  	public ZonedDecimalData pcUnitsf02 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 5);
  	public ZonedDecimalData pcUnitsf03 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 10);
  	public ZonedDecimalData pcUnitsf04 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 15);
  	public ZonedDecimalData pcUnitsf05 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 20);
  	public ZonedDecimalData pcUnitsf06 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 25);
  	public ZonedDecimalData pcUnitsf07 = new ZonedDecimalData(5, 2).isAPartOf(filler18, 30);
  	
  	public FixedLengthStringData yotalmth = new FixedLengthStringData(4).isAPartOf(t5536Rec, 600);  //348
  	public FixedLengthStringData filler19 = new FixedLengthStringData(148).isAPartOf(t5536Rec, 604, FILLER); //352


	public void initialize() {
		COBOLFunctions.initialize(t5536Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5536Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}