package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:55
 * Description:
 * Copybook name: INCIBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incibrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incibrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incibrkKey = new FixedLengthStringData(64).isAPartOf(incibrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData incibrkChdrcoy = new FixedLengthStringData(1).isAPartOf(incibrkKey, 0);
  	public FixedLengthStringData incibrkChdrnum = new FixedLengthStringData(8).isAPartOf(incibrkKey, 1);
  	public FixedLengthStringData incibrkCoverage = new FixedLengthStringData(2).isAPartOf(incibrkKey, 9);
  	public FixedLengthStringData incibrkRider = new FixedLengthStringData(2).isAPartOf(incibrkKey, 11);
  	public PackedDecimalData incibrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incibrkKey, 13);
  	public FixedLengthStringData incibrkLife = new FixedLengthStringData(2).isAPartOf(incibrkKey, 16);
  	public PackedDecimalData incibrkInciNum = new PackedDecimalData(3, 0).isAPartOf(incibrkKey, 18);
  	public PackedDecimalData incibrkSeqno = new PackedDecimalData(2, 0).isAPartOf(incibrkKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(incibrkKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incibrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incibrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}