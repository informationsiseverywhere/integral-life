package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:44
 * Description:
 * Copybook name: USTFSTMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustfstmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustfstmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustfstmKey = new FixedLengthStringData(64).isAPartOf(ustfstmFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustfstmChdrcoy = new FixedLengthStringData(1).isAPartOf(ustfstmKey, 0);
  	public FixedLengthStringData ustfstmChdrnum = new FixedLengthStringData(8).isAPartOf(ustfstmKey, 1);
  	public PackedDecimalData ustfstmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ustfstmKey, 9);
  	public FixedLengthStringData ustfstmLife = new FixedLengthStringData(2).isAPartOf(ustfstmKey, 12);
  	public FixedLengthStringData ustfstmCoverage = new FixedLengthStringData(2).isAPartOf(ustfstmKey, 14);
  	public FixedLengthStringData ustfstmRider = new FixedLengthStringData(2).isAPartOf(ustfstmKey, 16);
  	public FixedLengthStringData ustfstmUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(ustfstmKey, 18);
  	public FixedLengthStringData ustfstmUnitType = new FixedLengthStringData(1).isAPartOf(ustfstmKey, 22);
  	public PackedDecimalData ustfstmUstmno = new PackedDecimalData(5, 0).isAPartOf(ustfstmKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(ustfstmKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustfstmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustfstmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}