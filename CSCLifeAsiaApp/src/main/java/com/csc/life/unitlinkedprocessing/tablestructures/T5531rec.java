package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:22
 * Description:
 * Copybook name: T5531REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5531rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5531Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData chargeDeductInd = new FixedLengthStringData(1).isAPartOf(t5531Rec, 0);
  	public FixedLengthStringData cgmeths = new FixedLengthStringData(128).isAPartOf(t5531Rec, 1);
  	public FixedLengthStringData[] cgmeth = FLSArrayPartOfStructure(32, 4, cgmeths, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(128).isAPartOf(cgmeths, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cgmeth01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData cgmeth02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData cgmeth03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData cgmeth04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData cgmeth05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData cgmeth06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData cgmeth07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData cgmeth08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData cgmeth09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData cgmeth10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData cgmeth11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData cgmeth12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData cgmeth13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData cgmeth14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData cgmeth15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData cgmeth16 = new FixedLengthStringData(4).isAPartOf(filler, 60);
  	public FixedLengthStringData cgmeth17 = new FixedLengthStringData(4).isAPartOf(filler, 64);
  	public FixedLengthStringData cgmeth18 = new FixedLengthStringData(4).isAPartOf(filler, 68);
  	public FixedLengthStringData cgmeth19 = new FixedLengthStringData(4).isAPartOf(filler, 72);
  	public FixedLengthStringData cgmeth20 = new FixedLengthStringData(4).isAPartOf(filler, 76);
  	public FixedLengthStringData cgmeth21 = new FixedLengthStringData(4).isAPartOf(filler, 80);
  	public FixedLengthStringData cgmeth22 = new FixedLengthStringData(4).isAPartOf(filler, 84);
  	public FixedLengthStringData cgmeth23 = new FixedLengthStringData(4).isAPartOf(filler, 88);
  	public FixedLengthStringData cgmeth24 = new FixedLengthStringData(4).isAPartOf(filler, 92);
  	public FixedLengthStringData cgmeth25 = new FixedLengthStringData(4).isAPartOf(filler, 96);
  	public FixedLengthStringData cgmeth26 = new FixedLengthStringData(4).isAPartOf(filler, 100);
  	public FixedLengthStringData cgmeth27 = new FixedLengthStringData(4).isAPartOf(filler, 104);
  	public FixedLengthStringData cgmeth28 = new FixedLengthStringData(4).isAPartOf(filler, 108);
  	public FixedLengthStringData cgmeth29 = new FixedLengthStringData(4).isAPartOf(filler, 112);
  	public FixedLengthStringData cgmeth30 = new FixedLengthStringData(4).isAPartOf(filler, 116);
  	public FixedLengthStringData cgmeth31 = new FixedLengthStringData(4).isAPartOf(filler, 120);
  	public FixedLengthStringData cgmeth32 = new FixedLengthStringData(4).isAPartOf(filler, 124);
  	public FixedLengthStringData frequencys = new FixedLengthStringData(16).isAPartOf(t5531Rec, 129);
  	public FixedLengthStringData[] frequency = FLSArrayPartOfStructure(8, 2, frequencys, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(frequencys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData frequency01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData frequency02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData frequency03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData frequency04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData frequency05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData frequency06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData frequency07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData frequency08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(355).isAPartOf(t5531Rec, 145, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5531Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5531Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}