package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:46
 * Description:
 * Copybook name: USWHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Uswhkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData uswhFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData uswhKey = new FixedLengthStringData(256).isAPartOf(uswhFileKey, 0, REDEFINE);
  	public FixedLengthStringData uswhChdrcoy = new FixedLengthStringData(1).isAPartOf(uswhKey, 0);
  	public FixedLengthStringData uswhChdrnum = new FixedLengthStringData(8).isAPartOf(uswhKey, 1);
  	public PackedDecimalData uswhTranno = new PackedDecimalData(5, 0).isAPartOf(uswhKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(uswhKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(uswhFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		uswhFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}