package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Ureppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UreppfDAO extends BaseDAO<Ureppf> {
	public void updateUreppfRecord(List<Ureppf> urList);
	public Map<String, List<Ureppf>> searchSeqByChdrnum(List<String> chdrnumList);
	public void insertUreppfRecord(List<Ureppf> urList);
}