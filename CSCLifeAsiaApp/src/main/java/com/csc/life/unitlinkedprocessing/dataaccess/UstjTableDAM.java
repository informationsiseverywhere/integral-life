package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UstjTableDAM.java
 * Date: Sun, 30 Aug 2009 03:51:35
 * Class transformed from USTJ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UstjTableDAM extends UstmpfTableDAM {

	public UstjTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("USTJ");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "JCTLJNUMST"
		             + ", CHDRCOY"
		             + ", CHDRNUM"
		             + ", EFFDATE";
		
		QUALIFIEDCOLUMNS = 
		            "JCTLJNUMST, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "EFFDATE, " +
		            "USTMTFLAG, " +
		            "TRANNO, " +
		            "BATCPFX, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "VALIDFLAG, " +
		            "CNTCURR, " +
		            "USTMNO, " +
		            "STRPDATE, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "USTMTYP, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "JCTLJNUMST ASC, " +
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "EFFDATE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "JCTLJNUMST DESC, " +
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "EFFDATE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobnoStmt,
                               chdrcoy,
                               chdrnum,
                               effdate,
                               unitStmtFlag,
                               tranno,
                               batcpfx,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               validflag,
                               cntcurr,
                               ustmno,
                               strpdate,
                               clntcoy,
                               clntnum,
                               stmtType,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(45);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getJobnoStmt().toInternal()
					+ getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getEffdate().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobnoStmt);
			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(jobnoStmt.toInternal());
	nonKeyFiller20.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());
	nonKeyFiller40.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(110);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getUnitStmtFlag().toInternal()
					+ getTranno().toInternal()
					+ getBatcpfx().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getValidflag().toInternal()
					+ getCntcurr().toInternal()
					+ getUstmno().toInternal()
					+ getStrpdate().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getStmtType().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, unitStmtFlag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, batcpfx);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, ustmno);
			what = ExternalData.chop(what, strpdate);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, stmtType);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public PackedDecimalData getJobnoStmt() {
		return jobnoStmt;
	}
	public void setJobnoStmt(Object what) {
		setJobnoStmt(what, false);
	}
	public void setJobnoStmt(Object what, boolean rounded) {
		if (rounded)
			jobnoStmt.setRounded(what);
		else
			jobnoStmt.set(what);
	}
	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getUnitStmtFlag() {
		return unitStmtFlag;
	}
	public void setUnitStmtFlag(Object what) {
		unitStmtFlag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(Object what) {
		batcpfx.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getUstmno() {
		return ustmno;
	}
	public void setUstmno(Object what) {
		setUstmno(what, false);
	}
	public void setUstmno(Object what, boolean rounded) {
		if (rounded)
			ustmno.setRounded(what);
		else
			ustmno.set(what);
	}	
	public PackedDecimalData getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(Object what) {
		setStrpdate(what, false);
	}
	public void setStrpdate(Object what, boolean rounded) {
		if (rounded)
			strpdate.setRounded(what);
		else
			strpdate.set(what);
	}	
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}	
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}	
	public FixedLengthStringData getStmtType() {
		return stmtType;
	}
	public void setStmtType(Object what) {
		stmtType.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		jobnoStmt.clear();
		chdrcoy.clear();
		chdrnum.clear();
		effdate.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		unitStmtFlag.clear();
		tranno.clear();
		batcpfx.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		validflag.clear();
		cntcurr.clear();
		ustmno.clear();
		strpdate.clear();
		clntcoy.clear();
		clntnum.clear();
		stmtType.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}