/*
 * File: P5133.java
 * Date: 30 August 2009 0:12:47
 * Author: Quipoz Limited
 * 
 * Class transformed from P5133.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILIFE-8255
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnaloTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S5133ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf; //ILIFE-8255
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This is the submenu for Fund Switching
*
*****************************************************************
* </pre>
*/
public class P5133 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private BinaryData wsaaSub = new BinaryData(4, 0);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5133");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);

		/* WSAA-WORK */
	private FixedLengthStringData wsaaLastOption = new FixedLengthStringData(1);
	private Validator lastOptSwitching = new Validator(wsaaLastOption, "A");
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header File - Major Alts*/
//	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM(); //ILIFE-8255
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
		/*Unit Switch Header Details.*/
	private UswhTableDAM uswhIO = new UswhTableDAM();
		/*Undealt UTRNs*/
	private UtrnaloTableDAM utrnaloIO = new UtrnaloTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatchkey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5133ScreenVars sv = ScreenProgram.getScreenVars( S5133ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);
 //ILIFE-8255 start	
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
 //ILIFE-8255 end 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2290, 
		exit2890, 
		exit2990, 
		keeps3070, 
		batching3080, 
		exit3090, 
		continue3140, 
		exit3190
	}

	public P5133() {
		super();
		screenVars = sv;
		new ScreenModel("S5133", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void initialise1000()
	{
			initialise1100();
		}

protected void initialise1100()
	{
		/*  Release the USWH  I/O Module                                   */
		uswhIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(uswhIO.getParams());
		}
		if (isEQ(wsspcomn.flag,"K")) {
			wsspcomn.flag.set(SPACES);
			sv.redirectionRequired.set("N");
			wsaaLastOption.set(SPACES);
		}
		/* Test if BRKOUT in program P5143 which implies that redirection  */
		/* is not possible.                                                */
		if (isEQ(wsspcomn.flag,"B")) {
			wsspcomn.flag.set(SPACES);
			if (isEQ(sv.redirectionRequired,"Y")) {
				wsaaLastOption.set(SPACES);
				sv.redirectionRequired.set("N");
				scrnparams.errorCode.set(errorsInner.h977);
				/*****       MOVE F010               TO SCRN-ERROR-CODE       <003> */
			}
		}
		if (lastOptSwitching.isTrue()
		&& isEQ(sv.redirectionRequired,"Y")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		sv.redirectionRequired.set("N");
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Do not continue to fund redirection if F1 has been pressed *    */
		/* in subsequent programs.                                    *    */
		/* After having executed the switch,control is passed back to the  */
		/* Submenu so that,if a redirection had been selected, the         */
		/* transaction code can be reset. In non-client server mode        */
		/* the call to the screen IO in the 2000 section will be           */
		/* bypassed. In client-server mode the call to the screen IO is    */
		/* still taking place. To correct the situation the logic to reset */
		/* transaction code and check authorisation has been duplicated    */
		/* within the PRE section (by performing the new PRE-SET-TRANCODE  */
		/* section )from within the PRE section and the WSSP-SECTIONNO set */
		/* to 3000 so that the 2000 section will be bypassed.              */
		if (lastOptSwitching.isTrue()
		&& isEQ(sv.redirectionRequired,"Y")) {
			scrnparams.action.set("C");
			sv.redirectionRequired.set("N");
			wsspcomn.edterror.set(varcom.oK);
			/*    GO 2020-VALIDATE.                                         */
			/*    PERFORM 2050-VALIDATIONS                          <A07843>*/
			preResetTrancode();
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void preResetTrancode()
	{
		preResetTrancodeStart();
	}

protected void preResetTrancodeStart()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validations2050();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validations2050()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			return ;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
	}

protected void validateKeys2200()
	{
		try {
			chdrmja2230();
			ulnk2240();
			rlseCovr2250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void chdrmja2230()
	{
		if (isEQ(sv.chdrsel,SPACES)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
 //ILIFE-8255 start		
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrsel.toString());
		if((chdrpf == null) && isEQ(subprogrec.key1,"Y")){
			sv.chdrselErr.set(errorsInner.f259);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		/*chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(errorsInner.f259);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}*/
 //ILIFE-8255 end
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		/* Read table T5679 to evaluate the statuses                       */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		checkStatuses2800();
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(chdrpf.getChdrcoy()); //ILIFE-8255
		chdrenqIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8255
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void ulnk2240()
	{
		ulnkIO.setParams(SPACES);
		ulnkIO.setChdrcoy(wsspcomn.company);
		ulnkIO.setChdrnum(sv.chdrsel);
		wsaaChdrnum.set(sv.chdrsel);
		ulnkIO.setPlanSuffix(ZERO);
		ulnkIO.setFunction(varcom.begn);
		//start life performance atiwari23
		ulnkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ulnkIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		//end;
		SmartFileCode.execute(appVars, ulnkIO);
		if ((isEQ(ulnkIO.getStatuz(),varcom.endp)
		|| (isNE(ulnkIO.getChdrcoy(),wsspcomn.company)
		|| isNE(ulnkIO.getChdrnum(),wsaaChdrnum))) && (sv.chdrselErr.equals(SPACES))) {
			/*     MOVE U053                 TO S5133-CHDRSEL-ERR.           */
			sv.chdrselErr.set(errorsInner.g182);
		}
		if (isNE(scrnparams.action,"B")
		&& isNE(scrnparams.action,"C")) {
			checkUtrns2300();
		}
	}

protected void rlseCovr2250()
	{
		/*    Release any previously kept COVRMJA record.*/
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		/*REDIR-IND*/
		if (isNE(sv.redirectionRequired,"N")
		&& isNE(sv.redirectionRequired,"Y")) {
			sv.redirreqErr.set(errorsInner.g941);
		}
		/* If premium redirection requested but this is a single premium   */
		/* policy, display error as redirection only allowed on regular    */
		/* premium policies.                                               */
		if (isEQ(sv.redirectionRequired, "Y")
		&& isEQ(chdrpf.getBillfreq(), "00")) {
			sv.redirreqErr.set(errorsInner.h085);
		}
		
		//AFR
		 if ((isNE(sv.chdrsel, SPACES) || isEQ(sv.redirectionRequired, "Y") || isEQ(sv.redirectionRequired, "N"))) {
	        	checkzswrPos4100();
	           }
		 
		 //AFR
	}


protected void checkzswrPos4100() 
{
	List<Zswrpf> zswrpfiList=new ArrayList<>();
	Zswrpf zw=new Zswrpf();
	zw.setChdrpfx(chdrpf.getChdrpfx()); /* IJTI-1523 */
	zw.setChdrcoy(chdrpf.getChdrcoy().toString()); //ILIFE-8255
	zw.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1523 */ 
	zw.setValidflag("1" );
	zswrpfiList=zswrDAO.getZswrpfData( zw.getChdrnum() ,zw.getChdrcoy(),  zw.getChdrpfx()  ,  zw.getValidflag() );/* IJTI-1523 */
	if (zswrpfiList.size()>0 )
	{		
		 for (Zswrpf p : zswrpfiList) 
		 {   
			if(isNE(p.getZafritem(),"NO") && isEQ(sv.action ,"A")) 
			{
				sv.chdrselErr.set(errorsInner.rfu6);
				wsspcomn.edterror.set("Y");
			}
		}
	}
 
}
protected void checkUtrns2300()
	{
		checkUtrns2310();
	}

protected void checkUtrns2310()
	{
		/* check if outstanding UTRNS exist.  If they do exist, disallow   */
		/* fund switch                                                     */
		utrnaloIO.setRecKeyData(SPACES);
		utrnaloIO.setRecNonKeyData(SPACES);
		utrnaloIO.setChdrcoy(wsspcomn.company);
		utrnaloIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8255
		utrnaloIO.setFunction(varcom.begn);
		//start life performance atiwari23
		utrnaloIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnaloIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		//end;
		SmartFileCode.execute(appVars, utrnaloIO);
		if (isNE(utrnaloIO.getStatuz(),varcom.oK)
		&& isNE(utrnaloIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(utrnaloIO.getStatuz());
			syserrrec.params.set(utrnaloIO.getParams());
			fatalError600();
		}
		/* Where there are unprocessed UTRNS on a contract, regardless of  */
		/* whether they are for a single policy where the contract is a    */
		/* multiple policy contract, display the error message and do not  */
		/* allow the switch to take place.                                 */
		if (isNE(utrnaloIO.getChdrcoy(),wsspcomn.company)
		|| isNE(utrnaloIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(utrnaloIO.getStatuz(),varcom.endp)) {
			utrnaloIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(sv.action ,"D")) {
				sv.chdrselErr.set(errorsInner.rfu7);
			/* IBPLIFE-9780 */	 
			}else
			{
				if (isEQ(sv.chdrselErr,SPACES))
				{
				sv.chdrselErr.set(errorsInner.h355);
				}
			}
		}
	}

	/**
	* <pre>
	*     VALIDATE STATUS CODES FROM CONTRACT HEADER Vs T5679         
	* </pre>
	*/
protected void checkStatuses2800()
	{
		try {
		validateStatcode2810();
		validatePremcode2820();
		}
		catch (GOTOException e){
		/* Expected exception for control flow purposes. */
		}
	}

protected void validateStatcode2810()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.chdrselErr.set(errorsInner.e767);
			wsspcomn.edterror.set("Y");
			wsaaSub.set(ZERO);
			/*     GO TO 2290-EXIT                                   <S19FIX>*/
			goTo(GotoLabel.exit2890);
		}
		else {
			if (isNE(chdrpf.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				validateStatcode2810();
				return ;
			}
		}
		wsaaSub.set(ZERO);
	}

protected void validatePremcode2820()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.chdrselErr.set(errorsInner.h504);
			wsspcomn.edterror.set("Y");
			wsaaSub.set(ZERO);
			return ;
			/****     GO TO 2290-EXIT                                   <S19FIX>*/
		}
		else {
			if (isNE(chdrpf.getPstcde(),t5679rec.cnPremStat[wsaaSub.toInt()])) {
				validatePremcode2820();
				return ;
			}
		}
		wsaaSub.set(ZERO);
	}

protected void verifyBatchControl2900()
	{
		try {
			verifyBatchControl2300();
			batchProgs2310();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void verifyBatchControl2300()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(errorsInner.e073);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2990);
		}
		bcbprogrec.transcd.set(subprogrec.transcd);
	}

protected void batchProgs2310()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3100();
					sftlock3030();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3100()
	{
		wsaaLastOption.set(sv.action);
		if (isEQ(scrnparams.action,"C")) {
			wsspcomn.sbmaction.set("A");
		}
		else {
			wsspcomn.sbmaction.set(scrnparams.action);
		}
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		if (isNE(subprogrec.bchrqd,"Y")) {
			goTo(GotoLabel.keeps3070);
		}
	}

protected void sftlock3030()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum()); //ILIFE-8255
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void keeps3070()
	{
		/*    Store the Contract Header & action code*/
		if (isEQ(sv.action,"A")) {
			wsspcomn.flag.set("F");
 //ILIFE-8255 start
			chdrpfDAO.setCacheObject(chdrpf);
			if(chdrpf == null ){
				fatalError600();
			}
			else {
				goTo(GotoLabel.batching3080);
			}
			}
			/*chdrmjaIO.setFormat(chdrmjarec);
			chdrmjaIO.setFunction("KEEPS");
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				goTo(GotoLabel.batching3080);
			}
		}*/
		wsspcomn.flag.set("I");
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8255 end
	}

protected void batching3080()
	{
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateBatchControl3110();
					openNewBatch3130();
				case continue3140: 
					continue3140();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateBatchControl3110()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz,varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(batcchkrec.statuz,varcom.dupr)) {
			sv.actionErr.set(errorsInner.e132);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(batcchkrec.statuz,"INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.continue3140);
		}
		if (isNE(batcchkrec.statuz,varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3190);
		}
	}

protected void openNewBatch3130()
	{
		batcdorrec.function.set("AUTO");
	}

protected void continue3140()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e132 = new FixedLengthStringData(4).init("E132");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData g182 = new FixedLengthStringData(4).init("G182");
	private FixedLengthStringData g941 = new FixedLengthStringData(4).init("G941");
	private FixedLengthStringData h085 = new FixedLengthStringData(4).init("H085");
	private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");
	private FixedLengthStringData h504 = new FixedLengthStringData(4).init("H504");
	private FixedLengthStringData h977 = new FixedLengthStringData(4).init("H977");
	//AFR
	private FixedLengthStringData rfu6 = new FixedLengthStringData(4).init("RFU6");
	private FixedLengthStringData rfu7 = new FixedLengthStringData(4).init("RFU7");
}
}
