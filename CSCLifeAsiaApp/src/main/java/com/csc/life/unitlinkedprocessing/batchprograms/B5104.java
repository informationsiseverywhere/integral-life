/*
 * File: B5104.java
 * Date: 29 August 2009 20:55:23
 * Author: Quipoz Limited
 *
 * Class transformed from B5104.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.CovupfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UderpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Covupf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Uderpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  COVERAGE DEBT RECOVERY VIA UNIT DEALING.
*  ========================================
*
*  OVERVIEW.
*
*  This process program follows the Coverage Debt Splitter program
*  B5103. The splitter program will have extracted from COVRPF all
*  coverages with a non zero coverage debt. Each coverage key will
*  have been written to the COVU temporary file. This program will
*  read the COVU file and process each coverage present.
*
*  DETAILED DESCRIPTION.
*
*  Initialisation Section.
*
*  a) Check the restart method is '3'.
*  b) Call QCMDEXC to point the program to the correct temporary
*     file member.
*  c) Pre-load constantly referred to tables into working storage.
*     These are: T5645, T5688, T6647, T5515, T5540, T5687, T6597
*  d) Initialise constant feilds values of UTRN, PTRN
*
*  Read Section.
*
*  a) Read a COVU record from the temporary file.
*
*  Edit Section.
*
*  a) Softlock the contract on a change in contract number.
*
*  Update Section.
*
*  a) Read and hold the CHDR
*  b) Get the bid or offer from T6647 using BatcTrcde and
*      contract type.
*  c) Get accounting level from T5688 using the contract type.
*  d) Read and hold the COVR using the COVU details.
*  e) Get the "cancel intitial units" indicator using the
*     coverage/rider table from the COVR and the "initial units
*     discount factor".
*  f) Read through all the coverage level unit holdings (UTRS) for
*     the coverage (converting the unit values into the contract
*     currency if required) and store these values in a working
*     storage table keyed by fund and fund type. The norm will
*     be that the unit holdings will be positive but we separate
*     the positive and negitive unit holding as we will need to
*     buy/sell them differently.
*  g) Check if the coverage debt is negitive or positive. If the
*     debt is positive we will need to sell units to satisfy the
*     debt. If the debt is negitive (not usually the case) we
*     will be buying units.
*  h) Postive Coverage Debt (the norm).
*     Check that the value of the positive unit holdings of accum
*     units is greater than zero. If so, we sell units in proporti n
*     of each fund holding held against the coverage to satisify
*     the debt. If the debt is still greater than zero check
*     the T5540 flag to see if we can sell initial units. If so,
*     sell initial units in the same way.
*  i) Negative Coverage Debt (less usual).
*     Check that each fund holding is greater than zero. If not,
*     buy sufficient units for that fund to make the fund holding
*     greater than or equal to zero. Once all fund holdings are
*     greater than zero, buy units in proportion to the unit split
*     identified in UNLK.
*  j) Now check to see if in selling the units we have been able
*     satisfy or reduce the coverage debt. If the debt is still
*     the same as it was go to n).
*  k) Now work out whether the debt was completely satisfied or
*     only partially satisfied. Then head off to m).
*  l) If the debt has been completely satisfied by the selling
*     of units then we need to ensure that the total value of the
*     sold units is equal to the coverage debt i.e. no rounding
*     has occurred. Thus we loop through all the surrender amounts
*     held against each fund to get the total. Subtract the total
*     surrender amount from the original coverage debt to get a
*     remainder. If the remainder is zero - everything is fine.
*     If not then loop through each surrender amount adding or
*     subtracting 0.01 until the remainder is zero.
*  m) Loop through the unit holding table and create a UTRN for
*     each surrender amount. Rewrite the COVR with the reduced
*     coverage debt and, if we haven't already, update the CHDR
*     and write a PTRN.
*  n) If a coverage debt still exists, either because there were
*     no units to sell or we sold them all and the total value
*     was less than the debt, we need to work out what type of
*     "overdue" processing is required. Firstly calculate the
*     length of time the coverage has been in force. Then look
*     up T6647 to see if we should take one of three options:
*     1. Satisfy the debt by creating a negivitve unit holding
*        (by selling negitive units) on the basis that at some
*        stage in the future the unit holding will end up positive
*     2. Lapse the coverage.
*     3. Simply log it as an error and do nothing.
*
*  Finalise Section.
*
*  a) Close the temporary.
*
*
*  Control totals used in this program:
*
*  01 - COVU records read
*  02 - COVU With Locked Contracts
*  03 - Locked Contracts
*  04 - Debts > zero
*  05 - CHDRs Read
*  06 - COVRs Read
*  07 - UTRSs Read
*  08 - UTRS Funds Without Prices
*  09 - UTRSs Loaded in WS
*  10 - UTRNs Sells Created
*  11 - UTRNs Sells Value
*  12 - Debt Settled by Sells
*  13 - Debt Settled Value
*  14 - CHDRs Updates
*  15 - PTRNs Written
*  16 - NF Do Nothing
*  17 - NF Use Neg Units
*  18 - NF Increase Debt?
*  19 - NF Lapse
*  20 - NF Errors
*  21 - Debt Settled With NegU
*  22 - Debt Settled Value
*  23 - UTRNs Negs Units
*  24 - UTRNs Neh Unit Value
*  25 - Error record Written
*  26 - Debts < zero
*
*****************************************************************
* </pre>
*/
public class B5104 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	//private CovupfTableDAM covupf = new CovupfTableDAM();
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5104");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	protected PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaLapseTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(8, 0);
	protected ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4);
	private PackedDecimalData wsaaValAcumUnitsP = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaValInitUnitsP = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaValAcumUnitsN = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaValInitUnitsN = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(16, 5);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaChdrLapsed = new FixedLengthStringData(1).init("N");
	private Validator chdrLapsed = new Validator(wsaaChdrLapsed, "Y");

	private FixedLengthStringData wsaaChdrWritten = new FixedLengthStringData(1).init("N");
	private Validator chdrWritten = new Validator(wsaaChdrWritten, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	private Validator endOfCovr = new Validator(wsaaEndOfCovr, "Y");

	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init("N");
	private Validator validStatuz = new Validator(wsaaValidStatuz, "Y");

	private FixedLengthStringData wsaaPtrn = new FixedLengthStringData(1).init("N");
	private Validator ptrnWritten = new Validator(wsaaPtrn, "Y");
	private String wsaaValidStatus = "";
	private String wsaaValidPstatus = "";
	
	private PackedDecimalData wsaaOccdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCurrfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBalanceCheck = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaStoredItemitem = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaWithinRange = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaInsuffUnitsMeth = new FixedLengthStringData(1);
	private PackedDecimalData wsaaOrigCovrDebt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaPrevItemitem = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSurrPercent = new PackedDecimalData(14, 11);
	private PackedDecimalData wsaaSurrAmount = new PackedDecimalData(18, 5);

	private FixedLengthStringData wsaaChdrUpdated = new FixedLengthStringData(1).init("N");
	private Validator chdrUpdated = new Validator(wsaaChdrUpdated, "Y");
	
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaInitUnitDiscFact = new PackedDecimalData(10, 5);
	private PackedDecimalData wsaaTotalSurrAmt = new PackedDecimalData(18, 5).init(0);
	private PackedDecimalData wsaaNewCovrDebt = new PackedDecimalData(17, 2).init(0);
	private String wsaaChdrValid = "";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
		/*  COVU parameters*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaCovuFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCovuFn, 0, FILLER).init("COVU");
	private FixedLengthStringData wsaaCovuRunid = new FixedLengthStringData(2).isAPartOf(wsaaCovuFn, 4);
	private ZonedDecimalData wsaaCovuJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCovuFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

		/* WSAA-UTRS-ARRAY */
	private FixedLengthStringData[] wsaaUtrsRec = FLSInittedArray (50, 68);
	private BinaryData[] wsaaUtrsRrn = BDArrayPartOfArrayStructure(9, 0, wsaaUtrsRec, 0, 0);
	private FixedLengthStringData[] wsaaUtrsUnitVirtualFund = FLSDArrayPartOfArrayStructure(4, wsaaUtrsRec, 4);
	private FixedLengthStringData[] wsaaUtrsUnitType = FLSDArrayPartOfArrayStructure(1, wsaaUtrsRec, 8);
	private FixedLengthStringData[] wsaaUtrsFundCurr = FLSDArrayPartOfArrayStructure(4, wsaaUtrsRec, 9);	
	private PackedDecimalData[] wsaaUtrsCovFundAmtP = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 13); 
	private PackedDecimalData[] wsaaUtrsCovFundAmtN = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 22);
	private PackedDecimalData[] wsaaUtrsCurrentUnitBal = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 31);
	private PackedDecimalData[] wsaaUtrsCurrentDunitBal = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 40);
	private PackedDecimalData[] wsaaUtrsSurrAmt = PDArrayPartOfArrayStructure(17, 2, wsaaUtrsRec, 49);
	private FixedLengthStringData[] wsaaUtrsFundPool = FLSDArrayPartOfArrayStructure(1, wsaaUtrsRec, 67); 
	//private static final int wsaaT6647Size = 500;
	private static final int wsaaT6647Size = 2050;//ILIFE-2017  //ILIFE-6190
	
	//private static final int wsaaT5540Size = 50;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5540Size = 1000;

		/* WSAA-T5540-ARRAY */
	private FixedLengthStringData[] wsaaT5540Rec = FLSInittedArray (1000, 17);
	private FixedLengthStringData[] wsaaT5540Key = FLSDArrayPartOfArrayStructure(4, wsaaT5540Rec, 0);
	private FixedLengthStringData[] wsaaT5540Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5540Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5540Data = FLSDArrayPartOfArrayStructure(13, wsaaT5540Rec, 4);
	private FixedLengthStringData[] wsaaT5540UnitCancInit = FLSDArrayPartOfArrayStructure(1, wsaaT5540Data, 0);
	private FixedLengthStringData[] wsaaT5540WholeIuDiscFact = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 1);
	private FixedLengthStringData[] wsaaT5540IuDiscBasis = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 5);
	private FixedLengthStringData[] wsaaT5540IuDiscFact = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 9);
	private FixedLengthStringData wsaaT5540StoredItem = new FixedLengthStringData(8).init(SPACES);
	//private static final int wsaaT5515Size = 50;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5515Size = 1000;

	private FixedLengthStringData wsaaT5515Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFundCurrency = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 0);

		/* WSAA-T5515-ARRAY */
	private FixedLengthStringData[] wsaaT5515Rec = FLSInittedArray (1000, 7);
	private FixedLengthStringData[] wsaaT5515Key = FLSDArrayPartOfArrayStructure(4, wsaaT5515Rec, 0);
	private FixedLengthStringData[] wsaaT5515Fund = FLSDArrayPartOfArrayStructure(4, wsaaT5515Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaData = FLSDArrayPartOfArrayStructure(3, wsaaT5515Rec, 4, SPACES);
	private FixedLengthStringData[] wsaaT5515Currcode = FLSDArrayPartOfArrayStructure(3, wsaaData, 0);
	private FixedLengthStringData wsaaT5515Itemitem = new FixedLengthStringData(4).init(SPACES);
	//private static final int wsaaT5645Size = 60;
	//private static final int wsaaT5645Size = 500;//ILIFE-2017
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;

		/* WSAA-T5645 */
	//private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (60, 21);
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (1000, 21);//ILIFE-2017
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	//private static final int wsaaT5688Size = 100;
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 9);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(6, wsaaT5688Rec, 3);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
	private FixedLengthStringData wsaaStoredCnttype = new FixedLengthStringData(3).init(SPACES);

		/* WSAA-T5687-ARRAY
		 03 WSAA-T5687-REC                OCCURS 60                   */
	//private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (100, 24);
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 24);//ILIFE-2017
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(20, wsaaT5687Rec, 4);
	private FixedLengthStringData[] wsaaT5687NonForfeitMeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT6597Statuz = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 4);
	private FixedLengthStringData[] wsaaT6597Premsubr04 = FLSDArrayPartOfArrayStructure(8, wsaaT5687Data, 8);
	private FixedLengthStringData[] wsaaT6597Cpstat04 = FLSDArrayPartOfArrayStructure(2, wsaaT5687Data, 16);
	private FixedLengthStringData[] wsaaT6597Crstat04 = FLSDArrayPartOfArrayStructure(2, wsaaT5687Data, 18);
	//private static final int wsaaT5687Size = 100;
	//private static final int wsaaT5687Size = 200;//ILIFE-2017
	//ILIFE-2605 fixed--Array size increased
	private static final int wsaaT5687Size = 1000;
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";
	private static final String g027 = "G027";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData iu = new IntegerData();
	
	
	
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();

	private T5515rec t5515rec = new T5515rec();
	private T5540rec t5540rec = new T5540rec();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6597rec t6597rec = new T6597rec();
	private T5679rec t5679rec = new T5679rec();
	private T6647rec t6647rec = new T6647rec();
	private T5519rec t5519rec = new T5519rec();
	private T5539rec t5539rec = new T5539rec();
	private T6646rec t6646rec = new T6646rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaT6647ArrayInner wsaaT6647ArrayInner = new WsaaT6647ArrayInner();
	private BigDecimal wsaaAloTot;
	private BigDecimal wsaaTotCovrpuInstprem;
	protected int t5688Ix = 1;
	protected int t6647Ix = 1;
	protected int t5515Ix = 1;
	protected int t5540Ix = 1;
	private int t5687Ix = 1;
	protected int ix = 1;
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	protected UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	protected HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	protected HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	protected UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO", UlnkpfDAO.class);
	protected ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	protected PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	protected PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private CovupfDAO covupfDAO = getApplicationContext().getBean("covupfDAO", CovupfDAO.class);
	protected UderpfDAO uderpfDAO = getApplicationContext().getBean("uderpfDAO", UderpfDAO.class);
	
	protected Map<String, List<Itempf>> t5645Map = null;
	protected Map<String, List<Itempf>> t5688Map = null;
	protected Map<String, List<Itempf>> t6647Map = null;
	protected Map<String, List<Itempf>> t5515Map = null;
	protected Map<String, List<Itempf>> t5540Map = null;
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t6597Map = null;
	protected Map<String, List<Itempf>> t5519Map = null;
	protected Map<String, List<Itempf>> t5539Map = null;
	protected Map<String, List<Itempf>> t6646Map = null;
	protected Map<String, List<Chdrpf>> chdrlifMap = null;
	protected Map<String, List<Chdrpf>> chdrmjaMap = null;
	protected Map<String, List<Utrnpf>> utrnuddMap = null;
	protected Map<String, List<Hitrpf>> hitraloMap = null;
	protected Map<String, List<Covrpf>> covruddMap = null;
	protected Map<String, List<Utrspf>> utrsMap = null;
	protected Map<String, List<Hitspf>> hitsMap = null;
	protected Map<String, List<Ulnkpf>> ulnkrnlMap = null;
	protected Map<String, List<Zrstpf>> zrstnudMap = null;
	protected Map<String, List<Covrpf>> covrmjaMap = null;
	protected Map<String, List<Utrnpf>> utrnrevMap = null;
	protected Map<String, List<Payrpf>> payrMap = null;
	protected List<Covupf> covupfList = null;
	private Iterator<Covupf> iteratorList;
	//private Hitspf hitsIO;
	protected Chdrpf chdrlifIO;
	private Covrpf covruddIO;
	protected Utrnpf utrnaloIO;
	protected Ptrnpf ptrnIO;
	private Chdrpf chdrmjaIO;
	private Payrpf payrIO;
	private Uderpf uderIO;
	private Covupf covupfRec;
	protected List<Hitrpf> insertHitrpfList = null;
	protected List<Covrpf> updateCovrpfList = null;
	protected List<Covrpf> updateCovrpfTrannoList = null;
	protected List<Utrnpf> insertUtrnaloIOList = null;
	protected List<Chdrpf> updateChdrlifList = null;
	protected List<Chdrpf> updateChdrmjaList = null;
	protected List<Chdrpf> updateChdrList = null;
	protected List<Ptrnpf> insertPtrnpfList = null;
	protected List<Zrstpf> updateZrstpfList = null;
	protected List<Zrstpf> insertZrstpfList = null;
	protected List<Covrpf> updateCovrmjapfList = null;
	protected List<Covrpf> insertCovrmjapfList = null;
	protected List<Utrnpf> deleteUtrnpfList = null;
	protected List<Payrpf> updatePayrpfList = null;
	protected List<Payrpf> insertPayrpfList = null;
	protected List<Chdrpf> insertChdrlifList = null;
	protected List<Uderpf> insertUderpfList = null;
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	private int ct08Value = 0;
	private int ct09Value = 0;
	private int ct10Value = 0;
	private BigDecimal ct11Value = BigDecimal.ZERO;	
	private int ct12Value = 0;
	private BigDecimal ct13Value = BigDecimal.ZERO;	
	private int ct14Value = 0;
	protected int ct15Value = 0;
	private int ct16Value = 0;
	private int ct17Value = 0;
	private int ct18Value = 0;
	private int ct19Value = 0;
	private int ct20Value = 0;
	private int ct21Value = 0;
	private BigDecimal ct22Value = BigDecimal.ZERO;	
	private int ct23Value = 0;
	private BigDecimal ct24Value = BigDecimal.ZERO;	
	private int ct25Value = 0;
	private int ct26Value = 0;
	private int ct27Value = 0;
	private int ct28Value = 0;
	private BigDecimal ct29Value = BigDecimal.ZERO;	
	
	private String strEffDate;
	protected Map<String, List<Itempf>> t5687ListMap;
	protected Map<String, List<Itempf>> t6597ListMap;
	private String nonForfeitureMeth;
	private String premSubroutine;
	private String pStatCode;
	private String rStatCode;
	private String statuz;
	private Chdrpf chdrmjaInsert;
	private boolean nbpropo7 = false;
	
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr1420,
		exit1499,
		nextr1520,
		exit1590,
		nextr1620,
		exit1690,
		nextr1720,
		exit1790,
		nextr1808,
		exit1809,
		balanceCheck3030,
		updates3040,
		noUnitsSold3060,
		initials3120,
		buyUnits3130,
		exit3190,
		initials3220,
		exit3290,
		fixedTerm5153,
		wholeOfLife5154,
		exit5159,
		next5220,
		exit5290,
		next5252,
		exit5259,
		writeNewContractHeader6392,
		exit6392,
		exit6393,
		rewrite6394,
		writeRecord6394,
		fixedTerm7153,
		wholeOfLife7154,
		exit7090
	}

	public B5104() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		strEffDate = bsscIO.getEffectiveDate().toString();
		t5687ListMap = new HashMap<String, List<Itempf>>();
		t6597ListMap = new HashMap<String, List<Itempf>>();
		
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of COVUPF.*/
		wsaaCovuRunid.set(bprdIO.getSystemParam04());
		wsaaTranCode.set(bprdIO.getSystemParam02());
		wsaaCovuJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		
		covupfList = covupfDAO.searchCovuTempRecord(wsaaCovuFn.toString(), wsaaThreadMember.toString());
		if (covupfList == null  || covupfList.isEmpty())  {
			wsspEdterror.set(varcom.endp);
			return;
		}
		
		iteratorList = covupfList.iterator();
		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaTransactionDate.set(datcon1rec.intDate);
		wsaaDate.set(getCobolDate());
		/* Table T5679 : Statii required by this transaction.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());		
		
		storeTables1060();
		
		uderIO = new Uderpf();
		nbpropo7 = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "NBPROP07", appVars, "IT");
		
		
	}

protected void storeTables1060()
	{
		/* Table T5645 : Accounting rules.*/
		String coy = bsprIO.getCompany().toString();
		t5645Map = itemDAO.loadSmartTable("IT", coy, "T5645");
		loadT56451100();

		/* Table T5688 : Contract Processing Rules.*/
		t5688Ix = 1;
		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		loadT56881400();

		/* Table T6647 : Unit Linked Contract Detail.*/
		t6647Map = itemDAO.loadSmartTable("IT", coy, "T6647");
		t6647Ix = 1;
		loadT66471500();

		/* T5515 fund details:*/
		t5515Ix = 1;
		t5515Map = itemDAO.loadSmartTable("IT", coy, "T5515");
		loadT55151600();

		/* Table T5540 : General unit linked details.*/
		t5540Ix = 1;
		t5540Map = itemDAO.loadSmartTable("IT", coy, "T5540");
		loadT55401700();

		/* Load General Coverage/Rider Details for job effective date.*/
		/* Note the we also load the Non-fortfeiture table (T6597) at*/
		/* the same time into the same table as this is a one-to-one*/
		/* relationship between non-forteiture method and T6597 items.*/
		ix = 1;
		t5687ListMap = itemDAO.loadSmartTable("IT", coy, "T5687");
		//loadT56871800();

		t6597ListMap = itemDAO.loadSmartTable("IT", coy, "T6597");
		/*
		for (ix = 1; !(isGT(ix, wsaaT5687Size) || isEQ(wsaaT5687Crtable[ix], HIVALUE)); ix++){
			loadT6597IntoT56871900();
		}
		*/
		/*    Set up constants for UTRN's.*/
		utrnaloIO = new Utrnpf();
		utrnaloIO.setBatccoy(batcdorrec.company.toString());
		utrnaloIO.setBatcbrn(batcdorrec.branch.toString());
		utrnaloIO.setBatcactyr(batcdorrec.actyear.toInt());
		utrnaloIO.setBatcactmn(batcdorrec.actmonth.toInt());
		utrnaloIO.setBatctrcde(batcdorrec.trcde.toString());
		utrnaloIO.setBatcbatch(batcdorrec.batch.toString());
		utrnaloIO.setJobnoPrice(BigDecimal.ZERO);
		utrnaloIO.setStrpdate(0L);
		utrnaloIO.setUstmno(0);
		utrnaloIO.setInciNum(0);
		utrnaloIO.setInciPerd01(0);
		utrnaloIO.setInciPerd02(0);
		utrnaloIO.setInciprm01(BigDecimal.ZERO);
		utrnaloIO.setInciprm02(BigDecimal.ZERO);
		utrnaloIO.setSurrenderPercent(BigDecimal.ZERO);
		utrnaloIO.setNowDeferInd("N");
		utrnaloIO.setCovdbtind("D");
		utrnaloIO.setMoniesDate(bsscIO.getEffectiveDate().toLong());
		utrnaloIO.setSwitchIndicator(SPACES.toString());
		utrnaloIO.setTriggerModule(SPACES.toString());
		utrnaloIO.setTriggerKey(SPACES.toString());
		utrnaloIO.setTermid(varcom.vrcmTermid.toString());
		utrnaloIO.setUser(999999);
		utrnaloIO.setFundAmount(BigDecimal.ZERO);
		utrnaloIO.setNofUnits(BigDecimal.ZERO);
		utrnaloIO.setNofDunits(BigDecimal.ZERO);
		utrnaloIO.setPriceDateUsed(0L);
		utrnaloIO.setPriceUsed(BigDecimal.ZERO);
		utrnaloIO.setUnitBarePrice(BigDecimal.ZERO);
		utrnaloIO.setFeedbackInd(SPACES.toString());
		utrnaloIO.setSvp(BigDecimal.ONE);
		utrnaloIO.setDiscountFactor(BigDecimal.ONE);
		utrnaloIO.setFundRate(BigDecimal.ONE);
		utrnaloIO.setScheduleName(bsscIO.getScheduleName().toString());
		utrnaloIO.setScheduleNumber(bsscIO.getScheduleNumber().toInt());
		
		List<String> chdrnums = new ArrayList<>();
		if(covupfList!=null&&!covupfList.isEmpty()){
			for(Covupf c:covupfList){
				chdrnums.add(c.getChdrnum());
			}
		}
		chdrlifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnums);
		chdrmjaMap = chdrpfDAO.searchChdrmjaByChdrnum(chdrnums);
		utrnuddMap = utrnpfDAO.searchUtrnuddRecord(chdrnums);
		hitraloMap = hitrpfDAO.searchHitrRecordByChdrnum(chdrnums);
		covruddMap = covrpfDAO.searchCovruddByChdrnum(chdrnums);
		utrsMap = utrspfDAO.searchUtrs5104Record(chdrnums);
		hitsMap = hitspfDAO.searchHitsRecordByChdrnum(chdrnums);
		ulnkrnlMap = ulnkpfDAO.searchUlnkrnlRecordByChdrnum(chdrnums);
		zrstnudMap = zrstpfDAO.searchZrstnudRecordByChdrnum(chdrnums);
		covrmjaMap = covrpfDAO.searchCovrmjaByChdrnum(chdrnums);
		utrnrevMap = utrnpfDAO.searchUtrnrevRecord(chdrnums);
		payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrnums);
		
		t5519Map = itemDAO.loadSmartTable("IT", coy, "T5519");
		t5539Map = itemDAO.loadSmartTable("IT", coy, "T5539");
		t6646Map = itemDAO.loadSmartTable("IT", coy, "T6646");
	}

protected void loadT56451100()
	{
		if(t5645Map == null || t5645Map.isEmpty()){
			return;
		}
	    if (t5645Map.size() > wsaaT5645Size) {
	        syserrrec.statuz.set(h791);
	        syserrrec.params.set("t5645");
	        fatalError600();
	    }
	    if(t5645Map.containsKey(wsaaProg)){
	    	List<Itempf> items = t5645Map.get(wsaaProg);
	    	int t5645Ix = 1;
	    	for (Itempf item : items) {
	    		t5645rec.t5645Rec.set(StringUtil.rawToString(item.getGenarea()));
	    		int wsaaT5645Sub = 1;
	    		while ( !(isGT(wsaaT5645Sub, 15))) {
	    			wsaaT5645Glmap[t5645Ix].set(t5645rec.glmap[wsaaT5645Sub]);
	    			wsaaT5645Sacscode[t5645Ix].set(t5645rec.sacscode[wsaaT5645Sub]);
	    			wsaaT5645Sacstype[t5645Ix].set(t5645rec.sacstype[wsaaT5645Sub]);
	    			wsaaT5645Sub++;
	    			t5645Ix++;
	    		}
	        }
	    }
	}

protected void loadT56881400()
	{
	    if (t5688Map == null || t5688Map.size() == 0) {
	        return;
	    }
	    if (t5688Map.size() > wsaaT5688Size) {
	        syserrrec.statuz.set(h791);
	        syserrrec.params.set("t5688");
	        fatalError600();
	    }
	    for (List<Itempf> items : t5688Map.values()) {
	        for (Itempf item : items) {
	            if (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0 && isNE(item.getItemitem(), wsaaStoredCnttype)) {
	                t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
	        		wsaaT5688Cnttype[t5688Ix].set(item.getItemitem());
	        		wsaaStoredCnttype.set(item.getItemitem());
	        		wsaaT5688Comlvlacc[t5688Ix].set(t5688rec.comlvlacc);
	        		t5688Ix++;
	            }
	
	        }
	    }
	}

protected void loadT66471500()
 {
		if (t6647Map == null || t6647Map.size() == 0) {
			return;
		}
		if (t6647Map.size() > wsaaT6647Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("t6647");
			fatalError600();
		}
		for (List<Itempf> items : t6647Map.values()) {
			for (Itempf item : items) {
				if (item.getItemitem().startsWith(bprdIO.getAuthCode().toString())
						&& item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0
						&& isNE(item.getItemitem(), wsaaStoredItemitem)) {
					wsaaT6647ArrayInner.wsaaT6647Key[t6647Ix].set(item.getItemitem());
					t6647rec.t6647Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT6647ArrayInner.wsaaT6647Key[t6647Ix].set(item.getItemitem());
					wsaaStoredItemitem.set(item.getItemitem());
					wsaaT6647ArrayInner.wsaaT6647Itmfrm[t6647Ix].set(item.getItmfrm());
					wsaaT6647ArrayInner.wsaaT6647Aloind[t6647Ix].set(t6647rec.aloind);
					wsaaT6647ArrayInner.wsaaT6647Bidoffer[t6647Ix].set(t6647rec.bidoffer);
					wsaaT6647ArrayInner.wsaaT6647Dealin[t6647Ix].set(t6647rec.dealin);
					wsaaT6647ArrayInner.wsaaT6647Efdcode[t6647Ix].set(t6647rec.efdcode);
					wsaaT6647ArrayInner.wsaaT6647Enhall[t6647Ix].set(t6647rec.enhall);
					wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix].set(t6647rec.monthsDebt);
					wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix].set(t6647rec.monthsError);
					wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix].set(t6647rec.monthsLapse);
					wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix].set(t6647rec.monthsNegUnits);
					wsaaT6647ArrayInner.wsaaT6647ProcSeqNo[t6647Ix].set(t6647rec.procSeqNo);
					wsaaT6647ArrayInner.wsaaT6647UnitStatMethod[t6647Ix].set(t6647rec.unitStatMethod);
					wsaaT6647ArrayInner.wsaaT6647Swmeth[t6647Ix].set(t6647rec.swmeth);
					t6647Ix++;
				}
			}
		}
	}

protected void loadT55151600()
	{
		if (t5515Map == null || t5515Map.size() == 0) {
			return;
		}
		if (t5515Map.size() > wsaaT5515Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("t5515");
			fatalError600();
		}
		for (List<Itempf> items : t5515Map.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0
						&& isNE(item.getItemitem(), wsaaT5515Itemitem)) {
					t5515rec.t5515Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5515Key[t5515Ix].set(item.getItemitem());
					wsaaT5515Itemitem.set(item.getItemitem());
					wsaaT5515Currcode[t5515Ix].set(t5515rec.currcode);
					t5515Ix++;
				}
			}
		}
	}

protected void loadT55401700()
	{
		if (t5540Map == null || t5540Map.size() == 0) {
			return;
		}
		if (t5540Map.size() > wsaaT5540Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("t5540");
			fatalError600();
		}
		for (List<Itempf> items : t5540Map.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0
						&& isNE(item.getItemitem(), wsaaT5540StoredItem)) {
					t5540rec.t5540Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5540Key[t5540Ix].set(item.getItemitem());
					wsaaT5540StoredItem.set(item.getItemitem());
					wsaaT5540UnitCancInit[t5540Ix].set(t5540rec.unitCancInit);
					wsaaT5540WholeIuDiscFact[t5540Ix].set(t5540rec.wholeIuDiscFact);
					wsaaT5540IuDiscBasis[t5540Ix].set(t5540rec.iuDiscBasis);
					wsaaT5540IuDiscFact[t5540Ix].set(t5540rec.iuDiscFact);
					t5540Ix++;
				}
			}
		}
	}

protected boolean readT5687(String crTable){
	String keyItemitem = crTable.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5687ListMap.containsKey(keyItemitem)){	
		itempfList = t5687ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					nonForfeitureMeth = t5687rec.nonForfeitMethod.toString().trim();
					itemFound = true;
				}
			}else{
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				nonForfeitureMeth = t5687rec.nonForfeitMethod.toString().trim();
				itemFound = true;					
			}				
		}		
	}
	return itemFound;
}

protected void readT6597(String nfMethod){
	String keyItemitem = nfMethod.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	statuz = varcom.endp.toString();
	if (t6597ListMap.containsKey(keyItemitem)){	
		itempfList = t6597ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6597rec.t6597Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					statuz = varcom.oK.toString();
					premSubroutine = t6597rec.premsubr04.toString().trim();
					pStatCode = t6597rec.cpstat04.toString().trim();
					rStatCode = t6597rec.crstat04.toString().trim();					
					itemFound = true;
				}
			}else{
				t6597rec.t6597Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				statuz = varcom.oK.toString();
				premSubroutine = t6597rec.premsubr04.toString().trim();
				pStatCode = t6597rec.cpstat04.toString().trim();
				rStatCode = t6597rec.crstat04.toString().trim();					
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) return;
}


protected void readFile2000()
	{
		/*READ-FILE*/
		/*  Read the records from the temporary COVU file.*/
		if(iteratorList.hasNext()){
			covupfRec = iteratorList.next();
		}else{
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/*    Log number of COVU records read.*/
		ct01Value++;
		/*EXIT*/
	}

protected void edit2500()
	{
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(SPACES);
		/* Validate contract header status codes.  If contract status   */
		/* codes are not valid, do not process the record.              */
		if (isNE(covupfRec.getChdrnum(), wsaaALockedChdrnum)
		&& isNE(covupfRec.getChdrnum(), wsaaPrevChdrnum)) {
			wsaaChdrValid = "N";
			validateContract2700();
			if (isNE(wsaaChdrValid, "Y")) {
				return ;
			}
		}
		/*  If a UTRN record exists where the feedback indicator           */
		/*  has not been set to 'Y' ignore this record, add 1 to           */
		/*  CT27 and read the next input record.                           */
		/* IF  UTRNUDD-STATUZ          = O-K                            */
		/*     MOVE CT27               TO CONT-TOTNO                    */
		/*     MOVE 1                  TO CONT-TOTVAL                   */
		/*     PERFORM 001-CALL-CONTOT                                  */
		/*     GO TO 2590-EXIT.                                         */
		if (utrnuddMap != null && utrnuddMap.containsKey(covupfRec.getChdrnum())) {
			List<Utrnpf> utrnpfList = utrnuddMap.get(covupfRec.getChdrnum());
			for (Utrnpf u : utrnpfList) {
				if (u.getChdrcoy().equals(covupfRec.getChdrcoy()) && u.getLife().equals(covupfRec.getLife())
						&& u.getCoverage().equals(covupfRec.getCoverage()) && u.getRider().equals(covupfRec.getRider())
						&& u.getPlanSuffix() == covupfRec.getPlanSuffix()) {
					ct27Value++;
					return;
				}
			}
		}

		/* If an HITR record exists where the feedback indicator */
		/* has not been set to "Y", ignore this record, add 1 */
		/* to CT27 and read the next input record. */
		if (hitraloMap != null && hitraloMap.containsKey(covupfRec.getChdrnum())) {
			List<Hitrpf> hitrList = hitraloMap.get(covupfRec.getChdrnum());
			for (Hitrpf h : hitrList) {
				if (h.getChdrcoy().equals(covupfRec.getChdrcoy()) && h.getChdrnum().equals(covupfRec.getChdrnum())
						&& h.getLife().equals(covupfRec.getLife()) && h.getCoverage().equals(covupfRec.getCoverage())
						&& h.getRider().equals(covupfRec.getRider()) && h.getPlanSuffix() == covupfRec.getPlanSuffix()) {
					ct27Value++;
					return;
				}
			}
		}
		if (isEQ(covupfRec.getChdrnum(), wsaaALockedChdrnum)) {
			ct03Value++;
			return ;
		}
		if (isNE(covupfRec.getChdrnum(), wsaaPrevChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz, "LOCK")) {
				wsaaALockedChdrnum.set(covupfRec.getChdrnum());
				return ;
			}
			else {
				wsaaALockedChdrnum.set(SPACES);
			}
		}
		wsspEdterror.set(varcom.oK);
	}


protected void softlockPolicy2600()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(covupfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(covupfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(covupfRec.getChdrcoy());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(covupfRec.getChdrnum());
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			ct03Value++;
			ct02Value++;
		}
		else {
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
	}


protected void validateContract2700()
 {
		if (chdrlifMap != null && chdrlifMap.containsKey(covupfRec.getChdrnum())) {
			List<Chdrpf> chdrlifList = chdrlifMap.get(covupfRec.getChdrnum());
			for (Chdrpf c : chdrlifList) {
				if (covupfRec.getChdrcoy().equals(c.getChdrcoy().toString()) && c.getValidflag().equals('1')) {
					chdrlifIO = c;
					break;
				}
			}
		}
		if (chdrlifIO == null) {
			syserrrec.params.set(covupfRec.getChdrnum());
			fatalError600();
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)) {
			validateChdrStatus2720();
		}
	}


protected void validateChdrStatus2720()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], chdrlifIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				/*CHECK*/
				if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], chdrlifIO.getPstcde())) {
					wsaaSub.set(13);
					wsaaChdrValid = "Y";
				}
				/*EXIT*/
			}
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update3010();
				case balanceCheck3030:
					balanceCheck3030();
				case updates3040:
					updates3040();
				case noUnitsSold3060:
					noUnitsSold3060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		if (isNE(wsaaPrevChdrnum, covupfRec.getChdrnum())) {
			readhChdrAndTables5000();
		}
		readhCovrAndTables5100();
		/* MOVE 'N' TO WSAA-CHDR-UPDATED.                               */
		wsaaOrigCovrDebt.set(covruddIO.getCoverageDebt());
		/*    The next section reads through the UTRSs for this coverage*/
		/*    and loads the values found into a working storage table that*/
		/*    forms the centre point for all further processing. At the*/
		/*    same time the total value of init and accum units is calc'ed.*/
		loadUtrsCalcFundVals5200();
		loadHitsCalcFundVals5250();
		/*    OK.... Now we know three very important things:*/
		/*    a) The value of the debt to be settled*/
		/*    b) The total value of all positive fund holdings*/
		/*    c) The value of each fund holding*/
		/*    Firstly check whether we are selling units to settle the*/
		/*    coverage debt or whether (and unusally so) we are buying*/
		/*    units to clear the coverage debt.*/
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)<0) {
			buyUnits3100();
			goTo(GotoLabel.balanceCheck3030);
		}
		/*    We can now calculate the value of the units which must be*/
		/*    surrendered from each fund to satisfy the coverage debt.*/
		/*    We start with accumulation units and if allowed by T5540,*/
		/*    and, if the coverage debt is still not zero, finish with*/
		/*    initial units. Note that we only sell units from positive*/
		/*    fund balances as to do so from negitive fund values would*/
		/*    constitute a fund switch.*/
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)>0) {
			sellUnits3200();
			goTo(GotoLabel.balanceCheck3030);
		}
	}

protected void balanceCheck3030()
	{
		/*    Were we able to settle any debt? If not, we have no*/
		/*    balancing or updates to do so head off to the non-forfeiture*/
		/*    processing.*/
		if (covruddIO.getCoverageDebt().compareTo(wsaaOrigCovrDebt.getbigdata())==0) {
			goTo(GotoLabel.noUnitsSold3060);
		}
		/*    If the coverage debt is not ZERO then the units that could*/
		/*    be sold were not sufficient to completely settle the debt.*/
		/*    Thus we sum up the value of the what can be sold and subtract*/
		/*    it from the original coverage debt to arrive at the new debt.*/
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)!=0) {
			wsaaBalanceCheck.set(ZERO);
			for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
				wsaaBalanceCheck.add(wsaaUtrsSurrAmt[iu.toInt()]);
			}
			covruddIO.setCoverageDebt(sub(wsaaOrigCovrDebt, wsaaBalanceCheck).getbigdata());
			goTo(GotoLabel.updates3040);
		}
		/*    If the debt is zero the loop through the table and perform th*/
		/*    neccessary corrections. The emphasis here is to ensure the*/
		/*    coverage debt is zero (if not we'll be doing this again*/
		/*    next time with 0.01 widgits).*/
		wsaaBalanceCheck.set(ZERO);
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			wsaaBalanceCheck.add(wsaaUtrsSurrAmt[iu.toInt()]);
		}
		compute(wsaaBalanceCheck, 2).set(sub(wsaaBalanceCheck, wsaaOrigCovrDebt));
		/*    Loop thru each surrender amount, adding 0.01 widgits, until*/
		/*    the balancing amount is zero.*/
		iu.set(1);
		while ( !(isEQ(wsaaBalanceCheck, ZERO))) {
			if (isEQ(wsaaUtrsRrn[iu.toInt()], 0)) {
				iu.set(1);
			}
			if (isNE(wsaaUtrsSurrAmt[iu.toInt()], ZERO)) {
				if (isGT(wsaaBalanceCheck, ZERO)) {
					wsaaBalanceCheck.subtract(0.01);
					wsaaUtrsSurrAmt[iu.toInt()].subtract(0.01);
				}
				else {
					wsaaBalanceCheck.add(0.01);
					wsaaUtrsSurrAmt[iu.toInt()].add(0.01);
				}
			}
			iu.add(1);
		}
	}

protected void updates3040()
	{
		/*  Total up the value of the units possible to be*/
		/*  surrendered for the funds per coverage since the*/
		/*  WSAA-ORIG-COVR-DEBT holds the total coverage debt.*/
		wsaaTotalSurrAmt.set(0);
		/*  It is possible that the first Component(s) read would have  */
		/*  been Lapsed in which case the CHDRLIF Record would have     */
		/*  been rewritten and will now need READHing again.            */
		/*  Also, if this is the case then original increment of the    */
		/*  TRANNO will have been utilised for the Lapse, therefore     */
		/*  increment again.                                            */
		if (chdrWritten.isTrue()
		&& !chdrUpdated.isTrue()) {
			readhChdrlif5700();
			wsaaTranno.add(1);
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			wsaaTotalSurrAmt.add(wsaaUtrsSurrAmt[iu.toInt()]);
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isNE(wsaaUtrsSurrAmt[iu.toInt()], ZERO)) {
				if (isEQ(wsaaUtrsUnitType[iu.toInt()], "D")) {
					completeAndWriteHitr5350();
				}
				else {
					if (isEQ(wsaaUtrsUnitType[iu.toInt()], "A")) {
						utrnaloIO.setUnitSubAccount("ACUM");
					}
					else {
						utrnaloIO.setUnitSubAccount("INIT");
					}
					completeAndWriteUtrn5300();
				}
			}
		}
		/*    If the coverage debt has been reduced then rewrite the COVR*/
		/*    record. If this is also a new contract then update the contra*/
		/*    header and write a ptrn.*/
		/*    If a Lapse was actioned before a standard Debt clearance     */
		/*    then the CHDR will not yet have been updated and PTRN will   */
		/*    not have been written yet. If this has happened we will no   */
		/*    longer be on a CHDRNUM break therefore add in an extra       */
		/*    condition.                                           <D9607> */
		if (wsaaOrigCovrDebt.getbigdata().compareTo(covruddIO.getCoverageDebt())!=0) {
			rewriteCovr5400();
			if (isNE(wsaaPrevChdrnum, covupfRec.getChdrnum())
			|| (chdrWritten.isTrue()
			&& !chdrUpdated.isTrue())) {
				updateChdr5500();
				writePtrn5600();
			}
		}
		if (wsaaOrigCovrDebt.getbigdata().compareTo(covruddIO.getCoverageDebt())!=0) {
			/*     INITIALIZE               ZRST-PARAMS             <V65L19>*/
			/*     MOVE COVRUDD-CHDRCOY     TO ZRST-CHDRCOY         <V65L19>*/
			/*     MOVE COVRUDD-CHDRNUM     TO ZRST-CHDRNUM         <V65L19>*/
			/*     MOVE COVRUDD-LIFE        TO ZRST-LIFE            <V65L19>*/
			/*     MOVE COVRUDD-COVERAGE    TO ZRST-COVERAGE        <V65L19>*/
			/*     MOVE SPACES              TO ZRST-JLIFE           <V65L19>*/
			/*                                 ZRST-RIDER           <V65L19>*/
			/*     MOVE ZRSTREC             TO ZRST-FORMAT          <V65L19>*/
			/*     MOVE BEGNH               TO ZRST-FUNCTION        <V65L19>*/
			/*     MOVE BEGN                TO ZRST-FUNCTION        <V65L19>*/
			/*     PERFORM A100-UPDATE-ZRST UNTIL ZRST-STATUZ = ENDP<V65L19>*/
			if(zrstnudMap!=null&&zrstnudMap.containsKey(covruddIO.getChdrnum())){
				List<Zrstpf> zrstpfList = zrstnudMap.get(covruddIO.getChdrnum());
				for (Zrstpf zrstnudIO : zrstpfList) {
					if (zrstnudIO.getChdrcoy().equals(covruddIO.getChdrcoy())
							&& zrstnudIO.getChdrnum().equals(covruddIO.getChdrnum())
							&& zrstnudIO.getLife().equals(covruddIO.getLife())
							&& zrstnudIO.getCoverage().equals(covruddIO.getCoverage())) {
						zrstnudIO.setXtranno(wsaaTranno.toInt());
						zrstnudIO.setFeedbackInd("Y");
						if (updateZrstpfList == null) {
							updateZrstpfList = new ArrayList<>();
						}
						updateZrstpfList.add(zrstnudIO);
					}
				}
			}


			if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)>0) {
				b100CreateNewZrst();
			}
		}
	}

protected void noUnitsSold3060()
	{
		/*    If we have sold all the units we can and a debt still exists*/
		/*    then there are four options: debt, lapse, error and negative*/
		/*    units.*/
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)>0) {
			processUnpaidDebt6000();
		}
		/*SET-CTRL-BREAK*/
		wsaaPrevChdrnum.set(covupfRec.getChdrnum());
		/*EXIT*/
	}

protected void buyUnits3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					buyUnits3110();
				case initials3120:
					initials3120();
				case buyUnits3130:
					buyUnits3130();
				case exit3190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void buyUnits3110()
	{
		ct26Value++;
		/*    To have a negitive coverage debt is unusual although it*/
		/*    can happen. (The old version of this program, B5463, had*/
		/*    a bug which sometimes resulted in small negative debts)*/
		/*    It can also arise when units are surrendered, and due to*/
		/*    a price fluctuation, the value of the units sold no longer*/
		/*    equals the value attached to the coverage. In this case*/
		/*    the non fortfieture processing can make up this usualy small*/
		/*    shortfall by increasing the coverage debt.*/
		/*    The basic processing is to settle all negitive unit*/
		/*    holding first, thus reducing their value to zero and then*/
		/*    purchase units depending upon the latest ULNK values.*/
		
		if( (isGTE(wsaaValAcumUnitsN, ZERO) && nbpropo7 && isLT(covruddIO.getCoverageDebt(),ZERO)))
		{			
				wsaaSurrAmount.set(covruddIO.getCoverageDebt());
				covruddIO.setCoverageDebt(BigDecimal.ZERO);			
		}
		else 
			{
			if (isGTE(wsaaValAcumUnitsN, ZERO)) {
				goTo(GotoLabel.initials3120);
			}			
			if (wsaaValAcumUnitsN.getbigdata().compareTo(covruddIO.getCoverageDebt())<=0) {
				wsaaSurrAmount.set(covruddIO.getCoverageDebt());
				covruddIO.setCoverageDebt(BigDecimal.ZERO);
			}
			else {
				wsaaSurrAmount.set(wsaaValAcumUnitsN);
				covruddIO.setCoverageDebt(sub(covruddIO.getCoverageDebt(), wsaaSurrAmount).getbigdata());
			}
		}
		
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "A")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtN[iu.toInt()], wsaaValAcumUnitsN));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
				if (isNE(wsaaUtrsSurrAmt[iu.toInt()], 0)) {
					zrdecplrec.amountIn.set(wsaaUtrsSurrAmt[iu.toInt()]);
					zrdecplrec.currency.set(wsaaUtrsFundCurr[iu.toInt()]);
					callRounding8000();
					wsaaUtrsSurrAmt[iu.toInt()].set(zrdecplrec.amountOut);
				}
			}
		}
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)==0) {
			goTo(GotoLabel.exit3190);
		}
	}

protected void initials3120()
	{
		if (isGTE(wsaaValInitUnitsN, ZERO)) {
			goTo(GotoLabel.buyUnits3130);
		}
		if (wsaaValInitUnitsN.getbigdata().compareTo(covruddIO.getCoverageDebt())<=0) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(BigDecimal.ZERO);
		}
		else {
			wsaaSurrAmount.set(wsaaValInitUnitsN);
			covruddIO.setCoverageDebt(covruddIO.getCoverageDebt().subtract(wsaaSurrAmount.getbigdata()));
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "I")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtN[iu.toInt()], wsaaValInitUnitsN));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
				if (isNE(wsaaUtrsSurrAmt[iu.toInt()], 0)) {
					zrdecplrec.amountIn.set(wsaaUtrsSurrAmt[iu.toInt()]);
					zrdecplrec.currency.set(wsaaUtrsFundCurr[iu.toInt()]);
					callRounding8000();
					wsaaUtrsSurrAmt[iu.toInt()].set(zrdecplrec.amountOut);
				}
			}
		}
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)==0) {
			goTo(GotoLabel.exit3190);
		}
	}

protected void buyUnits3130()
	{
		compute(wsaaSurrAmount, 5).set(mult(covruddIO.getCoverageDebt(), -1));
		Ulnkpf ulnkrnlIO = readUlnk6120();
		wsaaAloTot = BigDecimal.ZERO;
		for (ix = 1; !(isGT(ix, 10)
		|| isEQ(ulnkrnlIO.getUalfnd(ix), SPACES)); ix++){
			completeAndWriteUtrn6130(ulnkrnlIO);
		}
	}

protected void sellUnits3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					sellUnits3210();
				case initials3220:
					initials3220();
				case exit3290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void sellUnits3210()
	{
		ct04Value++;
		if (isLTE(wsaaValAcumUnitsP, ZERO)) {
			goTo(GotoLabel.initials3220);
		}
		if (wsaaValAcumUnitsP.getbigdata().compareTo(covruddIO.getCoverageDebt())>=0) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(BigDecimal.ZERO);
		}
		else {
			wsaaSurrAmount.set(wsaaValAcumUnitsP);
			covruddIO.setCoverageDebt(covruddIO.getCoverageDebt().subtract(wsaaSurrAmount.getbigdata()));
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "A")
			|| isEQ(wsaaUtrsUnitType[iu.toInt()], "D")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtP[iu.toInt()], wsaaValAcumUnitsP));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
				if (isNE(wsaaUtrsSurrAmt[iu.toInt()], 0)) {
					zrdecplrec.amountIn.set(wsaaUtrsSurrAmt[iu.toInt()]);
					zrdecplrec.currency.set(wsaaUtrsFundCurr[iu.toInt()]);
					callRounding8000();
					wsaaUtrsSurrAmt[iu.toInt()].set(zrdecplrec.amountOut);
				}
			}
		}
		if (covruddIO.getCoverageDebt().compareTo(BigDecimal.ZERO)==0) {
			goTo(GotoLabel.exit3290);
		}
	}

protected void initials3220()
	{
		if (isLTE(wsaaValInitUnitsP, ZERO)) {
			return ;
		}
		if (isNE(wsaaT5540UnitCancInit[t5540Ix], "Y")) {
			return ;
		}
		if (wsaaValInitUnitsP.getbigdata().compareTo(covruddIO.getCoverageDebt())>=0) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(BigDecimal.ZERO);
		}
		else {
			wsaaSurrAmount.set(wsaaValInitUnitsP);
			covruddIO.setCoverageDebt(covruddIO.getCoverageDebt().subtract(wsaaSurrAmount.getbigdata()));
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "I")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtP[iu.toInt()], wsaaValInitUnitsP));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
				if (isNE(wsaaUtrsSurrAmt[iu.toInt()], 0)) {
					zrdecplrec.amountIn.set(wsaaUtrsSurrAmt[iu.toInt()]);
					zrdecplrec.currency.set(chdrlifIO.getCntcurr());
					callRounding8000();
					wsaaUtrsSurrAmt[iu.toInt()].set(zrdecplrec.amountOut);
				}
			}
		}
	}

protected void commit3500()
 {
		commitControlTotals();
		if (insertHitrpfList != null && !insertHitrpfList.isEmpty()) {
			hitrpfDAO.insertHitrpfRecord(insertHitrpfList);
			insertHitrpfList.clear();
		}
		if (updateCovrpfList != null && !updateCovrpfList.isEmpty()) {
			covrpfDAO.updateCovrDebt(updateCovrpfList);
			updateCovrpfList.clear();
		}
		if (updateCovrpfTrannoList != null && !updateCovrpfTrannoList.isEmpty()) {
			covrpfDAO.updateCovrmjaRecord(updateCovrpfTrannoList);
			updateCovrpfTrannoList.clear();
		}
		if (updateCovrmjapfList != null && !updateCovrmjapfList.isEmpty()) {
			covrpfDAO.updateCovrmjaRecord(updateCovrmjapfList);
			updateCovrmjapfList.clear();
		}
		if (insertCovrmjapfList != null && !insertCovrmjapfList.isEmpty()) {
			covrpfDAO.insertCovrpfList(insertCovrmjapfList);
			insertCovrmjapfList.clear();
		}
		if (updatePayrpfList != null && !updatePayrpfList.isEmpty()) {
			payrpfDAO.updatePayrRecord(updatePayrpfList);
			updatePayrpfList.clear();
		}		
		if (insertPayrpfList != null && !insertPayrpfList.isEmpty()) {
			payrpfDAO.insertPayrpfList(insertPayrpfList);
			insertPayrpfList.clear();
		}
		if (insertPtrnpfList != null && !insertPtrnpfList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
			insertPtrnpfList.clear();
		}
		if (insertUderpfList != null && !insertUderpfList.isEmpty()) {
			uderpfDAO.insertUderpfRecord(insertUderpfList);
			insertUderpfList.clear();
		}
		if (deleteUtrnpfList != null && !deleteUtrnpfList.isEmpty()) {
			utrnpfDAO.deleteUtrnrevRecord(deleteUtrnpfList);
			deleteUtrnpfList.clear();
		}
		if (insertZrstpfList != null && !insertZrstpfList.isEmpty()) {
			zrstpfDAO.insertZrstpfRecord(insertZrstpfList);
			insertZrstpfList.clear();
		}
		if (updateZrstpfList != null && !updateZrstpfList.isEmpty()) {
			zrstpfDAO.updateZrstXTranno(updateZrstpfList);
			updateZrstpfList.clear();
		}
		if (updateChdrlifList != null && !updateChdrlifList.isEmpty()) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(updateChdrlifList);
			updateChdrlifList.clear();
		}
		if (updateChdrmjaList != null && !updateChdrmjaList.isEmpty()) {
			chdrpfDAO.updateChdrAmt(updateChdrmjaList);
			updateChdrmjaList.clear();
		}	
		if (updateChdrList != null && !updateChdrList.isEmpty()) {
			chdrpfDAO.updateChdrByUniqueNumber(updateChdrList);
			updateChdrList.clear();
		}	
		if (insertChdrlifList != null && !insertChdrlifList.isEmpty()) {
			chdrpfDAO.insertChdrAmt(insertChdrlifList);
			insertChdrlifList.clear();
		}
		if (insertUtrnaloIOList != null && !insertUtrnaloIOList.isEmpty()) {
			utrnpfDAO.insertUtrnRecoed(insertUtrnaloIOList);
			insertUtrnaloIOList.clear();
		}
	}
protected void commitControlTotals(){
		contotrec.totno.set(controlTotalsInner.ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct01Value = 0;
		contotrec.totno.set(controlTotalsInner.ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct02Value = 0;
		contotrec.totno.set(controlTotalsInner.ct03);
		contotrec.totval.set(ct03Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct03Value = 0;
		contotrec.totno.set(controlTotalsInner.ct04);
		contotrec.totval.set(ct04Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct04Value = 0;
		contotrec.totno.set(controlTotalsInner.ct05);
		contotrec.totval.set(ct05Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct05Value = 0;
		contotrec.totno.set(controlTotalsInner.ct06);
		contotrec.totval.set(ct06Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct06Value = 0;
		contotrec.totno.set(controlTotalsInner.ct07);
		contotrec.totval.set(ct07Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct07Value = 0;
		contotrec.totno.set(controlTotalsInner.ct08);
		contotrec.totval.set(ct08Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct08Value = 0;
		contotrec.totno.set(controlTotalsInner.ct09);
		contotrec.totval.set(ct09Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct09Value = 0;
		contotrec.totno.set(controlTotalsInner.ct10);
		contotrec.totval.set(ct10Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct10Value = 0;
		contotrec.totno.set(controlTotalsInner.ct11);
		contotrec.totval.set(ct11Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct11Value = BigDecimal.ZERO;
		contotrec.totno.set(controlTotalsInner.ct12);
		contotrec.totval.set(ct12Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct12Value = 0;
		contotrec.totno.set(controlTotalsInner.ct13);
		contotrec.totval.set(ct13Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct13Value = BigDecimal.ZERO;
		contotrec.totno.set(controlTotalsInner.ct14);
		contotrec.totval.set(ct14Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct14Value = 0;
		contotrec.totno.set(controlTotalsInner.ct15);
		contotrec.totval.set(ct15Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct15Value = 0;
		contotrec.totno.set(controlTotalsInner.ct16);
		contotrec.totval.set(ct16Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct16Value = 0;
		contotrec.totno.set(controlTotalsInner.ct17);
		contotrec.totval.set(ct17Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct17Value = 0;
		contotrec.totno.set(controlTotalsInner.ct18);
		contotrec.totval.set(ct18Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct18Value = 0;
		contotrec.totno.set(controlTotalsInner.ct19);
		contotrec.totval.set(ct19Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct19Value = 0;
		contotrec.totno.set(controlTotalsInner.ct20);
		contotrec.totval.set(ct20Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct20Value = 0;
		contotrec.totno.set(controlTotalsInner.ct21);
		contotrec.totval.set(ct21Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct21Value = 0;
		contotrec.totno.set(controlTotalsInner.ct22);
		contotrec.totval.set(ct22Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct22Value = BigDecimal.ZERO;
		contotrec.totno.set(controlTotalsInner.ct23);
		contotrec.totval.set(ct23Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct23Value = 0;
		contotrec.totno.set(controlTotalsInner.ct24);
		contotrec.totval.set(ct24Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct24Value = BigDecimal.ZERO;
		contotrec.totno.set(controlTotalsInner.ct25);
		contotrec.totval.set(ct25Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct25Value = 0;
		contotrec.totno.set(controlTotalsInner.ct26);
		contotrec.totval.set(ct26Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct26Value = 0;
		contotrec.totno.set(controlTotalsInner.ct27);
		contotrec.totval.set(ct27Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct27Value = 0;
		contotrec.totno.set(controlTotalsInner.ct28);
		contotrec.totval.set(ct28Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct28Value = 0;
		contotrec.totno.set(controlTotalsInner.ct29);
		contotrec.totval.set(ct29Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct29Value = BigDecimal.ZERO;

}
protected void rollback3600()
	{
		/*ROLL*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		wsaaQcmdexc.set("DLTOVR FILE(COVUPF)");
	    com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
	    lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void readhChdrAndTables5000()
	{
		chdrlifIO = null;
		if (chdrlifMap != null && chdrlifMap.containsKey(covupfRec.getChdrnum())) {
			List<Chdrpf> chdrlifList = chdrlifMap.get(covupfRec.getChdrnum());
			for (Chdrpf c : chdrlifList) {
				if (covupfRec.getChdrcoy().equals(c.getChdrcoy().toString())) {
					chdrlifIO = c;
					break;
				}
			}
		}
		if(chdrlifIO == null){
			syserrrec.params.set(covupfRec.getChdrnum());
			fatalError600();
		}
		compute(wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));
		wsaaPtrn.set("N");
		wsaaChdrLapsed.set("N");
		wsaaChdrWritten.set("N");
		wsaaChdrUpdated.set("N");
		/*    Get the pre-loaded T6647 entry for this contract (remembering*/
		/*    that we have only loaded B633 entries) so we may establish*/
		/*    whether we are using Bid or Offer pricing.*/
		boolean foundFlag = false;
		itdmIO.getItemitem().setSub1String(1, 4, bprdIO.getAuthCode());
		itdmIO.getItemitem().setSub1String(5, 4, chdrlifIO.getCnttype());
		for(t6647Ix = 1;t6647Ix<wsaaT6647ArrayInner.wsaaT6647Key.length;t6647Ix++){
			if(wsaaT6647ArrayInner.wsaaT6647Key[t6647Ix].toString().equals(itdmIO.getItemitem().trim())){
				foundFlag = true;
				break;
			}
		}
		if(!foundFlag){
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(tablesInner.t6647);
			itdmIO.setFunction(varcom.readr);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}

		
		
		foundFlag = false;
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		for(t5688Ix = 1;t5688Ix<wsaaT5688Cnttype.length;t5688Ix++){
			if(wsaaT5688Cnttype[t5688Ix].toString().equals(itdmIO.getItemitem().trim())){
				foundFlag = true;
				break;
			}
		}
		
		if(!foundFlag){
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(tablesInner.t5688);
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*    MOVE WSAA-T5688-COMLVLACC(IX) TO WSAA-COMPON-LEVEL-ACCOUNTED.*/
		wsaaComponLevelAccounted.set(wsaaT5688Comlvlacc[t5688Ix]);
		ct05Value++;
	}


protected void readhCovrAndTables5100()
	{
		if (covruddMap != null && covruddMap.containsKey(covupfRec.getChdrnum())) {
			List<Covrpf> covrpfList = covruddMap.get(covupfRec.getChdrnum());
			for (Covrpf c : covrpfList) {
				if (c.getChdrcoy().equals(covupfRec.getChdrcoy()) && c.getChdrnum().equals(covupfRec.getChdrnum())
						&& c.getLife().equals(covupfRec.getLife()) && c.getCoverage().equals(covupfRec.getCoverage())
						&& c.getRider().equals(covupfRec.getRider()) && c.getPlanSuffix() == covupfRec.getPlanSuffix()) {
					covruddIO = c;
					break;
				}
			}
		}
		if(covruddIO == null){
			syserrrec.params.set(covupfRec.getChdrnum());
			fatalError600();
		}
		
		itdmIO.setItemitem(covruddIO.getCrtable());
		
		boolean foundFlag = false;
		for(t5540Ix = 1;t5540Ix<wsaaT5540Key.length;t5540Ix++){
			if(wsaaT5540Key[t5540Ix].toString().equals(itdmIO.getItemitem().trim())){
				foundFlag = true;
				break;
			}
		}
		
		if(!foundFlag){
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(tablesInner.t5540);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covruddIO.getCrrcd());
		datcon3rec.intDate2.set(covruddIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		/*  Store the integer of the factor in a field to be used*/
		/*  as the index to T5539.  If the DTC3-FREQ-FACTOR contain*/
		/*  a decimal value, correct the index up to the next whole*/
		/*  number.*/
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		if (isNE(wsaaRiskCessTerm, 0)) {
			findIuDiscountFactor5150();
		}
		ct06Value++;
	}

protected void findIuDiscountFactor5150()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start5151();
				case fixedTerm5153:
					fixedTerm5153();
				case wholeOfLife5154:
					wholeOfLife5154();
				case exit5159:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5151()
	{
		wsaaInitUnitDiscFact.set(1);
		/*  Table T5540: When Discount-Basis is set, Discount-Factor       */
		/*               should also be set, otherwise the table has       */
		/*               been set incorrectly.                             */
		/*  IF T5540-IU-DISC-BASIS NOT = SPACE                           */
		if (isNE(wsaaT5540IuDiscBasis[t5540Ix], SPACES)) {
			readT55195155();
		}
		if (isNE(wsaaT5540WholeIuDiscFact[t5540Ix], SPACES)) {
			goTo(GotoLabel.wholeOfLife5154);
		}
		/* There may not be any initial unit methods in T5540. Check if    */
		/* both spaces, skip this section                                  */
		/* IF T5540-IU-DISC-BASIS     = SPACE                   <LA3415>*/
		if (isEQ(wsaaT5540IuDiscBasis[t5540Ix], SPACES)) {
			goTo(GotoLabel.exit5159);
		}
		/* IF  T5519-FIXDTRM NOT   = 0                                  */
		/* AND WSAA-RISK-CESS-TERM > T5519-FIXDTRM                      */
		/*     GO TO 5153-FIXED-TERM.                                   */
		if (isNE(wsaaT5540IuDiscBasis[t5540Ix], SPACES)) {
			if (isNE(t5519rec.fixdtrm, ZERO)
			&& isGT(wsaaRiskCessTerm, t5519rec.fixdtrm)) {
				goTo(GotoLabel.fixedTerm5153);
			}
			readT55395155();
			if (isLT(wsaaRiskCessTerm, 100)) {
				wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
			}
			else {
				compute(wsaaIndex, 0).set(sub(wsaaRiskCessTerm, 99));
				wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaIndex.toInt()]);
			}
			utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000).getbigdata());
		}
		goTo(GotoLabel.exit5159);
	}

protected void fixedTerm5153()
	{
		readT55395155();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaIndex, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaIndex.toInt()]);
		}
		utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000).getbigdata());
		goTo(GotoLabel.exit5159);
	}

protected void wholeOfLife5154()
	{
		/* Use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (covruddIO.getAnbAtCcd() == 0) {
			return ;
		}
		if (isNE(wsaaT5540WholeIuDiscFact[t5540Ix], SPACES)) {
			readT66465155();
			if (covruddIO.getAnbAtCcd()<100) {
				wsaaInitUnitDiscFact.set(t6646rec.dfact[covruddIO.getAnbAtCcd()]);
			}
			else {
				compute(wsaaIndex, 0).set(sub(covruddIO.getAnbAtCcd(), 99));
				wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaIndex.toInt()]);
			}
			utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000).getbigdata());
		}
	}

protected void readT55395155()
	{
		/*  IF T5540-IU-DISC-FACT       = SPACES*/
		if (isEQ(wsaaT5540IuDiscFact[t5540Ix], SPACES)) {
			return;
		}
		if (t5539Map != null && t5539Map.containsKey(wsaaT5540IuDiscFact[t5540Ix])) {
			List<Itempf> itempfList = t5539Map.get(wsaaT5540IuDiscFact[t5540Ix]);
			for (Itempf item : itempfList) {
				if (item.getItemcoy().equals(covupfRec.getChdrcoy())) {
					t5539rec.t5539Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
			}
		}
		syserrrec.params.set(wsaaT5540IuDiscFact[t5540Ix]);
		fatalError600();
	}


protected void readT55195155()
	{
		if (t5519Map != null && t5519Map.containsKey(wsaaT5540IuDiscBasis[t5540Ix])) {
			List<Itempf> itempfList = t5519Map.get(wsaaT5540IuDiscBasis[t5540Ix]);
			for (Itempf i : itempfList) {
				if (i.getItemcoy().equals(covupfRec.getChdrcoy())
						&& i.getItmfrm().compareTo(wsaaTransactionDate.getbigdata()) <= 0) {
					t5519rec.t5519Rec.set(StringUtil.rawToString(i.getGenarea()));
					return;
				}
			}
		}
		syserrrec.params.set(wsaaT5540IuDiscBasis[t5540Ix]);
		syserrrec.statuz.set(g027);
		fatalError600();
		/* IF (ITDM-ITEMCOY NOT = CHDRCOY OF COVUPF-REC) OR */
		/* (ITDM-ITEMTABL NOT = T5519) OR */
		/* (ITDM-ITEMITEM NOT = T5540-IU-DISC-BASIS) */
		/* MOVE T5540-IU-DISC-BASIS TO ITDM-ITEMITEM */
		/* MOVE ITDM-PARAMS TO SYSR-PARAMS */
		/* MOVE G027 TO SYSR-STATUZ */
		/* PERFORM 600-FATAL-ERROR. */
	}


protected void readT66465155()
 {
		if (t6646Map != null && t6646Map.containsKey(wsaaT5540WholeIuDiscFact[t5540Ix])) {
			List<Itempf> itempfList = t6646Map.get(wsaaT5540WholeIuDiscFact[t5540Ix]);
			for (Itempf item : itempfList) {
				if (item.getItemcoy().equals(covupfRec.getChdrcoy())) {
					t6646rec.t6646Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
			}
		}
		syserrrec.params.set(wsaaT5540WholeIuDiscFact[t5540Ix]);
		fatalError600();
	}


protected void loadUtrsCalcFundVals5200()
 {
		/* Load all the UTRSs into our working storage table of UTRSs fo */
		/* this coverage so we do not have to keep re-reading the UTRS. */
		wsaaValInitUnitsP.set(ZERO);
		wsaaValAcumUnitsP.set(ZERO);
		wsaaValInitUnitsN.set(ZERO);
		wsaaValAcumUnitsN.set(ZERO);
		iu.set(1);
		if (utrsMap.containsKey(covupfRec.getChdrnum())) {
			List<Utrspf> utList = utrsMap.get(covupfRec.getChdrnum());
			for (Utrspf utrsIO : utList) {
				if (utrsIO.getChdrcoy().equals(covupfRec.getChdrcoy()) && utrsIO.getChdrnum().equals(covupfRec.getChdrnum())
						&& utrsIO.getLife().equals(covupfRec.getLife()) && utrsIO.getCoverage().equals(covupfRec.getCoverage())
						&& utrsIO.getRider().equals(covupfRec.getRider())
						&& utrsIO.getPlanSuffix() == covupfRec.getPlanSuffix()) {
					ct07Value++;
					utrsToProcess5230(utrsIO);
				}
			}
		}
		wsaaUtrsRrn[iu.toInt()].set(ZERO);
	}

protected void utrsToProcess5230(Utrspf utrsIO)
	{
		ufpricerec.function.set("DEALP");
		ufpricerec.company.set(bsprIO.getCompany());
		ufpricerec.unitVirtualFund.set(utrsIO.getUnitVirtualFund());
		ufpricerec.unitType.set(utrsIO.getUnitType());
		ufpricerec.effdate.set(bsscIO.getEffectiveDate());
		ufpricerec.nowDeferInd.set("N");
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
		&& isNE(ufpricerec.statuz, varcom.endp)) {
			syserrrec.params.set(ufpricerec.ufpriceRec);
			syserrrec.statuz.set(ufpricerec.statuz);
			fatalError600();
		}
		if (isEQ(ufpricerec.statuz, varcom.endp)) {
			ct08Value++;
			return;
		}
		/*    Using either the Bid or Offer price calculate the value of*/
		/*    units of this fund type attached to the coverage.*/
		if (isEQ(wsaaT6647ArrayInner.wsaaT6647Bidoffer[t6647Ix], "B")) {
			compute(wsaaFundValue, 6).setRounded(mult(ufpricerec.bidPrice, utrsIO.getCurrentUnitBal()));
		}
		else {
			compute(wsaaFundValue, 6).setRounded(mult(ufpricerec.offerPrice, utrsIO.getCurrentUnitBal()));
		}
		/*    The price will be in the currency of the fund. But before we*/
		/*    know the value to the coverage we may need to convert it into*/
		/*    the currency of the contract. Note that XCVRT is used only*/
		/*    to get the rate as allowing XCVRT to calculate would result*/
		/*    in a loss of precision.*/
		boolean foundFlag = false;
		for(t5515Ix = 1;t5515Ix<wsaaT5515Fund.length;t5515Ix++){
			if(wsaaT5515Fund[t5515Ix].toString().equals(utrsIO.getUnitVirtualFund().trim())){
				foundFlag = true;
				break;
			}
		}
		
		if(!foundFlag){
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(tablesInner.t5515);
			itdmIO.setItemitem(utrsIO.getUnitVirtualFund());
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		
		wsaaUtrsFundCurr[iu.toInt()].set(wsaaT5515Currcode[t5515Ix]);
		if (isEQ(chdrlifIO.getCntcurr(), wsaaT5515Currcode[t5515Ix])) {
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.rateUsed.set(1);
		}
		else {
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.currIn.set(wsaaT5515Currcode[t5515Ix]);
			/*     MOVE UFPR-PRICE-DATE     TO CLNK-CASHDATE                */
			conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
			conlinkrec.currOut.set(chdrlifIO.getCntcurr());
			conlinkrec.function.set("CVRT");
			x1000CallXcvrt();
		}
		if(nbpropo7 && isGT(wsaaFundValue, ZERO) && isLT(covruddIO.getCoverageDebt(),ZERO))
		{
			compute(wsaaUtrsCovFundAmtN[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtP[iu.toInt()].set(ZERO);
		}
		else if (isGT(wsaaFundValue, ZERO)) {
			compute(wsaaUtrsCovFundAmtP[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtN[iu.toInt()].set(ZERO);
		}
		else {
			compute(wsaaUtrsCovFundAmtN[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtP[iu.toInt()].set(ZERO);
		}
		if (isEQ(utrsIO.getUnitType(), "A")) {
			wsaaValAcumUnitsP.add(wsaaUtrsCovFundAmtP[iu.toInt()]);
			wsaaValAcumUnitsN.add(wsaaUtrsCovFundAmtN[iu.toInt()]);
		}
		else {
			wsaaValInitUnitsP.add(wsaaUtrsCovFundAmtP[iu.toInt()]);
			wsaaValInitUnitsN.add(wsaaUtrsCovFundAmtN[iu.toInt()]);
		}
		wsaaUtrsRrn[iu.toInt()].set(utrsIO.getUniqueNumber());
		wsaaUtrsUnitVirtualFund[iu.toInt()].set(utrsIO.getUnitVirtualFund());
		wsaaUtrsUnitType[iu.toInt()].set(utrsIO.getUnitType());
		wsaaUtrsCurrentUnitBal[iu.toInt()].set(utrsIO.getCurrentUnitBal());
		wsaaUtrsCurrentDunitBal[iu.toInt()].set(utrsIO.getCurrentDunitBal());
		wsaaUtrsSurrAmt[iu.toInt()].set(ZERO);
		wsaaUtrsFundPool[iu.toInt()].set(utrsIO.getFundPool());
		iu.add(1);
		ct09Value++;
	}

protected void loadHitsCalcFundVals5250()
 {
		/* Load all the HITSs into our working storage table for this */
		/* this coverage so we do not have to keep re-reading the HITS. */
		if (hitsMap != null && hitsMap.containsKey(covupfRec.getChdrnum())) {
			List<Hitspf> hitspfList = hitsMap.get(covupfRec.getChdrnum());
			for (Hitspf hitsIO : hitspfList) {
				if (hitsIO.getChdrcoy().equals(covupfRec.getChdrcoy()) && hitsIO.getChdrnum().equals(covupfRec.getChdrnum())
						&& hitsIO.getLife().equals(covupfRec.getLife()) && hitsIO.getCoverage().equals(covupfRec.getCoverage())
						&& hitsIO.getRider().equals(covupfRec.getRider())
						&& hitsIO.getPlanSuffix() == covupfRec.getPlanSuffix()) {
					ct27Value++;
					hitsToProcess5253(hitsIO);

				}
			}
		}
		wsaaUtrsRrn[iu.toInt()].set(ZERO);
	}


protected void hitsToProcess5253(Hitspf hitsIO)
	{
		wsaaFundValue.set(hitsIO.getZcurprmbal());
		
		boolean foundFlag = false;
		for(t5515Ix = 1;t5515Ix<wsaaT5515Fund.length;t5515Ix++){
			if(wsaaT5515Fund[t5515Ix].toString().equals(hitsIO.getZintbfnd().trim())){
				foundFlag = true;
				break;
			}
		}
		
		if(!foundFlag){
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(tablesInner.t5515);
			itdmIO.setItemitem(hitsIO.getZintbfnd());
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		
		wsaaUtrsFundCurr[iu.toInt()].set(wsaaT5515Currcode[t5515Ix]);
		if (isEQ(chdrlifIO.getCntcurr(), wsaaT5515Currcode[t5515Ix])) {
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.rateUsed.set(1);
		}
		else {
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.currIn.set(wsaaT5515Currcode[t5515Ix]);
			conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
			conlinkrec.currOut.set(chdrlifIO.getCntcurr());
			conlinkrec.function.set("CVRT");
			x1000CallXcvrt();
		}
		compute(wsaaUtrsCovFundAmtP[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
		wsaaUtrsCovFundAmtN[iu.toInt()].set(ZERO);
		wsaaValAcumUnitsP.add(wsaaUtrsCovFundAmtP[iu.toInt()]);
		wsaaUtrsRrn[iu.toInt()].set(hitsIO.getUniqueNumber());
		wsaaUtrsUnitVirtualFund[iu.toInt()].set(hitsIO.getZintbfnd());
		wsaaUtrsUnitType[iu.toInt()].set("D");
		wsaaUtrsCurrentUnitBal[iu.toInt()].set(ZERO);
		wsaaUtrsCurrentDunitBal[iu.toInt()].set(ZERO);
		wsaaUtrsSurrAmt[iu.toInt()].set(ZERO);
		iu.add(1);
	}

protected void completeAndWriteUtrn5300()
	{
		/*    We dont have a great deal to do here as Unit Deal (B5102)*/
		/*    can manage to complete most of the UTRN details thus we*/
		/*    do only what is necessary.*/
		utrnaloIO.setUnitVirtualFund(wsaaUtrsUnitVirtualFund[iu.toInt()].toString());
		utrnaloIO.setFundCurrency(wsaaUtrsFundCurr[iu.toInt()].toString());
		utrnaloIO.setUnitType(wsaaUtrsUnitType[iu.toInt()].toString());
		/*  We make sure Discount Factor is 1 when Unit type is Acum.      */
		/*  Otherwise we find the correct one for Init.                    */
		if (isEQ(utrnaloIO.getUnitType(), "A")) {
			utrnaloIO.setDiscountFactor(BigDecimal.ONE);
		}
		else {
			getDfactForInit7000();
		}
		utrnaloIO.setContractAmount(sub(ZERO, wsaaUtrsSurrAmt[iu.toInt()]).getbigdata());
		/*  If the debt exceeds the value of the units on the*/
		/*  coverage then the real and deemed unit total held*/
		/*  for that coverage (on UTRS) can be written to the*/
		/*  UTRN.  Otherwise force the unit processing program*/
		/*  to re-calculate the units.*/
		if (isGT(wsaaOrigCovrDebt, wsaaTotalSurrAmt)) {
			utrnaloIO.setNofUnits(sub(0, wsaaUtrsCurrentUnitBal[iu.toInt()]).getbigdata());
			if (utrnaloIO.getContractAmount().compareTo(wsaaUtrsCurrentUnitBal[iu.toInt()].getbigdata())<=0) {
				utrnaloIO.setNofDunits(sub(0, wsaaUtrsCurrentDunitBal[iu.toInt()]).getbigdata());
			}
			if (isEQ(utrnaloIO.getUnitType(), "A")) {
				utrnaloIO.setNofDunits(utrnaloIO.getNofUnits());
			}
		}
		else {
			utrnaloIO.setNofUnits(BigDecimal.ZERO);
			utrnaloIO.setNofDunits(BigDecimal.ZERO);
		}
		utrnaloIO.setTranno(wsaaTranno.toInt());
		utrnaloIO.setCntcurr(chdrlifIO.getCntcurr());
		utrnaloIO.setCnttyp(chdrlifIO.getCnttype());
		utrnaloIO.setChdrcoy(covruddIO.getChdrcoy());
		utrnaloIO.setChdrnum(covruddIO.getChdrnum());
		utrnaloIO.setLife(covruddIO.getLife());
		utrnaloIO.setCoverage(covruddIO.getCoverage());
		utrnaloIO.setRider(covruddIO.getRider());
		utrnaloIO.setPlanSuffix(covruddIO.getPlanSuffix());
		utrnaloIO.setCrComDate(new Long(covruddIO.getCrrcd()));
		utrnaloIO.setCrtable(covruddIO.getCrtable());
		utrnaloIO.setProcSeqNo(wsaaT6647ArrayInner.wsaaT6647ProcSeqNo[t6647Ix].toInt());
		/*  MOVE WSAA-TRANSACTION-DATE TO UTRNALO-TRANSACTION-DATE.      */
		utrnaloIO.setTransactionDate(wsaaDate.toInt());
		utrnaloIO.setTransactionTime( varcom.vrcmTime.toInt());
		if (componLevelAccounted.isTrue()) {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[2].toString());
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[2].toString());
			utrnaloIO.setGenlcde(wsaaT5645Glmap[2].toString());
		}
		else {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[1].toString());
			utrnaloIO.setGenlcde(wsaaT5645Glmap[1].toString());
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[1].toString());
		}
		
		if(insertUtrnaloIOList == null){
			insertUtrnaloIOList = new ArrayList<>();
		}
		utrnaloIO.setFundPool(wsaaUtrsFundPool[iu.toInt()].toString());
		insertUtrnaloIOList.add(utrnaloIO);
		utrnaloIO = new Utrnpf(utrnaloIO);
		ct10Value++;
		ct11Value = ct11Value.add(utrnaloIO.getContractAmount());
	}


protected void completeAndWriteHitr5350()
	{
		/*    We dont have a great deal to do here as Unit Deal (B5102)    */
		/*    can manage to complete most of the HITR details thus we      */
		/*    do only what is necessary.                                   */
		Hitrpf hitraloIO = new Hitrpf();
		hitraloIO.setBatccoy(batcdorrec.company.toString());
		hitraloIO.setBatcbrn(batcdorrec.branch.toString());
		hitraloIO.setBatcactyr(batcdorrec.actyear.toInt());
		hitraloIO.setBatcactmn(batcdorrec.actmonth.toInt());
		hitraloIO.setBatctrcde(batcdorrec.trcde.toString());
		hitraloIO.setBatcbatch(batcdorrec.batch.toString());
		hitraloIO.setZintrate(BigDecimal.ZERO);
		hitraloIO.setSurrenderPercent(BigDecimal.ZERO);
		hitraloIO.setUstmno(0);
		hitraloIO.setInciNum(0);
		hitraloIO.setInciPerd01(0);
		hitraloIO.setInciPerd02(0);
		hitraloIO.setInciprm01(BigDecimal.ZERO);
		hitraloIO.setInciprm02(BigDecimal.ZERO);
		hitraloIO.setZinteffdt(varcom.vrcmMaxDate.toInt());
		hitraloIO.setZlstintdte(varcom.vrcmMaxDate.toInt());
		hitraloIO.setCovdbtind("D");
		hitraloIO.setEffdate(bsscIO.getEffectiveDate().toInt());
		hitraloIO.setSwitchIndicator(SPACES.toString());
		hitraloIO.setTriggerModule(SPACES.toString());
		hitraloIO.setTriggerKey(SPACES.toString());
		hitraloIO.setFundAmount(BigDecimal.ZERO);
		hitraloIO.setFeedbackInd(SPACES.toString());
		hitraloIO.setSvp(BigDecimal.ONE);
		hitraloIO.setFundRate(BigDecimal.ONE);
		hitraloIO.setZintbfnd(wsaaUtrsUnitVirtualFund[iu.toInt()].toString());
		hitraloIO.setFundCurrency(wsaaUtrsFundCurr[iu.toInt()].toString());
		hitraloIO.setZrectyp("P");
		hitraloIO.setContractAmount(sub(ZERO, wsaaUtrsSurrAmt[iu.toInt()]).getbigdata());
		hitraloIO.setTranno(wsaaTranno.toInt());
		hitraloIO.setCntcurr(chdrlifIO.getCntcurr());
		hitraloIO.setCnttyp(chdrlifIO.getCnttype());
		hitraloIO.setChdrcoy(covruddIO.getChdrcoy());
		hitraloIO.setChdrnum(covruddIO.getChdrnum());
		hitraloIO.setLife(covruddIO.getLife());
		hitraloIO.setCoverage(covruddIO.getCoverage());
		hitraloIO.setRider(covruddIO.getRider());
		hitraloIO.setPlanSuffix(covruddIO.getPlanSuffix());
		hitraloIO.setCrtable(covruddIO.getCrtable());
		hitraloIO.setProcSeqNo(wsaaT6647ArrayInner.wsaaT6647ProcSeqNo[t6647Ix].toInt());
		if (componLevelAccounted.isTrue()) {
			hitraloIO.setSacscode(wsaaT5645Sacscode[2].toString());
			hitraloIO.setSacstyp(wsaaT5645Sacstype[2].toString());
			hitraloIO.setGenlcde(wsaaT5645Glmap[2].toString());
		}
		else {
			hitraloIO.setSacscode(wsaaT5645Sacscode[1].toString());
			hitraloIO.setGenlcde(wsaaT5645Glmap[1].toString());
			hitraloIO.setSacstyp(wsaaT5645Sacstype[1].toString());
		}
		if(insertHitrpfList == null){
			insertHitrpfList = new ArrayList<>();
		}
		insertHitrpfList.add(hitraloIO);
		ct28Value++;
		ct29Value = ct29Value.add(hitraloIO.getContractAmount());
	}


protected void rewriteCovr5400()
	{
		if(updateCovrpfList == null){
			updateCovrpfList = new ArrayList<Covrpf>();
		}
		updateCovrpfList.add(covruddIO);
		ct12Value++;
		ct13Value = ct13Value.add(sub(wsaaOrigCovrDebt, covruddIO.getCoverageDebt()).getbigdata());
	}


protected void updateChdr5500()
 {
		/* UPDATE */
		chdrlifIO.setTranno(wsaaTranno.toInt());
		if (updateChdrlifList == null) {
			updateChdrlifList = new ArrayList<>();
		}
		updateChdrlifList.add(chdrlifIO);
		ct14Value++;
		wsaaChdrUpdated.set("Y");
		/* EXIT */
	}

protected void writePtrn5600()
	{
		resetPtrn();
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaTranno.toInt());
		ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		/*  MOVE WSAA-TRANSACTION-DATE TO PTRN-TRANSACTION-DATE.         */
		ptrnIO.setTrdt(wsaaDate.toInt());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		
		if(insertPtrnpfList == null){
			insertPtrnpfList = new ArrayList<>();
		}
		insertPtrnpfList.add(ptrnIO);
		ct15Value++;
	}


protected void readhChdrlif5700()
	{
		if (chdrlifMap != null && chdrlifMap.containsKey(covupfRec.getChdrnum())) {
			List<Chdrpf> chdrlifList = chdrlifMap.get(covupfRec.getChdrnum());
			for (Chdrpf c : chdrlifList) {
				if (covupfRec.getChdrcoy().equals(c.getChdrcoy().toString())) {
					chdrlifIO = c;
					return;
				}
			}
		}
		syserrrec.params.set(covupfRec.getChdrnum());
		fatalError600();
	}

protected void processUnpaidDebt6000()
	{
		/* IF  COVRUDD-CRRCD >= BSSC-EFFECTIVE-DATE                     */
		if (covruddIO.getCrrcd() > bsscIO.getEffectiveDate().toInt()) {
			/*      MOVE CT16               TO CONT-TOTNO                    */
			/*      MOVE 1                  TO CONT-TOTVAL                   */
			/*      PERFORM 001-CALL-CONTOT                                  */
			return ;
		}

		if (!readT5687(covruddIO.getCrtable())) fatalError600();
		readT6597(nonForfeitureMeth);
			
		
		/* Already Lapsed*/

		//if (isEQ(wsaaT6597Crstat04[t5687Ix], covruddIO.getStatcode())) {
		if (isEQ(rStatCode, covruddIO.getStatcode())) {
			/*      MOVE CT16               TO CONT-TOTNO                    */
			ct28Value++;
			return ;
		}

		uderIO.setLapind(SPACES.toString());
		/*    Call the subroutine DATCON3 to establish how many months*/
		/*    have elapsed since coverage commence date.*/
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(covruddIO.getCrrcd());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		/*    Find the method with which to deal with the unpaid debt.*/
		/*    Read through the four options in the cancellation order set*/
		/*    out in T6647.  Ignoring  any  items  which are spaces or*/
		/*    less than the frequency factor returned from DATCON3 find*/
		/*    the one which is the closest to that number. Depending on*/
		/*    the option found carry out the following processing.*/
		/* MOVE 999                    TO WSAA-WITHIN-RANGE.            */
		wsaaWithinRange.set(1000);
		wsaaInsuffUnitsMeth.set(SPACES);
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix], datcon3rec.freqFactor)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix], ZERO)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsNegUnits[t6647Ix]);
			wsaaInsuffUnitsMeth.set("N");
		}
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix], datcon3rec.freqFactor)
		&& isLT(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix], wsaaWithinRange)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix], ZERO)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsDebt[t6647Ix]);
			wsaaInsuffUnitsMeth.set("D");
		}
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix], datcon3rec.freqFactor)
		&& isLT(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix], wsaaWithinRange)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix], ZERO)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsLapse[t6647Ix]);
			wsaaInsuffUnitsMeth.set("L");
		}
		if (isGTE(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix], datcon3rec.freqFactor)
		&& isLT(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix], wsaaWithinRange)
		&& isNE(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix], ZERO)) {
			wsaaWithinRange.set(wsaaT6647ArrayInner.wsaaT6647MonthsError[t6647Ix]);
			wsaaInsuffUnitsMeth.set("E");
		}
		if (isEQ(wsaaInsuffUnitsMeth, " ")){
			ct16Value++;
			return ;
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "N")){
			ct17Value++;
			useNegUnitsToPayDebt6100();
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "D")){
			ct18Value++;
			debtOption6200();
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "L")){
			ct19Value++;
			uderIO.setLapind("L");
			lapseOption6300();
			errorOption6400();
		}
		else if (isEQ(wsaaInsuffUnitsMeth, "E")){
			ct20Value++;
			errorOption6400();
		}

	}


protected void useNegUnitsToPayDebt6100()
	{
		/*    This form of debt settlement simply creates UTRNs which*/
		/*    are negative and therefore allows a negative fund holding.*/
		/*    Simply read "fund split" details and post UTRNs according*/
		/*    to the split until the debt is satisified.*/
		compute(wsaaSurrAmount, 5).set(mult(covruddIO.getCoverageDebt(), -1));
		Ulnkpf ulnkrnlIO = readUlnk6120();
		wsaaAloTot = BigDecimal.ZERO;
		for (ix = 1; !(isGT(ix, 10)
		|| isEQ(ulnkrnlIO.getUalfnd(ix), SPACES)); ix++){
			completeAndWriteUtrn6130(ulnkrnlIO);
		}
		/*    Rewrite the coverage debt as zero on the COVRPF.*/
		readhCovrAndTables5100();
		/*  It is possible that the first Component(s) read would hav   */
		/*  been Lapsed in which case the CHDRLIF Record would have     */
		/*  been rewritten and will now need READHing again.            */
		/*  Also, if this is the case then original increment of the    */
		/*  TRANNO will have been utilised for the Lapse, therefore     */
		/*  increment again.                                            */
		if (chdrWritten.isTrue()
		&& !chdrUpdated.isTrue()) {
			readhChdrlif5700();
			wsaaTranno.add(1);
		}
		ct21Value++;
		ct22Value = ct22Value.add(covruddIO.getCoverageDebt());
		covruddIO.setCoverageDebt(BigDecimal.ZERO);
		rewriteCovr5400();
		if (isNE(wsaaPrevChdrnum, covupfRec.getChdrnum())
		&& !chdrUpdated.isTrue()) {
			updateChdr5500();
			writePtrn5600();
		}

	}


protected Ulnkpf readUlnk6120()
 {
		/* Read ULNKRNL to get the fund split for the coverage/rider. */
		if (ulnkrnlMap != null && ulnkrnlMap.containsKey(covruddIO.getChdrnum())) {
			List<Ulnkpf> ulnkList = ulnkrnlMap.get(covruddIO.getChdrnum());
			for (Ulnkpf ulnkrnlIO : ulnkList) {
				if (ulnkrnlIO.getChdrcoy().equals(covruddIO.getChdrcoy())
						&& ulnkrnlIO.getLife().equals(covruddIO.getLife())
						&& ulnkrnlIO.getCoverage().equals(covruddIO.getCoverage())
						&& ulnkrnlIO.getRider().equals(covruddIO.getRider())
						&& ulnkrnlIO.getPlanSuffix() == covruddIO.getPlanSuffix()) {
					return ulnkrnlIO;
				}
			}
		}
		syserrrec.params.set(covruddIO.getChdrnum());
		fatalError600();
		return null;
	}

protected void completeAndWriteUtrn6130(Ulnkpf ulnkrnlIO)
	{
		/*    We dont have a great deal to do here as Unit Deal (B5102)*/
		/*    can manage to complete most of the UTRN details thus we*/
		/*    do only what is necessary.*/
		utrnaloIO.setContractAmount(div((mult(wsaaSurrAmount, ulnkrnlIO.getUalprc(ix))), 100).getbigdata());
		/* MOVE UTRNALO-CONTRACT-AMOUNT TO ZRDP-AMOUNT-IN.              */
		/* MOVE CHDRLIF-CNTCURR         TO ZRDP-CURRENCY.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO UTRNALO-CONTRACT-AMOUNT.     */
		if (utrnaloIO.getContractAmount().compareTo(BigDecimal.ZERO)!=0) {
			zrdecplrec.amountIn.set(utrnaloIO.getContractAmount());
			zrdecplrec.currency.set(chdrlifIO.getCntcurr());
			callRounding8000();
			utrnaloIO.setContractAmount(zrdecplrec.amountOut.getbigdata());
		}
		wsaaAloTot = wsaaAloTot.add(utrnaloIO.getContractAmount());
		/*    If the next fund on the UNLK is spaces then we have come to*/
		/*    the end. Thus we need to check that the amount of money buyin*/
		/*    units equals the total of the coverage debt. If not, a balanc*/
		/*    error results between the units purchased and the debt writte*/
		/*    off.*/
		if (isEQ(ulnkrnlIO.getUalfnd(ix+1), SPACES)) {
			if (isNE(wsaaAloTot, wsaaSurrAmount)) {
				compute(wsaaBalanceCheck, 5).set(sub(wsaaAloTot, wsaaSurrAmount));
				utrnaloIO.setContractAmount(utrnaloIO.getContractAmount().subtract(wsaaBalanceCheck.getbigdata()));
			}
		}
		covruddIO.setCoverageDebt(covruddIO.getCoverageDebt().subtract(utrnaloIO.getContractAmount()));
		utrnaloIO.setNofUnits(BigDecimal.ZERO);
		utrnaloIO.setNofDunits(BigDecimal.ZERO);
		utrnaloIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(ix));
		//san
		utrnaloIO.setFundPool(ulnkrnlIO.getFundPool(ix));
		//san

		boolean foundFlag = false;
		for(t5515Ix = 1;t5515Ix<wsaaT5515Fund.length;t5515Ix++){
			if(wsaaT5515Fund[t5515Ix].toString().equals(utrnaloIO.getUnitVirtualFund().trim())){
				foundFlag = true;
				break;
			}
		}
		
		if(!foundFlag){
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(tablesInner.t5515);
			itdmIO.setItemitem(utrnaloIO.getUnitVirtualFund());
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		utrnaloIO.setFundCurrency(wsaaT5515Currcode[t5515Ix].toString());
		utrnaloIO.setUnitType("A");
		utrnaloIO.setUnitSubAccount("ACUM");
		utrnaloIO.setDiscountFactor(BigDecimal.ONE);
		utrnaloIO.setTranno(wsaaTranno.toInt());
		utrnaloIO.setCntcurr(chdrlifIO.getCntcurr());
		utrnaloIO.setCnttyp(chdrlifIO.getCnttype());
		utrnaloIO.setChdrcoy(covruddIO.getChdrcoy());
		utrnaloIO.setChdrnum(covruddIO.getChdrnum());
		utrnaloIO.setLife(covruddIO.getLife());
		utrnaloIO.setCoverage(covruddIO.getCoverage());
		utrnaloIO.setRider(covruddIO.getRider());
		utrnaloIO.setPlanSuffix(covruddIO.getPlanSuffix());
		utrnaloIO.setCrtable(covruddIO.getCrtable());
		utrnaloIO.setCrComDate(new Long(covruddIO.getCrrcd()));
		utrnaloIO.setProcSeqNo(wsaaT6647ArrayInner.wsaaT6647ProcSeqNo[t6647Ix].toInt());
		/* MOVE WSAA-TRANSACTION-DATE TO UTRNALO-TRANSACTION-DATE.      */
		utrnaloIO.setTransactionDate(wsaaDate.toInt());
		utrnaloIO.setTransactionTime( varcom.vrcmTime.toInt());
		if (componLevelAccounted.isTrue()) {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[2].toString());
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[2].toString());
			utrnaloIO.setGenlcde(wsaaT5645Glmap[2].toString());
		}
		else {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[1].toString());
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[1].toString());
			utrnaloIO.setGenlcde(wsaaT5645Glmap[1].toString());
		}
		if(insertUtrnaloIOList == null){
			insertUtrnaloIOList = new ArrayList<>();
		}
		insertUtrnaloIOList.add(utrnaloIO);
		ct23Value++;
		ct24Value = ct24Value.add(utrnaloIO.getContractAmount());
	}


protected void debtOption6200()
	{
		/*DEBT*/
		/**     Not much point in performing this option. i.e reducing*/
		/**     the coverage debt by increasing the coverage debt*/
		/*EXIT*/
	}

protected void lapseOption6300()
 {
		readChdr6310();
		/* Set up params for COVRMJA IO module. COVRUDD file only */
		/* selects COVR's with a coverage Debt > 0. The first call to */
		/* CHDRMJAIO will return the coverage being lapsed. */
		/* Only unlock if necessary. */
		wsaaTotCovrpuInstprem = BigDecimal.ZERO;
		boolean firstFlag = true;
		if (covrmjaMap != null && covrmjaMap.containsKey(covruddIO.getChdrnum())) {
			List<Covrpf> covrList = covrmjaMap.get(covruddIO.getChdrnum());
			for (Covrpf covrmjaIO : covrList) {
				if (covrmjaIO.getChdrcoy().equals(covruddIO.getChdrcoy())&&covrmjaIO.getPlanSuffix() == covruddIO.getPlanSuffix()) {
					if(!firstFlag){
						checkT567963b0(covrmjaIO);
						if (isEQ(wsaaValidStatus, "N")
								|| isEQ(wsaaValidPstatus, "N")) {
									continue;
								}
					}
					firstFlag = false;
					/* If COVRMJA-STATCODE and COVRMJA-PSTATCODE not in T5679       */
					/* then go to next one.                                         */
					wsaaNewCovrDebt.set(covrmjaIO.getCoverageDebt());
					Covrpf covrmjaNew = rewriteHistoryCovr6340(covrmjaIO);
					lapseMethod6350(covrmjaNew);
					writeNewCovr6360(covrmjaNew);
				}
			}
		}

		lapseWritePtrn6391();
		lapseUpdateChdr6392();
	}


protected void readChdr6310()
 {
		/* We use CHDRMJA rather than CHDRLIF as it has all the */
		/* required fields for performing a Lapse. */
		if (chdrmjaMap != null && chdrmjaMap.containsKey(covupfRec.getChdrnum())) {
			List<Chdrpf> chdrlifList = chdrmjaMap.get(covupfRec.getChdrnum());
			for (Chdrpf c : chdrlifList) {
				if (covupfRec.getChdrcoy().equals(c.getChdrcoy().toString())) {
					chdrmjaIO = c;
					wsaaOccdate.set(chdrmjaIO.getOccdate());
					wsaaCurrfrom.set(chdrmjaIO.getCurrfrom());
					if (chdrUpdated.isTrue()) {
						compute(wsaaLapseTranno, 0).set(add(1, wsaaTranno));
					} else {
						wsaaLapseTranno.set(wsaaTranno);
					}
					return;
				}
			}
		}
		syserrrec.params.set(covupfRec.getChdrnum());
		fatalError600();

	}


protected Covrpf rewriteHistoryCovr6340(Covrpf covrmjaIO)
	{
	covrmjaIO.setValidflag("2");
	/* Do not move the coverage debt onto the component if it is    */
	/* a rider.                                                     */
	/* MOVE WSAA-ORIG-COVR-DEBT    TO COVRMJA-COVERAGE-DEBT.        */
	if (isEQ(covrmjaIO.getRider(), "00")) {
		covrmjaIO.setCoverageDebt(wsaaOrigCovrDebt.getbigdata());
	}
	covrmjaIO.setCurrto(chdrmjaIO.getBtdate());
	if(updateCovrmjapfList==null){
		updateCovrmjapfList = new ArrayList<>();
	}
	updateCovrmjapfList.add(covrmjaIO);
	
	wsaaTotCovrpuInstprem = wsaaTotCovrpuInstprem.add(covrmjaIO.getInstprem());

	Covrpf covrmjaNew = new Covrpf(covrmjaIO);
	/* MOVE WSAA-TRANNO            TO COVRMJA-TRANNO.               */
	covrmjaNew.setTranno(wsaaLapseTranno.toInt());
	covrmjaNew.setValidflag("1");
	/* MOVE CHDRMJA-BTDATE         TO COVRMJA-PREM-CESS-DATE.       */
	covrmjaNew.setCoverageDebt(wsaaNewCovrDebt.getbigdata());
	return covrmjaNew;

	}

protected void lapseMethod6350(Covrpf covrmjaIO)
 {
		if (isEQ(nonForfeitureMeth, SPACES)) {
			return;
		}
		if (isEQ(statuz, varcom.endp)) {
			itdmIO.setItemitem(nonForfeitureMeth);
			itdmIO.setItemcoy(utrnaloIO.getChdrcoy());
			itdmIO.setItemtabl(tablesInner.t6597);
			itdmIO.setItmfrm(bsscIO.getEffectiveDate());
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(premSubroutine, SPACES)) {
			return;
		}
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.company.set(chdrmjaIO.getCowncoy());
		ovrduerec.language.set(bsscIO.getLanguage());
		ovrduerec.trancode.set(batcdorrec.trcde);
		ovrduerec.acctyear.set(batcdorrec.actyear);
		ovrduerec.acctmonth.set(batcdorrec.actmonth);
		ovrduerec.batcbrn.set(batcdorrec.branch);
		ovrduerec.batcbatch.set(batcdorrec.batch);
		ovrduerec.user.set(ZERO);
		/* ACCEPT OVRD-TRAN-DATE FROM DATE. */
		ovrduerec.tranDate.set(wsaaDate);
		ovrduerec.tranTime.set(getCobolTime());
		ovrduerec.chdrnum.set(chdrmjaIO.getChdrnum());
		ovrduerec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		ovrduerec.cntcurr.set(chdrmjaIO.getCntcurr());
		ovrduerec.effdate.set(chdrmjaIO.getCcdate());
		ovrduerec.outstamt.set(chdrmjaIO.getOutstamt());
		ovrduerec.btdate.set(chdrmjaIO.getBtdate());
		ovrduerec.ptdate.set(chdrmjaIO.getPtdate());
		ovrduerec.ptdate.set(bsscIO.getEffectiveDate());
		ovrduerec.runDate.set(bsscIO.getEffectiveDate());
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		/* MOVE WSAA-TRANNO TO OVRD-TRANNO. */
		ovrduerec.tranno.set(wsaaLapseTranno);
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.life.set(covrmjaIO.getLife());
		ovrduerec.coverage.set(covrmjaIO.getCoverage());
		ovrduerec.rider.set(covrmjaIO.getRider());
		ovrduerec.planSuffix.set(covrmjaIO.getPlanSuffix());
		ovrduerec.crtable.set(covrmjaIO.getCrtable());
		ovrduerec.pumeth.set(SPACES);
		ovrduerec.billfreq.set(chdrmjaIO.getBillfreq());
		ovrduerec.instprem.set(covrmjaIO.getInstprem());
		ovrduerec.sumins.set(covrmjaIO.getSumins());
		ovrduerec.crrcd.set(covrmjaIO.getCrrcd());
		ovrduerec.premCessDate.set(covrmjaIO.getPremCessDate());
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.polsum.set(ZERO);
		callProgram(premSubroutine, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)) {
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		/* Read through the UTRNs for this Coverage key/tranno */
		/* using LIFO on logical and delete those written by */
		/* above routine. */
		if (utrnrevMap != null && utrnrevMap.containsKey(chdrmjaIO.getChdrnum())) {
			List<Utrnpf> utrnList = utrnrevMap.get(chdrmjaIO.getChdrnum());
			for (Utrnpf utrnrevIO : utrnList) {
				if (utrnrevIO.getChdrcoy().equals(chdrmjaIO.getChdrcoy())
						&& utrnrevIO.getLife().equals(covrmjaIO.getLife())
						&& utrnrevIO.getCoverage().equals(covrmjaIO.getCoverage())
						&& utrnrevIO.getRider().equals(covrmjaIO.getRider())
						&& utrnrevIO.getPlanSuffix() == covrmjaIO.getPlanSuffix()
						&& utrnrevIO.getTranno() == wsaaTranno.toInt()) {
					/* When we have reached those previously written by this */
					/* program, stop. */
					if (isEQ(utrnrevIO.getCovdbtind(), "D")) {
						break;
					}
					if (deleteUtrnpfList == null) {
						deleteUtrnpfList = new ArrayList<>();
					}
					deleteUtrnpfList.add(utrnrevIO);
				}
			}

		}

		if (isEQ(pStatCode, SPACES) && isEQ(rStatCode, SPACES)) {
			return;
		}
		covrmjaIO.setPstatcode(pStatCode);
		covrmjaIO.setStatcode(rStatCode);

	}

protected void writeNewCovr6360(Covrpf covrmjaIO)
	{
		/*START*/
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate.toInt());
		
		if(insertCovrmjapfList == null){
			insertCovrmjapfList = new ArrayList<>();
		}
		insertCovrmjapfList.add(covrmjaIO);
		
        List<Covrpf> covrList = new ArrayList<>();
        covrList.add(covrmjaIO);
        covrmjaMap.put(chdrmjaIO.getChdrnum(), covrList);	
		/*EXIT*/
	}

protected void checkT567963b0(Covrpf covrmjaIO)
	{
		wsaaValidStatus = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			if (isEQ(covrmjaIO.getRider(), "00")) {
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.covRiskStat[wsaaSub.toInt()])) {
					wsaaValidStatus = "Y";
					wsaaSub.set(12);
				}
			}
			else {
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.ridRiskStat[wsaaSub.toInt()])) {
					wsaaValidStatus = "Y";
					wsaaSub.set(12);
				}
			}
		}
		wsaaValidPstatus = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			if (isEQ(covrmjaIO.getRider(), "00")) {
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[wsaaSub.toInt()])) {
					wsaaValidPstatus = "Y";
					wsaaSub.set(12);
				}
			}
			else {
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.ridPremStat[wsaaSub.toInt()])) {
					wsaaValidPstatus = "Y";
					wsaaSub.set(12);
				}
			}
		}
	}

protected void lapseWritePtrn6391()
	{
		if (ptrnWritten.isTrue()) {
			return ;
		}
		/* IF  CHDR-UPDATED                                     <LA4300>*/
		/*     ADD 1 TO WSAA-TRANNO    GIVING WSAA-LAPSE-TRANNO <LA4300>*/
		/* ELSE                                                 <LA4300>*/
		/*     MOVE WSAA-TRANNO        TO     WSAA-LAPSE-TRANNO <LA4300>*/
		/* END-IF.                                              <LA4300>*/
		resetPtrn();
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		/* MOVE WSAA-TRANNO            TO PTRN-TRANNO.                  */
		ptrnIO.setTranno(wsaaLapseTranno.toInt());
		ptrnIO.setBatctrcde(wsaaTranCode.toString());
		/*  MOVE WSAA-TRANSACTION-DATE  TO PTRN-TRANSACTION-DATE.        */
		ptrnIO.setTrdt(wsaaDate.toInt());
		ptrnIO.setTrtm(Integer.parseInt(getCobolTime()));
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());
		if(insertPtrnpfList == null){
			insertPtrnpfList = new ArrayList<>();
		}
		insertPtrnpfList.add(ptrnIO);
		ct15Value++;
		wsaaPtrn.set("Y");
	}


protected void lapseUpdateChdr6392()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update6392();
				case writeNewContractHeader6392:
					writeNewContractHeader6392();
					writeRecord6392();
				case exit6392:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update6392()
	{
		/* Initialise flags for loop processing.*/
		if (chdrLapsed.isTrue()) {
			goTo(GotoLabel.exit6392);
		}
		wsaaValidPstatcode.set("Y");
		while ( !(endOfCovr.isTrue())) {
			checkChdrPaidup6393();
		}

		if (validStatuz.isTrue()) {
			wsaaChdrLapsed.set("Y");
		}
		if (chdrWritten.isTrue()) {
			update6394();
			goTo(GotoLabel.exit6392);
		}
		
		/* Rewrite the original contract header for historical purposes.*/
		Chdrpf chdrmjaUpdate = new Chdrpf();
		chdrmjaUpdate = chdrmjaIO;
		chdrmjaUpdate.setValidflag('2');
		/* Write the CURRTO and CURRFROM dates for valid flag '2'*/
		/* records on the CHDR File*/
		chdrmjaUpdate.setCurrto(chdrmjaIO.getBtdate());
		if (isGT(wsaaOccdate, wsaaCurrfrom)) {
			chdrmjaUpdate.setCurrfrom(wsaaOccdate.toInt());
		}
		else {
			chdrmjaUpdate.setCurrfrom(wsaaCurrfrom.toInt());
		}
		if(updateChdrList == null){
			updateChdrList = new ArrayList<>();
		}
		updateChdrList.add(chdrmjaUpdate);
		/* READH PAYR for update                                           */
		if(payrMap!=null&&payrMap.containsKey(ptrnIO.getChdrnum())){
			List<Payrpf> pList = payrMap.get(ptrnIO.getChdrnum());
			for(Payrpf p:pList){
				if(p.getChdrcoy().equals(ptrnIO.getChdrcoy())&&p.getPayrseqno() == 1){
					payrIO = p;
					break;
				}
			}
		}
		if (payrIO == null) {
			syserrrec.params.set(ptrnIO.getChdrnum());
			fatalError600();
		}
		/* Rewrite the PAYR with validflag = 2                             */
		if(updatePayrpfList == null){
			updatePayrpfList = new ArrayList<>();
		}
		Payrpf payrUpdate = new Payrpf();
		payrUpdate = payrIO;
		payrUpdate.setValidflag("2");
		updatePayrpfList.add(payrUpdate);
		
		if (isEQ(chdrmjaIO.getBillfreq(), "00")
		|| isEQ(chdrmjaIO.getBillfreq(), "  ")) {
			goTo(GotoLabel.writeNewContractHeader6392);
		}
		if (chdrLapsed.isTrue()) {
			/*       MOVE ZERO                 TO CHDRMJA-SINSTAMT01           */
			/*                                    CHDRMJA-SINSTAMT02           */
			/*                                    CHDRMJA-SINSTAMT03           */
			/*                                    CHDRMJA-SINSTAMT04           */
			/*                                    CHDRMJA-SINSTAMT05           */
			/*                                    CHDRMJA-SINSTAMT06           */
			/*       MOVE ZEROS                TO PAYR-SINSTAMT01      <LA4300>*/
			/*                                    PAYR-SINSTAMT02      <LA4300>*/
			/*                                    PAYR-SINSTAMT03      <LA4300>*/
			/*                                    PAYR-SINSTAMT04      <LA4300>*/
			/*                                    PAYR-SINSTAMT05      <LA4300>*/
			/*                                    PAYR-SINSTAMT06      <LA4300>*/
			goTo(GotoLabel.writeNewContractHeader6392);
		}
		if (chdrmjaIO.getSinstamt01().compareTo(wsaaTotCovrpuInstprem)!=0) {
			/*NEXT_SENTENCE*/
		}
		else {
			chdrmjaIO.setSinstamt02(BigDecimal.ZERO);
		}
		if (payrIO.getSinstamt01().compareTo(wsaaTotCovrpuInstprem)!=0) {
			/*NEXT_SENTENCE*/
		}
		else {
			payrIO.setSinstamt02(BigDecimal.ZERO);
		}
		chdrmjaIO.setSinstamt01(chdrmjaIO.getSinstamt01().subtract(wsaaTotCovrpuInstprem));
		payrIO.setSinstamt01(payrIO.getSinstamt01().subtract(wsaaTotCovrpuInstprem));
		chdrmjaIO.setSinstamt06(chdrmjaIO.getSinstamt01().add(chdrmjaIO.getSinstamt02()).add(chdrmjaIO.getSinstamt03()).add(chdrmjaIO.getSinstamt04()).add(chdrmjaIO.getSinstamt05()));
		payrIO.setSinstamt06(payrIO.getSinstamt01().add(payrIO.getSinstamt02()).add(payrIO.getSinstamt03()).add(payrIO.getSinstamt04()).add(payrIO.getSinstamt05()));
		chdrmjaIO.setInsttot01(BigDecimal.ZERO);
		chdrmjaIO.setInsttot02(BigDecimal.ZERO);
		chdrmjaIO.setInsttot03(BigDecimal.ZERO);
		chdrmjaIO.setInsttot04(BigDecimal.ZERO);
		chdrmjaIO.setInsttot05(BigDecimal.ZERO);
		chdrmjaIO.setInsttot06(BigDecimal.ZERO);
	}

protected void writeNewContractHeader6392()
	{
		/* MOVE WSAA-TRANNO            TO CHDRMJA-TRANNO.               */
		chdrmjaInsert = new Chdrpf();
		chdrmjaInsert = chdrmjaIO;
		chdrmjaInsert.setTranno(wsaaLapseTranno.toInt());
		chdrmjaInsert.setValidflag('1');
		/* Write the CURRTO and CURRFROM dates for valid flag '1'*/
		/* records on the CHDR File*/
		chdrmjaInsert.setCurrto(99999999);
		chdrmjaInsert.setCurrfrom(chdrmjaIO.getBtdate());
		if (chdrLapsed.isTrue()) {
			chdrmjaInsert.setPstcde(pStatCode);
			chdrmjaInsert.setStatcode(rStatCode);
		}
		/*    IF CHDR-LAPSED                                               */
		/*       NEXT SENTENCE                                             */
		/*    ELSE                                                         */
		/*       GO 6392-WRITE-RECORD.                                     */
		/*    MOVE WSAA-T6597-CPSTAT-04(T5687-IX)                          */
		/*                                TO CHDRMJA-PSTATCODE.            */
		/*    MOVE WSAA-T6597-CRSTAT-04(T5687-IX)                          */
		/*                                TO CHDRMJA-STATCODE.             */
		/*    MOVE SPACES                 TO PAYR-PARAMS.          <LA4300>*/
		/*    MOVE CHDRMJA-CHDRCOY        TO PAYR-CHDRCOY.         <LA4300>*/
		/*    MOVE CHDRMJA-CHDRNUM        TO PAYR-CHDRNUM.         <LA4300>*/
		/*    MOVE 1                      TO PAYR-PAYRSEQNO.       <LA4300>*/
		/*    MOVE READH                  TO PAYR-FUNCTION.        <LA4300>*/
		/*    MOVE PAYRREC                TO PAYR-FORMAT.          <LA4300>*/
		/*    CALL 'PAYRIO'               USING PAYR-PARAMS.       <LA4300>*/
		/*    IF PAYR-STATUZ              NOT = O-K                <LA4300>*/
		/*        MOVE PAYR-PARAMS        TO SYSR-PARAMS           <LA4300>*/
		/*        PERFORM 600-FATAL-ERROR                          <LA4300>*/
		/*    END-IF.                                              <LA4300>*/
		/* Rewrite the PAYR with validflag = 2                             */
		/*    MOVE '2'                    TO PAYR-VALIDFLAG.       <LA4300>*/
		/*    MOVE REWRT                  TO PAYR-FUNCTION.        <LA4300>*/
		/*    MOVE PAYRREC                TO PAYR-FORMAT.          <LA4300>*/
		/*    CALL 'PAYRIO'               USING PAYR-PARAMS.       <LA4300>*/
		/*    IF PAYR-STATUZ              NOT = O-K                <LA4300>*/
		/*        MOVE PAYR-PARAMS        TO SYSR-PARAMS           <LA4300>*/
		/*        PERFORM 600-FATAL-ERROR                          <LA4300>*/
		/*    END-IF.                                              <LA4300>*/
		/* Write the new PAYR with validflag = 1 and all fields            */
		/* correctly updated.                                              */
		/*    MOVE ZEROES                 TO PAYR-SINSTAMT01       <LA4300>*/
		/*                                   PAYR-SINSTAMT02       <LA4300>*/
		/*                                   PAYR-SINSTAMT03       <LA4300>*/
		/*                                   PAYR-SINSTAMT04       <LA4300>*/
		/*                                   PAYR-SINSTAMT05       <LA4300>*/
		/*                                   PAYR-SINSTAMT06.      <LA4300>*/
		Payrpf pIO = new Payrpf(payrIO);
		pIO.setPstatcode(chdrmjaIO.getPstcde());
		pIO.setValidflag("1");
		pIO.setTranno(chdrmjaIO.getTranno());
		pIO.setEffdate(chdrmjaIO.getCurrfrom());
		
		if(insertPayrpfList == null){
			insertPayrpfList = new ArrayList<>();
		}
		insertPayrpfList.add(pIO);
	}

protected void writeRecord6392()
	{
	
		if (insertChdrlifList == null) {
			insertChdrlifList = new ArrayList<>();
		}
		insertChdrlifList.add(chdrmjaInsert);
		ct14Value++;
		wsaaChdrWritten.set("Y");
		transferCovrTranno6395();
	}

protected void checkChdrPaidup6393()
	{
		wsaaValidStatuz.set("Y");
		if(covrmjaMap!=null&&covrmjaMap.containsKey(chdrmjaIO.getChdrnum())){
			List<Covrpf> covrList = covrmjaMap.get(chdrmjaIO.getChdrnum());
			for(Covrpf covrmjaIO:covrList){
				if(covrmjaIO.getChdrcoy().equals(chdrmjaIO.getChdrcoy().toString())){
					checkStatuz6393(covrmjaIO);
				}
			}
		}
		wsaaEndOfCovr.set("Y");
	}

protected void checkStatuz6393(Covrpf covrmjaIO)
	{
		wsaaEndOfCovr.set("N");
		/* IF COVRMJA-RIDER            NOT = '00'                       */
		/*  IF  WSAA-T6597-CPSTAT-04(T5687-IX) NOT = SPACES          */
		/*  AND WSAA-T6597-CRSTAT-04(T5687-IX) NOT = SPACES          */
		/*  AND WSAA-T6597-CRSTAT-04(T5687-IX) = COVRMJA-STATCODE    */
		/*  AND WSAA-T6597-CPSTAT-04(T5687-IX) = COVRMJA-PSTATCODE   */
		/*   MOVE 'Y'              TO WSAA-VALID-STATUZ           */
		/*  END-IF                                                   */
		/* ELSE                                                         */
		/*  IF  WSAA-T6597-CPSTAT-04(T5687-IX) NOT = SPACES          */
		/*  AND WSAA-T6597-CRSTAT-04(T5687-IX) NOT = SPACES          */
		/*  AND WSAA-T6597-CRSTAT-04(T5687-IX) = COVRMJA-STATCODE    */
		/*  AND WSAA-T6597-CPSTAT-04(T5687-IX) = COVRMJA-PSTATCODE   */
		/*      MOVE 'Y'              TO WSAA-VALID-STATUZ           */
		/*  END-IF                                                   */
		/* END-IF.                                                      */
		/*  Previous IF statement replaced. Checking should be carried  */
		/*  out as 'VALID' by default and then look for invalid statu   */
		/*  on each of the components.                                  */
		if (isNE(pStatCode, SPACES)
		&& isNE(rStatCode, SPACES)
		&& (isNE(rStatCode, covrmjaIO.getStatcode())
		|| isNE(pStatCode, covrmjaIO.getPstatcode()))) {
			wsaaValidStatuz.set("N");
		}
		if (validStatuz.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaEndOfCovr.set("Y");
			return ;
		}
	}

protected void update6394()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6394();
				case rewrite6394:
					rewrite6394();
				case writeRecord6394:
					writeRecord6394();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6394()
	{
		/* Update the PAYR record.                                         */
		if(payrMap!=null&&payrMap.containsKey(ptrnIO.getChdrnum())){
			List<Payrpf> pList = payrMap.get(ptrnIO.getChdrnum());
			for(Payrpf p:pList){
				if(p.getChdrcoy().equals(ptrnIO.getChdrcoy())&&p.getPayrseqno() == 1){
					payrIO = p;
					break;
				}
			}
		}
		if(payrIO == null){
			syserrrec.params.set(ptrnIO.getChdrnum());
			fatalError600();
		}

		if (isEQ(chdrmjaIO.getBillfreq(), "00")
		|| isEQ(chdrmjaIO.getBillfreq(), "  ")) {
			goTo(GotoLabel.rewrite6394);
		}
		if (chdrLapsed.isTrue()) {
			chdrmjaIO.setSinstamt01(BigDecimal.ZERO);
			chdrmjaIO.setSinstamt02(BigDecimal.ZERO);
			chdrmjaIO.setSinstamt03(BigDecimal.ZERO);
			chdrmjaIO.setSinstamt04(BigDecimal.ZERO);
			chdrmjaIO.setSinstamt05(BigDecimal.ZERO);
			chdrmjaIO.setSinstamt06(BigDecimal.ZERO);
			payrIO.setSinstamt01(BigDecimal.ZERO);
			payrIO.setSinstamt02(BigDecimal.ZERO);
			payrIO.setSinstamt03(BigDecimal.ZERO);
			payrIO.setSinstamt04(BigDecimal.ZERO);
			payrIO.setSinstamt05(BigDecimal.ZERO);
			payrIO.setSinstamt06(BigDecimal.ZERO);
			goTo(GotoLabel.rewrite6394);
		}
		if (chdrmjaIO.getSinstamt01().compareTo(wsaaTotCovrpuInstprem)==0) {
			chdrmjaIO.setSinstamt02(BigDecimal.ZERO);
		}
		chdrmjaIO.setSinstamt01(chdrmjaIO.getSinstamt01().subtract(wsaaTotCovrpuInstprem));
		payrIO.setSinstamt01(payrIO.getSinstamt01().subtract(wsaaTotCovrpuInstprem));
		chdrmjaIO.setSinstamt06(chdrmjaIO.getSinstamt01().add(chdrmjaIO.getSinstamt02()).add(chdrmjaIO.getSinstamt03()).add(chdrmjaIO.getSinstamt04()).add(chdrmjaIO.getSinstamt05()));
		payrIO.setSinstamt06(payrIO.getSinstamt01().add(payrIO.getSinstamt02()).add(payrIO.getSinstamt03()).add(payrIO.getSinstamt04()).add(payrIO.getSinstamt05()));
		chdrmjaIO.setInsttot01(BigDecimal.ZERO);
		chdrmjaIO.setInsttot02(BigDecimal.ZERO);
		chdrmjaIO.setInsttot03(BigDecimal.ZERO);
		chdrmjaIO.setInsttot04(BigDecimal.ZERO);
		chdrmjaIO.setInsttot05(BigDecimal.ZERO);
		chdrmjaIO.setInsttot06(BigDecimal.ZERO);
	}

protected void rewrite6394()
	{
		if (chdrLapsed.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.writeRecord6394);
		}
		chdrmjaIO.setPstcde(pStatCode);
		chdrmjaIO.setStatcode(rStatCode);
	}

protected void writeRecord6394()
	{
		if(updateChdrmjaList == null){
			updateChdrmjaList = new ArrayList<>();
		}
		updateChdrmjaList.add(chdrmjaIO);
		/* Update the PAYR record.                                         */
		
		if(updatePayrpfList == null){
			updatePayrpfList = new ArrayList<>();
		}
		payrIO.setPstatcode(chdrmjaIO.getPstcde());
		updatePayrpfList.add(payrIO);
	}

protected void transferCovrTranno6395()
 {
		if (covrmjaMap != null && covrmjaMap.containsKey(chdrmjaIO.getChdrnum())) {
			List<Covrpf> covrList = covrmjaMap.get(chdrmjaIO.getChdrnum());
			for (Covrpf covrmjaIO : covrList) {
				if (covrmjaIO.getChdrcoy().equals(chdrmjaIO.getChdrcoy().toString())
						&& covrmjaIO.getTranno() == wsaaTranno.toInt()) {
					covrmjaIO.setTranno(wsaaLapseTranno.toInt());
					if (updateCovrpfTrannoList == null) {
						updateCovrpfTrannoList = new ArrayList<>();
					}
					updateCovrpfTrannoList.add(covrmjaIO);
				}
			}
		}
	}

protected void errorOption6400()
	{
		uderIO.setRecordtype("COVR");
		uderIO.setRelativeRecordNo(covruddIO.getUniqueNumber());
		uderIO.setChdrcoy(covruddIO.getChdrcoy());
		uderIO.setChdrnum(covruddIO.getChdrnum());
		uderIO.setLife(covruddIO.getLife());
		uderIO.setCoverage(covruddIO.getCoverage());
		uderIO.setRider(covruddIO.getRider());
		uderIO.setPlanSuffix(covruddIO.getPlanSuffix());
		uderIO.setUnitVirtualFund(SPACES.toString());
		uderIO.setUnitType(SPACES.toString());
		uderIO.setTranno(wsaaTranno.toInt());
		uderIO.setBatctrcde(SPACES.toString());
		uderIO.setContractAmount(covruddIO.getCoverageDebt());
		uderIO.setCntcurr(chdrlifIO.getCntcurr());
		uderIO.setFundAmount(BigDecimal.ZERO);
		uderIO.setFundCurrency(SPACES.toString());
		uderIO.setFundRate(BigDecimal.ZERO);
		uderIO.setAge(datcon3rec.freqFactor.toInt());
		uderIO.setTriggerModule("UNPAID DBT");
		if(insertUderpfList == null){
			insertUderpfList = new ArrayList<>();
		}
		insertUderpfList.add(uderIO);
		uderIO = new Uderpf();
		ct25Value++;
	}

protected void b100CreateNewZrst()
	{
		if (updateZrstpfList != null && !updateZrstpfList.isEmpty()) {
			for (Zrstpf zrsttrnIO : updateZrstpfList) {
				Zrstpf zrstIO = new Zrstpf();
				zrstIO.setChdrcoy(zrsttrnIO.getChdrcoy());
				zrstIO.setChdrnum(zrsttrnIO.getChdrnum());
				zrstIO.setLife(zrsttrnIO.getLife());
				zrstIO.setJlife(zrsttrnIO.getJlife());
				zrstIO.setCoverage(zrsttrnIO.getCoverage());
				zrstIO.setRider(zrsttrnIO.getRider());
				zrstIO.setSeqno(zrsttrnIO.getSeqno());
				zrstIO.setTranno(zrsttrnIO.getTranno());
				zrstIO.setBatctrcde(zrsttrnIO.getBatctrcde());
				zrstIO.setSacstyp(zrsttrnIO.getSacstyp());
				zrstIO.setZramount01(zrsttrnIO.getZramount01());
				zrstIO.setZramount02(zrsttrnIO.getZramount02());
				zrstIO.setUstmno(zrsttrnIO.getUstmno());
				zrstIO.setTrandate(bsscIO.getEffectiveDate().toInt());
				zrstIO.setFeedbackInd(SPACES.toString());
				zrstIO.setXtranno(0);

				if (insertZrstpfList == null) {
					insertZrstpfList = new ArrayList<>();
				}
				insertZrstpfList.add(zrstIO);
			}
		}
	}


protected void x1000CallXcvrt()
	{
		/*X1010-CALL*/
		/*    If the Fund Currency and the Contract Currency are not the*/
		/*    same then the following conversion routine must be called.*/
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(bsprIO.getCompany());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/*X1090-EXIT*/
	}

protected void getDfactForInit7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begin7010();
				case fixedTerm7153:
					fixedTerm7153();
				case wholeOfLife7154:
					wholeOfLife7154();
				case exit7090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin7010()
	{
		if (isNE(wsaaT5540WholeIuDiscFact[t5540Ix], SPACES)) {
			goTo(GotoLabel.wholeOfLife7154);
		}
		if (isNE(wsaaT5540IuDiscBasis[t5540Ix], SPACES)) {
			if (isNE(t5519rec.fixdtrm, ZERO)
			&& isGT(wsaaRiskCessTerm, t5519rec.fixdtrm)) {
				goTo(GotoLabel.fixedTerm7153);
			}
			readT55395155();
			if (isLT(wsaaRiskCessTerm, 100)) {
				wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
			}
			else {
				compute(wsaaIndex, 0).set(sub(wsaaRiskCessTerm, 99));
				wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaIndex.toInt()]);
			}
			utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000).getbigdata());
		}
		goTo(GotoLabel.exit7090);
	}

protected void fixedTerm7153()
	{
		readT55395155();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM)                      <V73L03>*/
		/*                             TO WSAA-INIT-UNIT-DISC-FACT.<V73L03>*/
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaIndex, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaIndex.toInt()]);
		}
		utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000).getbigdata());
		goTo(GotoLabel.exit7090);
	}

protected void wholeOfLife7154()
	{
		/* Use the age at next birthday to read off T6646 to obtain        */
		/* the discount factor.                                            */
		if (covruddIO.getAnbAtCcd() == 0) {
			return ;
		}
		if (isNE(wsaaT5540WholeIuDiscFact[t5540Ix], SPACES)) {
			readT66465155();
			if (covruddIO.getAnbAtCcd()<100) {
				wsaaInitUnitDiscFact.set(t6646rec.dfact[covruddIO.getAnbAtCcd()]);
			}
			else {
				compute(wsaaIndex, 0).set(sub(covruddIO.getAnbAtCcd(), 99));
				wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaIndex.toInt()]);
				utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000).getbigdata());
			}
		}
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void resetPtrn(){
	ptrnIO = new Ptrnpf();
	ptrnIO.setUserT(999999);
	ptrnIO.setTermid("");
	ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
	ptrnIO.setBatccoy(batcdorrec.company.toString());
	ptrnIO.setBatcbrn(batcdorrec.branch.toString());
	ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnIO.setBatcbatch(batcdorrec.batch.toString());
	ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
	ptrnIO.setValidflag("1");	
	
}
/*
 * Class transformed  from Data Structure WSAA-T6647-ARRAY--INNER
 */
private static final class WsaaT6647ArrayInner {

		/* WSAA-T6647-ARRAY */
	private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (2050, 40);  //ILIFE-6190
	private FixedLengthStringData[] wsaaT6647Key = FLSDArrayPartOfArrayStructure(7, wsaaT6647Rec, 0);
	private FixedLengthStringData[] wsaaT6647Batctrcde = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT6647Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6647Key, 4, HIVALUE);
	private FixedLengthStringData[] wsaaT6647Data = FLSDArrayPartOfArrayStructure(33, wsaaT6647Rec, 7);
	private PackedDecimalData[] wsaaT6647Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6647Data, 0);
	private FixedLengthStringData[] wsaaT6647Aloind = FLSDArrayPartOfArrayStructure(1, wsaaT6647Data, 5);
	private FixedLengthStringData[] wsaaT6647Bidoffer = FLSDArrayPartOfArrayStructure(1, wsaaT6647Data, 6);
	private FixedLengthStringData[] wsaaT6647Dealin = FLSDArrayPartOfArrayStructure(1, wsaaT6647Data, 7);
	private FixedLengthStringData[] wsaaT6647Efdcode = FLSDArrayPartOfArrayStructure(2, wsaaT6647Data, 8);
	private FixedLengthStringData[] wsaaT6647Enhall = FLSDArrayPartOfArrayStructure(4, wsaaT6647Data, 10);
	private PackedDecimalData[] wsaaT6647MonthsDebt = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 14);
	private PackedDecimalData[] wsaaT6647MonthsError = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 16);
	private PackedDecimalData[] wsaaT6647MonthsLapse = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 18);
	private PackedDecimalData[] wsaaT6647MonthsNegUnits = PDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 20);
	private ZonedDecimalData[] wsaaT6647ProcSeqNo = ZDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 22);
	private FixedLengthStringData[] wsaaT6647UnitStatMethod = FLSDArrayPartOfArrayStructure(4, wsaaT6647Data, 25);
	private FixedLengthStringData[] wsaaT6647Swmeth = FLSDArrayPartOfArrayStructure(4, wsaaT6647Data, 29);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t5515 = new FixedLengthStringData(6).init("T5515");
	private FixedLengthStringData t5540 = new FixedLengthStringData(6).init("T5540");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	private FixedLengthStringData t6597 = new FixedLengthStringData(6).init("T6597");
	private FixedLengthStringData t6647 = new FixedLengthStringData(6).init("T6647");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
	private ZonedDecimalData ct13 = new ZonedDecimalData(2, 0).init(13).setUnsigned();
	private ZonedDecimalData ct14 = new ZonedDecimalData(2, 0).init(14).setUnsigned();
	private ZonedDecimalData ct15 = new ZonedDecimalData(2, 0).init(15).setUnsigned();
	private ZonedDecimalData ct16 = new ZonedDecimalData(2, 0).init(16).setUnsigned();
	private ZonedDecimalData ct17 = new ZonedDecimalData(2, 0).init(17).setUnsigned();
	private ZonedDecimalData ct18 = new ZonedDecimalData(2, 0).init(18).setUnsigned();
	private ZonedDecimalData ct19 = new ZonedDecimalData(2, 0).init(19).setUnsigned();
	private ZonedDecimalData ct20 = new ZonedDecimalData(2, 0).init(20).setUnsigned();
	private ZonedDecimalData ct21 = new ZonedDecimalData(2, 0).init(21).setUnsigned();
	private ZonedDecimalData ct22 = new ZonedDecimalData(2, 0).init(22).setUnsigned();
	private ZonedDecimalData ct23 = new ZonedDecimalData(2, 0).init(23).setUnsigned();
	private ZonedDecimalData ct24 = new ZonedDecimalData(2, 0).init(24).setUnsigned();
	private ZonedDecimalData ct25 = new ZonedDecimalData(2, 0).init(25).setUnsigned();
	private ZonedDecimalData ct26 = new ZonedDecimalData(2, 0).init(26).setUnsigned();
	private ZonedDecimalData ct27 = new ZonedDecimalData(3, 0).init(27).setUnsigned();
	private ZonedDecimalData ct28 = new ZonedDecimalData(3, 0).init(28).setUnsigned();
	private ZonedDecimalData ct29 = new ZonedDecimalData(3, 0).init(29).setUnsigned();
}

}