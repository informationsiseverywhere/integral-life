/*
 * File: P5509.java
 * Date: 30 August 2009 0:29:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P5509.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.screens.S5509ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5509rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*  VIRTUAL FUND BID/OFFER SPREAD
*
*  ALLOWS THE ENTRY OF THE BID.OFFER SPREAD AND ROUDING FACTORS
*  TO BE INVOLVED IN THE CALCULATION OF BID/OFFER PRICES FOR THAT
*
*
*****************************************************************
* </pre>
*/
public class P5509 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5509");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/* ERRORS */
	private String e017 = "E017";
	private String e492 = "E492";
	private String e031 = "E031";
	private String e186 = "E186";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5509rec t5509rec = new T5509rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5509ScreenVars sv = ScreenProgram.getScreenVars( S5509ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1190, 
		preExit, 
		exit2090, 
		exit3900, 
		exit5090
	}

	public P5509() {
		super();
		screenVars = sv;
		new ScreenModel("S5509", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1001();
				}
				case cont1190: {
					cont1190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setDataArea(SPACES);
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setStatuz(e031);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5509rec.t5509Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.cont1190);
		}
		t5509rec.acumbof.set(ZERO);
		t5509rec.initBidOffer.set(ZERO);
		t5509rec.accumRounding.set(ZERO);
		t5509rec.initialRounding.set(ZERO);
		t5509rec.managementCharge.set(ZERO);
		t5509rec.tolerance.set(ZERO);
	}

protected void cont1190()
	{
		sv.acumbof.set(t5509rec.acumbof);
		sv.accumRounding.set(t5509rec.accumRounding);
		sv.initBidOffer.set(t5509rec.initBidOffer);
		sv.initialRounding.set(t5509rec.initialRounding);
		if (isEQ(itemIO.getItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itemIO.getItmfrm());
		}
		if (isEQ(itemIO.getItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itemIO.getItmto());
		}
		sv.initAccumSame.set(t5509rec.initAccumSame);
		sv.managementCharge.set(t5509rec.managementCharge);
		sv.tolerance.set(t5509rec.tolerance);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2001();
			validate2100();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2001()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
	}

protected void validate2100()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.itmfrmErr,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.itmfrm,varcom.vrcmMaxDate)) {
			sv.itmfrmErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isGT(sv.itmfrm,sv.itmto)) {
			sv.itmfrmErr.set(e017);
			sv.itmtoErr.set(e017);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.initAccumSame,SPACES)) {
			sv.iuacumErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.initBidOffer,ZERO)) {
			sv.initbofErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.initialRounding,ZERO)) {
			sv.initrndErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.initAccumSame,"N")
		&& isEQ(sv.accumRounding,ZERO)) {
			sv.acumrndErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.initAccumSame,"Y")
		&& (isNE(sv.acumbof,ZERO)
		|| isNE(sv.accumRounding,ZERO))) {
			sv.acumbofErr.set(e492);
			sv.acumrndErr.set(e492);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.initAccumSame,"N")
		&& (isEQ(sv.acumbof,ZERO)
		|| isEQ(sv.accumRounding,ZERO))) {
			sv.acumbofErr.set(e186);
			sv.acumrndErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.managementCharge,ZERO)) {
			sv.mgmtchrgErr.set(e186);
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			loadWsspFields3100();
		}
		catch (GOTOException e){
		}
	}

protected void loadWsspFields3100()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(sv.initAccumSame,"Y")) {
			sv.accumRounding.set(sv.initialRounding);
			sv.acumbof.set(sv.initBidOffer);
		}
		updateRec5000();
		/*COMMIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void updateRec5000()
	{
		try {
			para5000();
			compareFields5020();
			writeRecord5080();
		}
		catch (GOTOException e){
		}
	}

protected void para5000()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		/*READ-RECORD*/
		callItemio5100();
	}

protected void compareFields5020()
	{
		wsaaUpdateFlag = "N";
		checkChanges5300();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.exit5090);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
		itemIO.setGenarea(t5509rec.t5509Rec);
	}

protected void writeRecord5080()
	{
		itemIO.setFunction(varcom.rewrt);
		callItemio5100();
	}

protected void callItemio5100()
	{
		/*PARA*/
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.dbparams.set(itemIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkChanges5300()
	{
		para5300();
	}

protected void para5300()
	{
		if (isNE(sv.acumbof,t5509rec.acumbof)) {
			t5509rec.acumbof.set(sv.acumbof);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.accumRounding,t5509rec.accumRounding)) {
			t5509rec.accumRounding.set(sv.accumRounding);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.initBidOffer,t5509rec.initBidOffer)) {
			t5509rec.initBidOffer.set(sv.initBidOffer);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.initialRounding,t5509rec.initialRounding)) {
			t5509rec.initialRounding.set(sv.initialRounding);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itemIO.getItmfrm())) {
			itemIO.setItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itemIO.getItmto())) {
			itemIO.setItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.initAccumSame,t5509rec.initAccumSame)) {
			t5509rec.initAccumSame.set(sv.initAccumSame);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.managementCharge,t5509rec.managementCharge)) {
			t5509rec.managementCharge.set(sv.managementCharge);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.tolerance,t5509rec.tolerance)) {
			t5509rec.tolerance.set(sv.tolerance);
			wsaaUpdateFlag = "Y";
		}
	}
}
