package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:22
 * Description:
 * Copybook name: T6578REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6578rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6578Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ustprcoms = new FixedLengthStringData(120).isAPartOf(t6578Rec, 0);
  	public FixedLengthStringData[] ustprcom = FLSArrayPartOfStructure(2, 60, ustprcoms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(ustprcoms, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ustprcom1 = new FixedLengthStringData(60).isAPartOf(filler, 0);
  	public FixedLengthStringData ustprcom2 = new FixedLengthStringData(60).isAPartOf(filler, 60);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(380).isAPartOf(t6578Rec, 120, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6578Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6578Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}