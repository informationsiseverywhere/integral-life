package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;


public class Ufnspf{
	private long uniqueNumber;
	private String company;
	private String vrtfund;
	private String unityp;
	private BigDecimal nofunts;
	private int jobno;
	private String userProfile;
	private String jobName;
	private String datime;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getVrtfund() {
		return vrtfund;
	}
	public void setVrtfund(String vrtfund) {
		this.vrtfund = vrtfund;
	}
	public String getUnityp() {
		return unityp;
	}
	public void setUnityp(String unityp) {
		this.unityp = unityp;
	}
	public BigDecimal getNofunts() {
		return nofunts;
	}
	public void setNofunts(BigDecimal nofunts) {
		this.nofunts = nofunts;
	}
	public int getJobno() {
		return jobno;
	}
	public void setJobno(int jobno) {
		this.jobno = jobno;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
}