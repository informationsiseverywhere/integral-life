/*
 * File: B6269.java
 * Date: 29 August 2009 21:20:56
 * Author: Quipoz Limited
 * 
 * Class transformed from B6269.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.CovrpfTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* B6269 - Regular Processing - Cancellation of initial units.
*
*    This is one of the modules in the Regular processing suite of
* programs.  It uses OPNQRYF in the calling CL program to select
* all  components which are due for anniversary processing.  Any
* components  due are then processed by calling generic routines
* as determined by the basis held in T5540.
*
*    Control totals maintained by this program are:
*
*      1. Total COVRs read
*      2. COVR cancellation of initial units due
*      3. COVR conversion of initial units due
*
*    The  calling  CL  program will use OPNQRYF to select into a
* file details from the generic component (COVRPF) file based on
* the following selection criteria:
*
*  Keyed on : Contract
*             Life
*             Coverage
*             Rider
*             Suffix
*
*  Select   : IF   COVR-INIT-UNIT-CANC-DATE <= parm date
*             and  COVR-VALIDFLAG = '1'
*             and  COVR-CURR-FROM-DATE <= COVR-INIT-UNIT-CANC-DATE
*             and  COVR-CURR-TO-DATE > COVR-INIT-UNIT-CANC-DATE
*  Omit     : IF   COVR-INIT-UNIT-CANC-DATE = 0
*
*  1.) Initialise.
*                                                                        
*    Reset   the   control  parameters  if  the  program  is  in
* recovery/restart  mode  by  performing  004-UPDATE-REQD  until
* UPDATE-FLAG  is  yes.  This  is  necessary as OPNQRYF will not
* select records updated previously.
*
*  2.) Process Components
*                                                                       
*    The  records from the passed file are read sequentially and
* for each:
*
*    Read   the   transaction  status  codes  from  T5679  using
* parameter  1  (tran  code) from the jobstream details for this
* program. If the COVR status fields do not match the table read
* the next sequential record.
*
*    Read  the COVRPF with the logical view COVR and softlock
* the record.
*
*    Add 1 to control total no.1.
*
*    Read  T5540  with  COVR-CRTABLE  as  the  key  to  get  the
* initial unit discount basis.   Read  T5519  (Discount basis
* details)  with  the basis as  the key to get the
* generic processing subroutine and relevant details.
*
*    Dynamically   call   the   subroutine  with  the  following
* parameters set up in ANNPROCREC:
*
*      - function
*      - status
*      - rider key
*      - plan suffix
*      - effective date (parm date)
*      - batchkey
*      - details from T5519
*
*    If   the  status  returned  is  O-K  recalculate  the  next
* COVR-INIT-UNIT-CANC-DATE  as  COVR-INIT-UNIT-CANC-DATE
* incremented by one factor of the charge frequency. Read and hold
* the record  and  rewrite  to  the database. Add 1 to control total
* no.2.
*
* The records selected by the OPNQRYF are in the following order:-
*
*     Contract
*     Life
*     Coverage
*     Rider
*     Plan Suffix
*
* The selected records are being put into COVRPF by the CLP. So
* this program will only need to read this file.
*
*
*****************************************************************
* </pre>
*/
public class B6269 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private CovrpfTableDAM covrpf = new CovrpfTableDAM();
	private CovrpfTableDAM covrpfRec = new CovrpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5094");

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRunparm1TranCode = new FixedLengthStringData(4).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaRunparm1Rest = new FixedLengthStringData(6).isAPartOf(wsaaRunparm1, 4);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private String endOfFile = "N";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).init(0).setUnsigned();
		/* TABLES */
	private String t5679 = "T5679";
	private String t5540 = "T5540";
	private String t5519 = "T5519";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String covr = "COVRREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private Annprocrec annprocrec = new Annprocrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Sftlockrec sftlockrec1 = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5519rec t5519rec = new T5519rec();
	private T5540rec t5540rec = new T5540rec();
	private T5679rec t5679rec = new T5679rec();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemkey = new Itemkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setConversionDate1720, 
		nearExit1800, 
		exit1900, 
		exit2990, 
		exit3490, 
		exit3990
	}

	public B6269() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		open110();
		openBatch120();
		mainProcess130();
		closeBatch170();
		out185();
	}

protected void open110()
	{
		covrpf.openInput();
	}

protected void openBatch120()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
		initialise200();
	}

protected void mainProcess130()
	{
		while ( !(isEQ(endOfFile,"Y"))) {
			mainProcessing1000();
		}
		
	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
	}

protected void out185()
	{
		covrpf.close();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaIndex.set(0);
		endOfFile = "N";
		wsaaItemkey.set(SPACES);
		wsaaRunparm1.set(runparmrec.runparm1);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t5679);
		wsaaItemkey.itemItemitem.set(wsaaRunparm1TranCode);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					process1100();
					processCovr1500();
					updateCovr1700();
				}
				case setConversionDate1720: {
					setConversionDate1720();
				}
				case nearExit1800: {
					nearExit1800();
				}
				case exit1900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void process1100()
	{
		updateReqd004();
		if (isEQ(controlrec.flag,"N")) {
			goTo(GotoLabel.exit1900);
		}
		covrpf.read(covrpfRec);
		if (covrpf.isAtEnd()) {
			endOfFile = "Y";
			goTo(GotoLabel.exit1900);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		wsaaValidStatus.set("N");
		if ((isNE(covrpfRec.coverage,SPACES))
		&& ((isEQ(covrpfRec.rider,SPACES))
		|| (isEQ(covrpfRec.rider,"00")))) {
			wsaaIndex.set(1);
			while ( !(isGT(wsaaIndex,12))) {
				validateCovrStatus2500();
			}
			
		}
		if ((isNE(covrpfRec.coverage,SPACES))
		&& ((isNE(covrpfRec.rider,SPACES))
		&& (isNE(covrpfRec.rider,"00")))) {
			wsaaIndex.set(1);
			while ( !(isGT(wsaaIndex,12))) {
				validateRiderStatus3000();
			}
			
		}
		if (isEQ(wsaaValidStatus,"N")) {
			goTo(GotoLabel.exit1900);
		}
	}

protected void processCovr1500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covrpfRec.crtable);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.statuz.set(itdmIO.getStatuz());
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5540))
		|| (isNE(itdmIO.getItemitem(),covrpfRec.crtable))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			goTo(GotoLabel.exit1900);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5519);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			conerrrec.statuz.set(itdmIO.getStatuz());
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if ((isNE(itdmIO.getItemcoy(),runparmrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5519))
		|| (isNE(itdmIO.getItemitem(),t5540rec.iuDiscBasis))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
		if (isEQ(t5519rec.subprog,SPACES)) {
			goTo(GotoLabel.exit1900);
		}
		softlockReadCovr3500();
		if (isEQ(sftlockrec1.statuz,"LOCK")) {
			appVars.addDiagnostic(covrpfRec.chdrnum.toString() +"ALREADY LOCKED - PROCESSING BYPASSED", 0);
			goTo(GotoLabel.exit1900);
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(covrpfRec.chdrcoy);
		annprocrec.chdrnum.set(covrpfRec.chdrnum);
		annprocrec.life.set(covrpfRec.life);
		annprocrec.coverage.set(covrpfRec.coverage);
		annprocrec.rider.set(covrpfRec.rider);
		annprocrec.planSuffix.set(covrpfRec.planSuffix);
		annprocrec.effdate.set(runparmrec.effdate);
		annprocrec.batctrcde.set(runparmrec.runparm1);
		annprocrec.crdate.set(covrpfRec.crrcd);
		annprocrec.crtable.set(covrpfRec.crtable);
		annprocrec.user.set(runparmrec.user);
		annprocrec.batcactyr.set(runparmrec.acctyear);
		annprocrec.batcactmn.set(runparmrec.acctmonth);
		annprocrec.batccoy.set(runparmrec.company);
		annprocrec.batcbrn.set(runparmrec.batcbranch);
		annprocrec.batcbatch.set(batcdorrec.batch);
		annprocrec.batcpfx.set(batcdorrec.prefix);
		annprocrec.initUnitChargeFreq.set(t5519rec.initUnitChargeFreq);
		annprocrec.annchg.set(t5519rec.annchg);
		callProgram(t5519rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(annprocrec.statuz);
			conerrrec.params.set(annprocrec.annpllRec);
			systemError005();
		}
	}

protected void updateCovr1700()
	{
		if (isEQ(covrIO.getInitUnitCancDate(),0)) {
			goTo(GotoLabel.setConversionDate1720);
		}
		if (isNE(t5519rec.initUnitChargeFreq,NUMERIC)) {
			t5519rec.initUnitChargeFreq.set("00");
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set(t5519rec.initUnitChargeFreq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(covrIO.getInitUnitCancDate());
		datcon2rec.intDate2.set(0);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		covrIO.setInitUnitCancDate(datcon2rec.intDate2);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.nearExit1800);
	}

protected void setConversionDate1720()
	{
		covrIO.setConvertInitialUnits(ZERO);
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void nearExit1800()
	{
		unlockRewriteCovr4000();
	}

protected void validateCovrStatus2500()
	{
		try {
			para2510();
		}
		catch (GOTOException e){
		}
	}

protected void para2510()
	{
		if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()],SPACES)) {
			if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpfRec.statcode))) {
				wsaaIndex.set(13);
				wsaaValidStatus.set("Y");
				goTo(GotoLabel.exit2990);
			}
		}
		wsaaIndex.add(1);
	}

protected void validateRiderStatus3000()
	{
		try {
			para3010();
		}
		catch (GOTOException e){
		}
	}

protected void para3010()
	{
		if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()],SPACES)) {
			if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrpfRec.statcode))) {
				wsaaIndex.set(13);
				wsaaValidStatus.set("Y");
				goTo(GotoLabel.exit3490);
			}
		}
		wsaaIndex.add(1);
	}

protected void softlockReadCovr3500()
	{
		try {
			para3510();
		}
		catch (GOTOException e){
		}
	}

protected void para3510()
	{
		sftlockrec1.function.set("LOCK");
		sftlockrec1.company.set(runparmrec.company);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.entity.set(covrpfRec.chdrnum);
		sftlockrec1.transaction.set(wsaaRunparm1TranCode);
		sftlockrec1.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if ((isNE(sftlockrec1.statuz,varcom.oK))
		&& (isNE(sftlockrec1.statuz,"LOCK"))) {
			conerrrec.params.set(sftlockrec1.sftlockRec);
			conerrrec.statuz.set(sftlockrec1.statuz);
			systemError005();
		}
		if (isEQ(sftlockrec1.statuz,"LOCK")) {
			goTo(GotoLabel.exit3990);
		}
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(covrpfRec.chdrcoy);
		covrIO.setChdrnum(covrpfRec.chdrnum);
		covrIO.setLife(covrpfRec.life);
		covrIO.setCoverage(covrpfRec.coverage);
		covrIO.setRider(covrpfRec.rider);
		covrIO.setPlanSuffix(covrpfRec.planSuffix);
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(covrIO.getStatuz());
			conerrrec.params.set(covrIO.getParams());
			databaseError006();
		}
	}

protected void unlockRewriteCovr4000()
	{
		para4010();
	}

protected void para4010()
	{
		covrIO.setFunction(varcom.rewrt);
		covrIO.setFormat(covr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(covrIO.getStatuz());
			conerrrec.params.set(covrIO.getParams());
			databaseError006();
		}
		sftlockrec1.sftlockRec.set(SPACES);
		sftlockrec1.company.set(runparmrec.company);
		sftlockrec1.entity.set(covrpfRec.chdrnum);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.user.set(runparmrec.user);
		sftlockrec1.transaction.set(wsaaRunparm1TranCode);
		sftlockrec1.statuz.set(SPACES);
		sftlockrec1.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz,varcom.oK)) {
			conerrrec.statuz.set(sftlockrec1.statuz);
			conerrrec.params.set(sftlockrec1.sftlockRec);
			systemError005();
		}
	}
}
