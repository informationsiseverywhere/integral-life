package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6519
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6519ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(895);
	public FixedLengthStringData dataFields = new FixedLengthStringData(463).isAPartOf(dataArea, 0);
	public FixedLengthStringData brchname = DD.brchlname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,62);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,92);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,147);
	public FixedLengthStringData jowner = DD.jowner.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData jownername = DD.jownername.copy().isAPartOf(dataFields,163);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,257);
	public ZonedDecimalData lststmdte = DD.lststmdte.copyToZonedDecimal().isAPartOf(dataFields,265);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,273);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,277);
	public FixedLengthStringData payer = DD.payer.copy().isAPartOf(dataFields,324);
	public FixedLengthStringData payername = DD.payername.copy().isAPartOf(dataFields,332);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,379);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,389);
	public FixedLengthStringData servagnam = DD.servagnam.copy().isAPartOf(dataFields,392);
	public FixedLengthStringData servagnt = DD.servagnt.copy().isAPartOf(dataFields,439);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,447);
	public ZonedDecimalData stmtdate = DD.stmtdate.copyToZonedDecimal().isAPartOf(dataFields,449);
	public FixedLengthStringData stmtlevel = DD.stmtlevel.copy().isAPartOf(dataFields,457);
	public ZonedDecimalData ustmno = DD.ustmno.copyToZonedDecimal().isAPartOf(dataFields,458);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 463);
	public FixedLengthStringData brchlnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jownerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lststmdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData payerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData payernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData servagnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData servagntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData stmtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData stmtlevelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData ustmnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 571);
	public FixedLengthStringData[] brchlnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jownerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lststmdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] payerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] payernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] servagnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] servagntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] stmtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] stmtlevelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] ustmnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lststmdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData stmtdateDisp = new FixedLengthStringData(10);

	public LongData S6519screenWritten = new LongData(0);
	public LongData S6519protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6519ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(numpolsOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(stmtdateOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stmtlevelOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, cownnum, ownername, jowner, jownername, payer, payername, servagnt, servagnam, servbr, brchname, currfrom, numpols, lststmdte, ustmno, stmtdate, stmtlevel};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, cownnumOut, ownernameOut, jownerOut, jownernameOut, payerOut, payernameOut, servagntOut, servagnamOut, servbrOut, brchlnameOut, currfromOut, numpolsOut, lststmdteOut, ustmnoOut, stmtdateOut, stmtlevelOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, cownnumErr, ownernameErr, jownerErr, jownernameErr, payerErr, payernameErr, servagntErr, servagnamErr, servbrErr, brchlnameErr, currfromErr, numpolsErr, lststmdteErr, ustmnoErr, stmtdateErr, stmtlevelErr};
		screenDateFields = new BaseData[] {currfrom, lststmdte, stmtdate};
		screenDateErrFields = new BaseData[] {currfromErr, lststmdteErr, stmtdateErr};
		screenDateDispFields = new BaseData[] {currfromDisp, lststmdteDisp, stmtdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6519screen.class;
		protectRecord = S6519protect.class;
	}

}
