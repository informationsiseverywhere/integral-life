/*
 * File: C6508.java
 * Date: 30 August 2009 2:58:56
 * Author: $Id$
 * 
 * Class transformed from C6508.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.unitlinkedprocessing.batchprograms.B6508;
import com.csc.smart.procedures.Passparm;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C6508 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData conjp = new FixedLengthStringData(860);
	private FixedLengthStringData effdate = new FixedLengthStringData(8);
	private FixedLengthStringData jobno = new FixedLengthStringData(8);
	private FixedLengthStringData stmtyp = new FixedLengthStringData(1);

	public C6508() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conjp});
					effdate.set(subString(conjp, 1, 8));
					jobno.set(subString(conjp, 9, 8));
					stmtyp.set(subString(conjp, 17, 1));
					/* CALLPROG*/
					callProgram(B6508.class, new Object[] {params});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
