package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5533screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5533ScreenVars sv = (S5533ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5533screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5533ScreenVars screenVars = (S5533ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.frequency01.setClassString("");
		screenVars.cmin01.setClassString("");
		screenVars.cmax01.setClassString("");
		screenVars.frequency02.setClassString("");
		screenVars.cmin02.setClassString("");
		screenVars.cmax02.setClassString("");
		screenVars.frequency03.setClassString("");
		screenVars.cmin03.setClassString("");
		screenVars.cmax03.setClassString("");
		screenVars.frequency04.setClassString("");
		screenVars.cmin04.setClassString("");
		screenVars.cmax04.setClassString("");
		screenVars.frequency05.setClassString("");
		screenVars.cmin05.setClassString("");
		screenVars.cmax05.setClassString("");
		screenVars.frequency06.setClassString("");
		screenVars.cmin06.setClassString("");
		screenVars.cmax06.setClassString("");
		screenVars.frequency07.setClassString("");
		screenVars.cmin07.setClassString("");
		screenVars.cmax07.setClassString("");
		screenVars.frequency08.setClassString("");
		screenVars.cmin08.setClassString("");
		screenVars.cmax08.setClassString("");
		screenVars.casualContribMin.setClassString("");
		screenVars.casualContribMax.setClassString("");
		screenVars.rndfact.setClassString("");
		screenVars.premUnit.setClassString("");
		screenVars.cIncrmax01.setClassString("");
		screenVars.cIncrmax02.setClassString("");
		screenVars.cIncrmax03.setClassString("");
		screenVars.cIncrmax04.setClassString("");
		screenVars.cIncrmax05.setClassString("");
		screenVars.cIncrmax06.setClassString("");
		screenVars.cIncrmax07.setClassString("");
		screenVars.cIncrmax08.setClassString("");
		screenVars.cIncrmin01.setClassString("");
		screenVars.cIncrmin02.setClassString("");
		screenVars.cIncrmin03.setClassString("");
		screenVars.cIncrmin04.setClassString("");
		screenVars.cIncrmin05.setClassString("");
		screenVars.cIncrmin06.setClassString("");
		screenVars.cIncrmin07.setClassString("");
		screenVars.cIncrmin08.setClassString("");
	}

/**
 * Clear all the variables in S5533screen
 */
	public static void clear(VarModel pv) {
		S5533ScreenVars screenVars = (S5533ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.frequency01.clear();
		screenVars.cmin01.clear();
		screenVars.cmax01.clear();
		screenVars.frequency02.clear();
		screenVars.cmin02.clear();
		screenVars.cmax02.clear();
		screenVars.frequency03.clear();
		screenVars.cmin03.clear();
		screenVars.cmax03.clear();
		screenVars.frequency04.clear();
		screenVars.cmin04.clear();
		screenVars.cmax04.clear();
		screenVars.frequency05.clear();
		screenVars.cmin05.clear();
		screenVars.cmax05.clear();
		screenVars.frequency06.clear();
		screenVars.cmin06.clear();
		screenVars.cmax06.clear();
		screenVars.frequency07.clear();
		screenVars.cmin07.clear();
		screenVars.cmax07.clear();
		screenVars.frequency08.clear();
		screenVars.cmin08.clear();
		screenVars.cmax08.clear();
		screenVars.casualContribMin.clear();
		screenVars.casualContribMax.clear();
		screenVars.rndfact.clear();
		screenVars.premUnit.clear();
		screenVars.cIncrmax01.clear();
		screenVars.cIncrmax02.clear();
		screenVars.cIncrmax03.clear();
		screenVars.cIncrmax04.clear();
		screenVars.cIncrmax05.clear();
		screenVars.cIncrmax06.clear();
		screenVars.cIncrmax07.clear();
		screenVars.cIncrmax08.clear();
		screenVars.cIncrmin01.clear();
		screenVars.cIncrmin02.clear();
		screenVars.cIncrmin03.clear();
		screenVars.cIncrmin04.clear();
		screenVars.cIncrmin05.clear();
		screenVars.cIncrmin06.clear();
		screenVars.cIncrmin07.clear();
		screenVars.cIncrmin08.clear();
	}
}
