package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:58
 * Description:
 * Copybook name: T6655REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6655rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6655Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData divisor = new ZonedDecimalData(6, 0).isAPartOf(t6655Rec, 0);
  	public FixedLengthStringData svpFactors = new FixedLengthStringData(250).isAPartOf(t6655Rec, 6);
  	public ZonedDecimalData[] svpFactor = ZDArrayPartOfStructure(50, 5, 0, svpFactors, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(250).isAPartOf(svpFactors, 0, FILLER_REDEFINE);
  	public ZonedDecimalData svpFactor01 = new ZonedDecimalData(5, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData svpFactor02 = new ZonedDecimalData(5, 0).isAPartOf(filler, 5);
  	public ZonedDecimalData svpFactor03 = new ZonedDecimalData(5, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData svpFactor04 = new ZonedDecimalData(5, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData svpFactor05 = new ZonedDecimalData(5, 0).isAPartOf(filler, 20);
  	public ZonedDecimalData svpFactor06 = new ZonedDecimalData(5, 0).isAPartOf(filler, 25);
  	public ZonedDecimalData svpFactor07 = new ZonedDecimalData(5, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData svpFactor08 = new ZonedDecimalData(5, 0).isAPartOf(filler, 35);
  	public ZonedDecimalData svpFactor09 = new ZonedDecimalData(5, 0).isAPartOf(filler, 40);
  	public ZonedDecimalData svpFactor10 = new ZonedDecimalData(5, 0).isAPartOf(filler, 45);
  	public ZonedDecimalData svpFactor11 = new ZonedDecimalData(5, 0).isAPartOf(filler, 50);
  	public ZonedDecimalData svpFactor12 = new ZonedDecimalData(5, 0).isAPartOf(filler, 55);
  	public ZonedDecimalData svpFactor13 = new ZonedDecimalData(5, 0).isAPartOf(filler, 60);
  	public ZonedDecimalData svpFactor14 = new ZonedDecimalData(5, 0).isAPartOf(filler, 65);
  	public ZonedDecimalData svpFactor15 = new ZonedDecimalData(5, 0).isAPartOf(filler, 70);
  	public ZonedDecimalData svpFactor16 = new ZonedDecimalData(5, 0).isAPartOf(filler, 75);
  	public ZonedDecimalData svpFactor17 = new ZonedDecimalData(5, 0).isAPartOf(filler, 80);
  	public ZonedDecimalData svpFactor18 = new ZonedDecimalData(5, 0).isAPartOf(filler, 85);
  	public ZonedDecimalData svpFactor19 = new ZonedDecimalData(5, 0).isAPartOf(filler, 90);
  	public ZonedDecimalData svpFactor20 = new ZonedDecimalData(5, 0).isAPartOf(filler, 95);
  	public ZonedDecimalData svpFactor21 = new ZonedDecimalData(5, 0).isAPartOf(filler, 100);
  	public ZonedDecimalData svpFactor22 = new ZonedDecimalData(5, 0).isAPartOf(filler, 105);
  	public ZonedDecimalData svpFactor23 = new ZonedDecimalData(5, 0).isAPartOf(filler, 110);
  	public ZonedDecimalData svpFactor24 = new ZonedDecimalData(5, 0).isAPartOf(filler, 115);
  	public ZonedDecimalData svpFactor25 = new ZonedDecimalData(5, 0).isAPartOf(filler, 120);
  	public ZonedDecimalData svpFactor26 = new ZonedDecimalData(5, 0).isAPartOf(filler, 125);
  	public ZonedDecimalData svpFactor27 = new ZonedDecimalData(5, 0).isAPartOf(filler, 130);
  	public ZonedDecimalData svpFactor28 = new ZonedDecimalData(5, 0).isAPartOf(filler, 135);
  	public ZonedDecimalData svpFactor29 = new ZonedDecimalData(5, 0).isAPartOf(filler, 140);
  	public ZonedDecimalData svpFactor30 = new ZonedDecimalData(5, 0).isAPartOf(filler, 145);
  	public ZonedDecimalData svpFactor31 = new ZonedDecimalData(5, 0).isAPartOf(filler, 150);
  	public ZonedDecimalData svpFactor32 = new ZonedDecimalData(5, 0).isAPartOf(filler, 155);
  	public ZonedDecimalData svpFactor33 = new ZonedDecimalData(5, 0).isAPartOf(filler, 160);
  	public ZonedDecimalData svpFactor34 = new ZonedDecimalData(5, 0).isAPartOf(filler, 165);
  	public ZonedDecimalData svpFactor35 = new ZonedDecimalData(5, 0).isAPartOf(filler, 170);
  	public ZonedDecimalData svpFactor36 = new ZonedDecimalData(5, 0).isAPartOf(filler, 175);
  	public ZonedDecimalData svpFactor37 = new ZonedDecimalData(5, 0).isAPartOf(filler, 180);
  	public ZonedDecimalData svpFactor38 = new ZonedDecimalData(5, 0).isAPartOf(filler, 185);
  	public ZonedDecimalData svpFactor39 = new ZonedDecimalData(5, 0).isAPartOf(filler, 190);
  	public ZonedDecimalData svpFactor40 = new ZonedDecimalData(5, 0).isAPartOf(filler, 195);
  	public ZonedDecimalData svpFactor41 = new ZonedDecimalData(5, 0).isAPartOf(filler, 200);
  	public ZonedDecimalData svpFactor42 = new ZonedDecimalData(5, 0).isAPartOf(filler, 205);
  	public ZonedDecimalData svpFactor43 = new ZonedDecimalData(5, 0).isAPartOf(filler, 210);
  	public ZonedDecimalData svpFactor44 = new ZonedDecimalData(5, 0).isAPartOf(filler, 215);
  	public ZonedDecimalData svpFactor45 = new ZonedDecimalData(5, 0).isAPartOf(filler, 220);
  	public ZonedDecimalData svpFactor46 = new ZonedDecimalData(5, 0).isAPartOf(filler, 225);
  	public ZonedDecimalData svpFactor47 = new ZonedDecimalData(5, 0).isAPartOf(filler, 230);
  	public ZonedDecimalData svpFactor48 = new ZonedDecimalData(5, 0).isAPartOf(filler, 235);
  	public ZonedDecimalData svpFactor49 = new ZonedDecimalData(5, 0).isAPartOf(filler, 240);
  	public ZonedDecimalData svpFactor50 = new ZonedDecimalData(5, 0).isAPartOf(filler, 245);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(244).isAPartOf(t6655Rec, 256, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6655Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6655Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}