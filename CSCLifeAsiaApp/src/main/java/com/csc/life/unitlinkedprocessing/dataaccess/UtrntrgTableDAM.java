package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrntrgTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:19
 * Class transformed from UTRNTRG.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrntrgTableDAM extends UtrnpfTableDAM {

	public UtrntrgTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UTRNTRG");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "JCTLJNUMPR, " +
		            "FDBKIND, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               jobnoPrice,
                               feedbackInd,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(52);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(97);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getJobnoPrice().toInternal()
					+ getFeedbackInd().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, jobnoPrice);
			what = ExternalData.chop(what, feedbackInd);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public PackedDecimalData getJobnoPrice() {
		return jobnoPrice;
	}
	public void setJobnoPrice(Object what) {
		setJobnoPrice(what, false);
	}
	public void setJobnoPrice(Object what, boolean rounded) {
		if (rounded)
			jobnoPrice.setRounded(what);
		else
			jobnoPrice.set(what);
	}	
	public FixedLengthStringData getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(Object what) {
		feedbackInd.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		jobnoPrice.clear();
		feedbackInd.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}