package com.csc.life.unitlinkedprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UstfpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:50
 * Class transformed from USTFPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UstfpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 198;
	public FixedLengthStringData ustfrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ustfpfRecord = ustfrec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(ustfrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ustfrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ustfrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ustfrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ustfrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ustfrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(ustfrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtOpDate = DD.stopdt.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtOpUnits = DD.stopun.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtOpDunits = DD.stopdun.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtClDate = DD.stcldt.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtClUnits = DD.stclun.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtClDunits = DD.stcldun.copy().isAPartOf(ustfrec);
	public PackedDecimalData strpdate = DD.strpdate.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtTrnUnits = DD.sttrun.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtTrnDunits = DD.sttrdun.copy().isAPartOf(ustfrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ustfrec);
	public FixedLengthStringData unitStmtFlag = DD.ustmtflag.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtOpFundCash = DD.stopfuca.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtOpContCash = DD.stopcoca.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtClFundCash = DD.stclfuca.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtClContCash = DD.stclcoca.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtTrFundCash = DD.sttrfuca.copy().isAPartOf(ustfrec);
	public PackedDecimalData stmtTrContCash = DD.sttrcoca.copy().isAPartOf(ustfrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(ustfrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ustfrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ustfrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ustfrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UstfpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UstfpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UstfpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UstfpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UstfpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UstfpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UstfpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("USTFPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRPFX, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"USTMNO, " +
							"VRTFND, " +
							"STOPDT, " +
							"STOPUN, " +
							"STOPDUN, " +
							"STCLDT, " +
							"STCLUN, " +
							"STCLDUN, " +
							"STRPDATE, " +
							"STTRUN, " +
							"STTRDUN, " +
							"PLNSFX, " +
							"USTMTFLAG, " +
							"STOPFUCA, " +
							"STOPCOCA, " +
							"STCLFUCA, " +
							"STCLCOCA, " +
							"STTRFUCA, " +
							"STTRCOCA, " +
							"UNITYP, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrpfx,
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     ustmno,
                                     unitVirtualFund,
                                     stmtOpDate,
                                     stmtOpUnits,
                                     stmtOpDunits,
                                     stmtClDate,
                                     stmtClUnits,
                                     stmtClDunits,
                                     strpdate,
                                     stmtTrnUnits,
                                     stmtTrnDunits,
                                     planSuffix,
                                     unitStmtFlag,
                                     stmtOpFundCash,
                                     stmtOpContCash,
                                     stmtClFundCash,
                                     stmtClContCash,
                                     stmtTrFundCash,
                                     stmtTrContCash,
                                     unitType,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrpfx.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		ustmno.clear();
  		unitVirtualFund.clear();
  		stmtOpDate.clear();
  		stmtOpUnits.clear();
  		stmtOpDunits.clear();
  		stmtClDate.clear();
  		stmtClUnits.clear();
  		stmtClDunits.clear();
  		strpdate.clear();
  		stmtTrnUnits.clear();
  		stmtTrnDunits.clear();
  		planSuffix.clear();
  		unitStmtFlag.clear();
  		stmtOpFundCash.clear();
  		stmtOpContCash.clear();
  		stmtClFundCash.clear();
  		stmtClContCash.clear();
  		stmtTrFundCash.clear();
  		stmtTrContCash.clear();
  		unitType.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUstfrec() {
  		return ustfrec;
	}

	public FixedLengthStringData getUstfpfRecord() {
  		return ustfpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUstfrec(what);
	}

	public void setUstfrec(Object what) {
  		this.ustfrec.set(what);
	}

	public void setUstfpfRecord(Object what) {
  		this.ustfpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ustfrec.getLength());
		result.set(ustfrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}