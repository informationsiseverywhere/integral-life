/*
 * File: Zruliss.java
 * Date: 30 August 2009 2:56:55
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRULISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg; //IBPLIFE-6312
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.terminationclaims.recordstructures.Untallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5535rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      UNIT LINKED GENERIC ISSUE SUBROUTINE
*
* OVERVIEW.
* ---------
*
* This program replaces UNLISS.
*
* This generic subroutine allocates units (premium) to a component
* at contract issue stage. It is identical to the subroutine
* UNLISS except that UTRN records are created at Coverage
* level, (even if the unit holding is applied to a Rider).
* The UTRN Sequence Number is used to differentiate between units
* allocated to Coverages & Riders.
* This change is required to bring unit allocation at Contract Issue
* and Premium Collection stages into line with Benefit Billing,
* where units are allocated at Coverage level, even when the benefit
* component is a Rider.
*
* T5671 has also been incorporated to facilitate calling the unit
* allocation subroutine generically.
*
* ZRULISS is the generic  subroutine  which  will  be  called  to
* perform the processing  necessary  on  the  issue  of  a  Unit
* Linked generic component.  It  will  be  called   (via  T5671,
* Coverage/Rider Switching) from within an AT module  for   each
* Unit Linked component found on a proposal being issued.
*
* ZRULISS will handle Coverage/Riders created at  proposal  issue
* and any new components added via automatic increases.
*
* It will perform the following functions:
*
* 1. Convert the UNLTUNL records to ULNKUNL records  and  delete
* UNLTUNL records afterwards.  (Note  that  'invest  by  amount'
* records will be converted into  'invest by percentage' values,
* even if the percentage amount  indicator on the UNLT is set to
* 'A'.)
*
* 2. Write an INCI record, (not for single premium only)
*
* 3. Write unit Fund transaction records:
*      UTRN record for the uninvested portion of the premium,
*      UTRN record for the Initial Units,
*      UTRN record for the Accumulation Units,
*
* 4. Update the Coverage/Rider record via COVRUNL.
*
* PROCESSING.
* -----------
*
* The processing takes place in five phases:
*
*      1. Obtain Existing Information.
*      2. Convert Fund Direction Transaction (UNLT) records to
*      ULNK records.
*      3. Create INCI (Increase) records.
*      4. Allocation - create UTRN records.
*      (This will be done by calling the subroutine ZRULALOC in
*      the case of regular premium contracts.)
*      5. Update the newly created Coverage/Rider record with
*      relevant data fields.
*
* 1. Obtain Existing Information.
* -------------------------------
*
* Read the Contract Header using  the  RETRV  function  and  the
* logical view CHDRLNB. This will  return  the  Contract  Header
* details for the component being processed.
*
* Read the Coverage/Rider record using the  key  passed  in  the
* linkage area,  a  function  of  READH  and  the  logical  view
* COVRUNL.
*
* The following dated tables are used by this program:
*
* T5687 - read using  the  Coverage/Rider code (COVR-CRTABLE) as
* key.
*
* T5540 - read using the Coverage/Rider code (as above).
*
* T5519 - if the Initial  Unit  Discount  Basis  from  T5540  is
* non-blank then use it to read T5519.
*
* T5535 - if the Loyalty/Persistency code  (LTYPST)  from  T5540
* is non-blank then use it to read T5535.
*
* Read T6647 with the Transaction Code (passed  in  the linkage)
* and CHDR-Contract Type as a key.   This  is  the  Unit  Linked
* Contract Details table and holds the  rules  specific  to that
* product and transaction.
*
* Read T5537 with a key of allocation basis  (from  T5540),  Sex
* and transaction code. If the full key is not found,  then  '*'
* is substituted for sex and the  table  read  again.  If  still
* unsuccessful, Allocation Basis,  Sex  and Transaction Code set
* as '****' is searched for. If still  unsuccessful,  Allocation
* Basis, Sex as '*' and Transaction code as '****'  is  searched
* for. If still unsuccessful, '********' is searched  for.  This
* table holds a matrix of  Allocation  Basis  codes  held  in  a
* two-dimensional table with  Age as the vertical ('y') axis and
* Term as the horizontal ('x')  axis.  Locate  the occurrence of
* Age that is greater than or equal to the  Age  Next  Birthday.
* Locate the occurrence of Term that is greater  than  or  equal
* to the Term. Locate the appropriate method.
* Move the Item Code at  this  position  to  a  working  storage
* variable for Allocation Basis  (WSAA-ALLOC-BASIS)  for  future
* use.
*
* Read T5536 with a key of  the  Allocation  Basis  just  found.
* Find the Billing Frequency  on  the  table  that  matches  the
* billing frequency from  the  contract  header  (BILLFREQ)  and
* move   its  associated  group  of  three  items:  MAX-PERIODS,
* PC-UNITS  and PC-INIT-UNITS to store for future use. Calculate
* PC-ACCUM-UNITS  as  (100 - PC-INIT-UNITS),  i.e.  if  not 100%
* allocated   to   initial   units,   the   remainder    becomes
* accumulation units.
*
* 2. Normal Processing.
* ---------------------
*
* This section deals  with  the  conversion  of  UNLT  to   ULNK
* records.  If  plan  processing  is  applicable  (and  this  is
* typically  only  the  case   in  UK  sites),   the   following
* information is of relevance, otherwise, go to Section 3.
*
* Plan Processing.
* ----------------
*
* When the COVTUNL records are  converted  into  COVR records by
* the Proposal Issue suite, one COVR record will  be created for
* each policy within the Plan except for the  first   group   of
* identical policies where one COVR record will  be created that
* represents a group of policies  that  are  identical,  i.e.  a
* summarised record. The  number  in  this  group  will  be  the
* smallest group represented  by  the  first  COVTUNL record for
* each Coverage/Rider. All other groups  will  be  'broken  out'
* into   individual   COVR  records.    The  following   example
* illustrates this.
*
*   Number of Policies in Plan = 10
*
*   Coverage #1, 3 COVTUNL records in groups of 5
*                                               3
*                                               2
*
*   Coverage #2, 2 COVTUNL records in groups of 3
*                                               7
* When the COVTUNL records are converted  into COVR records, the
* system will look at the first group of  definitions  for  each
* Coverage/Rider component (marked with  a  double  asterisk  in
* the above example) and select the  smallest  group -  in  this
* case 3. However,  the  COVTUNL  record  for  Coverage/Rider #1
* represents 5 policies.  This  will  have  to be converted to 3
* COVR records - #1 representing 3/5 of its  policies and #2 and
* #3 representing 1/5  each.   All  other  COVTUNL  records  for
* COVTUNL #1 will be  'broken out'  fully giving one COVR record
* per policy represented.
*
* The Plan Suffix will be '0000' on  the first COVR record as it
* represents a group of policies. The  other  COVR  records will
* have their  'Plan Suffixes'  allocated  sequentially  starting
* from a number  that allows for all the policies on the summary
* group and going  up  to  a  number equivalent to the number of
* policies in the plan.
*
* The following  example  shows  the  previous  COVTUNL  records
* converted to COVR  records.  The  largest  number  that can be
* summarised by a Coverage/Rider group for  the  whole  Plan  is
* '3', so the first group on Coverage/Rider #1  has to be broken
* down itself.
*
*   COV/RIDER      COVTUNL      COVR          PLAN SUFFIX
*                    No.
*                 Applicable
*
*      01             5          3/5              0000
*                                1/5              0004
*                                1/5              0005
*                     3          1/3              0006
*                                1/3              0007
*                                1/3              0008
*                     2          1/2              0009
*                                1/2              0010
*   - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*      02             3          3/3              0000
*                     7          1/7              0004
*                                1/7              0005
*                                1/7              0006
*                                1/7              0007
*                                1/7              0008
*                                1/7              0009
*                                1/7              0010
*
* The UNLTUNL records will be converted as follows:
*
* The ULNK records will mirror exactly the COVR  records created
* by  the  Proposal  Issue  suite.   The   group   of   policies
* represented  by  the  first  ULNK  record will be the smallest
* group for any Coverage/Rider component  defined  for the Plan.
* Note that this may be less than the group represented  by  the
* first COVTUNL and UNLTUNL record as in the above example.
*
* The number summarised by the first COVR record  will  be  held
* on the Contract Header record. This will tell the  program how
* many policies are to be summarised by the first  ULNK  record.
* All other UNLTUNL records will be broken down giving  one ULNK
* record per policy  and  each  having  its  own   'Plan Suffix'
* number. This will be  done  by  one  pass  through the UNLTUNL
* data-set on the first call  for  a  Coverage/Rider  component.
* The program maintains a flag  that  lets it know whether it is
* on its first pass or on subsequent passes.
*
* In the following example, it can  be  seen  that  the  UNLTUNL
* records first matched the COVTUNL  records  one  for one. When
* this program runs, the COVTUNL records will  already have been
* converted  into  the  COVR  records  shown  with  their  'Plan
* Suffixes'  in their keys. By looking at the Contract Header to
* see how many  policies  are  being  summarised,   it  will  be
* possible   to   convert  all  the  UNLTUNL  records  for  THIS
* Coverage/Rider component  in  one  pass.   Subsequent calls to
* this module for the SAME Coverage/Rider  component  will cause
* the program to read the corresponding ULNK record  created  on
* the first pass. The 'Plan Suffix' may be used to identify  the
* correct ULNK record.
*
* Note: this subroutine will be  called  once  for  every   COVR
* record created by the Proposal Issue suite.
*
*   COV/     COVTUNL --> COVR    PLAN   UNLTUNL --> ULNK   PLAN
*   RIDER      No.              SUFFIX    No.             SUFFIX
*           Applicable                 Applicable
*
*    01         5         3/5    0000      5        3/5    0000
*                         1/5    0004               1/5    0004
*                         1/5    0005               1/5    0005
*               3         1/3    0006      3        1/3    0006
*                         1/3    0007               1/3    0007
*                         1/3    0008               1/3    0008
*               2         1/2    0009      2        1/2    0009
*                         1/2    0010               1/2    0010
*   - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - -
*    02         3         3/3    0000      3        3/3    0000
*               7         1/7    0004      7        1/7    0004
*                         1/7    0005               1/7    0005
*                         1/7    0006               1/7    0006
*                         1/7    0007               1/7    0007
*                         1/7    0008               1/7    0008
*                         1/7    0009               1/7    0009
*                         1/7    0010               1/7    0010
*
* 3. Issue - Create INCI Record.
* ------------------------------
*
* Set up the fields for creating the  INCI  record  (Increases).
* The sources of the fields are:
*
*  Company Number      -  Passed in Linkage
*  Contract Number     -    "     "    "
*  Life                -    "     "    "
*  Coverage            -    "     "    "
*  Rider               -    "     "    "
*  Plan Suffix         - Calculated as described previously
*  Validflag           - '1'
*  Dormant Flag        - 'N'
*  INCI Number         - 001
*  TRANNO              - Passed in Linkage
*  RCDATE              - Contract Header
*  PREM-CESS-DATE      - Coverage/Rider
*  CRTABLE             -     "      "
*  ORIG-PREM           - COVRUNL-INSTPREM
*  CURR-PREM           - This either moves INSTPREM or
*                        WSAA-PREMIUM-PAID which is derived
*                        earlier by multiplying the INSTPREM by
*                        the FREQ-FACTOR.
*
* INCI Calculations.
* ------------------
*
* Other fields are calculated as follows:
*
* Here  we  are  first concerned with a group of fields on  the
* INCI record that occur in small groups of four:  INCI-PCUNIT,
* INCI-USPLITPC, INCI-PREMST and INCI-PREMCURR.
*
* Set INCI-PREMCURR  and  INCI-PREMST  to  the   Premium  No. of
* periods applicable from T5536.
*
* Set INCI-PCUNIT to the PCUINT from T5536.
*
* Set INCI-USPLITPC to PC-INIT-UNITS.
*
* The calling program  will  pass  as  one of its parameters the
* 'Premium   Available'.   This  may  differ  from  the   actual
* Instalment  Premium on the Coverage/Rider record.In most cases
* it will be  the  same  as the coverage although some contracts
* may be issued if there  is  no  money  actually  available  in
* suspense at the time of issue. If the  'Premium Available'  is
* zero   then   the   allocation  processing,   including  these
* calculations, should be skipped  entirely.  If  this  is   the
* case, then write the  INCI  record  now.   If  performing  the
* allocation processing,  then the 'Premium Available' should be
* used in case it differs from the Instalment Premium.
*
* First find the period with  which  we  are going to work. This
* will be the  first  of  the  INCI-PREMCURR  fields   that   is
* non-zero.
*
* If the split  for  the  period  is zero, then there is no more
* allocation to be done. Check   INCI-PCUNIT  and  INCI-USPLITPC
* and if both  are  zero  then   finish  with   the   allocation
* processing.
*
* Default the   enhancement  percentage  (PC-ENHANCE)   and life
* enhancement (LIFE-ENHANCE) to 100. If the Enhanced  Allocation
* code from T6647 (key = Contract Type)   is  not  spaces,  then
* read   T5545  with  the  Transaction  Code  and  the  Enhanced
* Allocation  Code  as the key. Match the Billing Frequency with
* that on T5545 and  take the corresponding Enhanced Percentages
* and Enhanced Premiums  group.   Match  the  Instalment Premium
* against the list to see if it is greater than  or equal to any
* of  the  Enhanced   Premiums.   If  it  is   then   take   the
* corresponding  Enhanced  Percentage  as  PC-ENHANCE  (Enhanced
* Percentage) to be used in later calculations.
*
* If the amount  of  the  Premium  Available  covers  the  first
* period and into  the  next  period, first allocate the premium
* to the current period and at  the  end  allocate the remainder
* to the next period.
*
* Set NET-PREMIUM to PREMIUM-AVAILABLE.
*
* If NET-PREMIUM is greater  than  INCI-PREMCURR  then  subtract
* INCI-PREMCURR  from  NET-PREMIUM  and  set  the  remainder  as
* NEXT-PERIOD and set NET-PREMIUM to INCI-PREMCURR.
*
* Calculate Current Period.
*
* Obtain the % Initial  Units  (PC-INIT-UNITS)  - from T5536 and
* the calculated %  Accumulation  units  (PC-ACCUM-UNITS).  Note
* that these figures  are still held as integers and are not yet
* held as percentage  multipliers.   To  obtain  these  figures,
* perform the  following  calculations.   The  receiving  fields
* should be defined as S9(03)V9(07) COMP-3.
*
* Divide  PC-UNITS, PC-INIT-UNITS, PC-ACCUM-UNITS and PC-ENHANCE
* by 100 rounding the answer.
*
* Calculate the number of UNITS (rounded) as PC-UNITS *
* PC-ENHANCE.
*
* Subtract the number of UNITS from 1 giving the UNINVESTED.
*
* Calculate the INIT-UNITS (rounded)        as PC-UNITS
*                                            * PC-INIT-UNITS
*                                            * PC-ENHANCE.
*
* Calculate the ACCUM-UNITS (rounded)       as PC-UNITS
*                                            * PC-ACCUM-UNITS
*                                            * PC-ENHANCE.
*
* Calculate the UNITS-VALUE (rounded)       as PC-UNITS
*                                            * NET-PREMIUM.
*
* Calculate the UNINVESTED-VALUE (rounded)  as UNINVESTED
*                                            * NET-PREMIUM.
*
* Calculate the INIT-UNITS-VALUE (rounded)  as INIT-UNITS
*                                            * NET-PREMIUM.
*
* Calculate the ACCUM-UNITS-VALUE (rounded) as ACCUM-UNITS
*                                            * NET-PREMIUM.
*
* If the  NEXT-PERIOD  is  greater  than zero, then perform  the
* calculations again for the next period using  the  NEXT-PERIOD
* as the new NET-PREMIUM.
*
* If the NEXT-PERIOD is not zero, subtract the second
* NET-PREMIUM from the second INCI-PREMCURR.
*
* Subtract the first NET-PREMIUM from the first INCI-PREMCURR.
*
* Write the INCI record.
*
* 4. Allocation - Create UTRN records.
* ------------------------------------
*
* Allocation of units  and  the  writing  of  UTRN  records  for
* Regular and Single premiums are dealt with separately.  Single
* premiums are allocated within this  module,  regular  premiums
* are handled by  calling  ZRULALOC.  To  cater  for  components
* which   have   a  Lump  Sum  paid  in  at  New  Business,  the
* appropriate  sections are only performed if the premium amount
* in question is non-zero.
*
* Monies Date - Unit Allocation Date.
* -----------------------------------
*
* The Monies Date,  the  effective  date  for  Unit  Allocation,
* should be calculated. The code returned  from  T6647  will  be
* either 'BD' - Business Date, 'DD' - Due Date,  which for Issue
* is the Risk Commencement Date or 'LO' -  Later  Of  the  above
* two. This will determine which  date  is to  be  used  as  the
* Monies Date by  Unit  Allocation.  If  it  is  'BD'  then  the
* current system  date will be used, if it is 'DD' then the Risk
* Commencement Date  will  be  used.   If  it  is  'LO' then the
* greater of the above two dates will be  used.   However,  this
* will be overridden if the user has entered  a  'Reserve  Date'
* which will be stored on the COVR record. If the  Reserve  Date
* from COVR is non zero then use this as the Monies Date.
*
* The allocation of units  will  be  on  either  a  'Now'  or  a
* 'Deferred'  basis.    This  depends  on  the  value   of   the
* 'Allocation'   field  from  T6647.    If  this  is  'D',  then
* allocation is  deferred, and if this is 'N' then allocation is
* immediate.
*
* Allocation.
* -----------
*
* First determine  the fund split by reading the ULNKUNL record.
* The correct ULNKUNL  record will be obtained by using the same
* key as is on the corresponding COVR record being processed.
*
* The following Allocation  Processing  will  be  performed once
* for the current period and possibly again if a  second  set of
* details was set up in the INCI record due to the  presence  of
* a greater premium than was required.
*
* A UTRN record will be created for the  uninvested  portion  of
* the premium. Note that there may be two sets of  INCI  details
* on the INCI record if there  is  enough  premium  to  allocate
* into the next period. If so  then  TWO  UTRN  records  will be
* created.The usual key and transaction  data  will  be  set  up
* plus the following:
*
*  UTRN-RLDGCOY - Company from CHDR
*  UTRN-RLDGACCT  - Contract Number from CHDR
*  UTRN-JOB-NAME-ORIG - spaces
*  UTRN-JOBNO-ORIG  - zero
*  UTRN-JOBNO-EXTRACT - zero
*  UTRN-JOBNO-UPDATE- zero
*  UTRN-JOBNO-PRICE - zero
*  UTRN-STAT-FUND - from COVRUNL
*  UTRN-STAT-SECT -   "     "
*  UTRN-STAT-SUBSECT- "     "
*  UTRN-UNIT-VIRTUAL-FUND - spaces
*  UTRN-UNIT-SUB-ACCOUNT- spaces
*  UTRN-EFFDATE - Risk Commencement Date
*  UTRN-STRPDATE  - zero
*  UTRN-NOW-DEFER-IND - 'N' - Now or 'D' -Deferred
*  UTRN-UNIT-TYPE - spaces
*  UTRN-NOF-UNITS - zero
*  UTRN-NOF-DUNITS  - zero
*  UTRN-UL-AMOUNT - Uninvested Amount from INCI record
*  UTRN-MONIES-DATE - The previously determined Monies Date
*  UTRN-PRICE-DATE-USED - Risk Commencement Date
*  UTRN-PRICE-USED  - zero
*  UTRN-BARE-PRICE  - zero
*  UTRN-CRTABLE - from COVRUNL
*  UTRN-CNTCURR - Contract Currency from CHDR
*  UTRN-CSHD-EFFD-IND - spaces
*  UTRN-FEEDBACK-IND- spaces
*  UTRN-INCI-NUM  - INCI number from the INCI record
*  UTRN-INCI-PERD - INCI period, i.e. 1 or 2
*  UTRN-INCI-PREMIUM- Uninvested Amount from INCI record
*  UTRN-SURR-PENALTY-FACTOR - zero
*
* The following processing will be carried out for each fund  in
* the fund split as specified on the ULNKUNL record. There is  a
* maximum of ten funds on the ULNKUNL table. The  processing  is
* divided into two  very  similar  parts:  firstly  for  Initial
* Units and then secondly for Accumulation Units.
*
*      For the Pricing  Date  the  Monies  Date  is  used  if no
*      Reserve  Units  Date  is  held  on  the  COVRUNL  record,
*      otherwise the COVRUNL Reserve Units Date is used.
*
*      Set the UTRN-UNIT-SUB-ACCOUNT to 'INIT', for Initial
*      Units.
*
*      Calculate  the  basic  premium  (unenhanced) for the fund
*      split. It may  be  used  later  for  windbacks.   If  the
*      ULNKUNL record  indicates  to use a percentage, calculate
*      the following:
*
*      INCI-FUND-AMT (rounded) = UNIT-PC
*                              * Percent/Amount (from ULNKUNL)
*                              / 100.
*
*      If the ULNKUNL record  indicates  that an amount has been
*      entered and not a percentage do the following
*      calculation:
*
*      INCI-FUND-AMT (rounded) = UNIT-PC
*                              * Percent/Amount (from ULNKUNL).
*
*      This result is then rounded to  two  decimal  places   by
*      adding zero to it with  the  rounding  clause  and  using
*      UTRN-INCI-AMT as the receiving field defined as
*      S9(11)V9(02) COMP-3.
*
*      The result - UTRN-INCI-AMT -  is later placed on the UTRN
*      record.
*
*      The Enhanced Premium  Amount  is  calculated  as  follows
*      where percentage are concerned:
*
*      FUND-AMT rounded = TEMP-AMT
*                       * Percentage/Amount (from ULNKUNL)
*                       / 100
*
*      where   TEMP-AMT  is  either   the   INIT-UNITS-VAL    or
*      ACCUM-UNITS-VAL calculated earlier depending  on  whether
*      initial units or accumulation units are  being  processed
*      - and as follows where an amount is concerned:
*
*      FUND-AMT rounded = UNIT-PC
*                       * Percentage/Amount (from ULNKUNL).
*
*      This result is also then rounded to  two  decimal  places
*      by adding zero to it with the rounding clause  and  using
*      UTRN-AMT as the receiving field defined as S9(11)V9(02)
*      COMP-3.
*
*      The number of units on the actual money  amount  must  be
*      calculated to two decimal places and rounded, whereas  up
*      to now it has been handled with seven decimal places.
*
*      When creating each UTRN record  note  that  the  Contract
*      Currency may differ from the Fund  Currency  as  held  on
*      T5515. In this case the Price and Units held on the  UTRN
*      record will have to be altered to take this into account.
*
* A partial UTRN record will have the UTRN-JOBNO,  UTRN-PRICEDT,
* UTRN-UNITS, UTRN-DUNITS, JOBNO-PRICE, OFFER-PRICE and
* BARE-PRICE set to zero.
*
* 5. Coverage Calculations.
* -------------------------
*
* The total premium for the coverage will have  been  passed  in
* the Linkage Section.
*
* Certain dates are calculated and stored on the COVR as
* follows:
*
*   ANNIVERSARY PROCESSING DATE
*   CONVERT INITIAL UNITS DATE
*   REVIEW PROCESSING DATE
*   UNIT STATEMENT DATE
*   CPI-DATE
*   INIT-UNIT-CANC-DATE
*   EXTRA-ALLOC-DATE
*   INIT-UNIT-INCREASE-DATE
*
* The dates are originally  set  to  all  '9's  and   then   the
* following logic is performed  which may or may not alter them.
* Anniversary Processing  Date -   this  processing  is   always
* performed:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1   -  OCCDATE
*      DTC2-FREQUENCY    -  '01'
*      DTC2-FREQ-FACTOR  -  1
*
*      Call DATCON2 and use the returned date to call DATCON2  a
*      second time with the parameters set as follows:
*
*      DTC2-INT-DATE-1   -  DTC2-INT-DATE-2
*      DTC2-FREQUENCY    -  'DY'
*      DTC2-FREQ-FACTOR  -  -1
*
*      Call DATCON2 and use the returned date as the
*      Anniversary Processing Date.
*
* Convert Initial Units Date  -  this  processing  is  dependent
* upon the retrieved data from a  read  of  T5519.   Read  T5519
* using the Initial Unit Discount  Basis  as  a  key.   If   the
* returned TERM-TO-RUN is 'Y' then perform  the  'Term  to  Run'
* processing, otherwise if the  'Whole  of  Life'  is  'Y'  then
* perform the 'Whole  of  Life'  processing,  otherwise  if  the
* FIXDTRM is not zero  then perform the 'Fixed Term' processing.
* If none of these has  been  set, then there is an error - give
* error message number E686   and  perform  the   system   error
* processing.
*
*      TERM TO RUN - set the following fields for a call to
*      DATCON2:
*
*      DTC2-INT-DATE-1  -  Risk Cessation Date
*      DTC2-FREQUENCY   -  'DY'
*      DTC2-FREQ-FACTOR -  -1
*
*      Call DATCON2 and use the returned date as the Convert
*      Initial Units Date.
*
*      WHOLE OF LIFE - set the Convert Initial Units Date to
*      all '9's.
*
*      FIXED TERM - set the following fields for a call to
*      DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  Fixed Term from T5519
*
*      Call DATCON2 and use the returned date to call  DATCON2 a
*      second time with the parameters set as follows:
*
*      DTC2-INT-DATE-1  -  DTC2-INT-DATE-2
*      DTC2-FREQUENCY   -  'DY'
*      DTC2-FREQ-FACTOR -  -1
*
*      Call DATCON2 and use the returned date as the Convert
*      Initial Units Date.
*
* Review Processing Date - perform this processing only  if this
* field (RVWPROC) from the COVRUNL is non-zero.
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  RVWPROC
*
*      Call DATCON2 and use the returned date as the Review
*      Processing Date.
*
* Unit Statement Date - this processing is always performed:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  1
*
*      Call DATCON2 and use the returned date as the Unit
*      Statement Date.
*
* Initial Unit Increase Date -  If  the  Initial  Unit  Discount
* Basis  from  T5540  is  blank  then  skip   this   processing,
* otherwise:
*
*      If the Differing Price flag (from T5519) is  not  'Y'  OR
*      the With Unit Adjustment flag  (from T5519)  is  not  'Y'
*      then skip this processing,  otherwise   set  the  Initial
*      Unit Increase Date to the Anniversary Processing Date.
*
* Initial Unit Cancellation Date - If the Initial Unit  Discount
* Basis from T5540 is blank then skip this processing,
* otherwise:
*
*      If the Unit Deduction Flag from T5519  is  'Y'  then  set
*      the Initial Unit Cancellation  Date  as  the  Anniversary
*      Processing Date.
*
* Extra Allocation  Date  -  if  the  Loyalty/Persistency   code
* (LTYPST) is spaces then skip this processing, otherwise:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '12'
*      DTC2-FREQ-FACTOR -  'After Duration' (1) from T5535
*
*      Call DATCON2 and use the returned date as the Extra
*      Allocation Date.
*
* Any date not modified by the previous processing  is  left  as
* all '9's.
*
* Update the COVRUNL record.
*
* MODIFICATIONS.
* --------------
*
* Tables accessed and their keys:
*
* T5687 - key = Coverage/Rider code - CRTABLE (from COVRUNL)
* T5540 - key = Coverage/Rider code - CRTABLE (from COVRUNL)
* T6647 - key = Transaction Code + Contract Type (from CHDRLNB)
* T5537 - key = Basis Select(from T5540) + sex + Transaction
* Code
* T5536 - key = Allocation Basis  (from T5537)
* T5545 - key = Enhanced Allocation Code(from T6647)
* T5519 - key = Initial Unit Discount Basis (from T5540)
* T5533 - key = Loyalty/Persistency Code(from T5540)
* T5515 - key = Fund Code
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Cater for Interest Bearing Fund lump sum investment and create
* a new Interest Details record (HITD) for each new Interest
* bearing fund. Pass ISUA-EFFDATE (which was set as the component
* commencement date in P5074AT) to RNLA-EFFDATE when calling the
* unit allocation routine instead of today's date, so that the
* HITRs are written with with the correct date.
*
*****************************************************************
* </pre>
*/
public class Zruliss extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zruliss.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZRULISS";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5537Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBasisSelect = new FixedLengthStringData(3).isAPartOf(wsaaT5537Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Key, 3).init(SPACES);
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5537Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);

	private FixedLengthStringData wsaaT5671Edtitm = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaEdtitmCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Edtitm, 0);
	private FixedLengthStringData wsaaEdtitmFiller = new FixedLengthStringData(1).isAPartOf(wsaaT5671Edtitm, 4);
	private ZonedDecimalData wsaaOrigDuedate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOrigDuedate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillmonth = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaBillday = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaT5544Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaSwmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5544Key, 4);
	private FixedLengthStringData wsaaJlifeExists = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaLifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaJlifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaJlifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTref, 15);
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaA = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaB = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaRiderNum = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	protected String wsaaFirstUnltunl = "Y";
	private PackedDecimalData wsaaNofPeriods = new PackedDecimalData(5, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(0);
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaPremiumPaid = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaSingPrem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaEnhancePerc = new PackedDecimalData(5, 2).init(ZERO);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCallCount = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaEnhancedAllocInd = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaIdx = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5545Item, 4);

		/* WSAA-T5536-VALUES */
	private FixedLengthStringData wsaaMaxPeriods = new FixedLengthStringData(28); //16
	private ZonedDecimalData[] wsaaMaxPeriod = ZDArrayPartOfStructure(7, 4, 0, wsaaMaxPeriods, 0);

	private FixedLengthStringData wsaaPcUnits = new FixedLengthStringData(35); //20
	private ZonedDecimalData[] wsaaPcUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcUnits, 0);  

	private FixedLengthStringData wsaaPcInitUnits = new FixedLengthStringData(35); //20
	private ZonedDecimalData[] wsaaPcInitUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcInitUnits, 0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaTaxs = new FixedLengthStringData(34);
	private ZonedDecimalData[] wsaaTax = ZDArrayPartOfStructure(2, 17, 2, wsaaTaxs, 0);
	private PackedDecimalData wsaaTotCd = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaDateSplit = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaDateSplit, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaYearSplit = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaUalprcTot = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaUalprcRunTot = new ZonedDecimalData(17, 2);
		/* WSAA-UALPRCS */
	private ZonedDecimalData[] wsaaUalprc = ZDInittedArray(10, 17, 2);
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaFact = new PackedDecimalData(3, 0);
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private InciTableDAM inciIO = new InciTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	protected UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	protected Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Itemkey wsaaItemkey = new Itemkey();
	private T5687rec t5687rec = new T5687rec();
	private T6646rec t6646rec = new T6646rec();
	private T5539rec t5539rec = new T5539rec();
	private T5540rec t5540rec = new T5540rec();
	private T5544rec t5544rec = new T5544rec();
	protected T6647rec t6647rec = new T6647rec();
	private T5519rec t5519rec = new T5519rec();
	private T5537rec t5537rec = new T5537rec();
	private T5536rec t5536rec = new T5536rec();
	private T5545rec t5545rec = new T5545rec();
	private T5515rec t5515rec = new T5515rec();
	private T5535rec t5535rec = new T5535rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	protected T6659rec t6659rec = new T6659rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Th510rec th510rec = new Th510rec();
	protected Annprocrec annprocrec = new Annprocrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Isuallrec isuallrec = new Isuallrec();
	protected ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();
	private ExternalisedRules er = new ExternalisedRules();
	private static final String  FMC_FEATURE_ID = "BTPRO026"; //IBPLIFE-6312
	private boolean fmcOnFlag = false; //IBPLIFE-6312

/**
 * Contains all possible labels used by goTo action.
 */
	public enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont10450, 
		cont10460, 
		keepsChdrmja, 
		readItem10515, 
		cont10520, 
		yOrdinate10540, 
		xOrdinate10580, 
		endLoop10650, 
		exit10690, 
		nextRecord20100, 
		read20200, 
		exit40090, 
		extraAllocDate50400, 
		unitStmntDate50500, 
		updateCovrunl50700, 
		exit50090, 
		fundAllocation60050, 
		endLoop60150, 
		exit60190, 
		comlvlacc61925, 
		cont61950, 
		enhanb63200, 
		enhanc63300, 
		enhand63400, 
		enhane63500, 
		enhanf63600, 
		nearExit63800, 
		next64150, 
		accumUnit64200, 
		next64250, 
		nearExit64800, 
		termToRun65200, 
		fixedTerm65300, 
		exit65900, 
		b100CreateZrst, 
		b100Exit, 
		b300WriteTax2
	}

	public Zruliss() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void mainline000()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		isuallrec.statuz.set(varcom.oK);
		obtainInfo10000();
		/*    Hopefully the first time this is called it will get the Summa*/
		/*    COVT and hence convert all the UNLTs to ULNKs!*/
		if ((isEQ(isuallrec.planSuffix,0))
		|| (isEQ(isuallrec.convertUnlt,"Y"))) {
			convertUnltToUlnk20000();
		}
		/* Must create INCI and allocate regular premium if this is*/
		/* not a single premium only contract.*/
		if (isNE(chdrlnbIO.getBillfreq(),"00")
		&& isNE(chdrlnbIO.getBillfreq(), "  ")
		&& isNE(t5687rec.singlePremInd,"Y")) {
			inciDetails30000();
			allocation40000();
		}
		/* Now, is single premium only, or regular premium contract with*/
		/* lump sum at issue, allocate the single premium.*/
		if (isEQ(chdrlnbIO.getBillfreq(),"00")
		|| isEQ(chdrlnbIO.getBillfreq(), "  ")
		|| isNE(wsaaSingPrem,ZERO)) {
			singlePremiumProcess60000();
		}
		updateCovr50000();
		a500UpdateHitd();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainInfo10000()
	{
		/*PARA*/
		varcom.vrcmTimex.set(getCobolTime());
		varcom.vrcmTime.set(varcom.vrcmTimen);
		getFiles10100();
		getT568710200();
		getT554010300();
		getT664710400();
		getT554410450();
		getT553710500();
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models start
		//ILIFE-3775 AT Job BOBM after issuing VAP
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VALCL1")  && er.isExternalized(chdrlnbIO.cnttype.toString(), covrunlIO.crtable.toString())))
		{
			getT553610600();
		}
		else
		{
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(isuallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(chdrlnbIO.occdate);
			untallrec.untaBillfreq.set(chdrlnbIO.billfreq);	

			callProgram("VALCL", untallrec.untallrec,chdrlnbIO);
			
			wsaaMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaPcUnit[5].set(untallrec.untaPcUnit05);
					
			wsaaMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaPcUnit[6].set(untallrec.untaPcUnit06);
					
			wsaaMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaPcUnit[7].set(untallrec.untaPcUnit07);
			
		}
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models end
		getT551910800();
		getT553511100();
		getT564511200();
		getT665911300();
		getT568811400();
	}

protected void getFiles10100()
	{
		para10110();
	}

protected void para10110()
	{
		/* Retrieve contract header details.*/
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(isuallrec.company);
		chdrlnbIO.setChdrnum(isuallrec.chdrnum);
		/*    Need to Hold record as we will have to update the Unit*/
		/*    Statement Date.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
		/*Retrieve the CHDRMJA logical view as this contains the free*/
		/*switch information fields which will be updated.*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setDataKey(chdrlnbIO.getDataKey());
		chdrmjaIO.setStatuz(varcom.oK);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			dbError8100();
		}
		/* Retrieve LIFE details.*/
		wsaaLifeCltsex.set(SPACES);
		wsaaJlifeExists.set(SPACES);
		wsaaLifeAnbAtCcd.set(0);
		wsaaTaxs.fill("0");
		wsaaJlifeAnbAtCcd.set(99);
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(isuallrec.company);
		lifelnbIO.setChdrnum(isuallrec.chdrnum);
		lifelnbIO.setLife(isuallrec.life);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//lifelnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError8100();
		}
		/* Need to see if a joint life exists. If so, use the younger of*/
		/* the two lives to find the unit allocation basis.*/
		wsaaLifeCltsex.set(lifelnbIO.getCltsex());
		wsaaLifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		lifelnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if ((isNE(lifelnbIO.getStatuz(),varcom.oK))
		&& (isNE(lifelnbIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError8100();
		}
		if ((isNE(isuallrec.company,lifelnbIO.getChdrcoy()))
		|| (isNE(isuallrec.chdrnum,lifelnbIO.getChdrnum()))
		|| (isNE(isuallrec.life,lifelnbIO.getLife()))
		|| (isEQ(lifelnbIO.getStatuz(),varcom.endp))) {
			wsaaJlifeExists.set("N");
		}
		else {
			wsaaJlifeExists.set("Y");
		}
		if (isEQ(wsaaJlifeExists,"Y")) {
			wsaaJlifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		}
		/* LIFELNB-CLTSEX and LIFELNB-ANB-AT-CCD are used to access*/
		/* T5536 and T5537, so we have to make sure that they have the*/
		/* correct informations in them.*/
		if (isGTE(wsaaJlifeAnbAtCcd,wsaaLifeAnbAtCcd)) {
			lifelnbIO.setCltsex(wsaaLifeCltsex);
			lifelnbIO.setAnbAtCcd(wsaaLifeAnbAtCcd);
		}
		/*  Read and hold coverage/rider record.*/
		/*  The COVR Record may be updated by the other programs that      */
		/*  this subroutine calls. As such, initial reading of the COVR    */
		/*  should be using READR.                                         */
		covrunlIO.setChdrcoy(isuallrec.company);
		covrunlIO.setChdrnum(isuallrec.chdrnum);
		covrunlIO.setLife(isuallrec.life);
		covrunlIO.setCoverage(isuallrec.coverage);
		covrunlIO.setRider(isuallrec.rider);
		covrunlIO.setPlanSuffix(isuallrec.planSuffix);
		/*    MOVE READH                  TO COVRUNL-FUNCTION.             */
		covrunlIO.setFunction(varcom.readr);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(),varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
		/* Calculate the premium for use within the processing.*/
		wsaaSingPrem.set(covrunlIO.getSingp());
		if (isNE(covrunlIO.getInstprem(),ZERO)) {
			wsaaInstprem.set(covrunlIO.getInstprem());
			compute(wsaaPremiumPaid, 5).set(mult(covrunlIO.getInstprem(),isuallrec.freqFactor));
		}
		else {
			wsaaPremiumPaid.set(ZERO);
			wsaaInstprem.set(ZERO);
		}
	}

protected void getT568710200()
	{
		para10210();
	}

protected void para10210()
	{
		/*  Read T5687 for general coverage/rider details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5687))
		|| (isNE(itdmIO.getItemitem(),covrunlIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5687);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h053);
			systemError8000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT554010300()
	{
		para10310();
	}

protected void para10310()
	{
		/*  Read T5540 for general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(),covrunlIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5540);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h055);
			systemError8000();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT664710400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10410();
				case cont10450: 
					cont10450();
				case cont10460: 
					cont10460();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10410()
	{
		/* Read T6647 for unit linked contract details.*/
		/* Key is transaction code plus contract type.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(isuallrec.batctrcde);
		wsaaContract.set(chdrlnbIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			goTo(GotoLabel.cont10450);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.cont10460);
		}
	}

protected void cont10450()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(isuallrec.batctrcde);
		wsaaContract.set("***");
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6647);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.h115);
			systemError8000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void cont10460()
	{
		/* Calculate the monies date.*/
		if (isNE(covrunlIO.getReserveUnitsDate(),ZERO)
		&& isNE(covrunlIO.getReserveUnitsDate(),varcom.vrcmMaxDate)) {
			wsaaMoniesDate.set(covrunlIO.getReserveUnitsDate());
			return ;
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode,"BD")) {
			return ;
		}
		if (isEQ(t6647rec.efdcode,"DD")) {
			wsaaMoniesDate.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t6647rec.efdcode,"LO")) {
			if (isGT(chdrlnbIO.getOccdate(),wsaaMoniesDate)) {
				wsaaMoniesDate.set(chdrlnbIO.getOccdate());
			}
		}
	}

protected void getT554410450()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10450();
					freeSwitches();
				case keepsChdrmja: 
					keepsChdrmja();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10450()
	{
		/* Read T5544 for free switch information.*/
		/* Key is switch method from T6647 and contract currency.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5544);
		wsaaSwmeth.set(t6647rec.swmeth);
		wsaaCntcurr.set(chdrlnbIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5544Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5544))
		|| (isNE(itdmIO.getItemitem(),wsaaT5544Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			if (isEQ(wsaaSwmeth,SPACES)) {
				goTo(GotoLabel.keepsChdrmja);
			}
			else {
				itdmIO.setItemcoy(isuallrec.company);
				itdmIO.setItemtabl(tablesInner.t5544);
				itdmIO.setItemitem(wsaaT5544Key);
				itdmIO.setItmfrm(chdrlnbIO.getOccdate());
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(errorsInner.g033);
				systemError8000();
			}
		}
		else {
			t5544rec.t5544Rec.set(itdmIO.getGenarea());
		}
	}

protected void freeSwitches()
	{
		chdrmjaIO.setFreeSwitchesLeft(t5544rec.noOfFreeSwitches);
		chdrmjaIO.setFreeSwitchesUsed(ZERO);
		chdrmjaIO.setLastSwitchDate(chdrmjaIO.getOccdate());
	}

protected void keepsChdrmja()
	{
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			dbError8100();
		}
		/*EXIT1*/
	}

protected void getT553710500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10510();
				case readItem10515: 
					readItem10515();
				case cont10520: 
					cont10520();
				case yOrdinate10540: 
					yOrdinate10540();
					increment10560();
				case xOrdinate10580: 
					xOrdinate10580();
					increment10585();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10510()
	{
		/* Read T5537 for unit allocation basis.*/
		wsaaBasisSelect.set(t5540rec.allbas);
		wsaaTranCode.set(isuallrec.batctrcde);
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
	}

protected void readItem10515()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5537);
		itemIO.setItemitem(wsaaT5537Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and Transaction code.*/
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/*    If NO record found then try secondly with the basis, Sex*/
		/*    and '****'  Transaction code.*/
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
		wsaaTranCode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaTranCode.fill("*");
		/* If no match is found, try again with the global sex code*/
		wsaaT5537Sex.set("*");
		/*    When doing a global change and the global data is being*/
		/*    moved to the wsaa-t5537-key make sure that you move the*/
		/*    wsaa-t5537-key to the ITEM-ITEMITEM.*/
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		/* IF (ITEM-STATUZ             NOT = O-K) AND                   */
		/*    (ITEM-STATUZ             NOT = MRNF)                      */
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
	}

protected void cont10520()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*  Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate10540()
	{
		wsaaZ.set(0);
	}

protected void increment10560()
	{
		wsaaZ.add(1);
		/* Check the continuation field for a key to re-read table.*/
		if (isGT(wsaaZ,10)
		&& isNE(t5537rec.agecont,SPACES)) {
			readAgecont80000();
			goTo(GotoLabel.yOrdinate10540);
		}
		else {
			if (isGT(wsaaZ,10)
			&& isEQ(t5537rec.agecont,SPACES)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(errorsInner.e706);
				systemError8000();
			}
		}
		if (isLTE(wsaaZ,10)) {
			/*        IF  LIFELNB-ANB-AT-CCD           > T5537-TOAGE(WSAA-Z)   */
			if (isGT(lifelnbIO.getAnbAtCcd(), t5537rec.ageIssageTo[wsaaZ.toInt()])) {
				increment10560();
				return ;
			}
			else {
				wsaaY.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.agecont);
			goTo(GotoLabel.readItem10515);
		}
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate10580()
	{
		wsaaZ.set(0);
	}

protected void increment10585()
	{
		wsaaZ.add(1);
		/* Check the term continuation field for a key to re-read table.*/
		if (isGT(wsaaZ,9)
		&& isNE(t5537rec.trmcont,SPACES)) {
			readTermcont90000();
			goTo(GotoLabel.xOrdinate10580);
		}
		else {
			if (isGT(wsaaZ,9)
			&& isEQ(t5537rec.trmcont,SPACES)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(errorsInner.e707);
				systemError8000();
			}
		}
		/*    Always calculate the term.*/
		calculateTerm12000();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isLTE(wsaaZ,9)) {
			if (isGT(wsaaTerm,t5537rec.toterm[wsaaZ.toInt()])) {
				increment10585();
				return ;
			}
			else {
				wsaaX.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.trmcont);
			goTo(GotoLabel.readItem10515);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY,9));
		wsaaIndex.add(wsaaX);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
	}

protected void getT553610600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10610();
					gotRecord10620();
					nextFreq10640();
				case endLoop10650: 
					endLoop10650();
				case exit10690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10610()
	{
		/* Read T5536 for unit allocation basis details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5536);
		itdmIO.setItemitem(wsaaAllocBasis);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5536))
		|| (isNE(itdmIO.getItemitem(),wsaaAllocBasis))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5536);
			itdmIO.setItemitem(wsaaAllocBasis);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.e615);
			systemError8000();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	* There are 6 sets of details in the table, each set is led by
	* the billing frequency. So to find the correct set of details,
	* match each of these frequencies with the billing frequency
	* specified in the contract header record.
	* </pre>
	*/
protected void gotRecord10620()
	{
		wsaaZ.set(0);
	}

protected void nextFreq10640()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ,6)) {
			goTo(GotoLabel.endLoop10650);
		}
		if (isEQ(t5536rec.billfreq[wsaaZ.toInt()],SPACES)) {
			nextFreq10640();
			return ;
		}
		/* If this is a single premium component then get the frequency*/
		/* of '00' from the allocation table.*/
		if (isEQ(t5687rec.singlePremInd,"Y")
		&& isNE(t5536rec.billfreq[wsaaZ.toInt()],"00")) {
			nextFreq10640();
			return ;
		}
		if (isNE(t5536rec.billfreq[wsaaZ.toInt()],chdrlnbIO.getBillfreq())
		&& isNE(t5687rec.singlePremInd,"Y")) {
			nextFreq10640();
			return ;
		}
		/* The appropriate allocation basis details found.*/
		if (isEQ(wsaaZ,1)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ,2)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ,3)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaPcUnits.set(t5536rec.pcUnitscs);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ,4)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaPcUnits.set(t5536rec.pcUnitsds);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ,5)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaPcUnits.set(t5536rec.pcUnitses);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ,6)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
			goTo(GotoLabel.exit10690);
		}
	}

protected void endLoop10650()
	{
		isuallrec.statuz.set(errorsInner.e616);
		exit090();
	}

protected void getT664610700()
	{
			para10710();
		}

protected void para10710()
	{
		/* Read T6646 for initial unit discount factor for whole of life.*/
		if (isEQ(t5540rec.wholeIuDiscFact,SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void getT551910800()
	{
			para10810();
		}

protected void para10810()
	{
		/*  Read T5519 for initial unit discount basis.*/
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5519);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5519))
		|| (isNE(itdmIO.getItemitem(),t5540rec.iuDiscBasis))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5519);
			itdmIO.setItemitem(t5540rec.iuDiscBasis);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g027);
			systemError8000();
		}
		else {
			t5519rec.t5519Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT553910900()
	{
			para10910();
		}

protected void para10910()
	{
		/*  Read T5539 for initial unit discount factor for term.*/
		if (isEQ(t5540rec.iuDiscFact,SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

protected void getT553511100()
	{
			para11110();
		}

protected void para11110()
	{
		/*  Read T5535 for loyalty allocation.*/
		if (isEQ(t5540rec.ltypst,SPACES)) {
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5535);
		itdmIO.setItemitem(t5540rec.ltypst);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
	    //performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5535))
		|| (isNE(itdmIO.getItemitem(),t5540rec.ltypst))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5535);
			itdmIO.setItemitem(t5540rec.ltypst);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g032);
			systemError8000();
		}
		else {
			t5535rec.t5535Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT564511200()
	{
		para11210();
	}

protected void para11210()
	{
		/*  Read T5645 for transaction accounting rules.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getT665911300()
	{
			para11310();
		}

protected void para11310()
	{
		/* Read T6659 to get the frequency by using the unit statement*/
		/* method from T6647 as the key.*/
		if (isEQ(t6647rec.unitStatMethod,SPACES)) {
			t6659rec.t6659Rec.set(SPACES);
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isNE(itdmIO.getItemitem(),t6647rec.unitStatMethod)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6659);
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g029);
			systemError8000();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t6659rec.subprog,SPACES)
		|| isNE(t6659rec.annOrPayInd,"P")
		|| isNE(t6659rec.osUtrnInd,"Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(isuallrec.company);
		annprocrec.chdrnum.set(isuallrec.chdrnum);
		annprocrec.effdate.set(isuallrec.effdate);
		annprocrec.batchkey.set(isuallrec.batchkey);
		annprocrec.user.set(isuallrec.user);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz,"****")) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			dbError8100();
		}
	}

protected void getT568811400()
	{
		para11410();
	}

protected void para11410()
	{
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrlnbIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.e308);
			systemError8000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void calculateTerm12000()
	{
		/*PARA*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		/*EXIT*/
	}

protected void convertUnltToUlnk20000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para20010();
				case nextRecord20100: 
					nextRecord20100();
				case read20200: 
					read20200();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para20010()
	{
		unltunlIO.setDataArea(SPACES);
		unltunlIO.setChdrcoy(isuallrec.company);
		unltunlIO.setChdrnum(isuallrec.chdrnum);
		unltunlIO.setLife(isuallrec.life);
		unltunlIO.setCoverage(isuallrec.coverage);
		unltunlIO.setRider(isuallrec.rider);
		unltunlIO.setSeqnbr(ZERO);
		wsaaFirstUnltunl = "Y";
		unltunlIO.setFunction(varcom.begn);
		goTo(GotoLabel.read20200);
	}

protected void nextRecord20100()
	{
		unltunlIO.setFunction(varcom.nextr);
	}

protected void read20200()
	{
		unltunlio28000();
		if ((isEQ(unltunlIO.getStatuz(),varcom.endp))) {
			return ;
		}
		if ((isNE(unltunlIO.getChdrcoy(),isuallrec.company))
		|| (isNE(unltunlIO.getChdrnum(),isuallrec.chdrnum))
		|| (isNE(unltunlIO.getLife(),isuallrec.life))
		|| (isNE(unltunlIO.getCoverage(),isuallrec.coverage))
		|| (isNE(unltunlIO.getRider(),isuallrec.rider))) {
			return ;
		}
		convertUnltunl22000();
		/* At this point delete the last UNLTUNL record read since all*/
		/* the informations have been fully used(converted into ULNK)*/
		/* and will no longer be useful.*/
		unltunlIO.setFunction(varcom.readh);
		unltunlio28000();
		unltunlIO.setFunction(varcom.delet);
		unltunlio28000();
		setPrecision(unltunlIO.getSeqnbr(), 0);
		unltunlIO.setSeqnbr(add(unltunlIO.getSeqnbr(),1));
		goTo(GotoLabel.nextRecord20100);
	}

protected void convertUnltunl22000()
	{
			convert22000();
		}

protected void convert22000()
	{
		storePrc23200();
		if (isEQ(wsaaFirstUnltunl,"Y")) {
			firstUnltunl23000();
			wsaaFirstUnltunl = "N";
			return ;
		}
		/* Write any other split records which relevant to this UNLTUNL.*/
		/* For 'invest by amount', we will actually convert it into*/
		/* 'invest by percentage'. This will make life easier for future*/
		/* processing, such as premium increase and enhanced allocation.*/
		/* First we accumulate the total premiums.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(),"A")) {
			wsaaUalprcTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA,10)); wsaaA.add(1)){
				amount23100();
			}
		}
		/* Once we have the total premium, we can then calculate the*/
		/* equalivant investing percentage for each non blank fund.*/
		/* Then perform the processing as though it is 'invest by*/
		/* percentage'.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(),"A")) {
			wsaaUalprcRunTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA,10)
			|| isEQ(wsaaUalprc[wsaaA.toInt()],0)); wsaaA.add(1)){
				amountToPerc23300();
			}
		}
		for (wsaaA.set(unltunlIO.getNumapp()); !(isEQ(wsaaA,ZERO)); wsaaA.add(-1)){
			splits24000();
		}
	}

protected void firstUnltunl23000()
	{
			para23010();
		}

protected void para23010()
	{
		wsaaPlanSuffix.set(0);
		compute(wsaaB, 0).set(sub(unltunlIO.getNumapp(),chdrlnbIO.getPolsum()));
		/* For 'invest by amount', we will actually convert it into*/
		/* 'invest by percentage'. This will make life easier for future*/
		/* processing, such as premium increase and enhanced allocation.*/
		/* First we accumulate the total premiums.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(),"A")) {
			wsaaUalprcTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA,10)); wsaaA.add(1)){
				amount23100();
			}
		}
		/* Once we have the total premium, we can then calculate the*/
		/* equalivant investing percentage for each non blank fund.*/
		/* Then perform the processing as though it is 'invest by*/
		/* percentage'.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(),"A")) {
			wsaaUalprcRunTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA,10)
			|| isEQ(wsaaUalprc[wsaaA.toInt()],0)); wsaaA.add(1)){
				amountToPerc23300();
			}
		}
		setupUlnk29000();
		if (isEQ(isuallrec.convertUnlt,"Y")) {
			ulnkIO.setPlanSuffix(isuallrec.planSuffix);
		}
		ulnkIO.setFunction(varcom.updat);
		ulnkio29500();
		if (isGT(chdrlnbIO.getPolinc(),1)) {
			compute(wsaaPlanSuffix, 0).set(add(wsaaPlanSuffix,chdrlnbIO.getPolsum()));
			wsaaPlanSuffix.add(1);
		}
		/* Write any other duplicate records for the first split.*/
		if (isEQ(wsaaB,ZERO)) {
			return ;
		}
		for (wsaaA.set(wsaaB); !(isEQ(wsaaA,ZERO)); wsaaA.add(-1)){
			splits24000();
		}
	}

protected void storePrc23200()
	{
		/*PARA*/
		wsaaUalprc[1].set(unltunlIO.getUalprc(1));
		wsaaUalprc[2].set(unltunlIO.getUalprc(2));
		wsaaUalprc[3].set(unltunlIO.getUalprc(3));
		wsaaUalprc[4].set(unltunlIO.getUalprc(4));
		wsaaUalprc[5].set(unltunlIO.getUalprc(5));
		wsaaUalprc[6].set(unltunlIO.getUalprc(6));
		wsaaUalprc[7].set(unltunlIO.getUalprc(7));
		wsaaUalprc[8].set(unltunlIO.getUalprc(8));
		wsaaUalprc[9].set(unltunlIO.getUalprc(9));
		wsaaUalprc[10].set(unltunlIO.getUalprc(10));
		/*EXIT*/
	}

protected void amount23100()
	{
		/*PARA*/
		compute(wsaaUalprcTot, 2).set(add(wsaaUalprcTot,wsaaUalprc[wsaaA.toInt()]));
		/*EXIT*/
	}

protected void amountToPerc23300()
	{
		/*PARA*/
		/* The following is to avoid rounding error.*/
		/* Keep a running total and use the remainder as the percentage*/
		/* when the last fund on the record is encountered.*/
		if (isEQ(wsaaA,10)) {
			compute(wsaaUalprc[wsaaA.toInt()], 2).set(sub(100,wsaaUalprcRunTot));
			return ;
		}
		if ((setPrecision(wsaaUalprc[add(wsaaA,1).toInt()], 0)
		&& isEQ(wsaaUalprc[add(wsaaA,1).toInt()],0))) {
			compute(wsaaUalprc[wsaaA.toInt()], 2).set(sub(100,wsaaUalprcRunTot));
			return ;
		}
		compute(wsaaUalprc[wsaaA.toInt()], 3).setRounded(div(mult(wsaaUalprc[wsaaA.toInt()],100),wsaaUalprcTot));
		wsaaUalprcRunTot.add(wsaaUalprc[wsaaA.toInt()]);
	}

protected void splits24000()
	{
		/*PARA*/
		setupUlnk29000();
		ulnkIO.setFunction(varcom.updat);
		ulnkio29500();
		wsaaPlanSuffix.add(1);
		/*EXIT*/
	}

protected void unltunlio28000()
	{
		/*READ*/
		unltunlIO.setFormat(formatsInner.unltunlrec);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(),varcom.oK)) {
			if (isNE(unltunlIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(unltunlIO.getParams());
				syserrrec.statuz.set(unltunlIO.getStatuz());
				dbError8100();
			}
		}
		/*EXIT*/
	}

protected void setupUlnk29000()
	{
		setup29010();
	}

protected void setup29010()
	{
		ulnkIO.setChdrcoy(unltunlIO.getChdrcoy());
		ulnkIO.setChdrnum(unltunlIO.getChdrnum());
		ulnkIO.setLife(unltunlIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(unltunlIO.getCoverage());
		ulnkIO.setRider(unltunlIO.getRider());
		ulnkIO.setPlanSuffix(wsaaPlanSuffix);
		ulnkIO.setUalfnds(unltunlIO.getUalfnds());
		ulnkIO.setUalprc(1, wsaaUalprc[1]);
		ulnkIO.setUalprc(2, wsaaUalprc[2]);
		ulnkIO.setUalprc(3, wsaaUalprc[3]);
		ulnkIO.setUalprc(4, wsaaUalprc[4]);
		ulnkIO.setUalprc(5, wsaaUalprc[5]);
		ulnkIO.setUalprc(6, wsaaUalprc[6]);
		ulnkIO.setUalprc(7, wsaaUalprc[7]);
		ulnkIO.setUalprc(8, wsaaUalprc[8]);
		ulnkIO.setUalprc(9, wsaaUalprc[9]);
		ulnkIO.setUalprc(10, wsaaUalprc[10]);
		ulnkIO.setUspcprs(unltunlIO.getUspcprs());
		ulnkIO.setTranno(unltunlIO.getTranno());
		ulnkIO.setPercOrAmntInd(unltunlIO.getPercOrAmntInd());
		ulnkIO.setFndSpl(unltunlIO.getFndSpl());/*ILIFE-4036*/
		ulnkIO.setCurrto(99999999);
		ulnkIO.setPremTopupInd(unltunlIO.getPremTopupInd());
		ulnkIO.setValidflag("1");
		ulnkIO.setCurrfrom(isuallrec.effdate);
		
		if (isEQ(isuallrec.batctrcde,"T642"))
		{
			if (isGT(wsaaUalprc[1],ZERO))
			{
				ulnkIO.setFundPool01("1");
			}
			if (isGT(wsaaUalprc[2],ZERO))
			{
			ulnkIO.setFundPool02("1");
			}
			if (isGT(wsaaUalprc[3],ZERO))
			{
			ulnkIO.setFundPool03("1");
			}
			if (isGT(wsaaUalprc[4],ZERO))
			{
			ulnkIO.setFundPool04("1");
			}
			if (isGT(wsaaUalprc[5],ZERO))
			{
			ulnkIO.setFundPool05("1");
			}
			if (isGT(wsaaUalprc[6],ZERO))
			{
			ulnkIO.setFundPool06("1");
			}
			if (isGT(wsaaUalprc[7],ZERO))
			{
			ulnkIO.setFundPool07("1");
			}
			if (isGT(wsaaUalprc[8],ZERO))
			{
			ulnkIO.setFundPool08("1");
			}
			if (isGT(wsaaUalprc[9],ZERO))
			{
			ulnkIO.setFundPool09("1");
			}
			if (isGT(wsaaUalprc[10],ZERO))
			{
			ulnkIO.setFundPool10("1");
			}
		}
	}

protected void ulnkio29500()
	{
		/*CALL*/
		ulnkIO.setFormat(formatsInner.ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void inciDetails30000()
	{
		/*PARA*/
		initialiseInci31000();
		setUpInciDetails32000();
		writeInci39000();
		/*EXIT*/
	}

protected void initialiseInci31000()
	{
		para31010();
		next31800();
	}

protected void para31010()
	{
		inciIO.setChdrcoy(isuallrec.company);
		inciIO.setChdrnum(isuallrec.chdrnum);
		inciIO.setLife(isuallrec.life);
		inciIO.setCoverage(isuallrec.coverage);
		inciIO.setRider(isuallrec.rider);
		inciIO.setPlanSuffix(isuallrec.planSuffix);
		inciIO.setInciNum("001");
		inciIO.setSeqno(1);
		inciIO.setValidflag("1");
		inciIO.setDormantFlag("N");
		inciIO.setTranno(chdrlnbIO.getTranno());
		inciIO.setRcdate(chdrlnbIO.getOccdate());
		inciIO.setPremCessDate(covrunlIO.getPremCessDate());
		inciIO.setCrtable(covrunlIO.getCrtable());
		inciIO.setCurrPrem(wsaaInstprem);
		inciIO.setOrigPrem(wsaaInstprem);
		inciIO.setUser(isuallrec.user);
		inciIO.setTermid(isuallrec.termid);
		inciIO.setTransactionDate(isuallrec.transactionDate);
		inciIO.setTransactionTime(isuallrec.transactionTime);
		wsaaZ.set(ZERO);
	}

protected void next31800()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ,7)) {
			return ;
		}
		inciIO.setPremst(wsaaZ, ZERO);
		inciIO.setPremcurr(wsaaZ, ZERO);
		inciIO.setPcunit(wsaaZ, ZERO);
		inciIO.setUsplitpc(wsaaZ, ZERO);
		next31800();
		return ;
	}

protected void setUpInciDetails32000()
	{
		para32010();
		nextPeriod32200();
	}

protected void para32010()
	{
		wsaaZ.set(0);
	}

protected void nextPeriod32200()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ,7)) {
			return ;
		}
		inciIO.setPcunit(wsaaZ, wsaaPcUnit[wsaaZ.toInt()]);
		inciIO.setUsplitpc(wsaaZ, wsaaPcInitUnit[wsaaZ.toInt()]);
		if (isEQ(wsaaMaxPeriod[wsaaZ.toInt()],0)) {
			wsaaMaxPeriod[wsaaZ.toInt()].set(9999);
			inciIO.setPremst(wsaaZ, 0);
			inciIO.setPremcurr(wsaaZ, 0);
			nextPeriod32200();
			return ;
		}
		if (isGT(wsaaZ,1)) {
			compute(wsaaX, 0).set(sub(wsaaZ,1));
			compute(wsaaNofPeriods, 0).set(sub(wsaaMaxPeriod[wsaaZ.toInt()],wsaaMaxPeriod[wsaaX.toInt()]));
		}
		else {
			wsaaNofPeriods.set(wsaaMaxPeriod[wsaaZ.toInt()]);
		}
		setPrecision(inciIO.getPremcurr(wsaaZ), 2);
		inciIO.setPremcurr(wsaaZ, mult(wsaaInstprem,wsaaNofPeriods));
		inciIO.setPremst(wsaaZ, inciIO.getPremcurr(wsaaZ));
		nextPeriod32200();
		return ;
	}

protected void writeInci39000()
	{
		/*WRITE-REC*/
		inciIO.setFunction(varcom.updat);
		inciIO.setFormat(formatsInner.incirec);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void allocation40000()
	{
		try {
			para40010();
			termFound40060();
			call10070();
			nextEdtitm10075();
		}
		catch (GOTOException e){
			LOGGER.error("allocation40000 method exception:",e);
		}
	}

protected void para40010()
	{
		/* If premium is zero skip all allocation processing.*/
		if (isEQ(wsaaInstprem,ZERO)) {
			goTo(GotoLabel.exit40090);
		}
		/* Set up correct parameters and call the subroutine ZRULALOC*/
		/* to allocate the premium to buy units(i.e. writing UTRNs).*/
		rnlallrec.company.set(isuallrec.company);
		rnlallrec.chdrnum.set(isuallrec.chdrnum);
		rnlallrec.life.set(isuallrec.life);
		rnlallrec.coverage.set(isuallrec.coverage);
		rnlallrec.rider.set(isuallrec.rider);
		rnlallrec.planSuffix.set(isuallrec.planSuffix);
		rnlallrec.billfreq.set(chdrlnbIO.getBillfreq());
		rnlallrec.billcd.set(chdrlnbIO.getBillcd());
		rnlallrec.duedate.set(chdrlnbIO.getInstfrom());
		rnlallrec.tranno.set(chdrlnbIO.getTranno());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			rnlallrec.sacscode.set(t5645rec.sacscode03);
			rnlallrec.genlcde.set(t5645rec.glmap03);
			rnlallrec.sacstyp.set(t5645rec.sacstype03);
			rnlallrec.sacscode02.set(t5645rec.sacscode04);
			rnlallrec.genlcde02.set(t5645rec.glmap04);
			rnlallrec.sacstyp02.set(t5645rec.sacstype04);
			rnlallrec.sacscode.set(t5645rec.sacscode06);
			rnlallrec.genlcde.set(t5645rec.glmap06);
			rnlallrec.sacstyp.set(t5645rec.sacstype06);
		}
		else {
			rnlallrec.sacscode.set(t5645rec.sacscode01);
			rnlallrec.genlcde.set(t5645rec.glmap01);
			rnlallrec.sacstyp.set(t5645rec.sacstype01);
			rnlallrec.sacscode02.set(t5645rec.sacscode02);
			rnlallrec.genlcde02.set(t5645rec.glmap02);
			rnlallrec.sacstyp02.set(t5645rec.sacstype02);
			rnlallrec.sacscode.set(t5645rec.sacscode05);
			rnlallrec.genlcde.set(t5645rec.glmap05);
			rnlallrec.sacstyp.set(t5645rec.sacstype05);
		}
		rnlallrec.cnttype.set(chdrlnbIO.getCnttype());
		rnlallrec.cntcurr.set(chdrlnbIO.getCntcurr());
		rnlallrec.batccoy.set(isuallrec.batccoy);
		rnlallrec.batcbrn.set(isuallrec.batcbrn);
		rnlallrec.batcactyr.set(isuallrec.batcactyr);
		rnlallrec.batcactmn.set(isuallrec.batcactmn);
		rnlallrec.batctrcde.set(isuallrec.batctrcde);
		rnlallrec.batcbatch.set(isuallrec.batcbatch);
		rnlallrec.user.set(isuallrec.user);
		rnlallrec.crtable.set(covrunlIO.getCrtable());
		rnlallrec.crdate.set(covrunlIO.getCrrcd());
		rnlallrec.moniesDate.set(wsaaMoniesDate);
		rnlallrec.anbAtCcd.set(covrunlIO.getAnbAtCcd());
		rnlallrec.language.set(isuallrec.language);
		/* Calculate the term left to run if risk cessation term is 0.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
	}

protected void termFound40060()
	{
		rnlallrec.termLeftToRun.set(wsaaRiskCessTerm);
		/* Use the pro-rata premium if premium has been received,*/
		/* otherwise use the coverage premium passed.*/
		/* Tot recd should only be set up when a pro-rata premium*/
		/* has been received.*/
		if (isNE(wsaaPremiumPaid,isuallrec.covrInstprem)) {
			rnlallrec.covrInstprem.set(wsaaPremiumPaid);
			rnlallrec.totrecd.set(wsaaPremiumPaid);
		}
		else {
			rnlallrec.covrInstprem.set(isuallrec.covrInstprem);
			rnlallrec.totrecd.set(0);
		}
		rnlallrec.effdate.set(isuallrec.effdate);
		if (isEQ(chdrlnbIO.getBtdate(),chdrlnbIO.getOccdate())) {
			goTo(GotoLabel.exit40090);
		}
		/* Check FUNCTION field to see if we should be calling ZRULALOC*/
		/* or not - we always want to call it unless this subroutine has*/
		/* been invoked as part of Automatic Increase processing.*/
		if (isNE(isuallrec.function,SPACES)) {
			goTo(GotoLabel.exit40090);
		}
	}

protected void call10070()
	{
		/* Call to relevant subroutine is generic using T5671.*/
		/* As the subroutines are executed serially from the*/
		/* calling program (P5074AT), a default item has been added -*/
		/* Tran Code + T5671-EDTITM - to point to the correct Unit*/
		/* Allocation routine.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5671);
		wsaaT5671Batc.set(isuallrec.batctrcde);
		wsaaT5671Crtable.set(covrunlIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Call the issue Allocation routine via the default item.*/
		wsaaIdx.set(ZERO);
	}

protected void nextEdtitm10075()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5671);
		wsaaT5671Batc.set(isuallrec.batctrcde);
		wsaaIdx.add(1);
		if (isGT(wsaaIdx,4)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(errorsInner.g513);
			dbError8100();
		}
		/* Check the 4 entries in T5671. Specifically, we are looking   */
		/* for the key 'ZRUL', which will be used to look for the       */
		/* Allocation Subroutine.                                       */
		wsaaT5671Edtitm.set(t5671rec.edtitm[wsaaIdx.toInt()]);
		if (isNE(wsaaEdtitmCrtable,SPACES)) {
			if (isNE(wsaaEdtitmFiller,SPACES)) {
				nextEdtitm10075();
				return ;
			}
		}
		else {
			nextEdtitm10075();
			return ;
		}
		wsaaT5671Crtable.set(t5671rec.edtitm[wsaaIdx.toInt()]);
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		wsaaOrigDuedate.set(rnlallrec.crdate);
		rnlallrec.duedate.set(rnlallrec.crdate);
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaIndex.set(1);
		if (isNE(t5671rec.subprog[wsaaIndex.toInt()],SPACES)) {
			callProgram(t5671rec.subprog[wsaaIndex.toInt()], rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(rnlallrec.statuz);
				syserrrec.params.set(rnlallrec.rnlallRec);
				systemError8000();
			}
			/* Advance the due date for the number of instalments paid so*/
			/* the ZRRA record can retain the original monies date (needed*/
			/* when the contract has been AFI'd and reissued, to locate*/
			/* the correct UTRN allocation prices).*/
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate1.set(rnlallrec.duedate);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.frequency.set(rnlallrec.billfreq);
			datcon4rec.freqFactor.set(isuallrec.freqFactor);
			datcon4rec.billday.set(wsaaBillday);
			datcon4rec.billmonth.set(wsaaBillmonth);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				systemError8000();
			}
			rnlallrec.duedate.set(datcon4rec.intDate2);
		}
	}

protected void updateCovr50000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readhCovr50000();
					convertAnniversaryDate50010();
					unitCancellationDate50100();
					unitConversionDate50200();
				case extraAllocDate50400: 
					extraAllocDate50400();
				case unitStmntDate50500: 
					unitStmntDate50500();
					iuIncrDate50600();
				case updateCovrunl50700: 
					updateCovrunl50700();
				case exit50090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* now that we need to update the COVR, we hold the record,        
	* </pre>
	*/
protected void readhCovr50000()
	{
		covrunlIO.setChdrcoy(isuallrec.company);
		covrunlIO.setChdrnum(isuallrec.chdrnum);
		covrunlIO.setLife(isuallrec.life);
		covrunlIO.setCoverage(isuallrec.coverage);
		covrunlIO.setRider(isuallrec.rider);
		covrunlIO.setPlanSuffix(isuallrec.planSuffix);
		covrunlIO.setFunction(varcom.readh);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setStatuz(covrunlIO.getStatuz());
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
	}

	/**
	* <pre>
	*    And also CHDR!
	* </pre>
	*/
protected void convertAnniversaryDate50010()
	{
		datcon2rec.intDate1.set(covrunlIO.getCrrcd());
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
		}
		datcon2rec.intDate1.set(datcon2rec.intDate2);
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
		}
		covrunlIO.setAnnivProcDate(datcon2rec.intDate2);
	}

protected void unitCancellationDate50100()
	{
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			goTo(GotoLabel.extraAllocDate50400);
		}
		if (isEQ(t5519rec.unitdeduc,"Y")) {
			/*         MOVE CHDRLNB-OCCDATE   TO DTC2-INT-DATE-1               */
			/*         MOVE COVRUNL-CRRCD     TO DTC2-INT-DATE-1       <LA4351>*/
			/*         MOVE T5519-INIT-UNIT-CHARGE-FREQ                        */
			/*                                TO DTC2-FREQUENCY                */
			/*         MOVE 1                 TO DTC2-FREQ-FACTOR              */
			/*         CALL 'DATCON2' USING   DTC2-DATCON2-REC                 */
			/*         IF   DTC2-STATUZ       NOT = O-K                        */
			/*              MOVE DTC2-STATUZ  TO SYSR-STATUZ                   */
			/*              MOVE DTC2-DATCON2-REC                              */
			/*                                TO SYSR-PARAMS                   */
			/*              PERFORM 8000-SYSTEM-ERROR                          */
			/*         ELSE                                                    */
			/*              MOVE DTC2-INT-DATE-2                               */
			/*                                TO COVRUNL-INIT-UNIT-CANC-DATE   */
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate1.set(covrunlIO.getCrrcd());
			wsaaOrigDuedate.set(covrunlIO.getCrrcd());
			datcon4rec.billday.set(wsaaBillday);
			datcon4rec.billmonth.set(wsaaBillmonth);
			datcon4rec.intDate2.set(99999999);
			datcon4rec.frequency.set(t5519rec.initUnitChargeFreq);
			datcon4rec.freqFactor.set(1);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon4rec.statuz);
				syserrrec.params.set(datcon4rec.datcon4Rec);
				systemError8000();
			}
			else {
				covrunlIO.setInitUnitCancDate(datcon4rec.intDate2);
				goTo(GotoLabel.extraAllocDate50400);
			}
		}
	}

protected void unitConversionDate50200()
	{
		if (isNE(t5519rec.unitadj,"Y")) {
			goTo(GotoLabel.extraAllocDate50400);
		}
		if (isEQ(t5519rec.fixdtrm,0)) {
			covrunlIO.setConvertInitialUnits(covrunlIO.getRiskCessDate());
			goTo(GotoLabel.extraAllocDate50400);
		}
		wsaaDateSplit.set(chdrlnbIO.getOccdate());
		wsaaYearSplit.add(t5519rec.fixdtrm);
		covrunlIO.setConvertInitialUnits(wsaaDateSplit);
	}

protected void extraAllocDate50400()
	{
		if (isEQ(t5540rec.ltypst,SPACES)) {
			goTo(GotoLabel.unitStmntDate50500);
		}
		/*    MOVE CHDRLNB-OCCDATE        TO DTC2-INT-DATE-1.              */
		/*    MOVE COVRUNL-CRRCD          TO DTC2-INT-DATE-1.      <LA4351>*/
		/*    MOVE T5535-UNIT-AFTER-DUR-01   TO DTC2-FREQ-FACTOR.          */
		/*    MOVE '12'                   TO DTC2-FREQUENCY.               */
		/*    CALL 'DATCON2' USING DTC2-DATCON2-REC.                       */
		/*    IF DTC2-STATUZ              NOT = O-K                        */
		/*        GO TO 50090-EXIT.                                        */
		/*    MOVE DTC2-INT-DATE-2        TO COVRUNL-REVIEW-PROCESSING.    */
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(covrunlIO.getCrrcd());
		wsaaOrigDuedate.set(covrunlIO.getCrrcd());
		datcon4rec.billday.set(wsaaBillday);
		datcon4rec.billmonth.set(wsaaBillmonth);
		datcon4rec.intDate2.set(99999999);
		datcon4rec.freqFactor.set(t5535rec.unitAfterDur01);
		datcon4rec.frequency.set("12");
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			goTo(GotoLabel.exit50090);
		}
		covrunlIO.setReviewProcessing(datcon4rec.intDate2);
	}

protected void unitStmntDate50500()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		/* Use the frequency from T6659 to calculate the unit statement*/
		/* date. Increment the risk commencement date by the frequency*/
		/* obtained, this will give the unit statement date.*/
		/*    IF T6659-FREQ = SPACE                                        */
		/*        MOVE '00'               TO DTC2-FREQUENCY                */
		/*    ELSE                                                         */
		/*        MOVE T6659-FREQ         TO DTC2-FREQUENCY.               */
		/*    MOVE CHDRLNB-OCCDATE        TO DTC2-INT-DATE-1.              */
		/*    MOVE 0                      TO DTC2-INT-DATE-2.              */
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.             */
		/*    CALL 'DATCON2' USING DTC2-DATCON2-REC.                       */
		/*    IF  DTC2-STATUZ NOT = O-K                                    */
		/*        MOVE DTC2-STATUZ TO SYSR-STATUZ                          */
		/*        MOVE DTC2-DATCON2-REC TO SYSR-PARAMS                     */
		/*        PERFORM 8000-SYSTEM-ERROR.                               */
		/*    MOVE DTC2-INT-DATE-2        TO CHDRLNB-STATEMENT-DATE.       */
		/*    MOVE DTC2-INT-DATE-2        TO COVRUNL-UNIT-STATEMENT-DATE.  */
		if (isEQ(t6659rec.freq,SPACES)) {
			datcon2rec.frequency.set("00");
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
		}
			else {
		chdrlnbIO.setStatementDate(datcon2rec.intDate2);
		covrunlIO.setUnitStatementDate(datcon2rec.intDate2);
			}
		}
		else {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.frequency.set(t6659rec.freq);
			datcon4rec.intDate1.set(chdrlnbIO.getOccdate());
			wsaaOrigDuedate.set(chdrlnbIO.getOccdate());
			datcon4rec.billday.set(wsaaBillday);
			datcon4rec.billmonth.set(wsaaBillmonth);
			datcon4rec.intDate2.set(99999999);
			datcon4rec.intDate2.set(0);
			datcon4rec.freqFactor.set(1);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon4rec.statuz);
				syserrrec.params.set(datcon4rec.datcon4Rec);
				systemError8000();
			}
			else {
				chdrlnbIO.setStatementDate(datcon4rec.intDate2);
				covrunlIO.setUnitStatementDate(datcon4rec.intDate2);
			}
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
	}

protected void iuIncrDate50600()
	{
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			goTo(GotoLabel.updateCovrunl50700);
		}
		if (isNE(t5519rec.diffprice,"Y")
		|| isNE(t5519rec.unitadj,"Y")) {
			goTo(GotoLabel.updateCovrunl50700);
		}
		covrunlIO.setInitUnitIncrsDate(covrunlIO.getAnnivProcDate());
	}

protected void updateCovrunl50700()
	{
 //IBPLIFE-6312, check to skip updating coverage debt if FMC flag is ON as per BSD.
	fmcOnFlag = FeaConfg.isFeatureExist(rnlallrec.company.toString().trim(), FMC_FEATURE_ID, appVars, "IT"); 
	if(!fmcOnFlag){	
		/* Update COVRUNL.*/
		covrunlIO.setChdrcoy(isuallrec.company);
		covrunlIO.setChdrnum(isuallrec.chdrnum);
		covrunlIO.setLife(isuallrec.life);
		covrunlIO.setCoverage(isuallrec.coverage);
		covrunlIO.setRider(isuallrec.rider);
		covrunlIO.setPlanSuffix(isuallrec.planSuffix);
		setPrecision(covrunlIO.getCoverageDebt(), 2);
		covrunlIO.setCoverageDebt(add(covrunlIO.getCoverageDebt(), add(wsaaTax[1], wsaaTax[2])));
		covrunlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(),varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
		}//IBPLIFE-6312
	}  

protected void singlePremiumProcess60000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para60010();
				case fundAllocation60050: 
					fundAllocation60050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para60010()
	{
		/* Need the allocation basis for single premiums if we are dealing*/
		/* with a lump sum.*/
		//getSingpAllocDets60100();
		//VPMS externalization code start
		//ILIFE-3775 AT Job BOBM after issuing VAP
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VALCL") && er.isExternalized(chdrlnbIO.cnttype.toString(), covrunlIO.crtable.toString())))
		{
			getSingpAllocDets60100();
		}
		else
		{
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(isuallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(chdrlnbIO.occdate);
			untallrec.untaBillfreq.set(chdrlnbIO.billfreq);	

			callProgram("VALCL", untallrec.untallrec,chdrlnbIO);
			
			
			wsaaMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaPcUnit[5].set(untallrec.untaPcUnit05);
					
			wsaaMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaPcUnit[6].set(untallrec.untaPcUnit06);
					
			wsaaMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaPcUnit[7].set(untallrec.untaPcUnit07);
			
		}
		//VPMS externalization code end
		wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(0);
		/* Calculate the uninvested allocation.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 3).setRounded(div((mult(wsaaSingPrem, (sub(100, wsaaPcUnit[1])))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying initial units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult((sub(wsaaSingPrem, wsaaAmountHoldersInner.wsaaNonInvestPrem)), wsaaPcInitUnit[1]), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying accumulation units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(sub(wsaaSingPrem, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		/* Check for any enhancement for these preimums from T6647.*/
		/* (T6647 has been read in 10400-GET-T6647 section.)*/
		if (isEQ(t6647rec.enhall,SPACES)) {
			goTo(GotoLabel.fundAllocation60050);
		}
		/* Read T5545 to get the enhancement percentage.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		wsaaT5545Cntcurr.set(chdrlnbIO.getCntcurr());
		wsaaT5545Enhall.set(t6647rec.enhall);
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5545))
		|| (isNE(itdmIO.getItemitem(),wsaaT5545Item))) {
			itdmIO.setGenarea(SPACES);
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
		if (isEQ(t5545rec.t5545Rec,SPACES)) {
			goTo(GotoLabel.fundAllocation60050);
		}
		/* Look for the correct percentage.*/
		wsaaEnhancePerc.set(0);
		wsaaIndex.set(1);
		while ( !((isGT(wsaaIndex,6))
		|| (isNE(wsaaEnhancePerc,0)))) {
			//matchEnhanceBasis63000();
			//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible START
			//ILIFE-3775 AT Job BOBM after issuing VAP
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VEUAL") && er.isExternalized(chdrlnbIO.cnttype.toString(), covrunlIO.crtable.toString())))
			{
				matchEnhanceBasis63000();
			}
			else
			{
				Untallrec untallrec = new Untallrec();
				untallrec.untaCompany.set(isuallrec.company);
				untallrec.untaBatctrcde.set(isuallrec.batctrcde);
				untallrec.untaEnhall.set(t6647rec.enhall);
				untallrec.untaCntcurr.set(chdrlnbIO.cntcurr);
				untallrec.untaCalcType.set("S");
				untallrec.untaEffdate.set(chdrlnbIO.occdate);
				untallrec.untaInstprem.set(isuallrec.covrSingp);
				untallrec.untaAllocInit.set(wsaaAmountHoldersInner.wsaaAllocInit);
				untallrec.untaAllocAccum.set(wsaaAmountHoldersInner.wsaaAllocAccum);
				untallrec.untaBillfreq.set(chdrlnbIO.getBillfreq());		

				callProgram("VEUAL", untallrec.untallrec,chdrlnbIO);
				
				wsaaEnhancePerc.set(untallrec.untaEnhancePerc);	
				wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(untallrec.untaEnhanceAlloc);	
				wsaaIndex.add(1);
			}
			//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible END
		}
		
		if (isEQ(wsaaEnhancePerc,0)) {
			goTo(GotoLabel.fundAllocation60050);
		}
		/* Calculate the enhanced premiums.*/
		wsaaAmountHoldersInner.wsaaOldAllocInit.set(wsaaAmountHoldersInner.wsaaAllocInit);
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(sub(wsaaAmountHoldersInner.wsaaOldAllocInit, wsaaAmountHoldersInner.wsaaAllocInit));
		wsaaAmountHoldersInner.wsaaOldAllocAccum.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(add(wsaaAmountHoldersInner.wsaaEnhanceAlloc, (sub(wsaaAmountHoldersInner.wsaaOldAllocAccum, wsaaAmountHoldersInner.wsaaAllocAccum))));
	}

protected void fundAllocation60050()
	{
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(isuallrec.company);
		ulnkrnlIO.setChdrnum(isuallrec.chdrnum);
		ulnkrnlIO.setLife(isuallrec.life);
		ulnkrnlIO.setCoverage(isuallrec.coverage);
		ulnkrnlIO.setRider(isuallrec.rider);
		ulnkrnlIO.setPlanSuffix(isuallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(formatsInner.ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			dbError8100();
		}
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		initUnitDiscFactor65000();
		wsaaIndex.set(1);
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaEnhPerFund.set(0);
		wsaaAmountHoldersInner.wsaaEnhanceAmt.set(0);
		wsaaAmountHoldersInner.wsaaRunTot.set(0);
		wsaaAmountHoldersInner.wsaaTotIbSplit.set(0);
		wsaaAmountHoldersInner.wsaaTotUlSplit.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		while ( !((isGT(wsaaIndex,10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaIndex),SPACES)))) {
			fundSplitAllocation64000();
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		/* The enhanced allocation amount should not be added into the*/
		/* Non-invested amoumt*/
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0)) {
			return ;
		}
		/* MOVE SPACE              TO UTRN-PARAMS.                      */
		/* PERFORM 66000-CHECK-EXISTING-UTRN.                           */
		/* IF UTRN-STATUZ = O-K                                         */
		/*     ADD WSAA-NON-INVEST-PREM TO UTRN-CONTRACT-AMOUNT         */
		/*     PERFORM 70000-REWRITE-EXISTING-UTRN                      */
		/* ELSE                                                         */
		/*     MOVE 'NVST'             TO UTRN-UNIT-SUB-ACCOUNT         */
		/*     MOVE WSAA-NON-INVEST-PREM TO UTRN-CONTRACT-AMOUNT        */
		/*     MOVE 0                  TO UTRN-DISCOUNT-FACTOR          */
		/*     PERFORM 61000-SET-UP-WRITE-UTRN.                         */
		if (isGT(wsaaAmountHoldersInner.wsaaTotUlSplit, 0)) {
			utrnIO.setParams(SPACES);
			utrnIO.setUnitSubAccount("NVST");
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
			zrdecplrec.amountIn.set(utrnIO.getContractAmount());
			a000CallRounding();
			utrnIO.setContractAmount(zrdecplrec.amountOut);
			utrnIO.setDiscountFactor(0);
			setUpWriteUtrn61000();
			if (isGT(wsaaAmountHoldersInner.wsaaTotIbSplit, 0)) {
				hitrIO.setParams(SPACES);
				setPrecision(hitrIO.getContractAmount(), 2);
				hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaNonInvestPrem, utrnIO.getContractAmount()));
				zrdecplrec.amountIn.set(hitrIO.getContractAmount());
				a000CallRounding();
				hitrIO.setContractAmount(zrdecplrec.amountOut);
				a200SetUpWriteHitr();
			}
		}
		else {
			hitrIO.setParams(SPACES);
			hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPrem);
			a200SetUpWriteHitr();
		}
		/* Perform to determine the tax.                                   */
		b100ProcessTax();
	}

protected void getSingpAllocDets60100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para60100();
					nextFreq60140();
				case endLoop60150: 
					endLoop60150();
				case exit60190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para60100()
	{
		/** There are 6 sets of details in the table, each set is led by*/
		/** the billing frequency. So to find the correct set of details,*/
		/** match each of these frequencies with a billing frequency of*/
		/** '00', for single premiums.*/
		/*GOT-RECORD*/
		wsaaZ.set(0);
	}

protected void nextFreq60140()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ,6)) {
			goTo(GotoLabel.endLoop60150);
		}
		if (isEQ(t5536rec.billfreq[wsaaZ.toInt()],SPACES)) {
			nextFreq60140();
			return ;
		}
		/* If this is a single premium component then get the frequency*/
		/* of '00' from the allocation table.*/
		if (isEQ(t5687rec.singlePremInd,"Y")
		&& isNE(t5536rec.billfreq[wsaaZ.toInt()],"00")) {
			nextFreq60140();
			return ;
		}
		if (isNE(t5536rec.billfreq[wsaaZ.toInt()],"00")
		&& isNE(t5687rec.singlePremInd,"Y")) {
			nextFreq60140();
			return ;
		}
		/* The appropriate allocation basis details found.*/
		if (isEQ(wsaaZ,1)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ,2)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ,3)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaPcUnits.set(t5536rec.pcUnitscs);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ,4)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaPcUnits.set(t5536rec.pcUnitsds);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ,5)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaPcUnits.set(t5536rec.pcUnitses);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ,6)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
			goTo(GotoLabel.exit60190);
		}
	}

protected void endLoop60150()
	{
		isuallrec.statuz.set(errorsInner.e616);
		exit090();
	}

protected void setUpWriteUtrn61000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para61010();
				case comlvlacc61925: 
					comlvlacc61925();
				case cont61950: 
					cont61950();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para61010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(),isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(),utrnIO.getUnitVirtualFund()))) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnIO.setChdrcoy(isuallrec.company);
		utrnIO.setChdrnum(isuallrec.chdrnum);
		utrnIO.setCoverage(isuallrec.coverage);
		utrnIO.setLife(isuallrec.life);
		utrnIO.setRider("00");
		utrnIO.setPlanSuffix(isuallrec.planSuffix);
		utrnIO.setTranno(chdrlnbIO.getTranno());
		utrnIO.setTransactionTime(varcom.vrcmTime);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		utrnIO.setTransactionDate(datcon1rec.intDate);
		utrnIO.setUser(isuallrec.user);
		utrnIO.setBatccoy(isuallrec.batccoy);
		utrnIO.setBatcbrn(isuallrec.batcbrn);
		utrnIO.setBatcactyr(isuallrec.batcactyr);
		utrnIO.setBatcactmn(isuallrec.batcactmn);
		utrnIO.setBatctrcde(isuallrec.batctrcde);
		utrnIO.setBatcbatch(isuallrec.batcbatch);
		utrnIO.setCrtable(covrunlIO.getCrtable());
		utrnIO.setCntcurr(chdrlnbIO.getCntcurr());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			goTo(GotoLabel.comlvlacc61925);
		}
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
			if (isEQ(wsaaEnhancedAllocInd,"Y")) {
				utrnIO.setSacscode(t5645rec.sacscode05);
				utrnIO.setSacstyp(t5645rec.sacstype05);
				utrnIO.setGenlcde(t5645rec.glmap05);
			}
		}
		goTo(GotoLabel.cont61950);
	}

protected void comlvlacc61925()
	{
		if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode04);
			utrnIO.setSacstyp(t5645rec.sacstype04);
			utrnIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
			utrnIO.setGenlcde(t5645rec.glmap03);
			if (isEQ(wsaaEnhancedAllocInd,"Y")) {
				utrnIO.setSacscode(t5645rec.sacscode06);
				utrnIO.setSacstyp(t5645rec.sacstype06);
				utrnIO.setGenlcde(t5645rec.glmap06);
			}
		}
	}

protected void cont61950()
	{
		utrnIO.setContractType(chdrlnbIO.getCnttype());
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(covrunlIO.getCrrcd());
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(),0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		utrnIO.setMoniesDate(wsaaMoniesDate);
		if (isEQ(isuallrec.rider,NUMERIC)) {
			wsaaRiderNum.set(isuallrec.rider);
		}
		else {
			wsaaRiderNum.set(0);
		}
		setPrecision(utrnIO.getProcSeqNo(), 0);
		utrnIO.setProcSeqNo(add(t6647rec.procSeqNo,wsaaRiderNum));
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setUstmno(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setInciNum(0);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			dbError8100();
		}
		wsaaEnhancedAllocInd.set(SPACES);
	}

protected void matchEnhanceBasis63000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para63010();
					enhana63100();
				case enhanb63200: 
					enhanb63200();
				case enhanc63300: 
					enhanc63300();
				case enhand63400: 
					enhand63400();
				case enhane63500: 
					enhane63500();
				case enhanf63600: 
					enhanf63600();
				case nearExit63800: 
					nearExit63800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para63010()
	{
		/* First match the billing frequency.*/
		/* We are only dealing with Single Prems here, so check for '00'*/
		if (isEQ(t5545rec.billfreq[wsaaIndex.toInt()],SPACES)) {
			goTo(GotoLabel.nearExit63800);
		}
		if (isNE(t5545rec.billfreq[wsaaIndex.toInt()],"00")) {
			goTo(GotoLabel.nearExit63800);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana63100()
	{
		if (isNE(wsaaIndex,1)) {
			goTo(GotoLabel.enhanb63200);
		}
		if ((isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05,0))) {
			wsaaEnhancePerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04,0))) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03,0))) {
					wsaaEnhancePerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02,0))) {
						wsaaEnhancePerc.set(t5545rec.enhanaPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhanb63200()
	{
		if (isNE(wsaaIndex,2)) {
			goTo(GotoLabel.enhanc63300);
		}
		if ((isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05,0))) {
			wsaaEnhancePerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04,0))) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03,0))) {
					wsaaEnhancePerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02,0))) {
						wsaaEnhancePerc.set(t5545rec.enhanbPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhanc63300()
	{
		if (isNE(wsaaIndex,3)) {
			goTo(GotoLabel.enhand63400);
		}
		if ((isGTE(isuallrec.covrSingp,t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05,0))) {
			wsaaEnhancePerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp,t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04,0))) {
				wsaaEnhancePerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp,t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03,0))) {
					wsaaEnhancePerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp,t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02,0))) {
						wsaaEnhancePerc.set(t5545rec.enhancPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhand63400()
	{
		if (isNE(wsaaIndex,4)) {
			goTo(GotoLabel.enhane63500);
		}
		if ((isGTE(isuallrec.covrSingp,t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05,0))) {
			wsaaEnhancePerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp,t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04,0))) {
				wsaaEnhancePerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp,t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03,0))) {
					wsaaEnhancePerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp,t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02,0))) {
						wsaaEnhancePerc.set(t5545rec.enhandPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhane63500()
	{
		if (isNE(wsaaIndex,5)) {
			goTo(GotoLabel.enhanf63600);
		}
		if ((isGTE(isuallrec.covrSingp,t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05,0))) {
			wsaaEnhancePerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp,t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04,0))) {
				wsaaEnhancePerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp,t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03,0))) {
					wsaaEnhancePerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp,t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02,0))) {
						wsaaEnhancePerc.set(t5545rec.enhanePc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhanf63600()
	{
		if (isNE(wsaaIndex,6)) {
			goTo(GotoLabel.nearExit63800);
		}
		if ((isGTE(isuallrec.covrSingp,t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05,0))) {
			wsaaEnhancePerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp,t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04,0))) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp,t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03,0))) {
					wsaaEnhancePerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp,t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02,0))) {
						wsaaEnhancePerc.set(t5545rec.enhanfPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit63800()
	{
		wsaaIndex.add(1);
		/*EXIT*/
	}

protected void fundSplitAllocation64000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit64100();
				case next64150: 
					next64150();
				case accumUnit64200: 
					accumUnit64200();
				case next64250: 
					next64250();
				case nearExit64800: 
					nearExit64800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit64100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaIndex));
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),ulnkrnlIO.getUalfnd(wsaaIndex))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.zfundtyp,"D")) {
			compute(wsaaAmountHoldersInner.wsaaAllocTot, 0).set(add(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaAllocAccum));
			wsaaAmountHoldersInner.wsaaTotIbSplit.add(ulnkrnlIO.getUalprc(wsaaIndex));
			if (isNE(wsaaAmountHoldersInner.wsaaAllocTot, 0)) {
				a100ProcessHitr();
			}
			goTo(GotoLabel.nearExit64800);
		}
		wsaaAmountHoldersInner.wsaaTotUlSplit.add(ulnkrnlIO.getUalprc(wsaaIndex));
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocInit, 0)) {
			goTo(GotoLabel.accumUnit64200);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaIndex,10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
			goTo(GotoLabel.next64150);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex,1)),SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
		}
	}

protected void next64150()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaUtrnContractAmount.set(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("I");
		/*  PERFORM 66000-CHECK-EXISTING-UTRN.                          */
		/*  IF UTRN-STATUZ = O-K                                        */
		/*      ADD WSAA-UTRN-CONTRACT-AMOUNT TO UTRN-CONTRACT-AMOUNT   */
		/*      PERFORM 70000-REWRITE-EXISTING-UTRN                     */
		/*  ELSE                                                        */
		utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaUtrnContractAmount);
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
		setUpWriteUtrn61000();
	}

	/**
	* <pre>
	* Invest the accumulation units premium in the funds specified in
	* the ULNKRNL record. Write a UTRN record for each fund.
	* </pre>
	*/
protected void accumUnit64200()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocAccum, 0)) {
			goTo(GotoLabel.nearExit64800);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaIndex,10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
			goTo(GotoLabel.next64250);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex,1)),SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
			compute(wsaaAmountHoldersInner.wsaaEnhPerFund, 0).set(sub(wsaaAmountHoldersInner.wsaaEnhanceAlloc, wsaaAmountHoldersInner.wsaaEnhanceAmt));
			zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaEnhPerFund);
			a000CallRounding();
			wsaaAmountHoldersInner.wsaaEnhPerFund.set(zrdecplrec.amountOut);
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
			compute(wsaaAmountHoldersInner.wsaaEnhPerFund, 0).set(div(mult(wsaaAmountHoldersInner.wsaaEnhanceAlloc, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
			zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaEnhPerFund);
			a000CallRounding();
			wsaaAmountHoldersInner.wsaaEnhPerFund.set(zrdecplrec.amountOut);
			wsaaAmountHoldersInner.wsaaEnhanceAmt.add(wsaaAmountHoldersInner.wsaaEnhPerFund);
		}
	}

protected void next64250()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaUtrnContractAmount.set(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("A");
		/*  PERFORM 66000-CHECK-EXISTING-UTRN.                          */
		/* If enhance allocation amount is not = 0, the amount is*/
		/* subtracted from the invested amount, since the enhanced alloc*/
		/* is to be written as a separate trx from the invested trx*/
		/*  IF UTRN-STATUZ = O-K                                        */
		/*      ADD WSAA-UTRN-CONTRACT-AMOUNT TO UTRN-CONTRACT-AMOUNT   */
		/*      ADD WSAA-ENH-PER-FUND  TO UTRN-CONTRACT-AMOUNT          */
		/*      PERFORM 70000-REWRITE-EXISTING-UTRN                     */
		/*  ELSE                                                        */
		utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaUtrnContractAmount);
		setPrecision(utrnIO.getContractAmount(), 2);
		utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaEnhPerFund));
		utrnIO.setUnitSubAccount("ACUM");
		utrnIO.setDiscountFactor(0);
		setUpWriteUtrn61000();
		/*  END-IF.                                                     */
		if (isLT(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0)) {
			utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
			utrnIO.setUnitType("A");
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(mult(wsaaAmountHoldersInner.wsaaEnhPerFund, -1));
			utrnIO.setUnitSubAccount("ACUM");
			utrnIO.setDiscountFactor(0);
			wsaaEnhancedAllocInd.set("Y");
			setUpWriteUtrn61000();
		}
	}

protected void nearExit64800()
	{
		wsaaIndex.add(1);
		/*EXIT*/
	}

protected void initUnitDiscFactor65000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para65010();
					wholeOfLife65100();
				case termToRun65200: 
					termToRun65200();
					termFound65250();
				case fixedTerm65300: 
					fixedTerm65300();
				case exit65900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para65010()
	{
		wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(1);
		if (((isEQ(t5540rec.iuDiscBasis,SPACES))
		|| (isEQ(t5540rec.iuDiscFact,SPACES)))
		&& (isEQ(t5540rec.wholeIuDiscFact,SPACES))) {
			goTo(GotoLabel.exit65900);
		}
	}

	/**
	* <pre>
	* There are 3 methods to obtain the discount factor, which one to
	* use is dependent on the field entries in T5540 & T5519 which hav
	* been read at inialisation stage.(Only one method is allowed.)
	* </pre>
	*/
protected void wholeOfLife65100()
	{
		/* If whole of life(T5540-WOL-IU-DISC-FACT) factor is not space,*/
		/* then use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (isEQ(t5540rec.wholeIuDiscFact,SPACES)) {
			goTo(GotoLabel.termToRun65200);
		}
		if (isEQ(covrunlIO.getAnbAtCcd(),0)) {
			goTo(GotoLabel.exit65900);
		}
		getT664610700();
		/*    MOVE T6646-DFACT(COVRUNL-ANB-AT-CCD) TO                      */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(covrunlIO.getAnbAtCcd(), 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.dfact[covrunlIO.getAnbAtCcd().toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(covrunlIO.getAnbAtCcd(), 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit65900);
	}

protected void termToRun65200()
	{
		/* If the discount basis(T5540-IU-DISC-BASIS) is not space, then*/
		/* use this basis to access T5519. If the fixed term(T5519-FIXDTRM)*/
		/* is not zero, use it to read off T5539 to obtain the discount*/
		/* factor, otherwise use the term left to run to read off T5539 to*/
		/* obtain the discount factor. T5539 is accessed by the discount*/
		/* factor(T5540-IU-DISC-FACT) fro T5540.*/
		if (isNE(t5519rec.fixdtrm,0)
		&& (isGT(wsaaRiskCessTerm,t5519rec.fixdtrm))) {
			goTo(GotoLabel.fixedTerm65300);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
	}

protected void termFound65250()
	{
		getT553910900();
		/*    MOVE T5539-DFACT(WSAA-RISK-CESS-TERM)    TO                  */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(wsaaRiskCessTerm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(wsaaRiskCessTerm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit65900);
	}

protected void fixedTerm65300()
	{
		getT553910900();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		return ;
	}

	/**
	* <pre>
	*66000-CHECK-EXISTING-UTRN SECTION.                               
	*66010-PARA.                                                      
	**** MOVE ISUA-COMPANY           TO UTRN-CHDRCOY.                 
	**** MOVE ISUA-CHDRNUM           TO UTRN-CHDRNUM.                 
	**** MOVE ISUA-COVERAGE          TO UTRN-COVERAGE.                
	**** MOVE ISUA-LIFE              TO UTRN-LIFE.                    
	**** MOVE '00'                   TO UTRN-RIDER.                   
	**** MOVE ISUA-PLAN-SUFFIX       TO UTRN-PLAN-SUFFIX.             
	**** MOVE CHDRLNB-TRANNO         TO UTRN-TRANNO.                  
	**** MOVE READH                  TO UTRN-FUNCTION.                
	**** MOVE UTRNREC                TO UTRN-FORMAT.                  
	**** CALL 'UTRNIO' USING UTRN-PARAMS.                             
	**** IF (UTRN-STATUZ NOT = O-K) AND                               
	****    (UTRN-STATUZ NOT = MRNF)                                  
	****     MOVE UTRN-PARAMS        TO SYSR-PARAMS                   
	****     MOVE UTRN-STATUZ        TO SYSR-STATUZ                   
	*69000-EXIT.                                                      
	****   EXIT.                                                      
	*70000-REWRITE-EXISTING-UTRN SECTION.                             
	*70010-PARA.                                                      
	**** MOVE REWRT                  TO UTRN-FUNCTION.                
	**** CALL 'UTRNIO' USING UTRN-PARAMS.                             
	**** IF (UTRN-STATUZ NOT = O-K)                                   
	****     MOVE UTRN-PARAMS        TO SYSR-PARAMS                   
	****     MOVE UTRN-STATUZ        TO SYSR-STATUZ                   
	*70090-EXIT.                                                      
	****   EXIT.                                                      
	* </pre>
	*/
protected void readAgecont80000()
	{
		/*READ-AGECONT*/
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
		/*EXIT*/
	}

protected void readTermcont90000()
	{
		/*READ-TERMCONT*/
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void a100ProcessHitr()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrIO.setParams(SPACES);
		if (isEQ(wsaaIndex,10)) {
			setPrecision(hitrIO.getContractAmount(), 0);
			hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex,1)),SPACES)) {
				setPrecision(hitrIO.getContractAmount(), 0);
				hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
			}
			else {
				setPrecision(hitrIO.getContractAmount(), 1);
				hitrIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocTot, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
				compute(wsaaAmountHoldersInner.wsaaInitAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
				compute(wsaaAmountHoldersInner.wsaaAccumAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
			}
		}
		zrdecplrec.amountIn.set(hitrIO.getContractAmount());
		a000CallRounding();
		hitrIO.setContractAmount(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaInitAmount);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaInitAmount.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAccumAmount);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAccumAmount.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunTot.add(hitrIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunInitTot.add(wsaaAmountHoldersInner.wsaaInitAmount);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(wsaaAmountHoldersInner.wsaaAccumAmount);
		hitrIO.setZintbfnd(ulnkrnlIO.getUalfnd(wsaaIndex));
		wsaaAmountHoldersInner.wsaaHitrContractAmount.set(hitrIO.getContractAmount());
		hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaHitrContractAmount);
		a200SetUpWriteHitr();
	}

protected void a200SetUpWriteHitr()
	{
		a210Para();
	}

protected void a210Para()
	{
		hitrIO.setChdrcoy(isuallrec.company);
		hitrIO.setChdrnum(isuallrec.chdrnum);
		hitrIO.setCoverage(isuallrec.coverage);
		hitrIO.setLife(isuallrec.life);
		hitrIO.setRider("00");
		hitrIO.setPlanSuffix(isuallrec.planSuffix);
		hitrIO.setTranno(chdrlnbIO.getTranno());
		hitrIO.setBatccoy(isuallrec.batccoy);
		hitrIO.setBatcbrn(isuallrec.batcbrn);
		hitrIO.setBatcactyr(isuallrec.batcactyr);
		hitrIO.setBatcactmn(isuallrec.batcactmn);
		hitrIO.setBatctrcde(isuallrec.batctrcde);
		hitrIO.setBatcbatch(isuallrec.batcbatch);
		hitrIO.setCrtable(covrunlIO.getCrtable());
		hitrIO.setCntcurr(chdrlnbIO.getCntcurr());
		if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
			if (isEQ(t5688rec.comlvlacc,"Y")) {
				hitrIO.setSacscode(t5645rec.sacscode10);
				hitrIO.setSacstyp(t5645rec.sacstype10);
				hitrIO.setGenlcde(t5645rec.glmap10);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode08);
				hitrIO.setSacstyp(t5645rec.sacstype08);
				hitrIO.setGenlcde(t5645rec.glmap08);
			}
		}
		else {
			if (isEQ(t5688rec.comlvlacc,"Y")) {
				hitrIO.setSacscode(t5645rec.sacscode09);
				hitrIO.setSacstyp(t5645rec.sacstype09);
				hitrIO.setGenlcde(t5645rec.glmap09);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode07);
				hitrIO.setSacstyp(t5645rec.sacstype07);
				hitrIO.setGenlcde(t5645rec.glmap07);
			}
		}
		hitrIO.setCnttyp(chdrlnbIO.getCnttype());
		hitrIO.setSvp(1);
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciNum(ZERO);
		if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
			hitrIO.setZrectyp("N");
		}
		else {
			hitrIO.setZrectyp("P");
		}
		hitrIO.setEffdate(isuallrec.effdate);
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZintrate(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(formatsInner.hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			syserrrec.statuz.set(hitrIO.getStatuz());
			dbError8100();
		}
	}

protected void a500UpdateHitd()
	{
		a510Hitd();
	}

protected void a510Hitd()
	{
		/* Read ULNKRNL to get the fund split for the coverage/rider       */
		ulnkrnlIO.setParams(SPACES);
		ulnkrnlIO.setChdrcoy(isuallrec.company);
		ulnkrnlIO.setChdrnum(isuallrec.chdrnum);
		ulnkrnlIO.setLife(isuallrec.life);
		ulnkrnlIO.setCoverage(isuallrec.coverage);
		ulnkrnlIO.setRider(isuallrec.rider);
		ulnkrnlIO.setPlanSuffix(isuallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(formatsInner.ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkrnlIO.getParams());
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			dbError8100();
		}
		/* For each interest bearing fund found on the ULNK, check if      */
		/* a corresponding HITD record exists.                             */
		wsaaIndex.set(1);
		while ( !(isGT(wsaaIndex,10)
		|| isEQ(ulnkrnlIO.getUalfnd(wsaaIndex),SPACES))) {
			a600CheckInterestBearing();
		}
		
	}

protected void a600CheckInterestBearing()
	{
					a610Check();
					a680Next();
				}

protected void a610Check()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaIndex));
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),ulnkrnlIO.getUalfnd(wsaaIndex))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			return ;
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isNE(t5515rec.zfundtyp,"D")) {
			return ;
		}
		hitdIO.setParams(SPACES);
		hitdIO.setChdrcoy(isuallrec.company);
		hitdIO.setChdrnum(isuallrec.chdrnum);
		hitdIO.setLife(isuallrec.life);
		hitdIO.setCoverage(isuallrec.coverage);
		hitdIO.setRider(isuallrec.rider);
		hitdIO.setPlanSuffix(isuallrec.planSuffix);
		hitdIO.setZintbfnd(ulnkrnlIO.getUalfnd(wsaaIndex));
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)
		&& isNE(hitdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hitdIO.getParams());
			syserrrec.statuz.set(hitdIO.getStatuz());
			dbError8100();
		}
		if (isEQ(hitdIO.getStatuz(),varcom.oK)) {
			return ;
		}
		hitdIO.setZlstintdte(varcom.vrcmMaxDate);
		hitdIO.setZlstsmtdt(varcom.vrcmMaxDate);
		hitdIO.setZlstfndval(ZERO);
		hitdIO.setZlstsmtno(ZERO);
		hitdIO.setZlstsmtbal(ZERO);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.th510);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaIndex));
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th510)
		|| isNE(itdmIO.getItemitem(),ulnkrnlIO.getUalfnd(wsaaIndex))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemtabl(tablesInner.th510);
			itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaIndex));
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		th510rec.th510Rec.set(itdmIO.getGenarea());
		a800GetNextFreq();
		hitdIO.setZnxtintdte(datcon2rec.intDate2);
		hitdIO.setTranno(chdrlnbIO.getTranno());
		hitdIO.setValidflag("1");
		hitdIO.setEffdate(isuallrec.effdate);
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			syserrrec.statuz.set(hitdIO.getStatuz());
			dbError8100();
		}
	}

protected void a680Next()
	{
		wsaaIndex.add(1);
		/*A690-EXIT*/
	}

protected void a800GetNextFreq()
	{
		a810Next();
	}

protected void a810Next()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(isuallrec.effdate);
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(th510rec.zintalofrq);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError8000();
		}
		if (isNE(th510rec.zintfixdd,ZERO)) {
			/*     MOVE TH510-ZINTFIXDD    TO DTC2-INT-DATE-2 (5:2) <LA4737>*/
			datcon2rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}

protected void systemError8000()
	{
			se8000();
			seExit8090();
		}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
			db8100();
			dbExit8190();
		}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(isuallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		zrdecplrec.batctrcde.set(isuallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*A900-EXIT*/
	}

protected void b100ProcessTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b100Start();
					b100CreateTaxd();
				case b100CreateZrst: 
					b100CreateZrst();
				case b100Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b100Start()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0)) {
			goTo(GotoLabel.b100Exit);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(isuallrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				dbError8100();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.b100Exit);
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covrunlIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(isuallrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				dbError8100();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(isuallrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				dbError8100();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		if (isNE(tr52erec.taxind05, "Y")) {
			goTo(GotoLabel.b100Exit);
		}
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covrunlIO.getLife());
		txcalcrec.coverage.set(covrunlIO.getCoverage());
		txcalcrec.rider.set(covrunlIO.getRider());
		txcalcrec.planSuffix.set(covrunlIO.getPlanSuffix());
		txcalcrec.crtable.set(covrunlIO.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.batckey.set(isuallrec.batchkey);
		txcalcrec.tranno.set(chdrlnbIO.getTranno());
		txcalcrec.language.set(isuallrec.language);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.transType.set("NINV");
		txcalcrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		txcalcrec.effdate.set(isuallrec.effdate);
		txcalcrec.tranno.set(chdrlnbIO.getTranno());
		txcalcrec.jrnseq.set(0);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			dbError8100();
		}
		if (isEQ(txcalcrec.taxAmt[1], 0)
		&& isEQ(txcalcrec.taxAmt[2], 0)) {
			goTo(GotoLabel.b100Exit);
		}
	}

protected void b100CreateTaxd()
	{
		/*   Tax on non invested premium may be calcuated on both reg     */
		/*   premium and singple premium in case where there is lump sum  */
		/*   at issue.  We need to add the tax on single premium to       */
		/*   the reg prem non invest tax if the records has alread been   */
		/*   created.                                                     */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(covrunlIO.getChdrcoy());
		taxdIO.setChdrnum(covrunlIO.getChdrnum());
		taxdIO.setLife(covrunlIO.getLife());
		taxdIO.setCoverage(covrunlIO.getCoverage());
		taxdIO.setRider(covrunlIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setInstfrom(isuallrec.effdate);
		taxdIO.setTrantype("NINV");
		taxdIO.setFunction(varcom.readh);
		taxdIO.setFormat(formatsInner.taxdrec);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)
		&& isNE(taxdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			dbError8100();
		}
		if (isEQ(taxdIO.getStatuz(), varcom.oK)) {
			setPrecision(taxdIO.getBaseamt(), 2);
			taxdIO.setBaseamt(add(taxdIO.getBaseamt(), txcalcrec.amountIn));
			setPrecision(taxdIO.getTaxamt01(), 2);
			taxdIO.setTaxamt01(add(taxdIO.getTaxamt01(), txcalcrec.taxAmt[1]));
			setPrecision(taxdIO.getTaxamt02(), 2);
			taxdIO.setTaxamt02(add(taxdIO.getTaxamt02(), txcalcrec.taxAmt[2]));
			taxdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, taxdIO);
			if (isNE(taxdIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdIO.getParams());
				syserrrec.statuz.set(taxdIO.getStatuz());
				dbError8100();
			}
			goTo(GotoLabel.b100CreateZrst);
		}
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(covrunlIO.getChdrcoy());
		taxdIO.setChdrnum(covrunlIO.getChdrnum());
		taxdIO.setLife(covrunlIO.getLife());
		taxdIO.setCoverage(covrunlIO.getCoverage());
		taxdIO.setRider(covrunlIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(isuallrec.effdate);
		taxdIO.setInstfrom(isuallrec.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(chdrlnbIO.getTranno());
		taxdIO.setTrantype("NINV");
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setBillcd(chdrlnbIO.getBillcd());
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			dbError8100();
		}
	}

protected void b100CreateZrst()
	{
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax[1].set(0);
		}
		else {
			wsaaTax[1].set(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax[2].set(0);
		}
		else {
			wsaaTax[2].set(txcalcrec.taxAmt[2]);
		}
		zrstIO.setDataArea(SPACES);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setCoverage(SPACES);
		zrstIO.setRider(SPACES);
		zrstIO.setFormat(formatsInner.zrstrec);
		zrstIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
			b200ReadZrst();
		}
		
		compute(wsaaTotCd, 2).add(add(wsaaTax[1], wsaaTax[2]));
		b300WriteZrst();
		if (isEQ(wsaaTax[1], ZERO)
		&& isEQ(wsaaTax[2], ZERO)) {
			return ;
		}
		/*    INITIALIZE                     TAXD-DATA-AREA.               */
		zrstIO.setDataArea(SPACES);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setCoverage(SPACES);
		zrstIO.setRider(SPACES);
		zrstIO.setFormat(formatsInner.zrstrec);
		zrstIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
			b400UpdateZrst();
		}
		
		/* Post coverage debt                                              */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(isuallrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(isuallrec.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(isuallrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(isuallrec.chdrnum);
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		lifacmvrec.batccoy.set(isuallrec.company);
		lifacmvrec.rldgcoy.set(isuallrec.company);
		lifacmvrec.genlcoy.set(isuallrec.company);
		lifacmvrec.batcactyr.set(isuallrec.batcactyr);
		lifacmvrec.batctrcde.set(isuallrec.batctrcde);
		lifacmvrec.batcactmn.set(isuallrec.batcactmn);
		lifacmvrec.batcbatch.set(isuallrec.batcbatch);
		lifacmvrec.batcbrn.set(isuallrec.batcbrn);
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(add(wsaaTax[1], wsaaTax[2]));
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(isuallrec.effdate);
		/* Set up TRANREF to contain the Full component key of the         */
		/* Coverage/Rider that is Creating the debt so that if a           */
		/* reversal is required at a later date, the correct component     */
		/* can be identified.                                              */
		wsaaTref.set(SPACES);
		wsaaTrefChdrcoy.set(isuallrec.company);
		wsaaTrefChdrnum.set(isuallrec.chdrnum);
		wsaaPlan.set(isuallrec.planSuffix);
		wsaaTrefLife.set(isuallrec.life);
		wsaaTrefCoverage.set(isuallrec.coverage);
		wsaaTrefRider.set(isuallrec.rider);
		wsaaTrefPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaTref);
		lifacmvrec.user.set(isuallrec.user);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* Check for Component Level accounting and act accordingly        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode12);
			lifacmvrec.sacstyp.set(t5645rec.sacstype12);
			lifacmvrec.glsign.set(t5645rec.sign12);
			lifacmvrec.glcode.set(t5645rec.glmap12);
			lifacmvrec.contot.set(t5645rec.cnttot12);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(isuallrec.chdrnum);
			wsaaPlan.set(isuallrec.planSuffix);
			wsaaRldgLife.set(isuallrec.life);
			wsaaRldgCoverage.set(isuallrec.coverage);
			/* Post ACMV against the Coverage, not Rider since the debt is also*/
			/* set against Coverage.                                           */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/* Set correct substitution code                                   */
			lifacmvrec.substituteCode[6].set(covrunlIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode11);
			lifacmvrec.sacstyp.set(t5645rec.sacstype11);
			lifacmvrec.glsign.set(t5645rec.sign11);
			lifacmvrec.glcode.set(t5645rec.glmap11);
			lifacmvrec.contot.set(t5645rec.cnttot11);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(isuallrec.chdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec.transactionDate.set(isuallrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError8000();
		}
	}

protected void b200ReadZrst()
	{
		b200Start();
	}

protected void b200Start()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			dbError8100();
		}
		if (isNE(zrstIO.getChdrcoy(), covrunlIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), covrunlIO.getChdrnum())
		|| isNE(zrstIO.getLife(), covrunlIO.getLife())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getFeedbackInd(), SPACES)) {
			wsaaTotCd.add(zrstIO.getZramount01());
		}
		zrstIO.setFunction(varcom.nextr);
	}

protected void b300WriteZrst()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b300WriteZrstPara();
				case b300WriteTax2: 
					b300WriteTax2();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*B300-WRITE-ZRST.                                         <S19FIX>
	* </pre>
	*/
protected void b300WriteZrstPara()
	{
		zrstIO.setParams(SPACES);
		zrstIO.setZramount01(0);
		zrstIO.setZramount02(0);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setJlife(SPACES);
		zrstIO.setCoverage(covrunlIO.getCoverage());
		zrstIO.setRider(covrunlIO.getRider());
		zrstIO.setBatctrcde(isuallrec.batctrcde);
		zrstIO.setTranno(isuallrec.newTranno);
		zrstIO.setTrandate(isuallrec.effdate);
		wsaaSeqno.add(1);
		zrstIO.setXtranno(ZERO);
		zrstIO.setSeqno(wsaaSeqno);
		zrstIO.setUstmno(ZERO);
		zrstIO.setFormat(formatsInner.zrstrec);
		if (isEQ(wsaaTax[1], 0)) {
			goTo(GotoLabel.b300WriteTax2);
		}
		zrstIO.setZramount01(wsaaTax[1]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP.         <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[1]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			dbError8100();
		}
	}

protected void b300WriteTax2()
	{
		if (isEQ(wsaaTax[2], 0)) {
			return ;
		}
		/*    MOVE WSAA-TAX(1)            TO ZRST-ZRAMOUNT01       <LA4895>*/
		zrstIO.setZramount01(wsaaTax[2]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP.         <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[2]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			dbError8100();
		}
	}

protected void b400UpdateZrst()
	{
		b410UpdateZrst();
	}

protected void b410UpdateZrst()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			dbError8100();
		}
		if (isNE(zrstIO.getChdrcoy(), covrunlIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), covrunlIO.getChdrnum())
		|| isNE(zrstIO.getLife(), covrunlIO.getLife())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getFeedbackInd(), SPACES)) {
			zrstIO.setZramount02(wsaaTotCd);
			zrstIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, zrstIO);
			if (isNE(zrstIO.getStatuz(), varcom.oK)
			&& isNE(zrstIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(zrstIO.getParams());
				syserrrec.statuz.set(zrstIO.getStatuz());
				dbError8100();
			}
		}
		zrstIO.setFunction(varcom.nextr);
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaUtrnContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAmt = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhPerFund = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaHitrContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaTotUlSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaTotIbSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaInitAmount = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAccumAmount = new ZonedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaAllocTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunTot = new ZonedDecimalData(18, 2).init(0);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
public static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e615 = new FixedLengthStringData(4).init("E615");
	private FixedLengthStringData e616 = new FixedLengthStringData(4).init("E616");
	private FixedLengthStringData e706 = new FixedLengthStringData(4).init("E706");
	private FixedLengthStringData e707 = new FixedLengthStringData(4).init("E707");
	private FixedLengthStringData g027 = new FixedLengthStringData(4).init("G027");
	public FixedLengthStringData g029 = new FixedLengthStringData(4).init("G029");
	private FixedLengthStringData g032 = new FixedLengthStringData(4).init("G032");
	private FixedLengthStringData g033 = new FixedLengthStringData(4).init("G033");
	private FixedLengthStringData g513 = new FixedLengthStringData(4).init("G513");
	private FixedLengthStringData h053 = new FixedLengthStringData(4).init("H053");
	private FixedLengthStringData h055 = new FixedLengthStringData(4).init("H055");
	private FixedLengthStringData h115 = new FixedLengthStringData(4).init("H115");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData ulnkrec = new FixedLengthStringData(10).init("ULNKREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData ulnkrnlrec = new FixedLengthStringData(10).init("ULNKRNLREC");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData incirec = new FixedLengthStringData(10).init("INCIREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitdrec = new FixedLengthStringData(10).init("HITDREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).init("ZRSTREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
public static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5544 = new FixedLengthStringData(5).init("T5544");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5535 = new FixedLengthStringData(5).init("T5535");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	public FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData th510 = new FixedLengthStringData(5).init("TH510");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
}
}
