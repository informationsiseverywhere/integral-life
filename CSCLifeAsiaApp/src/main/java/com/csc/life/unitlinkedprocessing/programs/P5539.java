/*
 * File: P5539.java
 * Date: 30 August 2009 0:30:09
 * Author: Quipoz Limited
 * 
 * Class transformed from P5539.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.screens.S5539ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5539 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5539");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5539rec t5539rec = new T5539rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5539ScreenVars sv = ScreenProgram.getScreenVars( S5539ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		other3080, 
		exit3090
	}

	public P5539() {
		super();
		screenVars = sv;
		new ScreenModel("S5539", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itemIO.getGenarea(), SPACES)) {
			return ;
		}
		t5539rec.dfact01.set(ZERO);
		t5539rec.dfact02.set(ZERO);
		t5539rec.dfact03.set(ZERO);
		t5539rec.dfact04.set(ZERO);
		t5539rec.dfact05.set(ZERO);
		t5539rec.dfact06.set(ZERO);
		t5539rec.dfact07.set(ZERO);
		t5539rec.dfact08.set(ZERO);
		t5539rec.dfact09.set(ZERO);
		t5539rec.dfact10.set(ZERO);
		t5539rec.dfact11.set(ZERO);
		t5539rec.dfact12.set(ZERO);
		t5539rec.dfact13.set(ZERO);
		t5539rec.dfact14.set(ZERO);
		t5539rec.dfact15.set(ZERO);
		t5539rec.dfact16.set(ZERO);
		t5539rec.dfact17.set(ZERO);
		t5539rec.dfact18.set(ZERO);
		t5539rec.dfact19.set(ZERO);
		t5539rec.dfact20.set(ZERO);
		t5539rec.dfact21.set(ZERO);
		t5539rec.dfact22.set(ZERO);
		t5539rec.dfact23.set(ZERO);
		t5539rec.dfact24.set(ZERO);
		t5539rec.dfact25.set(ZERO);
		t5539rec.dfact26.set(ZERO);
		t5539rec.dfact27.set(ZERO);
		t5539rec.dfact28.set(ZERO);
		t5539rec.dfact29.set(ZERO);
		t5539rec.dfact30.set(ZERO);
		t5539rec.dfact31.set(ZERO);
		t5539rec.dfact32.set(ZERO);
		t5539rec.dfact33.set(ZERO);
		t5539rec.dfact34.set(ZERO);
		t5539rec.dfact35.set(ZERO);
		t5539rec.dfact36.set(ZERO);
		t5539rec.dfact37.set(ZERO);
		t5539rec.dfact38.set(ZERO);
		t5539rec.dfact39.set(ZERO);
		t5539rec.dfact40.set(ZERO);
		t5539rec.dfact41.set(ZERO);
		t5539rec.dfact42.set(ZERO);
		t5539rec.dfact43.set(ZERO);
		t5539rec.dfact44.set(ZERO);
		t5539rec.dfact45.set(ZERO);
		t5539rec.dfact46.set(ZERO);
		t5539rec.dfact47.set(ZERO);
		t5539rec.dfact48.set(ZERO);
		t5539rec.dfact49.set(ZERO);
		t5539rec.dfact50.set(ZERO);
		t5539rec.dfact51.set(ZERO);
		t5539rec.dfact52.set(ZERO);
		t5539rec.dfact53.set(ZERO);
		t5539rec.dfact54.set(ZERO);
		t5539rec.dfact55.set(ZERO);
		t5539rec.dfact56.set(ZERO);
		t5539rec.dfact57.set(ZERO);
		t5539rec.dfact58.set(ZERO);
		t5539rec.dfact59.set(ZERO);
		t5539rec.dfact60.set(ZERO);
		t5539rec.dfact61.set(ZERO);
		t5539rec.dfact62.set(ZERO);
		t5539rec.dfact63.set(ZERO);
		t5539rec.dfact64.set(ZERO);
		t5539rec.dfact65.set(ZERO);
		t5539rec.dfact66.set(ZERO);
		t5539rec.dfact67.set(ZERO);
		t5539rec.dfact68.set(ZERO);
		t5539rec.dfact69.set(ZERO);
		t5539rec.dfact70.set(ZERO);
		t5539rec.dfact71.set(ZERO);
		t5539rec.dfact72.set(ZERO);
		t5539rec.dfact73.set(ZERO);
		t5539rec.dfact74.set(ZERO);
		t5539rec.dfact75.set(ZERO);
		t5539rec.dfact76.set(ZERO);
		t5539rec.dfact77.set(ZERO);
		t5539rec.dfact78.set(ZERO);
		t5539rec.dfact79.set(ZERO);
		t5539rec.dfact80.set(ZERO);
		t5539rec.dfact81.set(ZERO);
		t5539rec.dfact82.set(ZERO);
		t5539rec.dfact83.set(ZERO);
		t5539rec.dfact84.set(ZERO);
		t5539rec.dfact85.set(ZERO);
		t5539rec.dfact86.set(ZERO);
		t5539rec.dfact87.set(ZERO);
		t5539rec.dfact88.set(ZERO);
		t5539rec.dfact89.set(ZERO);
		t5539rec.dfact90.set(ZERO);
		t5539rec.dfact91.set(ZERO);
		t5539rec.dfact92.set(ZERO);
		t5539rec.dfact93.set(ZERO);
		t5539rec.dfact94.set(ZERO);
		t5539rec.dfact95.set(ZERO);
		t5539rec.dfact96.set(ZERO);
		t5539rec.dfact97.set(ZERO);
		t5539rec.dfact98.set(ZERO);
		t5539rec.dfact99.set(ZERO);
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 11); loopVar1 += 1){
			initFacts1500();
		}
	}

protected void generalArea1045()
	{
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 11); loopVar2 += 1){
			moveFacts1600();
		}
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 99); loopVar3 += 1){
			moveDfacts1700();
		}
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void initFacts1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t5539rec.fact[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveFacts1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(t5539rec.fact[wsaaSub1.toInt()], NUMERIC)) {
			sv.fact[wsaaSub1.toInt()].set(t5539rec.fact[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

protected void moveDfacts1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(t5539rec.dfact[wsaaSub1.toInt()], NUMERIC)) {
			sv.dfact[wsaaSub1.toInt()].set(t5539rec.dfact[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5539IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5539-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
		itemIO.setGenarea(t5539rec.t5539Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		/*CHECK*/
		/*    IF S5539-DFACTS                  NOT =                       */
		/*       T5539-DFACTS                                              */
		/*        MOVE S5539-DFACTS                                        */
		/*          TO T5539-DFACTS                                        */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 99); loopVar4 += 1){
			updateDfacts3300();
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 11); loopVar5 += 1){
			updateFacts3500();
		}
		/*EXIT*/
	}

protected void updateDfacts3300()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.dfact[wsaaSub1.toInt()], t5539rec.dfact[wsaaSub1.toInt()])) {
			t5539rec.dfact[wsaaSub1.toInt()].set(sv.dfact[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateFacts3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.fact[wsaaSub1.toInt()], t5539rec.fact[wsaaSub1.toInt()])) {
			t5539rec.fact[wsaaSub1.toInt()].set(sv.fact[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
