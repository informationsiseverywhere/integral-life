package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:24
 * Description:
 * Copybook name: ZRSTTRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrsttrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrsttrnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrsttrnKey = new FixedLengthStringData(64).isAPartOf(zrsttrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrsttrnChdrcoy = new FixedLengthStringData(1).isAPartOf(zrsttrnKey, 0);
  	public FixedLengthStringData zrsttrnChdrnum = new FixedLengthStringData(8).isAPartOf(zrsttrnKey, 1);
  	public FixedLengthStringData zrsttrnLife = new FixedLengthStringData(2).isAPartOf(zrsttrnKey, 9);
  	public FixedLengthStringData zrsttrnCoverage = new FixedLengthStringData(2).isAPartOf(zrsttrnKey, 11);
  	public PackedDecimalData zrsttrnXtranno = new PackedDecimalData(5, 0).isAPartOf(zrsttrnKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(zrsttrnKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrsttrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrsttrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}