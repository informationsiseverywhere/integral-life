/*
 * File: B5106.java
 * Date: 29 August 2009 20:56:34
 * Author: Quipoz Limited
 * 
 * Class transformed from B5106.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UreppfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ureppf;
import com.csc.life.unitlinkedprocessing.reports.R5106Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   Unit Movement Listing.
*
*   This batch program can be included as the last step in UNITDEAL.
*   It reports on all the UTRN records that have been processed in
*   the most recent (or one specified) run of the dealing program.
*   The precise name and number of the schedule that the program
*   is concerned with are held on the system parameters 01 and 02.
*   '*LAST' is specified for the schedule number when the most
*   recent run is of interest.
*
*   The UTRN records are extracted using an SQL block fetch with
*   criteria of:  CHDRCOY = BSPR-COMPANY
*                 FDBKIND = 'Y'
*                 BSCHEDNAM = BPRD-system-parameter01
*                        or = BSPR schedname if param01 = spaces
*                 BSCHEDNUM = BPRD-system-parameter02
*      order by:  CHDRCOY, VRTFND, UNITYP, CHDRNUM, BATCTRCDE
*
*   The report is set up in the 3000- section. Headings are written
*   for the first record, on a change of fund or type or when the
*   end of a page has been reached. Detail lines are produced when
*   the fund and unit type have the same value as the previous
*   record; total lines on change of fund/type.
*
*****************************************************************
* </pre>
*/
public class B5106 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlutrnCursorrs = null;
	private java.sql.PreparedStatement sqlutrnCursorps = null;
	private java.sql.Connection sqlutrnCursorconn = null;
	private R5106Report printerFile = new R5106Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5106");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaNewHeadPrinted = "N";
	private FixedLengthStringData wsaaLastVrtfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLastUnityp = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init("?", true);
	private FixedLengthStringData wsaaContractStart = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaContractEnd = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaProcflg = new FixedLengthStringData(1).init(" ");

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRec = new Validator(wsaaFirstRecord, "Y");

	private FixedLengthStringData wsaaFirstHeader = new FixedLengthStringData(1);
	private Validator firstHeader = new Validator(wsaaFirstHeader, "Y");
		/* SQLA-CODES */
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler, 8);
	private PackedDecimalData wsaaTotFundAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotDeemUnits = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaTotRealUnits = new PackedDecimalData(16, 5);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*01  WSAA-SCHEDNUM-NUM REDEFINES                                  */
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
		/* Subscript for fields fetched by SQL*/
	private PackedDecimalData isql = new PackedDecimalData(5, 0).init(ZERO);
	private int sqlerrd = 0;
	
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t1693 = "T1693";
	private static final String t5515 = "T5515";
	private static final String t3629 = "T3629";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5106h01Record = new FixedLengthStringData(99);
	private FixedLengthStringData r5106h01O = new FixedLengthStringData(99).isAPartOf(r5106h01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5106h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5106h01O, 1);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r5106h01O, 31);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r5106h01O, 35);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(r5106h01O, 65);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30).isAPartOf(r5106h01O, 68);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(1).isAPartOf(r5106h01O, 98);

	private FixedLengthStringData r5106h02Record = new FixedLengthStringData(2);

	private FixedLengthStringData r5106d01Record = new FixedLengthStringData(80);
	private FixedLengthStringData r5106d01O = new FixedLengthStringData(80).isAPartOf(r5106d01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5106d01O, 0);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2).isAPartOf(r5106d01O, 8);
	private ZonedDecimalData priceused = new ZonedDecimalData(9, 5).isAPartOf(r5106d01O, 25);
	private ZonedDecimalData nofrunts = new ZonedDecimalData(16, 5).isAPartOf(r5106d01O, 34);
	private FixedLengthStringData pricedt = new FixedLengthStringData(10).isAPartOf(r5106d01O, 50);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5106d01O, 60);
	private ZonedDecimalData nofdunts = new ZonedDecimalData(16, 5).isAPartOf(r5106d01O, 64);

		/*  Null record, written when no transactions are detected for sche*/
	private FixedLengthStringData r5106n01Record = new FixedLengthStringData(12);
	private FixedLengthStringData r5106n01O = new FixedLengthStringData(10).isAPartOf(r5106n01Record, 0);
	private FixedLengthStringData schalph = new FixedLengthStringData(10).isAPartOf(r5106n01O, 0);

	private FixedLengthStringData r5106t01Record = new FixedLengthStringData(50);
	private FixedLengthStringData r5106t01O = new FixedLengthStringData(50).isAPartOf(r5106t01Record, 0);
	private ZonedDecimalData totfndamt = new ZonedDecimalData(18, 2).isAPartOf(r5106t01O, 0);
	private ZonedDecimalData totrunts = new ZonedDecimalData(16, 5).isAPartOf(r5106t01O, 18);
	private ZonedDecimalData totdunts = new ZonedDecimalData(16, 5).isAPartOf(r5106t01O, 34);

	private FixedLengthStringData r5106e01Record = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getdescrec getdescrec = new Getdescrec();
	private T5515rec t5515rec = new T5515rec();
	private P6671par p6671par = new P6671par();
	private SqlUtrnpfInner sqlUtrnpfInner = new SqlUtrnpfInner();
	
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private int ct01Value = 0;
	private int ct02Value = 0;
	
    private Map<String, List<Itempf>> t5515Map = null;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private UreppfDAO ureppfDAO = getApplicationContext().getBean("ureppfDAO", UreppfDAO.class);
    private List<Ureppf> updateUreppfList = null;
    

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfCursor2080, 
		exit2090
	}

	public B5106() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** There is no additional restart processing to be done.*/
		/*EXIT*/
	}

protected void initialise1000()
 {

		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		indicArea.set("0");
		wsaaFirstRecord.set("Y");
		wsaaFirstHeader.set("Y");
		fundamnt.set(ZERO);
		priceused.set(ZERO);
		nofrunts.set(ZERO);
		pricedt.set(ZERO);
		nofdunts.set(ZERO);
		/* NOFDUNTS OF R5106D01-O */
		/* BSCHEDNUM OF R5106H01-O. */
		/* Get company name. */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(bsprIO.getCompany());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			wsaaCompanynm.fill("?");
		} else {
			wsaaCompanynm.set(getdescrec.longdesc);
		}
		printerFile.openOutput();
		/* Information about the schedule name and number is held in the */
		/* BPRD-parameters 1 and 2. Under normal circumstances the user wil */
		/* be interested in fund movements relating to the most recently ru */
		/* schedule and BPRD-PARAM02 will be set to *LAST. The LAST-JOB-NO */
		/* section will return this number. set to *LAST. */
		/* The user could also specify a schedule number for which he/sh */
		/* wants fund movement reports. */
		/* <D9703> */
		/* The following code is no longer relevant as a result of the */
		/* reporting change for Contract Windforward. <D9703> */
		/* <D9703> */
		/* IF BPRD-SYSTEM-PARAM01 = SPACES <D9703> */
		/* MOVE BSPR-SCHEDULE-NAME TO WSAA-SCHEDNAME <D9703> */
		/* BSCHEDNAM OF R5106H01-O */
		/* ELSE <D9703> */
		/* MOVE BPRD-SYSTEM-PARAM01 TO WSAA-SCHEDNAME <D9703> */
		/* BSCHEDNAM OF R5106H01-O. */
		/* <D9703> */
		/* IF BPRD-SYSTEM-PARAM02 = SPACES <D9703> */
		/* MOVE BSPR-SCHEDULE-NUMBER TO WSAA-SCHEDNUM-ALPHA<D9703> */
		/* BSCHEDNUM OF R5106H01-O */
		/* ELSE <D9703> */
		/* MOVE BPRD-SYSTEM-PARAM02 TO WSAA-SCHEDNUM-ALPHA. <D9703> */
		/* <D9703> */
		/* IF WSAA-SCHEDNUM-ALPHA = '*LAST' <D9703> */
		/* PERFORM 1100-LAST-JOB-NO. <D9703> */
		/* The schedule number starts off as alphanumeric data and if vali */
		/* must eventually be in a COMP-3 format - in WSAA-SCHEDNUM-COMP h */
		/* This conversion is started with the REDEFINES in the w.s.sectio */
		/* This is necessary for later comparison with the BSCHEDNUM field */
		/* the SQL-SELECT statement. */
		/* IF WSAA-SCHEDNUM-ALPHA NUMERIC <D9703> */
		/* MOVE WSAA-SCHEDNUM-NUM TO WSAA-SCHEDNUM-COMP <D9703> */
		/* BSCHEDNUM OF R5106H01-O */
		/* END-IF. <D9703> */
		/* <D9703> */
		/* IF WSAA-SCHEDNUM-ALPHA NOT = '*LAST' <D9703> */
		/* AND WSAA-SCHEDNUM-ALPHA NOT NUMERIC <D9703> */
		/* MOVE G159 TO CONL-ERROR <D9703> */
		/* MOVE BPRD-PARAMS TO CONL-PARAMS <D9703> */
		/* PERFORM 003-CALL-CONLOG <D9703> */
		/* PERFORM 3030-PAGE-HEADING <D9703> */
		/* MOVE 'N' TO WSAA-FIRST-HEADER <D9703> */
		/* SET IND-ON(20) TO TRUE <D9703> */
		/* <D9703> */
		/* IF WSAA-SCHEDNUM-ALPHA NOT = SPACES <D9703> */
		/* MOVE WSAA-SCHEDNUM-ALPHA TO SCHALPH OF R5106N01-O */
		/* ELSE MOVE '(BLANK)' TO SCHALPH OF R5106N01-O */
		/* <D9703> */
		/* WRITE PRINTER-REC FROM R5106N01-RECORD <D9703> */
		/* FORMAT 'R5106N01' <D9703> */
		/* INDICATORS INDIC-AREA <D9703> */
		/* WRITE PRINTER-REC FROM R5106D01-RECORD <D9703> */
		/* FORMAT 'R5106D01' <D9703> */
		/* INDICATORS INDIC-AREA <D9703> */
		/* MOVE ENDP TO WSSP-EDTERROR <D9703> */
		/* GO TO 1090-EXIT <D9703> */
		/* END-IF. <D9703> */
		wsaaCompany.set(bsprIO.getCompany());
		/* Since this program runs from a parameter screen move the */
		/* internal parameter area to the original parameter copybook */
		/* to retrieve the contract range <D9703> */
		/* <D9703> */
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES) && isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaContractStart.set(LOVALUE);
			wsaaContractEnd.set(HIVALUE);
		} else {
			wsaaContractStart.set(p6671par.chdrnum);
			wsaaContractEnd.set(p6671par.chdrnum1);
		}
		/* Pick out all the processed UTRNs for the requested run */
		/* remembering to exclude those which are Non-Fund UTRNs. */
		/* These can be recognised by the FundAmount and NofUnits */
		/* both being equal to zero. */
		/* Change the SQL selection criteria to select records from */
		/* the UREP file - cleared at the start of Unit Deal and */
		/* written in the dealing program B5102. */
		/* We need to exclude non-fund UREPs. */
		/* These can be recognised by the FundAmount and NofUnits */
		/* both being equal to zero. */
		/* SQL Selection criteria changed to remove Schedule Name */
		/* and Number and ADD Contract Range and Processed Flag.<D9703> */
		/* <D9703> */
		String sqlutrnCursor = " SELECT  CHDRCOY, CHDRNUM, VRTFND, UNITYP, FUNDAMNT, PRICEUSED, NOFUNT, PRICEDT, BATCTRCDE, NOFDUNT, PROCFLG, SEQNUM"
				+ " FROM   "
				+ getAppVars().getTableNameOverriden("UREP")
				+ " "
				+ " WHERE CHDRCOY = ?"
				+ " AND CHDRNUM >= ?"
				+ " AND CHDRNUM <= ?"
				+ " AND PROCFLG = ?"
				+ " AND NOFUNT <> 0"
				+ " AND FUNDAMNT <> 0" + " ORDER BY CHDRCOY, VRTFND, UNITYP, CHDRNUM, BATCTRCDE";
		sqlerrorflag = false;
		try {
			sqlutrnCursorconn = getAppVars().getDBConnectionForTable(
					new com.csc.life.unitlinkedprocessing.dataaccess.UrepTableDAM());
			sqlutrnCursorps = getAppVars().prepareStatementEmbeded(sqlutrnCursorconn, sqlutrnCursor, "UREP");
			getAppVars().setDBString(sqlutrnCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlutrnCursorps, 2, wsaaContractStart);
			getAppVars().setDBString(sqlutrnCursorps, 3, wsaaContractEnd);
			getAppVars().setDBString(sqlutrnCursorps, 4, wsaaProcflg);
			sqlutrnCursorrs = getAppVars().executeQuery(sqlutrnCursorps);
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		isql.set(wsaaRowsInBlock);
		sqlerrd = wsaaRowsInBlock.toInt();

		String coy = bsprIO.getCompany().toString();
		t5515Map = itemDAO.loadSmartTable("IT", coy, "T5515");
	}


	/**
	* <pre>
	*1100-LAST-JOB-NO SECTION.                                        
	*1110-LAST-JOB-NO.                                                
	* The object of this section is to find the job number pertaining
	* to the last run of Unit Deal. We need to know this value in
	* order to pinpoint only the most recently dealt UTRNS for inclusi
	* in this report.
	****                                                      <D9703> 
	**** The following code is no longer required as a result of the  
	**** reporting changes made for Contract Windforward.     <D9703> 
	****                                                      <D9703> 
	*    Firstly save this process's BSPR-Params.
	**** MOVE BSPR-PARAMS            TO WSAA-SAVE-BSPR-PARAMS.<D9703> 
	****                                                      <D9703> 
	**** MOVE BPRD-SYSTEM-PARAM01    TO BSPR-SCHEDULE-NAME.   <D9703> 
	**** MOVE 99999999               TO BSPR-SCHEDULE-NUMBER. <D9703> 
	**** MOVE SPACES                 TO BSPR-COMPANY.         <D9703> 
	**** MOVE SPACES                 TO BSPR-PROCESS-NAME.    <D9703> 
	**** MOVE ZERO                   TO BSPR-PROCESS-OCC-NUM. <D9703> 
	**** MOVE BSPRREC                TO BSPR-FORMAT.          <D9703> 
	**** MOVE ENDR                   TO BSPR-FUNCTION.        <D9703> 
	****                                                      <D9703> 
	**** CALL 'BSPRIO'            USING BSPR-PARAMS.          <D9703> 
	****                                                      <D9703> 
	**** IF  (BSPR-STATUZ        NOT = O-K                    <D9703> 
	****                     AND NOT = ENDP)                  <D9703> 
	**** OR  BSPR-SCHEDULE-NAME  NOT = BPRD-SYSTEM-PARAM01    <D9703> 
	**** OR  (BSPR-PROCESS-STATUS NOT = '90'                  <D9703> 
	****  AND BPRD-SYSTEM-PARAM01 NOT = SPACES)               <D9703> 
	****     MOVE BSPR-PARAMS        TO SYSR-PARAMS           <D9703> 
	****     PERFORM 600-FATAL-ERROR.                         <D9703> 
	****                                                      <D9703> 
	**** IF  BSPR-STATUZ             = ENDP                   <D9703> 
	****     MOVE 'ENDP'             TO WSAA-SCHEDNAME        <D9703> 
	****     MOVE ZEROS              TO WSAA-SCHEDNUM-COMP    <D9703> 
	**** END-IF.                                              <D9703> 
	**** MOVE BSPR-SCHEDULE-NUMBER   TO WSAA-SCHEDNUM-COMP    <D9703> 
	****                                BSCHEDNUM OF R5106H01-O.      
	*    Restore the BSPR-Params.                                     
	****                                                      <D9703> 
	**** MOVE WSAA-SAVE-BSPR-PARAMS  TO BSPR-PARAMS.          <D9703> 
	****                                                      <D9703> 
	*1190-EXIT.                                               <D9703> 
	**** EXIT.                                                <D9703> 
	* </pre>
	*/
protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfCursor2080: 
					endOfCursor2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		isql.add(1);
		/*    On the first time around, the ISQL pointer will now = 1001; t*/
		/*    SQLERRD(3) will have been set to 1000 in the 1000- section. A*/
		/*    result the program drops through the first IF statement to th*/
		/*    second one which will be satisfied. This will cause a new blo*/
		/*    be fetched, with the program fooled into thinking that we hav*/
		/*    reached the end of a block and it is time to get another. As */
		/*    reset the pointer to 1 at the end of the 2000-, the loop cont*/
		/*    normally afterwards.*/
		/*    N.B. SQLERRD(3) contains the number of rows that have actuall*/
		/*    been fetched.*/
		if (isGTE(sqlerrd, isql.toInt())) {
			/*        Still processing a block*/
			ct01Value++;
			goTo(GotoLabel.exit2090);
		}
		else {
			if (isEQ(sqlerrd, wsaaRowsInBlock.toInt())) {
				/*        The current block was full, get another one*/
				/*NEXT_SENTENCE*/
			}
			else {
				/*        The last block contained the last record, no point doing*/
				/*        further fetches, all done*/
				goTo(GotoLabel.endOfCursor2080);
			}
		}
		sqlerrorflag = false;
		int utrnCursorLoopIndex = 1;
		try {
			for (; isLT(utrnCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlutrnCursorrs); utrnCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlutrnCursorrs, 1, sqlUtrnpfInner.sqlChdrcoy[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 2, sqlUtrnpfInner.sqlChdrnum[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 3, sqlUtrnpfInner.sqlVrtfnd[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 4, sqlUtrnpfInner.sqlUnityp[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 5, sqlUtrnpfInner.sqlFundamnt[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 6, sqlUtrnpfInner.sqlPriceused[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 7, sqlUtrnpfInner.sqlNofunt[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 8, sqlUtrnpfInner.sqlPricedt[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 9, sqlUtrnpfInner.sqlBatctrcde[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 10, sqlUtrnpfInner.sqlNofdunt[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 11, sqlUtrnpfInner.sqlProcflg[utrnCursorLoopIndex]);
				getAppVars().getDBObject(sqlutrnCursorrs, 12, sqlUtrnpfInner.sqlSeqnum[utrnCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		sqlerrd = utrnCursorLoopIndex - 1;
		if (sqlerrd == 0) {
			goTo(GotoLabel.endOfCursor2080);
		}
		isql.set(1);
		ct01Value++;
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		getAppVars().freeDBConnectionIgnoreErr(sqlutrnCursorconn, sqlutrnCursorps, sqlutrnCursorrs);
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
 {
		if (firstRec.isTrue()) {
			wsaaFirstRecord.set("N");
			wsaaFirstHeader.set("N");
			setControlBreak3010();
			setUpFundDetails3020();
			pageHeading3030();
		}
		if (isNE(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()], wsaaLastVrtfnd)
				|| isNE(sqlUtrnpfInner.sqlUnityp[isql.toInt()], wsaaLastUnityp)) {
			totalsLine3040();
			zeroiseAccumulations3050();
			setUpFundDetails3020();
			pageHeading3030();
		}
		/* IF SQL-VRTFND (ISQL) = WSAA-LAST-VRTFND */
		/* OR SQL-UNITYP (ISQL) = WSAA-LAST-UNITYP */
		/* PERFORM 3060-ACCUMULATE */
		/* PERFORM 3070-PRINT-DETAIL-LINE */
		/* END-IF. */
		setControlBreak3010();
		accumulate3060();
		printDetailLine3070();
		updateUrep3080();
	}


protected void setControlBreak3010()
	{
		/*SET-CONTROL-BREAK*/
		wsaaLastVrtfnd.set(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()]);
		wsaaLastUnityp.set(sqlUtrnpfInner.sqlUnityp[isql.toInt()]);
		indOn.setTrue(10);
		/*EXIT*/
	}

protected void setUpFundDetails3020()
 {
		if (isEQ(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()], SPACES)) {
			return;
		}
		/* Get fund description */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(sqlUtrnpfInner.sqlChdrcoy[isql.toInt()]);
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()]);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			longdesc.fill("?");
		} else {
			longdesc.set(getdescrec.longdesc);
		}
		/* Get the fund currency dated by effective date. */
		if (t5515Map!= null && t5515Map.containsKey(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()])) {
			for (Itempf item : t5515Map.get(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()])) {
				if (item.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
					t5515rec.t5515Rec.set(StringUtil.rawToString(item.getGenarea()));
					break;
				}
			}
		} else {
			syserrrec.params.set("T5515:" + sqlUtrnpfInner.sqlVrtfnd[isql.toInt()]);
			fatalError600();
		}
		fndcurr.set(t5515rec.currcode);
		/* Look up currency description */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(t5515rec.currcode);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			curdesc.fill("?");
		} else {
			curdesc.set(getdescrec.longdesc);
		}
	}


protected void pageHeading3030()
	{
		pageHeading3031();
		printR5106h013032();
	}

protected void pageHeading3031()
	{
		company.set(bsprIO.getCompany());
		companynm.set(wsaaCompanynm);
		if (isNE(isql, 0)) {
			vrtfnd.set(sqlUtrnpfInner.sqlVrtfnd[isql.toInt()]);
			fundtyp.set(sqlUtrnpfInner.sqlUnityp[isql.toInt()]);
		}
	}

protected void printR5106h013032()
	{
		printerFile.printR5106h01(r5106h01Record, indicArea);
		printerFile.printR5106h02(r5106h02Record, indicArea);
		wsaaNewHeadPrinted = "Y";
		/*    Clock up number of pages printed.*/
		ct02Value++;
		/*EXIT*/
	}

protected void totalsLine3040()
	{
		/*TOTALS-LINE*/
		totrunts.set(wsaaTotRealUnits);
		totdunts.set(wsaaTotDeemUnits);
		totfndamt.set(wsaaTotFundAmount);
		/*PRINT-R5106T01*/
		printerFile.printR5106t01(r5106t01Record, indicArea);
		/*EXIT*/
	}

protected void zeroiseAccumulations3050()
	{
		/*ZEROISE-ACCUMULATIONS*/
		wsaaTotFundAmount.set(ZERO);
		wsaaTotDeemUnits.set(ZERO);
		wsaaTotRealUnits.set(ZERO);
		/*EXIT*/
	}

protected void accumulate3060()
	{
		/*ACCUMULATE*/
		wsaaTotFundAmount.add(sqlUtrnpfInner.sqlFundamnt[isql.toInt()]);
		wsaaTotDeemUnits.add(sqlUtrnpfInner.sqlNofdunt[isql.toInt()]);
		wsaaTotRealUnits.add(sqlUtrnpfInner.sqlNofunt[isql.toInt()]);
		/*EXIT*/
	}

protected void printDetailLine3070()
 {
		if (isNE(sqlUtrnpfInner.sqlChdrnum[isql.toInt()], wsaaLastChdrnum)) {
			wsaaLastChdrnum.set(sqlUtrnpfInner.sqlChdrnum[isql.toInt()]);
			indOn.setTrue(10);
		} else {
			indOff.setTrue(10);
		}
		if (isEQ(wsaaNewHeadPrinted, "Y")) {
			indOn.setTrue(10);
			wsaaNewHeadPrinted = "N";
		}
		chdrnum.set(sqlUtrnpfInner.sqlChdrnum[isql.toInt()]);
		fundamnt.set(sqlUtrnpfInner.sqlFundamnt[isql.toInt()]);
		priceused.set(sqlUtrnpfInner.sqlPriceused[isql.toInt()]);
		nofrunts.set(sqlUtrnpfInner.sqlNofunt[isql.toInt()]);
		batctrcde.set(sqlUtrnpfInner.sqlBatctrcde[isql.toInt()]);
		/* MOVE SQL-PRICEDT (ISQL) TO PRICEDT OF R5106D01-O. */
		/* Convert the Pricing date. */
		datcon1rec.intDate.set(sqlUtrnpfInner.sqlPricedt[isql.toInt()]);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		pricedt.set(datcon1rec.extDate);
		nofdunts.set(sqlUtrnpfInner.sqlNofdunt[isql.toInt()]);
		printerFile.printR5106d01(r5106d01Record, indicArea);
	}


	/**
	* <pre>
	**************************                                <D9703> 
	* </pre>
	*/
protected void updateUrep3080()
 {
		/* For the given contract that has just been printed, update */
		/* the PROCFLG on UREP to Y. <D9703> */
		/* Read/hold the record <D9703> */
		Ureppf u = new Ureppf();
 		
		u.setChdrnum(sqlUtrnpfInner.sqlChdrnum[isql.toInt()].toString());
		u.setChdrcoy(sqlUtrnpfInner.sqlChdrcoy[isql.toInt()].toString());
		u.setSeqnum(sqlUtrnpfInner.sqlSeqnum[isql.toInt()].toInt());
		/* Record has been found, so update it. <D9703> */
		u.setProcflg("Y");
		if(updateUreppfList == null){
			updateUreppfList = new ArrayList<>();
		}
		updateUreppfList.add(u);
	}


protected void commit3500()
	{
		/*COMMIT*/
		/** No additional commitment processing in here.*/
		/*EXIT*/
		if(updateUreppfList != null){
			ureppfDAO.updateUreppfRecord(updateUreppfList);
			updateUreppfList.clear();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);		
		ct01Value = 0;
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);		
		ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** No additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (firstHeader.isTrue()) {
			isql.set(1);
			indOn.setTrue(10);
			setUpFundDetails3020();
			pageHeading3030();
			/*    MOVE WSAA-SCHEDNUM-ALPHA TO SCHALPH OF R5106N01-O         */
			printerFile.printR5106n01(r5106n01Record, indicArea);
		}
		totalsLine3040();
		printerFile.printR5106e01(r5106e01Record, indicArea);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure SQL-UTRNPF--INNER
 */
private static final class SqlUtrnpfInner { 

		/* SQL-UTRNPF */
	private FixedLengthStringData[] sqlFields = FLSInittedArray (1000, 63);
	private FixedLengthStringData[] sqlChdrcoy = FLSDArrayPartOfArrayStructure(1, sqlFields, 0);
	private FixedLengthStringData[] sqlChdrnum = FLSDArrayPartOfArrayStructure(8, sqlFields, 1);
	private FixedLengthStringData[] sqlVrtfnd = FLSDArrayPartOfArrayStructure(4, sqlFields, 9);
	private FixedLengthStringData[] sqlUnityp = FLSDArrayPartOfArrayStructure(1, sqlFields, 13);
	private PackedDecimalData[] sqlFundamnt = PDArrayPartOfArrayStructure(17, 2, sqlFields, 14);
	private PackedDecimalData[] sqlPriceused = PDArrayPartOfArrayStructure(14, 5, sqlFields, 23);
	private PackedDecimalData[] sqlNofunt = PDArrayPartOfArrayStructure(16, 5, sqlFields, 31);
	private PackedDecimalData[] sqlPricedt = PDArrayPartOfArrayStructure(8, 0, sqlFields, 40);
	private FixedLengthStringData[] sqlBatctrcde = FLSDArrayPartOfArrayStructure(4, sqlFields, 45);
	private PackedDecimalData[] sqlNofdunt = PDArrayPartOfArrayStructure(16, 5, sqlFields, 49);
	private FixedLengthStringData[] sqlProcflg = FLSDArrayPartOfArrayStructure(1, sqlFields, 58);
	private PackedDecimalData[] sqlSeqnum = PDArrayPartOfArrayStructure(7, 0, sqlFields, 59);
}
}
