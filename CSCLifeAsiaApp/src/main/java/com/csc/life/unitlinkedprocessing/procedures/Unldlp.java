/*
 * File: Unldlp.java
 * Date: 30 August 2009 2:50:12
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLDLP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.recordstructures.Compdelrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*            DELETE UNIT LINKED PROPOSAL COMPONENTS
*
*
*  PROGRAMMING NOTES
* --------------------
*
* INITIALISE
*
*    This program may be called to delete unit linked components down to
*    three levels.  These are as follows:
*
*    1) Delete all U/L components for a life.
*
*    2) Delete all U/L components for a coverage.
*
*    3) Delete all U/L components for a rider.
*
* UPDATING
* --------
*    - Read then delete all records for the Company , contract
*      life , coverage and rider passed.
*
* NEXT
* ----
*
* Exit program and return to called from program. (P5306)
*
*
*
*****************************************************************
* </pre>
*/
public class Unldlp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(6).init("UNLDLP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Compdelrec compdelrec = new Compdelrec();
	private Syserrrec syserrrec = new Syserrrec();
		/*Unit Linked transaction details*/
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		dbExit8190
	}

	public Unldlp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		compdelrec.compdelRec = convertAndSetParam(compdelrec.compdelRec, parmArray, 0);
		try {
			initialise1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			retrieveUnltunl1020();
		}
		catch (GOTOException e){
		}
		finally{
			exit1090();
		}
	}

protected void initialise1010()
	{
		compdelrec.statuz.set(varcom.oK);
		unltunlIO.setChdrcoy(compdelrec.chdrcoy);
		unltunlIO.setChdrnum(compdelrec.chdrnum);
		unltunlIO.setLife(compdelrec.life);
		unltunlIO.setCoverage(compdelrec.coverage);
		unltunlIO.setRider(compdelrec.rider);
		unltunlIO.setSeqnbr(ZERO);
	}

protected void retrieveUnltunl1020()
	{
		unltunlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		unltunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		unltunlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(),varcom.oK)
		&& isNE(unltunlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(unltunlIO.getParams());
			dbError8100();
		}
		while ( !(isEQ(unltunlIO.getStatuz(),varcom.endp))) {
			deleteUnlt1050();
		}
		
		goTo(GotoLabel.exit1090);
	}

protected void deleteUnlt1050()
	{
		if (isNE(compdelrec.chdrcoy,unltunlIO.getChdrcoy())
		|| isNE(compdelrec.chdrnum,unltunlIO.getChdrnum())
		|| isNE(compdelrec.life,unltunlIO.getLife())
		|| (isNE(compdelrec.coverage,"00")
		&& isNE(compdelrec.coverage,unltunlIO.getCoverage()))
		|| (isNE(compdelrec.rider,"00")
		&& isNE(compdelrec.rider,unltunlIO.getRider()))) {
			unltunlIO.setStatuz(varcom.endp);
		}
		else {
			unltunlIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, unltunlIO);
			if (isNE(unltunlIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(unltunlIO.getParams());
				dbError8100();
			}
			else {
				unltunlIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, unltunlIO);
				if (isNE(unltunlIO.getStatuz(),varcom.oK)
				&& isNE(unltunlIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(unltunlIO.getParams());
					dbError8100();
				}
			}
		}
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		compdelrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
