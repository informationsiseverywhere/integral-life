package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:23
 * Description:
 * Copybook name: CHDRULSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrulskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrulsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrulsKey = new FixedLengthStringData(64).isAPartOf(chdrulsFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrulsChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrulsKey, 0);
  	public FixedLengthStringData chdrulsChdrnum = new FixedLengthStringData(8).isAPartOf(chdrulsKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrulsKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrulsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrulsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}