package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.regularprocessing.recordstructures.Fmcvpmsrec;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.CovxpfTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.CovxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Covxpf;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Zfmcpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;

public class Br2yt extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private Covxpf covxpfRec = new Covxpf();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR2YT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	/* COVX parameters */
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaCovxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCovxFn, 0, FILLER).init("COVX");
	private FixedLengthStringData wsaaCovxRunid = new FixedLengthStringData(2).isAPartOf(wsaaCovxFn, 4);
	private ZonedDecimalData wsaaCovxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCovxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER)
			.init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaLastLocked = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1);
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaJustCommited = new FixedLengthStringData(1).init("N");
	private Validator justCommited = new Validator(wsaaJustCommited, "Y");
	private PackedDecimalData ix = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).setUnsigned();

	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private static final String H791 = "H791";

	private static final int WSAAT5675SIZE = 70;

	private FixedLengthStringData[] wsaaT5675Rec = FLSInittedArray(70, 11);
	private FixedLengthStringData[] wsaaT5675Key = FLSDArrayPartOfArrayStructure(4, wsaaT5675Rec, 0);
	private FixedLengthStringData[] wsaaT5675Premmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5675Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5675Data = FLSDArrayPartOfArrayStructure(7, wsaaT5675Rec, 4);
	private FixedLengthStringData[] wsaaT5675Premsubr = FLSDArrayPartOfArrayStructure(7, wsaaT5675Data, 0);

	/* Storage for T6598 table items. */
	private static final int WSAAT6598SIZE = 200;

	/* WSAA-T6598-ARRAY */
	private FixedLengthStringData[] wsaaT6598Rec = FLSInittedArray(200, 11);
	private FixedLengthStringData[] wsaaT6598Key = FLSDArrayPartOfArrayStructure(4, wsaaT6598Rec, 0);
	private FixedLengthStringData[] wsaaT6598Calcmeth = FLSDArrayPartOfArrayStructure(4, wsaaT6598Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT6598Data = FLSDArrayPartOfArrayStructure(7, wsaaT6598Rec, 4);
	private FixedLengthStringData[] wsaaT6598Calcprog = FLSDArrayPartOfArrayStructure(7, wsaaT6598Data, 0);

	/* Storage for T5688 table items. */
	private static final int WSAAT5688SIZE = 70;

	/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray(70, 10);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
	private FixedLengthStringData[] wsaaT5688Revacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();
	/* ERRORS */
	private static final String IVRM = "IVRM";
	private static final String E101 = "E101";
	/* TABLES */
	private static final String T5679 = "T5679";
	private FixedLengthStringData t5675 = new FixedLengthStringData(6).init("T5675");
	private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(6).init("T6598");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	/* CONTROL-TOTALS */
	private static final int CT01 = 1;
	private static final int CT02 = 2;
	private static final int CT03 = 3;
	private static final int CT04 = 4;
	private static final int CT05 = 5;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5675Ix = new IntegerData();
	private IntegerData wsaaT1688Ix = new IntegerData();
	private IntegerData wsaaT6598Ix = new IntegerData();
	private IntegerData wsaaT5688Ix = new IntegerData();
	private CovxpfTableDAM covxpf = new CovxpfTableDAM();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private T5679rec t5679rec = new T5679rec();
	private T5675rec t5675rec = new T5675rec();
	private T6598rec t6598rec = new T6598rec();
	private T5688rec t5688rec = new T5688rec();
	private Ubblallpar ubblallpar = new Ubblallpar();

	private int ct03Value = 0;
	private Zfmcpf zfmcchdIO = new Zfmcpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Map<String, List<Itempf>> t5675Map = null;
	private Map<String, List<Itempf>> t5688Map = null;
	private Map<String, List<Itempf>> t6598Map = null;
	private ZfmcpfDAO zfmcpfDAO = getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Fmcvpmsrec fmcvpmsrec = new Fmcvpmsrec();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO" , CovrpfDAO.class);
	//IGI-BRD-07
		private Datcon1rec datcon1rec = new Datcon1rec();
		private String wsaaPtrnWritten="N";
		//IGI-BRD-07
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaPriceAvailable = new FixedLengthStringData(1);
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private ExternalisedRules er = new ExternalisedRules();
	private boolean itemFound = false;
	private PackedDecimalData noofdue=new PackedDecimalData(8);
	private List<Utrspf> utrspfList = new ArrayList<Utrspf>();
	private List<Hitspf> hitspfList = new ArrayList<Hitspf>();	//IBPLIFE-1937
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);	//IBPLIFE-1937
	private CovxpfDAO covxpfDAO = getApplicationContext().getBean("covxpfDAO", CovxpfDAO.class);
	private int intBatchExtractSize;
	private int intBatchID = 0;
	private Iterator<Covxpf> iteratorList;
	private Map<String, List<Utrnpf>> utrnuddMap = null;
	private Map<String, List<Hitrpf>> hitraloMap = null;
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	/**
	 * Contains all possible labels used by goTo action.
	 */

	public Br2yt() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/** This program may deal with many COVX records for the same */
		/** contract. Thus to avoid repetitive processing of the contract */
		/** level details we use a control break based upon contact number. */
		/** As we do not need to store any running totals this does not */
		/** cause too many problems. */
		/** Should this program fail, it will leave the last contract we */
		/** were attempting to process in a locked state. Thus when we */
		/** restart, the contract will be skipped and we will start */
		/** on the next available one. */
		/* EXIT */
	}

	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010() {
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(IVRM);
			fatalError600();
		}
		/* Point to correct member of COVXPF. */
		wsaaCovxRunid.set(bprdIO.getSystemParam04());
		wsaaCovxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		varcom.vrcmDate.set(getCobolDate());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(COVXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaCovxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		covxpf.openInput();
		/* Read T5679 for valid statii. */
		Itempf itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(bprdIO.getCompany().toString().trim());
		itempf.setItemtabl(T5679);
		itempf.setItemitem(bprdIO.getAuthCode().toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.params.set(bprdIO.getCompany().toString()+T5679+bprdIO.getAuthCode().toString());
			fatalError600();
		}
		else
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		loadT56751100();
		loadT65981300();
		loadT56881400();
		readChunkRecord();
	}
	
	private void readChunkRecord() {
		List<Covxpf> covxpfList = covxpfDAO.searchCovxRecord(wsaaCovxFn.toString(), wsaaThreadMember.toString(),
				intBatchExtractSize, intBatchID);
		iteratorList = covxpfList.iterator();
		List<String> chdrnums = new ArrayList<>();
		if (!covxpfList.isEmpty()) {
			for (Covxpf h : covxpfList) {
				chdrnums.add(h.getChdrnum());
			}
		}
		utrnuddMap = utrnpfDAO.searchUtrnuddRecord(chdrnums);
		hitraloMap = hitrpfDAO.searchHitrRecordByChdrnum(chdrnums);
	}

	protected void loadT56751100() {
		para1110();
	}

	protected void para1110() {
		t5675Map = itemDAO.loadSmartTable("IT", bprdIO.getCompany().toString(), t5675.toString());
		wsaaT5675Ix.set(1);
	}

	protected void readT56751120() {
		if (t5675Map != null && !t5675Map.isEmpty()) {
			for (List<Itempf> t5675items : t5675Map.values()) {
				for (Itempf item : t5675items) {
					t5675rec.t5675Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5675Premmeth[wsaaT5675Ix.toInt()].set(item.getItemitem());
					wsaaT5675Premsubr[wsaaT5675Ix.toInt()].set(t5675rec.premsubr);
					wsaaT5675Ix.add(1);
					if (isGT(wsaaT5675Ix, WSAAT5675SIZE)) {
						syserrrec.params.set(t5675);
						syserrrec.statuz.set(H791);
						fatalError600();
					}
				}
			}
		}
	}




	protected void loadT65981300() {
		para1310();	
	}

	protected void para1310() {
		t6598Map = itemDAO.loadSmartTable("IT", bprdIO.getCompany().toString(), t6598.toString());
		wsaaT6598Ix.set(1);
	}

	protected void readT65981320() {
		if (t6598Map != null && !t6598Map.isEmpty()) {
			for (List<Itempf> t6598items : t6598Map.values()) {
				for (Itempf item : t6598items) {
					t6598rec.t6598Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT6598Calcmeth[wsaaT6598Ix.toInt()].set(item.getItemitem());
					wsaaT6598Calcprog[wsaaT6598Ix.toInt()].set(t6598rec.calcprog);
					wsaaT6598Ix.add(1);
					if (isGT(wsaaT6598Ix, WSAAT6598SIZE)) {
						syserrrec.params.set(t6598);
						syserrrec.statuz.set(H791);
						fatalError600();
					}
				}
			}
		}
	}

	protected void loadT56881400() {
		para1410();
	}

	protected void para1410() {
		t5688Map = itemDAO.loadSmartTable("IT", bprdIO.getCompany().toString(), t5688.toString());
		wsaaT5688Ix.set(1);
	}

	protected void readT56881420() {
		if (t5688Map != null && !t5688Map.isEmpty()) {
			for (List<Itempf> t5688items : t5688Map.values()) {
				for (Itempf item : t5688items) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5688Itmfrm[wsaaT5688Ix.toInt()].set(item.getItmfrm());
					wsaaT5688Cnttype[wsaaT5688Ix.toInt()].set(item.getItemitem());
					wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()].set(t5688rec.comlvlacc);
					wsaaT5688Revacc[wsaaT5688Ix.toInt()].set(t5688rec.revacc);
					wsaaT5688Ix.add(1);
					if (isGT(wsaaT5688Ix, WSAAT5688SIZE)) {
						syserrrec.params.set(t5688);
						syserrrec.statuz.set(H791);
						fatalError600();
					}
				}
			}
		}
	}

	protected void readFile2000() {
		readFile2010();	
	}

	protected void readFile2010() {
		if (iteratorList != null && iteratorList.hasNext()) {
			covxpfRec = iteratorList.next();
			contotrec.totno.set(CT01);
			contotrec.totval.set(1);
			callContot001();
		} else {
			intBatchID++;
			readChunkRecord();
			if (iteratorList.hasNext()) {
				covxpfRec = iteratorList.next();
				contotrec.totno.set(CT01);
				contotrec.totval.set(1);
				callContot001();
			} else {
				wsspEdterror.set(varcom.endp);
				eof2080();
			}
		}
		
	}

	protected void eof2080() {
		return;
	}

	protected void edit2500() {
		edit2510();
	}

	protected void edit2510() {
		wsspEdterror.set(varcom.oK);
		if (utrnuddMap != null && utrnuddMap.containsKey(covxpfRec.getChdrnum())) {
			List<Utrnpf> utrnpfList = utrnuddMap.get(covxpfRec.getChdrnum());
			for (Utrnpf u : utrnpfList) {
				if (u.getChdrcoy().equals(covxpfRec.getChdrcoy()) && u.getLife().equals(covxpfRec.getLife())
						&& u.getCoverage().equals(covxpfRec.getCoverage()) && u.getRider().equals(covxpfRec.getRider())
						&& u.getPlanSuffix() == covxpfRec.getPlanSuffix()) {
					wsspEdterror.set(SPACES);
					return;
				}
			}
		}
		if (hitraloMap != null && hitraloMap.containsKey(covxpfRec.getChdrnum())) {
			List<Hitrpf> hitrList = hitraloMap.get(covxpfRec.getChdrnum());
			for (Hitrpf h : hitrList) {
				if (h.getChdrcoy().equals(covxpfRec.getChdrcoy()) && h.getChdrnum().equals(covxpfRec.getChdrnum())
						&& h.getLife().equals(covxpfRec.getLife()) && h.getCoverage().equals(covxpfRec.getCoverage())
						&& h.getRider().equals(covxpfRec.getRider()) && h.getPlanSuffix() == covxpfRec.getPlanSuffix()) {
					wsspEdterror.set(SPACES);
					return;
				}
			}
		}
		if (isNE(covxpfRec.getChdrnum(), lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz, "LOCK")) {
				if (isNE(covxpfRec.getChdrnum(), wsaaLastLocked)) {
					wsaaPlnsfx.set(covxpfRec.getPlanSuffix());
					StringUtil stringVariable1 = new StringUtil();
					stringVariable1.addExpression(covxpfRec.getChdrcoy());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getChdrnum());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getLife());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getCoverage());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getRider());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(wsaaPlnsfx);
					stringVariable1.setStringInto(conlogrec.params);
					/* MOVE SFTL-STATUZ TO CONL-ERROR */
					conlogrec.error.set(E101);
					callConlog003();
				}
				wsaaLastLocked.set(covxpfRec.getChdrnum());
				contotrec.totno.set(CT02);
				contotrec.totval.set(1);
				callContot001();
				wsspEdterror.set(SPACES);
				return;
			}
		}
	}

	protected void softlockPolicy2600() {
		softlockPolicy2610();
	}

	protected void softlockPolicy2610() {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(covxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(covxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(ZERO);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK) && isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void update3000() {
		update3010();
	}

	protected void update3010() {
		/* The splitter program may have extracted many COVRs for the */
		/* same contract. To minimise the repetivite processing this */
		/* program uses a control break on CHDRNUM. A chdrnum of spaces */
		/* means we are on the first record. */

        wsaaPtrnWritten="N";
        wsaaPriceAvailable.set(SPACES);
		if (isNE(covxpfRec.getChdrnum(), lastChdrnum)) {
			if (isNE(lastChdrnum, SPACES)) {
				unlockLastContract3900();
			}
			readhChdr3100();
			chdrStatusCheck3200();
		}
		if (!validStatus.isTrue()) {
			return;
		}
		contotrec.totno.set(CT03);
		contotrec.totval.set(1);
		callContot001();
		zfmcchdIO=zfmcpfDAO.getZfmcRecord(covxpfRec.getChdrcoy().trim(), covxpfRec.getChdrnum().trim());
		utrspfList = utrspfDAO.searchUtrsRecord(covxpfRec.getChdrcoy(), chdrpf.getChdrnum());
		itemFound = false;	
		setPrecision(chdrpf.getTranno(), 0);
		wsaaTranno.set(add(chdrpf.getTranno(), 1).toInt());
		// This catch upto effective date, not sure if we need to get the same
		// logic from bt001 when policy lapsed and reinstated.
		if(!utrspfList.isEmpty()){
		while (!(isGT(zfmcchdIO.getZfmcdat(), bsscIO.getEffectiveDate()))) {
			processFMC3100();
		}
		} else {	//IBPLIFE-1937
			Hitspf hitspf = new Hitspf();
			hitspf.setChdrcoy(covxpfRec.getChdrcoy());
			hitspf.setChdrnum(chdrpf.getChdrnum());
			hitspf.setLife(covxpfRec.getLife());
			hitspf.setCoverage(covxpfRec.getCoverage());
			hitspf.setRider(covxpfRec.getRider());
			hitspfList = hitspfDAO.searchHitsRecord(hitspf);
			if(!hitspfList.isEmpty()) {
				while (!(isGT(zfmcchdIO.getZfmcdat(), bsscIO.getEffectiveDate()))) {
					processFMC3100();
				}
			}
		}
		if(itemFound){
			chdrpf.setTranno(wsaaTranno.toInt());
			chdrpfDAO.updateChdrpf(chdrpf);
			if (isNE(wsaaPtrnWritten, "Y")) {	
				writePtrn3400();
				wsaaPtrnWritten="Y";
				contotrec.totno.set(CT03);
				contotrec.totval.set(1);
				callContot001();
			}
			rewriteZfmc3800();
		}
	 
	}

	protected void setCtrlBreak3080() {
		lastChdrnum.set(covxpfRec.getChdrnum());
	}

	protected void readhChdr3100() {
		/* READH-CHDR */
		chdrpf = chdrpfDAO.getChdrpf(covxpfRec.getChdrcoy(), covxpfRec.getChdrnum());
		if(null==chdrpf) {
			syserrrec.params.set(covxpfRec.getChdrcoy().trim()+ covxpfRec.getChdrnum().trim());
			fatalError600();
		}
		wsaaOccdate.set(chdrpf.getOccdate());
		/* EXIT */
	}

	protected void chdrStatusCheck3200() {
		/* CHDR-STATUS-CHECK */
		wsaaValidStatus.set("N");
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[ix.toInt()], chdrpf.getStatcode())) {
				ix.set(13);
				wsaaValidStatus.set("Y");
			}
		}
		if (!validStatus.isTrue()) {
			contotrec.totno.set(CT05);
			contotrec.totval.set(1);
			callContot001();
		}
		/* EXIT */
	}

	protected void processFMC3100() {

		List<Covrpf> covrpfList = covrpfDAO.getcovrtrbRecord(covxpfRec.getChdrcoy().trim(), covxpfRec.getChdrnum().trim(), covxpfRec.getLife().trim(), covxpfRec.getCoverage().trim(), covxpfRec.getRider().trim());
		if(!covrpfList.isEmpty()){
		for(Covrpf covrpf : covrpfList)
			checkStatProcFMC3300(covrpf);
		}
		
	}

	protected void checkStatProcFMC3300(Covrpf covrpf) {

		wsaaValidCoverage.set("N");
		if (isNE(covrpf.getCoverage(), SPACES) && isEQ(covrpf.getRider(), SPACES) || isEQ(covrpf.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)) {
				if (isEQ(t5679rec.covRiskStat[ix.toInt()], covrpf.getStatcode())) {
					ix.set("13");
					wsaaValidCoverage.set("Y");
				}
			}
		}
		/* If we have a rider, check we have the correct status */
		if (isNE(covrpf.getCoverage(), SPACES) && isNE(covrpf.getRider(), SPACES) || isNE(covrpf.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)) {
				if (isEQ(t5679rec.covPremStat[ix.toInt()], covrpf.getPstatcode())) {
					ix.set("13");
					wsaaValidCoverage.set("Y");
				}
			}
		}

		/* If we do not have a valid coverage or rider, skip to */
		/* the next record. */
		if (isEQ(wsaaValidCoverage, "N")) {
			ct03Value++;
			return; // --> From this method
		}
		itemFound = true;
		callT5534Subprog3310(covrpf);
		calcNewFmcDate3700();
	}

	protected void callT5534Subprog3310(Covrpf covrpf) {

		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(covxpfRec.getChdrcoy());
		ubblallpar.chdrChdrnum.set(covxpfRec.getChdrnum());
		ubblallpar.lifeLife.set(covrpf.getLife());
		ubblallpar.lifeJlife.set(covrpf.getJlife());
		ubblallpar.covrCoverage.set(covrpf.getCoverage());
		ubblallpar.covrRider.set(covrpf.getRider());
		ubblallpar.planSuffix.set(covrpf.getPlanSuffix());
		ubblallpar.billfreq.set(covxpfRec.getUnitFreq());
		ubblallpar.cntcurr.set(covxpfRec.getPremCurrency());

		ubblallpar.effdate.set(zfmcchdIO.getZfmcdat());
		ubblallpar.batchrundate.set(bsscIO.getEffectiveDate());
		ubblallpar.premMeth.set(covxpfRec.getPremmeth());
		ubblallpar.jlifePremMeth.set(covxpfRec.getJlPremMeth());
		ubblallpar.sumins.set(covrpf.getSumins());
		ubblallpar.premCessDate.set(covrpf.getPremCessDate());
		ubblallpar.crtable.set(covrpf.getCrtable());
		ubblallpar.mortcls.set(covrpf.getMortcls());
		ubblallpar.svMethod.set(covxpfRec.getSvMethod());
		ubblallpar.adfeemth.set(covxpfRec.getAdfeemth());
		/* Other values either available or found in this program. */
		ubblallpar.batccoy.set(batcdorrec.company);
		ubblallpar.batcbrn.set(batcdorrec.branch);
		ubblallpar.batcactmn.set(batcdorrec.actmonth);
		ubblallpar.batcactyr.set(batcdorrec.actyear);
		ubblallpar.batctrcde.set(batcdorrec.trcde);
		ubblallpar.batch.set(batcdorrec.batch);
		ubblallpar.billchnl.set(chdrpf.getBillchnl());
		ubblallpar.cnttype.set(chdrpf.getCnttype());
		ubblallpar.tranno.set(wsaaTranno);
		ubblallpar.polsum.set(chdrpf.getPolsum());
		ubblallpar.ptdate.set(chdrpf.getPtdate());
		ubblallpar.polinc.set(chdrpf.getPolinc());
		ubblallpar.singp.set(ZERO);
		ubblallpar.chdrRegister.set(chdrpf.getReg());
		ubblallpar.language.set(bsscIO.getLanguage());
		ubblallpar.user.set(ZERO);
		ubblallpar.function.set("RFMC");
		ubblallpar.znofdue.set(zfmcchdIO.getZnofdue());
		setupNewUbblFields5000();

		if (isNE(covxpfRec.getSubprog(),SPACES)) {
			/*IVE-869 LIFE RUL Benefit Billing Calculation Rework on Contract Issue P5074AT - Integration with latest PA compatible models*/
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(covxpfRec.getSubprog()) && er.isExternalized(chdrpf.getCnttype(), ubblallpar.crtable.toString())))
			{
				callProgramX(covxpfRec.getSubprog(), ubblallpar.ubblallRec); //ILIFE-7368
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxubblrec vpxubblrec = new Vpxubblrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
				vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);		
				vpxubblrec.function.set("INIT");
				callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec,vpxubblrec);	
				ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
				vpmfmtrec.initialize();
				datcon4rec.frequency.set("01");
				datcon4rec.freqFactor.set(1);
				datcon4rec.intDate1.set(vpxubblrec.crrcd);
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					fatalError600();
				}
				/*vpmfmtrec.date01.set(ubblallpar.effdate);*/
				vpmfmtrec.date01.set(datcon4rec.intDate2);	
				vpmfmtrec.previousSum.set(ZERO);
				vpmfmtrec.riskComDate.set(covrpf.getCurrfrom());
				callProgram(covxpfRec.getSubprog(), vpmfmtrec,ubblallpar.ubblallRec,vpxubblrec); 
				callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				
				if(isEQ(ubblallpar.statuz,SPACES))
					ubblallpar.statuz.set(Varcom.oK);
			}
			/*IVE-795 RUL Product - Benefit Billing Calculation end*/
			if (isNE(ubblallpar.statuz,varcom.oK)) {
				syserrrec.params.set(ubblallpar.ubblallRec);
				syserrrec.statuz.set(ubblallpar.statuz);
				fatalError600();
			}
		}
	}

	protected void writePtrn3400() {
		writePtrn3410();
	}

	protected void writePtrn3410() {
		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setChdrcoy(covxpfRec.getChdrcoy());
		ptrnpf.setChdrnum(covxpfRec.getChdrnum());
		ptrnpf.setTranno(wsaaTranno.toInt());
		ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setValidflag("1");
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setUserT(ZERO.intValue());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnpfDAO.insertPtrnPF(ptrnpf);
		
		contotrec.totno.set(CT04);
		contotrec.totval.set(1);
		callContot001();
	}

	protected void commit3500() {
		/* COMMIT */
		wsaaJustCommited.set("Y");
		/* EXIT */
	}

	protected void rollback3600() {
		/* ROLLBACK */
		/** This section should never be invoked by this program as */
		/** the only valid WSSP-EDTERROR statii are SPACES, O-K, ENDP. */
		/* EXIT */
	}


	protected void calcNewFmcDate3700() {
		calcNewFmcDate3710();
	}

	protected void calcNewFmcDate3710() {
		// TODO read VPMS
		
		if (isNE(covxpfRec.getUnitFreq(), NUMERIC)) {
	          covxpfRec.setUnitFreq("12");
	        }
		
		datcon4rec.frequency.set(covxpfRec.getUnitFreq());
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(covxpfRec.getSubprog()) && er.isExternalized(chdrpf.getCnttype(), ubblallpar.crtable.toString())){
		if(!utrspfList.isEmpty()) {
			for (Utrspf utrs : utrspfList){
				fmcvpmsrec.product.set(chdrpf.getCnttype());
				fmcvpmsrec.covrCoverage.set(covxpfRec.getCrtable());
				fmcvpmsrec.fund.set(utrs.getUnitVirtualFund());
				callProgram("FMCAPPCB", fmcvpmsrec);
				datcon4rec.frequency.set(fmcvpmsrec.frequnce);
			}
		} else if(!hitspfList.isEmpty()) {	//IBPLIFE-1937
			for (Hitspf hits : hitspfList){
				fmcvpmsrec.product.set(chdrpf.getCnttype());
				fmcvpmsrec.covrCoverage.set(covxpfRec.getCrtable());
				fmcvpmsrec.fund.set(hits.getZintbfnd());
				callProgram("FMCAPPCB", fmcvpmsrec);
				datcon4rec.frequency.set(fmcvpmsrec.frequnce);
				}
			}
		}
		datcon4rec.freqFactor.set(1);
		datcon4rec.intDate1.set(zfmcchdIO.getZfmcdat());
		datcon4rec.intDate2.set(0);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		zfmcchdIO.setZfmcdat(Integer.valueOf(datcon4rec.intDate2.toInt()));
		noofdue.set(zfmcchdIO.getZnofdue());
		compute(noofdue, 0).setRounded(add(noofdue, 1));
		zfmcchdIO.setZnofdue(noofdue.toInt());
		zfmcchdIO.setTranno(wsaaTranno.toInt());


	}

	protected void rewriteZfmc3800() {
		/* REWRITE-ZFMCHD */
		zfmcpfDAO.updateZfmcRecord(zfmcchdIO);
		/* EXIT */
	}

	protected void unlockLastContract3900() {
		sftlockrec.entity.set(lastChdrnum);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (justCommited.isTrue() && isEQ(sftlockrec.statuz, varcom.mrnf)) {
			wsaaJustCommited.set("N");
		} else {
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}

	}

	protected void close4000() {
		/* CLOSE-FILES */
		covxpf.close();
		lsaaStatuz.set(varcom.oK);
		/* EXIT */
	}

	protected void setupNewUbblFields5000() {
		para5010();
	}

	protected void para5010() {
		ubblallpar.occdate.set(chdrpf.getOccdate());
		ubblallpar.premsubr.set(" ");
		ubblallpar.jpremsubr.set(" ");
		ubblallpar.comlvlacc.set("  ");
		ubblallpar.svCalcprog.set(" ");

		/* Setup T1688 field */
		wsaaT1688Ix.set(1);

		Descpf descpf = descDAO.getdescData("IT", t1688.toString(), ubblallpar.batctrcde.toString(), bsprIO.getCompany().toString(),
				bsscIO.getLanguage().toString());
		if (descpf == null) {
			return;
		} else {
			ubblallpar.trandesc.set(descpf.getLongdesc());
		}
	}

}