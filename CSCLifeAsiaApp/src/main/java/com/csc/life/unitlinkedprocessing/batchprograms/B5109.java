/*
 * File: B5109.java
 * Date: 29 August 2009 20:57:12
 * Author: Quipoz Limited
 * 
 * Class transformed from B5109.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.UrepTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  UREP DELETION FOLLOWING UNIT DEALING.
*  =====================================
*
*  OVERVIEW.
*
*  This program must run before the dealing program B5102.
*  It will delete all the UREPs for the contract range input.
*  If no range is entered, all records will be deleted.
*
*  NOTE : Records will only be deleted where Processed Flag is
*         set to Y.
*
*  Control totals used in this program:
*
*  01 - No. of UREP recs read
*  02 - UREP contracts locked
*  03 - No. of UREP recs deleted
*
*****************************************************************
* </pre>
*/
public class B5109 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5109");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private UrepTableDAM urepIO = new UrepTableDAM();
	private P6671par p6671par = new P6671par();

	public B5109() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		urepIO.setDataArea(SPACES);
		urepIO.setStatuz(varcom.oK);
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		urepIO.setChdrcoy(bsprIO.getCompany());
		if (isEQ(p6671par.chdrnum,SPACES)) {
			p6671par.chdrnum.set(LOVALUE);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			p6671par.chdrnum1.set(HIVALUE);
		}
		urepIO.setChdrnum(p6671par.chdrnum);
		urepIO.setSeqnum(ZERO);
		urepIO.setFunction(varcom.begnh);
	}

protected void readFile2000()
	{
			readFile2010();
		}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, urepIO);
		if (isNE(urepIO.getStatuz(),varcom.oK)
		&& isNE(urepIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(urepIO.getParams());
			fatalError600();
		}
		if (isNE(urepIO.getChdrcoy(),bsprIO.getCompany())
		|| isGT(urepIO.getChdrnum(),p6671par.chdrnum1)) {
			urepIO.setFunction("REWRT");
			SmartFileCode.execute(appVars, urepIO);
			if (isNE(urepIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(urepIO.getParams());
				fatalError600();
			}
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isEQ(urepIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(urepIO.getStatuz(),varcom.oK)
		&& isNE(urepIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(urepIO.getParams());
			fatalError600();
		}
		/*    Log number of UREP records read.*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		/* If the Process flag on the UREP record is not = 'Y', then read  */
		/* the next record.                                                */
		if (isNE(urepIO.getProcflg(),"Y")) {
			wsspEdterror.set(SPACES);
			urepIO.setFunction(varcom.nextr);
			return ;
		}
		wsspEdterror.set(varcom.oK);
		if (isNE(urepIO.getChdrnum(),wsaaPrevChdrnum)) {
			softlockContract2600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			conlogrec.error.set(sftlockrec.statuz);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			urepIO.setFunction(varcom.nextr);
			wsspEdterror.set(SPACES);
		}
	}

protected void softlockContract2600()
	{
		start2610();
	}

protected void start2610()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.user.set(99999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.function.set("LOCK");
		sftlockrec.entity.set(urepIO.getChdrnum());
		sftlockrec.statuz.set(varcom.oK);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		wsaaPrevChdrnum.set(urepIO.getChdrnum());
	}

protected void update3000()
	{
		/*UPDATE*/
		urepIO.setStatuz(varcom.oK);
		urepIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, urepIO);
		if (isNE(urepIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(urepIO.getParams());
			fatalError600();
		}
		/*    Log number of UREP records deleted*/
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		urepIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLL*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
