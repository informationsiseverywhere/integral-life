/*
 * File: Unlsurc.java
 * Date: 30 August 2009 2:52:26
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLSURC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.CovrpenTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
/**
* <pre>
*REMARKS.
*
* UNLSURC - Full Surrender Calculation Method #1.
* -----------------------------------------------
*
* This  program  is  an  item  entry  on  T6598,  the  surrender  claim
* subroutine   method  table.  This  is  the  calculation  process  for
* obtaining the Bid value of the investments, the  amount  is  returned
* in the currency of the fund.
*
*       The  following  is  the  linkage  information  passed  to  this
* subroutine:-
*
*            - company
*            - contract header number
*            - suffix
*            - life number
*            - joint-life number
*            - coverage
*            - rider
*            - crtable
*            - effective date
*            - language
*            - estimated value
*            - actual value
*            - currency
*            - element code
*            - description
*            - type code
*            - status
*
* PROCESSING.
* ----------
*
* If it is the first time through this routine:
*
*     NB.  -  accumulate  all  records/amounts  for  the   same   fund,
* including initial and accumulation.
*
*     - set-up the key and BEGN option  and read fund UTRS
*         keyed:  Coy, Chdr, Life, Jlife, Coverage, Rider
*
*     - for each fund on component get the now bid price
*         read the VPRC (prices file), keyed on:
*         - Virtual fund, Unit type and Effective-date.
*
* If "INITIAL" units
*
*     Look-up  the  "Surrender Value Penalty (SVP)" factor table T6655,
*     keyed  on  method/premium  status.  The  table  contains  factors
*     accessed  by  the  number  of  years  to risk cessation date. The
*     factor obtained is then divided  by  the  value  in  the  divisor
*     field  to  give  a  calculated factor. This new factor is used in
*     the calculations.
*
*     the no. of initial deemed units are multiplied by the factor  and
*     the  result  is  multiplied  by the bid price. This result is the
*     surrender value amount of the initial units.
* If "ACCUMULATION" units
*
*
*
*
*          SVP = 1
*
*     Return one line for each fund and convert  the  currency  to  the
*     claim  currency,  or  if the claim currency field is blank to the
*     contract currency.
*
* Call "XCVRT" subroutine to do  the  currency  conversion.  The  table
* accessed  for  the  the  rates  is T3000, keyed by Currency Code. The
* calling program will provide this module with a  'From  Currency',  a
* 'To  Currency'  and  an  amount  in  the  'From Currency' and it will
* return to the calling program an amount in the  'To  Currency'  along
* with the rate that it used in the conversion.
*
*     Sum the amount for Policy or Plan.
*     Sum the annual premium (A/P).
*         - A/P is the single premium * billing frequency.
*
*     - calculate the penalty amount
*
*     look-up  the  "Surrender Value Penalty (SVP)" factor table T6656,
*     keyed  on  method/premium  status.  The  table  contains  factors
*     accessed  by  the number of years that the premium has been paid.
*     The factor obtained is divided by the value in the divisor  field
*     to  give  a  new factor value. Multiply the annual premium by the
*     factor to give the penalty amount.
*
*     return the "calculation method (T5687)" with the penalty  amount.
*     The  penalty  amount  is  the lessor of the total invested in the
*     fund (initial and accumulation combined) or the  product  of  the
*     (factor  *  annual  premium).  return  the  "calculation  method"
*     (T5687) with the penalty amount.
*
*     return amt, fund, desc, type = f, ccy fund
*
* else
*
*     set status = ENDP.
*
*****************************************************************
* </pre>
*/
public class Unlsurc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected final String wsaaSubr = "UNLSURC";
	protected PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);

	protected ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator policy1 = new Validator(wsaaPlnsfx, "1");

	protected ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator wholePlan = new Validator(wsaaPlanSwitch, "1");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "2");
	protected Validator summaryPart = new Validator(wsaaPlanSwitch, "3");
	protected ZonedDecimalData wsaaSurcCcdate = new ZonedDecimalData(8, 0).setUnsigned();
	protected ZonedDecimalData wsaaSurcCesdte = new ZonedDecimalData(8, 0).setUnsigned();

	protected FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	protected FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	protected FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);
		/* TABLES */
	protected static final String t5515 = "T5515";
	protected static final String t6644 = "T6644";
	protected static final String t6598 = "T6598";
	protected static final String t5551 = "T5551";
	protected PackedDecimalData wsaaInitialAmount = new PackedDecimalData(17, 5).init(0);
	protected PackedDecimalData wsaaAccumAmount = new PackedDecimalData(17, 5).init(0);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5).init(0);
	protected PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5).init(0);
	protected FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	protected FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	protected FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);

	protected FixedLengthStringData wsaaNoMore = new FixedLengthStringData(1);
	protected Validator endOfFund = new Validator(wsaaNoMore, "Y");
		/*  88  GOT-FREQ                VALUE 'Y'.                       
		    88  PS-FREQ                VALUE 'Y'.                        
		01  WSAA-FREQUENCIES.                                            
		      '0001020412'.                                            */
	protected PackedDecimalData wsaaEstimateTotAmt = new PackedDecimalData(18, 5).init(0);
	protected PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	protected FixedLengthStringData wsaaUtype = new FixedLengthStringData(1);
	protected Validator initialUnits = new Validator(wsaaUtype, "I");

	protected FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	protected Validator firstTime = new Validator(wsaaSwitch, "Y");
	protected CovrpenTableDAM covrpenIO = new CovrpenTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected HitsTableDAM hitsIO = new HitsTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected UtrsTableDAM utrsIO = new UtrsTableDAM();
	protected UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	protected VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	protected Varcom varcom = new Varcom();
	protected Conlinkrec conlinkrec = new Conlinkrec();
	protected Syserrrec syserrrec = new Syserrrec();
	protected T5515rec t5515rec = new T5515rec();
	protected T6598rec t6598rec = new T6598rec();
	protected T5551rec t5551rec = new T5551rec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected ExternalisedRules er = new ExternalisedRules();
	private static final String UNLSURCVPM = "UNLSURCVPM";
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAccum152, 
		exit155, 
		setType570
	}

	public Unlsurc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
			para010();
			exit090();
		}

protected void para010()
	{
		/*MOVE '****'                 TO SURC-STATUS.                  */
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		/* ENDF = "Y" is now used to indicate that all fund records have   */
		/* been processed and hence now calculate the penalty value.       */
		/* If the UTRS file status = ENDP then it is inherent that the     */
		/* ENDF value will have been set to "Y".                           */
		if (isEQ(srcalcpy.endf,"Y")) {
			/*    IF  UTRSSUR-STATUZ = 'ENDP'  OR                              */
			/*        UTRSCLM-STATUZ = 'ENDP'                                  */
			calcSurrPenVal500();
			wsaaEstimateTotAmt.set(0);
			return ;
		}
		if (firstTime.isTrue()) {
			wsaaChdrcoy.set(srcalcpy.chdrChdrcoy);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			wsaaPlanSuffix.set(srcalcpy.planSuffix);
			wsaaSurcCcdate.set(srcalcpy.crrcd);
			wsaaSurcCesdte.set(srcalcpy.convUnits);
			/*MOVE SURC-CURRCODE       TO WSAA-T5542-CURR               */
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			wsaaEstimateTotAmt.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			wsaaVirtualFund.set(SPACES);
			wsaaUnitType.set(SPACES);
			srcalcpy.description.set(SPACES);
			/*                              WSAA-READ-TABLE               */
			/* Three flags to say whether or not Part Surrender allowed.      */
			/*                              SURC-NE-UNITS                 */
			/*                              SURC-TM-UNITS                 */
			/*                              SURC-PS-NOT-ALLWD             */
			/* decide which part of the plan is being surrended*/
			if (isEQ(srcalcpy.planSuffix,ZERO)) {
				wsaaPlanSwitch.set(1);
			}
			else {
				if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)
				&& isNE(srcalcpy.polsum,1)) {
					wsaaPlanSuffix.set(0);
					wsaaPlanSwitch.set(3);
					if (isEQ(srcalcpy.planSuffix,1)) {
						wsaaPlnsfx.set(1);
					}
					else {
						wsaaPlnsfx.set(0);
					}
				}
				else {
					wsaaPlanSwitch.set(2);
				}
			}
		}
		if (summaryPart.isTrue()) {
			srcalcpy.planSuffix.set(0);
		}
		wsaaNoMore.set("N");
		if (isEQ(srcalcpy.endf,"I")) {
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
		}
		/*IF SURC-STATUS = '****'                                      */
		if (isEQ(srcalcpy.status,varcom.oK)) {
			if (isEQ(srcalcpy.endf,"I")) {
				a100ReadInterestBearing();
			}
			else {
				if (wholePlan.isTrue()) {
					readFundsWholePlan100();
				}
				else {
					readFundsPartPlan130();
				}
			}
		}
	}

	/**
	* <pre>
	*    IF  UTRSSUR-STATUZ = 'ENDP'  OR
	*        UTRSCLM-STATUZ = 'ENDP'
	*        MOVE 'Y'  TO  SURC-ENDF.
	* </pre>
	*/
protected void exit090()
	{
		exitProgram();
	}

protected void readFundsWholePlan100()
	{
			para101();
		}

protected void para101()
	{
		/* MOVE SPACES                 TO UTRSSUR-PARAMS.               */
		/* MOVE WSAA-CHDRCOY           TO UTRSSUR-CHDRCOY.              */
		/* MOVE WSAA-CHDRNUM           TO UTRSSUR-CHDRNUM.              */
		/* MOVE WSAA-LIFE              TO UTRSSUR-LIFE.                 */
		/* MOVE WSAA-COVERAGE          TO UTRSSUR-COVERAGE.             */
		/* MOVE WSAA-RIDER             TO UTRSSUR-RIDER.                */
		/* MOVE ZERO                   TO UTRSSUR-PLAN-SUFFIX.          */
		/* MOVE WSAA-VIRTUAL-FUND      TO UTRSSUR-UNIT-VIRTUAL-FUND.    */
		/* MOVE WSAA-UNIT-TYPE         TO UTRSSUR-UNIT-TYPE.            */
		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(wsaaChdrcoy);
		utrsIO.setChdrnum(wsaaChdrnum);
		utrsIO.setLife(wsaaLife);
		utrsIO.setCoverage(wsaaCoverage);
		utrsIO.setRider(wsaaRider);
		utrsIO.setPlanSuffix(ZERO);
		utrsIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsIO.setUnitType(wsaaUnitType);
		/* MOVE BEGN                   TO UTRSSUR-FUNCTION.        <CAS1*/
		utrsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, utrsIO);
		/* IF UTRSSUR-STATUZ        NOT = O-K  AND                 <CAS1*/
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			/*    MOVE UTRSSUR-PARAMS      TO SYSR-PARAMS                   */
			syserrrec.params.set(utrsIO.getParams());
			fatalError9000();
		}
		/* IF UTRSSUR-CHDRCOY          NOT = SURC-CHDR-CHDRCOY OR       */
		/*    UTRSSUR-CHDRNUM          NOT = SURC-CHDR-CHDRNUM OR       */
		/*    UTRSSUR-LIFE             NOT = SURC-LIFE-LIFE OR          */
		/*    UTRSSUR-COVERAGE         NOT = SURC-COVR-COVERAGE OR      */
		/*    UTRSSUR-RIDER            NOT = SURC-COVR-RIDER OR         */
		if (isNE(utrsIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsIO.getRider(),srcalcpy.covrRider)
		|| isNE(utrsIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			/*   UTRSSUR-STATUZ              = 'ENDP'                      */
			/*    MOVE 'Y'  TO  SURC-ENDF                           <INTBR> */
			srcalcpy.endf.set("I");
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			/*   MOVE 'ENDP'              TO    UTRSSUR-STATUZ             */
			/*    MOVE ENDP                TO    UTRSSUR-STATUZ        <CAS1*/
			utrsIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.fund.set(SPACES);
		/* MOVE UTRSSUR-UNIT-VIRTUAL-FUND TO WSAA-VIRTUAL-FUND.         */
		/* MOVE UTRSSUR-UNIT-TYPE         TO WSAA-UNIT-TYPE.            */
		wsaaVirtualFund.set(utrsIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsIO.getUnitType());
		while ( !(endOfFund.isTrue()
		|| isNE(utrsIO.getUnitVirtualFund(),wsaaVirtualFund)
		|| isNE(utrsIO.getUnitType(),wsaaUnitType))) {
			accumAmtWholePlan120();
		}
		
		/*IF UTRSSUR-STATUZ           = 'ENDP' OR '****'               */
		/* IF UTRSSUR-STATUZ           = ENDP OR O-K               <CAS1*/
		if (isEQ(utrsIO.getStatuz(),varcom.endp)
		|| isEQ(utrsIO.getStatuz(),varcom.oK)) {
			
			//ILIFE-9394
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UNLSURCVPM) && 
					er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))) {
				readVprnunl150();
				checkEof200();
			}
			else {
				calcFundValues();
			}
			/*      MOVE UTRSSUR-UNIT-VIRTUAL-FUND TO WSAA-VIRTUAL-FUND     */
			/*      MOVE UTRSSUR-UNIT-TYPE         TO WSAA-UNIT-TYPE        */
			/*      MOVE UTRSSUR-LIFE              TO WSAA-LIFE             */
			/*      MOVE UTRSSUR-RIDER             TO WSAA-RIDER            */
			/*      MOVE UTRSSUR-COVERAGE          TO WSAA-COVERAGE         */
			/*      MOVE UTRSSUR-CHDRNUM           TO WSAA-CHDRNUM          */
			wsaaVirtualFund.set(utrsIO.getUnitVirtualFund());
			wsaaUnitType.set(utrsIO.getUnitType());
			wsaaLife.set(utrsIO.getLife());
			wsaaRider.set(utrsIO.getRider());
			wsaaCoverage.set(utrsIO.getCoverage());
			wsaaChdrnum.set(utrsIO.getChdrnum());
			if (isEQ(srcalcpy.endf,"I")) {
				wsaaVirtualFund.set(SPACES);
				wsaaChdrnum.set(srcalcpy.chdrChdrnum);
				wsaaCoverage.set(srcalcpy.covrCoverage);
				wsaaRider.set(srcalcpy.covrRider);
				wsaaLife.set(srcalcpy.lifeLife);
			}
			wsaaSwitch.set("N");
		}
	}

protected void calcFundValues()
	{
		srcalcpy.estimatedVal.set(ZERO);
		Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
		vpxsurcrec.curduntbal.set(wsaaAccumAmount);
		vpxsurcrec.chargeCalc.set(srcalcpy.surrCalcMeth);
		srcalcpy.fund.set(wsaaVirtualFund);
		srcalcpy.type.set(utrsIO.getUnitType());
	
		callProgram(UNLSURCVPM, srcalcpy.surrenderRec, vpxsurcrec);//VPMS call
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			fatalError9000();
		}
		wsaaEstimateTotAmt.add(srcalcpy.estimatedVal);
	}

protected void accumAmtWholePlan120()
	{
		para121();
	}

protected void para121()
	{
		/* IF UTRSSUR-UNIT-TYPE        = 'I'                            */
		if (isEQ(utrsIO.getUnitType(),"I")) {
			/*ADD UTRSSUR-CURRENT-DUNIT-BAL TO WSAA-INITIAL-AMOUNT*/
			/*     ADD UTRSSUR-CURRENT-UNIT-BAL TO WSAA-INITIAL-AMOUNT      */
			wsaaInitialAmount.add(utrsIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			/*ADD UTRSSUR-CURRENT-DUNIT-BAL TO WSAA-ACCUM-AMOUNT*/
			/*     ADD UTRSSUR-CURRENT-UNIT-BAL TO WSAA-ACCUM-AMOUNT        */
			wsaaAccumAmount.add(utrsIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
		/*MOVE 'NEXTR'                TO UTRSSUR-FUNCTION.             */
		/* MOVE NEXTR                  TO UTRSSUR-FUNCTION.     <CAS1.0>*/
		utrsIO.setFunction(varcom.nextr);
		/* CALL 'UTRSSURIO'            USING UTRSSUR-PARAMS.            */
		SmartFileCode.execute(appVars, utrsIO);
		/*IF UTRSSUR-STATUZ           NOT = '****' AND                 */
		/*                            NOT = 'ENDP'                     */
		/* IF UTRSSUR-STATUZ           NOT = O-K  AND           <CAS1.0>*/
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			/*    MOVE UTRSSUR-PARAMS         TO SYSR-PARAMS                */
			syserrrec.params.set(utrsIO.getParams());
			fatalError9000();
		}
		/*IF UTRSSUR-CHDRCOY             NOT = SURC-CHDR-CHDRCOY OR    */
		/*   UTRSSUR-CHDRNUM             NOT = SURC-CHDR-CHDRNUM OR    */
		/*   UTRSSUR-LIFE                NOT = SURC-LIFE-LIFE OR       */
		/*   UTRSSUR-COVERAGE            NOT = SURC-COVR-COVERAGE OR   */
		/*   UTRSSUR-RIDER               NOT = SURC-COVR-RIDER OR      */
		if (isNE(utrsIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsIO.getRider(),srcalcpy.covrRider)
		|| isNE(utrsIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			/* A new coverage (or end of file) has been read, therefore,       */
			/* set ENDF processing indicator to "Y". This will be picked       */
			/* up immediately next time through and the PENALTY value          */
			/* will be then calculated. Avoids a needless call to the          */
			/* program between these two 'modes' of processing and hence       */
			/* eliminates the situation whereby, control is passed back        */
			/* to the calling program but there is no record to write to       */
			/* the subfile.                                                    */
			/*    MOVE 'Y'                    TO SURC-ENDF          <INTBR> */
			srcalcpy.endf.set("I");
			wsaaNoMore.set("Y");
		}
	}

protected void readFundsPartPlan130()
	{
			para131();
		}

protected void para131()
	{
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setChdrcoy(wsaaChdrcoy);
		utrsclmIO.setChdrnum(wsaaChdrnum);
		utrsclmIO.setLife(wsaaLife);
		utrsclmIO.setCoverage(wsaaCoverage);
		utrsclmIO.setRider(wsaaRider);
		utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
		utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsclmIO.setUnitType(wsaaUnitType);
		/*MOVE 'BEGN'                 TO UTRSCLM-FUNCTION.             */
		utrsclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, utrsclmIO);
		/*IF UTRSCLM-STATUZ              NOT = '****' AND              */
		/*                            NOT = 'ENDP'                     */
		/*   MOVE UTRSCLM-PARAMS         TO SYSR-PARAMS                */
		if (isNE(utrsclmIO.getStatuz(),varcom.oK)
		&& isNE(utrsclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsclmIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(),srcalcpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(),varcom.endp)) {
			/*    MOVE 'Y'  TO  SURC-ENDF                           <INTBR> */
			srcalcpy.endf.set("I");
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(srcalcpy.chdrChdrnum);
			wsaaCoverage.set(srcalcpy.covrCoverage);
			wsaaRider.set(srcalcpy.covrRider);
			wsaaLife.set(srcalcpy.lifeLife);
			/*   MOVE 'ENDP'                 TO    UTRSCLM-STATUZ          */
			utrsclmIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		while ( !(endOfFund.isTrue()
		|| isNE(utrsclmIO.getUnitVirtualFund(),wsaaVirtualFund)
		|| isNE(utrsclmIO.getUnitType(),wsaaUnitType))) {
			accumAmtPartPlan140();
		}
		
		/*IF UTRSCLM-STATUZ = '****' OR 'ENDP'                         */
		if (isEQ(utrsclmIO.getStatuz(),varcom.oK)
		|| isEQ(utrsclmIO.getStatuz(),varcom.endp)) {
			readVprnunl150();
			checkEof200();
			wsaaChdrnum.set(utrsclmIO.getChdrnum());
			wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
			wsaaUnitType.set(utrsclmIO.getUnitType());
			wsaaLife.set(utrsclmIO.getLife());
			wsaaRider.set(utrsclmIO.getRider());
			wsaaCoverage.set(utrsclmIO.getCoverage());
			wsaaPlanSuffix.set(utrsclmIO.getPlanSuffix());
			if (isEQ(srcalcpy.endf,"I")) {
				wsaaVirtualFund.set(SPACES);
				wsaaChdrnum.set(srcalcpy.chdrChdrnum);
				wsaaCoverage.set(srcalcpy.covrCoverage);
				wsaaRider.set(srcalcpy.covrRider);
				wsaaLife.set(srcalcpy.lifeLife);
			}
			wsaaSwitch.set("N");
		}
	}

protected void accumAmtPartPlan140()
	{
		para141();
	}

protected void para141()
	{
		if (isEQ(utrsclmIO.getUnitType(),"I")) {
			/*ADD UTRSCLM-CURRENT-DUNIT-BAL TO WSAA-INITIAL-AMOUNT*/
			wsaaInitialAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			/*ADD UTRSCLM-CURRENT-DUNIT-BAL TO WSAA-ACCUM-AMOUNT*/
			wsaaAccumAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
		/*MOVE 'NEXTR'                TO UTRSCLM-FUNCTION.             */
		utrsclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsclmIO);
		/*IF UTRSCLM-STATUZ              NOT = '****' AND              */
		/*                            NOT = 'ENDP'                     */
		/*   MOVE UTRSCLM-PARAMS         TO SYSR-PARAMS                */
		if (isNE(utrsclmIO.getStatuz(),varcom.oK)
		&& isNE(utrsclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsclmIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsclmIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(),srcalcpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(),srcalcpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(),srcalcpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(),varcom.endp)) {
			/* A new coverage/plan suffix (or end of file) has been read,      */
			/* set ENDF processing indicator to "Y". This will be picked       */
			/* up immediately next time through and the PENALTY value          */
			/* will be then calculated. Avoids a needless call to the          */
			/* program between these two 'modes' of processing and hence       */
			/* eliminates the situation whereby, control is passed back        */
			/* to the calling program but there is no record to write to       */
			/* the subfile.                                                    */
			/*    MOVE 'Y'                    TO SURC-ENDF          <INTBR> */
			srcalcpy.endf.set("I");
			wsaaNoMore.set("Y");
		}
	}

protected void readVprnunl150()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read151();
				case readAccum152: 
					readAccum152();
				case exit155: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read151()
	{
		/*  read the vprc file for each fund type*/
		if (isEQ(wsaaInitialAmount,ZERO)) {
			goTo(GotoLabel.readAccum152);
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(srcalcpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("I");
		vprnudlIO.setEffdate(srcalcpy.effdate);
		/*MOVE 'BEGN'                 TO VPRNUDL-FUNCTION.             */
		vprnudlIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		/*IF VPRNUDL-STATUZ           NOT = '****' AND                 */
		/*   VPRNUDL-STATUZ           NOT = 'ENDP'                     */
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		/* Move a status of NOPR to prevent a fatal error being caused  */
		/* in the calling program when there is no fund price before    */
		/* the effective date of the surrender.                         */
		if (isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(),"I")
		|| isNE(srcalcpy.chdrChdrcoy,vprnudlIO.getCompany())) {
			/*   MOVE 'MRNF'              TO SURC-STATUS                   */
			/*    MOVE MRNF                TO SURC-STATUS           <A05888>*/
			srcalcpy.status.set("NOPR");
			goTo(GotoLabel.exit155);
		}
		/*   We move information to system parameters                  */
		/*    MOVE VPRNUDL-PARAMS      TO SYSR-PARAMS           <A05888>*/
		/*    PERFORM 9000-FATAL-ERROR.                                 */
		wsaaInitialBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void readAccum152()
	{
		if (isEQ(wsaaAccumAmount,ZERO)) {
			return ;
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(srcalcpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("A");
		vprnudlIO.setEffdate(srcalcpy.effdate);
		/*MOVE 'BEGN'                 TO VPRNUDL-FUNCTION.             */
		vprnudlIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		/*IF VPRNUDL-STATUZ           NOT = '****' AND                 */
		/*   VPRNUDL-STATUZ              NOT = 'ENDP'                  */
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(),"A")
		|| isNE(srcalcpy.chdrChdrcoy,vprnudlIO.getCompany())) {
			/*   MOVE 'MRNF'              TO SURC-STATUS                   */
			/*    MOVE MRNF                TO SURC-STATUS           <A05888>*/
			srcalcpy.status.set("NOPR");
			return ;
		}
		/*   We move information to system parameters                  */
		/*    MOVE VPRNUDL-PARAMS      TO SYSR-PARAMS           <A05888>*/
		/*    PERFORM 9000-FATAL-ERROR.                                 */
		wsaaAccumBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void checkEof200()
	{
		eof210();
	}

protected void eof210()
	{
		/*   PERFORM 230-GET-SURRENDER-METH.*/
		/*IF  SURC-TYPE = 'F' OR 'P'                                   */
		/*PERFORM 250-READ-TABLE-T6655.                            */
		/*       PERFORM 260-READ-TABLE-T6656*/
		/*   ELSE*/
		/*       PERFORM 280-READ-TABLE-T5542.*/
		/*IF  SURC-PS-NOT-ALLWD = 'T'                                  */
		/*GO TO 219-EXIT.                                          */
		/* IF  SURC-TYPE = 'F'                                  <CAS1.0>*/
		/*     PERFORM 250-READ-TABLE-T6655.                    <CAS1.0>*/
		/*                                                         <CAS1.0.*/
		if (initialUnits.isTrue()) {
			compute(wsaaInitialAmount, 5).set(mult(wsaaInitialAmount,wsaaInitialBidPrice));
		}
		else {
			compute(wsaaAccumAmount, 5).set(mult(wsaaAccumAmount,wsaaAccumBidPrice));
		}
		if (initialUnits.isTrue()) {
			/*     MOVE WSAA-INITIAL-AMOUNT TO SURC-ESTIMATED-VAL           */
			compute(srcalcpy.estimatedVal, 6).setRounded(mult(wsaaInitialAmount, 1));
			srcalcpy.type.set("I");
		}
		else {
			srcalcpy.type.set("A");
			compute(srcalcpy.estimatedVal, 6).setRounded(mult(wsaaAccumAmount, 1));
		}
		/*     MOVE WSAA-ACCUM-AMOUNT   TO SURC-ESTIMATED-VAL.          */
		/* Accumulate the total surrender value at component level and     */
		/* in contract currency.                                           */
		readTable300();
		convertValue5200();
		/* ADD SURC-ESTIMATED-VAL       TO WSAA-ESTIMATE-TOT-AMT.       */
		wsaaEstimateTotAmt.add(conlinkrec.amountOut);
		srcalcpy.fund.set(wsaaVirtualFund);
	}

	/**
	* <pre>
	*230-GET-SURRENDER-METH     SECTION.                              
	*231-READ.                                                        
	**** MOVE SPACES                 TO ITDM-DATA-AREA.               
	**** MOVE SURC-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	**** MOVE T5687                  TO ITDM-ITEMTABL.                
	**** MOVE WSAA-ITEMITEM          TO ITDM-ITEMITEM.                
	**** MOVE WSAA-ITMFRM            TO ITDM-ITMFRM.                  
	*****MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	**** MOVE BEGN                   TO ITDM-FUNCTION.        <V5L012>
	**** CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	*****IF ITDM-STATUZ              NOT = '****' AND                 
	*****                            NOT = 'ENDP'                     
	**** IF ITDM-STATUZ           NOT = O-K  AND              <V5L012>
	****                          NOT = ENDP                  <V5L012>
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR.                                 
	**** IF ITDM-ITEMCOY NOT = SURC-CHDR-CHDRCOY                      
	****  OR ITDM-ITEMTABL NOT = T5687                                
	****  OR ITDM-ITEMITEM NOT = WSAA-ITEMITEM                        
	***** OR ITDM-STATUZ = 'ENDP'                                     
	****  OR ITDM-STATUZ = ENDP                               <V5L012>
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR                                  
	**** ELSE                                                         
	****    MOVE ITDM-GENAREA        TO T5687-T5687-REC.              
	*239-EXIT.                                                        
	**** EXIT.                                                        
	*250-READ-TABLE-T6655   SECTION.                                  
	*251-GO.                                                          
	**   MOVE 'Y'                    TO WSAA-READ-TABLE.
	**   MOVE SPACES                 TO ITDM-DATA-KEY.
	**   MOVE SURC-CHDR-CHDRCOY      TO ITDM-ITEMCOY.
	**   MOVE T6655                  TO ITDM-ITEMTABL.
	**   MOVE WSAA-T6655-KEY         TO ITDM-ITEMITEM.
	**   MOVE SURC-CRRCD             TO ITDM-ITMFRM.
	**   MOVE 'BEGN'                 TO ITDM-FUNCTION.
	**   CALL 'ITDMIO' USING         ITDM-PARAMS.
	**   IF ITDM-STATUZ              NOT = '****' AND
	**                               NOT = 'ENDP'
	**      MOVE ITDM-PARAMS         TO SYSR-PARAMS
	**      PERFORM 9000-FATAL-ERROR.
	**   IF ITDM-ITEMCOY             NOT = SURC-CHDR-CHDRCOY
	**    OR ITDM-ITEMTABL           NOT = T6655
	**    OR ITDM-ITEMITEM           NOT = WSAA-T6655-KEY
	**    OR ITDM-STATUZ             = 'ENDP'
	**      MOVE ITDM-PARAMS         TO SYSR-PARAMS
	**      PERFORM 9000-FATAL-ERROR
	**   ELSE
	**      MOVE ITDM-GENAREA        TO T6655-T6655-REC.
	**** IF  INITIAL-UNITS                                            
	********PERFORM 400-CALC-INIT-UNITS.
	****    MULTIPLY WSAA-INITIAL-AMOUNT BY  WSAA-INITIAL-BID-PRICE   
	****                               GIVING WSAA-INITIAL-AMOUNT     
	**** ELSE                                                         
	****    MULTIPLY WSAA-ACCUM-AMOUNT BY  WSAA-ACCUM-BID-PRICE       
	****                               GIVING WSAA-ACCUM-AMOUNT.      
	*259-EXIT.                                                        
	**** EXIT.                                                        
	*260-READ-TABLE-T6656   SECTION.                                  
	*261-GO.                                                          
	**** MOVE SPACES                 TO ITDM-DATA-AREA.               
	**** MOVE SURC-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	**** MOVE T6656                  TO ITDM-ITEMTABL.                
	**** MOVE WSAA-ITEMITEM          TO ITDM-ITEMITEM.                
	**** MOVE WSAA-ITMFRM            TO ITDM-ITMFRM.                  
	*****MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	**** MOVE BEGN                   TO ITDM-FUNCTION.        <V5L012>
	**** CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	*****IF ITDM-STATUZ              NOT = '****' AND                 
	****                             NOT = 'ENDP'                     
	**** IF ITDM-STATUZ           NOT = O-K  AND              <V5L012>
	****                          NOT = ENDP                  <V5L012>
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR.                                 
	**** IF ITDM-ITEMCOY             NOT = SURC-CHDR-CHDRCOY          
	****  OR ITDM-ITEMTABL           NOT = T6656                      
	****  OR ITDM-ITEMITEM           NOT = WSAA-ITEMITEM              
	***** OR ITDM-STATUZ             = 'ENDP'                         
	****  OR ITDM-STATUZ             = ENDP                   <V5L012>
	****  OR ITDM-VALIDFLAG          = '2'                            
	****    MOVE 'Y'                 TO WSAA-NO-T6656-ENTRY           
	**** ELSE                                                         
	*****   MOVE ' '                 TO WSAA-NO-T6656-ENTRY           
	****    MOVE SPACE               TO WSAA-NO-T6656-ENTRY   <V5L012>
	****    MOVE ITDM-GENAREA        TO T6656-T6656-REC.              
	*269-EXIT.                                                        
	**** EXIT.                                                        
	*280-READ-TABLE-T5542   SECTION.
	*281-GO.
	*****IF  T5687-PARTSURR  = SPACES                                 
	******   MOVE 'T' TO  SURC-PS-NOT-ALLWD                           
	******   GO TO 289-EXIT.                                          
	*****MOVE 'Y'                    TO WSAA-READ-TABLE.              
	*****MOVE T5687-PARTSURR         TO WSAA-T5542-METH.              
	*****MOVE SPACES                 TO ITDM-DATA-KEY.                
	*****MOVE SURC-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	*****MOVE T5542                  TO ITDM-ITEMTABL.                
	*****MOVE WSAA-T5542-KEY         TO ITDM-ITEMITEM.                
	*****MOVE SURC-CRRCD             TO ITDM-ITMFRM.                  
	*****MOVE 'BEGN'                 TO ITDM-FUNCTION.                
	*****CALL 'ITDMIO' USING         ITDM-PARAMS.                     
	*****IF ITDM-STATUZ              NOT = '****' AND                 
	*****                            NOT = 'ENDP'                     
	*****   MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	*****   PERFORM 9000-FATAL-ERROR.                                 
	*****IF ITDM-ITEMCOY             NOT = SURC-CHDR-CHDRCOY          
	***** OR ITDM-ITEMTABL           NOT = T5542                      
	***** OR ITDM-ITEMITEM           NOT = WSAA-T5542-KEY             
	***** OR ITDM-STATUZ             = 'ENDP'                         
	*****   MOVE 'T'                 TO SURC-PS-NOT-ALLWD             
	*****ELSE                                                         
	*****   MOVE ITDM-GENAREA        TO T5542-T5542-REC.              
	*289-EXIT.                                                        
	*****EXIT.                                                        
	* </pre>
	*/
protected void readTable300()
	{
			go300();
		}

protected void go300()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(srcalcpy.language);
		/*MOVE 'READR'                TO DESC-FUNCTION.                */
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		/*IF DESC-STATUZ              NOT = '****'                     */
		/*                            AND NOT = 'MRNF'                 */
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		/*IF DESC-STATUZ              = '****'                         */
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			srcalcpy.description.set(descIO.getLongdesc());
		}
		else {
			srcalcpy.description.set(SPACES);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVirtualFund);
		itdmIO.setItmfrm("99999999");
		/*MOVE 'BEGN'                 TO ITDM-FUNCTION.                */
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		/*IF ITDM-STATUZ              NOT = '****' AND                 */
		/*   ITDM-STATUZ              NOT = 'ENDP'                     */
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund,itdmIO.getItemitem())
		|| isNE(srcalcpy.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			srcalcpy.currcode.set(SPACES);
			return ;
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			srcalcpy.currcode.set(t5515rec.currcode);
		}
		srcalcpy.element.set(wsaaVirtualFund);
	}

	/**
	* <pre>
	*400-CALC-INIT-UNITS SECTION.
	*400-PARA.
	*To calculate the surrender value of initial units
	*we use the 'Nth' factor on table T6655 which has 50 occurences.
	*The occurence we use is the no. of years remaining before
	*cesstation. This factor is then used to multiply the deemed
	*units by. This is then multiplied by the bid price giving the
	*surrendrer value of initial units.
	*    DIVIDE  T6655-DIVISOR
	*            INTO T6655-SVP-FACTOR(WSAA-YRS-TO-CESS)
	*            GIVING WSAA-FACTOR.
	*    COMPUTE WSAA-INITIAL-AMOUNT ROUNDED =
	*       ((WSAA-INITIAL-AMOUNT * WSAA-FACTOR)
	*                             * WSAA-INITIAL-BID-PRICE).
	*400-EXIT.
	*    EXIT.
	* </pre>
	*/
protected void calcSurrPenVal500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para500();
					readT5551510();
					readT6598520();
					prepareToCalc530();
				case setType570: 
					setType570();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para500()
	{
		srcalcpy.actualVal.set(ZERO);
		wsaaT5551Key.set(srcalcpy.language);
		if (isNE(srcalcpy.type,"F")
		|| isEQ(wsaaEstimateTotAmt,ZERO)) {
			goTo(GotoLabel.setType570);
		}
		if (summaryPart.isTrue()) {
			if (policy1.isTrue()) {
				compute(srcalcpy.singp, 2).set((sub(srcalcpy.singp,(div(mult(srcalcpy.singp,(sub(srcalcpy.polsum,1))),srcalcpy.polsum)))));
			}
			else {
				compute(srcalcpy.singp, 3).setRounded(div(srcalcpy.singp,srcalcpy.polsum));
			}
		}
	}

protected void readT5551510()
	{
		/* Reading T5551 to obtain surrender penalty/charge method.        */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5551);
		wsaaT5551Crtable.set(srcalcpy.crtable);
		/* MOVE SURC-CURRCODE          TO WSAA-T5551-CURRCODE.  <LA4976>*/
		wsaaT5551Currcode.set(srcalcpy.chdrCurr);
		itdmIO.setItemitem(wsaaT5551Item);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemitem(),wsaaT5551Item)
		&& isEQ(itdmIO.getStatuz(),varcom.oK)) {
			wsaaT5551Key.set("*");
			readT5551510();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsaaT5551Item)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.setType570);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5551rec.svcmeth,SPACES)) {
			goTo(GotoLabel.setType570);
		}
	}

protected void readT6598520()
	{
		/* Reading T6598 using surrender penalty/charge method from T5551  */
		/* to obtain surrender penalty/charge calc routine.                */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5551rec.svcmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
		if (isEQ(t6598rec.calcprog,SPACES)) {
			goTo(GotoLabel.setType570);
		}
	}

protected void prepareToCalc530()
	{
		/* Here, we make use of the output fields as input for surrender   */
		/* penalty/charge calc routine. These fields will then move to     */
		/* WS fields in individual calc routine for further processing.    */
		srcalcpy.type.set(wsaaPlanSwitch);
		srcalcpy.estimatedVal.set(wsaaEstimateTotAmt);
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);	
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);


			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status,varcom.oK)
				//ILIFE-671
				&& isNE(srcalcpy.status, varcom.endp)) {
			
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
	}

protected void setType570()
	{
		/* Access table T6644 for description                              */
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t6644);
		descIO.setLanguage(srcalcpy.language);
		descIO.setDescitem("PEN");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setShortdesc("??????????");
		}
		srcalcpy.status.set(varcom.endp);
		wsaaSwitch.set("Y");
		srcalcpy.type.set("C");
		srcalcpy.description.set(descIO.getShortdesc());
		srcalcpy.currcode.set(SPACES);
		srcalcpy.estimatedVal.set(ZERO);
		utrsIO.setStatuz(SPACES);
		srcalcpy.endf.set(SPACES);
		srcalcpy.fund.set(SPACES);
		utrsclmIO.setStatuz(SPACES);
		covrpenIO.setStatuz(SPACES);
		vprnudlIO.setStatuz(SPACES);
		compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal,-1));
	}

	/**
	* <pre>
	*500-CALC-SURR-PEN-VAL SECTION.                                   
	*500-PARA.                                                        
	* To calculate the penalty amount which is applicable over
	*    the whole Contract/Policy/Coverage or Fund for
	*    Full Surrender.
	* We must initially access the SVP 'Surrender Value Penalty'
	*    factor table.
	* We use the 'Nth' factor on table T6656 which has 50 occurences.
	* The occurence we use is the no. of years that the premium has
	* been paid. This factor is then divided by the value in the
	* divisor field. We multiply the Annual premium by the new factor
	* to give the penalty amount.
	**   MOVE 'N'                    TO WSAA-NO-MORE.
	**   MOVE SURC-BILLFREQ          TO DTC3-FREQUENCY.
	**   MOVE SURC-CRRCD             TO WSAA-SURC-CCDATE.
	**   MOVE SURC-PTDATE            TO WSAA-SURC-PTDTE.
	**   SUBTRACT WSAA-YEAR2         FROM WSAA-YEAR1
	**                               GIVING WSAA-YRS-TO-CESS.
	**   SUBTRACT WSAA-YEAR          FROM WSAA-YEAR2
	**                               GIVING WSAA-PAID-TO-YRS.
	**   IF  WSAA-YRS-TO-CESS = 0
	**       MOVE 1 TO WSAA-YRS-TO-CESS.
	**   IF  WSAA-YRS-TO-CESS > 50
	**       MOVE 50 TO WSAA-YRS-TO-CESS.
	**   IF  WSAA-PAID-TO-YRS = 0
	**       MOVE 1 TO WSAA-PAID-TO-YRS.
	**   IF  WSAA-PAID-TO-YRS > 50
	**       MOVE 50 TO WSAA-PAID-TO-YRS.
	**** MOVE ZERO                   TO SURC-ACTUAL-VAL.              
	**** IF SUMMARY-PART                                              
	****    IF POLICY-1                                               
	****       COMPUTE SURC-SINGP    =                                
	****          (SURC-SINGP - (SURC-SINGP *                         
	****          (SURC-POLSUM - 1) / SURC-POLSUM))                   
	****    ELSE                                                      
	****       DIVIDE SURC-SINGP     BY SURC-POLSUM                   
	****                             GIVING SURC-SINGP ROUNDED.       
	**** MOVE ZEROES                 TO WSAA-PREM,                    
	****                                WSAA-ANNPREM,                 
	****                                WSAA-SVP-DIFF,                
	****                                WSAA-SVP-FACTOR,              
	****                                WSAA-SURC-FREQ,               
	****                                WSAA-PT-YEARS1,               
	****                                WSAA-PT-YEARS,                
	****                                WSAA-PT-MNTHS.                
	**** MOVE SURC-BILLFREQ          TO WSAA-SURC-FREQ.               
	**** IF  SURC-TYPE   NOT =  'F'                                   
	****     IF SURC-TYPE = 'P'                                  <007>
	****        MOVE ZEROES TO SURC-ACTUAL-VAL                   <007>
	****        GO TO 550-PART-SURR                              <007>
	****     ELSE                                                     
	**** IF  SURC-TYPE   NOT =  'F'                           <V5L012>
	****     MOVE ZEROES TO SURC-ACTUAL-VAL                   <V5L012>
	****     GO TO 570-SET-TYPE.                              <V5L012>
	*********GO TO 550-PART-SURR.
	* decide which part of the plan is being surrended
	**** IF SURC-PLAN-SUFFIX = ZERO                                   
	**** AND NOT SUMMARY-PART                                         
	* Begin on the coverage/rider record
	****    MOVE SPACES              TO COVRPEN-DATA-AREA             
	****    MOVE SURC-CHDR-CHDRCOY   TO COVRPEN-CHDRCOY               
	****    MOVE SURC-CHDR-CHDRNUM   TO COVRPEN-CHDRNUM               
	****    MOVE SURC-LIFE-LIFE      TO COVRPEN-LIFE                  
	****    MOVE SURC-COVR-COVERAGE  TO COVRPEN-COVERAGE              
	****    MOVE SURC-COVR-RIDER     TO COVRPEN-RIDER                 
	*****   MOVE 'BEGN'              TO COVRPEN-FUNCTION              
	****    MOVE BEGN                TO COVRPEN-FUNCTION      <V5L012>
	****    PERFORM 510-ACCUM-ANNPREM UNTIL                           
	*****                               COVRPEN-STATUZ = 'ENDP'       
	****                                COVRPEN-STATUZ = ENDP <V5L012>
	****     IF WSAA-ESTIMATE-TOT-AMT = 0                     <V5L012>
	****         MOVE 0              TO WSAA-ANNPREM          <V5L012>
	****     ELSE                                             <V5L012>
	****     IF WSAA-ESTIMATE-TOT-AMT < WSAA-ANNPREM          <V5L012>
	****         MOVE WSAA-ESTIMATE-TOT-AMT TO WSAA-ANNPREM   <V5L012>
	****     END-IF                                           <V5L012>
	****     END-IF                                           <V5L012>
	****    MOVE WSAA-ANNPREM TO SURC-ACTUAL-VAL                      
	****    GO TO 570-SET-TYPE.                                       
	**** MOVE ZEROES                 TO WSAA-ITEMITEM,                
	****                                WSAA-ITMFRM.                  
	**** MOVE SURC-CRTABLE           TO WSAA-ITEMITEM.                
	**** MOVE SURC-CRRCD             TO WSAA-ITMFRM.                  
	**** PERFORM 230-GET-SURRENDER-METH.                              
	**** MOVE ZEROES                 TO WSAA-ITEMITEM,                
	****                                WSAA-ITMFRM.                  
	**** MOVE T5687-SV-METHOD        TO WSAA-T6655-METH.              
	**** MOVE SURC-PSTATCODE         TO WSAA-PREM-STAT.               
	**** MOVE WSAA-T6655-KEY         TO WSAA-ITEMITEM.                
	**** MOVE SURC-CRRCD             TO WSAA-ITMFRM.                  
	**** PERFORM 260-READ-TABLE-T6656.                                
	**** IF NO-T6656-ENTRY                                            
	****    MOVE ZERO                TO WSAA-PREM                     
	**** ELSE                                                         
	****    MOVE SURC-SINGP          TO WSAA-PREM                     
	****    MOVE SPACES              TO DTC3-DATCON3-REC              
	****    MOVE SURC-CRRCD          TO DTC3-INT-DATE-1               
	****    PERFORM 520-FIND-SVP-FACTOR.                              
	* If the component has no surrender value, waive the penalty.     
	* If the penalty is higher than the surrender value, set the      
	* penalty equal to the surrender value.                           
	**** IF WSAA-ESTIMATE-TOT-AMT = 0                         <V5L012>
	****     MOVE 0                  TO WSAA-ANNPREM          <V5L012>
	****                                SURC-ACTUAL-VAL       <V5L012>
	**** ELSE                                                 <V5L012>
	**** IF WSAA-ESTIMATE-TOT-AMT < WSAA-PREM                 <V5L012>
	****     MOVE WSAA-ESTIMATE-TOT-AMT TO WSAA-ANNPREM       <V5L012>
	****                                   SURC-ACTUAL-VAL    <V5L012>
	**** ELSE                                                 <V5L012>
	**** MOVE WSAA-PREM              TO WSAA-ANNPREM                  
	****                                SURC-ACTUAL-VAL.              
	**** GO TO 570-SET-TYPE.                                          
	*550-PART-SURR.                                                   
	**The frequency codes on the table T5542 have been hard coded     
	**and are as follows 1) daily 2) single 3) annual 4) half yearly  
	**5) quarterly 6) monthly 7) every 4 weeks 8) twice per month 9)  
	**every fortnight 10) weekly.                                     
	**The penalty can be one of either A) Flat fee or B) % of surr.   
	**which is checked against a minimum and maximum value also       
	**held on the table.                                              
	**Enter a loop to get the correct surrender fee.                  
	*****IF  HAVE-TAB-DETS                                            
	*****    NEXT SENTENCE                                       <007>
	*****    GO TO HAVE-DETAILS.                                 <007>
	*****ELSE                                                    <007>
	*****MOVE ZEROES                 TO WSAA-ITEMITEM,           <007>
	*****                               WSAA-ITMFRM.             <007>
	*****MOVE SURC-CRTABLE           TO WSAA-ITEMITEM.           <007>
	*****MOVE SURC-CRRCD             TO WSAA-ITMFRM.             <007>
	*****PERFORM 230-GET-SURRENDER-METH.                         <007>
	*****IF SURC-CURRCODE NOT = SPACES                           <007>
	*****   MOVE SURC-CURRCODE       TO WSAA-T5542-CURR          <007>
	*****ELSE                                                    <007>
	*****   MOVE SURC-CHDR-CURR      TO WSAA-T5542-CURR.         <007>
	*****PERFORM 280-READ-TABLE-T5542.                           <007>
	*HAVE-DETAILS.                                               <007>
	*****                                                        <007>
	**** IF  SURC-PS-NOT-ALLWD =  'T'                                 
	****     GO TO 590-EXIT.                                          
	**** MOVE WSAA-ESTIMATE-TOT-AMT TO WSAA-ESTIMATE-TOT-AMT1.        
	**** MOVE ZERO TO WSAA-IND.                                       
	**** MOVE 'N'  TO WSAA-GOT-FREQ.                                  
	**** MOVE 'N'  TO WSAA-PS-FREQ.                                 <0
	**** IF SURC-TYPE NOT = 'P'                                     <0
	**** PERFORM 560-GET-FEE UNTIL GOT-FREQ                         <0
	**** ELSE                                                       <0
	****    PERFORM 560-GET-FEE UNTIL PS-FREQ.                      <0
	**** IF  T5542-WDL-FREQ(WSAA-IND)  NOT = ZERO                     
	**** AND SURC-EFFDATE NOT = ZERO                             <008>
	**** AND WSAA-SURC-CCDATE NOT = ZERO                         <008>
	****     PERFORM 600-CHECK-DATES-FRM-RCD.                         
	**** IF GOT-FREQ                                             <007>
	**** IF  T5542-WDL-AMOUNT(WSAA-IND) NOT = ZERO                    
	****     IF  T5542-WDL-AMOUNT(WSAA-IND) > WSAA-ESTIMATE-TOT-AMT1  
	****         MOVE 'Y' TO SURC-NE-UNITS.                           
	**** IF GOT-FREQ                                             <007>
	**** IF  T5542-WDREM(WSAA-IND) NOT = ZERO                         
	****     IF  T5542-WDREM(WSAA-IND) < WSAA-ESTIMATE-TOT-AMT1       
	****         MOVE 'Y' TO SURC-TM-UNITS.                           
	**** GO TO 570-SET-TYPE.                                          
	*560-GET-FEE.                                                     
	**** ADD 1 TO WSAA-IND.                                           
	*****IF SURC-TYPE = 'P'                                           
	*****   IF WSAA-IND = 1                                           
	*****   MOVE 'Y' TO WSAA-PS-FREQ                                  
	*****   GO TO CONTINUE-ON.                                        
	**** IF  WSAA-FREQ(WSAA-IND) = SURC-BILLFREQ                      
	****     MOVE 'Y' TO WSAA-GOT-FREQ.                               
	**** IF  NOT GOT-FREQ    AND                                      
	****     WSAA-IND = 5                                             
	****     MOVE 'Y' TO WSAA-GOT-FREQ                                
	****     MOVE ZERO TO SURC-ACTUAL-VAL                             
	****     GO TO 565-DUMMY.                                         
	*CONTINUE-ON.                                                <007>
	**** IF  GOT-FREQ  OR PS-FREQ                                   <0
	**** IF  GOT-FREQ                                            <008>
	****     IF  T5542-FFAMT(WSAA-IND) NOT = ZERO                     
	****         MOVE T5542-FFAMT(WSAA-IND) TO SURC-ACTUAL-VAL        
	****         GO TO 565-DUMMY.                                     
	*****IF  GOT-FREQ  OR PS-FREQ                                   <0
	**** IF  GOT-FREQ                                            <008>
	**** AND T5542-FEEPC(WSAA-IND)  = 0                          <008>
	****     IF  T5542-FEEPC(WSAA-IND)  = 0                      <008>
	****         MOVE ZERO TO SURC-ACTUAL-VAL                         
	****         GO TO 565-DUMMY                                      
	****     ELSE                                                <008>
	****         MOVE T5542-FEEPC(WSAA-IND) TO SURC-ACTUAL-VAL.  <008>
	**** IF  GOT-FREQ                                            <007>
	****     COMPUTE WSAA-ESTIMATE-TOT-AMT = (WSAA-ESTIMATE-TOT-AMT   
	****                             * T5542-FEEPC(WSAA-IND)) / 100   
	****     IF  WSAA-ESTIMATE-TOT-AMT < T5542-FEEMIN(WSAA-IND)       
	****         MOVE T5542-FEEMIN(WSAA-IND) TO SURC-ACTUAL-VAL       
	****     ELSE                                                     
	****         IF  WSAA-ESTIMATE-TOT-AMT  > T5542-FEEMAX(WSAA-IND)  
	****         AND T5542-FEEMAX(WSAA-IND) NOT = ZERO                
	****            MOVE T5542-FEEMAX(WSAA-IND) TO SURC-ACTUAL-VAL    
	****         ELSE                                                 
	****            MOVE WSAA-ESTIMATE-TOT-AMT TO SURC-ACTUAL-VAL.    
	**   IF  PS-FREQ                                                <0
	**       MOVE T5542-FEEPC(1) TO SURC-ACTUAL-VAL.                <0
	*                                                               <0
	*565-DUMMY.                                                       
	****************Do nowt.
	*570-SET-TYPE.                                                    
	* Access table T6644 for description                              
	**** MOVE SPACES               TO DESC-DATA-KEY.          <V5L012>
	**** MOVE 'IT'                 TO DESC-DESCPFX.           <V5L012>
	**** MOVE SURC-CHDR-CHDRCOY    TO DESC-DESCCOY.           <V5L012>
	**** MOVE T6644                TO DESC-DESCTABL.          <V5L012>
	**** MOVE 'E'                  TO DESC-LANGUAGE.          <CAS1.0>
	**** MOVE SURC-LANGUAGE        TO DESC-LANGUAGE.          <V5L012>
	**** MOVE 'PEN'                TO DESC-DESCITEM.          <V5L012>
	**** MOVE READR                TO DESC-FUNCTION.          <V5L012>
	**** MOVE DESCREC             TO DESC-FORMAT.             <V5L012>
	**** CALL 'DESCIO'             USING DESC-PARAMS.         <V5L012>
	**** IF DESC-STATUZ            NOT = O-K                  <V5L012>
	****    MOVE DESC-PARAMS       TO SYSR-PARAMS             <V5L012>
	****    PERFORM 9000-FATAL-ERROR.                         <V5L012>
	*****MOVE 'ENDP'  TO  SURC-STATUS.                                
	**** MOVE ENDP    TO  SURC-STATUS.                        <V5L012>
	**** MOVE 'Y'     TO  WSAA-SWITCH.                                
	**** MOVE 'C'     TO  SURC-TYPE.                                  
	**** MOVE SPACES    TO SURC-DESCRIPTION.                          
	*****MOVE 'PENALTY' TO SURC-DESCRIPTION.                          
	**** MOVE DESC-SHORTDESC       TO SURC-DESCRIPTION.       <V5L012>
	**** MOVE SPACES    TO SURC-CURRCODE.                             
	**** MOVE ZEROS   TO SURC-ESTIMATED-VAL.                  <V5L012>
	**** MOVE SPACES  TO UTRSSUR-STATUZ SURC-ENDF SURC-FUND           
	**** MOVE SPACES  TO UTRS-STATUZ SURC-ENDF SURC-FUND      <V5L012>
	****                 UTRSCLM-STATUZ COVRPEN-STATUZ                
	****                 VPRNUDL-STATUZ.                              
	*590-EXIT.                                                        
	**** EXIT.                                                        
	*510-ACCUM-ANNPREM SECTION.                                       
	*515-STRT.                                                        
	**** CALL 'COVRPENIO'            USING COVRPEN-PARAMS.            
	*****IF COVRPEN-STATUZ           NOT = '****'                     
	*****                        AND NOT = 'ENDP'                     
	**** IF COVRPEN-STATUZ        NOT = O-K  AND              <V5L012>
	****                          NOT = ENDP                  <V5L012>
	****    MOVE COVRPEN-PARAMS      TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR.                                 
	**** IF COVRPEN-CHDRCOY          NOT = SURC-CHDR-CHDRCOY          
	**** OR COVRPEN-CHDRNUM          NOT = SURC-CHDR-CHDRNUM          
	**** OR COVRPEN-LIFE             NOT = SURC-LIFE-LIFE             
	**** OR COVRPEN-COVERAGE         NOT = SURC-COVR-COVERAGE         
	**** OR COVRPEN-RIDER            NOT = SURC-COVR-RIDER            
	*****OR COVRPEN-STATUZ           = 'ENDP'                         
	*****   MOVE 'ENDP'              TO COVRPEN-STATUZ                
	**** OR COVRPEN-STATUZ           = ENDP                   <V5L012>
	****    MOVE ENDP                TO COVRPEN-STATUZ        <V5L012>
	****    GO TO 519-EXIT.                                           
	**** IF COVRPEN-VALIDFLAG         = '2'                   <V5L012>
	****    MOVE NEXTR               TO COVRPEN-FUNCTION      <V5L012>
	****    GO TO 519-EXIT.                                   <V5L012>
	**** MOVE ZEROES                 TO WSAA-ITEMITEM,                
	****                                WSAA-ITMFRM.                  
	**** MOVE COVRPEN-CRTABLE        TO WSAA-ITEMITEM.                
	**** MOVE COVRPEN-CRRCD          TO WSAA-ITMFRM.                  
	**** PERFORM 230-GET-SURRENDER-METH.                              
	**** MOVE ZEROES                 TO WSAA-ITEMITEM,                
	****                                WSAA-ITMFRM.                  
	**** MOVE T5687-SV-METHOD        TO WSAA-T6655-METH.              
	**** MOVE COVRPEN-PSTATCODE      TO WSAA-PREM-STAT.               
	**** MOVE WSAA-T6655-KEY         TO WSAA-ITEMITEM.                
	**** MOVE COVRPEN-CRRCD          TO WSAA-ITMFRM.                  
	**** PERFORM 260-READ-TABLE-T6656.                                
	**** MOVE ZEROES                 TO WSAA-PREM                     
	**** IF NO-T6656-ENTRY                                            
	*****   MOVE 'NEXTR'             TO COVRPEN-FUNCTION              
	****    MOVE NEXTR               TO COVRPEN-FUNCTION      <V5L012>
	****    GO TO 519-EXIT.                                           
	**** IF COVRPEN-INSTPREM         > ZERO                           
	****    MOVE COVRPEN-INSTPREM    TO WSAA-PREM                     
	**** ELSE                                                         
	****    MOVE COVRPEN-SINGP       TO WSAA-PREM.                    
	**** MOVE SPACES                 TO DTC3-DATCON3-REC.             
	**** MOVE COVRPEN-CRRCD          TO DTC3-INT-DATE-1.              
	**** PERFORM 520-FIND-SVP-FACTOR.                                 
	**** ADD WSAA-PREM               TO WSAA-ANNPREM.                 
	*****MOVE 'NEXTR'                TO COVRPEN-FUNCTION.             
	**** MOVE NEXTR                  TO COVRPEN-FUNCTION.     <V5L012>
	*519-EXIT.                                                        
	**** EXIT.                                                        
	*520-FIND-SVP-FACTOR SECTION.                                     
	*525-STRT.                                                        
	**** MOVE 'CMDF'                 TO DTC3-FUNCTION.                
	*****MOVE 'M'                    TO DTC3-FREQUENCY.               
	**** MOVE 12                     TO DTC3-FREQUENCY.               
	**** MOVE SURC-PTDATE            TO DTC3-INT-DATE-2.              
	**** CALL 'DATCON3'              USING DTC3-DATCON3-REC.          
	*****IF DTC3-STATUZ              NOT = '****'                     
	**** IF DTC3-STATUZ           NOT = O-K                   <V5L012>
	****    MOVE DTC3-DATCON3-REC    TO SYSR-PARAMS                   
	****    PERFORM 9000-FATAL-ERROR.                                 
	**** DIVIDE DTC3-FREQ-FACTOR     BY 12                            
	****                             GIVING WSAA-PT-YEARS             
	****                             REMAINDER WSAA-PT-MNTHS.         
	**** MULTIPLY WSAA-SURC-FREQ     BY WSAA-PREM                     
	****                             GIVING WSAA-PREM.                
	*  No need to subtract months from 12, months into current        
	*  year just as good as months to end of current year.            
	*    SUBTRACT WSAA-PT-MNTHS      FROM 12                          
	*                                GIVING WSAA-PT-MNTHS.            
	*    IF  WSAA-PT-YEARS           = 0                         <010>
	*        MOVE 1                  TO WSAA-PT-YEARS.           <010>
	*  No need to check here, simply add 1 anyway.                    
	*    IF  WSAA-PT-YEARS           = 0                         <010>
	*        MOVE 1                  TO WSAA-PT-YEARS            <010>
	*    ELSE                                                    <010>
	*        ADD 1 TO WSAA-PT-YEARS.                             <010>
	**** ADD 1 TO WSAA-PT-YEARS.                              <V5L012>
	**** IF  WSAA-PT-YEARS           > 50                             
	****     MOVE ZERO               TO WSAA-PT-MNTHS                 
	****     MOVE 50                 TO WSAA-PT-YEARS.                
	*  Only need to check if not on a full year boundary ie. that     
	*  MNTHS NOT = 0. Doesnt matter how many years.                   
	**** IF WSAA-PT-MNTHS            NOT = 0                          
	*    AND WSAA-PT-YEARS           < 50                             
	****    PERFORM 530-SVP-CALC                                      
	**** ELSE                                                         
	****    MOVE T6656-SVP-FACTOR(WSAA-PT-YEARS)                      
	****                             TO WSAA-SVP-FACTOR.              
	**** DIVIDE T6656-DIVISOR        INTO WSAA-SVP-FACTOR             
	****                             GIVING WSAA-FACTOR.              
	**** MULTIPLY WSAA-PREM          BY WSAA-FACTOR                   
	*                                GIVING WSAA-PREM.                
	****                             GIVING WSAA-PREM ROUNDED.<V5L012>
	*529-EXIT.                                                        
	**** EXIT.                                                        
	*530-SVP-CALC SECTION.                                            
	*535-STRT.                                                        
	**** MOVE WSAA-PT-YEARS          TO WSAA-PT-YEARS1.               
	**** ADD 1                       TO WSAA-PT-YEARS1.               
	*  Replace entire interpolation routine using the following.      
	*  Given :                                                        
	*      FACTOR  is the factor at  YEARS                            
	*      FACTOR1 is the factor at  YEARS1                           
	*  Calculate :                                                    
	*      A factor F at a general time T between YEARS and YEARS1    
	*  Using the relationship :                                       
	*            T  -  YEARS             F - FACTOR                   
	*          YEARS1 - YEARS         FACTOR1 - FACTOR                
	*  Get the formula, F = :                                         
	*  (((T-YEARS) * (FACTOR1-FACTOR)) / (YEARS1-YEARS))   +   FACTOR 
	*  Where :                                                        
	*      YEARS   = WSAA-PT-YEARS (expressed in months)              
	*      YEARS1  = WSAA-PT-YEARS1 (expressed in months)             
	*      FACTOR  = T6656-SVP-FACTOR(WSAA-PT-YEARS)                  
	*      FACTOR1 = T6656-SVP-FACTOR(WSAA-PT-YEARS1)                 
	*  Note :                                                         
	*      T       = WSAA-PT-YEARS + WSAA-PT-MNTHS                    
	*       ( hence, T - YEARS = WSAA-PT-MONTHS )                     
	*      YEARS1 - YEARS always gives 12 months                      
	*  Store value of WSAA-PT-YEARS before converting to months       
	**** MOVE WSAA-PT-YEARS              TO WSAA-STORE-YEARS. <V5L012>
	*  Find the difference between the two relevant factors.          
	**** SUBTRACT T6656-SVP-FACTOR(WSAA-PT-YEARS)                     
	****    FROM T6656-SVP-FACTOR(WSAA-PT-YEARS1)                     
	****       GIVING WSAA-SVP-DIFF.                                  
	*  Convert figures to months.                                     
	**** MULTIPLY WSAA-PT-YEARS  BY 12 GIVING WSAA-PT-YEARS.  <V5L012>
	**** MULTIPLY WSAA-PT-YEARS1 BY 12 GIVING WSAA-PT-YEARS1. <V5L012>
	**** COMPUTE WSAA-SVP-FACTOR =                            <V5L012>
	*        ( WSAA-PT-MNTHS * WSAA-SVP-DIFF / 12 )  +                
	****     ( WSAA-SVP-DIFF * WSAA-PT-MNTHS / 12 )  +        <V5L012>
	****                  T6656-SVP-FACTOR (WSAA-STORE-YEARS).<V5L012>
	*    IF WSAA-SVP-DIFF            < ZERO                           
	*       MULTIPLY WSAA-SVP-DIFF   BY -1                            
	*                                GIVING WSAA-SVP-DIFF.            
	*    IF WSAA-SVP-DIFF            = ZERO                           
	*       MOVE T6656-SVP-FACTOR(WSAA-PT-YEARS)                      
	*          TO WSAA-SVP-FACTOR                                     
	*       GO TO 539-EXIT.                                           
	*    COMPUTE WSAA-SVP-DIFF ROUNDED =                              
	*       ((WSAA-SVP-DIFF / 12) * WSAA-PT-MNTHS).                   
	*    IF T6656-SVP-FACTOR(WSAA-PT-YEARS) >                         
	*             T6656-SVP-FACTOR(WSAA-PT-YEARS1)                    
	*       ADD WSAA-SVP-DIFF                                         
	*          TO T6656-SVP-FACTOR(WSAA-PT-YEARS1)                    
	*       MOVE T6656-SVP-FACTOR(WSAA-PT-YEARS1)                     
	*          TO WSAA-SVP-FACTOR                                     
	*    ELSE                                                         
	*       ADD WSAA-SVP-DIFF                                         
	*          TO T6656-SVP-FACTOR(WSAA-PT-YEARS)                     
	*       MOVE T6656-SVP-FACTOR(WSAA-PT-YEARS)                      
	*          TO WSAA-SVP-FACTOR.                                    
	*539-EXIT.                                                        
	**** EXIT.                                                        
	*600-CHECK-DATES-FRM-RCD SECTION.                                 
	*600-STRT.                                                        
	**** MOVE     SPACES           TO DTC3-DATCON3-REC.               
	**** MOVE    'CMDF'            TO DTC3-FUNCTION.                  
	**** MOVE     'M'              TO DTC3-FREQUENCY.                 
	**** MOVE     WSAA-SURC-CCDATE TO DTC3-INT-DATE-1.                
	**** MOVE     SURC-EFFDATE     TO DTC3-INT-DATE-2.                
	**** CALL     'DATCON3' USING     DTC3-DATCON3-REC.               
	**** IF  DTC3-STATUZ NOT = '****'                                 
	****     MOVE DTC3-DATCON3-REC TO SYSR-PARAMS                     
	****     PERFORM 9000-FATAL-ERROR.                                
	**** IF  DTC3-FREQ-FACTOR < T5542-WDL-FREQ(WSAA-IND)              
	****     MOVE 'Y' TO SURC-PS-NOT-ALLWD.                           
	*690-EXIT.                                                        
	**** EXIT.                                                        
	* </pre>
	*/
protected void convertValue5200()
	{
			call5210();
		}

protected void call5210()
	{
		if (isEQ(srcalcpy.currcode,srcalcpy.chdrCurr)) {
			conlinkrec.amountOut.set(srcalcpy.estimatedVal);
			return ;
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(srcalcpy.estimatedVal);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(srcalcpy.effdate);
		conlinkrec.currIn.set(srcalcpy.currcode);
		conlinkrec.currOut.set(srcalcpy.chdrCurr);
		conlinkrec.company.set(srcalcpy.chdrChdrcoy);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError9000();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(srcalcpy.chdrChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(srcalcpy.chdrCurr);
		zrdecplrec.batctrcde.set("****");
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A000-EXIT*/
	}

protected void a100ReadInterestBearing()
	{
			a110Para();
		}

	/**
	* <pre>
	************************************                      <INTBR> 
	* </pre>
	*/
protected void a110Para()
	{
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(wsaaChdrcoy);
		hitsIO.setChdrnum(wsaaChdrnum);
		hitsIO.setLife(wsaaLife);
		hitsIO.setCoverage(wsaaCoverage);
		hitsIO.setRider(wsaaRider);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setZintbfnd(wsaaVirtualFund);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(hitsIO.getLife(),srcalcpy.lifeLife)
		|| isNE(hitsIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(hitsIO.getRider(),srcalcpy.covrRider)
		|| isEQ(hitsIO.getStatuz(),varcom.endp)) {
			srcalcpy.endf.set("Y");
			hitsIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.fund.set(SPACES);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		while ( !(endOfFund.isTrue()
		|| isNE(hitsIO.getZintbfnd(),wsaaVirtualFund))) {
			a120AccumInterestBearing();
		}
		
		if (isEQ(hitsIO.getStatuz(),varcom.endp)
		|| isEQ(hitsIO.getStatuz(),varcom.oK)) {
			
			//ILIFE-9394
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UNLSURCVPM) && 
					er.isExternalized(srcalcpy.cnttype.toString(), srcalcpy.crtable.toString()))) {
				a200CheckEof();
			}
			else {
				calcInterestBearing();
			}
			
			wsaaVirtualFund.set(hitsIO.getZintbfnd());
			wsaaLife.set(hitsIO.getLife());
			wsaaRider.set(hitsIO.getRider());
			wsaaCoverage.set(hitsIO.getCoverage());
			wsaaChdrnum.set(hitsIO.getChdrnum());
			wsaaSwitch.set("N");
		}
	}

protected void calcInterestBearing() {
	srcalcpy.estimatedVal.set(ZERO);
	Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
	vpxsurcrec.curduntbal.set(wsaaAccumAmount);
	vpxsurcrec.chargeCalc.set(srcalcpy.surrCalcMeth);
	srcalcpy.fund.set(wsaaVirtualFund);
	srcalcpy.type.set("D");

	callProgram(UNLSURCVPM, srcalcpy.surrenderRec, vpxsurcrec);//VPMS call
	if (isNE(srcalcpy.status,varcom.oK)
	&& isNE(srcalcpy.status,varcom.endp)) {
		fatalError9000();
	}
	wsaaEstimateTotAmt.add(srcalcpy.estimatedVal);
}

protected void a120AccumInterestBearing()
	{
		/*A121-PARA*/
		wsaaAccumAmount.add(hitsIO.getZcurprmbal());
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(hitsIO.getLife(),srcalcpy.lifeLife)
		|| isNE(hitsIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(hitsIO.getRider(),srcalcpy.covrRider)
		|| isEQ(hitsIO.getStatuz(),varcom.endp)) {
			/* A new coverage (or end of file) has been read, therefore,       */
			/* set ENDF processing indicator to "Y". This will be picked       */
			/* up immediately next time through and the PENALTY value  <INTBR> */
			/* will be then calculated. Avoids a needless call to the  <INTBR> */
			/* program between these two 'modes' of processing and hence       */
			/* eliminates the situation whereby, control is passed back<INTBR> */
			/* to the calling program but there is no record to write to       */
			/* the subfile.                                            <INTBR> */
			srcalcpy.endf.set("Y");
			wsaaNoMore.set("Y");
		}
		/*A129-EXIT*/
	}

protected void a200CheckEof()
	{
		/*A210-CHECK*/
		/* MOVE WSAA-ACCUM-AMOUNT      TO SURC-ESTIMATED-VAL.   <V76F06>*/
		compute(srcalcpy.estimatedVal, 6).setRounded(mult(wsaaAccumAmount, 1));
		readTable300();
		convertValue5200();
		wsaaEstimateTotAmt.add(conlinkrec.amountOut);
		srcalcpy.fund.set(wsaaVirtualFund);
		srcalcpy.type.set("D");
		/*A290-EXIT*/
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		/*IF SYSR-STATUZ              = 'BOMB'                         */
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		/*MOVE 'BOMB'                 TO SURC-STATUS.                  */
		srcalcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
