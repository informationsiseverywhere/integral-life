package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5428
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5428ScreenVars extends SmartVarModel { 

//ILIFE-1340 Start by vchawda
	public FixedLengthStringData dataArea = new FixedLengthStringData(91);
	public FixedLengthStringData dataFields = new FixedLengthStringData(27).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData jobno = DD.jobno.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 27);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData jobnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 43);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] jobnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	
//ILIFE-1340 End
	public FixedLengthStringData subfileArea = new FixedLengthStringData(223);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(77).isAPartOf(subfileArea, 0);
	public ZonedDecimalData accBidPrice = DD.abidpr.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData accBarePrice = DD.abrepr.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData accOfferPrice = DD.aoffpr.copyToZonedDecimal().isAPartOf(subfileFields,20);
	public ZonedDecimalData initBidPrice = DD.ibidpr.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData initBarePrice = DD.ibrepr.copyToZonedDecimal().isAPartOf(subfileFields,40);
	public ZonedDecimalData initOfferPrice = DD.ioffpr.copyToZonedDecimal().isAPartOf(subfileFields,50);
	public FixedLengthStringData update = DD.update.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,68);
	public ZonedDecimalData xtranno = DD.xtranno.copyToZonedDecimal().isAPartOf(subfileFields,72);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 77);
	public FixedLengthStringData abidprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData abreprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData aoffprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData ibidprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData ibreprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData ioffprErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData updateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData xtrannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 113);
	public FixedLengthStringData[] abidprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] abreprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] aoffprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] ibidprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] ibreprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] ioffprOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] updateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] xtrannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 221);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S5428screensflWritten = new LongData(0);
	public LongData S5428screenctlWritten = new LongData(0);
	public LongData S5428screenWritten = new LongData(0);
	public LongData S5428protectWritten = new LongData(0);
	public GeneralTable s5428screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData flag=new FixedLengthStringData(1).init(BaseScreenData.ENABLED);
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5428screensfl;
	}

	public S5428ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vrtfndOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ibreprOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ibidprOut,new String[] {"03","09","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ioffprOut,new String[] {"04","09","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(abreprOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(abidprOut,new String[] {"06","10","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aoffprOut,new String[] {"07","10","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(updateOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jobnoOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {xtranno, unitVirtualFund, initBarePrice, initBidPrice, initOfferPrice, accBarePrice, accBidPrice, accOfferPrice, update};
		screenSflOutFields = new BaseData[][] {xtrannoOut, vrtfndOut, ibreprOut, ibidprOut, ioffprOut, abreprOut, abidprOut, aoffprOut, updateOut};
		screenSflErrFields = new BaseData[] {xtrannoErr, vrtfndErr, ibreprErr, ibidprErr, ioffprErr, abreprErr, abidprErr, aoffprErr, updateErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {company, effdate, jobno, shortdesc};
		screenOutFields = new BaseData[][] {companyOut, effdateOut, jobnoOut, shortdescOut};
		screenErrFields = new BaseData[] {companyErr, effdateErr, jobnoErr, shortdescErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5428screen.class;
		screenSflRecord = S5428screensfl.class;
		screenCtlRecord = S5428screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5428protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5428screenctl.lrec.pageSubfile);
	}
}
