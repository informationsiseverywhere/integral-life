/*
 * File: T5543pt.java
 * Date: 30 August 2009 2:22:24
 * Author: Quipoz Limited
 * 
 * Class transformed from T5543PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5543.
*
*
*****************************************************************
* </pre>
*/
public class T5543pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Available Virtual Funds                 S5543");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 25, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(26);
	private FixedLengthStringData filler9 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Available Virtual Funds:");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(51);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(39).isAPartOf(wsaaPrtLine005, 12, FILLER).init("Code                        Description");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(62);
	private FixedLengthStringData filler12 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 12);
	private FixedLengthStringData filler13 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 32);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(62);
	private FixedLengthStringData filler14 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 12);
	private FixedLengthStringData filler15 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine007, 32);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(62);
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 12);
	private FixedLengthStringData filler17 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine008, 32);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(62);
	private FixedLengthStringData filler18 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 12);
	private FixedLengthStringData filler19 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 32);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(62);
	private FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 12);
	private FixedLengthStringData filler21 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 32);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(62);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 12);
	private FixedLengthStringData filler23 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 32);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(62);
	private FixedLengthStringData filler24 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 12);
	private FixedLengthStringData filler25 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 32);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(62);
	private FixedLengthStringData filler26 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 12);
	private FixedLengthStringData filler27 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 32);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(62);
	private FixedLengthStringData filler28 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 12);
	private FixedLengthStringData filler29 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine014, 32);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(62);
	private FixedLengthStringData filler30 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 12);
	private FixedLengthStringData filler31 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine015, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine015, 32);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(62);
	private FixedLengthStringData filler32 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 12);
	private FixedLengthStringData filler33 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine016, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine016, 32);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(62);
	private FixedLengthStringData filler34 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 12);
	private FixedLengthStringData filler35 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine017, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine017, 32);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5543rec t5543rec = new T5543rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5543pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5543rec.t5543Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5543rec.unitVirtualFund1);
		fieldNo008.set(t5543rec.vfundesc1);
		fieldNo009.set(t5543rec.unitVirtualFund2);
		fieldNo010.set(t5543rec.vfundesc2);
		fieldNo011.set(t5543rec.unitVirtualFund3);
		fieldNo012.set(t5543rec.vfundesc3);
		fieldNo013.set(t5543rec.unitVirtualFund4);
		fieldNo014.set(t5543rec.vfundesc4);
		fieldNo015.set(t5543rec.unitVirtualFund5);
		fieldNo016.set(t5543rec.vfundesc5);
		fieldNo017.set(t5543rec.unitVirtualFund6);
		fieldNo018.set(t5543rec.vfundesc6);
		fieldNo019.set(t5543rec.unitVirtualFund7);
		fieldNo020.set(t5543rec.vfundesc7);
		fieldNo021.set(t5543rec.unitVirtualFund8);
		fieldNo022.set(t5543rec.vfundesc8);
		fieldNo023.set(t5543rec.unitVirtualFund9);
		fieldNo024.set(t5543rec.vfundesc9);
		fieldNo025.set(t5543rec.unitVirtualFund10);
		fieldNo026.set(t5543rec.vfundesc10);
		fieldNo027.set(t5543rec.unitVirtualFund11);
		fieldNo028.set(t5543rec.vfundesc11);
		fieldNo029.set(t5543rec.unitVirtualFund12);
		fieldNo030.set(t5543rec.vfundesc12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
