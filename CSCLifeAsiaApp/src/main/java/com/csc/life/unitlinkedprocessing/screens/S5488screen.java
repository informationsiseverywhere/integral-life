package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5488screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5488ScreenVars sv = (S5488ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5488screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5488ScreenVars screenVars = (S5488ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.winfndopt.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.rcdateDisp.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbrcd.setClassString("");
		screenVars.instprem.setClassString("");
		screenVars.virtFundSplitMethod.setClassString("");
		screenVars.percOrAmntInd.setClassString("");
		screenVars.unitVirtualFund01.setClassString("");
		screenVars.unitAllocPercAmt01.setClassString("");
		screenVars.unitVirtualFund02.setClassString("");
		screenVars.unitAllocPercAmt02.setClassString("");
		screenVars.unitVirtualFund03.setClassString("");
		screenVars.unitAllocPercAmt03.setClassString("");
		screenVars.unitVirtualFund04.setClassString("");
		screenVars.unitAllocPercAmt04.setClassString("");
		screenVars.unitVirtualFund05.setClassString("");
		screenVars.unitAllocPercAmt05.setClassString("");
		screenVars.unitVirtualFund06.setClassString("");
		screenVars.unitAllocPercAmt06.setClassString("");
		screenVars.unitVirtualFund07.setClassString("");
		screenVars.unitAllocPercAmt07.setClassString("");
		screenVars.unitVirtualFund08.setClassString("");
		screenVars.unitAllocPercAmt08.setClassString("");
		screenVars.unitVirtualFund09.setClassString("");
		screenVars.unitAllocPercAmt09.setClassString("");
		screenVars.unitVirtualFund10.setClassString("");
		screenVars.unitAllocPercAmt10.setClassString("");
		screenVars.planSuffix.setClassString("");
		//ILIFE-8164- STARTS
		screenVars.newFundList01.setClassString("");
		screenVars.newFundList02.setClassString("");
		screenVars.newFundList03.setClassString("");
		screenVars.newFundList04.setClassString("");
		screenVars.newFundList05.setClassString("");
		screenVars.newFundList06.setClassString("");
		screenVars.newFundList07.setClassString("");
		screenVars.newFundList08.setClassString("");
		screenVars.newFundList09.setClassString("");
		screenVars.newFundList10.setClassString("");
		screenVars.newFundList11.setClassString("");
		screenVars.newFundList12.setClassString("");
		//ILIFE-8164- ENDS
	}

/**
 * Clear all the variables in S5488screen
 */
	public static void clear(VarModel pv) {
		S5488ScreenVars screenVars = (S5488ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.winfndopt.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifenum.clear();
		screenVars.linsname.clear();
		screenVars.crtable.clear();
		screenVars.crtabdesc.clear();
		screenVars.rcdateDisp.clear();
		screenVars.rcdate.clear();
		screenVars.zagelit.clear();
		screenVars.anbrcd.clear();
		screenVars.instprem.clear();
		screenVars.virtFundSplitMethod.clear();
		screenVars.percOrAmntInd.clear();
		screenVars.unitVirtualFund01.clear();
		screenVars.unitAllocPercAmt01.clear();
		screenVars.unitVirtualFund02.clear();
		screenVars.unitAllocPercAmt02.clear();
		screenVars.unitVirtualFund03.clear();
		screenVars.unitAllocPercAmt03.clear();
		screenVars.unitVirtualFund04.clear();
		screenVars.unitAllocPercAmt04.clear();
		screenVars.unitVirtualFund05.clear();
		screenVars.unitAllocPercAmt05.clear();
		screenVars.unitVirtualFund06.clear();
		screenVars.unitAllocPercAmt06.clear();
		screenVars.unitVirtualFund07.clear();
		screenVars.unitAllocPercAmt07.clear();
		screenVars.unitVirtualFund08.clear();
		screenVars.unitAllocPercAmt08.clear();
		screenVars.unitVirtualFund09.clear();
		screenVars.unitAllocPercAmt09.clear();
		screenVars.unitVirtualFund10.clear();
		screenVars.unitAllocPercAmt10.clear();
		screenVars.planSuffix.clear();
		//ILIFE-8164 -STARTS
		screenVars.newFundList01.clear();
		screenVars.newFundList02.clear();
		screenVars.newFundList03.clear();
		screenVars.newFundList04.clear();
		screenVars.newFundList05.clear();
		screenVars.newFundList06.clear();
		screenVars.newFundList07.clear();
		screenVars.newFundList08.clear();
		screenVars.newFundList09.clear();
		screenVars.newFundList10.clear();
		screenVars.newFundList11.clear();
		screenVars.newFundList12.clear();
		//ILIFE-8164 -ENDS
	}
}
