package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Utrnpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String termid = "";
	private BigDecimal jobnoPrice;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private String unitVirtualFund = "";
	private String unitSubAccount = "";
	private Long strpdate;
	private String nowDeferInd = "";
	private BigDecimal nofUnits;
	private String unitType = "";
	private String batccoy = "";
	private String batcbrn = "";
	private int batcactyr;
	private int batcactmn;
	private String batctrcde = "";
	private String batcbatch = "";
	private Long priceDateUsed;
	private Long moniesDate;
	private BigDecimal priceUsed;
	private String cntcurr = "";
	private String crtable = "";
	private BigDecimal nofDunits;
	private String feedbackInd = "";
	private String covdbtind = "";
	private BigDecimal unitBarePrice;
	private int inciNum;
	private int inciPerd01;
	private int inciPerd02;
	private BigDecimal inciprm01;
	private BigDecimal inciprm02;
	private int ustmno;
	private BigDecimal contractAmount;
	private String fundCurrency = "";
	private BigDecimal fundAmount;
	private BigDecimal fundRate;
	private String sacscode = "";
	private String sacstyp = "";
	private String genlcde = "";
	private String triggerModule = "";
	private String triggerKey = "";
	private String cnttyp = "";
	private int procSeqNo;
	private BigDecimal surrenderPercent;
	private BigDecimal svp;
	private BigDecimal discountFactor;
	private Long crComDate;
	private String canInitUnitInd = "";
	private String switchIndicator = "";
	private String scheduleName = "";
	private int scheduleNumber;
	private String userProfile;
	private String jobName;
	private String datime;
	private Dry5102DTO dry5101Dto;
	private String fundpool;
	private String contyp; //ILIFE-8786
	public Utrnpf(){
		
	}

	public Utrnpf(Utrnpf u) {
		this.uniqueNumber = u.uniqueNumber;
		this.chdrcoy = u.chdrcoy;
		this.chdrnum = u.chdrnum;
		this.life = u.life;
		this.coverage = u.coverage;
		this.rider = u.rider;
		this.planSuffix = u.planSuffix;
		this.tranno = u.tranno;
		this.termid = u.termid;
		this.jobnoPrice = u.jobnoPrice;
		this.transactionDate = u.transactionDate;
		this.transactionTime = u.transactionTime;
		this.user = u.user;
		this.unitVirtualFund = u.unitVirtualFund;
		this.unitSubAccount = u.unitSubAccount;
		this.strpdate = u.strpdate;
		this.nowDeferInd = u.nowDeferInd;
		this.nofUnits = u.nofUnits;
		this.unitType = u.unitType;
		this.batccoy = u.batccoy;
		this.batcbrn = u.batcbrn;
		this.batcactyr = u.batcactyr;
		this.batcactmn = u.batcactmn;
		this.batctrcde = u.batctrcde;
		this.batcbatch = u.batcbatch;
		this.priceDateUsed = u.priceDateUsed;
		this.moniesDate = u.moniesDate;
		this.priceUsed = u.priceUsed;
		this.cntcurr = u.cntcurr;
		this.crtable = u.crtable;
		this.nofDunits = u.nofDunits;
		this.feedbackInd = u.feedbackInd;
		this.covdbtind = u.covdbtind;
		this.unitBarePrice = u.unitBarePrice;
		this.inciNum = u.inciNum;
		this.inciPerd01 = u.inciPerd01;
		this.inciPerd02 = u.inciPerd02;
		this.inciprm01 = u.inciprm01;
		this.inciprm02 = u.inciprm02;
		this.ustmno = u.ustmno;
		this.contractAmount = u.contractAmount;
		this.fundCurrency = u.fundCurrency;
		this.fundAmount = u.fundAmount;
		this.fundRate = u.fundRate;
		this.sacscode = u.sacscode;
		this.sacstyp = u.sacstyp;
		this.genlcde = u.genlcde;
		this.triggerModule = u.triggerModule;
		this.triggerKey = u.triggerKey;
		this.cnttyp = u.cnttyp;
		this.procSeqNo = u.procSeqNo;
		this.surrenderPercent = u.surrenderPercent;
		this.svp = u.svp;
		this.discountFactor = u.discountFactor;
		this.crComDate = u.crComDate;
		this.canInitUnitInd = u.canInitUnitInd;
		this.switchIndicator = u.switchIndicator;
		this.scheduleName = u.scheduleName;
		this.scheduleNumber = u.scheduleNumber;
		this.userProfile = u.userProfile;
		this.jobName = u.jobName;
		this.datime = u.datime;
		this.dry5101Dto = u.dry5101Dto;
		this.fundpool = u.fundpool;
		this.contyp = u.contyp; //ILIFE-8786
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public BigDecimal getJobnoPrice() {
		return jobnoPrice;
	}

	public void setJobnoPrice(BigDecimal jobnoPrice) {
		this.jobnoPrice = jobnoPrice;
	}

	public int getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}

	public int getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getUnitVirtualFund() {
		return unitVirtualFund;
	}

	public void setUnitVirtualFund(String unitVirtualFund) {
		this.unitVirtualFund = unitVirtualFund;
	}

	public String getUnitSubAccount() {
		return unitSubAccount;
	}

	public void setUnitSubAccount(String unitSubAccount) {
		this.unitSubAccount = unitSubAccount;
	}

	public Long getStrpdate() {
		return strpdate;
	}

	public void setStrpdate(Long strpdate) {
		this.strpdate = strpdate;
	}

	public String getNowDeferInd() {
		return nowDeferInd;
	}

	public void setNowDeferInd(String nowDeferInd) {
		this.nowDeferInd = nowDeferInd;
	}

	public BigDecimal getNofUnits() {
		return nofUnits;
	}

	public void setNofUnits(BigDecimal nofUnits) {
		this.nofUnits = nofUnits;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getBatccoy() {
		return batccoy;
	}

	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}

	public String getBatcbrn() {
		return batcbrn;
	}

	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}

	public int getBatcactyr() {
		return batcactyr;
	}

	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}

	public int getBatcactmn() {
		return batcactmn;
	}

	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	public String getBatcbatch() {
		return batcbatch;
	}

	public void setBatcbatch(String batcbatch) {
		this.batcbatch = batcbatch;
	}

	public Long getPriceDateUsed() {
		return priceDateUsed;
	}

	public void setPriceDateUsed(Long priceDateUsed) {
		this.priceDateUsed = priceDateUsed;
	}

	public Long getMoniesDate() {
		return moniesDate;
	}

	public void setMoniesDate(Long moniesDate) {
		this.moniesDate = moniesDate;
	}

	public BigDecimal getPriceUsed() {
		return priceUsed;
	}

	public void setPriceUsed(BigDecimal priceUsed) {
		this.priceUsed = priceUsed;
	}

	public String getCntcurr() {
		return cntcurr;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public BigDecimal getNofDunits() {
		return nofDunits;
	}

	public void setNofDunits(BigDecimal nofDunits) {
		this.nofDunits = nofDunits;
	}

	public String getFeedbackInd() {
		return feedbackInd;
	}

	public void setFeedbackInd(String feedbackInd) {
		this.feedbackInd = feedbackInd;
	}

	public String getCovdbtind() {
		return covdbtind;
	}

	public void setCovdbtind(String covdbtind) {
		this.covdbtind = covdbtind;
	}

	public BigDecimal getUnitBarePrice() {
		return unitBarePrice;
	}

	public void setUnitBarePrice(BigDecimal unitBarePrice) {
		this.unitBarePrice = unitBarePrice;
	}

	public int getInciNum() {
		return inciNum;
	}

	public void setInciNum(int inciNum) {
		this.inciNum = inciNum;
	}

	public int getInciPerd01() {
		return inciPerd01;
	}

	public void setInciPerd01(int inciPerd01) {
		this.inciPerd01 = inciPerd01;
	}

	public int getInciPerd02() {
		return inciPerd02;
	}

	public void setInciPerd02(int inciPerd02) {
		this.inciPerd02 = inciPerd02;
	}

	public BigDecimal getInciprm01() {
		return inciprm01;
	}

	public void setInciprm01(BigDecimal inciprm01) {
		this.inciprm01 = inciprm01;
	}

	public BigDecimal getInciprm02() {
		return inciprm02;
	}

	public void setInciprm02(BigDecimal inciprm02) {
		this.inciprm02 = inciprm02;
	}

	public int getUstmno() {
		return ustmno;
	}

	public void setUstmno(int ustmno) {
		this.ustmno = ustmno;
	}

	public BigDecimal getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}

	public String getFundCurrency() {
		return fundCurrency;
	}

	public void setFundCurrency(String fundCurrency) {
		this.fundCurrency = fundCurrency;
	}

	public BigDecimal getFundAmount() {
		return fundAmount;
	}

	public void setFundAmount(BigDecimal fundAmount) {
		this.fundAmount = fundAmount;
	}

	public BigDecimal getFundRate() {
		return fundRate;
	}

	public void setFundRate(BigDecimal fundRate) {
		this.fundRate = fundRate;
	}

	public String getSacscode() {
		return sacscode;
	}

	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}

	public String getSacstyp() {
		return sacstyp;
	}

	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}

	public String getGenlcde() {
		return genlcde;
	}

	public void setGenlcde(String genlcde) {
		this.genlcde = genlcde;
	}

	public String getTriggerModule() {
		return triggerModule;
	}

	public void setTriggerModule(String triggerModule) {
		this.triggerModule = triggerModule;
	}

	public String getTriggerKey() {
		return triggerKey;
	}

	public void setTriggerKey(String triggerKey) {
		this.triggerKey = triggerKey;
	}

	public String getCnttyp() {
		return cnttyp;
	}

	public void setCnttyp(String cnttyp) {
		this.cnttyp = cnttyp;
	}

	public int getProcSeqNo() {
		return procSeqNo;
	}

	public void setProcSeqNo(int procSeqNo) {
		this.procSeqNo = procSeqNo;
	}

	public BigDecimal getSurrenderPercent() {
		return surrenderPercent;
	}

	public void setSurrenderPercent(BigDecimal surrenderPercent) {
		this.surrenderPercent = surrenderPercent;
	}

	public BigDecimal getSvp() {
		return svp;
	}

	public void setSvp(BigDecimal svp) {
		this.svp = svp;
	}

	public BigDecimal getDiscountFactor() {
		return discountFactor;
	}

	public void setDiscountFactor(BigDecimal discountFactor) {
		this.discountFactor = discountFactor;
	}

	public Long getCrComDate() {
		return crComDate;
	}

	public void setCrComDate(Long crComDate) {
		this.crComDate = crComDate;
	}

	public String getCanInitUnitInd() {
		return canInitUnitInd;
	}

	public void setCanInitUnitInd(String canInitUnitInd) {
		this.canInitUnitInd = canInitUnitInd;
	}

	public String getSwitchIndicator() {
		return switchIndicator;
	}

	public void setSwitchIndicator(String switchIndicator) {
		this.switchIndicator = switchIndicator;
	}

	public String getScheduleName() {
		return scheduleName;
	}

	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	public int getScheduleNumber() {
		return scheduleNumber;
	}

	public void setScheduleNumber(int scheduleNumber) {
		this.scheduleNumber = scheduleNumber;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public Dry5102DTO getDry5101Dto() {
		return dry5101Dto;
	}

	public void setDry5101Dto(Dry5102DTO dry5101Dto) {
		this.dry5101Dto = dry5101Dto;
	}
	public void setFundPool(String fundpool) {
		this.fundpool = fundpool;
	}

	public String getFundPool() {
		return fundpool;
	}

	@Override
	public String toString() {
		return "Utrnpf [uniqueNumber=" + uniqueNumber + ", chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", life="
				+ life + ", coverage=" + coverage + ", rider=" + rider + ", planSuffix=" + planSuffix + ", tranno="
				+ tranno + ", termid=" + termid + ", jobnoPrice=" + jobnoPrice + ", transactionDate=" + transactionDate
				+ ", transactionTime=" + transactionTime + ", user=" + user + ", unitVirtualFund=" + unitVirtualFund
				+ ", unitSubAccount=" + unitSubAccount + ", strpdate=" + strpdate + ", nowDeferInd=" + nowDeferInd
				+ ", nofUnits=" + nofUnits + ", unitType=" + unitType + ", batccoy=" + batccoy + ", batcbrn=" + batcbrn
				+ ", batcactyr=" + batcactyr + ", batcactmn=" + batcactmn + ", batctrcde=" + batctrcde + ", batcbatch="
				+ batcbatch + ", priceDateUsed=" + priceDateUsed + ", moniesDate=" + moniesDate + ", priceUsed="
				+ priceUsed + ", cntcurr=" + cntcurr + ", crtable=" + crtable + ", nofDunits=" + nofDunits
				+ ", feedbackInd=" + feedbackInd + ", covdbtind=" + covdbtind + ", unitBarePrice=" + unitBarePrice
				+ ", inciNum=" + inciNum + ", inciPerd01=" + inciPerd01 + ", inciPerd02=" + inciPerd02 + ", inciprm01="
				+ inciprm01 + ", inciprm02=" + inciprm02 + ", ustmno=" + ustmno + ", contractAmount=" + contractAmount
				+ ", fundCurrency=" + fundCurrency + ", fundAmount=" + fundAmount + ", fundRate=" + fundRate
				+ ", sacscode=" + sacscode + ", sacstyp=" + sacstyp + ", genlcde=" + genlcde + ", triggerModule="
				+ triggerModule + ", triggerKey=" + triggerKey + ", cnttyp=" + cnttyp + ", procSeqNo=" + procSeqNo
				+ ", surrenderPercent=" + surrenderPercent + ", svp=" + svp + ", discountFactor=" + discountFactor
				+ ", crComDate=" + crComDate + ", canInitUnitInd=" + canInitUnitInd + ", switchIndicator="
				+ switchIndicator + ", scheduleName=" + scheduleName + ", scheduleNumber=" + scheduleNumber
				+ ", userProfile=" + userProfile + ", jobName=" + jobName + ", datime=" + datime + ", dry5101Dto="
				+ dry5101Dto + "]";
	}
}