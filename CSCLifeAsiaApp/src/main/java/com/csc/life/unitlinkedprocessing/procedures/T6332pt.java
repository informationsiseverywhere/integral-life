/*
 * File: T6332pt.java
 * Date: 30 August 2009 2:27:17
 * Author: Quipoz Limited
 * 
 * Class transformed from T6332PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T6332rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6332.
*
*
*****************************************************************
* </pre>
*/
public class T6332pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Unit Linked Single Premium Edit Rules           S6332");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23, FILLER).init(SPACES);
	private FixedLengthStringData filler6 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine002, 28, FILLER).init("Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler7 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(42);
	private FixedLengthStringData filler8 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler9 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 25, FILLER).init("   To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 32);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(73);
	private FixedLengthStringData filler10 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Sum Insured Min:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 18).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine004, 33, FILLER).init("    Prem. Multiplier for S.I.:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine004, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(33);
	private FixedLengthStringData filler12 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 13, FILLER).init("Max:");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 18).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(28);
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine006, 7, FILLER).init("Unit Reserve Flag:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 27);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(54);
	private FixedLengthStringData filler16 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine007, 7, FILLER).init("Lien Codes:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27);
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 32);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 37);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 42);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 47);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 52);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6332rec t6332rec = new T6332rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6332pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6332rec.t6332Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6332rec.sumInsMin);
		fieldNo008.set(t6332rec.premmult);
		fieldNo009.set(t6332rec.sumInsMax);
		fieldNo010.set(t6332rec.rsvflg);
		fieldNo011.set(t6332rec.liencd01);
		fieldNo012.set(t6332rec.liencd02);
		fieldNo013.set(t6332rec.liencd03);
		fieldNo014.set(t6332rec.liencd04);
		fieldNo015.set(t6332rec.liencd05);
		fieldNo016.set(t6332rec.liencd06);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
