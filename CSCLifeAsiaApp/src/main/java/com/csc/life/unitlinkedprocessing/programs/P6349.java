/*
 * File: P6349.java
 * Date: 30 August 2009 0:44:10
 * Author: Quipoz Limited
 * 
 * Class transformed from P6349.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.screens.S6349ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
*               P6349 - Component Journal Submenu.
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Initialise the screen for display.
*
*  Call DATCON1 with today's date and move to the screen Effdate
*  field and WSSP-Effdate.
*
*
*Validation
*----------
*
*  Validate contract number entered by performing a READR on the
*  Logical file CHDRMJA.
*
*  Validate Key - 1
*
*       If S6349-CHDRSEL not  equal  to  spaces call the CHDRMJA
*       I/O module for  the  contract.  If  any  combination  of
*       CHDRSEL and SUBP-KEY1 other than O-K and 'Y' then error.
*
*       Release the COVRMJA I/O module in case it has previously
*       been held.
*
*       Check STATCODE to validate  status of the contract is In
*       Force (IF), against T5679.
*
*       If NOT valid then Error.
*
*  Validate Key - 2
*
*       If the screen  effective  date has changed then validate
*       this date against the header record (CHDRMJA).
*
*       If S6349-effdate less than CHDRMJA-occdate then error.
*
*       If   S6349-effdate   greater   than   CHDRMJA-currto  or
*       S6349-effdate less than CHDRMJA-currfrom then error.
*
*Updating
*--------
*
*
*  At this point  we  must  distinguish  Journals from any other
*  Submenu as Journals  will  share  the following common select
*  programs. The reason  being, Selection across  the whole plan
*  (select option =  '0000')  is  only valid for Journals if the
*  number summarised equals  the  number  of policies within the
*  Plan. Fund Switching, however,  can select across  broken out
*  policies within a Plan.
*
*       If SCRN-ACTION = 'I' move 'J' to WSSP-FLAG
*       Else move 'N' to WSSP-FLAG.
*
*
*  If  WSSP-FLAG  is NOT = 'J' then softlock the contract header
*  by  calling SFTLOCK. If the record is already locked - Statuz
*  =  'LOCK', then  error  with a code of F910 and redisplay the
*  screen.
*
*  If  a valid contract header record found then perform a KEEPS
*  on  the  record  in  order  to store it for use in the select
*  programs.
*
*
*Next Program.
*-------------
*
*  Otherwise add 1 to the Program pointer and exit to the Policy
*  selection screen.
*
*
*****************************************************************
* </pre>
*/
public class P6349 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6349");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaStatFlag = new FixedLengthStringData(1);
	private Validator wsaaExitStat = new Validator(wsaaStatFlag, "Y");
		/* TABLES */
	private static final String t5679 = "T5679";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Subprogrec subprogrec = new Subprogrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6349ScreenVars sv = ScreenProgram.getScreenVars( S6349ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2290, 
		exit2590, 
		batching3080, 
		exit3090
	}

	public P6349() {
		super();
		screenVars = sv;
		new ScreenModel("S6349", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Initialise screen.*/
		sv.dataArea.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		wsaaStatFlag.set("N");
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		/*    Move todays date to the screen effective date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.effdate.set(datcon1rec.intDate);
		wsspsmart.effdate.set(datcon1rec.intDate);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6349IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6349-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*    Validate all fields including batch request if required.*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2500();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		/*    Call subprog routine to load the stack with the next*/
		/*    programs.*/
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		/*    Call sanctn routine to check that the user has been*/
		/*    sanctioned to execute Journaling.*/
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		try {
			validateKey12210();
			validateKey22220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateKey12210()
	{
		/*    Release any previously kept CHDRMJA record.*/
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*    Read the Header record for the contract number entered.*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.reads);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrmjaIO);
		}
		else {
			chdrmjaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*    If no record found and Key1 in T1690 is set to 'Y' then*/
		/*    error.*/
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set(errorsInner.e544);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2290);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		/*    Release any previously kept COVRMJA record.*/
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		/*    Validate contract selected is of the correct status and*/
		/*    from the correct branch.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2300();
		}
		/*    If incorrect Branch then error.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrmjaIO.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
		}
	}

protected void validateKey22220()
	{
		/*    Validation of the effective date has already been done*/
		/*    against the table by the I/O module.*/
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")
		&& isEQ(sv.effdateErr, SPACES)) {
			return ;
		}
		if (isNE(sv.effdateErr, SPACES)) {
			return ;
		}
		/*    Validate the effective date is within a valid range within*/
		/*    the contract.*/
		if (isLT(sv.effdate, chdrmjaIO.getOccdate())) {
			sv.effdateErr.set(errorsInner.t044);
		}
		else {
			if ((isGT(sv.effdate, chdrmjaIO.getCurrto())
			|| isLT(sv.effdate, chdrmjaIO.getCurrfrom()))) {
				/* AND S6349-EFFDATE           < CHDRMJA-CURRFROM)              */
				sv.effdateErr.set(errorsInner.f990);
			}
		}
	}

protected void checkStatus2300()
	{
		readStatusTable2310();
	}

protected void readStatusTable2310()
	{
		/*    Read the Status table to check it is an in force contract.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.action.set(errorsInner.f321);
			return ;
		}
		/*    GO TO 2490-EXIT.                                          */
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		/*    Look through the statii.*/
		wsaaStatFlag.set("N");
		while ( !(wsaaExitStat.isTrue())) {
			lookForStat2400();
		}
		
		if (isEQ(sv.chdrselErr, SPACES)) {
			wsaaStatFlag.set("N");
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| isEQ(wsaaStatFlag, "Y")); wsaaSub.add(1)){
				chdrPremStat2450();
			}
			if (isEQ(wsaaStatFlag, "N")) {
				sv.chdrselErr.set(errorsInner.f204);
			}
		}
		/* If unprocessed HITRs exists, you are not allowed to             */
		/* create Interest Journals.                                       */
		if (isNE(scrnparams.action, "C")) {
			return ;
		}
		hitraloIO.setRecKeyData(SPACES);
		hitraloIO.setChdrnum(sv.chdrsel);
		hitraloIO.setChdrcoy(wsspcomn.company);
		hitraloIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(), varcom.oK)
		&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(hitraloIO.getStatuz());
			syserrrec.params.set(hitraloIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.chdrsel, hitraloIO.getChdrnum())
		&& isEQ(wsspcomn.company, hitraloIO.getChdrcoy())
		&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
			sv.chdrselErr.set(errorsInner.hl08);
			return ;
		}
	}

protected void lookForStat2400()
	{
		/*SEARCH*/
		/*    Table Loop.*/
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
			wsaaStatFlag.set("Y");
		}
		else {
			if (isNE(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStatFlag.set("N");
			}
			else {
				wsaaStatFlag.set("Y");
			}
		}
		/*EXIT*/
	}

protected void chdrPremStat2450()
	{
		/*START*/
		if (isEQ(chdrmjaIO.getPstatcode(), t5679rec.cnPremStat[wsaaSub.toInt()])) {
			wsaaStatFlag.set("Y");
		}
		/*EXIT*/
	}

protected void verifyBatchControl2500()
	{
		try {
			validateRequest2510();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2510()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
		}
		goTo(GotoLabel.exit2590);
	}

protected void retrieveBatchProgs2520()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
					screenAction3020();
					softLock3030();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		/*    Update the WSSP storage area.*/
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*MOVE S6349-EFFDATE TO WSSP-EFFDATE.                          */
		if (isNE(sv.effdate, varcom.vrcmMaxDate)) {
			wsspsmart.effdate.set(sv.effdate);
		}
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
	}

protected void screenAction3020()
	{
		/*    Update the WSSP flag.*/
		/*    WSSP-FLAG will be used to validate selection accross the*/
		/*    plan in the Plan selection screen.*/
		/* IF SCRN-ACTION              = 'I'                            */
		if (isEQ(scrnparams.action, "I")
		|| isEQ(scrnparams.action, "H")) {
			wsspcomn.flag.set("J");
		}
		/*    IF SCRN-ACTION              = 'A'                            */
		/* IF SCRN-ACTION              = 'A' OR 'B'             <V75L01>*/
		if (isEQ(scrnparams.action, "A")
		|| isEQ(scrnparams.action, "B")
		|| isEQ(scrnparams.action, "C")) {
			wsspcomn.flag.set("N");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			batching3080();
			goTo(GotoLabel.exit3090);
		}
		/* IF SCRN-ACTION              = 'I'                    <V75L01>*/
		if (isEQ(scrnparams.action, "I")
		|| isEQ(scrnparams.action, "H")) {
			goTo(GotoLabel.batching3080);
		}
	}

protected void softLock3030()
	{
		/*    Soft lock the contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S6349-CHDRSEL          TO SFTL-ENTITY.                  */
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

	/**
	* <pre>
	*3070-KEEPS.
	*   Store the contract header for use by the transaction programs
	*    MOVE 'KEEPS'                TO CHDRMJA-FUNCTION.
	*    MOVE CHDRMJAREC             TO CHDRMJA-FORMAT.
	*    CALL 'CHDRMJAIO' USING CHDRMJA-PARAMS.
	*    IF CHDRMJA-STATUZ           NOT = O-K
	*       MOVE CHDRMJA-PARAMS      TO SYSR-PARAMS
	*       PERFORM 600-FATAL-ERROR.
	* </pre>
	*/
protected void batching3080()
	{
		/*    Update Batch control if required.*/
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData f990 = new FixedLengthStringData(4).init("F990");
	private FixedLengthStringData t044 = new FixedLengthStringData(4).init("T044");
	private FixedLengthStringData f204 = new FixedLengthStringData(4).init("F204");
	private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
}
}
