package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList; //ILIFE-8786
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZrstpfDAOImpl extends BaseDAOImpl<Zrstpf> implements ZrstpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZrstpfDAOImpl.class);

	public Map<String, List<Zrstpf>> searchZrstnudRecordByChdrnum(List<String> chdrnumList) {
		StringBuilder sqlZrstSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,USTMNO,FDBKIND ");
		sqlZrstSelect1.append("FROM ZRSTNUD WHERE ");
		sqlZrstSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlZrstSelect1
		.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psZrstSelect = getPrepareStatement(sqlZrstSelect1.toString());
		ResultSet sqlzrstpf1rs = null;
		Map<String, List<Zrstpf>> resultMap = new HashMap<>();
		try {
			sqlzrstpf1rs = executeQuery(psZrstSelect);
			while (sqlzrstpf1rs.next()) {
				Zrstpf z = new Zrstpf();
				z.setUniqueNumber(sqlzrstpf1rs.getLong("UNIQUE_NUMBER"));
				z.setChdrcoy(sqlzrstpf1rs.getString("chdrcoy"));
				z.setChdrnum(sqlzrstpf1rs.getString("chdrnum"));
				z.setLife(sqlzrstpf1rs.getString("life"));
				z.setJlife(sqlzrstpf1rs.getString("jlife"));
				z.setCoverage(sqlzrstpf1rs.getString("coverage"));
				z.setRider(sqlzrstpf1rs.getString("rider"));
				z.setSeqno(sqlzrstpf1rs.getInt("seqno"));
				z.setTranno(sqlzrstpf1rs.getInt("tranno"));
				z.setXtranno(sqlzrstpf1rs.getInt("xtranno"));
				z.setBatctrcde(sqlzrstpf1rs.getString("batctrcde"));
				z.setSacstyp(sqlzrstpf1rs.getString("sacstyp"));
				z.setZramount01(sqlzrstpf1rs.getBigDecimal("zramount01"));
				z.setZramount02(sqlzrstpf1rs.getBigDecimal("zramount02"));
				z.setTrandate(sqlzrstpf1rs.getInt("trandate"));
				z.setUstmno(sqlzrstpf1rs.getInt("ustmno"));
				z.setFeedbackInd(sqlzrstpf1rs.getString("fdbkind"));
				if (resultMap.containsKey(z.getChdrnum())) {
					resultMap.get(z.getChdrnum()).add(z);
				} else {
					List<Zrstpf> zrstpfList = new ArrayList<>();
					zrstpfList.add(z);
					resultMap.put(z.getChdrnum(), zrstpfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchZrstnudRecordByChdrnum()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psZrstSelect, sqlzrstpf1rs);
		}
		return resultMap;
	}


	public void insertZrstpfRecord(List<Zrstpf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ZRSTPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,USTMNO,FDBKIND,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());

		try {
			for (Zrstpf u : urList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getJlife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setInt(i++, u.getSeqno());
				ps.setInt(i++, u.getTranno());
				ps.setInt(i++, u.getXtranno());
				ps.setString(i++, u.getBatctrcde());
				ps.setString(i++, u.getSacstyp());
				ps.setBigDecimal(i++, u.getZramount01());
				ps.setBigDecimal(i++, u.getZramount02());
				ps.setInt(i++, u.getTrandate());
				ps.setInt(i++, u.getUstmno());
				ps.setString(i++, u.getFeedbackInd());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
			    ps.addBatch();
            }		
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertZrstpfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public void updateZrstXTranno(List<Zrstpf> urList) {
		if (urList == null || urList.isEmpty()) {
			return;
		}
		String sql = "UPDATE ZRSTPF SET XTRANNO=?,FDBKIND=?,USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=?";
		PreparedStatement ps = getPrepareStatement(sql);

		try {
			for (Zrstpf u : urList) {
				int i = 1;
				ps.setInt(i++, u.getXtranno());
				ps.setString(i++, u.getFeedbackInd());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.setLong(i++, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateZrstXTranno()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public Map<String, List<Zrstpf>> searchZrstRecordByChdrnum(List<String> chdrnumList, String chdrcoy) {
		StringBuilder sqlZrstSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,USTMNO,FDBKIND ");
		sqlZrstSelect1.append("FROM ZRST WHERE LIFE='01' AND COVERAGE='01' AND RIDER='00' AND CHDRCOY=? AND ");

		sqlZrstSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlZrstSelect1
		.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psZrstSelect = getPrepareStatement(sqlZrstSelect1.toString());
		ResultSet sqlzrstpf1rs = null;
		Map<String, List<Zrstpf>> resultMap = new HashMap<>();
		try {
			psZrstSelect.setString(1, chdrcoy);
			sqlzrstpf1rs = executeQuery(psZrstSelect);
			while (sqlzrstpf1rs.next()) {
				Zrstpf z = new Zrstpf();
				z.setUniqueNumber(sqlzrstpf1rs.getLong("UNIQUE_NUMBER"));
				z.setChdrcoy(sqlzrstpf1rs.getString("chdrcoy"));
				z.setChdrnum(sqlzrstpf1rs.getString("chdrnum"));
				z.setLife(sqlzrstpf1rs.getString("life"));
				z.setJlife(sqlzrstpf1rs.getString("jlife"));
				z.setCoverage(sqlzrstpf1rs.getString("coverage"));
				z.setRider(sqlzrstpf1rs.getString("rider"));
				z.setSeqno(sqlzrstpf1rs.getInt("seqno"));
				z.setTranno(sqlzrstpf1rs.getInt("tranno"));
				z.setXtranno(sqlzrstpf1rs.getInt("xtranno"));
				z.setBatctrcde(sqlzrstpf1rs.getString("batctrcde"));
				z.setSacstyp(sqlzrstpf1rs.getString("sacstyp"));
				z.setZramount01(sqlzrstpf1rs.getBigDecimal("zramount01"));
				z.setZramount02(sqlzrstpf1rs.getBigDecimal("zramount02"));
				z.setTrandate(sqlzrstpf1rs.getInt("trandate"));
				z.setUstmno(sqlzrstpf1rs.getInt("ustmno"));
				z.setFeedbackInd(sqlzrstpf1rs.getString("fdbkind"));
				if (resultMap.containsKey(z.getChdrnum())) {
					resultMap.get(z.getChdrnum()).add(z);
				} else {
					List<Zrstpf> zrstpfList = new ArrayList<>();
					zrstpfList.add(z);
					resultMap.put(z.getChdrnum(), zrstpfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchZrstRecordByChdrnum()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psZrstSelect, sqlzrstpf1rs);
		}
		return resultMap;
	}
	public Map<String, List<Zrstpf>> searchZrsttraRecordByChdrnum(List<String> chdrnumList, String chdrcoy) {
		StringBuilder sqlZrstSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,USTMNO,FDBKIND ");

		sqlZrstSelect1.append("FROM ZRSTTRA WHERE LIFE='01' AND COVERAGE='01' AND RIDER='00' AND CHDRCOY=? AND ");

		sqlZrstSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlZrstSelect1
		.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psZrstSelect = getPrepareStatement(sqlZrstSelect1.toString());
		ResultSet sqlzrstpf1rs = null;
		Map<String, List<Zrstpf>> resultMap = new HashMap<>();
		try {
			psZrstSelect.setString(1, chdrcoy);
			sqlzrstpf1rs = executeQuery(psZrstSelect);
			while (sqlzrstpf1rs.next()) {
				Zrstpf z = new Zrstpf();
				z.setUniqueNumber(sqlzrstpf1rs.getLong("UNIQUE_NUMBER"));
				z.setChdrcoy(sqlzrstpf1rs.getString("chdrcoy"));
				z.setChdrnum(sqlzrstpf1rs.getString("chdrnum"));
				z.setLife(sqlzrstpf1rs.getString("life"));
				z.setJlife(sqlzrstpf1rs.getString("jlife"));
				z.setCoverage(sqlzrstpf1rs.getString("coverage"));
				z.setRider(sqlzrstpf1rs.getString("rider"));
				z.setSeqno(sqlzrstpf1rs.getInt("seqno"));
				z.setTranno(sqlzrstpf1rs.getInt("tranno"));
				z.setXtranno(sqlzrstpf1rs.getInt("xtranno"));
				z.setBatctrcde(sqlzrstpf1rs.getString("batctrcde"));
				z.setSacstyp(sqlzrstpf1rs.getString("sacstyp"));
				z.setZramount01(sqlzrstpf1rs.getBigDecimal("zramount01"));
				z.setZramount02(sqlzrstpf1rs.getBigDecimal("zramount02"));
				z.setTrandate(sqlzrstpf1rs.getInt("trandate"));
				z.setUstmno(sqlzrstpf1rs.getInt("ustmno"));
				z.setFeedbackInd(sqlzrstpf1rs.getString("fdbkind"));
				if (resultMap.containsKey(z.getChdrnum())) {
					resultMap.get(z.getChdrnum()).add(z);
				} else {
					List<Zrstpf> zrstpfList = new ArrayList<>();
					zrstpfList.add(z);
					resultMap.put(z.getChdrnum(), zrstpfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchZrsttraRecordByChdrnum()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psZrstSelect, sqlzrstpf1rs);
		}
		return resultMap;
	}	

	public Map<String, List<Zrstpf>> searchZrststmByChdrnum(List<String> chdrnumList, String chdrcoy) {
		StringBuilder sqlZrstSelect1 = new StringBuilder(
				"SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,USTMNO,FDBKIND ");
		sqlZrstSelect1.append("FROM ZRSTPF WHERE LIFE='01' AND CHDRCOY=? AND ");

		sqlZrstSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlZrstSelect1
		.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psZrstSelect = getPrepareStatement(sqlZrstSelect1.toString());
		ResultSet sqlzrstpf1rs = null;
		Map<String, List<Zrstpf>> resultMap = new HashMap<>();
		try {
			psZrstSelect.setString(1, chdrcoy);
			sqlzrstpf1rs = executeQuery(psZrstSelect);
			while (sqlzrstpf1rs.next()) {
				Zrstpf z = new Zrstpf();
				z.setUniqueNumber(sqlzrstpf1rs.getLong("UNIQUE_NUMBER"));
				z.setChdrcoy(sqlzrstpf1rs.getString("chdrcoy"));
				z.setChdrnum(sqlzrstpf1rs.getString("chdrnum"));
				z.setLife(sqlzrstpf1rs.getString("life"));
				z.setJlife(sqlzrstpf1rs.getString("jlife"));
				z.setCoverage(sqlzrstpf1rs.getString("coverage"));
				z.setRider(sqlzrstpf1rs.getString("rider"));
				z.setSeqno(sqlzrstpf1rs.getInt("seqno"));
				z.setTranno(sqlzrstpf1rs.getInt("tranno"));
				z.setXtranno(sqlzrstpf1rs.getInt("xtranno"));
				z.setBatctrcde(sqlzrstpf1rs.getString("batctrcde"));
				z.setSacstyp(sqlzrstpf1rs.getString("sacstyp"));
				z.setZramount01(sqlzrstpf1rs.getBigDecimal("zramount01"));
				z.setZramount02(sqlzrstpf1rs.getBigDecimal("zramount02"));
				z.setTrandate(sqlzrstpf1rs.getInt("trandate"));
				z.setUstmno(sqlzrstpf1rs.getInt("ustmno"));
				z.setFeedbackInd(sqlzrstpf1rs.getString("fdbkind"));
				if (resultMap.containsKey(z.getChdrnum())) {
					resultMap.get(z.getChdrnum()).add(z);
				} else {
					List<Zrstpf> zrstpfList = new ArrayList<>();
					zrstpfList.add(z);
					resultMap.put(z.getChdrnum(), zrstpfList);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("searchZrstRecordByChdrnum()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psZrstSelect, sqlzrstpf1rs);
		}
		return resultMap;
	}
	public void updateZrststm(List<Zrstpf> zrstUpdateList) {
		if (zrstUpdateList == null || zrstUpdateList.isEmpty()) {
			return;
		}
		String sql = "UPDATE ZRSTPF SET USTMNO = ?,ZRAMOUNT02 = ?, USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=?"; //ILIFE-8786
		PreparedStatement ps = getPrepareStatement(sql);

		try {
			for (Zrstpf u : zrstUpdateList) {
				int i = 1;
				ps.setInt(i++, u.getUstmno());
				ps.setBigDecimal(i++, u.getZramount02()); //ILIFE-8786
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.setLong(i++, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateZrststm()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	public List<Zrstpf> searchZrstReccordByChdrnumFdbkInd(String chdrcoy, String chdrnum){
		List<Zrstpf> zrstList = new ArrayList<>();
		Zrstpf zrstpf;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, TRANNO, XTRANNO, ZRAMOUNT01, ZRAMOUNT02 FROM ZRSTPF WHERE CHDRCOY=? AND CHDRNUM=? AND FDBKIND=?");
		PreparedStatement ps = null;
		ResultSet rs=null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, " ");
			rs = ps.executeQuery();
			while(rs.next()) {
				zrstpf = new Zrstpf();
				zrstpf.setChdrcoy(rs.getString("CHDRCOY"));
				zrstpf.setChdrnum(rs.getString("CHDRNUM"));
				zrstpf.setLife(rs.getString("LIFE"));
				zrstpf.setJlife(rs.getString("JLIFE"));
				zrstpf.setCoverage(rs.getString("COVERAGE"));
				zrstpf.setRider(rs.getString("RIDER"));
				zrstpf.setTranno(rs.getInt("TRANNO"));
				zrstpf.setXtranno(rs.getInt("XTRANNO"));
				zrstpf.setZramount01(rs.getBigDecimal("ZRAMOUNT01"));
				zrstpf.setZramount02(rs.getBigDecimal("ZRAMOUNT02"));
				zrstList.add(zrstpf);
			}
		}
		catch(SQLException e) {
			LOGGER.error("searchZrstReccordByChdrnumFdbkInd()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return zrstList;
	}

 //ILIFE-8786 start
	@Override
	public List<Zrstpf> searchZrstnudRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, String fdbkind) {
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,SEQNO,TRANNO,XTRANNO,BATCTRCDE,SACSTYP,ZRAMOUNT01,ZRAMOUNT02,TRANDATE,USTMNO,FDBKIND,USRPRF,JOBNM,DATIME  ");
		sqlSelect1.append("FROM ZRSTPF ");
		sqlSelect1.append(" WHERE  CHDRCOY = ?  and CHDRNUM = ? and LIFE = ? and COVERAGE = ? AND RIDER = ? and FDBKIND = ?");
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Zrstpf> zrstnudList=new LinkedList<>();

		try {
			psSelect.setString(1, chdrcoy);
			psSelect.setString(2, chdrnum);
			psSelect.setString(3, life);
			psSelect.setString(4, coverage);
			psSelect.setString(5, rider);
			psSelect.setString(6, fdbkind);

			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Zrstpf z = new Zrstpf();
				z.setUniqueNumber(sqlrs.getLong("UNIQUE_NUMBER"));
				z.setChdrcoy(sqlrs.getString("chdrcoy"));
				z.setChdrnum(sqlrs.getString("chdrnum"));
				z.setLife(sqlrs.getString("life"));
				z.setJlife(sqlrs.getString("jlife"));
				z.setCoverage(sqlrs.getString("coverage"));
				z.setRider(sqlrs.getString("rider"));
				z.setSeqno(sqlrs.getInt("seqno"));
				z.setTranno(sqlrs.getInt("tranno"));
				z.setXtranno(sqlrs.getInt("xtranno"));
				z.setBatctrcde(sqlrs.getString("batctrcde"));
				z.setSacstyp(sqlrs.getString("sacstyp"));
				z.setZramount01(sqlrs.getBigDecimal("zramount01"));
				z.setZramount02(sqlrs.getBigDecimal("zramount02"));
				z.setTrandate(sqlrs.getInt("trandate"));
				z.setUstmno(sqlrs.getInt("ustmno"));
				z.setFeedbackInd(sqlrs.getString("fdbkind"));
				z.setUserProfile(getUsrprf());
				z.setJobName(getJobnm());
				z.setDatime(new Timestamp(System.currentTimeMillis()));
				zrstnudList.add(z);
			}

		} catch (SQLException e) {
			LOGGER.error("searchZrstnudRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return zrstnudList;

	}
 //ILIFE-8786 end 

	/*
	 * IBPLIFE-3963 start, new method created as per the requirement. The method
	 * check different parameter in where clause Update method created to update
	 * ZRAMOUNT02 column.
	 */
	
	@Override
	public List<Zrstpf> searchZrstReccordByChdrnumLife(String chdrcoy, String chdrnum, 
			String life, String covergae) {
		List<Zrstpf> zrstList = new ArrayList<>();
		Zrstpf zrstpf;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT UNIQUE_NUMBER,CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER,"
				+ " TRANNO, XTRANNO, ZRAMOUNT01, ZRAMOUNT02 "
				+ "FROM ZRSTPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND FDBKIND=? AND COVERAGE=? "
				+ "ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, "
				+ "COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = null;
		ResultSet rs=null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, " ");
			ps.setString(5, covergae);
			rs = ps.executeQuery();
			while(rs.next()) {
				zrstpf = new Zrstpf();
				zrstpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				zrstpf.setChdrcoy(rs.getString("CHDRCOY"));
				zrstpf.setChdrnum(rs.getString("CHDRNUM"));
				zrstpf.setLife(rs.getString("LIFE"));
				zrstpf.setJlife(rs.getString("JLIFE"));
				zrstpf.setCoverage(rs.getString("COVERAGE"));
				zrstpf.setRider(rs.getString("RIDER"));
				zrstpf.setTranno(rs.getInt("TRANNO"));
				zrstpf.setXtranno(rs.getInt("XTRANNO"));
				zrstpf.setZramount01(rs.getBigDecimal("ZRAMOUNT01"));
				zrstpf.setZramount02(rs.getBigDecimal("ZRAMOUNT02"));
				zrstList.add(zrstpf);
			}
		}
		catch(SQLException e) {
			LOGGER.error("searchZrstReccordByChdrnumLife()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return zrstList;
	}


	@Override
	public void updateZrst(List<Zrstpf> zrstUpdateList) {
		if (zrstUpdateList == null || zrstUpdateList.isEmpty()) {
			return;
		}
		String sql = "UPDATE ZRSTPF SET ZRAMOUNT02 = ?, USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=?";
		PreparedStatement ps = getPrepareStatement(sql);

		try {
			for (Zrstpf u : zrstUpdateList) {
				ps.setBigDecimal(1, u.getZramount02()); 
				ps.setString(2, getUsrprf());
				ps.setString(3, getJobnm());
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setLong(5, u.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateZrst()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	//IBPLIFE-3963 end
}