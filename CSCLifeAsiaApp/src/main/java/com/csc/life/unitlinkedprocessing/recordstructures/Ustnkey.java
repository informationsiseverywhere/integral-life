package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:45
 * Description:
 * Copybook name: USTNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ustnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ustnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ustnKey = new FixedLengthStringData(64).isAPartOf(ustnFileKey, 0, REDEFINE);
  	public FixedLengthStringData ustnChdrcoy = new FixedLengthStringData(1).isAPartOf(ustnKey, 0);
  	public FixedLengthStringData ustnChdrnum = new FixedLengthStringData(8).isAPartOf(ustnKey, 1);
  	public PackedDecimalData ustnPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ustnKey, 9);
  	public FixedLengthStringData ustnLife = new FixedLengthStringData(2).isAPartOf(ustnKey, 12);
  	public FixedLengthStringData ustnCoverage = new FixedLengthStringData(2).isAPartOf(ustnKey, 14);
  	public FixedLengthStringData ustnRider = new FixedLengthStringData(2).isAPartOf(ustnKey, 16);
  	public FixedLengthStringData ustnUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(ustnKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(ustnKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ustnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ustnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}