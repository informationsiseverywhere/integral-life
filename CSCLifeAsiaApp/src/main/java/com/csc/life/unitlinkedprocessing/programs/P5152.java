/*   
 * File: P5152.java
 * Date: 30 August 2009 0:15:52
 * Author: Quipoz Limited
 * 
 * Class transformed from P5152.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;//ILIFE-8078
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltmjaTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S5152ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Nxtprogrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*AUTHOR.            PAXUS FINANCIAL SYSTEMS PTY. LTD.
*REMARKS.
*  P5152 - Component Change - Premium Direction.
*  --------------------------------------------
*
* This program creates transaction records which describe a set
* of Premium Direction details which apply to Coverage/Rider
* group transaction records defined by program P5151
* (Coverage/Rider Component Changes program for Unit Linked
* products).
*
*  Overview.
*  ---------
*
*
*  This  document  specifies the Premium Direction   processing
*  program for Unit Linked products.
*
*  P5152  will  be used to specify Premium  Direction  for both
*  single premium products and for  regular  premium  products.
*  There  will  be  no difference in the processing for the two
*  types.
*
*  This program will  not  create  nor  maintain  Unit   Linked
*  records  directly    on  to  the   ULNK  data  set.  Instead
*  it  will  create Transaction record(s) which will  have    a
*  similar  format  to  the  UNLK  records  and  these  will be
*  converted  to   UNLK    records  by  AT.  The  logical  view
*  UNLTMJA will be used by this program.
*
*  The  transaction record just processed by the Coverage/Rider
*  program will be  available in the I/O module COVTMJA.
*
*  The Component Change - Premium Direction Screen, S5152, does
*  not allow entry for Component Modify or Enquiry.  Premium
*  Redirection is available as a seperate process from the
*  Contract Servicing Sub-Menu.
*
*  Currently this program will  not  be  followed by any  other
*  further 'down' the stack.
*
*
*  Initialise.
*  -----------
*
*
*  Initialise the S5152-DATA-AREA.
*
*  The    details    of  the contract being processed will have
*  been stored in the  CHDRMJA  I/O  module.    Use  the  RETRV
*  function to obtain the contract  header details.
*
*  The  details  of  the Coverage/Rider being processed will be
*  on the most recent COVTMJA  transaction  record    processed
*  and will therefore be available in the I/O module COVTMJA.
*
*  1.   Perform a RETRV on  COVTMJA. This will provide a key of
*  Company (1 byte), Contract (8  bytes),   Life    (2  bytes),
*  Coverage (2 bytes) and Rider (2 bytes).
*
*  2.    Obtain  the  main  Life  details by using the Company,
*  Contract and Life  returned from COVTMJA plus a  Joint  Life
*  of '00' to do a READR on LIFE.
*
*  3.      Use   the   Client  Number  from  the  LIFE  record,
*  (LIFE-LIFCNUM), to do a READR on CLTS  to  obtain  the  main
*  life details.
*
*  4.    Use  the same LIFE key except with a Joint Life set to
*  '01' to READR on LIFE again in order to retrieve  the  Joint
*  Life details if they exist.
*
*  5.    If  found then use the Client number on that record to
*  obtain the Joint Life details for display as at 3 above.
*
*  6.  Use the same COVTMJA key to perform a READS on  UNLTMJA.
*  If  no  matching  record is found then details are yet to be
*  created, otherwise display the details  from the  record  so
*  that the user may amend them.
*
*  The    clients'  names  that  will  appear as 'confirmation'
*  descriptions on  the    screen    will  be  formatted  by  a
*  standard routine in the copybook CONFNAME.
*
*  Obtain  the  general    Coverage/Rider    details from T5687
*  using ITMDIO with a function of   READS.    The    key    is
*  Company,  'T5687' and the Coverage/Rider code.
*
*  Obtain  the  long description  from DESC using DESCIO with a
*  function of READS. (See the call to GETDESC  within  GETMETH
*  for details.)
*
*
*  Validation.
*  -----------
*
*
*  Read the screen using 'S5152IO'.
*
*  If the 'KILL' function was requested skip all validation.
*
*  If  'CALC'  was  pressed and the 'Fund Split Plan' field was
*  blank then ignore the 'CALC' request and  simply  re-display
*  the  screen.  If there is an entry in 'Fund Split Plan' then
*  proceed as follows:
*
*  Read T5510 using ITMDIO and the Fund Split Plan code.
*
*  Initialise the screen table fields,
*
*  Place the T5510 fields on the screen,
*
*  Continue  processing  from    the     start      of      the
*  2000-SCREEN-EDIT section.
*
*  If Modify or Enquire options were requested skip all
*  validation.
*
*  Validate  the  screen  according to  the  rules  defined  by
*  the field help.  For all  fields  which  have  descriptions,
*  look them up again.
*
*  In particular validate the following:
*
*  The  Percentage/Amount  field  may  be  'P' (Percentage), or
*  'A' (Amount).
*
*  The 'Fund Split Plan' is validated as follows:
*
*  It must , firstly, be  a  valid entry on  T5510.  If  it  is
*  there display the entries on the screen table.
*
*  If  it  is  present,   take  the  Allowable  Funds  entry on
*  T5551, (T5551-ALFUNDS), and obtain the available funds  list
*  on  T5543.  Read  T5543  using  ITMDIO    and    a   key  of
*  Company, 'T5543' and T5551-ALFUNDS. Check  that    all    of
*  the  funds  specified  on the screen exist on the extra data
*  screen of the T5543 item.
*
*  Check that if percentages are  entered they  total  100  and
*  that  if  amounts  are  entered  they  total the 'Instalment
*  Premium'.
*
*
*  Updating.
*  ---------
*
*
*  If the 'KILL' function key was  pressed  or  if  in  enquiry
*  or modify mode, or if no changes were made, skip the
*  updating.
*
*  Either add or rewrite the UNLTMJA record as required.
*
*
*  Next Program.
*  -------------
*
*  Add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5152 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5152");
	private String changedInd = "";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-VALUE */
	private FixedLengthStringData wsaaFndopt = new FixedLengthStringData(4);
	private String wsaaT5671Found = "N";
	private FixedLengthStringData totPolsInPlan = new FixedLengthStringData(4);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData y = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaT5515Sub = new PackedDecimalData(2, 0).init(ZERO);

	private FixedLengthStringData wsaaStandardPlan = new FixedLengthStringData(1).init(SPACES);
	private Validator standardPlan = new Validator(wsaaStandardPlan, "Y");
	private Validator nonStandardPlan = new Validator(wsaaStandardPlan, "N");
	private PackedDecimalData wsaaTotUalprc = new PackedDecimalData(17, 2);
	private PackedDecimalData one = new PackedDecimalData(3, 0).init(1).setUnsigned();
	private int two = 2;
	private int ten = 10;
	private int oneHundred = 100;

	private FixedLengthStringData wsaaUnltmjaFound = new FixedLengthStringData(1).init(SPACES);
	private Validator unltmjaFound = new Validator(wsaaUnltmjaFound, "Y");

	private FixedLengthStringData wsaaGotFund = new FixedLengthStringData(1).init(SPACES);
	private Validator gotAtLeast1Fund = new Validator(wsaaGotFund, "Y");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsbbFndsplItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbFndopt = new FixedLengthStringData(4).isAPartOf(wsbbFndsplItem, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsbbFndsplItem, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbT5551Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbKeypart1 = new FixedLengthStringData(5).isAPartOf(wsbbT5551Key, 0);
	private FixedLengthStringData wsbbKeypart2 = new FixedLengthStringData(3).isAPartOf(wsbbT5551Key, 5);

	private FixedLengthStringData wsaaSaveFunds = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSaveFund = FLSArrayPartOfStructure(10, 4, wsaaSaveFunds, 0);

	private FixedLengthStringData wsaaItemCheck = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCheck1st4 = new FixedLengthStringData(4).isAPartOf(wsaaItemCheck, 0);
	private FixedLengthStringData wsaaItemCheckEnd4 = new FixedLengthStringData(4).isAPartOf(wsaaItemCheck, 4);

	private FixedLengthStringData wsaaFundOk = new FixedLengthStringData(1);
	private Validator fundNotFound = new Validator(wsaaFundOk, "N");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private String e631 = "E631";
	private String f025 = "F025";
	private String f348 = "F348";
	private String f351 = "F351";
	private String f294 = "F294";
	private String g037 = "G037";
	private String g103 = "G103";
	private String g104 = "G104";
	private String g105 = "G105";
	private String g106 = "G106";
	private String g347 = "G347";
	private String h365 = "H365";
	private String h404 = "H404";
	private String h456 = "H456";
		/* TABLES */
	private String t2240 = "T2240";
	private String t5510 = "T5510";
	private String t5515 = "T5515";
	private String t5540 = "T5540";
	private String t5543 = "T5543";
	private String t5687 = "T5687";
	private String t5551 = "T5551";
	private String t5671 = "T5671";
	private String t5688 = "T5688";
	private String itemrec = "ITEMREC";
		/*Contract header file*/
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Nxtprogrec nxtprogrec = new Nxtprogrec();
	private T2240rec t2240rec = new T2240rec();
	private T5510rec t5510rec = new T5510rec();
	private T5540rec t5540rec = new T5540rec();
	private T5543rec t5543rec = new T5543rec();
	private T5551rec t5551rec = new T5551rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
		/*Unit Linked Fund Direction.*/
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
		/*Unit Transactions for Maj Alts*/
	private UnltmjaTableDAM unltmjaIO = new UnltmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S5152ScreenVars sv = ScreenProgram.getScreenVars( S5152ScreenVars.class);
	
	//ILIFE-8078
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO" , CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	private static final String wsaaComp = "T555";
	private String lastProgram="";
	private boolean fundList = false;//ILIFE-8164

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nxtpGoingUp, 
		nxtpEnd, 
		nxtpExit, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1900, 
		exit1500, 
		preExit, 
		validate2100, 
		exit2090, 
		exit3900, 
		exit6099, 
		exit6049, 
		checkAmts, 
		checkFieldsExit, 
		gotIt5106, 
		exit5109, 
		loop5220, 
		checkPrice5240, 
		checkTotal5260, 
		exit5299, 
		exit6690, 
		exit6790, 
		exit6890
	}

	public P5152() {
		super();
		screenVars = sv;
		new ScreenModel("S5152", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void nxtprog()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nxtpMainline();
					nxtpGoingDown();
				}
				case nxtpGoingUp: {
					nxtpGoingUp();
				}
				case nxtpEnd: {
					nxtpEnd();
				}
				case nxtpExit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nxtpMainline()
	{
		nxtprogrec.statuz.set("****");
		if (isEQ(wsspcomn.submenu,"P0075")) {
			wsspcomn.programPtr.add(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.nxtpExit);
		}
		if (isEQ(wsspcomn.nextprog,scrnparams.scrname)) {
			wsspcomn.lastprog.set(wsaaProg);
		}
		else {
			wsspcomn.lastprog.set(wsspcomn.nextprog);
		}
		wsspcomn.nextprog.set(SPACES);
		if (isEQ(nxtprogrec.function,"U")) {
			goTo(GotoLabel.nxtpGoingUp);
		}
	}

protected void nxtpGoingDown()
	{
		if (isEQ(wsspcomn.lastprog,wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next4prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next1prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.submenu)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpGoingUp()
	{
		if (isEQ(wsspcomn.lastprog,wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next4prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpEnd()
	{
		if (isEQ(wsspcomn.nextprog,SPACES)
		&& isNE(wsspcomn.flag,"W")) {
			wsspcomn.nextprog.set(wsspcomn.submenu);
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1100();
			unltmja1300();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1100()
	{
	 if(lastProgram.equals(wsaaProg.toString()) && wsaaComp.equals(wsaaBatckey.batcBatctrcde.toString())) {//ILIFE-8263
		 lastProgram = "";
		 wsspcomn.sectionno.set("4000");
		 return ;
	    }
	    lastProgram=wsaaProg.toString();
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.instprem.set(ZERO);
		sv.unitAllocPercAmt01.set(ZERO);
		sv.unitAllocPercAmt02.set(ZERO);
		sv.unitAllocPercAmt03.set(ZERO);
		sv.unitAllocPercAmt04.set(ZERO);
		sv.unitAllocPercAmt05.set(ZERO);
		sv.unitAllocPercAmt06.set(ZERO);
		sv.unitAllocPercAmt07.set(ZERO);
		sv.unitAllocPercAmt08.set(ZERO);
		sv.unitAllocPercAmt09.set(ZERO);
		sv.unitAllocPercAmt10.set(ZERO);
		sv.anbAtCcd.set(ZERO);
		wsaaUnltmjaFound.set(SPACES);
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			/* MTL002 */
			}
		}
		totPolsInPlan.set(chdrpf.getPolinc());
		readCovr1995();
		covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtmjaIO.setInstprem(0);
		covtmjaIO.setSingp(0);
		covtmjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)) {
			readCovr1995();
		}
		sv.coverage.set(covtmjaIO.getCoverage());
		sv.rider.set(covtmjaIO.getRider());
		lifelnbIO.setChdrcoy(covtmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtmjaIO.getChdrnum());
		sv.chdrnum.set(covtmjaIO.getChdrnum());
		lifelnbIO.setLife(covtmjaIO.getLife());
		sv.life.set(covtmjaIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.anbAtCcd.set(lifelnbIO.getAnbAtCcd());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		sv.lifenum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			sv.jlifcnum.set("NONE");
			sv.jlinsname.set("NONE");
		}
		else {
			sv.jlifcnum.set(lifelnbIO.getLifcnum());
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setFunction("READR");
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
			}
		}
		sv.crtable.set(covtmjaIO.getCrtable());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.crtabdesc.fill("?");
		}
		else {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5687");
		itdmIO.setItemitem(covtmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covtmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covtmjaIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		sv.statfund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.stsubsect.set(t5687rec.statSubSect);
		readT55516700();
		if (isEQ(covtmjaIO.getInstprem(),ZERO)) {
			sv.instprem.set(covtmjaIO.getSingp());
		}
		else {
			sv.instprem.set(covtmjaIO.getInstprem());
		}
		sv.percOrAmntInd.set("P");
		sv.virtFundSplitMethod.set(SPACES);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5540");
		wsbbFndopt.set(covtmjaIO.getCrtable());
		itdmIO.setItemitem(wsbbFndsplItem);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.endp))
		&& (isNE(itdmIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5540)
		|| isNE(itdmIO.getItemitem(),covtmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
			sv.virtFundSplitMethod.set(t5540rec.fundSplitPlan);
		}
		fundList = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP106", appVars, "IT");//ILIFE-8164
		//ILIFE-8164- STARTS
		crossCheckT55436600();
		
		if(fundList)
		{
			int ix = 0;
			for(FixedLengthStringData recData:t5543rec.unitVirtualFund) {
				if(recData != null) {
					sv.newFundList[ix].set(recData);	
				}
				ix++;
			}
			
			
				sv.newFundList01Out[varcom.nd.toInt()].set("N");
				sv.newFundList02Out[varcom.nd.toInt()].set("N");
				sv.newFundList03Out[varcom.nd.toInt()].set("N");
				sv.newFundList04Out[varcom.nd.toInt()].set("N");
				sv.newFundList05Out[varcom.nd.toInt()].set("N");
				sv.newFundList06Out[varcom.nd.toInt()].set("N");
				sv.newFundList07Out[varcom.nd.toInt()].set("N");
				sv.newFundList08Out[varcom.nd.toInt()].set("N");
				sv.newFundList09Out[varcom.nd.toInt()].set("N");
				sv.newFundList10Out[varcom.nd.toInt()].set("N");
				sv.newFundList11Out[varcom.nd.toInt()].set("N");
				sv.newFundList12Out[varcom.nd.toInt()].set("N");
				
			}
		else {

			sv.newFundList01Out[varcom.nd.toInt()].set("Y");
			sv.newFundList02Out[varcom.nd.toInt()].set("Y");
			sv.newFundList03Out[varcom.nd.toInt()].set("Y");
			sv.newFundList04Out[varcom.nd.toInt()].set("Y");
			sv.newFundList05Out[varcom.nd.toInt()].set("Y");
			sv.newFundList06Out[varcom.nd.toInt()].set("Y");
			sv.newFundList07Out[varcom.nd.toInt()].set("Y");
			sv.newFundList08Out[varcom.nd.toInt()].set("Y");
			sv.newFundList09Out[varcom.nd.toInt()].set("Y");
			sv.newFundList10Out[varcom.nd.toInt()].set("Y");
			sv.newFundList11Out[varcom.nd.toInt()].set("Y");
			sv.newFundList12Out[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-8164- ENDS
		
	}

protected void unltmja1300()
	{
		unltmjaIO.setParams(SPACES);
		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isEQ(unltmjaIO.getStatuz(),"MRNF")
		|| isEQ(unltmjaIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(unltmjaIO.getParams());
			syserrrec.statuz.set(unltmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(unltmjaIO.getStatuz(),"MRNF")) {
			wsaaUnltmjaFound.set("N");
			checkUlnkRecord1500();
			goTo(GotoLabel.exit1900);
		}
		wsaaUnltmjaFound.set("Y");
		for (x.set(one); !(isGT(x,ten)); x.add(one)){
			move1400();
		}
		sv.percOrAmntInd.set(unltmjaIO.getPercOrAmntInd());
		sv.virtFundSplitMethod.set(SPACES);
	}

protected void move1400()
	{
		/*MOVE-VALUES*/
		sv.unitVirtualFund[x.toInt()].set(unltmjaIO.getUalfnd(x));
		sv.unitAllocPercAmt[x.toInt()].set(unltmjaIO.getUalprc(x));
		/*EXIT*/
	}

protected void checkUlnkRecord1500()
	{
		try {
			start1500();
		}
		catch (GOTOException e){
		}
	}

protected void start1500()
	{
		ulnkIO.setParams(SPACES);
		ulnkIO.setChdrcoy(covtmjaIO.getChdrcoy());
		ulnkIO.setChdrnum(covtmjaIO.getChdrnum());
		ulnkIO.setLife(covtmjaIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(covtmjaIO.getCoverage());
		ulnkIO.setRider(covtmjaIO.getRider());
		if (isGT(covtmjaIO.getPlanSuffix(),chdrpf.getPolsum())) {
			ulnkIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		}
		else {
			ulnkIO.setPlanSuffix(ZERO);
		}
		ulnkIO.setFunction("READR");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isEQ(ulnkIO.getStatuz(),"MRNF")
		|| isEQ(ulnkIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ulnkIO.getStatuz(),"MRNF")) {
			goTo(GotoLabel.exit1500);
		}
		for (x.set(one); !(isGT(x,ten)); x.add(one)){
			move1600();
		}
		sv.percOrAmntInd.set(ulnkIO.getPercOrAmntInd());
		sv.virtFundSplitMethod.set(SPACES);
	}

protected void move1600()
	{
		/*MOVE-VALUES*/
		sv.unitVirtualFund[x.toInt()].set(ulnkIO.getUalfnd(x));
		sv.unitAllocPercAmt[x.toInt()].set(ulnkIO.getUalprc(x));
		/*EXIT*/
	}

protected void readCovr1995()
	{
		para1995();
	}

protected void para1995()
	{
		//LIFE-8078
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
			}
		}
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		covtmjaIO.setCoverage(covrpf.getCoverage());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setRider(covrpf.getRider());
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setCrtable(covrpf.getCrtable());
		covtmjaIO.setInstprem(covrpf.getInstprem());
		covtmjaIO.setSingp(covrpf.getSingp());
		covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"S") || isEQ(wsspcomn.flag,"P")) {  // Ticket #ILIFE-7244
			scrnparams.function.set(varcom.prot);
		}
		if (isEQ(wsaaT5671Found,"N")) {
			scrnparams.errorCode.set(f025);
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case validate2100: {
					validate2100();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz,"CALC")) {
			if (isEQ(sv.virtFundSplitMethod,SPACES)) {
				goTo(GotoLabel.validate2100);
			}
			else {
				wsaaFndopt.set(sv.virtFundSplitMethod);
				readItem5100();
				if (isNE(sv.fndsplErr,SPACES)) {
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
				else {
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
			}
		}
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2100()
	{
		if (isEQ(wsspcomn.flag,"M")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		sv.errorIndicators.set(SPACES);
		if (isNE(sv.percOrAmntInd,"P")
		&& isNE(sv.percOrAmntInd,"A")) {
			sv.prcamtindErr.set(g347);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		wsaaSaveFunds.set(sv.unitVirtualFunds);
		if (isNE(sv.virtFundSplitMethod,SPACES)) {
			wsaaFndopt.set(sv.virtFundSplitMethod);
			readItem5100();
			if (isNE(sv.fndsplErr,SPACES)) {
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		if (isNE(sv.virtFundSplitMethod,SPACES)
		&& isNE(wsaaSaveFunds,SPACES)) {
			if (isNE(t5510rec.unitVirtualFunds,wsaaSaveFunds)) {
				sv.fndsplErr.set(h404);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		editFundSplit5200();
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		for (wsaaT5515Sub.set(1); !(isGT(wsaaT5515Sub,10)); wsaaT5515Sub.add(1)){
			readT55156800();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		crossCheckT55436600();
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		wsaaStandardPlan.set("Y");
		scrTabCheck5400();
		if (isNE(sv.percOrAmntInd,"P")) {
			wsaaStandardPlan.set("N");
		}
	}

protected void update3000()
	{
		try {
			loadWsspFields3100();
		}
		catch (GOTOException e){
		}
	}

protected void loadWsspFields3100()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(wsspcomn.flag,"M")) {
			goTo(GotoLabel.exit3900);
		}
		updates6000();
		if (nonStandardPlan.isTrue()) {
			wsspwindow.value.set(SPACES);
		}
		else {
			wsspwindow.value.set(sv.virtFundSplitMethod);
		}
	}

protected void updates6000()
	{
		try {
			para6010();
		}
		catch (GOTOException e){
		}
	}

protected void para6010()
	{
		if (!unltmjaFound.isTrue()) {
			write6020();
			goTo(GotoLabel.exit6099);
		}
		changedInd = "N";
		checkChangedFields();
		if (isEQ(changedInd,"Y")) {
			rewrite6040();
		}
	}

protected void write6020()
	{
		/*WRITE*/
		newUnltmja6060();
		for (x.set(one); !(isGT(x,ten)); x.add(one)){
			move6050();
		}
		unltmjaIO.setPercOrAmntInd(sv.percOrAmntInd);
		unltmjaIO.setFunction(varcom.writr);
		unltmjaIO.setFormat("UNLTMJAREC");
		accessUnltmja6080();
		/*EXIT*/
	}

protected void rewrite6040()
	{
		try {
			rewrite6041();
		}
		catch (GOTOException e){
		}
	}

protected void rewrite6041()
	{
		unltmjaIO.setFunction(varcom.readh);
		accessUnltmja6080();
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit6049);
		}
		unltmjaIO.setPercOrAmntInd(sv.percOrAmntInd);
		for (x.set(one); !(isGT(x,ten)); x.add(one)){
			move6050();
		}
		unltmjaIO.setNumapp(ZERO);
		unltmjaIO.setFunction(varcom.rewrt);
		accessUnltmja6080();
	}

protected void move6050()
	{
		/*MOVE*/
		unltmjaIO.setUalfnd(x, sv.unitVirtualFund[x.toInt()]);
		unltmjaIO.setUalprc(x, sv.unitAllocPercAmt[x.toInt()]);
		/*EXIT*/
	}

protected void newUnltmja6060()
	{
		newUnltmja6061();
	}

protected void newUnltmja6061()
	{
		unltmjaIO.setDataArea(SPACES);
		unltmjaIO.setGenDate(varcom.vrcmDate);
		unltmjaIO.setGenTime(varcom.vrcmTime);
		unltmjaIO.setVn(wsspcomn.version);
		unltmjaIO.setIo("UNLTMJAIO");
		unltmjaIO.setFormat("UNLTMJAREC");
		unltmjaIO.setDataKey(covtmjaIO.getDataKey());
		unltmjaIO.setCurrto(varcom.vrcmMaxDate);
		unltmjaIO.setCurrfrom(chdrpf.getOccdate());
		for (x.set(one); !(isGT(x,ten)); x.add(one)){
			zeroise6070();
		}
		setPrecision(unltmjaIO.getTranno(), 0);
		unltmjaIO.setTranno(add(chdrpf.getTranno(),1));
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setNumapp(ZERO);
	}

protected void zeroise6070()
	{
		/*ZEROISE*/
		unltmjaIO.setUalprc(x, ZERO);
		unltmjaIO.setUspcpr(x, ZERO);
		/*EXIT*/
	}

protected void accessUnltmja6080()
	{
		/*ACCESS-UNLTMJA*/
		unltmjaIO.setStatuz(SPACES);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(unltmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkChangedFields()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					checkFields();
				}
				case checkAmts: {
					checkAmts();
				}
				case checkFieldsExit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkFields()
	{
		if (isNE(sv.percOrAmntInd,unltmjaIO.getPercOrAmntInd())) {
			changedInd = "Y";
			goTo(GotoLabel.checkFieldsExit);
		}
		x.set(1);
	}

protected void checkAmts()
	{
		if ((isNE(sv.unitAllocPercAmt[x.toInt()],unltmjaIO.getUalprc(x)))
		|| (isNE(sv.unitVirtualFund[x.toInt()],unltmjaIO.getUalfnd(x)))) {
			changedInd = "Y";
			goTo(GotoLabel.checkFieldsExit);
		}
		else {
			x.add(1);
			if (isGT(x,10)) {
				goTo(GotoLabel.checkFieldsExit);
			}
			else {
				goTo(GotoLabel.checkAmts);
			}
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/	   
		 wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void readItem5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para5100();
				}
				case gotIt5106: {
					gotIt5106();
				}
				case exit5109: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5100()
	{
		if (isEQ(wsaaFndopt,SPACES)) {
			for (x.set(one); !(isGT(x,10)); x.add(one)){
				zeroise5108();
			}
			goTo(GotoLabel.exit5109);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5510");
		wsbbFndopt.set(wsaaFndopt);
		itdmIO.setItemitem(wsbbFndsplItem);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5510)
		|| isNE(itdmIO.getItemitem(),wsbbFndsplItem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.fndsplErr.set(g103);
			goTo(GotoLabel.exit5109);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t5510rec.t5510Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.gotIt5106);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.mrnf)) {
			sv.fndsplErr.set(g103);
			goTo(GotoLabel.exit5109);
		}
		for (x.set(one); !(isGT(x,10)); x.add(one)){
			zeroise5108();
		}
		goTo(GotoLabel.exit5109);
	}

protected void gotIt5106()
	{
		for (x.set(one); !(isGT(x,ten)); x.add(one)){
			move5107();
		}
		goTo(GotoLabel.exit5109);
	}

protected void move5107()
	{
		sv.unitVirtualFund[x.toInt()].set(t5510rec.unitVirtualFund[x.toInt()]);
		if (isNE(sv.percOrAmntInd,"A")) {
			sv.unitAllocPercAmt[x.toInt()].set(t5510rec.unitPremPercent[x.toInt()]);
		}
	}

protected void zeroise5108()
	{
		t5510rec.unitPremPercent[x.toInt()].set(ZERO);
		t5510rec.unitVirtualFund[x.toInt()].set(SPACES);
	}

protected void readItem5110()
	{
		/*READ-ITEM*/
		if (isLT(chdrpf.getOccdate(),itdmIO.getItmfrm())
		|| isGT(chdrpf.getOccdate(),itdmIO.getItmto())) {
			sv.fndsplErr.set(g104);
		}
		/*EXIT*/
	}

protected void editFundSplit5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para5210();
				}
				case loop5220: {
					loop5220();
					checkUalprc5230();
				}
				case checkPrice5240: {
					checkPrice5240();
				}
				case checkTotal5260: {
					checkTotal5260();
				}
				case exit5299: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5210()
	{
		x.set(ZERO);
		wsaaTotUalprc.set(ZERO);
		wsaaGotFund.set("N");
	}

protected void loop5220()
	{
		x.add(one);
		if (isGT(x,10)) {
			goTo(GotoLabel.checkTotal5260);
		}
	}

protected void checkUalprc5230()
	{
		if (isNE(sv.unitVirtualFund[x.toInt()],SPACES)) {
			wsaaGotFund.set("Y");
		}
		if (isEQ(sv.unitVirtualFund[x.toInt()],SPACES)) {
			if (isNE(sv.unitAllocPercAmt[x.toInt()],ZERO)) {
				sv.ualprcErr[x.toInt()].set(g105);
				goTo(GotoLabel.checkPrice5240);
			}
			else {
				goTo(GotoLabel.checkPrice5240);
			}
		}
		else {
			if (isLTE(sv.unitAllocPercAmt[x.toInt()],ZERO)) {
				sv.ualprcErr[x.toInt()].set(f351);
				goTo(GotoLabel.checkPrice5240);
			}
		}
		if (isEQ(sv.percOrAmntInd,"P")
		&& isGT(sv.unitAllocPercAmt[x.toInt()],oneHundred)) {
			sv.ualprcErr[x.toInt()].set(f348);
			goTo(GotoLabel.checkPrice5240);
		}
		wsaaTotUalprc.add(sv.unitAllocPercAmt[x.toInt()]);
	}

protected void checkPrice5240()
	{
		/*LOOP-BACK*/
		goTo(GotoLabel.loop5220);
	}

protected void checkTotal5260()
	{
		if (!gotAtLeast1Fund.isTrue()) {
			sv.vrtfndErr[1].set(h365);
		}
		if (isEQ(sv.percOrAmntInd,"P")
		&& isNE(wsaaTotUalprc,oneHundred)) {
			sv.ualprcErr[one.toInt()].set(e631);
			for (x.set(two); !(isGT(x,ten)); x.add(one)){
				ualprcErrs5270();
			}
			goTo(GotoLabel.exit5299);
		}
		if (isEQ(sv.percOrAmntInd,"A")) {
			if (isEQ(sv.instprem,ZERO)) {
				goTo(GotoLabel.exit5299);
			}
			else {
				if (isNE(wsaaTotUalprc,sv.instprem)) {
					sv.ualprcErr[one.toInt()].set(g106);
					for (x.set(two); !(isGT(x,ten)); x.add(one)){
						ualprcErrs5270();
					}
				}
			}
		}
		goTo(GotoLabel.exit5299);
	}

protected void ualprcErrs5270()
	{
		if (isNE(sv.unitAllocPercAmt[x.toInt()],ZERO)) {
			sv.ualprcErr[x.toInt()].set(sv.ualprcErr[one.toInt()]);
		}
	}

protected void scrTabCheck5400()
	{
		/*PARA*/
		if ((isNE(t5510rec.unitVirtualFunds,sv.unitVirtualFunds))) {
			wsaaStandardPlan.set("N");
		}
		/*EXIT*/
	}

protected void crossCheckT55436600()
	{
		try {
			para6600();
		}
		catch (GOTOException e){
		}
	}

protected void para6600()
	{
		if (isEQ(t5551rec.alfnds,SPACES)) {
			goTo(GotoLabel.exit6690);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5543");
		wsbbFndopt.set(wsaaFndopt);
		itdmIO.setItemitem(t5551rec.alfnds);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5543)
		|| isNE(itdmIO.getItemitem(),t5551rec.alfnds)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t5543rec.t5543Rec.set(SPACES);
			goTo(GotoLabel.exit6690);
		}
		else {
			t5543rec.t5543Rec.set(itdmIO.getGenarea());
		}
		wsaaItemCheck.set(itdmIO.getItemitem());
		if (isNE(wsaaItemCheck1st4,t5551rec.alfnds)) {
			goTo(GotoLabel.exit6690);
		}
		x.set(0);
		while ( !(isEQ(x,10))) {
			check6650();
		}
		
		goTo(GotoLabel.exit6690);
	}

protected void check6650()
	{
		x.add(1);
		wsaaFundOk.set("N");
		if (isEQ(sv.unitVirtualFund[x.toInt()],ZERO)
		|| isEQ(sv.unitVirtualFund[x.toInt()],SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			y.set(0);
			while ( !(isEQ(y,12))) {
				check16675();
			}
			
		}
	}

protected void check16675()
	{
		y.add(1);
		if (isEQ(sv.unitVirtualFund[x.toInt()],t5543rec.unitVirtualFund[y.toInt()])) {
			wsaaFundOk.set("Y");
			y.set(12);
		}
		if (isEQ(y,12)
		&& fundNotFound.isTrue()) {
			sv.vrtfndErr[x.toInt()].set(h456);
		}
	}

protected void readT55516700()
	{
		try {
			para6700();
		}
		catch (GOTOException e){
		}
	}

protected void para6700()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(sv.crtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5671Found = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,4)
		|| isEQ(wsaaT5671Found,"Y")); wsaaIndex.add(1)){
			if (isEQ(t5671rec.pgm[wsaaIndex.toInt()],wsaaProg)) {
				if (isEQ(t5671rec.edtitm[wsaaIndex.toInt()],SPACES)) {
					scrnparams.errorCode.set(f025);
					wsspcomn.edterror.set("Y");
					wsaaIndex.set(5);
				}
				else {
					wsbbKeypart1.set(t5671rec.edtitm[wsaaIndex.toInt()]);
					wsaaT5671Found = "Y";
				}
			}
		}
		if (isEQ(wsaaT5671Found,"N")) {
			goTo(GotoLabel.exit6790);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5551");
		wsbbKeypart2.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsbbT5551Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsbbT5551Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT55156800()
	{
		try {
			start6800();
		}
		catch (GOTOException e){
		}
	}

protected void start6800()
	{
		if (isEQ(sv.unitVirtualFund[wsaaT5515Sub.toInt()],SPACES)) {
			goTo(GotoLabel.exit6890);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(sv.unitVirtualFund[wsaaT5515Sub.toInt()]);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isEQ(itdmIO.getStatuz(),varcom.endp))
		|| (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(),t5515))
		|| (isNE(itdmIO.getItemitem(),sv.unitVirtualFund[wsaaT5515Sub.toInt()]))) {
			sv.vrtfndErr[wsaaT5515Sub.toInt()].set(g037);
			goTo(GotoLabel.exit6890);
		}
	}
}
