package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:07
 * Description:
 * Copybook name: VPRCUPDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprcupdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcupdFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcupdKey = new FixedLengthStringData(256).isAPartOf(vprcupdFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcupdCompany = new FixedLengthStringData(1).isAPartOf(vprcupdKey, 0);
  	public PackedDecimalData vprcupdEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcupdKey, 1);
  	public PackedDecimalData vprcupdJobno = new PackedDecimalData(8, 0).isAPartOf(vprcupdKey, 6);
  	public FixedLengthStringData vprcupdUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprcupdKey, 11);
  	public FixedLengthStringData vprcupdUnitType = new FixedLengthStringData(1).isAPartOf(vprcupdKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(vprcupdKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcupdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcupdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}