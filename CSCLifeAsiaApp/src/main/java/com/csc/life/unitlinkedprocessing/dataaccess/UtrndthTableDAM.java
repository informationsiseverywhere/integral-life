package com.csc.life.unitlinkedprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UtrndthTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:07
 * Class transformed from UTRNDTH.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UtrndthTableDAM extends UtrnpfTableDAM {

	public UtrndthTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UTRNDTH");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", VRTFND"
		             + ", UNITYP"
		             + ", PRCSEQ"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VRTFND, " +
		            "UNITYP, " +
		            "TRANNO, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "FUNDAMNT, " +
		            "CNTAMNT, " +
		            "FDBKIND, " +
		            "SVP, " +
		            "PRCSEQ, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "VRTFND ASC, " +
		            "UNITYP ASC, " +
		            "PRCSEQ ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "VRTFND DESC, " +
		            "UNITYP DESC, " +
		            "PRCSEQ DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               unitVirtualFund,
                               unitType,
                               tranno,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               fundAmount,
                               contractAmount,
                               feedbackInd,
                               svp,
                               procSeqNo,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(36);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ getProcSeqNo().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, procSeqNo);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller8 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller9 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller18 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());
	nonKeyFiller7.setInternal(unitVirtualFund.toInternal());
	nonKeyFiller8.setInternal(unitType.toInternal());
	nonKeyFiller9.setInternal(tranno.toInternal());
	nonKeyFiller18.setInternal(procSeqNo.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(113);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ nonKeyFiller8.toInternal()
					+ nonKeyFiller9.toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getFundAmount().toInternal()
					+ getContractAmount().toInternal()
					+ getFeedbackInd().toInternal()
					+ getSvp().toInternal()
					+ nonKeyFiller18.toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, nonKeyFiller8);
			what = ExternalData.chop(what, nonKeyFiller9);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, fundAmount);
			what = ExternalData.chop(what, contractAmount);
			what = ExternalData.chop(what, feedbackInd);
			what = ExternalData.chop(what, svp);
			what = ExternalData.chop(what, nonKeyFiller18);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}
	public PackedDecimalData getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(Object what) {
		setProcSeqNo(what, false);
	}
	public void setProcSeqNo(Object what, boolean rounded) {
		if (rounded)
			procSeqNo.setRounded(what);
		else
			procSeqNo.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public PackedDecimalData getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(Object what) {
		setFundAmount(what, false);
	}
	public void setFundAmount(Object what, boolean rounded) {
		if (rounded)
			fundAmount.setRounded(what);
		else
			fundAmount.set(what);
	}	
	public PackedDecimalData getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(Object what) {
		setContractAmount(what, false);
	}
	public void setContractAmount(Object what, boolean rounded) {
		if (rounded)
			contractAmount.setRounded(what);
		else
			contractAmount.set(what);
	}	
	public FixedLengthStringData getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(Object what) {
		feedbackInd.set(what);
	}	
	public PackedDecimalData getSvp() {
		return svp;
	}
	public void setSvp(Object what) {
		setSvp(what, false);
	}
	public void setSvp(Object what, boolean rounded) {
		if (rounded)
			svp.setRounded(what);
		else
			svp.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		unitVirtualFund.clear();
		unitType.clear();
		procSeqNo.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		nonKeyFiller8.clear();
		nonKeyFiller9.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		fundAmount.clear();
		contractAmount.clear();
		feedbackInd.clear();
		svp.clear();
		nonKeyFiller18.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}