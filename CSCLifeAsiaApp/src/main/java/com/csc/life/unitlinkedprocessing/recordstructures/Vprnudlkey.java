package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:08
 * Description:
 * Copybook name: VPRNUDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprnudlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprnudlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprnudlKey = new FixedLengthStringData(256).isAPartOf(vprnudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprnudlCompany = new FixedLengthStringData(1).isAPartOf(vprnudlKey, 0);
  	public FixedLengthStringData vprnudlUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprnudlKey, 1);
  	public FixedLengthStringData vprnudlUnitType = new FixedLengthStringData(1).isAPartOf(vprnudlKey, 5);
  	public PackedDecimalData vprnudlEffdate = new PackedDecimalData(8, 0).isAPartOf(vprnudlKey, 6);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(vprnudlKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprnudlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprnudlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}