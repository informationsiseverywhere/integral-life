package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5146screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 1, 2, 20, 22, 15, 16}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {13, 22, 12, 67}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5146ScreenVars sv = (S5146ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5146screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5146screensfl, 
			sv.S5146screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5146ScreenVars sv = (S5146ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5146screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5146ScreenVars sv = (S5146ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5146screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5146screensflWritten.gt(0))
		{
			sv.s5146screensfl.setCurrentIndex(0);
			sv.S5146screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5146ScreenVars sv = (S5146ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5146screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5146ScreenVars screenVars = (S5146ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.currcy.setFieldName("currcy");
				screenVars.pcntamt.setFieldName("pcntamt");
				screenVars.fndtyp.setFieldName("fndtyp");
				screenVars.vfund.setFieldName("vfund");
				screenVars.fndcur.setFieldName("fndcur");
				screenVars.percentage.setFieldName("percentage");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.currcy.set(dm.getField("currcy"));
			screenVars.pcntamt.set(dm.getField("pcntamt"));
			screenVars.fndtyp.set(dm.getField("fndtyp"));
			screenVars.vfund.set(dm.getField("vfund"));
			screenVars.fndcur.set(dm.getField("fndcur"));
			screenVars.percentage.set(dm.getField("percentage"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5146ScreenVars screenVars = (S5146ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.currcy.setFieldName("currcy");
				screenVars.pcntamt.setFieldName("pcntamt");
				screenVars.fndtyp.setFieldName("fndtyp");
				screenVars.vfund.setFieldName("vfund");
				screenVars.fndcur.setFieldName("fndcur");
				screenVars.percentage.setFieldName("percentage");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("currcy").set(screenVars.currcy);
			dm.getField("pcntamt").set(screenVars.pcntamt);
			dm.getField("fndtyp").set(screenVars.fndtyp);
			dm.getField("vfund").set(screenVars.vfund);
			dm.getField("fndcur").set(screenVars.fndcur);
			dm.getField("percentage").set(screenVars.percentage);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5146screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5146ScreenVars screenVars = (S5146ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.currcy.clearFormatting();
		screenVars.pcntamt.clearFormatting();
		screenVars.fndtyp.clearFormatting();
		screenVars.vfund.clearFormatting();
		screenVars.fndcur.clearFormatting();
		screenVars.percentage.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5146ScreenVars screenVars = (S5146ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.currcy.setClassString("");
		screenVars.pcntamt.setClassString("");
		screenVars.fndtyp.setClassString("");
		screenVars.vfund.setClassString("");
		screenVars.fndcur.setClassString("");
		screenVars.percentage.setClassString("");
	}

/**
 * Clear all the variables in S5146screensfl
 */
	public static void clear(VarModel pv) {
		S5146ScreenVars screenVars = (S5146ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.currcy.clear();
		screenVars.pcntamt.clear();
		screenVars.fndtyp.clear();
		screenVars.vfund.clear();
		screenVars.fndcur.clear();
		screenVars.percentage.clear();
	}
}
