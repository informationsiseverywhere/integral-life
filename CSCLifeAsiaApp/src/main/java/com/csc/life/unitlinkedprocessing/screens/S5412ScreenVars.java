package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5412
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5412ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(129);
	public FixedLengthStringData dataFields = new FixedLengthStringData(33).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData dteeff = DD.dteeff.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData jobno = DD.jobno.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData unitPartFund = DD.partfund.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData todate = DD.todate.copyToZonedDecimal().isAPartOf(dataFields,21);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 33);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData dteeffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData jobnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData partfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 57);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] dteeffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] jobnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] partfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dteeffDisp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);

	public LongData S5412screenWritten = new LongData(0);
	public LongData S5412protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5412ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dteeffOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jobnoOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vrtfndOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(todateOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(partfundOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {dteeff, jobno, unitVirtualFund, todate, action, unitPartFund};
		screenOutFields = new BaseData[][] {dteeffOut, jobnoOut, vrtfndOut, todateOut, actionOut, partfundOut};
		screenErrFields = new BaseData[] {dteeffErr, jobnoErr, vrtfndErr, todateErr, actionErr, partfundErr};
		screenDateFields = new BaseData[] {dteeff, todate};
		screenDateErrFields = new BaseData[] {dteeffErr, todateErr};
		screenDateDispFields = new BaseData[] {dteeffDisp, todateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5412screen.class;
		protectRecord = S5412protect.class;
	}

}
