/*
 * File: C6230.java
 * Date: 30 August 2009 2:58:52
 * Author: $Id$
 * 
 * Class transformed from C6230.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator;

import com.csc.life.unitlinkedprocessing.batchprograms.B6230;
import com.csc.smart.procedures.Passparm;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.VprcpfDAO;
import com.csc.smart400framework.dataaccess.model.Vprcpf;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C6230 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData conj = new FixedLengthStringData(860);

	public C6230() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 2;
		final int returnVar = 3;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					
					String parmAsat = subString(conj, 1, 1).toString();
					Integer parmDateeff = subString(conj, 2, 8).toInt();
					Integer parmJobno = subString(conj, 10, 8).toInt();
					String parmFund = subString(conj, 19, 4).toString();
					Character parmCompany = subString(params, 6, 1).toString().toCharArray()[0];
					
					VprcpfDAO vprcpfDAO = DAOFactory.getVprcpfDAO();
					Iterator<Vprcpf> vprcpfIterator;
					vprcpfIterator = vprcpfDAO.findByCriteriaAndOrder(parmAsat, parmDateeff, parmJobno, parmFund, parmCompany).iterator();
					
					callProgram(B6230.class, new Object[] {params, vprcpfIterator});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
