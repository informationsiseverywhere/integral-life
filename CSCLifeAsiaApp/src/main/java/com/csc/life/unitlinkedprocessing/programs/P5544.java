/*
 * File: P5544.java
 * Date: 30 August 2009 0:30:39
 * Author: Quipoz Limited
 * 
 * Class transformed from P5544.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.procedures.T5544pt;
import com.csc.life.unitlinkedprocessing.screens.S5544ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5544 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5544");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5544rec t5544rec = new T5544rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5544ScreenVars sv = ScreenProgram.getScreenVars( S5544ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5544() {
		super();
		screenVars = sv;
		new ScreenModel("S5544", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5544rec.t5544Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5544rec.discountOfferPercent.set(ZERO);
		t5544rec.cfrwd.set(ZERO);
		t5544rec.feemax.set(ZERO);
		t5544rec.feemin.set(ZERO);
		t5544rec.feepc.set(ZERO);
		t5544rec.ffamt.set(ZERO);
		t5544rec.noOfFreeSwitches.set(ZERO);
		t5544rec.ppyears.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.btobid.set(t5544rec.btobid);
		sv.btodisc.set(t5544rec.btodisc);
		sv.discountOfferPercent.set(t5544rec.discountOfferPercent);
		sv.bidToFund.set(t5544rec.bidToFund);
		sv.btooff.set(t5544rec.btooff);
		sv.cfrwd.set(t5544rec.cfrwd);
		sv.feemax.set(t5544rec.feemax);
		sv.feemin.set(t5544rec.feemin);
		sv.feepc.set(t5544rec.feepc);
		sv.ffamt.set(t5544rec.ffamt);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.nowDeferInd.set(t5544rec.nowDeferInd);
		sv.noOfFreeSwitches.set(t5544rec.noOfFreeSwitches);
		sv.otooff.set(t5544rec.otooff);
		sv.ppyears.set(t5544rec.ppyears);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5544rec.t5544Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.btobid,t5544rec.btobid)) {
			t5544rec.btobid.set(sv.btobid);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.btodisc,t5544rec.btodisc)) {
			t5544rec.btodisc.set(sv.btodisc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.discountOfferPercent,t5544rec.discountOfferPercent)) {
			t5544rec.discountOfferPercent.set(sv.discountOfferPercent);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bidToFund,t5544rec.bidToFund)) {
			t5544rec.bidToFund.set(sv.bidToFund);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.btooff,t5544rec.btooff)) {
			t5544rec.btooff.set(sv.btooff);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cfrwd,t5544rec.cfrwd)) {
			t5544rec.cfrwd.set(sv.cfrwd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.feemax,t5544rec.feemax)) {
			t5544rec.feemax.set(sv.feemax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.feemin,t5544rec.feemin)) {
			t5544rec.feemin.set(sv.feemin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.feepc,t5544rec.feepc)) {
			t5544rec.feepc.set(sv.feepc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ffamt,t5544rec.ffamt)) {
			t5544rec.ffamt.set(sv.ffamt);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nowDeferInd,t5544rec.nowDeferInd)) {
			t5544rec.nowDeferInd.set(sv.nowDeferInd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.noOfFreeSwitches,t5544rec.noOfFreeSwitches)) {
			t5544rec.noOfFreeSwitches.set(sv.noOfFreeSwitches);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.otooff,t5544rec.otooff)) {
			t5544rec.otooff.set(sv.otooff);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ppyears,t5544rec.ppyears)) {
			t5544rec.ppyears.set(sv.ppyears);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5544pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
