/*
 * File: P6251.java
 * Date: 30 August 2009 0:39:35
 * Author: Quipoz Limited
 *
 * Class transformed from P6251.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
 //ILIFE-8089 start
import java.util.ArrayList;
import java.util.List;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
 //ILIFE-8089 end
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.RdirTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;  //ILIFE-8089
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This program is part of the fund redirections and is used
* for Program switching,  there is no screen attached.
*
*
*****************************************************************
* </pre>
*/
public class P6251 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6251");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaSubmitAt = "";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler1 = new FixedLengthStringData(181).isAPartOf(wsaaTransactionRec, 19, FILLER).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String ptrnrec = "PTRNREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Atreqrec atreqrec = new Atreqrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Fund Redirection Details*/
	private RdirTableDAM rdirIO = new RdirTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();
	
	//ILIFE-8089 start
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	//ILIFE-8089 end
	
	public P6251() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		/*EXIT*/
	}

protected void screenEdit2000()
	{
		/*GO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		retrvChdrmja3005();
		readh3015();
		rdir3020();
		chkSubmission3030();
		updatChdrmja3040();
		updConHdr3050();
		ptrn3060();
	}

protected void retrvChdrmja3005()
	{
	 //ILIFE-8089 start
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		chdrpfDAO.deleteCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		RLSE-CHDRMJA
		chdrmjaIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8089 end
	}

protected void readh3015()
	{
	//ILIFE-8089 start
		chdrpf = chdrpfDAO.getchdrRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (chdrpf == null) {
			fatalError600();
		}
		/*chdrmjaIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	//ILIFE-8089 end	
	}

protected void rdir3020()
	{
		rdirIO.setDataKey(SPACES);
		rdirIO.setChdrcoy(wsspcomn.company);
		rdirIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8089
		rdirIO.setPlanSuffix(ZERO);
		rdirIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, rdirIO);
		if (isNE(rdirIO.getStatuz(),varcom.oK)
		&& isNE(rdirIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(rdirIO.getParams());
			fatalError600();
		}
	}

protected void chkSubmission3030()
	{
 //ILIFE-8089
		if (isEQ(rdirIO.getChdrcoy(),wsspcomn.company)
		&& isEQ(rdirIO.getChdrnum(),chdrpf.getChdrnum())
		&& isEQ(rdirIO.getStatuz(),varcom.oK)) {
			sftlckCallAt3100();
		}
		else {
			rlseSoftlock3200();
		}
	}

protected void updatChdrmja3040()
	{
 //ILIFE-8089 start
		List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		chdrpfUpdateList.add(chdrpf);
		chdrpfDAO.updateChdrTrannoByUniqueNo(chdrpfUpdateList);
		
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8089 end
	}

protected void updConHdr3050()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

protected void ptrn3060()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy()); //ILIFE-8089
		ptrnIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8089
		ptrnIO.setTranno(chdrpf.getTranno()); //ILIFE-8089
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid); //PINNACLE-2931
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void sftlckCallAt3100()
	{
		softlock3110();
	}

protected void softlock3110()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum()); //ILIFE-8089
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P6251AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.company.set(wsspcomn.company);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum()); //ILIFE-8089
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaPolsum.set(chdrpf.getPolsum()); //ILIFE-8089
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void rlseSoftlock3200()
	{
		rlse3210();
	}

protected void rlse3210()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum()); //ILIFE-8089
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
