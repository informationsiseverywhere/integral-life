/*
 * File: P6348at.java
 * Date: 30 August 2009 0:44:05
 * Author: Quipoz Limited
 * 
 * Class transformed from P6348AT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtbrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UjnlmjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnmjaTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*              UNIT JOURNALS BREAKOUT - AT MODULE
*              ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*
*****************************************************
* </pre>
*/
public class P6348at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P6348AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransArea, 16);
	private FixedLengthStringData filler1 = new FixedLengthStringData(181).isAPartOf(wsaaTransArea, 19, FILLER).init(SPACES);
	private static final String utrnmjarec = "UTRNMJAREC";
	private CovtbrkTableDAM covtbrkIO = new CovtbrkTableDAM();
	private UjnlmjaTableDAM ujnlmjaIO = new UjnlmjaTableDAM();
	private UtrnmjaTableDAM utrnmjaIO = new UtrnmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Atmodrec atmodrec = new Atmodrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit2190, 
		exit3190
	}

	public P6348at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		deleteCovt2000();
		utrnProcess3000();
		releaseSftlck4000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* START                                                       *
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initial1010();
			tempFile1020();
			callBreakout1030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initial1010()
	{
		wsaaTransArea.set(atmodrec.transArea);
		wsaaBatckey.set(atmodrec.batchKey);
	}

protected void tempFile1020()
	{
		covtbrkIO.setDataArea(SPACES);
		covtbrkIO.setChdrcoy(atmodrec.company);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		covtbrkIO.setChdrnum(wsaaPrimaryChdrnum);
		covtbrkIO.setPlanSuffix(ZERO);
		covtbrkIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covtbrkIO);
		if (isNE(covtbrkIO.getStatuz(), varcom.oK)
		&& isNE(covtbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtbrkIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covtbrkIO.getChdrcoy(), atmodrec.company)
		|| isNE(covtbrkIO.getChdrnum(), wsaaPrimaryChdrnum)
		|| isEQ(covtbrkIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit1090);
		}
	}

protected void callBreakout1030()
	{
		/* Call the BRKOUT subroutine to breakout COVR and AGCM*/
		brkoutrec.outRec.set(SPACES);
		brkoutrec.brkChdrcoy.set(atmodrec.company);
		brkoutrec.brkChdrnum.set(wsaaPrimaryChdrnum);
		brkoutrec.brkOldSummary.set(wsaaPolsum);
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		compute(brkoutrec.brkNewSummary, 0).set(sub(covtbrkIO.getPlanSuffix(), 1));
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
			syserrrec.params.set(brkoutrec.brkStatuz);
			xxxxFatalError();
		}
	}

protected void deleteCovt2000()
	{
		/*BEGN-COVT*/
		/* Begn COVTBRK file*/
		covtbrkIO.setChdrcoy(atmodrec.company);
		covtbrkIO.setChdrnum(wsaaPrimaryChdrnum);
		covtbrkIO.setPlanSuffix(ZERO);
		covtbrkIO.setFunction("BEGNH");
		/* DELETE the broken out records from records on COVTBRK*/
		while ( !(isEQ(covtbrkIO.getStatuz(), varcom.endp))) {
			deleteCovtLoop2100();
		}
		
		/*EXIT*/
	}

protected void deleteCovtLoop2100()
	{
		try {
			findCovtbrk2110();
			deleteCovtbrk2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void findCovtbrk2110()
	{
		SmartFileCode.execute(appVars, covtbrkIO);
		if (isNE(covtbrkIO.getStatuz(), varcom.oK)
		&& isNE(covtbrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtbrkIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covtbrkIO.getChdrcoy(), atmodrec.company)
		|| isNE(covtbrkIO.getChdrnum(), wsaaPrimaryChdrnum)
		|| isEQ(covtbrkIO.getStatuz(), varcom.endp)) {
			covtbrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2190);
		}
	}

protected void deleteCovtbrk2120()
	{
		/* Delete the COVTBRK record*/
		covtbrkIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covtbrkIO);
		if (isNE(covtbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtbrkIO.getParams());
			xxxxFatalError();
		}
		covtbrkIO.setFunction(varcom.nextr);
	}

protected void utrnProcess3000()
	{
		/*BEGN-UJNLMJA-TO-UTRN*/
		/* Begn UJNLMJA file*/
		ujnlmjaIO.setChdrcoy(atmodrec.company);
		ujnlmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		ujnlmjaIO.setSeqno(ZERO);
		ujnlmjaIO.setFunction("BEGNH");
		/* CREATE the Unit Transaction record (UTRN) from the Unit Journal*/
		/* (UJNLMJA). This has avoided a run of UNITDEAL before the AT brea*/
		/* out has been processed.*/
		while ( !(isEQ(ujnlmjaIO.getStatuz(), varcom.endp))) {
			createUtrnLoop3100();
		}
		
		/*EXIT*/
	}

protected void createUtrnLoop3100()
	{
		try {
			findUjnlmja3110();
			createUtrn3120();
			deleteUjnlmja3130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void findUjnlmja3110()
	{
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(), varcom.oK)
		&& isNE(ujnlmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ujnlmjaIO.getParams());
			xxxxFatalError();
		}
		if (isNE(ujnlmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(ujnlmjaIO.getChdrnum(), wsaaPrimaryChdrnum)
		|| isEQ(ujnlmjaIO.getStatuz(), varcom.endp)) {
			ujnlmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
	}

protected void createUtrn3120()
	{
		/* Set up all field for the creation of a Unit transaction for*/
		/* a Journal entry.*/
		/* NOTE : All fields are moved to the UTRN file even though the*/
		/*        temporary record (UJNLMJA) need not have held many of the*/
		/*        fields, but to avoid confusion and maintain a consistent*/
		/*        shadow file all fields are included.*/
		/*        However anything that is zeroes or unchangeable through-*/
		/*        out is repeated unnecessarily again just for ease.*/
		/* FUTURE:It is realised that all field creation within P6352*/
		/*        could be put in the AT module and UJNLMJA cut to hold onl*/
		/*        necessary fields to save time and space. But ease of*/
		/*        maintenace may be lost.*/
		utrnmjaIO.setDataArea(SPACES);
		utrnmjaIO.setChdrcoy(ujnlmjaIO.getChdrcoy());
		utrnmjaIO.setChdrnum(ujnlmjaIO.getChdrnum());
		utrnmjaIO.setLife(ujnlmjaIO.getLife());
		utrnmjaIO.setCoverage(ujnlmjaIO.getCoverage());
		utrnmjaIO.setRider(ujnlmjaIO.getRider());
		utrnmjaIO.setPlanSuffix(ujnlmjaIO.getPlanSuffix());
		utrnmjaIO.setUnitVirtualFund(ujnlmjaIO.getUnitVirtualFund());
		utrnmjaIO.setUnitType(ujnlmjaIO.getUnitType());
		utrnmjaIO.setTranno(ujnlmjaIO.getTranno());
		utrnmjaIO.setTermid(ujnlmjaIO.getTermid());
		utrnmjaIO.setTransactionDate(ujnlmjaIO.getTransactionDate());
		utrnmjaIO.setTransactionTime(ujnlmjaIO.getTransactionTime());
		utrnmjaIO.setUser(ujnlmjaIO.getUser());
		utrnmjaIO.setBatccoy(ujnlmjaIO.getBatccoy());
		utrnmjaIO.setBatcbrn(ujnlmjaIO.getBatcbrn());
		utrnmjaIO.setBatcactyr(ujnlmjaIO.getBatcactyr());
		utrnmjaIO.setBatcactmn(ujnlmjaIO.getBatcactmn());
		utrnmjaIO.setBatctrcde(ujnlmjaIO.getBatctrcde());
		utrnmjaIO.setBatcbatch(SPACES);
		utrnmjaIO.setJobnoPrice(ZERO);
		utrnmjaIO.setStrpdate(ZERO);
		utrnmjaIO.setInciNum(ZERO);
		utrnmjaIO.setInciPerd01(ZERO);
		utrnmjaIO.setInciPerd02(ZERO);
		utrnmjaIO.setInciprm01(ZERO);
		utrnmjaIO.setInciprm02(ZERO);
		utrnmjaIO.setSurrenderPercent(ZERO);
		utrnmjaIO.setUnitSubAccount(ujnlmjaIO.getUnitSubAccount());
		utrnmjaIO.setNowDeferInd("N");
		utrnmjaIO.setNofUnits(ujnlmjaIO.getNofUnits());
		utrnmjaIO.setNofDunits(ujnlmjaIO.getNofDunits());
		utrnmjaIO.setMoniesDate(ujnlmjaIO.getMoniesDate());
		utrnmjaIO.setPriceDateUsed(ujnlmjaIO.getPriceDateUsed());
		utrnmjaIO.setPriceUsed(ujnlmjaIO.getPriceUsed());
		utrnmjaIO.setUnitBarePrice(ujnlmjaIO.getPriceUsed());
		utrnmjaIO.setCrtable(ujnlmjaIO.getCrtable());
		utrnmjaIO.setCntcurr(ujnlmjaIO.getCntcurr());
		utrnmjaIO.setFeedbackInd(" ");
		utrnmjaIO.setContractAmount(ZERO);
		utrnmjaIO.setFundRate(1);
		utrnmjaIO.setFundCurrency(ujnlmjaIO.getFundCurrency());
		utrnmjaIO.setFundAmount(ZERO);
		utrnmjaIO.setSacscode(ujnlmjaIO.getSacscode());
		utrnmjaIO.setSacstyp(ujnlmjaIO.getSacstyp());
		utrnmjaIO.setGenlcde(ujnlmjaIO.getGenlcde());
		utrnmjaIO.setContractType(ujnlmjaIO.getContractType());
		utrnmjaIO.setTriggerModule(SPACES);
		utrnmjaIO.setTriggerKey(SPACES);
		utrnmjaIO.setProcSeqNo(ujnlmjaIO.getProcSeqNo());
		utrnmjaIO.setSvp(1);
		utrnmjaIO.setDiscountFactor(1);
		utrnmjaIO.setCrComDate(ujnlmjaIO.getCrComDate());
		utrnmjaIO.setFormat(utrnmjarec);
		utrnmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnmjaIO);
		if (isNE(utrnmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnmjaIO.getParams());
			xxxxFatalError();
		}
	}

protected void deleteUjnlmja3130()
	{
		/* Delete the UJNLMJA record*/
		ujnlmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ujnlmjaIO.getParams());
			xxxxFatalError();
		}
		ujnlmjaIO.setFunction(varcom.nextr);
	}

protected void releaseSftlck4000()
	{
		releaseSoftlock4010();
	}

protected void releaseSoftlock4010()
	{
		/* Release Soft locked record.                                 **/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(atmodrec.primaryKey);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}
}
