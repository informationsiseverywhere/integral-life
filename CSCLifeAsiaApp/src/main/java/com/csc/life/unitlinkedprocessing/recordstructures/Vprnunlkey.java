package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:09
 * Description:
 * Copybook name: VPRNUNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprnunlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprnunlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprnunlKey = new FixedLengthStringData(256).isAPartOf(vprnunlFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprnunlCompany = new FixedLengthStringData(1).isAPartOf(vprnunlKey, 0);
  	public FixedLengthStringData vprnunlUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprnunlKey, 1);
  	public FixedLengthStringData vprnunlUnitType = new FixedLengthStringData(1).isAPartOf(vprnunlKey, 5);
  	public PackedDecimalData vprnunlEffdate = new PackedDecimalData(8, 0).isAPartOf(vprnunlKey, 6);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(vprnunlKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprnunlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprnunlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}