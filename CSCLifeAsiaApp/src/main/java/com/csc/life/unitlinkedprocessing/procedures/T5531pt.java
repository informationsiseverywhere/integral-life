/*
 * File: T5531pt.java
 * Date: 30 August 2009 2:21:26
 * Author: Quipoz Limited
 * 
 * Class transformed from T5531PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T5531rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5531.
*
*
*****************************************************************
* </pre>
*/
public class T5531pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Unit Link Charge Basis                 S5531");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 16);
	private FixedLengthStringData filler8 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine003, 26, FILLER).init("   To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(43);
	private FixedLengthStringData filler9 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine004, 16, FILLER).init("Monetary/Unit Deduction:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 42);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(53);
	private FixedLengthStringData filler11 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine005, 16, FILLER).init("Frequency              Charge Methods");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(59);
	private FixedLengthStringData filler13 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 19);
	private FixedLengthStringData filler14 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 34);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 41);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 48);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 55);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(59);
	private FixedLengthStringData filler18 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 19);
	private FixedLengthStringData filler19 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 34);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 41);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 48);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 55);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(59);
	private FixedLengthStringData filler23 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 19);
	private FixedLengthStringData filler24 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 34);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 41);
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 48);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 55);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(59);
	private FixedLengthStringData filler28 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 19);
	private FixedLengthStringData filler29 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 34);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 41);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 48);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 55);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(59);
	private FixedLengthStringData filler33 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 19);
	private FixedLengthStringData filler34 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 34);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 41);
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 48);
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 55);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(59);
	private FixedLengthStringData filler38 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 19);
	private FixedLengthStringData filler39 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 34);
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 41);
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 48);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 55);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(59);
	private FixedLengthStringData filler43 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 19);
	private FixedLengthStringData filler44 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 34);
	private FixedLengthStringData filler45 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 41);
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 48);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 55);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(59);
	private FixedLengthStringData filler48 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 19);
	private FixedLengthStringData filler49 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 21, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 34);
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 41);
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 48);
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 55);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5531rec t5531rec = new T5531rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5531pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5531rec.t5531Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo007.set(t5531rec.chargeDeductInd);
		fieldNo008.set(t5531rec.frequency01);
		fieldNo009.set(t5531rec.cgmeth01);
		fieldNo010.set(t5531rec.cgmeth02);
		fieldNo011.set(t5531rec.cgmeth03);
		fieldNo012.set(t5531rec.cgmeth04);
		fieldNo013.set(t5531rec.frequency02);
		fieldNo014.set(t5531rec.cgmeth05);
		fieldNo015.set(t5531rec.cgmeth06);
		fieldNo016.set(t5531rec.cgmeth07);
		fieldNo017.set(t5531rec.cgmeth08);
		fieldNo018.set(t5531rec.frequency03);
		fieldNo019.set(t5531rec.cgmeth09);
		fieldNo020.set(t5531rec.cgmeth10);
		fieldNo021.set(t5531rec.cgmeth11);
		fieldNo022.set(t5531rec.cgmeth12);
		fieldNo023.set(t5531rec.frequency04);
		fieldNo024.set(t5531rec.cgmeth13);
		fieldNo025.set(t5531rec.cgmeth14);
		fieldNo026.set(t5531rec.cgmeth15);
		fieldNo027.set(t5531rec.cgmeth16);
		fieldNo028.set(t5531rec.frequency05);
		fieldNo029.set(t5531rec.cgmeth17);
		fieldNo030.set(t5531rec.cgmeth18);
		fieldNo031.set(t5531rec.cgmeth19);
		fieldNo032.set(t5531rec.cgmeth20);
		fieldNo033.set(t5531rec.frequency06);
		fieldNo034.set(t5531rec.cgmeth21);
		fieldNo035.set(t5531rec.cgmeth22);
		fieldNo036.set(t5531rec.cgmeth23);
		fieldNo037.set(t5531rec.cgmeth24);
		fieldNo038.set(t5531rec.frequency07);
		fieldNo039.set(t5531rec.cgmeth25);
		fieldNo040.set(t5531rec.cgmeth26);
		fieldNo041.set(t5531rec.cgmeth27);
		fieldNo042.set(t5531rec.cgmeth28);
		fieldNo043.set(t5531rec.frequency08);
		fieldNo044.set(t5531rec.cgmeth29);
		fieldNo045.set(t5531rec.cgmeth30);
		fieldNo046.set(t5531rec.cgmeth31);
		fieldNo047.set(t5531rec.cgmeth32);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
