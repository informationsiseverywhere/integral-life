package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Incipf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private String validflag;
	private int tranno;
	private int rcdate;
	private int premCessDate;
	private BigDecimal origPrem;
	private BigDecimal currPrem;
	private BigDecimal premStart01;
	private BigDecimal premStart02;
	private BigDecimal premStart03;
	private BigDecimal premStart04;
	private BigDecimal premCurr01;
	private BigDecimal premCurr02;
	private BigDecimal premCurr03;
	private BigDecimal premCurr04;
	private BigDecimal pcUnits01;
	private BigDecimal pcUnits02;
	private BigDecimal pcUnits03;
	private BigDecimal pcUnits04;
	private BigDecimal unitSplit01;
	private BigDecimal unitSplit02;
	private BigDecimal unitSplit03;
	private BigDecimal unitSplit04;
	private int inciNum;
	private String crtable;
	private int user;
	private int transactionTime;
	private int transactionDate;
	private String termid;
	private int planSuffix;
	private int seqno;
	private String dormantFlag;
	private String userProfile;
	private String jobName;
	private String datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public int getRcdate() {
		return rcdate;
	}

	public void setRcdate(int rcdate) {
		this.rcdate = rcdate;
	}

	public int getPremCessDate() {
		return premCessDate;
	}

	public void setPremCessDate(int premCessDate) {
		this.premCessDate = premCessDate;
	}

	public BigDecimal getOrigPrem() {
		return origPrem;
	}

	public void setOrigPrem(BigDecimal origPrem) {
		this.origPrem = origPrem;
	}

	public BigDecimal getCurrPrem() {
		return currPrem;
	}

	public void setCurrPrem(BigDecimal currPrem) {
		this.currPrem = currPrem;
	}

	public BigDecimal getPremStart01() {
		return premStart01;
	}

	public void setPremStart01(BigDecimal premStart01) {
		this.premStart01 = premStart01;
	}

	public BigDecimal getPremStart02() {
		return premStart02;
	}

	public void setPremStart02(BigDecimal premStart02) {
		this.premStart02 = premStart02;
	}

	public BigDecimal getPremStart03() {
		return premStart03;
	}

	public void setPremStart03(BigDecimal premStart03) {
		this.premStart03 = premStart03;
	}

	public BigDecimal getPremStart04() {
		return premStart04;
	}

	public void setPremStart04(BigDecimal premStart04) {
		this.premStart04 = premStart04;
	}

	public BigDecimal getPremCurr01() {
		return premCurr01;
	}

	public void setPremCurr01(BigDecimal premCurr01) {
		this.premCurr01 = premCurr01;
	}

	public BigDecimal getPremCurr02() {
		return premCurr02;
	}

	public void setPremCurr02(BigDecimal premCurr02) {
		this.premCurr02 = premCurr02;
	}

	public BigDecimal getPremCurr03() {
		return premCurr03;
	}

	public void setPremCurr03(BigDecimal premCurr03) {
		this.premCurr03 = premCurr03;
	}

	public BigDecimal getPremCurr04() {
		return premCurr04;
	}

	public void setPremCurr04(BigDecimal premCurr04) {
		this.premCurr04 = premCurr04;
	}

	public BigDecimal getPcUnits01() {
		return pcUnits01;
	}

	public void setPcUnits01(BigDecimal pcUnits01) {
		this.pcUnits01 = pcUnits01;
	}

	public BigDecimal getPcUnits02() {
		return pcUnits02;
	}

	public void setPcUnits02(BigDecimal pcUnits02) {
		this.pcUnits02 = pcUnits02;
	}

	public BigDecimal getPcUnits03() {
		return pcUnits03;
	}

	public void setPcUnits03(BigDecimal pcUnits03) {
		this.pcUnits03 = pcUnits03;
	}

	public BigDecimal getPcUnits04() {
		return pcUnits04;
	}

	public void setPcUnits04(BigDecimal pcUnits04) {
		this.pcUnits04 = pcUnits04;
	}

	public BigDecimal getUnitSplit01() {
		return unitSplit01;
	}

	public void setUnitSplit01(BigDecimal unitSplit01) {
		this.unitSplit01 = unitSplit01;
	}

	public BigDecimal getUnitSplit02() {
		return unitSplit02;
	}

	public void setUnitSplit02(BigDecimal unitSplit02) {
		this.unitSplit02 = unitSplit02;
	}

	public BigDecimal getUnitSplit03() {
		return unitSplit03;
	}

	public void setUnitSplit03(BigDecimal unitSplit03) {
		this.unitSplit03 = unitSplit03;
	}

	public BigDecimal getUnitSplit04() {
		return unitSplit04;
	}

	public void setUnitSplit04(BigDecimal unitSplit04) {
		this.unitSplit04 = unitSplit04;
	}

	public int getInciNum() {
		return inciNum;
	}

	public void setInciNum(int inciNum) {
		this.inciNum = inciNum;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public int getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}

	public int getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getSeqno() {
		return seqno;
	}

	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	public String getDormantFlag() {
		return dormantFlag;
	}

	public void setDormantFlag(String dormantFlag) {
		this.dormantFlag = dormantFlag;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
}