package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:58
 * Description:
 * Copybook name: INCITRDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incitrdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incitrdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incitrdKey = new FixedLengthStringData(64).isAPartOf(incitrdFileKey, 0, REDEFINE);
  	public FixedLengthStringData incitrdChdrcoy = new FixedLengthStringData(1).isAPartOf(incitrdKey, 0);
  	public FixedLengthStringData incitrdChdrnum = new FixedLengthStringData(8).isAPartOf(incitrdKey, 1);
  	public FixedLengthStringData incitrdLife = new FixedLengthStringData(2).isAPartOf(incitrdKey, 9);
  	public FixedLengthStringData incitrdCoverage = new FixedLengthStringData(2).isAPartOf(incitrdKey, 11);
  	public FixedLengthStringData incitrdRider = new FixedLengthStringData(2).isAPartOf(incitrdKey, 13);
  	public PackedDecimalData incitrdPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incitrdKey, 15);
  	public PackedDecimalData incitrdInciNum = new PackedDecimalData(3, 0).isAPartOf(incitrdKey, 18);
  	public FixedLengthStringData incitrdValidflag = new FixedLengthStringData(1).isAPartOf(incitrdKey, 20);
  	public PackedDecimalData incitrdTranno = new PackedDecimalData(5, 0).isAPartOf(incitrdKey, 21);
  	public PackedDecimalData incitrdSeqno = new PackedDecimalData(2, 0).isAPartOf(incitrdKey, 24);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(incitrdKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incitrdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incitrdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}