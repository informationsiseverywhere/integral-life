/******************************************************************************
 * File Name 		: UswdpfDAO.java
 * Author			: sjothiman2
 * Creation Date	: 23 May 2018
 * Project			: Integral Life
 * Description		: The DAO Interface for USWDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Uswdpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface UswdpfDAO extends BaseDAO<Uswdpf> {

	public List<Uswdpf> searchUswdpfRecord(Uswdpf uswdpf ) throws SQLRuntimeException;

	public void insertIntoUswdpf(Uswdpf uswdpf) throws SQLRuntimeException;

	public void deleteUswdpf(Uswdpf uswdpf) throws SQLRuntimeException;
	public List<Uswdpf> searchAllUswdpf(Uswdpf uswdpf ) throws SQLRuntimeException;

}
