package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:55
 * Description:
 * Copybook name: INCIBCHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incibchkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incibchFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incibchKey = new FixedLengthStringData(64).isAPartOf(incibchFileKey, 0, REDEFINE);
  	public FixedLengthStringData incibchChdrcoy = new FixedLengthStringData(1).isAPartOf(incibchKey, 0);
  	public FixedLengthStringData incibchChdrnum = new FixedLengthStringData(8).isAPartOf(incibchKey, 1);
  	public FixedLengthStringData incibchLife = new FixedLengthStringData(2).isAPartOf(incibchKey, 9);
  	public FixedLengthStringData incibchCoverage = new FixedLengthStringData(2).isAPartOf(incibchKey, 11);
  	public FixedLengthStringData incibchRider = new FixedLengthStringData(2).isAPartOf(incibchKey, 13);
  	public PackedDecimalData incibchPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incibchKey, 15);
  	public PackedDecimalData incibchInciNum = new PackedDecimalData(3, 0).isAPartOf(incibchKey, 18);
  	public PackedDecimalData incibchSeqno = new PackedDecimalData(2, 0).isAPartOf(incibchKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(incibchKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incibchFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incibchFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}