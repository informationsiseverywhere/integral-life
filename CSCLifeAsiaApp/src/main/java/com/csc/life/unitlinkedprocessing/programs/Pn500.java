/*
 * File: Pn500.java
 * Date: 30 August 2009 1:14:11
 * Author: Quipoz Limited
 * 
 * Class transformed from PN500.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NEGATIVE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcddmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrncfiTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrcfiTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.terminationclaims.recordstructures.Untallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.impl.ZutrpfDAOImpl;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.screens.Sn500ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
* This function will be used to apply a single premium top-up to
* a component within a contract. The amount(s) will be applied
* to individual components and so it will be up to the user to
* register the correct dissection amount to each coverage if the
* total is being spread across several components.
*
* The user may have selected to apply the top-up to either
* individual policies within the plan or to the whole plan. After
* this the actual component will be selected. If the top-up is
* being applied to individual policies then this screen will be
* invoked once for each selected component.
*
* If the whole plan has been selected, (COVRMJA-PLAN-SUFFIX =
* 0000), then this screen will again be invoked once for each
* component selected but it must loop around within itself and
* display the screen once for each matching COVR record that
* exists within the plan. It will loop around between the 2000
* and 4000 sections until all the COVR records with a key
* matching on Company, Contract Number, Life, Coverage and Rider
* have been processed. The program will read all the COVR records
* from the summary record, if it exists, to the last COVR record
* in the plan for the given rider key. The plan suffix being
* processed will be displayed in the heading portion of the
* screen in order to show to the user which policy is currently
* being processed. When the summary record is being processed
* display the range of policies represented by the summary, i.e.
*
*       Policy Number: 0001 - 0006
*
* INITIALISE (1000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* Perform a RETRV on the COVRMJA I/O module. This will return
* the first COVRMJA record on the selected policy.
*
* If COVRMJA-PLAN-SUFFIX = 0000  then the whole plan is being
* processed and every COVRMJA record whose key matches on the
* passed Company, Contract Number, Life, Coverage and Rider will
* be processed.
*
* If the COVRMJA-PLAN-SUFFIX is not greater than the value of
* CHDRMJA-POLSUM then a 'notional' policy is being processed,
* i.e. a policy has been selected that is at present part of the
* summarised portion of the contract. If this is the case then
* the COVRMJA summary record will have been saved by the KEEPS
* command and the values from here will be displayed.
*
* However, when the default values from the corresponding ULNK
* record are displayed if they are amounts and not percentages
* they must be divided by the number of policies summarised,
* (CHDRMJA-POLSUM) before being displayed. Any remainder from
* the division must be shown as belonging to the amounts when
* policy number one is displayed, as the following example will
* serve to illustrate:
*
*     Policies Summarised = 3
*     Amount = $100
*
* For notional policy #1 the calculation will be:
*
*     Value to Display = Amount - (Amount * (POLSUM - 1))
*                                           -------------
*                                             POLSUM
*                      = 100 - 100 * (3 - 1) / 3
*                      = 100 - 100 * 2/3
*                      = $34
*
* For notional policies #2 and #3 the value will simply be
* divided by 3 giving $33 each.
*
* If processing a notional policy remember to use a Plan Suffix
* of 0000 when reading the corresponding ULNK record. The UTRN
* records will however be written with the selected policy number
* and breakout will handle the physical breakout of all the other
* Contract Components later.
*
* The details of the contract being processed will be stored in
* the CHDRMJA I/O module. Retrieve the details user RETRV.
*
* The total amount of the transaction for all components must be
* maintained and validated against the amount currently in
* Contract Header suspense. The ACBL record will be read with
* a key of 'CH', the contract Company, Contract number
* Contract Currency, a SACSCODE of 'LP' and SACSTYP of 'S'.
* Within this transaction a field called WSSP-BIG-AMT which is
* held in the COPYBOOK WSSPLIFE, will be used to keep a total of
* all Single Premium TOPUPS done against all components
* attached to this contract.
* WSSP-BIG-AMT will be subtracted from the amount on the ACBL
* record to give the total actually left in CONTRACT suspense.
*
* Get the following descriptions and names:
*    - Contract Type  (T5688)
*    - Contract Status  (T3623)
*    - Premium Status  (T3588)
*    - Servicing Branch  (T1692)
*    - Owner Client   (CLTS)
*    - First Life Assured  (LIFE)
*    - Joint Life Assured  (LIFE)
*
* Read the corresponding ULNK record to obtain the existing fund
* split plan. If the fund split is by percentage then display
* the existing funds and percentages from ULNK and their
* Currency Codes from T5515. The user may then use the existing
* split as a default but will be allowed to amend it if required.
*
* Read T5540. If the Available Fund List code is blank then
* there is no list of permitted funds and the user will be
* allowed to enter any valid funds of his choice. If there is
* an available fund list code then all Fund Code fields on the
* screen must be protected.
*
* If the details from the ULNK records were not displayed as
* they were not split by percentages then use the Available
* Fund List code to read T5543 and display the permitted fund
* codes on the screen. Protect all the fund code fields and the
* Fund Split Plan field. Also display each fund currency from
* T5515.
*
* Set the Percent/Amount Indicator to 'P'.
*
* VALIDATION (2000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* The effective date may not be in the past.
*
* The Percent/Amount indicator may only be 'P' or 'A'.
*
* If the Fund Split Plan is entered use it to read T5510 and
* obtain the Standard Unit Allocation. Set up the funds and their
* percentages and obtain their currency codes from T5515. Set the
* Percent/Amount indicator to 'P'. The screen will be re-displayed
* if the Fund Split Plan was entered so Fund Split Plan must be
* blanked out before looping back around to re-display the
* screen so that the program will not continue re-displaying the
* screen indefinitely.
*
* The WSAA-CHDR-SUSPENSE is found by looking at line 3, item
* PN500 on table T5645 which will give either the normal
* suspense account or a special suspense account for Single
* Premium Top-up. This account needs to be equal to line 3,
* item P5147 on the same table. The ammount is the balance
* on one of these accounts.
*
* If the Premium entered on the screen is greater then amount of
* available in Contract suspence ie (SN500-SUSAMT) then the
* transaction may not proceed. Display an error message saying
* 'Insufficient money in suspense' and do not allow the user
* to continue except to kill the transaction or change the amount
*
* If it is not greater add the premium amount to WSSP-BIG-AMT.
* WSSP-BIG-AMT will thus be carried forward to all future occure-
* nces of the screen within the transaction and incremented
* each time by the new premium amount as the total amount for the
* whole transaction may not be greater than the amount in premium
* suspense.
*
* If amounts are entered they must add up to the entered premium
* for the component. If percentages are entered they must total
* 100%.
*
* The Commission Indicator may only have a spaces of 'X'.
*
* If the program is to loop around and re-display the screen or
* if 'KILL' has been pressed subtract the premium amount from
* WSSP-BIG-AMT.
*
* Commission Indicator is only allowed if an increase from the
* previous level has occurred.
*
* UPDATING (3000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* Each time we enter the 3000 section for a COMPONENT any
* existing UTRNS will be deleted any new ones created. This is to
* ensure that if we select and then return from the commission
* screen, if the screen details have changed then the UTRN record
* (s) created will reflect this.
*
* Write one UTRN for each fund with an amount or percentage. Set
* the following fields on the UTRN:
*
* CHDRCOY    |
* CHDRNUM    |
* LIFE       |
* COVERAGE   |     Full key using the new tranno
* RIDER      |
* PLNSFX     |
* TRANNO     | New Transaction Number (CHDRMJA-TRANNO + 1)
* USER        - WSSP information
* VTRFUND     - Fund Code
* UNITSA      - ?
* NDFIND      from T6647, use the allocation indicator
* UNITYP      the BASIS from T5540, add to it the sex and
*             transaction code as the key to T5537 which in
*             turn provides the key to T5536. In this table
*             select the values attributed to frequency 00.
* BATCOY     |
* BATCBRN    |
* BATCACTYR  |
* BATCACTMN  |     Standard WSSP information
* BATCTRCDE  |
* BATCBATCH  |
* MONIESDT    - from T6647, Effective Date for Transaction.
* CRTABLE     - CRTABLE Code from COVR.
* CNTCURR     - from CHDR
* CNTAMNT     - the amount of the premium for the particular fund
*               in contract currency * the enhancement factor.
*               Read T6647 for the Enhanced Allocation Basis. Add
*               to this the currency code to form the key to
*               T5545. Use the factor applicable to frequency 00.
* FNDCURR     - Fund Currency from T5515
* SACSCODE    - From T5645 keyed by Program Id. line 1
* SACSTYP     -  "     "     "   "    "      "   "   "
* GENLCDE     -  "     "     "   "    "      "   "   "
* CONTYP      -
* PRCSEQ      - From T6647
* CRCDTE      -
*
* If the entry in T5536 indicates that the amount invested is
* < 100% write a NON INVESTED UTRN as well as the standard UTRN.
*
* NEXT PROGRAM (4000- SECTION)
*
* If 'KILL' was requested move spaces to the current program
* pointer and exit.
*
* If the current stack action field is '*' then re-load the next
* 8 positions in the program stack from the saved area.
*
* If the Commission Indicator is '?' then overwrite it with '+'.
*
* If the Commission Indicator was selected with an 'X' then call
* GENSSW with an action of 'A' to obatin the optional program
* switching. Save the next 8 programs from the program stack and
* re-load those positions from the programs returned from GENSSW.
* Move '*' to the current stack action field and replace the
* 'X' in the Commission Indicator with '?'.
*
* If processing the Whole Plan, (COVRMJA-PLAN-SUFFIX = 0000),
* then the program will read the next COVRMJA record for the
* matching Company, Contract Number, Life, Coverage and Rider
* and loop back around to the 2000 section and re-display the
* details from the next COVRMJA record as thet were first set up
* by the first pass through the 1000 section. This will continue
* until all the matching COVRMJA records have been processed.
*
* Add 1 to the current program pointer.
*
******************Enhancements for Life Asia 1.0****************
*
* This enhancement allows the user to indicate which currency
* suspense to be debited to cover the top up amount, the default
* is contract currency. The logic applies is to convert the total
* top up amount from contract currency to the suspense currency
* indicated on the screen.  The amount is then compared with the
* suspense amount available, any insufficiency will be subject to
* the tolerance limit set on T5667.  Note that premiums can be
* injected onto more than one coverage within the same
* transaction.
*
*****************************************************************
*                                                                     *
* 16/07/96    CAS1.0        Dominic Dunbar.
*                   New Version of P5144 with Singapore specific
*                   enhancements.
*
*                   The screen has been modified to display
*                   the current sum assured, new sum assured
*                   after top-up & the new total sum assured.
*                   A checkbox is also included to allow users
*                   to enter any Special Terms. In the event
*                   that a Special Term code is entered, users
*                   is allowed to reduce the Sum Assured to
*                   either 110% or 115% of the Single Premium.
*
*                   To include accounting entries for
*                   large premium enhanced allocation.
*                   Not to include large premium enhanced
*                   allocation amount into the UTRN
*                   Contract amount for investment.
*                   The accounting entries are separated
*                   into premium investment amt and enhanced
*                   allocation amount. Create Utrn record
*                   for the enhanced allocation amount.
*                                                                     *
*                   Statement of Account
*                   To check for T6647-unit-stat-method and
*                   using it to access T6659.
*                   To call T6659 subroutine if set up
*
* 17/07/96   01/01  CAS1.0       Rob Cooper                           *
*                   Display the CNTTYPE.                              *
*                                                                     *
* 14/08/96   01/01  CAS1.0       Cat Chiu                             *
*                   To initialize UTRN-WRITTEN field to
*                   enable subsequent account for Top-Up
*                   to be written into the UTRN file
*
* 23/08/96   01/01  CAS1.0       Cat Chiu
*                   Allow user to indicate which currency suspense
*                   to be debited to cover the top up amount.
*
*  4/09/96   01/01  CAS1.0       Cat Chiu
*                   Remove updating to COVR in 3000- section which
*                   cause DUPR in P5147AT processing.
*                                                                     *
**DD/MM/YY*************************************************************
/
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pn500 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pn500.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PN500");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaUlnkFound = new FixedLengthStringData(1).init(SPACES);
	private Validator ulnkFound = new Validator(wsaaUlnkFound, "Y");

	private FixedLengthStringData wsaaUtrnFound = new FixedLengthStringData(1).init(SPACES);
	private Validator utrnFound = new Validator(wsaaUtrnFound, "Y");

	private FixedLengthStringData wsaaUtrnWritten = new FixedLengthStringData(1).init(SPACES);
	private Validator utrnWritten = new Validator(wsaaUtrnWritten, "Y");

	private FixedLengthStringData wsaaHitrFound = new FixedLengthStringData(1).init(SPACES);
	private Validator hitrFound = new Validator(wsaaHitrFound, "Y");

	private FixedLengthStringData wsaaHitrWritten = new FixedLengthStringData(1).init(SPACES);
	private Validator hitrWritten = new Validator(wsaaHitrWritten, "Y");
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaIfValidFund = new FixedLengthStringData(1).init(SPACES);
	private Validator wsaaValidFund = new Validator(wsaaIfValidFund, "Y");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalNetprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaChdrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInvSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinglePrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaUtrnSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaHitrSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPerc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPcUnits = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaPcInitUnits = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaPcMaxPeriods = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4);
	private PackedDecimalData wsaaPerc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaCltdob = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaCltage = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaNetPremium = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaCurr = new FixedLengthStringData(3);
	private PackedDecimalData wsaaExrate = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaCalcTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance2 = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaToleranceApp = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaToleranceApp2 = new ZonedDecimalData(4, 2);
	private PackedDecimalData wsaaToleranceLimit = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaToleranceLimit2 = new PackedDecimalData(17, 2);
	private String wsaaT5671Found = "N";
	private String wsaaT5540Found = "N";
	private PackedDecimalData wsaaPrevSa = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAnbAtCcd2 = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaSex2 = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(13, 2).init(0);
	private FixedLengthStringData wsaaEnhancedAllocInd = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaProcSeqno = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaRiderAlpha = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRiderNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderAlpha, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaT5515Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5515Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5536Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5536Allbas = new FixedLengthStringData(4).isAPartOf(wsaaT5536Item, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT5536Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5537Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5537Allbas = new FixedLengthStringData(3).isAPartOf(wsaaT5537Item, 0);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Item, 3);
	private FixedLengthStringData wsaaT5537Trans = new FixedLengthStringData(4).isAPartOf(wsaaT5537Item, 4);

	private FixedLengthStringData wsaaT5540Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5540Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5540Item, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaT5540Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5543Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5543Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5543Item, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(4).isAPartOf(wsaaT5543Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 4);

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Part1 = new FixedLengthStringData(5).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Part2 = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);

	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(5).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(3).isAPartOf(wsaaT5645Item, 5, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);
	private FixedLengthStringData filler5 = new FixedLengthStringData(1).isAPartOf(wsaaT6647Item, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaFndsplItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFndopt = new FixedLengthStringData(4).isAPartOf(wsaaFndsplItem, 0);
	private FixedLengthStringData filler6 = new FixedLengthStringData(4).isAPartOf(wsaaFndsplItem, 4, FILLER).init(SPACES);
		/* WSAA-UNIT-VIRTUAL-FUNDS */
	private FixedLengthStringData[] wsaaUnitVirtualFund = FLSInittedArray(10, 4);
		/* WSAA-CURRCYS */
	private FixedLengthStringData[] wsaaCurrcy = FLSInittedArray(10, 3);

	private FixedLengthStringData wsaaUnitAllPercAmts = new FixedLengthStringData(170);
	private ZonedDecimalData[] wsaaUnitAllPercAmt = ZDArrayPartOfStructure(10, 17, 2, wsaaUnitAllPercAmts, 0);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator wsaaWholePlan = new Validator(wsaaPlanproc, "Y");
	private Validator wsaaSgleComponent = new Validator(wsaaPlanproc, "N");

	private FixedLengthStringData wsaaIfFirstTime = new FixedLengthStringData(1);
	private Validator wsaaFirstTime = new Validator(wsaaIfFirstTime, "Y");

	private FixedLengthStringData wsaaAgtTerminateFlag = new FixedLengthStringData(1);
	private Validator agtTerminated = new Validator(wsaaAgtTerminateFlag, "Y");
	private Validator agtNotTerminated = new Validator(wsaaAgtTerminateFlag, "N");

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);

	private FixedLengthStringData wsaaPayrkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrkey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrkey, 8);
	private PackedDecimalData wsaaTotTax = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaTaxableAmt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotNonInvest = new ZonedDecimalData(17, 2);
	private String wsaaTaxTranType = "";
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub3 = new PackedDecimalData(3, 0);
	private PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLifeassurSex = new FixedLengthStringData(1);
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);	
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitrcfiTableDAM hitrcfiIO = new HitrcfiTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcddmjaTableDAM pcddmjaIO = new PcddmjaTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrncfiTableDAM utrncfiIO = new UtrncfiTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5510rec t5510rec = new T5510rec();
	private T5515rec t5515rec = new T5515rec();
	private T5536rec t5536rec = new T5536rec();
	private T5537rec t5537rec = new T5537rec();
	private T5540rec t5540rec = new T5540rec();
	private T5543rec t5543rec = new T5543rec();
	private T5545rec t5545rec = new T5545rec();
	private T5551rec t5551rec = new T5551rec();
	private T5671rec t5671rec = new T5671rec();
	private T5645rec t5645rec = new T5645rec();
	private T6647rec t6647rec = new T6647rec();
	private T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private T5687rec t5687rec = new T5687rec();
	private T5675rec t5675rec = new T5675rec();
	private T5667rec t5667rec = new T5667rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Prasrec prasrec = new Prasrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Premiumrec premiumrec = new Premiumrec();
	private T6659rec t6659rec = new T6659rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private Sn500ScreenVars sv = ScreenProgram.getScreenVars( Sn500ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ExternalisedRules er = new ExternalisedRules();
	private Zutrpf zutrpf = new Zutrpf();
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAOImpl.class);
	private FixedLengthStringData checkIfFirstTime = new FixedLengthStringData(1);
	
	//ILIFE-8288 start
	//private PackedDecimalData cnttypercd = null;
	private int cnttypercd;
	//ILIFE-8288 end
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ZonedDecimalData outdate1 = null;
	private ZonedDecimalData[] outdates = new ZonedDecimalData[12];
	private ZonedDecimalData outdate2 = null;
	private ZonedDecimalData cnttypepresentdate = null;
	private int[] cnttypeallowedperiod = new int[12];
	private boolean fundPermission = false;
	private int allowperiod =0;
	//ILIFE-8137 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	private boolean fundList = false;//ILIFE-8164
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readUlnks1050, 
		exit1090, 
		utrnLoop1205, 
		exit1390, 
		preExit, 
		validate2020, 
		fndLoop2030, 
		endLoop2040, 
		redisplay2080, 
		exit2090, 
		utrnLoop3030, 
		continue3080, 
		processTax3050, 
		nonInvestTax3060, 
		exit3090, 
		exit4190, 
		init4850, 
		endloop14720, 
		endloop24740, 
		exit14790, 
		endloop34820, 
		exit5190, 
		para5900, 
		gotT55375920, 
		yOrdinate5940, 
		increment5960, 
		xOrdinate5980, 
		increment5985, 
		n00Loop5950, 
		exit5990, 
		pcddLoop7010, 
		addPcdt7020, 
		call7030, 
		enhanb9020, 
		enhanc9030, 
		enhand9040, 
		enhane9050, 
		enhanf9060, 
		nearExit9080, 
		a105HitrLoop, 
		calc12200, 
		calc12300, 
		exit12090
	}

	public Pn500() {
		super();
		screenVars = sv;
		new ScreenModel("Sn500", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					retriveChdrDetails1010();
					getCovrPayrLife1020();
					readT55401040();
				case readUlnks1050: 
					readUlnks1050();
					sacs1060();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsaaTotalPremium.set(sv.totlprem);
			wsaaTotalNetprem.set(sv.totlprem);
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaBusinessDate.set(datcon1rec.intDate);
		/* Initialise*/
		/* MOVE ZEROES                 TO SN500-TOTPREM                 */
		sv.totlprem.set(ZERO);
		sv.susamt.set(ZERO);
		sv.instprem.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.numpols.set(ZERO);
		sv.effdate.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.unitAllocPercAmt01.set(ZERO);
		sv.unitAllocPercAmt02.set(ZERO);
		sv.unitAllocPercAmt03.set(ZERO);
		sv.unitAllocPercAmt04.set(ZERO);
		sv.unitAllocPercAmt05.set(ZERO);
		sv.unitAllocPercAmt06.set(ZERO);
		sv.unitAllocPercAmt07.set(ZERO);
		sv.unitAllocPercAmt08.set(ZERO);
		sv.unitAllocPercAmt09.set(ZERO);
		sv.unitAllocPercAmt10.set(ZERO);
		wsaaPrevSa.set(ZERO);
		sv.sumins.set(ZERO);
		sv.zrtopusum.set(ZERO);
		sv.zrtotsumin.set(ZERO);
		sv.unitVirtualFund01.set(SPACES);
		sv.unitVirtualFund02.set(SPACES);
		sv.unitVirtualFund03.set(SPACES);
		sv.unitVirtualFund04.set(SPACES);
		sv.unitVirtualFund05.set(SPACES);
		sv.unitVirtualFund06.set(SPACES);
		sv.unitVirtualFund07.set(SPACES);
		sv.unitVirtualFund08.set(SPACES);
		sv.unitVirtualFund09.set(SPACES);
		sv.unitVirtualFund10.set(SPACES);
		sv.currcy01.set(SPACES);
		sv.currcy02.set(SPACES);
		sv.currcy03.set(SPACES);
		sv.currcy04.set(SPACES);
		sv.currcy05.set(SPACES);
		sv.currcy06.set(SPACES);
		sv.currcy07.set(SPACES);
		sv.currcy08.set(SPACES);
		sv.currcy09.set(SPACES);
		sv.currcy10.set(SPACES);
		wsaaTotalPerc.set(ZERO);
		wsaaUnitAllPercAmts.set(ZERO);
		wsaaUlnkFound.set(SPACES);
		prasrec.taxrelamt.set(ZERO);
		wsaaLifeassurSex.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.reserveUnitsDate.set(varcom.vrcmMaxDate); // ILIFE-5450
		wsaaUtrnWritten.set(SPACES);
		wsaaHitrWritten.set(SPACES);
		fundPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP086", appVars, "IT");
		fundList = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP106", appVars, "IT");//ILIFE-8164

	}



protected void allowperiodcalulation2021()
{   int i =0;
     int j=0;
	allowperiod=0;
	int k=0;
	
	  
    initialize(datcon1rec.datcon1Rec);
    datcon1rec.datcon1Rec.set(SPACES);
    datcon1rec.function.set(varcom.tday);
    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    cnttypepresentdate = datcon1rec.intDate;
	
	
	if (fundPermission == true )
	{
		//ILIFE-8288 start
		//cnttypercd = new PackedDecimalData(chdrpf.getCcdate());
		cnttypercd = chdrpf.getCcdate();
		//ILIFE-8288 end
		readT55511500();
		readTableT55436600();
		for(FixedLengthStringData funds:sv.unitVirtualFund) { //loop through all funds selected on UI
		for(i=0; i<12;i++)
		{
			if(funds!=null && funds.equals(t5543rec.unitVirtualFund[i])) {
				break;
			}
			
		}
		if(i==12)
			k=11;
		else
			k=i;
		cnttypeallowedperiod[j]=0;
		if(!isEQ(t5543rec.allowperiod[i],SPACES) && !isEQ(t5543rec.unitVirtualFund[i],SPACES)) {
			cnttypeallowedperiod[j] = t5543rec.allowperiod[i].toInt();
		
		}
	
	    
		outdates[j]=null; 
		if (cnttypeallowedperiod[j]!=0)
		{
			
			initialize(datcon2rec.datcon2Rec);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(cnttypercd);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(cnttypeallowedperiod[j]);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);  
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				datcon2rec.freqFactorx.set(ZERO);
			}
			outdates[j] = datcon2rec.intDate2;
		 }
	
		if (outdates[j]!= null && isLT(cnttypepresentdate.toInt(),outdates[j]) ) { 
			sv.vrtfndErr[j].set(errorsInner.rrku);
			wsspcomn.edterror.set("Y");
			
		}
		j++;
		}
	}  
}
protected void retriveChdrDetails1010()
	{
		/* RETRIEVE CONTRACT HEADER DETAILS WHICH HAVE BEEN STORED IN*/
		/* THE CHDRMJA I/O MODULE.*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
					chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.cnttype.set(chdrpf.getCnttype());
		sv.register.set(chdrpf.getReg());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.numpols.set(chdrpf.getPolinc());
	}

protected void getCovrPayrLife1020()
	{
		/*  The next check is to stop the program abending*/
		/*  when it is passed a blank key. In this case the program*/
		/*  should just go thru without processing.*/
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isEQ(covrmjaIO.getStatuz(), varcom.mrnf)) {
			covrmjaIO.setChdrnum(SPACES);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		//ILIFE-8137
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		/* Read the PAYR file with payer sequence number 1.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy());
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Read the client role file to get the payer number.*/
		clrfIO.setForepfx(chdrpf.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaPayrkey);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		sv.billcurr.set(payrIO.getBillcurr());
		/* If the user has already entered the Top-Up for a component*/
		/* do not allow them to alter it, instead redisplay the*/
		/* component selection screen.*/
		utrnExist1200();
		a100HitrExist();
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.planSuffix.set(covrpf.getPlanSuffix());
		sv.sumins.set(covrpf.getSumins());
		wsaaPrevSa.set(covrpf.getSumins());
		compute(sv.zrtotsumin, 2).set(add(sv.sumins, sv.zrtopusum));
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		sv.chdrnum.set(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		sv.life.set(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		wsaaCltdob.set(lifemjaIO.getCltdob());
		wsaaLifeassurSex.set(lifemjaIO.getCltsex());
		wsaaAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* CHECK FOR EXISTENCE OF JOINT LIFE DETAILS.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaAnbAtCcd2.set(lifemjaIO.getAnbAtCcd());
			wsaaSex2.set(lifemjaIO.getCltsex());
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setFunction("READR");
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				syserrrec.statuz.set(cltsIO.getStatuz());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*  Check if Special Terms has been entered.                       */
		checkLext1600();
		sv.crtable.set(covrpf.getCrtable());
		sv.instprem.set(ZERO);
		sv.percentAmountInd.set("P");
		taxMethT56881300();
	}

protected void readT55401040()
	{
		/* Get fund split plan*/
		sv.virtFundSplitMethod.set(SPACES);
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		//end;  
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5540Item.set(SPACES);
		wsaaT5540Crtable.set(covrpf.getCrtable());
		itdmIO.setItemitem(wsaaT5540Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5540)
		|| isNE(itdmIO.getItemitem(), wsaaT5540Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.h023);
			wsaaT5540Found = "N";
			goTo(GotoLabel.readUlnks1050);
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
			wsaaT5540Found = "Y";
		}
		sv.virtFundSplitMethod.set(SPACES);
		readT56711400();
		t5551rec.alfnds.set(SPACES);
		if (isEQ(wsaaT5671Found, "N")) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
		}
		else {
			readT55511500();
		}
		if (isNE(t5551rec.alfnds, SPACES)) {
			readTableT55436600();
		}
		readTr52d1700();
		if (isEQ(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			readTr52e1800();
		}
		//ILIFE-8164- STARTS
		if(fundList)
		{
			int ix = 0;
			for(FixedLengthStringData recData:t5543rec.unitVirtualFund) {
				if(recData != null) {
					sv.newFundList[ix].set(recData);	
				}
				ix++;
			}
			
			
				sv.newFundList01Out[varcom.nd.toInt()].set("N");
				sv.newFundList02Out[varcom.nd.toInt()].set("N");
				sv.newFundList03Out[varcom.nd.toInt()].set("N");
				sv.newFundList04Out[varcom.nd.toInt()].set("N");
				sv.newFundList05Out[varcom.nd.toInt()].set("N");
				sv.newFundList06Out[varcom.nd.toInt()].set("N");
				sv.newFundList07Out[varcom.nd.toInt()].set("N");
				sv.newFundList08Out[varcom.nd.toInt()].set("N");
				sv.newFundList09Out[varcom.nd.toInt()].set("N");
				sv.newFundList10Out[varcom.nd.toInt()].set("N");
				sv.newFundList11Out[varcom.nd.toInt()].set("N");
				sv.newFundList12Out[varcom.nd.toInt()].set("N");
				
			}
		else {

			sv.newFundList01Out[varcom.nd.toInt()].set("Y");
			sv.newFundList02Out[varcom.nd.toInt()].set("Y");
			sv.newFundList03Out[varcom.nd.toInt()].set("Y");
			sv.newFundList04Out[varcom.nd.toInt()].set("Y");
			sv.newFundList05Out[varcom.nd.toInt()].set("Y");
			sv.newFundList06Out[varcom.nd.toInt()].set("Y");
			sv.newFundList07Out[varcom.nd.toInt()].set("Y");
			sv.newFundList08Out[varcom.nd.toInt()].set("Y");
			sv.newFundList09Out[varcom.nd.toInt()].set("Y");
			sv.newFundList10Out[varcom.nd.toInt()].set("Y");
			sv.newFundList11Out[varcom.nd.toInt()].set("Y");
			sv.newFundList12Out[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-8164- ENDS
	}

protected void readUlnks1050()
	{
		/*  Read ULNK to see if Details already exist*/
		ulnkIO.setDataKey(SPACES);
		ulnkIO.setCoverage(covrpf.getCoverage());
		ulnkIO.setRider(covrpf.getRider());
		ulnkIO.setChdrcoy(covrpf.getChdrcoy());
		ulnkIO.setChdrnum(covrpf.getChdrnum());
		ulnkIO.setLife(covrpf.getLife());
		ulnkIO.setJlife("00");
		/* If the COVRMJA-PLAN-SUFFIX is not greater than the value of*/
		/* CHDRMJA-POLSUM then a 'notional' policy is being processed.*/
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			ulnkIO.setPlanSuffix(ZERO);
		}
		else {
			ulnkIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		ulnkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, ulnkIO);
		/*    Record exists then use dets*/
		/*    or No record so use 'blank' screen.*/
		if (isEQ(ulnkIO.getStatuz(), varcom.mrnf)
		|| isEQ(ulnkIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ulnkIO.getStatuz(), varcom.mrnf)) {
			wsaaUlnkFound.set("N");
			goTo(GotoLabel.exit1090);
		}
		wsaaUlnkFound.set("Y");
		for (x.set(1); !(isGT(x, 10)); x.add(1)){
			move1100();
		}
		sv.percentAmountInd.set(ulnkIO.getPercOrAmntInd());
	}

protected void sacs1060()
	{
		if (isEQ(wsspcomn.flag, "N")) {
			wsaaTotalPremium.set(ZERO);
			wsaaTotalNetprem.set(ZERO);
		}
		if (isNE(wsspcomn.flag, "N")) {
			sv.totlprem.set(wsaaTotalPremium);
			/* If Tolerance has been added to the previous premium, the        */
			/* TOTAL-NETPREM will be > than the Suspense amount. In this       */
			/* situation do not attempt to subtract TOTAL-NETPREM              */
			/* from Suspense, set Suspense amount to zero.                     */
			if (isGT(wsaaTotalNetprem, wsaaChdrSuspense)) {
				sv.susamt.set(ZERO);
			}
			else {
				compute(sv.susamt, 2).set(sub(wsaaChdrSuspense, wsaaTotalNetprem));
			}
			sv.paycurr.set(wsaaCurr);
			sv.exrat.set(wsaaExrate);
			sv.paycurrOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		wsspcomn.flag.set(SPACES);
		acblIO.setParams(SPACES);
		/* MOVE ZEROS                  TO ACBL-RLDGACCT.                */
		acblIO.setRldgcoy(wsspcomn.company);
		acblIO.setRldgacct(chdrpf.getChdrnum());
		acblIO.setOrigcurr(chdrpf.getCntcurr());
		/* Line 3, item PN500 on table T5645 is read and the right*/
		/* suspense account is chosen.*/
		readT56456400();
		acblIO.setSacscode(t5645rec.sacscode[3]);
		acblIO.setSacstyp(t5645rec.sacstype[3]);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError600();
		}
		/* IF ACBL-STATUZ              =  MRNF                          */
		if ((isEQ(acblIO.getStatuz(), varcom.mrnf))
		|| (isNE(acblIO.getSacscurbal().getbigdata(), NEGATIVE))) {
			wsaaChdrSuspense.set(ZERO);
			sv.exrat.set(ZERO);
		}
		else {
			compute(wsaaChdrSuspense, 2).set(mult(acblIO.getSacscurbal(), -1));
		}
		compute(sv.susamt, 2).set(sub(wsaaChdrSuspense, wssplife.bigAmt));
		sv.totlprem.set(wssplife.bigAmt);
		sv.paycurr.set(chdrpf.getCntcurr());
		wsaaCurr.set(chdrpf.getCntcurr());
		/*    Payment currency is not necessary contract currency, so      */
		/*    default to zero instead of 1. User can get the exchange      */
		/*    rate by pressing Refresh key.                                */
		sv.exrat.set(ZERO);
		wsaaExrate.set(ZERO);
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
			covrpf.setPlanSuffix(9999);
			//covrmjaIO.setFunction(varcom.begn);
			covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
			wsaaPlanproc.set("Y");
		}
		else {
			wsaaPlanproc.set("N");
			//covrmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		policyLoad5000();
	}

	/**
	* <pre>
	* Sections performed from the 1000 section.
	* </pre>
	*/
protected void move1100()
	{
		/*MOVE-VALUES*/
		sv.unitVirtualFund[x.toInt()].set(ulnkIO.getUalfnd(x));
		wsaaUnitVirtualFund[x.toInt()].set(ulnkIO.getUalfnd(x));
		sv.unitAllocPercAmt[x.toInt()].set(ulnkIO.getUalprc(x));
		wsaaUnitAllPercAmt[x.toInt()].set(ulnkIO.getUalprc(x));
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[x.toInt()]);
		if (isNE(sv.unitVirtualFund[x.toInt()], SPACES)) {
			readT55156300();
			if (isEQ(wsspcomn.edterror, "Y")) {
				return ;
			}
			else {
				sv.currcy[x.toInt()].set(t5515rec.currcode);
				wsaaCurrcy[x.toInt()].set(t5515rec.currcode);
			}
		}
		/*EXIT*/
	}

protected void utrnExist1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					utrn1200();
				case utrnLoop1205: 
					utrnLoop1205();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void utrn1200()
	{
		/* Read UTRN to see if Details already exist*/
		wsaaUtrnFound.set("N");
		utrnIO.setDataKey(SPACES);
		utrnIO.setCoverage(covrpf.getCoverage());
		utrnIO.setRider(covrpf.getRider());
		utrnIO.setChdrcoy(covrpf.getChdrcoy());
		utrnIO.setChdrnum(covrpf.getChdrnum());
		utrnIO.setLife(covrpf.getLife());
		utrnIO.setPlanSuffix(covrpf.getPlanSuffix());
		utrnIO.setTranno(ZERO);
		utrnIO.setFunction(varcom.begn);
	}

protected void utrnLoop1205()
	{
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.endp)) {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			fatalError600();
		}
		if (isEQ(utrnIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(utrnIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(utrnIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(utrnIO.getCoverage(), covrpf.getCoverage())
		|| isNE(utrnIO.getLife(), covrpf.getLife())
		|| isNE(utrnIO.getRider(), covrpf.getRider())
		|| isNE(utrnIO.getPlanSuffix(), covrpf.getPlanSuffix())) {
			return ;
		}
		if ((setPrecision(utrnIO.getTranno(), 0)
		&& isEQ(utrnIO.getTranno(), (add(chdrpf.getTranno(), 1))))) {
			wsaaUtrnFound.set("Y");
			return ;
		}
		utrnIO.setFunction(varcom.nextr);
		goTo(GotoLabel.utrnLoop1205);
	}

protected void taxMethT56881300()
	{
		try {
			para1310();
			readT66871320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para1310()
	{
		/* Look up the tax relief method on T5688.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		//end;  
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5688rec.taxrelmth, SPACES)) {
			t6687rec.t6687Rec.set(SPACES);
			goTo(GotoLabel.exit1390);
		}
	}

protected void readT66871320()
	{
		/* Look up the tax relief subroutine on T6687*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6687rec.t6687Rec.set(itemIO.getGenarea());
	}

protected void readT56711400()
	{
		para1400();
	}

protected void para1400()
	{
		/* The Available Fund List default field is held on both table*/
		/* T5551 and T5540. Table T5551 is used at New Business to obtain*/
		/* the available Fund List, while table T5540 is used at Single*/
		/* Prem Top Up. This can lead to Inconsistencies when both tables -*/
		/* are set up with different values. This program will now use*/
		/* table T5551.*/
		/* Read table T5671 in order to get part of the key needed while*/
		/* reading table T5551.*/
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrpf.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5551Item.set(SPACES);
		wsaaT5671Found = "N";
		for (x.set(1); !(isGT(x, 4)
		|| isEQ(wsaaT5671Found, "Y")); x.add(1)){
			if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
				if (isEQ(t5671rec.edtitm[x.toInt()], SPACES)) {
					scrnparams.errorCode.set(errorsInner.f025);
					wsspcomn.edterror.set("Y");
					x.set(5);
				}
				else {
					wsaaT5551Part1.set(t5671rec.edtitm[x.toInt()]);
					wsaaT5671Found = "Y";
				}
			}
		}
	}

protected void readT55511500()
	{
		para1500();
	}

	/**
	* <pre>
	* Get T5551 to obtain correct available fund list default
	* </pre>
	*/
protected void para1500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		//end; 
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5551);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5551Part2.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaT5551Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5551)
		|| isNE(itdmIO.getItemitem(), wsaaT5551Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
		
		//sv.reserveUnitsInd.set(t5551rec.rsvflg); // ILIFE-5450
	}

protected void checkLext1600()
	{
		checkLext1610();
	}

protected void checkLext1610()
	{
		lextIO.setChdrcoy(covrpf.getChdrcoy());
		lextIO.setChdrnum(covrpf.getChdrnum());
		lextIO.setLife(covrpf.getLife());
		lextIO.setCoverage(covrpf.getCoverage());
		lextIO.setRider(covrpf.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
	
		//Start Life Performance atiwari23
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		//end; 
	
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(covrpf.getChdrcoy(), lextIO.getChdrcoy())) {
			if (isNE(lextIO.getStatuz(), varcom.oK)
			&& isNE(lextIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(lextIO.getParams());
				fatalError600();
			}
		}
		if (isNE(covrpf.getChdrcoy(), lextIO.getChdrcoy())
		|| isNE(covrpf.getChdrnum(), lextIO.getChdrnum())
		|| isNE(covrpf.getLife(), lextIO.getLife())
		|| isNE(covrpf.getCoverage(), lextIO.getCoverage())
		|| isNE(covrpf.getRider(), lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(SPACES);
		}
		else {
			sv.optextind.set("+");
		}
	}

protected void readTr52d1700()
	{
		start1710();
	}

protected void start1710()
	{
		/* READ TR52D.                                                     */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrpf.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void readTr52e1800()
	{
		start1810();
	}

protected void start1810()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(wsaaTr52eKey);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrpf.getBtdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(chdrpf.getChdrcoy());
			itdmIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			itdmIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrpf.getBtdate());
			itdmIO.setFormat(formatsInner.itemrec);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
		}
		if ((isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(chdrpf.getChdrcoy());
			itdmIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itdmIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrpf.getBtdate());
			itdmIO.setFormat(formatsInner.itemrec);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		try {
			preStart();
			callScreen2020();
			
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| utrnFound.isTrue()) {
			calcPremium12000();
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.positionCursor.set("EFFDATE   ");
		if (isNE(scrnparams.errorCode, SPACES)) {
			scrnparams.function.set(varcom.prot);
			wsspcomn.flag.set("I");
		}
		
		//ILIFE-5946 Starts
		if(isNE(t5551rec.rsvflg, "Y")){
			sv.rundteOut[varcom.pr.toInt()].set("Y");
			sv.rsuninOut[varcom.pr.toInt()].set("Y");
		}//ILIFE-5946 END
		
		//ILIFE-5450 Starts
		if ((isEQ(t5551rec.rsvflg, "Y") && isEQ(sv.reserveUnitsInd, "Y")) ||
				(isEQ(t5551rec.rsvflg, "Y") && isEQ(sv.reserveUnitsInd, SPACE))){
			if(isNE(checkIfFirstTime, "Y")){
				sv.reserveUnitsInd.set(t5551rec.rsvflg);
			}
		} 
		
		if ((isEQ(t5551rec.rsvflg, "N") && isEQ(sv.reserveUnitsInd, "N")) ||
				(isEQ(t5551rec.rsvflg, "N") && isEQ(sv.reserveUnitsInd, SPACE))){
			if(isNE(checkIfFirstTime, "Y")){
				sv.reserveUnitsInd.set(t5551rec.rsvflg);
			}
		}
		
		if ((isEQ(t5551rec.rsvflg, SPACES) && isEQ(sv.reserveUnitsInd, SPACES)) ||
				(isEQ(t5551rec.rsvflg, SPACES) && isEQ(sv.reserveUnitsInd, SPACE))){
				sv.reserveUnitsInd.set(t5551rec.rsvflg);
		}
		//ILIFE-5450 Ends
		
		
	}

protected void callScreen2020()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
				case validate2020: 
					validate2020();
				case fndLoop2030: 
					fndLoop2030();
				case endLoop2040: 
					endLoop2040();
				case redisplay2080: 
					redisplay2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SN500IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   SN500-DATA-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaT5540Found, "N")) {
			scrnparams.errorCode.set(errorsInner.h023);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.redisplay2080);
		}
		if (isEQ(wsaaT5671Found, "N")) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.redisplay2080);
		}
		/* FOR 'CALC' OPTION, EITHER VALIDATE ALL FIELDS OR*/
		/* READ T5510 AND REDISPLAY.*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			if (isEQ(sv.virtFundSplitMethod, SPACES)) {
				goTo(GotoLabel.validate2020);
			}
			else {
				wsaaFndopt.set(sv.virtFundSplitMethod);
				readItem5100();
				if (isNE(sv.fndsplErr, SPACES)) {
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
				else {
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
			}
		}
		else {
			if (isNE(sv.virtFundSplitMethod, SPACES)) {
				wsaaFndopt.set(sv.virtFundSplitMethod);
				readItem5100();
				/** Screen errors are now handled in the calling program.           */
				/**          PERFORM 200-SCREEN-ERRORS                              */
			}
		}
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.validate2020);
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/* UNREACHABLE CODE
		wsspcomn.edterror.set(varcom.oK);
		 */
		allowperiodcalulation2021();
	}

protected void validate2020()
	{
		/* Validate fields*/
		sv.errorIndicators.set(SPACES);
		sub1.set(ZERO);
		allowperiodcalulation2021();//ILIFE-7891
	}

protected void fndLoop2030()
	{
		sub1.add(1);
		if (isGT(sub1, 10)) {
			goTo(GotoLabel.endLoop2040);
		}
		if (isEQ(sv.unitVirtualFund[1], SPACES)) {
			sv.vrtfndErr[sub1.toInt()].set(errorsInner.g037);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.unitVirtualFund[sub1.toInt()], SPACES)) {
			sv.currcy[sub1.toInt()].set(SPACES);
			goTo(GotoLabel.fndLoop2030);
		}
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[sub1.toInt()]);
		readT55156300();
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.exit2090);
		}
		sv.currcy[sub1.toInt()].set(t5515rec.currcode);
		wsaaCurrcy[sub1.toInt()].set(t5515rec.currcode);
		wsaaIfValidFund.set("N");
		if (isNE(sv.unitVirtualFund[sub1.toInt()], SPACES)
		&& isNE(t5551rec.alfnds, SPACES)) {
			for (sub3.set(1); !(isGT(sub3, 12)
			|| wsaaValidFund.isTrue()); sub3.add(1)){
				validateFund8000();
			}
			if (!wsaaValidFund.isTrue()) {
				sv.vrtfndErr[sub1.toInt()].set(errorsInner.h456);
			}
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		goTo(GotoLabel.fndLoop2030);
	}

protected void endLoop2040()
	{
		if (isNE(sv.percentAmountInd, "P")
		&& isNE(sv.percentAmountInd, "A")) {
			sv.pcamtindErr.set(errorsInner.g347);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.effdate, 99999999)) {
			sv.effdateErr.set(errorsInner.e859);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isLT(sv.effdate, varcom.vrcmDate)) {
			sv.effdateErr.set(errorsInner.e859);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isLT(sv.effdate, chdrpf.getOccdate())) {
			sv.effdateErr.set(errorsInner.h359);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.effdate, covrpf.getPremCessDate())) {
			sv.effdateErr.set(errorsInner.rf51);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* IF  SN500-SUSAMT            =  ZERO                          */
		/*     MOVE E751               TO SN500-SUSAMT-ERR              */
		/*     MOVE 'Y'                TO WSSP-EDTERROR                 */
		/*     GO TO 2090-EXIT                                          */
		/* END-IF.                                                      */
		if (isEQ(sv.instprem, ZERO)) {
			sv.instpremErr.set(errorsInner.e199);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.paycurr, SPACES)) {
			sv.paycurrErr.set(errorsInner.h960);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.instprem, ZERO)) {
			zrdecplrec.amountIn.set(sv.instprem);
			b000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.instprem)) {
				sv.instpremErr.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.sumins, ZERO)) {
			zrdecplrec.amountIn.set(sv.sumins);
			b000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.sumins)) {
				sv.suminsErr.set(errorsInner.rfik);
			}
		}
		calcPremium12000();
		if (isNE(sv.comind, "X")
		&& isNE(sv.comind, "+")
		&& isNE(sv.comind, SPACES)) {
			sv.comindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.optextind, "X")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, SPACES)) {
			sv.optextindErr.set(errorsInner.g620);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		editFundSplit5500();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* If the tax relief method is not spaces calculate the tax*/
		/* relief amount and deduct it from the premium.*/
		prasrec.taxrelamt.set(ZERO);
		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(clrfIO.getClntnum());
			prasrec.clntcoy.set(clrfIO.getClntcoy());
			prasrec.incomeSeqNo.set(payrIO.getIncomeSeqNo());
			prasrec.cnttype.set(chdrpf.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.effdate.set(sv.effdate);
			prasrec.company.set(chdrpf.getChdrcoy());
			prasrec.grossprem.set(sv.instprem);
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		b000CallRounding();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
		compute(wsaaNetPremium, 2).set(sub(sv.instprem, prasrec.taxrelamt));
		wsaaTotTax.set(ZERO);
		if (isNE(tr52drec.txcode, SPACES)
		&& isNE(sv.instprem, ZERO)
		&& isEQ(tr52erec.taxind01, "Y")) {
			wsaaTaxableAmt.set(sv.instprem);
			wsaaTaxTranType = "PREM";
			a500CalcTax();
		}
		wsaaNetPremium.add(wsaaTotTax);
		compute(sv.taxamt, 2).set(add(sv.instprem, wsaaTotTax));
		/* If Payment of premium is in a currency other than the           */
		/* Contract currency, retrieve the Suspense amount. The            */
		/* Currency can only be altered on the first Coverage,             */
		/* thereafter it is protected and used in all future iterations.   */
		/* If currency changed, validate against T3629.                    */
		if (isNE(sv.paycurr, wsaaCurr)) {
			itemIO.setDataArea(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.t3629);
			itemIO.setItemitem(sv.paycurr);
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				sv.paycurrErr.set(errorsInner.f982);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			findSuspense2000a();
		}
		/* Convert Net Premium if paying in currency other than the        */
		/* Contract currency.                                              */
		if (isNE(sv.paycurr, chdrpf.getCntcurr())) {
			convertNetPremium2000b();
		}
		/* If insufficient Suspense, calculate Tolerance.                  */
		wsaaTolerance.set(ZERO);
		if (((setPrecision(wsaaChdrSuspense, 2)
		&& isGT((add(wsaaNetPremium, wsaaTotalNetprem)), wsaaChdrSuspense)))) {
			checkAgent2000e();
			calcTolerance2000c();
		}
		/* Check sufficient suspense available.                            */
		if (((setPrecision(null, 2)
		&& isGT((add(wsaaNetPremium, wsaaTotalNetprem)), (add(wsaaTolerance, wsaaChdrSuspense)))))) {
			if (((setPrecision(null, 2)
			&& isGT((add(wsaaNetPremium, wsaaTotalNetprem)), (add(wsaaTolerance2, wsaaChdrSuspense)))))) {
				sv.instpremErr.set(errorsInner.hl06);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		/* Must convert premium if payment currency different to           */
		/* Contract currency before adding to total premium amount.        */
		/* Keep cummulative total of premiums in Contract currency.        */
		if (isEQ(scrnparams.statuz, "CALC")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(sv.paycurr, chdrpf.getCntcurr())) {
				wsaaTotalPremium.add(sv.instprem);
			}
			else {
				convertPremium2000d();
			}
			wsaaTotalNetprem.add(wsaaNetPremium);
		}
		
		// ILIFE-5450 Started		
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")){
			
			if (isEQ(sv.reserveUnitsDate, 99999999) || isEQ(sv.reserveUnitsDate, SPACES) || isEQ(sv.reserveUnitsDate, 00000000)) {
				sv.rundteErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			if (isLT(sv.reserveUnitsDate, chdrpf.getCcdate())) {
				sv.rundteErr.set(errorsInner.h359);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			if (isGT(sv.reserveUnitsDate, datcon1rec.intDate)) {
				sv.rundteErr.set(errorsInner.rlcc);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("N") 
				&& !sv.reserveUnitsDate.equals(varcom.vrcmMaxDate)){
			sv.rundteErr.set(errorsInner.g099);
			sv.rsuninErr.set(errorsInner.g099);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		
		if (isEQ(sv.reserveUnitsInd, SPACES)) {
			checkIfFirstTime.set("Y");
			sv.rsuninErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		
		// ILIFE-5450 Ended
	}

	/**
	* <pre>
	* Check the contract suspense for the net premium
	* This checking is removed.                                       
	**** IF  WSAA-NET-PREMIUM        >  SN500-SUSAMT                  
	****     MOVE E751               TO SN500-INSTPREM-ERR            
	****     MOVE 'Y'                TO WSSP-EDTERROR                 
	****     GO TO 2090-EXIT                                          
	**** END-IF.                                                      
	* Redisplay the screen if 'CALC'
	* </pre>
	*/
protected void redisplay2080()
	{
		if (isNE(scrnparams.statuz, "CALC")) {
			return ;
		}
		else {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     LOCATE SUSPENSE IN REQUESTED CURRENCY.                      
	* </pre>
	*/
protected void findSuspense2000a()
	{
		read2010a();
	}

protected void read2010a()
	{
		/* Retrieve Suspense for requested currency.                       */
		acblIO.setParams(SPACES);
		acblIO.setRldgacct(ZERO);
		acblIO.setRldgcoy(wsspcomn.company);
		acblIO.setRldgacct(chdrpf.getChdrnum());
		acblIO.setOrigcurr(sv.paycurr);
		acblIO.setSacscode(t5645rec.sacscode[3]);
		acblIO.setSacstyp(t5645rec.sacstype[3]);
		acblIO.setFormat(formatsInner.acblrec);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(), varcom.oK))
		&& (isNE(acblIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError600();
		}
		//ILIFE-7324
		/* Reset Suspense totals for new currency and exchange rate        */
		/* if reverting back to Contract currency.                         */
		if ((isEQ(acblIO.getStatuz(), varcom.mrnf))
		|| (isNE(acblIO.getSacscurbal().getbigdata(), NEGATIVE))) {
			wsaaChdrSuspense.set(ZERO);
			sv.susamt.set(ZERO);
			sv.exrat.set(ZERO);
			wsaaExrate.set(ZERO);
		}
		else {
			compute(wsaaChdrSuspense, 2).set(mult(acblIO.getSacscurbal(), -1));
			sv.susamt.set(wsaaChdrSuspense);
			if (isEQ(sv.paycurr, chdrpf.getCntcurr())) {
				sv.exrat.set(1);
				wsaaExrate.set(1);
			}
		}
		wsaaCurr.set(sv.paycurr);
	}

	/**
	* <pre>
	*     CONVERT THE NET PREMIUM TO THE SUSPENSE CURRENCY.           
	* </pre>
	*/
protected void convertNetPremium2000b()
	{
		call2010b();
	}

protected void call2010b()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currIn.set(chdrpf.getCntcurr());
		conlinkrec.currOut.set(sv.paycurr);
		conlinkrec.amountIn.set(wsaaNetPremium);
		conlinkrec.company.set(wsspcomn.company);
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		b000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaNetPremium.set(conlinkrec.amountOut);
		sv.exrat.set(conlinkrec.rateUsed);
		wsaaExrate.set(conlinkrec.rateUsed);
	}

	/**
	* <pre>
	*     CALCULATE TOLERANCE LEVEL.                                  
	* </pre>
	*/
protected void calcTolerance2000c()
	{
		read2010c();
	}

protected void read2010c()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5667);
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5667Curr.set(sv.paycurr);
		itemIO.setItemitem(wsaaT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		/* Calculate the Tolerance level for the payment frequency.        */
		wsaaToleranceApp.set(ZERO);
		wsaaToleranceLimit.set(ZERO);
		wsaaToleranceApp2.set(ZERO);
		wsaaToleranceLimit2.set(ZERO);
		for (sub1.set(1); !(isGT(sub1, 11)); sub1.add(1)){
			if (isEQ(chdrpf.getBillfreq(), t5667rec.freq[sub1.toInt()])) {
				wsaaToleranceApp.set(t5667rec.prmtol[sub1.toInt()]);
				wsaaToleranceApp2.set(t5667rec.prmtoln[sub1.toInt()]);
				wsaaToleranceLimit.set(t5667rec.maxAmount[sub1.toInt()]);
				wsaaToleranceLimit2.set(t5667rec.maxamt[sub1.toInt()]);
				sub1.set(11);
			}
		}
		/* Calculate the tolerance applicable.                             */
		compute(wsaaCalcTolerance, 2).set((div((mult(wsaaToleranceApp, (add(wsaaNetPremium, wsaaTotalNetprem)))), 100)));
		zrdecplrec.amountIn.set(wsaaCalcTolerance);
		b000CallRounding();
		wsaaCalcTolerance.set(zrdecplrec.amountOut);
		wsaaCalcTolerance2.set(0);
		if (isNE(wsaaToleranceLimit2, 0)) {
			if (agtNotTerminated.isTrue()
			|| (agtTerminated.isTrue()
			&& isEQ(t5667rec.sfind, "2"))) {
				compute(wsaaCalcTolerance2, 2).set((div((mult(wsaaToleranceApp2, (add(wsaaNetPremium, wsaaTotalNetprem)))), 100)));
			}
		}
		zrdecplrec.amountIn.set(wsaaCalcTolerance2);
		b000CallRounding();
		wsaaCalcTolerance2.set(zrdecplrec.amountOut);
		/* Check % amount is less than Limit on T5667, if so use this      */
		/* else use Limit.                                                 */
		if (isLT(wsaaCalcTolerance, wsaaToleranceLimit)) {
			wsaaTolerance.set(wsaaCalcTolerance);
		}
		else {
			wsaaTolerance.set(wsaaToleranceLimit);
		}
		if (isLT(wsaaCalcTolerance2, wsaaToleranceLimit2)) {
			wsaaTolerance2.set(wsaaCalcTolerance2);
		}
		else {
			wsaaTolerance2.set(wsaaToleranceLimit2);
		}
	}

	/**
	* <pre>
	*     CONVERT THE PREMIUM TO THE SUSPENSE CURRENCY.               
	* </pre>
	*/
protected void convertPremium2000d()
	{
		call2010d();
	}

protected void call2010d()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currIn.set(chdrpf.getCntcurr());
		conlinkrec.currOut.set(sv.paycurr);
		conlinkrec.amountIn.set(sv.instprem);
		conlinkrec.company.set(wsspcomn.company);
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		b000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaTotalPremium.add(conlinkrec.amountOut);
	}

protected void checkAgent2000e()
	{
		readAglf2010e();
	}

protected void readAglf2010e()
	{
		wsaaAgtTerminateFlag.set("N");
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrpf.getAgntcoy());
		aglfIO.setAgntnum(chdrpf.getAgntnum());
		aglfIO.setFormat(formatsInner.aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isLT(aglfIO.getDtetrm(), wsaaBusinessDate)
		|| isLT(aglfIO.getDteexp(), wsaaBusinessDate)
		|| isGT(aglfIO.getDteapp(), wsaaBusinessDate)) {
			wsaaAgtTerminateFlag.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
					setUpUtrn3020();
				case utrnLoop3030: 
					utrnLoop3030();
				case continue3080: 
					continue3080();
				case processTax3050: 
					processTax3050();
				case nonInvestTax3060: 
					nonInvestTax3060();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		wsaaTotNonInvest.set(ZERO);
		/* If CF12 is pressed delete any records already created.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			delUtrn3100();
			a200DelHitr();
			delPcdt3300();
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| utrnFound.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(covrpf.getChdrnum(), SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.tranrate.set(sv.instprem);
		wssplife.effdate.set(sv.effdate);
		wsspcomn.billcurr.set(sv.paycurr);
		calcPremium12000();
		/* INITIALIZE                  COVR-PARAMS.                     */
		/* MOVE COVRMJA-CHDRCOY        TO COVR-CHDRCOY.                 */
		/* MOVE COVRMJA-CHDRNUM        TO COVR-CHDRNUM.                 */
		/* MOVE COVRMJA-LIFE           TO COVR-LIFE.                    */
		/* MOVE COVRMJA-COVERAGE       TO COVR-COVERAGE.                */
		/* MOVE COVRMJA-RIDER          TO COVR-RIDER.                   */
		/* MOVE COVRMJA-PLAN-SUFFIX    TO COVR-PLAN-SUFFIX.             */
		/* MOVE BEGN                   TO COVR-FUNCTION.                */
		/* MOVE COVRREC                TO COVR-FORMAT.                  */
		/* MOVE 0                      TO WSAA-COVR-TRANNO.             */
		/* COMPUTE WSAA-COVR-TRANNO     = CHDRMJA-TRANNO + 1.           */
		/* PERFORM 13000-PROCESS-COVR UNTIL COVR-STATUZ = ENDP.         */
		/* If no PCDT record is to be created then the existing agent is*/
		/* to receive commission, therefore set up current details in*/
		/* PCDT to make AT processing uniform throughout.*/
		pcdtmjaIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		//end; 
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covrpf.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			defaultPcdt7000();
		}
		else {
			if (isNE(sv.comind, "X")) {
				updatePcdt7100();
			}
		}
		delUtrn3100();
		a200DelHitr();
		
		writeReservePricing(); 
		
	}

protected void setUpUtrn3020()
	{
		index1.set(ZERO);
		if (utrnWritten.isTrue()) {
			utrnIO.setFunction(varcom.updat);
		}
		else {
			utrnIO.setFunction(varcom.writr);
		}
		if (hitrWritten.isTrue()) {
			hitrIO.setFunction(varcom.updat);
		}
		else {
			hitrIO.setFunction(varcom.writr);
		}
	}

	/**
	* <pre>
	**** MOVE 'Y'                       TO WSAA-UTRN-WRITTEN. <LA4281>
	* </pre>
	*/
protected void utrnLoop3030()
	{
		index1.add(1);
		if (isGT(index1, 10)) {
			/*        GO TO 3090-EXIT                                          */
			if (isNE(sv.comind, "X")
			&& isNE(sv.optextind, "X")) {
				goTo(GotoLabel.processTax3050);
			}
			else {
				goTo(GotoLabel.exit3090);
			}
		}
		if (isEQ(sv.unitVirtualFund[index1.toInt()], SPACES)) {
			/*        GO TO 3090-EXIT                                          */
			if (isNE(sv.comind, "X")
			&& isNE(sv.optextind, "X")) {
				goTo(GotoLabel.processTax3050);
			}
			else {
				goTo(GotoLabel.exit3090);
			}
		}
		readT5537T55365900();
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models start
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VALCL")  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())) 
		{			
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(wsspcomn.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(chdrpf.getOccdate());
			untallrec.untaBillfreq.set("00");	

			callProgram("VALCL", untallrec.untallrec,chdrpf); 
			
			wsaaPcMaxPeriods.set(untallrec.untaMaxPeriod01);	
			wsaaPcInitUnits.set(untallrec.untaPcInitUnit01);
			wsaaPcUnits.set(untallrec.untaPcUnit01);
		}
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models end
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.exit3090);
		}
		readT66475800();
		if (isNE(t6647rec.unitStatMethod, SPACES)) {
			readT66598500();
		}
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[index1.toInt()]);
		readT55156300();
		//IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible START
		//ILIFE-2330 Start
		//ILIFE-2802 Start
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal("VALCL") || er.isCallExternal("VEUAL"))  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable()))) 
		{
			readT55456500();
		}	
		else
		{			
			Untallrec untallrec = new Untallrec();
			untallrec.untaCompany.set(wsspcomn.company);
			untallrec.untaBatctrcde.set(wsaaBatckey.batcBatctrcde);
			untallrec.untaEnhall.set(t6647rec.enhall);
			untallrec.untaCntcurr.set(chdrpf.getCntcurr());
			untallrec.untaCalcType.set("S");
			untallrec.untaEffdate.set(chdrpf.getOccdate());
			untallrec.untaInstprem.set(sv.instprem);
			untallrec.untaAllocInit.set(sv.instprem);
			untallrec.untaAllocAccum.set(ZERO);
			untallrec.untaBillfreq.set("00");		

			callProgram("VEUAL", untallrec.untallrec,chdrpf);
			
			wsaaEnhanceAlloc.set(untallrec.untaEnhanceAlloc);
			wsaaPerc.set(untallrec.untaEnhancePerc);
		}
		//ILIFE-2330 End
		///IVE-800 RUL Product - Enhanced Allocation Calculation - Integration with latest PA compatible END

		/* Set WSAA-INV-SINGP to the premium entered for the COVR.*/
		if (isEQ(sv.percentAmountInd, "P")) {
			wsaaSinglePrem.set(sv.instprem);
			wsaaInvSingp.set(sv.instprem);
		}
		else {
			wsaaSinglePrem.set(sv.unitAllocPercAmt[index1.toInt()]);
			wsaaInvSingp.set(sv.unitAllocPercAmt[index1.toInt()]);
		}
		/* ENHANCE THE PREMIUM AMOUNT BEFORE APPORTIONING TO FUND TYPE.*/
		compute(wsaaSinglePrem, 3).setRounded(div(mult(wsaaSinglePrem, wsaaPerc), 100));
		/* Derive the Enhanced Amount                                   */
		//ILife-3802
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VEUAL")  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())))  
		{
			compute(wsaaEnhanceAlloc, 3).setRounded(sub(wsaaSinglePrem, wsaaInvSingp));
		}
		//		MIBT-129 STARTS
//		if (isEQ(wsaaEnhanceAlloc, ZERO)) {
//			goTo(GotoLabel.continue3080);
//		}
		
		if(isLT(wsaaEnhanceAlloc,ZERO)){             // MTL002
			wsaaInvSingp.set(wsaaSinglePrem);        // MTL002
		}       //MTL002
		// if (isEQ(wsaaEnhanceAlloc,ZERO)) { MTL002
		if (isEQ(wsaaEnhanceAlloc,ZERO) ||  //MTL002
		   isLT(wsaaEnhanceAlloc,ZERO))  //MTL002	
		{
			goTo(GotoLabel.continue3080);
		}
//		MIBT-129 ENDS
		
		/* Derive the non-invested Enhanced Amount                      */
		compute(wsaaEnhanceAlloc, 3).setRounded(mult((sub(wsaaSinglePrem, wsaaInvSingp)), (sub(1, div(wsaaPcUnits, 100)))));
		/* Generate UTRN for the non-invested Enhanced Amount           */
		if (isGT(wsaaEnhanceAlloc, ZERO)) {
			wsaaEnhancedAllocInd.set("N");
			wsaaUnitType.set(SPACES);
			wsaaUtrnSingp.set(wsaaEnhanceAlloc);
			if (isNE(t5515rec.zfundtyp, "D")) {
				movesUtrn6000();
			}
			else {
				wsaaHitrSingp.set(wsaaEnhanceAlloc);
				a300MovesHitr();
			}
		}
		/* Derive the invested Enhanced Amount                          */
		compute(wsaaEnhanceAlloc, 3).setRounded(div(mult((sub(wsaaSinglePrem, wsaaInvSingp)), wsaaPcUnits), 100));
		/* Generate UTRN for the invested Enhanced Amount               */
		if (isGT(wsaaEnhanceAlloc, ZERO)) {
			wsaaEnhancedAllocInd.set("Y");
			wsaaUnitType.set(SPACES);
			wsaaUtrnSingp.set(wsaaEnhanceAlloc);
			if (isNE(t5515rec.zfundtyp, "D")) {
				movesUtrn6000();
			}
			else {
				wsaaHitrSingp.set(wsaaEnhanceAlloc);
				a300MovesHitr();
			}
		}
		wsaaEnhancedAllocInd.set("N");
	}

protected void continue3080()
	{
		/* If UNITS not 100% then adjust WSAA-INV-SINGP and create the*/
		/* uninvested UTRN.*/
		/* COMPUTE WSAA-UTRN-SINGP =  WSAA-INV-SINGP -                  */
		/*      (( WSAA-SINGLE-PREM * WSAA-PC-UNITS) / 100).            */
		compute(wsaaUtrnSingp, 2).set(sub(wsaaInvSingp, (div((mult(wsaaInvSingp, wsaaPcUnits)), 100))));
		zrdecplrec.amountIn.set(wsaaUtrnSingp);
		b000CallRounding();
		wsaaUtrnSingp.set(zrdecplrec.amountOut);
		wsaaTotNonInvest.set(wsaaUtrnSingp);
//		MIBT-129 STARTS

		if (isLT(wsaaEnhanceAlloc, ZERO)) {  //MTL002
			wsaaTotNonInvest.set(0);     //MTL002
			compute(wsaaUtrnSingp, 2).set(mult(wsaaEnhanceAlloc,-1));  //MTL002
			wsaaTotNonInvest.set(wsaaUtrnSingp); //MTL002
		} //MTL002
//		MIBT-129 ENDS
		/*Instead of posting the enhanced allocation amount as             */
		/*Non-investment, post it as an actual enhanced allocation amount  */
		/*whenever there is an enhanced allocation amount                  */
		/* MOVE ZEROES                 TO WSAA-ENHANCE-ALLOC.   <LA4281>*/
		if (isNE(wsaaUtrnSingp, ZERO)) {
			if (isNE(t5515rec.zfundtyp, "D")) {
				wsaaUnitType.set(SPACES);
				/*        COMPUTE WSAA-UTRN-SINGP  = WSAA-UTRN-SINGP * -1  <LA4281>*/
				/*        MOVE WSAA-UTRN-SINGP    TO WSAA-ENHANCE-ALLOC    <LA4281>*/
				/*        MOVE 'Y'              TO WSAA-ENHANCED-ALLOC-IND <LA4281>*/
				movesUtrn6000();
				/**        MOVE SPACES           TO WSAA-ENHANCED-ALLOC-IND <LA4281>*/
			}
			else {
				wsaaUnitType.set(SPACES);
				wsaaHitrSingp.set(wsaaUtrnSingp);
				a300MovesHitr();
			}
		}
		/* COMPUTE WSAA-INV-SINGP = WSAA-SINGLE-PREM *                  */
		/*                          WSAA-PC-UNITS / 100.                */
		/* Derive the invested amount                                   */
		compute(wsaaInvSingp, 2).set(div(mult(wsaaInvSingp, wsaaPcUnits), 100));
		/* Not to include the enhanced allocation amount into Single       */
		/* Premium Top-Up amount                                           */
		/* IF WSAA-ENHANCE-ALLOC        > 0                     <LA4281>*/
		/*    SUBTRACT WSAA-ENHANCE-ALLOC FROM WSAA-INV-SINGP   <LA4281>*/
		/* END-IF.                                              <LA4281>*/
		if (isEQ(t5515rec.zfundtyp, "D")) {
			wsaaUnitType.set("D");
			wsaaHitrSingp.set(wsaaInvSingp);
			a300MovesHitr();
			goTo(GotoLabel.utrnLoop3030);
		}
		/* Create UTRN for Initial*/
		if (isNE(wsaaPcInitUnits, ZERO)) {
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaInvSingp, wsaaPcInitUnits), 100));
			zrdecplrec.amountIn.set(wsaaUtrnSingp);
			b000CallRounding();
			wsaaUtrnSingp.set(zrdecplrec.amountOut);
			wsaaUnitType.set("I");
			movesUtrn6000();
		}
		/* If Initial Units not 100% then create Accumulation UTRN.*/
		if (isNE(wsaaPcInitUnits, 100)) {
			compute(wsaaUtrnSingp, 2).set(div(mult(wsaaInvSingp, (sub(100, wsaaPcInitUnits))), 100));
			zrdecplrec.amountIn.set(wsaaUtrnSingp);
			b000CallRounding();
			wsaaUtrnSingp.set(zrdecplrec.amountOut);
			wsaaUnitType.set("A");
			movesUtrn6000();
		}
		goTo(GotoLabel.utrnLoop3030);
	}

protected void processTax3050()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		/*  TXCL variable should hold the top up premium tax values.       */
		/*  We need to store the tax details for later posting.            */
		if (isEQ(txcalcrec.taxAmt[1], ZERO)
		&& isEQ(txcalcrec.taxAmt[2], ZERO)) {
			goTo(GotoLabel.nonInvestTax3060);
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrpf.getChdrcoy());
		taxdIO.setChdrnum(chdrpf.getChdrnum());
		taxdIO.setTrantype(txcalcrec.transType);
		taxdIO.setLife(covrpf.getLife());
		taxdIO.setCoverage(covrpf.getCoverage());
		taxdIO.setRider(covrpf.getRider());
		taxdIO.setPlansfx(covrpf.getPlanSuffix());
		taxdIO.setEffdate(sv.effdate);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(taxdIO.getTranref());
		taxdIO.setInstfrom(sv.effdate);
		taxdIO.setInstto(covrpf.getPremCessDate());
		taxdIO.setBillcd(sv.effdate);
		setPrecision(taxdIO.getTranno(), 0);
		taxdIO.setTranno(add(chdrpf.getTranno(), 1));
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg(SPACES);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError600();
		}
	}

protected void nonInvestTax3060()
	{
		if (isEQ(wsaaTotNonInvest, ZERO)) {
			return ;
		}
		wsaaTaxableAmt.set(wsaaTotNonInvest);
		wsaaTaxTranType = "NINV";
		a500CalcTax();
		if (isEQ(txcalcrec.taxAmt[1], ZERO)
		&& isEQ(txcalcrec.taxAmt[2], ZERO)) {
			return ;
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrpf.getChdrcoy());
		taxdIO.setChdrnum(chdrpf.getChdrnum());
		taxdIO.setTrantype(txcalcrec.transType);
		taxdIO.setLife(covrpf.getLife());
		taxdIO.setCoverage(covrpf.getCoverage());
		taxdIO.setRider(covrpf.getRider());
		taxdIO.setPlansfx(covrpf.getPlanSuffix());
		taxdIO.setEffdate(sv.effdate);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(taxdIO.getTranref());
		taxdIO.setInstfrom(sv.effdate);
		taxdIO.setInstto(covrpf.getPremCessDate());
		taxdIO.setBillcd(sv.effdate);
		setPrecision(taxdIO.getTranno(), 0);
		taxdIO.setTranno(add(chdrpf.getTranno(), 1));
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg(SPACES);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError600();
		}
	}

protected void delUtrn3100()
	{
		para3150();
	}

protected void para3150()
	{
		utrncfiIO.setParams(SPACES);
		utrncfiIO.setChdrcoy(covrpf.getChdrcoy());
		utrncfiIO.setChdrnum(covrpf.getChdrnum());
		utrncfiIO.setLife(covrpf.getLife());
		utrncfiIO.setCoverage(covrpf.getCoverage());
		utrncfiIO.setRider(covrpf.getRider());
		utrncfiIO.setPlanSuffix(covrpf.getPlanSuffix());
		wsaaTranno.set(ZERO);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(), 1));
		utrncfiIO.setTranno(wsaaTranno);
		utrncfiIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		utrncfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		//end; 
		
		while ( !(isEQ(utrncfiIO.getStatuz(),varcom.endp))) {
			deleteUtrn3200();
		}
		
	}

protected void deleteUtrn3200()
	{
		utrnLoop3310();
	}

protected void utrnLoop3310()
	{
		/* Delete UTRN's previously written by this transactiion.*/
		/* This only happens during modify mode.*/
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(), varcom.oK)
		&& isNE(utrncfiIO.getStatuz(), varcom.endp)) {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError600();
		}
		if (isNE(utrncfiIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(utrncfiIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(utrncfiIO.getCoverage(), covrpf.getCoverage())
		|| isNE(utrncfiIO.getLife(), covrpf.getLife())
		|| isNE(utrncfiIO.getRider(), covrpf.getRider())
		|| isNE(utrncfiIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isNE(utrncfiIO.getTranno(), wsaaTranno)
		|| isEQ(utrncfiIO.getStatuz(), varcom.endp)) {
			utrncfiIO.setStatuz(varcom.endp);
			return ;
			/****      GO TO 3190-EXIT                                          */
		}
		utrncfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError600();
		}
		compute(wssplife.bigAmt, 2).set(sub(wssplife.bigAmt, utrncfiIO.getContractAmount()));
		utrncfiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError600();
		}
		utrncfiIO.setFunction(varcom.nextr);
	}

protected void delPcdt3300()
	{
		para3310();
	}

protected void para3310()
	{
		pcdtmjaIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		//end;
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covrpf.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())
		|| isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			pcdtmjaIO.setStatuz(varcom.endp);
			return ;
		}
		pcdtmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		pcdtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4700();
			return ;
		}
		wsspcomn.nextprog.set(wsaaProg);
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (isEQ(sv.optextind, "X")) {
			sptermsExe4700();
		}
		else {
			if (isEQ(sv.optextind, "?")) {
				sptermsRet4800();
			}
			else {
				if (isEQ(sv.comind, "X")) {
					optionsExe4100();
				}
				else {
					if (isEQ(sv.comind, "?")) {
						optionsRet4300();
					}
					else {
						if (wsaaWholePlan.isTrue()) {
							//covrmjaIO.setFunction(varcom.nextr);
							clearScreen4800();
							policyLoad5000();
							if (covrpf != null) {
								wsspcomn.nextprog.set(wsaaProg);
								wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
								wayout4700();
							}
							else {
								wsspcomn.nextprog.set(scrnparams.scrname);
								wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
							}
						}
						else {
							wsspcomn.nextprog.set(wsaaProg);
							wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
							wayout4700();
						}
					}
				}
			}
		}
	}

protected void optionsExe4100()
	{
		try {
			para4100();
			endsave4120();
			endload4140();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para4100()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		/*covrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		covrpfDAO.setCacheObject(covrpf);
		sv.comind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			save4200();
		}
	}

protected void endsave4120()
	{
		/*  Call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*  program switching required,  and  move  them to the stack.*/
		gensswrec.function.set("A");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4190);
		}
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			load4250();
		}
	}

protected void endload4140()
	{
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4250()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4300()
	{
		para4300();
		endRestore4310();
	}

protected void para4300()
	{
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the options/extras indicator will be '?'. To handle*/
		/* the return from options and extras:*/
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			restore4400();
		}
	}

protected void endRestore4310()
	{
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		if (isEQ(sv.comind, "?")) {
			sv.comind.set("+");
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		utrncfiIO.setParams(SPACES);
		utrncfiIO.setChdrcoy(covrpf.getChdrcoy());
		utrncfiIO.setChdrnum(covrpf.getChdrnum());
		utrncfiIO.setLife(covrpf.getLife());
		utrncfiIO.setCoverage(covrpf.getCoverage());
		utrncfiIO.setRider(covrpf.getRider());
		utrncfiIO.setPlanSuffix(covrpf.getPlanSuffix());
		wsaaTranno.set(ZERO);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(), 1));
		utrncfiIO.setTranno(wsaaTranno);
		utrncfiIO.setFunction(varcom.begn);
		//Start Life Performance atiwari23
		utrncfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		//end;
		while ( !(isEQ(utrncfiIO.getStatuz(),varcom.endp))) {
			checkUtrn6200();
		}
		
	}

protected void restore4400()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		/*PARA*/
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/* If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/* spaces to  the current program entry in the program*/
		/* stack and exit.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void clearScreen4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case init4850: 
					init4850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		acblIO.setSacscode(t5645rec.sacscode[3]);
		acblIO.setSacstyp(t5645rec.sacstype[3]);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsaaChdrSuspense.set(ZERO);
		}
		else {
			compute(wsaaChdrSuspense, 2).set(mult(acblIO.getSacscurbal(), -1));
		}
		compute(sv.susamt, 2).set(sub(wsaaChdrSuspense, wssplife.bigAmt));
		/* MOVE WSSP-BIG-AMT           TO SN500-TOTPREM                 */
		sv.totlprem.set(wssplife.bigAmt);
		sv.instprem.set(ZERO);
		sv.effdate.set(ZERO);
		sv.reserveUnitsDate.set(ZERO);
		sub1.set(ZERO);
		x.set(ZERO);
	}

protected void init4850()
	{
		x.add(1);
		if (isGT(x, 10)) {
			x.set(ZERO);
			return ;
		}
		sv.currcy[x.toInt()].set(wsaaCurrcy[x.toInt()]);
		sv.unitVirtualFund[x.toInt()].set(wsaaUnitVirtualFund[x.toInt()]);
		sv.unitAllocPercAmt[x.toInt()].set(wsaaUnitAllPercAmt[x.toInt()]);
		goTo(GotoLabel.init4850);
	}

protected void sptermsExe4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para14700();
				case endloop14720: 
					endloop14720();
				case endloop24740: 
					endloop24740();
				case exit14790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para14700()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an   */
		/* options/extras request. If the indicator is 'X', a request to   */
		/* visit options and extras has been made. In this case:           */
		/*  - change the options/extras request indicator to '?',          */
		/*covrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covrpfDAO.setCacheObject(covrpf);
		sv.optextind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			save4710();
		}
		goTo(GotoLabel.endloop14720);
	}

protected void save4710()
	{
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
	}

protected void endloop14720()
	{
		/*  - call GENSSWCH with and  action  of 'B' to retrieve the       */
		/*       program switching required,  and  move  them to the       */
		/*       stack,                                                    */
		gensswrec.function.set("B");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator              */
		/* with its initial load value                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit14790);
		}
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			load4730();
		}
		goTo(GotoLabel.endloop24740);
	}

protected void load4730()
	{
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
	}

protected void endloop24740()
	{
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void sptermsRet4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para14800();
				case endloop34820: 
					endloop34820();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para14800()
	{
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			restore4810();
		}
		goTo(GotoLabel.endloop34820);
	}

protected void restore4810()
	{
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
	}

protected void endloop34820()
	{
		if (isEQ(sv.optextind, "?")) {
			sv.optextind.set(" ");
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
		/* returning to re-display the screen).                            */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT1*/
	}

protected void policyLoad5000()
	{
		/*LOAD-POLICY*/
//ILIFE-8265 start
	boolean foundCovr = false;
	if (wsaaWholePlan.isTrue()) {
		readCovr5400();
		
		for (Covrpf covr : covrpfList) {
			if (covr == null) {
				return ;
			}
			else if (isEQ(covr.getChdrcoy(),wsaaCovrChdrcoy)
					&& isEQ(covr.getChdrnum(),wsaaCovrChdrnum)
					&& isEQ(covr.getLife(),wsaaCovrLife)
					&& isEQ(covr.getCoverage(),wsaaCovrCoverage)
					&& isEQ(covr.getRider(),wsaaCovrRider)) {
					covrpf = covr;
					foundCovr = true;
					break;
				}else {
					continue;
				}
			}
		}

		if (!foundCovr)
			return;
//ILIFE-8265 end
		/* The header is set to show which selection is being actioned.*/
		if (wsaaWholePlan.isTrue()) {
			if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
				sv.planSuffix.set(chdrpf.getPolsum());
				sv.plnsfxOut[varcom.hi.toInt()].set("Y");
			}
			else {
				sv.planSuffix.set(covrpf.getPlanSuffix());
			}
		}
		/*EXIT*/
	}

protected void readItem5100()
	{
		try {
			para5100();
			gotIt5120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para5100()
	{
		/* Gets Fund Split Plan from T5510.*/
		/* If Fund Option = spaces, zeroise all numeric fields*/
		/* for use in Standard Split check later, and that's all!*/
		if (isEQ(wsaaFndopt, SPACES)) {
			for (x.set(1); !(isGT(x, 10)); x.add(1)){
				zeroise5200();
			}
			goTo(GotoLabel.exit5190);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		
		//end;
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5510);
		itdmIO.setItemitem(wsaaFndsplItem);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5510))
		|| (isNE(itdmIO.getItemitem(), wsaaFndsplItem))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			sv.fndsplErr.set(errorsInner.g103);
			goTo(GotoLabel.exit5190);
		}
		else {
			t5510rec.t5510Rec.set(itdmIO.getGenarea());
		}
	}

protected void gotIt5120()
	{
		for (x.set(1); !(isGT(x, 10)); x.add(1)){
			move5300();
		}
	}

protected void zeroise5200()
	{
		/*PARA*/
		t5510rec.unitPremPercent[x.toInt()].set(ZERO);
		t5510rec.unitVirtualFund[x.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void move5300()
	{
		para5310();
	}

protected void para5310()
	{
		/* The default value should override any other values entered.*/
		sv.unitVirtualFund[x.toInt()].set(t5510rec.unitVirtualFund[x.toInt()]);
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[x.toInt()]);
		/* Only read T5515 if unit virtual fund is not equal to spaces*/
		/* (also move the control subscript ('X') to the subscript*/
		/* which is used in the read of T5515 (SUB1)).*/
		if (isNE(wsaaT5515Item, SPACES)) {
			readT55156300();
			if (isEQ(wsspcomn.edterror, "Y")) {
				sv.currcy[x.toInt()].set(SPACES);
			}
			else {
				sv.currcy[x.toInt()].set(t5515rec.currcode);
			}
		}
		else {
			sv.currcy[x.toInt()].set(SPACES);
		}
		if (isNE(sv.percentAmountInd, "A")) {
			sv.unitAllocPercAmt[x.toInt()].set(t5510rec.unitPremPercent[x.toInt()]);
		}
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			sv.unitAllocPercAmt[x.toInt()].set(ZERO);
		}
	}

protected void readCovr5400()
	{
		/*READ-COVRMJA*/
		//ILIFE-8137
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
//ILIFE-8265 start
		if (covrpfList == null || covrpfList.isEmpty()) {
			syserrrec.params.set(covrpf.getChdrnum());
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
		/*for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)
					|| (!covrpfList.isEmpty())) {
						return;
					}
		}*/
//ILIFE-8265 end
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		/*EXIT*/
	}

protected void editFundSplit5500()
	{
		para5510();
		checkTotal5530();
	}

protected void para5510()
	{
		x.set(ZERO);
		wsaaTotalPerc.set(ZERO);
		/*LOOP*/
		for (x.set(1); !(isGT(x, 10)); x.add(1)){
			checkUalprc5600();
		}
	}

protected void checkTotal5530()
	{
		if (isEQ(sv.percentAmountInd, "P")
		&& isNE(wsaaTotalPerc, 100)) {
			sv.ualprcErr[1].set(errorsInner.e631);
			for (x.set(2); !(isGT(x, 10)); x.add(1)){
				ualprcErrs5700();
			}
			return ;
		}
		if (isEQ(sv.percentAmountInd, "A")) {
			if (isEQ(sv.instprem, ZERO)) {
				return ;
			}
			else {
				if (isNE(wsaaTotalPerc, sv.instprem)) {
					sv.ualprcErr[1].set(errorsInner.g106);
					for (x.set(2); !(isGT(x, 10)); x.add(1)){
						ualprcErrs5700();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void checkUalprc5600()
	{
		/*PARA*/
		if (isEQ(sv.unitVirtualFund[x.toInt()], SPACES)) {
			if (isNE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
				sv.ualprcErr[x.toInt()].set(errorsInner.g105);
				return ;
			}
			else {
				return ;
			}
		}
		else {
			if (isLTE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
				sv.ualprcErr[x.toInt()].set(errorsInner.f351);
			}
		}
		if (isEQ(sv.percentAmountInd, "P")
		&& isGT(sv.unitAllocPercAmt[x.toInt()], 100)) {
			sv.ualprcErr[x.toInt()].set(errorsInner.f348);
		}
		wsaaTotalPerc.add(sv.unitAllocPercAmt[x.toInt()]);
		/*EXIT*/
	}

protected void ualprcErrs5700()
	{
		/*PARA*/
		if (isNE(sv.unitAllocPercAmt[x.toInt()], ZERO)) {
			sv.ualprcErr[x.toInt()].set(sv.ualprcErr[1]);
		}
		/*EXIT*/
	}

protected void readT66475800()
	{
		para5800();
	}

protected void para5800()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		
		//end;
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT6647Trcde.set(wsaaBatckey.batcBatctrcde);
		wsaaT6647Cnttype.set(chdrpf.getCnttype());
		itdmIO.setItemitem(wsaaT6647Item);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Item))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT5537T55365900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para5900: 
					para5900();
				case gotT55375920: 
					gotT55375920();
				case yOrdinate5940: 
					yOrdinate5940();
				case increment5960: 
					increment5960();
				case xOrdinate5980: 
					xOrdinate5980();
				case increment5985: 
					increment5985();
				case n00Loop5950: 
					n00Loop5950();
				case exit5990: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5900()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5537);
		wsaaT5537Item.set(SPACES);
		wsaaT5537Allbas.set(t5540rec.allbas);
		wsaaT5537Sex.set(wsaaLifeassurSex);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaT5537Trans.set(wsaaBatckey.batcBatctrcde);
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/*   If NO record found then try first with the basis, * for*/
		/*   Sex and Transaction code.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT5537Sex.set("*");
			itemIO.setItemitem(wsaaT5537Item);
			SmartFileCode.execute(appVars, itemIO);
			if ((isNE(itemIO.getStatuz(), varcom.oK))
			&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(itemIO.getParams());
				itemIO.setStatuz(itemIO.getStatuz());
				fatalError600();
			}
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/*    If NO record found then try secondly with the basis, Sex*/
		/*    and '****'  Transaction code.*/
		wsaaT5537Sex.set(wsaaLifeassurSex);
		wsaaT5537Trans.fill("*");
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			itemIO.setStatuz(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and **** for Transaction code.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT5537Sex.set("*");
			wsaaT5537Trans.fill("*");
		}
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaT5537Trans.fill("*");
		/* If no match is found, try again with the global sex code*/
		/*    When doing a global change and the global data is being*/
		/*    moved to the WSAA-T5537-key make sure that you move the*/
		/*    WSAA-T5537-ITEM to the ITEM-ITEMITEM.*/
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.gotT55375920);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void gotT55375920()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*  Always calculate the age.*/
		calculateAge6700();
	}

	/**
	* <pre>
	*  Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate5940()
	{
		wsaaZ.set(0);
	}

protected void increment5960()
	{
		wsaaZ.add(1);
		/* Check the continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 10)
		&& isNE(t5537rec.agecont, SPACES)) {
			readAgecont6900();
			goTo(GotoLabel.yOrdinate5940);
		}
		else {
			if (isGT(wsaaZ, 10)
			&& isEQ(t5537rec.agecont, SPACES)) {
				sv.effdateErr.set(errorsInner.e706);
				wsspcomn.nextprog.set(scrnparams.scrname);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit5990);
			}
		}
		/*    IF  WSAA-CLTAGE             >  T5537-TOAGE(WSAA-Z) AND       */
		if (isGT(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])
		&& isLTE(wsaaZ, 10)) {
			goTo(GotoLabel.increment5960);
		}
		/*    IF  WSAA-CLTAGE             <  T5537-TOAGE(WSAA-Z) OR        */
		/*                                =  T5537-TOAGE(WSAA-Z)           */
		if (isLT(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])
		|| isEQ(wsaaCltage, t5537rec.ageIssageTo[wsaaZ.toInt()])) {
			wsaaY.set(wsaaZ);
		}
		else {
			wsaaT5537Item.set(t5537rec.agecont);
			goTo(GotoLabel.para5900);
		}
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate5980()
	{
		wsaaZ.set(0);
	}

protected void increment5985()
	{
		wsaaZ.add(1);
		/* Check the term continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 9)
		&& isNE(t5537rec.trmcont, SPACES)) {
			readTermcont6950();
			goTo(GotoLabel.xOrdinate5980);
		}
		else {
			if (isGT(wsaaZ, 9)
			&& isEQ(t5537rec.trmcont, SPACES)) {
				sv.effdateErr.set(errorsInner.e707);
				wsspcomn.nextprog.set(scrnparams.scrname);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit5990);
			}
		}
		/*    Always calculate the term.*/
		calculateTerm6800();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isGT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])
		&& isLT(wsaaZ, 10)) {
			goTo(GotoLabel.increment5985);
		}
		if (isLT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])
		|| isEQ(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])) {
			wsaaX.set(wsaaZ);
		}
		else {
			wsaaT5537Item.set(t5537rec.trmcont);
			goTo(GotoLabel.para5900);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY, 9));
		wsaaIndex.add(wsaaX);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
		
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal("VALCL") 
		|| er.isCallExternal("VEUAL"))  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())))
		{
			itdmIO.setDataArea(SPACES);
			itdmIO.setFunction(varcom.begn);		
	
			//Start Life Performance atiwari23
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
			//end;
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(tablesInner.t5536);
			itdmIO.setItmfrm(chdrpf.getOccdate());
			wsaaT5536Item.set(SPACES);
			wsaaT5536Allbas.set(wsaaAllocBasis);
			itdmIO.setItemitem(wsaaT5536Item);
			SmartFileCode.execute(appVars, itdmIO);
			if ((isNE(itdmIO.getStatuz(), varcom.oK))
			&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
			|| (isNE(itdmIO.getItemtabl(), tablesInner.t5536))
			|| (isNE(itdmIO.getItemitem(), wsaaT5536Item))
			|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			else {
				t5536rec.t5536Rec.set(itdmIO.getGenarea());
			}
			sub1.set(ZERO);
		}
		//VPMS externalization code end
	}

protected void n00Loop5950()
	{
	//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal("VALCL") || er.isCallExternal("VEUAL"))  && er.isExternalized(chdrpf.getCnttype(), covrpf.getCrtable())))/* IJTI-1523 */
		{
			sub1.add(1);
			if (isGT(sub1, 6)) {
				scrnparams.errorCode.set(errorsInner.t090);
				wsspcomn.edterror.set("Y");
				return ;
			}
			if (isNE(t5536rec.billfreq[sub1.toInt()], "00")) {
				goTo(GotoLabel.n00Loop5950);
			}
			if (isEQ(sub1, 1)) {
				wsaaPcUnits.set(t5536rec.pcUnitsa[1]);
				wsaaPcInitUnits.set(t5536rec.pcInitUnitsa[1]);
			}
			if (isEQ(sub1, 2)) {
				wsaaPcUnits.set(t5536rec.pcUnitsb[1]);
				wsaaPcInitUnits.set(t5536rec.pcInitUnitsb[1]);
			}
			if (isEQ(sub1, 3)) {
				wsaaPcUnits.set(t5536rec.pcUnitsc[1]);
				wsaaPcInitUnits.set(t5536rec.pcInitUnitsc[1]);
			}
			if (isEQ(sub1, 4)) {
				wsaaPcUnits.set(t5536rec.pcUnitsd[1]);
				wsaaPcInitUnits.set(t5536rec.pcInitUnitsd[1]);
			}
			if (isEQ(sub1, 5)) {
				wsaaPcUnits.set(t5536rec.pcUnitse[1]);
				wsaaPcInitUnits.set(t5536rec.pcInitUnitse[1]);
			}
			if (isEQ(sub1, 6)) {
				wsaaPcUnits.set(t5536rec.pcUnitsf[1]);
				wsaaPcInitUnits.set(t5536rec.pcInitUnitsf[1]);
			}
		}
		//VPMS externalization code end
	}

protected void movesUtrn6000()
	{
		moveValues6000();
	}

protected void moveValues6000()
	{
		wsaaUtrnWritten.set("Y");
		utrnIO.setStatuz(SPACES);
		utrnIO.setDataKey(SPACES);
		utrnIO.setChdrcoy(chdrpf.getChdrcoy());
		utrnIO.setChdrnum(chdrpf.getChdrnum());
		utrnIO.setLife(covrpf.getLife());
		utrnIO.setCoverage(covrpf.getCoverage());
		utrnIO.setRider(covrpf.getRider());
		utrnIO.setFormat(formatsInner.utrnrec);
		setPrecision(utrnIO.getTranno(), 0);
		utrnIO.setTranno(add(chdrpf.getTranno(), 1));
		utrnIO.setTermid(varcom.vrcmTermid);
		utrnIO.setTransactionDate(varcom.vrcmDate);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setUser(varcom.vrcmUser);
		utrnIO.setBatccoy(wsspcomn.company);
		utrnIO.setBatcbrn(wsspcomn.branch);
		wsaaBatckey.set(wsspcomn.batchkey);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		readT66475800();
		utrnIO.setNowDeferInd(t6647rec.aloind);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		if (isEQ(t6647rec.efdcode, "BD")) {
			utrnIO.setMoniesDate(wsaaBusinessDate);
		}
		else {
			if (isEQ(t6647rec.efdcode, "DD")) {
				utrnIO.setMoniesDate(sv.effdate);
			}
			else {
				if (isEQ(t6647rec.efdcode, "LO")) {
					utrnIO.setMoniesDate(sv.effdate);
				}
				else {
					utrnIO.setMoniesDate(ZERO);
				}
			}
		}
		if (isEQ(t6647rec.efdcode, "LO")
		&& isGT(wsaaBusinessDate, sv.effdate)) {
			utrnIO.setMoniesDate(wsaaBusinessDate);
		}
		// ILIFE-5450 Start
				if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")&&
						(isGT(sv.reserveUnitsDate,0)  || (!sv.reserveUnitsDate.toString().trim().equals(varcom.vrcmMaxDate)))){ 
					utrnIO.setMoniesDate(sv.reserveUnitsDate); 
					utrnIO.setNowDeferInd('N'); 
				}
				// ILIFE-5450 End	
		utrnIO.setCrtable(covrpf.getCrtable());
		utrnIO.setPlanSuffix(covrpf.getPlanSuffix());
		utrnIO.setCntcurr(chdrpf.getCntcurr());
		utrnIO.setContractType(chdrpf.getCnttype());
		utrnIO.setCrComDate(sv.effdate);
		utrnIO.setUnitVirtualFund(sv.unitVirtualFund[index1.toInt()]);
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[index1.toInt()]);
		utrnIO.setFundCurrency(sv.currcy[index1.toInt()]);
		readT56456400();
		/* If UTRN for Initial or Accumulate use the invested account*/
		/* otherwise use the non invested account. (GL, on table T5645)*/
		if (isEQ(wsaaUnitType, "I")
		|| isEQ(wsaaUnitType, "A")
		&& isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[4]);
			utrnIO.setSacstyp(t5645rec.sacstype[4]);
			utrnIO.setGenlcde(t5645rec.glmap[4]);
		}
		if (isEQ(wsaaUnitType, "I")
		|| isEQ(wsaaUnitType, "A")
		&& isNE(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[1]);
			utrnIO.setSacstyp(t5645rec.sacstype[1]);
			utrnIO.setGenlcde(t5645rec.glmap[1]);
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[5]);
			utrnIO.setSacstyp(t5645rec.sacstype[5]);
			utrnIO.setGenlcde(t5645rec.glmap[5]);
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& isNE(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode[2]);
			utrnIO.setSacstyp(t5645rec.sacstype[2]);
			utrnIO.setGenlcde(t5645rec.glmap[2]);
		}
		if (isEQ(wsaaEnhancedAllocInd, "Y")) {
			if (isEQ(wsaaUnitType, SPACES)
			&& isEQ(t5688rec.comlvlacc, "Y")) {
				utrnIO.setSacscode(t5645rec.sacscode[7]);
				utrnIO.setSacstyp(t5645rec.sacstype[7]);
				utrnIO.setGenlcde(t5645rec.glmap[7]);
			}
		}
		if (isEQ(wsaaEnhancedAllocInd, "Y")) {
			if (isEQ(wsaaUnitType, SPACES)
			&& isNE(t5688rec.comlvlacc, "Y")) {
				utrnIO.setSacscode(t5645rec.sacscode[6]);
				utrnIO.setSacstyp(t5645rec.sacstype[6]);
				utrnIO.setGenlcde(t5645rec.glmap[6]);
			}
		}
		if (isEQ(wsaaUnitType, "I")) {
			utrnIO.setUnitSubAccount("INIT");
		}
		if (isEQ(wsaaUnitType, "A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& (isNE(wsaaEnhancedAllocInd, "Y"))) {
			utrnIO.setUnitVirtualFund(SPACES);
			utrnIO.setUnitSubAccount("NVST");
		}
		if (isEQ(wsaaUnitType, SPACES)
		&& (isEQ(wsaaEnhancedAllocInd, "Y"))) {
			utrnIO.setUnitSubAccount("ACUM");
			wsaaUnitType.set("A");
		}
		if (isEQ(sv.percentAmountInd, "P")) {
			setPrecision(utrnIO.getContractAmount(), 3);
			utrnIO.setContractAmount(div(mult(wsaaUtrnSingp, sv.unitAllocPercAmt[index1.toInt()]), 100), true);
		}
		else {
			utrnIO.setContractAmount(wsaaUtrnSingp);
		}
		compute(wssplife.bigAmt, 2).set(add(wssplife.bigAmt, utrnIO.getContractAmount()));
		utrnIO.setUnitType(wsaaUnitType);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setSvp(1);
		utrnIO.setSwitchIndicator(SPACES);
		utrnIO.setFeedbackInd(SPACES);
		utrnIO.setTriggerModule(SPACES);
		utrnIO.setTriggerKey(SPACES);
		/* MOVE WRITR                  TO UTRN-FUNCTION.                */
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			fatalError600();
		}
			
	}

protected void checkUtrn6200()
	{
		/*UTRN*/
		/* Delete UTRN's previously written by this transactiion.*/
		/* This only happens during modify mode.*/
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(), varcom.oK)
		&& isNE(utrncfiIO.getStatuz(), varcom.endp)) {
			syserrrec.syserrType.set("1");
			syserrrec.dbparams.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError600();
		}
		if (isNE(utrncfiIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(utrncfiIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(utrncfiIO.getCoverage(), covrpf.getCoverage())
		|| isNE(utrncfiIO.getLife(), covrpf.getLife())
		|| isNE(utrncfiIO.getRider(), covrpf.getRider())
		|| isNE(utrncfiIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isNE(utrncfiIO.getTranno(), wsaaTranno)
		|| isEQ(utrncfiIO.getStatuz(), varcom.endp)) {
			utrncfiIO.setStatuz(varcom.endp);
			return ;
		}
		utrncfiIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void readT55156300()
	{
		para6300();
	}

protected void para6300()
	{
		wsspcomn.edterror.set(varcom.oK);
		itdmIO.setDataArea(SPACES);
		itdmIO.setFunction(varcom.begn);
		

		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		
		//end;
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setItemitem(wsaaT5515Item);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(), wsaaT5515Item))) {
			sv.vrtfndErr[sub1.toInt()].set(errorsInner.g037);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void readT56456400()
	{
		para6400();
	}

protected void para6400()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5645);
		wsaaT5645Item.set(SPACES);
		wsaaT5645Prog.set(wsaaProg);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT55456500()
	{
		para6500();
	}

protected void para6500()
	{
		if (isEQ(t6647rec.enhall, SPACES)) {
			wsaaPerc.set(100);
			return ;
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5545Item.set(SPACES);
		wsaaT5545Enhall.set(t6647rec.enhall);
		wsaaT5545Currcode.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFunction(varcom.begn);
		

		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		
		//end;
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5545)
		|| isNE(itdmIO.getItemitem(), wsaaT5545Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5545rec.t5545Rec.set(itdmIO.getGenarea());
		}
		wsaaPerc.set(0);
		sub1.set(1);
		while ( !((isGT(sub1, 6))
		|| (isNE(wsaaPerc, 0)))) {
			matchEnhanceBasis9000();
		}
		
	}

protected void readTableT55436600()
	{
		start6600();
	}

protected void start6600()
	{
		itemIO.setDataArea(SPACES);
		t5543rec.t5543Rec.set(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5543);
		wsaaT5543Item.set(SPACES);
		wsaaT5543Fund.set(t5551rec.alfnds);
		itemIO.setItemitem(wsaaT5543Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t5543rec.t5543Rec.set(itemIO.getGenarea());
		}
	}

protected void calculateAge6700()
	{
		para6700();
	}

protected void para6700()
	{
		/*  Round up the Age*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(wsaaCltdob);
		agecalcrec.intDate2.set(sv.effdate);
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaCltage.set(agecalcrec.agerating);
	}

protected void calculateTerm6800()
	{
		/*PARA*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(sv.effdate);
		datcon3rec.intDate2.set(wsaaCltdob);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		/*EXIT*/
	}

protected void readAgecont6900()
	{
		/*READ-AGECONT*/
		/* Read Age continuation Item.*/
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getDataArea());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
		/*EXIT*/
	}

protected void readTermcont6950()
	{
		/*READ-TERMCONT*/
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void defaultPcdt7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					moveValues7000();
					servagntComm7005();
				case pcddLoop7010: 
					pcddLoop7010();
				case addPcdt7020: 
					addPcdt7020();
				case call7030: 
					call7030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void moveValues7000()
	{
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		pcdtmjaIO.setInstprem(sv.instprem);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
	}

	/**
	* <pre>
	* When no Commission Details are input the Servicing Agent gets
	* all the Commission.
	* </pre>
	*/
protected void servagntComm7005()
	{
		if (isEQ(sv.comind, SPACES)) {
			wsaaIndex.set(1);
			pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
			pcdtmjaIO.setSplitc(wsaaIndex, 100);
			wsaaIndex.add(1);
			goTo(GotoLabel.addPcdt7020);
		}
		pcddmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcddmjaIO.setChdrnum(chdrpf.getChdrnum());
		pcddmjaIO.setFunction(varcom.begn);

		//Start Life Performance atiwari23
		pcddmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcddmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		//end;
		
		wsaaIndex.set(ZERO);
	}

protected void pcddLoop7010()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 10)) {
			goTo(GotoLabel.call7030);
		}
		pcddmjaIO.setFormat(formatsInner.pcddmjarec);
		SmartFileCode.execute(appVars, pcddmjaIO);
		if (isNE(pcddmjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(pcddmjaIO.getChdrcoy(), chdrpf.getChdrcoy())) {
			pcddmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcddmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcddmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcddmjaIO.getParams());
			syserrrec.statuz.set(pcddmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(pcddmjaIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.addPcdt7020);
		}
		pcdtmjaIO.setAgntnum(wsaaIndex, pcddmjaIO.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, pcddmjaIO.getSplitBcomm());
		pcddmjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.pcddLoop7010);
	}

protected void addPcdt7020()
	{
		if (isGT(wsaaIndex, 10)) {
			goTo(GotoLabel.call7030);
		}
		pcdtmjaIO.setAgntnum(wsaaIndex, SPACES);
		pcdtmjaIO.setSplitc(wsaaIndex, ZERO);
		wsaaIndex.add(1);
		goTo(GotoLabel.addPcdt7020);
	}

protected void call7030()
	{
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void updatePcdt7100()
	{
		para7110();
	}

protected void para7110()
	{
		/* Update the PCDT to ensure that if the premium amount on the*/
		/* screen happens to change then the installment premium on the*/
		/* PCDT record is also updated. The Instalment premium on the*/
		/* PCDT record will be used later to create the Suspense ACMV*/
		/* posting.*/
		pcdtmjaIO.setParams(SPACES);
		pcdtmjaIO.setFunction(varcom.readh);
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		pcdtmjaIO.setInstprem(sv.instprem);
		pcdtmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validateFund8000()
	{
		/*START*/
		if (isEQ(t5543rec.unitVirtualFund[sub3.toInt()], SPACES)) {
			return ;
		}
		if (isEQ(sv.unitVirtualFund[sub1.toInt()], t5543rec.unitVirtualFund[sub3.toInt()])) {
			wsaaIfValidFund.set("Y");
			return ;
		}
		/*EXIT*/
	}

protected void readT66598500()
	{
		read8501();
		checkT6659Details8550();
	}

protected void read8501()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(wssplife.effdate);
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM");
		
		//end;
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(t6647rec.unitStatMethod, itdmIO.getItemitem())
		|| isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g029);
			fatalError600();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkT6659Details8550()
	{
		/* Check the details and call the generic subroutine from T6659.   */
		if (isEQ(t6659rec.subprog, SPACES)
		|| isNE(t6659rec.annOrPayInd, "P")
		|| isNE(t6659rec.osUtrnInd, "Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(wsspcomn.company);
		annprocrec.chdrnum.set(chdrpf.getChdrnum());
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(wssplife.effdate);
		annprocrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(varcom.vrcmUser);
		annprocrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		annprocrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		annprocrec.batccoy.set(wsaaBatckey.batcBatccoy);
		annprocrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		annprocrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		annprocrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError600();
		}
	}

protected void matchEnhanceBasis9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
					enhana9010();
				case enhanb9020: 
					enhanb9020();
				case enhanc9030: 
					enhanc9030();
				case enhand9040: 
					enhand9040();
				case enhane9050: 
					enhane9050();
				case enhanf9060: 
					enhanf9060();
				case nearExit9080: 
					nearExit9080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		/* First match the billing frequency.*/
		/* We are only dealing with Single Prems here, so check for '00'*/
		if (isEQ(t5545rec.billfreq[sub1.toInt()], SPACES)) {
			goTo(GotoLabel.nearExit9080);
		}
		if (isNE(t5545rec.billfreq[sub1.toInt()], "00")) {
			goTo(GotoLabel.nearExit9080);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana9010()
	{
		if (isNE(sub1, 1)) {
			goTo(GotoLabel.enhanb9020);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanaPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhanb9020()
	{
		if (isNE(sub1, 2)) {
			goTo(GotoLabel.enhanc9030);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanbPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhanc9030()
	{
		if (isNE(sub1, 3)) {
			goTo(GotoLabel.enhand9040);
		}
		if ((isGTE(sv.instprem, t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhancPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhand9040()
	{
		if (isNE(sub1, 4)) {
			goTo(GotoLabel.enhane9050);
		}
		if ((isGTE(sv.instprem, t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhandPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhane9050()
	{
		if (isNE(sub1, 5)) {
			goTo(GotoLabel.enhanf9060);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanePc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit9080);
	}

protected void enhanf9060()
	{
		if (isNE(sub1, 6)) {
			goTo(GotoLabel.nearExit9080);
		}
		if ((isGTE(sv.instprem, t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05, 0))) {
			wsaaPerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(sv.instprem, t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04, 0))) {
				wsaaPerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(sv.instprem, t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03, 0))) {
					wsaaPerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(sv.instprem, t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02, 0))) {
						wsaaPerc.set(t5545rec.enhanfPc02);
					}
					else {
						wsaaPerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit9080()
	{
		sub1.add(1);
		/*EXIT*/
	}

protected void a100HitrExist()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a100Hitr();
				case a105HitrLoop: 
					a105HitrLoop();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a100Hitr()
	{
		/* Read T6647 to retrieve SEQ-NO, which is used to distinquish     */
		/* Coverages from Riders.                                          */
		readT66475800();
		/* Read HITR to see if Details already exist                       */
		wsaaHitrFound.set("N");
		hitrIO.setDataKey(SPACES);
		hitrIO.setCoverage(covrpf.getCoverage());
		hitrIO.setRider("00");
		hitrIO.setChdrcoy(covrpf.getChdrcoy());
		hitrIO.setChdrnum(covrpf.getChdrnum());
		hitrIO.setLife(covrpf.getLife());
		hitrIO.setPlanSuffix(covrpf.getPlanSuffix());
		hitrIO.setTranno(ZERO);
		hitrIO.setFunction(varcom.begn);
		wsaaRiderAlpha.set(covrpf.getRider());
		compute(wsaaProcSeqno, 0).set(add(t6647rec.procSeqNo, wsaaRiderNum));
	}

protected void a105HitrLoop()
	{
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)
		&& isNE(hitrIO.getStatuz(), varcom.endp)) {
			syserrrec.dbparams.set(hitrIO.getParams());
			fatalError600();
		}
		if (isEQ(hitrIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(hitrIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(hitrIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(hitrIO.getCoverage(), covrpf.getCoverage())
		|| isNE(hitrIO.getLife(), covrpf.getLife())
		|| isNE(hitrIO.getPlanSuffix(), covrpf.getPlanSuffix())) {
			return ;
		}
		if ((setPrecision(hitrIO.getTranno(), 0)
		&& isEQ(hitrIO.getTranno(), add(chdrpf.getTranno(), 1)))
		&& isEQ(hitrIO.getProcSeqNo(), wsaaProcSeqno)) {
			wsaaHitrFound.set("Y");
			return ;
		}
		hitrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.a105HitrLoop);
	}

protected void a200DelHitr()
	{
		a201Hitr();
	}

protected void a201Hitr()
	{
		hitrcfiIO.setParams(SPACES);
		hitrcfiIO.setChdrcoy(covrpf.getChdrcoy());
		hitrcfiIO.setChdrnum(covrpf.getChdrnum());
		hitrcfiIO.setLife(covrpf.getLife());
		hitrcfiIO.setCoverage(covrpf.getCoverage());
		hitrcfiIO.setRider(covrpf.getRider());
		hitrcfiIO.setPlanSuffix(covrpf.getPlanSuffix());
		wsaaTranno.set(ZERO);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(), 1));
		hitrcfiIO.setTranno(wsaaTranno);
		hitrcfiIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		hitrcfiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrcfiIO.setFitKeysSearch("CHDRNUM","LIFE","COVERAGE","CHDRCOY","PLNSFX","TRANNO");
		//end;
		while ( !(isEQ(hitrcfiIO.getStatuz(),varcom.endp))) {
			a250DeleteHitr();
		}
		
	}

protected void a250DeleteHitr()
	{
		a251HitrLoop();
	}

protected void a251HitrLoop()
	{
		/* Delete HITRs previously written by this transactiion.           */
		/* This only happens during modify mode.                           */
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(), varcom.oK)
		&& isNE(hitrcfiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			fatalError600();
		}
		if (isNE(hitrcfiIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(hitrcfiIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(hitrcfiIO.getCoverage(), covrpf.getCoverage())
		|| isNE(hitrcfiIO.getLife(), covrpf.getLife())
		|| isNE(hitrcfiIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isNE(hitrcfiIO.getTranno(), wsaaTranno)
		|| isEQ(hitrcfiIO.getStatuz(), varcom.endp)) {
			hitrcfiIO.setStatuz(varcom.endp);
			return ;
		}
		/* Skip if this HITR is for a different Coverage/Rider.            */
		/* This Section is not just called when modifying HITRs.           */
		if (isNE(hitrIO.getProcSeqNo(), wsaaProcSeqno)) {
			hitrcfiIO.setFunction(varcom.nextr);
			return ;
		}
		hitrcfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			fatalError600();
		}
		compute(wssplife.bigAmt, 2).set(sub(wssplife.bigAmt, hitrcfiIO.getContractAmount()));
		hitrcfiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			fatalError600();
		}
		hitrcfiIO.setFunction(varcom.nextr);
	}

protected void a300MovesHitr()
	{
		a310Hitr();
	}

protected void a310Hitr()
	{
		wsaaHitrWritten.set("Y");
		hitrIO.setDataKey(SPACES);
		hitrIO.setChdrcoy(chdrpf.getChdrcoy());
		hitrIO.setChdrnum(chdrpf.getChdrnum());
		hitrIO.setLife(covrpf.getLife());
		hitrIO.setCoverage(covrpf.getCoverage());
		hitrIO.setRider("00");
		hitrIO.setFormat(formatsInner.hitrrec);
		setPrecision(hitrIO.getTranno(), 0);
		hitrIO.setTranno(add(chdrpf.getTranno(), 1));
		hitrIO.setBatccoy(wsspcomn.company);
		hitrIO.setBatcbrn(wsspcomn.branch);
		wsaaBatckey.set(wsspcomn.batchkey);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		readT66475800();
		hitrIO.setProcSeqNo(wsaaProcSeqno);
		hitrIO.setEffdate(sv.effdate);
		hitrIO.setCrtable(covrpf.getCrtable());
		hitrIO.setPlanSuffix(covrpf.getPlanSuffix());
		hitrIO.setCntcurr(chdrpf.getCntcurr());
		hitrIO.setCnttyp(chdrpf.getCnttype());
		hitrIO.setZintbfnd(sv.unitVirtualFund[index1.toInt()]);
		wsaaT5515Item.set(SPACES);
		wsaaT5515Fund.set(sv.unitVirtualFund[index1.toInt()]);
		hitrIO.setFundCurrency(sv.currcy[index1.toInt()]);
		readT56456400();
		if (isEQ(wsaaUnitType, SPACES)) {
			if (isEQ(wsaaEnhancedAllocInd, "Y")) {
				if (isEQ(t5688rec.comlvlacc, "Y")) {
					hitrIO.setSacscode(t5645rec.sacscode[7]);
					hitrIO.setSacstyp(t5645rec.sacstype[7]);
					hitrIO.setGenlcde(t5645rec.glmap[7]);
				}
				else {
					hitrIO.setSacscode(t5645rec.sacscode[6]);
					hitrIO.setSacstyp(t5645rec.sacstype[6]);
					hitrIO.setGenlcde(t5645rec.glmap[6]);
				}
				hitrIO.setZrectyp("P");
			}
			else {
				if (isEQ(t5688rec.comlvlacc, "Y")) {
					hitrIO.setSacscode(t5645rec.sacscode[11]);
					hitrIO.setSacstyp(t5645rec.sacstype[11]);
					hitrIO.setGenlcde(t5645rec.glmap[11]);
				}
				else {
					hitrIO.setSacscode(t5645rec.sacscode[9]);
					hitrIO.setSacstyp(t5645rec.sacstype[9]);
					hitrIO.setGenlcde(t5645rec.glmap[9]);
				}
				hitrIO.setZrectyp("N");
				hitrIO.setZintbfnd(SPACES);
			}
		}
		else {
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				hitrIO.setSacscode(t5645rec.sacscode[10]);
				hitrIO.setSacstyp(t5645rec.sacstype[10]);
				hitrIO.setGenlcde(t5645rec.glmap[10]);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode[8]);
				hitrIO.setSacstyp(t5645rec.sacstype[8]);
				hitrIO.setGenlcde(t5645rec.glmap[8]);
			}
			hitrIO.setZrectyp("P");
		}
		if (isEQ(sv.percentAmountInd, "P")) {
			setPrecision(hitrIO.getContractAmount(), 3);
			hitrIO.setContractAmount(div(mult(wsaaHitrSingp, sv.unitAllocPercAmt[index1.toInt()]), 100), true);
		}
		else {
			hitrIO.setContractAmount(wsaaHitrSingp);
		}
		compute(wssplife.bigAmt, 2).set(add(wssplife.bigAmt, hitrIO.getContractAmount()));
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setZintrate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setZintrate(ZERO);
		hitrIO.setSvp(1);
		hitrIO.setSwitchIndicator(SPACES);
		hitrIO.setFeedbackInd(SPACES);
		hitrIO.setTriggerModule(SPACES);
		hitrIO.setTriggerKey(SPACES);
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError600();
		}
	}

protected void calcPremium12000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					calcPremium12100();
				case calc12200: 
					calc12200();
				case calc12300: 
					calc12300();
				case exit12090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calcPremium12100()
	{
		/*  Obtain the general Coverage/Rider details (including Premium   */
		/*  Calculation Method details from T5687 using the coverage/      */
		/*  rider code as part of the key (COVRMJA-CRTABLE).               */
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		
		//Start Life Performance atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL","ITEMITEM","ITEMCOY");
		
		//end;
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(tablesInner.t5687);
			itdmIO.setItemitem(covrpf.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.e366);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/*  Use the premium-method selected from T5687, if not             */
		/*  blank,  to access T5675.  This gives the subroutine            */
		/*  to use for the calculation.                                    */
		if ((isEQ(t5687rec.premmeth, SPACES)
		&& isEQ(sv.jlife, SPACES))
		|| (isEQ(t5687rec.jlPremMeth, SPACES)
		&& isNE(sv.jlife, SPACES))) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 12900-EXIT                                         */
			goTo(GotoLabel.exit12090);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		if (isEQ(sv.jlife, SPACES)) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		/*    IF T5687-PREMMETH           = SPACES                         */
		/*        MOVE WSAA-TOPUP-PREM-METH TO ITEM-ITEMITEM               */
		/*    END-IF.                                                      */
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		wsaaPremMeth.set(itemIO.getItemitem());
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		/* Check for a joint life for separate processing.                 */
		if (isEQ(sv.jlife, SPACES)) {
			goTo(GotoLabel.calc12200);
		}
		else {
			goTo(GotoLabel.calc12300);
		}
	}

protected void calc12200()
	{
		premiumrec.premiumRec.set(SPACES);
		premiumrec.function.set("TOPU");
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaLifeassurSex);
		premiumrec.jlage.set(wsaaAnbAtCcd2);
		premiumrec.jlsex.set(wsaaSex2);
		premiumrec.effectdt.set(chdrpf.getOccdate());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor, .99999));
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.sumin.set(wsaaPrevSa);
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.instprem);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		premiumrec.plnsfx.set(covrpf.getPlanSuffix());
		premiumrec.inputPrevPrem.set(ZERO);//ILIFE-7524

		//****Ticket #VE-155/263 TRM Calculations - Premium calculation start
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) && er.isExternalized(premiumrec.cnttype.toString(), premiumrec.crtable.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			premiumrec.premMethod.set(wsaaPremMeth);
			//ILIFE-3178 Starts
			premiumrec.calcBasPrem.set(sv.instprem);
			//ILIFE-3178 Ends
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec,vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation end

		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.instpremErr.set(premiumrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 12900-EXIT                                          */
			goTo(GotoLabel.exit12090);
		}
		zrdecplrec.amountIn.set(premiumrec.sumin);
		b000CallRounding();
		premiumrec.sumin.set(zrdecplrec.amountOut);
		sv.zrtopusum.set(premiumrec.sumin);
		compute(sv.zrtotsumin, 2).set(add(sv.sumins, sv.zrtopusum));
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 12900-EXIT.                                            */
		goTo(GotoLabel.exit12090);
	}

protected void calc12300()
	{
		premiumrec.function.set("TOPU");
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("01");
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaLifeassurSex);
		premiumrec.jlage.set(wsaaAnbAtCcd2);
		premiumrec.jlsex.set(wsaaSex2);
		premiumrec.effectdt.set(chdrpf.getOccdate());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor, .99999));
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.sumin.set(wsaaPrevSa);
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.instprem);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		premiumrec.plnsfx.set(covrpf.getPlanSuffix());
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation start
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) && er.isExternalized(premiumrec.cnttype.toString(), premiumrec.crtable.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			premiumrec.premMethod.set(wsaaPremMeth);
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.instpremErr.set(premiumrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 12900-EXIT                                          */
			return ;
		}
		zrdecplrec.amountIn.set(premiumrec.sumin);
		b000CallRounding();
		premiumrec.sumin.set(zrdecplrec.amountOut);
		sv.zrtopusum.set(premiumrec.sumin);
		compute(sv.zrtotsumin, 2).set(add(sv.sumins, sv.zrtopusum));
	}

	/**
	* <pre>
	*13000-PROCESS-COVR SECTION.                                      
	*13100-PROCESS-COVR.                                              
	**** CALL 'COVRIO'               USING COVR-PARAMS.               
	**** IF  COVR-STATUZ         NOT = ENDP                           
	**** AND COVR-STATUZ         NOT = O-K                            
	****     MOVE COVR-PARAMS        TO SYSR-PARAMS                   
	****     PERFORM 600-FATAL-ERROR                                  
	**** END-IF.                                                      
	**** IF COVR-CHDRCOY         NOT = COVRMJA-CHDRCOY                
	**** OR COVR-CHDRNUM         NOT = COVRMJA-CHDRNUM                
	**** OR COVR-LIFE            NOT = COVRMJA-LIFE                   
	**** OR COVR-COVERAGE        NOT = COVRMJA-COVERAGE               
	**** OR COVR-RIDER           NOT = COVR-RIDER                     
	**** OR COVR-PLAN-SUFFIX     NOT = COVR-PLAN-SUFFIX               
	**** OR COVR-STATUZ              = ENDP                           
	****    MOVE ENDP                TO COVR-STATUZ                   
	****    GO TO 13900-EXIT                                          
	**** END-IF.                                                      
	**** IF COVR-VALIDFLAG           = '2'                            
	****    MOVE NEXTR               TO COVR-FUNCTION                 
	****    GO TO 13900-EXIT                                          
	**** END-IF.                                                      
	**** IF COVR-TRANNO               =  WSAA-COVR-TRANNO             
	****    MOVE UPDAT               TO COVR-FUNCTION                 
	****    GO TO 13200-CALL-COVR                                     
	**** END-IF.                                                      
	**** MOVE '2'                    TO COVR-VALIDFLAG.               
	**** MOVE WSSP-EFFDATE           TO COVR-CURRTO.                  
	**** MOVE UPDAT                  TO COVR-FUNCTION.                
	**** MOVE COVRREC                TO COVR-FORMAT.                  
	**** CALL 'COVRIO'               USING COVR-PARAMS.               
	**** IF COVR-STATUZ          NOT = O-K                            
	****    MOVE COVR-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 600-FATAL-ERROR                                   
	**** END-IF.                                                      
	**** MOVE WRITR                  TO COVR-FUNCTION.                
	**** MOVE SN500-INSTPREM         TO WSAA-PREV-INSTPRM             
	****                                WSAA-UPDAT-INSTPRM.           
	*13200-CALL-COVR.                                                 
	**** MOVE '1'                    TO COVR-VALIDFLAG.               
	**** MOVE VRCM-MAX-DATE          TO COVR-CURRTO.                  
	**** MOVE SN500-ZRTOTSUMIN       TO COVR-SUMINS.                  
	**** MOVE WSAA-COVR-TRANNO       TO COVR-TRANNO.                  
	**** ADD WSAA-UPDAT-INSTPRM      TO COVR-SINGP.                   
	**** MOVE COVRREC                TO COVR-FORMAT.                  
	**** CALL 'COVRIO'               USING COVR-PARAMS.               
	**** IF COVR-STATUZ          NOT = O-K                            
	****    MOVE COVR-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 600-FATAL-ERROR                                   
	**** END-IF.                                                      
	**** MOVE ENDP                   TO COVR-STATUZ.                  
	*13900-EXIT.                                                      
	****  EXIT.                                                       
	* </pre>
	*/
protected void a500CalcTax()
	{
		a500Calc();
	}

protected void a500Calc()
	{
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set(wsaaTaxTranType);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.amountIn.set(wsaaTaxableAmt);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.cntcurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*B900-EXIT*/
	}

protected void writeReservePricing(){ 
	
			zutrpf.setChdrpfx(chdrpf.getChdrpfx().trim());/* IJTI-1523 */
			zutrpf.setChdrcoy(chdrpf.getChdrcoy().toString().trim());
			zutrpf.setChdrnum(sv.chdrnum.toString().trim());
			zutrpf.setLife(covrpf.getLife().trim());	/* IJTI-1523 */
			zutrpf.setCoverage(covrpf.getCoverage().trim());/* IJTI-1523 */
			zutrpf.setRider(covrpf.getRider().trim().trim());/* IJTI-1523 */
			zutrpf.setPlanSuffix(covrpf.getPlanSuffix());
			zutrpf.setTranno(chdrpf.getTranno());  
			zutrpf.setReserveUnitsInd(sv.reserveUnitsInd.toString().trim());
			zutrpf.setReserveUnitsDate(sv.reserveUnitsDate.toInt());
			zutrpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());	
			zutrpf.setTransactionDate(varcom.vrcmDate.toInt());
			zutrpf.setTransactionTime(varcom.vrcmTime.toInt());
			zutrpf.setUser(varcom.vrcmUser.toInt());
			zutrpf.setEffdate(sv.effdate.toInt());
			zutrpf.setValidflag("1");
			zutrpf.setDatesub(wsaaBusinessDate.toInt());
			zutrpf.setCrtuser(wsspcomn.userid.toString());  
			zutrpf.setUserProfile(wsspcomn.userid.toString()); 
			zutrpf.setJobName(appVars.getLoggedOnUser());
			zutrpf.setDatime("");			
  
			try{
				zutrpfDAO.insertZutrpfRecord(zutrpf);
			}catch(Exception e){
				LOGGER.error("Exception occured in writeReservePricing()",e);
				fatalError600();
			}	 

}

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e199 = new FixedLengthStringData(4).init("E199");
	private FixedLengthStringData e366 = new FixedLengthStringData(4).init("E366");
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData e706 = new FixedLengthStringData(4).init("E706");
	private FixedLengthStringData e707 = new FixedLengthStringData(4).init("E707");
	private FixedLengthStringData e859 = new FixedLengthStringData(4).init("E859");
	private FixedLengthStringData f348 = new FixedLengthStringData(4).init("F348");
	private FixedLengthStringData f351 = new FixedLengthStringData(4).init("F351");
	private FixedLengthStringData f982 = new FixedLengthStringData(4).init("F982");
	private FixedLengthStringData g037 = new FixedLengthStringData(4).init("G037");
	private FixedLengthStringData g103 = new FixedLengthStringData(4).init("G103");
	private FixedLengthStringData g105 = new FixedLengthStringData(4).init("G105");
	private FixedLengthStringData g106 = new FixedLengthStringData(4).init("G106");
	private FixedLengthStringData g347 = new FixedLengthStringData(4).init("G347");
	private FixedLengthStringData f025 = new FixedLengthStringData(4).init("F025");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData h023 = new FixedLengthStringData(4).init("H023");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData h456 = new FixedLengthStringData(4).init("H456");
	private FixedLengthStringData h960 = new FixedLengthStringData(4).init("H960");
	private FixedLengthStringData hl06 = new FixedLengthStringData(4).init("HL06");
	private FixedLengthStringData rrku = new FixedLengthStringData(4).init("RRKU");
	private FixedLengthStringData t090 = new FixedLengthStringData(4).init("T090");
	private FixedLengthStringData g029 = new FixedLengthStringData(4).init("G029");
	private FixedLengthStringData rf51 = new FixedLengthStringData(4).init("RF51");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186"); 
	private FixedLengthStringData rlcc = new FixedLengthStringData(4).init("RLCC"); 
	private FixedLengthStringData g099 = new FixedLengthStringData(4).init("G099");
	
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5510 = new FixedLengthStringData(5).init("T5510");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5543 = new FixedLengthStringData(5).init("T5543");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).init("T5551");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData pcddmjarec = new FixedLengthStringData(10).init("PCDDMJAREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC");
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
}
