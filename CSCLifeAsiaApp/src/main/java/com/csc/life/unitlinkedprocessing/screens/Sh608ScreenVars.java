package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH608
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh608ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(672);
	public FixedLengthStringData dataFields = new FixedLengthStringData(416).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData daywhs = new FixedLengthStringData(372).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] daywh = FLSArrayPartOfStructure(12, 31, daywhs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(372).isAPartOf(daywhs, 0, FILLER_REDEFINE);
	public FixedLengthStringData daywh01 = DD.dwh.copy().isAPartOf(filler,0);
	public FixedLengthStringData daywh02 = DD.dwh.copy().isAPartOf(filler,31);
	public FixedLengthStringData daywh03 = DD.dwh.copy().isAPartOf(filler,62);
	public FixedLengthStringData daywh04 = DD.dwh.copy().isAPartOf(filler,93);
	public FixedLengthStringData daywh05 = DD.dwh.copy().isAPartOf(filler,124);
	public FixedLengthStringData daywh06 = DD.dwh.copy().isAPartOf(filler,155);
	public FixedLengthStringData daywh07 = DD.dwh.copy().isAPartOf(filler,186);
	public FixedLengthStringData daywh08 = DD.dwh.copy().isAPartOf(filler,217);
	public FixedLengthStringData daywh09 = DD.dwh.copy().isAPartOf(filler,248);
	public FixedLengthStringData daywh10 = DD.dwh.copy().isAPartOf(filler,279);
	public FixedLengthStringData daywh11 = DD.dwh.copy().isAPartOf(filler,310);
	public FixedLengthStringData daywh12 = DD.dwh.copy().isAPartOf(filler,341);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,381);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,411);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 416);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData dwhsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] dwhErr = FLSArrayPartOfStructure(12, 4, dwhsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(48).isAPartOf(dwhsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData dwh01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData dwh02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData dwh03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData dwh04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData dwh05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData dwh06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData dwh07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData dwh08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData dwh09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData dwh10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData dwh11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData dwh12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 480);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData dwhsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] dwhOut = FLSArrayPartOfStructure(12, 12, dwhsOut, 0);
	public FixedLengthStringData[][] dwhO = FLSDArrayPartOfArrayStructure(12, 1, dwhOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(144).isAPartOf(dwhsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dwh01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] dwh02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] dwh03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] dwh04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] dwh05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] dwh06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] dwh07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] dwh08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] dwh09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] dwh10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] dwh11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] dwh12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh608screenWritten = new LongData(0);
	public LongData Sh608protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh608ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, daywh01, daywh02, daywh03, daywh04, daywh05, daywh06, daywh07, daywh08, daywh09, daywh10, daywh11, daywh12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, dwh01Out, dwh02Out, dwh03Out, dwh04Out, dwh05Out, dwh06Out, dwh07Out, dwh08Out, dwh09Out, dwh10Out, dwh11Out, dwh12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, dwh01Err, dwh02Err, dwh03Err, dwh04Err, dwh05Err, dwh06Err, dwh07Err, dwh08Err, dwh09Err, dwh10Err, dwh11Err, dwh12Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh608screen.class;
		protectRecord = Sh608protect.class;
	}

}
