package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:19
 * Description:
 * Copybook name: T5509REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5509rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5509Rec = new FixedLengthStringData(499);
  	public ZonedDecimalData acumbof = new ZonedDecimalData(8, 5).isAPartOf(t5509Rec, 0);
  	public ZonedDecimalData accumRounding = new ZonedDecimalData(3, 2).isAPartOf(t5509Rec, 8);
  	public ZonedDecimalData initBidOffer = new ZonedDecimalData(8, 5).isAPartOf(t5509Rec, 11);
  	public ZonedDecimalData initialRounding = new ZonedDecimalData(3, 2).isAPartOf(t5509Rec, 19);
  	public FixedLengthStringData initAccumSame = new FixedLengthStringData(1).isAPartOf(t5509Rec, 22);
  	public ZonedDecimalData managementCharge = new ZonedDecimalData(5, 2).isAPartOf(t5509Rec, 23);
  	public ZonedDecimalData tolerance = new ZonedDecimalData(4, 2).isAPartOf(t5509Rec, 28);
  	public FixedLengthStringData filler = new FixedLengthStringData(467).isAPartOf(t5509Rec, 32, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5509Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5509Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}