package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:31
 * Description:
 * Copybook name: COVTUNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtunlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtunlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtunlKey = new FixedLengthStringData(64).isAPartOf(covtunlFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtunlChdrcoy = new FixedLengthStringData(1).isAPartOf(covtunlKey, 0);
  	public FixedLengthStringData covtunlChdrnum = new FixedLengthStringData(8).isAPartOf(covtunlKey, 1);
  	public FixedLengthStringData covtunlLife = new FixedLengthStringData(2).isAPartOf(covtunlKey, 9);
  	public FixedLengthStringData covtunlCoverage = new FixedLengthStringData(2).isAPartOf(covtunlKey, 11);
  	public FixedLengthStringData covtunlRider = new FixedLengthStringData(2).isAPartOf(covtunlKey, 13);
  	public PackedDecimalData covtunlSeqnbr = new PackedDecimalData(3, 0).isAPartOf(covtunlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covtunlKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtunlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtunlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}