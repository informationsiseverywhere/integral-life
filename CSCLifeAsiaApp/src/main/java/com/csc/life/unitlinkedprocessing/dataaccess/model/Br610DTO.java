package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Br610DTO {

	private String company;// 1
	private String chdrnum;
	private int jctljnumst;
	private int ustmno;
	private long strpdate;
	private String ustmtflag;
	private String fdbkind;
	private int moniesdt;
	private int tranno;
	private String vrtfnd;
	private BigDecimal fundamnt;//
	private BigDecimal cntamnt;//
	private BigDecimal nofunts;//
	private BigDecimal nofdunts;//
	private int plnsfx;
	private String life;
	private String coverage;
	private String rider;
	private String unityp;
	private long ustn_Unique_number;
	private String ustfstmno;
	private String ustfChdrcoy;
	private String ustfChdrnum;
	private int ustfPlnsfx;
	private String ustfLife;
	private String ustfCoverage;
	private String ustfRider;
	private String ustfVrtfnd;
	private String ustfUnityp;
	private int stcldt;
	private BigDecimal stclun;
	private BigDecimal stcldun;
	private BigDecimal stclfuca;
	private BigDecimal stclcoca;
	private BigDecimal sttrfuca;
	private BigDecimal sttrcoca;
	private BigDecimal sttrdun;
	private BigDecimal sttrun;
	private BigDecimal stopfuca;
	private BigDecimal stopcoca;
	private BigDecimal stopun;
	private BigDecimal stopdun;
	private long ustf_Unique_number;
	private String cnttype;
	private BigDecimal polsum;
	private int occdate;
	private String cntcurr;
	private String billfreq;
	private String cownnum;
	private String cownpfx;
	private String agntcoy;
	private String agntnum;
	private long chdruls_Unique_number;
	private String lifcnum ;
	private String chdruls_chdrnum;
	private String chdruls_chdrcoy;


	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getJctljnumst() {
		return jctljnumst;
	}
	public void setJctljnumst(int jctljnumst) {
		this.jctljnumst = jctljnumst;
	}
	public int getUstmno() {
		return ustmno;
	}
	public String getUstfChdrcoy() {
		return ustfChdrcoy;
	}
	public void setUstfChdrcoy(String ustfChdrcoy) {
		this.ustfChdrcoy = ustfChdrcoy;
	}
	public String getUstfChdrnum() {
		return ustfChdrnum;
	}
	public void setUstfChdrnum(String ustfChdrnum) {
		this.ustfChdrnum = ustfChdrnum;
	}
	public int getUstfPlnsfx() {
		return ustfPlnsfx;
	}
	public void setUstfPlnsfx(int ustfPlnsfx) {
		this.ustfPlnsfx = ustfPlnsfx;
	}
	public String getUstfLife() {
		return ustfLife;
	}
	public void setUstfLife(String ustfLife) {
		this.ustfLife = ustfLife;
	}
	public String getUstfCoverage() {
		return ustfCoverage;
	}
	public void setUstfCoverage(String ustfCoverage) {
		this.ustfCoverage = ustfCoverage;
	}
	public String getUstfRider() {
		return ustfRider;
	}
	public void setUstfRider(String ustfRider) {
		this.ustfRider = ustfRider;
	}
	public String getUstfVrtfnd() {
		return ustfVrtfnd;
	}
	public void setUstfVrtfnd(String ustfVrtfnd) {
		this.ustfVrtfnd = ustfVrtfnd;
	}
	public String getUstfUnityp() {
		return ustfUnityp;
	}
	public void setUstfUnityp(String ustfUnityp) {
		this.ustfUnityp = ustfUnityp;
	}
	public void setUstmno(int ustmno) {
		this.ustmno = ustmno;
	}
	public long getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(long strpdate) {
		this.strpdate = strpdate;
	}
	public String getUstmtflag() {
		return ustmtflag;
	}
	public void setUstmtflag(String ustmtflag) {
		this.ustmtflag = ustmtflag;
	}
	public String getFdbkind() {
		return fdbkind;
	}
	public void setFdbkind(String fdbkind) {
		this.fdbkind = fdbkind;
	}
	public int getMoniesdt() {
		return moniesdt;
	}
	public void setMoniesdt(int moniesdt) {
		this.moniesdt = moniesdt;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getVrtfnd() {
		return vrtfnd;
	}
	public void setVrtfnd(String vrtfnd) {
		this.vrtfnd = vrtfnd;
	}
	public BigDecimal getFundamnt() {
		return fundamnt;
	}
	public void setFundamnt(BigDecimal fundamnt) {
		this.fundamnt = fundamnt;
	}
	public BigDecimal getCntamnt() {
		return cntamnt;
	}
	public void setCntamnt(BigDecimal cntamnt) {
		this.cntamnt = cntamnt;
	}
	public BigDecimal getNofunts() {
		return nofunts;
	}
	public void setNofunts(BigDecimal nofunts) {
		this.nofunts = nofunts;
	}
	public BigDecimal getNofdunts() {
		return nofdunts;
	}
	public void setNofdunts(BigDecimal nofdunts) {
		this.nofdunts = nofdunts;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getUnityp() {
		return unityp;
	}
	public void setUnityp(String unityp) {
		this.unityp = unityp;
	}

	public String getUstfstmno() {
		return ustfstmno;
	}
	public void setUstfstmno(String ustfstmno) {
		this.ustfstmno = ustfstmno;
	}

	public long getUstn_Unique_number() {
		return ustn_Unique_number;
	}
	public void setUstn_Unique_number(long ustn_Unique_number) {
		this.ustn_Unique_number = ustn_Unique_number;
	}
	public int getStcldt() {
		return stcldt;
	}
	public void setStcldt(int stcldt) {
		this.stcldt = stcldt;
	}
	public BigDecimal getStclun() {
		return stclun;
	}
	public void setStclun(BigDecimal stclun) {
		this.stclun = stclun;
	}
	public BigDecimal getStcldun() {
		return stcldun;
	}
	public void setStcldun(BigDecimal stcldun) {
		this.stcldun = stcldun;
	}
	public BigDecimal getStclfuca() {
		return stclfuca;
	}
	public void setStclfuca(BigDecimal stclfuca) {
		this.stclfuca = stclfuca;
	}
	public BigDecimal getStclcoca() {
		return stclcoca;
	}
	public void setStclcoca(BigDecimal stclcoca) {
		this.stclcoca = stclcoca;
	}
	public BigDecimal getSttrfuca() {
		return sttrfuca;
	}
	public void setSttrfuca(BigDecimal sttrfuca) {
		this.sttrfuca = sttrfuca;
	}
	public BigDecimal getSttrcoca() {
		return sttrcoca;
	}
	public void setSttrcoca(BigDecimal sttrcoca) {
		this.sttrcoca = sttrcoca;
	}
	public BigDecimal getSttrdun() {
		return sttrdun;
	}
	public void setSttrdun(BigDecimal sttrdun) {
		this.sttrdun = sttrdun;
	}
	public BigDecimal getSttrun() {
		return sttrun;
	}
	public void setSttrun(BigDecimal sttrun) {
		this.sttrun = sttrun;
	}
	public BigDecimal getStopfuca() {
		return stopfuca;
	}
	public void setStopfuca(BigDecimal stopfuca) {
		this.stopfuca = stopfuca;
	}
	public BigDecimal getStopcoca() {
		return stopcoca;
	}
	public void setStopcoca(BigDecimal stopcoca) {
		this.stopcoca = stopcoca;
	}
	public BigDecimal getStopun() {
		return stopun;
	}
	public void setStopun(BigDecimal stopun) {
		this.stopun = stopun;
	}
	public BigDecimal getStopdun() {
		return stopdun;
	}
	public void setStopdun(BigDecimal stopdun) {
		this.stopdun = stopdun;
	}
	public long getUstf_Unique_number() {
		return ustf_Unique_number;
	}
	public void setUstf_Unique_number(long ustf_Unique_number) {
		this.ustf_Unique_number = ustf_Unique_number;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public BigDecimal getPolsum() {
		return polsum;
	}
	public void setPolsum(BigDecimal polsum) {
		this.polsum = polsum;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public long getChdruls_Unique_number() {
		return chdruls_Unique_number;
	}
	public void setChdruls_Unique_number(long chdruls_Unique_number) {
		this.chdruls_Unique_number = chdruls_Unique_number;
	}
	public String getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}
	public String getChdruls_chdrnum() {
		return chdruls_chdrnum;
	}
	public void setChdruls_chdrnum(String chdruls_chdrnum) {
		this.chdruls_chdrnum = chdruls_chdrnum;
	}
	public String getChdruls_chdrcoy() {
		return chdruls_chdrcoy;
	}
	public void setChdruls_chdrcoy(String chdruls_chdrcoy) {
		this.chdruls_chdrcoy = chdruls_chdrcoy;
	}
}
