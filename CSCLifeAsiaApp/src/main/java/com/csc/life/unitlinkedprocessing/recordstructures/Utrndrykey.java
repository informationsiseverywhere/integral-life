package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class Utrndrykey extends ExternalData {

	public FixedLengthStringData wskyUtrndryFileKey = new FixedLengthStringData(256);
	public FixedLengthStringData wskyUtrndryKey = new FixedLengthStringData(256).isAPartOf(wskyUtrndryFileKey, 0, REDEFINE);

	public FixedLengthStringData wskyUtrndryChdrcoy = new FixedLengthStringData(1).isAPartOf(wskyUtrndryKey, 0);

	public FixedLengthStringData wskyUtrndryChdrnum = new FixedLengthStringData(8).isAPartOf(wskyUtrndryKey, 1);
	public PackedDecimalData wskyUtrndryMoniesDate  = new PackedDecimalData(8, 0).isAPartOf(wskyUtrndryKey, 9);
	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(wskyUtrndryKey, 13, FILLER);//ILIFE-1953


	public void initialize() {
		COBOLFunctions.initialize(wskyUtrndryFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			wskyUtrndryFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}

