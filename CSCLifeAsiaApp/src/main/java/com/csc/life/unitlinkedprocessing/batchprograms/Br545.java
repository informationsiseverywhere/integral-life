/*
 * File: Br545.java
 * Date: 29 August 2009 22:19:09
 * Author: Quipoz Limited
 * 
 * Class transformed from BR545.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.dataaccess.AdocsciTableDAM;
import com.csc.fsu.general.dataaccess.RdocTableDAM;
import com.csc.fsu.general.procedures.Getfsu;
import com.csc.fsu.general.recordstructures.Getfsurec;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.unitlinkedprocessing.reports.Rr545Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Spcoutrec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrncpy;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This program prints the 'Company Suspense'
*
*   The records are selected from ACMV file and RTRN file when
*   1. Sub-account code (SACSCODE) = 'SC'
*   2. Sub-account type (SACSTYP) = 'I'
*   3. Sub-account = spaces
*
*   The records are sorted on Receipt no. and transaction
*   date.
*
*
*   Control totals:
*     01  -  Number of records selected
*     02  -  Number of transaction printed
*
*****************************************************************
* </pre>
*/
public class Br545 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr545Report printerFile = new Rr545Report();
	private SortFileDAM sortSuspFile = new SortFileDAM("LU00");
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);

	private FixedLengthStringData sortSuspRec = new FixedLengthStringData(331);
	private FixedLengthStringData sortSortKey = new FixedLengthStringData(26).isAPartOf(sortSuspRec, 0);
	private FixedLengthStringData sortDataArea = new FixedLengthStringData(305).isAPartOf(sortSuspRec, 26);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR545");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDate1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDate1Yy = new FixedLengthStringData(4).isAPartOf(wsaaDate1, 0);
	private FixedLengthStringData wsaaDate1Mm = new FixedLengthStringData(2).isAPartOf(wsaaDate1, 4);
	private FixedLengthStringData wsaaDate1Dd = new FixedLengthStringData(2).isAPartOf(wsaaDate1, 6);

	private FixedLengthStringData wsaaDate2 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaDate2Dd = new FixedLengthStringData(2).isAPartOf(wsaaDate2, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaDate2, 2, FILLER).init("/");
	private FixedLengthStringData wsaaDate2Mm = new FixedLengthStringData(2).isAPartOf(wsaaDate2, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaDate2, 5, FILLER).init("/");
	private FixedLengthStringData wsaaDate2Yy = new FixedLengthStringData(4).isAPartOf(wsaaDate2, 6);
	private ZonedDecimalData wsaaDate3 = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaValidCompany = new FixedLengthStringData(1);
	private Validator validCompany = new Validator(wsaaValidCompany, "Y");
	private BinaryData ix = new BinaryData(2, 0).init(0);
	private FixedLengthStringData wsaaLastBranch = new FixedLengthStringData(2).init(SPACES);
	private static final int wsaaCnameLen = 47;
	private static final String wsaaPayeeGivn = "PYNMN";
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final String descrec = "DESCREC";
	private static final String clntrec = "CLNTREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData wsaaSortKey = new FixedLengthStringData(27);
	private FixedLengthStringData wsaaSortBranch = new FixedLengthStringData(2).isAPartOf(wsaaSortKey, 0);
	private FixedLengthStringData wsaaSortRdocnum = new FixedLengthStringData(9).isAPartOf(wsaaSortKey, 2);
	private PackedDecimalData wsaaSortTrandate = new PackedDecimalData(8, 0).isAPartOf(wsaaSortKey, 11);

	private FixedLengthStringData wsaaSortData = new FixedLengthStringData(39);
	private FixedLengthStringData wsaaSortRdoccoy = new FixedLengthStringData(1).isAPartOf(wsaaSortData, 0);
	private FixedLengthStringData wsaaSortRldgpfx = new FixedLengthStringData(9).isAPartOf(wsaaSortData, 1);
	private FixedLengthStringData wsaaSortRldgcoy = new FixedLengthStringData(2).isAPartOf(wsaaSortData, 10);
	private FixedLengthStringData wsaaSortRldgacct = new FixedLengthStringData(16).isAPartOf(wsaaSortData, 12);
	private FixedLengthStringData wsaaSortOrccy = new FixedLengthStringData(3).isAPartOf(wsaaSortData, 28);
	private PackedDecimalData wsaaSortOrigamt = new PackedDecimalData(14, 3).isAPartOf(wsaaSortData, 31);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEofSort = new FixedLengthStringData(1).init("N");
	private Validator eofSort = new Validator(wsaaEofSort, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rr545H01 = new FixedLengthStringData(73);
	private FixedLengthStringData rr545h01O = new FixedLengthStringData(73).isAPartOf(rr545H01, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr545h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr545h01O, 1);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr545h01O, 31);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr545h01O, 41);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr545h01O, 43);

	private FixedLengthStringData rr545D01 = new FixedLengthStringData(86);
	private FixedLengthStringData rr545d01O = new FixedLengthStringData(86).isAPartOf(rr545D01, 0);
	private FixedLengthStringData rd01Rdocnum = new FixedLengthStringData(9).isAPartOf(rr545d01O, 0);
	private FixedLengthStringData rd01Date = new FixedLengthStringData(10).isAPartOf(rr545d01O, 9);
	private FixedLengthStringData rd01Cname = new FixedLengthStringData(47).isAPartOf(rr545d01O, 19);
	private FixedLengthStringData rd01Orccy = new FixedLengthStringData(3).isAPartOf(rr545d01O, 66);
	private ZonedDecimalData rd01Origamt = new ZonedDecimalData(17, 2).isAPartOf(rr545d01O, 69);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AdocsciTableDAM adocsciIO = new AdocsciTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private RdocTableDAM rdocIO = new RdocTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Getfsurec getfsurec = new Getfsurec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Spcoutrec spcoutrec = new Spcoutrec();
	private Dbcstrncpy dbcstrncpy = new Dbcstrncpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2399, 
		returnNext2601, 
		exit2699, 
		checkCoyLoop2750, 
		exit2799
	}

	public Br545() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		/* IF BSSC-LANGUAGE             = 'E'                           */
		indOn.setTrue(21);
	}

	/**
	* <pre>
	**** END-IF.                                                      
	* </pre>
	*/
protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		/* MOVE BPRD-DEFAULT-BRANCH    TO DESC-DESCITEM.                */
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		/* MOVE BPRD-DEFAULT-BRANCH    TO RH01-BRANCH.                  */
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaOverflow.set("Y");
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		wsspEdterror.set(varcom.oK);
		sortSuspFile.openOutput();
		xtractTransactions2100();
		sortSuspFile.close();
		FileSort fs1 = new FileSort(sortSuspFile);
		fs1.addSortKey(sortSortKey, true);
		fs1.sort();
		sortSuspFile.openInput();
		printReport2600();
		sortSuspFile.close();
		/*EXIT*/
	}

protected void xtractTransactions2100()
	{
		/*SELECT-RECORDS*/
		/* MOVE SPACES                 TO ADOCSCI-DATA-KEY.             */
		adocsciIO.setParams(SPACES);
		adocsciIO.setAdocRtrnJrnseq(ZERO);
		adocsciIO.setFunction(varcom.begn);
		adocsciIO.setFormat(SPACES);
		while ( !(isEQ(adocsciIO.getStatuz(), varcom.endp))) {
			callAdocnxt2200();
		}
		
		/*EXIT*/
	}

protected void callAdocnxt2200()
	{
		start2201();
	}

protected void start2201()
	{
		SmartFileCode.execute(appVars, adocsciIO);
		if (isNE(adocsciIO.getStatuz(), varcom.oK)
		&& isNE(adocsciIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(adocsciIO.getParams());
			syserrrec.statuz.set(adocsciIO.getStatuz());
			fatalError600();
		}
		if (isEQ(adocsciIO.getStatuz(), varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			adocsciIO.setStatuz(varcom.endp);
			return ;
		}
		setupSortRecord2300();
		adocsciIO.setFunction(varcom.nextr);
	}

protected void setupSortRecord2300()
	{
		try {
			start2301();
			releaseToSort2350();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start2301()
	{
		rdocIO.setDataKey(SPACES);
		rdocIO.setRdocpfx("CA");
		rdocIO.setRdoccoy(adocsciIO.getAdocRtrnRdoccoy());
		rdocIO.setRdocnum(adocsciIO.getAdocRtrnRdocnum());
		rdocIO.setTranseq("0000");
		rdocIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, rdocIO);
		if (isNE(rdocIO.getStatuz(), varcom.oK)
		&& isNE(rdocIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(rdocIO.getParams());
			syserrrec.statuz.set(rdocIO.getStatuz());
			fatalError600();
		}
		if (isEQ(rdocIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit2399);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void releaseToSort2350()
	{
		wsaaSortRdoccoy.set(adocsciIO.getAdocRtrnRdoccoy());
		wsaaSortRdocnum.set(adocsciIO.getAdocRtrnRdocnum());
		wsaaSortOrccy.set(adocsciIO.getAdocRtrnOrigccy());
		wsaaSortOrigamt.set(adocsciIO.getAdocRtrnOrigamt());
		wsaaSortTrandate.set(adocsciIO.getAdocRtrnTrandate());
		wsaaSortRldgpfx.set(rdocIO.getRldgpfx());
		wsaaSortRldgcoy.set(rdocIO.getRldgcoy());
		wsaaSortRldgacct.set(rdocIO.getRldgacct());
		wsaaSortBranch.set(rdocIO.getBatcbrn());
		sortSortKey.set(wsaaSortKey);
		sortDataArea.set(wsaaSortData);
		sortSuspFile.write(sortSuspRec);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void printReport2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case returnNext2601: 
					returnNext2601();
					processSortRecord2650();
				case exit2699: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void returnNext2601()
	{
		sortSuspFile.read(sortSuspRec);
		if (sortSuspFile.isAtEnd()) {
			wsaaEofSort.set("Y");
			goTo(GotoLabel.exit2699);
		}
		wsaaSortData.set(sortDataArea);
		wsaaSortKey.set(sortSortKey);
	}

protected void processSortRecord2650()
	{
		validateCompany2700();
		if (!validCompany.isTrue()) {
			return ;
		}
		rd01Rdocnum.set(wsaaSortRdocnum);
		wsaaDate3.set(wsaaSortTrandate);
		wsaaDate1.set(wsaaDate3);
		wsaaDate2Dd.set(wsaaDate1Dd);
		wsaaDate2Mm.set(wsaaDate1Mm);
		wsaaDate2Yy.set(wsaaDate1Yy);
		rd01Date.set(wsaaDate2);
		rd01Orccy.set(wsaaSortOrccy);
		rd01Origamt.set(wsaaSortOrigamt);
		//snayeni modified for ILIFE-3040 start
		namadrsrec.namadrsRec.set(SPACES);
		//snayeni modified for ILIFE-3040 End
		getClntname2800();
		/*  MOVE SPCO-FIELD             TO RD01-CNAME.                   */
		dbcstrncpy.dbcsInputString.set(namadrsrec.name);
		dbcstrncpy.dbcsOutputLength.set(wsaaCnameLen);
		dbcstrncpy.dbcsStatuz.set(SPACES);
		dbcsTrnc(dbcstrncpy.rec);
		if (isNE(dbcstrncpy.dbcsStatuz, "****")) {
			dbcstrncpy.dbcsOutputString.set(SPACES);
		}
		rd01Cname.set(dbcstrncpy.dbcsOutputString);
		writeDetails3200();
		goTo(GotoLabel.returnNext2601);
	}

protected void validateCompany2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2701();
				case checkCoyLoop2750: 
					checkCoyLoop2750();
				case exit2799: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2701()
	{
		if (isEQ(wsaaSortRdoccoy, bsprIO.getCompany())) {
			wsaaValidCompany.set("Y");
			goTo(GotoLabel.exit2799);
		}
		getAuthorisedCoys2900();
		if (isEQ(wsaaSortRdoccoy, getfsurec.fsuco)) {
			wsaaValidCompany.set("Y");
			goTo(GotoLabel.exit2799);
		}
		/*  If not an FSU company ,  not allowed to inquire on*/
		/*  other subordinate companies*/
		if (isNE(bsprIO.getCompany(), getfsurec.fsuco)) {
			wsaaValidCompany.set("N");
			goTo(GotoLabel.exit2799);
		}
		ix.set(getfsurec.noOfSub);
	}

protected void checkCoyLoop2750()
	{
		if (isEQ(wsaaSortRdoccoy, getfsurec.suborcos[ix.toInt()])) {
			wsaaValidCompany.set("Y");
			return ;
		}
		ix.subtract(1);
		if (isGT(ix, 0)) {
			goTo(GotoLabel.checkCoyLoop2750);
		}
		wsaaValidCompany.set("N");
	}

protected void getClntname2800()
	{
		start2801();
	}

protected void start2801()
	{
	
		clntIO.setClntpfx(wsaaSortRldgpfx);
		clntIO.setClntcoy(wsaaSortRldgcoy);
		clntIO.setClntnum(wsaaSortRldgacct);
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)
		&& isNE(clntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clntIO.getParams());
			syserrrec.statuz.set(clntIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clntIO.getStatuz(), varcom.mrnf)
		|| isNE(clntIO.getValidflag(), 1)) {
			spcoutrec.field.fill("?");
			return ;
		}
	   //namadrsrec.namadrsRec.set(SPACES); commented in this section and placed in 509 line for ILIFE-3040 
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.clntNumber.set(clntIO.getClntnum());
		namadrsrec.clntPrefix.set(clntIO.getClntpfx());
		namadrsrec.clntCompany.set(clntIO.getClntcoy());
		namadrsrec.function.set(wsaaPayeeGivn);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
	}

protected void getAuthorisedCoys2900()
	{
		/*START*/
		getfsurec.function.set("GETALLCO");
		getfsurec.company.set(bsprIO.getCompany());
		callProgram(Getfsu.class, getfsurec.getfsuRec);
		if (isNE(getfsurec.statuz, varcom.oK)) {
			syserrrec.params.set(getfsurec.getfsuRec);
			syserrrec.statuz.set(getfsurec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/** Update database records.*/
		/*WRITE-DETAIL*/
		/** If first page/overflow - write standard headings*/
		/*EXIT*/
	}

protected void writeHeader3100()
	{
		start3101();
		print3150();
	}

protected void start3101()
	{
		if (isNE(wsaaSortBranch, wsaaLastBranch)) {
			wsaaLastBranch.set(wsaaSortBranch);
		}
		else {
			return ;
		}
		rh01Branch.set(wsaaLastBranch);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaLastBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
	}

protected void print3150()
	{
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			rh01Branchnm.fill("?");
		}
		else {
			rh01Branchnm.set(descIO.getLongdesc());
		}
		printerFile.printRr545h01(rr545H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void writeDetails3200()
	{
		start3201();
	}

protected void start3201()
	{
		if (newPageReq.isTrue()) {
			writeHeader3100();
			wsaaOverflow.set("N");
		}
		if (isNE(wsaaSortBranch, wsaaLastBranch)) {
			writeHeader3100();
			wsaaOverflow.set("N");
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		printerFile.printRr545d01(rr545D01, indicArea);
	}

protected void writeEndMessage3300()
	{
		/*START*/
		printerFile.printRr545h02(printerRec, indicArea);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		finished9000();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void finished9000()
	{
		/*CLOSE-FILES*/
		/* Write out the header even if no data found                      */
		if (newPageReq.isTrue()) {
			writeHeader3100();
			wsaaOverflow.set("N");
		}
		writeEndMessage3300();
		printerFile.close();
		/*EXIT*/
	}
}
