package com.csc.life.unitlinkedprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UtrnpfDAO extends BaseDAO<Utrnpf> {
	public List<Utrnpf> searchUtrnRecord(Utrnpf utrnpfData);
	List<Utrnpf> readUtrnpfForUnitDealProcess(Utrnpf utrnpf);

	// ILIFE-4187
	public Map<String, List<Utrnpf>> checkUtrnRecordByChdrnum(List<String> chdrnumList);
	public Map<String, List<Utrnpf>> searchUtrnRecordByFdbkind(List<String> chdrnumList);
	public void updateUtrnpfRecord(List<Utrnpf> urList);

	public Map<String, List<Utrnpf>> searchUtrnuddRecord(List<String> chdrnumList);
	public Map<String, List<Utrnpf>> searchUtrnrevRecord(List<String> chdrnumList);
	public void deleteUtrnrevRecord(List<Utrnpf> utrnList);
	public void insertUtrnRecoed(List<Utrnpf> utrnBulkOpList);

	List<Utrnpf> readUtrnpf(Utrnpf utrnpf);
	public List<String> checkUtrnrnlRecordByChdrnum(String chdrcoy, List<String> chdrnumList);
	public int getUnprocessedUtrnCount(String chdrCoy, String chdrNum ); //ILIFE-4559: Life performance tuning

	public List<Utrnpf> sqlSwch1400ChunkRead(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String swchind, String chdrnumFrom, String chdrnumTo);
	public List<Utrnpf> sqlDebt1300ChunkRead(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String covdbtind, String chdrnumFrom, String chdrnumTo);
	public List<Utrnpf> sqlDeal1200ChunkRead(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String chdrnumFrom, String chdrnumTo);
	public Utrnpf findUtrnpf(Utrnpf utrnpf);

	public List<Utrnpf> searchUtrnextRecord(String chdrcoy, String chdrnum);

	public List<Utrnpf> searchUtrnRecord(String chdrcoy, String chdrnum);
	public void insertUstnRecord(List<Utrnpf> ustnUpdateList) ;
	public List<Utrnpf> searchUtrnpfRecord(Utrnpf utrnpf); //ILIFE-8786
	public void updatedUtrnpfRecord(List<Utrnpf> utrnList); //ILIFE-8786
	public void deleteUtrnRecord(List<Utrnpf> utrnList); //ILIFE-8786
	public Map<String, List<Utrnpf>> searchUstsByChdrnum(List<String> chdrnumList, String chdrcoy);
	public List<Utrnpf> sqlDebt1300ChunkReadUtrnZfmcPayr(String tableId, int batchExtractSize, int batchID, String chdrcoy, String fdbkind, String covdbtind, String chdrnumFrom, String chdrnumTo, int wsaaNextdate);
	public List<Utrnpf> searchUtrnRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider, int tranno);
}