/*
 * File: P6352.java
 * Date: 30 August 2009 0:44:45
 * Author: Quipoz Limited
 * 
 * Class transformed from P6352.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
//import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtbrkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UjnlmjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.screens.S6352ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
//ILIFE-8137
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*
*                    UNIT JOURNALS.
*                    ==============
*
*Unit Journals will enable the input of information to change the
*units under a contract's fund and type.
*
*The Journals will  create  UTRN records  which will be processed
*through  the  Batch UNITDEAL   run, to  buy  and  sell  units as
*appropriate within a component's funds.
*
*Initialise
*----------
*
*  - Clear the subfile ready for loading.
*
*  - The details of the  contract  being  enquired  upon will be
*  stored in the CHDRMJA  I/O  module.  Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  - The details of the  component  to  which  the  journal will
*  relate  are  stored  in  the  COVR  I/O module.  Retrieve the
*  details and complete the header portion of the screen.
*
*  - Load the subfile as follows:
*
*       Load any existing Unit Journal  up  to  a maximum of one
*       full page. For each record, add to the subfile and set a
*       flag in a 'hidden' field to indicate that the record was
*       loaded from the database (required during update).
*
*       If the first page is  not  full, add the number of blank
*       records  required  to complete  a  full  page.  Set  the
*       subfile more indicator to  'Y'.  Set  a  flag to signify
*       first page.
*
*
*Validation
*----------
*
*  If  in  enquiry  mode   (WSSP-FLAG   =   'J')  move  PROT  to
*  SCRN-FUNCTION prior to output.
*
*  After screen display;
*
*  - If SCRN-STATUZ = KILL  or  in enquiry  mode,  exit  to 3000
*       section.
*
*  - If SCRN-STATUZ =  CALC  redisplay  the  screen, display the
*       summary details for each subfile record.
*
*  - If SCRN-STATUZ = ROLD  and  First  page  flag  set ON, then
*       error with code F498 and redisplay screen.
*    Else set working storage flag to signify ROLD.
*
*  - If SCRN-STATUZ = ROLU  and no more Unit journals to display
*       then error with code F499 and redisplay.
*    Else set working storage flag to signify ROLU.
*
*  - Sequentially  read through the subflie and validate the
*    subfile record.
*
*       For each altered record;
*
*       - Check if all the fields for a record have been blanked
*       out, i.e. Deleted. Move 'D' to hidden field.
*
*       - Check if any fields for a new record have been blanked
*       out. Move 'X' to hidden field.
*
*       - Check  if any  fields  have  changed  on  an  existing
*       Journal record. Move 'U' to hidden field.
*
*       - If this is a new entry (hidden field = space) move 'A'
*       to hidden field.
*
*       - Validate Fund and Unit type against table T5515.
*
*       -  If  a valid  fund,  check  against  T5540  for  Funds
*       acceptable  against  a   coverage   or  rider  type.
*
*            If T5540-fund-split-plan is  equal  to spaces, then
*                 any valid fund  can  be  used.
*
*            If T5540-fund-split-plan is  NOT  equal  to spaces,
*                 then read T5543. If Field not found then fatal
*                 error else if  O-K  and  Fund  is not included
*                 within the table list, then error.
*
*       - Check the Fund and Unit Type against the Unit  Summary
*         record.  If no Unit Summary record exists for the fund
*         being   journalled  to  or  from,  display  an  error.
*         Otherwise,  move  the  Unit  Summary  holdings  to the
*         screen.
*
*Updating
*--------
*
*  All  existing Unit journal records must be updated or deleted
*  and  any  new  records  added. Updating will provide distinct
*  UTRN records for UNITDEAL processing.
*
*  - If SCRN-STATUZ = KILL  or  in  enquiry mode,  exit  to 4000
*  section.
*
*  We must update the records for a page at a time. If there are
*  several  pages  they must request rolldown or rollup in order
*  to  key  the  data. This will be handled in the 4000 section,
*  which  will  display the next screen after having updated the
*  previous subfile records. Therefore
*
*       - Perform a  section  to  action the record according to
*       the hidden field for the  whole  of  the  subfile  page,
*       increasing  the  relative   record  number  sequentially
*       through the subfile records.
*
*       - Use this to read each record directly.
*
*       - On each record, look at the hidden action field.
*
*            * If the hidden field is equal to spaces or 'X' or
*                 'N', go on to the next record.
*
*            * If the hidden  field is equal  to 'D', delete the
*                 Unit journal record.
*
*            * If the hidden  field is  equal to 'U', update the
*                 existing Unit  Journal  record with the screen
*                 fields.
*
*            * If the hidden field  is  equal  to  'A', move the
*                 screen  fields to the Unit  Journal record and
*                 write a new Unit Journal record.
*
*            * If a component policy has been selected which is
*                 within a summarised record, write a temporary
*                 broken-out component record which will be
*                 transformed into a permanent component record
*                 within the AT program.
*
*Next Program
*------------
*
*ROLLUP:
*
*  If the Rollup flag is set  ON,  then set first page flag OFF.
*  Provided  that the Unit  Journal component key is the same as
*  the Coverage  component  key and that we have not reached the
*  end of the Unit Journal file, load the next screen.
*
*ROLLDOWN:
*
*  If the Rolldown flag  is  set ON, then move the stored record
*  key from the last screen  to  the  Unit Journal key and begin
*  reading the file. If this is sucessful, read the Unit Journal
*  file backwards until the screen has been filled.
*
*  However, if we reach the end of the Unit Journal file or pick
*  up a Unit  Journal record for another  component or contract,
*  load the first page.
*
*NEXT:
*
*  If the Rollup or  Rolldown flags are set ON, switch them off,
*  reset the screen and exit.
*
*  Otherwise,
*
*      - Increment the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P6352 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6352");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private String wsaaNoMoreJrnls = "";
	private PackedDecimalData wsaaJournal = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaRrn = new PackedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaT5671Found = "N";
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLine = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPageNo = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaConcatT6647key = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaSeqnbr = new PackedDecimalData(3, 0).setUnsigned();

		/* WSBB-STACK-ARRAY */
	private FixedLengthStringData wsbbVrtfndArray = new FixedLengthStringData(48);
	private FixedLengthStringData[] wsbbVrtfnd = FLSArrayPartOfStructure(12, 4, wsbbVrtfndArray, 0);

	private FixedLengthStringData wsbbUnitypArray = new FixedLengthStringData(12);
	private FixedLengthStringData[] wsbbUnityp = FLSArrayPartOfStructure(12, 1, wsbbUnitypArray, 0);

	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(12);
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(12, 1, wsbbChangeArray, 0);

	private FixedLengthStringData wsaaFundCurrArray = new FixedLengthStringData(70);
	private FixedLengthStringData[] wsaaFundCurrRec = FLSArrayPartOfStructure(10, 7, wsaaFundCurrArray, 0);
	private FixedLengthStringData[] wsaaFund = FLSDArrayPartOfArrayStructure(4, wsaaFundCurrRec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaFundCurr = FLSDArrayPartOfArrayStructure(3, wsaaFundCurrRec, 4);

	private FixedLengthStringData wsaaFirstJrnlKey = new FixedLengthStringData(69);
	private FixedLengthStringData wsaaJrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstJrnlKey, 0);
	private FixedLengthStringData wsaaJrnlChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstJrnlKey, 1);
	private FixedLengthStringData wsaaJrnlLife = new FixedLengthStringData(2).isAPartOf(wsaaFirstJrnlKey, 9);
	private FixedLengthStringData wsaaJrnlCoverage = new FixedLengthStringData(2).isAPartOf(wsaaFirstJrnlKey, 11);
	private FixedLengthStringData wsaaJrnlRider = new FixedLengthStringData(2).isAPartOf(wsaaFirstJrnlKey, 13);
	private PackedDecimalData wsaaJrnlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaFirstJrnlKey, 15);
	private FixedLengthStringData wsaaJrnlVrtfnd = new FixedLengthStringData(4).isAPartOf(wsaaFirstJrnlKey, 18);
	private FixedLengthStringData wsaaJrnlUnityp = new FixedLengthStringData(1).isAPartOf(wsaaFirstJrnlKey, 22);
	private PackedDecimalData wsaaJrnlTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaFirstJrnlKey, 23);
	private FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(wsaaFirstJrnlKey, 26, FILLER).init(SPACES);

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private FixedLengthStringData filler1 = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);

		/* WSAA-ROLL-FLGS */
	private FixedLengthStringData wsaaRolu = new FixedLengthStringData(1);
	private Validator rollup = new Validator(wsaaRolu, "Y");

	private FixedLengthStringData wsaaRold = new FixedLengthStringData(1);
	private Validator rolldown = new Validator(wsaaRold, "Y");

	private FixedLengthStringData wsaaSearchFlag = new FixedLengthStringData(1);
	private Validator wsaaSearchExit = new Validator(wsaaSearchFlag, "Y", "N");
	private Validator wsaaNoFundFound = new Validator(wsaaSearchFlag, "N");

	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevel = new Validator(wsaaPlanPolicyFlag, "Y");
	private Validator policyLevel = new Validator(wsaaPlanPolicyFlag, "N");

	private FixedLengthStringData wsaaNoUlnkFlag = new FixedLengthStringData(1);
	private Validator notUnitLinked = new Validator(wsaaNoUlnkFlag, "Y");

	private FixedLengthStringData wsaaCalcOnFlag = new FixedLengthStringData(1);
	private Validator cf9Pressed = new Validator(wsaaCalcOnFlag, "Y");

	private FixedLengthStringData wsaaBreakoutProcessed = new FixedLengthStringData(1);
	private Validator covtbrkWritten = new Validator(wsaaBreakoutProcessed, "Y");
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Part1 = new FixedLengthStringData(5).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Part2 = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);
		/* ERRORS */
	private String f025 = "F025";
	private String f498 = "F498";
	private String e304 = "E304";
	private String g144 = "G144";
	private String g282 = "G282";
	private String g695 = "G695";
	private String g230 = "G230";
	private String e308 = "E308";
	private String g255 = "G255";
	private String h264 = "H264";
	private String h265 = "H265";
	private String h266 = "H266";
	private String h972 = "H972";
		/* TABLES */
	private String t5515 = "T5515";
	private String t5540 = "T5540";
	private String t5543 = "T5543";
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t6647 = "T6647";
	private String t5688 = "T5688";
	private String t5551 = "T5551";
	private String t5671 = "T5671";
	private String covrmjarec = "COVRMJAREC";
	private String covtbrkrec = "COVTBRKREC";
	private String ujnlmjarec = "UJNLMJAREC";
	private String utrsrec = "UTRSREC   ";
	private String itdmrec = "ITEMREC";
	private IntegerData wsaaFundIx = new IntegerData();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage temporary Breakout file*/
	private CovtbrkTableDAM covtbrkIO = new CovtbrkTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T5540rec t5540rec = new T5540rec();
	private T5543rec t5543rec = new T5543rec();
	private T5551rec t5551rec = new T5551rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
		/*Unit Journal Temporary file - Major Alts*/
	private UjnlmjaTableDAM ujnlmjaIO = new UjnlmjaTableDAM();
		/*Unit Linked Fund Direction.*/
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
		/*Unit transaction summary*/
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
		/*VPRCPF view for now price for units*/
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6352ScreenVars sv = ScreenProgram.getScreenVars( S6352ScreenVars.class);
	
	//ILIFE-8137 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1690, 
		preExit, 
		redisplay2080, 
		exit2090, 
		exit2190, 
		fundSplitPlan2250, 
		updateOrAdd2270, 
		exit2290, 
		exit2390, 
		updateScreen2450, 
		exit2490, 
		exit3090, 
		exit3190, 
		exit3490, 
		exit4090, 
		exit5090, 
		noMoreJournals5125, 
		addSubfileRecord5130
	}

	public P6352() {
		super();
		screenVars = sv;
		new ScreenModel("S6352", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			contractHeader1020();
			contractCoverage1030();
			validUnitLinked1035();
			headerToScreen1040();
			headingsContd1050();
			loadSubfile1060();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBreakoutProcessed.set("N");
		wsaaRrn.set(ZERO);
		wsaaSeqnbr.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaFirstJrnlKey.set(SPACES);
		wsaaJrnlPlanSuffix.set(ZERO);
		wsaaJrnlTranno.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.currentDunitBal.set(ZERO);
		sv.currentUnitBal.set(ZERO);
		sv.nofDunits.set(ZERO);
		sv.nofUnits.set(ZERO);
		sv.instprem.set(ZERO);
		sv.hseqno.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		wsaaFundCurrArray.set(SPACES);
		sv.effdate.set(wsspsmart.effdate);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void contractHeader1020()
	{
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void contractCoverage1030()
	{
		//ILIFE-8137
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, covrmjaIO);
				if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
			
			covrpfDAO.deleteCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void validUnitLinked1035()
	{
		wsaaNoUlnkFlag.set("N");
		ulnkIO.setDataArea(SPACES);
		ulnkIO.setChdrcoy(chdrpf.getChdrcoy());
		ulnkIO.setChdrnum(chdrpf.getChdrnum());
		ulnkIO.setLife(covrpf.getLife());
		if (isEQ(covrpf.getJlife(),SPACES)) {
			ulnkIO.setJlife("00");
		}
		else {
			ulnkIO.setJlife(covrpf.getJlife());
		}
		ulnkIO.setCoverage(covrpf.getCoverage());
		ulnkIO.setRider(covrpf.getRider());
		if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
			if (isEQ(chdrpf.getPolinc(),1)) {
				ulnkIO.setPlanSuffix(ZERO);
			}
			else {
				ulnkIO.setPlanSuffix(ZERO);
			}
		}
		else {
			ulnkIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		ulnkIO.setFunction("READR");
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)
		&& isNE(ulnkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(ulnkIO.getStatuz());
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		if (isEQ(ulnkIO.getStatuz(),varcom.mrnf)) {
			wsaaNoUlnkFlag.set("Y");
			goTo(GotoLabel.exit1090);
		}
	}

protected void headerToScreen1040()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill(SPACES);
		}
	}

protected void headingsContd1050()
	{
		sv.numpols.set(chdrpf.getPolinc());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrnum(chdrpf.getChdrnum());
		lifemjaIO.setChdrcoy(chdrpf.getChdrcoy());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void loadSubfile1060()
	{
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			covrpf.setPlanSuffix(9999);
			//covrmjaIO.setFunction(varcom.begn);
			covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanPolicyFlag.set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		readTableT56881500();
		readT5671T55511600();
		policyLoad5000();
	}

protected void readTableT56881500()
	{
		read1510();
	}

protected void read1510()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
 
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrpf.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readT5671T55511600()
	{
		try {
			para1610();
		}
		catch (GOTOException e){
		}
	}

protected void para1610()
	{
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrpf.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5551Item.set(SPACES);
		wsaaT5551Item.set(SPACES);
		wsaaT5671Found = "N";
		for (x.set(1); !(isGT(x,4)
		|| isEQ(wsaaT5671Found,"Y")); x.add(1)){
			if (isEQ(t5671rec.pgm[x.toInt()],wsaaProg)) {
				if (isEQ(t5671rec.edtitm[x.toInt()],SPACES)) {
					scrnparams.errorCode.set(f025);
					wsspcomn.edterror.set("Y");
					x.set(5);
				}
				else {
					wsaaT5551Part1.set(t5671rec.edtitm[x.toInt()]);
					wsaaT5671Found = "Y";
				}
			}
		}
		if (isEQ(wsaaT5671Found,"N")) {
			scrnparams.errorCode.set(f025);
			wsspcomn.edterror.set("Y");
			t5551rec.t5551Rec.set(SPACES);
			goTo(GotoLabel.exit1690);
		}
		itdmIO.setDataArea(SPACES);
		t5551rec.alfnds.set(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5551);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5551Part2.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaT5551Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5551)
		|| isNE(itdmIO.getItemitem(),wsaaT5551Item)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.statuz.set(varcom.oK);
		if (notUnitLinked.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"J")
		|| isEQ(wsaaT5671Found,"N")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateScreen2020();
					checkForErrors2030();
				}
				case redisplay2080: {
					redisplay2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsaaT5671Found,"N")) {
			scrnparams.errorCode.set(f025);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.redisplay2080);
		}
	}

protected void validateScreen2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"J")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsaaCalcOnFlag.set("Y");
		}
		else {
			wsaaCalcOnFlag.set("N");
		}
		if (isEQ(scrnparams.statuz,varcom.rold)) {
			if (isEQ(wsaaPageNo,1)
			|| isEQ(wsaaJrnlChdrnum,SPACES)) {
				scrnparams.errorCode.set(f498);
			}
			else {
				wsaaRold.set("Y");
			}
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaRolu.set("Y");
		}
	}

protected void checkForErrors2030()
	{
		if (isNE(scrnparams.errorCode,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SUBFILE*/
		for (scrnparams.subfileRrn.set(1); !(isEQ(scrnparams.statuz,varcom.mrnf)
		|| isEQ(wsspcomn.edterror,"Y")); scrnparams.subfileRrn.add(1)){
			validateSubfile2100();
		}
		if (cf9Pressed.isTrue()) {
			wsspcomn.edterror.set("Y");
			scrnparams.subfileRrn.set(sv.subfilePage);
		}
	}

protected void redisplay2080()
	{
		if (isNE(scrnparams.statuz,"CALC")) {
			goTo(GotoLabel.exit2090);
		}
		else {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		try {
			readNextModifiedRecord2110();
			updateErrorIndicators2130();
		}
		catch (GOTOException e){
		}
	}

protected void readNextModifiedRecord2110()
	{
		scrnparams.function.set(varcom.sread);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.mrnf)) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		/*FIELD-VALIDATIONS*/
		fieldValidations2200();
	}

protected void updateErrorIndicators2130()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,varcom.oK)) {
			displayUtrs2400();
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void fieldValidations2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					delete2210();
					fundCheck2230();
					unitTypeCheck2240();
				}
				case fundSplitPlan2250: {
					fundSplitPlan2250();
					coverageFunds2260();
				}
				case updateOrAdd2270: {
					updateOrAdd2270();
				}
				case exit2290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void delete2210()
	{
		if (isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.unitType,SPACES)
		&& isEQ(sv.nofUnits,ZERO)
		&& isEQ(sv.nofDunits,ZERO)
		&& (isEQ(sv.updteflag,"N")
		|| isEQ(sv.updteflag,"U"))) {
			sv.updteflag.set("D");
			goTo(GotoLabel.exit2290);
		}
		/*BLANK*/
		if (isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.unitType,SPACES)
		&& isEQ(sv.nofUnits,ZERO)
		&& isEQ(sv.nofDunits,ZERO)
		&& (isEQ(sv.updteflag,SPACES)
		|| isEQ(sv.updteflag,"X"))) {
			sv.updteflag.set("X");
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.unitType,SPACES)
		&& isEQ(sv.nofUnits,ZERO)
		&& isEQ(sv.nofDunits,ZERO)
		&& isEQ(sv.updteflag,"D")) {
			goTo(GotoLabel.exit2290);
		}
	}

protected void fundCheck2230()
	{
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5515);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			itdmIO.setItemcoy(chdrpf.getChdrcoy());
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(sv.unitVirtualFund);
			itdmIO.setItmfrm(sv.effdate);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItempfx(),"IT")
		|| isNE(itdmIO.getItemitem(),sv.unitVirtualFund)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.vrtfndErr.set(g695);
			goTo(GotoLabel.fundSplitPlan2250);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void unitTypeCheck2240()
	{
		if (isNE(sv.unitType,t5515rec.unitType)
		&& isNE(t5515rec.unitType,"B")) {
			sv.vrtfndErr.set(h972);
		}
		for (wsaaFundIx.set(1); !(isGT(wsaaFundIx,10)); wsaaFundIx.add(1)){
			if (isEQ(wsaaFund[wsaaFundIx.toInt()],itdmIO.getItemitem())) {
				wsaaFundIx.set(11);
			}
			else {
				if (isEQ(wsaaFund[wsaaFundIx.toInt()],SPACES)) {
					wsaaFund[wsaaFundIx.toInt()].set(itdmIO.getItemitem());
					wsaaFundCurr[wsaaFundIx.toInt()].set(t5515rec.currcode);
					wsaaFundIx.set(11);
				}
			}
		}
	}

protected void fundSplitPlan2250()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5540)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(g144);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		if (isEQ(t5540rec.fundSplitPlan,SPACES)) {
			goTo(GotoLabel.updateOrAdd2270);
		}
	}

protected void coverageFunds2260()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(t5543);
		itemIO.setItemitem(t5551rec.alfnds);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.vrtfndErr.set(g144);
			goTo(GotoLabel.exit2290);
		}
		t5543rec.t5543Rec.set(itemIO.getGenarea());
		wsaaSearchFlag.set(SPACES);
		wsaaSub.set(ZERO);
		while ( !(wsaaSearchExit.isTrue())) {
			fundSearch2300();
		}
		
		if (wsaaNoFundFound.isTrue()) {
			sv.vrtfndErr.set(g282);
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(sv.unitType,"A")) {
			if (isNE(sv.nofUnits,sv.nofDunits)) {
				sv.nofuntjErr.set(h264);
				goTo(GotoLabel.exit2290);
			}
		}
		if (isEQ(sv.unitType,"I")) {
			if (isGTE(sv.nofUnits,0)) {
				if (isGTE(sv.nofUnits,sv.nofDunits)) {
					sv.nofuntjErr.set(h265);
					goTo(GotoLabel.exit2290);
				}
			}
			else {
				if (isLTE(sv.nofUnits,sv.nofDunits)) {
					sv.nofuntjErr.set(h266);
					goTo(GotoLabel.exit2290);
				}
			}
		}
	}

protected void updateOrAdd2270()
	{
		if (isEQ(sv.updteflag,"N")) {
			sv.updteflag.set("U");
		}
		if (isEQ(sv.updteflag,SPACES)) {
			sv.updteflag.set("A");
		}
	}

protected void fundSearch2300()
	{
		try {
			search2310();
		}
		catch (GOTOException e){
		}
	}

protected void search2310()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			wsaaSearchFlag.set("N");
		}
		else {
			if (isNE(sv.unitVirtualFund,t5543rec.unitVirtualFund[wsaaSub.toInt()])) {
				goTo(GotoLabel.exit2390);
			}
			else {
				wsaaSearchFlag.set("Y");
			}
		}
	}

protected void displayUtrs2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readSubfileRec2410();
					utrsRecord2430();
					utrsToScreen2440();
				}
				case updateScreen2450: {
				}
				case exit2490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readSubfileRec2410()
	{
		/*NO-UPDATES*/
		if (isEQ(sv.unitVirtualFund,SPACES)
		&& isEQ(sv.unitType,SPACES)) {
			goTo(GotoLabel.exit2490);
		}
	}

protected void utrsRecord2430()
	{
		utrsIO.setDataArea(SPACES);
		utrsIO.setChdrcoy(chdrpf.getChdrcoy());
		utrsIO.setChdrnum(chdrpf.getChdrnum());
		utrsIO.setLife(covrpf.getLife());
		utrsIO.setCoverage(covrpf.getCoverage());
		utrsIO.setRider(covrpf.getRider());
		if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
			utrsIO.setPlanSuffix(ZERO);
		}
		else {
			utrsIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		utrsIO.setUnitVirtualFund(sv.unitVirtualFund);
		utrsIO.setUnitType(sv.unitType);
		utrsIO.setFormat(utrsrec);
		utrsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(utrsIO.getStatuz());
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
	}

protected void utrsToScreen2440()
	{
		if (isEQ(utrsIO.getStatuz(),varcom.mrnf)) {
			sv.currentUnitBal.set(ZERO);
			sv.currentDunitBal.set(ZERO);
			sv.unitypErr.set(g255);
			sv.vrtfndErr.set(g255);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateScreen2450);
		}
		if (isEQ(chdrpf.getPolinc(),1)) {
			sv.currentUnitBal.set(utrsIO.getCurrentUnitBal());
			sv.currentDunitBal.set(utrsIO.getCurrentDunitBal());
		}
		else {
			if (isEQ(chdrpf.getPolinc(),chdrpf.getPolsum())) {
				if (planLevel.isTrue()) {
					sv.currentUnitBal.set(utrsIO.getCurrentUnitBal());
					sv.currentDunitBal.set(utrsIO.getCurrentDunitBal());
				}
				else {
					compute(sv.currentUnitBal, 6).setRounded(div(utrsIO.getCurrentUnitBal(),chdrpf.getPolsum()));
					compute(sv.currentDunitBal, 6).setRounded(div(utrsIO.getCurrentDunitBal(),chdrpf.getPolsum()));
				}
			}
			else {
				if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
					sv.currentUnitBal.set(utrsIO.getCurrentUnitBal());
					sv.currentDunitBal.set(utrsIO.getCurrentDunitBal());
				}
				else {
					compute(sv.currentUnitBal, 6).setRounded(div(utrsIO.getCurrentUnitBal(),chdrpf.getPolsum()));
					compute(sv.currentDunitBal, 6).setRounded(div(utrsIO.getCurrentDunitBal(),chdrpf.getPolsum()));
				}
			}
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"J")
		|| notUnitLinked.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		for (wsaaRrn.set(1); !(isGT(wsaaRrn,sv.subfilePage)); wsaaRrn.add(1)){
			updateJournals3100();
		}
	}

protected void updateJournals3100()
	{
		try {
			readSubfile3110();
			noUpdates3120();
			setUtrnKey3130();
			deleteUtrn3140();
			addUtrn3150();
			updateUtrn3160();
		}
		catch (GOTOException e){
		}
	}

protected void readSubfile3110()
	{
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void noUpdates3120()
	{
		if (isEQ(sv.updteflag,"X")) {
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(sv.updteflag,SPACES)
		|| isEQ(sv.updteflag,"N")) {
			if (isEQ(scrnparams.subfileRrn,sv.subfilePage)
			&& isNE(sv.unitVirtualFund,SPACES)) {
				ujnlmjaIO.setDataArea(SPACES);
				ujnlmjaIO.setChdrcoy(chdrpf.getChdrcoy());
				ujnlmjaIO.setChdrnum(chdrpf.getChdrnum());
				ujnlmjaIO.setLife(covrpf.getLife());
				ujnlmjaIO.setCoverage(covrpf.getCoverage());
				ujnlmjaIO.setRider(covrpf.getRider());
				ujnlmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
				ujnlmjaIO.setSeqno(sv.hseqno);
				ujnlmjaIO.setUnitVirtualFund(sv.unitVirtualFund);
				ujnlmjaIO.setUnitType(sv.unitType);
				ujnlmjaIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, ujnlmjaIO);
				if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)
				&& isNE(ujnlmjaIO.getStatuz(),varcom.endp)) {
					syserrrec.statuz.set(ujnlmjaIO.getStatuz());
					syserrrec.params.set(ujnlmjaIO.getParams());
					fatalError600();
				}
			}
			goTo(GotoLabel.exit3190);
		}
	}

protected void setUtrnKey3130()
	{
		ujnlmjaIO.setDataKey(SPACES);
		ujnlmjaIO.setSeqno(ZERO);
		ujnlmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		ujnlmjaIO.setChdrnum(chdrpf.getChdrnum());
		ujnlmjaIO.setLife(covrpf.getLife());
		ujnlmjaIO.setCoverage(covrpf.getCoverage());
		ujnlmjaIO.setRider(covrpf.getRider());
		ujnlmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		ujnlmjaIO.setUnitVirtualFund(wsbbVrtfnd[wsaaSub.toInt()]);
		ujnlmjaIO.setUnitType(wsbbUnityp[wsaaSub.toInt()]);
		ujnlmjaIO.setSeqno(sv.hseqno);
	}

protected void deleteUtrn3140()
	{
		if (isEQ(sv.updteflag,"D")) {
			ujnlmjaIO.setFunction(varcom.readh);
			ujnlmjaFile3300();
			ujnlmjaIO.setFormat(ujnlmjarec);
			ujnlmjaIO.setFunction(varcom.delet);
			ujnlmjaFile3300();
			goTo(GotoLabel.exit3190);
		}
	}

protected void addUtrn3150()
	{
		if (isEQ(sv.updteflag,"A")) {
			addUjnlmja3200();
			if (isEQ(wsaaRrn,1)) {
				wsaaFirstJrnlKey.set(ujnlmjaIO.getDataKey());
			}
			goTo(GotoLabel.exit3190);
		}
	}

protected void updateUtrn3160()
	{
		if (isEQ(sv.updteflag,"U")) {
			ujnlmjaIO.setFunction(varcom.readh);
			ujnlmjaFile3300();
			ujnlmjaIO.setUnitVirtualFund(sv.unitVirtualFund);
			ujnlmjaIO.setUnitType(sv.unitType);
			if (isEQ(sv.unitType,"A")) {
				ujnlmjaIO.setUnitSubAccount("ACUM");
			}
			else {
				ujnlmjaIO.setUnitSubAccount("INIT");
			}
			ujnlmjaIO.setNowDeferInd("N");
			ujnlmjaIO.setNofUnits(sv.nofUnits);
			ujnlmjaIO.setNofDunits(sv.nofDunits);
			ujnlmjaIO.setTermid(varcom.vrcmTermid);
			ujnlmjaIO.setUser(varcom.vrcmUser);
			ujnlmjaIO.setTransactionDate(varcom.vrcmDate);
			ujnlmjaIO.setTransactionTime(varcom.vrcmTime);
			ujnlmjaIO.setFormat(ujnlmjarec);
			ujnlmjaIO.setFunction(varcom.rewrt);
			ujnlmjaFile3300();
		}
	}

protected void addUjnlmja3200()
	{
		accRulesTable3210();
		procSeqNo3220();
		ujnlmjaFile3240();
		breakoutReqd3250();
	}

protected void accRulesTable3210()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(covrpf.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void procSeqNo3220()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t6647);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaBatckey.batcBatctrcde.toString());
		stringVariable1.append(chdrpf.getCnttype());/* IJTI-1523 */
		itdmIO.getItemitem().setLeft(stringVariable1.toString());
		wsaaConcatT6647key.set(itdmIO.getItemitem());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isNE(itdmIO.getItemitem(),wsaaConcatT6647key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(g230);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void ujnlmjaFile3240()
	{
		ujnlmjaIO.setChdrcoy(covrpf.getChdrcoy());
		ujnlmjaIO.setChdrnum(covrpf.getChdrnum());
		ujnlmjaIO.setLife(covrpf.getLife());
		ujnlmjaIO.setCoverage(covrpf.getCoverage());
		ujnlmjaIO.setRider(covrpf.getRider());
		ujnlmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		ujnlmjaIO.setUnitVirtualFund(sv.unitVirtualFund);
		ujnlmjaIO.setUnitType(sv.unitType);
		wsaaSeqno.add(1);
		ujnlmjaIO.setSeqno(wsaaSeqno);
		setPrecision(ujnlmjaIO.getTranno(), 0);
		ujnlmjaIO.setTranno(add(chdrpf.getTranno(),1));
		ujnlmjaIO.setTermid(varcom.vrcmTermid);
		ujnlmjaIO.setTransactionDate(varcom.vrcmDate);
		ujnlmjaIO.setTransactionTime(varcom.vrcmTime);
		ujnlmjaIO.setUser(varcom.vrcmUser);
		ujnlmjaIO.setBatccoy(wsspcomn.company);
		ujnlmjaIO.setBatcbrn(wsspcomn.branch);
		ujnlmjaIO.setBatcactyr(wsspcomn.acctyear);
		ujnlmjaIO.setBatcactmn(wsspcomn.acctmonth);
		ujnlmjaIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ujnlmjaIO.setBatcbatch(SPACES);
		ujnlmjaIO.setJobnoPrice(ZERO);
		ujnlmjaIO.setStrpdate(ZERO);
		ujnlmjaIO.setInciNum(ZERO);
		ujnlmjaIO.setInciPerd01(ZERO);
		ujnlmjaIO.setInciPerd02(ZERO);
		ujnlmjaIO.setInciprm01(ZERO);
		ujnlmjaIO.setInciprm02(ZERO);
		ujnlmjaIO.setSurrenderPercent(ZERO);
		if (isEQ(sv.unitType,"A")) {
			ujnlmjaIO.setUnitSubAccount("ACUM");
		}
		else {
			ujnlmjaIO.setUnitSubAccount("INIT");
		}
		ujnlmjaIO.setNowDeferInd("N");
		ujnlmjaIO.setNofUnits(sv.nofUnits);
		ujnlmjaIO.setNofDunits(sv.nofDunits);
		ujnlmjaIO.setMoniesDate(sv.effdate);
		ujnlmjaIO.setPriceUsed(0);
		ujnlmjaIO.setUnitBarePrice(0);
		ujnlmjaIO.setPriceDateUsed(0);
		ujnlmjaIO.setCrtable(covrpf.getCrtable());
		ujnlmjaIO.setCntcurr(chdrpf.getCntcurr());
		ujnlmjaIO.setFeedbackInd(SPACES);
		ujnlmjaIO.setContractAmount(ZERO);
		ujnlmjaIO.setFundRate(1);
		wsaaFundIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaFundIx,wsaaFundCurrRec.length); wsaaFundIx.add(1)){
				if (isEQ(wsaaFund[wsaaFundIx.toInt()],sv.unitVirtualFund)) {
					ujnlmjaIO.setFundCurrency(wsaaFundCurr[wsaaFundIx.toInt()]);
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
		ujnlmjaIO.setFundAmount(ZERO);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			ujnlmjaIO.setSacscode(t5645rec.sacscode02);
			ujnlmjaIO.setSacstyp(t5645rec.sacstype02);
			ujnlmjaIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			ujnlmjaIO.setSacscode(t5645rec.sacscode01);
			ujnlmjaIO.setSacstyp(t5645rec.sacstype01);
			ujnlmjaIO.setGenlcde(t5645rec.glmap01);
		}
		ujnlmjaIO.setContractType(chdrpf.getCnttype());
		ujnlmjaIO.setTriggerModule(SPACES);
		ujnlmjaIO.setTriggerKey(SPACES);
		ujnlmjaIO.setProcSeqNo(t6647rec.procSeqNo);
		ujnlmjaIO.setSvp(1);
		ujnlmjaIO.setDiscountFactor(1);
		ujnlmjaIO.setCrComDate(covrpf.getCrrcd());
		ujnlmjaIO.setFormat(ujnlmjarec);
		ujnlmjaIO.setFunction(varcom.writr);
		ujnlmjaFile3300();
	}

protected void breakoutReqd3250()
	{
		if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())
		&& isNE(covrpf.getPlanSuffix(),ZERO)
		&& !covtbrkWritten.isTrue()) {
			breakoutRecord3400();
		}
		/*EXIT*/
	}

protected void ujnlmjaFile3300()
	{
		/*UJNLMJA-FILE*/
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ujnlmjaIO.getStatuz());
			syserrrec.params.set(ujnlmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void breakoutRecord3400()
	{
		try {
			covtbrkFile3410();
		}
		catch (GOTOException e){
		}
	}

protected void covtbrkFile3410()
	{
		covtbrkIO.setDataArea(SPACES);
		covtbrkIO.setChdrcoy(covrpf.getChdrcoy());
		covtbrkIO.setChdrnum(covrpf.getChdrnum());
		covtbrkIO.setPlanSuffix(covrpf.getPlanSuffix());
		covtbrkIO.setFormat(covtbrkrec);
		covtbrkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtbrkIO);
		if (isNE(covtbrkIO.getStatuz(),varcom.oK)
		&& isNE(covtbrkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covtbrkIO.getStatuz());
			syserrrec.params.set(covtbrkIO.getParams());
			fatalError600();
		}
		if (isEQ(covtbrkIO.getStatuz(),varcom.oK)) {
			wsaaBreakoutProcessed.set("Y");
			goTo(GotoLabel.exit3490);
		}
		covtbrkIO.setDataArea(SPACES);
		covtbrkIO.setChdrcoy(covrpf.getChdrcoy());
		covtbrkIO.setChdrnum(covrpf.getChdrnum());
		covtbrkIO.setPlanSuffix(covrpf.getPlanSuffix());
		wsaaSeqnbr.set(covrpf.getPlanSuffix());
		covtbrkIO.setSeqnbr(wsaaSeqnbr);
		covtbrkIO.setFormat(covtbrkrec);
		covtbrkIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covtbrkIO);
		if (isNE(covtbrkIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covtbrkIO.getStatuz());
			syserrrec.params.set(covtbrkIO.getParams());
			fatalError600();
		}
		wsaaBreakoutProcessed.set("Y");
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (notUnitLinked.isTrue()) {
			wsspcomn.msgarea.set("Component is NOT Unit Linked: T5671 entry invalid");
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		if (rollup.isTrue()) {
			wsaaPageNo.add(1);
			clearSubfile4300();
			wsaaLine.set(1);
			while ( !(isGT(wsaaLine,sv.subfilePage))) {
				roluSubfileLoad4100();
			}
			
		}
		if (rolldown.isTrue()) {
			ujnlmjaIO.setParams(SPACES);
			ujnlmjaIO.setDataKey(wsaaFirstJrnlKey);
			ujnlmjaIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, ujnlmjaIO);
			if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)
			&& isNE(ujnlmjaIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(ujnlmjaIO.getStatuz());
				syserrrec.params.set(ujnlmjaIO.getParams());
				fatalError600();
			}
			clearSubfile4300();
			wsaaPageNo.subtract(1);
			ujnlmjaIO.setFunction(varcom.nextp);
			wsaaSub1.set(1);
			while ( !(isEQ(wsaaSub1,sv.subfilePage)
			|| isNE(ujnlmjaIO.getChdrcoy(),chdrpf.getChdrcoy())
			|| isNE(ujnlmjaIO.getChdrnum(),chdrpf.getChdrnum())
			|| isEQ(ujnlmjaIO.getStatuz(),varcom.endp))) {
				SmartFileCode.execute(appVars, ujnlmjaIO);
				if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)
				&& isNE(ujnlmjaIO.getStatuz(),varcom.endp)) {
					syserrrec.statuz.set(ujnlmjaIO.getStatuz());
					syserrrec.params.set(ujnlmjaIO.getParams());
					fatalError600();
				}
				wsaaSub1.add(1);
			}
			
			roldSubfileLoad4200();
		}
		if (rollup.isTrue()
		|| rolldown.isTrue()) {
			wsaaRolu.set("N");
			wsaaRold.set("N");
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		if (planLevel.isTrue()) {
			//covrmjaIO.setFunction(varcom.nextr);
			clearSubfile4300();
			policyLoad5000();
			if (covrpf != null) {
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.programPtr.add(1);
			}
			/*if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.programPtr.add(1);
			}*/
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
		}
	}

protected void roluSubfileLoad4100()
	{
		roluSubfileLoad4110();
		addSubfileRecord4120();
	}

protected void roluSubfileLoad4110()
	{
		if (isNE(ujnlmjaIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(ujnlmjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(ujnlmjaIO.getLife(),covrpf.getLife())
		|| isNE(ujnlmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(ujnlmjaIO.getRider(),covrpf.getRider())
		|| isNE(ujnlmjaIO.getPlanSuffix(),covrpf.getPlanSuffix())
		|| isEQ(ujnlmjaIO.getStatuz(),varcom.endp)) {
			wsaaNoMoreJrnls = "Y";
			sv.subfileFields.set(SPACES);
			sv.updteflag.set(SPACES);
			sv.currentUnitBal.set(ZERO);
			sv.currentDunitBal.set(ZERO);
			sv.nofDunits.set(ZERO);
			sv.nofUnits.set(ZERO);
			sv.hseqno.set(ZERO);
		}
		else {
			loadSubfile4400();
		}
	}

protected void addSubfileRecord4120()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaLine.add(1);
		/*EXIT*/
	}

protected void roldSubfileLoad4200()
	{
		roldSubfileLoad4210();
	}

protected void roldSubfileLoad4210()
	{
		if (isNE(ujnlmjaIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(ujnlmjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(ujnlmjaIO.getLife(),covrpf.getLife())
		|| isNE(ujnlmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(ujnlmjaIO.getRider(),covrpf.getRider())
		|| isNE(ujnlmjaIO.getPlanSuffix(),covrpf.getPlanSuffix())
		|| isEQ(ujnlmjaIO.getStatuz(),varcom.endp)) {
			ujnlmjaIO.setParams(SPACES);
			ujnlmjaIO.setDataKey(SPACES);
			ujnlmjaIO.setSeqno(ZERO);
			ujnlmjaIO.setChdrcoy(chdrpf.getChdrcoy());
			ujnlmjaIO.setChdrnum(chdrpf.getChdrnum());
			ujnlmjaIO.setLife(covrpf.getLife());
			ujnlmjaIO.setCoverage(covrpf.getCoverage());
			ujnlmjaIO.setRider(covrpf.getRider());
			ujnlmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
			ujnlmjaIO.setUnitVirtualFund(SPACES);
			ujnlmjaIO.setUnitType(SPACES);
			ujnlmjaIO.setSeqno(ZERO);
			ujnlmjaIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, ujnlmjaIO);
			if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)
			&& isNE(ujnlmjaIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(ujnlmjaIO.getStatuz());
				syserrrec.params.set(ujnlmjaIO.getParams());
				fatalError600();
			}
			wsaaPageNo.set(1);
		}
		wsaaLine.set(1);
		while ( !(isGT(wsaaLine,sv.subfilePage))) {
			roluSubfileLoad4100();
		}
		
	}

protected void clearSubfile4300()
	{
		/*CLEAR-SUBFILE*/
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void loadSubfile4400()
	{
		loadScreen4410();
		rolu4410();
	}

protected void loadScreen4410()
	{
		sv.unitVirtualFund.set(ujnlmjaIO.getUnitVirtualFund());
		wsbbVrtfnd[wsaaSub.toInt()].set(ujnlmjaIO.getUnitVirtualFund());
		sv.unitType.set(ujnlmjaIO.getUnitType());
		wsbbUnityp[wsaaSub.toInt()].set(ujnlmjaIO.getUnitType());
		sv.hseqno.set(ujnlmjaIO.getSeqno());
		wsaaSeqno.set(ujnlmjaIO.getSeqno());
		sv.nofUnits.set(ujnlmjaIO.getNofUnits());
		sv.nofDunits.set(ujnlmjaIO.getNofDunits());
		sv.updteflag.set("N");
		sv.currentUnitBal.set(ZERO);
		sv.currentDunitBal.set(ZERO);
	}

protected void rolu4410()
	{
		ujnlmjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ujnlmjaIO.getStatuz());
			syserrrec.params.set(ujnlmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaLine,1)) {
			wsaaFirstJrnlKey.set(ujnlmjaIO.getDataKey());
		}
		ujnlmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)
		&& isNE(ujnlmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ujnlmjaIO.getStatuz());
			syserrrec.params.set(ujnlmjaIO.getParams());
			fatalError600();
		}
	}

protected void policyLoad5000()
	{
		try {
			policyCovr5010();
			searchForUtrn5020();
			firstPage5030();
		}
		catch (GOTOException e){
		}
	}

protected void policyCovr5010()
	{
		if (planLevel.isTrue()) {
			readCovr5200();
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					goTo(GotoLabel.exit5090);
				}
				else {
					if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
						sv.planSuffix.set(chdrpf.getPolsum());
						sv.plnsfxOut[varcom.hi.toInt()].set("Y");
					}
					else {
						sv.planSuffix.set(covrpf.getPlanSuffix());
					}
				}
				covrpf.setPlanSuffix(covr.getPlanSuffix());
			}
			/*if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
				goTo(GotoLabel.exit5090);
			}*/
			
		}
		else {
			/*NEXT_SENTENCE*/
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())) {
			if (isEQ(sv.planSuffix,1)) {
				compute(sv.instprem, 2).set((sub(covrpf.getInstprem(),(div(mult(covrpf.getInstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(sv.instprem, 3).setRounded(div(covrpf.getInstprem(),chdrpf.getPolsum()));
			}
		}
		else {
			sv.instprem.set(covrpf.getInstprem());
		}
	}

protected void searchForUtrn5020()
	{
		ujnlmjaIO.setDataKey(SPACES);
		ujnlmjaIO.setSeqno(ZERO);
		ujnlmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		ujnlmjaIO.setChdrnum(chdrpf.getChdrnum());
		ujnlmjaIO.setLife(covrpf.getLife());
		ujnlmjaIO.setCoverage(covrpf.getCoverage());
		ujnlmjaIO.setRider(covrpf.getRider());
		ujnlmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		ujnlmjaIO.setUnitVirtualFund(SPACES);
		ujnlmjaIO.setUnitType(SPACES);
		ujnlmjaIO.setSeqno(ZERO);
		ujnlmjaIO.setFunction("BEGN");
	}

protected void firstPage5030()
	{
		wsaaSub.set(ZERO);
		wsaaNoMoreJrnls = "N";
		scrnparams.subfileRrn.set(1);
		scrnparams.subfileMore.set("Y");
		wsaaPageNo.set("1");
		for (wsaaSub.set(1); !(isGT(wsaaSub,sv.subfilePage)); wsaaSub.add(1)){
			loadSubfile5100();
		}
	}

protected void loadSubfile5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					utrnJournals5110();
					utrnToScreen5120();
				}
				case noMoreJournals5125: {
					noMoreJournals5125();
				}
				case addSubfileRecord5130: {
					addSubfileRecord5130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void utrnJournals5110()
	{
		if (isEQ(wsaaNoMoreJrnls,"Y")) {
			goTo(GotoLabel.noMoreJournals5125);
		}
		SmartFileCode.execute(appVars, ujnlmjaIO);
		if (isNE(ujnlmjaIO.getStatuz(),varcom.oK)
		&& isNE(ujnlmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ujnlmjaIO.getStatuz());
			syserrrec.params.set(ujnlmjaIO.getParams());
			fatalError600();
		}
		ujnlmjaIO.setFunction(varcom.nextr);
		if (isNE(ujnlmjaIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(ujnlmjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(ujnlmjaIO.getLife(),covrpf.getLife())
		|| isNE(ujnlmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(ujnlmjaIO.getRider(),covrpf.getRider())
		|| isNE(ujnlmjaIO.getPlanSuffix(),covrpf.getPlanSuffix())
		|| isEQ(ujnlmjaIO.getStatuz(),varcom.endp)) {
			wsaaNoMoreJrnls = "Y";
			goTo(GotoLabel.noMoreJournals5125);
		}
		wsbbVrtfnd[wsaaSub.toInt()].set(ujnlmjaIO.getUnitVirtualFund());
		wsbbUnityp[wsaaSub.toInt()].set(ujnlmjaIO.getUnitType());
		if (isEQ(wsaaSub,1)) {
			wsaaFirstJrnlKey.set(ujnlmjaIO.getRecKeyData());
		}
	}

protected void utrnToScreen5120()
	{
		sv.unitVirtualFund.set(ujnlmjaIO.getUnitVirtualFund());
		wsbbVrtfnd[wsaaSub.toInt()].set(ujnlmjaIO.getUnitVirtualFund());
		sv.unitType.set(ujnlmjaIO.getUnitType());
		wsbbUnityp[wsaaSub.toInt()].set(ujnlmjaIO.getUnitType());
		sv.hseqno.set(ujnlmjaIO.getSeqno());
		sv.nofUnits.set(ujnlmjaIO.getNofUnits());
		sv.nofDunits.set(ujnlmjaIO.getNofDunits());
		sv.updteflag.set("N");
		goTo(GotoLabel.addSubfileRecord5130);
	}

protected void noMoreJournals5125()
	{
		sv.subfileFields.set(SPACES);
		sv.updteflag.set(SPACES);
		sv.currentDunitBal.set(ZERO);
		sv.currentUnitBal.set(ZERO);
		sv.nofDunits.set(ZERO);
		sv.nofUnits.set(ZERO);
		sv.hseqno.set(ZERO);
	}

protected void addSubfileRecord5130()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6352", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void readCovr5200()
	{
		/*READ-COVRMJA*/
		//ILIFE-8137
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)
					|| (!covrpfList.isEmpty())) {
						return;
					}
		}
		/*EXIT*/
	}
}
