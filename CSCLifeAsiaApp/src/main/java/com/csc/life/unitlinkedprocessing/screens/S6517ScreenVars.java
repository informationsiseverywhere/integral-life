package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6517
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6517ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(111);
	public FixedLengthStringData dataFields = new FixedLengthStringData(63).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 63);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 75);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(81);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(15).isAPartOf(subfileArea, 0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData stmtClDate = DD.stcldt.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public FixedLengthStringData stmtlevel = DD.stmtlevel.copy().isAPartOf(subfileFields,9);
	public ZonedDecimalData ustmno = DD.ustmno.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 15);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData stcldtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData stmtlevelErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData ustmnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 31);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] stcldtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] stmtlevelOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] ustmnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 79);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData stmtClDateDisp = new FixedLengthStringData(10);

	public LongData S6517screensflWritten = new LongData(0);
	public LongData S6517screenctlWritten = new LongData(0);
	public LongData S6517screenWritten = new LongData(0);
	public LongData S6517protectWritten = new LongData(0);
	public GeneralTable s6517screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6517screensfl;
	}

	public S6517ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, ustmno, stmtClDate, stmtlevel};
		screenSflOutFields = new BaseData[][] {selectOut, ustmnoOut, stcldtOut, stmtlevelOut};
		screenSflErrFields = new BaseData[] {selectErr, ustmnoErr, stcldtErr, stmtlevelErr};
		screenSflDateFields = new BaseData[] {stmtClDate};
		screenSflDateErrFields = new BaseData[] {stcldtErr};
		screenSflDateDispFields = new BaseData[] {stmtClDateDisp};

		screenFields = new BaseData[] {chdrnum, cownnum, ownername};
		screenOutFields = new BaseData[][] {chdrnumOut, cownnumOut, ownernameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cownnumErr, ownernameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6517screen.class;
		screenSflRecord = S6517screensfl.class;
		screenCtlRecord = S6517screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6517protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6517screenctl.lrec.pageSubfile);
	}
}
