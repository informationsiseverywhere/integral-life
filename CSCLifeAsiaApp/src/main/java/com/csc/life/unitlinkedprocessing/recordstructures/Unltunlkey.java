package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:36
 * Description:
 * Copybook name: UNLTUNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Unltunlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData unltunlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData unltunlKey = new FixedLengthStringData(64).isAPartOf(unltunlFileKey, 0, REDEFINE);
  	public FixedLengthStringData unltunlChdrcoy = new FixedLengthStringData(1).isAPartOf(unltunlKey, 0);
  	public FixedLengthStringData unltunlChdrnum = new FixedLengthStringData(8).isAPartOf(unltunlKey, 1);
  	public FixedLengthStringData unltunlLife = new FixedLengthStringData(2).isAPartOf(unltunlKey, 9);
  	public FixedLengthStringData unltunlCoverage = new FixedLengthStringData(2).isAPartOf(unltunlKey, 11);
  	public FixedLengthStringData unltunlRider = new FixedLengthStringData(2).isAPartOf(unltunlKey, 13);
  	public PackedDecimalData unltunlSeqnbr = new PackedDecimalData(3, 0).isAPartOf(unltunlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(unltunlKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(unltunlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		unltunlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}