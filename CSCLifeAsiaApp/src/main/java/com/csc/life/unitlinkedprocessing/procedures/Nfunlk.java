/*
 * File: Nfunlk.java
 * Date: 29 August 2009 23:00:58
 * Author: Quipoz Limited
 * 
 * Class transformed from NFUNLK.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*           NON-FORFEITURE UNIT LINKED CONTRACT SUBROUTINE
*           ----------------------------------------------
*
* OVERVIEW.
* ---------
*
*   This subroutine forms part of the PAID-UP/LAPSE processing
*   routine and it will perform the following functions:
*
*   1. Through OVRDUEREC, to receive contract/LIFE/Coverage/
*      Rider numbers and read UTRS to check if there are any
*      REAL units. If there are units in holding:-
*
*        Write a UTRN record to cancel all existing units.
*
*   2. Performs Agent Commission claw back routine.
*
* PROCESSING
* ----------
*
* The processing will take place in 3 phases as follow:
*
*     1. Obtain sub account entries form T5645
*        Read T5645 using program ID as item to obtain the
*        account entries. These codes will be used in writing
*        the  ACMV and the UTRN records.
*
*     2. Read UTRS to check if there are units in holding for
*        the coverage. This is necessary because if units
*        held = NIL, there is no need to write a negative
*        UTRN and hence the program can move on to next
*        step and start the commission claw back routine.
*        The sources of the fields are:
*
*        Company Number        -  Passed in Linkage
*        Contract Number       -     "    "    "
*        Life                  -     "    "    "
*        Coverage              -     "    "    "
*        Rider                 -     "    "    "
*        Plan Suffix           -     "    "    "
*
*       If the program returns with a different key values
*       i.e. key breaks or UTRS-STATUZ equals ENDP, go to exist.
*
*       If UTRS-CURRENT-UNIT-BAL = 0, i.e. No REAL units in
*       holding then move 'NEXTR' into the UTRS-FUNCTION and read
*       the next record.
*       Else
*       Obtain fund currency from T5515
*       Write a UTRN record:
*
*       MOVE OVRD-CHDRCOY           TO UTRN-CHDRCOY.
*       MOVE OVRD-CHDRNUM           TO UTRN-CHDRNUM.
*       MOVE OVRD-PLAN-SUFFIX       TO UTRN-PLAN-SUFFIX.
*       MOVE OVRD-LIFE              TO UTRN-LIFE.
*       MOVE OVRD-COVERAGE          TO UTRN-COVERAGE.
*       MOVE OVRD-RIDER             TO UTRN-RIDER.
*       MOVE OVRD-EFFDATE           TO UTRN-MONIES-DATE.
*       MOVE OVRD-TRANNO            TO UTRN-TRANNO.
*       MOVE OVRD-CNTCURR           TO UTRN-CNTCURR.
*       MOVE OVRD-ACCTYEAR          TO UTRN-BATCACTYR.
*       MOVE OVRD-ACCTMONTH         TO UTRN-BATCACTMN.
*       MOVE 100                    TO UTRN-SURRENDER-PERCENT.
*       MOVE +0                     TO UTRN-FUND-AMOUNT.
*       MOVE 0                      TO UTRN-UNIT-BARE-PRICE.
*       MOVE 0                      TO UTRN-NOF-UNITS.
*       MOVE UTRS-UNIT-VIRTUAL-FUND TO UTRN-UNIT-VIRTUAL-FUND.
*       MOVE UTRS-UNIT-TYPE         TO UTRN-UNIT-TYPE.
*       MOVE T5645-SACSCODE-01      TO UTRN-SACSCODE.
*       MOVE T5645-SACSTYPE-01      TO UTRN-SACSTYP.
*       MOVE T5645-GLMAP-01         TO UTRN-GENLCDE.
*       MOVE OVRD-TRAN-DATE         TO UTRN-TRANSACTION-DATE.
*       MOVE OVRD-TRAN-TIME         TO UTRN-TRANSACTION-TIME.
*       MOVE OVRD-USER              TO UTRN-USER.
*       MOVE OVRD-TRANCODE          TO UTRN-BATCTRCDE.
*       MOVE OVRD-CNTTYPE           TO UTRN-CONTRACT-TYPE.
*       MOVE T5515-CURRCODE         TO UTRN-FUND-CURRENCY.
*       MOVE OVRD-CRTABLE           TO UTRN-CRTABLE.
*       MOVE OVRD-ALOIND            TO UTRN-NOW-DEFER-IND.
*
*    3. Commission claw back routine.
*
*       This is the third processing step of this program. It
*       aims to deal with agent commission records in ACMVSUR.
*
*       To read records from ACMVSUR, we would set up the
*       key fields as follow :
*
*       Company Number        -  Passed in Linkage
*       Contract Number       -     "    "    "
*       Life                  -     "    "    "
*       Coverage              -     "    "    "
*       Rider                 -     "    "    "
*       Plan Suffix           -     "    "    "
*       Agntnum               -     "    "    "
*
*       If the program returns with a different key values
*       i.e. key breaks or AGCM-STATUZ equals ENDP, go to exit.
*
*       If AGCM commission paid = commission earned
*       (AGCMLAP-COMPAY = AGCMLAP-COMERN) then read the next recor 
*       Else
*       Create two ACMV records for each transaction, one for
*       Commission Clawback for agents, and one for Re-crediting
*       Agent Commission Advance. The details for these will be
*       held on T5645, accessed by  program  Id.  Line # 1  will
*       give the  details for the first posting for which the
*       amount on the ACMV will be negative  and  line  #2  will
*       give  the  details  for  the  second  posting  for which t e
*       amount on the ACMV will be positive.
*
*       The subroutine LIFACMV  will  be  called  to  add  the
*       ACMV records.
*
*       ACMV-RDOCNUM           -  OVRD-CHDRNUM
*       ACMV-RLDGACCT          -  AGCMLAP-AGNTNUM
*       ACMV-BATCCOY           -  OVRD-CHDRCOY
*       ACMV-ORIGCURR          -  OVRD-CNTCURR
*       ACMV-TRANNO            -  OVRD-TRANNO
*       ACMV-JRNSEQ            -  1
*       ACMV-ORIGAMT           -  Difference between commission
*                                 paid/earned.
*                                 (Earned minus Paid).
*       ACMV-TRANREF           -  OVRD-CHDRNUM
*       ACMV-SACSTYP           -  FROM T5645
*       ACMV-SACSCODE          -  FROM T5645
*       ACMV-GLCODE            -  FROM T5645
*       ACMV-GLSIGN            -  FROM T5645
*       ACMV-CNTTOT            -  FROM T5645
*       ACMV-POSTYEAR          -  From LINKAGE
*       ACMV-POSTMONTH         -    "    "
*       ACMV-EFFDATE           -  OVRD-PTDATE
*       ACMV-TRANSACTION-DATE  -  OVRD-TRAN-DATE
*       ACMV-TRANSACTION-TIME  -  OVRD-TRAN-TIME
*       ACMV-USER              -  OVRD USER
*       ACMV-TERMID            -  Term Id. passed in linkage
*
*****************************************************************
* </pre>
*/
public class Nfunlk extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "NFUNLK";
	private ZonedDecimalData wsaaCommRecovered = new ZonedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
		/* ERRORS */
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5515 = "T5515";
	private static final String t5688 = "T5688";
	private static final String t6647 = "T6647";
	private static final String th605 = "TH605";
		/* FORMATS */
	private static final String descrec = "DESCREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String utrnrec = "UTRNREC   ";
	private static final String utrsrec = "UTRSREC   ";
	private static final String agcmrec = "AGCMREC   ";
	private static final String agcmdbcrec = "AGCMREC   ";
	private static final String hitsrec = "HITSREC   ";
	private static final String hitrrec = "HITRREC   ";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5515rec t5515rec = new T5515rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
	private Th605rec th605rec = new Th605rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	protected Ovrduerec ovrduerec1 = new Ovrduerec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		call1050, 
		a150Call, 
		callAgcmio2020
	}

	public Nfunlk() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec1.ovrdueRec = convertAndSetParam(ovrduerec1.ovrdueRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec1.statuz.set(varcom.oK);
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.jrnseq.set(0);
		/* IF OVRD-PTDATE          NOT = OVRD-BTDATE               <001>*/
		/*    PERFORM 500-CALL-REVERBILL.                          <001>*/
		/*    Read T5645 for sub account code.*/
		readT56886000();
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		/*    MOVE OVRD-COMPANY           TO ITEM-ITEMCOY.                 */
		itemIO.setItemcoy(ovrduerec1.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError5000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*  Read TH605 to see whether system is "Override based on Agent   */
		/*  Details"                                                       */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec1.chdrcoy);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(ovrduerec1.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError5000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		checkUtrs1000();
		a100CheckHits();
		commClawBack2000();
		ovrduerec1.newSumins.set(ovrduerec1.sumins);
		ovrduerec1.newPremCessDate.set(ovrduerec1.premCessDate);
		processReassurance8000();
	}

protected void exit090()
	{
		exitProgram();
	}

	/**
	* <pre>
	*500-CALL-REVERBILL SECTION.                                 <001>
	****************************                                 <001>
	*                                                            <001>
	*510-START.                                                  <001>
	**** MOVE OVRD-CHDRCOY           TO REVE-COMPANY.            <001>
	**** MOVE OVRD-CHDRNUM           TO REVE-CHDRNUM.            <001>
	**** MOVE OVRD-LIFE              TO REVE-LIFE.               <001>
	**** MOVE OVRD-COVERAGE          TO REVE-COVERAGE.           <001>
	**** MOVE OVRD-RIDER             TO REVE-RIDER.              <001>
	**** MOVE OVRD-PLAN-SUFFIX       TO REVE-PLAN-SUFFIX.        <001>
	**** MOVE SPACES                 TO REVE-BATCPFX.            <001>
	**** MOVE OVRD-COMPANY           TO REVE-BATCCOY.            <001>
	**** MOVE OVRD-BATCBRN           TO REVE-BATCBRN.            <001>
	**** MOVE OVRD-ACCTYEAR          TO REVE-BATCACTYR.          <001>
	**** MOVE OVRD-ACCTMONTH         TO REVE-BATCACTMN.          <001>
	**** MOVE SPACES                 TO REVE-BATCTRCDE.          <001>
	**** MOVE OVRD-BATCBATCH         TO REVE-BATCBATCH.          <001>
	**** MOVE OVRD-PTDATE            TO REVE-EFFDATE-1.          <001>
	**** MOVE VRCM-MAX-DATE          TO REVE-EFFDATE-2.          <001>
	**** MOVE ZEROS                  TO REVE-TRANNO.             <001>
	**** MOVE OVRD-TRANNO            TO REVE-NEW-TRANNO.         <001>
	**** MOVE OVRD-TRANCODE          TO REVE-OLD-BATCTRCDE.      <001>
	**** MOVE ZEROES                 TO REVE-PTRNEFF.            <002>
	****                                                         <001>
	**** CALL 'REVBILL' USING REVE-REVERSE-REC.                  <001>
	****                                                         <001>
	**** IF REVE-STATUZ           NOT = O-K                      <001>
	****    MOVE REVE-STATUZ          TO SYSR-STATUZ             <001>
	****    MOVE REVE-REVERSE-REC     TO SYSR-PARAMS             <001>
	****                                                         <001>
	*590-EXIT.                                                   <001>
	**** EXIT.                                                   <001>
	* </pre>
	*/
protected void checkUtrs1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
				case call1050: 
					call1050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		/* Read UTRS to check if any units in holding.*/
		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(ovrduerec1.chdrcoy);
		utrsIO.setChdrnum(ovrduerec1.chdrnum);
		utrsIO.setLife(ovrduerec1.life);
		utrsIO.setCoverage(ovrduerec1.coverage);
		utrsIO.setRider(ovrduerec1.rider);
		utrsIO.setPlanSuffix(ovrduerec1.planSuffix);
		utrsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		utrsIO.setFormat(utrsrec);
	}

protected void call1050()
	{
		SmartFileCode.execute(appVars, utrsIO);
		if ((isNE(utrsIO.getStatuz(), varcom.oK))
		&& (isNE(utrsIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(utrsIO.getStatuz());
			syserrrec.params.set(utrsIO.getParams());
			dbError5000();
		}
		if (isEQ(utrsIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*  Check record returns*/
		if ((isNE(utrsIO.getChdrcoy(), ovrduerec1.chdrcoy))
		|| (isNE(utrsIO.getChdrnum(), ovrduerec1.chdrnum))
		|| (isNE(utrsIO.getLife(), ovrduerec1.life))
		|| (isNE(utrsIO.getCoverage(), ovrduerec1.coverage))
		|| (isNE(utrsIO.getRider(), ovrduerec1.rider))
		|| (isNE(utrsIO.getPlanSuffix(), ovrduerec1.planSuffix))) {
			utrsIO.setStatuz(varcom.endp);
			return ;
		}
		/*  If REAL unit balance >0 then write a UTRN record.*/
		if (isGT(utrsIO.getCurrentUnitBal(), 0)) {
			writeUtrn3000();
		}
		/*  Read the next UTRS record.*/
		utrsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call1050);
	}

protected void a100CheckHits()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a110Para();
				case a150Call: 
					a150Call();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Para()
	{
		/* Read HITS to check for positive fund balance.                   */
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(ovrduerec1.chdrcoy);
		hitsIO.setChdrnum(ovrduerec1.chdrnum);
		hitsIO.setLife(ovrduerec1.life);
		hitsIO.setCoverage(ovrduerec1.coverage);
		hitsIO.setRider(ovrduerec1.rider);
		hitsIO.setPlanSuffix(ovrduerec1.planSuffix);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		hitsIO.setFormat(hitsrec);
	}

protected void a150Call()
	{
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			dbError5000();
		}
		if (isNE(hitsIO.getChdrcoy(), ovrduerec1.chdrcoy)
		|| isNE(hitsIO.getChdrnum(), ovrduerec1.chdrnum)
		|| isNE(hitsIO.getLife(), ovrduerec1.life)
		|| isNE(hitsIO.getCoverage(), ovrduerec1.coverage)
		|| isNE(hitsIO.getRider(), ovrduerec1.rider)
		|| isNE(hitsIO.getPlanSuffix(), ovrduerec1.planSuffix)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			hitsIO.setStatuz(varcom.endp);
			return ;
		}
		/*  If current premium balance > 0 then write a HITR record.       */
		if (isGT(hitsIO.getZcurprmbal(), 0)) {
			a200WriteHitr();
		}
		/*  Read the next HITS record.                                     */
		hitsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.a150Call);
	}

protected void a200WriteHitr()
	{
		a210Para();
	}

protected void a210Para()
	{
		readTableT66477000();
		hitrIO.setParams(SPACES);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setTranno(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setPlanSuffix(ZERO);
		hitrIO.setBatcactyr(ZERO);
		hitrIO.setBatcactmn(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSvp(ZERO);
		hitrIO.setZintrate(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setChdrcoy(ovrduerec1.chdrcoy);
		hitrIO.setChdrnum(ovrduerec1.chdrnum);
		hitrIO.setPlanSuffix(ovrduerec1.planSuffix);
		hitrIO.setLife(ovrduerec1.life);
		hitrIO.setCoverage(ovrduerec1.coverage);
		hitrIO.setRider(ovrduerec1.rider);
		hitrIO.setEffdate(ovrduerec1.ptdate);
		hitrIO.setTranno(ovrduerec1.tranno);
		hitrIO.setCntcurr(ovrduerec1.cntcurr);
		hitrIO.setBatccoy(ovrduerec1.company);
		hitrIO.setBatcbrn(ovrduerec1.batcbrn);
		hitrIO.setBatcbatch(ovrduerec1.batcbatch);
		hitrIO.setBatcactyr(ovrduerec1.acctyear);
		hitrIO.setBatcactmn(ovrduerec1.acctmonth);
		hitrIO.setSurrenderPercent(100);
		hitrIO.setSvp(1);
		hitrIO.setFundAmount(0);
		hitrIO.setZintbfnd(hitsIO.getZintbfnd());
		hitrIO.setZrectyp("P");
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			hitrIO.setSacscode(t5645rec.sacscode08);
			hitrIO.setSacstyp(t5645rec.sacstype08);
			hitrIO.setGenlcde(t5645rec.glmap08);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode07);
			hitrIO.setSacstyp(t5645rec.sacstype07);
			hitrIO.setGenlcde(t5645rec.glmap07);
		}
		hitrIO.setBatctrcde(ovrduerec1.trancode);
		hitrIO.setCnttyp(ovrduerec1.cnttype);
		hitrIO.setCrtable(ovrduerec1.crtable);
		wsaaT5515Fund.set(hitsIO.getZintbfnd());
		readT55154600();
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setZintalloc("N");
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			dbError5000();
		}
	}

	/**
	* <pre>
	*2000-COMM-CLAW-BACK SECTION.                                     
	*2010-PARA.                                                       
	**** PERFORM 4000-READ-T1688.                                     
	* Read all the relevant AGCM record for this contract.
	*    MOVE SPACE                  TO AGCMSUR-DATA-AREA.            
	*    MOVE OVRD-CHDRNUM           TO AGCMSUR-CHDRNUM.              
	*    MOVE OVRD-CHDRCOY           TO AGCMSUR-CHDRCOY.              
	*    MOVE OVRD-LIFE              TO AGCMSUR-LIFE.                 
	*    MOVE OVRD-COVERAGE          TO AGCMSUR-COVERAGE.             
	*    MOVE OVRD-RIDER             TO AGCMSUR-RIDER.                
	*    MOVE OVRD-PLAN-SUFFIX       TO AGCMSUR-PLAN-SUFFIX.          
	*    MOVE OVRD-AGNTNUM           TO AGCMSUR-AGNTNUM.              
	*    MOVE BEGNH                  TO AGCMSUR-FUNCTION.             
	*    MOVE AGCMSURREC             TO AGCMSUR-FORMAT.               
	**** MOVE SPACE                  TO AGCMLAP-DATA-AREA.       <004>
	**** MOVE OVRD-CHDRNUM           TO AGCMLAP-CHDRNUM.         <004>
	**** MOVE OVRD-CHDRCOY           TO AGCMLAP-CHDRCOY.         <004>
	**** MOVE OVRD-LIFE              TO AGCMLAP-LIFE.            <004>
	**** MOVE OVRD-COVERAGE          TO AGCMLAP-COVERAGE.        <004>
	**** MOVE OVRD-RIDER             TO AGCMLAP-RIDER.           <004>
	**** MOVE OVRD-PLAN-SUFFIX       TO AGCMLAP-PLAN-SUFFIX.     <004>
	*****MOVE OVRD-AGNTNUM           TO AGCMLAP-AGNTNUM.              
	**** MOVE BEGNH                  TO AGCMLAP-FUNCTION.        <004>
	**** MOVE BEGN                   TO AGCMLAP-FUNCTION.        <006>
	**** MOVE AGCMLAPREC             TO AGCMLAP-FORMAT.          <004>
	*2050-CALL-AGCMSURIO.                                             
	*2050-CALL-AGCMLAPIO.                                        <004>
	*    CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS.            
	*    IF (AGCMSUR-STATUZ          NOT = O-K) AND                   
	*       (AGCMSUR-STATUZ          NOT = ENDP)                      
	*       MOVE AGCMSUR-STATUZ      TO SYSR-STATUZ                   
	*       MOVE AGCMSUR-PARAMS      TO SYSR-PARAMS                   
	*    IF AGCMSUR-STATUZ           = ENDP                           
	*    GO TO 2090-EXIT.                                             
	*    IF (AGCMSUR-CHDRCOY         NOT = OVRD-CHDRCOY) OR           
	*       (AGCMSUR-CHDRNUM         NOT = OVRD-CHDRNUM) OR           
	*       (AGCMSUR-LIFE            NOT = OVRD-LIFE) OR              
	*       (AGCMSUR-COVERAGE        NOT = OVRD-COVERAGE) OR          
	*       (AGCMSUR-RIDER           NOT = OVRD-RIDER)OR              
	*       (AGCMSUR-PLAN-SUFFIX     NOT = OVRD-PLAN-SUFFIX)OR        
	*       (AGCMSUR-AGNTNUM         NOT = OVRD-AGNTNUM)              
	*       MOVE ENDP                TO AGCMSUR-STATUZ                
	**** CALL 'AGCMLAPIO'            USING AGCMLAP-PARAMS.       <004>
	**** IF (AGCMLAP-STATUZ          NOT = O-K) AND              <004>
	****    (AGCMLAP-STATUZ          NOT = ENDP)                 <004>
	****    MOVE AGCMLAP-STATUZ      TO SYSR-STATUZ              <004>
	****    MOVE AGCMLAP-PARAMS      TO SYSR-PARAMS              <004>
	**** IF AGCMLAP-STATUZ           = ENDP                      <004>
	**** GO TO 2090-EXIT.                                        <004>
	**** IF (AGCMLAP-CHDRCOY         NOT = OVRD-CHDRCOY) OR      <004>
	****    (AGCMLAP-CHDRNUM         NOT = OVRD-CHDRNUM) OR      <004>
	****    (AGCMLAP-LIFE            NOT = OVRD-LIFE) OR         <004>
	****    (AGCMLAP-COVERAGE        NOT = OVRD-COVERAGE) OR     <004>
	****    (AGCMLAP-RIDER           NOT = OVRD-RIDER)OR         <004>
	********(AGCMLAP-PLAN-SUFFIX     NOT = OVRD-PLAN-SUFFIX)OR        
	****    (AGCMLAP-PLAN-SUFFIX     NOT = OVRD-PLAN-SUFFIX)     <004>
	********(AGCMLAP-AGNTNUM         NOT = OVRD-AGNTNUM)              
	****    MOVE ENDP                TO AGCMLAP-STATUZ           <004>
	**** GO TO 2090-EXIT.                                             
	* Check to see if agent commission clawback is required. If
	* the commission paid is the same as commission earned, no
	* clawback is required.
	*    IF AGCMSUR-COMPAY           = AGCMSUR-COMERN                 
	*       MOVE NEXTR               TO AGCMSUR-FUNCTION              
	*    GO TO 2050-CALL-AGCMSURIO.                                   
	**** IF AGCMLAP-COMPAY           = AGCMLAP-COMERN            <004>
	****    MOVE NEXTR               TO AGCMLAP-FUNCTION         <004>
	**** GO TO 2050-CALL-AGCMLAPIO.                              <004>
	* Calculate differences between commission earned/paid.
	**** COMPUTE WSAA-COMM-RECOVERED =                                
	***********(AGCMSUR-COMPAY - AGCMSUR-COMERN).                     
	****       (AGCMLAP-COMPAY - AGCMLAP-COMERN).                <004>
	*****MOVE AGCMSUR-COMERN         TO AGCMSUR-COMPAY.               
	**** MOVE AGCMLAP-COMERN         TO AGCMLAP-COMPAY.          <004>
	* Check if this is override commission or normal clawback         
	* commission and call LIFACMV using appropriate accounting rules. 
	*****IF AGCMSUR-OVRDCAT = 'O'                                     
	**** IF AGCMLAP-OVRDCAT = 'O'                                <004>
	****     PERFORM 4700-OVERRIDE-COMM-ACMV                     <004>
	**** ELSE                                                         
	****     PERFORM 4500-COMM-CLAWBACK-ACMV.                    <004>
	*****PERFORM 4500-COMM-CLAWBACK-ACMV.                             
	* Rewrite the AGCM record and read the next one.
	*    MOVE AGCMSURREC             TO AGCMSUR-FORMAT.               
	*    MOVE REWRT                  TO AGCMSUR-FUNCTION.             
	*    CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS.            
	*    IF AGCMSUR-STATUZ           NOT = O-K                        
	*        MOVE AGCMSUR-PARAMS     TO SYSR-PARAMS                   
	*        MOVE AGCMSUR-STATUZ     TO SYSR-STATUZ                   
	*    MOVE NEXTR                  TO AGCMSUR-FUNCTION.             
	*    GO TO 2050-CALL-AGCMSURIO.                                   
	****                                                         <006>
	**** MOVE AGCMLAPREC             TO AGCMLAP-FORMAT.          <006>
	**** MOVE READH                  TO AGCMLAP-FUNCTION.        <006>
	****                                                         <006>
	**** CALL 'AGCMLAPIO'            USING AGCMLAP-PARAMS.       <006>
	****                                                         <006>
	**** IF AGCMLAP-STATUZ           NOT = O-K                   <006>
	****     MOVE AGCMLAP-PARAMS     TO SYSR-PARAMS              <006>
	****     MOVE AGCMLAP-STATUZ     TO SYSR-STATUZ              <006>
	**** MOVE AGCMLAPREC             TO AGCMLAP-FORMAT.          <004>
	**** MOVE REWRT                  TO AGCMLAP-FUNCTION.        <004>
	****                                                         <004>
	**** CALL 'AGCMLAPIO'            USING AGCMLAP-PARAMS.       <004>
	****                                                         <004>
	**** IF AGCMLAP-STATUZ           NOT = O-K                   <004>
	****     MOVE AGCMLAP-PARAMS     TO SYSR-PARAMS              <004>
	****     MOVE AGCMLAP-STATUZ     TO SYSR-STATUZ              <004>
	* Read all the relevant AGCM record for this contract.            
	**** MOVE SPACE                  TO AGCMSUR-DATA-AREA        <008>
	**** MOVE OVRD-CHDRNUM           TO AGCMSUR-CHDRNUM          <008>
	**** MOVE OVRD-CHDRCOY           TO AGCMSUR-CHDRCOY          <008>
	**** MOVE OVRD-LIFE              TO AGCMSUR-LIFE             <008>
	**** MOVE OVRD-COVERAGE          TO AGCMSUR-COVERAGE         <008>
	**** MOVE OVRD-RIDER             TO AGCMSUR-RIDER            <008>
	**** MOVE OVRD-PLAN-SUFFIX       TO AGCMSUR-PLAN-SUFFIX      <008>
	**** MOVE AGCMLAP-AGNTNUM        TO AGCMSUR-AGNTNUM          <008>
	**** MOVE READH                  TO AGCMSUR-FUNCTION         <008>
	**** MOVE AGCMSURREC             TO AGCMSUR-FORMAT           <008>
	**** CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS        <008>
	**** IF AGCMSUR-STATUZ           NOT = O-K                   <008>
	****    MOVE AGCMSUR-STATUZ      TO SYSR-STATUZ              <008>
	****    MOVE AGCMSUR-PARAMS      TO SYSR-PARAMS              <008>
	**** END-IF                                                  <008>
	**** MOVE AGCMSUR-COMERN         TO AGCMSUR-COMPAY           <008>
	**** MOVE AGCMSURREC             TO AGCMSUR-FORMAT           <008>
	**** MOVE REWRT                  TO AGCMSUR-FUNCTION         <008>
	**** CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS        <008>
	**** IF AGCMSUR-STATUZ           NOT = O-K                   <008>
	****    MOVE AGCMSUR-STATUZ      TO SYSR-STATUZ              <008>
	****    MOVE AGCMSUR-PARAMS      TO SYSR-PARAMS              <008>
	**** END-IF                                                  <008>
	**** MOVE NEXTR                  TO AGCMLAP-FUNCTION.        <004>
	**** GO TO 2050-CALL-AGCMLAPIO.                              <004>
	*2090-EXIT.                                                       
	****  EXIT.                                                       
	* </pre>
	*/
protected void commClawBack2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2010();
				case callAgcmio2020: 
					callAgcmio2020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2010()
	{
		readT16884000();
		/* Read the first AGCM record for this contract.                   */
		agcmIO.setParams(SPACES);
		agcmIO.setChdrnum(ovrduerec1.chdrnum);
		agcmIO.setChdrcoy(ovrduerec1.chdrcoy);
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		agcmIO.setFormat(agcmrec);
	}

protected void callAgcmio2020()
	{
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			dbError5000();
		}
		/* If the end of the AGCM file has been reached or an AGCM         */
		/* record was read but not for the contract being processed,       */
		/* then exit the section.                                          */
		if (isEQ(agcmIO.getStatuz(), varcom.endp)
		|| isNE(agcmIO.getChdrcoy(), ovrduerec1.chdrcoy)
		|| isNE(agcmIO.getChdrnum(), ovrduerec1.chdrnum)) {
			return ;
		}
		/* for the AGCM record just read.                                  */
		if (isEQ(agcmIO.getChdrcoy(), ovrduerec1.chdrcoy)
		&& isEQ(agcmIO.getChdrnum(), ovrduerec1.chdrnum)
		&& isEQ(agcmIO.getLife(), ovrduerec1.life)
		&& isEQ(agcmIO.getCoverage(), ovrduerec1.coverage)
		&& isEQ(agcmIO.getRider(), ovrduerec1.rider)
		&& isEQ(agcmIO.getPlanSuffix(), ovrduerec1.planSuffix)
		&& isNE(agcmIO.getCompay(), agcmIO.getComern())) {
			updateAgcmFile2100();
		}
		/* Loop round to see if there any other AGCM records to            */
		/* process for the contract.                                       */
		agcmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmio2020);
	}

protected void updateAgcmFile2100()
	{
		readh2110();
		rewrt2120();
		writr2130();
	}

	/**
	* <pre>
	* previous processing loop on AGCM is not upset.                  
	* created with commission paid set to earned, valid-flag of "1"   
	* and correct tranno. Also create accounts records (ACMV).        
	* </pre>
	*/
protected void readh2110()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError5000();
		}
	}

protected void rewrt2120()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(ovrduerec1.effdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError5000();
		}
	}

protected void writr2130()
	{
		/* correct tranno, and new commission paid.                        */
		compute(wsaaCommRecovered, 2).set(sub(agcmIO.getCompay(), agcmIO.getComern()));
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			overrideCommAcmv4700();
		}
		else {
			commClawbackAcmv4500();
		}
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setPtdate(agcmIO.getPtdate());
		agcmdbcIO.setCedagent(agcmIO.getCedagent());
		agcmdbcIO.setOvrdcat(agcmIO.getOvrdcat());
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setTranno(ovrduerec1.tranno);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError5000();
		}
	}

protected void writeUtrn3000()
	{
		para3010();
	}

protected void para3010()
	{
		readTableT66477000();
		utrnIO.setParams(SPACES);
		utrnIO.setNowDeferInd(t6647rec.aloind);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setTranno(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setSvp(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setChdrcoy(ovrduerec1.chdrcoy);
		utrnIO.setChdrnum(ovrduerec1.chdrnum);
		utrnIO.setPlanSuffix(ovrduerec1.planSuffix);
		utrnIO.setLife(ovrduerec1.life);
		utrnIO.setCoverage(ovrduerec1.coverage);
		utrnIO.setRider(ovrduerec1.rider);
		/*    MOVE OVRD-EFFDATE           TO UTRN-MONIES-DATE.             */
		utrnIO.setMoniesDate(ovrduerec1.ptdate);
		utrnIO.setTranno(ovrduerec1.tranno);
		utrnIO.setCntcurr(ovrduerec1.cntcurr);
		utrnIO.setBatcactyr(ovrduerec1.acctyear);
		utrnIO.setBatcactmn(ovrduerec1.acctmonth);
		utrnIO.setSurrenderPercent(100);
		utrnIO.setFundAmount(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNofUnits(0);
		utrnIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		utrnIO.setUnitType(utrsIO.getUnitType());
		/*MOVE T5645-SACSCODE-01      TO UTRN-SACSCODE.                */
		/*MOVE T5645-SACSTYPE-01      TO UTRN-SACSTYP.                 */
		/*MOVE T5645-GLMAP-01         TO UTRN-GENLCDE.                 */
		/*  MOVE T5645-SACSCODE-05      TO UTRN-SACSCODE.           <004>*/
		/*  MOVE T5645-SACSTYPE-05      TO UTRN-SACSTYP.            <004>*/
		/*  MOVE T5645-GLMAP-05         TO UTRN-GENLCDE.            <004>*/
		/* Decide whether we are doing component level accounting or       */
		/*  not and act accordingly                                        */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			utrnIO.setSacscode(t5645rec.sacscode08);
			utrnIO.setSacstyp(t5645rec.sacstype08);
			utrnIO.setGenlcde(t5645rec.glmap08);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode07);
			utrnIO.setSacstyp(t5645rec.sacstype07);
			utrnIO.setGenlcde(t5645rec.glmap07);
		}
		utrnIO.setTransactionDate(ovrduerec1.tranDate);
		utrnIO.setTransactionTime(ovrduerec1.tranTime);
		utrnIO.setUser(ovrduerec1.user);
		utrnIO.setBatctrcde(ovrduerec1.trancode);
		utrnIO.setContractType(ovrduerec1.cnttype);
		utrnIO.setCrtable(ovrduerec1.crtable);
		/* MOVE OVRD-ALOIND            TO UTRN-NOW-DEFER-IND.           */
		/*   Obtain fund currency from T5515*/
		wsaaT5515Fund.set(utrsIO.getUnitVirtualFund());
		readT55154600();
		utrnIO.setFundCurrency(t5515rec.currcode);
		utrnIO.setSvp(1);
		utrnIO.setBatccoy(ovrduerec1.company);
		utrnIO.setBatcbrn(ovrduerec1.batcbrn);
		utrnIO.setBatcbatch(ovrduerec1.batcbatch);
		if (isEQ(utrnIO.getUnitType(), "A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		else {
			utrnIO.setUnitSubAccount("INIT");
		}
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			dbError5000();
		}
	}

protected void readT16884000()
	{
		para4010();
	}

protected void para4010()
	{
		/* Obtain transaction description from T1688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(ovrduerec1.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ovrduerec1.trancode);
		descIO.setLanguage(ovrduerec1.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError5000();
		}
		wsaaTransDesc.set(descIO.getLongdesc());
	}

protected void commClawbackAcmv4500()
	{
		para4510();
	}

protected void para4510()
	{
		if (isEQ(wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.*/
		lifacmvrec.rdocnum.set(ovrduerec1.chdrnum);
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.batccoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec1.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec1.trancode);
		lifacmvrec.batcactmn.set(ovrduerec1.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec1.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec1.batcbrn);
		lifacmvrec.origcurr.set(ovrduerec1.cntcurr);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.tranno.set(ovrduerec1.tranno);
		lifacmvrec.effdate.set(ovrduerec1.ptdate);
		lifacmvrec.tranref.set(ovrduerec1.chdrnum);
		lifacmvrec.user.set(ovrduerec1.user);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE OVRD-AGNTNUM           TO LIFA-RLDGACCT.                */
		/* MOVE AGCMLAP-AGNTNUM        TO LIFA-RLDGACCT            <008>*/
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec1.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec1.tranTime);
		lifacmvrec.termid.set(ovrduerec1.termid);
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.origamt.set(wsaaCommRecovered);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			callZorcompy9000();
		}
		/* Write a ACMV for the paid commission to be taken off.*/
		/*    MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-SIGN-02          TO LIFA-GLSIGN.                  */
		/*    MOVE T5645-GLMAP-02         TO LIFA-GLCODE.                  */
		/*    MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.                  */
		/*    MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.            */
		/*    MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.               */
		/*    MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.           */
		/*    MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.              */
		/*    MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.                */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.        */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.                */
		/*    MOVE OVRD-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).      */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			wsaaRldgChdrnum.set(ovrduerec1.chdrnum);
			wsaaRldgLife.set(ovrduerec1.life);
			wsaaRldgCoverage.set(ovrduerec1.coverage);
			wsaaRldgRider.set(ovrduerec1.rider);
			wsaaPlansuff.set(ovrduerec1.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec1.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.rldgacct.set(ovrduerec1.chdrnum);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
	}

protected void readT55154600()
	{
		para4610();
	}

protected void para4610()
	{
		/* Read T5515 with unit fund as the key.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		/*    MOVE OVRD-COMPANY           TO ITEM-ITEMCOY.                 */
		itemIO.setItemcoy(ovrduerec1.chdrcoy);
		itemIO.setItemtabl(t5515);
		/* MOVE UTRS-UNIT-VIRTUAL-FUND TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(wsaaT5515Fund);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError5000();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
	}

protected void overrideCommAcmv4700()
	{
		para4710();
	}

protected void para4710()
	{
		if (isEQ(wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.                       */
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rdocnum.set(ovrduerec1.chdrnum);
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.batccoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec1.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec1.trancode);
		lifacmvrec.batcactmn.set(ovrduerec1.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec1.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec1.batcbrn);
		lifacmvrec.tranno.set(ovrduerec1.tranno);
		lifacmvrec.origcurr.set(ovrduerec1.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec1.ptdate);
		lifacmvrec.tranref.set(ovrduerec1.chdrnum);
		lifacmvrec.user.set(ovrduerec1.user);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE OVRD-AGNTNUM           TO LIFA-RLDGACCT.           <004>*/
		/* MOVE AGCMLAP-AGNTNUM        TO LIFA-RLDGACCT            <008>*/
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec1.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec1.tranTime);
		lifacmvrec.termid.set(ovrduerec1.termid);
		lifacmvrec.origamt.set(wsaaCommRecovered);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
		/* Write a ACMV for the paid commission to be taken off.*/
		/*    MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-SIGN-04          TO LIFA-GLSIGN.                  */
		/*    MOVE T5645-GLMAP-04         TO LIFA-GLCODE.                  */
		/*    MOVE T5645-CNTTOT-04        TO LIFA-CONTOT.                  */
		/*    MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.            */
		/*    MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.               */
		/*    MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.           */
		/*    MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.              */
		/*    MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.                */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.        */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.                */
		/*    MOVE OVRD-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).      */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			wsaaRldgChdrnum.set(ovrduerec1.chdrnum);
			wsaaRldgLife.set(ovrduerec1.life);
			wsaaRldgCoverage.set(ovrduerec1.coverage);
			wsaaRldgRider.set(ovrduerec1.rider);
			wsaaPlansuff.set(ovrduerec1.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec1.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			lifacmvrec.rldgacct.set(ovrduerec1.chdrnum);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
	}

protected void dbError5000()
	{
		db5000();
		dbExit5090();
	}

protected void db5000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit5090()
	{
		ovrduerec1.statuz.set(varcom.bomb);
		exit090();
	}

protected void readT56886000()
	{
		read6000();
	}

protected void read6000()
	{
		itdmIO.setItemcoy(ovrduerec1.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(ovrduerec1.cnttype);
		itdmIO.setItmfrm(ovrduerec1.ptdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
 
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError5000();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec1.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), ovrduerec1.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(ovrduerec1.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			dbError5000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readTableT66477000()
	{
		start7010();
	}

protected void start7010()
	{
		/*    Read the Unit Linked Contract Details Table                  */
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
 
		itdmIO.setItemcoy(ovrduerec1.chdrcoy);
		itdmIO.setItemtabl(t6647);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ovrduerec1.trancode);
		stringVariable1.addExpression(ovrduerec1.cnttype);
		stringVariable1.addExpression(" ");
		stringVariable1.setStringInto(itdmIO.getItemitem());
		itdmIO.setItmfrm(ovrduerec1.occdate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError5000();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec1.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6647)
		|| isNE(subString(itdmIO.getItemitem(), 1, 4), ovrduerec1.trancode)
		|| isNE(subString(itdmIO.getItemitem(), 5, 3), ovrduerec1.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(ovrduerec1.chdrcoy);
			itdmIO.setItemtabl(t6647);
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(ovrduerec1.trancode);
			stringVariable2.addExpression(ovrduerec1.cnttype);
			stringVariable2.addExpression(" ");
			stringVariable2.setStringInto(itdmIO.getItemitem());
			itdmIO.setItmfrm(ovrduerec1.occdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError5000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	************************************                      <R96REA>
	* </pre>
	*/
protected void processReassurance8000()
	{
		trmreas8001();
	}

	/**
	* <pre>
	************************************                      <R96REA>
	* </pre>
	*/
protected void trmreas8001()
	{
		wsaaBatckey.batcBatcpfx.set("BA");
		wsaaBatckey.batcBatccoy.set(ovrduerec1.chdrcoy);
		wsaaBatckey.batcBatcbrn.set(ovrduerec1.batcbrn);
		wsaaBatckey.batcBatcactyr.set(ovrduerec1.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec1.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec1.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec1.batcbatch);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(ovrduerec1.chdrcoy);
		trmreasrec.chdrnum.set(ovrduerec1.chdrnum);
		trmreasrec.life.set(ovrduerec1.life);
		trmreasrec.coverage.set(ovrduerec1.coverage);
		trmreasrec.rider.set(ovrduerec1.rider);
		trmreasrec.planSuffix.set(ovrduerec1.planSuffix);
		trmreasrec.cnttype.set(ovrduerec1.cnttype);
		trmreasrec.crtable.set(ovrduerec1.crtable);
		trmreasrec.polsum.set(ovrduerec1.polsum);
		trmreasrec.effdate.set(ovrduerec1.ptdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(ovrduerec1.tranno);
		trmreasrec.language.set(ovrduerec1.language);
		trmreasrec.billfreq.set(ovrduerec1.billfreq);
		trmreasrec.ptdate.set(ovrduerec1.ptdate);
		trmreasrec.origcurr.set(ovrduerec1.cntcurr);
		trmreasrec.acctcurr.set(ovrduerec1.cntcurr);
		trmreasrec.crrcd.set(ovrduerec1.crrcd);
		trmreasrec.convUnits.set(ZERO);
		trmreasrec.jlife.set(wsaaJlife);
		trmreasrec.singp.set(ovrduerec1.instprem);
		trmreasrec.oldSumins.set(ovrduerec1.sumins);
		trmreasrec.pstatcode.set(ovrduerec1.pstatcode);
		trmreasrec.clmPercent.set(ZERO);
		trmreasrec.newSumins.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			dbError5000();
		}
	}

protected void callZorcompy9000()
	{
		start9010();
	}

protected void start9010()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set("Y");
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(agcmIO.getEfdate());
		zorlnkrec.ptdate.set(agcmIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(lifacmvrec.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			dbError5000();
		}
	}
}
