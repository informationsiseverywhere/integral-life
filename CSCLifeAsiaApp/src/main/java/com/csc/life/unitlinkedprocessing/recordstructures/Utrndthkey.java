package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:48
 * Description:
 * Copybook name: UTRNDTHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrndthkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrndthFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrndthKey = new FixedLengthStringData(64).isAPartOf(utrndthFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrndthChdrcoy = new FixedLengthStringData(1).isAPartOf(utrndthKey, 0);
  	public FixedLengthStringData utrndthChdrnum = new FixedLengthStringData(8).isAPartOf(utrndthKey, 1);
  	public FixedLengthStringData utrndthLife = new FixedLengthStringData(2).isAPartOf(utrndthKey, 9);
  	public FixedLengthStringData utrndthCoverage = new FixedLengthStringData(2).isAPartOf(utrndthKey, 11);
  	public FixedLengthStringData utrndthRider = new FixedLengthStringData(2).isAPartOf(utrndthKey, 13);
  	public PackedDecimalData utrndthPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrndthKey, 15);
  	public FixedLengthStringData utrndthUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrndthKey, 18);
  	public FixedLengthStringData utrndthUnitType = new FixedLengthStringData(1).isAPartOf(utrndthKey, 22);
  	public PackedDecimalData utrndthProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(utrndthKey, 23);
  	public PackedDecimalData utrndthTranno = new PackedDecimalData(5, 0).isAPartOf(utrndthKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(utrndthKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrndthFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrndthFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}