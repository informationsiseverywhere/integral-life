/*
 * File: Cr542.java
 * Date: 30 August 2009 2:59:19
 * Author: $Id$
 * 
 * Class transformed from CR542.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator;

import com.csc.fsu.general.procedures.Dtc2call;
import com.csc.life.unitlinkedprocessing.batchprograms.Br542;
import com.csc.smart.procedures.Passparm;
import com.csc.smart400framework.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cr542 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData datc2 = new FixedLengthStringData(80);
	private FixedLengthStringData intd1 = new FixedLengthStringData(8);
	private FixedLengthStringData conj = new FixedLengthStringData(860);
	private FixedLengthStringData sign = new FixedLengthStringData(1);

	public Cr542() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					intd1.set(subString(params, 113, 8));
					datc2.setSub1String(1, 4, "    ");
					datc2.setSub1String(5, 4, "    ");
					datc2.setSub1String(9, 8, intd1);
					datc2.setSub1String(25, 2, "DY");
					datc2.setSub1String(27, 5, 3);
					sign.set("-");
					callProgram(Dtc2call.class, new Object[] {datc2, sign});
					
					String parmChdrnumfrm = subString(conj, 1, 8).toString();
					String parmChdrnumto = subString(conj, 9, 8).toString();
					Integer parmIntd1 = intd1.toInt();
					Character parmCoy = subString(params, 6, 1).toString().charAt(0);
					
					//ChdrpfDAO chdrpfDAO = DAOFactory.getDAO(ChdrpfDAO.class);
					ChdrpfDAO chdrpfDAO = DAOFactory.getChdrpfDAO();
					Iterator<Chdrpf> chdrpfIterator;
					chdrpfIterator = chdrpfDAO.findContractsByStatement(parmChdrnumfrm, parmChdrnumto, parmIntd1, parmCoy).iterator();

					callProgram(Br542.class, new Object[] {params, chdrpfIterator});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
