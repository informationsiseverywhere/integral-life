/*
 * File: Br506.java
 * Date: 29 August 2009 22:10:31
 * Author: Quipoz Limited
 * 
 * Class transformed from BR506.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.recordstructures.Srvunitcpy;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.reports.Rr506Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* BR506 - Fund Valuation Proposals Suspense Report
*
*
*****************************************************************
* </pre>
*/
public class Br506 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrpf1rs = null;
	private java.sql.PreparedStatement sqlchdrpf1ps = null;
	private java.sql.Connection sqlchdrpf1conn = null;
	private String sqlchdrpf1 = "";
	private Rr506Report printerFile = new Rr506Report();
	private SortFileDAM sortFile = new SortFileDAM("BR506SRT");
	private FixedLengthStringData printerRec = new FixedLengthStringData(320);

	private FixedLengthStringData sortRec = new FixedLengthStringData(58);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortRec, 0);
	private PackedDecimalData sortOccdate = new PackedDecimalData(8, 0).isAPartOf(sortRec, 8);
	private PackedDecimalData sortRundte = new PackedDecimalData(8, 0).isAPartOf(sortRec, 13);
	private PackedDecimalData sortSingp = new PackedDecimalData(17, 2).isAPartOf(sortRec, 18);
	private PackedDecimalData sortSacscurbal = new PackedDecimalData(17, 2).isAPartOf(sortRec, 27);
	private FixedLengthStringData sortCrtable = new FixedLengthStringData(4).isAPartOf(sortRec, 36);
	private FixedLengthStringData sortStatcode = new FixedLengthStringData(2).isAPartOf(sortRec, 40);
	private FixedLengthStringData sortCntcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 42);
	private FixedLengthStringData sortUalfnd = new FixedLengthStringData(4).isAPartOf(sortRec, 45);
	private PackedDecimalData sortUalprc = new PackedDecimalData(17, 2).isAPartOf(sortRec, 49);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR506");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaUalfnd = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private ZonedDecimalData wsaaContractTot = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaPremTot = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaFundTot = new ZonedDecimalData(11, 2);
	private static final String itemrec = "ITEMREC";
	private static final String descrec = "DESCREC";
	private static final String acblrec = "ACBLREC";
	private static final String unltunlrec = "UNLTUNLREC";
	private static final String covtunlrec = "COVTUNLREC";
		/* ERRORS */
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5515 = "T5515";
	private static final String t5645 = "T5645";

	private FixedLengthStringData wsaaEofSort = new FixedLengthStringData(1).init("N");
	private Validator endOfSortFile = new Validator(wsaaEofSort, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaaSrvuLife = new FixedLengthStringData(2);

	private FixedLengthStringData rr506H01 = new FixedLengthStringData(78);
	private FixedLengthStringData rr506h01O = new FixedLengthStringData(78).isAPartOf(rr506H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr506h01O, 0);
	private FixedLengthStringData ualfnd = new FixedLengthStringData(4).isAPartOf(rr506h01O, 10);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rr506h01O, 14);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rr506h01O, 44);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rr506h01O, 47);
	private FixedLengthStringData ultype = new FixedLengthStringData(1).isAPartOf(rr506h01O, 77);

	private FixedLengthStringData rr506D01 = new FixedLengthStringData(85);
	private FixedLengthStringData rr506d01O = new FixedLengthStringData(85).isAPartOf(rr506D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr506d01O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rr506d01O, 8);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(rr506d01O, 12);
	private FixedLengthStringData rundte = new FixedLengthStringData(10).isAPartOf(rr506d01O, 29);
	private ZonedDecimalData ualprc = new ZonedDecimalData(17, 2).isAPartOf(rr506d01O, 39);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(rr506d01O, 56);
	private FixedLengthStringData statcode = new FixedLengthStringData(2).isAPartOf(rr506d01O, 66);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2).isAPartOf(rr506d01O, 68);

	private FixedLengthStringData rr506S01 = new FixedLengthStringData(40);
	private FixedLengthStringData rr506s01O = new FixedLengthStringData(40).isAPartOf(rr506S01, 0);
	private ZonedDecimalData totccount = new ZonedDecimalData(9, 0).isAPartOf(rr506s01O, 0);
	private ZonedDecimalData totprm = new ZonedDecimalData(13, 2).isAPartOf(rr506s01O, 9);
	private ZonedDecimalData totfundval = new ZonedDecimalData(18, 5).isAPartOf(rr506s01O, 22);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-CHDRPF */
	private FixedLengthStringData sqlChdrrec = new FixedLengthStringData(18);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlChdrrec, 0);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlChdrrec, 8);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlChdrrec, 11);
	private PackedDecimalData sqlOccdate = new PackedDecimalData(8, 0).isAPartOf(sqlChdrrec, 13);
		/* COPY TR386REC.                                       <LA3235>*/
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Coverage transactions - unit linked*/
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Srvunitcpy srvunitcpy = new Srvunitcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2108, 
		exit2109
	}

	public Br506() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		wsaaUalfnd.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaSrvuLife.set(srvunitcpy.lifeVal);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		sqlchdrpf1 = " SELECT  CHDRNUM, CNTCURR, STATCODE, OCCDATE" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE VALIDFLAG = '3'" +
" AND SERVUNIT = ?";
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlchdrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrpf1ps = getAppVars().prepareStatementEmbeded(sqlchdrpf1conn, sqlchdrpf1, "CHDRPF");
			getAppVars().setDBString(sqlchdrpf1ps, 1, wsaaSrvuLife);
			sqlchdrpf1rs = getAppVars().executeQuery(sqlchdrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

	/**
	* <pre>
	*1020-READ-TR386.                                         <LA3235>
	*--- Read TR386 table to get screen literals                      
	**** MOVE ITEMREC                TO ITEM-FORMAT.          <LA3235>
	**** MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>
	**** MOVE BSPR-COMPANY           TO ITEM-ITEMCOY.         <LA3235>
	**** MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>
	**** MOVE BSSC-LANGUAGE          TO ITEM-ITEMITEM.        <LA3235>
	**** MOVE WSAA-PROG              TO ITEM-ITEMITEM(2:5).   <LA3235>
	**** MOVE READR                  TO ITEM-FUNCTION.        <LA3235>
	****                                                      <LA3235>
	**** CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>
	****                                                      <LA3235>
	**** IF  ITEM-STATUZ             NOT = O-K                <LA3235>
	****     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <LA3235>
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <LA3235>
	****     PERFORM 600-FATAL-ERROR                          <LA3235>
	**** END-IF.                                              <LA3235>
	****                                                      <LA3235>
	**** MOVE ITEM-GENAREA        TO TR386-TR386-REC.         <LA3235>
	****                                                      <LA3235>
	*1090-EXIT.                                                       
	****  EXIT.                                                       
	* </pre>
	*/
protected void readFile2000()
	{
		/*READ-FILE*/
		sortFile.openOutput();
		retrieveData2050();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortUalfnd, true);
		fs1.addSortKey(sortChdrnum, true);
		fs1.sort();
		sortFile.openInput();
		retrieveSorted2200();
		sortFile.close();
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void retrieveData2050()
	{
		/*RETRIEVAL*/
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			releaseToSort2100();
		}
		
		/*EXIT*/
	}

protected void releaseToSort2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					release2101();
				case eof2108: 
					eof2108();
				case exit2109: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void release2101()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrpf1rs)) {
				getAppVars().getDBObject(sqlchdrpf1rs, 1, sqlChdrnum);
				getAppVars().getDBObject(sqlchdrpf1rs, 2, sqlCntcurr);
				getAppVars().getDBObject(sqlchdrpf1rs, 3, sqlStatcode);
				getAppVars().getDBObject(sqlchdrpf1rs, 4, sqlOccdate);
			}
			else {
				goTo(GotoLabel.eof2108);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		readAcbl2600();
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit2109);
		}
		unltunlIO.setParams(SPACES);
		unltunlIO.setChdrcoy(bsprIO.getCompany());
		unltunlIO.setChdrnum(sqlChdrnum);
		unltunlIO.setLife(SPACES);
		unltunlIO.setCoverage(SPACES);
		unltunlIO.setRider(SPACES);
		unltunlIO.setSeqnbr(ZERO);
		unltunlIO.setFormat(unltunlrec);
		unltunlIO.setFunction(varcom.begn);
		while ( !(isEQ(unltunlIO.getStatuz(),varcom.endp))) {
			readUnltunl2700();
		}
		
		goTo(GotoLabel.exit2109);
	}

protected void eof2108()
	{
		wsaaEofSort.set("Y");
	}

protected void retrieveSorted2200()
	{
		/*STARTED*/
		wsaaContractTot.set(0);
		wsaaEofSort.set("N");
		while ( !(endOfSortFile.isTrue())) {
			printReport2250();
		}
		
		/* To print Summary total of details*/
		if (isGT(wsaaContractTot,0)) {
			printSummary2900();
		}
		/*EXIT*/
	}

protected void printReport2250()
	{
			print2251();
		}

protected void print2251()
	{
		sortFile.read(sortRec);
		if (sortFile.isAtEnd()) {
			wsaaEofSort.set("Y");
			return ;
		}
		/* Check for change in Fund Code*/
		if (isNE(wsaaUalfnd,SPACES)) {
			if (isNE(wsaaUalfnd,sortUalfnd)) {
				printSummary2900();
			}
		}
		/* Check for change in Fund Code or New page*/
		if (isNE(wsaaUalfnd,sortUalfnd)
		|| newPageReq.isTrue()) {
			headerDetails2800();
		}
		chdrnum.set(sortChdrnum);
		crtable.set(sortCrtable);
		singp.set(sortSingp);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortRundte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rundte.set(datcon1rec.extDate);
		ualprc.set(sortUalprc);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sortOccdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		occdate.set(datcon1rec.extDate);
		statcode.set(sortStatcode);
		sacscurbal.set(sortSacscurbal);
		printerFile.printRr506d01(rr506D01);
		/*                             INDICATORS INDIC-AREA    <S19FIX>*/
		/* Add detail total to Summary total.*/
		wsaaContractTot.add(1);
		wsaaPremTot.add(sortSingp);
		wsaaFundTot.add(sortUalprc);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void readAcbl2600()
	{
			readAcbl2610();
		}

protected void readAcbl2610()
	{
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(bsprIO.getCompany());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setOrigcurr(sqlCntcurr);
		acblIO.setRldgacct(sqlChdrnum);
		acblIO.setFormat(acblrec);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		compute(sortSacscurbal, 2).set(mult(acblIO.getSacscurbal(),-1));
	}

protected void readUnltunl2700()
	{
			readUnltunl2710();
		}

protected void readUnltunl2710()
	{
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(),varcom.oK)
		&& isNE(unltunlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		if (isNE(bsprIO.getCompany(),unltunlIO.getChdrcoy())
		|| isNE(sqlChdrnum,unltunlIO.getChdrnum())
		|| isEQ(unltunlIO.getStatuz(),varcom.endp)) {
			unltunlIO.setStatuz(varcom.endp);
			return ;
		}
		covtunlIO.setParams(SPACES);
		covtunlIO.setChdrcoy(unltunlIO.getChdrcoy());
		covtunlIO.setChdrnum(unltunlIO.getChdrnum());
		covtunlIO.setLife(unltunlIO.getLife());
		covtunlIO.setCoverage(unltunlIO.getCoverage());
		covtunlIO.setRider(unltunlIO.getRider());
		covtunlIO.setSeqnbr(ZERO);
		covtunlIO.setFunction(varcom.begn);
		covtunlIO.setFormat(covtunlrec);
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(),varcom.oK)
		|| isNE(covtunlIO.getChdrcoy(),unltunlIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(),unltunlIO.getChdrnum())
		|| isNE(covtunlIO.getLife(),unltunlIO.getLife())
		|| isNE(covtunlIO.getCoverage(),unltunlIO.getCoverage())
		|| isNE(covtunlIO.getRider(),unltunlIO.getRider())) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		sortCrtable.set(covtunlIO.getCrtable());
		sortRundte.set(covtunlIO.getReserveUnitsDate());
		if (isEQ(covtunlIO.getBillfreq(),"00")) {
			sortSingp.set(covtunlIO.getSingp());
		}
		else {
			sortSingp.set(covtunlIO.getInstprem());
		}
		/* Check FUND Code and Transaction Value.*/
		for (sub.set(1); !(isGT(sub,10)); sub.add(1)){
			if (isNE(unltunlIO.getUalfnd(sub),SPACES)) {
				sortChdrnum.set(sqlChdrnum);
				sortOccdate.set(sqlOccdate);
				sortStatcode.set(sqlStatcode);
				sortCntcurr.set(sqlCntcurr);
				sortUalfnd.set(unltunlIO.getUalfnd(sub));
				if (isEQ(unltunlIO.getPercOrAmntInd(),"A")) {
					sortUalprc.set(unltunlIO.getUalprc(sub));
				}
				else {
					compute(sortUalprc, 4).set(mult(sortSingp,(div(unltunlIO.getUalprc(sub),100))));
				}
				sortFile.write(sortRec);
			}
		}
		unltunlIO.setFunction(varcom.nextr);
	}

protected void headerDetails2800()
	{
		headerDetails2810();
	}

protected void headerDetails2810()
	{
		initialize(indicArea);
		/* Read T5515 with Fund Code to retrieve the Fund Currency and*/
		/* Unit Type*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(sortUalfnd);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5515);
		descIO.setDescitem(sortUalfnd);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		ualfnd.set(sortUalfnd);
		wsaaUalfnd.set(sortUalfnd);
		descrip.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(t5515rec.currcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		currcode.set(t5515rec.currcode);
		currencynm.set(descIO.getLongdesc());
		ultype.set(t5515rec.unitType);
		if (isEQ(t5515rec.unitType,"I")) {
			indicTable[11].set("1");
			indicTable[12].set("0");
			indicTable[13].set("0");
		}
		else {
			if (isEQ(t5515rec.unitType,"A")) {
				indicTable[12].set("1");
				indicTable[11].set("0");
				indicTable[13].set("0");
			}
			else {
				if (isEQ(t5515rec.unitType,"B")) {
					indicTable[13].set("1");
					indicTable[11].set("0");
					indicTable[12].set("0");
				}
			}
		}
		printerFile.printRr506h01(rr506H01);
		wsaaOverflow.set("N");
	}

protected void printSummary2900()
	{
		/*PRINT-SUMMARY*/
		totccount.set(wsaaContractTot);
		totprm.set(wsaaPremTot);
		totfundval.set(wsaaFundTot);
		printerFile.printRr506s01(rr506S01);
		/*                              INDICATORS INDIC-AREA    <S19FIX>*/
		initialize(wsaaContractTot);
		initialize(wsaaPremTot);
		initialize(wsaaFundTot);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		printerFile.close();
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
