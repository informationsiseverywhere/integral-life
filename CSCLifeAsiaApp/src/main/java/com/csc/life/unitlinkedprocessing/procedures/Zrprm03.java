/*
 * File: Zrprm03.java
 * Date: 30 August 2009 2:56:21
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRPRM03.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5533rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*          NEW PREMIUM CALCULATION METHOD 03
*                (Investment Linked)
*
* Note : Clone from PRMPM03.
*
* This new program is similar to PRMPM03 except for the following
* changes :-
*
* - A new section to check if any LEXT records exist. If exist,
*   change the factor to LEXT-ZNADJPERC / 100.
*
* - To validate CPRM-CALC-PREM against the casual limits of T5533
*   CPRM-FUNCTION is 'TOPU'.
*
*****************************************************************
* </pre>
*/
public class Zrprm03 extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrprm03.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZRPRM03";

	private FixedLengthStringData wsaaT5533Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5533Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 0);
	private FixedLengthStringData wsaaT5533Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 4);
	private PackedDecimalData wsaaAge00 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaAge01 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaDob00 = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaDob01 = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaYoungerLife = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaYoungerDob = new PackedDecimalData(8, 0).init(ZERO);
	private ZonedDecimalData wsaaRndfact = new ZonedDecimalData(6, 0);
	private PackedDecimalData wsaaTotPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(7, 4);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(6, 0).init(ZERO);
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler, 12).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 13).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler4, 14).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(17).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler6, 15);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaDateToUse = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
		/* FORMATS */
	private static final String lextrec = "LEXTREC";
		/* ERRORS */
	private static final String g070 = "G070";
	private static final String g071 = "G071";
	private static final String g072 = "G072";
	private static final String h068 = "H068";
	private static final String t041 = "T041";
		/* TABLES */
	private static final String t5646 = "T5646";
	private static final String t5533 = "T5533";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5533rec t5533rec = new T5533rec();
	private T5646rec t5646rec = new T5646rec();
	private Premiumrec premiumrec = new Premiumrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		para660
	}

	public Zrprm03() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void startSubr010()
	{
		/*INIT*/
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		main100();
		/*EXIT*/
		exitProgram();
	}

protected void main100()
	{
		para100();
	}

protected void para100()
	{
		initialize200();
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			readTablT5533250();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			readLifeDetails300();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			readTablT5646400();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			premiumMethods500();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			determineFactor600();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			a100ReadLext();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			suminRounded700();
		}
	}

protected void initialize200()
	{
		/*PARA*/
		wsaaYoungerLife.set(ZERO);
		wsaaFactor.set(ZERO);
		wsaaTotPremium.set(ZERO);
		wsaaAge00.set(ZERO);
		wsaaAge01.set(ZERO);
		wsaaDob00.set(ZERO);
		wsaaDob01.set(ZERO);
		wsaaDateToUse.set(ZERO);
		wsaaRndfact.set(ZERO);
		wsaaSub.set(ZERO);
		/*EXIT*/
	}

protected void readTablT5533250()
	{
		para250();
	}

protected void para250()
	{
		/* CHECK PREMIUM AGAINST THE LIMITS SET IN T5533 ACCORDING TO*/
		/* THE RANGE LIMITS FOR BILLING FREQUENCY.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5533);
		wsaaT5533Crtable.set(premiumrec.crtable);
		wsaaT5533Currcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5533Item);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(wsaaT5533Item, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5533)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			premiumrec.statuz.set(g071);
			return ;
		}
		else {
			t5533rec.t5533Rec.set(itdmIO.getGenarea());
		}
		/* Process to find MAX/MIN limits for frequency required.*/
		if (isNE(premiumrec.function, "TOPU")) {
			wsaaRndfact.set(t5533rec.rndfact);
			for (wsaaSub.set(1); !(isGT(wsaaSub, 8)
			|| isEQ(premiumrec.billfreq, t5533rec.frequency[wsaaSub.toInt()])); wsaaSub.add(1)){
				/*    If a range is not found or premium is out with the range*/
				/*    then error.*/
				/*    If this program is called by a INCREASE module, skip this*/
				/*    test because the CPRM-CALC-PREM holds the increased part of*/
				/*    the premium and not the actual premium.*/
				findBillfreq270();
			}
			if (isGT(wsaaSub, 8)) {
				premiumrec.statuz.set(g072);
			}
			else {
				if (((isLT(premiumrec.calcPrem, t5533rec.cmin[wsaaSub.toInt()]))
				|| (isGT(premiumrec.calcPrem, t5533rec.cmax[wsaaSub.toInt()])))
				&& isNE(premiumrec.function, "INCR")) {
					premiumrec.statuz.set(g070);
				}
			}
		}
		if (isEQ(premiumrec.function, "TOPU")
		&& ((isLT(premiumrec.calcPrem, t5533rec.casualContribMin)
		|| isGT(premiumrec.calcPrem, t5533rec.casualContribMax)))) {
			premiumrec.statuz.set(g070);
		}
	}

protected void findBillfreq270()
	{
		/*READ*/
		/*** Dummy paragraph to find billing frequency.*/
		/*EXIT*/
	}

protected void readLifeDetails300()
	{
		read310();
	}

protected void read310()
	{
		/* Obtain jointlife and life date of birth*/
		/* from the LIFELNB file.*/
		/* Read the first life.*/
		lifelnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lifelnbIO.setChdrnum(premiumrec.chdrChdrnum);
		lifelnbIO.setLife(premiumrec.lifeLife);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError9000();
		}
		wsaaAge00.set(premiumrec.lage);
		wsaaDob00.set(lifelnbIO.getCltdob());
		/* Ascertain if joint life present*/
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.oK)) {
			wsaaAge01.set(premiumrec.jlage);
			wsaaDob01.set(lifelnbIO.getCltdob());
		}
		/*Store the younger of the life and joint life ages(when joint*/
		/*life present) or the main life age if joint life not present*/
		if (isLT(wsaaAge01, wsaaAge00)
		&& isNE(wsaaAge01, ZERO)) {
			wsaaYoungerLife.set(wsaaAge01);
			wsaaYoungerDob.set(wsaaDob01);
		}
		else {
			wsaaYoungerDob.set(wsaaDob00);
			wsaaYoungerLife.set(wsaaAge00);
		}
	}

protected void readTablT5646400()
	{
		para410();
	}

protected void para410()
	{
		/* Read table T5646 to see if there is an age limit present.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5646);
		itdmIO.setItemitem(premiumrec.crtable);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(premiumrec.crtable, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5646)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			premiumrec.statuz.set(h068);
		}
		else {
			t5646rec.t5646Rec.set(itdmIO.getGenarea());
		}
	}

protected void premiumMethods500()
	{
		para510();
	}

protected void para510()
	{
		/* Age limit not present in table T5646 so use the premium*/
		/* cessation date from linkage area for further calculations.*/
		if (isEQ(t5646rec.agelimit, ZERO)
		|| isEQ(premiumrec.billfreq, "00")) {
			wsaaDateToUse.set(premiumrec.termdate);
			calcNumInstalments550();
			return ;
		}
		/* Age limit present - calculate younger life's birthday*/
		/* (obtained from life file) at age limit. Compare this date*/
		/* with the premium cessation date and use the lower of these*/
		/* for further calculations*/
		datcon2rec.intDate1.set(wsaaYoungerDob);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t5646rec.agelimit);
		datcon2rec.function.set(SPACES);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError9000();
		}
		/* We now have the Clients birthday date at the agelimit*/
		/* taken from T5646 (stored in WSAA-DATE-TO-USE)*/
		wsaaDateToUse.set(datcon2rec.intDate2);
		/* Check that the Date to use is not > Risk Comm Date*/
		/* If so display error message 'T5646 Age Limit < ANB'*/
		if (isLT(wsaaDateToUse, premiumrec.effectdt)) {
			premiumrec.statuz.set(t041);
			return ;
		}
		/* Take the earliest date.*/
		if (isLT(premiumrec.termdate, wsaaDateToUse)) {
			wsaaDateToUse.set(premiumrec.termdate);
		}
		calcNumInstalments550();
	}

protected void calcNumInstalments550()
	{
		para560();
	}

protected void para560()
	{
		/* Calculate no of instalments to be paid by calling DATCON3*/
		/* passing the premium cessation date, the risk commencement*/
		/* date (or date calculated ) and the billing frequency.*/
		if (isEQ(premiumrec.billfreq, "00")
		|| isEQ(premiumrec.function, "TOPU")) {
			wsaaTotPremium.set(premiumrec.calcPrem);
			return ;
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(wsaaDateToUse);
		datcon3rec.frequency.set(premiumrec.billfreq);
		datcon3rec.function.set(SPACES);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		/* Round up the frequency factor to the higher whole unit.*/
		compute(wsaaFreqFactor, 5).set((add(datcon3rec.freqFactor, 0.99999)));
		compute(wsaaTotPremium, 2).set(mult(premiumrec.calcPrem, wsaaFreqFactor));
	}

protected void determineFactor600()
	{
		/*PARA*/
		/* Find the sum assured factor which will determine the sum*/
		/* assured according to the Premium.*/
		wsaaSub.set(0);
		findFactor650();
		if (isGT(wsaaSub, 12)) {
			premiumrec.statuz.set(h068);
		}
		else {
			wsaaFactor.set(t5646rec.factorsa[wsaaSub.toInt()]);
		}
		/*EXIT*/
	}

protected void findFactor650()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para660: 
					para660();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para660()
	{
		/* Routine to search through table T5646 to find factor accord-*/
		/* to the age input.*/
		wsaaSub.add(1);
		if (isLTE(wsaaSub, 12)) {
			/*       IF WSAA-YOUNGER-LIFE  NOT < T5646-AGFRM(WSAA-SUB)         */
			/*                         AND NOT > T5646-AGTO(WSAA-SUB)          */
			if (isGTE(wsaaYoungerLife, t5646rec.ageIssageFrm[wsaaSub.toInt()])
			&& isLTE(wsaaYoungerLife, t5646rec.ageIssageTo[wsaaSub.toInt()])) {
				/*NEXT_SENTENCE*/
			}
			else {
				goTo(GotoLabel.para660);
			}
		}
		/*EXIT*/
	}

protected void suminRounded700()
	{
		para710();
	}

protected void para710()
	{
		/* Multiply the tot premium over the term by the factor found.*/
		compute(wsaaTotPremium, 4).set(mult(wsaaTotPremium, wsaaFactor));
		/* Round up the Sum insured amount to its nearest whole unit.*/
		wsaaRoundNum.set(wsaaTotPremium);
		if (isEQ(wsaaRndfact, 1)
		|| isEQ(wsaaRndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
				if (isEQ(wsaaRndfact, 10)) {
					if (isLT(wsaaRound1, 5)) {
						wsaaRound1.set(0);
					}
					else {
						wsaaRoundNum.add(10);
						wsaaRound1.set(0);
					}
				}
			}
		}
		if (isEQ(wsaaRndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(wsaaRndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound100.set(0);
			}
		}
		premiumrec.sumin.set(wsaaRoundNum);
	}

protected void a100ReadLext()
	{
		a110ReadLext();
	}

protected void a110ReadLext()
	{
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(), varcom.oK)
		&& isEQ(premiumrec.chdrChdrcoy, lextIO.getChdrcoy())
		&& isEQ(premiumrec.chdrChdrnum, lextIO.getChdrnum())
		&& isEQ(premiumrec.lifeLife, lextIO.getLife())
		&& isEQ(premiumrec.covrCoverage, lextIO.getCoverage())
		&& isEQ(premiumrec.covrRider, lextIO.getRider())
		&& isNE(lextIO.getZnadjperc(), ZERO)) {
			compute(wsaaFactor, 5).setRounded(div(lextIO.getZnadjperc(), 100));
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
