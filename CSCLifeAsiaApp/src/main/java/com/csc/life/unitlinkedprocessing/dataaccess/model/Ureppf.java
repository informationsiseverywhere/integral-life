package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Ureppf{

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private String unitVirtualFund;
	private BigDecimal nofUnits;
	private String unitType;
	private String batctrcde;
	private int priceDateUsed;
	private int moniesDate;
	private BigDecimal priceUsed;
	private BigDecimal nofDunits;
	private String fundCurrency;
	private BigDecimal fundAmount;
	private String scheduleName;
	private int scheduleNumber;
	private String datime;
	private String userProfile;
	private String jobName;
	private int seqnum;
	private String procflg;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(String unitVirtualFund) {
		this.unitVirtualFund = unitVirtualFund;
	}
	public BigDecimal getNofUnits() {
		return nofUnits;
	}
	public void setNofUnits(BigDecimal nofUnits) {
		this.nofUnits = nofUnits;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public int getPriceDateUsed() {
		return priceDateUsed;
	}
	public void setPriceDateUsed(int priceDateUsed) {
		this.priceDateUsed = priceDateUsed;
	}
	public int getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(int moniesDate) {
		this.moniesDate = moniesDate;
	}
	public BigDecimal getPriceUsed() {
		return priceUsed;
	}
	public void setPriceUsed(BigDecimal priceUsed) {
		this.priceUsed = priceUsed;
	}
	public BigDecimal getNofDunits() {
		return nofDunits;
	}
	public void setNofDunits(BigDecimal nofDunits) {
		this.nofDunits = nofDunits;
	}
	public String getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(String fundCurrency) {
		this.fundCurrency = fundCurrency;
	}
	public BigDecimal getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(BigDecimal fundAmount) {
		this.fundAmount = fundAmount;
	}
	public String getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}
	public int getScheduleNumber() {
		return scheduleNumber;
	}
	public void setScheduleNumber(int scheduleNumber) {
		this.scheduleNumber = scheduleNumber;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public int getSeqnum() {
		return seqnum;
	}
	public void setSeqnum(int seqnum) {
		this.seqnum = seqnum;
	}
	public String getProcflg() {
		return procflg;
	}
	public void setProcflg(String procflg) {
		this.procflg = procflg;
	}
}