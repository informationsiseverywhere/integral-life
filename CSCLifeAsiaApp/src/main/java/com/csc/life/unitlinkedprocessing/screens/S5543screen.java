package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5543screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5543ScreenVars sv = (S5543ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5543screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5543ScreenVars screenVars = (S5543ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unitVirtualFund01.setClassString("");
		screenVars.vfundesc01.setClassString("");
		screenVars.allowperiod01.setClassString("");
		screenVars.unitVirtualFund02.setClassString("");
		screenVars.vfundesc02.setClassString("");
		screenVars.allowperiod02.setClassString("");
		screenVars.unitVirtualFund03.setClassString("");
		screenVars.vfundesc03.setClassString("");
		screenVars.allowperiod03.setClassString("");
		screenVars.unitVirtualFund04.setClassString("");
		screenVars.vfundesc04.setClassString("");
		screenVars.allowperiod04.setClassString("");
		screenVars.unitVirtualFund05.setClassString("");
		screenVars.vfundesc05.setClassString("");
		screenVars.allowperiod06.setClassString("");
		screenVars.unitVirtualFund06.setClassString("");
		screenVars.vfundesc06.setClassString("");
		screenVars.allowperiod06.setClassString("");
		screenVars.unitVirtualFund07.setClassString("");
		screenVars.vfundesc07.setClassString("");
		screenVars.allowperiod07.setClassString("");
		screenVars.unitVirtualFund08.setClassString("");
		screenVars.vfundesc08.setClassString("");
		screenVars.allowperiod08.setClassString("");
		screenVars.unitVirtualFund09.setClassString("");
		screenVars.vfundesc09.setClassString("");
		screenVars.allowperiod09.setClassString("");
		screenVars.unitVirtualFund10.setClassString("");
		screenVars.vfundesc10.setClassString("");
		screenVars.allowperiod10.setClassString("");
		screenVars.unitVirtualFund11.setClassString("");
		screenVars.vfundesc11.setClassString("");
		screenVars.allowperiod11.setClassString("");
		screenVars.unitVirtualFund12.setClassString("");
		screenVars.vfundesc12.setClassString("");
		screenVars.allowperiod12.setClassString("");
	}

/**
 * Clear all the variables in S5543screen
 */
	public static void clear(VarModel pv) {
		S5543ScreenVars screenVars = (S5543ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unitVirtualFund01.clear();
		screenVars.vfundesc01.clear();
		screenVars.allowperiod01.clear();
		screenVars.unitVirtualFund02.clear();
		screenVars.vfundesc02.clear(); 
		screenVars.allowperiod02.clear();
		screenVars.unitVirtualFund03.clear();
		screenVars.vfundesc03.clear();
		screenVars.allowperiod03.clear();
		screenVars.unitVirtualFund04.clear();
		screenVars.vfundesc04.clear();
		screenVars.allowperiod04.clear();
		screenVars.unitVirtualFund05.clear();
		screenVars.vfundesc05.clear();
		screenVars.allowperiod05.clear();
		screenVars.unitVirtualFund06.clear();
		screenVars.vfundesc06.clear();
		screenVars.allowperiod06.clear();
		screenVars.unitVirtualFund07.clear();
		screenVars.vfundesc07.clear();
		screenVars.allowperiod07.clear();
		screenVars.unitVirtualFund08.clear();
		screenVars.vfundesc08.clear();
		screenVars.allowperiod08.clear();
		screenVars.unitVirtualFund09.clear();
		screenVars.vfundesc09.clear();
		screenVars.allowperiod09.clear();
		screenVars.unitVirtualFund10.clear();
		screenVars.vfundesc10.clear();
		screenVars.allowperiod10.clear();
		screenVars.unitVirtualFund11.clear();
		screenVars.vfundesc11.clear();
		screenVars.allowperiod11.clear();
		screenVars.unitVirtualFund12.clear();
		screenVars.vfundesc12.clear();
		screenVars.allowperiod12.clear();
		
	}
}
