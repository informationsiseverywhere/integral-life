/*
 * File: P5126.java
 * Date: 30 August 2009 0:10:33
 * Author: Quipoz Limited
 * 
 * Class transformed from P5126.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.recordstructures.P5126par;
import com.csc.life.unitlinkedprocessing.screens.S5126ScreenVars;
import com.csc.smart.procedures.Jctlem;
import com.csc.smart.procedures.Jobparm;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Jctlrec;
import com.csc.smart.recordstructures.Jobparmrec;
import com.csc.smart.recordstructures.Jobsrec;
import com.csc.smart.recordstructures.Subjobrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.batch.cls.Subjob;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            REGULAR WITHDRAWALS PROCESSING
*
*   Parameter prompt program
*
*****************************************************************
* </pre>
*/
public class P5126 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5126");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e215 = "E215";
		/* FORMATS */
	private String jctlrec = "JCTLREC";

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Jctlrec jctlrec1 = new Jctlrec();
	private Jobparmrec jobparmrec = new Jobparmrec();
	private P5126par p5126par = new P5126par();
	private Subjobrec subjobrec = new Subjobrec();
	private Batckey wsaaBatckey = new Batckey();
	private Jobsrec wsaaJobskey = new Jobsrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5126ScreenVars sv = ScreenProgram.getScreenVars( S5126ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit
	}

	public P5126() {
		super();
		screenVars = sv;
		new ScreenModel("S5126", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaJobskey.set(wsspsmart.jobskey);
		sv.effdate.set(wsspsmart.effdate);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.acctmonth.set(wsaaBatckey.batcBatcactmn);
		sv.acctyear.set(wsaaBatckey.batcBatcactyr);
		sv.jobname.set(wsaaJobskey.jobsJctljn);
		jobparmrec.company.set(wsspcomn.company);
		jobparmrec.jobname.set(wsaaJobskey.jobsJctljn);
		jobparmrec.tranid.set(wsspcomn.tranid);
		jobparmrec.function.set("READ");
		callProgram(Jobparm.class, jobparmrec.jobparmRec);
		if (isNE(jobparmrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(jobparmrec.statuz);
			fatalError600();
		}
		sv.runid.set(jobparmrec.jobcRunid);
		sv.jobq.set(jobparmrec.jobcJobq);
		sv.runlib.set(jobparmrec.jobcRunlib);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*LOAD-FIELDS*/
		getNextJob3100();
		moveParameters3200();
		initialiseJctl3300();
		submitJob3400();
		/*EXIT*/
	}

protected void getNextJob3100()
	{
		/*NEXT-JOB-NO*/
		jobparmrec.company.set(wsaaJobskey.jobsJctlcoy);
		jobparmrec.jobname.set(wsaaJobskey.jobsJctljn);
		jobparmrec.tranid.set(wsspcomn.tranid);
		jobparmrec.function.set("UPDAT");
		callProgram(Jobparm.class, jobparmrec.jobparmRec);
		if (isNE(jobparmrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(jobparmrec.statuz);
			fatalError600();
		}
		wsaaJobskey.jobsJctljnum.set(jobparmrec.jobcJobno);
		/*EXIT*/
	}

protected void moveParameters3200()
	{
		/*MOVE-TO-PARM-RECORD*/
		p5126par.date_var.set(sv.date_var);
		/*EXIT*/
	}

protected void initialiseJctl3300()
	{
		initJctl3310();
	}

protected void initJctl3310()
	{
		jctlrec1.dataArea.set(SPACES);
		jctlrec1.jobsts.set("W");
		jctlrec1.jctlpfx.set("JC");
		jctlrec1.jctlcoy.set(wsaaJobskey.jobsJctlcoy);
		jctlrec1.jctljn.set(wsaaJobskey.jobsJctljn);
		jctlrec1.jctlacyr.set(wsaaJobskey.jobsJctlacyr);
		jctlrec1.jctlacmn.set(wsaaJobskey.jobsJctlacmn);
		jctlrec1.jctljnum.set(wsaaJobskey.jobsJctljnum);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		jctlrec1.tranid.set(varcom.vrcmCompTranid);
		jctlrec1.s38jobno.set(0);
		jctlrec1.user.set(varcom.vrcmUser);
		jctlrec1.company.set(wsaaJobskey.jobsJctlcoy);
		jctlrec1.language.set(wsspcomn.language);
		wsaaBatckey.set(wsspcomn.batchkey);
		jctlrec1.branch.set(wsaaBatckey.batcBatcbrn);
		jctlrec1.batcbranch.set(wsaaBatckey.batcBatcbrn);
		jctlrec1.acctyear.set(wsaaJobskey.jobsJctlacyr);
		jctlrec1.acctmonth.set(wsaaJobskey.jobsJctlacmn);
		jctlrec1.jobd.set(jobparmrec.jobcJobd);
		jctlrec1.jobq.set(jobparmrec.jobcJobq);
		jctlrec1.runid.set(jobparmrec.jobcRunid);
		jctlrec1.runlib.set(jobparmrec.jobcRunlib);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		jctlrec1.datesub.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		jctlrec1.timesub.set(wsaaHhmmss);
		jctlrec1.effdate.set(wsspsmart.effdate);
		jctlrec1.datestart.set(0);
		jctlrec1.timestart.set(0);
		jctlrec1.jobstep.set("000");
		jctlrec1.dateend.set(0);
		jctlrec1.timeend.set(0);
		jctlrec1.eltime.set(0);
		jctlrec1.pstdate.set(0);
		jctlrec1.psttime.set(0);
		jctlrec1.pnddate.set(0);
		jctlrec1.pndtime.set(0);
		jctlrec1.parmarea.set(p5126par.parmRecord);
	}

protected void submitJob3400()
	{
		submitJob3410();
		writeJctlRecord3420();
	}

protected void submitJob3410()
	{
		subjobrec.company.set(jctlrec1.jctlcoy);
		subjobrec.jobname.set(jctlrec1.jctljn);
		subjobrec.acctyear.set(jctlrec1.jctlacyr);
		subjobrec.acctmonth.set(jctlrec1.jctlacmn);
		subjobrec.jobnum.set(jctlrec1.jctljnum);
		subjobrec.jobd.set(jobparmrec.jobcJobd);
		subjobrec.jobq.set(jobparmrec.jobcJobq);
		callProgram(Subjob.class, subjobrec.subjobRec);
		if (isNE(subjobrec.statuz,varcom.oK)) {
			subjobrec.statuz.set(e215);
			fatalError600();
		}
		jctlrec1.s38jobno.set(subjobrec.jobnum);
		jctlrec1.s38user.set(subjobrec.user);
		jctlrec1.function.set(varcom.writr);
		jctlrec1.format.set(jctlrec);
	}

protected void writeJctlRecord3420()
	{
		callProgram(Jctlem.class, jctlrec1.params);
		if (isNE(jctlrec1.statuz,varcom.oK)) {
			syserrrec.params.set(jctlrec1.params);
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
