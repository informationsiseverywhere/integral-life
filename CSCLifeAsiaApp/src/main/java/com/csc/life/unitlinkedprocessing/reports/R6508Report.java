package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R6508.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R6508Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData cntamnt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData ctypedes = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3);
	private FixedLengthStringData fndcurrdsc = new FixedLengthStringData(30);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData fundrate = new ZonedDecimalData(18, 9);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private FixedLengthStringData lifename = new FixedLengthStringData(47);
	private FixedLengthStringData moniesdt = new FixedLengthStringData(10);
	private ZonedDecimalData nofdunt = new ZonedDecimalData(16, 5);
	private ZonedDecimalData pagenum = new ZonedDecimalData(4, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData rpntlin1 = new FixedLengthStringData(60);
	private FixedLengthStringData rpntlin2 = new FixedLengthStringData(60);
	private FixedLengthStringData stmtdate = new FixedLengthStringData(10);
	private ZonedDecimalData stopcoca = new ZonedDecimalData(17, 2);
	private FixedLengthStringData stopdt = new FixedLengthStringData(10);
	private ZonedDecimalData stopdun = new ZonedDecimalData(16, 5);
	private ZonedDecimalData stopfuca = new ZonedDecimalData(17, 2);
	private FixedLengthStringData unitypdsc = new FixedLengthStringData(12);
	private ZonedDecimalData ustmno = new ZonedDecimalData(5, 0);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4);
	private FixedLengthStringData vrtfnddsc = new FixedLengthStringData(30);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R6508Report() {
		super();
	}


	/**
	 * Print the XML for R6508d01
	 */
	public void printR6508d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		moniesdt.setFieldName("moniesdt");
		moniesdt.setInternal(subString(recordData, 1, 10));
		nofdunt.setFieldName("nofdunt");
		nofdunt.setInternal(subString(recordData, 11, 16));
		fundamnt.setFieldName("fundamnt");
		fundamnt.setInternal(subString(recordData, 27, 17));
		cntamnt.setFieldName("cntamnt");
		cntamnt.setInternal(subString(recordData, 44, 17));
		fundrate.setFieldName("fundrate");
		fundrate.setInternal(subString(recordData, 61, 18));
		printLayout("R6508d01",			// Record name
			new BaseData[]{			// Fields:
				moniesdt,
				nofdunt,
				fundamnt,
				cntamnt,
				fundrate
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R6508d02
	 */
	public void printR6508d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 1, 4));
		vrtfnddsc.setFieldName("vrtfnddsc");
		vrtfnddsc.setInternal(subString(recordData, 5, 30));
		unitypdsc.setFieldName("unitypdsc");
		unitypdsc.setInternal(subString(recordData, 35, 12));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 47, 3));
		fndcurrdsc.setFieldName("fndcurrdsc");
		fndcurrdsc.setInternal(subString(recordData, 50, 30));
		nofdunt.setFieldName("nofdunt");
		nofdunt.setInternal(subString(recordData, 80, 16));
		printLayout("R6508d02",			// Record name
			new BaseData[]{			// Fields:
				vrtfnd,
				vrtfnddsc,
				unitypdsc,
				fndcurr,
				fndcurrdsc,
				nofdunt
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R6508h01
	 */
	public void printR6508h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(10).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		pagenum.setFieldName("pagenum");
		pagenum.setInternal(subString(recordData, 1, 4));
		stmtdate.setFieldName("stmtdate");
		stmtdate.setInternal(subString(recordData, 5, 10));
		ustmno.setFieldName("ustmno");
		ustmno.setInternal(subString(recordData, 15, 5));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 20, 8));
		ctypedes.setFieldName("ctypedes");
		ctypedes.setInternal(subString(recordData, 28, 30));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 58, 4));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 62, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 65, 30));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 95, 2));
		lifename.setFieldName("lifename");
		lifename.setInternal(subString(recordData, 97, 47));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 144, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 146, 2));
		printLayout("R6508h01",			// Record name
			new BaseData[]{			// Fields:
				pagenum,
				stmtdate,
				ustmno,
				chdrnum,
				ctypedes,
				plnsfx,
				cntcurr,
				currdesc,
				life,
				lifename,
				coverage,
				rider
			}
			, new Object[] {			// indicators
				new Object[]{"ind10", indicArea.charAt(10)}
			}
		);
		
		currentPrintLine.set(9);
	}

	/**
	 * Print the XML for R6508h02
	 */
	public void printR6508h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 1, 4));
		vrtfnddsc.setFieldName("vrtfnddsc");
		vrtfnddsc.setInternal(subString(recordData, 5, 30));
		unitypdsc.setFieldName("unitypdsc");
		unitypdsc.setInternal(subString(recordData, 35, 12));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 47, 3));
		fndcurrdsc.setFieldName("fndcurrdsc");
		fndcurrdsc.setInternal(subString(recordData, 50, 30));
		printLayout("R6508h02",			// Record name
			new BaseData[]{			// Fields:
				vrtfnd,
				vrtfnddsc,
				unitypdsc,
				fndcurr,
				fndcurrdsc
			}
		);

	}

	/**
	 * Print the XML for R6508h03
	 */
	public void printR6508h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		stopdt.setFieldName("stopdt");
		stopdt.setInternal(subString(recordData, 1, 10));
		stopdun.setFieldName("stopdun");
		stopdun.setInternal(subString(recordData, 11, 16));
		stopfuca.setFieldName("stopfuca");
		stopfuca.setInternal(subString(recordData, 27, 17));
		stopcoca.setFieldName("stopcoca");
		stopcoca.setInternal(subString(recordData, 44, 17));
		printLayout("R6508h03",			// Record name
			new BaseData[]{			// Fields:
				stopdt,
				stopdun,
				stopfuca,
				stopcoca
			}
		);

		currentPrintLine.add(5);
	}

	/**
	 * Print the XML for R6508h04
	 */
	public void printR6508h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R6508h04",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for R6508h05
	 */
	public void printR6508h05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		stopdt.setFieldName("stopdt");
		stopdt.setInternal(subString(recordData, 1, 10));
		stopdun.setFieldName("stopdun");
		stopdun.setInternal(subString(recordData, 11, 16));
		stopfuca.setFieldName("stopfuca");
		stopfuca.setInternal(subString(recordData, 27, 17));
		stopcoca.setFieldName("stopcoca");
		stopcoca.setInternal(subString(recordData, 44, 17));
		printLayout("R6508h05",			// Record name
			new BaseData[]{			// Fields:
				stopdt,
				stopdun,
				stopfuca,
				stopcoca
			}
		);

		currentPrintLine.add(5);
	}

	/**
	 * Print the XML for R6508h06
	 */
	public void printR6508h06(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R6508h06",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for R6508h07
	 */
	public void printR6508h07(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		rpntlin1.setFieldName("rpntlin1");
		rpntlin1.setInternal(subString(recordData, 1, 60));
		rpntlin2.setFieldName("rpntlin2");
		rpntlin2.setInternal(subString(recordData, 61, 60));
		printLayout("R6508h07",			// Record name
			new BaseData[]{			// Fields:
				rpntlin1,
				rpntlin2
			}
		);

		currentPrintLine.add(1);
	}


}
