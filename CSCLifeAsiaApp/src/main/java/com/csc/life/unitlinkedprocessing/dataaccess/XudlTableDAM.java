package com.csc.life.unitlinkedprocessing.dataaccess;


import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.csc.life.enquiries.dataaccess.UfndpfTableDAM;
import com.csc.smart400framework.dataaccess.MultiViewSmartFileCode;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * The Multi-view DAM class
 * File: XudlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:52:54
 * Class transformed from XUDL
 * Author: Quipoz Limited
 *
 * Copyright (2009) CSC Asia, all rights reserved
 *
 */
 
public class XudlTableDAM extends MultiViewSmartFileCode {
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(294);
	public FixedLengthStringData dataKey = new FixedLengthStringData(64).isAPartOf(dataArea, 0);
	public FixedLengthStringData udelrecKeyData = new FixedLengthStringData(64).isAPartOf(dataKey, 0, REDEFINE);
	public FixedLengthStringData udelChdrcoy = new FixedLengthStringData(1).isAPartOf(udelrecKeyData, 0);
	public FixedLengthStringData udelUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(udelrecKeyData, 1);
	public FixedLengthStringData udelUnitType = new FixedLengthStringData(1).isAPartOf(udelrecKeyData, 5);
	public FixedLengthStringData udelChdrnum = new FixedLengthStringData(8).isAPartOf(udelrecKeyData, 6);
	public FixedLengthStringData udelLife = new FixedLengthStringData(2).isAPartOf(udelrecKeyData, 14);
	public FixedLengthStringData udelCoverage = new FixedLengthStringData(2).isAPartOf(udelrecKeyData, 16);
	public FixedLengthStringData udelRider = new FixedLengthStringData(2).isAPartOf(udelrecKeyData, 18);
	public FixedLengthStringData udelBatctrcde = new FixedLengthStringData(4).isAPartOf(udelrecKeyData, 20);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(udelrecKeyData, 24, FILLER);
	public FixedLengthStringData ufndrecKeyData = new FixedLengthStringData(64).isAPartOf(dataKey, 0, REDEFINE);
	public FixedLengthStringData ufndCompany = new FixedLengthStringData(1).isAPartOf(ufndrecKeyData, 0);
	public FixedLengthStringData ufndVirtualFund = new FixedLengthStringData(4).isAPartOf(ufndrecKeyData, 1);
	public FixedLengthStringData ufndUnitType = new FixedLengthStringData(1).isAPartOf(ufndrecKeyData, 5);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(ufndrecKeyData, 6, FILLER);
	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(ufndrecKeyData, 14, FILLER);
	public FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(ufndrecKeyData, 16, FILLER);
	public FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(ufndrecKeyData, 18, FILLER);
	public FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(ufndrecKeyData, 20, FILLER);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(ufndrecKeyData, 24, FILLER);
	public FixedLengthStringData nonKey = new FixedLengthStringData(230).isAPartOf(dataArea, 64);
	public FixedLengthStringData udelrecNonKeyData = new FixedLengthStringData(230).isAPartOf(nonKey, 0, REDEFINE);
	public FixedLengthStringData filler7 = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 0, FILLER);
	public FixedLengthStringData filler8 = new FixedLengthStringData(4).isAPartOf(udelrecNonKeyData, 1, FILLER);
	public FixedLengthStringData filler9 = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 5, FILLER);
	public FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(udelrecNonKeyData, 6, FILLER);
	public FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(udelrecNonKeyData, 14, FILLER);
	public FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(udelrecNonKeyData, 16, FILLER);
	public FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(udelrecNonKeyData, 18, FILLER);
	public PackedDecimalData udelPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(udelrecNonKeyData, 20);
	public PackedDecimalData udelTranno = new PackedDecimalData(5, 0).isAPartOf(udelrecNonKeyData, 23);
	public FixedLengthStringData udelTermid = new FixedLengthStringData(4).isAPartOf(udelrecNonKeyData, 26);
	public PackedDecimalData udelTransactionDate = new PackedDecimalData(6, 0).isAPartOf(udelrecNonKeyData, 30);
	public PackedDecimalData udelTransactionTime = new PackedDecimalData(6, 0).isAPartOf(udelrecNonKeyData, 34);
	public PackedDecimalData udelUser = new PackedDecimalData(6, 0).isAPartOf(udelrecNonKeyData, 38);
	public FixedLengthStringData udelBatccoy = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 42);
	public FixedLengthStringData udelBatcbrn = new FixedLengthStringData(2).isAPartOf(udelrecNonKeyData, 43);
	public PackedDecimalData udelBatcactyr = new PackedDecimalData(4, 0).isAPartOf(udelrecNonKeyData, 45);
	public PackedDecimalData udelBatcactmn = new PackedDecimalData(2, 0).isAPartOf(udelrecNonKeyData, 48);
	public FixedLengthStringData filler14 = new FixedLengthStringData(4).isAPartOf(udelrecNonKeyData, 50, FILLER);
	public FixedLengthStringData udelBatcbatch = new FixedLengthStringData(5).isAPartOf(udelrecNonKeyData, 54);
	public PackedDecimalData udelFundRate = new PackedDecimalData(18, 9).isAPartOf(udelrecNonKeyData, 59);
	public FixedLengthStringData udelUnitSubAccount = new FixedLengthStringData(4).isAPartOf(udelrecNonKeyData, 69);
	public PackedDecimalData udelStrpdate = new PackedDecimalData(8, 0).isAPartOf(udelrecNonKeyData, 73);
	public FixedLengthStringData udelNowDeferInd = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 78);
	public PackedDecimalData udelNofUnits = new PackedDecimalData(16, 5).isAPartOf(udelrecNonKeyData, 79);
	public PackedDecimalData udelNofDunits = new PackedDecimalData(16, 5).isAPartOf(udelrecNonKeyData, 88);
	public PackedDecimalData udelMoniesDate = new PackedDecimalData(8, 0).isAPartOf(udelrecNonKeyData, 97);
	public PackedDecimalData udelPriceDateUsed = new PackedDecimalData(8, 0).isAPartOf(udelrecNonKeyData, 102);
	public PackedDecimalData udelPriceUsed = new PackedDecimalData(9, 5).isAPartOf(udelrecNonKeyData, 107);
	public PackedDecimalData udelUnitBarePrice = new PackedDecimalData(9, 5).isAPartOf(udelrecNonKeyData, 112);
	public FixedLengthStringData udelCrtable = new FixedLengthStringData(4).isAPartOf(udelrecNonKeyData, 117);
	public FixedLengthStringData udelCntcurr = new FixedLengthStringData(3).isAPartOf(udelrecNonKeyData, 121);
	public FixedLengthStringData udelFeedbackInd = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 124);
	public PackedDecimalData udelInciNum = new PackedDecimalData(3, 0).isAPartOf(udelrecNonKeyData, 125);
	public PackedDecimalData udelInciPerd = new PackedDecimalData(1, 0).isAPartOf(udelrecNonKeyData, 127);
	public PackedDecimalData udelContractAmount = new PackedDecimalData(17, 2).isAPartOf(udelrecNonKeyData, 128);
	public FixedLengthStringData udelFundCurrency = new FixedLengthStringData(3).isAPartOf(udelrecNonKeyData, 137);
	public PackedDecimalData udelFundAmount = new PackedDecimalData(17, 2).isAPartOf(udelrecNonKeyData, 140);
	public FixedLengthStringData udelAllInd = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 149);
	public FixedLengthStringData udelContractType = new FixedLengthStringData(3).isAPartOf(udelrecNonKeyData, 150);
	public FixedLengthStringData udelTriggerModule = new FixedLengthStringData(10).isAPartOf(udelrecNonKeyData, 153);
	public FixedLengthStringData udelTriggerKey = new FixedLengthStringData(20).isAPartOf(udelrecNonKeyData, 163);
	public FixedLengthStringData udelSwitchIndicator = new FixedLengthStringData(1).isAPartOf(udelrecNonKeyData, 183);
	public FixedLengthStringData udelUserProfile = new FixedLengthStringData(10).isAPartOf(udelrecNonKeyData, 184);
	public FixedLengthStringData udelJobName = new FixedLengthStringData(10).isAPartOf(udelrecNonKeyData, 194);
	public FixedLengthStringData udelDatime = new FixedLengthStringData(26).isAPartOf(udelrecNonKeyData, 204);
	public FixedLengthStringData ufndrecNonKeyData = new FixedLengthStringData(230).isAPartOf(nonKey, 0, REDEFINE);
	public FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(ufndrecNonKeyData, 0, FILLER);
	public FixedLengthStringData filler16 = new FixedLengthStringData(4).isAPartOf(ufndrecNonKeyData, 1, FILLER);
	public FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(ufndrecNonKeyData, 5, FILLER);
	public PackedDecimalData ufndNofunts = new PackedDecimalData(16, 5).isAPartOf(ufndrecNonKeyData, 6);
	public FixedLengthStringData ufndUserProfile = new FixedLengthStringData(10).isAPartOf(ufndrecNonKeyData, 15);
	public FixedLengthStringData ufndJobName = new FixedLengthStringData(10).isAPartOf(ufndrecNonKeyData, 25);
	public FixedLengthStringData ufndDatime = new FixedLengthStringData(26).isAPartOf(ufndrecNonKeyData, 35);
	public FixedLengthStringData filler18 = new FixedLengthStringData(169).isAPartOf(ufndrecNonKeyData, 61, FILLER);

	
	//=====================Getter Setter Start========================
	
	public FixedLengthStringData getDataArea() {
		return this.dataArea;
	}

	public FixedLengthStringData getDataKey() {
		return this.dataKey;
	}
	
	public FixedLengthStringData getNonKey() {
		return this.nonKey;
	}


	public void setUdelrecKeyData(Object what) {
		this.udelrecKeyData.set(what);
	}	
	public FixedLengthStringData getUdelrecKeyData() {
		return this.udelrecKeyData;
	}		

	public void setUdelChdrcoy(Object what) {
		this.udelChdrcoy.set(what);
	}	
	public FixedLengthStringData getUdelChdrcoy() {
		return this.udelChdrcoy;
	}		

	public void setUdelUnitVirtualFund(Object what) {
		this.udelUnitVirtualFund.set(what);
	}	
	public FixedLengthStringData getUdelUnitVirtualFund() {
		return this.udelUnitVirtualFund;
	}		

	public void setUdelUnitType(Object what) {
		this.udelUnitType.set(what);
	}	
	public FixedLengthStringData getUdelUnitType() {
		return this.udelUnitType;
	}		

	public void setUdelChdrnum(Object what) {
		this.udelChdrnum.set(what);
	}	
	public FixedLengthStringData getUdelChdrnum() {
		return this.udelChdrnum;
	}		

	public void setUdelLife(Object what) {
		this.udelLife.set(what);
	}	
	public FixedLengthStringData getUdelLife() {
		return this.udelLife;
	}		

	public void setUdelCoverage(Object what) {
		this.udelCoverage.set(what);
	}	
	public FixedLengthStringData getUdelCoverage() {
		return this.udelCoverage;
	}		

	public void setUdelRider(Object what) {
		this.udelRider.set(what);
	}	
	public FixedLengthStringData getUdelRider() {
		return this.udelRider;
	}		

	public void setUdelBatctrcde(Object what) {
		this.udelBatctrcde.set(what);
	}	
	public FixedLengthStringData getUdelBatctrcde() {
		return this.udelBatctrcde;
	}		

	public void setUfndrecKeyData(Object what) {
		this.ufndrecKeyData.set(what);
	}	
	public FixedLengthStringData getUfndrecKeyData() {
		return this.ufndrecKeyData;
	}		

	public void setUfndCompany(Object what) {
		this.ufndCompany.set(what);
	}	
	public FixedLengthStringData getUfndCompany() {
		return this.ufndCompany;
	}		

	public void setUfndVirtualFund(Object what) {
		this.ufndVirtualFund.set(what);
	}	
	public FixedLengthStringData getUfndVirtualFund() {
		return this.ufndVirtualFund;
	}		

	public void setUfndUnitType(Object what) {
		this.ufndUnitType.set(what);
	}	
	public FixedLengthStringData getUfndUnitType() {
		return this.ufndUnitType;
	}		

	public void setUdelrecNonKeyData(Object what) {
		this.udelrecNonKeyData.set(what);
	}	
	public FixedLengthStringData getUdelrecNonKeyData() {
		return this.udelrecNonKeyData;
	}		

	public void setUdelPlanSuffix(Object what) {
		this.udelPlanSuffix.set(what);
	}
	public void setUdelPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			this.udelPlanSuffix.setRounded(what);
		else
			this.udelPlanSuffix.set(what);
	}
	public PackedDecimalData getUdelPlanSuffix() {
		return this.udelPlanSuffix;
	}		

	public void setUdelTranno(Object what) {
		this.udelTranno.set(what);
	}
	public void setUdelTranno(Object what, boolean rounded) {
		if (rounded)
			this.udelTranno.setRounded(what);
		else
			this.udelTranno.set(what);
	}
	public PackedDecimalData getUdelTranno() {
		return this.udelTranno;
	}		

	public void setUdelTermid(Object what) {
		this.udelTermid.set(what);
	}	
	public FixedLengthStringData getUdelTermid() {
		return this.udelTermid;
	}		

	public void setUdelTransactionDate(Object what) {
		this.udelTransactionDate.set(what);
	}
	public void setUdelTransactionDate(Object what, boolean rounded) {
		if (rounded)
			this.udelTransactionDate.setRounded(what);
		else
			this.udelTransactionDate.set(what);
	}
	public PackedDecimalData getUdelTransactionDate() {
		return this.udelTransactionDate;
	}		

	public void setUdelTransactionTime(Object what) {
		this.udelTransactionTime.set(what);
	}
	public void setUdelTransactionTime(Object what, boolean rounded) {
		if (rounded)
			this.udelTransactionTime.setRounded(what);
		else
			this.udelTransactionTime.set(what);
	}
	public PackedDecimalData getUdelTransactionTime() {
		return this.udelTransactionTime;
	}		

	public void setUdelUser(Object what) {
		this.udelUser.set(what);
	}
	public void setUdelUser(Object what, boolean rounded) {
		if (rounded)
			this.udelUser.setRounded(what);
		else
			this.udelUser.set(what);
	}
	public PackedDecimalData getUdelUser() {
		return this.udelUser;
	}		

	public void setUdelBatccoy(Object what) {
		this.udelBatccoy.set(what);
	}	
	public FixedLengthStringData getUdelBatccoy() {
		return this.udelBatccoy;
	}		

	public void setUdelBatcbrn(Object what) {
		this.udelBatcbrn.set(what);
	}	
	public FixedLengthStringData getUdelBatcbrn() {
		return this.udelBatcbrn;
	}		

	public void setUdelBatcactyr(Object what) {
		this.udelBatcactyr.set(what);
	}
	public void setUdelBatcactyr(Object what, boolean rounded) {
		if (rounded)
			this.udelBatcactyr.setRounded(what);
		else
			this.udelBatcactyr.set(what);
	}
	public PackedDecimalData getUdelBatcactyr() {
		return this.udelBatcactyr;
	}		

	public void setUdelBatcactmn(Object what) {
		this.udelBatcactmn.set(what);
	}
	public void setUdelBatcactmn(Object what, boolean rounded) {
		if (rounded)
			this.udelBatcactmn.setRounded(what);
		else
			this.udelBatcactmn.set(what);
	}
	public PackedDecimalData getUdelBatcactmn() {
		return this.udelBatcactmn;
	}		

	public void setUdelBatcbatch(Object what) {
		this.udelBatcbatch.set(what);
	}	
	public FixedLengthStringData getUdelBatcbatch() {
		return this.udelBatcbatch;
	}		

	public void setUdelFundRate(Object what) {
		this.udelFundRate.set(what);
	}
	public void setUdelFundRate(Object what, boolean rounded) {
		if (rounded)
			this.udelFundRate.setRounded(what);
		else
			this.udelFundRate.set(what);
	}
	public PackedDecimalData getUdelFundRate() {
		return this.udelFundRate;
	}		

	public void setUdelUnitSubAccount(Object what) {
		this.udelUnitSubAccount.set(what);
	}	
	public FixedLengthStringData getUdelUnitSubAccount() {
		return this.udelUnitSubAccount;
	}		

	public void setUdelStrpdate(Object what) {
		this.udelStrpdate.set(what);
	}
	public void setUdelStrpdate(Object what, boolean rounded) {
		if (rounded)
			this.udelStrpdate.setRounded(what);
		else
			this.udelStrpdate.set(what);
	}
	public PackedDecimalData getUdelStrpdate() {
		return this.udelStrpdate;
	}		

	public void setUdelNowDeferInd(Object what) {
		this.udelNowDeferInd.set(what);
	}	
	public FixedLengthStringData getUdelNowDeferInd() {
		return this.udelNowDeferInd;
	}		

	public void setUdelNofUnits(Object what) {
		this.udelNofUnits.set(what);
	}
	public void setUdelNofUnits(Object what, boolean rounded) {
		if (rounded)
			this.udelNofUnits.setRounded(what);
		else
			this.udelNofUnits.set(what);
	}
	public PackedDecimalData getUdelNofUnits() {
		return this.udelNofUnits;
	}		

	public void setUdelNofDunits(Object what) {
		this.udelNofDunits.set(what);
	}
	public void setUdelNofDunits(Object what, boolean rounded) {
		if (rounded)
			this.udelNofDunits.setRounded(what);
		else
			this.udelNofDunits.set(what);
	}
	public PackedDecimalData getUdelNofDunits() {
		return this.udelNofDunits;
	}		

	public void setUdelMoniesDate(Object what) {
		this.udelMoniesDate.set(what);
	}
	public void setUdelMoniesDate(Object what, boolean rounded) {
		if (rounded)
			this.udelMoniesDate.setRounded(what);
		else
			this.udelMoniesDate.set(what);
	}
	public PackedDecimalData getUdelMoniesDate() {
		return this.udelMoniesDate;
	}		

	public void setUdelPriceDateUsed(Object what) {
		this.udelPriceDateUsed.set(what);
	}
	public void setUdelPriceDateUsed(Object what, boolean rounded) {
		if (rounded)
			this.udelPriceDateUsed.setRounded(what);
		else
			this.udelPriceDateUsed.set(what);
	}
	public PackedDecimalData getUdelPriceDateUsed() {
		return this.udelPriceDateUsed;
	}		

	public void setUdelPriceUsed(Object what) {
		this.udelPriceUsed.set(what);
	}
	public void setUdelPriceUsed(Object what, boolean rounded) {
		if (rounded)
			this.udelPriceUsed.setRounded(what);
		else
			this.udelPriceUsed.set(what);
	}
	public PackedDecimalData getUdelPriceUsed() {
		return this.udelPriceUsed;
	}		

	public void setUdelUnitBarePrice(Object what) {
		this.udelUnitBarePrice.set(what);
	}
	public void setUdelUnitBarePrice(Object what, boolean rounded) {
		if (rounded)
			this.udelUnitBarePrice.setRounded(what);
		else
			this.udelUnitBarePrice.set(what);
	}
	public PackedDecimalData getUdelUnitBarePrice() {
		return this.udelUnitBarePrice;
	}		

	public void setUdelCrtable(Object what) {
		this.udelCrtable.set(what);
	}	
	public FixedLengthStringData getUdelCrtable() {
		return this.udelCrtable;
	}		

	public void setUdelCntcurr(Object what) {
		this.udelCntcurr.set(what);
	}	
	public FixedLengthStringData getUdelCntcurr() {
		return this.udelCntcurr;
	}		

	public void setUdelFeedbackInd(Object what) {
		this.udelFeedbackInd.set(what);
	}	
	public FixedLengthStringData getUdelFeedbackInd() {
		return this.udelFeedbackInd;
	}		

	public void setUdelInciNum(Object what) {
		this.udelInciNum.set(what);
	}
	public void setUdelInciNum(Object what, boolean rounded) {
		if (rounded)
			this.udelInciNum.setRounded(what);
		else
			this.udelInciNum.set(what);
	}
	public PackedDecimalData getUdelInciNum() {
		return this.udelInciNum;
	}		

	public void setUdelInciPerd(Object what) {
		this.udelInciPerd.set(what);
	}
	public void setUdelInciPerd(Object what, boolean rounded) {
		if (rounded)
			this.udelInciPerd.setRounded(what);
		else
			this.udelInciPerd.set(what);
	}
	public PackedDecimalData getUdelInciPerd() {
		return this.udelInciPerd;
	}		

	public void setUdelContractAmount(Object what) {
		this.udelContractAmount.set(what);
	}
	public void setUdelContractAmount(Object what, boolean rounded) {
		if (rounded)
			this.udelContractAmount.setRounded(what);
		else
			this.udelContractAmount.set(what);
	}
	public PackedDecimalData getUdelContractAmount() {
		return this.udelContractAmount;
	}		

	public void setUdelFundCurrency(Object what) {
		this.udelFundCurrency.set(what);
	}	
	public FixedLengthStringData getUdelFundCurrency() {
		return this.udelFundCurrency;
	}		

	public void setUdelFundAmount(Object what) {
		this.udelFundAmount.set(what);
	}
	public void setUdelFundAmount(Object what, boolean rounded) {
		if (rounded)
			this.udelFundAmount.setRounded(what);
		else
			this.udelFundAmount.set(what);
	}
	public PackedDecimalData getUdelFundAmount() {
		return this.udelFundAmount;
	}		

	public void setUdelAllInd(Object what) {
		this.udelAllInd.set(what);
	}	
	public FixedLengthStringData getUdelAllInd() {
		return this.udelAllInd;
	}		

	public void setUdelContractType(Object what) {
		this.udelContractType.set(what);
	}	
	public FixedLengthStringData getUdelContractType() {
		return this.udelContractType;
	}		

	public void setUdelTriggerModule(Object what) {
		this.udelTriggerModule.set(what);
	}	
	public FixedLengthStringData getUdelTriggerModule() {
		return this.udelTriggerModule;
	}		

	public void setUdelTriggerKey(Object what) {
		this.udelTriggerKey.set(what);
	}	
	public FixedLengthStringData getUdelTriggerKey() {
		return this.udelTriggerKey;
	}		

	public void setUdelSwitchIndicator(Object what) {
		this.udelSwitchIndicator.set(what);
	}	
	public FixedLengthStringData getUdelSwitchIndicator() {
		return this.udelSwitchIndicator;
	}		

	public void setUdelUserProfile(Object what) {
		this.udelUserProfile.set(what);
	}	
	public FixedLengthStringData getUdelUserProfile() {
		return this.udelUserProfile;
	}		

	public void setUdelJobName(Object what) {
		this.udelJobName.set(what);
	}	
	public FixedLengthStringData getUdelJobName() {
		return this.udelJobName;
	}		

	public void setUdelDatime(Object what) {
		this.udelDatime.set(what);
	}	
	public FixedLengthStringData getUdelDatime() {
		return this.udelDatime;
	}		

	public void setUfndrecNonKeyData(Object what) {
		this.ufndrecNonKeyData.set(what);
	}	
	public FixedLengthStringData getUfndrecNonKeyData() {
		return this.ufndrecNonKeyData;
	}		

	public void setUfndNofunts(Object what) {
		this.ufndNofunts.set(what);
	}
	public void setUfndNofunts(Object what, boolean rounded) {
		if (rounded)
			this.ufndNofunts.setRounded(what);
		else
			this.ufndNofunts.set(what);
	}
	public PackedDecimalData getUfndNofunts() {
		return this.ufndNofunts;
	}		

	public void setUfndUserProfile(Object what) {
		this.ufndUserProfile.set(what);
	}	
	public FixedLengthStringData getUfndUserProfile() {
		return this.ufndUserProfile;
	}		

	public void setUfndJobName(Object what) {
		this.ufndJobName.set(what);
	}	
	public FixedLengthStringData getUfndJobName() {
		return this.ufndJobName;
	}		

	public void setUfndDatime(Object what) {
		this.ufndDatime.set(what);
	}	
	public FixedLengthStringData getUfndDatime() {
		return this.ufndDatime;
	}		


	//=====================Getter Setter End ========================
	
	public String getTable() {
		return "XUDL";
	}

	public void setTable() {
		this.TABLE = this.getTable();
	}
	
	protected PFAdapterDAM getPFDAMByFormat(String format) {
		if(format.equalsIgnoreCase("ufndrec")) {
			return new UfndpfTableDAM();
		}
		return new UdelpfTableDAM();
	}

	protected String[] getPFFieldsByFormat(String format) {  
		if(format.equalsIgnoreCase("ufndrec")) {
			return ufndrecPfString;
		}	
		return udelrecPfString;
	}

	@Override
	protected void  clearRecKeyData() {
		this.dataKey.set("");
	}
	
	@Override
	protected void clearRecNonKeyData() {
		this.nonKey.set("");
	}
	
	@Override
	protected FixedLengthStringData getRecKeyData() {
		return this.dataKey;
	}

	@Override
	protected FixedLengthStringData getRecNonKeyData() {
		return this.nonKey;
	}

	@Override
	protected FixedLengthStringData setRecKeyData(Object what) {
		this.dataKey.set(what);
		return this.dataKey;
	}

	@Override
	protected FixedLengthStringData setRecNonKeyData(Object what) {
		this.nonKey.set(what);
		return this.nonKey;
	}
	
	@Override
	protected String[] getFieldListByFormat(String format) {  
		if(format.equalsIgnoreCase("ufndrec")) {
			return ufndrecData;
		}
		return udelrecData;
	}	
	
	@Override
	protected Object[] getFieldsByFormat(String format) {
		if(format.equalsIgnoreCase("ufndrec")) {
			return ufndrecObj;
		}
		return udelrecObj;
	}	

	@Override
	protected String[] getKeyArrayByFormat(String format) {	
		return new String[] { "CHDRCOY", "VRTFND", "UNITYP", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "BATCTRCDE"};
	}

	@Override
	protected String[] getKeyOrderArrayByFormat(String format) {	
		return new String[] { "ASC", "ASC", "ASC", "ASC", "ASC", "ASC", "ASC", "ASC"};
	}

	@Override
	protected String getOrderByFormat(String format) {	
		return "CHDRCOY ASC, VRTFND ASC, UNITYP ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, BATCTRCDE ASC, UNIQUE_NUMBER DESC";
	}

	@Override
	protected String getReverseOrderByFormat(String format) {	
		return "CHDRCOY DESC, VRTFND DESC, UNITYP DESC, CHDRNUM DESC, LIFE DESC, COVERAGE DESC, RIDER DESC, BATCTRCDE DESC, UNIQUE_NUMBER ASC";
	}

	//===============================================================
		
	private static final String[]  udelrecData = { 
		"udelCHDRCOY",
		"udelVRTFND",
		"udelUNITYP",
		"udelCHDRNUM",
		"udelLIFE",
		"udelCOVERAGE",
		"udelRIDER",
		"udelPLNSFX",
		"udelTRANNO",
		"udelTERMID",
		"udelTRDT",
		"udelTRTM",
		"udelUSER_T",
		"udelBATCCOY",
		"udelBATCBRN",
		"udelBATCACTYR",
		"udelBATCACTMN",
		"udelBATCTRCDE",
		"udelBATCBATCH",
		"udelFUNDRATE",
		"udelUNITSA",
		"udelSTRPDATE",
		"udelNDFIND",
		"udelNOFUNT",
		"udelNOFDUNT",
		"udelMONIESDT",
		"udelPRICEDT",
		"udelPRICEUSED",
		"udelUBREPR",
		"udelCRTABLE",
		"udelCNTCURR",
		"udelFDBKIND",
		"udelINCINUM",
		"udelINCIPERD",
		"udelCNTAMNT",
		"udelFNDCURR",
		"udelFUNDAMNT",
		"udelALLIND",
		"udelCONTYP",
		"udelTRIGER",
		"udelTRIGKY",
		"udelSWCHIND",
		"udelUSRPRF",
		"udelJOBNM",
		"udelDATIME"
	};	

	private static final String[]  ufndrecData = { 
		"ufndCOMPANY",
		"ufndVRTFUND",
		"ufndUNITYP",
		"ufndNOFUNTS",
		"ufndUSRPRF",
		"ufndJOBNM",
		"ufndDATIME"
	};	

    private final BaseData[]  udelrecObj = { 
		udelChdrcoy,
		udelUnitVirtualFund,
		udelUnitType,
		udelChdrnum,
		udelLife,
		udelCoverage,
		udelRider,
		udelPlanSuffix,
		udelTranno,
		udelTermid,
		udelTransactionDate,
		udelTransactionTime,
		udelUser,
		udelBatccoy,
		udelBatcbrn,
		udelBatcactyr,
		udelBatcactmn,
		udelBatctrcde,
		udelBatcbatch,
		udelFundRate,
		udelUnitSubAccount,
		udelStrpdate,
		udelNowDeferInd,
		udelNofUnits,
		udelNofDunits,
		udelMoniesDate,
		udelPriceDateUsed,
		udelPriceUsed,
		udelUnitBarePrice,
		udelCrtable,
		udelCntcurr,
		udelFeedbackInd,
		udelInciNum,
		udelInciPerd,
		udelContractAmount,
		udelFundCurrency,
		udelFundAmount,
		udelAllInd,
		udelContractType,
		udelTriggerModule,
		udelTriggerKey,
		udelSwitchIndicator,
		udelUserProfile,
		udelJobName,
		udelDatime
	};	

    private final BaseData[]  ufndrecObj = { 
		ufndCompany,
		ufndVirtualFund,
		ufndUnitType,
		ufndNofunts,
		ufndUserProfile,
		ufndJobName,
		ufndDatime
	};	
	
 	private static final String[]  udelrecPfString = { 
		"udelCHDRCOY",
		"udelVRTFND",
		"udelUNITYP",
		"udelCHDRNUM",
		"udelLIFE",
		"udelCOVERAGE",
		"udelRIDER",
		"udelPLNSFX",
		"udelTRANNO",
		"udelTERMID",
		"udelTRDT",
		"udelTRTM",
		"udelUSER_T",
		"udelBATCCOY",
		"udelBATCBRN",
		"udelBATCACTYR",
		"udelBATCACTMN",
		"udelBATCTRCDE",
		"udelBATCBATCH",
		"udelFUNDRATE",
		"udelUNITSA",
		"udelSTRPDATE",
		"udelNDFIND",
		"udelNOFUNT",
		"udelNOFDUNT",
		"udelMONIESDT",
		"udelPRICEDT",
		"udelPRICEUSED",
		"udelUBREPR",
		"udelCRTABLE",
		"udelCNTCURR",
		"udelFDBKIND",
		"udelINCINUM",
		"udelINCIPERD",
		"udelCNTAMNT",
		"udelFNDCURR",
		"udelFUNDAMNT",
		"udelALLIND",
		"udelCONTYP",
		"udelTRIGER",
		"udelTRIGKY",
		"udelSWCHIND",
		"udelUSRPRF",
		"udelJOBNM",
		"udelDATIME"
	};		

 	private static final String[]  ufndrecPfString = { 
		"ufndCOMPANY",
		"ufndVRTFUND",
		"ufndUNITYP",
		"ufndNOFUNTS",
		"ufndUSRPRF",
		"ufndJOBNM",
		"ufndDATIME"
	};		

	//===============================================================	
	
	@Override
	public FixedLengthStringData setDataArea(Object obj) {
		this.dataArea.set(obj);
		return this.dataArea;
	}
	
	@Override
	public FixedLengthStringData setDataKey(Object obj) {
		this.dataKey.set(obj);
		return this.dataKey;
	}
	
	@Override
	public FixedLengthStringData setNonKey(Object obj) {
		this.nonKey.set(obj);
		return this.nonKey;
	}
	
}