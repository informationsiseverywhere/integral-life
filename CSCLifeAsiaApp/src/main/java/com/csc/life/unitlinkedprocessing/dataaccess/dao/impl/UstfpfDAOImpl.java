package com.csc.life.unitlinkedprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.csc.life.unitlinkedprocessing.dataaccess.dao.Br610TempDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UstfpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Br610DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ustfpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.UstmData;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UstfpfDAOImpl extends BaseDAOImpl<Ustfpf> implements UstfpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UstfpfDAOImpl.class);
	public void insertUstfstmRecord(List<Ustfpf> ustfstmInsertList) {
		if (ustfstmInsertList != null && !ustfstmInsertList.isEmpty()) {

			String SQL_UTRN_INSERT = "INSERT INTO USTFPF(CHDRPFX,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,USTMNO,VRTFND,STOPDT,STOPUN,STOPDUN,STCLDT,STCLUN,"
					+ "STCLDUN,STRPDATE,STTRUN,STTRDUN,PLNSFX,USTMTFLAG,STOPFUCA,STOPCOCA,"
					+ "STCLFUCA,STCLCOCA,STTRFUCA,STTRCOCA,UNITYP,USRPRF,JOBNM,DATIME ) "

	               		+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement psUtrnInsert = getPrepareStatement(SQL_UTRN_INSERT);
			try {
				for (Ustfpf u : ustfstmInsertList) {
					int i = 1;
					psUtrnInsert.setString(i++, u.getChdrpfx());
					psUtrnInsert.setString(i++, u.getChdrcoy());
					psUtrnInsert.setString(i++, u.getChdrnum());
					psUtrnInsert.setString(i++, u.getLife());
					psUtrnInsert.setString(i++, u.getCoverage());
					psUtrnInsert.setString(i++, u.getRider());
					psUtrnInsert.setInt(i++, u.getUstmno());
					psUtrnInsert.setString(i++, u.getVrtfnd());
					psUtrnInsert.setInt(i++, u.getStopdt());
					psUtrnInsert.setBigDecimal(i++, u.getStopun());
					psUtrnInsert.setBigDecimal(i++, u.getStopdun());
					psUtrnInsert.setLong(i++, u.getStcldt());
					psUtrnInsert.setBigDecimal(i++, u.getStclun());
					psUtrnInsert.setBigDecimal(i++, u.getStcldun());
					psUtrnInsert.setInt(i++, u.getStrpdate());
					psUtrnInsert.setBigDecimal(i++, u.getSttrun());
					psUtrnInsert.setBigDecimal(i++, u.getSttrdun());
					psUtrnInsert.setInt(i++, u.getPlnsfx());
					psUtrnInsert.setString(i++, u.getUstmtflag());
					psUtrnInsert.setBigDecimal(i++, u.getStofuca());
					psUtrnInsert.setBigDecimal(i++, u.getStopcoca());
					psUtrnInsert.setBigDecimal(i++, u.getStclfuca());
					psUtrnInsert.setBigDecimal(i++, u.getStclcoca());
					psUtrnInsert.setBigDecimal(i++, u.getSttrfuca());
					psUtrnInsert.setBigDecimal(i++, u.getSttrcoca());
					psUtrnInsert.setString(i++, u.getUnityp());
					psUtrnInsert.setString(i++, getUsrprf());
					psUtrnInsert.setString(i++, getJobnm());
					psUtrnInsert.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
					psUtrnInsert.addBatch();
				}
				psUtrnInsert.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("insertUstfstmRecord", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psUtrnInsert, null);
			}
		}
	}

	public void deleteUstfstmRecord(List<Ustfpf> ustfstmDeleteList) {
		String sql = "DELETE FROM USTFSTM WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Ustfpf u:ustfstmDeleteList){
				ps.setLong(1, u.getUnique_number());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteUstfstmRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public Map<String, List<Ustfpf>> readUstfData(String coy,List<String> chdrnumList) {
		StringBuilder sqlstmt = new StringBuilder();

		sqlstmt.append("SELECT CHDRCOY,CHDRNUM,USTMNO,PLNSFX,VRTFND,STOPDT,STOPFUCA,STOPCOCA,STOPUN,STOPDUN,LIFE,COVERAGE,RIDER,UNITYP,STCLDT,STCLUN,STCLDUN,STCLFUCA,STCLCOCA,STTRFUCA,STTRCOCA,STTRDUN,STTRUN ");
		sqlstmt.append("FROM USTF WHERE CHDRCOY=? AND ");

		sqlstmt.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlstmt.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement ps = getPrepareStatement(sqlstmt.toString());
		ResultSet rs = null;
		Map<String, List<Ustfpf>> ustfMap = new LinkedHashMap<String, List<Ustfpf>>();
		try {
			ps.setString(1, coy);
			rs = executeQuery(ps);
			while(rs.next()){
				Ustfpf ustfpf = new Ustfpf();
				ustfpf.setChdrcoy(rs.getString("CHDRCOY"));
				ustfpf.setChdrnum(rs.getString("CHDRNUM"));
				ustfpf.setUstmno(rs.getInt("USTMNO"));
				ustfpf.setPlnsfx(rs.getInt("PLNSFX"));
				ustfpf.setVrtfnd(rs.getString("VRTFND"));
				ustfpf.setStopdt(rs.getInt("STOPDT"));
				ustfpf.setStofuca(rs.getBigDecimal("STOPFUCA"));
				ustfpf.setStopcoca(rs.getBigDecimal("STOPCOCA"));
				ustfpf.setStopun(rs.getBigDecimal("STOPUN"));
				ustfpf.setStopdun(rs.getBigDecimal("STOPDUN"));
				ustfpf.setLife(rs.getString("LIFE"));
				ustfpf.setCoverage(rs.getString("COVERAGE"));
				ustfpf.setRider(rs.getString("RIDER"));
				ustfpf.setUnityp(rs.getString("UNITYP"));
				ustfpf.setStcldt(rs.getInt("STCLDT"));
				ustfpf.setStclun(rs.getBigDecimal("STCLUN"));
				ustfpf.setStcldun(rs.getBigDecimal("STCLDUN"));
				ustfpf.setStclfuca(rs.getBigDecimal("STCLFUCA"));
				ustfpf.setStclcoca(rs.getBigDecimal("STCLCOCA"));
				ustfpf.setSttrfuca(rs.getBigDecimal("STTRFUCA"));
				ustfpf.setSttrcoca(rs.getBigDecimal("STTRCOCA"));
				ustfpf.setSttrdun(rs.getBigDecimal("STTRDUN"));
				ustfpf.setSttrun(rs.getBigDecimal("STTRUN"));


				if (ustfMap.containsKey(ustfpf.getChdrnum())) {
					ustfMap.get(ustfpf.getChdrnum()).add(ustfpf);
				} else {
					List<Ustfpf> ustmList = new ArrayList<Ustfpf>();
					ustmList.add(ustfpf);
					ustfMap.put(ustfpf.getChdrnum().trim(), ustmList);
				}
			}	
		}catch(SQLException e) {
			LOGGER.error("UstmPfDAOImpl:getUstmDetails() threw SQLException :", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return ustfMap;
	}




}               