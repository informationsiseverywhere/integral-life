package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5102
 * @version 1.0 generated on 30/08/09 06:34
 * @author Quipoz
 */
public class S5102ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1269);
	public FixedLengthStringData dataFields = new FixedLengthStringData(517).isAPartOf(dataArea, 0);
	public ZonedDecimalData anvdate = DD.anvdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData aprvdate = DD.aprvdate.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,16);
	public ZonedDecimalData cancelDate = DD.canceldate.copyToZonedDecimal().isAPartOf(dataFields,24);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData claimcur = DD.claimcur.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData clmcurdsc = DD.clmcurdsc.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,56);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,64);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(dataFields,68);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(dataFields,150);
	public ZonedDecimalData finalPaydate = DD.epaydate.copyToZonedDecimal().isAPartOf(dataFields,160);
	public ZonedDecimalData firstPaydate = DD.fpaydate.copyToZonedDecimal().isAPartOf(dataFields,168);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,176);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,186);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,187);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,195);
	public ZonedDecimalData lastPaydate = DD.lpaydate.copyToZonedDecimal().isAPartOf(dataFields,242);
	public ZonedDecimalData nextPaydate = DD.npaydate.copyToZonedDecimal().isAPartOf(dataFields,250);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,258);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,266);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(dataFields,313);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,321);
	public ZonedDecimalData plansfx = DD.plansfx.copyToZonedDecimal().isAPartOf(dataFields,368);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,372);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(dataFields,376);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,381);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,391);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(dataFields,399);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(dataFields,416);
	public ZonedDecimalData revdte = DD.revdte.copyToZonedDecimal().isAPartOf(dataFields,418);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,426);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(dataFields,427);
	public FixedLengthStringData rgpyshort = DD.rgpyshort.copy().isAPartOf(dataFields,432);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(dataFields,442);
	public FixedLengthStringData rgpytypesd = DD.rgpytypesd.copy().isAPartOf(dataFields,444);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,454);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,464);
	public ZonedDecimalData totamnt = DD.totamnt.copyToZonedDecimal().isAPartOf(dataFields,474);
	public ZonedDecimalData totestamt = DD.totestamt.copyToZonedDecimal().isAPartOf(dataFields,492);
	public ZonedDecimalData riskcommdte = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,509);	//ILJ-48
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(188).isAPartOf(dataArea, 517);
	public FixedLengthStringData anvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData aprvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData canceldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData claimcurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData clmcurdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData destkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData epaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData fpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData lpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData npaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData paycltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData plansfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData regpayfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData revdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData rgpyshortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData rgpystatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData rgpytypesdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData totestamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData riskcommdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);	//ILJ-48
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(564).isAPartOf(dataArea, 705);
	public FixedLengthStringData[] anvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] aprvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] canceldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] claimcurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] clmcurdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] destkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] epaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] fpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] lpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] npaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] paycltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] plansfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] regpayfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] revdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] rgpyshortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] rgpystatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] rgpytypesdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] totestamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] riskcommdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);	//ILJ-48
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData anvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData aprvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cancelDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData finalPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData firstPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData revdteDisp = new FixedLengthStringData(10);
	//ILJ-48 Starts
	public FixedLengthStringData riskcommdteDisp = new FixedLengthStringData(10);
	//ILJ-48 End

	public LongData S5102screenWritten = new LongData(0);
	public LongData S5102protectWritten = new LongData(0);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return false;
	}


	public S5102ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plansfxOut,new String[] {null, null, "50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpymopOut,new String[] {"04","34","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(regpayfreqOut,new String[] {"05","35","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycltOut,new String[] {"01","36","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcntOut,new String[] {"60","38","-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(destkeyOut,new String[] {"11","37","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pymtOut,new String[] {"12","38","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimcurOut,new String[] {"75","39","-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtdateOut,new String[] {"13","44","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revdteOut,new String[] {"16","41","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fpaydateOut,new String[] {"15","23","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(npaydateOut,new String[] {"40","76","-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anvdateOut,new String[] {"18","42","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(epaydateOut,new String[] {"17","43","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(canceldateOut,new String[] {null, null, null, "31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"81", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, lifcnum, linsname, cownnum, ownername, occdate, rstate, pstate, ptdate, btdate, currcd, currds, plansfx, polinc, rgpynum, rgpytypesd, rgpystat, statdsc, rgpymop, rgpyshort, regpayfreq, frqdesc, payclt, payenme, prcnt, destkey, pymt, totamnt, totestamt, claimcur, clmcurdsc, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, cancelDate, ddind, crtable, descrip, fupflg , riskcommdte};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifcnumOut, linsnameOut, cownnumOut, ownernameOut, occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut, currcdOut, currdsOut, plansfxOut, polincOut, rgpynumOut, rgpytypesdOut, rgpystatOut, statdscOut, rgpymopOut, rgpyshortOut, regpayfreqOut, frqdescOut, paycltOut, payenmeOut, prcntOut, destkeyOut, pymtOut, totamntOut, totestamtOut, claimcurOut, clmcurdscOut, aprvdateOut, crtdateOut, revdteOut, fpaydateOut, lpaydateOut, npaydateOut, anvdateOut, epaydateOut, canceldateOut, ddindOut, crtableOut, descripOut, fupflgOut, riskcommdteOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifcnumErr, linsnameErr, cownnumErr, ownernameErr, occdateErr, rstateErr, pstateErr, ptdateErr, btdateErr, currcdErr, currdsErr, plansfxErr, polincErr, rgpynumErr, rgpytypesdErr, rgpystatErr, statdscErr, rgpymopErr, rgpyshortErr, regpayfreqErr, frqdescErr, paycltErr, payenmeErr, prcntErr, destkeyErr, pymtErr, totamntErr, totestamtErr, claimcurErr, clmcurdscErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, canceldateErr, ddindErr, crtableErr, descripErr, fupflgErr , riskcommdteErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, cancelDate, riskcommdte};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, canceldateErr , riskcommdteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, aprvdateDisp, crtdateDisp, revdteDisp, firstPaydateDisp, lastPaydateDisp, nextPaydateDisp, anvdateDisp, finalPaydateDisp, cancelDateDisp , riskcommdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5102screen.class;
		protectRecord = S5102protect.class;
	}

}
