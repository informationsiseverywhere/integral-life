package com.csc.life.unitlinkedprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Utrxpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String unitVirtualFund;
	private String unitType;
	private int tranno;
	private String batctrcde;
	private String unitSubAccount;
	private String nowDeferInd;
	private BigDecimal nofUnits;
	private int moniesDate;
	private String cntcurr;
	private BigDecimal nofDunits;
	private String feedbackInd;
	private BigDecimal contractAmount;
	private BigDecimal fundAmount;
	private String contractType;
	private int procSeqNo;
	private BigDecimal surrenderPercent;
	private BigDecimal fundRate;
	private String memberName;
	private String fundpool;
	
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(String unitVirtualFund) {
		this.unitVirtualFund = unitVirtualFund;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getUnitSubAccount() {
		return unitSubAccount;
	}
	public void setUnitSubAccount(String unitSubAccount) {
		this.unitSubAccount = unitSubAccount;
	}
	public String getNowDeferInd() {
		return nowDeferInd;
	}
	public void setNowDeferInd(String nowDeferInd) {
		this.nowDeferInd = nowDeferInd;
	}
	public BigDecimal getNofUnits() {
		return nofUnits;
	}
	public void setNofUnits(BigDecimal nofUnits) {
		this.nofUnits = nofUnits;
	}
	public int getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(int moniesDate) {
		this.moniesDate = moniesDate;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public BigDecimal getNofDunits() {
		return nofDunits;
	}
	public void setNofDunits(BigDecimal nofDunits) {
		this.nofDunits = nofDunits;
	}
	public String getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(String feedbackInd) {
		this.feedbackInd = feedbackInd;
	}
	public BigDecimal getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}
	public BigDecimal getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(BigDecimal fundAmount) {
		this.fundAmount = fundAmount;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public int getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(int procSeqNo) {
		this.procSeqNo = procSeqNo;
	}
	public BigDecimal getSurrenderPercent() {
		return surrenderPercent;
	}
	public void setSurrenderPercent(BigDecimal surrenderPercent) {
		this.surrenderPercent = surrenderPercent;
	}
	public BigDecimal getFundRate() {
		return fundRate;
	}
	public void setFundRate(BigDecimal fundRate) {
		this.fundRate = fundRate;
	}
	public String getFundPool() {
		return fundpool;
	}
	public void setFundPool(String fundpool) {
		this.fundpool = fundpool;
	}
}