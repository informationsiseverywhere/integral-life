/*
 * File: B5107.java
 * Date: 29 August 2009 20:56:51
 * Author: Quipoz Limited
 *
 * Class transformed from B5107.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.unitlinkedprocessing.reports.R5107Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  This program produces a report on the errors found by the unit
*  dealing program B5102, which it immediately follows as the final
*  step in a suite of programs. When errors are detected in B5102
*  members are created in UDERpf, a file uniquely for this purpose.
*  This is a very simple program which just outputs details of
*  all records held on UDER.
*
*  Records are extracted via an SQL SELECT with the criteria:
*       ORDER BY RECORDTYPE DESC, CHDRNUM
*
*  RECORDTYPE will either be UTRN or COVR (in that order as the
*  DESC for descending specifies), represented by 'U' or 'C'
*  respectively on the report.
*
*****************************************************************
* </pre>
*/
public class B5107 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqluderCursorrs = null;
	private java.sql.PreparedStatement sqluderCursorps = null;
	private java.sql.Connection sqluderCursorconn = null;
	private String sqluderCursor = "";
	private R5107Report printerFile = new R5107Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5107");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("F");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private Validator firstRec = new Validator(wsaaOverflow, "F");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/* SQLA-CODES */
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler1 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler1, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler1, 8);

		/* SQL-UTRNPF */
	private FixedLengthStringData sqlStorage = new FixedLengthStringData(80);
	private FixedLengthStringData sqlRectype = new FixedLengthStringData(4).isAPartOf(sqlStorage, 0);
	private FixedLengthStringData sqlLapind = new FixedLengthStringData(1).isAPartOf(sqlStorage, 4);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlStorage, 5);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlStorage, 13);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlStorage, 15);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlStorage, 17);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlStorage, 19);
	private FixedLengthStringData sqlVrtfnd = new FixedLengthStringData(4).isAPartOf(sqlStorage, 22);
	private FixedLengthStringData sqlUnityp = new FixedLengthStringData(1).isAPartOf(sqlStorage, 26);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(sqlStorage, 27);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(sqlStorage, 30);
	private PackedDecimalData sqlCntamnt = new PackedDecimalData(17, 2).isAPartOf(sqlStorage, 34);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlStorage, 43);
	private PackedDecimalData sqlFundamnt = new PackedDecimalData(17, 2).isAPartOf(sqlStorage, 46);
	private FixedLengthStringData sqlFndcurr = new FixedLengthStringData(3).isAPartOf(sqlStorage, 55);
	private PackedDecimalData sqlFundrate = new PackedDecimalData(18, 9).isAPartOf(sqlStorage, 58);
	private PackedDecimalData sqlAge = new PackedDecimalData(3, 0).isAPartOf(sqlStorage, 68);
	private FixedLengthStringData sqlTriger = new FixedLengthStringData(10).isAPartOf(sqlStorage, 70);
		/* ERRORS */
	private String ivrm = "IVRM";
	private String esql = "ESQL";
	private String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 1;

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5107h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5107h01O = new FixedLengthStringData(31).isAPartOf(r5107h01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5107h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5107h01O, 1);

	private FixedLengthStringData r5107d01Record = new FixedLengthStringData(105);
	private FixedLengthStringData r5107d01O = new FixedLengthStringData(105).isAPartOf(r5107d01Record, 0);
	private FixedLengthStringData rectype = new FixedLengthStringData(1).isAPartOf(r5107d01O, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5107d01O, 1);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5107d01O, 9);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5107d01O, 11);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5107d01O, 13);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5107d01O, 15);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r5107d01O, 19);
	private FixedLengthStringData unityp = new FixedLengthStringData(1).isAPartOf(r5107d01O, 23);
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(r5107d01O, 24);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5107d01O, 29);
	private ZonedDecimalData cntamnt = new ZonedDecimalData(17, 2).isAPartOf(r5107d01O, 33);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(r5107d01O, 50);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2).isAPartOf(r5107d01O, 53);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(r5107d01O, 70);
	private ZonedDecimalData fundrate = new ZonedDecimalData(18, 9).isAPartOf(r5107d01O, 73);
	private FixedLengthStringData lapind = new FixedLengthStringData(1).isAPartOf(r5107d01O, 91);
	private ZonedDecimalData age = new ZonedDecimalData(3, 0).isAPartOf(r5107d01O, 92);
	private FixedLengthStringData triger = new FixedLengthStringData(10).isAPartOf(r5107d01O, 95);

	private FixedLengthStringData r5107e01Record = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();

	// Added by Quoc Nguyen (ILIFE-20 - Batch L2NEWUNITD reports not generated - SubTask: ILIFE-29)

	private boolean hasErrorData = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endOfCursor2080,
		exit2090
	}

	public B5107() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		wsaaOverflow.set("F");
		printerFile.openOutput();
	}

protected void setUpHeadingCompany1020()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(bsprIO.getCompany());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			companynm.fill("?");
		}
		else {
			companynm.set(getdescrec.longdesc);
		}
		company.set(bsprIO.getCompany());
		sqluderCursor = " SELECT  RECORDTYPE, LAPIND, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, VRTFND, UNITYP, TRANNO, BATCTRCDE, CNTAMNT, CNTCURR, FUNDAMNT, FNDCURR, FUNDRATE, AGE, TRIGER" +
" FROM   " + appVars.getTableNameOverriden("UDERPF") + " " +
" ORDER BY RECORDTYPE DESC, CHDRNUM";
		sqlerrorflag = false;
		try {
			sqluderCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.unitlinkedprocessing.dataaccess.UderpfTableDAM());
			sqluderCursorps = appVars.prepareStatementEmbeded(sqluderCursorconn, sqluderCursor, "UDERPF");
			sqluderCursorrs = appVars.executeQuery(sqluderCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case endOfCursor2080: {
					endOfCursor2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqluderCursorrs.next())
			{
				hasErrorData = true;

				appVars.getDBObject(sqluderCursorrs, 1, sqlRectype);
				appVars.getDBObject(sqluderCursorrs, 2, sqlLapind);
				appVars.getDBObject(sqluderCursorrs, 3, sqlChdrnum);
				appVars.getDBObject(sqluderCursorrs, 4, sqlLife);
				appVars.getDBObject(sqluderCursorrs, 5, sqlCoverage);
				appVars.getDBObject(sqluderCursorrs, 6, sqlRider);
				appVars.getDBObject(sqluderCursorrs, 7, sqlPlnsfx);
				appVars.getDBObject(sqluderCursorrs, 8, sqlVrtfnd);
				appVars.getDBObject(sqluderCursorrs, 9, sqlUnityp);
				appVars.getDBObject(sqluderCursorrs, 10, sqlTranno);
				appVars.getDBObject(sqluderCursorrs, 11, sqlBatctrcde);
				appVars.getDBObject(sqluderCursorrs, 12, sqlCntamnt);
				appVars.getDBObject(sqluderCursorrs, 13, sqlCntcurr);
				appVars.getDBObject(sqluderCursorrs, 14, sqlFundamnt);
				appVars.getDBObject(sqluderCursorrs, 15, sqlFndcurr);
				appVars.getDBObject(sqluderCursorrs, 16, sqlFundrate);
				appVars.getDBObject(sqluderCursorrs, 17, sqlAge);
				appVars.getDBObject(sqluderCursorrs, 18, sqlTriger);

			} else {
				goTo(GotoLabel.endOfCursor2080);
			}

		} catch (SQLException ex){
			sqlca = ex;
			if (hasErrorData) {
				sqlerrorflag = true;
			} else {
				wsspEdterror.set(varcom.endp);
			}
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		appVars.freeDBConnectionIgnoreErr(sqluderCursorconn, sqluderCursorps, sqluderCursorrs);

		if (!hasErrorData)
		{
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printR5107h01(r5107h01Record, indicArea);
			wsaaOverflow.set("N");
		}

		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (newPageReq.isTrue() || firstRec.isTrue()) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printR5107h01(r5107h01Record, indicArea);
			wsaaOverflow.set("N");
		}
		if (isEQ(sqlRectype,"UTRN")) {
			rectype.set("U");
		}
		if (isEQ(sqlRectype,"COVR")) {
			rectype.set("C");
		}
		lapind.set(sqlLapind);
		chdrnum.set(sqlChdrnum);
		life.set(sqlLife);
		coverage.set(sqlCoverage);
		rider.set(sqlRider);
		plnsfx.set(sqlPlnsfx);
		vrtfnd.set(sqlVrtfnd);
		unityp.set(sqlUnityp);
		tranno.set(sqlTranno);
		batctrcde.set(sqlBatctrcde);
		cntamnt.set(sqlCntamnt);
		cntcurr.set(sqlCntcurr);
		fundamnt.set(sqlFundamnt);
		fndcurr.set(sqlFndcurr);
		fundrate.set(sqlFundrate);
		age.set(sqlAge);
		triger.set(sqlTriger);

		printerFile.printR5107d01(r5107d01Record, indicArea);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (firstRec.isTrue()) {
			/*CONTINUE_STMT*/
		}
		else {
			printerFile.printR5107e01(r5107e01Record, indicArea);
		}
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
