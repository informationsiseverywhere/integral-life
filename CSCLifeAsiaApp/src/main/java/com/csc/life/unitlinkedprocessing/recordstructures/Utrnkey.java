package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:49
 * Description:
 * Copybook name: UTRNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnKey = new FixedLengthStringData(64).isAPartOf(utrnFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnKey, 0);
  	public FixedLengthStringData utrnChdrnum = new FixedLengthStringData(8).isAPartOf(utrnKey, 1);
  	public FixedLengthStringData utrnLife = new FixedLengthStringData(2).isAPartOf(utrnKey, 9);
  	public FixedLengthStringData utrnCoverage = new FixedLengthStringData(2).isAPartOf(utrnKey, 11);
  	public FixedLengthStringData utrnRider = new FixedLengthStringData(2).isAPartOf(utrnKey, 13);
  	public PackedDecimalData utrnPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnKey, 15);
  	public FixedLengthStringData utrnUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnKey, 18);
  	public FixedLengthStringData utrnUnitType = new FixedLengthStringData(1).isAPartOf(utrnKey, 22);
  	public PackedDecimalData utrnTranno = new PackedDecimalData(5, 0).isAPartOf(utrnKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(utrnKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}