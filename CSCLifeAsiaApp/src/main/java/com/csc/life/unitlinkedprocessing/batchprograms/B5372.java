/*
 * File: B5372.java
 * Date: 29 August 2009 21:16:05
 * Author: Quipoz Limited
 *
 * Class transformed from B5372.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.BwrkTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* B5372 - Initial Unit Cancellation.
* ----------------------------------
*
* This program processes coverages which are due for initial unit
* cancellation based on the Initial Unit Cancellation Date of the
* coverage.
* The program selects valid coverages with a valid contract based
* on the following selection criteria:
*
*     Chdrnum                within P6671 contract range
*     And Chdrcoy             = Batch Run Company
*     And Contract Branch     = Batch Run Branch
*     And Initial Unit Cancellation Date <= Batch Run Date
*     And Initial Unit Cancellation Date not = 0
*     And Billing Channel not = 'N'
*
* The records are ordered in coverage key sequence, namely:
*
*     Chdrcoy, Chdrnum, Life, Coverage, Rider, Plan-Suffix.
*
* The contract and coverage status codes are validated against
* T5679.
*
* Using the Coverage type, the General Unit Linked Details table,
* T5540, is read to obtain the Initial Unit Discount Factor. This
* is used to read Initial Unit Discount Basis table, T5519.  If
* either of the table reads are unsuccessful or if the processing
* subroutine on T5519 is empty, do not process the coverage.
*
* The processing subroutine on T5519 is called for the coverage.
*
* The Initial Unit Cancellation Date on the coverage is increased
* by one frequency from T5519 before updating the coverage.
*
* On each contract break, the Contract Header record is updated
* and a PTRN transaction record written.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5372 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlcovrpf1rs = null;
	private java.sql.PreparedStatement sqlcovrpf1ps = null;
	private java.sql.Connection sqlcovrpf1conn = null;
	private String sqlcovrpf1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5372");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* SQL Host Variables*/
	private FixedLengthStringData wsaaSqlCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSqlBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSqlValidflag = new FixedLengthStringData(1).init("1");
	private ZonedDecimalData wsaaSqlDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
		/* T5519 array variables.*/
	//private final int wsaaT5519Size = 20;
	//ILIFE-2628 fixed--Array size increased
	private final int wsaaT5519Size = 1000;
	private PackedDecimalData wsaaT5519IxMax = new PackedDecimalData(5, 0);

		/* WSAA-T5519-ARRAY */
	private FixedLengthStringData[] wsaaT5519Rec = FLSInittedArray (1000, 31);
	private FixedLengthStringData[] wsaaT5519Key = FLSDArrayPartOfArrayStructure(4, wsaaT5519Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5519Iudiscbas = FLSDArrayPartOfArrayStructure(4, wsaaT5519Key, 0);
	private FixedLengthStringData[] wsaaT5519Data = FLSDArrayPartOfArrayStructure(27, wsaaT5519Rec, 4);
	private PackedDecimalData[] wsaaT5519Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5519Data, 0);
	private PackedDecimalData[] wsaaT5519Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT5519Data, 5);
	private FixedLengthStringData[] wsaaT5519Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT5519Data, 10);
	private FixedLengthStringData[] wsaaT5519Iuchfrq = FLSDArrayPartOfArrayStructure(2, wsaaT5519Data, 20);
	private ZonedDecimalData[] wsaaT5519Annchg = ZDArrayPartOfArrayStructure(5, 2, wsaaT5519Data, 22);
	private String wsaaT5519ItemFound = "";
		/* T5540 array variables.*/
	//private static final int wsaaT5540Size = 40
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5540Size = 1000;
	private PackedDecimalData wsaaT5540IxMax = new PackedDecimalData(5, 0);

		/* WSAA-T5540-ARRAY */
	private FixedLengthStringData[] wsaaT5540Rec = FLSInittedArray (1000, 18);
	private FixedLengthStringData[] wsaaT5540Key = FLSDArrayPartOfArrayStructure(4, wsaaT5540Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5540Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5540Key, 0);
	private FixedLengthStringData[] wsaaT5540Data = FLSDArrayPartOfArrayStructure(14, wsaaT5540Rec, 4);
	private PackedDecimalData[] wsaaT5540Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5540Data, 0);
	private PackedDecimalData[] wsaaT5540Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT5540Data, 5);
	private FixedLengthStringData[] wsaaT5540Iudiscbas = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 10);
	private String wsaaT5540ItemFound = "";
	private FixedLengthStringData wsaaIudiscbas = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSubprog = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaIuchfrq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaAnnchg = new ZonedDecimalData(5, 2);
	private String wsaaCovrValid = "";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaBwrkDetails = new FixedLengthStringData(20);
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaBwrkDetails, 0);
	private FixedLengthStringData lockedContract = new FixedLengthStringData(8).isAPartOf(wsaaBwrkDetails, 8);
	private FixedLengthStringData validContract = new FixedLengthStringData(1).isAPartOf(wsaaBwrkDetails, 16);
	private PackedDecimalData newTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaBwrkDetails, 17);
		/* TABLES */
	private static final String t5519 = "T5519";
	private static final String t5540 = "T5540";
	private static final String t5679 = "T5679";
		/* ERRORS */
	private static final String h791 = "H791";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5519Ix = new IntegerData();
	private IntegerData wsaaT5540Ix = new IntegerData();
	private BwrkTableDAM bwrkIO = new BwrkTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private T5519rec t5519rec = new T5519rec();
	private T5540rec t5540rec = new T5540rec();
	private T5679rec t5679rec = new T5679rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private P6671par p6671par = new P6671par();
	private FormatsInner formatsInner = new FormatsInner();
	private SqlCovrpfInner sqlCovrpfInner = new SqlCovrpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endOfFile2050,
		exit2090,
		checkValidContract2500,
		exit2590
	}

	public B5372() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		bwrkIO.setFunction(varcom.readh);
		bwrkIO.setDataKey(bsprIO.getDataKey());
		bwrkIO.setFormat(formatsInner.bwrkrec);
		SmartFileCode.execute(appVars, bwrkIO);
		if (isNE(bwrkIO.getStatuz(),varcom.oK)
		&& isNE(bwrkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(bwrkIO.getStatuz());
			syserrrec.params.set(bwrkIO.getParams());
			fatalError600();
		}
		if (isEQ(bwrkIO.getStatuz(),varcom.oK)) {
			wsaaBwrkDetails.set(bwrkIO.getWorkAreaData());
		}
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5519);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItempfx("IT");
		wsaaT5519Ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadT55191100();
		}

		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItempfx("IT");
		wsaaT5540Ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadT55401200();
		}

		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		else {
			wsaaChdrnumfrm.set(p6671par.chdrnum);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
		else {
			wsaaChdrnumto.set(p6671par.chdrnum1);
		}
		wsaaSqlCompany.set(batcdorrec.company);
		wsaaSqlDate.set(bsscIO.getEffectiveDate());
		sqlcovrpf1 = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, STATCODE, PSTATCODE, CRTABLE, CRRCD, ICANDT" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + " " +
" WHERE CHDRNUM BETWEEN ? AND ?" +
" AND CHDRCOY = ?" +
" AND VALIDFLAG = ?" +
" AND ICANDT <= ?" +
" AND NOT ICANDT = 0" +
" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX";
		sqlerrorflag = false;
		try {
			sqlcovrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlcovrpf1ps = getAppVars().prepareStatementEmbeded(sqlcovrpf1conn, sqlcovrpf1, "COVRPF");
			getAppVars().setDBString(sqlcovrpf1ps, 1, wsaaChdrnumfrm);
			getAppVars().setDBString(sqlcovrpf1ps, 2, wsaaChdrnumto);
			getAppVars().setDBString(sqlcovrpf1ps, 3, wsaaSqlCompany);
			getAppVars().setDBString(sqlcovrpf1ps, 4, wsaaSqlValidflag);
			getAppVars().setDBNumber(sqlcovrpf1ps, 5, wsaaSqlDate);
			sqlcovrpf1rs = getAppVars().executeQuery(sqlcovrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError();
		}
	}

protected void loadT55191100()
	{
			begn1110();
		}

protected void begn1110()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemtabl(),t5519)
		|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
			/*****     END-IF                                                   */
		}
		if (isGT(wsaaT5519Ix,wsaaT5519Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5519);
			fatalError600();
		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
		wsaaT5519Key[wsaaT5519Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5519Itmfrm[wsaaT5519Ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT5519Itmto[wsaaT5519Ix.toInt()].set(itdmIO.getItmto());
		wsaaT5519Subprog[wsaaT5519Ix.toInt()].set(t5519rec.subprog);
		wsaaT5519Iuchfrq[wsaaT5519Ix.toInt()].set(t5519rec.initUnitChargeFreq);
		wsaaT5519Annchg[wsaaT5519Ix.toInt()].set(t5519rec.annchg);
		itdmIO.setFunction(varcom.nextr);
		wsaaT5519IxMax.set(wsaaT5519Ix);
		wsaaT5519Ix.add(1);
	}

protected void loadT55401200()
	{
			begn1210();
		}

protected void begn1210()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemtabl(),t5540)
		|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			if (isEQ(itdmIO.getFunction(),varcom.begn)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			else {
				itdmIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isGT(wsaaT5540Ix,wsaaT5540Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5540);
			fatalError600();
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		wsaaT5540Key[wsaaT5540Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5540Itmfrm[wsaaT5540Ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT5540Itmto[wsaaT5540Ix.toInt()].set(itdmIO.getItmto());
		wsaaT5540Iudiscbas[wsaaT5540Ix.toInt()].set(t5540rec.iuDiscBasis);
		itdmIO.setFunction(varcom.nextr);
		wsaaT5540IxMax.set(wsaaT5540Ix);
		wsaaT5540Ix.add(1);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile2010();
				case endOfFile2050:
					endOfFile2050();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlcovrpf1rs)) {
				getAppVars().getDBObject(sqlcovrpf1rs, 1, sqlCovrpfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlcovrpf1rs, 2, sqlCovrpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlcovrpf1rs, 3, sqlCovrpfInner.sqlLife);
				getAppVars().getDBObject(sqlcovrpf1rs, 4, sqlCovrpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlcovrpf1rs, 5, sqlCovrpfInner.sqlRider);
				getAppVars().getDBObject(sqlcovrpf1rs, 6, sqlCovrpfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlcovrpf1rs, 7, sqlCovrpfInner.sqlCostatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 8, sqlCovrpfInner.sqlCopstatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 9, sqlCovrpfInner.sqlCrtable);
				getAppVars().getDBObject(sqlcovrpf1rs, 10, sqlCovrpfInner.sqlCrrcd);
				getAppVars().getDBObject(sqlcovrpf1rs, 11, sqlCovrpfInner.sqlIcandt);
			}
			else {
				goTo(GotoLabel.endOfFile2050);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError();
		}
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2050()
	{
		wsaaEof.set("Y");
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					edit2510();
				case checkValidContract2500:
					checkValidContract2500();
				case exit2590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2510()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/* If the contract is already softlocked, do no further*/
		/* validation and do not process the record.*/
		if (isEQ(sqlCovrpfInner.sqlChdrnum, lockedContract)) {
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		/* If we are dealing with the same contract number, do not*/
		/* process the record if the contract was found to be invalid.*/
		/* If the contract was valid, skip the CHDR validation checks*/
		/* and softlocking of the contract and perform coverage*/
		/* validation.*/
		if (isEQ(sqlCovrpfInner.sqlChdrnum, lastChdrnum)) {
			if (isNE(validContract,"Y")) {
				contotrec.totno.set(ct05);
				contotrec.totval.set(1);
				callContot001();
				wsspEdterror.set(SPACES);
				goTo(GotoLabel.exit2590);
			}
			else {
				wsaaCovrValid = "N";
				validateCovr2700();
				goTo(GotoLabel.exit2590);
			}
		}
		/* Validate Contract Header Status codes.*/
		validContract.set("N");
		readChdr2600a();
		if (isEQ(chdrlifIO.getBillchnl(),"N")) {
			goTo(GotoLabel.checkValidContract2500);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			validateChdrStatus2600();
		}
	}

protected void checkValidContract2500()
	{
		if (isNE(validContract,"Y")) {
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/* Validate Coverage.*/
		wsaaCovrValid = "N";
		validateCovr2700();
		if (isEQ(wsaaCovrValid,"N")) {
			return ;
		}
		softlockContract2800();
	}

protected void readChdr2600a()
	{
		/*A-START*/
		chdrlifIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		chdrlifIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*A-EXIT*/
	}

protected void validateChdrStatus2600()
	{
		/*CHECK*/
		/*   IF T5679-CN-RISK-STAT(WSAA-SUB) = SQL-CHSTATCODE             */
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()],chdrlifIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
				validateChdrPremStatus2650();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus2650()
	{
		/*CHECK*/
		/*    IF T5679-CN-PREM-STAT(WSAA-SUB) = SQL-CHPSTATCODE            */
		if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()],chdrlifIO.getPstatcode())) {
			wsaaSub.set(13);
			validContract.set("Y");
		}
		/*EXIT*/
	}

protected void validateCovr2700()
	{
					check2710();
					validCheck2715();
				}

protected void check2710()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			validateCovrStatus2720();
		}
		if (isNE(wsaaCovrValid,"Y")) {
			return ;
		}
		/* Look for the coverage type in the T5540 working storage*/
		/* array.  If it is not found or the discount basis on the item*/
		/* found is equal to spaces, do not process the coverage.*/
		wsaaT5540ItemFound = "N";
		checkT55402760();
		if (isNE(wsaaT5540ItemFound,"Y")
		|| isEQ(wsaaIudiscbas,SPACES)) {
			wsaaCovrValid = "N";
			return ;
		}
		/* Look for the discount basis in the T5519 working storage*/
		/* array.  If it is not found or the processing subroutine on*/
		/* the item found is equal to spaces, do not process the*/
		/* coverage.*/
		wsaaT5519ItemFound = "N";
		checkT55192780();
		if (isNE(wsaaT5519ItemFound,"Y")
		|| isEQ(wsaaSubprog,SPACES)) {
			wsaaCovrValid = "N";
		}
	}

protected void validCheck2715()
	{
		if (isEQ(wsaaCovrValid,"N")) {
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/*EXIT*/
	}

protected void validateCovrStatus2720()
	{
		/*CHECK*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], sqlCovrpfInner.sqlCostatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
				validateCovrPremStatus2740();
			}
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus2740()
	{
		/*CHECK*/
		if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], sqlCovrpfInner.sqlCopstatcode)) {
			wsaaSub.set(13);
			wsaaCovrValid = "Y";
		}
		/*EXIT*/
	}

protected void checkT55402760()
	{
		/*SEARCH*/
		for (wsaaT5540Ix.set(wsaaT5540IxMax); !(isEQ(wsaaT5540Ix,0)
		|| isEQ(wsaaT5540ItemFound,"Y")); wsaaT5540Ix.add(-1)){
			if (isEQ(sqlCovrpfInner.sqlCrtable, wsaaT5540Key[wsaaT5540Ix.toInt()])
			&& isGTE(sqlCovrpfInner.sqlCrrcd, wsaaT5540Itmfrm[wsaaT5540Ix.toInt()])
			&& isLTE(sqlCovrpfInner.sqlCrrcd, wsaaT5540Itmto[wsaaT5540Ix.toInt()])) {
				wsaaT5540ItemFound = "Y";
				wsaaIudiscbas.set(wsaaT5540Iudiscbas[wsaaT5540Ix.toInt()]);
			}
		}
		/*EXIT*/
	}

protected void checkT55192780()
	{
		/*SEARCH*/
		for (wsaaT5519Ix.set(wsaaT5519IxMax); !(isEQ(wsaaT5519Ix,0)
		|| isEQ(wsaaT5519ItemFound,"Y")); wsaaT5519Ix.add(-1)){
			if (isEQ(wsaaT5519Key[wsaaT5519Ix.toInt()],wsaaIudiscbas)
			&& isGTE(sqlCovrpfInner.sqlCrrcd, wsaaT5519Itmfrm[wsaaT5519Ix.toInt()])
			&& isLTE(sqlCovrpfInner.sqlCrrcd, wsaaT5519Itmto[wsaaT5519Ix.toInt()])) {
				wsaaT5519ItemFound = "Y";
				wsaaIuchfrq.set(wsaaT5519Iuchfrq[wsaaT5519Ix.toInt()]);
				wsaaAnnchg.set(wsaaT5519Annchg[wsaaT5519Ix.toInt()]);
				wsaaSubprog.set(wsaaT5519Subprog[wsaaT5519Ix.toInt()]);
			}
		}
		/*EXIT*/
	}

protected void softlockContract2800()
	{
		softlock2810();
	}

protected void softlock2810()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(sqlCovrpfInner.sqlChdrcoy);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sqlCovrpfInner.sqlChdrnum);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(sqlCovrpfInner.sqlChdrcoy);
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(sqlCovrpfInner.sqlChdrnum);
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			lockedContract.set(sqlCovrpfInner.sqlChdrnum);
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		/* If this is a new contract, update the contract and write*/
		/* a PTRN.  This must be done prior to updating the coverage*/
		/* because the processing subroutine uses the transaction*/
		/* number on the contract when writing UTRNs.*/
		if (isNE(sqlCovrpfInner.sqlChdrnum, lastChdrnum)) {
			updateChdrWritePtrn3100();
		}
		callT5519Subroutine3200();
		writeVf2Covr3300();
		writeNewCovr3400();
		/*EXIT*/
	}

protected void updateChdrWritePtrn3100()
	{
		readh3110();
	}

protected void readh3110()
	{
		lastChdrnum.set(sqlCovrpfInner.sqlChdrnum);
		chdrlifIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		chdrlifIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		compute(newTranno, 0).set(add(chdrlifIO.getTranno(),1));
		chdrlifIO.setTranno(newTranno);
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/* Write PTRN.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTransactionDate(sqlCovrpfInner.sqlIcandt);
		ptrnIO.setPtrneff(sqlCovrpfInner.sqlIcandt);
		/*                                PTRN-PTRNEFF                  */
		/*                                PTRN-DATESUB.                 */
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(subString(bsscIO.getDatimeInit(), 21, 6));
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setBatccoy(batcdorrec.company);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		ptrnIO.setBatctrcde(bprdIO.getAuthCode());
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
	}

protected void callT5519Subroutine3200()
	{
		call3210();
	}

protected void call3210()
	{
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(sqlCovrpfInner.sqlChdrcoy);
		annprocrec.chdrnum.set(sqlCovrpfInner.sqlChdrnum);
		annprocrec.life.set(sqlCovrpfInner.sqlLife);
		annprocrec.coverage.set(sqlCovrpfInner.sqlCoverage);
		annprocrec.rider.set(sqlCovrpfInner.sqlRider);
		annprocrec.planSuffix.set(sqlCovrpfInner.sqlPlnsfx);
		annprocrec.effdate.set(sqlCovrpfInner.sqlIcandt);
		annprocrec.batctrcde.set(bprdIO.getAuthCode());
		annprocrec.crdate.set(sqlCovrpfInner.sqlCrrcd);
		annprocrec.crtable.set(sqlCovrpfInner.sqlCrtable);
		annprocrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
		annprocrec.batcactyr.set(batcdorrec.actyear);
		annprocrec.batcactmn.set(batcdorrec.actmonth);
		annprocrec.batccoy.set(batcdorrec.company);
		annprocrec.batcbrn.set(batcdorrec.branch);
		annprocrec.batcbatch.set(batcdorrec.batch);
		annprocrec.batcpfx.set(batcdorrec.prefix);
		annprocrec.initUnitChargeFreq.set(wsaaIuchfrq);
		annprocrec.annchg.set(wsaaAnnchg);
		callProgram(wsaaSubprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError600();
		}
	}

protected void writeVf2Covr3300()
	{
		readh3310();
	}

protected void readh3310()
	{
		covrIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		covrIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		covrIO.setLife(sqlCovrpfInner.sqlLife);
		covrIO.setCoverage(sqlCovrpfInner.sqlCoverage);
		covrIO.setRider(sqlCovrpfInner.sqlRider);
		covrIO.setPlanSuffix(sqlCovrpfInner.sqlPlnsfx);
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		covrIO.setValidflag("2");
		covrIO.setCurrto(sqlCovrpfInner.sqlIcandt);
		covrIO.setFunction(varcom.rewrt);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
	}

protected void writeNewCovr3400()
	{
		writr3410();
	}

protected void writr3410()
	{
		if (isEQ(covrIO.getInitUnitCancDate(),0)) {
			setConversionDate3430();
		}
		else {
			setNextCancelDate3450();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrfrom(sqlCovrpfInner.sqlIcandt);
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setTranno(newTranno);
		covrIO.setFunction(varcom.writr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
	}

protected void setConversionDate3430()
	{
		/*PARA*/
		covrIO.setConvertInitialUnits(ZERO);
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void setNextCancelDate3450()
	{
		para3450();
	}

protected void para3450()
	{
		if (isNE(wsaaIuchfrq,NUMERIC)) {
			wsaaIuchfrq.set("00");
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set(wsaaIuchfrq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(covrIO.getInitUnitCancDate());
		datcon2rec.intDate2.set(0);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		covrIO.setInitUnitCancDate(datcon2rec.intDate2);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		bwrkIO.setDataKey(bsprIO.getDataKey());
		bwrkIO.setWorkAreaData(wsaaBwrkDetails);
		bwrkIO.setFormat(formatsInner.bwrkrec);
		bwrkIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, bwrkIO);
		if (isNE(bwrkIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(bwrkIO.getStatuz());
			syserrrec.params.set(bwrkIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/* Close SQL file.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlcovrpf1conn, sqlcovrpf1ps, sqlcovrpf1rs);
		/* Delete the Batch working area record.                        */
		bwrkIO.setDataKey(bsprIO.getDataKey());
		bwrkIO.setFormat(formatsInner.bwrkrec);
		bwrkIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bwrkIO);
		if (isNE(bwrkIO.getStatuz(),varcom.oK)
		&& isNE(bwrkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(bwrkIO.getStatuz());
			syserrrec.params.set(bwrkIO.getParams());
			fatalError600();
		}
		if (isEQ(bwrkIO.getStatuz(),varcom.oK)) {
			bwrkIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, bwrkIO);
			if (isNE(bwrkIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(bwrkIO.getStatuz());
				syserrrec.params.set(bwrkIO.getParams());
				fatalError600();
			}
		}
		lsaaStatuz.set(varcom.oK);
	}

protected void sqlError()
	{
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.statuz.set(sqlStatuz);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData bwrkrec = new FixedLengthStringData(10).init("BWRKREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
}
/*
 * Class transformed  from Data Structure SQL-COVRPF--INNER
 */
private static final class SqlCovrpfInner {

		/* SQL-COVRPF */
	private FixedLengthStringData sqlCovrrec = new FixedLengthStringData(36);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlCovrrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlCovrrec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 9);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 11);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 13);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlCovrrec, 15);
	private FixedLengthStringData sqlCostatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 18);
	private FixedLengthStringData sqlCopstatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 20);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlCovrrec, 22);
	private PackedDecimalData sqlCrrcd = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 26);
	private PackedDecimalData sqlIcandt = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 31);
}
}
