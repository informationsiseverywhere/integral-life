package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5540screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5540ScreenVars sv = (S5540ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5540screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5540ScreenVars screenVars = (S5540ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.allbas.setClassString("");
		screenVars.rvwproc.setClassString("");
		screenVars.fundSplitPlan.setClassString("");
		screenVars.iuDiscBasis.setClassString("");
		screenVars.ltypst.setClassString("");
		screenVars.iuDiscFact.setClassString("");
		screenVars.wdmeth.setClassString("");
		screenVars.alfnds.setClassString("");
		screenVars.maxfnd.setClassString("");
		screenVars.wholeIuDiscFact.setClassString("");
		screenVars.unitCancInit.setClassString("");
		screenVars.zafropt1.setClassString("");//BRD-411
		screenVars.zvariance.setClassString("");//BRD-411
	}

/**
 * Clear all the variables in S5540screen
 */
	public static void clear(VarModel pv) {
		S5540ScreenVars screenVars = (S5540ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.allbas.clear();
		screenVars.rvwproc.clear();
		screenVars.fundSplitPlan.clear();
		screenVars.iuDiscBasis.clear();
		screenVars.ltypst.clear();
		screenVars.iuDiscFact.clear();
		screenVars.wdmeth.clear();
		screenVars.alfnds.clear();
		screenVars.maxfnd.clear();
		screenVars.wholeIuDiscFact.clear();
		screenVars.unitCancInit.clear();
		screenVars.zafropt1.clear();//BRD-411
		screenVars.zvariance.clear();//BRD-411
	}
}
