package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:22:07
 * Description:
 * Copybook name: VPRCUPBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vprcupbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData vprcupbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData vprcupbKey = new FixedLengthStringData(256).isAPartOf(vprcupbFileKey, 0, REDEFINE);
  	public FixedLengthStringData vprcupbCompany = new FixedLengthStringData(1).isAPartOf(vprcupbKey, 0);
  	public PackedDecimalData vprcupbEffdate = new PackedDecimalData(8, 0).isAPartOf(vprcupbKey, 1);
  	public PackedDecimalData vprcupbJobno = new PackedDecimalData(8, 0).isAPartOf(vprcupbKey, 6);
  	public FixedLengthStringData vprcupbUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(vprcupbKey, 11);
  	public FixedLengthStringData vprcupbUnitType = new FixedLengthStringData(1).isAPartOf(vprcupbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(vprcupbKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(vprcupbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		vprcupbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}