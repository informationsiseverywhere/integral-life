package com.csc.life.unitlinkedprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5542screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5542ScreenVars sv = (S5542ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5542screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5542ScreenVars screenVars = (S5542ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.billfreq01.setClassString("");
		screenVars.wdlFreq01.setClassString("");
		screenVars.wdlAmount01.setClassString("");
		screenVars.wdrem01.setClassString("");
		screenVars.ffamt01.setClassString("");
		screenVars.feepc01.setClassString("");
		screenVars.feemin01.setClassString("");
		screenVars.feemax01.setClassString("");
		screenVars.billfreq02.setClassString("");
		screenVars.wdlFreq02.setClassString("");
		screenVars.wdlAmount02.setClassString("");
		screenVars.wdrem02.setClassString("");
		screenVars.ffamt02.setClassString("");
		screenVars.feepc02.setClassString("");
		screenVars.feemin02.setClassString("");
		screenVars.feemax02.setClassString("");
		screenVars.billfreq03.setClassString("");
		screenVars.wdlFreq03.setClassString("");
		screenVars.wdlAmount03.setClassString("");
		screenVars.wdrem03.setClassString("");
		screenVars.ffamt03.setClassString("");
		screenVars.feepc03.setClassString("");
		screenVars.feemin03.setClassString("");
		screenVars.feemax03.setClassString("");
		screenVars.billfreq04.setClassString("");
		screenVars.wdlFreq04.setClassString("");
		screenVars.wdlAmount04.setClassString("");
		screenVars.wdrem04.setClassString("");
		screenVars.ffamt04.setClassString("");
		screenVars.feepc04.setClassString("");
		screenVars.feemin04.setClassString("");
		screenVars.feemax04.setClassString("");
		screenVars.billfreq05.setClassString("");
		screenVars.wdlFreq05.setClassString("");
		screenVars.wdlAmount05.setClassString("");
		screenVars.wdrem05.setClassString("");
		screenVars.ffamt05.setClassString("");
		screenVars.feepc05.setClassString("");
		screenVars.feemin05.setClassString("");
		screenVars.feemax05.setClassString("");
		screenVars.billfreq06.setClassString("");
		screenVars.wdlFreq06.setClassString("");
		screenVars.wdlAmount06.setClassString("");
		screenVars.wdrem06.setClassString("");
		screenVars.ffamt06.setClassString("");
		screenVars.feepc06.setClassString("");
		screenVars.feemin06.setClassString("");
		screenVars.feemax06.setClassString("");
		screenVars.maxwith01.setClassString("");//ILIFE-7956
		screenVars.maxwith02.setClassString("");//ILIFE-7956
		screenVars.maxwith03.setClassString("");//ILIFE-7956
		screenVars.maxwith04.setClassString("");//ILIFE-7956
		screenVars.maxwith05.setClassString("");//ILIFE-7956
		screenVars.maxwith06.setClassString("");//ILIFE-7956
		
	}

/**
 * Clear all the variables in S5542screen
 */
	public static void clear(VarModel pv) {
		S5542ScreenVars screenVars = (S5542ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.billfreq01.clear();
		screenVars.wdlFreq01.clear();
		screenVars.wdlAmount01.clear();
		screenVars.wdrem01.clear();
		screenVars.ffamt01.clear();
		screenVars.feepc01.clear();
		screenVars.feemin01.clear();
		screenVars.feemax01.clear();
		screenVars.billfreq02.clear();
		screenVars.wdlFreq02.clear();
		screenVars.wdlAmount02.clear();
		screenVars.wdrem02.clear();
		screenVars.ffamt02.clear();
		screenVars.feepc02.clear();
		screenVars.feemin02.clear();
		screenVars.feemax02.clear();
		screenVars.billfreq03.clear();
		screenVars.wdlFreq03.clear();
		screenVars.wdlAmount03.clear();
		screenVars.wdrem03.clear();
		screenVars.ffamt03.clear();
		screenVars.feepc03.clear();
		screenVars.feemin03.clear();
		screenVars.feemax03.clear();
		screenVars.billfreq04.clear();
		screenVars.wdlFreq04.clear();
		screenVars.wdlAmount04.clear();
		screenVars.wdrem04.clear();
		screenVars.ffamt04.clear();
		screenVars.feepc04.clear();
		screenVars.feemin04.clear();
		screenVars.feemax04.clear();
		screenVars.billfreq05.clear();
		screenVars.wdlFreq05.clear();
		screenVars.wdlAmount05.clear();
		screenVars.wdrem05.clear();
		screenVars.ffamt05.clear();
		screenVars.feepc05.clear();
		screenVars.feemin05.clear();
		screenVars.feemax05.clear();
		screenVars.billfreq06.clear();
		screenVars.wdlFreq06.clear();
		screenVars.wdlAmount06.clear();
		screenVars.wdrem06.clear();
		screenVars.ffamt06.clear();
		screenVars.feepc06.clear();
		screenVars.feemin06.clear();
		screenVars.feemax06.clear();
		screenVars.maxwith01.clear();//ILIFE-7956
		screenVars.maxwith02.clear();//ILIFE-7956
		screenVars.maxwith03.clear();//ILIFE-7956
		screenVars.maxwith04.clear();//ILIFE-7956
		screenVars.maxwith05.clear();//ILIFE-7956
		screenVars.maxwith06.clear();//ILIFE-7956
	}
}
