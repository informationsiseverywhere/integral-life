package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:56
 * Description:
 * Copybook name: INCIMJ2KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incimj2key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incimj2FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incimj2Key = new FixedLengthStringData(64).isAPartOf(incimj2FileKey, 0, REDEFINE);
  	public FixedLengthStringData incimj2Chdrcoy = new FixedLengthStringData(1).isAPartOf(incimj2Key, 0);
  	public FixedLengthStringData incimj2Chdrnum = new FixedLengthStringData(8).isAPartOf(incimj2Key, 1);
  	public FixedLengthStringData incimj2Life = new FixedLengthStringData(2).isAPartOf(incimj2Key, 9);
  	public FixedLengthStringData incimj2Coverage = new FixedLengthStringData(2).isAPartOf(incimj2Key, 11);
  	public FixedLengthStringData incimj2Rider = new FixedLengthStringData(2).isAPartOf(incimj2Key, 13);
  	public PackedDecimalData incimj2PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incimj2Key, 15);
  	public PackedDecimalData incimj2InciNum = new PackedDecimalData(3, 0).isAPartOf(incimj2Key, 18);
  	public PackedDecimalData incimj2Seqno = new PackedDecimalData(2, 0).isAPartOf(incimj2Key, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(incimj2Key, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incimj2FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incimj2FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}