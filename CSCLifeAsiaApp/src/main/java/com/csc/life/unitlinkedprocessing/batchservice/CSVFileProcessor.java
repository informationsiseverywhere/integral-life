package com.csc.life.unitlinkedprocessing.batchservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.integral.batch.service.FileProcessor;

public class CSVFileProcessor extends FileProcessor{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CSVFileProcessor.class);
	private static Log log = LogFactory.getLog(CSVFileProcessor.class);
	
	protected String fileFormat = "csv";
	
	protected String delimiter = ",";
	
	private List<String> headers;
	
	private static final String ERROR_COLUMN = "Result";

	
	
	public CSVFileProcessor(File csvfile){
		super(csvfile);
	}
	
	public CSVFileProcessor(String csvfilePath){
		super(csvfilePath);
	}
	
	@Override
	protected boolean validateFileFormat() {
		String format = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".") + 1);
		if(format != null && !format.trim().equalsIgnoreCase("") && format.equalsIgnoreCase(fileFormat)){
			return true;
		}else{
			errorMessage = "Invalid File fomat : " + format;
			return false;
		}
	}
	
	@Override
	public List<Map<String, String>> parseFile(){
		List<Map<String, String>> dataList = null;
		if(validateFile()){
			dataList = new ArrayList<>();
			try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
				String line = null;
				Map<String, String> data = null;
				boolean isNotHeader = false;
				headers = new ArrayList<>();
				while((line = bufferedReader.readLine()) != null){
					line = line.trim();
					if(line.contains("\"")){
						errorMessage = "Header or Values should not contain comma (,) ";
						break;
					}else{
						String[] lineSplitArray = line.split(delimiter);
						if(isNotHeader){
							data = new LinkedHashMap<>();
							for(int index = 0; index < headers.size(); index++){
								if(index < lineSplitArray.length){
									data.put(headers.get(index), lineSplitArray[index].trim());
								}else{
									data.put(headers.get(index), "");
								}
								
							}
							dataList.add(data);
						}else{
							for(String header: lineSplitArray){
								if(header != null && !(header = header.trim()).equalsIgnoreCase("")){
									headers.add(header);
								}else{
									// Empty headers marks end of headers
									break;
								}
							}
							isNotHeader = true;
						}	
					}
				}
			} catch (Exception e) {
				LOGGER.error("CSV File Processing failed", e);
				log.error("CSV File Processing failed", e);
				errorMessage = "Not able to process the file because of " + e.getMessage();
			}
		}
		return dataList;
	}
	
	public List<String> getHeaders(){
		return headers;
	}

	protected File createFile(String path) {
		String filePath=FilenameUtils.getFullPath(path); //IJTI-463
    	File outputFile = FileSystems.getDefault().getPath(filePath + FilenameUtils.getName(path)).toFile();//IJTI-593
    	try {
       	  if(!outputFile.exists()) {
    		 outputFile.createNewFile();
    	  }
    	}catch(IOException e) {
    		LOGGER.error("No output file ",e);
			errorMessage = "No output file: "+e.getMessage();
			return null;
    	}
    	return outputFile;
	}
	@Override
	protected boolean createOutputFile(String outputFilePath, List<Map<String, String>> data) {
		
		File outputFile = createFile(outputFilePath);

		try(FileWriter fileWriter = new FileWriter(outputFile)) {
			String line = "";
			// Writing header line
			for(String header: headers){
				line = line + "," + header; 
			}
			line = line.trim();
			line = (line.startsWith(",")) ? line.substring(1) : line ;
			line = line + "," + ERROR_COLUMN; // Adding last column
			fileWriter.write(line + "\n");
			// Writing data lines
			for(Map<String, String> dataLine : data){
				Iterator<Entry<String, String>> recordValueIterator = dataLine.entrySet().iterator();
				line = "";
				while(recordValueIterator.hasNext()){
					Entry<String, String> recordMap = recordValueIterator.next();
					String value = recordMap.getValue();
					line = line + "," + value;
				}
				line = (line.startsWith(",")) ? line.substring(1) : line ;
				fileWriter.write(line + "\n");
			}
			fileWriter.flush();
		} catch (IOException e) {
			LOGGER.error("Error in writing output file ",e);
			errorMessage = "Error in writing output file: "+e.getMessage();
			return false;
		}
		return true;
	}
	
	@Override
	protected boolean errorRecordPresent(List<Map<String, String>> updatedData){
		for(Map<String,String> value : updatedData){
			String error = value.get("Error");
			if(error != null && !error.equalsIgnoreCase("")){
				return false;
			}
		}
		return true;
	}
	
}
