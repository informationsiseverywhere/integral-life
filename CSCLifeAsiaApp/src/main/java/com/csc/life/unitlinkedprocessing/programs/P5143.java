/*
 * File: P5143.java
 * Date: 30 August 2009 0:13:17
 * Author: Quipoz Limited
 *
 * Class transformed from P5143.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

//ILIFE-8255
import java.util.ArrayList;
import java.util.List;
//ILIFE-8255
import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILIFE-8255
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf; //ILIFE-8255
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
*  P5143 - Unit Switching AT Request Program.
*  ------------------------------------------
*
*  This program will be used to finalise the processing of  the
*  on-line portion of Unit Switching. It has no screen.
*
*  It will perform the following tasks:
*
*       . Update CHDR with the new TRANNO,
*
*       . Transfer the softlocking to AT,
*
*       . Request an AT function which will call the breakout
*         transaction,
*
*       . Call BATCUP to update the CHDR Batch Control Header,
*
*       . Write a transaction record to PTRN,
*
*       . Retain the CHDR record for the AT program.
*
*  Processing.
*  -----------
*
*  Perform a RETRV on CHDRMJA.
*
*  Perform a RLSE on CHDRMJA.
*
*  Perform a READH on CHDRMJA using the returned key.
*
*  Use  the Company and Contract Number from CHDRMJA along with
*  a TRANNO of all 9's to perform a  BEGN  on  USWH,  the  Unit
*  Switch  Header  file.  The  TRANNO  in  the  USWH  key is in
*  descending sequence and so if  there  are  multiple  pending
*  switches  for  differing components then this read will pick
*  up the most recent USWH record,  ie.  that  associated  with
*  this transaction.
*
*  If  end of file or no matching record is found then the user
*  made no switches  during  the  transaction  and  no  further
*  processing  is required except to call SFTLOCK to unlock the
*  Contract Header.
*
*  Similarly if a matching USWH record is found with  a  TRANNO
*  less  than or equal to the TRANNO on the CHDRMJA record then
*  this must be for a previous pending switch and therefore  no
*  switches  were made during the current transaction and so no
*  further processing is required except  to  call  SFTLOCK  to
*  unlock the Contract Header.
*
*  Otherwise continue processing as follows:
*
*  Increment  the  TRANNO  on  the  Contract Header and rewrite
*  CHDRMJA.  Perform  a  KEEPS  on  CHDRMJA   to   retain   the
*  information for the next program.
*
*  Call BATCUP to update the Contract Control Header.
*
*  Write a transaction record to PTRN.
*  If  breakout  is  required  call  SFTLOCK with a function of
*  'TOAT', (Transfer to AT),  to  allocate  possession  of  the
*  contract  to  the  AT transaction. Breakout will be required
*  if the Plan Suffix on any USWD record is less than or  equal
*  to the number of policies summarised from CHDR, (POLSUM).
*   Use the following parameters:
*
*       SFTL-FUNCTION    - 'TOAT'
*       SFTL-COMPANY     - CHDRMJA-COMPANY
*       SFTL-ENTITY      - CHDRMJA-CHDRNUM
*       SFTL-ENTTYP      - 'CH'
*       SFTL-TRANSACTION - WSKY-BATC-BATCTRCDE
*       SFTL-USER        - VRCM-USER
*
*  If  breakout  is  required  call  ATREQ  with  a  request to
*  run  P5143AT  which  will  call  BRKOUT.  Set  the   Primary
*  key   for   AT   as   the   Company,  Contract  Number,  the
*  existing  Contract  Header  Plan  Suffix  and   the   lowest
*  Plan Suffix on any USWD record.
*
*
*****************************************************************
* </pre>
*/
public class P5143 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5143");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaBreakout = "";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);
	private PackedDecimalData wsaaNewSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData filler1 = new FixedLengthStringData(178).isAPartOf(wsaaTransactionRec, 22, FILLER).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* TABLES */
	private static final String t7508 = "T7508";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String ptrnrec = "PTRNREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UswdTableDAM uswdIO = new UswdTableDAM();
	private UswhTableDAM uswhIO = new UswhTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Atreqrec atreqrec = new Atreqrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T7508rec t7508rec = new T7508rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
 //ILIFE-8255 start	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
 //ILIFE-8255 end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		rewrt3030,
		nextUswd3080,
		check3085,
		exit3090
	}

	public P5143() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		/*    No Processing required*/
		/*         except...                                               */
		wsaaBreakout = "N";
		/*EXIT*/
	}

protected void screenEdit2000()
	{
		/*GO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					retrvChdrmja3005();
					readh3015();
					begnUswh3020();
				case rewrt3030:
					rewrt3030();
					keeps3040();
					batcup3050();
					ptrn3060();
					uswdBegn3070();
				case nextUswd3080:
					nextUswd3080();
				case check3085:
					check3085();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void retrvChdrmja3005()
	{
		/*    Retrieve CHDRMJA record*/
 //ILIFE-8255 start	
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		chdrpfDAO.deleteCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*RLSE-CHDRMJA*/
		/*    Release CHDRMJA*/
		/*chdrmjaIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8255 end
	}

protected void readh3015()
	{
		/*    Read & hold CHDRMJA record*/
 //ILIFE-8255 start
		chdrpf = chdrpfDAO.getChdrpf(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
		if(chdrpf == null){
			fatalError600();
		}
		/*chdrmjaIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8255 end
	}

protected void begnUswh3020()
	{
		/*    BEGN on USWH file*/
		uswhIO.setChdrcoy(chdrpf.getChdrcoy());
		uswhIO.setChdrnum(chdrpf.getChdrnum());
		uswhIO.setTranno("99999");
		uswhIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		//uswhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//uswhIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, uswhIO);
		if (isNE(uswhIO.getStatuz(), varcom.oK)
		&& isNE(uswhIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswhIO.getParams());
			fatalError600();
		}
		if (isEQ(uswhIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(uswhIO.getChdrnum(), chdrpf.getChdrnum())
		&& isGT(uswhIO.getTranno(), chdrpf.getTranno())) {
			goTo(GotoLabel.rewrt3030);
		}
		dryProcessing5000();
		rlseSoftlock3200();
		goTo(GotoLabel.exit3090);
	}

protected void rewrt3030()
	{
		/*    Update CHDRMJA header with TRANNO + 1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
 //ILIFE-8255 start
		chdrpf.setCurrto(wsaaToday.toInt());
		chdrpf.setValidflag('2');
		List<Chdrpf> chdrList = new ArrayList<>();
		chdrList.add(chdrpf);
		chdrpfDAO.updateInvalidChdrRecords(chdrList);
		
		/*chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		chdrpf.setValidflag('1');
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		chdrpf.setCurrfrom(wsaaToday.toInt());
		chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfDAO.insertChdrRecords(chdrpf);
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("WRITR");
		 MOVE 'REWRT'                 TO CHDRMJA-FUNCTION.            
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8255 end
	}

protected void keeps3040()
	{
		/*    Perform KEEPS on contract header for next program*/
 //ILIFE-8255 start
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
 //ILIFE-8255 end
	}

protected void batcup3050()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

protected void ptrn3060()
	{
		/*    Writes a transaction record to PTRN file*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrpfx(chdrpf.getChdrpfx()); //ILIFE-8255
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy()); //ILIFE-8255
		ptrnIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8255
		ptrnIO.setTranno(chdrpf.getTranno()); //ILIFE-8255
		ptrnIO.setUser(varcom.vrcmUser);
		/* MOVE TDAY                   TO DTC1-FUNCTION.                */
		/* CALL 'DATCON1'              USING DTC1-DATCON1-REC.          */
		/* MOVE DTC1-INT-DATE          TO WSAA-TODAY.                   */
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void uswdBegn3070()
	{
		begnUswd3300();
		if (isEQ(uswdIO.getStatuz(), varcom.endp)) {
			dryProcessing5000();
			rlseSoftlock3200();
			goTo(GotoLabel.exit3090);
		}
		if (isLTE(chdrpf.getPolsum(), 1)) {
			wsaaBreakout = "N";
			goTo(GotoLabel.check3085);
		}
		if (isGT(uswdIO.getPlanSuffix(), chdrpf.getPolsum())
		|| isEQ(uswdIO.getPlanSuffix(), ZERO)) {
			goTo(GotoLabel.nextUswd3080);
		}
		wsaaBreakout = "Y";
		goTo(GotoLabel.check3085);
	}

protected void nextUswd3080()
	{
		nextUswd3400();
		if (isEQ(uswdIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.check3085);
		}
		if (isGT(uswdIO.getPlanSuffix(), chdrpf.getPolsum())
		|| isEQ(uswdIO.getPlanSuffix(), ZERO)) {
			goTo(GotoLabel.nextUswd3080);
		}
		wsaaBreakout = "Y";
	}

protected void check3085()
	{
		if (isNE(wsaaBreakout, "Y")) {
			dryProcessing5000();
			rlseSoftlock3200();
			return ;
		}
		wsspcomn.flag.set("B");
		sftlckCallAt3100();
	}

protected void sftlckCallAt3100()
	{
		softlock3110();
	}

protected void softlock3110()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum()); //ILIFE-8255
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*  Call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5143AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.company.set(wsspcomn.company);
		atreqrec.reqUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO  ATRT-REQ-TERM.                 */
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum()); //ILIFE-8255
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaPolsum.set(chdrpf.getPolsum()); //ILIFE-8255
		/* MOVE VRCM-TERM            TO WSAA-TERMID.                    */
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaNewSuffix.set(uswdIO.getPlanSuffix());
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void rlseSoftlock3200()
	{
		rlse3210();
	}

	/**
	* <pre>
	*    Release Soft Lock if no call to AT
	* </pre>
	*/
protected void rlse3210()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum()); //ILIFE-8255
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void begnUswd3300()
	{
		begn3310();
	}

protected void begn3310()
	{
		/*    BEGN on USWD file to find if breakout required*/
		uswdIO.setDataKey(SPACES);
		uswdIO.setChdrcoy(chdrpf.getChdrcoy()); //ILIFE-8255
		uswdIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8255
		uswdIO.setTranno(chdrpf.getTranno()); //ILIFE-8255
		uswdIO.setPlanSuffix(ZERO);
		uswdIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		uswhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		uswhIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isNE(uswdIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(uswdIO.getChdrnum(), chdrpf.getChdrnum())) {
			uswdIO.setStatuz(varcom.endp);
		}
	}

protected void nextUswd3400()
	{
		/*READ-NEXT*/
		/*    Continues reading thru USWD for contract no. until plan*/
		/*    suffix < policies summarised or end of file reached*/
		uswdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, uswdIO);
		if (isNE(uswdIO.getStatuz(), varcom.oK)
		&& isNE(uswdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(uswdIO.getParams());
			fatalError600();
		}
		if (isNE(uswdIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(uswdIO.getChdrnum(), chdrpf.getChdrnum())) {
			uswdIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void dryProcessing5000()
	{
		start5010();
	}

protected void start5010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrpf.getCnttype()); //ILIFE-8255
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75085100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75085100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum()); //ILIFE-8255
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
 //ILIFE-8255 start
		drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
		drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
 //ILIFE-8255 end
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75085100()
	{
		start5110();
	}

protected void start5110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
