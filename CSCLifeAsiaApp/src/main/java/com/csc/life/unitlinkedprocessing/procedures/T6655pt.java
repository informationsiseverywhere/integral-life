/*
 * File: T6655pt.java
 * Date: 30 August 2009 2:29:11
 * Author: Quipoz Limited
 * 
 * Class transformed from T6655PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.unitlinkedprocessing.tablestructures.T6655rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6655.
*
*
*****************************************************************
* </pre>
*/
public class T6655pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Surrender Value Penalty - Discount Factors       S6655");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(55);
	private FixedLengthStringData filler9 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine004, 28, FILLER).init("Years premium has been paid");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler11 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine005, 10, FILLER).init("0      1      2     3      4      5      6      7      8      9");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler13 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init("   0+");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler23 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  10+");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(76);
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  20+");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(76);
	private FixedLengthStringData filler43 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  30+");
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(76);
	private FixedLengthStringData filler53 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  40+");
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 8).setPattern("ZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 15).setPattern("ZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 22).setPattern("ZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 29).setPattern("ZZZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 36).setPattern("ZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 50).setPattern("ZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 71).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(21);
	private FixedLengthStringData filler63 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  Divisor:");
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 15).setPattern("ZZZZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6655rec t6655rec = new T6655rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6655pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6655rec.t6655Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6655rec.svpFactor50);
		fieldNo008.set(t6655rec.svpFactor01);
		fieldNo009.set(t6655rec.svpFactor02);
		fieldNo010.set(t6655rec.svpFactor03);
		fieldNo011.set(t6655rec.svpFactor04);
		fieldNo012.set(t6655rec.svpFactor05);
		fieldNo013.set(t6655rec.svpFactor06);
		fieldNo014.set(t6655rec.svpFactor07);
		fieldNo015.set(t6655rec.svpFactor08);
		fieldNo016.set(t6655rec.svpFactor09);
		fieldNo017.set(t6655rec.svpFactor10);
		fieldNo018.set(t6655rec.svpFactor11);
		fieldNo019.set(t6655rec.svpFactor12);
		fieldNo020.set(t6655rec.svpFactor13);
		fieldNo021.set(t6655rec.svpFactor14);
		fieldNo022.set(t6655rec.svpFactor15);
		fieldNo023.set(t6655rec.svpFactor16);
		fieldNo024.set(t6655rec.svpFactor17);
		fieldNo025.set(t6655rec.svpFactor18);
		fieldNo026.set(t6655rec.svpFactor19);
		fieldNo027.set(t6655rec.svpFactor20);
		fieldNo028.set(t6655rec.svpFactor21);
		fieldNo029.set(t6655rec.svpFactor22);
		fieldNo030.set(t6655rec.svpFactor23);
		fieldNo031.set(t6655rec.svpFactor24);
		fieldNo032.set(t6655rec.svpFactor25);
		fieldNo033.set(t6655rec.svpFactor26);
		fieldNo034.set(t6655rec.svpFactor27);
		fieldNo035.set(t6655rec.svpFactor28);
		fieldNo036.set(t6655rec.svpFactor29);
		fieldNo037.set(t6655rec.svpFactor30);
		fieldNo038.set(t6655rec.svpFactor31);
		fieldNo039.set(t6655rec.svpFactor32);
		fieldNo040.set(t6655rec.svpFactor33);
		fieldNo041.set(t6655rec.svpFactor34);
		fieldNo042.set(t6655rec.svpFactor35);
		fieldNo043.set(t6655rec.svpFactor36);
		fieldNo044.set(t6655rec.svpFactor37);
		fieldNo045.set(t6655rec.svpFactor38);
		fieldNo046.set(t6655rec.svpFactor39);
		fieldNo047.set(t6655rec.svpFactor40);
		fieldNo048.set(t6655rec.svpFactor41);
		fieldNo049.set(t6655rec.svpFactor42);
		fieldNo050.set(t6655rec.svpFactor43);
		fieldNo051.set(t6655rec.svpFactor44);
		fieldNo052.set(t6655rec.svpFactor45);
		fieldNo053.set(t6655rec.svpFactor46);
		fieldNo054.set(t6655rec.svpFactor47);
		fieldNo055.set(t6655rec.svpFactor48);
		fieldNo056.set(t6655rec.svpFactor49);
		fieldNo057.set(t6655rec.divisor);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
