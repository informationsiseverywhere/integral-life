package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:25
 * Description:
 * Copybook name: COVRRGWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrrgwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrrgwFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrrgwKey = new FixedLengthStringData(64).isAPartOf(covrrgwFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrrgwChdrcoy = new FixedLengthStringData(1).isAPartOf(covrrgwKey, 0);
  	public FixedLengthStringData covrrgwChdrnum = new FixedLengthStringData(8).isAPartOf(covrrgwKey, 1);
  	public FixedLengthStringData covrrgwLife = new FixedLengthStringData(2).isAPartOf(covrrgwKey, 9);
  	public FixedLengthStringData covrrgwCoverage = new FixedLengthStringData(2).isAPartOf(covrrgwKey, 11);
  	public FixedLengthStringData covrrgwRider = new FixedLengthStringData(2).isAPartOf(covrrgwKey, 13);
  	public PackedDecimalData covrrgwPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrrgwKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrrgwKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrrgwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrrgwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}