/*
 * File: P5412.java
 * Date: 30 August 2009 0:24:24
 * Author: Quipoz Limited
 * 
 * Class transformed from P5412.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupdTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.P5412up;
import com.csc.life.unitlinkedprocessing.recordstructures.P5412rec;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.life.unitlinkedprocessing.screens.S5412ScreenVars;
import com.csc.smart.dataaccess.BatcTableDAM;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This is the submenu program for the Unit Link Price system
*
*
*  The input fields into this submenu are:
*      Date                  (All options)
*      Job Number            (Option A and F)
*      Virtual Fund Code     (Option E only)
*      Partial Virtual Fund  (Option A, B, D and F)
*      To date               (Option E only)
*      Action                (All options)
*
*   The available action codes lead to :
*                 - Create New Bare Prices
*                 - Modify Bare Prices
*                 - Calculate Bid/Offer Prices
*                 - Modify Bid/Offer Prices
*                 - Inquire on Existing Prices
*                 - Direct input of Bid and Offer prices
*                 - Activate fund prices
*
*
*   This program flow is as follows:-
*
*       Initialise and default fields.
*
*       Check Action and Job number.
*       If job number not entered
*           display scroll screen
*       Else
*           call the appropriate routine according to action
*           to validate screen fields until there is no error.
*       There will be no Job Number Scroll for Action A and F.
*
*       Update Linkage Area.
*
*       Set up secondary switching parameters.
*
*
*****************************************************************
* </pre>
*/
public class P5412 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5412");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaTdayDate = new ZonedDecimalData(8, 0);
	private String wsaaScroll = "N";
		/* FORMATS */
	private static final String vprcupdrec = "VPRCUPDREC";
	private BatcTableDAM batcIO = new BatcTableDAM();
	private VprcupdTableDAM vprcupdIO = new VprcupdTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P5412rec p5412rec = new P5412rec();
	private Wsspuprc wsspuprc = new Wsspuprc();
	private S5412ScreenVars sv = ScreenProgram.getScreenVars( S5412ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit3900, 
		call5000, 
		exit5900, 
		call6000, 
		exit6900, 
		call7000, 
		exit7900, 
		call8000, 
		exit8900, 
		call11000, 
		exit11900
	}

	public P5412() {
		super();
		screenVars = sv;
		new ScreenModel("S5412", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1100();
	}

protected void initialise1100()
	{
		wsspcomn.tranno.set(0);
		sv.dataArea.set(SPACES);
		p5412rec.p5412Rec.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* IF WSSP-UPRC-JOBNO IS NUMERIC AND                            */
		/*     WSSP-UPRC-JOBNO NOT = 0                                  */
		/*     MOVE WSSP-UPRC-JOBNO    TO S5412-JOBNO                   */
		/*     MOVE WSSP-UPRC-EFFDATE  TO S5412-DTEEFF                  */
		/* ELSE                                                         */
		/*     MOVE ZERO               TO S5412-JOBNO                   */
		/*     MOVE DTC1-INT-DATE      TO S5412-DTEEFF.                 */
		/* In some situations the WSSP-USER-AREA may contain information   */
		/* from a previous transaction, e.g. contract enquiry, that can    */
		/* be confused with a JOBNO.  In this case the EFFDATE may not     */
		/* contain information even though this program assumed it would   */
		/* which results in a decimal data error.  By checking the EFFDATE */
		/* for NUMERIC this can be avoided.                                */
		if (isEQ(wsspuprc.uprcJobno, NUMERIC)
		&& isNE(wsspuprc.uprcJobno, 0)
		&& isEQ(wsspuprc.uprcEffdate, NUMERIC)) {
			sv.jobno.set(wsspuprc.uprcJobno);
			sv.dteeff.set(wsspuprc.uprcEffdate);
		}
		else {
			sv.jobno.set(ZERO);
			sv.dteeff.set(datcon1rec.intDate);
		}
		sv.todate.set(datcon1rec.intDate);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2100();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5412IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5412-DATA-AREA.                       */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "BACH")) {
			/*GO TO 2100-VALIDATE.                                      */
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2100()
	{
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		wsspcomn.next1prog.set(subprogrec.nxt1prog);
		wsspcomn.next2prog.set(subprogrec.nxt2prog);
		wsspcomn.next3prog.set(subprogrec.nxt3prog);
		wsspcomn.next4prog.set(subprogrec.nxt4prog);
		/* Call SANCTN to validate that the user is authorised to*/
		/* perform the function.*/
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		wsaaScroll = "N";
		if ((isEQ(sv.action, "A"))
		|| (isEQ(sv.action, "F"))) {
			createBareBidPrices5000();
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			return ;
		}
		if (isEQ(sv.action, "E")) {
			inquirePrices9000();
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		/* If job number not entered, display Job Number Scroll*/
		/* Screen(S6220). WSAA-SCROLL is used to control whether to display*/
		/* S6220.*/
		if ((isEQ(sv.jobno, NUMERIC))
		&& (isNE(sv.jobno, 0))) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(sv.dteeff, ZERO)
			|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
				sv.dteeffErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*            GO TO 2900-EXIT                                      */
				return ;
			}
			else {
				wsaaScroll = "Y";
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*            GO TO 2900-EXIT.                                     */
				return ;
			}
		}
		if (isEQ(sv.action, "B")) {
			modifyBarePrices6000();
		}
		if (isEQ(sv.action, "C")) {
			calculateBidOffer7000();
		}
		if (isEQ(sv.action, "D")) {
			modifyBidOffer8000();
		}
		if (isEQ(sv.action, "G")) {
			activatePrices11000();
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			loadWsspFields3100();
			loadRrec3200();
			callP5412up3500();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadWsspFields3100()
	{
		wsspuprc.uprcEffdate.set(sv.dteeff);
		wsspcomn.currfrom.set(sv.dteeff);
		wsspcomn.currto.set(sv.todate);
		wsspuprc.uprcJobno.set(sv.jobno);
		if (isEQ(sv.unitVirtualFund, SPACES)) {
			wsspuprc.uprcVrtfnd.set(sv.unitPartFund);
		}
		else {
			wsspuprc.uprcVrtfnd.set(sv.unitVirtualFund);
		}
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey.batcKey);
		wsspcomn.submenu.set(wsaaProg);
		wsspcomn.flag.set(sv.action);
		if (isEQ(wsaaScroll, "Y")) {
			goTo(GotoLabel.exit3900);
		}
	}

protected void loadRrec3200()
	{
		p5412rec.progname.set(wsaaProg);
		p5412rec.dteeff.set(sv.dteeff);
		p5412rec.jobno.set(sv.jobno);
		p5412rec.vrtfnd.set(sv.unitVirtualFund);
		p5412rec.partfund.set(sv.unitPartFund);
		p5412rec.todate.set(sv.todate);
		p5412rec.tranid.set(wsspcomn.tranid);
		p5412rec.batchkey.set(wsspcomn.batchkey);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(subprogrec.bchrqd, "Y")) {
			goTo(GotoLabel.exit3900);
		}
	}

protected void callP5412up3500()
	{
		callProgram(P5412up.class, p5412rec.p5412Rec);
		if (isEQ(p5412rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		if (isNE(p5412rec.statuz, varcom.oK)) {
			sv.actionErr.set(p5412rec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsspcomn.batchkey.set(p5412rec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.secProg[1].set(wsspcomn.next1prog);
		wsspcomn.secProg[2].set(wsspcomn.next2prog);
		wsspcomn.secProg[3].set(wsspcomn.next3prog);
		wsspcomn.secProg[4].set(wsspcomn.next4prog);
		wsspcomn.programPtr.set(1);
		/* If they enter job number, do not go to scroll program./*/
		if (isEQ(sv.jobno, NUMERIC)
		&& isNE(sv.jobno, 0)
		&& isNE(sv.action, "A")
		&& isNE(sv.action, "E")
		&& isNE(sv.action, "F")) {
			wsspcomn.programPtr.set(2);
		}
		/*EXIT*/
	}

protected void createBareBidPrices5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5000();
				case call5000: 
					call5000();
				case exit5900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5000()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		//performance improvement -- < Niharika Modi >
		vprcupdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcupdIO.setFitKeysSearch("EFFDATE", "JOBNO");
		
		vprcupdIO.setFormat(vprcupdrec);
		/* Effective Date must be entered.*/
		if (isEQ(sv.dteeff, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* When get to this point(Scroll screen not required), Job Number*/
		/* must be entered.*/
		if (isEQ(sv.jobno, ZERO)) {
			sv.jobnoErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* Virtual Fund must not be entered.*/
		if (isNE(sv.unitVirtualFund, SPACES)) {
			sv.vrtfndErr.set(errorsInner.e374);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.exit5900);
		}
		/* The combination of the key must not exist.*/
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(sv.dteeff);
		vprcupdIO.setJobno(sv.jobno);
	}

protected void call5000()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(), varcom.oK)) {
			if (isNE(vprcupdIO.getCompany(), wsspcomn.company)) {
				return ;
			}
		}
		if (isEQ(vprcupdIO.getStatuz(), varcom.oK)
		&& isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)) {
			sv.dteeffErr.set(errorsInner.g089);
			sv.jobnoErr.set(errorsInner.g089);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(vprcupdIO.getStatuz(), varcom.endp)) {
			return ;
		}
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call5000);
	}

protected void modifyBarePrices6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6000();
				case call6000: 
					call6000();
				case exit6900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6000()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		/* Effective Date must be entered.*/
		if (isEQ(sv.dteeff, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* When get to this point(Scroll screen not required), Job Number*/
		/* must be entered.*/
		if (isEQ(sv.jobno, ZERO)) {
			sv.jobnoErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* Virtual Fund must not be entered.*/
		if (isNE(sv.unitVirtualFund, SPACES)) {
			sv.vrtfndErr.set(errorsInner.e492);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.exit6900);
		}
		/* The combination of the key must exist.*/
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(sv.dteeff);
		vprcupdIO.setJobno(sv.jobno);
	}

protected void call6000()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(), varcom.endp)) {
			sv.dteeffErr.set(errorsInner.g090);
			sv.jobnoErr.set(errorsInner.g090);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getCompany(), wsspcomn.company)) {
			sv.dteeffErr.set(errorsInner.g090);
			sv.jobnoErr.set(errorsInner.g090);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)
		&& isEQ(vprcupdIO.getProcflag(), "Y")) {
			sv.dteeffErr.set(errorsInner.g091);
			sv.jobnoErr.set(errorsInner.g091);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)
		&& isNE(vprcupdIO.getProcflag(), "Y")) {
			return ;
		}
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call6000);
	}

protected void calculateBidOffer7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para7000();
				case call7000: 
					call7000();
				case exit7900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para7000()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		/* Effective Date must be entered.*/
		if (isEQ(sv.dteeff, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* When get to this point(Scroll screen not required), Job Number*/
		/* must be entered.*/
		if (isEQ(sv.jobno, ZERO)) {
			sv.jobnoErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* Virtual Fund must not be entered.*/
		if (isNE(sv.unitVirtualFund, SPACES)) {
			sv.vrtfndErr.set(errorsInner.e492);
			wsspcomn.edterror.set("Y");
		}
		/* Partial Virtual Fund must not be entered.*/
		if (isNE(sv.unitPartFund, SPACES)) {
			sv.partfundErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.exit7900);
		}
		/* The combination of the key must exist.*/
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(sv.dteeff);
		vprcupdIO.setUnitVirtualFund(sv.unitVirtualFund);
		vprcupdIO.setJobno(sv.jobno);
	}

protected void call7000()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(), varcom.endp)) {
			sv.dteeffErr.set(errorsInner.g090);
			sv.jobnoErr.set(errorsInner.g090);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getCompany(), wsspcomn.company)) {
			sv.dteeffErr.set(errorsInner.g090);
			sv.jobnoErr.set(errorsInner.g090);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(vprcupdIO.getStatuz(), varcom.oK)
		&& isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)
		&& isEQ(vprcupdIO.getProcflag(), "Y")) {
			sv.dteeffErr.set(errorsInner.g091);
			sv.jobnoErr.set(errorsInner.g091);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(vprcupdIO.getStatuz(), varcom.oK)
		&& isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)) {
			return ;
		}
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call7000);
	}

protected void modifyBidOffer8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para8000();
				case call8000: 
					call8000();
				case exit8900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para8000()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		/* Effective Date must be entered.*/
		if (isEQ(sv.dteeff, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* When get to this point(Scroll screen not required), Job Number*/
		/* must be entered.*/
		if (isEQ(sv.jobno, ZERO)) {
			sv.jobnoErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* Virtual Fund must not be entered.*/
		if (isNE(sv.unitVirtualFund, SPACES)) {
			sv.vrtfndErr.set(errorsInner.e492);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.exit8900);
		}
		/* The combination of the key must exist.*/
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(sv.dteeff);
		vprcupdIO.setJobno(sv.jobno);
	}

protected void call8000()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(), varcom.endp)) {
			sv.dteeffErr.set(errorsInner.g092);
			sv.jobnoErr.set(errorsInner.g092);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getCompany(), wsspcomn.company)) {
			sv.dteeffErr.set(errorsInner.g092);
			sv.jobnoErr.set(errorsInner.g092);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)
		&& isEQ(vprcupdIO.getProcflag(), "Y")
		&& isNE(vprcupdIO.getValidflag(), vflagcpy.inForceVal)) {
			return ;
		}
		if (isEQ(vprcupdIO.getEffdate(), sv.dteeff)
		&& isEQ(vprcupdIO.getJobno(), sv.jobno)
		&& isEQ(vprcupdIO.getProcflag(), "Y")
		&& isEQ(vprcupdIO.getValidflag(), vflagcpy.inForceVal)) {
			sv.dteeffErr.set(errorsInner.g216);
			sv.jobnoErr.set(errorsInner.g216);
			wsspcomn.edterror.set("Y");
			return ;
		}
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call8000);
	}

protected void inquirePrices9000()
	{
		para9000();
	}

protected void para9000()
	{
		/* Effective Date must be entered.*/
		if (isEQ(sv.dteeff, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* To Date must be entered.*/
		if (isEQ(sv.todate, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* Job number must not be entered.*/
		if (isNE(sv.jobno, ZERO)) {
			sv.jobnoErr.set(errorsInner.e374);
			wsspcomn.edterror.set("Y");
		}
		/* Virtual Fund must not be entered.*/
		if (isNE(sv.unitPartFund, SPACES)) {
			sv.partfundErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			return ;
		}
	}

protected void activatePrices11000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para11000();
				case call11000: 
					call11000();
				case exit11900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para11000()
	{
		vprcupdIO.setParams(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		vprcupdIO.setFormat(vprcupdrec);
		/* Effective Datwe must entered.*/
		if (isEQ(sv.dteeff, ZERO)
		|| isEQ(sv.dteeff, varcom.vrcmMaxDate)) {
			sv.dteeffErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* When get to this point(Scroll screen not required), Job Number*/
		/* must be entered.*/
		if (isEQ(sv.jobno, ZERO)) {
			sv.jobnoErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		/* Virtual Fund must not be entered.*/
		if (isNE(sv.unitVirtualFund, SPACES)) {
			sv.vrtfndErr.set(errorsInner.e492);
			wsspcomn.edterror.set("Y");
		}
		/* Partial Virtual Fund must not be entered.*/
		if (isNE(sv.unitPartFund, SPACES)) {
			sv.partfundErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.exit11900);
		}
		/* The combination of the key must exist.*/
		vprcupdIO.setEffdate(sv.dteeff);
		vprcupdIO.setJobno(sv.jobno);
		vprcupdIO.setCompany(wsspcomn.company);
	}

protected void call11000()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(), varcom.endp)) {
			sv.dteeffErr.set(errorsInner.g094);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if (isNE(vprcupdIO.getCompany(), wsspcomn.company)) {
			sv.dteeffErr.set(errorsInner.g094);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(sv.jobno, vprcupdIO.getJobno())
		&& isEQ(sv.dteeff, vprcupdIO.getEffdate())
		&& isNE(vprcupdIO.getProcflag(), "Y")) {
			sv.dteeffErr.set(errorsInner.g092);
			sv.jobnoErr.set(errorsInner.g092);
			return ;
		}
		if (isEQ(sv.jobno, vprcupdIO.getJobno())
		&& isEQ(sv.dteeff, vprcupdIO.getEffdate())
		&& isNE(vprcupdIO.getValidflag(), vflagcpy.propVal)) {
			sv.dteeffErr.set(errorsInner.g216);
			sv.jobnoErr.set(errorsInner.g216);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(sv.jobno, ZERO)
		&& isEQ(sv.jobno, vprcupdIO.getJobno())
		&& isEQ(sv.dteeff, vprcupdIO.getEffdate())) {
			return ;
		}
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call11000);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
	private FixedLengthStringData e374 = new FixedLengthStringData(4).init("E374");
	private FixedLengthStringData g089 = new FixedLengthStringData(4).init("G089");
	private FixedLengthStringData g090 = new FixedLengthStringData(4).init("G090");
	private FixedLengthStringData g091 = new FixedLengthStringData(4).init("G091");
	private FixedLengthStringData g092 = new FixedLengthStringData(4).init("G092");
	private FixedLengthStringData g094 = new FixedLengthStringData(4).init("G094");
	private FixedLengthStringData g216 = new FixedLengthStringData(4).init("G216");
}
}
