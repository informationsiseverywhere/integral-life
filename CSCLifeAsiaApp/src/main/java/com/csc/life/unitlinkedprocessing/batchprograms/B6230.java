/*
 * File: B6230.java
 * Date: 29 August 2009 21:20:51
 * Author: Quipoz Limited
 * 
 * Class transformed from B6230.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

//ILIFE-1213
import com.csc.smart400framework.dataaccess.model.Vprcpf;

import java.util.Iterator;

import com.csc.life.unitlinkedprocessing.dataaccess.VprcpfTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.P5430par;
import com.csc.life.unitlinkedprocessing.reports.R6230Report;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*AUTHOR.         PAXUS(UK).
*
*REMARKS.
*
* B6230 - U.L. Fund Prices Report.
*
*
* </pre>
*/
public class B6230 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R6230Report printerFile = new R6230Report();
	private VprcpfTableDAM vprcpf = new VprcpfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
		/*    COPY DD-ALL-FORMATS-O OF R6230.*/
	//ILIFE-1213
	//private VprcpfTableDAM vprcpfRec = new VprcpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6230");
	private final int wsaaPageSize = 58;
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaSysdate = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaOption = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaFundFound = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSavedUnitFund = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRecord = new Validator(wsaaFirstRecord, "Y");

	private FixedLengthStringData wsaaUnitDataMoved = new FixedLengthStringData(1);
	private Validator unitDataMoved = new Validator(wsaaUnitDataMoved, "Y");
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private String endOfFile = "N";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaFundTable = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaFundEle = FLSArrayPartOfStructure(100, 4, wsaaFundTable, 0);
	private FixedLengthStringData[] wsaaFund = FLSDArrayPartOfArrayStructure(4, wsaaFundEle, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSearch = new ZonedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData r6230h01Record = new FixedLengthStringData(42);
	private FixedLengthStringData r6230h01O = new FixedLengthStringData(42).isAPartOf(r6230h01Record, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(r6230h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r6230h01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r6230h01O, 11);
	private FixedLengthStringData action = new FixedLengthStringData(1).isAPartOf(r6230h01O, 41);

	private FixedLengthStringData r6230t01Record = new FixedLengthStringData(2);
	private Itemkey wsaaItemkey = new Itemkey();
	private Varcom varcom = new Varcom();
	private Getdescrec getdescrec = new Getdescrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private P5430par p5430par = new P5430par();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private R6230d01RecordInner r6230d01RecordInner = new R6230d01RecordInner();
	//ILIFE-1213 starts
	private Iterator<Vprcpf> vprcpfIterator;
	private Vprcpf vprcpfEntity;
	//ILIFE-1213 ends	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1200, 
		exit1900, 
		exit2900
	}

	public B6230() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		//ILIFE
		vprcpfIterator = (Iterator<Vprcpf>) parmArray[1];
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		printerFile.openOutput();
		
		//ILIFE-1213 starts
		//vprcpf.openInput();
		initialise200();
		while ( !(isEQ(endOfFile, "Y"))) {
			mainProcessing1000();
		}
		//ILIFE-1213 ends
		
		/*OUT*/
		printerFile.close();
		vprcpf.close();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		p5430par.parmRecord.set(conjobrec.params);
		wsaaFundTable.set(SPACES);
		wsaaSavedUnitFund.set(SPACES);
		wsaaLineCount.set(99);
		indicArea.set("0");
		wsaaIndex.set(0);
		wsaaSearch.set(0);
		endOfFile = "N";
		wsaaUnitDataMoved.set("N");
		wsaaFirstRecord.set("Y");
		/* Option 2 and 5 needs special attention in this program.*/
		/* So identify before proceeding further.*/
		/* All the selections are processed in C6230, by using OPNQRYF.    */
		/* Validation has been added to the parameter prompt program P5430 */
		/* to prevent a Job Number and AS AT indicator of 'N' being        */
		/* entered                                                         */
		/*    IF (P5430-VIRTUAL-FUND = SPACE) AND                          */
		if ((isEQ(p5430par.unitVirtualFund, SPACES))
		&& (isEQ(p5430par.asAtDate, "N"))
		&& (isEQ(p5430par.jobno, 0))) {
			wsaaOption.set("1");
		}
		/*    IF (P5430-VIRTUAL-FUND = SPACE) AND                          */
		if ((isEQ(p5430par.unitVirtualFund, SPACES))
		&& (isEQ(p5430par.asAtDate, "Y"))
		&& (isEQ(p5430par.jobno, 0))) {
			wsaaOption.set("2");
		}
		/*    IF (P5430-VIRTUAL-FUND = SPACE) AND                          */
		if ((isEQ(p5430par.unitVirtualFund, SPACES))
		&& (isEQ(p5430par.asAtDate, "Y"))
		&& (isNE(p5430par.jobno, 0))) {
			wsaaOption.set("3");
		}
		/*    IF (P5430-VIRTUAL-FUND NOT = SPACE) AND                      */
		if ((isNE(p5430par.unitVirtualFund, SPACES))
		&& (isEQ(p5430par.asAtDate, "N"))
		&& (isEQ(p5430par.jobno, 0))) {
			wsaaOption.set("4");
		}
		/*    IF (P5430-VIRTUAL-FUND NOT = SPACE) AND                      */
		if ((isNE(p5430par.unitVirtualFund, SPACES))
		&& (isEQ(p5430par.asAtDate, "Y"))
		&& (isEQ(p5430par.jobno, 0))) {
			wsaaOption.set("5");
		}
		/* Check if the user has asked to print bare prices.*/
		if (isEQ(p5430par.printBarePrice, "Y")) {
			indOn.setTrue(10);
		}
		else {
			indOff.setTrue(10);
		}
		/* Convert Effective date.*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(p5430par.dteeff);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSysdate.set(datcon1rec.extDate);
		/* Get company name.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set("0");
		wsaaItemkey.itemItemtabl.set(t1693);
		wsaaItemkey.itemItemitem.set(runparmrec.company);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(runparmrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			wsaaCompanynm.set(SPACES);
		}
		else {
			wsaaCompanynm.set(getdescrec.longdesc);
		}
		r6230d01RecordInner.ibrepr.set(ZERO);
		r6230d01RecordInner.jobno.set(ZERO);
		r6230d01RecordInner.ibidpr.set(ZERO);
		r6230d01RecordInner.ioffpr.set(ZERO);
		r6230d01RecordInner.abrepr.set(ZERO);
		r6230d01RecordInner.abidpr.set(ZERO);
		r6230d01RecordInner.aoffpr.set(ZERO);
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					process1100();
				case next1200: 
					next1200();
				case exit1900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void process1100()
	{
		//ILIFE-1213 starts
		/*
		vprcpf.read(vprcpfRec);
		if (vprcpf.isAtEnd()) {
			endOfFile = "Y";
		}
		*/

		if (vprcpfIterator.hasNext()) {
			vprcpfEntity = vprcpfIterator.next();
		} else {
			endOfFile = "Y";
		}		
		//ILIFE-1213 ends
		
		
		/* If end of file reached before last record printed then print    */
		/* it now                                                          */
		if (unitDataMoved.isTrue()
		&& isEQ(endOfFile, "Y")) {
			pageOverflow2000();
			printDetails6000();
			if (!firstRecord.isTrue()) {
				printerFile.printR6230t01(r6230t01Record, indicArea);
			}
			goTo(GotoLabel.exit1900);
		}
		if (isEQ(endOfFile, "Y")) {
			if (!firstRecord.isTrue()) {
				printerFile.printR6230t01(r6230t01Record, indicArea);
			}
			goTo(GotoLabel.exit1900);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/* IF WSAA-OPTION = '2'                                         */
		if (isNE(wsaaOption, "2")) {
			goTo(GotoLabel.next1200);
		}
		/* Special attention for option 2.*/
		/* Only print the most recent prices of each fund.*/
		/* The selected records are in the order of Effective Date/*/
		/* Job Number/Fund. So the most recent fund prices will be at the*/
		/* beginning of the file. Any subsequent prices of the same fund*/
		/* that has been printed before would not be required to be*/
		/* printed again. So to avoid reprinting the same fund, we store*/
		/* the name of each printed fund in a table such that cross*/
		/* checking can be performed before printing each record. If the*/
		/* fund is already in the table, we ignore that record and read*/
		/* the next one and only print the record if the fund is not*/
		/* found in the table.*/
		/* Note that for each fund there will be two corresponding records;*/
		/* Initial and Accumulated.                                        */
		/* Initial and Accumulated. WSAA-REC-IN is used to distinguish     */
		/* how many records has been read for a particular fund.           */
		/* Duplicate funds will not get skipped if the code below remains. */
		/* IF WSAA-REC-IN NOT = 0                                       */
		/*     GO TO 1200-NEXT.                                         */
		wsaaFundFound.set("N");
		/* Search table.*/
		if (isNE(wsaaIndex, 0)) {
			wsaaSearch.set(1);
			while ( !(isGT(wsaaSearch, wsaaIndex))) {
				searchFundTable7000();
			}
			
		}
		/* Ignore the record and read the next one if the fund is not found*/
		if (isEQ(wsaaFundFound, "Y")) {
			goTo(GotoLabel.exit1900);
		}
	}

protected void next1200()
	{
		/* First time through save unit fund for later processing          */
		if (firstRecord.isTrue()) {
			//ILIFE-1213 starts
			//wsaaSavedUnitFund.set(vprcpfRec.unitVirtualFund);
			wsaaSavedUnitFund.set(vprcpfEntity.getVrtfnd());
			//ILIFE-1213 ends
			wsaaFirstRecord.set("N");
		}
		/* If unit fund has both an initial and accumulation price for the */
		/* same day save this data before printing the record              */
		/* Only print the first record for option 5.                       */
		/* The records are in the same order as those for option 2,        */
		/* so the first one would be the most recent one.                  */
		//ILIFE-1213
		//if (isEQ(vprcpfRec.unitVirtualFund, wsaaSavedUnitFund)) {
		if (isEQ(vprcpfEntity.getVrtfnd(), wsaaSavedUnitFund)) {
			//ILIFE-1213
			//if (isEQ(vprcpfRec.unitType, "I")) {
			if (isEQ(vprcpfEntity.getUltype(), "I")) {
				moveInitUnit4000();
			}
			else {
				moveAccumUnit5000();
			}
			getFundDescription3000();
			wsaaUnitDataMoved.set("Y");
			if (isEQ(wsaaOption, "5")) {
				endOfFile = "Y";
				pageOverflow2000();
				printDetails6000();
			}
			if (isEQ(wsaaOption, "4")) {
				pageOverflow2000();
				printDetails6000();
			}			
			return ;
		}
		/* On fund change print previous unit fund first if one has        */
		/* already been saved otherwise continue and print current         */
		/* unit fund                                                       */
		//ILIFE-1213
		//if (isNE(vprcpfRec.unitVirtualFund, wsaaSavedUnitFund)
		if (isNE(vprcpfEntity.getVrtfnd(), wsaaSavedUnitFund)
		//ILIFE-1213 ends		
		&& unitDataMoved.isTrue()) {
			if (isEQ(wsaaOption, "2")) {
				loadFundTable7500();
			}
			pageOverflow2000();
			printDetails6000();
			//ILIFE-1213
			//wsaaSavedUnitFund.set(vprcpfRec.unitVirtualFund);
			wsaaSavedUnitFund.set(vprcpfEntity.getVrtfnd());
			wsaaUnitDataMoved.set("N");
		}
		
		//ILIFE-1213
		//if (isEQ(vprcpfRec.unitType, "I")) {
		if (isEQ(vprcpfEntity.getUltype(), "I")) {
			moveInitUnit4000();
		}
		if (isEQ(vprcpfEntity.getUltype(), "A")) {
			moveAccumUnit5000();
		}
		
		/* ADD 1                       TO WSAA-REC-IN.                  */
		/* IF WSAA-REC-IN < 2                                           */
		/*     GO TO 1900-EXIT.                                         */
		if (isEQ(wsaaOption, "2")) {
			loadFundTable7500();
		}
		getFundDescription3000();
		pageOverflow2000();
		printDetails6000();
	}

protected void pageOverflow2000()
	{
		try {
			para2100();
			printR6230d012200();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para2100()
	{
		if (isLT(wsaaLineCount, wsaaPageSize)) {
			goTo(GotoLabel.exit2900);
		}
		printerRec.set(SPACES);
		repdate.set(wsaaSysdate);
		action.set(wsaaOption);
		company.set(runparmrec.company);
		companynm.set(wsaaCompanynm);
	}

protected void printR6230d012200()
	{
		printerFile.printR6230h01(r6230h01Record, indicArea);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		wsaaLineCount.set(12);
	}

protected void getFundDescription3000()
	{
		para3100();
	}

protected void para3100()
	{
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t5515);
		//ILIFE-1213
		//wsaaItemkey.itemItemitem.set(vprcpfRec.unitVirtualFund);
		wsaaItemkey.itemItemitem.set(vprcpfEntity.getVrtfnd());
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(runparmrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			r6230d01RecordInner.longdesc.set(SPACES);
			r6230d01RecordInner.r6230d01O.set(SPACES);
		}
		else {
			r6230d01RecordInner.longdesc.set(getdescrec.longdesc);
		}
		//ILIFE-1213 starts
		/*
		r6230d01RecordInner.vrtfnd.set(vprcpfRec.unitVirtualFund);
		r6230d01RecordInner.expiry.set(vprcpfRec.updateDate);
		*/		
		r6230d01RecordInner.vrtfnd.set(vprcpfEntity.getVrtfnd());
		r6230d01RecordInner.expiry.set(vprcpfEntity.getUpddte());		
		//ILIFE-1213 ends
	}

protected void moveInitUnit4000()
	{
		/*PARA*/
		//ILIFE-1213
		//datcon1rec.intDate.set(vprcpfRec.effdate);
		datcon1rec.intDate.set(vprcpfEntity.getEffdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r6230d01RecordInner.effdate.set(datcon1rec.extDate);
		if (isEQ(p5430par.printBarePrice, "Y")) {
			//ILIFE-1213
			//r6230d01RecordInner.ibrepr.set(vprcpfRec.unitBarePrice);
			r6230d01RecordInner.ibrepr.set(vprcpfEntity.getUbrepr());
		}
		else {
			r6230d01RecordInner.ibrepr.set(0);
		}
		//ILIFE-1213 starts
		/*
		r6230d01RecordInner.jobno.set(vprcpfRec.jobno);
		r6230d01RecordInner.ibidpr.set(vprcpfRec.unitBidPrice);
		r6230d01RecordInner.ioffpr.set(vprcpfRec.unitOfferPrice);
		*/
		r6230d01RecordInner.jobno.set(vprcpfEntity.getJobno());
		r6230d01RecordInner.ibidpr.set(vprcpfEntity.getUbidpr());
		r6230d01RecordInner.ioffpr.set(vprcpfEntity.getUoffpr());
		//ILIFE-1213 ends
		/*EXIT*/
	}

protected void moveAccumUnit5000()
	{
		/*PARA*/
	//ILIFE-1213
	//datcon1rec.intDate.set(vprcpfRec.effdate);
	datcon1rec.intDate.set(vprcpfEntity.getEffdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r6230d01RecordInner.effdate.set(datcon1rec.extDate);
		//ILIFE-1213
		//r6230d01RecordInner.jobno.set(vprcpfRec.jobno);
		r6230d01RecordInner.jobno.set(vprcpfEntity.getJobno());
		
		if (isEQ(p5430par.printBarePrice, "Y")) {
			//ILIFE-1213
			//r6230d01RecordInner.abrepr.set(vprcpfRec.unitBarePrice);
			r6230d01RecordInner.abrepr.set(vprcpfEntity.getUbrepr());
		}
		else {
			r6230d01RecordInner.abrepr.set(0);
		}
		//ILIFE-1213 starts
		/*
		r6230d01RecordInner.abidpr.set(vprcpfRec.unitBidPrice);
		r6230d01RecordInner.aoffpr.set(vprcpfRec.unitOfferPrice);
		*/
		//ILIFE-2747 starts
		r6230d01RecordInner.abidpr.set(vprcpfEntity.getUbidpr());
		r6230d01RecordInner.aoffpr.set(vprcpfEntity.getUoffpr());
		//ILIFE-2747 ends
		//ILIFE-1213 ends
		/*EXIT*/
	}

protected void printDetails6000()
	{
		/*PARA*/		
		printerRec.set(SPACES);
		printerFile.printR6230d01(r6230d01RecordInner.r6230d01Record, indicArea);
		wsaaLineCount.add(1);
		initialize(r6230d01RecordInner.r6230d01Record);
		/*EXIT*/
	}

protected void searchFundTable7000()
	{
		/*PARA*/
		//ILIFE-1213
		//if (isEQ(wsaaFund[wsaaSearch.toInt()], vprcpfRec.unitVirtualFund)) {
		if (isEQ(wsaaFund[wsaaSearch.toInt()], vprcpfEntity.getVrtfnd())) {
			wsaaFundFound.set("Y");
		}
		wsaaSearch.add(1);
		/*EXIT*/
	}

protected void loadFundTable7500()
	{
		/*PARA*/
		wsaaIndex.add(1);
		/*  MOVE VRTFND OF VPRCPF-REC  TO WSAA-FUND(WSAA-INDEX).        */
		/* Use previous Unit Fund if data moved already and not printed    */
		if (unitDataMoved.isTrue()) {
			wsaaFund[wsaaIndex.toInt()].set(wsaaSavedUnitFund);
		}
		else {
			//ILIFE-1213	
			//wsaaFund[wsaaIndex.toInt()].set(vprcpfRec.unitVirtualFund);
			wsaaFund[wsaaIndex.toInt()].set(vprcpfEntity.getVrtfnd());
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure R6230D01-RECORD--INNER
 */
private static final class R6230d01RecordInner { 

	private FixedLengthStringData r6230d01Record = new FixedLengthStringData(114);
	private FixedLengthStringData r6230d01O = new FixedLengthStringData(114).isAPartOf(r6230d01Record, 0);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r6230d01O, 0);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r6230d01O, 4);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r6230d01O, 34);
	private ZonedDecimalData jobno = new ZonedDecimalData(8, 0).isAPartOf(r6230d01O, 44);
	private ZonedDecimalData ibrepr = new ZonedDecimalData(9, 5).isAPartOf(r6230d01O, 52);
	private ZonedDecimalData ibidpr = new ZonedDecimalData(9, 5).isAPartOf(r6230d01O, 61);
	private ZonedDecimalData ioffpr = new ZonedDecimalData(9, 5).isAPartOf(r6230d01O, 70);
	private ZonedDecimalData abrepr = new ZonedDecimalData(9, 5).isAPartOf(r6230d01O, 79);
	private ZonedDecimalData abidpr = new ZonedDecimalData(9, 5).isAPartOf(r6230d01O, 88);
	private ZonedDecimalData aoffpr = new ZonedDecimalData(9, 5).isAPartOf(r6230d01O, 97);
	private FixedLengthStringData expiry = new FixedLengthStringData(8).isAPartOf(r6230d01O, 106);
}
}
