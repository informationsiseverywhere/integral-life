package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:49
 * Description:
 * Copybook name: T6646REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6646rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6646Rec = new FixedLengthStringData(330);
  		/*      03  T6646-DFACTS                   .
  		                                           OCCURS 99 .
  		      03  FILLER REDEFINES T6646-DFACTS                   .*/
  	public FixedLengthStringData dfacts = new FixedLengthStringData(297).isAPartOf(t6646Rec, 0);
  	public PackedDecimalData[] dfact = PDArrayPartOfStructure(99, 5, 0, dfacts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(297).isAPartOf(dfacts, 0, FILLER_REDEFINE);
  	public PackedDecimalData dfact01 = new PackedDecimalData(5, 0).isAPartOf(filler, 0);
  	public PackedDecimalData dfact02 = new PackedDecimalData(5, 0).isAPartOf(filler, 3);
  	public PackedDecimalData dfact03 = new PackedDecimalData(5, 0).isAPartOf(filler, 6);
  	public PackedDecimalData dfact04 = new PackedDecimalData(5, 0).isAPartOf(filler, 9);
  	public PackedDecimalData dfact05 = new PackedDecimalData(5, 0).isAPartOf(filler, 12);
  	public PackedDecimalData dfact06 = new PackedDecimalData(5, 0).isAPartOf(filler, 15);
  	public PackedDecimalData dfact07 = new PackedDecimalData(5, 0).isAPartOf(filler, 18);
  	public PackedDecimalData dfact08 = new PackedDecimalData(5, 0).isAPartOf(filler, 21);
  	public PackedDecimalData dfact09 = new PackedDecimalData(5, 0).isAPartOf(filler, 24);
  	public PackedDecimalData dfact10 = new PackedDecimalData(5, 0).isAPartOf(filler, 27);
  	public PackedDecimalData dfact11 = new PackedDecimalData(5, 0).isAPartOf(filler, 30);
  	public PackedDecimalData dfact12 = new PackedDecimalData(5, 0).isAPartOf(filler, 33);
  	public PackedDecimalData dfact13 = new PackedDecimalData(5, 0).isAPartOf(filler, 36);
  	public PackedDecimalData dfact14 = new PackedDecimalData(5, 0).isAPartOf(filler, 39);
  	public PackedDecimalData dfact15 = new PackedDecimalData(5, 0).isAPartOf(filler, 42);
  	public PackedDecimalData dfact16 = new PackedDecimalData(5, 0).isAPartOf(filler, 45);
  	public PackedDecimalData dfact17 = new PackedDecimalData(5, 0).isAPartOf(filler, 48);
  	public PackedDecimalData dfact18 = new PackedDecimalData(5, 0).isAPartOf(filler, 51);
  	public PackedDecimalData dfact19 = new PackedDecimalData(5, 0).isAPartOf(filler, 54);
  	public PackedDecimalData dfact20 = new PackedDecimalData(5, 0).isAPartOf(filler, 57);
  	public PackedDecimalData dfact21 = new PackedDecimalData(5, 0).isAPartOf(filler, 60);
  	public PackedDecimalData dfact22 = new PackedDecimalData(5, 0).isAPartOf(filler, 63);
  	public PackedDecimalData dfact23 = new PackedDecimalData(5, 0).isAPartOf(filler, 66);
  	public PackedDecimalData dfact24 = new PackedDecimalData(5, 0).isAPartOf(filler, 69);
  	public PackedDecimalData dfact25 = new PackedDecimalData(5, 0).isAPartOf(filler, 72);
  	public PackedDecimalData dfact26 = new PackedDecimalData(5, 0).isAPartOf(filler, 75);
  	public PackedDecimalData dfact27 = new PackedDecimalData(5, 0).isAPartOf(filler, 78);
  	public PackedDecimalData dfact28 = new PackedDecimalData(5, 0).isAPartOf(filler, 81);
  	public PackedDecimalData dfact29 = new PackedDecimalData(5, 0).isAPartOf(filler, 84);
  	public PackedDecimalData dfact30 = new PackedDecimalData(5, 0).isAPartOf(filler, 87);
  	public PackedDecimalData dfact31 = new PackedDecimalData(5, 0).isAPartOf(filler, 90);
  	public PackedDecimalData dfact32 = new PackedDecimalData(5, 0).isAPartOf(filler, 93);
  	public PackedDecimalData dfact33 = new PackedDecimalData(5, 0).isAPartOf(filler, 96);
  	public PackedDecimalData dfact34 = new PackedDecimalData(5, 0).isAPartOf(filler, 99);
  	public PackedDecimalData dfact35 = new PackedDecimalData(5, 0).isAPartOf(filler, 102);
  	public PackedDecimalData dfact36 = new PackedDecimalData(5, 0).isAPartOf(filler, 105);
  	public PackedDecimalData dfact37 = new PackedDecimalData(5, 0).isAPartOf(filler, 108);
  	public PackedDecimalData dfact38 = new PackedDecimalData(5, 0).isAPartOf(filler, 111);
  	public PackedDecimalData dfact39 = new PackedDecimalData(5, 0).isAPartOf(filler, 114);
  	public PackedDecimalData dfact40 = new PackedDecimalData(5, 0).isAPartOf(filler, 117);
  	public PackedDecimalData dfact41 = new PackedDecimalData(5, 0).isAPartOf(filler, 120);
  	public PackedDecimalData dfact42 = new PackedDecimalData(5, 0).isAPartOf(filler, 123);
  	public PackedDecimalData dfact43 = new PackedDecimalData(5, 0).isAPartOf(filler, 126);
  	public PackedDecimalData dfact44 = new PackedDecimalData(5, 0).isAPartOf(filler, 129);
  	public PackedDecimalData dfact45 = new PackedDecimalData(5, 0).isAPartOf(filler, 132);
  	public PackedDecimalData dfact46 = new PackedDecimalData(5, 0).isAPartOf(filler, 135);
  	public PackedDecimalData dfact47 = new PackedDecimalData(5, 0).isAPartOf(filler, 138);
  	public PackedDecimalData dfact48 = new PackedDecimalData(5, 0).isAPartOf(filler, 141);
  	public PackedDecimalData dfact49 = new PackedDecimalData(5, 0).isAPartOf(filler, 144);
  	public PackedDecimalData dfact50 = new PackedDecimalData(5, 0).isAPartOf(filler, 147);
  	public PackedDecimalData dfact51 = new PackedDecimalData(5, 0).isAPartOf(filler, 150);
  	public PackedDecimalData dfact52 = new PackedDecimalData(5, 0).isAPartOf(filler, 153);
  	public PackedDecimalData dfact53 = new PackedDecimalData(5, 0).isAPartOf(filler, 156);
  	public PackedDecimalData dfact54 = new PackedDecimalData(5, 0).isAPartOf(filler, 159);
  	public PackedDecimalData dfact55 = new PackedDecimalData(5, 0).isAPartOf(filler, 162);
  	public PackedDecimalData dfact56 = new PackedDecimalData(5, 0).isAPartOf(filler, 165);
  	public PackedDecimalData dfact57 = new PackedDecimalData(5, 0).isAPartOf(filler, 168);
  	public PackedDecimalData dfact58 = new PackedDecimalData(5, 0).isAPartOf(filler, 171);
  	public PackedDecimalData dfact59 = new PackedDecimalData(5, 0).isAPartOf(filler, 174);
  	public PackedDecimalData dfact60 = new PackedDecimalData(5, 0).isAPartOf(filler, 177);
  	public PackedDecimalData dfact61 = new PackedDecimalData(5, 0).isAPartOf(filler, 180);
  	public PackedDecimalData dfact62 = new PackedDecimalData(5, 0).isAPartOf(filler, 183);
  	public PackedDecimalData dfact63 = new PackedDecimalData(5, 0).isAPartOf(filler, 186);
  	public PackedDecimalData dfact64 = new PackedDecimalData(5, 0).isAPartOf(filler, 189);
  	public PackedDecimalData dfact65 = new PackedDecimalData(5, 0).isAPartOf(filler, 192);
  	public PackedDecimalData dfact66 = new PackedDecimalData(5, 0).isAPartOf(filler, 195);
  	public PackedDecimalData dfact67 = new PackedDecimalData(5, 0).isAPartOf(filler, 198);
  	public PackedDecimalData dfact68 = new PackedDecimalData(5, 0).isAPartOf(filler, 201);
  	public PackedDecimalData dfact69 = new PackedDecimalData(5, 0).isAPartOf(filler, 204);
  	public PackedDecimalData dfact70 = new PackedDecimalData(5, 0).isAPartOf(filler, 207);
  	public PackedDecimalData dfact71 = new PackedDecimalData(5, 0).isAPartOf(filler, 210);
  	public PackedDecimalData dfact72 = new PackedDecimalData(5, 0).isAPartOf(filler, 213);
  	public PackedDecimalData dfact73 = new PackedDecimalData(5, 0).isAPartOf(filler, 216);
  	public PackedDecimalData dfact74 = new PackedDecimalData(5, 0).isAPartOf(filler, 219);
  	public PackedDecimalData dfact75 = new PackedDecimalData(5, 0).isAPartOf(filler, 222);
  	public PackedDecimalData dfact76 = new PackedDecimalData(5, 0).isAPartOf(filler, 225);
  	public PackedDecimalData dfact77 = new PackedDecimalData(5, 0).isAPartOf(filler, 228);
  	public PackedDecimalData dfact78 = new PackedDecimalData(5, 0).isAPartOf(filler, 231);
  	public PackedDecimalData dfact79 = new PackedDecimalData(5, 0).isAPartOf(filler, 234);
  	public PackedDecimalData dfact80 = new PackedDecimalData(5, 0).isAPartOf(filler, 237);
  	public PackedDecimalData dfact81 = new PackedDecimalData(5, 0).isAPartOf(filler, 240);
  	public PackedDecimalData dfact82 = new PackedDecimalData(5, 0).isAPartOf(filler, 243);
  	public PackedDecimalData dfact83 = new PackedDecimalData(5, 0).isAPartOf(filler, 246);
  	public PackedDecimalData dfact84 = new PackedDecimalData(5, 0).isAPartOf(filler, 249);
  	public PackedDecimalData dfact85 = new PackedDecimalData(5, 0).isAPartOf(filler, 252);
  	public PackedDecimalData dfact86 = new PackedDecimalData(5, 0).isAPartOf(filler, 255);
  	public PackedDecimalData dfact87 = new PackedDecimalData(5, 0).isAPartOf(filler, 258);
  	public PackedDecimalData dfact88 = new PackedDecimalData(5, 0).isAPartOf(filler, 261);
  	public PackedDecimalData dfact89 = new PackedDecimalData(5, 0).isAPartOf(filler, 264);
  	public PackedDecimalData dfact90 = new PackedDecimalData(5, 0).isAPartOf(filler, 267);
  	public PackedDecimalData dfact91 = new PackedDecimalData(5, 0).isAPartOf(filler, 270);
  	public PackedDecimalData dfact92 = new PackedDecimalData(5, 0).isAPartOf(filler, 273);
  	public PackedDecimalData dfact93 = new PackedDecimalData(5, 0).isAPartOf(filler, 276);
  	public PackedDecimalData dfact94 = new PackedDecimalData(5, 0).isAPartOf(filler, 279);
  	public PackedDecimalData dfact95 = new PackedDecimalData(5, 0).isAPartOf(filler, 282);
  	public PackedDecimalData dfact96 = new PackedDecimalData(5, 0).isAPartOf(filler, 285);
  	public PackedDecimalData dfact97 = new PackedDecimalData(5, 0).isAPartOf(filler, 288);
  	public PackedDecimalData dfact98 = new PackedDecimalData(5, 0).isAPartOf(filler, 291);
  	public PackedDecimalData dfact99 = new PackedDecimalData(5, 0).isAPartOf(filler, 294);
  	public FixedLengthStringData facts = new FixedLengthStringData(33).isAPartOf(t6646Rec, 297);
  	public PackedDecimalData[] fact = PDArrayPartOfStructure(11, 5, 0, facts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(33).isAPartOf(facts, 0, FILLER_REDEFINE);
  	public PackedDecimalData fact01 = new PackedDecimalData(5, 0).isAPartOf(filler1, 0);
  	public PackedDecimalData fact02 = new PackedDecimalData(5, 0).isAPartOf(filler1, 3);
  	public PackedDecimalData fact03 = new PackedDecimalData(5, 0).isAPartOf(filler1, 6);
  	public PackedDecimalData fact04 = new PackedDecimalData(5, 0).isAPartOf(filler1, 9);
  	public PackedDecimalData fact05 = new PackedDecimalData(5, 0).isAPartOf(filler1, 12);
  	public PackedDecimalData fact06 = new PackedDecimalData(5, 0).isAPartOf(filler1, 15);
  	public PackedDecimalData fact07 = new PackedDecimalData(5, 0).isAPartOf(filler1, 18);
  	public PackedDecimalData fact08 = new PackedDecimalData(5, 0).isAPartOf(filler1, 21);
  	public PackedDecimalData fact09 = new PackedDecimalData(5, 0).isAPartOf(filler1, 24);
  	public PackedDecimalData fact10 = new PackedDecimalData(5, 0).isAPartOf(filler1, 27);
  	public PackedDecimalData fact11 = new PackedDecimalData(5, 0).isAPartOf(filler1, 30);


	public void initialize() {
		COBOLFunctions.initialize(t6646Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6646Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}