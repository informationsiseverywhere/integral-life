/*
 * File: P5533.java
 * Date: 30 August 2009 0:29:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5533.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.unitlinkedprocessing.procedures.T5533pt;
import com.csc.life.unitlinkedprocessing.screens.S5533ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5533rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;	//ILIFE-7420
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5533 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5533");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();	//ILIFE-7420
	private T5533rec t5533rec = new T5533rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private boolean isContIncrLimits = false; //ALS-4566
	private S5533ScreenVars sv = ScreenProgram.getScreenVars( S5533ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5533() {
		super();
		screenVars = sv;
		new ScreenModel("S5533", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		isContIncrLimits = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM005", appVars, "IT"); //ALS-4566
		if(isContIncrLimits) {
			sv.contIncrLimitFlag.set("Y");
		}
		else
			sv.contIncrLimitFlag.set("N");
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		/*	START OF ILIFE-7420	*/
		itdmIO.setDataKey(wsspsmart.itmdkey);
		itdmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
		/*	END OF ILIFE-7420	*/
	}

protected void readRecord1031()
	{
		/*	START OF ILIFE-7420	*/
		descIO.setDescpfx(itdmIO.getItempfx());
		descIO.setDesccoy(itdmIO.getItemcoy());
		descIO.setDesctabl(itdmIO.getItemtabl());
		descIO.setDescitem(itdmIO.getItemitem());
		/*	END OF ILIFE-7420	*/
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		/*	START OF ILIFE-7420	*/
		sv.company.set(itdmIO.getItemcoy());
		sv.tabl.set(itdmIO.getItemtabl());
		sv.item.set(itdmIO.getItemitem());
		/*	END OF ILIFE-7420	*/
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		/*	START OF ILIFE-7420	*/
		t5533rec.t5533Rec.set(itdmIO.getGenarea());
		if (isNE(itdmIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		/*	END OF ILIFE-7420	*/
		t5533rec.cmax01.set(ZERO);
		t5533rec.cmax02.set(ZERO);
		t5533rec.cmax03.set(ZERO);
		t5533rec.cmax04.set(ZERO);
		t5533rec.cmax05.set(ZERO);
		t5533rec.cmax06.set(ZERO);
		t5533rec.cmax07.set(ZERO);
		t5533rec.cmax08.set(ZERO);
		t5533rec.cmin01.set(ZERO);
		t5533rec.cmin02.set(ZERO);
		t5533rec.cmin03.set(ZERO);
		t5533rec.cmin04.set(ZERO);
		t5533rec.cmin05.set(ZERO);
		t5533rec.cmin06.set(ZERO);
		t5533rec.cmin07.set(ZERO);
		t5533rec.cmin08.set(ZERO);
		if(isContIncrLimits) {
			t5533rec.cIncrmin01.set(ZERO);
			t5533rec.cIncrmin02.set(ZERO);
			t5533rec.cIncrmin03.set(ZERO);
			t5533rec.cIncrmin04.set(ZERO);
			t5533rec.cIncrmin05.set(ZERO);
			t5533rec.cIncrmin06.set(ZERO);
			t5533rec.cIncrmin07.set(ZERO);
			t5533rec.cIncrmin08.set(ZERO);
			t5533rec.cIncrmax01.set(ZERO);
			t5533rec.cIncrmax02.set(ZERO);
			t5533rec.cIncrmax03.set(ZERO);
			t5533rec.cIncrmax04.set(ZERO);
			t5533rec.cIncrmax05.set(ZERO);
			t5533rec.cIncrmax06.set(ZERO);
			t5533rec.cIncrmax07.set(ZERO);
			t5533rec.cIncrmax08.set(ZERO);	
		}		
		t5533rec.casualContribMax.set(ZERO);
		t5533rec.casualContribMin.set(ZERO);
		t5533rec.premUnit.set(ZERO);
		t5533rec.rndfact.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.cmaxs.set(t5533rec.cmaxs);
		sv.cmins.set(t5533rec.cmins);
		sv.cIncrmaxs.set(t5533rec.cIncrmaxs);
		sv.cIncrmins.set(t5533rec.cIncrmins);
		sv.casualContribMax.set(t5533rec.casualContribMax);
		sv.casualContribMin.set(t5533rec.casualContribMin);
		sv.frequencys.set(t5533rec.frequencys);
		if (isEQ(itdmIO.getItmfrm(),0)) {	//ILIFE-7420
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itdmIO.getItmfrm());	//ILIFE-7420
		}
		if (isEQ(itdmIO.getItmto(),0)) {	//ILIFE-7420
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itdmIO.getItmto());	//ILIFE-7420
		}
		sv.premUnit.set(t5533rec.premUnit);
		sv.rndfact.set(t5533rec.rndfact);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		/*	START OF ILIFE-7420	*/
		itdmIO.setFunction(varcom.readh);
		itdmIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/*	END OF ILIFE-7420	*/
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itdmIO.setTranid(varcom.vrcmCompTranid);	//ILIFE-7420
	}

protected void updateRecord3055()
	{
		/*	START OF ILIFE-7420	*/
		itdmIO.setTableprog(wsaaProg);
		itdmIO.setGenarea(t5533rec.t5533Rec);
		itdmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/*	END OF ILIFE-7420	*/
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.cmaxs,t5533rec.cmaxs)) {
			t5533rec.cmaxs.set(sv.cmaxs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cmins,t5533rec.cmins)) {
			t5533rec.cmins.set(sv.cmins);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.casualContribMax,t5533rec.casualContribMax)) {
			t5533rec.casualContribMax.set(sv.casualContribMax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.casualContribMin,t5533rec.casualContribMin)) {
			t5533rec.casualContribMin.set(sv.casualContribMin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.frequencys,t5533rec.frequencys)) {
			t5533rec.frequencys.set(sv.frequencys);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itdmIO.getItmfrm())) {	//ILIFE-7420
			itdmIO.setItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itdmIO.getItmto())) {	//ILIFE-7420
			itdmIO.setItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premUnit,t5533rec.premUnit)) {
			t5533rec.premUnit.set(sv.premUnit);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rndfact,t5533rec.rndfact)) {
			t5533rec.rndfact.set(sv.rndfact);
			wsaaUpdateFlag = "Y";
		}
		if(isContIncrLimits) {
			if (isNE(sv.cIncrmaxs,t5533rec.cIncrmaxs)) {
				t5533rec.cIncrmaxs.set(sv.cIncrmaxs);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(sv.cIncrmins,t5533rec.cIncrmins)) {
				t5533rec.cIncrmins.set(sv.cIncrmins);
				wsaaUpdateFlag = "Y";
			}
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5533pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
