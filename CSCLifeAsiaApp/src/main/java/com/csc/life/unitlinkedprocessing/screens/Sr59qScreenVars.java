package com.csc.life.unitlinkedprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr59q
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class Sr59qScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(111);
	public FixedLengthStringData dataFields = new FixedLengthStringData(15).isAPartOf(dataArea, 0);
	public FixedLengthStringData gmib = DD.conprosal.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData gmdb = DD.continueVar.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData gmwb = DD.contreq.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData gmibsel = DD.bnfgrp.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData gmdbsel = DD.functn.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData gmwbsel = DD.caccy.copy().isAPartOf(dataFields,11);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 15);
	public FixedLengthStringData gmibErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData gmdbErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData gmwbErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData gmibselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData gmdbselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData gmwbselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea,39);
	public FixedLengthStringData[] gmibOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] gmdbOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] gmwbOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] gmibselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] gmdbselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] gmwbselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData gmiblongdesc = new FixedLengthStringData(30);	
	public FixedLengthStringData gmdblongdesc = new FixedLengthStringData(30);
	public FixedLengthStringData gmwblongdesc = new FixedLengthStringData(30);
	
	public LongData Sr59qscreenWritten = new LongData(0);
	public LongData Sr59qwindowWritten = new LongData(0);
	public LongData Sr59qhideWritten = new LongData(0);
	public LongData Sr59qprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr59qScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(gmibOut,new String[] {"3", "4", "-3", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gmdbOut,new String[] {"5", "6", "-5", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gmibselOut,new String[] {"7", "8", "-7", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gmdbselOut,new String[] {"9", "10", "-9", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gmwbOut,new String[] {"11", "12", "-11", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gmwbselOut,new String[] {"13", "14", "-13", null, null, null, null, null, null, null, null, null});
		/*BRD-306 END */
		screenFields = new BaseData[] {gmib, gmdb, gmwb, gmibsel, gmdbsel,gmwbsel };
		screenOutFields = new BaseData[][] {gmibOut, gmdbOut, gmwbOut, gmibselOut, gmdbselOut, gmwbselOut};
		screenErrFields = new BaseData[] {gmibErr, gmdbErr, gmwbErr, gmibselErr, gmdbselErr, gmwbselErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr59qscreen.class;
		protectRecord = Sr59qprotect.class;
	}

}