/*
 * File: P6317.java
 * Date: 30 August 2009 0:43:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P6317.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.unitlinkedprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcupdTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Wsspuprc;
import com.csc.smart.cls.Jobsws;
import com.csc.smart.recordstructures.Jobswsrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.programs.Infosdsp;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* P6317 - Activate Unit Bid/Offer Prices.
*
* This program updates the valid flag in VPRCPF.
*
* The records  being  updated are identified by  Company/Date/Job
* number. This program only works on fund records that have bare,
* bid and offer price.
*
* It is called from the Unit  Linked  Entry  submenu  (P5412)  by
* means of selecting the appropriate option.
*
*
*****************************************************************
* </pre>
*/
public class P6317 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6317");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final String wsaaLoadMessage = "               Activate Bid/Offer Prices            ";
		/* FORMATS */
	private static final String vprcupdrec = "VPRCUPDREC";
	private VprcupdTableDAM vprcupdIO = new VprcupdTableDAM();
	private Jobswsrec jobswsrec = new Jobswsrec();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private Wsspuprc wsspuprc = new Wsspuprc();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		callVprcupdio2005
	}

	public P6317() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		wsspuprc.userArea = convertAndSetParam(wsspuprc.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.errorline.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		callProgram(Jobsws.class, jobswsrec.jobswsRec);
		if (isEQ(jobswsrec.switch1, "0")) {
			scrnparams.errorline.set(wsaaLoadMessage);
			scrnparams.function.set(varcom.info);
			callProgram(Infosdsp.class, scrnparams.screenParams);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     CALL VIRTUAL PRICE AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					vprcupdio2001();
				case callVprcupdio2005: 
					callVprcupdio2005();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Read VPRCUPD to select all those records that has the
	* required company, date and job number.
	* </pre>
	*/
protected void vprcupdio2001()
	{
		vprcupdIO.setDataArea(SPACES);
		vprcupdIO.setFunction(varcom.begn);
		
        //performance improvement -- < Niharika Modi >
        vprcupdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
        vprcupdIO.setFitKeysSearch("COMPANY","EFFDATE","JOBNO");
        
		vprcupdIO.setFormat(vprcupdrec);
		vprcupdIO.setCompany(wsspcomn.company);
		vprcupdIO.setEffdate(wsspuprc.uprcEffdate);
		vprcupdIO.setJobno(wsspuprc.uprcJobno);
	}

protected void callVprcupdio2005()
	{
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isEQ(vprcupdIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(vprcupdIO.getStatuz());
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		if ((isNE(vprcupdIO.getEffdate(), wsspuprc.uprcEffdate))
		|| (isNE(vprcupdIO.getCompany(), wsspcomn.company))
		|| (isNE(vprcupdIO.getJobno(), wsspuprc.uprcJobno))) {
			return ;
		}
		/* Chcck if bid and offer price has been calculated.*/
		vflagcpy.validFlag.set(vprcupdIO.getValidflag());
		if ((isNE(vprcupdIO.getProcflag(), "Y"))
		|| !vflagcpy.prop.isTrue()) {
			vprcupdIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callVprcupdio2005);
		}
		updateVprcupd5000();
		vprcupdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callVprcupdio2005);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-PAR*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void updateVprcupd5000()
	{
		para5000();
	}

protected void para5000()
	{
		/* VFLG-IN-FORCE-VAL (1) denotes the price is valid.*/
		vprcupdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
		vprcupdIO.setValidflag(vflagcpy.inForceVal);
		vprcupdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, vprcupdIO);
		if (isNE(vprcupdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(vprcupdIO.getParams());
			fatalError600();
		}
	}
}
