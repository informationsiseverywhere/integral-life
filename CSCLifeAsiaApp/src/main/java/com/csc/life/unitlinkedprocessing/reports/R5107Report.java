package com.csc.life.unitlinkedprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5107.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5107Report extends SMARTReportLayout { 

	private ZonedDecimalData age = new ZonedDecimalData(3, 0);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData cntamnt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData fundrate = new ZonedDecimalData(18, 9);
	private FixedLengthStringData lapind = new FixedLengthStringData(1);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private FixedLengthStringData rectype = new FixedLengthStringData(1);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0);
	private FixedLengthStringData triger = new FixedLengthStringData(10);
	private FixedLengthStringData unityp = new FixedLengthStringData(1);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5107Report() {
		super();
	}


	/**
	 * Print the XML for R5107d01
	 */
	public void printR5107d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		rectype.setFieldName("rectype");
		rectype.setInternal(subString(recordData, 1, 1));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 2, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 10, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 12, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 14, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 16, 4));
		vrtfnd.setFieldName("vrtfnd");
		vrtfnd.setInternal(subString(recordData, 20, 4));
		unityp.setFieldName("unityp");
		unityp.setInternal(subString(recordData, 24, 1));
		tranno.setFieldName("tranno");
		tranno.setInternal(subString(recordData, 25, 5));
		batctrcde.setFieldName("batctrcde");
		batctrcde.setInternal(subString(recordData, 30, 4));
		cntamnt.setFieldName("cntamnt");
		cntamnt.setInternal(subString(recordData, 34, 17));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 51, 3));
		fundamnt.setFieldName("fundamnt");
		fundamnt.setInternal(subString(recordData, 54, 17));
		fndcurr.setFieldName("fndcurr");
		fndcurr.setInternal(subString(recordData, 71, 3));
		fundrate.setFieldName("fundrate");
		fundrate.setInternal(subString(recordData, 74, 18));
		lapind.setFieldName("lapind");
		lapind.setInternal(subString(recordData, 92, 1));
		age.setFieldName("age");
		age.setInternal(subString(recordData, 93, 3));
		triger.setFieldName("triger");
		triger.setInternal(subString(recordData, 96, 10));
		printLayout("R5107d01",			// Record name
			new BaseData[]{			// Fields:
				rectype,
				chdrnum,
				life,
				coverage,
				rider,
				plnsfx,
				vrtfnd,
				unityp,
				tranno,
				batctrcde,
				cntamnt,
				cntcurr,
				fundamnt,
				fndcurr,
				fundrate,
				lapind,
				age,
				triger
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5107e01
	 */
	public void printR5107e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R5107e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5107h01
	 */
	public void printR5107h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5107h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(12);
	}


}
