package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:23
 * Description:
 * Copybook name: ZRSTNUDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrstnudkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrstnudFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrstnudKey = new FixedLengthStringData(64).isAPartOf(zrstnudFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrstnudChdrcoy = new FixedLengthStringData(1).isAPartOf(zrstnudKey, 0);
  	public FixedLengthStringData zrstnudChdrnum = new FixedLengthStringData(8).isAPartOf(zrstnudKey, 1);
  	public FixedLengthStringData zrstnudLife = new FixedLengthStringData(2).isAPartOf(zrstnudKey, 9);
  	public FixedLengthStringData zrstnudCoverage = new FixedLengthStringData(2).isAPartOf(zrstnudKey, 11);
  	public FixedLengthStringData zrstnudRider = new FixedLengthStringData(2).isAPartOf(zrstnudKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(zrstnudKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrstnudFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrstnudFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}