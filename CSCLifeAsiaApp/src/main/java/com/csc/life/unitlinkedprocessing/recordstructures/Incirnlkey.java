package com.csc.life.unitlinkedprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:57
 * Description:
 * Copybook name: INCIRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incirnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incirnlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incirnlKey = new FixedLengthStringData(64).isAPartOf(incirnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData incirnlChdrcoy = new FixedLengthStringData(1).isAPartOf(incirnlKey, 0);
  	public FixedLengthStringData incirnlChdrnum = new FixedLengthStringData(8).isAPartOf(incirnlKey, 1);
  	public FixedLengthStringData incirnlLife = new FixedLengthStringData(2).isAPartOf(incirnlKey, 9);
  	public FixedLengthStringData incirnlCoverage = new FixedLengthStringData(2).isAPartOf(incirnlKey, 11);
  	public FixedLengthStringData incirnlRider = new FixedLengthStringData(2).isAPartOf(incirnlKey, 13);
  	public PackedDecimalData incirnlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incirnlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incirnlKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incirnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incirnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}