package com.csc.life.unitlinkedprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:46
 * Description:
 * Copybook name: T5545REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5545rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5545Rec = new FixedLengthStringData(372);
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(12).isAPartOf(t5545Rec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(6, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData enhanaPcs = new FixedLengthStringData(15).isAPartOf(t5545Rec, 12);
  	public PackedDecimalData[] enhanaPc = PDArrayPartOfStructure(5, 5, 2, enhanaPcs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(15).isAPartOf(enhanaPcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanaPc01 = new PackedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public PackedDecimalData enhanaPc02 = new PackedDecimalData(5, 2).isAPartOf(filler1, 3);
  	public PackedDecimalData enhanaPc03 = new PackedDecimalData(5, 2).isAPartOf(filler1, 6);
  	public PackedDecimalData enhanaPc04 = new PackedDecimalData(5, 2).isAPartOf(filler1, 9);
  	public PackedDecimalData enhanaPc05 = new PackedDecimalData(5, 2).isAPartOf(filler1, 12);
  	public FixedLengthStringData enhanaPrms = new FixedLengthStringData(45).isAPartOf(t5545Rec, 27);
  	public PackedDecimalData[] enhanaPrm = PDArrayPartOfStructure(5, 17, 2, enhanaPrms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(enhanaPrms, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanaPrm01 = new PackedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public PackedDecimalData enhanaPrm02 = new PackedDecimalData(17, 2).isAPartOf(filler2, 9);
  	public PackedDecimalData enhanaPrm03 = new PackedDecimalData(17, 2).isAPartOf(filler2, 18);
  	public PackedDecimalData enhanaPrm04 = new PackedDecimalData(17, 2).isAPartOf(filler2, 27);
  	public PackedDecimalData enhanaPrm05 = new PackedDecimalData(17, 2).isAPartOf(filler2, 36);
  	public FixedLengthStringData enhanbPcs = new FixedLengthStringData(15).isAPartOf(t5545Rec, 72);
  	public PackedDecimalData[] enhanbPc = PDArrayPartOfStructure(5, 5, 2, enhanbPcs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(15).isAPartOf(enhanbPcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanbPc01 = new PackedDecimalData(5, 2).isAPartOf(filler3, 0);
  	public PackedDecimalData enhanbPc02 = new PackedDecimalData(5, 2).isAPartOf(filler3, 3);
  	public PackedDecimalData enhanbPc03 = new PackedDecimalData(5, 2).isAPartOf(filler3, 6);
  	public PackedDecimalData enhanbPc04 = new PackedDecimalData(5, 2).isAPartOf(filler3, 9);
  	public PackedDecimalData enhanbPc05 = new PackedDecimalData(5, 2).isAPartOf(filler3, 12);
  	public FixedLengthStringData enhanbPrms = new FixedLengthStringData(45).isAPartOf(t5545Rec, 87);
  	public PackedDecimalData[] enhanbPrm = PDArrayPartOfStructure(5, 17, 2, enhanbPrms, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(45).isAPartOf(enhanbPrms, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanbPrm01 = new PackedDecimalData(17, 2).isAPartOf(filler4, 0);
  	public PackedDecimalData enhanbPrm02 = new PackedDecimalData(17, 2).isAPartOf(filler4, 9);
  	public PackedDecimalData enhanbPrm03 = new PackedDecimalData(17, 2).isAPartOf(filler4, 18);
  	public PackedDecimalData enhanbPrm04 = new PackedDecimalData(17, 2).isAPartOf(filler4, 27);
  	public PackedDecimalData enhanbPrm05 = new PackedDecimalData(17, 2).isAPartOf(filler4, 36);
  	public FixedLengthStringData enhancPcs = new FixedLengthStringData(15).isAPartOf(t5545Rec, 132);
  	public PackedDecimalData[] enhancPc = PDArrayPartOfStructure(5, 5, 2, enhancPcs, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(15).isAPartOf(enhancPcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhancPc01 = new PackedDecimalData(5, 2).isAPartOf(filler5, 0);
  	public PackedDecimalData enhancPc02 = new PackedDecimalData(5, 2).isAPartOf(filler5, 3);
  	public PackedDecimalData enhancPc03 = new PackedDecimalData(5, 2).isAPartOf(filler5, 6);
  	public PackedDecimalData enhancPc04 = new PackedDecimalData(5, 2).isAPartOf(filler5, 9);
  	public PackedDecimalData enhancPc05 = new PackedDecimalData(5, 2).isAPartOf(filler5, 12);
  	public FixedLengthStringData enhancPrms = new FixedLengthStringData(45).isAPartOf(t5545Rec, 147);
  	public PackedDecimalData[] enhancPrm = PDArrayPartOfStructure(5, 17, 2, enhancPrms, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(45).isAPartOf(enhancPrms, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhancPrm01 = new PackedDecimalData(17, 2).isAPartOf(filler6, 0);
  	public PackedDecimalData enhancPrm02 = new PackedDecimalData(17, 2).isAPartOf(filler6, 9);
  	public PackedDecimalData enhancPrm03 = new PackedDecimalData(17, 2).isAPartOf(filler6, 18);
  	public PackedDecimalData enhancPrm04 = new PackedDecimalData(17, 2).isAPartOf(filler6, 27);
  	public PackedDecimalData enhancPrm05 = new PackedDecimalData(17, 2).isAPartOf(filler6, 36);
  	public FixedLengthStringData enhandPcs = new FixedLengthStringData(15).isAPartOf(t5545Rec, 192);
  	public PackedDecimalData[] enhandPc = PDArrayPartOfStructure(5, 5, 2, enhandPcs, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(enhandPcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhandPc01 = new PackedDecimalData(5, 2).isAPartOf(filler7, 0);
  	public PackedDecimalData enhandPc02 = new PackedDecimalData(5, 2).isAPartOf(filler7, 3);
  	public PackedDecimalData enhandPc03 = new PackedDecimalData(5, 2).isAPartOf(filler7, 6);
  	public PackedDecimalData enhandPc04 = new PackedDecimalData(5, 2).isAPartOf(filler7, 9);
  	public PackedDecimalData enhandPc05 = new PackedDecimalData(5, 2).isAPartOf(filler7, 12);
  	public FixedLengthStringData enhandPrms = new FixedLengthStringData(45).isAPartOf(t5545Rec, 207);
  	public PackedDecimalData[] enhandPrm = PDArrayPartOfStructure(5, 17, 2, enhandPrms, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(45).isAPartOf(enhandPrms, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhandPrm01 = new PackedDecimalData(17, 2).isAPartOf(filler8, 0);
  	public PackedDecimalData enhandPrm02 = new PackedDecimalData(17, 2).isAPartOf(filler8, 9);
  	public PackedDecimalData enhandPrm03 = new PackedDecimalData(17, 2).isAPartOf(filler8, 18);
  	public PackedDecimalData enhandPrm04 = new PackedDecimalData(17, 2).isAPartOf(filler8, 27);
  	public PackedDecimalData enhandPrm05 = new PackedDecimalData(17, 2).isAPartOf(filler8, 36);
  	public FixedLengthStringData enhanePcs = new FixedLengthStringData(15).isAPartOf(t5545Rec, 252);
  	public PackedDecimalData[] enhanePc = PDArrayPartOfStructure(5, 5, 2, enhanePcs, 0);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(15).isAPartOf(enhanePcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanePc01 = new PackedDecimalData(5, 2).isAPartOf(filler9, 0);
  	public PackedDecimalData enhanePc02 = new PackedDecimalData(5, 2).isAPartOf(filler9, 3);
  	public PackedDecimalData enhanePc03 = new PackedDecimalData(5, 2).isAPartOf(filler9, 6);
  	public PackedDecimalData enhanePc04 = new PackedDecimalData(5, 2).isAPartOf(filler9, 9);
  	public PackedDecimalData enhanePc05 = new PackedDecimalData(5, 2).isAPartOf(filler9, 12);
  	public FixedLengthStringData enhanePrms = new FixedLengthStringData(45).isAPartOf(t5545Rec, 267);
  	public PackedDecimalData[] enhanePrm = PDArrayPartOfStructure(5, 17, 2, enhanePrms, 0);
  	public FixedLengthStringData filler10 = new FixedLengthStringData(45).isAPartOf(enhanePrms, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanePrm01 = new PackedDecimalData(17, 2).isAPartOf(filler10, 0);
  	public PackedDecimalData enhanePrm02 = new PackedDecimalData(17, 2).isAPartOf(filler10, 9);
  	public PackedDecimalData enhanePrm03 = new PackedDecimalData(17, 2).isAPartOf(filler10, 18);
  	public PackedDecimalData enhanePrm04 = new PackedDecimalData(17, 2).isAPartOf(filler10, 27);
  	public PackedDecimalData enhanePrm05 = new PackedDecimalData(17, 2).isAPartOf(filler10, 36);
  	public FixedLengthStringData enhanfPcs = new FixedLengthStringData(15).isAPartOf(t5545Rec, 312);
  	public PackedDecimalData[] enhanfPc = PDArrayPartOfStructure(5, 5, 2, enhanfPcs, 0);
  	public FixedLengthStringData filler11 = new FixedLengthStringData(15).isAPartOf(enhanfPcs, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanfPc01 = new PackedDecimalData(5, 2).isAPartOf(filler11, 0);
  	public PackedDecimalData enhanfPc02 = new PackedDecimalData(5, 2).isAPartOf(filler11, 3);
  	public PackedDecimalData enhanfPc03 = new PackedDecimalData(5, 2).isAPartOf(filler11, 6);
  	public PackedDecimalData enhanfPc04 = new PackedDecimalData(5, 2).isAPartOf(filler11, 9);
  	public PackedDecimalData enhanfPc05 = new PackedDecimalData(5, 2).isAPartOf(filler11, 12);
  	public FixedLengthStringData enhanfPrms = new FixedLengthStringData(45).isAPartOf(t5545Rec, 327);
  	public PackedDecimalData[] enhanfPrm = PDArrayPartOfStructure(5, 17, 2, enhanfPrms, 0);
  	public FixedLengthStringData filler12 = new FixedLengthStringData(45).isAPartOf(enhanfPrms, 0, FILLER_REDEFINE);
  	public PackedDecimalData enhanfPrm01 = new PackedDecimalData(17, 2).isAPartOf(filler12, 0);
  	public PackedDecimalData enhanfPrm02 = new PackedDecimalData(17, 2).isAPartOf(filler12, 9);
  	public PackedDecimalData enhanfPrm03 = new PackedDecimalData(17, 2).isAPartOf(filler12, 18);
  	public PackedDecimalData enhanfPrm04 = new PackedDecimalData(17, 2).isAPartOf(filler12, 27);
  	public PackedDecimalData enhanfPrm05 = new PackedDecimalData(17, 2).isAPartOf(filler12, 36);


	public void initialize() {
		COBOLFunctions.initialize(t5545Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5545Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}